# Description
Please include summary why code change is beneficial for Lineage2TS project. Include as many words as you see fit.
Such description can include comparison in functionality between old and new changes, or any other demonstrated benefit.

# Testing
Please specify how code change was tested, or how to replicate steps of testing you performed.

Before code change can be accepted, please ensure that:

- [ ] Code builds and lints without any errors
- [ ] Game server starts and runs without errors
- [ ] You have tested at least one affected functionality touched by code changes

Thank you for your contribution to the Lineage2TS project.