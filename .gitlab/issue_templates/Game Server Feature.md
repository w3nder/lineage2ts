(Replace this hint with a summarised description of feature.)

*New feature behaviour*

**:clipboard: Steps to verify functionality**
1. (if feature is added, how do you know it works)
1.
1.

**:mag: Background information**

* (useful links to any online sources, discussions, articles, or existing code example)
* (if related to game mechanics, specify item or npc ids, location in game world/map)
* (if technical improvement, any previous research metrics)