#!/bin/bash

loginpid=0
gamepid=0

# SIGUSR1-handler
# SIGUSR2-handler
user_handler() {
  if [ $loginpid -ne 0 ]; then
    kill -SIGTERM "$loginpid"
    wait "$loginpid"
  fi

  if [ $gamepid -ne 0 ]; then
    kill -SIGTERM "$gamepid"
    wait "$gamepid"
  fi

  exit 130; # 128 + 2 -- SIGUSR1
}

# SIGTERM-handler
# SIGINT-handler
sigterm_handler() {
  if [ $loginpid -ne 0 ]; then
    kill -SIGTERM "$loginpid"
    wait "$loginpid"
  fi

  if [ $gamepid -ne 0 ]; then
    kill -SIGTERM "$gamepid"
    wait "$gamepid"
  fi

  exit 143; # 128 + 15 -- SIGTERM
}

trap 'kill ${!}; user_handler' SIGUSR1
trap 'kill ${!}; user_handler' SIGUSR2
trap 'kill ${!}; sigterm_handler' SIGTERM
trap 'kill ${!}; sigterm_handler' SIGINT

# login server
exec node /opt/lineage2ts/login-server/dist/source/Start.js &
loginpid="$!"

# game server
exec node /opt/lineage2ts/game-server/dist/source/Start.js &
gamepid="$!"

# wait forever
while true
do
  tail -f /dev/null & wait ${!}
done