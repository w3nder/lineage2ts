import { ReadableClientPacket } from 'lineage2ts-testing/source/client/packets/ReadableClientPacket'
import { LoginSuccessEvent } from 'lineage2ts-testing/source/client/packets/login/receive/LoginSuccess'

export interface RequestServerLoginEvent extends LoginSuccessEvent {
    serverId: number
}

export function RequestServerLogin( packet: ReadableClientPacket ) : RequestServerLoginEvent {
    const one = packet.readD(), two = packet.readD(), serverId = packet.readC()

    return {
        one,
        serverId,
        two
    }
}