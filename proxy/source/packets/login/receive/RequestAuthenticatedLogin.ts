import { ReadableClientPacket } from 'lineage2ts-testing/source/client/packets/ReadableClientPacket'
import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'

export interface RequestAuthenticatedLoginEvent extends PacketEvent {
    encryptedData : Buffer
    sessionId: number
}

export function RequestAuthenticatedLogin( packet: ReadableClientPacket ) : RequestAuthenticatedLoginEvent {
    const encryptedData = packet.readB( 128 ), sessionId = packet.readD()

    return {
        encryptedData,
        sessionId
    }
}