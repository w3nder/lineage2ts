import {
    LoginServerListEvent,
    ServerCharacterDetails,
    ServerStatus
} from 'lineage2ts-testing/source/client/packets/login/receive/ServerList'
import { DeclaredServerPacket } from 'lineage2ts-login/source/packets/DeclaredServerPacket'

export function ServerList( data : LoginServerListEvent, ipOverride : Array<number>, portOverride: number ) : Buffer {
    let allExpirations = data.charactersOnServers.reduce( ( total: number, server: ServerCharacterDetails ) : number => {
        return total + server.accountExpirations.length
    }, 0 )

    let packet = new DeclaredServerPacket( 6 + data.availableServers.length * 21 + data.charactersOnServers.length * 3 + allExpirations * 4 )
        .writeC( 0x04 )
        .writeC( data.availableServers.length )
        .writeC( data.lastServerId )

    for ( const server of data.availableServers ) {
        packet
            .writeC( server.id )
            .writeC( ipOverride[ 0 ] )
            .writeC( ipOverride[ 1 ] )
            .writeC( ipOverride[ 2 ] )

            .writeC( ipOverride[ 3 ] )
            .writeD( portOverride )
            .writeC( server.ageLimit ) // Age Limit 0, 15, 18
            .writeC( server.isPVP ? 0x01 : 0x00 )

            .writeH( server.onlinePlayers )
            .writeH( server.maximumPlayers )
            .writeC( server.status === ServerStatus.Down ? 0x00 : 0x01 )
            .writeD( server.serverType ) // 1: Normal, 2: Relax, 4: Public Test, 8: No Label, 16: Character Creation Restricted, 32: Event, 64: Free

            .writeC( server.usingBrackets ? 0x01 : 0x00 )
    }

    for ( const server of data.charactersOnServers ) {
        packet
            .writeC( server.serverId )
            .writeC( server.ownedCharacterAmount )
            .writeC( server.accountExpirations.length )

        if ( server.accountExpirations.length > 0 ) {
            for ( const expirationSeconds of server.accountExpirations ) {
                packet.writeD( expirationSeconds )
            }
        }
    }

    return packet.getBuffer()
}