import { PacketMethodMap } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'
import { RequestServerLogin } from './login/receive/RequestServerLogin'
import { RequestAuthenticatedLogin } from './login/receive/RequestAuthenticatedLogin'
import { RequestServerList } from './login/receive/RequestServerList'
import { RequestAuthenticatedGG } from './login/receive/RequestAuthenticatedGG'

export const LoginClientToServerMethods : PacketMethodMap = {
    0x00: RequestAuthenticatedLogin,
    0x02: RequestServerLogin,
    0x05: RequestServerList,
    0x07: RequestAuthenticatedGG
}