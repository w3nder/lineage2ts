import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'

export type ProxyPacketModifierMethod = ( name: string, event: PacketEvent, rawPacket: Buffer ) => Buffer
export type ProxyPacketModifier = ( event: PacketEvent, rawPacket: Buffer ) => Buffer