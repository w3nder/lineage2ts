import { ReadableClientPacket } from 'lineage2ts-testing/source/client/packets/ReadableClientPacket'
import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'

export interface ProtocolVersionEvent extends PacketEvent {
    version: number
}

export function ProtocolVersion( packet : ReadableClientPacket ) : ProtocolVersionEvent {
    return {
        version: packet.readD()
    }
}