import { LoginClientConnection } from './connection/LoginClientConnection'
import { LoginServerConnection } from './connection/LoginServerConnection'
import { Socket } from 'net'
import { LoginEventIndicator } from '../enum/LoginEventIndicators'
import { GameClientConnection } from './connection/GameClientConnection'
import { GameServerConnection } from './connection/GameServerConnection'
import { GameEventIndicator } from '../enum/GameEventIndicator'
import { ServerLog } from '../logger/Logger'
import { AuthenticatedLogin } from 'lineage2ts-testing/source/client/packets/game/send/AuthenticatedLogin'
import { ClientCache } from '../cache/ClientCache'
import _ from 'lodash'
import { ProtocolVersion, ProtocolVersionEvent } from '../packets/game/receive/ProtocolVersion'
import chalk from 'chalk'
import { ListenerCache } from '../cache/ListenerCache'
import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'
import { ListenerType } from '../models/ListenerDefinition'
import { PlayerSay, PlayerSayEvent } from '../packets/game/receive/PlayerSay'
import { ProxyCommands } from '../enum/ProxyCommands'
import { BypassToServer, BypassToServerEvent } from '../packets/game/receive/BypassToServer'
import { ConfigManager } from '../config/ConfigManager'

export class ClientProxy {
    loginClient: LoginClientConnection
    loginServer: LoginServerConnection

    gameClient: GameClientConnection
    gameServer: GameServerConnection

    disconnectTimeout: NodeJS.Timeout
    joinClientsTimeout: NodeJS.Timeout

    constructor( clientConnection: Socket, port: number, host: string, proxyHost : string ) {
        let loginBlowfishKey = this.generateLoginClientBlowfishKey()

        this.loginClient = new LoginClientConnection( this.onLoginClientEvent.bind( this ), clientConnection, loginBlowfishKey )
        this.loginServer = new LoginServerConnection( this.onLoginServerEvent.bind( this ), port, host, proxyHost, loginBlowfishKey )

        this.loginClient.proxyToConnection( this.loginServer )
        this.loginServer.proxyToConnection( this.loginClient )
    }

    onLoginClientEvent( indicator: LoginEventIndicator ) : void {
        switch ( indicator ) {
            case LoginEventIndicator.ConnectionTerminated:
                ServerLog.trace( 'Login Client closed connection' )
                return this.loginServer.abortConnection()
        }
    }

    onLoginServerEvent( indicator: LoginEventIndicator ) : void {
        switch ( indicator ) {
            case LoginEventIndicator.ConnectionTerminated:
                ServerLog.trace( 'Login Server closed connection' )
                return this.loginClient.abortConnection()

            case LoginEventIndicator.FlowFinished:
                return this.registerWaitingClient()
        }
    }

    onGameServerEvent( indicator: GameEventIndicator ) : void {
        switch ( indicator ) {
            /*
                We most likely are receiving event during packet process, so we cannot
                directly join connections yet, since if done now packet from server
                can be proxied to client. We must wait a fraction of time.
             */
            case GameEventIndicator.ServerProxyInitialized:
                this.joinClientsTimeout = setTimeout( this.joinGameConnections.bind( this ), 100 )
                return

            case GameEventIndicator.ConnectionTerminated:
                ServerLog.trace( 'Game Server closed connection' )
                this.gameClient.abortConnection()
                return this.cleanUp()
        }
    }

    onGameClientEvent( indicator: GameEventIndicator ) : void {
        switch ( indicator ) {
            case GameEventIndicator.ConnectionTerminated:
                ServerLog.trace( 'Game Client closed connection' )
                this.gameServer.abortConnection()
                return this.cleanUp()
        }
    }

    /*
        1. Register parameters to recognize incoming game client and wait for game client
        - login parameters are used to recognize incoming client after first few packet interactions
    */
    private registerWaitingClient() : void {
        this.disconnectTimeout = setTimeout( this.terminateClient.bind( this ), ConfigManager.server.getWaitingClientTimeout(), 'Connections timed out' )
        return ClientCache.registerWaitingClient( this )
    }

    /*
        2. When connected L2 client is identified to have same login parameters,
        it is possible now to join both client and server connections to exchange packets.
        However, proxy must now relay same packet data to game server to ensure that protocol version
        and login parameters are accepted.
     */
    addGameClientConnection( connection : GameClientConnection ) : void {
        this.gameClient = connection

        let serverId = this.loginClient.serverLoginEvent.serverId
        let serverData = this.loginServer.servers.availableServers.find( server => server.id === serverId )

        if ( !serverData ) {
            return this.terminateClient( `Unable to find server with id=${serverId} in available servers (size=${this.loginServer.servers.availableServers.length})` )
        }

        ServerLog.info( `Proxy => Game Server : starting connection for serverId=${serverId} on ${serverData.ip}:${serverData.port}` )

        let protocolVersionEvent = this.gameClient.recordedPackets[ ProtocolVersion.name ].event as ProtocolVersionEvent
        this.gameServer = new GameServerConnection( serverData.ip, serverData.port, protocolVersionEvent.version, this.onGameServerEvent.bind( this ), chalk.blueBright( 'Game Server <=> Proxy' ) )
    }

    terminateClient( reason: string ) : void {
        ServerLog.info( `Terminating client due to: ${reason}` )

        this.loginServer.abortConnection()
        this.loginClient.abortConnection()

        if ( this.gameClient ) {
            this.gameClient.abortConnection()
        }

        if ( this.gameServer ) {
            this.gameServer.abortConnection()
        }

        return this.cleanUp()
    }

    private cleanUp() : void {
        this.stopTimeouts()
        ClientCache.removeClient( this )
        ClientCache.removeWaitingClient( this )
    }

    private stopTimeouts() : void {
        if ( this.disconnectTimeout ) {
            clearTimeout( this.disconnectTimeout )
            this.disconnectTimeout = null
        }

        if ( this.joinClientsTimeout ) {
            clearTimeout( this.joinClientsTimeout )
            this.joinClientsTimeout = null
        }
    }

    /*
        3. When both client and server connections are ready to act as proxy,
        we establish link between processing of packets to facilitate proper game
        server packet flow. The game flow is started by sending AuthenticatedLogin to game
        server, with data obtained from login server packet flow.
        - while it is possible to re-send ProtocolVersion packet, such action can be
          used to identify proxy behavior, so it is easier to simply rely on fact that if communication
          to game server cannot be accomplished (incompatible L2 client), server will reject
          such data with normal (according to particular game server implementation) flow
     */
    private joinGameConnections() : void {
        this.gameServer.proxyToConnection( this.gameClient )
        this.gameClient.proxyToConnection( this.gameServer )

        this.gameClient.setCallback( this.onGameClientEvent.bind( this ) )
        this.gameServer.sendData( AuthenticatedLogin( this.gameClient.authenticatedLogin ) )

        this.stopTimeouts()
        this.terminateLoginConnections()
        ServerLog.trace( 'Game Client and Server connections are paired' )

        /*
            Client packet modifiers allow us to build proxy-specific functionality,
            such as voice commands and bypass handling.
         */
        this.gameClient.setPacketModifier( this.onGameClientPacket.bind( this ) )
        this.gameServer.setPacketModifier( this.onGameServerPacket.bind( this ) )
    }

    private generateLoginClientBlowfishKey(): Buffer {
        const numberSequence: Array<number> = _.times( 16, (): number => {
            return _.random( 255 )
        } )

        return Buffer.from( numberSequence )
    }

    private terminateLoginConnections() : void {
        this.loginServer.abortConnection()
        this.loginClient.abortConnection()

        this.loginServer = null
        this.loginClient = null
    }

    private onGameClientPacket( name: string, event: PacketEvent, rawPacket: Buffer ) : Buffer {
        if ( name === PlayerSay.name && ( event as PlayerSayEvent ).message.startsWith( ProxyCommands.VoicePrefix ) ) {
            let commandChunks : Array<string> = ( event as PlayerSayEvent ).message.split( ' ' )

            if ( ListenerCache.hasListeners( ListenerType.ProxyVoiceCommand, commandChunks[ 0 ] ) ) {
                return ListenerCache.processListeners( ListenerType.ProxyVoiceCommand, commandChunks[ 0 ], event, this, rawPacket )
            }
        }

        if ( name === BypassToServer.name && ( event as BypassToServerEvent ).command.startsWith( ProxyCommands.BypassPrefix ) ) {
            let commandChunks : Array<string> = ( event as BypassToServerEvent ).command.split( ' ' )

            if ( ListenerCache.hasListeners( ListenerType.ProxyBypass, commandChunks[ 0 ] ) ) {
                return ListenerCache.processListeners( ListenerType.ProxyBypass, commandChunks[ 0 ], event, this, rawPacket )
            }
        }

        if ( ListenerCache.hasListeners( ListenerType.GameClientPacket, name ) ) {
            return ListenerCache.processListeners( ListenerType.GameClientPacket, name, event, this, rawPacket )
        }

        return rawPacket
    }

    private onGameServerPacket( name: string, event: PacketEvent, rawPacket: Buffer ) : Buffer {
        if ( ListenerCache.hasListeners( ListenerType.GameServerPacket, name ) ) {
            return ListenerCache.processListeners( ListenerType.GameServerPacket, name, event, this, rawPacket )
        }

        return rawPacket
    }
}