import { ProxyCallback, ProxyConnection } from './ProxyConnection'
import {
    LoginInitialConfiguration,
    LoginInitialConfigurationEvent
} from 'lineage2ts-testing/source/client/packets/login/receive/InitialConfiguration'
import {
    LoginServerApprovedEvent,
    ServerLoginApproved
} from 'lineage2ts-testing/source/client/packets/login/receive/ServerLoginApproved'
import { LoginSuccess, LoginSuccessEvent } from 'lineage2ts-testing/source/client/packets/login/receive/LoginSuccess'
import { InitialConfiguration } from 'lineage2ts-login/source/packets/send/InitialConfiguration'
import {
    LoginServerList,
    LoginServerListEvent
} from 'lineage2ts-testing/source/client/packets/login/receive/ServerList'
import { ServerList } from '../../packets/login/send/ServerList'
import { LoginPacketMethods } from 'lineage2ts-testing/source/client/packets/LoginPacketMethods'
import { ServerLog } from '../../logger/Logger'
import { LoginEventIndicator } from '../../enum/LoginEventIndicators'
import { LoginEncryption } from 'lineage2ts-testing/source/client/security/LoginEncryption'
import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'
import chalk from 'chalk'

export class LoginServerConnection extends ProxyConnection {
    playApproved : LoginServerApprovedEvent
    loginApproved : LoginSuccessEvent
    servers: LoginServerListEvent

    proxyHostIp: Array<number>
    clientEncryptionKey : Buffer

    constructor( callback: ProxyCallback, port: number, host: string, proxyHost : string, clientEncryptionKey: Buffer ) {
        super( callback, chalk.magenta( 'Proxy <=> Login Server' ) )

        this.proxyHostIp = proxyHost.split( '.' ).map( value => parseInt( value, 10 ) )
        this.parameters = {
            port,
            host
        }

        this.clientEncryptionKey = clientEncryptionKey
        this.encryption = new LoginEncryption()
        this.packetMap = LoginPacketMethods

        this.addPacketListeners( {
            [ LoginInitialConfiguration.name ]: this.onInitialConfiguration.bind( this ),
            [ ServerLoginApproved.name ]: this.onServerLoginApproved.bind( this ),
            [ LoginSuccess.name ]: this.onLoginSuccess.bind( this )
        } )

        this.setPacketModifier( this.onPacketModifier.bind( this ) )

        this.startConnection()
    }

    private onInitialConfiguration( data: LoginInitialConfigurationEvent ) : void {
        this.encryption.setKey( data.blowfishKey )
    }

    /*
        Step 1. Substitute blowfish decryption key from server, with one you can control
        communication with only client.

        Now proxy will have to use two keys, one provided by server,
        and one generated for client. Two keys allow additional packet to be sent to both server
        or client, if needed. Neither server, nor client would be able to tell the difference
        they are using different keys.
     */
    private modifyInitialConfiguration( data: LoginInitialConfigurationEvent ) : Buffer {
        return InitialConfiguration( data.unDecodedPublicKey, this.clientEncryptionKey, data.sessionId )
    }

    /*
        Step 2. Substitute IP addresses of available servers with external proxy IP.

        Such action allows proxy to represent game servers and further packet modification,
        or additional programmatic packet generation.
     */
    private modifyServerList( data : LoginServerListEvent ) : Buffer {
        this.servers = data
        return ServerList( data, this.proxyHostIp, 7777 )
    }

    /*
        Step 3. Initialize game server connection with known parameters.
        At this point, client normally connects directly to game server and
        starts chain of packets. Proxy now would have to emulate few initial packet
        responses to connected client. Once four values for approved data are sent from
        client, we should be able to proxy game server connection with client.
     */
    private onServerLoginApproved( data : LoginServerApprovedEvent ) : void {
        this.playApproved = data
        this.callback( LoginEventIndicator.FlowFinished )
    }

    private onLoginSuccess( data : LoginSuccessEvent ) : void {
        this.loginApproved = data
    }

    onConnectionError( error : { code: string } & Error ) : void {
        switch ( error.code ) {
            case 'ECONNRESET':
                ServerLog.error( `${this.name} : Connection closed for ${this.parameters.host}:${this.parameters.port}` )
                break

            case 'ENOTFOUND':
                ServerLog.error( `${this.name} : Unable to resolve server DNS/IP address for ${this.parameters.host}` )
                break

            case 'ECONNREFUSED':
                ServerLog.error( `${this.name}: Server is refusing connection for ${this.parameters.host}:${this.parameters.port}` )
                break

            case 'ECONNABORTED':
                ServerLog.error( `${this.name}: Server forcibly terminated connection for ${this.parameters.host}:${this.parameters.port}` )
                break

            case 'EHOSTUNREACH':
                ServerLog.error( `: ${this.name}: Connection failed due to firewall or gateway at ${this.parameters.host}:${this.parameters.port}` )
                break
        }

        super.onConnectionError( error )
    }

    private onPacketModifier( name: string, event: PacketEvent, rawPacket: Buffer ) : Buffer {
        switch ( name ) {
            case LoginInitialConfiguration.name:
                return this.modifyInitialConfiguration( event as LoginInitialConfigurationEvent )

            case LoginServerList.name:
                return this.modifyServerList( event as LoginServerListEvent )
        }

        return rawPacket
    }
}