import net, { Socket } from 'net'
import {
    PacketEvent,
    PacketHandlingMethod,
    PacketListenerMap,
    PacketListenerMethod,
    PacketMethodMap
} from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'
import { L2ClientPacketProcessor } from 'lineage2ts-testing/source/client/types/PacketProcessor'
import { EncryptionOperations } from 'lineage2ts-testing/source/client/security/EncryptionOperations'
import { ReadableClientPacket } from 'lineage2ts-testing/source/client/packets/ReadableClientPacket'
import { ServerLog } from '../../logger/Logger'
import chalk from 'chalk'
import { ProxyPacketModifierMethod } from '../../packets/PacketModifiers'
import _ from 'lodash'
import { LoginEventIndicator } from '../../enum/LoginEventIndicators'
import { diffString } from 'json-diff'
import { ConfigManager } from '../../config/ConfigManager'
import { ProxyConnectionStatistics } from '../../models/ProxyConnectionStatistics'

export interface ConnectionParameters {
    port: number
    host: string
}

export type SendDataMethod = ( data: Buffer ) => void
export type ProxyCallback = ( state: number ) => void

export interface RecordedPacket {
    time: number
    event: PacketEvent
    data: Buffer
}

export class ProxyConnection {
    protected socket : Socket
    protected terminateConnection: boolean = false
    protected parameters: ConnectionParameters

    readonly recordedPackets: Record<string, RecordedPacket> = {}
    readonly packetProcessors: Set<L2ClientPacketProcessor> = new Set<L2ClientPacketProcessor>()
    statistics: ProxyConnectionStatistics = {
        bytesWritten: 0,
        bytesRead: 0,
        packetProcessErrorCount: 0,
        startTime: 0
    }

    protected encryption: EncryptionOperations
    protected packetMap: PacketMethodMap = {}
    protected listenerMap: PacketListenerMap = {}
    protected packetModifier: ProxyPacketModifierMethod

    protected sendDataMethod: SendDataMethod = () => {}
    protected callback: ProxyCallback
    readonly name: string

    constructor( callback: ProxyCallback, name: string ) {
        this.callback = callback
        this.name = name
    }

    proxyToConnection( connection: ProxyConnection ) : void {
        this.sendDataMethod = connection.sendData.bind( connection )
    }

    protected addConnection( socket: Socket ) : void {
        this.socket = socket
        this.initialize()

        this.statistics.startTime = Date.now()
    }

    protected startConnection() : void {
        this.socket = new net.Socket()
        this.initialize()
        this.socket.connect( this.parameters.port, this.parameters.host )
        this.statistics.startTime = Date.now()
    }

    initialize() : void {
        this.socket.on( 'data', this.processPackets.bind( this ) )
        this.socket.on( 'close', this.onConnectionClose.bind( this ) )
        this.socket.on( 'error', this.onConnectionError.bind( this ) )
    }

    private processPackets( data : Buffer ) : void {
        let packetSizeIndex = 0

        while ( packetSizeIndex < data.length ) {
            let packetSize = data.readInt16LE( packetSizeIndex )
            this.statistics.bytesRead = this.statistics.bytesRead + packetSize

            let rawPacket = data.subarray( packetSizeIndex, packetSizeIndex + packetSize )
            packetSizeIndex += packetSize

            this.processPacketData( rawPacket )

            if ( this.terminateConnection ) {
                return this.onConnectionClose()
            }
        }
    }

    abortConnection() : void {
        if ( this.socket.destroyed ) {
            return
        }

        this.socket.end()
        this.socket.destroy()
    }

    processPacketData( rawPacket: Buffer ) : void {
        let decryptedData : Buffer
        let packetSignature : string = 'not-decrypted'

        try {
            decryptedData = this.encryption.decrypt( rawPacket.subarray( 2 ) )
            packetSignature = `0x${this.getPacketSignature( decryptedData ).toString( 16 )}`
            let method : PacketHandlingMethod = this.getPacketMethod( decryptedData )

            let dataToRelay : Buffer = method ? this.processMethodData( decryptedData, method ) : decryptedData
            if ( dataToRelay ) {
                /*
                    Please note that packet data must have first two bytes specified as
                    packet length.
                 */
                let packetName = chalk.greenBright( method?.name ? `${method.name} ${packetSignature}` : packetSignature )
                if ( decryptedData === dataToRelay ) {
                    ServerLog.trace( `${this.name} : sending un-modified packet ${packetName}` )

                    this.sendDataMethod( rawPacket )

                    return
                }

                ServerLog.trace( `${this.name} : sending generated packet ${packetName}` )
                this.sendDataMethod( dataToRelay )
            }

        } catch ( error ) {
            if ( decryptedData ) {
                ServerLog.error( `${this.name} : error processing packet signature ${packetSignature}` )
            }

            ServerLog.error( `${this.name} error: ${error.message}` )
            this.terminateConnection = true
        }
    }

    protected processMethodData( decryptedData: Readonly<Buffer>, method: PacketHandlingMethod ) : Buffer {
        let event: PacketEvent

        /*
            When processing packet data it is possible for client (unlikely for server) to send junk data
            due to various reasons (encryption is off, malicious attempts to elicit crash, or simple exploration).
            In such case packet reading can produce an error, hence try/catch guard to ensure bad packet data is
            sent directly to proxy connection.
         */
        let offset : number
        try {
            offset = this.getPacketOffset( this.getPacketSignature( decryptedData ) )
            event = method( new ReadableClientPacket( decryptedData, offset ) )
        } catch ( error ) {
            this.statistics.packetProcessErrorCount++
            return decryptedData
        }

        if ( event && this.listenerMap[ method.name ] ) {
            this.listenerMap[ method.name ].map( ( listener: PacketListenerMethod ) => {
                if ( listener ) {
                    listener( event )
                }
            } )
        }

        let dataToRelay: Buffer = decryptedData
        if ( event && this.packetModifier ) {
            dataToRelay = this.packetModifier( method.name, event, decryptedData )
        }

        this.updatePacketTracking( method.name, event, decryptedData )
        ServerLog.trace( `${ this.name } : processed packet ${ chalk.greenBright( method.name ) }, size=${decryptedData.length + 2}` )

        return dataToRelay
    }

    getPacketSignature( packetData: Buffer ) : number {
        return packetData.readUInt8( 0 )
    }

    updatePacketTracking( name: string, data: PacketEvent, rawPacket: Readonly<Buffer> ) : void {
        this.logPacketDiffs( name, data, rawPacket )

        let packet = this.recordedPackets[ name ]
        if ( !packet ) {
            packet = {
                data: undefined,
                event: undefined,
                time: 0
            }

            this.recordedPackets[ name ] = packet
        }

        let copyData = Buffer.allocUnsafe( rawPacket.length )
        rawPacket.copy( copyData )

        packet.event = data
        packet.time = Date.now()
        packet.data = copyData

        if ( this.packetProcessors.size > 0 ) {
            this.packetProcessors.forEach( ( processor: L2ClientPacketProcessor ) => processor( name, data, rawPacket ) )
        }
    }

    onConnectionClose() : void {
        this.abortConnection()
        this.callback( LoginEventIndicator.ConnectionTerminated )
    }

    onConnectionError( error : { code: string } & Error ) : void {
        this.abortConnection()
    }

    addPacketListeners( listeners: Record<string, PacketListenerMethod> ) : void {
        this.listenerMap = _.reduce( listeners, ( map: PacketListenerMap, packetListener: PacketListenerMethod, name: string ) : PacketListenerMap => {
            if ( !map[ name ] ) {
                map[ name ] = []
            }

            map[ name ].push( packetListener )

            return map
        }, this.listenerMap )
    }

    removePacketListeners( listeners: Record<string, PacketListenerMethod> ) : void {
        this.listenerMap = _.reduce( listeners, ( map: PacketListenerMap, packetListener: PacketListenerMethod, name: string ) : PacketListenerMap => {
            if ( map[ name ] ) {
                _.pull( map[ name ], packetListener )
            }

            return map
        }, this.listenerMap )
    }

    setPacketModifier( modifier: ProxyPacketModifierMethod ) : void {
        this.packetModifier = modifier
    }

    sendData( data: Buffer ) : void {
        let encryptedData = this.encryption.encrypt( data )
        this.statistics.bytesWritten = this.statistics.bytesWritten + encryptedData.length

        this.socket.write( encryptedData )
    }

    setCallback( callback: ProxyCallback ) : void {
        this.callback = callback
    }

    getPacketOffset( value: number ) : number {
        return 1
    }

    getPacketMethod( packetData: Buffer ) : PacketHandlingMethod {
        return this.packetMap[ this.getPacketSignature( packetData ) ]
    }

    logPacketDiffs( name: string, data: PacketEvent, rawPacket: Buffer ) : void {
        if ( !this.shouldLogPacketDiff( name ) ) {
            return
        }

        let packet = this.recordedPackets[ name ]

        if ( packet ) {
            ServerLog.info( `${ this.name } : packet ${chalk.redBright( name )} diff ${diffString( data, packet.event )}` )
            ServerLog.info( `${ this.name } : packet data ${chalk.redBright( name )} diff ${diffString( rawPacket, packet.data )}` )
        }
    }

    shouldLogPacketDiff( name: string ) : boolean {
        return ConfigManager.debug.enableLogPacketDiff()
    }
}