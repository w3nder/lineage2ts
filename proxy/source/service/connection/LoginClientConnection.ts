import { ProxyCallback, ProxyConnection } from './ProxyConnection'
import { Socket } from 'net'
import { RequestServerLogin, RequestServerLoginEvent } from '../../packets/login/receive/RequestServerLogin'
import { LoginClientToServerMethods } from '../../packets/LoginClientToServerMethods'
import { LoginCrypt } from '../../encryption/LoginCrypt'
import chalk from 'chalk'

export class LoginClientConnection extends ProxyConnection {
    serverLoginEvent: RequestServerLoginEvent

    constructor( callback: ProxyCallback, connection: Socket, futureEncryptionKey: Buffer ) {
        super( callback, chalk.yellow( 'Login Client => Proxy' ) )

        this.addPacketListeners( {
            [ RequestServerLogin.name ]: this.onClientRequestServerLogin.bind( this ),
        } )

        this.packetMap = LoginClientToServerMethods
        this.encryption = new LoginCrypt()
        this.encryption.setKey( futureEncryptionKey )

        this.addConnection( connection )
    }

    /*
        When processing data from client, it is important to capture server id
        that client used to choose from available servers. It will be used later to
        initiate game server connection, when incoming connection for game server through
        proxy have matched all the login parameters.
     */
    private onClientRequestServerLogin( data : RequestServerLoginEvent ) : void {
        this.serverLoginEvent = data
    }
}