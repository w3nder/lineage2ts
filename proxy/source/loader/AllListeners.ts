import { ProxyListener } from '../models/ProxyListener'
import { ProxyStats } from '../listeners/ProxyStats'
import { ListenerCache } from '../cache/ListenerCache'

const allListeners : Array<ProxyListener> = [
    new ProxyStats()
]

export function loadAllListeners() : void {
    for ( const listener of allListeners ) {
        ListenerCache.addDefinitions( listener.getListenerDefinitions() )
    }
}