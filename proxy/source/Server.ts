import { ClientCache } from './cache/ClientCache'
import { ServerLog } from './logger/Logger'
import net, { Socket } from 'net'
import { ClientProxy } from './service/ClientProxy'
import { GameClientConnection } from './service/connection/GameClientConnection'
import { ConfigManager } from './config/ConfigManager'
import { loadAllListeners } from './loader/AllListeners'

const enum ProxyPorts {
    GameServerPort = 7777,
    LoginServerPort = 2106
}

let gameProxy : net.Server
let loginProxy : net.Server

function onGameClientJoining( connection: Socket ) : void {
    ServerLog.info( `L2 game client joining from: ${ connection.remoteAddress }` )
    ClientCache.addUnmatchedConnection( new GameClientConnection( connection ) )
}

function onLoginClientJoining( connection: Socket ) : void {
    ServerLog.info( `L2 login client joining from: ${ connection.remoteAddress }` )
    ClientCache.addClient( new ClientProxy( connection, ConfigManager.server.getLoginServerPort(), ConfigManager.server.getLoginServerHost(), ConfigManager.server.getExternalProxyHost() ) )
}

function startProxyServer() : void {
    gameProxy = net.createServer( onGameClientJoining ).listen( ProxyPorts.GameServerPort, () => ServerLog.info( `GameServerProxy : started on ${ConfigManager.server.getExternalProxyHost()}:${ProxyPorts.GameServerPort}` ) )
    loginProxy = net.createServer( onLoginClientJoining ).listen( ProxyPorts.LoginServerPort, () => ServerLog.info( `LoginServerProxy: started on ${ConfigManager.server.getExternalProxyHost()}:${ProxyPorts.LoginServerPort}` ) )

    ServerLog.info( `Proxying to login server on ${ConfigManager.server.getLoginServerHost()}:${ConfigManager.server.getLoginServerPort()}` )
}

async function shutdownSequence() : Promise<void> {

    gameProxy.close()
    loginProxy.close()

    ServerLog.info( 'Proxy Shutdown : terminated all network connections' )
    ServerLog.flush( () => process.exit() )
}

/*
    Used by various restart utilities that manage application process.
 */
process.once( 'SIGUSR1', shutdownSequence )
process.once( 'SIGUSR2', shutdownSequence )

/*
    Used by terminal applications to interrupt process, aka Ctrl + C.
 */
process.on( 'SIGINT', shutdownSequence )

/*
    Termination signals used by OS to signify process must be killed (similar to SIGKILL).
 */
process.on( 'SIGTERM', shutdownSequence )
process.on( 'SIGHUP', shutdownSequence )

loadAllListeners()
startProxyServer()