export const enum GameEventIndicator {
    ConnectionTerminated,
    ServerProxyInitialized
}