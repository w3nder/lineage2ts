import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { DebugConfigurationApi } from '../../interface/DebugConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

class DebugConfiguration extends L2ConfigurationSubscription implements DebugConfigurationApi {

    configuration: { [ name: string ]: string }
    cachedValues: { [ name: string ]: any }

    getLogLevel(): string {
        return this.configuration[ 'LogLevel' ]
    }

    enableLogPacketDiff(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( this.configuration, this.cachedValues, 'LogPacketDiff' )
    }

    async load(): Promise<void> {
        this.configuration = await DotEnvConfigurationEngineHelper.getConfig( 'debug.properties' )
        this.cachedValues = {}
    }
}

export const DotEnvDebugConfiguration: DebugConfigurationApi = new DebugConfiguration()