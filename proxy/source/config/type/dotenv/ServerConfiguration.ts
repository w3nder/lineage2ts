import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { ServerConfigurationApi } from '../../interface/ServerConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

class ServerConfiguration extends L2ConfigurationSubscription implements ServerConfigurationApi {
    getWaitingClientTimeout(): number {
        return DotEnvConfigurationEngineHelper.getNumber( this.configuration, this.cachedValues, 'ConnectionWaitingTimeout' )
    }

    configuration: { [ name: string ]: string }
    cachedValues: { [ name: string ]: any }

    getExternalProxyHost(): string {
        return this.configuration[ 'ProxyHost' ]
    }

    getLoginServerHost(): string {
        return this.configuration[ 'LoginServerHost' ]
    }

    getLoginServerPort(): number {
        return DotEnvConfigurationEngineHelper.getNumber( this.configuration, this.cachedValues, 'LoginServerPort' )
    }

    async load(): Promise<void> {
        this.configuration = await DotEnvConfigurationEngineHelper.getConfig( 'server.properties' )
        this.cachedValues = {}
    }
}

export const DotEnvServerConfiguration: ServerConfigurationApi = new ServerConfiguration()