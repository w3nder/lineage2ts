import { L2ProxyConfigEngine } from './IConfigEngine'
import { ProxyConfigurationApi } from './IConfiguration'
import { DotEnvConfigurationEngine } from './engine/dotenv'
import pkgDir from 'pkg-dir'
import logSymbols from 'log-symbols'
import aigle from 'aigle'
import _ from 'lodash'
import dotenv from 'dotenv'

const rootDirectory = pkgDir.sync( __dirname )
dotenv.config( { path: `${ rootDirectory }/.env` } )

const { configurationEngine, configurationReloadMinutes } = process.env
const reloadTime = _.parseInt( configurationReloadMinutes ) * 60000

const allEngines: { [ name: string ]: L2ProxyConfigEngine } = {
    'dotenv': DotEnvConfigurationEngine,
}

console.log( logSymbols.info, `Proxy: using '${ configurationEngine }' engine` )

if ( !_.isString( configurationEngine ) || !allEngines[ configurationEngine ] ) {
    throw new Error( 'Please specify valid \'configurationEngine\' value!' )
}

export const ConfigManager: L2ProxyConfigEngine = allEngines[ configurationEngine as string ]

export async function loadConfiguration() {
    await aigle.resolve( ConfigManager ).each( async ( config: ProxyConfigurationApi ) => {
        return config.load()
    } )

    Object.values( ConfigManager ).forEach( ( config: ProxyConfigurationApi ) => {
        config.getSubscriptions().forEach( method => method() )
    } )

    console.log( `${ logSymbols.success } Proxy: Loaded ${ _.size( ConfigManager ) } configs.` )
}

if ( reloadTime > 0 ) {
    setInterval( loadConfiguration, reloadTime )
} else {
    console.log( `${ logSymbols.info } Proxy: Hot reloading is disabled.` )
}