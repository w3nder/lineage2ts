import { ProxyConfigurationApi } from '../IConfiguration'

export interface ServerConfigurationApi extends ProxyConfigurationApi {
    getLoginServerHost() : string
    getLoginServerPort() : number
    getExternalProxyHost() : string
    getWaitingClientTimeout() : number
}