import { ClientProxy } from '../service/ClientProxy'
import { AuthenticatedLoginEvent } from '../packets/game/receive/AuthenticatedLogin'
import { GameClientConnection } from '../service/connection/GameClientConnection'
import { ServerLog } from '../logger/Logger'

class Manager {

    allClients: Set<ClientProxy> = new Set<ClientProxy>()
    incomingGCConnections: Set<GameClientConnection> = new Set<GameClientConnection>()
    waitingClients: Record<string, ClientProxy> = {}

    addUnmatchedConnection( connection: GameClientConnection ) : void {
        this.incomingGCConnections.add( connection )
    }

    removeUnmatchedConnection( connection: GameClientConnection ) : void {
        this.incomingGCConnections.delete( connection )
    }

    addClient( client: ClientProxy ) : void {
        this.allClients.add( client )
    }

    removeClient( client: ClientProxy ) : void {
        this.allClients.delete( client )
    }

    registerWaitingClient( client: ClientProxy ) : void {
        const loginOne = client.loginClient.serverLoginEvent.one
        const loginTwo = client.loginClient.serverLoginEvent.two
        const playOne = client.loginServer.playApproved.one
        const playTwo = client.loginServer.playApproved.two

        let key = this.createWaitingClientKey( loginOne, loginTwo, playOne, playTwo )
        if ( this.waitingClients[ key ] ) {
            return
        }

        ServerLog.trace( `ClientCache: registered waiting client for ${key}` )
        this.waitingClients[ key ] = client
    }

    private createWaitingClientKey( loginOne : number, loginTwo : number, playOne : number, playTwo : number ) : string {
        return `${loginOne}:${loginTwo}:${playOne}:${playTwo}`
    }

    getWaitingClient( data : AuthenticatedLoginEvent ) : ClientProxy {
        let key = this.createWaitingClientKey( data.loginOne, data.loginTwo, data.playOne, data.playTwo )
        let proxy = this.waitingClients[ key ]

        delete this.waitingClients[ key ]

        return proxy
    }

    removeWaitingClient( client: ClientProxy ) : void {
        if ( !client.loginClient?.serverLoginEvent || !client.loginServer?.playApproved ) {
            return
        }

        const loginOne = client.loginClient.serverLoginEvent.one
        const loginTwo = client.loginClient.serverLoginEvent.two
        const playOne = client.loginServer.playApproved.one
        const playTwo = client.loginServer.playApproved.two

        let key = this.createWaitingClientKey( loginOne, loginTwo, playOne, playTwo )

        if ( !this.waitingClients[ key ] ) {
            return
        }

        delete this.waitingClients[ key ]
        ServerLog.trace( `ClientCache: removed waiting client for ${key}` )
    }
}

export const ClientCache = new Manager()