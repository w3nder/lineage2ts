import { ListenerDefinition, ListenerType } from '../models/ListenerDefinition'
import { PacketEvent } from 'lineage2ts-testing/source/client/packets/PacketMethodTypes'
import { ClientProxy } from '../service/ClientProxy'

class Manager {
    private clientListeners : Record<string, Array<ListenerDefinition>> = {}
    private serverListeners: Record<string, Array<ListenerDefinition>> = {}

    hasListeners( type: ListenerType, name: string ) : boolean {
        return !!this.getListeners( type, name )
    }

    processListeners( type: ListenerType, name: string, event: PacketEvent, proxy: ClientProxy, rawPacket: Readonly<Buffer> ) : Buffer | null {
        let allListeners = this.getListeners( type, name )
        if ( !allListeners ) {
            return rawPacket
        }

        for ( const listener of allListeners ) {
            const outcome = listener.method( event, proxy, rawPacket )
            if ( outcome ) {
                return outcome
            }
        }

        return null
    }

    private getListeners( type: ListenerType, name: string ) : Array<ListenerDefinition> {
        return type === ListenerType.GameClientPacket ? this.clientListeners[ name ] : this.serverListeners[ name ]
    }

    addDefinitions( listeners: Array<ListenerDefinition> ) : void {
        for ( const currentListener of listeners ) {
            let existingListeners = currentListener.type === ListenerType.GameClientPacket ? this.clientListeners : this.serverListeners

            let namedListeners = existingListeners[ currentListener.name ]
            if ( !namedListeners ) {
                namedListeners = []
                existingListeners[ currentListener.name ] = namedListeners
            }

            namedListeners.push( currentListener )
        }
    }
}

export const ListenerCache = new Manager()