export interface ProxyConnectionStatistics {
    startTime: number
    bytesRead: number
    bytesWritten: number
    packetProcessErrorCount: number
}