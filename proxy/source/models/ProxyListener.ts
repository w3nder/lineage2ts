import { ListenerDefinition } from './ListenerDefinition'

export abstract class ProxyListener {
    name: string

    constructor( name: string ) {
        this.name = name
    }

    abstract getListenerDefinitions() : Array<ListenerDefinition>
}