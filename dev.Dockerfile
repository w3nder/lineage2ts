# Special version of local build without artifact generation, meaning no data/geo-pack or database generation via cli
FROM node:22.9.0-bullseye AS build

RUN mkdir -p /opt/lineage2ts

COPY node_modules /opt/lineage2ts/node_modules
COPY game-server /opt/lineage2ts/game-server
COPY login-server /opt/lineage2ts/login-server

WORKDIR /opt/lineage2ts

# We do need to install dependencies properly since host machine has most likely different OS
RUN cd /opt/lineage2ts/game-server && npm ci && npm run build && npm prune --production
RUN cd /opt/lineage2ts/login-server && npm ci && npm run build && npm prune --production

FROM node:22.9.0-slim

RUN mkdir -p /opt/lineage2ts/game-server && mkdir -p /opt/lineage2ts/login-server
WORKDIR /opt/lineage2ts

# All compiled JS files along with any pack files should already be in server directories

COPY --from=build /opt/lineage2ts/game-server /opt/lineage2ts/game-server
COPY --from=build /opt/lineage2ts/login-server /opt/lineage2ts/login-server
COPY docker-entry.sh /docker-entry.sh

LABEL maintainer="MrTREX" version="dev"  website="https://gitlab.com/MrTREX/lineage2ts"
EXPOSE 7777 2106
VOLUME ["/opt/lineage2ts/login-server", "/opt/lineage2ts/game-server"]

RUN chmod +x /docker-entry.sh
CMD [ "/docker-entry.sh" ]