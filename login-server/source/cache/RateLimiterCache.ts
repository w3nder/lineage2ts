import { ConfigManager } from '../config/ConfigManager'
import _ from 'lodash'
import { FastRateLimit } from 'fast-ratelimit'

class Cache {
    connectionLimiter: FastRateLimit // ip limiter
    connectionCounter: { [ ipAddress: string ]: number }
    accountLimiter: FastRateLimit // account name limiter to prevent frequent login attempts from same account (attack vector)

    constructor() {
        this.connectionLimiter = new FastRateLimit( {
            threshold: 1, // available tokens over timespan
            ttl: _.defaultTo( ConfigManager.server.getRateLimitingBurstRate(), 2 ), // time-to-live value of token bucket (in seconds)
        } )

        this.accountLimiter = new FastRateLimit( {
            threshold: 1, // available tokens over timespan
            ttl: _.defaultTo( ConfigManager.server.getRateLimitingAccountRate(), 15 ), // time-to-live value of token bucket (in seconds)
        } )

        this.connectionCounter = {}
    }

    isAllowedIpAddress( ipAddress: string ): boolean {
        if ( !ConfigManager.server.isRateLimitingEnabled() ) {
            return true
        }

        let counter = _.get( this.connectionCounter, ipAddress, 0 ) + 1

        if ( counter > ConfigManager.server.getRateLimitingMaxConnections() ) {
            return false
        }

        if ( counter > ConfigManager.server.getRateLimitingBurstThreshold() ) {
            return this.applyRateLimiting( counter, ipAddress )
        }

        this.incrementConnectionCounter( ipAddress, counter )

        return true
    }

    applyRateLimiting( counter: number, ipAddress: string ): boolean {
        if ( this.connectionLimiter.consumeSync( ipAddress ) ) {
            this.incrementConnectionCounter( ipAddress, counter )

            return true
        }

        return false
    }

    incrementConnectionCounter( ipAddress: string, value: number ): void {
        value++
        this.connectionCounter[ ipAddress ] = value
    }

    isAllowedAccount( name: string ): boolean {
        if ( !ConfigManager.server.isRateLimitingEnabled() ) {
            return true
        }

        return this.accountLimiter.consumeSync( name )
    }
}

export const RateLimiterCache = new Cache()