import { L2AccountsAccessTime } from '../database/interface/AccountsTableApi'
import { DatabaseManager } from '../database/manager'
import _, { DebouncedFunc } from 'lodash'

class Manager {
    lastAccessTimes: Map<string, number> = new Map<string, number>()
    debounceLastAccessTimes: DebouncedFunc<any>

    constructor() {
        this.debounceLastAccessTimes = _.debounce( this.runUpdateLastAccessTimes.bind( this ), 30000, {
            maxWait: 5000
        } )
    }
    refreshLastAccess( accountName: string ) : void {
        this.lastAccessTimes.set( accountName, Date.now() )
        this.debounceLastAccessTimes()
    }

    runUpdateLastAccessTimes() : Promise<void> {
        let records : Array<L2AccountsAccessTime> = []
        this.lastAccessTimes.forEach( ( time: number, name: string ) : void => {
            records.push( {
                name,
                time
            } )
        } )

        if ( records.length === 0 ) {
            return
        }

        this.lastAccessTimes.clear()
        return DatabaseManager.getAccounts().updateLastAccess( records )
    }
}

export const AccountUpdateManager = new Manager()