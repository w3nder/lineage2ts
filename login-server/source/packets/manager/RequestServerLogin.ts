import { SessionKeyHelper, SessionKeyProperties } from '../../service/SessionKey'
import { LoginManager } from '../../cache/LoginManager'
import { PlayOK } from '../send/PlayOK'
import { PlayFail, PlayFailReason } from '../send/PlayFail'
import { LoginFail, LoginFailReason } from '../send/LoginFail'
import { L2LoginClient } from '../../service/L2LoginClient'
import { ConfigManager } from '../../config/ConfigManager'
import { ReadableClientPacket } from '../ReadableClientPacket'

export function RequestServerLogin( client: L2LoginClient, data: Buffer ) : Buffer {
    let packet = new ReadableClientPacket( data, 1 )
    let loginOk1 = packet.readD()
    let loginOk2 = packet.readD()

    if ( !ConfigManager.server.showLicense() || SessionKeyHelper.compareKeys( client.getSessionKey(), {
        loginOk1,
        loginOk2,
    } as SessionKeyProperties ) ) {
        let serverId = packet.readC()

        if ( LoginManager.isLoginPossible( client, serverId ) ) {
            client.setJoinedGS( true )

            return PlayOK( client.getSessionKey() )
        }

        return PlayFail( PlayFailReason.serverOverloaded )
    }

    return LoginFail( LoginFailReason.accessFailed )
}