import { SessionKeyHelper, SessionKeyProperties } from '../../service/SessionKey'
import { ServerList } from '../send/ServerList'
import { LoginFail, LoginFailReason } from '../send/LoginFail'
import { L2LoginClient } from '../../service/L2LoginClient'
import { ReadableClientPacket } from '../ReadableClientPacket'

export function RequestServerList( client: L2LoginClient, data: Buffer ) {
    let packet = new ReadableClientPacket( data, 1 )
    let loginOk1 = packet.readD()
    let loginOk2 = packet.readD()

    if ( SessionKeyHelper.compareKeys( client.getSessionKey(), { loginOk1, loginOk2 } as SessionKeyProperties ) ) {
        client.enableServerListUpdates = true
        return ServerList( client )
    }

    return LoginFail( LoginFailReason.accessFailed )
}