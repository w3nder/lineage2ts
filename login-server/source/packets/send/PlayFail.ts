import { DeclaredServerPacket } from '../DeclaredServerPacket'

export enum PlayFailReason {
    noMessage = 0x00,
    systemErrorLoginLater = 0x01,
    userOrPassWrong = 0x02,
    accessFailedTryAgain = 0x04,
    accountInfoIncorrectContactSupport = 0x05,
    accountInUse = 0x07,
    under18YearsKr = 0x0C,
    serverOverloaded = 0x0F,
    serverMaintenance = 0x10,
    tempPassExpired = 0x11,
    gameTimeExpired = 0x12,
    noTimeLeft = 0x13,
    systemError = 0x14,
    accessFailed = 0x15,
    restrictedIp = 0x16,
    weekUsageFinished = 0x1E,
    securityCardNumberInvalid = 0x1F,
    ageNotVerifiedCantLogBetween10Pm6Am = 0x20,
    serverCannotBeAccessedByYourCoupon = 0x21,
    dualBox = 0x23,
    inactive = 0x24,
    userAgreementRejectedOnWebsite = 0x25,
    guardianConsentRequired = 0x26,
    userAgreementDeclinedOrWithdrawalRequest = 0x27,
    accountSuspendedCall = 0x28,
    changePasswordAndQuizOnWebsite = 0x29,
    alreadyLoggedInto10Accounts = 0x2A,
    masterAccountRestricted = 0x2B,
    certificationFailed = 0x2E,
    telephoneCertificationUnavailable = 0x2F,
    telephoneSignalsDelayed = 0x30,
    certificationFailedLineBusy = 0x31,
    certificationServiceNumberExpiredOrIncorrect = 0x32,
    certificationServiceCurrentlyBeingChecked = 0x33,
    certificationServiceCantBeUsedHeavyVolume = 0x34,
    certificationServiceExpiredGameplayBlocked = 0x35,
    certificationFailed3TimesGameplayBlocked30Min = 0x36,
    certificationDailyUseExceeded = 0x37,
    certificationUnderwayTryAgainLater = 0x38,
}

export function PlayFail( reason: PlayFailReason ): Buffer {
    return new DeclaredServerPacket( 2 )
            .writeC( 0x06 )
            .writeC( reason )
            .getBuffer()
}