import { ServerStatus } from '../../service/GameServerEnums'
import { GameServerManager } from '../../service/GameServerManager'
import { L2LoginClient } from '../../service/L2LoginClient'
import { ConnectedGameServer } from '../../models/RegisteredGameServer'
import { L2GameServerParameters } from '../../rpc/interface/LoginServerOperations'
import { DeclaredServerPacket } from '../DeclaredServerPacket'
import { IServerPacket } from '../IServerPacket'
import _ from 'lodash'

interface RegisteredServerDetails {
    ip: Array<number>
    port: number
    pvp: boolean
    serverType: number
    currentPlayers: number
    maxPlayers: number
    ageLimit: number
    useBrackets: boolean
    status: number
    serverId: number
}

export function ServerList( client: L2LoginClient ): Buffer {
    let servers: Array<RegisteredServerDetails> = getServerDataList( client.getAccessLevel() )
    let charactersOnServers = client.getCharactersOnServer()
    let charactersToDelete = client.getCharactersWaitingDeleteOnServer()
    let serversTotal: number = servers.length
    let dynamicSize: number = _.reduce( charactersToDelete, ( total: number, characters: Object ): number => {
        return total + _.size( characters )
    }, 0 )

    let packet = new DeclaredServerPacket( 6 + serversTotal * 21 + _.size( charactersOnServers ) * 3 + dynamicSize * 4 )
            .writeC( 0x04 )
            .writeC( serversTotal )
            .writeC( client.getLastServer() )

    applyServerDataChunk( servers, packet )
    applyCharacterDataChunk( charactersOnServers, charactersToDelete, packet )

    return packet.getBuffer()
}

function applyServerDataChunk( servers: Array<RegisteredServerDetails>, serverPacket: IServerPacket ): void {
    servers.forEach( ( currentServer: RegisteredServerDetails ) => {
        serverPacket
                .writeC( currentServer.serverId )
                .writeC( currentServer.ip[ 0 ] )
                .writeC( currentServer.ip[ 1 ] )
                .writeC( currentServer.ip[ 2 ] )

                .writeC( currentServer.ip[ 3 ] )
                .writeD( currentServer.port )
                .writeC( currentServer.ageLimit ) // Age Limit 0, 15, 18
                .writeC( currentServer.pvp ? 0x01 : 0x00 )

                .writeH( currentServer.currentPlayers )
                .writeH( currentServer.maxPlayers )
                .writeC( currentServer.status === ServerStatus.statusDown ? 0x00 : 0x01 )
                .writeD( currentServer.serverType ) // 1: Normal, 2: Relax, 4: Public Test, 8: No Label, 16: Character Creation Restricted, 32: Event, 64: Free

                .writeC( currentServer.useBrackets ? 0x01 : 0x00 )
    } )

    serverPacket.writeH( 0 )
}

function applyCharacterDataChunk( charactersOnServers, charactersToDelete, serverPacket: IServerPacket ): void {
    serverPacket.writeC( _.size( charactersOnServers ) )

    _.each( charactersOnServers, ( characters: number, serverId: string ) => {
        serverPacket
                .writeC( _.parseInt( serverId ) )
                .writeC( characters )

        if ( _.has( charactersToDelete, serverId ) ) {
            let currentlyDeletedCharacters = charactersToDelete[ serverId ]
            serverPacket.writeC( _.size( currentlyDeletedCharacters ) )

            _.each( currentlyDeletedCharacters, ( deleteTime ) => {
                let secondsLeft = ( deleteTime - Date.now() ) / 1000
                serverPacket.writeD( secondsLeft )
            } )

            return
        }

        serverPacket.writeC( 0x00 )
    } )
}

function getServerDataList( currentAccessLevel: number ): Array<RegisteredServerDetails> {
    return _.reduce( GameServerManager.getRegisteredGameServers(), ( serverData: Array<RegisteredServerDetails>, server: ConnectedGameServer ) => {

        if ( server && server.client && server.client.getServerProperties().isServerAcceptingPlayers ) {
            let properties: L2GameServerParameters = server.client.getServerProperties()

            if ( properties.serverStatus === ServerStatus.statusGmOnly && currentAccessLevel < 10 ) {
                return serverData
            }

            let adjustedStatus = currentAccessLevel > 0 ? properties.serverStatus : ServerStatus.statusDown
            let item: RegisteredServerDetails = {
                ip: properties.ip,
                port: properties.port,
                pvp: properties.isPVP,
                serverType: properties.serverType,
                currentPlayers: server.client.getPlayerCount(),
                maxPlayers: properties.maxPlayers,
                ageLimit: properties.ageLimit,
                useBrackets: properties.useBrackets,
                status: properties.serverStatus !== ServerStatus.statusGmOnly ? properties.serverStatus : adjustedStatus,
                serverId: properties.serverId,
            }

            serverData.push( item )
        }

        return serverData
    }, [] )
}