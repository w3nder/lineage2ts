import { DeclaredServerPacket } from '../DeclaredServerPacket'

export enum AccountKickedReason {
    dataStealer = 0x01,
    genericViolation = 0x08,
    sevenDaysSuspended = 0x10,
    permanentlyBanned = 0x20,
}

export function AccountKicked( errorCode: AccountKickedReason ): Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x02 )
            .writeD( errorCode )
            .getBuffer()
}