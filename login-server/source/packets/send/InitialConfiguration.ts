import { DeclaredServerPacket } from '../DeclaredServerPacket'

let data: Buffer = Buffer.from( [ 0x4E, 0x95, 0xDD, 0x29, 0xFC, 0x9C, 0xC3, 0x77, 0x20, 0xB6, 0xAD, 0x97, 0xF7, 0xE0, 0xBD, 0x07 ] )
const premadeBinaryChunk = Buffer.allocUnsafeSlow( data.length )

data.copy( premadeBinaryChunk )

export function InitialConfiguration( publicKey: Buffer, blowfishKey: Buffer, sessionId: number, protocolVersion : number = 0x0000c621 ): Buffer {
    /*
        First packet to be sent out. It will need additional four bytes for XORPass information.
     */
    return new DeclaredServerPacket( 10 + publicKey.length + premadeBinaryChunk.length + blowfishKey.length + 4 )
            .writeC( 0x00 )
            .writeD( sessionId )
            .writeD( protocolVersion )
            .writeB( publicKey )

            .writeB( premadeBinaryChunk )
            .writeB( blowfishKey )
            .writeC( 0x00 )
            .getBuffer()
}