// readC - 1 byte
// readH - 2 byte
// readD - 4 byte
// readF - 8 byte
// readB - Uint8 array/buffer
// readS - string
// readQ - 64-bit integer or BigInt
export interface IClientPacket {
    readC() : number
    readH() : number
    readD() : number
    readF() : number
    readB( length : number ) : Buffer
    readS() : string
    readQ() : BigInt
}