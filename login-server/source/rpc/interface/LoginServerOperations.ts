import { SessionKeyProperties } from '../../service/SessionKey'

export const enum L2LoginServerOperations {
    RegisterServer,
    PlayerLogin,
    PlayerLogout,
    PlayerAuthentication,
    PlayerCharactersOnServer,
    PlayerChangesPassword,
    PlayerChangesAccessLevel,
    ServerStatus,
    AccountBan,
    IPBan
}

export interface L2LoginServerCharacters {
    accountName : string
    totalCharacters : number
    timesToDelete : Array<number>
}

export interface L2GameServerParameters {
    isPVP: boolean
    serverType: number
    currentPlayers: number
    maxPlayers: number
    ageLimit: number
    useBrackets: boolean
    serverStatus: number
    isServerAcceptingPlayers: boolean
    serverId : number
    ip: Array<number>
    port: number
}

export interface L2LoginServerEvent {
    operationId : number
}

export interface L2LoginRegisterServerEvent extends L2LoginServerEvent, L2GameServerParameters {
    isAcceptAlternativeId: boolean
}

export interface L2LoginPlayerLogoutEvent extends L2LoginServerEvent {
    accountName: string
}

export interface L2LoginPlayerLoginEvent extends L2LoginServerEvent {
    accountName: string
}

export interface L2LoginPlayerAuthenticationEvent extends L2LoginServerEvent {
    accountName: string
    key: SessionKeyProperties
}

export interface L2LoginPlayerCharactersOnServerEvent extends L2LoginServerEvent, L2LoginServerCharacters {

}

export interface L2LoginPlayerChangesPasswordEvent extends L2LoginServerEvent {
    accountName: string
    currentPassword: string
    futurePassword: string
}

export interface L2LoginPlayerChangesAccessLevelEvent extends L2LoginServerEvent {
    accountName: string
    level : number
    reason: string
}

export interface L2LoginServerStatusEvent extends L2LoginServerEvent, L2GameServerParameters {

}

export interface L2LoginServerBanEvent {
    expirationTime: number
    reason: string
}

export interface L2LoginAccountBanEvent extends L2LoginServerEvent, L2LoginServerBanEvent {
    accountName: string
}

export interface L2LoginIPBanEvent extends L2LoginServerEvent, L2LoginServerBanEvent {
    ipAddress: string
}