import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2LoginServerConfigurationApi } from '../../interface/ServerConfigurationApi'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

export const DotEnvServerConfiguration: L2LoginServerConfigurationApi = {
    getGameServerPort(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'GameServerPort' )
    },

    getLoginServerPort(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'LoginServerPort' )
    },

    getLoginServerResponseDelay(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'LoginServerResponseDelay' )
    },

    getAutoCreateAccessLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AutoCreateAccessLevel' )
    },

    getRateLimitingAccountRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RateLimitingAccountRate' )
    },

    getLoginServerConnectionDuration(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'LoginServerConnectionDuration' )
    },

    getBadPasswordLockoutTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BadPasswordLockoutTime' )
    },

    getBadPasswordRetries(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BadPasswordRetries' )
    },

    getRateLimitingBurstRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RateLimitingBurstRate' )
    },

    getRateLimitingBurstThreshold(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RateLimitingBurstThreshold' )
    },

    getRateLimitingMaxConnections(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RateLimitingMaxConnections' )
    },

    shouldAutoCreateAccounts(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AutoCreateAccounts' )
    },

    isGameServerAutoAssignId(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GameServerAutoAssignId' )
    },

    isRateLimitingEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RateLimitingEnabled' )
    },

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'server.properties' )
        cachedValues = {}
        return
    },

    showLicense(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ShowLicence' )
    }
}