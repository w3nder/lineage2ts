import { L2LoginDatabaseConfigurationApi } from '../../interface/DatabaseConfigurationApi'
import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

export const DotEnvDatabaseConfiguration: L2LoginDatabaseConfigurationApi = {
    getDatabaseName(): string {
        return configuration[ 'DatabaseName' ]
    },

    getEngine(): string {
        return configuration[ 'Engine' ]
    },

    getDatabaseHost(): string {
        return configuration[ 'DatabaseHost' ]
    },

    getDatabasePassword(): string {
        return configuration[ 'DatabasePassword' ]
    },

    getDatabasePort(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DatabasePort' )
    },

    getDatabaseUsername(): string {
        return configuration[ 'DatabaseUsername' ]
    },

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'database.properties' )
        cachedValues = {}
        return
    },
}