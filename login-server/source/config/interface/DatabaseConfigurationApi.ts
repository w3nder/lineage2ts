import { L2LoginConfigurationApi } from '../IConfiguration'

export interface L2LoginDatabaseConfigurationApi extends L2LoginConfigurationApi {
    getEngine(): string

    getDatabaseHost() : string
    getDatabasePort() : number
    getDatabaseUsername() : string
    getDatabasePassword() : string
    getDatabaseName() : string
}