import { L2LoginConfigurationApi } from '../IConfiguration'

export interface L2LoginServerConfigurationApi extends L2LoginConfigurationApi {

    shouldAutoCreateAccounts(): boolean
    getAutoCreateAccessLevel() : number
    isGameServerAutoAssignId(): boolean

    showLicense(): boolean

    getBadPasswordRetries() : number
    getBadPasswordLockoutTime() : number

    isRateLimitingEnabled() : boolean
    getRateLimitingMaxConnections() : number
    getRateLimitingBurstThreshold() : number
    getRateLimitingBurstRate() : number
    getRateLimitingAccountRate() : number

    getLoginServerConnectionDuration() : number
    getLoginServerResponseDelay() : number

    getLoginServerPort() : number
    getGameServerPort() : number
}