import { L2LoginDatabaseConfigurationApi } from './interface/DatabaseConfigurationApi'
import { L2LoginServerConfigurationApi } from './interface/ServerConfigurationApi'

export interface L2LoginConfigEngine {
    database: L2LoginDatabaseConfigurationApi
    server: L2LoginServerConfigurationApi
}