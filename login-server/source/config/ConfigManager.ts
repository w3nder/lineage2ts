import { L2LoginConfigEngine } from './IConfigEngine'
import { L2LoginConfigurationApi } from './IConfiguration'
import { DotEnvConfigurationEngine } from './engine/dotenv'
import pkgDir from 'pkg-dir'
import logSymbols from 'log-symbols'
import aigle from 'aigle'
import _ from 'lodash'
import dotenv from 'dotenv'

const rootDirectory = pkgDir.sync( __dirname )
dotenv.config( { path: `${ rootDirectory }/.env` } )

const { configurationEngine, configurationReloadMinutes } = process.env
const reloadTime = _.parseInt( configurationReloadMinutes ) * 60000

const allEngines: { [ name: string ]: L2LoginConfigEngine } = {
    'dotenv': DotEnvConfigurationEngine,
}

console.log( logSymbols.info, `Login Server: using '${ configurationEngine }' engine` )

if ( !_.isString( configurationEngine ) || !allEngines[ configurationEngine ] ) {
    throw new Error( 'Please specify valid \'configurationEngine\' value!' )
}

export const ConfigManager: L2LoginConfigEngine = allEngines[ configurationEngine as string ]

export async function loadConfiguration() {
    await aigle.resolve( ConfigManager ).each( async ( config: L2LoginConfigurationApi ) => {
        return config.load()
    } )

    console.log( `${ logSymbols.success } Login Server: Loaded ${ _.size( ConfigManager ) } configs.` )
}

if ( reloadTime > 0 ) {
    setInterval( loadConfiguration, reloadTime )
} else {
    console.log( `${ logSymbols.info } Login Server: Hot reloading is disabled.` )
}