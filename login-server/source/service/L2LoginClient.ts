import { Socket } from 'net'
import { LoginCrypt } from '../security/crypt/LoginCrypt'
import { SessionKeyHelper, SessionKeyProperties } from './SessionKey'
import { LoginManager } from '../cache/LoginManager'
import { AuthenticatedGameGuard } from '../packets/receive/AuthenticatedGameGuard'
import { GameGuardSuccess } from '../packets/send/GameGuardSuccess'
import { LoginFail, LoginFailReason } from '../packets/send/LoginFail'
import { AuthenticatedLogin } from '../packets/manager/AuthenticatedLogin'
import { RequestServerLogin } from '../packets/manager/RequestServerLogin'
import { RequestServerList } from '../packets/manager/RequestServerList'
import { InitialConfiguration } from '../packets/send/InitialConfiguration'
import { ConfigManager } from '../config/ConfigManager'
import { ServerList } from '../packets/send/ServerList'
import { createKeyPair, L2KeyPair } from '../security/ScrambledKeyPair'
import { ServerLog } from '../logger/Logger'
import Timeout = NodeJS.Timeout

enum LoginClientState {
    CONNECTED,
    AUTHED_GG,
    AUTHED_LOGIN,
}

export class L2LoginClient {
    connection: Socket
    state: LoginClientState = LoginClientState.CONNECTED
    scrambledPair: L2KeyPair = createKeyPair()
    blowfishKey: Buffer = LoginManager.getBlowfishKey()
    sessionId: number = SessionKeyHelper.createSessionId()
    loginCrypt: LoginCrypt
    charactersOnServers: any = {}
    charactersToDelete: any = {}
    lastServer: any = null
    accessLevel: number = null
    connectionHash: string
    account: any
    joinedGS: boolean = false
    sessionKey: any
    enableServerListUpdates: boolean = false

    sendWelcomePacketTask: Timeout
    cleanUpTask: Timeout
    loginTimeExpiredTask: Timeout

    constructor( socket : Socket ) {
        /*
            Disabling Nagle's algorithm delay before sending packet data.
         */
        socket.setNoDelay( true )

        this.connection = socket
        this.loginCrypt = new LoginCrypt( this.blowfishKey )

        this.connectionHash = [ this.connection.remoteAddress, this.connection.remotePort, this.sessionId.toString() ].join( '#' )

        this.connection.on( 'data', this.onProcessData.bind( this ) )
        this.connection.on( 'close', this.onProcessClose.bind( this ) )
        this.connection.on( 'error', this.onProcessError.bind( this ) )

        let delay: number = Math.max( ConfigManager.server.getLoginServerResponseDelay(), 500 )
        this.sendWelcomePacketTask = setTimeout( this.sendWelcomePacket.bind( this ), delay )
        this.loginTimeExpiredTask = setTimeout( this.onLoginTimeExpired.bind( this ), ConfigManager.server.getLoginServerConnectionDuration() * 1000 + delay )
    }

    sendWelcomePacket() {
        this.sendPacket( InitialConfiguration( this.scrambledPair.modulus, this.blowfishKey, this.sessionId ) )
    }

    onProcessData( incomingData : Buffer ): void {

        try {
            let decryptedPacket = this.loginCrypt.decrypt( incomingData.subarray( 2 ) ) // stripping packet size bytes

            let opcode : number = decryptedPacket.readUInt8( 0 )

            switch ( this.state ) {
                case LoginClientState.CONNECTED:
                    return this.onConnected( opcode, decryptedPacket )

                case LoginClientState.AUTHED_GG:
                    return this.onAuthenticatedGameGuard( opcode, decryptedPacket )

                case LoginClientState.AUTHED_LOGIN:
                    return this.onAuthenticatedLogin( opcode, decryptedPacket )
            }

            ServerLog.error( 'Unknown connection state detected' )
        } catch ( error: any ) {
            ServerLog.error( error, 'Failed L2LoginClient.onProcessData' )
        }

        this.connection.end()
    }

    onConnected( operation: number, packet : Buffer ) : void {
        if ( operation === 0x07 && AuthenticatedGameGuard( this.sessionId, packet ) ) {
            this.state = LoginClientState.AUTHED_GG
            return this.sendPacket( GameGuardSuccess( this.sessionId ) )
        }

        ServerLog.trace( `onConnected operation '${ operation }' is not recognized.` )
        return this.closeConnection( LoginFail( LoginFailReason.accessFailed ) )
    }

    onAuthenticatedGameGuard( operation: number, packet: Buffer ) : void {
        if ( operation === 0x00 ) {
            AuthenticatedLogin.process( this, packet )
            return
        }

        ServerLog.trace( `L2LoginClient.onAuthenticatedGameGuard operation '${ operation }' is not recognized.` )
        return this.closeConnection( LoginFail( LoginFailReason.accessFailed ) )
    }

    onAuthenticatedLogin( operation: number, packet: Buffer ) : void {
        if ( operation === 0x02 ) {
            return this.sendPacket( RequestServerLogin( this, packet ) )
        }

        if ( operation === 0x05 ) {
            return this.sendPacket( RequestServerList( this, packet ) )
        }

        ServerLog.trace( `L2LoginClient.onAuthenticatedLogin operation '${ operation }' is not recognized.` )
        return this.closeConnection( LoginFail( LoginFailReason.accessFailed ) )
    }

    onProcessClose() {
        this.enableServerListUpdates = false

        /*
            When client disconnects, login client would still need to be available since GS would need to reach to login
            server and validate session keys in order for client to login into game server. Hence, login client should be silently cleaned up later.
         */
        ServerLog.trace( `Connection to the login server is closed by: ${ this.connection.remoteAddress }:${ this.connection.remotePort }` )
        this.stopCleanUpTask()
        this.cleanUpTask = setTimeout( this.runCleanupTask.bind( this ), 10000 )
    }

    onProcessError() {
        ServerLog.info( `Client connection lost for: ${ this.connection.remoteAddress }:${ this.connection.remotePort }` )
        LoginManager.removeClient( this.connectionHash )
    }

    stopCleanUpTask() {
        if ( this.cleanUpTask ) {
            clearTimeout( this.cleanUpTask )
            this.cleanUpTask = null
        }
    }

    getConnectionHash() {
        return this.connectionHash
    }

    getConnectionIpAddress() : string {
        return this.connection.remoteAddress
    }

    getState() {
        return this.state
    }

    setStateLoginAuthenticated() {
        this.state = LoginClientState.AUTHED_LOGIN
    }

    getRSAKeyPair() : any {
        return this.scrambledPair.value
    }

    getAccount() {
        return this.account
    }

    setAccount( account ) {
        this.account = account
    }

    setAccessLevel( accessLevel ) {
        this.accessLevel = accessLevel
    }

    getAccessLevel() {
        return this.accessLevel
    }

    setLastServer( lastServer ) {
        this.lastServer = lastServer
    }

    getLastServer() {
        return this.lastServer
    }

    getSessionId() {
        return this.sessionId
    }

    setJoinedGS( value: boolean ) {
        this.joinedGS = value
    }

    setSessionKey( sessionKey: SessionKeyProperties ): void {
        this.sessionKey = sessionKey
    }

    getSessionKey(): SessionKeyProperties {
        return this.sessionKey
    }

    sendPacket( packet ) : void {
        this.connection.write( this.loginCrypt.encrypt( packet ) )
    }

    closeConnection( packet ) : void {
        this.connection.end( this.loginCrypt.encrypt( packet ) )
    }

    setCharactersOnServer( serverId: number, numberOfCharacters: number ) {
        this.charactersOnServers[ serverId ] = numberOfCharacters
    }

    getCharactersOnServer() {
        return this.charactersOnServers
    }

    setCharactersWaitingDeleteOnServer( serverId: number, timesToDelete: Array<number> ) {
        this.charactersToDelete[ serverId ] = timesToDelete
    }

    getCharactersWaitingDeleteOnServer() {
        return this.charactersToDelete
    }

    runCleanupTask() {
        clearTimeout( this.sendWelcomePacketTask )
        clearTimeout( this.loginTimeExpiredTask )

        LoginManager.removeClient( this.connectionHash )
    }

    onLoginTimeExpired() {
        this.closeConnection( LoginFail( LoginFailReason.inactive ) )
    }

    sendServerList() {
        if ( !this.connection ) {
            return
        }

        this.sendPacket( ServerList( this ) )
    }
}