import { L2GameServerClient } from './GameServerClient'
import { ConnectedGameServer } from '../models/RegisteredGameServer'
import { AvailableServerNames } from '../config/serverNames'
import { BanManager } from '../cache/BanManager'
import { L2GameServerParameters, L2LoginRegisterServerEvent } from '../rpc/interface/LoginServerOperations'
import { Socket } from 'net'
import _ from 'lodash'
import { ServerLog } from '../logger/Logger'

const allPossibleServerIds: Array<number> = Object.keys( AvailableServerNames ).map( value => parseInt( value ) )

class Manager {
    connectedClients: { [ key: string ]: L2GameServerClient } = {}
    registeredGameServers: Array<ConnectedGameServer> = []

    addClient( client: L2GameServerClient ) : void {
        this.connectedClients[ client.getConnectionHash() ] = client
    }

    registerServer( client: L2GameServerClient, event: L2LoginRegisterServerEvent ): boolean {
        this.removeRegisteredServer( event.serverId )
        return this.registerWithSpecifiedId( client, event )
    }

    /*
        TODO: is prefetch really needed?
     */
    prefetchCharactersOnAccount( accountName: string ) : void {
        this.registeredGameServers.forEach( ( currentServer: ConnectedGameServer ) => {
            if ( currentServer.client && currentServer.client.getServerProperties().isServerAcceptingPlayers ) {
                currentServer.client.requestServerCharacters( accountName )
            }
        } )
    }

    getRegisteredGameServerById( serverId: number ): ConnectedGameServer {
        return this.registeredGameServers.find( server => server.serverId === serverId )
    }

    getRegisteredGameServers(): Array<ConnectedGameServer> {
        return this.registeredGameServers
    }

    isAllowed( connection : Socket ) : boolean {
        return BanManager.isAllowed( connection.remoteAddress )
    }

    canRegister( id: number ): boolean {
        return !this.getRegisteredGameServerById( id )
    }

    registerWithSpecifiedId( client: L2GameServerClient, event: L2LoginRegisterServerEvent ): boolean {
        let existingServer: ConnectedGameServer = this.getRegisteredGameServerById( event.serverId )

        if ( !existingServer && AvailableServerNames[ event.serverId ] ) {
            let server: ConnectedGameServer = {
                serverId: event.serverId,
                serverName: AvailableServerNames[ event.serverId ],
                host: event.ip.join( '.' ),
                client,
                port: event.port,
            }

            this.registeredGameServers.push( server )

            ServerLog.info( `Registered game server '${ server.serverName }' on ${ server.host }:${ server.port }` )
            return true
        }

        return false
    }

    registerWithFirstAvailableId( client: L2GameServerClient, serverProperties: L2LoginRegisterServerEvent ): number {
        let usedServerIds = new Set<number>( allPossibleServerIds )
        this.registeredGameServers.forEach( server => usedServerIds.delete( server.serverId ) )

        if ( usedServerIds.size === 0 ) {
            ServerLog.warn( 'No server registration ids left!' )
            return 0
        }

        let values = usedServerIds.values()
        let nextFreeId : number = values.next().value

        let server: ConnectedGameServer = {
            serverId: nextFreeId,
            serverName: AvailableServerNames[ nextFreeId ],
            host: serverProperties.ip.join( '.' ),
            client,
            port: serverProperties.port,
        }

        this.registeredGameServers.push( server )

        ServerLog.info( `Auto-registered game server '${ server.serverName }' on ${ server.host }:${ server.port }` )
        return nextFreeId
    }

    removeClient( connectionHash : string ) {
        let client = this.connectedClients[ connectionHash ]
        if ( client ) {
            let parameters: L2GameServerParameters = client.getServerProperties()
            if ( parameters ) {
                this.removeRegisteredServer( parameters.serverId )
            }
        }

        delete this.connectedClients[ connectionHash ]
    }

    removeRegisteredServer( serverId : number ) {
        _.remove( this.registeredGameServers, ( server : ConnectedGameServer ) => server.serverId === serverId )
    }

    getClientByAccountName( accountName : string ) : L2GameServerClient {
        let server : ConnectedGameServer = this.registeredGameServers.find( ( currentServer: ConnectedGameServer ) : boolean => {
            return currentServer.client && currentServer.client.hasAccountOnGameServer( accountName )
        } )

        return server.client
    }
}

export const GameServerManager = new Manager()