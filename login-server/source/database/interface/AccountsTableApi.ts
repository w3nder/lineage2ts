export interface L2AccountsTableApi {
    updateAccountLastServerId( accountName: string, serverId: number ) : Promise<void>
    updateAccountPassword( accountName: string, passwordHash: string ) : Promise<void>
    setAccountAccessLevel( accountName: string, level: number ) : Promise<void>
    updateLastAccess( times: Array<L2AccountsAccessTime> ) : Promise<void>
    createAccount( accountName: string, passwordHash: string, accessLevel: number, passwordVersion: number ) : Promise<void>
    getAccount( accountName: string ) : Promise<L2AccountInformation>
    setVariables( accountName : string, variables : { [ key: string] : any } ) : Promise<void>
    getAccountSummary() : Promise<L2AccountsCount>
}

export interface L2AccountInformation {
    login: string
    password: string
    accessLevel : number
    createdTime : number
    lastAccess : number
    lastServerId : number
    variables : { [ key: string] : any }
}

export interface L2AccountsCount {
    total: number
}

export interface L2AccountsAccessTime {
    name: string
    time: number
}