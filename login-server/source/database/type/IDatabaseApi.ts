import { L2AccountsTableApi } from '../interface/AccountsTableApi'
import { L2BanTableApi } from '../interface/BanTableApi'

export interface L2LoginDatabaseApi {
    accountsTable: L2AccountsTableApi
    banTable : L2BanTableApi
    operations: L2DatabaseOperations
}

export interface L2DatabaseOperations {
    shutdown() : Promise<void>
}