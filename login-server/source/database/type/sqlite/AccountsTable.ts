import { L2AccountInformation, L2AccountsAccessTime, L2AccountsCount, L2AccountsTableApi } from '../../interface/AccountsTableApi'
import { SQLiteDatabaseEngine } from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteAccountsTable: L2AccountsTableApi = {
    async getAccountSummary(): Promise<L2AccountsCount> {
        const query = 'select count(login) as total from accounts'
        let data = SQLiteDatabaseEngine.getOne( query )
        return {
            total: data[ 'total' ]
        }
    },

    async setVariables( accountName: string, variables: { [ p: string ]: any } ): Promise<void> {
        const query = 'UPDATE accounts SET variables_json = ? WHERE login = ?'
        return SQLiteDatabaseEngine.insertOne( query, JSON.stringify( variables ), accountName )
    },

    async updateAccountLastServerId( accountName: string, serverId: number ): Promise<void> {
        const query = 'UPDATE accounts SET lastServerId = ? WHERE login = ?'
        return SQLiteDatabaseEngine.insertOne( query, serverId, accountName )
    },

    async updateLastAccess( times: Array<L2AccountsAccessTime> ): Promise<void> {
        const query = 'UPDATE accounts SET lastAccess = ? WHERE login = ?'
        const parameters = times.map( ( item: L2AccountsAccessTime ) => {
            return [ item.time, item.name ]
        } )

        return SQLiteDatabaseEngine.insertMany( query, parameters )
    },


    async createAccount( accountName: string, passwordHash: string, accessLevel: number, passwordVersion: number ): Promise<void> {
        const query = 'INSERT INTO accounts (login, password, accessLevel, createdTime, lastAccess, passwordVersion) values (?, ?, ?, ?, ?, ?)'
        return SQLiteDatabaseEngine.insertOne( query, accountName, passwordHash, accessLevel, Date.now(), Date.now(), passwordVersion )
    },

    async getAccount( accountName: string ): Promise<L2AccountInformation> {
        const query = 'SELECT * FROM accounts WHERE login=?'
        const databaseItem: any = SQLiteDatabaseEngine.getOne( query, accountName )

        if ( !databaseItem ) {
            return
        }

        const variables = _.attempt( JSON.parse, databaseItem[ 'variables_json' ] )
        return {
            accessLevel: databaseItem.accessLevel,
            createdTime: databaseItem.createdTime,
            lastAccess: databaseItem.lastAccess,
            lastServerId: databaseItem.lastServerId,
            login: databaseItem.login,
            password: databaseItem.password,
            variables: _.isError( variables ) ? null : variables,
        }
    },

    async setAccountAccessLevel( accountName: string, level: number ): Promise<void> {
        const query = 'UPDATE accounts SET accessLevel = ? WHERE login = ?'
        return SQLiteDatabaseEngine.insertOne( query, level, accountName )
    },

    async updateAccountPassword( accountName: string, passwordHash: string ): Promise<void> {
        const query = 'UPDATE accounts SET password=? WHERE login=?'
        return SQLiteDatabaseEngine.insertOne( query, passwordHash, accountName )
    }
}