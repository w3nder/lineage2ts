import { L2LoginDatabaseApi } from './IDatabaseApi'
import { SQLiteAccountsTable } from './sqlite/AccountsTable'
import { SQLiteBanTable } from './sqlite/BanTable'
import { SQLiteLoginDatabaseOperations } from './sqlite/Operations'

export const SQLiteLoginDatabase : L2LoginDatabaseApi = {
    accountsTable: SQLiteAccountsTable,
    banTable: SQLiteBanTable,
    operations: SQLiteLoginDatabaseOperations
}