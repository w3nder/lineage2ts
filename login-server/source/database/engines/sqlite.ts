import { ConfigManager } from '../../config/ConfigManager'
import _ from 'lodash'
import pkgDir from 'pkg-dir'
import Database from 'better-sqlite3'
import { ServerLog } from '../../logger/Logger'

const rootDirectory = pkgDir.sync( __dirname )
const database = new Database( `${rootDirectory}/${ConfigManager.database.getDatabaseName()}`, {
    fileMustExist: true
} )

export const SQLiteDatabaseEngine = {
    getOne( query: string, ...parameters ): unknown {
        try {
            return database.prepare( query ).get( parameters )
        } catch ( error ) {
            ServerLog.fatal( error, 'Query failed: %s', query )
        }

        return null
    },
    getMany( query: string, ...parameters ): Array<unknown> {
        try {
            return database.prepare( query ).all( ...parameters )
        } catch ( error ) {
            ServerLog.fatal( error, 'Query failed: %s', query )
        }

        return []
    },

    insertOne( query: string, ...parameters ): void {
        try {
            database.prepare( query ).run( ...parameters )
        } catch ( error ) {
            ServerLog.fatal( error, 'Query failed: %s', query )
        }
    },

    getManyForValues( query: string, parameters: Array<number | string | boolean> ) : Array<unknown> {
        /**
         * Makes easy to supply multiple values for lookups
         * Example query: SELECT charId FROM characters WHERE charId in (#values#)
         */
        let finalQuery = query.replace( '#values#', _.times( parameters.length, _.constant( '?' ) ).join( ',' ) )
        return this.getMany( finalQuery, parameters )
    },

    insertMany( query: string, parameters: Array<Array<any>> ) : void {
        try {
            let statement = database.prepare( query )

            const insertBulk = database.transaction( ( allItems ) => _.each( allItems, ( itemParameters ) => {
                statement.run( ...itemParameters )
            } ) )

            insertBulk( parameters )
        } catch ( error ) {
            ServerLog.fatal( error, 'Query failed: %s', query )
        }
    },

    deleteManyForValues( query: string, parameters: Array<any> ) : void {
        /**
         * Makes easy to supply multiple values for deletes
         * Example query: DELETE FROM items WHERE object_id in (#values#)
         */
        let finalQuery = query.replace( '#values#', _.times( parameters.length, _.constant( '?' ) ).join( ',' ) )
        return this.insertOne( finalQuery, parameters )
    },

    shutdown() : void {
        database.close()
    }
}