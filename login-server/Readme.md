# Lineage2TS Project Login Server

## Introduction
- login server that allows players to verify and authenticate clients
- allows multiple game servers to connect and register
- only H5 client versions are supported

## How to run Login Server?

- install dependencies via `npm install`
- compile Typescrip to JS via `npm run build`
- ensure you have `login.database` SQLite file in same directory as `package.json`
- run JS file in `./dist/source/Start.js`

## Server configuration

- configuration files are available in `./configuration` directory
- if configured (by default), server will reload configuration file on interval described in `.env` file
- observe default or example values provided with documentation blocks in each file

## Networking
- server requires `2106` be available

## Contributions
Any and all code contributions are welcome. Please see [template](../.gitlab/merge_request_templates/login-server.md) for code change acceptance actions.

## License
Any and all Lineage2TS Project files use `AGPL-3.0-or-later` license, see [LICENSE](LICENSE) file for legal description