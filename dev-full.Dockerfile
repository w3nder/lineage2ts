# Normal build process that involes fresh build of databases and assets
FROM node:22.9.0-bullseye AS build

RUN mkdir -p /opt/lineage2ts
WORKDIR /opt/lineage2ts

COPY cli /opt/lineage2ts/cli
COPY game-server /opt/lineage2ts/game-server
COPY login-server /opt/lineage2ts/login-server

WORKDIR /opt/lineage2ts/
RUN npm ci

WORKDIR /opt/lineage2ts/cli
RUN chmod +x ./provisionDefaultArtifacts.sh && ./provisionDefaultArtifacts.sh
RUN chmod +x ./provisionGeopackArtifacts.sh && ./provisionGeopackArtifacts.sh

RUN cd /opt/lineage2ts/game-server && npm run build
RUN cd /opt/lineage2ts/login-server && npm run build
RUN cd /opt/lineage2ts && npm prune --production

FROM node:22.9.0-slim

RUN mkdir -p /opt/lineage2ts/game-server && mkdir -p /opt/lineage2ts/login-server
WORKDIR /opt/lineage2ts

COPY --from=build /opt/lineage2ts/login.database /opt/lineage2ts/login-server/login.database
COPY --from=build /opt/lineage2ts/game.database /opt/lineage2ts/game-server/game.database
COPY --from=build /opt/lineage2ts/datapack.database /opt/lineage2ts/game-server/datapack.database
COPY --from=build /opt/lineage2ts/cli/geopack.database /opt/lineage2ts/game-server/geopack.database

COPY --from=build /opt/lineage2ts/game-server /opt/lineage2ts/game-server
COPY --from=build /opt/lineage2ts/login-server /opt/lineage2ts/login-server
COPY --from=build /opt/lineage2ts/node_modules /opt/lineage2ts/node_modules

COPY docker-entry.sh /docker-entry.sh

ENV NODE_ENV production

LABEL maintainer="MrTREX" version="dev"  website="https://gitlab.com/MrTREX/lineage2ts"

EXPOSE 7777 2106
VOLUME ["/opt/lineage2ts/login-server", "/opt/lineage2ts/game-server"]

RUN chmod +x /docker-entry.sh
CMD [ "/docker-entry.sh" ]