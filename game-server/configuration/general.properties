# ---------------------------------------------------------------------------
# General Server Settings
# ---------------------------------------------------------------------------
# The defaults are set to be retail-like. If you modify any of these settings your server will deviate from being retail-like.
# Warning:
# Please take extreme caution when changing anything. Also please understand what you are changing before you do so on a live server.

# ---------------------------------------------------------------------------
# Administrator
# ---------------------------------------------------------------------------

# When True, every player will have master access level (8).
# Default: False
EverybodyHasAdminRights = False

# If True, only accounts with GM access can enter the server.
# Default: False
ServerGMOnly = False

# Enable GMs to have the glowing aura of a Hero character on login.
# Notes:
#	GMs can do "///hero" on themselves and get this aura voluntarily.
#	It's advised to keep this off due to graphic lag.
# Default: False
GMHeroAura = False

# Auto set invulnerable status to a GM on login.
# Default: False
GMStartupInvulnerable = False

# Auto set invisible status to a GM on login.
# Default: False
GMStartupInvisible = False

# Auto block private messages to a GM on login.
# Default: False
GMStartupSilence = False

# Auto list GMs in GM list (/gmlist) on login.
# Default: False
GMStartupAutoList = False

# Auto set diet mode on to a GM on login (affects your weight penalty).
# Default: False
GMStartupDietMode = False

# Item restrictions apply to GMs as well? (True = restricted usage)
# Default: True
GMItemRestriction = True

# Skill restrictions apply to GMs as well? (True = restricted usage)
# Default: True
GMSkillRestriction = True

# Allow GMs to drop/trade non-tradable and quest(drop only) items
# Default: False
GMTradeRestrictedItems = False

# Allow GMs to restart/exit while is fighting stance
# Default: True
GMRestartFighting = True

# Show the GM's name behind an announcement made by him
# example: "Announce: hi (HanWik)"
GMShowAnnouncerName = False

# Show the GM's name before an announcement made by him
# example: "Nyaran: hi"
GMShowCritAnnouncerName = False

# Give special skills for every GM
# 7029,7041-7064,7088-7096,23238-23249 (Master's Blessing)
# Default: False
GMGiveSpecialSkills = False

# Give special aura skills for every GM
# 7029,23238-23249,23253-23296 (Master's Blessing)
# Default: False
GMGiveSpecialAuraSkills = False


# ---------------------------------------------------------------------------
# Server Security
# ---------------------------------------------------------------------------

# Enforce gameguard for clients. Sends a gameguard query on character login.
# Default: False
GameGuardEnforce = False

# Don't allow player to perform trade, talk with npc, or move until gameguard reply is received.
# Default: False
GameGuardProhibitAction = False

#Logging settings. The following four settings, while enabled, will increase writing to your hard drive(s) considerably. Depending on the size of your server, the amount of players, and other factors, you may suffer a noticable performance hit.
# Default: False
LogChat = False

# Default: False
LogAutoAnnouncements = False

# Default: False
LogItems = False

# Log only Adena and equippable items if LogItems is enabled
LogItemsSmallLog = False

# Default: False
LogItemEnchants = False

# Default: False
LogSkillEnchants = False

# Default: False
GMAudit = False

# Check players for non-allowed skills
# Default: False
SkillCheckEnable = False

# If true, remove invalid skills from player and database.
# Report only, if false.
# Default: False
SkillCheckRemove = False

# Check also GM characters (only if SkillCheckEnable = True)
# Default: True
SkillCheckGM = True


# ---------------------------------------------------------------------------
# Thread Configuration
# ---------------------------------------------------------------------------

# Extreme caution should be here, set to defaults if you do not know what you are doing.
# These could possibly hurt your servers performance or improve it depending on your server's configuration, size, and other factors.
# Default: 10
ThreadPoolSizeEffects = 10

# Default: 13
ThreadPoolSizeGeneral = 13

# Default: 2
ThreadPoolSizeEvents = 2

# Default: 2
UrgentPacketThreadCoreSize = 2

# Default: 4
GeneralPacketThreadCoreSize = 4

# Default: 4
GeneralThreadCoreSize = 4

# Default: 6
AiMaxThread = 6

# Default: 5
EventsMaxThread = 5

# Dead Lock Detector (a separate thread for detecting deadlocks).
# For improved crash logs and automatic restart in deadlock case if enabled.
# Check interval is in seconds.
# Default: True
DeadLockDetector = True

# Default: 20
DeadLockCheckInterval = 20

# Default: False
RestartOnDeadlock = False

# ---------------------------------------------------------------------------
# Optimization
# ---------------------------------------------------------------------------

# Items on ground management.
# Allow players to drop items on the ground.
# Default: True
AllowDiscardItem = True

# List of item id that will not be destroyed (separated by "," like 57,5575,6673).
# Notes:
#	Make sure the lists do NOT CONTAIN trailing spaces or spaces between the numbers!
#	Items on this list will be protected regardless of the following options.
# Default:
ProtectedItems =

# Cleans up the server database on startup.
# The bigger the database is, the longer it will take to clean up the database(the slower the server will start).
# Sometimes this ends up with 0 elements cleaned up, and a lot of wasted time on the server startup.
# If you want a faster server startup, set this to 'false', but its recommended to clean up the database from time to time.
# Default: True
DatabaseCleanUp = True

# Delete invalid quest from players.
# Default: False
AutoDeleteInvalidQuestData = True

# If True, allows a special handling for drops when chance raises over 100% (eg. when applying chance rates).
# True value causes better drop handling at higher rates.
# Default: True
PreciseDropCalculation = True

# Allow creating multiple non-stackable items at one time?
# Default: True
MultipleItemDrop = True

# Forces full item inventory packet to be sent for any item change.
# Notes:
#	This can increase network traffic
# Default: False
ForceInventoryUpdate = False

# ---------------------------------------------------------------------------
# NPC Animations
# Integer value of ms
# ---------------------------------------------------------------------------
# Default: 10000
MinNPCAnimation = 10000

# Default: 20000
MaxNPCAnimation = 20000

# Default: 5000
MinMonsterAnimation = 5000

# Default: 20000
MaxMonsterAnimation = 20000

# ---------------------------------------------------------------------------
# Player discovery settings
# ---------------------------------------------------------------------------

# Scan interval for player to discover new objects, integer value of ms
# Default: 1250
PlayerObjectScanInterval = 1250

# ---------------------------------------------------------------------------
# Falling Damage
# ---------------------------------------------------------------------------

# Allow characters to receive damage from falling.
# CoordSynchronize = 2 is recommended.
# Default: True
EnableFallingDamage = True


# ---------------------------------------------------------------------------
# Features
# ---------------------------------------------------------------------------

# Peace Zone Modes:
# 0 = Peace All the Time
# 1 = PVP During Siege for siege participants
# 2 = PVP All the Time
# Default: 0
PeaceZoneMode = 0

# Global Chat.
# Available Options: ON, OFF, GM, GLOBAL
# Default: ON
GlobalChat = ON

# Trade Chat.
# Available Options: ON, OFF, GM, GLOBAL
# Default: ON
TradeChat = ON

# Default: True
AllowRefund = True

# Default: True
AllowMail = True

# Default: True
AllowAttachments = True

# If True player can try on weapon and armor in shop.
# Default: True
AllowWear = True

# Default: 5
WearDelay = 5

#Adena cost to try on an item.
# Default: 10
WearPrice = 10

# Chat text limit, number of letters
# Example: 10, meaning messages longer than 10 will be flagged as spam
# Default: 500
ChatTextLimit=500

# Maximum number of linked items in chat
# Example: 10, meaning other items will not be linked
# Default: 20
ChatLinkedItemLimit=20

# ---------------------------------------------------------------------------
# Instances
# ---------------------------------------------------------------------------
# Restores the player to their previous instance (ie. an instanced area/dungeon) on EnterWorld.
# Default: False
RestorePlayerInstance = False

# Set whether summon skills can be used to summon players inside an instance.
# Default: False
AllowSummonInInstance = False

# When a player dies, is removed from instance after a fixed period of time.
# Time in seconds.
# Default: 60
EjectDeadPlayerTime = 60

# When is instance finished, is set time to destruction currency instance.
# Time in seconds.
# Default: 300
InstanceFinishTime = 300


# ---------------------------------------------------------------------------
# Misc Settings
# ---------------------------------------------------------------------------

# Default: True
AllowRace = True

# Default: True
AllowWater = True

# Enable pets for rent (wyvern & strider) from pet managers.
# Default: False
AllowRentPet = False

# Default: True
AllowBoat = True

# Boat broadcast radius.
# If players getting annoyed by boat shouts then radius can be decreased.
# Default: 20000
BoatBroadcastRadius = 20000

# Default: True
AllowCursedWeapons = True

#Allow Pet manager's pets to walk around.
# Default: True
AllowPetWalkers = True

# Should show html rendered page when player logs in, boolean flag
# Default: False
ServerNewsEnabled = False

# Show html rendered page when player logs in, path string
# Default: overrides/html/system/servernews.htm
ServerNewsPath = overrides/html/system/servernews.htm

# Should show clan notices, boolean flag
# Default: True
ClanNoticeTemplateEnabled = True

# Show clan notice messages using predefined template path, path string
# Default: data/html/clanNotice.htm
ClanNoticeTemplatePath = data/html/clanNotice.htm

# Enable the Community Board.
# Default: True
EnableCommunityBoard = True

# Default Community Board page.
# Default: _bbshome
BBSDefault = _bbshome

# Enable chat filter
# Default = False
UseChatFilter = False

# Replace filter words with following chars
ChatFilterChars = ^_^

ChatFilter = suck,gay,rape,fuck,dick

# Banchat for channels, split ";"
# 0 = ALL (white)
# 1 = SHOUT (!)
# 2 = TELL (")
# 3 = PARTY (#)
# 4 = CLAN (@)
# 5 = GM (//gmchat)
# 6 = PETITION_PLAYER (*)
# 7 = PETITION_GM (*)
# 8 = TRADE (+)
# 9 = ALLIANCE ($)
# 10 = ANNOUNCEMENT
# 11 = BOAT
# 12 = L2FRIEND
# 13 = MSNCHAT
# 14 = PARTYMATCH_ROOM
# 15 = PARTYROOM_COMMANDER (Yellow)
# 16 = PARTYROOM_ALL (Red)
# 17 = HERO_VOICE (&)
# 18 = CRITICAL_ANNOUNCE
# 19 = SCREEN_ANNOUNCE
# 20 = BATTLEFIELD
# 21 = MPCC_ROOM
# Default: 0,1,8,17
BanChatChannels = 0,1,8,17

# ---------------------------------------------------------------------------
# Manor
# ---------------------------------------------------------------------------

# Default: True
AllowManor = True

# Manor refresh time in military hours.
# Default: 20 (8pm)
ManorRefreshTime = 20

# Manor refresh time (minutes).
# Default: 00 (start of the hour)
ManorRefreshMin = 00

# Manor period approve time in military hours.
# Default: 4 (4am)
ManorApproveTime = 4

# Manor period approve time (minutes).
# Default: 30
ManorApproveMin = 30

# Manor maintenance time (minutes).
# Default: 6
ManorMaintenanceMin = 6

# Manor Save Type.
# True = Save data into the database after every action
# Default: False
ManorSaveAllActions = False

# Manor Auto Save Period
# Number of hours between saves, set to zero to disable autosave.
# Default: 2 (hour)
ManorSavePeriodRate = 2


# ---------------------------------------------------------------------------
# Lottery
# ---------------------------------------------------------------------------

# Default: True
AllowLottery = True

# Initial Lottery prize.
# Default: 50000
LotteryPrize = 50000

# Lottery Ticket Price
# Default: 2000
LotteryTicketPrice = 2000

# What part of jackpot amount should receive characters who pick 5 wining numbers
# Default: 0.6
Lottery5NumberRate = 0.6

# What part of jackpot amount should receive characters who pick 4 wining numbers
# Default: 0.2
Lottery4NumberRate = 0.2

# What part of jackpot amount should receive characters who pick 3 wining numbers
# Default: 0.2
Lottery3NumberRate = 0.2

# How much Adena receive characters who pick two or less of the winning number
# Default: 200
Lottery2and1NumberPrize = 200

# How long does lottery period last in days
# Set to zero, if you want to have default behavior when lottery ends on Saturday, 19:00
# Use fractional value if you want less than a day period, 0.0416666666667 = 1 hour
# If fraction value is used, hour calculations start from 19:00 (so adding 1 hour makes 20:00)
# Default: 7
LotteryPeriod = 7

# ---------------------------------------------------------------------------
# Item Auction
# ---------------------------------------------------------------------------

# Item auction functionality is enabled/disabled (True/False)
# Default: True
ItemAuctionEnabled = True

# Delay between finished auction and when new auction is started
ItemAuctionDelaySecondsBeforeStart = 60

# Auction extends to specified amount of seconds if one or more new bids added.
# By default auction extends only two times, by 5 and 3 minutes, this custom value used after it.
# Values higher than 60s is not recommended.
# Default: 0
ItemAuctionTimeExtendsOnBid = 0

# Winning item delivery method used to send item to player, case-insensitive string
# Possible values: warehouse, dimensionalMerchant, mail
# Default: dimensionalMerchant
ItemAuctionDeliveryMethod = dimensionalMerchant

# Cron expression for how often new item auction happens
# If cron is happening too fast, delay before start will be enforced
# Default (every Friday at 6 pm) : 0 18 * * 5
ItemAuctionStartCron = 0 18 * * 5

# ---------------------------------------------------------------------------
# Dimension Rift
# ---------------------------------------------------------------------------

# Minimal party size to enter rift. Min = 2, Max = 9.
# If while inside the rift, the party becomes smaller, all members will be teleported back.
# Default: 2
RiftMinPartySize = 2

# Number of maximum jumps between rooms allowed, after this time party will be teleported back
# Default: 4
MaxRiftJumps = 4

# Time in ms the party has to wait until the mobs spawn when entering a room. C4 retail: 10s
# Default: 10000
RiftSpawnDelay = 10000

# Time between automatic jumps in seconds
# Default: 480
AutoJumpsDelayMin = 480

# Default: 600
AutoJumpsDelayMax = 600

# Time Multiplier for stay in the boss room
# Default: 1.5
BossRoomTimeMultiply = 1.5

# Cost in dimension fragments to enter the rift, each party member must own this amount
# Default: 18
RecruitCost = 18

# Default: 21
SoldierCost = 21

# Default: 24
OfficerCost = 24

# Default: 27
CaptainCost = 27

# Default: 30
CommanderCost = 30

# Default: 33
HeroCost = 33


# ---------------------------------------------------------------------------
# Four Sepulchers
# ---------------------------------------------------------------------------

# Default: 50
TimeOfAttack = 50

# Default: 5
TimeOfCoolDown = 5

# Default: 3
TimeOfEntry = 3

# Default: 2
TimeOfWarmUp = 2

# Default: 4
NumberOfNecessaryPartyMembers = 4


# ---------------------------------------------------------------------------
# Punishment
# ---------------------------------------------------------------------------

# Player punishment for illegal actions:
# NONE      - do nothing
# BROADCAST - broadcast warning to GMs only
# KICK      - kick player(default)
# KICK_BAN  - kick & ban player
# JAIL      - jail player (define minutes of jail with param: 0 = infinite)
# Default: KICK
DefaultPunish = KICK

# This setting typically specifies the duration of the above punishment.
# Default: 0
DefaultPunishParam = 0

# Apply default punish if player buy items for zero Adena.
# Default: True
OnlyGMItemsFree = True

# Jail is a PvP zone.
# Default: False
JailIsPvp = False

# Disable all chat in jail (except normal one)
# Default: True
JailDisableChat = True

# Disable all transaction in jail
# Trade/Store/Drop
# Default: False
JailDisableTransaction = False

# Enchant Skill Details Settings
# Default: 1,5
NormalEnchantCostMultipiler = 1
SafeEnchantCostMultipiler = 5

# ---------------------------------------------------------------------------
# Custom Components
# ---------------------------------------------------------------------------

# Default: False
CustomSpawnlistTable = False

# Option to save GM spawn only in the custom table.
# Default: False
SaveGmSpawnOnCustom = False

# Default: False
CustomNpcData = False

# Default: False
CustomNpcBufferTables = False

# Default: False
CustomSkillsLoad = False

# Default: False
CustomItemsLoad = False

# Default: False
CustomMultisellLoad = False

# Default: False
CustomBuyListLoad = False

# ---------------------------------------------------------------------------
# Birthday Settings (sending gifts every year since account creation)
# ---------------------------------------------------------------------------

# Method by which to deliver birthday gifts
# Two options: DimensionalTransfer or Mail
# Default: DimensionalTransfer
BirthdayGiftDelivery = Mail

# Gifts sent with Mail System, comma delimited
# Default: 22187
# Example: 22187, 21619
BirthdayGifts = 22187, 21619

# Mail Subject
BirthdayMailSubject = Happy Birthday!

# Mail Content
# %playerName%: Player name
BirthdayMailText = Hello %playerName%!! Seeing as you're one year older now, I thought I would send you some birthday cheer! Please find your birthday gift attached. May this gift bring you joy and happiness on this very special day.\n\nSincerely, Alegria

# ---------------------------------------------------------------------------
# Handy's Block Checker Event Settings
# ---------------------------------------------------------------------------

# Enable the Handy's Block Checker event
# Default: True
EnableBlockCheckerEvent = True

# Minimum number of members on each team before
# be able to start the event
# Min: 1
# Max: 6
# Retail: 2
BlockCheckerMinTeamMembers = 2

# Fair play
# Players can choose what team to play. However, by
# enabling this property to true, the teams will be
# balanced in the teleport to the arena
HBCEFairPlay = True

# ---------------------------------------------------------------------------
# Hellbound Settings
# ---------------------------------------------------------------------------
# If true, players can enter the Hellbound island without any quests
# Default: False
HellboundWithoutQuest = False

# ---------------------------------------------------------------------------
# Bot Report Button settings
# ---------------------------------------------------------------------------

# Enable the bot report button on the desired game servers.
# Default: True
EnableBotReportButton = True

# Report points restart hour. Format: HH:MM ( PM mode, 24 hours clock)
# Default: 00:00
BotReportPointsResetHour = 00:00

# Delay between reports from the same player (in minutes)
# Default: 30 minutes
BotReportDelay = 30

# Allow players from the same clan to report the same bot
# Default: False
AllowReportsFromSameClanMembers = False

# ---------------------------------------------------------------------------
# Fishing settings
# ---------------------------------------------------------------------------

# Fishing is enabled
# Default: True
AllowFishing = True

# Applies multiplier to extend or minimize total time required to catch fish
# Float based value
# Default: 1
FishingDurationMultiplier = 1.5

# When fish is caught add small exp value to character
# Default: False
AllowFishingExperience = True

# Fishing exp value is 1/10 of total fish starting HP, total = (multiplier) * (1/10 of HP)
# Float based value
# Example: 2.03
# Default: 1
FishingExperienceMultiplier = 0.5

# Default: True
AllowFishingChampionship = True

# Item Id used as reward
FishingChampionshipRewardItemId = 57

# Item count used as reward (for the 5 first winners)
FishingChampionshipReward1 = 800000
FishingChampionshipReward2 = 500000
FishingChampionshipReward3 = 300000
FishingChampionshipReward4 = 200000
FishingChampionshipReward5 = 100000

# Item count used for additional participants
# Only used if amount of participants are higher than first five
FishingChampionshipRewardOthers = 50000

# Amount of champions
# Default : 5
FishingChampionshipParticipantAmount = 10

# Interval cron to end fishing championship
# Default (every Wednesday at 7 pm) : 0 19 * * 3
FishingChampionshipIntervalCron = 0 19 * * 3

# ---------------------------------------------------------------------------
# Items on ground settings
# ---------------------------------------------------------------------------

# Save any dropped items on ground so that they can be preserved through server restarts.
# Default: False
SaveDropped = True

# Time interval in minutes to save in DB items on ground. Disabled = 0.
# Notes:
# Default: 30
SaveDroppedInterval = 30

# Amount of minutes how long dropped item data should be preserved
# If value is zero or negative, all stored data is removed
# Default: 360 (or 6 hours)
SaveDroppedExpiration = 360

# Item types that never expire
SaveDroppedProtectedTypes = ARMOR,WEAPON

# Enable/Disable the emptying of the stored dropped items table after items are loaded into memory (safety setting).
# If the server crashed before saving items, on next start old items will be restored and players may already have picked up some of them so this will prevent duplicates.
# Default: False
SaveDroppedResetAfterLoad = False

# ---------------------------------------------------------------------------
# Mail settings
# ---------------------------------------------------------------------------

# String displayed in from name field when message is sent by game server
# Default: ****
MailSystemName = ****

# Time to expire COD messages, integer value of ms
# Default time is 12 hours, or 12 * 3600000 ms = 43200000
# Value cannot be below one hour or 3600000 ms, otherwise 3600000 will be used
# Default: 43200000
MailCodExpirationTime = 43200000

# Time to expire normal mail messages, integer value of ms
# Default time is 15 days, or 15 * 86400000 = 1296000000
# Value cannot be below one hour or 3600000 ms, otherwise 3600000 will be used
# Default: 1296000000
MailDefaultExpirationTime = 1296000000