import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import _ from 'lodash'

const OLF_ADAMS = 32612
const FREYA = 29179
const FREYAS_SOUL_FRAGMENT = 21723
const SOUL_CLOAK_OF_FREYA = 21720
const minimumLevel = 82
const totalAmount = 20

export class FreyaEmbroideredSoulCloak extends ListenerLogic {
    constructor() {
        super( 'Q10502_FreyaEmbroideredSoulCloak', 'listeners/tracked-10500/FreyaEmbroideredSoulCloak.ts' )
        this.questId = 10502
        this.questItemIds = [ FREYAS_SOUL_FRAGMENT ]
    }

    getQuestStartIds(): Array<number> {
        return [ OLF_ADAMS ]
    }

    getTalkIds(): Array<number> {
        return [ OLF_ADAMS ]
    }

    getAttackableKillIds(): Array<number> {
        return [ FREYA ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10502_FreyaEmbroideredSoulCloak'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        if ( data.eventName === '32612-04.html' ) {
            state.startQuest()

            return this.getPath( data.eventName )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '32612-02.html' : '32612-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32612-05.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, FREYAS_SOUL_FRAGMENT ) >= totalAmount ) {
                            await QuestHelper.giveSingleItem( player, SOUL_CLOAK_OF_FREYA, 1 )
                            await state.exitQuest( false, true )

                            return this.getPath( '32612-06.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '32612-03.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForCommandChannel( data.playerId, data.targetId, this.getName(), async ( state : QuestState, player : L2PcInstance ) : Promise<void> => {
            if ( state.isCondition( 1 ) ) {
                return
            }

            await QuestHelper.rewardAndProgressState( player, state, FREYAS_SOUL_FRAGMENT, _.random( 1, 3 ), totalAmount, data.isChampion, 2 )
        } )

        return
    }
}