import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import _ from 'lodash'

const OLF_ADAMS = 32612
const SCARLET_VAN_HALISHA = 29047
const FRINTEZZAS_SOUL_FRAGMENT = 21724
const SOUL_CLOAK_OF_FRINTEZZA = 21721
const minimumLevel = 80
const totalAmount = 20

export class FrintezzaEmbroideredSoulCloak extends ListenerLogic {
    constructor() {
        super( 'Q10503_FrintezzaEmbroideredSoulCloak', 'listeners/tracked-10500/FrintezzaEmbroideredSoulCloak.ts' )
        this.questId = 10503
        this.questItemIds = [ FRINTEZZAS_SOUL_FRAGMENT ]
    }

    getQuestStartIds(): Array<number> {
        return [ OLF_ADAMS ]
    }

    getTalkIds(): Array<number> {
        return [ OLF_ADAMS ]
    }

    getAttackableKillIds(): Array<number> {
        return [ SCARLET_VAN_HALISHA ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10503_FrintezzaEmbroideredSoulCloak'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        if ( data.eventName === '32612-04.html' ) {
            state.startQuest()

            return this.getPath( data.eventName )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '32612-02.html' : '32612-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32612-05.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, FRINTEZZAS_SOUL_FRAGMENT ) >= totalAmount ) {
                            await QuestHelper.giveSingleItem( player, SOUL_CLOAK_OF_FRINTEZZA, 1 )
                            await state.exitQuest( false, true )

                            return this.getPath( '32612-06.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '32612-03.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForCommandChannel( data.playerId, data.targetId, this.getName(), async ( state : QuestState, player : L2PcInstance ) : Promise<void> => {
            if ( state.isCondition( 1 ) ) {
                return
            }

            await QuestHelper.rewardAndProgressState( player, state, FRINTEZZAS_SOUL_FRAGMENT, _.random( 1, 3 ), totalAmount, data.isChampion, 2 )
        } )

        return
    }
}