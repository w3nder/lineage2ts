import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

const THEODRIC = 30755
const ANTHARAS = 29068
const CLEAR_CRYSTAL = 21905
const FILLED_CRYSTAL_ANTHARAS_ENERGY = 21907
const JEWEL_OF_ANTHARAS = 21898
const PORTAL_STONE = 3865
const minimumLevel = 84

export class JewelOfAntharas extends ListenerLogic {
    constructor() {
        super( 'Q10504_JewelOfAntharas', 'listeners/tracked-10500/JewelOfAntharas.ts' )
        this.questId = 10504
        this.questItemIds = [ CLEAR_CRYSTAL, FILLED_CRYSTAL_ANTHARAS_ENERGY ]
    }

    getQuestStartIds(): Array<number> {
        return [ THEODRIC ]
    }

    getTalkIds(): Array<number> {
        return [ THEODRIC ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ANTHARAS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10504_JewelOfAntharas'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        if ( player.getLevel() < minimumLevel || !QuestHelper.hasQuestItem( player, PORTAL_STONE ) ) {
            return
        }

        switch ( data.eventName ) {
            case '30755-05.htm':
            case '30755-06.htm':
                break

            case '30755-07.html':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, CLEAR_CRYSTAL, 1 )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForCommandChannel( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance ): Promise<void> => {
            if ( !state.isCondition( 1 ) ) {
                return
            }

            await QuestHelper.takeSingleItem( player, CLEAR_CRYSTAL, -1 )
            await QuestHelper.giveSingleItem( player, FILLED_CRYSTAL_ANTHARAS_ENERGY, 1 )

            state.setConditionWithSound( 2, true )
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30755-02.html' )
                }

                if ( !QuestHelper.hasQuestItems( player, PORTAL_STONE ) ) {
                    return this.getPath( '30755-04.html' )
                }

                return this.getPath( '30755-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.hasQuestItems( player, CLEAR_CRYSTAL ) ) {
                            return this.getPath( '30755-08.html' )
                        }

                        await QuestHelper.giveSingleItem( player, CLEAR_CRYSTAL, 1 )
                        return this.getPath( '30755-09.html' )

                    case 2:
                        await QuestHelper.giveSingleItem( player, JEWEL_OF_ANTHARAS, 1 )
                        await state.exitQuest( false, true )

                        return this.getPath( '30755-10.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '30755-03.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}