import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { DatabaseManager } from '../../database/manager'
import { ConfigManager } from '../../config/ConfigManager'
import parser from 'cron-parser'
import { DataManager } from '../../data/manager'
import _ from 'lodash'
import schedule, { Job } from 'node-schedule'
import { Skill } from '../../gameService/models/Skill'

const itemIds : Array<number> = [
    10632, // Wondrous Cubic
    21106 // Wondrous Cubic - 1 time use
]

/*
    Listener overrides reuse data for Wondrous Cubic items based on reset cron.
 */
export class WondrousCubicReuse extends ListenerLogic {
    task : Job // cron task to clean up skill reuse times
    skillIds : Array<number>

    constructor() {
        super( 'WondrousCubicReuse', 'listeners/tasks/WondrousCubicReuse.ts' )
        ConfigManager.tuning.subscribe( this.initialize.bind( this ) )
    }

    async runCleanUp() : Promise<void> {
        return DatabaseManager.getCharacterSkillsSave().deleteReuseBySkillIds( this.skillIds )
    }

    getReuseTime() : number {
        let cron = parser.parseExpression( ConfigManager.tuning.getWondrousCubicResetCron() )
        return cron.next().toDate().valueOf() - cron.prev().toDate().valueOf()
    }

    async initialize(): Promise<void> {
        let reuseTime = this.getReuseTime()
        this.skillIds = _.flatten( _.compact( itemIds.map( ( itemId : number ) => {
            let item = DataManager.getItems().getTemplate( itemId )
            if ( !item ) {
                return
            }

            item.reuseDelay = reuseTime
            return item.getSkills().map( ( skill : Skill ) => {
                return skill.getId()
            } )
        } ) ) )

        if ( this.task ) {
            this.task.cancel()
        }

        this.task = schedule.scheduleJob( ConfigManager.tuning.getWondrousCubicResetCron(), this.runCleanUp.bind( this ) )
    }
}