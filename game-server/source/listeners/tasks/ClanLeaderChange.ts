import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType } from '../../gameService/models/events/EventType'
import { ClanCache } from '../../gameService/cache/ClanCache'
import { L2Clan } from '../../gameService/models/L2Clan'
import { L2ClanMember } from '../../gameService/models/L2ClanMember'
import Timeout = NodeJS.Timeout
import { ConfigManager } from '../../config/ConfigManager'
import aigle from 'aigle'
import moment from 'moment'

export class ClanLeaderChange extends ListenerLogic {
    task: Timeout

    constructor() {
        super( 'ClanLeaderChange', 'listeners/tasks/ClanLeaderChange.ts' )
        this.scheduleTask()
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerClanLeaderChangeScheduled,
                method: this.onClanLeaderScheduled.bind( this ),
                ids: null,
            },
        ]
    }

    async onClanLeaderScheduled(): Promise<void> {
        if ( this.task ) {
            return
        }

        this.scheduleTask()
    }

    scheduleTask() {
        this.task = setTimeout( this.runCleanLeaderChange.bind( this ), this.getTimeToNextChange() )
    }

    async runCleanLeaderChange(): Promise<void> {
        await aigle.resolve( ClanCache.getClans() ).eachSeries( async ( clan: L2Clan ) => {
            if ( clan.getNewLeaderId() === 0 ) {
                return
            }

            let member: L2ClanMember = clan.getClanMember( clan.getNewLeaderId() )
            if ( member ) {
                await clan.setNewLeader( member )
                return
            }

            clan.resetNewLeaderId()
        } )

        this.task = null
    }

    getTimeToNextChange() : number {
        let date = moment()

        if ( ConfigManager.character.getClanLeaderWeekdayChange() !== 0 ) {
            let weekDay : number = ConfigManager.character.getClanLeaderWeekdayChange()
            if ( date.day() !== ( weekDay - 1 ) ) {
                date.day( weekDay )
            }
        }

        date.hour( ConfigManager.character.getClanLeaderHourChange() )

        let currentTime = Date.now()
        if ( date.valueOf() < currentTime ) {
            date.add( 1, 'day' )
        }

        return date.valueOf() - currentTime
    }
}