import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventTerminationResult, EventType, NpcBypassEvent } from '../../gameService/models/events/EventType'
import { DataManager } from '../../data/manager'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { InventoryAction } from '../../gameService/enums/InventoryAction'
import { L2NpcTemplate } from '../../gameService/models/actor/templates/L2NpcTemplate'
import { L2MonsterInstance } from '../../gameService/models/actor/instance/L2MonsterInstance'
import { L2World } from '../../gameService/L2World'
import { L2FortLogisticsInstance } from '../../gameService/models/actor/instance/L2FortLogisticsInstance'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { TerminatedResultHelper } from '../helpers/TerminatedResultHelper'

const npcIds: Array<number> = [
    35664,
    35696,
    35733,
    35765,
    35802,
    35833,
    35865,
    35902,
    35934,
    35972,
    36009,
    36041,
    36079,
    36116,
    36147,
    36179,
    36217,
    36255,
    36292,
    36324,
    36362,
]

const supplyBoxIds: Array<number> = [
    35665,
    35697,
    35734,
    35766,
    35803,
    35834,
    35866,
    35903,
    35935,
    35973,
    36010,
    36042,
    36080,
    36117,
    36148,
    36180,
    36218,
    36256,
    36293,
    36325,
    36363,
]

const defaultResult: EventTerminationResult = TerminatedResultHelper.defaultSuccess

export class FortLogisticsBypass extends ListenerLogic {
    constructor() {
        super( 'FortLogisticsBypass', 'listeners/bypass/FortLogistics.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcBypass,
                method: this.onNpcBypass.bind( this ),
                ids: npcIds,
            },
        ]
    }

    async onNpcBypass( data: NpcBypassEvent ): Promise<EventTerminationResult> {
        let commandChunks: Array<string> = data.command.split( ' ' )
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.originatorId ) as L2FortLogisticsInstance

        switch ( commandChunks[ 0 ].toLowerCase() ) {
            case 'rewards':
                this.onRewards( player, npc )
                return defaultResult

            case 'blood':
                await this.onBlood( player, npc )
                return defaultResult

            case 'supplylvl':
                this.onSupplyLevel( player, npc )
                return defaultResult

            case 'supply':
                await this.onFortSupply( player, npc )
        }

        return TerminatedResultHelper.defaultFailure
    }

    sendHtmlMessage( player: L2PcInstance, htmlPath: string, npcObjectId: number ): void {
        let html: string = DataManager.getHtmlData().getItem( htmlPath )
        player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), npcObjectId ) )
    }

    onRewards( player: L2PcInstance, npc: L2FortLogisticsInstance ): void {
        if ( npc.isLord( player ) ) {
            let path = 'data/html/fortress/logistics-rewards.htm'
            let html: string = DataManager.getHtmlData().getItem( path )
                                          .replace( /%bloodoath%/g, player.getClan().getBloodOathCount().toString() )

            return player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
        }

        return this.sendHtmlMessage( player, 'data/html/fortress/logistics-noprivs.htm', npc.getObjectId() )
    }

    async onBlood( player: L2PcInstance, npc: L2FortLogisticsInstance ): Promise<void> {
        if ( npc.isLord( player ) ) {
            let amount = player.getClan().getBloodOathCount()
            if ( amount > 0 ) {
                await player.addItem( 9910, amount, -1, npc.getObjectId(), InventoryAction.Other )
                await player.getClan().resetBloodOathCount()

                return this.sendHtmlMessage( player, 'data/html/fortress/logistics-blood.htm', npc.getObjectId() )
            }

            return this.sendHtmlMessage( player, 'data/html/fortress/logistics-noblood.htm', npc.getObjectId() )
        }

        return this.sendHtmlMessage( player, 'data/html/fortress/logistics-noprivs.htm', npc.getObjectId() )
    }

    onSupplyLevel( player: L2PcInstance, npc: L2FortLogisticsInstance ): void {
        let fort = npc.getFort()
        if ( fort.getFortState() === 2 ) {
            if ( player.isClanLeader() ) {
                let path = 'data/html/fortress/logistics-supplylvl.htm'
                let html: string = DataManager.getHtmlData().getItem( path )
                                              .replace( /%supplylvl%/g, fort.getSupplyLevel().toString() )

                return player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
            }

            return this.sendHtmlMessage( player, 'data/html/fortress/logistics-noprivs.htm', npc.getObjectId() )
        }

        return this.sendHtmlMessage( player, 'data/html/fortress/logistics-1.htm', npc.getObjectId() )
    }

    async onFortSupply( player: L2PcInstance, npc: L2FortLogisticsInstance ): Promise<void> {
        if ( npc.isLord( player ) ) {
            let fort = npc.getFort()
            if ( fort.getSiege().isInProgress() ) {
                return this.sendHtmlMessage( player, 'data/html/fortress/logistics-siege.htm', npc.getObjectId() )
            }

            let level = fort.getSupplyLevel()
            if ( level > 0 ) {
                let boxTemplate: L2NpcTemplate = DataManager.getNpcData().getTemplate( supplyBoxIds[ level - 1 ] )
                let box: L2MonsterInstance = new L2MonsterInstance( boxTemplate )

                box.setCurrentHp( box.getMaxHp() )
                box.setCurrentMp( box.getMaxMp() )
                box.setHeading( 0 )
                box.spawnMe( npc.getX() - 23, npc.getY() + 41, npc.getZ() )

                fort.setSupplyLevel( 0 )
                fort.saveFortVariables()

                return this.sendHtmlMessage( player, 'data/html/fortress/logistics-supply.htm', npc.getObjectId() )
            }

            return this.sendHtmlMessage( player, 'data/html/fortress/logistics-nosupply.htm', npc.getObjectId() )
        }

        return this.sendHtmlMessage( player, 'data/html/fortress/logistics-noprivs.htm', npc.getObjectId() )
    }
}