import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventTerminationResult, EventType, NpcBypassEvent } from '../../gameService/models/events/EventType'
import { L2ClanHallManagerInstance } from '../../gameService/models/actor/instance/L2ClanHallManagerInstance'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { DataManager } from '../../data/manager'
import moment from 'moment'
import { ClanHall } from '../../gameService/models/entity/ClanHall'
import { ConfigManager } from '../../config/ConfigManager'
import _ from 'lodash'
import { ClanHallManager } from '../../gameService/instancemanager/ClanHallManager'
import { AgitDecoInfo } from '../../gameService/packets/send/AgitDecoInfo'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { L2EffectType } from '../../gameService/enums/effects/L2EffectType'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { ActionFailed } from '../../gameService/packets/send/ActionFailed'
import { TeleportLocationManager } from '../../gameService/cache/TeleportLocationManager'
import { TerminatedResultHelper } from '../helpers/TerminatedResultHelper'
import { MsInDays } from '../../gameService/enums/MsFromTime'
import { ClanPrivilege } from '../../gameService/enums/ClanPriviledge'
import { ClanHallFunctionType } from '../../gameService/enums/clanhall/ClanHallFunctionType'

const npcIds: Array<number> = [
    35383,
    35384,
    35386,
    35388,
    35390,
    35392,
    35394,
    35396,
    35398,
    35400,
    35403,
    35405,
    35407,
    35421,
    35438,
    35439,
    35441,
    35443,
    35445,
    35447,
    35449,
    35451,
    35453,
    35455,
    35457,
    35459,
    35461,
    35463,
    35465,
    35467,
    35566,
    35568,
    35570,
    35572,
    35574,
    35576,
    35578,
    35580,
    35582,
    35584,
    35586,
    35605,
    35640,
]

const enum EventNames {
    Remove = 'banish_foreigner',
    ManageVault = 'manage_vault',
    ManageDoors = 'door',
    UseFunctions = 'functions',
    ManageFunctions = 'manage',
    UseBuffs = 'support',
    ShowMenu = 'list_back',
    ShowBuffMenu = 'support_back',
    UseTeleport = 'goto'
}

const successMessage : EventTerminationResult = TerminatedResultHelper.defaultSuccess

// TODO : convert into config to implement custom clan hall functions
/*
    Custom functions:
    - CP recovery
    - spoil chance increase, multiplicative, max 25%, need to be priced accordingly
    - reduced crafting MP, multiplicative, max 25%
    - additional quest experience, multiplicative, max 25%
 */

export class ClanHallManagerBypass extends ListenerLogic {
    constructor() {
        super( 'ClanHallManagerBypass', 'listeners/bypass/ClanHallManager.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcBypass,
                method: this.onNpcBypass.bind( this ),
                ids: npcIds,
            },
        ]
    }

    async onNpcBypass( data: NpcBypassEvent ): Promise<EventTerminationResult> {
        let commandChunks = data.command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case EventNames.Remove:
                await this.onRemove( data, commandChunks )
                return successMessage

            case EventNames.ManageVault:
                this.onManageVault( data )
                return successMessage

            case EventNames.ManageDoors:
                this.onManageDoors( data, commandChunks )
                return successMessage

            case EventNames.UseFunctions:
                this.onUseFunctions( data, commandChunks )
                return successMessage

            case EventNames.ManageFunctions:
                await this.onManageFunctions( data, commandChunks )
                return successMessage

            case EventNames.UseBuffs:
                await this.onUseBuffs( data, commandChunks )
                return successMessage

            case EventNames.ShowMenu:
                this.onShowMenu( data )
                return successMessage

            case EventNames.ShowBuffMenu:
                this.onShowBuffMenu( data )
                return successMessage

            case EventNames.UseTeleport:
                await this.onUseTeleport( data, commandChunks )
                return successMessage
        }

        return
    }

    getPathPrefix(): string {
        return 'data/html/clanHallManager'
    }

    sendHtml( player: L2PcInstance, name: string, npcObjectId: number ): void {
        let path = this.getPath( name )
        player.sendOwnedData( NpcHtmlMessagePath( DataManager.getHtmlData().getItem( path ), path, player.getObjectId(), npcObjectId, 0 ) )
    }

    sendFormattedHtml( player: L2PcInstance, name: string, npcObjectId: number, formatMethod: ( value: string ) => string ): void {
        let path = this.getPath( name )
        player.sendOwnedData( NpcHtmlMessagePath( formatMethod( DataManager.getHtmlData().getItem( path ) ), path, player.getObjectId(), npcObjectId, 0 ) )
    }

    getClanHall( objectId: number ): ClanHall {
        let npc = L2World.getObjectById( objectId ) as L2ClanHallManagerInstance
        return npc.getClanHall()
    }

    async onRemove( data: NpcBypassEvent, commandChunks: Array<string> ): Promise<void> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.hasClanPrivilege( ClanPrivilege.ClanHallDismiss ) ) {
            switch ( commandChunks[ 1 ] ) {
                case 'list':
                    this.sendHtml( player, 'banish-list.htm', data.originatorId )
                    break

                case 'banish':
                    let npc = L2World.getObjectById( data.originatorId ) as L2ClanHallManagerInstance
                    await npc.getClanHall().banishForeigners()

                    this.sendHtml( player, 'banish.htm', data.originatorId )
                    break
            }

            return
        }

        this.sendHtml( player, 'not_authorized.htm', data.originatorId )
    }

    onManageVault( data: NpcBypassEvent ): void {
        let player = L2World.getPlayer( data.playerId )

        if ( player.hasClanPrivilege( ClanPrivilege.WarehouseAccess ) ) {
            let clanHall = this.getClanHall( data.originatorId )

            if ( clanHall.getLease() <= 0 ) {
                return this.sendHtml( player, 'vault-chs.htm', data.originatorId )
            }

            return this.sendFormattedHtml( player, 'vault.htm', data.originatorId, ( html: string ): string => {
                return html
                        .replace( '%rent%', clanHall.getLease().toString() )
                        .replace( '%date%', moment( clanHall.getExpirationTime() ).format() )
            } )
        }

        this.sendHtml( player, 'not_authorized.htm', data.originatorId )
    }

    onManageDoors( data: NpcBypassEvent, commandChunks: Array<string> ): void {
        let player = L2World.getPlayer( data.playerId )
        if ( player.hasClanPrivilege( ClanPrivilege.ClanHallOpenDoors ) ) {
            let [ , action ] = commandChunks
            switch ( action ) {
                case 'open':
                    this.getClanHall( data.originatorId ).openCloseDoors( true )
                    return this.sendHtml( player, 'door-open.htm', data.originatorId )

                case 'close':
                    this.getClanHall( data.originatorId ).openCloseDoors( false )
                    return this.sendHtml( player, 'door-close.htm', data.originatorId )
            }

            return this.sendHtml( player, 'door.htm', data.originatorId )
        }

        this.sendHtml( player, 'not_authorized.htm', data.originatorId )
    }

    onUseFunctions( data: NpcBypassEvent, commandChunks: Array<string> ): void {
        let [ , action, value ] = commandChunks
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.originatorId ) as L2ClanHallManagerInstance
        let clanHall = npc.getClanHall()

        switch ( action ) {
            case 'tele':
                let teleportFunction = clanHall.getFunction( ClanHallFunctionType.Teleport )
                if ( !teleportFunction ) {
                    return this.sendHtml( player, 'chamberlain-nac.htm', data.originatorId )
                }

                return this.sendHtml( player, `tele${ clanHall.getLocation() }${ teleportFunction.level }.htm`, data.originatorId )

            case 'item_creation':
                let createFunction = clanHall.getFunction( ClanHallFunctionType.CreateItem )
                if ( !createFunction ) {
                    return this.sendHtml( player, 'chamberlain-nac.htm', data.originatorId )
                }

                if ( !value ) {
                    return
                }

                let listId: number = parseInt( value, 10 ) + ( createFunction.level * 100000 )
                return npc.showBuyWindow( player, listId )

            case 'support':
                let buffFunction = clanHall.getFunction( ClanHallFunctionType.SupportBuffs )
                if ( !buffFunction ) {
                    return this.sendHtml( player, 'chamberlain-nac.htm', data.originatorId )
                }

                return this.sendFormattedHtml( player, `support${ buffFunction.level }.htm`, data.originatorId, ( value: string ): string => {
                    return value.replace( '%mp%', Math.floor( npc.getCurrentMp() ).toString() )
                } )

            case 'back':
                return npc.showChatWindowDefault( player )
        }

        return this.sendFormattedHtml( player, 'functions.htm', data.originatorId, ( html: string ): string => {
            return html
                    .replace( '%xp_regen%', clanHall.getFunction( ClanHallFunctionType.RestoreXp ) ? clanHall.getFunction( ClanHallFunctionType.RestoreXp ).level.toString() : '0' )
                    .replace( '%hp_regen%', clanHall.getFunction( ClanHallFunctionType.RestoreHp ) ? clanHall.getFunction( ClanHallFunctionType.RestoreHp ).level.toString() : '0' )
                    .replace( '%mp_regen%', clanHall.getFunction( ClanHallFunctionType.RestoreMp ) ? clanHall.getFunction( ClanHallFunctionType.RestoreMp ).level.toString() : '0' )
        } )
    }

    async onManageFunctions( data: NpcBypassEvent, commandChunks: Array<string> ): Promise<void> {
        let player = L2World.getPlayer( data.playerId )

        if ( !player.hasClanPrivilege( ClanPrivilege.ClanHallSetFunctions ) ) {
            return this.sendHtml( player, 'not_authorized.htm', data.originatorId )
        }

        let [ , action ] = commandChunks
        switch ( action ) {
            case 'recovery':
                if ( commandChunks.length > 2 ) {
                    return this.onManageRecoveryParameters( data, commandChunks, player )
                }

                return this.onManageRecovery( data, player )

            case 'other':
                if ( commandChunks.length > 2 ) {
                    return this.onManageOtherParameters( data, commandChunks, player )
                }

                return this.onManageOther( data, player )

            case 'deco':
                if ( commandChunks.length > 2 && !this.getClanHall( data.originatorId ).isSiegableHall() ) {
                    return this.onManageDecorationParameters( data, commandChunks, player )
                }

                return this.onManageDecoration( data, player )

            case 'back':
                return ( L2World.getObjectById( data.originatorId ) as L2Npc ).showChatWindowDefault( player )

            default:
                return this.sendHtml( player, this.getClanHall( data.originatorId ).isSiegableHall() ? 'manage_siegable.htm' : 'manage.htm', data.originatorId )
        }
    }

    async onManageRecoveryParameters( data: NpcBypassEvent, commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
        let clanHall = this.getClanHall( data.originatorId )
        if ( !clanHall.getOwnerId() ) {
            return player.sendMessage( 'This clan hall has no owner, you cannot change the configuration.' )
        }

        let action = _.toLower( commandChunks[ 3 ] )
        switch ( action ) {
            case 'hp_cancel':
            case 'mp_cancel':
            case 'exp_cancel':
                return this.sendFormattedHtml( player, 'functions-cancel.htm', data.originatorId, ( html: string ): string => {
                    return html.replace( '%apply%', `recovery ${ action.split( '_' )[ 0 ] } 0` )
                } )

            case 'edit_hp':
                return this.sendFormattedHtml( player, 'functions-apply.htm', data.originatorId, ( html: string ): string => {
                    let percent = _.parseInt( commandChunks[ 4 ] )
                    return html
                            .replace( '%name%', 'Fireplace (HP Recovery Device)' )
                            .replace( '%cost%', `${ getHpRegenerationCost( percent ) }</font>Adena / ${ Math.floor( ConfigManager.clanhall.getHpRegenerationFunctionFeeRatio() / MsInDays.One ) } Day</font>)` )
                            .replace( '%use%', `Provides additional HP recovery for clan members in the clan hall.<font color='00FFFF'>${ percent }%</font>` )
                            .replace( '%apply%', `recovery hp ${ percent }` )
                } )

            case 'edit_mp':
                return this.sendFormattedHtml( player, 'functions-apply.htm', data.originatorId, ( html: string ): string => {
                    let percent = _.parseInt( commandChunks[ 4 ] )
                    return html
                            .replace( '%name%', 'Carpet (MP Recovery)' )
                            .replace( '%cost%', `${ getMpRegenerationCost( percent ) }</font>Adena / ${ Math.floor( ConfigManager.clanhall.getMpRegenerationFunctionFeeRatio() / MsInDays.One ) } Day</font>)` )
                            .replace( '%use%', `Provides additional MP recovery for clan members in the clan hall.<font color='00FFFF'>${ percent }%</font>` )
                            .replace( '%apply%', `recovery mp ${ percent }` )
                } )

            case 'edit_exp':
                return this.sendFormattedHtml( player, 'functions-apply.htm', data.originatorId, ( html: string ): string => {
                    let percent = _.parseInt( commandChunks[ 4 ] )
                    return html
                            .replace( '%name%', 'Chandelier (EXP Recovery Device)' )
                            .replace( '%cost%', `${ getExpRegenerationCost( percent ) }</font>Adena / ${ Math.floor( ConfigManager.clanhall.getMpRegenerationFunctionFeeRatio() / MsInDays.One ) } Day</font>)` )
                            .replace( '%use%', `Restores the Exp of any clan member who is resurrected in the clan hall.<font color='00FFFF'>${ percent }%</font>` )
                            .replace( '%apply%', `recovery exp ${ percent }` )
                } )

            case 'hp':
                return this.sendFunctionApplication( clanHall, player, ClanHallFunctionType.RestoreHp, commandChunks[ 4 ], getHpRegenerationCost, ConfigManager.clanhall.getHpRegenerationFunctionFeeRatio(), data.originatorId )

            case 'mp':
                return this.sendFunctionApplication( clanHall, player, ClanHallFunctionType.RestoreMp, commandChunks[ 4 ], getMpRegenerationCost, ConfigManager.clanhall.getMpRegenerationFunctionFeeRatio(), data.originatorId )

            case 'exp':
                return this.sendFunctionApplication( clanHall, player, ClanHallFunctionType.RestoreXp, commandChunks[ 4 ], getExpRegenerationCost, ConfigManager.clanhall.getExpRegenerationFunctionFeeRatio(), data.originatorId )
        }
    }

    onManageRecovery( data: NpcBypassEvent, player: L2PcInstance ): void {
        let path = this.getPath( 'edit_recovery.htm' )
        let html = DataManager.getHtmlData().getItem( path )
        let clanHall = this.getClanHall( data.originatorId )

        html = applyRecoveryFunction( html, clanHall, ClanHallFunctionType.RestoreHp, 'hp', Math.floor( ConfigManager.clanhall.getHpRegenerationFunctionFeeRatio() / MsInDays.One ), ( value: number ): Array<number> => {
            switch ( value ) {
                case 0:
                    return [ 20, 40, 120 ]

                case 1:
                    return [ 40, 100, 160 ]

                case 2:
                    return [ 80, 140, 200, 260 ]

                case 3:
                    return [ 80, 120, 180, 240, 300 ]
            }
        }, createRecoveryBypass, 'recovery' )

        html = applyRecoveryFunction( html, clanHall, ClanHallFunctionType.RestoreMp, 'mp', Math.floor( ConfigManager.clanhall.getMpRegenerationFunctionFeeRatio() / MsInDays.One ), ( value: number ): Array<number> => {
            switch ( value ) {
                case 0:
                    return [ 5, 10, 15, 20 ]

                case 1:
                    return [ 10, 15, 20, 25 ]

                case 2:
                    return [ 15, 20, 25, 30 ]

                case 3:
                    return [ 20, 25, 30, 40 ]
            }
        }, createRecoveryBypass, 'recovery' )

        html = applyRecoveryFunction( html, clanHall, ClanHallFunctionType.RestoreXp, 'exp', Math.floor( ConfigManager.clanhall.getExpRegenerationFunctionFeeRatio() / MsInDays.One ), ( value: number ): Array<number> => {
            switch ( value ) {
                case 0:
                    return [ 5, 10, 15, 25 ]

                case 1:
                    return [ 10, 15, 20, 30 ]

                case 2:
                    return [ 15, 20, 30, 40 ]

                case 3:
                    return [ 20, 30, 40, 50 ]
            }
        }, createRecoveryBypass, 'recovery' )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), data.originatorId, 0 ) )
    }

    async onManageOtherParameters( data: NpcBypassEvent, commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
        let clanHall = this.getClanHall( data.originatorId )
        if ( clanHall.getOwnerId() === 0 ) {
            return player.sendMessage( 'This clan hall has no owner, you cannot change the configuration.' )
        }

        let action = _.toLower( commandChunks[ 3 ] )
        switch ( action ) {
            case 'item_cancel':
            case 'tele_cancel':
            case 'support_cancel':
                return this.sendFormattedHtml( player, 'functions-cancel.htm', data.originatorId, ( html: string ): string => {
                    return html.replace( '%apply%', `other ${ action.split( '_' )[ 0 ] } 0` )
                } )

            case 'edit_item':
                return this.sendFormattedHtml( player, 'functions-apply.htm', data.originatorId, ( html: string ): string => {
                    let value = _.parseInt( commandChunks[ 4 ] )
                    return html
                            .replace( '%name%', 'Magic Equipment (Item Production Facilities)' )
                            .replace( '%cost%', `${ getItemFunctionFee( value ) }</font>Adena / ${ Math.floor( ConfigManager.clanhall.getItemCreationFunctionFeeRatio() / MsInDays.One ) } Day</font>)` )
                            .replace( '%use%', 'Allow the purchase of special items at fixed intervals.' )
                            .replace( '%apply%', `other item ${ commandChunks[ 4 ] }` )
                } )

            case 'edit_support':
                return this.sendFormattedHtml( player, 'functions-apply.htm', data.originatorId, ( html: string ): string => {
                    let value = _.parseInt( commandChunks[ 4 ] )
                    return html
                            .replace( '%name%', 'Magic Equipment (Item Production Facilities)' )
                            .replace( '%cost%', `${ getSupportFunctionFee( value ) }</font>Adena / ${ Math.floor( ConfigManager.clanhall.getSupportFunctionFeeRatio() / MsInDays.One ) } Day</font>)` )
                            .replace( '%use%', 'Enables the use of supplementary magic.' )
                            .replace( '%apply%', `other support ${ commandChunks[ 4 ] }` )
                } )

            case 'edit_tele':
                return this.sendFormattedHtml( player, 'functions-apply.htm', data.originatorId, ( html: string ): string => {
                    let value = _.parseInt( commandChunks[ 4 ] )
                    return html
                            .replace( '%name%', 'Mirror (Teleportation Device)' )
                            .replace( '%cost%', `${ getTeleportFunctionFee( value ) }</font>Adena / ${ Math.floor( ConfigManager.clanhall.getTeleportFunctionFeeRatio() / MsInDays.One ) } Day</font>)` )
                            .replace( '%use%', `Teleports clan members in a clan hall to the target <font color="00FFFF">Stage ${ value }</font> staging area` )
                            .replace( '%apply%', `other tele ${ commandChunks[ 4 ] }` )
                } )

            case 'item':
                return this.sendFunctionApplication( clanHall, player, ClanHallFunctionType.CreateItem, commandChunks[ 4 ], getItemFunctionFee, ConfigManager.clanhall.getItemCreationFunctionFeeRatio(), data.originatorId )

            case 'tele':
                return this.sendFunctionApplication( clanHall, player, ClanHallFunctionType.Teleport, commandChunks[ 4 ], getTeleportFunctionFee, ConfigManager.clanhall.getTeleportFunctionFeeRatio(), data.originatorId )

            case 'support':
                return this.sendFunctionApplication( clanHall, player, ClanHallFunctionType.SupportBuffs, commandChunks[ 4 ], getSupportFunctionFee, ConfigManager.clanhall.getSupportFunctionFeeRatio(), data.originatorId )

        }
    }

    onManageOther( data: NpcBypassEvent, player: L2PcInstance ): void {
        let path = this.getPath( 'edit_other.htm' )
        let html = DataManager.getHtmlData().getItem( path )
        let clanHall = this.getClanHall( data.originatorId )

        html = applyRecoveryFunction( html, clanHall, ClanHallFunctionType.Teleport, 'tele', Math.floor( ConfigManager.clanhall.getTeleportFunctionFeeRatio() / MsInDays.One ), ( value: number ): Array<number> => {
            return [ 1, 2 ]
        }, createOtherBypass, 'other' )

        html = applyRecoveryFunction( html, clanHall, ClanHallFunctionType.SupportBuffs, 'support', Math.floor( ConfigManager.clanhall.getSupportFunctionFeeRatio() / MsInDays.One ), ( value: number ): Array<number> => {
            switch ( value ) {
                case 0:
                    return [ 1, 2 ]

                case 1:
                    return [ 2, 3, 4 ]

                case 2:
                    return [ 3, 4, 5 ]

                case 3:
                    return [ 4, 5, 7, 8 ]
            }
        }, createOtherBypass, 'other' )

        html = applyRecoveryFunction( html, clanHall, ClanHallFunctionType.CreateItem, 'item', Math.floor( ConfigManager.clanhall.getItemCreationFunctionFeeRatio() / MsInDays.One ), ( value: number ): Array<number> => {
            return [ 1, 2, 3 ]
        }, createOtherBypass, 'other' )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), data.originatorId, 0 ) )
    }

    async onManageDecorationParameters( data: NpcBypassEvent, commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
        let clanHall = this.getClanHall( data.originatorId )
        if ( !clanHall.getOwnerId() ) {
            return player.sendMessage( 'This clan hall has no owner, you cannot change the configuration.' )
        }

        let action = _.toLower( commandChunks[ 3 ] )
        switch ( action ) {
            case 'curtains_cancel':
            case 'fixtures_cancel':
                return this.sendFormattedHtml( player, 'functions-cancel.htm', data.originatorId, ( html: string ): string => {
                    return html.replace( '%apply%', `deco ${ action.split( '_' )[ 0 ] } 0` )
                } )

            case 'edit_curtains':
                return this.sendFormattedHtml( player, 'functions-apply.htm', data.originatorId, ( html: string ): string => {
                    let value = _.parseInt( commandChunks[ 4 ] )
                    return html
                            .replace( '%name%', 'Curtains (Decoration)' )
                            .replace( '%cost%', `${ getCurtainFunctionFee( value ) }</font>Adena / ${ Math.floor( ConfigManager.clanhall.getCurtainFunctionFeeRatio() / MsInDays.One ) } Day</font>)` )
                            .replace( '%use%', 'These curtains can be used to decorate the clan hall.' )
                            .replace( '%apply%', `deco curtains ${ commandChunks[ 4 ] }` )
                } )

            case 'edit_fixtures':
                return this.sendFormattedHtml( player, 'functions-apply.htm', data.originatorId, ( html: string ): string => {
                    let value = _.parseInt( commandChunks[ 4 ] )
                    return html
                            .replace( '%name%', 'Front Platform (Decoration)' )
                            .replace( '%cost%', `${ getFrontPlatformFunctionFee( value ) }</font>Adena / ${ Math.floor( ConfigManager.clanhall.getFrontPlatformFunctionFeeRatio() / MsInDays.One ) } Day</font>)` )
                            .replace( '%use%', 'Used to decorate the clan hall.' )
                            .replace( '%apply%', `deco fixtures ${ commandChunks[ 4 ] }` )
                } )

            case 'curtains':
                return this.sendFunctionApplication( clanHall, player, ClanHallFunctionType.DecorationCurtains, commandChunks[ 4 ], getCurtainFunctionFee, ConfigManager.clanhall.getCurtainFunctionFeeRatio(), data.originatorId )

            case 'fixtures':
                return this.sendFunctionApplication( clanHall, player, ClanHallFunctionType.DecorationFront, commandChunks[ 4 ], getFrontPlatformFunctionFee, ConfigManager.clanhall.getFrontPlatformFunctionFeeRatio(), data.originatorId )
        }
    }

    onManageDecoration( data: NpcBypassEvent, player: L2PcInstance ): void {
        let path = this.getPath( 'deco.htm' )
        let html = DataManager.getHtmlData().getItem( path )
        let clanHall = this.getClanHall( data.originatorId )

        html = applyRecoveryFunction( html, clanHall, ClanHallFunctionType.DecorationCurtains, 'curtains', Math.floor( ConfigManager.clanhall.getCurtainFunctionFeeRatio() / MsInDays.One ), ( value: number ): Array<number> => {
            return [ 1, 2 ]
        }, createDecorationBypass, 'deco' )

        html = applyRecoveryFunction( html, clanHall, ClanHallFunctionType.DecorationFront, 'fixtures', Math.floor( ConfigManager.clanhall.getFrontPlatformFunctionFeeRatio() / MsInDays.One ), ( value: number ): Array<number> => {
            return [ 1, 2 ]
        }, createDecorationBypass, 'deco' )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), data.originatorId, 0 ) )
    }

    async onUseBuffs( data: NpcBypassEvent, commandChunks: Array<string> ): Promise<void> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.isCursedWeaponEquipped() ) {
            return player.sendMessage( 'Cannot receive heals or buffs with cursed weapon equipped.' )
        }

        let npc = L2World.getObjectById( data.originatorId ) as L2ClanHallManagerInstance
        let clanHall = npc.getClanHall()

        let buffFunction = clanHall.getFunction( ClanHallFunctionType.SupportBuffs )
        if ( !buffFunction || !buffFunction.level ) {
            return player.sendMessage( 'This clan hall has no associated support function, you cannot receive support at this time.' )
        }

        let [ , skillIdValue, skillLevelValue ] = commandChunks
        let skillId = _.parseInt( skillIdValue )
        let skillLevel = _.defaultTo( _.parseInt( skillLevelValue ), 1 )
        let skill = SkillCache.getSkill( skillId, skillLevel )

        if ( !skillId || !skill ) {
            if ( player.isGM() ) {
                player.sendMessage( `Invalid skill level, needs to be a number, passed value = ${ skillIdValue }` )
            }

            return
        }

        npc.setTarget( player )

        if ( skill.hasEffectType( L2EffectType.Summon ) ) {
            await player.doSimultaneousCast( skill )
        } else {
            if ( !ConfigManager.clanhall.mpBuffFree() && npc.getCurrentMp() < skill.getTotalCostMp() ) {
                return this.sendFormattedHtml( player, 'support-no_mana.htm', data.originatorId, ( html: string ): string => {
                    return html.replace( '%mp%', Math.floor( npc.getCurrentMp() ).toString() )
                } )
            }

            await npc.doCast( skill )
        }

        return this.sendFormattedHtml( player, 'support-done.htm', data.originatorId, ( html: string ): string => {
            return html.replace( '%mp%', Math.floor( npc.getCurrentMp() ).toString() )
        } )
    }

    onShowMenu( data: NpcBypassEvent ): void {
        let name = `chamberlain-${ data.npcId }.htm`
        if ( !DataManager.getHtmlData().hasItem( this.getPath( name ) ) ) {
            name = 'chamberlain.htm'
        }

        return this.sendFormattedHtml( L2World.getPlayer( data.playerId ), name, data.originatorId, ( html: string ): string => {
            return html.replace( '%npcname%', DataManager.getNpcData().getTemplate( data.npcId ).getName() )
        } )
    }

    onShowBuffMenu( data: NpcBypassEvent ): void {
        let npc = L2World.getObjectById( data.originatorId ) as L2ClanHallManagerInstance
        let clanHall = npc.getClanHall()
        let player = L2World.getPlayer( data.playerId )

        let buffFunction = clanHall.getFunction( ClanHallFunctionType.SupportBuffs )
        if ( !buffFunction || !buffFunction.level ) {
            return player.sendMessage( 'This clan hall has no associated support function, you cannot receive support at this time.' )
        }

        return this.sendFormattedHtml( player, `support${ buffFunction.level }.htm`, data.originatorId, ( html: string ): string => {
            return html.replace( '%mp%', Math.floor( npc.getCurrentMp() ).toString() )
        } )
    }

    async onUseTeleport( data: NpcBypassEvent, commandChunks: Array<string> ): Promise<void> {
        let player = L2World.getPlayer( data.playerId )
        if ( player.isCombatFlagEquipped() ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_TELEPORT_WHILE_IN_POSSESSION_OF_A_WARD ) )
        }

        let [ , value ] = commandChunks
        let teleportId: number = _.parseInt( value )
        await TeleportLocationManager.teleportPlayer( player, teleportId )

        player.sendOwnedData( ActionFailed() )
    }

    async sendFunctionApplication( clanHall: ClanHall, player: L2PcInstance, functionType: ClanHallFunctionType, value: string, costGenerationMethod: ( value: number ) => number, ratio: number, originatorId: number ): Promise<void> {
        let currentFunction = clanHall.getFunction( functionType )
        let levelValue = _.parseInt( value )

        if ( currentFunction && currentFunction.level === levelValue ) {
            return this.sendFormattedHtml( player, 'functions-used.htm', originatorId, ( html: string ): string => {
                return html.replace( '%val%', `${ value }%` )
            } )
        }

        let fee = costGenerationMethod( levelValue )
        if ( !await clanHall.updateFunction( player, functionType, levelValue, fee, ratio, !currentFunction ) ) {
            return this.sendHtml( player, 'low_adena.htm', originatorId )
        }

        ClanHallManager.scheduleHallUpdate( clanHall.getId() )
        sendClanHallUpdate( player )
        return this.sendHtml( player, fee === 0 ? 'functions-cancel_confirmed.htm' : 'functions-apply_confirmed.htm', originatorId )
    }
}

function getHpRegenerationCost( percentValue: number ): number {
    switch ( percentValue ) {
        case 20:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl1()
        case 40:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl2()
        case 80:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl3()
        case 100:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl4()
        case 120:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl5()
        case 140:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl6()
        case 160:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl7()
        case 180:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl8()
        case 200:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl9()
        case 220:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl10()
        case 240:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl11()
        case 260:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl12()
        default:
            return ConfigManager.clanhall.getHpRegenerationFeeLvl13()
    }
}

function getMpRegenerationCost( percentValue: number ): number {
    switch ( percentValue ) {
        case 5:
            return ConfigManager.clanhall.getMpRegenerationFeeLvl1()
        case 10:
            return ConfigManager.clanhall.getMpRegenerationFeeLvl2()
        case 15:
            return ConfigManager.clanhall.getMpRegenerationFeeLvl3()
        case 30:
            return ConfigManager.clanhall.getMpRegenerationFeeLvl4()
        default:
            return ConfigManager.clanhall.getMpRegenerationFeeLvl5()
    }
}

function getExpRegenerationCost( percentValue: number ): number {
    switch ( percentValue ) {
        case 5:
            return ConfigManager.clanhall.getExpRegenerationFeeLvl1()
        case 10:
            return ConfigManager.clanhall.getExpRegenerationFeeLvl2()
        case 15:
            return ConfigManager.clanhall.getExpRegenerationFeeLvl3()
        case 25:
            return ConfigManager.clanhall.getExpRegenerationFeeLvl4()
        case 35:
            return ConfigManager.clanhall.getExpRegenerationFeeLvl5()
        case 40:
            return ConfigManager.clanhall.getExpRegenerationFeeLvl6()
        default:
            return ConfigManager.clanhall.getExpRegenerationFeeLvl7()
    }
}

function sendClanHallUpdate( player: L2PcInstance ): void {
    let hall = ClanHallManager.getClanHallByOwner( player.getClan() )
    if ( !hall ) {
        return
    }

    player.sendOwnedData( AgitDecoInfo( hall ) )
}

function createRecoveryBypass( property: string, value: number ): string {
    return `[<a action="bypass -h manage recovery edit_${ property } ${ value }">${ value }%</a>]`
}

function createOtherBypass( property: string, value: number ): string {
    return `[<a action="bypass -h manage other edit_${ property } ${ value }">Level ${ value }</a>]`
}

function createDecorationBypass( property: string, value: number ): string {
    return `[<a action="bypass -h manage deco edit_${ property } ${ value }">Level ${ value }</a>]`
}

function applyRecoveryFunction( html: string, clanHall: ClanHall, type: ClanHallFunctionType, property: string, interval: number, gradeValuesMethod: ( value: number ) => Array<number>, bypassMethod: ( property: string, value: number ) => string, bypassType: string ) {
    let currentFunction = clanHall.getFunction( type )
    let gradeHtml: string = gradeValuesMethod( clanHall.getGrade() ).map( bypassMethod.bind( null, property ) ).join( '' )

    if ( currentFunction ) {
        return html
                .replace( `%${ property }_recovery%`, `${ currentFunction.level }%</font> (<font color="FFAABB">${ currentFunction.fee }</font>Adena / ${ interval } Day)` )
                .replace( `%${ property }_period%`, `Withdraw the fee for the next time at ${ moment( currentFunction.endDate ).format() }` )
                .replace( `%change_${ property }%`, `[<a action="bypass -h manage ${ bypassType } ${ property }_cancel">Deactivate</a>]${ gradeHtml }` )
    }

    return html.replace( `%${ property }_recovery%`, 'none' )
               .replace( `%${ property }_period%`, 'none' )
               .replace( `%change_${ property }%`, gradeHtml )
}

function getItemFunctionFee( value: number ): number {
    switch ( value ) {
        case 1:
            return ConfigManager.clanhall.getItemCreationFunctionFeeLvl1()
        case 2:
            return ConfigManager.clanhall.getItemCreationFunctionFeeLvl2()
        default:
            return ConfigManager.clanhall.getItemCreationFunctionFeeLvl3()
    }
}

function getSupportFunctionFee( value: number ): number {
    switch ( value ) {
        case 1:
            return ConfigManager.clanhall.getSupportFeeLvl1()
        case 2:
            return ConfigManager.clanhall.getSupportFeeLvl2()
        case 3:
            return ConfigManager.clanhall.getSupportFeeLvl3()
        case 4:
            return ConfigManager.clanhall.getSupportFeeLvl4()
        case 5:
            return ConfigManager.clanhall.getSupportFeeLvl5()
        case 6:
            return ConfigManager.clanhall.getSupportFeeLvl6()
        case 7:
            return ConfigManager.clanhall.getSupportFeeLvl7()
        default:
            return ConfigManager.clanhall.getSupportFeeLvl8()
    }
}

function getTeleportFunctionFee( value: number ): number {
    switch ( value ) {
        case 1:
            return ConfigManager.clanhall.getTeleportFunctionFeeLvl1()

        default:
            return ConfigManager.clanhall.getTeleportFunctionFeeLvl2()
    }
}

function getCurtainFunctionFee( value: number ): number {
    switch ( value ) {
        case 1:
            return ConfigManager.clanhall.getCurtainFunctionFeeLvl1()

        default:
            return ConfigManager.clanhall.getCurtainFunctionFeeLvl2()
    }
}

function getFrontPlatformFunctionFee( value: number ): number {
    switch ( value ) {
        case 1:
            return ConfigManager.clanhall.getFrontPlatformFunctionFeeLvl1()

        default:
            return ConfigManager.clanhall.getFrontPlatformFunctionFeeLvl2()
    }
}