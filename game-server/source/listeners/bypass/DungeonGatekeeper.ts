import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventTerminationResult, EventType, NpcBypassEvent } from '../../gameService/models/events/EventType'
import { TerminatedResultHelper } from '../helpers/TerminatedResultHelper'
import { ActionFailed } from '../../gameService/packets/send/ActionFailed'
import { SevenSignsSeal, SevenSignsSide, SevenSignsValues } from '../../gameService/values/SevenSignsValues'
import { SevenSigns } from '../../gameService/directives/SevenSigns'
import { DataManager } from '../../data/manager'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { TeleportLocationManager } from '../../gameService/cache/TeleportLocationManager'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'

const npcIds: Array<number> = [
    31095,
    31096,
    31097,
    31098,
    31099,
    31100,
    31101,
    31102,
    31103,
    31104,
    31105,
    31106,
    31107,
    31108,
    31109,
    31110,
    31114,
    31115,
    31116,
    31117,
    31118,
    31119,
    31120,
    31121,
    31122,
    31123,
    31124,
    31125,
]

const defaultResult: EventTerminationResult = TerminatedResultHelper.defaultSuccess

export class DungeonGatekeeperBypass extends ListenerLogic {
    constructor() {
        super( 'DungeonGatekeeperBypass', 'listeners/bypass/DungeonGatekeeper.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcBypass,
                method: this.onNpcBypass.bind( this ),
                ids: npcIds,
            },
        ]
    }

    async onNpcBypass( data: NpcBypassEvent ): Promise<EventTerminationResult> {
        let player = L2World.getPlayer( data.playerId )
        player.sendOwnedData( ActionFailed() )

        let commandChunks: Array<string> = data.command.split( ' ' )
        let currentCommand = commandChunks[ 0 ]
        let basePath = SevenSignsValues.SEVEN_SIGNS_HTML_PATH
        let sealAvariceOwner = SevenSigns.getWinningSealSide( SevenSignsSeal.Avarice )

        let sealGnosisOwner = SevenSigns.getWinningSealSide( SevenSignsSeal.Gnosis )
        let playerCabal = SevenSigns.getPlayerSide( player.getObjectId() )
        let isSealValidationPeriod = SevenSigns.isSealValidationPeriod()
        let winner = SevenSigns.getWinnerSide()

        let teleportId = commandChunks.length > 1 ? parseInt( commandChunks[ 1 ], 10 ) : 0

        switch ( currentCommand ) {
            case 'necro':
                let canTeleportToNecropolis = this.isPermittedToNecropolis( player, isSealValidationPeriod, winner, playerCabal, sealAvariceOwner )

                if ( !canTeleportToNecropolis ) {
                    let path = `${ basePath }necro_no.htm`
                    let html: string = DataManager.getHtmlData().getItem( path )
                    player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), data.originatorId ) )
                } else {
                    player.setIsIn7sDungeon( true )
                    await this.doTeleport( player, teleportId )
                }


                return defaultResult

            case 'cata':
                let canTeleportToCatacombs = this.isPermittedToCatacombs( player, isSealValidationPeriod, winner, playerCabal, sealGnosisOwner )

                if ( !canTeleportToCatacombs ) {
                    let path = `${ basePath }cata_no.htm`
                    let html: string = DataManager.getHtmlData().getItem( path )
                    player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), data.originatorId ) )
                } else {
                    player.setIsIn7sDungeon( true )
                    await this.doTeleport( player, teleportId )
                }

                return defaultResult

            case 'exit':
                player.setIsIn7sDungeon( false )
                await this.doTeleport( player, teleportId )

                return defaultResult

            case 'goto':
                await this.doTeleport( player, teleportId )
                return defaultResult
        }

        return TerminatedResultHelper.defaultFailure
    }

    async doTeleport( player: L2PcInstance, teleportId: number ): Promise<void> {
        if ( !player.isDead() || !player.isAlikeDead() ) {
            let teleportData = TeleportLocationManager.getLocation( teleportId )
            if ( teleportData ) {
                await player.teleportToLocationCoordinates( teleportData.x, teleportData.y, teleportData.z, player.getHeading() )
            }
        }

        player.sendOwnedData( ActionFailed() )
    }

    isPermittedToCatacombs( player: L2PcInstance,
            isSealValidationPeriod: boolean,
            winner: number,
            playerCabal: number,
            sealGnosisOwner: number ): boolean {

        if ( player.isGM() ) {
            return true
        }

        if ( isSealValidationPeriod ) {
            if ( ( winner === SevenSignsSide.Dawn )
                    && ( ( playerCabal !== SevenSignsSide.Dawn ) || ( sealGnosisOwner !== SevenSignsSide.Dawn ) ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAN_BE_USED_BY_DAWN ) )
                return false
            }

            if ( ( winner === SevenSignsSide.Dusk )
                    && ( ( playerCabal !== SevenSignsSide.Dusk ) || ( sealGnosisOwner !== SevenSignsSide.Dusk ) ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAN_BE_USED_BY_DUSK ) )
                return false
            }

            if ( winner === SevenSignsSide.None && playerCabal !== SevenSignsSide.None ) {
                return true
            }
        }

        return playerCabal !== SevenSignsSide.None
    }

    isPermittedToNecropolis( player: L2PcInstance,
            isSealValidationPeriod: boolean,
            winner: number,
            playerCabal: number,
            sealAvariceOwner: number ): boolean {

        if ( player.isGM() ) {
            return true
        }

        if ( isSealValidationPeriod ) {
            if ( ( winner === SevenSignsSide.Dawn )
                    && ( ( playerCabal !== SevenSignsSide.Dawn ) || ( sealAvariceOwner !== SevenSignsSide.Dawn ) ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAN_BE_USED_BY_DAWN ) )
                return false
            }

            if ( ( winner === SevenSignsSide.Dusk )
                    && ( ( playerCabal !== SevenSignsSide.Dusk ) || ( sealAvariceOwner !== SevenSignsSide.Dusk ) ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAN_BE_USED_BY_DUSK ) )
                return false
            }

            if ( ( winner === SevenSignsSide.None ) && ( playerCabal !== SevenSignsSide.None ) ) {
                return true
            }
        }

        return playerCabal !== SevenSignsSide.None
    }
}