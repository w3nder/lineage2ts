import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventTerminationResult, EventType, NpcBypassEvent } from '../../gameService/models/events/EventType'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { L2World } from '../../gameService/L2World'
import { TerminatedResultHelper } from '../helpers/TerminatedResultHelper'
import { hallsKeyItemId, L2SepulcherNpcInstance } from '../../gameService/models/actor/instance/L2SepulcherNpcInstance'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import aigle from 'aigle'
import { FourSepulchersManager } from '../../gameService/cache/FourSepulchersManager'
import { InstanceType } from '../../gameService/enums/InstanceType'

const npcIds: Array<number> = [
    31452,
    31453,
    31454,
    31455,
    31456,
    31457,
    31458,
    31459,
    31460,
    31461,
    31462,
    31463,
    31464,
    31465,
    31466,
    31467,
    31468,
    31469,
    31470,
    31471,
    31472,
    31473,
    31474,
    31475,
    31476,
    31477,
    31478,
    31479,
    31480,
    31481,
    31482,
    31483,
    31484,
    31485,
    31486,
    31487,
    31919,
    31920,
    31921,
    31922,
    31923,
    31924,
    31925,
    31926,
    31927,
    31928,
    31929,
    31930,
    31931,
    31932,
    31933,
    31934,
    31935,
    31936,
    31937,
    31938,
    31939,
    31940,
    31941,
    31942,
    31943,
    31944,
]

const spawnRequiredIds = new Set<number>(
        [
            31929,
            31934,
            31939,
            31944,
        ],
)

export class SepulcherBypass extends ListenerLogic {
    constructor() {
        super( 'SepulcherBypass', 'listeners/bypass/Sepulcher.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcBypass,
                method: this.onNpcBypass.bind( this ),
                ids: npcIds,
            },
        ]
    }

    async onNpcBypass( data: NpcBypassEvent ): Promise<EventTerminationResult> {
        if ( data.instanceType !== InstanceType.L2SepulcherNpcInstance ) {
            return
        }

        let npc = L2World.getObjectById( data.originatorId ) as L2SepulcherNpcInstance
        let player = L2World.getPlayer( data.playerId )

        if ( data.isBusy ) {
            npc.showBusyMessage( player )
            return TerminatedResultHelper.defaultSuccess
        }

        let commandChunks: Array<string> = data.command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case 'Chat':
                npc.showChatWindow( player, commandChunks.length > 1 ? parseInt( commandChunks[ 1 ] ) : 0 )
                return TerminatedResultHelper.defaultSuccess

            case 'open_gate':
                let hallsKey: L2ItemInstance = player.getInventory().getItemByItemId( hallsKeyItemId )
                if ( !hallsKey ) {
                    npc.showHtml( player, 'Gatekeeper-no.htm' )
                } else {
                    await this.onOpenGate( player, npc )
                }

                return TerminatedResultHelper.defaultSuccess
        }

        return TerminatedResultHelper.defaultFailure
    }

    async onOpenGate( player: L2PcInstance, npc: L2SepulcherNpcInstance ): Promise<void> {
        if ( !FourSepulchersManager.isAttackTime() ) {
            return
        }

        if ( spawnRequiredIds.has( npc.getId() ) ) {
            FourSepulchersManager.spawnShadow( npc.getId() )
        }

        npc.openNextDoor()
        let party = player.getParty()
        if ( !party ) {
            return QuestHelper.takeSingleItem( player, hallsKeyItemId, -1 )
        }

        await aigle.resolve( party.getMembers() ).each( async ( memberId: number ): Promise<void> => {
            let currentPlayer = L2World.getPlayer( memberId )
            if ( currentPlayer ) {
                return QuestHelper.takeSingleItem( currentPlayer, hallsKeyItemId, -1 )
            }
        } )
    }
}