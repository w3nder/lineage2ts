import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventTerminationResult, EventType, NpcBypassEvent } from '../../gameService/models/events/EventType'
import { TerminatedResultHelper } from '../helpers/TerminatedResultHelper'
import { DataManager } from '../../data/manager'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { CharacterSummonCache } from '../../gameService/cache/CharacterSummonCache'
import { InventoryAction } from '../../gameService/enums/InventoryAction'
import { L2PetManagerInstance } from '../../gameService/models/actor/instance/L2PetManagerInstance'
import { L2World } from '../../gameService/L2World'

const npcIds: Array<number> = [
    30731,
    30827,
    30828,
    30829,
    30830,
    30831,
    30869,
    31067,
    31265,
    31309,
    31954,
    36478,
]

const defaultResult: EventTerminationResult = TerminatedResultHelper.defaultSuccess

export class PetManagerBypass extends ListenerLogic {
    constructor() {
        super( 'PetManagerBypass', 'listeners/bypass/PetManager.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcBypass,
                method: this.onNpcBypass.bind( this ),
                ids: npcIds,
            },
        ]
    }

    async onNpcBypass( data: NpcBypassEvent ): Promise<EventTerminationResult> {
        let commandChunks: Array<string> = data.command.split( ' ' )
        if ( commandChunks.length < 2 ) {
            return defaultResult
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.originatorId ) as L2PetManagerInstance

        switch ( commandChunks[ 0 ] ) {
            case 'exchange':
                await this.onExchange( player, commandChunks[ 1 ], npc )
                return defaultResult

            case 'evolve':
                await this.onEvolve( player, commandChunks[ 1 ], npc )
                return defaultResult

            case 'restore':
                await this.onRestore( player, commandChunks[ 1 ], npc )
                return defaultResult
        }

        return TerminatedResultHelper.defaultFailure
    }

    async getRestoreResult( player: L2PcInstance, value: string, npc: L2PetManagerInstance ): Promise<boolean> {
        switch ( value ) {
            case '1':
                return CharacterSummonCache.restoreAndEvolvePet( player, npc, 10307, 9882, 55 )

            case '2':
                return CharacterSummonCache.restoreAndEvolvePet( player, npc, 10611, 10426, 70 )

            case '3':
                return CharacterSummonCache.restoreAndEvolvePet( player, npc, 10308, 4422, 55 )

            case '4':
                return CharacterSummonCache.restoreAndEvolvePet( player, npc, 10309, 4423, 55 )

            case '5':
                return CharacterSummonCache.restoreAndEvolvePet( player, npc, 10310, 4424, 55 )

            default:
                return false
        }
    }

    async getUpgradeResult( player: L2PcInstance, value: string, npc: L2PetManagerInstance ): Promise<boolean> {
        switch ( value ) {
            case '1':
                return CharacterSummonCache.upgradePet( player, npc, 2375, 9882, 55 )

            case '2':
                return CharacterSummonCache.upgradePet( player, npc, 9882, 10426, 70 )

            case '3':
                return CharacterSummonCache.upgradePet( player, npc, 6648, 10311, 55 )

            case '4':
                return CharacterSummonCache.upgradePet( player, npc, 6650, 10313, 55 )

            case '5':
                return CharacterSummonCache.upgradePet( player, npc, 6649, 10312, 55 )

            default:
                return false
        }
    }

    async makeItemExchange( player: L2PcInstance, npc: L2PetManagerInstance, consumedItemId: number, gainedItemId: number ): Promise<void> {
        if ( await player.destroyItemByItemId( consumedItemId, 1, true, 'PetManager.makeItemExchange' ) ) {
            await player.addItem( gainedItemId, 1, -1, npc.getObjectId(), InventoryAction.Merchandise )

            let path = `data/html/petmanager/${ this.getId() }.htm`
            let html: string = DataManager.getHtmlData().getItem( path )
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
            return
        }

        let path = 'data/html/petmanager/exchange_no.htm'
        let html: string = DataManager.getHtmlData().getItem( path )
        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
    }

    onExchange( player: L2PcInstance, value: string, npc: L2PetManagerInstance ): Promise<void> {
        switch ( value ) {
            case '1':
                return this.makeItemExchange( player, npc, 7585, 6650 )
            case '2':
                return this.makeItemExchange( player, npc, 7583, 6648 )
            case '3':
                return this.makeItemExchange( player, npc, 7584, 6649 )
        }
    }

    async onEvolve( player: L2PcInstance, value: string, npc: L2PetManagerInstance ): Promise<void> {
        let status = await this.getUpgradeResult( player, value, npc )
        if ( !status ) {
            let path = 'data/html/petmanager/evolve_no.htm'
            let html: string = DataManager.getHtmlData().getItem( path )
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
        }
    }

    async onRestore( player: L2PcInstance, value: string, npc: L2PetManagerInstance ): Promise<void> {
        let status: boolean = await this.getRestoreResult( player, value, npc )

        if ( !status ) {
            let path = 'data/html/petmanager/restore_no.htm'
            let html: string = DataManager.getHtmlData().getItem( path )
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
        }
    }
}