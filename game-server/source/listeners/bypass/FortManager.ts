import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventTerminationResult, EventType, NpcBypassEvent } from '../../gameService/models/events/EventType'
import _ from 'lodash'
import { DataManager } from '../../data/manager'
import { ConfigManager } from '../../config/ConfigManager'
import { WarehouseListType } from '../../gameService/values/WarehouseListType'
import { getWarehouseWithdrawSortOrder, WarehouseWithdrawSort } from '../../gameService/values/SortedWareHouseWithdrawalListValues'
import { FortFunctionType } from '../../gameService/models/entity/Fort'
import moment from 'moment/moment'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { L2EffectType } from '../../gameService/enums/effects/L2EffectType'
import { TerminatedResultHelper } from '../helpers/TerminatedResultHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { ActionFailed } from '../../gameService/packets/send/ActionFailed'
import { WareHouseDepositList } from '../../gameService/packets/send/WareHouseDepositList'
import { WarehouseDepositType } from '../../gameService/values/warehouseDepositType'
import { SortedWareHouseWithdrawalList } from '../../gameService/packets/send/SortedWareHouseWithdrawalList'
import { WareHouseWithdrawalList } from '../../gameService/packets/send/WareHouseWithdrawalList'
import { TeleportLocationManager } from '../../gameService/cache/TeleportLocationManager'
import { L2World } from '../../gameService/L2World'
import { L2FortManagerInstance } from '../../gameService/models/actor/instance/L2FortManagerInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { ClanPrivilege } from '../../gameService/enums/ClanPriviledge'

const npcIds: Array<number> = [
    35658,
    35689,
    35727,
    35758,
    35796,
    35827,
    35858,
    35896,
    35927,
    35965,
    36003,
    36034,
    36072,
    36110,
    36141,
    36172,
    36210,
    36248,
    36286,
    36317,
    36355,
]

const dateFormatting: string = 'dddd, MMMM Do YYYY, h:mm:ss a'
const cancelPath = 'data/html/fortress/functions-cancel.htm'
const applyPath = 'data/html/fortress/functions-apply.htm'

export class FortManagerBypass extends ListenerLogic {
    constructor() {
        super( 'FortManagerBypass', 'listeners/bypass/FortManager.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcBypass,
                method: this.onNpcBypass.bind( this ),
                ids: npcIds,
            },
        ]
    }

    async onNpcBypass( data: NpcBypassEvent ): Promise<EventTerminationResult> {
        let commandChunks: Array<string> = data.command.split( ' ' )
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.originatorId ) as L2FortManagerInstance

        switch ( commandChunks[ 0 ].toLowerCase() ) {
            case 'expel':
                this.onExpel( player, npc )
                return TerminatedResultHelper.defaultSuccess

            case 'banish_foreigner':
                await this.onBanishForeigners( player, npc )
                return TerminatedResultHelper.defaultSuccess

            case 'receive_report':
                this.onReceiveReport( player, npc )
                return TerminatedResultHelper.defaultSuccess

            case 'operate_door':
                this.onOperateDoors( player, npc, commandChunks )
                return TerminatedResultHelper.defaultSuccess

            case 'manage_vault':
                this.onManageVault( player, npc, commandChunks )
                return TerminatedResultHelper.defaultSuccess

            case 'withdrawsortedc':
                this.onWithdrawSortedItems( player, npc, commandChunks )
                return TerminatedResultHelper.defaultSuccess

            case 'functions':
                this.onUseFunctions( player, npc, commandChunks )
                return TerminatedResultHelper.defaultSuccess

            case 'manage':
                await this.onManageFort( player, npc, commandChunks )
                return TerminatedResultHelper.defaultSuccess

            case 'support':
                await this.onSupportBuff( player, npc, commandChunks )
                return TerminatedResultHelper.defaultSuccess

            case 'support_back':
                this.onSupportMenu( player, npc )
                return TerminatedResultHelper.defaultSuccess

            case 'goto':
                await this.onTeleport( player, commandChunks )
                return TerminatedResultHelper.defaultSuccess
        }

        return TerminatedResultHelper.defaultFailure
    }

    sendHtmlMessage( player: L2PcInstance, html: string, path: string, npc: L2Npc ): void {
        html = html.replace( /%npcId%/g, npc.getId().toString() )
        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
    }

    showVaultWindowDeposit( player: L2PcInstance ): void {
        player.sendOwnedData( ActionFailed() )
        player.setActiveWarehouse( player.getClan().getWarehouse() )
        player.sendOwnedData( WareHouseDepositList( player, WarehouseDepositType.Clan ) )
    }

    showVaultWindowWithdraw( player: L2PcInstance, type: WarehouseListType, sortOrder: number, npc: L2Npc ): void {
        if ( player.isClanLeader() || player.hasClanPrivilege( ClanPrivilege.WarehouseAccess ) ) {

            player.sendOwnedData( ActionFailed() )
            let warehouse = player.getClan().getWarehouse()
            player.setActiveWarehouse( warehouse )

            if ( warehouse.getSize() === 0 ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_ITEM_DEPOSITED_IN_WH ) )
                return
            }

            if ( type ) {
                return player.sendOwnedData( SortedWareHouseWithdrawalList( warehouse, player.getAdena(), WarehouseDepositType.Clan, type, sortOrder ) )
            }

            return player.sendOwnedData( WareHouseWithdrawalList( warehouse, WarehouseDepositType.Clan, player.getAdena() ) )
        }

        let path = 'data/html/fortress/foreman-noprivs.htm'
        let html: string = DataManager.getHtmlData().getItem( path )
        return this.sendHtmlMessage( player, html, path, npc )
    }

    async onTeleport( player: L2PcInstance, commandChunks: Array<string> ): Promise<void> {
        if ( commandChunks.length < 2 ) {
            return
        }

        let teleportId = parseInt( commandChunks[ 1 ] )
        await TeleportLocationManager.teleportPlayer( player, teleportId )

        player.sendOwnedData( ActionFailed() )
    }

    onExpel( player: L2PcInstance, npc: L2FortManagerInstance ): void {
        if ( player.hasClanPrivilege( ClanPrivilege.CastleDismiss ) ) {
            let expelPath = 'data/html/fortress/foreman-expel.htm'
            return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( expelPath ), expelPath, npc )
        }

        let noPriviledgesPath = 'data/html/fortress/foreman-noprivs.htm'
        return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( noPriviledgesPath ), noPriviledgesPath, npc )
    }

    async onBanishForeigners( player: L2PcInstance, npc: L2FortManagerInstance ): Promise<void> {
        if ( player.hasClanPrivilege( ClanPrivilege.CastleDismiss ) ) {
            await npc.getFort().banishForeigners()

            let expelledPath = 'data/html/fortress/foreman-expeled.htm'
            return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( expelledPath ), expelledPath, npc )
        }

        let noPriviledgesPath = 'data/html/fortress/foreman-noprivs.htm'
        return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( noPriviledgesPath ), noPriviledgesPath, npc )
    }

    onReceiveReport( player: L2PcInstance, npc: L2FortManagerInstance ): void {
        let fort = npc.getFort()
        if ( fort.getFortState() < 2 ) {
            let path = 'data/html/fortress/foreman-report.htm'
            let html: string = DataManager.getHtmlData().getItem( path )

            if ( ConfigManager.fortress.getMaxKeepTime() > 0 ) {
                let hour = Math.floor( fort.getTimeTillRebelArmy() / 3600 )
                let minutes = Math.floor( ( fort.getTimeTillRebelArmy() - ( hour * 3600 ) ) / 60 )
                html = html.replace( /%hr%/g, hour.toString() )
                           .replace( /%min%/g, minutes.toString() )
            } else {
                let hour = Math.floor( fort.getOwnedTime() / 3600 )
                let minutes = Math.floor( ( fort.getOwnedTime() - ( hour * 3600 ) ) / 60 )
                html = html.replace( /%hr%/g, hour.toString() )
                           .replace( /%min%/g, minutes.toString() )
            }

            return this.sendHtmlMessage( player, html, path, npc )
        }

        let reportPath = 'data/html/fortress/foreman-castlereport.htm'
        let html: string = DataManager.getHtmlData().getItem( reportPath )
        let hour, minutes
        if ( ConfigManager.fortress.getMaxKeepTime() > 0 ) {
            hour = Math.floor( fort.getTimeTillRebelArmy() / 3600 )
            minutes = Math.floor( ( fort.getTimeTillRebelArmy() - ( hour * 3600 ) ) / 60 )

            html = html.replace( /%hr%/g, hour.toString() )
                       .replace( /%min%/g, minutes.toString() )
        } else {
            hour = Math.floor( fort.getOwnedTime() / 3600 )
            minutes = Math.floor( ( fort.getOwnedTime() - ( hour * 3600 ) ) / 60 )

            html = html.replace( /%hr%/g, hour.toString() )
                       .replace( /%min%/g, minutes.toString() )
        }

        let timeLeft: number = fort.getTimeTillNextFortUpdate()
        hour = Math.floor( timeLeft / 3600 )
        minutes = Math.floor( ( timeLeft - ( hour * 3600 ) ) / 60 )

        html = html.replace( /%castle%/g, fort.getContractedCastle().getName() )
                   .replace( /%hr2%/g, hour.toString() )
                   .replace( /%min2%/g, minutes.toString() )

        return this.sendHtmlMessage( player, html, reportPath, npc )
    }

    onOperateDoors( player: L2PcInstance, npc: L2FortManagerInstance, commandChunks: Array<string> ): void {
        if ( player.hasClanPrivilege( ClanPrivilege.CastleOpenDoors ) ) {
            if ( commandChunks.length > 1 ) {
                let shouldOpen = commandChunks[ 1 ] === '1'
                let fort = npc.getFort()

                commandChunks.slice( 2 ).forEach( ( currentValue: string ) => {
                    fort.openCloseDoor( player, parseInt( currentValue, 10 ), shouldOpen )
                } )

                if ( shouldOpen ) {
                    let openedPath = 'data/html/fortress/foreman-opened.htm'
                    return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( openedPath ), openedPath, npc )
                }

                let closedPath = 'data/html/fortress/foreman-closed.htm'
                return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( closedPath ), closedPath, npc )
            }

            let htmlPath = `data/html/fortress/${ npc.getId() }-d.htm`
            let html = DataManager.getHtmlData().getItem( htmlPath )
                                  .replace( /%npcname%/g, this.getName() )
            return this.sendHtmlMessage( player, html, htmlPath, npc )
        }

        let noPriviledgesPath = 'data/html/fortress/foreman-noprivs.htm'
        return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( noPriviledgesPath ), noPriviledgesPath, npc )
    }

    onManageVault( player: L2PcInstance, npc: L2FortManagerInstance, commandChunks: Array<string> ): void {
        if ( player.hasClanPrivilege( ClanPrivilege.WarehouseAccess ) && commandChunks.length > 1 ) {
            switch ( commandChunks[ 1 ].toLowerCase() ) {
                case 'deposit':
                    return this.showVaultWindowDeposit( player )

                case 'withdraw':
                    if ( ConfigManager.customs.enableWarehouseSortingClan() ) {
                        let path = 'data/html/mods/WhSortedC.htm'
                        if ( DataManager.getHtmlData().hasItem( path ) ) {
                            return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( path ), path, npc )
                        }
                    }

                    return this.showVaultWindowWithdraw( player, null, 0, npc )
            }

            let vaultPath = 'data/html/fortress/foreman-vault.htm'
            return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( vaultPath ), vaultPath, npc )
        }

        let noPriviledgesPath = 'data/html/fortress/foreman-noprivs.htm'
        return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( noPriviledgesPath ), noPriviledgesPath, npc )
    }

    onWithdrawSortedItems( player: L2PcInstance, npc: L2FortManagerInstance, commandChunks: Array<string> ): void {
        if ( commandChunks.length > 2 ) {
            return this.showVaultWindowWithdraw( player, WarehouseListType[ commandChunks[ 1 ] ], getWarehouseWithdrawSortOrder( commandChunks[ 2 ] ), npc )
        }

        if ( commandChunks.length > 1 ) {
            return this.showVaultWindowWithdraw( player, WarehouseListType[ commandChunks[ 1 ] ], WarehouseWithdrawSort.A2Z, npc )
        }

        return this.showVaultWindowWithdraw( player, WarehouseListType.ALL, WarehouseWithdrawSort.A2Z, npc )
    }

    onUseFunctions( player: L2PcInstance, npc: L2FortManagerInstance, commandChunks: Array<string> ): void {
        let fort = npc.getFort()

        // TODO : add additional function types that exist on clan hall
        if ( commandChunks.length > 1 ) {
            switch ( commandChunks[ 1 ].toLowerCase() ) {
                case 'tele':
                    let teleportFunction = fort.getFunction( FortFunctionType.Teleport )
                    let path = teleportFunction ?
                            `data/html/fortress/${ npc.getId() }-t${ teleportFunction.level }.htm` :
                            'data/html/fortress/foreman-nac.htm'

                    return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( path ), path, npc )

                case 'support':
                    let html: string
                    let htmlPath: string

                    let supportFunction = fort.getFunction( FortFunctionType.SupportBuffs )
                    if ( !supportFunction ) {
                        htmlPath = 'data/html/fortress/foreman-nac.htm'
                        html = DataManager.getHtmlData().getItem( htmlPath )
                    } else {
                        htmlPath = `data/html/fortress/support${ supportFunction.level }.htm`
                        html = DataManager.getHtmlData().getItem( htmlPath )
                                          .replace( /%mp%/g, npc.getCurrentMp().toString() )
                    }

                    return this.sendHtmlMessage( player, html, htmlPath, npc )

                case 'back':
                    return npc.showChatWindowDefault( player )
            }
        }

        let restoreExpFunction = fort.getFunction( FortFunctionType.RestoreExp )
        let restoreExp: string = restoreExpFunction ? restoreExpFunction.level.toString() : '0'

        let restoreHpFunction = fort.getFunction( FortFunctionType.RestoreHp )
        let restoreHp: string = restoreHpFunction ? restoreHpFunction.level.toString() : '0'

        let restoreMpFunction = fort.getFunction( FortFunctionType.RestoreMp )
        let restoreMp: string = restoreMpFunction ? restoreMpFunction.level.toString() : '0'

        let functionsPath = 'data/html/fortress/foreman-functions.htm'
        let html: string = DataManager.getHtmlData().getItem( functionsPath )
                                      .replace( /%xp_regen%/g, restoreExp )
                                      .replace( /%hp_regen%/g, restoreHp )
                                      .replace( /%mp_regen%/g, restoreMp )

        return this.sendHtmlMessage( player, html, functionsPath, npc )
    }

    async onManageFort( player: L2PcInstance, npc: L2FortManagerInstance, commandChunks: Array<string> ): Promise<void> {
        if ( player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) && commandChunks.length > 1 ) {
            return this.onManageFortFunctions( player, npc, commandChunks )
        }

        let path = 'data/html/fortress/foreman-noprivs.htm'
        let html = DataManager.getHtmlData().getItem( path )
        return this.sendHtmlMessage( player, html, path, npc )
    }

    async onManageFortFunctions( player: L2PcInstance, npc: L2FortManagerInstance, commandChunks: Array<string> ): Promise<void> {
        switch ( commandChunks[ 1 ].toLowerCase() ) {
            case 'recovery':
                if ( commandChunks.length > 2 ) {
                    return this.onEditFunction( player, npc, commandChunks )
                }

                return this.onEditRecovery( player, npc )

            case 'other':
                if ( commandChunks.length > 2 ) {
                    return this.onEditOtherFunction( player, npc, commandChunks )
                }

                return this.onEditOther( player, npc )

            case 'back':
                return npc.showChatWindowDefault( player )
        }

        let path = 'data/html/fortress/manage.htm'
        return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( path ), path, npc )
    }

    async onEditFunction( player: L2PcInstance, npc: L2FortManagerInstance, commandChunks: Array<string> ): Promise<void> {
        let fort = npc.getFort()
        if ( !fort || !fort.getOwnerClan() ) {
            player.sendMessage( 'This fortress has no owner, you cannot change the configuration.' )
            return
        }

        switch ( commandChunks[ 2 ] ) {
            case 'hp_cancel':
                return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( cancelPath )
                                                                .replace( /%apply%/g, 'recovery hp 0' ), cancelPath, npc )

            case 'mp_cancel':
                return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( cancelPath )
                                                                .replace( /%apply%/g, 'recovery mp 0' ), cancelPath, npc )

            case 'exp_cancel':
                return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( cancelPath )
                                                                .replace( /%apply%/g, 'recovery exp 0' ), cancelPath, npc )

            case 'edit_hp':
                return this.onEditHpFunction( player, npc, commandChunks.length > 3 ? commandChunks[ 3 ] : '0' )

            case 'edit_mp':
                return this.onEditMpFunction( player, npc, commandChunks.length > 3 ? commandChunks[ 3 ] : '0' )

            case 'edit_exp':
                return this.onEditExpFunction( player, npc, commandChunks.length > 3 ? commandChunks[ 3 ] : '0' )

            case 'hp':
                return this.onConfirmHp( player, npc, commandChunks.length > 3 ? commandChunks[ 3 ] : '0' )

            case 'mp':
                return this.onConfirmMp( player, npc, commandChunks.length > 3 ? commandChunks[ 3 ] : '0' )

            case 'exp':
                return this.onConfirmExp( player, npc, commandChunks.length > 3 ? commandChunks[ 3 ] : '0' )
        }
    }

    onEditHpFunction( player: L2PcInstance, npc: L2FortManagerInstance, value: string ): void {
        let quantity: number = _.defaultTo( parseInt( value, 10 ), 0 )
        let cost: number = quantity <= 300 ? ConfigManager.fortress.getHpRegenerationFeeLvl1() : ConfigManager.fortress.getHpRegenerationFeeLvl2()

        let editHpHtml: string = DataManager.getHtmlData().getItem( applyPath )
                                            .replace( '%name%', '(HP Recovery Device)' )
                                            .replace( '%cost%', `${ cost }</font>Adena (${ ConfigManager.fortress.getHpRegenerationFunctionFeeRatio() / 1000 / 60 / 60 / 24 } Day</font>)` )
                                            .replace( '%use%', `Provides additional HP recovery for clan members in the fortress.<font color="00FFFF">${ quantity }%</font>` )
                                            .replace( '%apply%', `recovery hp ${ quantity }` )
        return this.sendHtmlMessage( player, editHpHtml, applyPath, npc )
    }

    onEditMpFunction( player: L2PcInstance, npc: L2FortManagerInstance, value: string ): void {
        let quantity: number = _.defaultTo( parseInt( value, 10 ), 0 )
        let cost: number = quantity <= 40 ? ConfigManager.fortress.getMpRegenerationFeeLvl1() : ConfigManager.fortress.getMpRegenerationFeeLvl2()

        let html: string = DataManager.getHtmlData().getItem( applyPath )
                                      .replace( '%name%', '(MP Recovery Device)' )
                                      .replace( '%cost%', `${ cost }</font>Adena (${ ConfigManager.fortress.getMpRegenerationFunctionFeeRatio() / 1000 / 60 / 60 / 24 } Day</font>)` )
                                      .replace( '%use%', `Provides additional MP recovery for clan members in the fortress.<font color="00FFFF">${ quantity }%</font>` )
                                      .replace( '%apply%', `recovery mp ${ quantity }` )

        return this.sendHtmlMessage( player, html, applyPath, npc )
    }

    onEditExpFunction( player: L2PcInstance, npc: L2FortManagerInstance, value: string ): void {
        let quantity: number = _.defaultTo( parseInt( value, 10 ), 0 )
        let cost: number = quantity <= 45 ? ConfigManager.fortress.getExpRegenerationFeeLvl1() : ConfigManager.fortress.getExpRegenerationFeeLvl2()

        let html: string = DataManager.getHtmlData().getItem( applyPath )
                                      .replace( '%name%', '(EXP Recovery Device)' )
                                      .replace( '%cost%', `${ cost }</font>Adena (${ ConfigManager.fortress.getExpRegenerationFunctionFeeRatio() / 1000 / 60 / 60 / 24 } Day</font>)` )
                                      .replace( '%use%', `Restores the Exp of any clan member who is resurrected in the fortress.<font color="00FFFF">${ quantity }%</font>` )
                                      .replace( '%apply%', `recovery exp ${ quantity }` )

        return this.sendHtmlMessage( player, html, applyPath, npc )
    }

    async onConfirmHp( player: L2PcInstance, npc: L2FortManagerInstance, value: string ): Promise<void> {
        let amount: number = _.defaultTo( parseInt( value, 10 ), 0 )

        let fort = npc.getFort()
        let restoreHp = fort.getFunction( FortFunctionType.RestoreHp )
        if ( restoreHp && restoreHp.level === amount ) {
            let functionsUsedPath = 'data/html/fortress/functions-used.htm'
            let html: string = DataManager.getHtmlData().getItem( functionsUsedPath )
                                          .replace( /%val%/g, `${ amount }%` )

            return this.sendHtmlMessage( player, html, functionsUsedPath, npc )
        }

        let fee: number = 0
        let path: string = 'data/html/fortress/functions-apply_confirmed.htm'

        switch ( amount ) {
            case 0:
                path = 'data/html/fortress/functions-cancel_confirmed.htm'
                break

            case 300:
                fee = ConfigManager.fortress.getHpRegenerationFeeLvl1()
                break

            default:
                fee = ConfigManager.fortress.getHpRegenerationFeeLvl2()
                break
        }

        let isApplied: boolean = await fort.updateFunctions( player,
                FortFunctionType.RestoreHp,
                amount,
                fee,
                ConfigManager.fortress.getHpRegenerationFunctionFeeRatio(),
                !fort.getFunction( FortFunctionType.RestoreHp ) )
        if ( !isApplied ) {
            let lowAdenaPath = 'data/html/fortress/low_adena.htm'
            this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( lowAdenaPath ), lowAdenaPath, npc )
        }

        return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( path ), path, npc )
    }

    async onConfirmMp( player: L2PcInstance, npc: L2FortManagerInstance, value: string ): Promise<void> {
        let amount: number = _.defaultTo( parseInt( value, 10 ), 0 )
        let fort = npc.getFort()

        let restoreMp = fort.getFunction( FortFunctionType.RestoreMp )
        if ( restoreMp && restoreMp.level === amount ) {
            let functionsUsedPath = 'data/html/fortress/functions-used.htm'
            let html: string = DataManager.getHtmlData().getItem( functionsUsedPath )
                                          .replace( /%val%/g, `${ amount }%` )

            return this.sendHtmlMessage( player, html, functionsUsedPath, npc )
        }

        let fee: number = 0
        let path: string = 'data/html/fortress/functions-apply_confirmed.htm'

        switch ( amount ) {
            case 0:
                path = 'data/html/fortress/functions-cancel_confirmed.htm'
                break

            case 40:
                fee = ConfigManager.fortress.getMpRegenerationFeeLvl1()
                break

            default:
                fee = ConfigManager.fortress.getMpRegenerationFeeLvl2()
                break
        }

        let isApplied: boolean = await fort.updateFunctions( player,
                FortFunctionType.RestoreMp,
                amount,
                fee,
                ConfigManager.fortress.getMpRegenerationFunctionFeeRatio(),
                !fort.getFunction( FortFunctionType.RestoreMp ) )
        if ( !isApplied ) {
            let lowAdenaPath = 'data/html/fortress/low_adena.htm'
            this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( lowAdenaPath ), lowAdenaPath, npc )
        }

        return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( path ), path, npc )
    }

    async onConfirmExp( player: L2PcInstance, npc: L2FortManagerInstance, value: string ): Promise<void> {
        let amount: number = _.defaultTo( parseInt( value, 10 ), 0 )
        let fort = npc.getFort()

        let restoreExp = fort.getFunction( FortFunctionType.RestoreExp )
        if ( restoreExp && restoreExp.level === amount ) {
            let functionsUsedPath = 'data/html/fortress/functions-used.htm'
            let html: string = DataManager.getHtmlData().getItem( functionsUsedPath )
                                          .replace( /%val%/g, `${ amount }%` )

            return this.sendHtmlMessage( player, html, functionsUsedPath, npc )
        }

        let fee: number = 0
        let path: string = 'data/html/fortress/functions-apply_confirmed.htm'

        switch ( amount ) {
            case 0:
                path = 'data/html/fortress/functions-cancel_confirmed.htm'
                break

            case 45:
                fee = ConfigManager.fortress.getExpRegenerationFeeLvl1()
                break

            default:
                fee = ConfigManager.fortress.getExpRegenerationFeeLvl2()
                break
        }

        let isApplied: boolean = await fort.updateFunctions( player,
                FortFunctionType.RestoreExp,
                amount,
                fee,
                ConfigManager.fortress.getMpRegenerationFunctionFeeRatio(),
                !fort.getFunction( FortFunctionType.RestoreExp ) )
        if ( !isApplied ) {
            let lowAdeanPath = 'data/html/fortress/low_adena.htm'
            this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( lowAdeanPath ), lowAdeanPath, npc )
        }

        return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( path ), path, npc )
    }

    onEditRecovery( player: L2PcInstance, npc: L2FortManagerInstance ): void {
        let editRecoveryPath = 'data/html/fortress/edit_recovery.htm'
        let html: string = DataManager.getHtmlData().getItem( editRecoveryPath )

        const hp = '[<a action="bypass -h manage recovery edit_hp 300">300%</a>][<a action="bypass -h manage recovery edit_hp 400">400%</a>]'
        const exp = '[<a action="bypass -h manage recovery edit_exp 45">45%</a>][<a action="bypass -h manage recovery edit_exp 50">50%</a>]'
        const mp = '[<a action="bypass -h manage recovery edit_mp 40">40%</a>][<a action="bypass -h manage recovery edit_mp 50">50%</a>]'

        let fort = npc.getFort()
        let hpFunction = fort.getFunction( FortFunctionType.RestoreHp )
        if ( hpFunction ) {
            html = html.replace( /%hp_recovery%/g, `${ hpFunction.level }%</font> (<font color="FFAABB">${ hpFunction.fee }</font>Adena / ${ ConfigManager.fortress.getHpRegenerationFunctionFeeRatio() / 1000 / 60 / 60 / 24 } Day)` )
                       .replace( /%hp_period%/g, `Withdraw the fee for the next time at ${ moment( hpFunction.endDate ).format( dateFormatting ) }` )
                       .replace( /%change_hp%/g, `[<a action="bypass -h manage recovery hp_cancel">Deactivate</a>] ${ hp }` )
        } else {
            html = html.replace( /%hp_recovery%/g, 'none' )
                       .replace( /%hp_period%/g, 'none' )
                       .replace( /%change_hp%/g, hp )
        }

        let expFunction = fort.getFunction( FortFunctionType.RestoreExp )
        if ( expFunction ) {
            html = html.replace( /%exp_recovery%/g, `${ expFunction.level }%</font> (<font color="FFAABB">${ expFunction.fee }</font>Adena / ${ ConfigManager.fortress.getExpRegenerationFunctionFeeRatio() / 1000 / 60 / 60 / 24 } Day)` )
                       .replace( /%exp_period%/g, `Withdraw the fee for the next time at ${ moment( expFunction.endDate ).format( dateFormatting ) }` )
                       .replace( /%change_exp%/g, `[<a action="bypass -h manage recovery exp_cancel">Deactivate</a>] ${ exp }` )
        } else {
            html = html.replace( /%exp_recovery%/g, 'none' )
                       .replace( /%exp_period%/g, 'none' )
                       .replace( /%change_exp%/g, exp )
        }

        let mpFunction = fort.getFunction( FortFunctionType.RestoreMp )
        if ( mpFunction ) {
            html.replace( /%mp_recovery%/g, `${ mpFunction.level }%</font> (<font color="FFAABB">${ mpFunction.fee }</font>Adena / ${ ConfigManager.fortress.getMpRegenerationFunctionFeeRatio() / 1000 / 60 / 60 / 24 } Day)` )
                .replace( /%mp_period%/g, `Withdraw the fee for the next time at ${ moment( mpFunction.endDate ).format( dateFormatting ) }` )
                .replace( /%change_mp%/g, `[<a action="bypass -h manage recovery mp_cancel">Deactivate</a>] ${ mp }` )
        } else {
            html = html.replace( /%mp_recovery%/g, 'none' )
                       .replace( /%mp_period%/g, 'none' )
                       .replace( /%change_mp%/g, mp )
        }

        return this.sendHtmlMessage( player, html, editRecoveryPath, npc )
    }

    async onEditOtherFunction( player: L2PcInstance, npc: L2FortManagerInstance, commandChunks: Array<string> ): Promise<void> {
        let fort = npc.getFort()
        if ( !fort || !fort.getOwnerClan() ) {
            player.sendMessage( 'This fortress has no owner, you cannot change the configuration.' )
            return
        }

        switch ( commandChunks[ 2 ] ) {
            case 'tele_cancel':
                return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( cancelPath )
                                                                .replace( /%apply%/g, 'other tele 0' ), cancelPath, npc )

            case 'support_cancel':
                return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( cancelPath )
                                                                .replace( /%apply%/g, 'other support 0' ), cancelPath, npc )

            case 'edit_support':
                return this.onEditSupportFunction( player, npc, commandChunks.length > 3 ? commandChunks[ 3 ] : '0' )

            case 'edit_tele':
                return this.onEditTeleportFunction( player, npc, commandChunks.length > 3 ? commandChunks[ 3 ] : '0' )

            case 'tele':
                return this.onConfirmTeleport( player, npc, commandChunks.length > 3 ? commandChunks[ 3 ] : '0' )

            case 'support':
                return this.onConfirmSupport( player, npc, commandChunks.length > 3 ? commandChunks[ 3 ] : '0' )
        }
    }

    onEditSupportFunction( player: L2PcInstance, npc: L2FortManagerInstance, value: string ): void {
        let stage: number = _.defaultTo( parseInt( value, 10 ), 0 )
        let cost = stage === 1 ? ConfigManager.fortress.getSupportFeeLvl1() : ConfigManager.fortress.getSupportFeeLvl2()
        let applyPath = 'data/html/fortress/functions-apply.htm'
        let html: string = DataManager.getHtmlData().getItem( applyPath )
                                      .replace( /%name%/g, 'Insignia (Supplementary Magic)' )
                                      .replace( /%cost%/g, `${ cost } </font>Adena / ${ ConfigManager.fortress.getSupportFunctionFeeRatio() / 1000 / 60 / 60 / 24 } Day</font>)` )
                                      .replace( /%use%/g, 'Enables the use of supplementary magic.' )
                                      .replace( /%apply%/g, `other support ${ stage }` )

        return this.sendHtmlMessage( player, html, applyPath, npc )
    }

    onEditTeleportFunction( player: L2PcInstance, npc: L2FortManagerInstance, value: string ): void {
        let stage: number = _.defaultTo( parseInt( value, 10 ), 0 )
        let cost = stage === 1 ? ConfigManager.fortress.getTeleportFunctionFeeLvl1() : ConfigManager.fortress.getTeleportFunctionFeeLvl2()
        let applyPath = 'data/html/fortress/functions-apply.htm'
        let html: string = DataManager.getHtmlData().getItem( applyPath )
                                      .replace( /%name%/g, 'Mirror (Teleportation Device)' )
                                      .replace( /%cost%/g, `${ cost } </font>Adena / ${ ConfigManager.fortress.getTeleportFunctionFeeRatio() / 1000 / 60 / 60 / 24 } Day</font>)` )
                                      .replace( /%use%/g, `Teleports clan members in a fort to the target <font color="00FFFF">Stage ${ stage }</font> staging area` )
                                      .replace( /%apply%/g, `other tele ${ stage }` )

        return this.sendHtmlMessage( player, html, applyPath, npc )
    }

    async onConfirmTeleport( player: L2PcInstance, npc: L2FortManagerInstance, value: string ): Promise<void> {
        let stage: number = _.defaultTo( parseInt( value, 10 ), 0 )
        let fort = npc.getFort()

        let currentFunction = fort.getFunction( FortFunctionType.Teleport )
        if ( currentFunction && currentFunction.level === stage ) {
            let functionsUsedPath = 'data/html/fortress/functions-used.htm'
            let html: string = DataManager.getHtmlData().getItem( functionsUsedPath )
                                          .replace( /%val%/g, `Stage ${ stage }%` )

            return this.sendHtmlMessage( player, html, functionsUsedPath, npc )
        }

        let fee: number = 0
        let path: string = 'data/html/fortress/functions-apply_confirmed.htm'

        switch ( stage ) {
            case 0:
                path = 'data/html/fortress/functions-cancel_confirmed.htm'
                break

            case 1:
                fee = ConfigManager.fortress.getTeleportFunctionFeeLvl1()
                break

            default:
                fee = ConfigManager.fortress.getTeleportFunctionFeeLvl2()
                break
        }

        let isApplied: boolean = await fort.updateFunctions( player,
                FortFunctionType.Teleport,
                stage,
                fee,
                ConfigManager.fortress.getTeleportFunctionFeeRatio(), !currentFunction )

        if ( !isApplied ) {
            let lowAdenaPath = 'data/html/fortress/low_adena.htm'
            this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( lowAdenaPath ), lowAdenaPath, npc )
        }

        return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( path ), path, npc )
    }

    async onConfirmSupport( player: L2PcInstance, npc: L2FortManagerInstance, value: string ): Promise<void> {
        let stage: number = _.defaultTo( parseInt( value, 10 ), 0 )
        let fort = npc.getFort()

        let currentFunction = fort.getFunction( FortFunctionType.SupportBuffs )
        if ( currentFunction && currentFunction.level === stage ) {
            let path = 'data/html/fortress/functions-used.htm'
            let html: string = DataManager.getHtmlData().getItem( path )
                                          .replace( /%val%/g, `Stage ${ stage }%` )

            return this.sendHtmlMessage( player, html, path, npc )
        }

        let fee: number = 0
        let path: string = 'data/html/fortress/functions-apply_confirmed.htm'

        switch ( stage ) {
            case 0:
                path = 'data/html/fortress/functions-cancel_confirmed.htm'
                break

            case 1:
                fee = ConfigManager.fortress.getSupportFeeLvl1()
                break

            default:
                fee = ConfigManager.fortress.getSupportFeeLvl2()
                break
        }

        let isApplied: boolean = await fort.updateFunctions( player,
                FortFunctionType.SupportBuffs,
                stage,
                fee,
                ConfigManager.fortress.getTeleportFunctionFeeRatio(), !currentFunction )

        if ( !isApplied ) {
            let lowAdenaPath = 'data/html/fortress/low_adena.htm'
            this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( lowAdenaPath ), lowAdenaPath, npc )
        }

        return this.sendHtmlMessage( player, DataManager.getHtmlData().getItem( path ), path, npc )
    }

    onEditOther( player: L2PcInstance, npc: L2FortManagerInstance ): void {
        let editOtherPath = 'data/html/fortress/edit_other.htm'
        let html: string = DataManager.getHtmlData().getItem( editOtherPath )

        const teleport = '[<a action="bypass -h manage other edit_tele 1">Level 1</a>][<a action="bypass -h manage other edit_tele 2">Level 2</a>]'
        const support = '[<a action="bypass -h manage other edit_support 1">Level 1</a>][<a action="bypass -h manage other edit_support 2">Level 2</a>]'

        let fort = npc.getFort()
        let teleportFunction = fort.getFunction( FortFunctionType.Teleport )
        if ( teleportFunction ) {
            html = html.replace( /%tele%/g, `Stage ${ teleportFunction.level }</font> (<font color="FFAABB">${ teleportFunction.fee }</font>Adena / ${ ConfigManager.fortress.getTeleportFunctionFeeRatio() / 1000 / 60 / 60 / 24 } Day)` )
                       .replace( /%tele_period%/g, `Withdraw the fee for the next time at ${ moment( teleportFunction.endDate ).format( dateFormatting ) }` )
                       .replace( /%change_tele%/g, `[<a action="bypass -h manage other tele_cancel">Deactivate</a>] ${ teleport }` )
        } else {
            html = html.replace( /%tele%/g, 'none' )
                       .replace( '%tele_period%', 'none' )
                       .replace( '%change_tele%', teleport )
        }

        let supportFunction = fort.getFunction( FortFunctionType.SupportBuffs )
        if ( supportFunction ) {
            html = html.replace( /%support%/g, `Stage ${ supportFunction.level }</font> (<font color="FFAABB">${ supportFunction.fee }</font>Adena / ${ ConfigManager.fortress.getSupportFunctionFeeRatio() / 1000 / 60 / 60 / 24 } Day)` )
                       .replace( /%support_period%/g, `Withdraw the fee for the next time at ${ moment( supportFunction.endDate ).format( dateFormatting ) }` )
                       .replace( /%change_support%/g, `[<a action="bypass -h manage other support_cancel">Deactivate</a>] ${ support }` )
        } else {
            html = html.replace( /%support%/g, 'none' )
                       .replace( /%support_period%/g, 'none' )
                       .replace( /%change_support%/g, support )
        }

        return this.sendHtmlMessage( player, html, editOtherPath, npc )
    }

    async onSupportBuff( player: L2PcInstance, npc: L2FortManagerInstance, commandChunks: Array<string> ) {
        let fort = npc.getFort()
        if ( commandChunks.length < 3
                || !fort
                || !fort.getFunction( FortFunctionType.SupportBuffs )
                || fort.getFunction( FortFunctionType.SupportBuffs ).level === 0 ) {
            return
        }

        npc.setTarget( player )

        let skill = SkillCache.getSkill( parseInt( commandChunks[ 1 ] ), parseInt( commandChunks[ 2 ] ) )
        if ( skill.hasEffectType( L2EffectType.Summon ) ) {
            await player.doCast( skill )
        } else {
            let notEnoughMp: boolean = ( skill.getStartCostMp() + skill.getFinishCostMp() ) > npc.getCurrentMp()

            if ( notEnoughMp ) {
                let path = 'data/html/fortress/support-no_mana.htm'
                let html: string = DataManager.getHtmlData().getItem( path )
                                              .replace( /%mp%/g, Math.floor( npc.getCurrentMp() ).toString() )

                return this.sendHtmlMessage( player, html, path, npc )
            }

            await npc.doCast( skill )
        }

        let path = 'data/html/fortress/support-done.htm'
        let html: string = DataManager.getHtmlData().getItem( path )
                                      .replace( /%mp%/g, Math.floor( npc.getCurrentMp() ).toString() )
        return this.sendHtmlMessage( player, html, path, npc )
    }

    onSupportMenu( player: L2PcInstance, npc: L2FortManagerInstance ) {
        let fort = npc.getFort()
        if ( !fort ) {
            return
        }

        let supportFunction = fort.getFunction( FortFunctionType.SupportBuffs )
        if ( !supportFunction || supportFunction.level === 0 ) {
            return
        }

        let path = `data/html/fortress/support${ supportFunction.level }.htm`
        let html: string = DataManager.getHtmlData().getItem( path )
                                      .replace( /%mp%/g, Math.floor( npc.getCurrentMp() ).toString() )
        return this.sendHtmlMessage( player, html, path, npc )
    }
}