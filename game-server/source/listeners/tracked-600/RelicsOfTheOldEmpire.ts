import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import _ from 'lodash'

const GHOST_OF_ADVENTURER = 31538
const ENTRANCE_PASS_TO_THE_SEPULCHER = 7075
const BROKEN_RELIC_PART = 7254
const minimumLevel = 74
const REQUIRED_RELIC_COUNT = 1000

const playerRewardItemIds: Array<number> = [
    6881, // Recipe: Forgotten Blade (60%)
    6883, // Recipe: Basalt Battlehammer (60%)
    6885, // Recipe: Imperial Staff (60%)
    6887, // Recipe: Angel Slayer (60%)
    6891, // Recipe: Dragon Hunter Axe (60%)
    6893, // Recipe: Saint Spear (60%)
    6895, // Recipe: Demon Splinter (60%)
    6897, // Recipe: Heavens Divider (60%)
    6899, // Recipe: Arcana Mace (60%)
    7580, // Recipe: Draconic Bow (60%)
]

type MonsterData = [ number, number, boolean ] // chance to get, chance to get double amount, is dropping pass
const monsterData: { [ npcId: number ]: MonsterData } = {
    21396: [ 0.51, 0, true ], // carrion_scarab
    21397: [ 0.50, 0, true ], // carrion_scarab_a
    21398: [ 0.95, 0, true ], // soldier_scarab
    21399: [ 0.84, 0, true ], // soldier_scarab_a
    21400: [ 0.76, 0, true ], // hexa_beetle
    21401: [ 0.67, 0, true ], // hexa_beetle_a
    21402: [ 0.69, 0, true ], // katraxith
    21403: [ 0.80, 0, true ], // katraxith_a
    21404: [ 0.90, 0, true ], // tera_beetle
    21405: [ 0.64, 0, true ], // tera_beetle_a
    21406: [ 0.87, 0, true ], // imperial_knight
    21407: [ 0.56, 0, true ], // imperial_knight_a
    21408: [ 0.82, 0, true ], // imperial_guard
    21409: [ 0.92, 0, true ], // imperial_guard_a
    21410: [ 0.81, 0, true ], // guardian_scarab
    21411: [ 0.66, 0, true ], // guardian_scarab_a
    21412: [ 1.00, 6, true ], // ustralith
    21413: [ 0.81, 0, true ], // ustralith_a
    21414: [ 0.79, 0, true ], // imperial_assassin
    21415: [ 0.80, 0, true ], // imperial_assassin_a
    21416: [ 0.82, 0, true ], // imperial_warlord
    21417: [ 1.00, 0.27, true ], // imperial_warlord_a
    21418: [ 0.66, 0, true ], // imperial_highguard
    21419: [ 0.67, 0, true ], // imperial_highguard_a
    21420: [ 0.82, 0, true ], // ashuras
    21421: [ 0.77, 0, true ], // ashuras_a
    21422: [ 0.88, 0, true ], // imperial_dancer
    21423: [ 0.94, 0, true ], // imperial_dancer_a
    21424: [ 1.00, 0.19, true ], // ashikenas
    21425: [ 1.00, 0.21, true ], // ashikenas_a
    21426: [ 1.00, 0.08, true ], // abraxian
    21427: [ 0.74, 0, true ], // abraxian_a
    21428: [ 0.76, 0, true ], // hasturan
    21429: [ 0.80, 0, true ], // hasturan_a
    21430: [ 1.00, 0.10, true ], // ahrimanes
    21431: [ 0.94, 0, true ], // ahrimanes_a
    21432: [ 1.00, 0.34, true ], // chakram_beetle
    21433: [ 1.00, 0.34, true ], // jamadhr_beetle
    21434: [ 1.00, 0.90, true ], // priest_of_blood
    21435: [ 1.00, 0.60, true ], // sacrifice_guide
    21436: [ 1.00, 0.66, true ], // sacrifice_bearer
    21437: [ 0.69, 0, true ], // sacrifice_scarab
    21798: [ 0.33, 0, true ], // guard_skeleton_2d
    21799: [ 0.61, 0, true ], // guard_skeleton_3d
    21800: [ 0.31, 0, true ], // guard_undead
    18120: [ 1.00, 0.28, false ], // r11_roomboss_strong
    18121: [ 1.00, 0.21, false ], // r11_roomboss_weak
    18122: [ 0.93, 0, false ], // r11_roomboss_teleport
    18123: [ 1.00, 0.28, false ], // r12_roomboss_strong
    18124: [ 1.00, 0.21, false ], // r12_roomboss_weak
    18125: [ 0.93, 0, false ], // r12_roomboss_teleport
    18126: [ 1.00, 0.28, false ], // r13_roomboss_strong
    18127: [ 1.00, 0.21, false ], // r13_roomboss_weak
    18128: [ 0.93, 0, false ], // r13_roomboss_teleport
    18129: [ 1.00, 0.28, false ], // r14_roomboss_strong
    18130: [ 1.00, 0.21, false ], // r14_roomboss_weak
    18131: [ 0.93, 0, false ], // r14_roomboss_teleport
    18132: [ 1.00, 0.30, false ], // r1_beatle_healer
    18133: [ 1.00, 0.20, false ], // r1_scorpion_warrior
    18134: [ 0.90, 0, false ], // r1_warrior_longatk1_h
    18135: [ 1.00, 0.20, false ], // r1_warrior_longatk2
    18136: [ 1.00, 0.20, false ], // r1_warrior_selfbuff
    18137: [ 0.89, 0, false ], // r1_wizard_h
    18138: [ 1.00, 0.19, false ], // r1_wizard_clanbuff
    18139: [ 1.00, 0.17, false ], // r1_wizard_debuff
    18140: [ 1.00, 0.19, false ], // r1_wizard_selfbuff
    18141: [ 0.76, 0, false ], // r21_scarab_roombosss
    18142: [ 0.76, 0, false ], // r22_scarab_roombosss
    18143: [ 0.76, 0, false ], // r23_scarab_roombosss
    18144: [ 0.76, 0, false ], // r24_scarab_roombosss
    18145: [ 0.65, 0, false ], // r2_wizard_clanbuff
    18146: [ 0.66, 0, false ], // r2_warrior_longatk2
    18147: [ 0.62, 0, false ], // r2_wizard
    18148: [ 0.72, 0, false ], // r2_warrior
    18149: [ 0.63, 0, false ], // r2_bomb
    18166: [ 0.92, 0, false ], // r3_warrior
    18167: [ 0.92, 0, false ], // r3_warrior_longatk1_h
    18168: [ 0.93, 0, false ], // r3_warrior_longatk2
    18169: [ 0.90, 0, false ], // r3_warrior_selfbuff
    18170: [ 0.90, 0, false ], // r3_wizard_h
    18171: [ 0.94, 0, false ], // r3_wizard_clanbuff
    18172: [ 0.89, 0, false ], // r3_wizard_selfbuff
    18173: [ 0.99, 0, false ], // r41_roomboss_strong
    18174: [ 1.00, 0.22, false ], // r41_roomboss_weak
    18175: [ 0.93, 0, false ], // r41_roomboss_teleport
    18176: [ 0.99, 0, false ], // r42_roomboss_strong
    18177: [ 1.00, 0.22, false ], // r42_roomboss_weak
    18178: [ 0.93, 0, false ], // r42_roomboss_teleport
    18179: [ 0.99, 0, false ], // r43_roomboss_strong
    18180: [ 1.00, 0.22, false ], // r43_roomboss_weak
    18181: [ 0.93, 0, false ], // r43_roomboss_teleport
    18182: [ 1.00, 0.22, false ], // r44_roomboss_weak
    18183: [ 0.99, 0, false ], // r44_roomboss_strong
    18184: [ 0.93, 0, false ], // r44_roomboss_teleport
    18185: [ 1.00, 0.23, false ], // r4_healer_srddmagic
    18186: [ 1.00, 0.24, false ], // r4_hearler_srdebuff
    18187: [ 1.00, 0.20, false ], // r4_warrior
    18188: [ 0.90, 0, false ], // r4_warrior_longatk1_h
    18189: [ 1.00, 0.20, false ], // r4_warrior_longatk2
    18190: [ 1.00, 0.20, false ], // r4_warrior_selfbuff
    18191: [ 0.89, 0, false ], // r4_wizard_h
    18192: [ 1.00, 0.19, false ], // r4_wizard_clanbuff
    18193: [ 1.00, 0.17, false ], // r4_wizard_debuff
    18194: [ 1.00, 0.19, false ], // r4_wizard_selfbuff
    18195: [ 0.91, 0, false ], // r4_bomb
    18220: [ 1.00, 0.24, false ], // r5_healer1
    18221: [ 1.00, 0.27, false ], // r5_healer2
    18222: [ 1.00, 0.21, false ], // r5_warrior
    18223: [ 0.90, 0, false ], // r5_warrior_longatk1_h
    18224: [ 1.00, 0.22, false ], // r5_warrior_longatk2
    18225: [ 1.00, 0.21, false ], // r5_warrior_sbuff
    18226: [ 0.89, 0, false ], // r5_wizard_h
    18227: [ 1.00, 0.53, false ], // r5_wizard_clanbuff
    18228: [ 1.00, 0.15, false ], // r5_wizard_debuff
    18229: [ 1.00, 0.19, false ], // r5_wizard_slefbuff
    18230: [ 0.49, 0, false ], // r5_bomb
}

const ARCHON_OF_HALISHA: Array<number> = [
    18212,
    18213,
    18214,
    18215,
    18216,
    18217,
    18218,
    18219,
]

const entrancePassDropRate: number = 0.0333333333333

export class RelicsOfTheOldEmpire extends ListenerLogic {
    constructor() {
        super( 'Q00619_RelicsOfTheOldEmpire', 'listeners/tracked-600/RelicsOfTheOldEmpire.ts' )
        this.questId = 619
        this.questItemIds = [
            BROKEN_RELIC_PART,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ GHOST_OF_ADVENTURER ]
    }

    getTalkIds(): Array<number> {
        return [ GHOST_OF_ADVENTURER ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ...ARCHON_OF_HALISHA,
            ..._.keys( monsterData ).map( value => _.parseInt( value ) ),
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31538-02.htm':
                state.startQuest()
                break

            case '31538-05.html':
                break

            case '31538-06.html':
                if ( QuestHelper.getQuestItemsCount( player, BROKEN_RELIC_PART ) >= REQUIRED_RELIC_COUNT ) {
                    await QuestHelper.takeSingleItem( player, BROKEN_RELIC_PART, REQUIRED_RELIC_COUNT )
                    await QuestHelper.rewardSingleItem( player, _.sample( playerRewardItemIds ), 1 )

                    break
                }

                return

            case '31538-08.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        if ( ARCHON_OF_HALISHA.includes( data.npcId ) ) {
            let amount: number = Math.random() < QuestHelper.getAdjustedChance( BROKEN_RELIC_PART, 0.79, data.isChampion ) ? 4 : 3
            await QuestHelper.rewardSingleQuestItem( player, BROKEN_RELIC_PART, amount, data.isChampion )

            return
        }

        let [ chance, doubleChance, isDroppingPass ]: MonsterData = monsterData[ data.npcId ]
        if ( Math.random() > QuestHelper.getAdjustedChance( BROKEN_RELIC_PART, chance, data.isChampion ) ) {
            return
        }

        let amount: number = doubleChance > 0 && Math.random() < QuestHelper.getAdjustedChance( BROKEN_RELIC_PART, doubleChance, data.isChampion ) ? 2 : 1
        await QuestHelper.rewardSingleQuestItem( player, BROKEN_RELIC_PART, amount, data.isChampion )

        if ( isDroppingPass && Math.random() < QuestHelper.getAdjustedChance( ENTRANCE_PASS_TO_THE_SEPULCHER, entrancePassDropRate, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, ENTRANCE_PASS_TO_THE_SEPULCHER, 1, data.isChampion )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31538-01.htm' : '31538-03.html' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.getQuestItemsCount( player, BROKEN_RELIC_PART ) >= REQUIRED_RELIC_COUNT ? '31538-04.html' : '31538-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00619_RelicsOfTheOldEmpire'
    }
}