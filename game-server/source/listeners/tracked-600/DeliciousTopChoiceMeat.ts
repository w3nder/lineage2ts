import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const TUNATUN = 31537
const TOP_QUALITY_MEAT = 7546
const PRIME_MEAT = 15534
const minimumLevel = 82
const requiredAmount = 120
const recipeIds: Array<number> = [
    10373, // Recipe - Icarus Sawsword (60%)
    10374, // Recipe - Icarus Disperser (60%)
    10375, // Recipe - Icarus Spirit (60%)
    10376, // Recipe - Icarus Heavy Arms (60%)
    10377, // Recipe - Icarus Trident (60%)
    10378, // Recipe - Icarus Hammer (60%)
    10379, // Recipe - Icarus Hand (60%)
    10380, // Recipe - Icarus Hall (60%)
    10381, // Recipe - Icarus Spitter (60%)
]

const icarusItemIds: Array<number> = [
    10397, // Icarus Sawsword Piece
    10398, // Icarus Disperser Piece
    10399, // Icarus Spirit Piece
    10400, // Icarus Heavy Arms Piece
    10401, // Icarus Trident Piece
    10402, // Icarus Hammer Piece
    10403, // Icarus Hand Piece
    10404, // Icarus Hall Piece
    10405, // Icarus Spitter Piece
]

const GOLDEN_SPICE_CRATE = 15482
const CRYSTAL_SPICE_COMPRESSED_PACK = 15483

const monsterRewardChances: { [ npcId: number ]: number } = {
    18878: 0.172, // Full Grown Kookaburra
    18879: 0.334, // Full Grown Kookaburra
    18885: 0.172, // Full Grown Cougar
    18886: 0.334, // Full Grown Cougar
    18892: 0.182, // Full Grown Buffalo
    18893: 0.349, // Full Grown Buffalo
    18899: 0.182, // Full Grown Grendel
    18900: 0.349, // Full Grown Grendel
}

export class DeliciousTopChoiceMeat extends ListenerLogic {
    constructor() {
        super( 'Q00631_DeliciousTopChoiceMeat', 'listeners/tracked-600/DeliciousTopChoiceMeat.ts' )
        this.questId = 631
        this.questItemIds = [ TOP_QUALITY_MEAT, PRIME_MEAT ]
    }

    getQuestStartIds(): Array<number> {
        return [ TUNATUN ]
    }

    getTalkIds(): Array<number> {
        return [ TUNATUN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00631_DeliciousTopChoiceMeat'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'quest_accept':
                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    return this.getPath( '31537-02.html' )
                }

                return this.getPath( '31537-03.html' )

            case '31537-06.html':
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, PRIME_MEAT ) >= requiredAmount ) {
                    await this.rewardPlayer( player )
                    await state.exitQuest( true, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async rewardPlayer( player: L2PcInstance ): Promise<void> {
        switch ( _.random( 9 ) ) {
            case 0:
                return QuestHelper.rewardSingleItem( player, _.sample( recipeIds ), 1 )

            case 1:
                return QuestHelper.rewardSingleItem( player, _.sample( recipeIds ), 1 )

            case 2:
                return QuestHelper.rewardSingleItem( player, _.sample( recipeIds ), 2 )

            case 3:
                return QuestHelper.rewardSingleItem( player, _.sample( recipeIds ), 3 )

            case 4:
                return QuestHelper.rewardSingleItem( player, _.sample( recipeIds ), _.random( 4 ) + 2 )

            case 5:
                return QuestHelper.rewardSingleItem( player, _.sample( recipeIds ), _.random( 6 ) + 2 )

            case 6:
                return QuestHelper.rewardSingleItem( player, GOLDEN_SPICE_CRATE, 1 )

            case 7:
                return QuestHelper.rewardSingleItem( player, GOLDEN_SPICE_CRATE, 2 )

            case 8:
                return QuestHelper.rewardSingleItem( player, CRYSTAL_SPICE_COMPRESSED_PACK, 1 )

            case 9:
                return QuestHelper.rewardSingleItem( player, CRYSTAL_SPICE_COMPRESSED_PACK, 2 )
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( PRIME_MEAT, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 3, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardAndProgressState( player, state, PRIME_MEAT, 1, requiredAmount, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( '31537-01.htm' )

            case QuestStateValues.STARTED:
                let hasLessItems: boolean = QuestHelper.getQuestItemsCount( player, PRIME_MEAT ) < requiredAmount

                switch ( state.getCondition() ) {
                    case 1:
                        if ( hasLessItems ) {
                            return this.getPath( '31537-04.html' )
                        }

                        break

                    case 2:
                        if ( !hasLessItems ) {
                            return this.getPath( '31537-05.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}