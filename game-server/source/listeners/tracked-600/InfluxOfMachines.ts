import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const monsterRewardChances: { [ npcId: number ]: number } = {
    22801: 0.280, // Cruel Pincer Golem
    22802: 0.227, // Cruel Pincer Golem
    22803: 0.286, // Cruel Pincer Golem
    22804: 0.288, // Horrifying Jackhammer Golem
    22805: 0.235, // Horrifying Jackhammer Golem
    22806: 0.295, // Horrifying Jackhammer Golem
    22807: 0.273, // Scout-type Golem No. 28
    22808: 0.143, // Scout-type Golem No. 2
    22809: 0.629, // Guard Golem
    22810: 0.465, // Micro Scout Golem
    22811: 0.849, // Great Chaos Golem
    22812: 0.463, // Boom Golem
}

const rewardItemIds: Array<number> = [
    6881, // Recipe: Forgotten Blade (60%)
    6883, // Recipe: Basalt Battlehammer (60%)
    6885, // Recipe: Imperial Staff (60%)
    6887, // Recipe: Angel Slayer (60%)
    6891, // Recipe: Dragon Hunter Axe (60%)
    6893, // Recipe: Saint Spear (60%)
    6895, // Recipe: Demon Splinter (60%)
    6897, // Recipe: Heavens Divider (60%)
    6899, // Recipe: Arcana Mace (60%)
    7580, // Recipe: Draconic Bow (60%)
]

const minimumLevel = 70
const requiredAmount = 500
const BROKEN_GOLEM_FRAGMENT = 15521
const GUTENHAGEN = 32069

export class InfluxOfMachines extends ListenerLogic {
    constructor() {
        super( 'Q00647_InfluxOfMachines', 'listeners/tracked-600/InfluxOfMachines.ts' )
        this.questId = 647
        this.questItemIds = [ BROKEN_GOLEM_FRAGMENT ]
    }

    getQuestStartIds(): Array<number> {
        return [ GUTENHAGEN ]
    }

    getTalkIds(): Array<number> {
        return [ GUTENHAGEN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32069-03.htm':
                state.startQuest()
                break

            case '32069-06.html':
                let player = L2World.getPlayer( data.playerId )
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, BROKEN_GOLEM_FRAGMENT ) >= requiredAmount ) {
                    await QuestHelper.rewardSingleItem( player, _.sample( rewardItemIds ), 1 )
                    await state.exitQuest( true, true )

                    break
                }

                return this.getPath( '32069-07.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( BROKEN_GOLEM_FRAGMENT, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberStateForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !state ) {
            return
        }

        await QuestHelper.rewardAndProgressState( state.getPlayer(), state, BROKEN_GOLEM_FRAGMENT, 1, requiredAmount, data.isChampion, 2 )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00647_InfluxOfMachines'
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32069-01.htm' : '32069-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32069-04.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, BROKEN_GOLEM_FRAGMENT ) >= requiredAmount ) {
                            return this.getPath( '32069-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}