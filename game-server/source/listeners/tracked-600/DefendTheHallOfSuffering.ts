import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestHelper } from '../helpers/QuestHelper'
import { InstanceManager } from '../../gameService/instancemanager/InstanceManager'
import { InstanceWorld } from '../../gameService/models/instancezone/InstanceWorld'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { Instance } from '../../gameService/models/entity/Instance'
import _ from 'lodash'
import aigle from 'aigle'
import moment from 'moment'

const TEPIOS = 32603
const TEPIOS2 = 32530
const minimumLevel = 75
const maximumLevel = 82
const SOE = 736
const worldTempateId = 116

// TODO : common variables for all Hall of Suffering operations, refactor into separate location
const variableNames = {
    rewardId: 'reward',
}

const rewardIdToPath: { [ itemId: number ]: string } = {
    13777: '32530-00.html',
    13778: '32530-01.html',
    13779: '32530-02.html',
    13780: '32530-03.html',
    13781: '32530-04.html',
    13782: '32530-05.html',
    13783: '32530-06.html',
    13784: '32530-07.html',
    13785: '32530-08.html',
    13786: '32530-09.html',
}

export class DefendTheHallOfSuffering extends ListenerLogic {
    constructor() {
        super( 'Q00695_DefendTheHallOfSuffering', 'listeners/tracked-600/DefendTheHallOfSuffering.ts' )
        this.questId = 695
    }

    getQuestStartIds(): Array<number> {
        return [ TEPIOS ]
    }

    getTalkIds(): Array<number> {
        return [ TEPIOS, TEPIOS2 ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32603-02.html':
                state.startQuest( false )

                PacketDispatcher.sendCopyData( data.playerId, SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let playerLevel = player.getLevel()
                if ( playerLevel < minimumLevel ) {
                    return this.getPath( '32603-04.htm' )
                }

                if ( playerLevel <= maximumLevel ) {
                    return this.getPath( '32603-01.htm' )
                }

                if ( player.getLevel() >= minimumLevel && player.getLevel() <= maximumLevel ) {
                    if ( this.getCurrentStage() === 4 ) {
                        return this.getPath( '32603-01.htm' )
                    }


                    return this.getPath( '32603-04.htm' )
                }

                await state.exitQuest( true )
                return this.getPath( '32603-00.html' )


            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case TEPIOS:
                        return this.getPath( '32603-01a.html' )

                    case TEPIOS2:
                        let world: InstanceWorld = InstanceManager.getPlayerWorld( data.playerId )
                        if ( !world || world.getTemplateId() !== worldTempateId ) {
                            return this.getPath( '32530-11.html' )
                        }

                        let value = world.getVariable( variableNames.rewardId ) as number
                        let path: string = rewardIdToPath[ value ]
                        if ( !path ) {
                            return this.getPath( '32530-11.html' )
                        }

                        let party = player.getParty()
                        if ( !party || party.getLeaderObjectId() !== data.playerId ) {
                            return this.getPath( '32530-10.html' )
                        }

                        let questName: string = this.getName()

                        await aigle.resolve( party.getMembers() ).each( async ( playerId: number ) => {
                            let currentState: QuestState = QuestStateCache.getQuestState( playerId, questName, false )
                            if ( !currentState ) {
                                return
                            }

                            let currentPlayer = L2World.getPlayer( playerId )
                            await QuestHelper.rewardMultipleItems( currentPlayer, 1, value, SOE )
                            return currentState.exitQuest( true, true )
                        } )

                        await this.finishInstance( world )
                        return this.getPath( path )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'overrides/html/quests/Q00695_DefendTheHallOfSuffering'
    }

    getCurrentStage(): number {
        // TODO : Add stage information for Seed of Infinity progress
        return 4
    }

    async finishInstance( world: InstanceWorld ): Promise<void> {
        let message: Buffer = new SystemMessageBuilder( SystemMessageIds.INSTANT_ZONE_FROM_HERE_S1_S_ENTRY_HAS_BEEN_RESTRICTED )
                .addInstanceName( worldTempateId )
                .getBuffer()

        let futureTime: number = moment()
                .add( 1, 'day' )
                .hours( 6 )
                .minutes( 30 )
                .valueOf()

        let promises: Array<Promise<void>> = _.map( Array.from( world.allowedObjectIds ), ( playerId: number ) => {
            PacketDispatcher.sendCopyData( playerId, message )

            return InstanceManager.setInstanceTime( playerId, worldTempateId, futureTime )
        } )

        await Promise.all( promises )

        let instance: Instance = InstanceManager.getInstance( world.getInstanceId() )
        instance.setDuration( 5 * 60000 )
        instance.emptyDestroyTime = 0
    }
}