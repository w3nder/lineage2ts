import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const KADUN = 31370
const HORN = 7186
const MANE = 7233
const minimumLevel = 74
const MANE_COUNT = 100

const monsterRewardChances: { [ npcId: number ]: number } = {
    21350: 0.500, // Varka Silenos Recruit
    21353: 0.510, // Varka Silenos Scout
    21354: 0.522, // Varka Silenos Hunter
    21355: 0.519, // Varka Silenos Shaman
    21357: 0.529, // Varka Silenos Priest
    21358: 0.529, // Varka Silenos Warrior
    21360: 0.539, // Varka Silenos Medium
    21362: 0.539, // Varka Silenos Officer
    21364: 0.558, // Varka Silenos Seer
    21365: 0.568, // Varka Silenos Great Magus
    21366: 0.568, // Varka Silenos General
    21368: 0.568, // Varka Silenos Great Seer
    21369: 0.664, // Varka's Commander
    21371: 0.713, // Varka's Head Magus
    21373: 0.738, // Varka's Prophet
}

export class BattleAgainstVarkaSilenos extends ListenerLogic {
    constructor() {
        super( 'Q00606_BattleAgainstVarkaSilenos', 'listeners/tracked-600/BattleAgainstVarkaSilenos.ts' )
        this.questId = 606
        this.questItemIds = [
            MANE,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ KADUN ]
    }

    getTalkIds(): Array<number> {
        return [ KADUN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31370-03.htm':
                state.startQuest()
                break

            case '31370-06.html':
                break

            case '31370-07.html':
                if ( QuestHelper.getQuestItemsCount( player, MANE ) < MANE_COUNT ) {
                    return this.getPath( '31370-08.html' )
                }

                await QuestHelper.takeSingleItem( player, MANE, MANE_COUNT )
                await QuestHelper.giveSingleItem( player, HORN, 20 )

                break

            case '31370-09.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( MANE, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, MANE, 1, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31370-01.htm' : '31370-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItem( player, MANE ) ? '31370-04.html' : '31370-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00606_BattleAgainstVarkaSilenos'
    }
}