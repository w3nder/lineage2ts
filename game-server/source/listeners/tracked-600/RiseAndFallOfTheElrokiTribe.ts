import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const SINGSING = 32106
const KARAKAWEI = 32117
const BONES_OF_A_PLAINS_DINOSAUR = 8776
const minimumLevel = 75
const CHANCE_MOBS1 = 116
const CHANCE_MOBS2 = 360
const CHANCE_DEINO = 558
const exchangeAmount = 300

const playerRewardItemIds: Array<number> = [
    8712, // Sirra's Blade Edge
    8713, // Sword of Ipos Blade
    8714, // Barakiel's Axe Piece
    8715, // Behemoth's Tuning Fork Piece
    8716, // Naga Storm Piece
    8717, // Tiphon's Spear Edge
    8718, // Shyeed's Bow Shaft
    8719, // Sobekk's Hurricane Edge
    8720, // Themis' Tongue Piece
    8721, // Cabrio's Hand Head
    8722, // Daimon Crystal Fragment
]

const monsterRewardChances = {
    22200: CHANCE_MOBS1, // Ornithomimus
    22201: CHANCE_MOBS1, // Ornithomimus
    22202: CHANCE_MOBS1, // Ornithomimus
    22204: CHANCE_MOBS1, // Deinonychus
    22205: CHANCE_MOBS1, // Deinonychus
    22208: CHANCE_MOBS1, // Pachycephalosaurus
    22209: CHANCE_MOBS1, // Pachycephalosaurus
    22210: CHANCE_MOBS1, // Pachycephalosaurus
    22211: CHANCE_MOBS1, // Wild Strider
    22212: CHANCE_MOBS1, // Wild Strider
    22213: CHANCE_MOBS1, // Wild Strider
    22219: CHANCE_MOBS1, // Ornithomimus
    22220: CHANCE_MOBS1, // Deinonychus
    22221: CHANCE_MOBS1, // Pachycephalosaurus
    22222: CHANCE_MOBS1, // Wild Strider
    22224: CHANCE_MOBS1, // Ornithomimus
    22225: CHANCE_MOBS1, // Deinonychus
    22226: CHANCE_MOBS1, // Pachycephalosaurus
    22227: CHANCE_MOBS1, // Wild Strider
    22742: CHANCE_MOBS2, // Ornithomimus
    22743: CHANCE_MOBS2, // Deinonychus
    22744: CHANCE_MOBS2, // Ornithomimus
    22745: CHANCE_MOBS2, // Deinonychus
    22203: CHANCE_DEINO, // Deinonychus
}

const constantRewardMonsterIds = new Set<number>(
        [
            22200, // Ornithomimus
            22201, // Ornithomimus
            22202, // Ornithomimus
            22204, // Deinonychus
            22205, // Deinonychus
            22208, // Pachycephalosaurus
            22209, // Pachycephalosaurus
            22210, // Pachycephalosaurus
            22211, // Wild Strider
            22212, // Wild Strider
            22213, // Wild Strider
            22219, // Ornithomimus
            22220, // Deinonychus
            22221, // Pachycephalosaurus
            22222, // Wild Strider
            22224, // Ornithomimus
            22225, // Deinonychus
            22226, // Pachycephalosaurus
            22227, // Wild Strider
        ],
)

const variableNames = {
    haveTalked: 'ht',
}

export class RiseAndFallOfTheElrokiTribe extends ListenerLogic {
    constructor() {
        super( 'Q00643_RiseAndFallOfTheElrokiTribe', 'listeners/tracked-600/RiseAndFallOfTheElrokiTribe.ts' )
        this.questId = 643
        this.questItemIds = [ BONES_OF_A_PLAINS_DINOSAUR ]
    }

    getQuestStartIds(): Array<number> {
        return [ SINGSING ]
    }

    getTalkIds(): Array<number> {
        return [ SINGSING, KARAKAWEI ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32106-02.htm':
            case '32106-04.htm':
            case '32106-05.html':
            case '32106-10.html':
            case '32106-13.html':
            case '32117-02.html':
            case '32117-06.html':
            case '32117-07.html':
                break

            case 'quest_accept':
                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()

                    return this.getPath( '32106-03.html' )
                }

                return this.getPath( '32106-07.html' )

            case '32106-09.html':
                let boneAmount = QuestHelper.getQuestItemsCount( player, BONES_OF_A_PLAINS_DINOSAUR )
                if ( boneAmount > 0 ) {
                    await QuestHelper.takeSingleItem( player, BONES_OF_A_PLAINS_DINOSAUR, -1 )
                    await QuestHelper.giveAdena( player, 1374 * boneAmount, true )
                }

                break

            case 'exit':
                let amount = QuestHelper.getQuestItemsCount( player, BONES_OF_A_PLAINS_DINOSAUR )
                if ( amount > 0 ) {
                    await QuestHelper.giveAdena( player, 1374 * amount, true )
                    await state.exitQuest( true, true )

                    return this.getPath( '32106-12.html' )
                }

                await state.exitQuest( true, true )
                return this.getPath( '32106-11.html' )

            case 'exchange':
                if ( QuestHelper.getQuestItemsCount( player, BONES_OF_A_PLAINS_DINOSAUR ) < exchangeAmount ) {
                    return this.getPath( '32117-04.html' )
                }

                await QuestHelper.rewardSingleItem( player, _.sample( playerRewardItemIds ), 5 )
                await QuestHelper.takeSingleItem( player, BONES_OF_A_PLAINS_DINOSAUR, exchangeAmount )

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                return this.getPath( '32117-05.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( !constantRewardMonsterIds.has( data.npcId )
                && Math.random() > QuestHelper.getAdjustedChance( BONES_OF_A_PLAINS_DINOSAUR, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        let amount: number = constantRewardMonsterIds.has( data.npcId ) && Math.random() < QuestHelper.getAdjustedChance( BONES_OF_A_PLAINS_DINOSAUR, monsterRewardChances[ data.npcId ], data.isChampion ) ? 2 : 1
        await QuestHelper.rewardSingleQuestItem( player, BONES_OF_A_PLAINS_DINOSAUR, amount, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32106-01.htm' : '32106-06.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case SINGSING:
                        return this.getPath( QuestHelper.hasQuestItem( player, BONES_OF_A_PLAINS_DINOSAUR ) ? '32106-08.html' : '32106-14.html' )

                    case KARAKAWEI:
                        if ( !state.getVariable( variableNames.haveTalked ) ) {
                            state.setVariable( variableNames.haveTalked, true )
                            return this.getPath( '32117-01.html' )
                        }

                        return this.getPath( '32117-03.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00643_RiseAndFallOfTheElrokiTribe'
    }
}