import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const JEREMY = 31521
const TRUNK_OF_NEPENTHES = 7202
const FOOT_OF_BANDERSNATCHLING = 7203
const SECRET_SPICE = 7204
const ICE_CRYSTAL = 7080
const SOY_SAUCE_JAR = 7205
const minimumLevel = 73

const monsterRewards: { [ npcId: number ]: number } = {
    21314: FOOT_OF_BANDERSNATCHLING, // Hot Springs Bandersnatchling
    21317: SECRET_SPICE, // Hot Springs Atroxspawn
    21319: TRUNK_OF_NEPENTHES, // Hot Springs Nepenthes
    21321: SECRET_SPICE, // Hot Springs Atrox
}

export class TheFinestIngredientsPartOne extends ListenerLogic {
    constructor() {
        super( 'Q00624_TheFinestIngredientsPart1', 'listeners/tracked-600/TheFinestIngredientsPartOne.ts' )
        this.questId = 624
        this.questItemIds = [
            TRUNK_OF_NEPENTHES,
            FOOT_OF_BANDERSNATCHLING,
            SECRET_SPICE,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ JEREMY ]
    }

    getTalkIds(): Array<number> {
        return [ JEREMY ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00624_TheFinestIngredientsPart1'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31521-02.htm':
                state.startQuest()
                break

            case '31521-05.html':
                if ( state.isCondition( 2 ) && QuestHelper.getItemsSumCount( player, ...this.questItemIds ) >= 150 ) {
                    await QuestHelper.giveMultipleItems( player, 1, ICE_CRYSTAL, SOY_SAUCE_JAR )
                    await state.exitQuest( true, true )

                    return this.getPath( '31521-05.html' )
                }

                return this.getPath( '31521-06.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let itemId: number = monsterRewards[ data.npcId ]
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= 50 ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        let amount: number = QuestHelper.getQuestItemsCount( player, itemId )
        if ( amount >= 50 ) {
            if ( QuestHelper.getItemsSumCount( player, ...this.questItemIds ) >= 150 ) {
                let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
                state.setConditionWithSound( 2, true )

                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_FANFARE_MIDDLE )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31521-01.htm' : '31521-00.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '31521-03.html' )

                    case 2:
                        return this.getPath( '31521-04.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}