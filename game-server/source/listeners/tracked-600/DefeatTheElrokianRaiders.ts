import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

const ELROKI = 22214
const DINN = 32105
const DINOSAUR_FANG_NECKLACE = 8785
const minimumLevel = 75
const DROP_RATE = 448
const requiredAmount = 100

export class DefeatTheElrokianRaiders extends ListenerLogic {
    constructor() {
        super( 'Q00688_DefeatTheElrokianRaiders', 'listeners/tracked-600/DefeatTheElrokianRaiders.ts' )
        this.questId = 688
        this.questItemIds = [ DINOSAUR_FANG_NECKLACE ]
    }

    getQuestStartIds(): Array<number> {
        return [ DINN ]
    }

    getTalkIds(): Array<number> {
        return [ DINN ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ELROKI ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00688_DefeatTheElrokianRaiders'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32105-02.htm':
            case '32105-10.html':
                break

            case '32105-03.html':
                state.startQuest()
                break

            case '32105-06.html':
                let fangAmount: number = QuestHelper.getQuestItemsCount( player, DINOSAUR_FANG_NECKLACE )
                if ( fangAmount > 0 ) {
                    await QuestHelper.takeSingleItem( player, DINOSAUR_FANG_NECKLACE, -1 )
                    await QuestHelper.giveAdena( player, 3000 * fangAmount, true )

                    break
                }

                return

            case 'donation':
                if ( QuestHelper.getQuestItemsCount( player, DINOSAUR_FANG_NECKLACE ) < requiredAmount ) {
                    return this.getPath( '32105-07.html' )
                }

                await QuestHelper.takeSingleItem( player, DINOSAUR_FANG_NECKLACE, requiredAmount )

                if ( Math.random() > 0.5 ) {
                    await QuestHelper.giveAdena( player, 450000, true )
                    return this.getPath( '32105-08.html' )
                }

                await QuestHelper.giveAdena( player, 150000, true )
                return this.getPath( '32105-09.html' )

            case '32105-11.html':
                let amount: number = QuestHelper.getQuestItemsCount( player, DINOSAUR_FANG_NECKLACE )
                if ( amount > 0 ) {
                    await QuestHelper.giveAdena( player, 3000 * amount, true )
                }

                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32105-01.htm' : '32105-04.html' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItem( player, DINOSAUR_FANG_NECKLACE ) ? '32105-05.html' : '32105-12.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( DINOSAUR_FANG_NECKLACE, DROP_RATE, data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, DINOSAUR_FANG_NECKLACE, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}