import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

import _ from 'lodash'

const WAHKAN = 31371
const VARKA_BADGE_SOLDIER = 7216
const VARKA_BADGE_OFFICER = 7217
const VARKA_BADGE_CAPTAIN = 7218
const VALOR_TOTEM = 7219
const WISDOM_TOTEM = 7220
const minimumLevel = 74

type MonsterData = [ number, number, number ] // chance, required condition, itemId
const monsterDropData: { [ npcId: number ]: MonsterData } = {
    21350: [ 0.500, 1, VARKA_BADGE_SOLDIER ], // Varka Silenos Recruit
    21351: [ 0.500, 1, VARKA_BADGE_SOLDIER ], // Varka Silenos Footman
    21353: [ 0.509, 1, VARKA_BADGE_SOLDIER ], // Varka Silenos Scout
    21354: [ 0.521, 1, VARKA_BADGE_SOLDIER ], // Varka Silenos Hunter
    21355: [ 0.519, 1, VARKA_BADGE_SOLDIER ], // Varka Silenos Shaman
    21357: [ 0.500, 2, VARKA_BADGE_OFFICER ], // Varka Silenos Priest
    21358: [ 0.500, 2, VARKA_BADGE_OFFICER ], // Varka Silenos Warrior
    21360: [ 0.509, 2, VARKA_BADGE_OFFICER ], // Varka Silenos Medium
    21361: [ 0.518, 2, VARKA_BADGE_OFFICER ], // Varka Silenos Magus
    21362: [ 0.518, 2, VARKA_BADGE_OFFICER ], // Varka Silenos Officer
    21364: [ 0.527, 2, VARKA_BADGE_OFFICER ], // Varka Silenos Seer
    21365: [ 0.500, 3, VARKA_BADGE_CAPTAIN ], // Varka Silenos Great Magus
    21366: [ 0.500, 3, VARKA_BADGE_CAPTAIN ], // Varka Silenos General
    21368: [ 0.508, 3, VARKA_BADGE_CAPTAIN ], // Varka Silenos Great Seer
    21369: [ 0.628, 2, VARKA_BADGE_OFFICER ], // Varka's Commander
    21370: [ 0.604, 2, VARKA_BADGE_OFFICER ], // Varka's Elite Guard
    21371: [ 0.627, 3, VARKA_BADGE_CAPTAIN ], // Varka's Head Magus
    21372: [ 0.604, 3, VARKA_BADGE_CAPTAIN ], // Varka's Head Guard
    21373: [ 0.649, 3, VARKA_BADGE_CAPTAIN ], // Varka's Prophet
    21374: [ 0.626, 3, VARKA_BADGE_CAPTAIN ], // Prophet's Guard
    21375: [ 0.626, 3, VARKA_BADGE_CAPTAIN ], // Disciple of Prophet
}

const ketraMarks: Array<number> = [
    7211, // Mark of Ketra's Alliance - Level 1
    7212, // Mark of Ketra's Alliance - Level 2
    7213, // Mark of Ketra's Alliance - Level 3
    7214, // Mark of Ketra's Alliance - Level 4
    7215, // Mark of Ketra's Alliance - Level 5
]

const varkaMarks: Array<number> = [
    7221, // Mark of Varka's Alliance - Level 1
    7222, // Mark of Varka's Alliance - Level 2
    7223, // Mark of Varka's Alliance - Level 3
    7224, // Mark of Varka's Alliance - Level 4
    7225, // Mark of Varka's Alliance - Level 5
]

const soldierBadgeCounts: Array<number> = [
    100, // cond 1
    200, // cond 2
    300, // cond 3
    300, // cond 4
    400, // cond 5
]

const officerBadgeCounts: Array<number> = [
    0, // cond 1
    100, // cond 2
    200, // cond 3
    300, // cond 4
    400, // cond 5
]

const captainBadgeCounts: Array<number> = [
    0, // cond 1
    0, // cond 2
    100, // cond 3
    200, // cond 4
    200, // cond 5
]

const itemCountMapping: { [ itemId: number ]: Array<number> } = {
    [ VARKA_BADGE_SOLDIER ]: soldierBadgeCounts,
    [ VARKA_BADGE_OFFICER ]: officerBadgeCounts,
    [ VARKA_BADGE_CAPTAIN ]: captainBadgeCounts,
}

export class AllianceWithKetraOrcs extends ListenerLogic {
    constructor() {
        super( 'Q00605_AllianceWithKetraOrcs', 'listeners/tracked-600/AllianceWithKetraOrcs.ts' )
        this.questId = 605
        this.questItemIds = [
            VARKA_BADGE_SOLDIER,
            VARKA_BADGE_OFFICER,
            VARKA_BADGE_CAPTAIN,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ WAHKAN ]
    }

    getTalkIds(): Array<number> {
        return [ WAHKAN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterDropData ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31371-12a.html':
            case '31371-12b.html':
            case '31371-25.html':
                break

            case '31371-04.htm':
                if ( QuestHelper.hasAtLeastOneQuestItem( player, ...varkaMarks ) ) {
                    return this.getPath( '31371-03.htm' )
                }

                state.startQuest()

                let index: number = _.findIndex( ketraMarks, ( itemId: number ): boolean => {
                    return QuestHelper.hasQuestItem( player, itemId )
                } )

                if ( index !== -1 ) {
                    state.setConditionWithSound( index + 2 )
                    return this.getPath( `31371-0${ index + 5 }.htm` )
                }

                state.setConditionWithSound( 1 )
                break

            case '31371-12.html':
                if ( QuestHelper.getQuestItemsCount( player, VARKA_BADGE_SOLDIER ) < soldierBadgeCounts[ 0 ] ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeSingleItem( player, VARKA_BADGE_SOLDIER, -1 )
                await QuestHelper.giveSingleItem( player, ketraMarks[ 0 ], 1 )

                state.setConditionWithSound( 2, true )
                break

            case '31371-15.html':
                if ( QuestHelper.getQuestItemsCount( player, VARKA_BADGE_SOLDIER ) < soldierBadgeCounts[ 1 ]
                        || QuestHelper.getQuestItemsCount( player, VARKA_BADGE_OFFICER ) < officerBadgeCounts[ 1 ] ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeMultipleItems( player, -1, VARKA_BADGE_SOLDIER, VARKA_BADGE_OFFICER, ketraMarks[ 0 ] )
                await QuestHelper.giveSingleItem( player, ketraMarks[ 1 ], 1 )

                state.setConditionWithSound( 3, true )
                break

            case '31371-18.html':
                if ( QuestHelper.getQuestItemsCount( player, VARKA_BADGE_SOLDIER ) < soldierBadgeCounts[ 2 ]
                        || QuestHelper.getQuestItemsCount( player, VARKA_BADGE_OFFICER ) < officerBadgeCounts[ 2 ]
                        || QuestHelper.getQuestItemsCount( player, VARKA_BADGE_CAPTAIN ) < captainBadgeCounts[ 2 ] ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeMultipleItems( player, -1, VARKA_BADGE_SOLDIER, VARKA_BADGE_OFFICER, VARKA_BADGE_CAPTAIN, ketraMarks[ 1 ] )
                await QuestHelper.giveSingleItem( player, ketraMarks[ 2 ], 1 )

                state.setConditionWithSound( 4, true )
                break

            case '31371-21.html':
                if ( !QuestHelper.hasQuestItem( player, VALOR_TOTEM )
                        || QuestHelper.getQuestItemsCount( player, VARKA_BADGE_SOLDIER ) < soldierBadgeCounts[ 3 ]
                        || QuestHelper.getQuestItemsCount( player, VARKA_BADGE_OFFICER ) < officerBadgeCounts[ 3 ]
                        || QuestHelper.getQuestItemsCount( player, VARKA_BADGE_CAPTAIN ) < captainBadgeCounts[ 3 ] ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeMultipleItems( player, -1, VARKA_BADGE_SOLDIER, VARKA_BADGE_OFFICER, VARKA_BADGE_CAPTAIN, VALOR_TOTEM, ketraMarks[ 2 ] )
                await QuestHelper.giveSingleItem( player, ketraMarks[ 3 ], 1 )

                state.setConditionWithSound( 5, true )
                break

            case '31371-26.html':
                await QuestHelper.takeMultipleItems( player, -1, VALOR_TOTEM, WISDOM_TOTEM, ...ketraMarks )
                await state.exitQuest( true, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00605_AllianceWithKetraOrcs'
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ chance, minimumConditionRequired, itemId ] = monsterDropData[ data.npcId ]
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerStateType( L2World.getPlayer( data.playerId ), QuestStateValues.STARTED )

        if ( !player ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )

        if ( state.getCondition() < minimumConditionRequired || !this.canReceiveItem( player, state, itemId ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
    }

    canReceiveItem( player: L2PcInstance, state: QuestState, itemId: number ): boolean {
        let itemAmounts: Array<number> = itemCountMapping[ itemId ]
        if ( !itemAmounts ) {
            return false
        }

        return QuestHelper.getQuestItemsCount( player, itemId ) < itemAmounts[ state.getCondition() - 1 ]
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31371-01.htm' : '31371-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( this.hasMatchingBadges( player, 0 ) ? '31371-11.html' : '31371-10.html' )

                    case 2:
                        return this.getPath( QuestHelper.hasQuestItem( player, ketraMarks[ 0 ] ) && this.hasMatchingBadges( player, 1 ) ? '31371-14.html' : '31371-13.html' )

                    case 3:
                        return this.getPath( QuestHelper.hasQuestItem( player, ketraMarks[ 1 ] ) && this.hasMatchingBadges( player, 2 ) ? '31371-17.html' : '31371-16.html' )

                    case 4:
                        return this.getPath( QuestHelper.hasQuestItems( player, ketraMarks[ 2 ], VALOR_TOTEM ) && this.hasMatchingBadges( player, 3 ) ? '31371-20.html' : '31371-19.html' )

                    case 5:
                        if ( !QuestHelper.hasQuestItem( player, ketraMarks[ 3 ] )
                                || !QuestHelper.hasQuestItem( player, WISDOM_TOTEM )
                                || QuestHelper.getQuestItemsCount( player, VARKA_BADGE_SOLDIER ) < soldierBadgeCounts[ 4 ]
                                || QuestHelper.getQuestItemsCount( player, VARKA_BADGE_OFFICER ) < officerBadgeCounts[ 4 ]
                                || QuestHelper.getQuestItemsCount( player, VARKA_BADGE_CAPTAIN ) < captainBadgeCounts[ 4 ] ) {
                            return this.getPath( '31371-22.html' )
                        }

                        state.setConditionWithSound( 6, true )

                        await QuestHelper.takeMultipleItems( player, -1, VARKA_BADGE_SOLDIER, VARKA_BADGE_OFFICER, VARKA_BADGE_CAPTAIN, WISDOM_TOTEM, ketraMarks[ 3 ] )
                        await QuestHelper.giveSingleItem( player, ketraMarks[ 4 ], 1 )

                        return this.getPath( '31371-23.html' )

                    case 6:
                        if ( QuestHelper.hasQuestItems( player, ketraMarks[ 4 ] ) ) {
                            return this.getPath( '31371-24.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    hasMatchingBadges( player: L2PcInstance, index: number ): boolean {
        switch ( index ) {
            case 0:
                return QuestHelper.getQuestItemsCount( player, VARKA_BADGE_SOLDIER ) >= soldierBadgeCounts[ index ]

            case 1:
                return QuestHelper.getQuestItemsCount( player, VARKA_BADGE_SOLDIER ) >= soldierBadgeCounts[ index ]
                        && QuestHelper.getQuestItemsCount( player, VARKA_BADGE_OFFICER ) >= officerBadgeCounts[ index ]

            case 2:
            case 3:
            case 4:
                return QuestHelper.getQuestItemsCount( player, VARKA_BADGE_SOLDIER ) >= soldierBadgeCounts[ index ]
                        && QuestHelper.getQuestItemsCount( player, VARKA_BADGE_OFFICER ) >= officerBadgeCounts[ index ]
                        && QuestHelper.getQuestItemsCount( player, VARKA_BADGE_CAPTAIN ) >= captainBadgeCounts[ index ]

        }

        return false
    }
}