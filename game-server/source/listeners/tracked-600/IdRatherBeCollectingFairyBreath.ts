import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const GALATEA = 30634
const FAIRY_BREATH = 8286
const minimumLevel = 26

const monsterRewardChances: { [ npcId: number ]: number } = {
    20078: 0.98, // whispering_wind
    21023: 0.82, // sobing_wind
    21024: 0.86, // babbleing_wind
    21025: 0.90, // giggleing_wind
    21026: 0.96, // singing_wind
}

export class IdRatherBeCollectingFairyBreath extends ListenerLogic {
    constructor() {
        super( 'Q00659_IdRatherBeCollectingFairyBreath', 'listeners/tracked-600/IdRatherBeCollectingFairyBreath.ts' )
        this.questId = 659
        this.questItemIds = [ FAIRY_BREATH ]
    }

    getQuestStartIds(): Array<number> {
        return [ GALATEA ]
    }

    getTalkIds(): Array<number> {
        return [ GALATEA ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30634-02.htm':
                state.startQuest()
                break

            case 'REWARD':
                let amount: number = QuestHelper.getQuestItemsCount( player, FAIRY_BREATH )
                if ( amount > 0 ) {
                    let adenaBonus = ( ( amount >= 10 ) ? 5365 : 0 )
                    await QuestHelper.takeSingleItem( player, FAIRY_BREATH, -1 )
                    await QuestHelper.giveAdena( player, ( amount * 50 ) + adenaBonus, true )

                    return this.getPath( '30634-05.html' )
                }

                return this.getPath( '30634-06.html' )

            case '30634-07.html':
                break

            case '30634-08.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( FAIRY_BREATH, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, FAIRY_BREATH, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30634-01.htm' : '30634-03.html' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItem( player, FAIRY_BREATH ) ? '30634-04.html' : '30634-09.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00659_IdRatherBeCollectingFairyBreath'
    }
}