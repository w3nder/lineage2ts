import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { DataManager } from '../../data/manager'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const KLUMP = 30845
const RED_GEM = 8765
const ZIGGOS_GEMSTONE = 8868
const minimumLevel = 61
const requiredAmount = 50

const variableNames = {
    firstValue: 'one',
    secondValue: 'two',
    thirdValue: 'three',
    fourthValue: 'four',
    fifthValue: 'five',
}

const allVariableNames: Array<string> = _.values( variableNames )

const responseSymbols: { [ code: number ]: string } = {
    1: '!',
    2: '=',
    3: 'T',
    4: 'V',
    5: 'O',
    6: 'P',
    7: 'S',
    8: 'E',
    9: 'H',
    10: 'A',
    11: 'R',
    12: 'D',
    13: 'I',
    14: 'N',
}

const monsterRewardChances: { [ npcId: number ]: number } = {
    20672: 0.357, // Trives
    20673: 0.357, // Falibati
    20674: 0.583, // Doom Knight
    20677: 0.435, // Tulben
    20955: 0.358, // Ghostly Warrior
    20958: 0.283, // Death Agent
    20959: 0.455, // Dark Guard
    20961: 0.365, // Bloody Knight
    20962: 0.348, // Bloody Priest
    20965: 0.457, // Chimera Piece
    20966: 0.493, // Changed Creation
    20968: 0.418, // Nonexistent Man
    20972: 0.350, // Shaman of Ancient Times
    20973: 0.453, // Forgotten Ancient People
    21002: 0.315, // Doom Scout
    21004: 0.320, // Dismal Pole
    21006: 0.335, // Doom Servant
    21008: 0.462, // Doom Archer
    21010: 0.397, // Doom Warrior
    21109: 0.507, // Hames Orc Scout
    21112: 0.552, // Hames Orc Footman
    21114: 0.587, // Cursed Guardian
    21116: 0.812, // Hames Orc Overlord
    21278: 0.483, // Antelope
    21279: 0.483, // Antelope
    21280: 0.483, // Antelope
    21286: 0.515, // Buffalo
    21287: 0.515, // Buffalo
    21288: 0.515, // Buffalo
    21508: 0.493, // Splinter Stakato
    21510: 0.527, // Splinter Stakato Soldier
    21513: 0.562, // Needle Stakato
    21515: 0.598, // Needle Stakato Soldier
    21520: 0.458, // Eye of Splendor
    21526: 0.552, // Wisdom of Splendor
    21530: 0.488, // Victory of Splendor
    21535: 0.573, // Signet of Splendor
    18001: 0.232, // Blood Queen
}

export class AGameOfCards extends ListenerLogic {
    constructor() {
        super( 'Q00662_AGameOfCards', 'listeners/tracked-600/AGameOfCards.ts' )
        this.questId = 662
    }

    getQuestStartIds(): Array<number> {
        return [ KLUMP ]
    }

    getTalkIds(): Array<number> {
        return [ KLUMP ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00662_AGameOfCards'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30845-03.htm':
                if ( player.getLevel() >= minimumLevel ) {
                    if ( state.isCreated() ) {
                        state.startQuest()
                    }
                    break
                }

                return

            case '30845-06.html':
            case '30845-08.html':
            case '30845-09.html':
            case '30845-09a.html':
            case '30845-09b.html':
            case '30845-10.html':
                break

            case '30845-07.html':
                await state.exitQuest( true, true )
                break

            case 'return':
                return this.getPath( QuestHelper.getQuestItemsCount( player, RED_GEM ) < requiredAmount ? '30845-04.html' : '30845-05.html' )

            case '30845-11.html':
                if ( QuestHelper.getQuestItemsCount( player, RED_GEM ) >= requiredAmount ) {
                    this.onCutCardDeck( state )
                    await QuestHelper.takeSingleItem( player, RED_GEM, requiredAmount )
                    break
                }

                return

            case 'turncard1':
            case 'turncard2':
            case 'turncard3':
            case 'turncard4':
            case 'turncard5':
                return this.onTurnCard( data.eventName, state, player )

            case 'playagain':
                return this.getPath( QuestHelper.getQuestItemsCount( player, RED_GEM ) < requiredAmount ? '30845-21.html' : '30845-20.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    onCutCardDeck( state: QuestState ): void {
        let values = new Set<number>( [ 0, 0, 0, 0, 0 ] )
        while ( values.size < 5 ) {
            values.clear()
            values
                    .add( _.random( 1, 70 ) )
                    .add( _.random( 1, 70 ) )
                    .add( _.random( 1, 70 ) )
                    .add( _.random( 1, 70 ) )
                    .add( _.random( 1, 70 ) )
        }

        let [ first, second, third, fourth, fifth ] = [ ...values ]


        if ( first >= 57 ) {
            first = first - 56
        } else if ( first >= 43 ) {
            first = first - 42
        } else if ( first >= 29 ) {
            first = first - 28
        } else if ( first >= 15 ) {
            first = first - 14
        }

        if ( second >= 57 ) {
            second = second - 56
        } else if ( second >= 43 ) {
            second = second - 42
        } else if ( second >= 29 ) {
            second = second - 28
        } else if ( second >= 15 ) {
            second = second - 14
        }

        if ( third >= 57 ) {
            third = third - 56
        } else if ( third >= 43 ) {
            third = third - 42
        } else if ( third >= 29 ) {
            third = third - 28
        } else if ( third >= 15 ) {
            third = third - 14
        }

        if ( fourth >= 57 ) {
            fourth = fourth - 56
        } else if ( fourth >= 43 ) {
            fourth = fourth - 42
        } else if ( fourth >= 29 ) {
            fourth = fourth - 28
        } else if ( fourth >= 15 ) {
            fourth = fourth - 14
        }

        if ( fifth >= 57 ) {
            fifth = fifth - 56
        } else if ( fifth >= 43 ) {
            fifth = fifth - 42
        } else if ( fifth >= 29 ) {
            fifth = fifth - 28
        } else if ( fifth >= 15 ) {
            fifth = fifth - 14
        }

        state.setVariable( variableNames.firstValue, first )
        state.setVariable( variableNames.secondValue, second )
        state.setVariable( variableNames.thirdValue, third )
        state.setVariable( variableNames.fourthValue, fourth )
        state.setVariable( variableNames.fifthValue, fifth )
    }

    async onTurnCard( eventName: string, state: QuestState, player: L2PcInstance ): Promise<string> {
        let fifthModulus: number = state.getVariable( variableNames.fifthValue ) % 100
        let fifthDivided: number = Math.floor( state.getVariable( variableNames.fifthValue ) / 100 )
        let first: number = state.getVariable( variableNames.firstValue )
        let second: number = state.getVariable( variableNames.secondValue )
        let third: number = state.getVariable( variableNames.thirdValue )
        let fourth: number = state.getVariable( variableNames.fourthValue )

        switch ( eventName ) {
            case 'turncard1':
                if ( ( fifthDivided % 2 ) < 1 ) {
                    fifthDivided = fifthDivided + 1
                }

                if ( ( fifthDivided % 32 ) < 31 ) {
                    state.setVariable( variableNames.fifthValue, ( fifthDivided * 100 ) + fifthModulus )
                }

                break

            case 'turncard2':
                if ( ( fifthDivided % 4 ) < 2 ) {
                    fifthDivided = fifthDivided + 2
                }

                if ( ( fifthDivided % 32 ) < 31 ) {
                    state.setVariable( variableNames.fifthValue, ( fifthDivided * 100 ) + fifthModulus )
                }

                break

            case 'turncard3':
                if ( ( fifthDivided % 8 ) < 4 ) {
                    fifthDivided = fifthDivided + 4
                }

                if ( ( fifthDivided % 32 ) < 31 ) {
                    state.setVariable( variableNames.fifthValue, ( fifthDivided * 100 ) + fifthModulus )
                }

                break

            case 'turncard4':
                if ( ( fifthDivided % 16 ) < 8 ) {
                    fifthDivided = fifthDivided + 8
                }

                if ( ( fifthDivided % 32 ) < 31 ) {
                    state.setVariable( variableNames.fifthValue, ( fifthDivided * 100 ) + fifthModulus )
                }

                break

            case 'turncard5':
                if ( ( fifthDivided % 32 ) < 16 ) {
                    fifthDivided = fifthDivided + 16
                }

                if ( ( fifthDivided % 32 ) < 31 ) {
                    state.setVariable( variableNames.fifthValue, ( fifthDivided * 100 ) + fifthModulus )
                }

                return

        }

        if ( ( fifthDivided % 32 ) < 31 ) {
            return this.getPath( '30845-12.html' )
        }

        if ( ( fifthDivided % 32 ) === 31 ) {
            let six = 0
            let seven = 0

            if ( first >= 1
                    && first <= 14
                    && second >= 1
                    && second <= 14
                    && third >= 1
                    && third <= 14
                    && fourth >= 1
                    && fourth <= 14
                    && fifthModulus >= 1
                    && fifthModulus <= 14 ) {
                if ( first === second ) {
                    six = six + 10
                    seven = seven + 8
                }

                if ( first === third ) {
                    six = six + 10
                    seven = seven + 4
                }

                if ( first === fourth ) {
                    six = six + 10
                    seven = seven + 2
                }

                if ( first === fifthModulus ) {
                    six = six + 10
                    seven = seven + 1
                }

                if ( ( six % 100 ) < 10 ) {
                    if ( ( seven % 16 ) < 8 ) {
                        if ( ( seven % 8 ) < 4 ) {
                            if ( second === third ) {
                                six = six + 10
                                seven = seven + 4
                            }
                        }
                        if ( ( seven % 4 ) < 2 ) {
                            if ( second === fourth ) {
                                six = six + 10
                                seven = seven + 2
                            }
                        }
                        if ( ( seven % 2 ) < 1 ) {
                            if ( second === fifthModulus ) {
                                six = six + 10
                                seven = seven + 1
                            }
                        }
                    }
                } else if ( ( six % 10 ) === 0 ) {
                    if ( ( seven % 16 ) < 8 ) {
                        if ( ( seven % 8 ) < 4 ) {
                            if ( second === third ) {
                                six = six + 1
                                seven = seven + 4
                            }
                        }
                        if ( ( seven % 4 ) < 2 ) {
                            if ( second === fourth ) {
                                six = six + 1
                                seven = seven + 2
                            }
                        }
                        if ( ( seven % 2 ) < 1 ) {
                            if ( second === fifthModulus ) {
                                six = six + 1
                                seven = seven + 1
                            }
                        }
                    }
                }

                if ( ( six % 100 ) < 10 ) {
                    if ( ( seven % 8 ) < 4 ) {
                        if ( ( seven % 4 ) < 2 ) {
                            if ( third === fourth ) {
                                six = six + 10
                                seven = seven + 2
                            }
                        }
                        if ( ( seven % 2 ) < 1 ) {
                            if ( third === fifthModulus ) {
                                six = six + 10
                                seven = seven + 1
                            }
                        }
                    }
                } else if ( ( six % 10 ) === 0 ) {
                    if ( ( seven % 8 ) < 4 ) {
                        if ( ( seven % 4 ) < 2 ) {
                            if ( third === fourth ) {
                                six = six + 1
                                seven = seven + 2
                            }
                        }
                        if ( ( seven % 2 ) < 1 ) {
                            if ( third === fifthModulus ) {
                                six = six + 1
                                seven = seven + 1
                            }
                        }
                    }
                }

                if ( ( six % 100 ) < 10 ) {
                    if ( ( seven % 4 ) < 2 ) {
                        if ( ( seven % 2 ) < 1 ) {
                            if ( fourth === fifthModulus ) {
                                six = six + 10
                            }
                        }
                    }
                } else if ( ( six % 10 ) === 0 ) {
                    if ( ( seven % 4 ) < 2 ) {
                        if ( ( seven % 2 ) < 1 ) {
                            if ( fourth === fifthModulus ) {
                                six = six + 1
                            }
                        }
                    }
                }
            }

            switch ( six ) {
                case 40:
                    await QuestHelper.rewardSingleItem( player, ZIGGOS_GEMSTONE, 43 )
                    await QuestHelper.rewardSingleItem( player, 959, 3 ) // Scroll: Enchant Weapon (S-Grade)
                    await QuestHelper.rewardSingleItem( player, 729, 1 ) // Scroll: Enchant Weapon (A-Grade)

                    state.unsetVariables( ...allVariableNames )

                    return this.addColors( '30845-13.html', first, second, third, fourth, fifthDivided, fifthModulus )

                case 30:
                    await QuestHelper.rewardSingleItem( player, 959, 2 ) // Scroll: Enchant Weapon (S-Grade)
                    await QuestHelper.rewardSingleItem( player, 951, 2 ) // Scroll: Enchant Weapon (C-Grade)

                    state.unsetVariables( ...allVariableNames )

                    return this.addColors( '30845-13.html', first, second, third, fourth, fifthDivided, fifthModulus )

                case 21:
                case 12:
                    await QuestHelper.rewardSingleItem( player, 729, 1 ) // Scroll: Enchant Weapon (A-Grade)
                    await QuestHelper.rewardSingleItem( player, 947, 2 ) // Scroll: Enchant Weapon (B-Grade)
                    await QuestHelper.rewardSingleItem( player, 955, 1 ) // Scroll: Enchant Weapon (D-Grade)

                    state.unsetVariables( ...allVariableNames )

                    return this.addColors( '30845-15.html', first, second, third, fourth, fifthDivided, fifthModulus )

                case 20:
                    await QuestHelper.rewardSingleItem( player, 951, 2 ) // Scroll: Enchant Weapon (C-Grade)

                    state.unsetVariables( ...allVariableNames )

                    return this.addColors( '30845-16.html', first, second, third, fourth, fifthDivided, fifthModulus )

                case 11:
                    await QuestHelper.rewardSingleItem( player, 951, 1 ) // Scroll: Enchant Weapon (C-Grade)
                    state.unsetVariables( ...allVariableNames )

                    return this.addColors( '30845-17.html', first, second, third, fourth, fifthDivided, fifthModulus )

                case 10:
                    await QuestHelper.rewardSingleItem( player, 956, 2 ) // Scroll: Enchant Armor (D-Grade)

                    state.unsetVariables( ...allVariableNames )

                    return this.addColors( '30845-18.html', first, second, third, fourth, fifthDivided, fifthModulus )

                case 0:
                    state.unsetVariables( ...allVariableNames )
                    return this.addColors( '30845-19.html', first, second, third, fourth, fifthDivided, fifthModulus )
            }
        }
    }

    addColors( path: string, first: number, second: number, third: number, fourth: number, fifthDivided: number, fifthModulus: number ): string {
        let html: string = DataManager.getHtmlData().getItem( this.getPath( path ) )

        if ( ( fifthDivided % 2 ) < 1 ) {
            html = html
                    .replaceAll( 'FontColor1', 'FFFF00' )
                    .replaceAll( 'Cell1', '?' )
        } else {
            html = html.replaceAll( 'FontColor1', 'FF6F6F' )
                    .replaceAll( 'Cell1', responseSymbols[ first ] )
        }

        if ( ( fifthDivided % 4 ) < 2 ) {
            html = html.replaceAll( 'FontColor2', 'FFFF00' )
                    .replaceAll( 'Cell2', '?' )
        } else {
            html = html.replaceAll( 'FontColor2', 'FF6F6F' )
                    .replaceAll( 'Cell2', responseSymbols[ second ] )
        }

        if ( ( fifthDivided % 8 ) < 4 ) {
            html = html.replaceAll( 'FontColor3', 'FFFF00' )
                    .replaceAll( 'Cell3', '?' )
        } else {
            html = html.replaceAll( 'FontColor3', 'FF6F6F' )
                    .replaceAll( 'Cell3', responseSymbols[ third ] )
        }

        if ( ( fifthDivided % 16 ) < 8 ) {
            html = html.replaceAll( 'FontColor4', 'FFFF00' )
                    .replaceAll( 'Cell4', '?' )
        } else {
            html = html.replaceAll( 'FontColor4', 'FF6F6F' )
                    .replaceAll( 'Cell4', responseSymbols[ fourth ] )
        }

        if ( ( fifthDivided % 32 ) < 16 ) {
            html = html.replaceAll( 'FontColor5', 'FFFF00' )
                    .replaceAll( 'Cell5', '?' )
        } else {
            html = html.replaceAll( 'FontColor5', 'FF6F6F' )
                    .replaceAll( 'Cell5', responseSymbols[ fifthModulus ] )
        }

        return html
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( RED_GEM, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 2, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, RED_GEM, 1, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '30845-02.html' : '30845-01.htm' )

            case QuestStateValues.STARTED:
                if ( state.getVariable( variableNames.firstValue ) ) {
                    let fifthModulus: number = state.getVariable( variableNames.fifthValue ) % 100
                    let fifthDivided: number = Math.floor( state.getVariable( variableNames.fifthValue ) / 100 )
                    let first: number = state.getVariable( variableNames.firstValue )
                    let second: number = state.getVariable( variableNames.secondValue )
                    let third: number = state.getVariable( variableNames.thirdValue )
                    let fourth: number = state.getVariable( variableNames.fourthValue )

                    return this.addColors( '30845-11a.html', first, second, third, fourth, fifthDivided, fifthModulus )
                }

                return this.getPath( QuestHelper.getQuestItemsCount( player, RED_GEM ) < requiredAmount ? '30845-04.html' : '30845-05.html' )

            case QuestStateValues.COMPLETED:
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}