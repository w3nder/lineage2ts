import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const NAMELESS_SPIRIT = 31453
const ANTELOPE_SKIN = 8072
const FRINTEZZAS_SCROLL = 8073
const minimumLevel = 74

export class JourneyToASettlement extends ListenerLogic {
    constructor() {
        super( 'Q00654_JourneyToASettlement', 'listeners/tracked-600/JourneyToASettlement.ts' )
        this.questId = 654
        this.questItemIds = [ ANTELOPE_SKIN ]
    }

    getQuestStartIds(): Array<number> {
        return [ NAMELESS_SPIRIT ]
    }

    getTalkIds(): Array<number> {
        return [ NAMELESS_SPIRIT ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            21294,
            21295,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31453-02.htm':
                state.startQuest()
                break

            case '31453-03.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '31453-07.html':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, ANTELOPE_SKIN ) ) {
                    await QuestHelper.rewardSingleItem( player, FRINTEZZAS_SCROLL, 1 )
                    await state.exitQuest( true, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( ANTELOPE_SKIN, data.npcId === 21294 ? 0.840 : 0.893, data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 2, 2, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        await QuestHelper.giveSingleItem( state.getPlayer(), ANTELOPE_SKIN, 1 )
        state.setConditionWithSound( 3, true )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let shouldProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00119_LastImperialPrince' )
                return this.getPath( shouldProceed ? '31453-01.htm' : '31453-04.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        state.setConditionWithSound( 2, true )
                        return this.getPath( '31453-03.html' )

                    case 2:
                    case 3:
                        return this.getPath( QuestHelper.hasQuestItem( player, ANTELOPE_SKIN ) ? '31453-06.html' : '31453-05.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00654_JourneyToASettlement'
    }
}