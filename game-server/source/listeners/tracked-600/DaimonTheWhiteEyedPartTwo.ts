import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { AttackableKillEvent, NpcGeneralEvent, NpcSpawnEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestHelper } from '../helpers/QuestHelper'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SpawnMakerCache } from '../../gameService/cache/SpawnMakerCache'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const DAIMONS_ALTAR = 31541
const EYE_OF_ARGOS = 31683
const DAIMON_THE_WHITE_EYED = 25290
const UNFINISHED_SUMMON_CRYSTAL = 7192
const SUMMON_CRYSTAL = 7193
const ESSENCE_OF_DAIMON = 7194
const minimumLevel = 73
const DAIMON_THE_WHITE_EYED_LOC = new Location( 186320, -43904, -3175 )
const DYE_I2M2_C = 4595 // Greater Dye of INT <Int+2 Men-2>
const DYE_I2W2_C = 4596 // Greater Dye of INT <Int+2 Wit-2>
const DYE_M2I2_C = 4597 // Greater Dye of MEN <Men+2 Int-2>
const DYE_M2W2_C = 4598 // Greater Dye of MEN <Men+2 Wit-2>
const DYE_W2I2_C = 4599 // Greater Dye of WIT <Wit+2 Int-2>
const DYE_W2M2_C = 4600 // Greater Dye of WIT <Wit+2 Men-2>

const eventNames = {
    despawn: 'ds',
}

export class DaimonTheWhiteEyedPartTwo extends ListenerLogic {
    constructor() {
        super( 'Q00604_DaimonTheWhiteEyedPart2', 'listeners/tracked-600/DaimonTheWhiteEyedPartTwo.ts' )
        this.questId = 604
        this.questItemIds = [ SUMMON_CRYSTAL, ESSENCE_OF_DAIMON ]
    }

    getQuestStartIds(): Array<number> {
        return [ EYE_OF_ARGOS ]
    }

    getTalkIds(): Array<number> {
        return [ EYE_OF_ARGOS, DAIMONS_ALTAR ]
    }

    getSpawnIds(): Array<number> {
        return [ DAIMON_THE_WHITE_EYED ]
    }

    getAttackableKillIds(): Array<number> {
        return [ DAIMON_THE_WHITE_EYED ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === eventNames.despawn ) {
            if ( this.isDaimonSpawned() ) {
                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.CAN_LIGHT_EXIST_WITHOUT_DARKNESS )
                await npc.deleteMe()
            }

            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31683-04.htm':
                state.startQuest()
                state.setMemoState( 11 )

                await QuestHelper.takeSingleItem( player, UNFINISHED_SUMMON_CRYSTAL, 1 )
                await QuestHelper.giveSingleItem( player, SUMMON_CRYSTAL, 1 )

                break

            case '31683-07.html':
                if ( QuestHelper.hasQuestItem( player, ESSENCE_OF_DAIMON ) ) {

                    await QuestHelper.takeSingleItem( player, ESSENCE_OF_DAIMON, 1 )
                    await QuestHelper.rewardSingleItem( player, this.getItemIdReward(), 5 )
                    await state.exitQuest( true, true )

                    break
                }

                return this.getPath( '31683-08.html' )

            case '31541-02.html':
                if ( QuestHelper.hasQuestItem( player, SUMMON_CRYSTAL ) ) {
                    if ( !this.isDaimonSpawned() ) {
                        await QuestHelper.takeSingleItem( player, SUMMON_CRYSTAL, 1 )
                        QuestHelper.addSpawnAtLocation( DAIMON_THE_WHITE_EYED, DAIMON_THE_WHITE_EYED_LOC )

                        let npc = L2World.getObjectById( data.characterId ) as L2Npc
                        await npc.deleteMe()

                        state.setMemoState( 21 )
                        state.setConditionWithSound( 2, true )

                        break
                    }

                    return this.getPath( '31541-03.html' )
                }

                return this.getPath( '31541-04.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    isDaimonSpawned(): boolean {
        return !!SpawnMakerCache.getFirstSpawn( DAIMON_THE_WHITE_EYED )
    }

    getItemIdReward(): number {
        let random = Math.random()
        if ( random < 0.167 ) {
            return DYE_I2M2_C
        }

        if ( random < 0.334 ) {
            return DYE_I2W2_C
        }

        if ( random < 0.501 ) {
            return DYE_M2I2_C
        }

        if ( random < 0.668 ) {
            return DYE_M2W2_C
        }

        if ( random < 0.835 ) {
            return DYE_W2I2_C
        }

        return DYE_W2M2_C
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        this.startQuestTimer( eventNames.despawn, 1200000, data.characterId, 0 )

        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.WHO_IS_CALLING_ME )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance, npc: L2Npc ): Promise<void> => {
            if ( !state || state.getMemoState() < 11 && state.getMemoState() > 21 ) {
                return
            }

            if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
                return
            }

            if ( QuestHelper.hasQuestItem( player, ESSENCE_OF_DAIMON ) ) {
                state.setConditionWithSound( 3, true )
                state.setMemoState( 22 )
            }

            await QuestHelper.rewardSingleQuestItem( player, ESSENCE_OF_DAIMON, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31683-01.htm' )
                }

                if ( !QuestHelper.hasQuestItem( player, UNFINISHED_SUMMON_CRYSTAL ) ) {
                    return this.getPath( '31683-02.htm' )
                }

                return this.getPath( '31683-03.htm' )

            case QuestStateValues.STARTED:
                if ( data.characterNpcId === EYE_OF_ARGOS ) {
                    if ( state.isMemoState( 11 ) ) {
                        return this.getPath( '31683-05.html' )
                    }

                    if ( state.getMemoState() >= 22 ) {
                        return this.getPath( QuestHelper.hasQuestItem( player, ESSENCE_OF_DAIMON ) ? '31683-06.html' : '31683-09.html' )
                    }

                    break
                }

                if ( state.isMemoState( 11 ) ) {
                    if ( QuestHelper.hasQuestItem( player, SUMMON_CRYSTAL ) ) {
                        return this.getPath( '31541-01.html' )
                    }

                    break
                }

                if ( state.isMemoState( 21 ) ) {
                    if ( !this.isDaimonSpawned() ) {
                        QuestHelper.addSpawnAtLocation( DAIMON_THE_WHITE_EYED, DAIMON_THE_WHITE_EYED_LOC )

                        let npc = L2World.getObjectById( data.characterId ) as L2Npc
                        await npc.deleteMe()

                        return this.getPath( '31541-02.html' )
                    }

                    return this.getPath( '31541-03.html' )
                }

                if ( state.getMemoState() >= 22 ) {
                    return this.getPath( '31541-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00604_DaimonTheWhiteEyedPart2'
    }
}