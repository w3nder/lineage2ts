import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const PIERCE = 31553
const TALON_OF_STAKATO = 7250
const GOLDEN_RAM_COIN = 7251
const requiredAmount = 100
const minimumLevel = 66

const monsterRewardChances: { [ npcId: number ]: number } = {
    21508: 0.599, // splinter_stakato
    21509: 0.524, // splinter_stakato_worker
    21510: 0.640, // splinter_stakato_soldier
    21511: 0.830, // splinter_stakato_drone
    21512: 0.970, // splinter_stakato_drone_a
    21513: 0.682, // needle_stakato
    21514: 0.595, // needle_stakato_worker
    21515: 0.727, // needle_stakato_soldier
    21516: 0.879, // needle_stakato_drone
    21517: 0.999, // needle_stakato_drone_a
}

export class CleanUpTheSwampOfScreams extends ListenerLogic {
    constructor() {
        super( 'Q00629_CleanUpTheSwampOfScreams', 'listeners/tracked-600/CleanUpTheSwampOfScreams.ts' )
        this.questId = 629
        this.questItemIds = [
            TALON_OF_STAKATO,
            GOLDEN_RAM_COIN,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ PIERCE ]
    }

    getTalkIds(): Array<number> {
        return [ PIERCE ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31553-03.htm':
                state.startQuest()
                break

            case '31553-04.html':
            case '31553-06.html':
                break

            case '31553-07.html':
                if ( QuestHelper.getQuestItemsCount( player, TALON_OF_STAKATO ) >= requiredAmount ) {
                    await QuestHelper.takeSingleItem( player, TALON_OF_STAKATO, requiredAmount )
                    await QuestHelper.rewardSingleItem( player, GOLDEN_RAM_COIN, 20 )

                    break
                }

                return this.getPath( '31553-08.html' )

            case '31553-09.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( TALON_OF_STAKATO, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 1, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( state.getPlayer(), TALON_OF_STAKATO, 1, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31553-01.htm' : '31553-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.getQuestItemsCount( player, TALON_OF_STAKATO ) >= requiredAmount ? '31553-04.html' : '31553-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00629_CleanUpTheSwampOfScreams'
    }
}