import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const DINN = 32105
const DINOSAUR_TISSUE = 8774
const DINOSAUR_EGG = 8775
const minimumLevel = 75
const ANCIENT_EGG = 18344

const monsterRewardChances: { [ npcId: number ]: number } = {
    22196: 0.309, // Velociraptor
    22197: 0.309, // Velociraptor
    22198: 0.309, // Velociraptor
    22199: 0.309, // Pterosaur
    22215: 0.988, // Tyrannosaurus
    22216: 0.988, // Tyrannosaurus
    22217: 0.988, // Tyrannosaurus
    22218: 0.309, // Velociraptor
    22223: 0.309, // Velociraptor
    [ ANCIENT_EGG ]: 1,
}

export class APowerfulPrimevalCreature extends ListenerLogic {
    constructor() {
        super( 'Q00642_APowerfulPrimevalCreature', 'listeners/tracked-600/APowerfulPrimevalCreature.ts' )
        this.questId = 642
        this.questItemIds = [ DINOSAUR_TISSUE, DINOSAUR_EGG ]
    }

    getQuestStartIds(): Array<number> {
        return [ DINN ]
    }

    getTalkIds(): Array<number> {
        return [ DINN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32105-05.html':
                state.startQuest()
                break

            case '32105-06.htm':
                await state.exitQuest( true )
                break

            case '32105-09.html':
                let tissueAmount: number = QuestHelper.getQuestItemsCount( player, DINOSAUR_TISSUE )
                if ( tissueAmount > 0 ) {
                    await QuestHelper.takeSingleItem( player, DINOSAUR_TISSUE, -1 )
                    await QuestHelper.giveAdena( player, 5000 * tissueAmount, true )
                }

                return this.getPath( '32105-14.html' )

            case 'exit':
                let amount: number = QuestHelper.getQuestItemsCount( player, DINOSAUR_TISSUE )
                if ( amount > 0 ) {
                    await QuestHelper.giveAdena( player, 5000 * amount, true )
                    await state.exitQuest( true, true )

                    return this.getPath( '32105-12.html' )
                }

                await state.exitQuest( true, true )
                return this.getPath( '32105-13.html' )


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let itemId: number = ANCIENT_EGG === data.npcId ? DINOSAUR_EGG : DINOSAUR_TISSUE
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '32105-01.htm' : '32105-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasAtLeastOneQuestItem( player, DINOSAUR_TISSUE, DINOSAUR_EGG ) ? '32105-08.html' : '32105-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00642_APowerfulPrimevalCreature'
    }
}