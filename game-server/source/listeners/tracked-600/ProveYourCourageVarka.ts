import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'

const ASHAS = 31377
const HEKATON = 25299
const HEKATON_HEAD = 7240
const VALOR_FEATHER = 7229
const VARKA_ALLIANCE_THREE = 7223
const minimumLevel = 75

export class ProveYourCourageVarka extends ListenerLogic {
    constructor() {
        super( 'Q00613_ProveYourCourageVarka', 'listeners/tracked-600/ProveYourCourageVarka.ts' )
        this.questId = 613
        this.questItemIds = [ HEKATON_HEAD ]
    }

    getQuestStartIds(): Array<number> {
        return [ ASHAS ]
    }

    getTalkIds(): Array<number> {
        return [ ASHAS ]
    }

    getAttackableKillIds(): Array<number> {
        return [ HEKATON ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00613_ProveYourCourageVarka'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31377-04.htm':
                state.startQuest()
                break

            case '31377-07.html':
                if ( QuestHelper.hasQuestItem( player, HEKATON_HEAD ) && state.isCondition( 2 ) ) {
                    await QuestHelper.giveSingleItem( player, VALOR_FEATHER, 1 )
                    await QuestHelper.addExpAndSp( player, 10000, 0 )
                    await state.exitQuest( true, true )

                    break
                }

                return QuestHelper.getNoQuestMessagePath()

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance, npc: L2Npc ): Promise<void> => {
            if ( !state || !state.isCondition( 1 ) ) {
                return
            }

            if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
                return
            }

            await QuestHelper.giveSingleItem( player, HEKATON_HEAD, 1 )
            state.setConditionWithSound( 2, true )
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31377-03.htm' )
                }

                return this.getPath( QuestHelper.hasQuestItem( player, VARKA_ALLIANCE_THREE ) ? '31377-01.htm' : '31377-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, HEKATON_HEAD ) ? '31377-05.html' : '31377-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}