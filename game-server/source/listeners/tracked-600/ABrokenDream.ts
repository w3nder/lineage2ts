import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const GHOST_OF_A_RAILROAD_ENGINEER = 32054
const REMNANTS_OF_OLD_DWARVES_DREAMS = 8514
const minimumLevel = 39

export class ABrokenDream extends ListenerLogic {
    constructor() {
        super( 'Q00650_ABrokenDream', 'listeners/tracked-600/ABrokenDream.ts' )
        this.questId = 650
        this.questItemIds = [ REMNANTS_OF_OLD_DWARVES_DREAMS ]
    }

    getQuestStartIds(): Array<number> {
        return [ GHOST_OF_A_RAILROAD_ENGINEER ]
    }

    getTalkIds(): Array<number> {
        return [ GHOST_OF_A_RAILROAD_ENGINEER ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            22027,
            22028,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32054-03.htm':
                state.startQuest()
                break

            case '32054-07.html':
            case '32054-08.html':
                break

            case '32054-09.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( REMNANTS_OF_OLD_DWARVES_DREAMS, data.npcId === 22027 ? 0.575 : 0.515, data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), 1, 2, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, REMNANTS_OF_OLD_DWARVES_DREAMS, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '32054-02.htm' )
                }

                return this.getPath( player.hasQuestCompleted( 'Q00117_TheOceanOfDistantStars' ) ? '32054-01.htm' : '32054-04.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItem( player, REMNANTS_OF_OLD_DWARVES_DREAMS ) ? '32054-05.html' : '32054-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00650_ABrokenDream'
    }
}