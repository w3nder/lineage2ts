import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { DataManager } from '../../data/manager'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const MATRAS = 32245
const RED_GEM = 10372
const DYNASTY_SOUL_II = 10413
const minimumLevel = 76

const monsterRewardChances: { [ npcId: number ]: number } = {
    22363: 0.890,
    22364: 0.261,
    22365: 0.560,
    22366: 0.560,
    22367: 0.190,
    22368: 0.129,
    22369: 0.210,
    22370: 0.787,
    22371: 0.257,
    22372: 0.656,
}

const variableNames = {
    gems: 'gems',
}

export class MatrasSuspiciousRequest extends ListenerLogic {
    constructor() {
        super( 'Q00691_MatrasSuspiciousRequest', 'listeners/tracked-600/MatrasSuspiciousRequest.ts' )
        this.questId = 691
    }

    getQuestStartIds(): Array<number> {
        return [ MATRAS ]
    }

    getTalkIds(): Array<number> {
        return [ MATRAS ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00691_MatrasSuspiciousRequest'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32245-02.htm':
            case '32245-11.html':
                break

            case '32245-04.htm':
                state.startQuest()
                state.setVariable( variableNames.gems, 0 )
                break

            case 'take_reward':
                let rewardGems = state.getVariable( variableNames.gems )
                if ( rewardGems >= 744 ) {
                    state.setVariable( variableNames.gems, rewardGems - 744 )
                    await QuestHelper.rewardSingleItem( player, DYNASTY_SOUL_II, 1 )

                    return this.getPath( '32245-09.html' )
                }

                return DataManager.getHtmlData().getItem( this.getPath( '32245-10.html' ) )
                        .replace( '%itemcount%', state.getVariable( variableNames.gems ).toString() )

            case '32245-08.html':
                let amount: number = QuestHelper.getQuestItemsCount( player, RED_GEM )

                if ( amount > 0 ) {
                    await QuestHelper.takeSingleItem( player, RED_GEM, -1 )
                    state.incrementVariable( variableNames.gems, amount )
                }

                return DataManager.getHtmlData().getItem( this.getPath( '32245-08.html' ) )
                        .replace( '%itemcount%', state.getVariable( variableNames.gems ).toString() )

            case '32245-12.html':
                let total: number = state.getVariable( variableNames.gems )
                if ( total > 0 ) {
                    await QuestHelper.giveAdena( player, total * 10000, true )
                }

                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( RED_GEM, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, RED_GEM, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32245-01.htm' : '32245-03.html' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.hasQuestItem( player, RED_GEM ) ) {
                    return this.getPath( '32245-05.html' )
                }

                let amount: number = state.getVariable( variableNames.gems )
                if ( amount > 0 ) {
                    return DataManager.getHtmlData().getItem( this.getPath( '32245-07.html' ) )
                            .replace( '%itemcount%', state.getVariable( variableNames.gems ).toString() )
                }

                return this.getPath( '32245-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}