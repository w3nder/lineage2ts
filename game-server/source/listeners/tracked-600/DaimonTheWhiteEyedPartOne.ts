import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const EYE_OF_ARGOS = 31683
const TABLET_1 = 31548
const TABLET_2 = 31549
const TABLET_3 = 31550
const TABLET_4 = 31551
const TABLET_5 = 31552
const SPIRIT_OF_DARKNESS = 7190
const BROKEN_CRYSTAL = 7191
const UNFINISHED_CRYSTAL = 7192
const minimumLevel = 73
const spiritLimit = 200

const monsterRewardChances: { [ npcId: number ]: number } = {
    21297: 0.5, // Canyon Bandersnatch Slave
    21299: 0.519, // Buffalo Slave
    21304: 0.673, // Grendel Slave
}

const variableNames = {
    tabletOne: 't1',
    tabletTwo: 't2',
    tabletThree: 't3',
    tabletFour: 't4',
    tabletFive: 't5',
}

export class DaimonTheWhiteEyedPartOne extends ListenerLogic {
    constructor() {
        super( 'Q00603_DaimonTheWhiteEyedPart1', 'listeners/tracked-600/DaimonTheWhiteEyedPartOne.ts' )
        this.questId = 603
        this.questItemIds = [
            SPIRIT_OF_DARKNESS,
            BROKEN_CRYSTAL,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ EYE_OF_ARGOS ]
    }

    getTalkIds(): Array<number> {
        return [
            EYE_OF_ARGOS,
            TABLET_1,
            TABLET_2,
            TABLET_3,
            TABLET_4,
            TABLET_5,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31683-03.htm':
                if ( state.isCreated() ) {
                    state.setVariable( variableNames.tabletOne, 0 )
                    state.setVariable( variableNames.tabletTwo, 0 )
                    state.setVariable( variableNames.tabletThree, 0 )
                    state.setVariable( variableNames.tabletFour, 0 )
                    state.setVariable( variableNames.tabletFive, 0 )

                    state.startQuest()
                    break
                }

                return

            case '31548-02.html':
            case '31549-02.html':
            case '31550-02.html':
            case '31551-02.html':
            case '31552-02.html':
                if ( state.getCondition() < 6 ) {
                    await QuestHelper.giveSingleItem( player, BROKEN_CRYSTAL, 1 )

                    state.setVariable( data.characterNpcId.toString(), 1 )
                    state.setConditionWithSound( state.getCondition() + 1, true )

                    break
                }

                return

            case '31683-06.html':
                if ( state.isCondition( 6 ) && QuestHelper.getQuestItemsCount( player, BROKEN_CRYSTAL ) >= 5 ) {
                    await QuestHelper.takeSingleItem( player, BROKEN_CRYSTAL, -1 )

                    state.setConditionWithSound( 7, true )
                    break
                }

                return

            case '31683-10.html':
                if ( state.isCondition( 8 ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, SPIRIT_OF_DARKNESS ) >= spiritLimit ) {
                        await QuestHelper.takeSingleItem( player, SPIRIT_OF_DARKNESS, -1 )
                        await QuestHelper.rewardSingleItem( player, UNFINISHED_CRYSTAL, 1 )
                        await state.exitQuest( true, true )

                        break
                    }

                    return this.getPath( '31683-11.html' )
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00603_DaimonTheWhiteEyedPart1'
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( SPIRIT_OF_DARKNESS, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 7, 3, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardAndProgressState( player, state, SPIRIT_OF_DARKNESS, 1, spiritLimit, data.isChampion, 8 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === EYE_OF_ARGOS ) {
                    return this.getPath( player.getLevel() < minimumLevel ? '31683-02.html' : '31683-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                if ( data.characterNpcId === EYE_OF_ARGOS ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            return this.getPath( '31683-04.html' )

                        case 6:
                            return this.getPath( '31683-05.html' )

                        case 7:
                            return this.getPath( '31683-07.html' )

                        case 8:
                            return this.getPath( '31683-08.html' )
                    }

                    break
                }

                if ( !state.getVariable( data.characterNpcId.toString() ) ) {
                    return this.getPath( `${ data.characterNpcId }-01.html` )
                }

                return this.getPath( `${ data.characterNpcId }-03.html` )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}