import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

const GALIBREDO = 30181
const SUKI = 32013
const SOE = 736
const minimumLevel = 36

export class WildMaiden extends ListenerLogic {
    constructor() {
        super( 'Q00653_WildMaiden', 'listeners/tracked-600/WildMaiden.ts' )
        this.questId = 653
    }

    getQuestStartIds(): Array<number> {
        return [ SUKI ]
    }

    getTalkIds(): Array<number> {
        return [
            GALIBREDO,
            SUKI,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00653_WildMaiden'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32013-03.html':
                break

            case '32013-04.htm':
                let player = L2World.getPlayer( data.playerId )
                if ( !QuestHelper.hasQuestItem( player, SOE ) ) {
                    return this.getPath( '32013-05.htm' )
                }

                state.startQuest()
                await QuestHelper.takeSingleItem( player, SOE, 1 )

                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                await npc.deleteMe()

                return this.getPath( Math.random() > 0.5 ? data.eventName : '32013-04a.htm' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== SUKI ) {
                    break
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '32013-01.htm' : '32013-01a.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case SUKI:
                        return this.getPath( '32013-02.htm' )

                    case GALIBREDO:
                        await QuestHelper.giveAdena( player, 2553, true )
                        await state.exitQuest( true, true )

                        return this.getPath( '30181-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}