import {
    PersistedConfigurationLogic,
    PersistedConfigurationLogicType,
} from '../../gameService/models/listener/PersistedConfigurationLogic'
import { QuestHelper } from '../helpers/QuestHelper'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ConfigManager } from '../../config/ConfigManager'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const ASEFA = 31372
const VARKA_TOTEM = 31560
const ASHUTAR = 25316
const GREEN_TOTEM = 7238
const ASHUTAR_HEART = 7239
const minimumLevel = 75

interface QuestConfiguration extends PersistedConfigurationLogicType {
    futureRespawnTime: number
}

const eventNames = {
    spawnVarkaTotem: 'svt',
    despawnAshutar: 'da',
}

export class MagicalPowerOfWaterPartTwo extends PersistedConfigurationLogic<QuestConfiguration> {
    constructor() {
        super( 'Q00610_MagicalPowerOfWaterPart2', 'listeners/tracked-600/MagicalPowerOfWaterPartTwo.ts' )
        this.questId = 610
        this.questItemIds = [ GREEN_TOTEM, ASHUTAR_HEART ]
    }

    async initialize(): Promise<void> {
        let currentTime = Date.now()

        if ( this.configuration.futureRespawnTime > currentTime ) {
            this.startQuestTimer( eventNames.spawnVarkaTotem, this.configuration.futureRespawnTime - currentTime, 0, 0 )
            return
        }
    }

    getDefaultConfiguration(): QuestConfiguration {
        return {
            futureRespawnTime: 0,
        }
    }

    getQuestStartIds(): Array<number> {
        return [ ASEFA ]
    }

    getTalkIds(): Array<number> {
        return [ ASEFA, VARKA_TOTEM ]
    }

    getAttackableAttackIds(): Array<number> {
        return [ ASHUTAR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00610_MagicalPowerOfWaterPart2'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case eventNames.despawnAshutar:
                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_POWER_OF_CONSTRAINT_IS_GETTING_WEAKER_YOUR_RITUAL_HAS_FAILED )
                await npc.deleteMe()

                QuestHelper.addGenericSpawn( null, VARKA_TOTEM, 105452, -36775, -1050, 34000, false, 0, true )
                return

            case eventNames.spawnVarkaTotem:
                QuestHelper.addGenericSpawn( null, VARKA_TOTEM, 105452, -36775, -1050, 34000, false, 0, true )
                return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31372-02.html':
                state.startQuest()
                break

            case 'give_heart':
                if ( QuestHelper.hasQuestItem( player, ASHUTAR_HEART ) ) {
                    await QuestHelper.addExpAndSp( player, 10000, 0 )
                    await state.exitQuest( true, true )

                    this.getPath( '31372-06.html' )
                }

                return this.getPath( '31372-07.html' )

            case 'spawn_totem':
                return this.getPath( QuestHelper.hasQuestItem( player, GREEN_TOTEM ) ? await this.spawnAshutar( player, L2World.getObjectById( data.characterId ) as L2Npc, state ) : '31560-04.html' )
        }

        return this.getPath( data.eventName )
    }

    async spawnAshutar( player: L2PcInstance, npc: L2Npc, state: QuestState ): Promise<string> {
        if ( this.getQuestTimer( eventNames.spawnVarkaTotem, 0, 0 ) ) {
            return this.getPath( '31560-03.html' )
        }

        if ( state.isCondition( 1 ) ) {
            await QuestHelper.takeSingleItem( player, GREEN_TOTEM, 1 )
            state.setConditionWithSound( 2, true )
        }

        await npc.deleteMe()
        let ashutar: L2Npc = QuestHelper.addGenericSpawn( null, ASHUTAR, 104825, -36926, -1136, 0, false, 0 )
        this.startQuestTimer( eventNames.despawnAshutar, 1200000, ashutar.getObjectId(), 0 )

        BroadcastHelper.broadcastNpcSayStringId( ashutar, NpcSayType.NpcAll, NpcStringIds.THE_MAGICAL_POWER_OF_WATER_COMES_FROM_THE_POWER_OF_STORM_AND_HAIL_IF_YOU_DARE_TO_CONFRONT_IT_ONLY_DEATH_WILL_AWAIT_YOU )
        return this.getPath( '31560-02.html' )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== ASEFA ) {
                    break
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31372-00b.html' )
                }

                return this.getPath( QuestHelper.hasQuestItem( player, GREEN_TOTEM ) ? '31372-01.htm' : '31372-00a.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case ASEFA:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '31372-03.html' )
                        }

                        return this.getPath( QuestHelper.hasQuestItem( player, ASHUTAR_HEART ) ? '31372-04.html' : '31372-05.html' )

                    case VARKA_TOTEM:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31560-01.html' )

                            case 2:
                                return this.spawnAshutar( player, L2World.getObjectById( data.characterId ) as L2Npc, state )

                            case 3:
                                return this.getPath( '31560-05.html' )
                        }

                        break
                }
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let respawnMinDelay = Math.floor( GeneralHelper.hoursToMillis( 12 ) * ConfigManager.npc.getRaidMinRespawnMultiplier() )
        let respawnMaxDelay = Math.floor( GeneralHelper.hoursToMillis( 36 ) * ConfigManager.npc.getRaidMaxRespawnMultiplier() )
        let respawnDelay = _.random( respawnMinDelay, respawnMaxDelay )

        this.stopQuestTimer( eventNames.despawnAshutar, data.targetId, 0 )
        this.startQuestTimer( eventNames.spawnVarkaTotem, respawnDelay, 0, 0 )

        this.configuration.futureRespawnTime = Date.now() + respawnDelay
        this.saveConfiguration()

        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance, npc: L2Npc ): Promise<void> => {
            if ( !state ) {
                return
            }

            if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
                return
            }

            switch ( state.getCondition() ) {
                case 1:
                    await QuestHelper.giveSingleItem( player, GREEN_TOTEM, 1 )

                case 2:
                    if ( !QuestHelper.hasQuestItem( player, ASHUTAR_HEART ) ) {
                        await QuestHelper.giveSingleItem( player, ASHUTAR_HEART, 1 )
                    }

                    state.setConditionWithSound( 3, true )

                    return
            }
        } )

        return
    }
}