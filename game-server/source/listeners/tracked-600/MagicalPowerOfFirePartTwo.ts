import {
    PersistedConfigurationLogic,
    PersistedConfigurationLogicType,
} from '../../gameService/models/listener/PersistedConfigurationLogic'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { ConfigManager } from '../../config/ConfigManager'
import _ from 'lodash'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const UDAN = 31379
const KETRA_TOTEM = 31558
const NASTRON = 25306
const RED_TOTEM = 7243
const NASTRON_HEART = 7244
const minimumLevel = 75

interface QuestConfiguration extends PersistedConfigurationLogicType {
    futureRespawnTime: number
}

const eventNames = {
    spawnKetraTotem: 'skt',
    despawnNastron: 'dn',
}

export class MagicalPowerOfFirePartTwo extends PersistedConfigurationLogic<QuestConfiguration> {
    constructor() {
        super( 'Q00616_MagicalPowerOfFirePart2', 'listeners/tracked-600/MagicalPowerOfFirePartTwo.ts' )
        this.questId = 616
        this.questItemIds = [
            RED_TOTEM,
            NASTRON_HEART,
        ]
    }

    async initialize(): Promise<void> {
        let currentTime = Date.now()

        if ( this.configuration.futureRespawnTime > currentTime ) {
            this.startQuestTimer( eventNames.spawnKetraTotem, this.configuration.futureRespawnTime - currentTime, 0, 0 )
            return
        }
    }

    getDefaultConfiguration(): QuestConfiguration {
        return {
            futureRespawnTime: 0,
        }
    }

    getQuestStartIds(): Array<number> {
        return [ UDAN ]
    }

    getTalkIds(): Array<number> {
        return [ UDAN, KETRA_TOTEM ]
    }

    getAttackableKillIds(): Array<number> {
        return [ NASTRON ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let respawnMinDelay = Math.floor( 43200000 * ConfigManager.npc.getRaidMinRespawnMultiplier() )
        let respawnMaxDelay = Math.floor( 129600000 * ConfigManager.npc.getRaidMaxRespawnMultiplier() )
        let respawnDelay = _.random( respawnMinDelay, respawnMaxDelay )

        this.stopQuestTimer( eventNames.despawnNastron, data.targetId, 0 )
        this.configuration.futureRespawnTime = Date.now() + respawnDelay
        this.saveConfiguration()

        this.startQuestTimer( eventNames.spawnKetraTotem, respawnDelay, null, null )
        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance, npc: L2Npc ): Promise<void> => {
            if ( !state ) {
                return
            }

            if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
                return
            }

            switch ( state.getCondition() ) {
                case 1:
                    await QuestHelper.takeSingleItem( player, RED_TOTEM, 1 )

                case 2:
                    if ( !QuestHelper.hasQuestItem( player, NASTRON_HEART ) ) {
                        await QuestHelper.giveSingleItem( player, NASTRON_HEART, 1 )
                    }

                    state.setConditionWithSound( 3, true )

                    return
            }
        } )

        return
    }

    async spawnNastron( player: L2PcInstance, npc: L2Npc, state: QuestState ): Promise<string> {
        if ( this.getQuestTimer( eventNames.despawnNastron, 0, 0 ) ) {
            return this.getPath( '31558-03.html' )
        }

        if ( state.isCondition( 1 ) ) {
            await QuestHelper.takeSingleItem( player, RED_TOTEM, 1 )
            state.setConditionWithSound( 2, true )
        }

        await npc.deleteMe()
        let nastron: L2Npc = QuestHelper.addGenericSpawn( null, NASTRON, 142528, -82528, -6496, 0, false, 0 )
        this.startQuestTimer( eventNames.despawnNastron, 1200000, nastron.getObjectId(), 0 )

        BroadcastHelper.broadcastNpcSayStringId( nastron, NpcSayType.NpcAll, NpcStringIds.THE_MAGICAL_POWER_OF_FIRE_IS_ALSO_THE_POWER_OF_FLAMES_AND_LAVA_IF_YOU_DARE_TO_CONFRONT_IT_ONLY_DEATH_WILL_AWAIT_YOU )
        return this.getPath( '31558-02.html' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case eventNames.despawnNastron:
                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_POWER_OF_CONSTRAINT_IS_GETTING_WEAKER_YOUR_RITUAL_HAS_FAILED )
                await npc.deleteMe()

                QuestHelper.addGenericSpawn( null, KETRA_TOTEM, 142368, -82512, -6487, 58000, false, 0, true )
                return

            case eventNames.spawnKetraTotem:
                QuestHelper.addGenericSpawn( null, KETRA_TOTEM, 142368, -82512, -6487, 58000, false, 0, true )
                return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31379-02.html':
                state.startQuest()
                break

            case 'give_heart':
                if ( QuestHelper.hasQuestItem( player, NASTRON_HEART ) ) {
                    await QuestHelper.addExpAndSp( player, 10000, 0 )
                    await state.exitQuest( true, true )

                    return this.getPath( '31379-06.html' )
                }

                return this.getPath( '31379-07.html' )

            case 'spawn_totem':
                return this.getPath( QuestHelper.hasQuestItem( player, RED_TOTEM ) ? await this.spawnNastron( player, L2World.getObjectById( data.characterId ) as L2Npc, state ) : '31558-04.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== UDAN ) {
                    break
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31379-00b.html' )
                }

                return this.getPath( QuestHelper.hasQuestItem( player, RED_TOTEM ) ? '31379-01.htm' : '31379-00a.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case UDAN:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '31379-03.html' )
                        }

                        return this.getPath( QuestHelper.hasQuestItem( player, NASTRON_HEART ) ? '31379-04.html' : '31379-05.html' )

                    case KETRA_TOTEM:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31558-01.html' )

                            case 2:
                                return this.spawnNastron( player, L2World.getObjectById( data.characterId ) as L2Npc, state )

                            case 3:
                                return this.getPath( '31558-05.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00616_MagicalPowerOfFirePart2'
    }
}