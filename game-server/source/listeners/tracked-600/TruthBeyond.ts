import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import {
    CharacterEnterZoneEvent,
    EventType,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { InstanceType } from '../../gameService/enums/InstanceType'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { AreaCache } from '../../gameService/cache/AreaCache'

const ELIYAH = 31329
const FLAURON = 32010
const VISITOR_MARK = 8064
const FADED_MARK = 8065
const MARK = 8067

export class TruthBeyond extends ListenerLogic {
    areaId: number

    constructor() {
        super( 'Q00636_TruthBeyond', 'listeners/tracked-600/TruthBeyond.ts' )
        this.questId = 636
    }

    async initialize(): Promise<void> {
        this.areaId = AreaCache.getAreaByName( '636_pagans_mark' ).id
    }

    getQuestStartIds(): Array<number> {
        return [ ELIYAH ]
    }

    getTalkIds(): Array<number> {
        return [
            ELIYAH,
            FLAURON,
        ]
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.AreaId,
                eventType: EventType.CharacterEnterArea,
                method: this.onEnterZone.bind( this ),
                ids: [ this.areaId ]
            },
        ]
    }

    async onEnterZone( data: CharacterEnterZoneEvent ): Promise<void> {
        if ( data.characterInstanceType !== InstanceType.L2PcInstance ) {
            return
        }

        let player = L2World.getPlayer( data.characterId )
        await QuestHelper.takeSingleItem( player, VISITOR_MARK, 1 )
        await QuestHelper.giveSingleItem( player, FADED_MARK, 1 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31329-04.htm':
                state.startQuest()
                break

            case '32010-02.htm':
                await QuestHelper.giveSingleItem( player, VISITOR_MARK, 1 )
                await state.exitQuest( true, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== ELIYAH ) {
                    break
                }

                if ( QuestHelper.hasAtLeastOneQuestItem( player, VISITOR_MARK, FADED_MARK, MARK ) ) {
                    await state.exitQuest( true )

                    return this.getPath( '31329-mark.htm' )
                }

                if ( player.getLevel() > 72 ) {
                    return this.getPath( '31329-02.htm' )
                }

                await state.exitQuest( true )

                return this.getPath( '31329-01.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case ELIYAH:
                        return this.getPath( '31329-05.htm' )

                    case FLAURON:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '32010-01.htm' )
                        }

                        await state.exitQuest( true )
                        return this.getPath( '32010-03.htm' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00636_TruthBeyond'
    }
}