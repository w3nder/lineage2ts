import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const RAFFORTY = 32020
const ICE_SHELF = 32023
const SILVER_HEMOCYTE = 8057
const SILVER_ICE_CRYSTAL = 8077
const BLACK_ICE_CRYSTAL = 8078
const minimumLevel = 53

type RewardChances = [ number, number ] // first chance, second chance
const monsterRewardChances: { [ npcId: number ]: RewardChances } = {
    22080: [ 0.285, 0.048 ], // Massive Maze Bandersnatch
    22081: [ 0.443, 0.0 ], // Lost Watcher
    22082: [ 0.510, 0.0 ], // Elder Lost Watcher
    22083: [ 0.477, 0.049 ], // Baby Panthera
    22084: [ 0.477, 0.049 ], // Panthera
    22085: [ 0.420, 0.043 ], // Lost Gargoyle
    22086: [ 0.490, 0.050 ], // Lost Gargoyle Youngling
    22087: [ 0.787, 0.081 ], // Pronghorn Spirit
    22088: [ 0.480, 0.049 ], // Pronghorn
    22089: [ 0.550, 0.056 ], // Ice Tarantula
    22090: [ 0.570, 0.058 ], // Frost Tarantula
    22091: [ 0.623, 0.0 ], // Lost Iron Golem
    22092: [ 0.623, 0.0 ], // Frost Iron Golem
    22093: [ 0.910, 0.093 ], // Lost Buffalo
    22094: [ 0.553, 0.057 ], // Frost Buffalo
    22095: [ 0.593, 0.061 ], // Ursus Cub
    22096: [ 0.593, 0.061 ], // Ursus
    22097: [ 0.693, 0.071 ], // Lost Yeti
    22098: [ 0.717, 0.074 ], // Frost Yeti
}

const variableNames = {
    value: 'value',
}

export class AnIceMerchantsDream extends ListenerLogic {
    constructor() {
        super( 'Q00648_AnIceMerchantsDream', 'listeners/tracked-600/AnIceMerchantsDream.ts' )
        this.questId = 648
        this.questItemIds = [
            SILVER_HEMOCYTE,
            SILVER_ICE_CRYSTAL,
            BLACK_ICE_CRYSTAL,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ RAFFORTY ]
    }

    getTalkIds(): Array<number> {
        return [ RAFFORTY, ICE_SHELF ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                state.startQuest()
                if ( this.isCompanionQuestCompleted( data.playerId ) ) {
                    return this.getPath( '32020-04.htm' )
                }

                state.setConditionWithSound( 2 )
                return this.getPath( '32020-05.htm' )

            case 'ASK':
                return this.getPath( this.isCompanionQuestCompleted( data.playerId ) ? '32020-14.html' : '32020-15.html' )

            case 'LATER':
                return this.getPath( this.isCompanionQuestCompleted( data.playerId ) ? '32020-19.html' : '32020-20.html' )

            case 'REWARD':
                let silverCryCount = QuestHelper.getQuestItemsCount( player, SILVER_ICE_CRYSTAL )
                let blackCryCount = QuestHelper.getQuestItemsCount( player, BLACK_ICE_CRYSTAL )
                if ( ( silverCryCount + blackCryCount ) > 0 ) {
                    let adenaAmount: number = ( silverCryCount * 300 ) + ( blackCryCount * 1200 )
                    await QuestHelper.takeMultipleItems( player, -1, SILVER_ICE_CRYSTAL, BLACK_ICE_CRYSTAL )
                    await QuestHelper.giveAdena( player, adenaAmount, true )

                    return this.getPath( this.isCompanionQuestCompleted( data.playerId ) ? '32020-16.html' : '32020-17.html' )
                }

                return this.getPath( '32020-18.html' )

            case 'QUIT':
                if ( this.isCompanionQuestCompleted( data.playerId ) ) {
                    await state.exitQuest( true, true )
                    return this.getPath( '32020-21.html' )
                }

                return this.getPath( '32020-22.html' )

            case '32020-06.html':
            case '32020-07.html':
            case '32020-08.html':
            case '32020-09.html':
                break

            case '32020-23.html':
                await state.exitQuest( true, true )
                break

            case '32023-04.html':
                if ( QuestHelper.hasQuestItem( player, SILVER_ICE_CRYSTAL ) && !state.getVariable( variableNames.value ) ) {
                    state.setVariable( variableNames.value, ( _.random( 3 ) + 1 ) * 10 )
                    break
                }

                return

            case '32023-05.html':
                if ( QuestHelper.hasQuestItem( player, SILVER_ICE_CRYSTAL ) && state.getVariable( variableNames.value ) > 0 ) {
                    await QuestHelper.takeSingleItem( player, SILVER_ICE_CRYSTAL, 1 )

                    state.incrementVariable( variableNames.value, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_BROKEN_KEY )

                    break
                }

                return

            case '32023-06.html':
                if ( QuestHelper.hasQuestItems( player, SILVER_ICE_CRYSTAL ) && state.getVariable( variableNames.value ) > 0 ) {
                    await QuestHelper.takeSingleItem( player, SILVER_ICE_CRYSTAL, 1 )

                    state.incrementVariable( variableNames.value, 2 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_BROKEN_KEY )

                    break
                }

                return

            case 'REPLY4':
            case 'REPLY5':
                if ( state.getVariable( variableNames.value ) > 0 ) {
                    let value = state.getVariable( variableNames.value )
                    let modifier = data.eventName === 'REPLY5' ? 2 : 0
                    let firstValue = Math.floor( value / 10 )
                    let secondValue = value - ( firstValue * 10 ) + modifier

                    state.unsetVariable( variableNames.value )

                    if ( firstValue === secondValue ) {
                        await QuestHelper.giveSingleItem( player, BLACK_ICE_CRYSTAL, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_ENCHANT_SUCCESS )

                        return this.getPath( '32023-07.html' )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_ENCHANT_FAILED )
                    return this.getPath( '32023-08.html' )
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    isCompanionQuestCompleted( playerId: number ): boolean {
        let state: QuestState = QuestStateCache.getQuestState( playerId, 'Q00115_TheOtherSideOfTruth', false )
        return state && state.isCompleted()
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ firstChance, secondChance ]: RewardChances = monsterRewardChances[ data.npcId ]
        let shouldGiveCrystal = Math.random() < QuestHelper.getAdjustedChance( SILVER_ICE_CRYSTAL, firstChance, data.isChampion )
        let shouldGiveHemocyte = Math.random() < QuestHelper.getAdjustedChance( SILVER_HEMOCYTE, secondChance, data.isChampion )

        if ( !shouldGiveCrystal && !shouldGiveHemocyte ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )
        if ( !state ) {
            return
        }

        if ( shouldGiveCrystal && state.isStarted() ) {
            await QuestHelper.rewardSingleQuestItem( state.getPlayer(), SILVER_ICE_CRYSTAL, 1, data.isChampion )
        }

        if ( !shouldGiveHemocyte || state.getCondition() < 2 || !this.isCompanionQuestCompleted( state.getPlayerId() ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( state.getPlayer(), SILVER_HEMOCYTE, 1, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                switch ( data.characterNpcId ) {
                    case RAFFORTY:
                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '32020-01.htm' )
                        }

                        return this.getPath( this.isCompanionQuestCompleted( data.playerId ) ? '32020-02.htm' : '32020-03.htm' )

                    case ICE_SHELF:
                        return this.getPath( '32023-01.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case RAFFORTY:
                        let totalAmount: number = QuestHelper.getItemsSumCount( player, SILVER_ICE_CRYSTAL, BLACK_ICE_CRYSTAL )
                        if ( this.isCompanionQuestCompleted( data.playerId ) ) {
                            if ( state.isCondition( 1 ) ) {
                                state.setConditionWithSound( 2, true )
                            }

                            return this.getPath( totalAmount > 0 ? '32020-13.html' : '32020-11.html' )
                        }

                        return this.getPath( totalAmount > 0 ? '32020-12.html' : '32020-10.html' )

                    case ICE_SHELF:
                        if ( QuestHelper.hasQuestItem( player, SILVER_ICE_CRYSTAL ) ) {
                            let value = _.defaultTo( state.getVariable( variableNames.value ), 0 )
                            let adjustedValue = value % 10
                            if ( adjustedValue === 0 ) {
                                state.unsetVariable( variableNames.value )

                                return this.getPath( '32023-03.html' )
                            }

                            return this.getPath( '32023-09.html' )
                        }

                        return this.getPath( '32023-02.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00648_AnIceMerchantsDream'
    }
}