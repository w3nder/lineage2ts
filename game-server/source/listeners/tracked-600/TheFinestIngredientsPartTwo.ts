import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import {
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcSpawnEvent,
    NpcTalkEvent
} from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SpawnMakerCache } from '../../gameService/cache/SpawnMakerCache'
import { createItemDefinition } from '../../gameService/interface/ItemDefinition'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const JEREMY = 31521
const YETIS_TABLE = 31542
const ICICLE_EMPEROR_BUMBALUMP = 25296
const SOY_SOURCE_JAR = createItemDefinition( 7205, 1 )
const FOOD_FOR_BUMBALUMP = createItemDefinition( 7209, 1 )
const SPECIAL_YETI_MEAT = createItemDefinition( 7210, 1 )
const GREATER_DYE_OF_STR_1 = createItemDefinition( 4589, 5 )
const GREATER_DYE_OF_STR_2 = createItemDefinition( 4590, 5 )
const GREATER_DYE_OF_CON_1 = createItemDefinition( 4591, 5 )
const GREATER_DYE_OF_CON_2 = createItemDefinition( 4592, 5 )
const GREATER_DYE_OF_DEX_1 = createItemDefinition( 4593, 5 )
const GREATER_DYE_OF_DEX_2 = createItemDefinition( 4594, 5 )
const spawnLocation = new Location( 158240, -121536, -2222 )
const minimumLevel = 73

const eventNames = {
    npcSay: 'ns',
}

export class TheFinestIngredientsPartTwo extends ListenerLogic {
    constructor() {
        super( 'Q00625_TheFinestIngredientsPart2', 'listeners/tracked-600/TheFinestIngredientsPartTwo.ts' )
        this.questId = 625
        this.questItemIds = [
            FOOD_FOR_BUMBALUMP.id,
            SPECIAL_YETI_MEAT.id,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ JEREMY ]
    }

    getTalkIds(): Array<number> {
        return [ JEREMY, YETIS_TABLE ]
    }

    getSpawnIds(): Array<number> {
        return [ ICICLE_EMPEROR_BUMBALUMP ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ICICLE_EMPEROR_BUMBALUMP ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00625_TheFinestIngredientsPart2'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31521-04.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    await QuestHelper.takeSingleItem( player, SOY_SOURCE_JAR.id, SOY_SOURCE_JAR.count )
                    await QuestHelper.giveSingleItem( player, FOOD_FOR_BUMBALUMP.id, FOOD_FOR_BUMBALUMP.count )

                    break
                }

                return

            case '31521-08.html':
                if ( !state.isCondition( 3 ) ) {
                    return
                }

                if ( QuestHelper.hasItem( player, SPECIAL_YETI_MEAT ) ) {
                    await this.rewardPlayer( player )
                    await state.exitQuest( false, true )

                    break
                }

                return this.getPath( '31521-09.html' )

            case '31542-02.html':
                if ( !state.isCondition( 1 ) ) {
                    return
                }

                if ( QuestHelper.hasItem( player, FOOD_FOR_BUMBALUMP ) ) {
                    if ( !this.isBumbalumpSpawned() ) {
                        await QuestHelper.takeSingleItem( player, FOOD_FOR_BUMBALUMP.id, FOOD_FOR_BUMBALUMP.count )
                        state.setConditionWithSound( 2, true )

                        let monster: L2Npc = QuestHelper.addSpawnAtLocation( ICICLE_EMPEROR_BUMBALUMP, spawnLocation )
                        monster.setSummoner( player.getObjectId() )

                        break
                    }

                    return this.getPath( '31542-03.html' )
                }

                return this.getPath( '31542-04.html' )

            case eventNames.npcSay:
                if ( this.isBumbalumpSpawned() ) {
                    BroadcastHelper.broadcastNpcSayStringId( L2World.getObjectById( data.characterId ) as L2Npc, NpcSayType.NpcAll, NpcStringIds.OOOH )
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async rewardPlayer( player: L2PcInstance ): Promise<void> {
        let chance = Math.random()
        if ( chance < 0.167 ) {
            return QuestHelper.rewardSingleItem( player, GREATER_DYE_OF_STR_1.id, GREATER_DYE_OF_STR_1.count )
        }

        if ( chance < 0.334 ) {
            return QuestHelper.rewardSingleItem( player, GREATER_DYE_OF_STR_2.id, GREATER_DYE_OF_STR_2.count )
        }

        if ( chance < 0.501 ) {
            return QuestHelper.rewardSingleItem( player, GREATER_DYE_OF_CON_1.id, GREATER_DYE_OF_CON_1.count )
        }

        if ( chance < 0.668 ) {
            return QuestHelper.rewardSingleItem( player, GREATER_DYE_OF_CON_2.id, GREATER_DYE_OF_CON_2.count )
        }

        if ( chance < 0.835 ) {
            return QuestHelper.rewardSingleItem( player, GREATER_DYE_OF_DEX_1.id, GREATER_DYE_OF_DEX_1.count )
        }

        return QuestHelper.rewardSingleItem( player, GREATER_DYE_OF_DEX_2.id, GREATER_DYE_OF_DEX_2.count )
    }

    isBumbalumpSpawned() {
        return !!SpawnMakerCache.getFirstSpawn( ICICLE_EMPEROR_BUMBALUMP )
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        this.startQuestTimer( eventNames.npcSay, 1200000, data.characterId, 0 )
        return BroadcastHelper.broadcastNpcSayStringId( L2World.getObjectById( data.characterId ) as L2Npc, NpcSayType.NpcAll, NpcStringIds.I_SMELL_SOMETHING_DELICIOUS )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        if ( npc.getSummonerId() !== data.playerId ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 2, npc )
        if ( !state ) {
            return
        }

        await QuestHelper.giveSingleItem( state.getPlayer(), SPECIAL_YETI_MEAT.id, SPECIAL_YETI_MEAT.count )
        state.setConditionWithSound( 3, true )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== JEREMY ) {
                    break
                }

                if ( player.getLevel() >= minimumLevel ) {
                    return this.getPath( QuestHelper.hasItem( player, SOY_SOURCE_JAR ) ? '31521-01.htm' : '31521-02.htm' )
                }

                return this.getPath( '31521-03.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case JEREMY:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31521-05.html' )

                            case 2:
                                return this.getPath( '31521-06.html' )

                            case 3:
                                return this.getPath( '31521-07.html' )
                        }

                        break

                    case YETIS_TABLE:
                        switch ( state.getCondition() ) {
                            case 1:
                                if ( QuestHelper.hasItem( player, FOOD_FOR_BUMBALUMP ) ) {
                                    return this.getPath( '31542-01.html' )
                                }

                                break

                            case 2:
                                if ( !this.isBumbalumpSpawned() ) {
                                    let monster: L2Npc = QuestHelper.addSpawnAtLocation( ICICLE_EMPEROR_BUMBALUMP, spawnLocation )
                                    monster.setSummoner( player.getObjectId() )

                                    return this.getPath( '31542-02.html' )
                                }

                                return this.getPath( '31542-03.html' )

                            case 3:
                                return this.getPath( '31542-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId !== JEREMY ) {
                    break
                }

                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}