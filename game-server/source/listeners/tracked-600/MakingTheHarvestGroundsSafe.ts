import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const NORMAN = 30210
const BIG_HORNET_STING = 8283
const CLOUD_GEM = 8284
const YOUNG_ARANEID_CLAW = 8285
const minimumLevel = 21

type MonsterData = [ number, number ] // itemId, chance
const monsterRewards: { [ npcId: number ]: MonsterData } = {
    21095: [ BIG_HORNET_STING, 0.508 ], // Giant Poison Bee
    21096: [ CLOUD_GEM, 0.5 ], // Cloudy Beast
    21097: [ YOUNG_ARANEID_CLAW, 0.516 ], // Young Araneid
}

export class MakingTheHarvestGroundsSafe extends ListenerLogic {
    constructor() {
        super( 'Q00661_MakingTheHarvestGroundsSafe', 'listeners/tracked-600/MakingTheHarvestGroundsSafe.ts' )
        this.questId = 661
        this.questItemIds = [ BIG_HORNET_STING, CLOUD_GEM, YOUNG_ARANEID_CLAW ]
    }

    getQuestStartIds(): Array<number> {
        return [ NORMAN ]
    }

    getTalkIds(): Array<number> {
        return [ NORMAN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00661_MakingTheHarvestGroundsSafe'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30210-01.htm':
            case '30210-02.htm':
            case '30210-04.html':
            case '30210-06.html':
                break

            case '30210-03.htm':
                state.startQuest()
                break

            case '30210-08.html':
                let stingAmount = QuestHelper.getQuestItemsCount( player, BIG_HORNET_STING )
                let gemAmount = QuestHelper.getQuestItemsCount( player, CLOUD_GEM )
                let clawAmount = QuestHelper.getQuestItemsCount( player, YOUNG_ARANEID_CLAW )
                let adenaReward = ( 57 * stingAmount ) + ( 56 * gemAmount ) + ( 60 * clawAmount )

                if ( ( stingAmount + gemAmount + clawAmount ) >= 10 ) {
                    adenaReward += 5773
                }

                await QuestHelper.takeMultipleItems( player, -1, BIG_HORNET_STING, CLOUD_GEM, YOUNG_ARANEID_CLAW )
                await QuestHelper.giveAdena( player, adenaReward, true )

                break

            case '30210-09.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ itemId, chance ]: MonsterData = monsterRewards[ data.npcId ]
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30210-01.htm' : '30210-02.htm' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.hasQuestItems( player, BIG_HORNET_STING, CLOUD_GEM, YOUNG_ARANEID_CLAW ) ) {
                    return this.getPath( '30210-04.html' )
                }

                return this.getPath( '30210-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}