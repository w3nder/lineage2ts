import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'

import _ from 'lodash'

const INNOCENTIN = 31328
const TOTEM = 8068
const ANTEROOM_KEY = 8273
const CHAPEL_KEY = 8274
const KEY_OF_DARKNESS = 8275
const minimumLevel = 73
const requiredAmount = 2000
const SCROLL_ENCHANT_W_S = 959
const SCROLL_ENCHANT_A_S = 960

const monsterRewardChances: { [ npcId: number ]: number } = {
    22136: 0.55, // Gatekeeper Zombie
    22137: 0.06, // Penance Guard
    22138: 0.06, // Chapel Guard
    22139: 0.54, // Old Aristocrat's Soldier
    22140: 0.54, // Zombie Worker
    22141: 0.55, // Forgotten Victim
    22142: 0.54, // Triol's Layperson
    22143: 0.62, // Triol's Believer
    22144: 0.54, // Resurrected Temple Knight
    22145: 0.53, // Ritual Sacrifice
    22146: 0.54, // Triol's Priest
    22147: 0.55, // Ritual Offering
    22148: 0.45, // Triol's Believer
    22149: 0.54, // Ritual Offering
    22150: 0.46, // Triol's Believer
    22151: 0.62, // Triol's Priest
    22152: 0.55, // Temple Guard
    22153: 0.54, // Temple Guard Captain
    22154: 0.53, // Ritual Sacrifice
    22155: 0.75, // Triol's High Priest
    22156: 0.67, // Triol's Priest
    22157: 0.66, // Triol's Priest
    22158: 0.67, // Triol's Believer
    22159: 0.75, // Triol's High Priest
    22160: 0.67, // Triol's Priest
    22161: 0.78, // Ritual Sacrifice
    22162: 0.67, // Triol's Believer
    22163: 0.87, // Triol's High Priest
    22164: 0.67, // Triol's Believer
    22165: 0.66, // Triol's Priest
    22166: 0.66, // Triol's Believer
    22167: 0.75, // Triol's High Priest
    22168: 0.66, // Triol's Priest
    22169: 0.78, // Ritual Sacrifice
    22170: 0.67, // Triol's Believer
    22171: 0.87, // Triol's High Priest
    22172: 0.78, // Ritual Sacrifice
    22173: 0.66, // Triol's Priest
    22174: 0.67, // Triol's Priest
    22175: 0.03, // Andreas' Captain of the Royal Guard
    22176: 0.03, // Andreas' Royal Guards
    22188: 0.03, // Andreas' Captain of the Royal Guard
    22189: 0.03, // Andreas' Royal Guards
    22190: 0.03, // Ritual Sacrifice
    22191: 0.03, // Andreas' Captain of the Royal Guard
    22192: 0.03, // Andreas' Royal Guards
    22193: 0.03, // Andreas' Royal Guards
    22194: 0.03, // Penance Guard
}

type MonsterKeyDrop = [ number, number, number ] // itemId, chance, amount
const monsterKeyDrops: { [ npcId: number ]: MonsterKeyDrop } = {
    22143: [ CHAPEL_KEY, 100, 1 ], // Triol's Believer
    22146: [ KEY_OF_DARKNESS, 10, 1 ], // Triol's Priest
    22149: [ ANTEROOM_KEY, 100, 6 ], // Ritual Offering
    22151: [ KEY_OF_DARKNESS, 10, 1 ], // Triol's Priest
}

export class SeekersOfTheHolyGrail extends ListenerLogic {
    constructor() {
        super( 'Q00638_SeekersOfTheHolyGrail', 'listeners/tracked-600/SeekersOfTheHolyGrail.ts' )
        this.questId = 638
        this.questItemIds = [ TOTEM ]
    }

    getQuestStartIds(): Array<number> {
        return [ INNOCENTIN ]
    }

    getTalkIds(): Array<number> {
        return [ INNOCENTIN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31328-03.htm':
                state.startQuest()
                break

            case '31328-06.html':
                break

            case 'reward':
                if ( QuestHelper.getQuestItemsCount( player, TOTEM ) < requiredAmount ) {
                    return
                }

                await QuestHelper.takeSingleItem( player, TOTEM, requiredAmount )


                if ( Math.random() < 0.80 ) {
                    let itemId: number = Math.random() < 0.5 ? SCROLL_ENCHANT_A_S : SCROLL_ENCHANT_W_S
                    await QuestHelper.rewardSingleItem( player, itemId, 1 )

                    return this.getPath( '31328-07.html' )
                }

                await QuestHelper.giveAdena( player, 3576000, true )

                return this.getPath( '31328-08.html' )


            case '31328-09.html':
                await state.exitQuest( true, true )

                return this.getPath( '31328-09.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( TOTEM, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, npc )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, TOTEM, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

        if ( monsterKeyDrops[ data.npcId ] ) {
            let [ itemId, chance, amount ]: MonsterKeyDrop = monsterKeyDrops[ data.npcId ]
            if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
                return
            }

            let adjustedAmount: number = QuestHelper.getAdjustedAmount( itemId, amount, data.isChampion )
            npc.dropSingleItem( itemId, adjustedAmount, player.getObjectId() )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31328-01.htm' : '31328-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.getQuestItemsCount( player, TOTEM ) >= requiredAmount ? '31328-04.html' : '31328-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00638_SeekersOfTheHolyGrail'
    }
}