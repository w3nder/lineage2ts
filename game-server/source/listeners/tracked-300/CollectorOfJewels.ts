import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const NELL = 30376
const FLARE_SHARD = 5882
const FREEZING_SHARD = 5883
const minimumLevel = 25

type MonsterRewardData = [ number, number, number ] // itemId, chance, amount
const monsterRewards: { [ npcId: number ]: MonsterRewardData } = {
    20609: [ FLARE_SHARD, 0.75, 1 ], // salamander lakin
    20612: [ FLARE_SHARD, 0.91, 1 ], // salamander rowin
    20749: [ FLARE_SHARD, 1, 2 ], // death fire
    20616: [ FREEZING_SHARD, 0.81, 1 ], // undine lakin
    20619: [ FREEZING_SHARD, 0.87, 1 ], // undine rowin
    20747: [ FREEZING_SHARD, 1, 2 ], // roxide
}

export class CollectorOfJewels extends ListenerLogic {
    constructor() {
        super( 'Q00369_CollectorOfJewels', 'listeners/tracked-300/CollectorOfJewels.ts' )
        this.questId = 369
        this.questItemIds = [
            FLARE_SHARD,
            FREEZING_SHARD,
        ]
    }

    checkPartyMember( state: QuestState ): boolean {
        return [ 1, 3 ].includes( state.getMemoState() )
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00369_CollectorOfJewels'
    }

    getQuestStartIds(): Array<number> {
        return [ NELL ]
    }

    getTalkIds(): Array<number> {
        return [ NELL ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ itemId, chance, amount ]: MonsterRewardData = monsterRewards[ data.npcId ]
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 1, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        let maximumAmount = state.isMemoState( 1 ) ? 50 : 200
        let adjustedAmount = _.random( amount, maximumAmount )
        let player = state.getPlayer()

        await QuestHelper.rewardSingleQuestItem( player, itemId, adjustedAmount, data.isChampion )

        if ( QuestHelper.getItemsSumCount( player, FLARE_SHARD, FREEZING_SHARD ) >= ( maximumAmount * 2 ) ) {
            state.setConditionWithSound( state.isMemoState( 1 ) ? 2 : 4 )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30376-02.htm':
                state.startQuest()
                state.setMemoState( 1 )
                break

            case '30376-05.html':
                break

            case '30376-06.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '30376-07.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30376-01.htm' : '30376-03.html' )

            case QuestStateValues.STARTED:
                switch ( state.getMemoState() ) {
                    case 1:
                        if ( QuestHelper.getItemsSumCount( player, FLARE_SHARD, FREEZING_SHARD ) >= 100 ) {
                            await QuestHelper.giveAdena( player, 31810, true )
                            await QuestHelper.takeMultipleItems( player, -1, FLARE_SHARD, FREEZING_SHARD )
                            state.setMemoState( 2 )

                            return this.getPath( '30376-04.html' )
                        }

                        return this.getPath( '30376-08.html' )

                    case 2:
                        return this.getPath( '30376-09.html' )

                    case 3:
                        if ( QuestHelper.getItemsSumCount( player, FLARE_SHARD, FREEZING_SHARD ) >= 400 ) {
                            await QuestHelper.giveAdena( player, 84415, true )
                            await QuestHelper.takeMultipleItems( player, -1, FLARE_SHARD, FREEZING_SHARD )
                            await state.exitQuest( true, true )

                            return this.getPath( '30376-10.html' )
                        }

                        return this.getPath( '30376-11.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}