import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const WESLEY = 30166
const ALCHEMIST_MIXING_URN = 31149
const HALLATE_MAID = 20822
const HALLATE_GUARDIAN = 21061
const HAMES_ORC_SHAMAN = 21115
const LAVA_WYRM = 21111
const CRENDION = 20813
const PLATINUM_TRIBE_SHAMAN = 20828
const PLATINUM_GUARDIAN_SHAMAN = 21066
const REAGENT_POUNCH1 = 6007
const REAGENT_POUNCH2 = 6008
const REAGENT_POUNCH3 = 6009
const REAGENT_BOX = 6010
const WYRM_BLOOD = 6011
const LAVA_STONE = 6012
const MOONSTONE_SHARD = 6013
const ROTTEN_BONE = 6014
const DEMONS_BLOOD = 6015
const INFERNIUM_ORE = 6016
const BLOOD_ROOT = 6017
const VOLCANIC_ASH = 6018
const QUICKSILVER = 6019
const SULFUR = 6020
const DRACOPLASM = 6021
const MAGMA_DUST = 6022
const MOON_DUST = 6023
const NECROPLASM = 6024
const DEMONPLASM = 6025
const INFERNO_DUST = 6026
const DRACONIC_ESSENCE = 6027
const FIRE_ESSENCE = 6028
const LUNARGENT = 6029
const MIDNIGHT_OIL = 6030
const DEMONIC_ESSENCE = 6031
const ABYSS_OIL = 6032
const HELLFIRE_OIL = 6033
const NIGHTMARE_OIL = 6034
const PURE_SILVER = 6320
const MIXING_MANUAL = 6317
const WESLEYS_MIXING_STONE = 5904
const minimumLevel = 57

const eventMemoStates = {
    '31149-03.html': 11,
    '31149-04.html': 12,
    '31149-05.html': 13,
    '31149-06.html': 14,
    '31149-07.html': 15,
    '31149-08.html': 16,
    '31149-09.html': 17,
    '31149-10.html': 18,
    '31149-11.html': 19,
    '31149-12.html': 20,
    '31149-13.html': 21,
    '31149-14.html': 22,
    '31149-15.html': 23,
    '31149-16.html': 24,
    '31149-19.html': 1100,
    '31149-20.html': 1200,
    '31149-21.html': 1300,
    '31149-22.html': 1400,
    '31149-23.html': 1500,
    '31149-24.html': 1600,
}

type ItemReward = [ number, number ] // itemId, amount
const memoItemData: { [ memoState: number ]: ItemReward } = {
    11: [ WYRM_BLOOD, 10 ],
    12: [ LAVA_STONE, 10 ],
    13: [ MOONSTONE_SHARD, 10 ],
    14: [ ROTTEN_BONE, 10 ],
    15: [ DEMONS_BLOOD, 10 ],
    16: [ INFERNIUM_ORE, 10 ],
    17: [ DRACOPLASM, 10 ],
    18: [ MAGMA_DUST, 10 ],
    19: [ MOON_DUST, 10 ],
    20: [ NECROPLASM, 10 ],
    21: [ DEMONPLASM, 10 ],
    22: [ INFERNO_DUST, 10 ],
    23: [ FIRE_ESSENCE, 1 ],
    24: [ LUNARGENT, 1 ],
    1100: [ BLOOD_ROOT, 1 ],
    1200: [ VOLCANIC_ASH, 1 ],
    1300: [ QUICKSILVER, 1 ],
    1400: [ SULFUR, 1 ],
    1500: [ DEMONIC_ESSENCE, 1 ],
    1600: [ MIDNIGHT_OIL, 1 ],
}

type RewardProgress = [ number, string ] // itemId, eventName
const memoRewardProgress: { [ memoState: number ]: RewardProgress } = {
    1111: [ DRACOPLASM, '31149-30.html' ],
    1212: [ MAGMA_DUST, '31149-31.html' ],
    1213: [ MOON_DUST, '31149-32.html' ],
    1114: [ NECROPLASM, '31149-33.html' ],
    1115: [ DEMONPLASM, '31149-34.html' ],
    1216: [ INFERNO_DUST, '31149-35.html' ],
    1317: [ DRACONIC_ESSENCE, '31149-36.html' ],
    1418: [ FIRE_ESSENCE, '31149-37.html' ],
    1319: [ LUNARGENT, '31149-38.html' ],
    1320: [ MIDNIGHT_OIL, '31149-39.html' ],
    1421: [ DEMONIC_ESSENCE, '31149-40.html' ],
    1422: [ ABYSS_OIL, '31149-41.html' ],
    1523: [ HELLFIRE_OIL, '31149-42.html' ],
    1624: [ NIGHTMARE_OIL, '31149-43.html' ],
    1324: [ PURE_SILVER, '31149-46.html' ],
}

const variableNames = {
    value: 'vl'
}

export class SupplierOfReagents extends ListenerLogic {
    constructor() {
        super( 'Q00373_SupplierOfReagents', 'listeners/tracked-300/SupplierOfReagents.ts' )
        this.questId = 373
        this.questItemIds = [
            WESLEYS_MIXING_STONE,
            MIXING_MANUAL,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            HALLATE_GUARDIAN,
            HALLATE_MAID,
            HAMES_ORC_SHAMAN,
            LAVA_WYRM,
            CRENDION,
            PLATINUM_GUARDIAN_SHAMAN,
            PLATINUM_TRIBE_SHAMAN,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00373_SupplierOfReagents'
    }

    getQuestStartIds(): Array<number> {
        return [ WESLEY ]
    }

    getTalkIds(): Array<number> {
        return [
            WESLEY,
            ALCHEMIST_MIXING_URN,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        let chance = Math.random()
        switch ( data.npcId ) {
            case HALLATE_GUARDIAN:
                if ( chance < QuestHelper.getAdjustedChance( DEMONS_BLOOD, 0.766, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, DEMONS_BLOOD, 3, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    return
                }

                if ( chance < QuestHelper.getAdjustedChance( MOONSTONE_SHARD, 0.876, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, MOONSTONE_SHARD, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    return
                }

                return

            case HALLATE_MAID:
                if ( chance < QuestHelper.getAdjustedChance( REAGENT_POUNCH1, 0.45, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, REAGENT_POUNCH1, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    return
                }

                if ( chance < QuestHelper.getAdjustedChance( VOLCANIC_ASH, 0.65, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, VOLCANIC_ASH, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    return
                }

                return

            case HAMES_ORC_SHAMAN:
                if ( chance < QuestHelper.getAdjustedChance( REAGENT_POUNCH3, 0.616, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, REAGENT_POUNCH3, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case LAVA_WYRM:
                if ( chance < QuestHelper.getAdjustedChance( WYRM_BLOOD, 0.666, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, WYRM_BLOOD, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    return
                }

                if ( chance < QuestHelper.getAdjustedChance( LAVA_STONE, 0.989, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, LAVA_STONE, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    return
                }

                return

            case CRENDION:
                if ( chance < QuestHelper.getAdjustedChance( ROTTEN_BONE, 0.618, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, ROTTEN_BONE, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    return
                }

                await QuestHelper.rewardSingleQuestItem( player, QUICKSILVER, 1, data.isChampion )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                return

            case PLATINUM_GUARDIAN_SHAMAN:
                if ( chance < QuestHelper.getAdjustedChance( REAGENT_BOX, 0.444, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, REAGENT_BOX, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case PLATINUM_TRIBE_SHAMAN:
                if ( chance < QuestHelper.getAdjustedChance( REAGENT_POUNCH2, 0.658, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, REAGENT_POUNCH2, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    return
                }

                await QuestHelper.rewardSingleQuestItem( player, QUICKSILVER, 1, data.isChampion )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30166-03.htm':
            case '30166-06.html':
            case '30166-04a.html':
            case '30166-04b.html':
            case '30166-04c.html':
            case '30166-04d.html':
            case '31149-18.html':
                break

            case '30166-04.html':
                if ( player.getLevel() >= minimumLevel && state.isCreated() ) {
                    await QuestHelper.giveMultipleItems( player, 1, WESLEYS_MIXING_STONE, MIXING_MANUAL )
                    state.startQuest()

                    break
                }

                return

            case '30166-07.html':
                await state.exitQuest( true, true )
                break

            case '31149-02.html':
                state.setMemoState( 0 )
                state.setVariable( variableNames.value, 0 )
                break

            case '31149-03.html':
            case '31149-04.html':
            case '31149-05.html':
            case '31149-06.html':
            case '31149-07.html':
            case '31149-08.html':
            case '31149-09.html':
            case '31149-10.html':
            case '31149-11.html':
            case '31149-12.html':
            case '31149-13.html':
            case '31149-14.html':
            case '31149-15.html':
            case '31149-16.html':
            case '31149-19.html':
            case '31149-20.html':
            case '31149-21.html':
            case '31149-22.html':
            case '31149-23.html':
            case '31149-24.html':
                let memoStateValue: number = eventMemoStates[ data.eventName ]
                let [ itemId, amount ] = memoItemData[ memoStateValue ]
                if ( QuestHelper.getQuestItemsCount( player, itemId ) >= amount ) {
                    // If the player has the chosen item (ingredient or catalyst), we save it (for the catalyst or the reward)
                    state.setMemoState( state.getMemoState() + memoStateValue )
                    player.sendCopyData( SoundPacket.SKILLSOUND_LIQUID_MIX )

                    break
                }

                // If the player has not the chosen catalyst, we take the ingredient previously saved (if not null)
                let [ removeItemId, removeAmount ] = memoItemData[ state.getMemoState() ]
                await QuestHelper.takeSingleItem( player, removeItemId, removeAmount )

                return this.getPath( data.eventName === '31149-19.html' ? '31149-25.html' : '31149-17.html' )

            case '31149-26.html':
                if ( state.isMemoState( 1324 ) ) {
                    return this.getPath( '31149-26a.html' )
                }

                break

            case '31149-27.html':
                state.setVariable( variableNames.value, 1 ) // Temperature Salamander
                break

            case '31149-28a.html':
                state.setVariable( variableNames.value, _.random( 100 ) < 33 ? 3 : 0 )
                break

            case '31149-29a.html':
                state.setVariable( variableNames.value, _.random( 100 ) < 20 ? 5 : 0 )
                break

            case 'mixitems':
                let memoState = state.getMemoState()
                let firstItem: ItemReward = memoItemData[ memoState % 100 ]
                let secondItem: ItemReward = memoItemData[ Math.round( ( memoState / 100 ) * 100 ) ]
                let progress: RewardProgress = memoRewardProgress[ memoState ]

                if ( !progress || !state.getVariable( variableNames.value ) ) {
                    await QuestHelper.takeSingleItem( player, firstItem[ 0 ], firstItem[ 1 ] )
                    await QuestHelper.takeSingleItem( player, secondItem[ 0 ], secondItem[ 1 ] )

                    player.sendCopyData( SoundPacket.SKILLSOUND_LIQUID_FAIL )
                    return this.getPath( progress ? '31149-44.html' : '31149-45.html' )
                }

                let elixirQuest: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00235_MimirsElixir' )
                let [ rewardItemId, nextPagePath ] = progress

                if ( memoState !== 1324
                        || ( ( memoState === 1324 )
                                && elixirQuest
                                && elixirQuest.isStarted()
                                && !QuestHelper.hasQuestItem( player, rewardItemId ) ) ) {
                    if ( firstItem
                            && secondItem
                            && QuestHelper.getQuestItemsCount( player, firstItem[ 0 ] ) >= firstItem[ 1 ]
                            && QuestHelper.getQuestItemsCount( player, secondItem[ 0 ] ) >= secondItem[ 1 ] ) {
                        await QuestHelper.takeSingleItem( player, firstItem[ 0 ], firstItem[ 1 ] )
                        await QuestHelper.takeSingleItem( player, secondItem[ 0 ], secondItem[ 1 ] )

                        await QuestHelper.giveSingleItem( player, rewardItemId, ( memoState === 1324 ) ? 1 : state.getVariable( variableNames.value ) )
                        state.setMemoState( 0 )
                        state.setVariable( variableNames.value, 0 )

                        player.sendCopyData( SoundPacket.SKILLSOUND_LIQUID_SUCCESS )
                        return this.getPath( nextPagePath )

                    }

                    player.sendCopyData( SoundPacket.SKILLSOUND_LIQUID_FAIL )
                    return this.getPath( '31149-44.html' )
                }

                return this.getPath( '31149-44.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '30166-01.html' : '30166-02.htm' )

            case QuestStateValues.STARTED:
                if ( data.characterNpcId === WESLEY ) {
                    return this.getPath( '30166-05.html' )
                }

                return this.getPath( '31149-01.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}