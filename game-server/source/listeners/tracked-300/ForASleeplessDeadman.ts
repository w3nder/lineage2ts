import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ORVEN = 30857
const REMAINS_OF_ADEN_RESIDENTS = 5869
const minimumLevel = 60
const requiredItemAmount = 60

const rewardItemIds = [
    5494, // Sealed Dark Crystal Shield Fragment
    5495, // Sealed Shield of Nightmare Fragment
    6341, // Sealed Phoenix Earring Gemstone
    6342, // Sealed Majestic Earring Gemstone
    6343, // Sealed Phoenix Necklace Beads
    6344, // Sealed Majestic Necklace Beads
    6345, // Sealed Phoenix Ring Gemstone
    6346, // Sealed Majestic Ring Gemstone
]

const monsterRewardChances = {
    21006: 0.365, // doom servant
    21007: 0.392, // doom guard
    21008: 0.503, // doom archer
}

export class ForASleeplessDeadman extends ListenerLogic {
    constructor() {
        super( 'Q00359_ForASleeplessDeadman', 'listeners/tracked-300/ForASleeplessDeadman.ts' )
        this.questId = 359
        this.questItemIds = [
            REMAINS_OF_ADEN_RESIDENTS,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00359_ForASleeplessDeadman'
    }

    getQuestStartIds(): Array<number> {
        return [ ORVEN ]
    }

    getTalkIds(): Array<number> {
        return [ ORVEN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( REMAINS_OF_ADEN_RESIDENTS, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardAndProgressState( state.getPlayer(), state, REMAINS_OF_ADEN_RESIDENTS, 1, requiredItemAmount, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30857-02.htm':
            case '30857-03.htm':
            case '30857-04.htm':
                break

            case '30857-05.htm':
                state.startQuest()
                state.setMemoState( 1 )
                break

            case '30857-10.html':
                await QuestHelper.rewardSingleItem( L2World.getPlayer( data.playerId ), _.sample( rewardItemIds ), 4 )
                await state.exitQuest( true, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30857-01.htm' : '30857-06.html' )

            case QuestStateValues.STARTED:
                switch ( state.getMemoState() ) {
                    case 1:
                        if ( QuestHelper.getQuestItemsCount( player, REMAINS_OF_ADEN_RESIDENTS ) < requiredItemAmount ) {
                            return this.getPath( '30857-07.html' )
                        }

                        await QuestHelper.takeSingleItem( player, REMAINS_OF_ADEN_RESIDENTS, -1 )

                        state.setMemoState( 2 )
                        state.setConditionWithSound( 3, true )

                        return this.getPath( '30857-08.html' )

                    case 2:
                        return this.getPath( '30857-09.html' )
                }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}