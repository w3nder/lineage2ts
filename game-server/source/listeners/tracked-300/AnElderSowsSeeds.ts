import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const CASIAN = 30612
const SPELLBOOK_PAGE = 5916
const CHAPTER_OF_FIRE = 5917
const CHAPTER_OF_WATER = 5918
const CHAPTER_OF_WIND = 5919
const CHAPTER_OF_EARTH = 5920
const minimumLevel = 28

const monsterRewardChances = {
    20082: 0.09, // ant recruit
    20086: 0.09, // ant guard
    20090: 0.22, // noble ant leader
    20084: 0.101, // ant patrol
    20089: 0.100, // noble ant
}

export class AnElderSowsSeeds extends ListenerLogic {
    constructor() {
        super( 'Q00370_AnElderSowsSeeds', 'listeners/tracked-300/AnElderSowsSeeds.ts' )
        this.questId = 370
    }

    async exchangeChapters( player: L2PcInstance, removeAll: boolean ): Promise<boolean> {
        let waterChapters = QuestHelper.getQuestItemsCount( player, CHAPTER_OF_WATER )
        let earthChapters = QuestHelper.getQuestItemsCount( player, CHAPTER_OF_EARTH )
        let windChapters = QuestHelper.getQuestItemsCount( player, CHAPTER_OF_WIND )
        let fireChapters = QuestHelper.getQuestItemsCount( player, CHAPTER_OF_FIRE )

        let adenaMultiplier = Math.min( waterChapters, earthChapters, windChapters, fireChapters )

        if ( adenaMultiplier > 0 ) {
            await QuestHelper.giveAdena( player, adenaMultiplier * 3600, true )
        }

        let amountToRemove = removeAll ? -1 : adenaMultiplier
        await QuestHelper.takeMultipleItems( player, amountToRemove, CHAPTER_OF_WATER, CHAPTER_OF_EARTH, CHAPTER_OF_WIND, CHAPTER_OF_FIRE )

        return adenaMultiplier > 0
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00370_AnElderSowsSeeds'
    }

    getQuestStartIds(): Array<number> {
        return [ CASIAN ]
    }

    getTalkIds(): Array<number> {
        return [ CASIAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( SPELLBOOK_PAGE, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let chance = [ 20084, 20089 ].includes( data.npcId ) ? 3 : 1
        let player = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, chance, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, SPELLBOOK_PAGE, 1, data.isChampion )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30612-02.htm':
            case '30612-03.htm':
            case '30612-06.html':
            case '30612-07.html':
            case '30612-09.html':
                break

            case '30612-04.htm':
                state.startQuest()
                break

            case 'REWARD':
                if ( await this.exchangeChapters( player, false ) ) {
                    return this.getPath( '30612-08.html' )
                }

                return this.getPath( '30612-11.html' )

            case '30612-10.html':
                await this.exchangeChapters( player, true )
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30612-01.htm' : '30612-05.html' )

            case QuestStateValues.STARTED:
                return this.getPath( '30612-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}