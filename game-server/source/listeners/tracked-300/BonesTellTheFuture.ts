import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const TETRACH_KAITAR = 30359
const BONE_FRAGMENT = 809
const minimumLevel = 10
const REQUIRED_BONE_COUNT = 10
const DROP_CHANCE = 0.18

export class BonesTellTheFuture extends ListenerLogic {
    constructor() {
        super( 'Q00320_BonesTellTheFuture', 'listeners/tracked-300/BonesTellTheFuture.ts' )
        this.questId = 320
        this.questItemIds = [
            BONE_FRAGMENT,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20517, // Skeleton Hunter
            20518, // Skeleton Hunter Archer
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ TETRACH_KAITAR ]
    }

    getTalkIds(): Array<number> {
        return [ TETRACH_KAITAR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00320_BonesTellTheFuture'
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        if ( Math.random() >= QuestHelper.getAdjustedChance( BONE_FRAGMENT, DROP_CHANCE, data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardAndProgressState( state.getPlayer(), state, BONE_FRAGMENT, 1, REQUIRED_BONE_COUNT, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30359-04.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.DARK_ELF ) {
                    return this.getPath( '30359-00.htm' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30359-03.htm' : '30359-02.htm' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.getQuestItemsCount( player, BONE_FRAGMENT ) < REQUIRED_BONE_COUNT ) {
                    return this.getPath( '30359-05.html' )
                }

                await QuestHelper.giveAdena( player, 8470, true )
                await state.exitQuest( true, true )
                return this.getPath( '30359-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}