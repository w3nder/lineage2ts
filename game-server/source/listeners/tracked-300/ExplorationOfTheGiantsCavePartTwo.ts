import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const SOBLING = 31147
const TITAN_ANCIENT_BOOK = 14847
const BOOK1 = 14842
const BOOK2 = 14843
const BOOK3 = 14844
const BOOK4 = 14845
const BOOK5 = 14846
const minimumLevel = 79

const monsterRewardChances = {
    22660: 366, // lesser giant re
    22661: 424, // lesser giant soldier re
    22662: 304, // lesser giant shooter re
    22663: 304, // lesser giant scout re
    22664: 354, // lesser giant mage re
    22665: 324, // lesser giant elder re
    22666: 0.276, // barif re
    22667: 0.284, // barif pet re
    22668: 0.240, // gamlin re
    22669: 0.240, // leogul re
}

export class ExplorationOfTheGiantsCavePartTwo extends ListenerLogic {
    constructor() {
        super( 'Q00377_ExplorationOfTheGiantsCavePart2', 'listeners/tracked-300/ExplorationOfTheGiantsCavePartTwo.ts' )
        this.questId = 377
        this.questItemIds = [
            TITAN_ANCIENT_BOOK,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00377_ExplorationOfTheGiantsCavePart2'
    }

    getQuestStartIds(): Array<number> {
        return [ SOBLING ]
    }

    getTalkIds(): Array<number> {
        return [ SOBLING ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        let isDirectChance = [ 22666, 22667, 22668, 22669 ].includes( data.npcId )
        let amount: number = isDirectChance ? 1 : 3
        if ( Math.random() > QuestHelper.getAdjustedChance( TITAN_ANCIENT_BOOK, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            if ( isDirectChance ) {
                return
            }

            amount = 2
        }

        await QuestHelper.rewardSingleQuestItem( player, TITAN_ANCIENT_BOOK, amount, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31147-02.htm':
                state.startQuest()
                break

            case '31147-04.html':
            case '31147-cont.html':
                break

            case '31147-quit.html':
                await state.exitQuest( true, true )
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31147-01.htm' : '31147-00.html' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItems( player, BOOK1, BOOK2, BOOK3, BOOK4, BOOK5 ) ? '31147-03.html' : '31147-02a.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}