import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const BELTON = 30125
const HARPY_FEATHER = 1452
const MEDUSA_VENOM = 1453
const WYRMS_TOOTH = 1454
const minimumLevel = 32
const HARPY_FEATHER_ADENA = 78
const MEDUSA_VENOM_ADENA = 88
const WYRMS_TOOTH_ADENA = 92
const BONUS = 3100
const BONUS_COUNT = 10

type MonsterReward = [ number, number ] // itemId, chance
const monsterRewardChances: { [ npcId: number ]: MonsterReward } = {
    20145: [ HARPY_FEATHER, 0.59 ], // Harpy
    20158: [ MEDUSA_VENOM, 0.61 ], // Medusa
    20176: [ WYRMS_TOOTH, 0.60 ], // Wyrm
}

export class ArrowOfVengeance extends ListenerLogic {
    constructor() {
        super( 'Q00331_ArrowOfVengeance', 'listeners/tracked-300/ArrowOfVengeance.ts' )
        this.questId = 331
        this.questItemIds = [
            HARPY_FEATHER,
            MEDUSA_VENOM,
            WYRMS_TOOTH,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00331_ArrowOfVengeance'
    }

    getQuestStartIds(): Array<number> {
        return [ BELTON ]
    }

    getTalkIds(): Array<number> {
        return [ BELTON ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let [ itemId, chance ] = monsterRewardChances[ data.npcId ]
        if ( Math.random() >= QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30125-03.htm':
                state.startQuest()
                break

            case '30125-06.html':
                await state.exitQuest( true, true )
                break

            case '30125-07.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '30125-01.htm' : '30125-02.htm' )

            case QuestStateValues.STARTED:
                const harpyFeathers = QuestHelper.getQuestItemsCount( player, HARPY_FEATHER )
                const medusaVenoms = QuestHelper.getQuestItemsCount( player, MEDUSA_VENOM )
                const wyrmsTeeth = QuestHelper.getQuestItemsCount( player, WYRMS_TOOTH )
                const totalAmount = harpyFeathers + medusaVenoms + wyrmsTeeth

                if ( totalAmount === 0 ) {
                    return this.getPath( '30125-04.html' )
                }

                let adena = ( harpyFeathers * HARPY_FEATHER_ADENA ) + ( medusaVenoms * MEDUSA_VENOM_ADENA ) + ( wyrmsTeeth * WYRMS_TOOTH_ADENA ) + ( totalAmount >= BONUS_COUNT ? BONUS : 0 )
                await QuestHelper.giveAdena( player, adena, true )
                await QuestHelper.takeMultipleItems( player, -1, HARPY_FEATHER, MEDUSA_VENOM, WYRMS_TOOTH )

                return this.getPath( '30125-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}