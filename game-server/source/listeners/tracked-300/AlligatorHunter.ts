import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ENVERUN = 30892
const ALLIGATOR = 20135
const ALLIGATOR_LEATHER = 4337
const minimumLevel = 40
const SECOND_CHANCE = 0.19

export class AlligatorHunter extends ListenerLogic {
    constructor() {
        super( 'Q00338_AlligatorHunter', 'listeners/tracked-300/AlligatorHunter.ts' )
        this.questId = 338
        this.questItemIds = [
            ALLIGATOR_LEATHER,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ALLIGATOR ]
    }

    getQuestStartIds(): Array<number> {
        return [ ENVERUN ]
    }

    getTalkIds(): Array<number> {
        return [ ENVERUN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        await QuestHelper.rewardSingleQuestItem( player, ALLIGATOR_LEATHER, 1, data.isChampion )

        if ( Math.random() < QuestHelper.getAdjustedChance( ALLIGATOR_LEATHER, SECOND_CHANCE, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, ALLIGATOR_LEATHER, 1, data.isChampion )
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30892-03.htm':
                state.startQuest()
                break

            case '30892-06.html':
                let player = L2World.getPlayer( data.playerId )
                if ( !QuestHelper.hasQuestItem( player, ALLIGATOR_LEATHER ) ) {
                    return '30892-05.html'
                }

                let leatherAmount: number = QuestHelper.getQuestItemsCount( player, ALLIGATOR_LEATHER )
                let adenaReward: number = 60 * leatherAmount + ( leatherAmount >= 10 ? 3430 : 0 )

                await QuestHelper.giveAdena( player, adenaReward, true )
                await QuestHelper.takeSingleItem( player, ALLIGATOR_LEATHER, -1 )

                break

            case '30892-10.html':
                await state.exitQuest( true, true )
                break

            case '30892-07.html':
            case '30892-08.html':
            case '30892-09.html':
                break

            default:
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30892-02.htm' : '30892-01.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( '30892-04.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00338_AlligatorHunter'
    }
}