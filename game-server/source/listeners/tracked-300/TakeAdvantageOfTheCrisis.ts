import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const FILAUR = 30535
const MINERAL_FRAGMENT = 14875
const minimumLevel = 80

const monsterRewardChances = {
    22678: 291, // Grave Robber Summoner (Lunatic)
    22679: 596, // Grave Robber Magician (Lunatic)
    22680: 610, // Grave Robber Worker (Lunatic)
    22681: 626, // Grave Robber Warrior (Lunatic)
    22682: 692, // Grave Robber Warrior of Light (Lunatic)
    22683: 650, // Servitor of Darkness
    22684: 310, // Servitor of Darkness
    22685: 626, // Servitor of Darkness
    22686: 626, // Servitor of Darkness
    22687: 308, // Phantoms of the Mine
    22688: 416, // Evil Spirits of the Mine
    22689: 212, // Mine Bug
    22690: 748, // Earthworm's Descendant
}

export class TakeAdvantageOfTheCrisis extends ListenerLogic {
    constructor() {
        super( 'Q00312_TakeAdvantageOfTheCrisis', 'listeners/tracked-300/TakeAdvantageOfTheCrisis.ts' )
        this.questId = 312
        this.questItemIds = [
            MINERAL_FRAGMENT,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00312_TakeAdvantageOfTheCrisis'
    }

    getQuestStartIds(): Array<number> {
        return [ FILAUR ]
    }

    getTalkIds(): Array<number> {
        return [ FILAUR ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        if ( _.random( 1000 ) >= QuestHelper.getAdjustedChance( MINERAL_FRAGMENT, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, MINERAL_FRAGMENT, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30535-02.html':
            case '30535-03.html':
            case '30535-04.html':
            case '30535-05.htm':
            case '30535-09.html':
            case '30535-10.html':
                break

            case '30535-06.htm':
                state.startQuest()
                break

            case '30535-11.html':
                await state.exitQuest( true, true )
                break

            default:
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30535-01.htm' : '30535-00.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItem( player, MINERAL_FRAGMENT ) ? '30535-08.html' : '30535-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}