import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2World } from '../../gameService/L2World'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const ELLENIA = 30155
const WERERAT_FANG = 1042
const VAROOL_FOULCLAW_FANG = 1043
const minimumLevel = 18
const VAROOL_FOULCLAW = 27020

type MonsterReward = [ number, number, number ] // chance, itemId, amount
const monsterRewards: { [ npcId: number ]: MonsterReward } = {
    20040: [ 0.5, WERERAT_FANG, 5 ], // Sukar Wererat
    20047: [ 0.5, WERERAT_FANG, 5 ], // Sukar Wererat Leader
    [ VAROOL_FOULCLAW ]: [ 0.7, VAROOL_FOULCLAW_FANG, 7 ], // Varool Foulclaw
}

const variableNames = {
    isAttacked: 'a',
    hasClaw: 'c',
}

export class DestroyPlagueCarriers extends ListenerLogic {
    constructor() {
        super( 'Q00316_DestroyPlagueCarriers', 'listeners/tracked-300/DestroyPlagueCarriers.ts' )
        this.questId = 316
        this.questItemIds = [
            WERERAT_FANG,
            VAROOL_FOULCLAW_FANG,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [ VAROOL_FOULCLAW ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00316_DestroyPlagueCarriers'
    }

    getQuestStartIds(): Array<number> {
        return [ ELLENIA ]
    }

    getTalkIds(): Array<number> {
        return [ ELLENIA ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        if ( !NpcVariablesManager.get( data.targetId, variableNames.isAttacked ) ) {
            let npc = L2World.getObjectById( data.targetId )
            BroadcastHelper.broadcastNpcSayStringId( npc as L2Npc, NpcSayType.NpcAll, NpcStringIds.WHY_DO_YOU_OPPRESS_US_SO )
            NpcVariablesManager.set( data.targetId, variableNames.isAttacked, true )
        }

        return
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        let [ chance, itemId, amount ] = monsterRewards[ data.npcId ]
        if ( Math.random() >= QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let player = state.getPlayer()
        if ( data.npcId === VAROOL_FOULCLAW ) {
            if ( state.getVariable( variableNames.hasClaw ) ) {
                return
            }

            await QuestHelper.giveSingleItem( player, itemId, 1 )

            state.setVariable( variableNames.hasClaw, true )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, amount, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30155-04.htm':
                state.startQuest()
                break

            case '30155-08.html':
                await state.exitQuest( true, true )
                break

            case '30155-09.html':
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.ELF ) {
                    return this.getPath( '30155-00.htm' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30155-02.htm' )
                }

                return this.getPath( '30155-03.htm' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.hasAtLeastOneQuestItem( player, ...this.questItemIds ) ) {
                    let fangs = QuestHelper.getQuestItemsCount( player, WERERAT_FANG )
                    let claws = QuestHelper.getQuestItemsCount( player, VAROOL_FOULCLAW_FANG )

                    let amount: number = ( fangs * 30 ) + ( claws * 10000 ) + ( ( fangs + claws ) >= 10 ? 5000 : 0 )

                    await QuestHelper.giveAdena( player, amount, true )
                    await QuestHelper.takeMultipleItems( player, -1, ...this.questItemIds )

                    return this.getPath( '30155-07.html' )
                }

                return this.getPath( '30155-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}