import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ESPEN = 30890
const PIRATES_CHEST = 31148
const THIEF_KEY = 1661
const PIRATES_TREASURE_MAP = 5915
const minimumLevel = 42
const SCROLL_ENCHANT_ARMOR_C = 952
const SCROLL_ENCHANT_ARMOR_D = 956
const EMERALD = 1337
const BLUE_ONYX = 1338
const ONYX = 1339
const MITHRIL_GLOVES = 2450
const SAGES_WORN_GLOVES = 2451
const MOONSTONE = 3447
const ALEXANDRITE = 3450
const FIRE_EMERALD = 3453
const IMPERIAL_DIAMOND = 3456
const MUSICAL_SCORE_THEME_OF_LOVE = 4408
const MUSICAL_SCORE_THEME_OF_BATTLE = 4409
const MUSICAL_SCORE_THEME_OF_CELEBRATION = 4418
const MUSICAL_SCORE_THEME_OF_COMEDY = 4419
const DYE_S1C3_C = 4481 // Greater Dye of STR <Str+1 Con-3>
const DYE_S1D3_C = 4482 // Greater Dye of STR <Str+1 Dex-3>
const DYE_C1S3_C = 4483 // Greater Dye of CON<Con+1 Str-3>
const DYE_C1C3_C = 4484 // Greater Dye of CON<Con+1 Dex-3>
const DYE_D1S3_C = 4485 // Greater Dye of DEX <Dex+1 Str-3>
const DYE_D1C3_C = 4486 // Greater Dye of DEX <Dex+1 Con-3>
const DYE_I1M3_C = 4487 // Greater Dye of INT <Int+1 Men-3>
const DYE_I1W3_C = 4488 // Greater Dye of INT <Int+1 Wit-3>
const DYE_M1I3_C = 4489 // Greater Dye of MEN <Men+1 Int-3>
const DYE_M1W3_C = 4490 // Greater Dye of MEN <Men+1 Wit-3>
const DYE_W1I3_C = 4491 // Greater Dye of WIT <Wit+1 Int-3>
const DYE_W1M3_C = 4492 // Greater Dye of WIT <Wit+1 Men-3>

export class TreasureHunt extends ListenerLogic {
    constructor() {
        super( 'Q00383_TreasureHunt', 'listeners/tracked-300/TreasureHunt.ts' )
        this.questId = 383
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00383_TreasureHunt'
    }

    getQuestStartIds(): Array<number> {
        return [ ESPEN ]
    }

    getTalkIds(): Array<number> {
        return [ ESPEN, PIRATES_CHEST ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30890-04.htm':
                break

            case '30890-05.htm':
                if ( QuestHelper.hasQuestItem( player, PIRATES_TREASURE_MAP ) ) {
                    await QuestHelper.takeSingleItem( player, PIRATES_TREASURE_MAP, 1 )
                    await QuestHelper.giveAdena( player, 1000, false )

                    break
                }

                return

            case '30890-06.htm':
                return this.getPath( QuestHelper.hasQuestItem( player, PIRATES_TREASURE_MAP ) ? data.eventName : '30890-12.html' )

            case '30890-07.htm':
                if ( QuestHelper.hasQuestItem( player, PIRATES_TREASURE_MAP ) ) {
                    state.startQuest()
                    await QuestHelper.takeSingleItem( player, PIRATES_TREASURE_MAP, 1 )

                    break
                }

                return

            case '30890-08.html':
            case '30890-09.html':
            case '30890-10.html':
                if ( state.isCondition( 1 ) ) {
                    break
                }

                return

            case '30890-11.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '31148-02.html':
                if ( !state.isCondition( 2 ) ) {
                    return
                }

                if ( QuestHelper.hasQuestItem( player, THIEF_KEY ) ) {
                    await QuestHelper.takeSingleItem( player, THIEF_KEY, 1 )


                    let adena: number = await this.rewardItems( player )

                    if ( adena > 0 ) {
                        await QuestHelper.giveAdena( player, adena, true )
                    }

                    await state.exitQuest( true, true )

                    break
                }

                return this.getPath( '31148-03.html' )


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30890-01.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, PIRATES_TREASURE_MAP ) ) {
                    return this.getPath( '30890-02.html' )
                }

                return this.getPath( '30890-03.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case PIRATES_CHEST:
                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '31148-01.html' )
                        }

                        break

                    case ESPEN:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30890-13.html' )
                        }

                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '30890-14.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardItems( player: L2PcInstance ): Promise<number> {
        let bonus = 0
        let random = Math.random()

        if ( random < 0.05 ) {
            await QuestHelper.rewardSingleItem( player, MITHRIL_GLOVES, 1 )
        } else if ( random < 0.06 ) {
            await QuestHelper.rewardSingleItem( player, SAGES_WORN_GLOVES, 1 )
        } else if ( random < 0.18 ) {
            await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_ARMOR_D, 1 )
        } else if ( random < 0.28 ) {
            await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_ARMOR_C, 1 )
        } else {
            bonus += 500
        }

        random = Math.random()

        if ( random < 0.025 ) {
            await QuestHelper.rewardSingleItem( player, DYE_S1C3_C, 1 )
        } else if ( random < 0.050 ) {
            await QuestHelper.rewardSingleItem( player, DYE_S1D3_C, 1 )
        } else if ( random < 0.075 ) {
            await QuestHelper.rewardSingleItem( player, DYE_C1S3_C, 1 )
        } else if ( random < 0.1 ) {
            await QuestHelper.rewardSingleItem( player, DYE_C1C3_C, 1 )
        } else if ( random < 0.125 ) {
            await QuestHelper.rewardSingleItem( player, DYE_D1S3_C, 1 )
        } else if ( random < 0.150 ) {
            await QuestHelper.rewardSingleItem( player, DYE_D1C3_C, 1 )
        } else if ( random < 0.175 ) {
            await QuestHelper.rewardSingleItem( player, DYE_I1M3_C, 1 )
        } else if ( random < 0.200 ) {
            await QuestHelper.rewardSingleItem( player, DYE_I1W3_C, 1 )
        } else if ( random < 0.225 ) {
            await QuestHelper.rewardSingleItem( player, DYE_M1I3_C, 1 )
        } else if ( random < 0.250 ) {
            await QuestHelper.rewardSingleItem( player, DYE_M1W3_C, 1 )
        } else if ( random < 0.275 ) {
            await QuestHelper.rewardSingleItem( player, DYE_W1I3_C, 1 )
        } else if ( random < 0.3 ) {
            await QuestHelper.rewardSingleItem( player, DYE_W1M3_C, 1 )
        } else {
            bonus += 300
        }

        random = Math.random()

        if ( random < 0.04 ) {
            await QuestHelper.rewardSingleItem( player, EMERALD, 1 )
        } else if ( random < 0.08 ) {
            await QuestHelper.rewardSingleItem( player, BLUE_ONYX, 2 )
        } else if ( random < 0.12 ) {
            await QuestHelper.rewardSingleItem( player, ONYX, 2 )
        } else if ( random < 0.16 ) {
            await QuestHelper.rewardSingleItem( player, MOONSTONE, 2 )
        } else if ( random < 0.20 ) {
            await QuestHelper.rewardSingleItem( player, ALEXANDRITE, 1 )
        } else if ( random < 0.25 ) {
            await QuestHelper.rewardSingleItem( player, FIRE_EMERALD, 1 )
        } else if ( random < 0.27 ) {
            await QuestHelper.rewardSingleItem( player, IMPERIAL_DIAMOND, 1 )
        } else {
            bonus += 500
        }

        random = Math.random()

        if ( random < 0.2 ) {
            await QuestHelper.rewardSingleItem( player, MUSICAL_SCORE_THEME_OF_LOVE, 1 )
        } else if ( random < 0.4 ) {
            await QuestHelper.rewardSingleItem( player, MUSICAL_SCORE_THEME_OF_BATTLE, 1 )
        } else if ( random < 0.6 ) {
            await QuestHelper.rewardSingleItem( player, MUSICAL_SCORE_THEME_OF_CELEBRATION, 1 )
        } else if ( random < 0.8 ) {
            await QuestHelper.rewardSingleItem( player, MUSICAL_SCORE_THEME_OF_COMEDY, 1 )
        } else {
            bonus += 500
        }

        return bonus
    }
}