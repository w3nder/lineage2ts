import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const MINIA = 30029
const ORCISH_ARROWHEAD = 963
const minimumLevel = 10
const REQUIRED_ITEM_COUNT = 10
const TUNATH_ORC_MARKSMAN = 20361

export class CollectArrowheads extends ListenerLogic {
    constructor() {
        super( 'Q00303_CollectArrowheads', 'listeners/tracked-300/CollectArrowheads.ts' )
        this.questId = 303
        this.questItemIds = [
            ORCISH_ARROWHEAD,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            TUNATH_ORC_MARKSMAN,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00303_CollectArrowheads'
    }

    getQuestStartIds(): Array<number> {
        return [ MINIA ]
    }

    getTalkIds(): Array<number> {
        return [ MINIA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )

        if ( !player || Math.random() >= QuestHelper.getAdjustedChance( ORCISH_ARROWHEAD, 0.4, data.isChampion ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
        await QuestHelper.rewardAndProgressState( player, state, ORCISH_ARROWHEAD, 1, REQUIRED_ITEM_COUNT, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30029-04.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30029-03.htm' : '30029-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30029-05.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, ORCISH_ARROWHEAD ) < REQUIRED_ITEM_COUNT ) {
                            return this.getPath( '30029-05.html' )
                        }

                        await QuestHelper.giveAdena( player, 1000, true )
                        await QuestHelper.addExpAndSp( player, 2000, 0 )
                        await state.exitQuest( true, true )

                        return this.getPath( '30029-06.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}