import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const COLLOB = 30092
const RANDOLF = 30095
const PIRATES_TREASURE_CHEST = 5873
const ENCHANT_WEAPON_C = 951
const ENCHANT_ARMOR_C = 952
const ENCHANT_WEAPON_D = 955
const ENCHANT_ARMOR_D = 956
const THREAD = 1868
const ANIMAL_BONE = 1872
const COKES = 1879
const STEEL = 1880
const COARSE_BONE_POWDER = 1881
const LEATHER = 1882
const CORD = 1884
const minimumLevel = 39

const poisonSkill = 4035
const monsterRewardChances = {
    20836: 0.47, // pirates zombie
    20845: 0.40, // pirates zombie captain
    21629: 0.40, // pirates zombie captain 1
    21630: 0.40, // pirates zombie captain 2
}

export class DevilsLegacy extends ListenerLogic {
    constructor() {
        super( 'Q00365_DevilsLegacy', 'listeners/tracked-300/DevilsLegacy.ts' )
        this.questId = 365
        this.questItemIds = [
            PIRATES_TREASURE_CHEST,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00365_DevilsLegacy'
    }

    getQuestStartIds(): Array<number> {
        return [ RANDOLF ]
    }

    getTalkIds(): Array<number> {
        return [
            RANDOLF,
            COLLOB,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( PIRATES_TREASURE_CHEST, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, PIRATES_TREASURE_CHEST, 1, data.isChampion )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30095-02.htm':
                state.startQuest()
                state.setMemoState( 1 )
                break

            case '30095-05.html':
                await state.exitQuest( true, true )
                break

            case '30095-06.html':
                break

            case 'REWARD':
                if ( !state.isMemoState( 1 ) ) {
                    return this.getPath( '30092-04.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, PIRATES_TREASURE_CHEST ) ) {
                    return this.getPath( '30092-02.html' )
                }

                if ( player.getAdena() < 600 ) {
                    return this.getPath( '30092-03.html' )
                }

                return this.rewardPlayer( player, data.characterId, state )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== RANDOLF ) {
                    break
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30095-01.htm' : '30095-03.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case COLLOB:
                        return this.getPath( state.isMemoState( 1 ) ? '30092-01.html' : '30092-07.html' )

                    case RANDOLF:
                        let amount: number = QuestHelper.getQuestItemsCount( player, PIRATES_TREASURE_CHEST )
                        if ( amount > 0 ) {
                            let adena = amount * 400 + 19800
                            await QuestHelper.giveAdena( player, adena, true )
                            await QuestHelper.takeSingleItem( player, PIRATES_TREASURE_CHEST, -1 )

                            return this.getPath( '30095-04.html' )
                        }

                        return this.getPath( '30095-07.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardPlayer( player: L2PcInstance, npcObjectId: number, state: QuestState ): Promise<string> {
        await QuestHelper.takeSingleItem( player, PIRATES_TREASURE_CHEST, 1 )
        await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 600 )

        if ( _.random( 100 ) < 80 ) {
            let chance = _.random( 100 )
            let itemId: number

            if ( chance < 1 ) {
                itemId = ENCHANT_WEAPON_D
            } else if ( chance < 4 ) {
                itemId = ENCHANT_ARMOR_D
            } else if ( chance < 36 ) {
                itemId = THREAD
            } else if ( chance < 68 ) {
                itemId = CORD
            } else {
                itemId = ANIMAL_BONE
            }

            await QuestHelper.rewardSingleItem( player, itemId, 1 )
            return this.getPath( '30092-05.html' )
        }

        let chance = _.random( 1000 )
        let itemId: number

        if ( chance < 10 ) {
            itemId = ENCHANT_WEAPON_C
        } else if ( chance < 40 ) {
            itemId = ENCHANT_ARMOR_C
        } else if ( chance < 60 ) {
            itemId = ENCHANT_WEAPON_D
        } else if ( chance < 260 ) {
            itemId = ENCHANT_ARMOR_D
        } else if ( chance < 445 ) {
            itemId = COKES
        } else if ( chance < 630 ) {
            itemId = STEEL
        } else if ( chance < 815 ) {
            itemId = LEATHER
        } else {
            itemId = COARSE_BONE_POWDER
        }

        await QuestHelper.rewardSingleItem( player, itemId, 1 )

        let npc = L2World.getObjectById( npcObjectId ) as L2Npc

        npc.setTarget( player )
        await npc.doCast( SkillCache.getSkill( poisonSkill, 2 ) )
        npc.setCurrentMp( npc.getMaxMp() )

        state.setMemoState( 2 )
        return this.getPath( '30092-06.html' )
    }
}