import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ROOD = 31067
const LIENRIK_EGG1 = 5860
const LIENRIK_EGG2 = 5861
const minimumLevel = 39

type MonsterReward = [ number, number ] // first chance, second chance
const monsterRewardChances: { [ npcId: number ]: MonsterReward } = {
    20786: [ 46, 48 ], // lienrik
    21644: [ 46, 48 ], // lienrik_a
    21645: [ 69, 71 ], // lienrik_lad_a
}

export class HelpRoodRaiseANewPet extends ListenerLogic {
    constructor() {
        super( 'Q00352_HelpRoodRaiseANewPet', 'listeners/tracked-300/HelpRoodRaiseANewPet.ts' )
        this.questId = 352
        this.questItemIds = [
            LIENRIK_EGG1,
            LIENRIK_EGG2,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00352_HelpRoodRaiseANewPet'
    }

    getQuestStartIds(): Array<number> {
        return [ ROOD ]
    }

    getTalkIds(): Array<number> {
        return [ ROOD ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let [ firstChance, secondChance ] = monsterRewardChances[ data.npcId ]
        let random = _.random( 100 )

        if ( random < QuestHelper.getAdjustedChance( LIENRIK_EGG1, firstChance, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, LIENRIK_EGG1, 1, data.isChampion )
            return
        }

        if ( random < QuestHelper.getAdjustedChance( LIENRIK_EGG2, secondChance, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, LIENRIK_EGG2, 1, data.isChampion )
            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31067-02.htm':
            case '31067-03.htm':
            case '31067-07.html':
            case '31067-10.html':
                break

            case '31067-04.htm':
                state.setMemoState( 1 )
                state.startQuest()
                break

            case '31067-08.html':
                await state.exitQuest( true, true )
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31067-01.htm' : '31067-05.html' )

            case QuestStateValues.STARTED:
                let firstEggAmount = QuestHelper.getQuestItemsCount( player, LIENRIK_EGG1 )
                let secondEggAmount = QuestHelper.getQuestItemsCount( player, LIENRIK_EGG2 )

                if ( firstEggAmount == 0 && secondEggAmount == 0 ) {
                    return this.getPath( '31067-06.html' )
                }

                if ( firstEggAmount >= 1 ) {
                    if ( secondEggAmount === 0 ) {
                        let adenaReward = firstEggAmount * 34 + ( firstEggAmount >= 10 ? 4000 : 2000 )

                        await QuestHelper.giveAdena( player, adenaReward, true )
                        await QuestHelper.takeSingleItem( player, LIENRIK_EGG1, -1 )
                        return this.getPath( '31067-10.html' )
                    }

                    let adenaReward = firstEggAmount * 34 + secondEggAmount * 1025
                    await QuestHelper.giveAdena( player, 4000 + adenaReward, true )
                    await QuestHelper.takeMultipleItems( player, -1, LIENRIK_EGG1, LIENRIK_EGG2 )

                    return this.getPath( '31067-11.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}