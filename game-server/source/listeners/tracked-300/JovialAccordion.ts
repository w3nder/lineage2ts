import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

const SABRIN = 30060
const XABER = 30075
const SWAN = 30957
const BARBADO = 30959
const BEER_CHEST = 30960
const CLOTH_CHEST = 30961
const STOLEN_BLACK_BEER = 4321
const STOLEN_EVENT_CLOTHES = 4322
const CLOTHES_CHEST_KEY = 4323
const BEER_CHEST_KEY = 4324
const THEME_OF_THE_FEAST = 4421
const MIN_LEVEL = 15

export class JovialAccordion extends ListenerLogic {
    constructor() {
        super( 'Q00364_JovialAccordion', 'listeners/tracked-300/JovialAccordion.ts' )
        this.questId = 364
        this.questItemIds = [
            STOLEN_BLACK_BEER,
            STOLEN_EVENT_CLOTHES,
            CLOTHES_CHEST_KEY,
            BEER_CHEST_KEY,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00364_JovialAccordion'
    }

    getQuestStartIds(): Array<number> {
        return [ BARBADO ]
    }

    getTalkIds(): Array<number> {
        return [
            BARBADO,
            BEER_CHEST,
            CLOTH_CHEST,
            SABRIN,
            XABER,
            SWAN,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'START':
                if ( player.getLevel() >= MIN_LEVEL ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    return this.getPath( '30959-02.htm' )
                }

                return this.getPath( '30959-03.htm' )

            case 'OPEN_CHEST':
                if ( QuestHelper.hasQuestItem( player, BEER_CHEST_KEY ) ) {
                    await QuestHelper.takeSingleItem( player, BEER_CHEST_KEY, -1 )

                    if ( Math.random() > 0.5 ) {
                        await QuestHelper.giveSingleItem( player, STOLEN_BLACK_BEER, 1 )
                        return this.getPath( '30960-02.html' )
                    }

                    return this.getPath( '30960-03.html' )
                }

                return this.getPath( '30960-04.html' )

            case 'OPEN_CLOTH_CHEST':
                if ( QuestHelper.hasQuestItem( player, CLOTHES_CHEST_KEY ) ) {
                    await QuestHelper.takeSingleItem( player, CLOTHES_CHEST_KEY, -1 )

                    if ( Math.random() > 0.5 ) {
                        await QuestHelper.giveSingleItem( player, STOLEN_EVENT_CLOTHES, 1 )
                        return this.getPath( '30961-02.html' )
                    }

                    return this.getPath( '30961-03.html' )
                }

                return this.getPath( '30961-04.html' )

            case '30957-02.html':
                await QuestHelper.giveMultipleItems( player, 1, CLOTHES_CHEST_KEY, BEER_CHEST_KEY )

                state.setMemoState( 2 )
                state.setConditionWithSound( 2, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === BARBADO ) {
                    return this.getPath( '30959-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case BARBADO:
                        switch ( state.getMemoState() ) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                                return this.getPath( '30959-04.html' )

                            case 5:
                                await QuestHelper.rewardSingleItem( player, THEME_OF_THE_FEAST, 1 )
                                await state.exitQuest( true, true )

                                return this.getPath( '30959-05.html' )
                        }

                        break

                    case BEER_CHEST:
                        return this.getPath( '30960-01.html' )

                    case CLOTH_CHEST:
                        return this.getPath( '30961-01.html' )

                    case SABRIN:
                        if ( QuestHelper.hasQuestItem( player, STOLEN_BLACK_BEER ) ) {
                            await QuestHelper.takeSingleItem( player, STOLEN_BLACK_BEER, -1 )

                            if ( [ 2, 3 ].includes( state.getMemoState() ) ) {
                                state.setMemoState( state.getMemoState() + 1 )
                            }

                            return this.getPath( '30060-01.html' )

                        }

                        return this.getPath( '30060-02.html' )

                    case XABER:
                        if ( QuestHelper.hasQuestItem( player, STOLEN_EVENT_CLOTHES ) ) {
                            await QuestHelper.takeSingleItem( player, STOLEN_EVENT_CLOTHES, -1 )

                            if ( [ 2, 3 ].includes( state.getMemoState() ) ) {
                                state.setMemoState( state.getMemoState() + 1 )
                            }

                            return this.getPath( '30075-01.html' )

                        }

                        return this.getPath( '30075-02.html' )

                    case SWAN:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '30957-01.html' )

                            case 2:
                            case 3:
                                if ( QuestHelper.hasAtLeastOneQuestItem( player, BEER_CHEST_KEY, CLOTHES_CHEST_KEY, STOLEN_BLACK_BEER, STOLEN_EVENT_CLOTHES ) ) {
                                    return this.getPath( '30957-03.html' )
                                }

                                if ( state.isMemoState( 2 ) ) {
                                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_GIVEUP )

                                    await state.exitQuest( true, true )

                                    return this.getPath( '30957-06.html' )
                                }

                                state.setMemoState( 5 )
                                state.setConditionWithSound( 3, true )

                                return this.getPath( '30957-04.html' )

                            case 4:
                                if ( !QuestHelper.hasQuestItems( player, BEER_CHEST_KEY, CLOTHES_CHEST_KEY, STOLEN_BLACK_BEER, STOLEN_EVENT_CLOTHES ) ) {
                                    state.setMemoState( 5 )
                                    state.setConditionWithSound( 3, true )

                                    await QuestHelper.giveAdena( player, 100, true )

                                    return this.getPath( '30957-05.html' )
                                }

                                break

                            case 5:
                                return this.getPath( '30957-07.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}