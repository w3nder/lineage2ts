import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const REVA = 30867
const PATRIN = 30929
const ANCIENT_ASH_URN = 5903
const ANCIENT_PORCELAIN = 6002
const ANCIENT_PORCELAIN_EXCELLENT = 6003
const ANCIENT_PORCELAIN_HIGH_QUALITY = 6004
const ANCIENT_PORCELAIN_LOW_QUALITY = 6005
const ANCIENT_PORCELAIN_LOWEST_QUALITY = 6006
const MIN_LEVEL = 59

type MonsterChances = [ number, number ] // first, second
const monsterDropChances: { [ npcId: number ]: MonsterChances } = {
    20818: [ 0.350, 0.400 ], // hallates_warrior
    20820: [ 0.583, 0.673 ], // hallates_knight
    20824: [ 0.458, 0.538 ], // hallates_commander
}

const monsterRewards: Array<number> = [ ANCIENT_ASH_URN, ANCIENT_PORCELAIN ]

export class ShrieksOfGhosts extends ListenerLogic {
    constructor() {
        super( 'Q00371_ShrieksOfGhosts', 'listeners/tracked-300/ShrieksOfGhosts.ts' )
        this.questId = 371
        this.questItemIds = [
            ANCIENT_ASH_URN,
            ANCIENT_PORCELAIN,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterDropChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00371_ShrieksOfGhosts'
    }

    getQuestStartIds(): Array<number> {
        return [ REVA ]
    }

    getTalkIds(): Array<number> {
        return [ REVA, PATRIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let chance: number = Math.random()
        let dropIndex: number = _.findIndex( monsterDropChances[ data.npcId ], ( dropChance: number, index: number ) => {
            let itemId: number = monsterRewards[ index ]
            return chance < QuestHelper.getAdjustedChance( itemId, dropChance, data.isChampion )
        } )

        if ( dropIndex >= 0 ) {
            await QuestHelper.rewardSingleQuestItem( player, monsterRewards[ dropIndex ], 1, data.isChampion )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30867-02.htm':
                state.startQuest()
                break

            case '30867-05.html':
                let itemAmount = QuestHelper.getQuestItemsCount( player, ANCIENT_ASH_URN )

                if ( itemAmount === 0 ) {
                    break
                }

                await QuestHelper.takeSingleItem( player, ANCIENT_ASH_URN, -1 )
                let adena = itemAmount * 1000

                if ( itemAmount < 100 ) {
                    await QuestHelper.giveAdena( player, adena + 15000, true )
                    return this.getPath( '30867-06.html' )
                }

                await QuestHelper.giveAdena( player, adena + 37700, true )
                return this.getPath( '30867-07.html' )

            case '30867-08.html':
            case '30929-01.html':
            case '30929-02.html':
                break

            case '30867-09.html':
                await QuestHelper.giveAdena( player, QuestHelper.getQuestItemsCount( player, ANCIENT_ASH_URN ) * 1000, true )
                await state.exitQuest( true, true )
                return this.getPath( '30867-09.html' )

            case '30929-03.html':
                if ( !QuestHelper.hasQuestItem( player, ANCIENT_PORCELAIN ) ) {
                    break
                }

                await QuestHelper.takeSingleItem( player, ANCIENT_PORCELAIN, 1 )

                return this.rewardPlayer( player )


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= MIN_LEVEL ? '30867-01.htm' : '30867-03.htm' )

            case QuestStateValues.STARTED:
                if ( data.characterNpcId === REVA ) {
                    return this.getPath( QuestHelper.hasQuestItem( player, ANCIENT_PORCELAIN ) ? '30867-04.html' : '30867-10.html' )
                }

                return this.getPath( '30929-01.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardPlayer( player: L2PcInstance ): Promise<string> {
        let chance = _.random( 100 )

        if ( chance < 2 ) {
            await QuestHelper.rewardSingleItem( player, ANCIENT_PORCELAIN_EXCELLENT, 1 )
            return this.getPath( '30929-04.html' )
        }

        if ( chance < 32 ) {
            await QuestHelper.rewardSingleItem( player, ANCIENT_PORCELAIN_HIGH_QUALITY, 1 )
            return this.getPath( '30929-05.html' )
        }

        if ( chance < 62 ) {
            await QuestHelper.rewardSingleItem( player, ANCIENT_PORCELAIN_LOW_QUALITY, 1 )
            return this.getPath( '30929-06.html' )
        }

        if ( chance < 77 ) {
            await QuestHelper.rewardSingleItem( player, ANCIENT_PORCELAIN_LOWEST_QUALITY, 1 )
            return this.getPath( '30929-07.html' )
        }

        return this.getPath( '30929-08.html' )
    }
}