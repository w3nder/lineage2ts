import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'

const WOODROW = 30837
const NANARIN = 30956
const SWAN = 30957
const GALION = 30958
const SWANS_FLUTE = 4316
const SWANS_LETTER = 4317
const THEME_OF_JOURNEY = 4410
const minimumLevel = 15

export class BardsMandolin extends ListenerLogic {
    constructor() {
        super( 'Q00362_BardsMandolin', 'listeners/tracked-300/BardsMandolin.ts' )
        this.questId = 362
        this.questItemIds = [
            SWANS_FLUTE,
            SWANS_LETTER,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ SWAN ]
    }

    getTalkIds(): Array<number> {
        return [
            SWAN,
            GALION,
            WOODROW,
            NANARIN,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00362_BardsMandolin'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30957-02.htm':
                state.startQuest()
                state.setMemoState( 1 )
                break

            case '30957-07.html':
            case '30957-08.html':
                if ( state.isMemoState( 5 ) ) {
                    let player = L2World.getPlayer( data.playerId )

                    await QuestHelper.giveAdena( player, 10000, true )
                    await QuestHelper.rewardSingleItem( player, THEME_OF_JOURNEY, 1 )
                    await state.exitQuest( true, true )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== SWAN ) {
                    break
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30957-01.htm' : '30957-03.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case SWAN:
                        switch ( state.getMemoState() ) {
                            case 1:
                            case 2:
                                return this.getPath( '30957-04.html' )

                            case 3:
                                state.setConditionWithSound( 4, true )
                                state.setMemoState( 4 )

                                await QuestHelper.giveSingleItem( player, SWANS_LETTER, 1 )

                                return this.getPath( '30957-05.html' )

                            case 4:
                                return this.getPath( '30957-05.html' )

                            case 5:
                                return this.getPath( '30957-06.html' )
                        }

                        break

                    case GALION:
                        if ( state.isMemoState( 2 ) ) {
                            state.setMemoState( 3 )
                            state.setConditionWithSound( 3, true )

                            await QuestHelper.giveSingleItem( player, SWANS_FLUTE, 1 )

                            return this.getPath( '30958-01.html' )
                        }

                        if ( state.getMemoState() >= 3 ) {
                            return this.getPath( '30958-02.html' )
                        }

                        break

                    case WOODROW:
                        if ( state.isMemoState( 1 ) ) {
                            state.setMemoState( 2 )
                            state.setConditionWithSound( 2, true )

                            return this.getPath( '30837-01.html' )
                        }

                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '30837-02.html' )
                        }

                        if ( state.getMemoState() >= 3 ) {
                            return this.getPath( '30837-03.html' )
                        }

                        break

                    case NANARIN:
                        if ( state.isMemoState( 4 ) && QuestHelper.hasQuestItems( player, SWANS_FLUTE, SWANS_LETTER ) ) {
                            state.setMemoState( 5 )
                            state.setConditionWithSound( 5, true )

                            await QuestHelper.takeMultipleItems( player, -1, SWANS_FLUTE, SWANS_LETTER )

                            return this.getPath( '30956-01.html' )
                        }

                        if ( state.getMemoState() >= 5 ) {
                            return this.getPath( '30956-02.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}