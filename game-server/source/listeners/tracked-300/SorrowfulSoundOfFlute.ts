import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

import _ from 'lodash'

const ALDO = 30057
const HOLVAS = 30058
const POITAN = 30458
const RANSPO = 30594
const OPIX = 30595
const NANARIN = 30956
const BARBADO = 30959
const EVENT_CLOTHES = 4318
const NANARINS_FLUTE = 4319
const SABRINS_BLACK_BEER = 4320
const THEME_OF_SOLITUDE = 4420
const minimumLevel = 15

const variableNames = {
    value: 'v',
}

export class SorrowfulSoundOfFlute extends ListenerLogic {
    constructor() {
        super( 'Q00363_SorrowfulSoundOfFlute', 'listeners/tracked-300/SorrowfulSoundOfFlute.ts' )
        this.questId = 363
        this.questItemIds = [
            EVENT_CLOTHES,
            NANARINS_FLUTE,
            SABRINS_BLACK_BEER,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00363_SorrowfulSoundOfFlute'
    }

    getQuestStartIds(): Array<number> {
        return [ NANARIN ]
    }

    getTalkIds(): Array<number> {
        return [ NANARIN, POITAN, RANSPO, ALDO, HOLVAS, OPIX, BARBADO ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'START':
                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    state.setMemoState( 2 )

                    return this.getPath( '30956-02.htm' )
                }

                return this.getPath( '30956-03.htm' )

            case '30956-05.html':
                await QuestHelper.giveSingleItem( player, EVENT_CLOTHES, 1 )

                state.setMemoState( 4 )
                state.setConditionWithSound( 3, true )

                break

            case '30956-06.html':
                await QuestHelper.giveSingleItem( player, NANARINS_FLUTE, 1 )

                state.setMemoState( 4 )
                state.setConditionWithSound( 3, true )

                break

            case '30956-07.html':
                await QuestHelper.giveSingleItem( player, SABRINS_BLACK_BEER, 1 )

                state.setMemoState( 4 )
                state.setConditionWithSound( 3, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === NANARIN ) {
                    return this.getPath( '30956-01.htm' )
                }

                break

            case QuestStateValues.STARTED: {
                switch ( data.characterNpcId ) {
                    case NANARIN:
                        switch ( state.getMemoState() ) {
                            case 2:
                                return this.getPath( '30956-04.html' )

                            case 4:
                                return this.getPath( '30956-08.html' )

                            case 5:
                                await QuestHelper.rewardSingleItem( player, THEME_OF_SOLITUDE, 1 )
                                await state.exitQuest( true, true )

                                return this.getPath( '30956-09.html' )

                            case 6:
                                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_GIVEUP )
                                await state.exitQuest( true, false )

                                return this.getPath( '30956-10.html' )
                        }

                        break

                    case POITAN:
                        return this.processNpcTalk( state, POITAN, 100, 10, 11 )

                    case RANSPO:
                        return this.processNpcTalk( state, RANSPO, 10000, 1000, 1001 )

                    case ALDO:
                        return this.processNpcTalk( state, ALDO, 100000, 10000, 10001 )

                    case HOLVAS:
                        return this.processNpcTalk( state, HOLVAS, 1000, 100, 101 )

                    case OPIX:
                        return this.processNpcTalk( state, OPIX, 1, 100000, 100001 )

                    case BARBADO:
                        if ( state.isMemoState( 4 ) ) {
                            let value = ( state.getVariable( variableNames.value ) % 10 ) * 20
                            if ( _.random( 100 ) < value ) {
                                await this.removeItems( player )

                                state.setMemoState( 5 )
                                state.setConditionWithSound( 4, true )

                                return this.getPath( '30959-01.html' )
                            }

                            state.setMemoState( 6 )
                            state.setConditionWithSound( 4, true )

                            return this.getPath( '30959-02.html' )
                        }

                        if ( state.getMemoState() >= 5 ) {
                            return this.getPath( '30959-03.html' )
                        }

                        break
                }
                break
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    processNpcTalk( state: QuestState, npcId: number, modulus: number, threshold: number, increment: number ) {
        let isBelowTen = ( state.getVariable( variableNames.value ) % modulus ) < threshold
        if ( state.isMemoState( 2 ) && isBelowTen ) {
            let value: number = state.getVariable( variableNames.value )

            state.setVariable( variableNames.value, value + increment )
            state.setConditionWithSound( 2, true )

            switch ( _.random( 2 ) ) {
                case 0:
                    return this.getPath( `${ npcId }-01.html` )

                case 1:
                    return this.getPath( `${ npcId }-02.html` )

                case 2:
                    return this.getPath( `${ npcId }-03.html` )
            }

            return QuestHelper.getNoQuestMessagePath()
        }

        if ( state.getMemoState() >= 2 && !isBelowTen ) {
            return this.getPath( '30458-04.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async removeItems( player: L2PcInstance ): Promise<void> {
        if ( QuestHelper.hasQuestItem( player, EVENT_CLOTHES ) ) {
            return QuestHelper.takeSingleItem( player, EVENT_CLOTHES, -1 )
        }

        if ( QuestHelper.hasQuestItem( player, NANARINS_FLUTE ) ) {
            return QuestHelper.takeSingleItem( player, NANARINS_FLUTE, -1 )
        }

        if ( QuestHelper.hasQuestItem( player, SABRINS_BLACK_BEER ) ) {
            return QuestHelper.takeSingleItem( player, SABRINS_BLACK_BEER, -1 )
        }
    }
}