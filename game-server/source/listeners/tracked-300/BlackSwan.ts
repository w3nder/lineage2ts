import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ROMAN = 30897
const GOSTA = 30916
const IASON_HEINE = 30969
const ORDER_OF_GOSTA = 4296
const LIZARD_FANG = 4297
const BARREL_OF_LEAGUE = 4298
const BILL_OF_IASON_HEINE = 4407
const minimumLevel = 32
const TASABA_LIZARDMAN1 = 20784
const TASABA_LIZARDMAN_SHAMAN1 = 20785
const TASABA_LIZARDMAN2 = 21639
const TASABA_LIZARDMAN_SHAMAN2 = 21640

const monsterRewardChances = {
    [ TASABA_LIZARDMAN1 ]: 4,
    [ TASABA_LIZARDMAN_SHAMAN1 ]: 3,
    [ TASABA_LIZARDMAN2 ]: 4,
    [ TASABA_LIZARDMAN_SHAMAN2 ]: 3,
}

export class BlackSwan extends ListenerLogic {
    constructor() {
        super( 'Q00351_BlackSwan', 'listeners/tracked-300/BlackSwan.ts' )
        this.questId = 351
        this.questItemIds = [
            ORDER_OF_GOSTA,
            LIZARD_FANG,
            BARREL_OF_LEAGUE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ TASABA_LIZARDMAN1, TASABA_LIZARDMAN_SHAMAN1, TASABA_LIZARDMAN2, TASABA_LIZARDMAN_SHAMAN2 ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00351_BlackSwan'
    }

    getQuestStartIds(): Array<number> {
        return [ GOSTA, ROMAN ]
    }

    getTalkIds(): Array<number> {
        return [ GOSTA, IASON_HEINE, ROMAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        let chance = _.random( 20 )
        if ( chance < QuestHelper.getAdjustedChance( LIZARD_FANG, 10, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, LIZARD_FANG, 1, data.isChampion )

            if ( _.random( 100 ) <= QuestHelper.getAdjustedChance( BARREL_OF_LEAGUE, 5, data.isChampion ) ) {
                await QuestHelper.rewardSingleQuestItem( player, BARREL_OF_LEAGUE, 1, data.isChampion )
            }

            return
        }

        if ( chance < QuestHelper.getAdjustedChance( LIZARD_FANG, 15, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, LIZARD_FANG, 2, data.isChampion )

            if ( _.random( 100 ) <= QuestHelper.getAdjustedChance( BARREL_OF_LEAGUE, 5, data.isChampion ) ) {
                await QuestHelper.rewardSingleQuestItem( player, BARREL_OF_LEAGUE, 1, data.isChampion )
            }

            return
        }

        if ( _.random( 100 ) < QuestHelper.getAdjustedChance( LIZARD_FANG, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, BARREL_OF_LEAGUE, 1, data.isChampion )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30916-02.htm':
            case '30969-03.html':
                break

            case '30916-03.htm':
                await QuestHelper.giveSingleItem( player, ORDER_OF_GOSTA, 1 )
                state.startQuest()
                break

            case '30969-02.html':
                let fangAmount = QuestHelper.getQuestItemsCount( player, LIZARD_FANG )

                if ( fangAmount === 0 ) {
                    break
                }

                let adena = 20 * fangAmount + ( fangAmount >= 10 ? 3880 : 0 )
                await QuestHelper.giveAdena( player, adena, true )
                await QuestHelper.takeSingleItem( player, LIZARD_FANG, -1 )

                return this.getPath( '30969-04.html' )

            case '30969-05.html':
                let barrelOfLeagueCount = QuestHelper.getQuestItemsCount( player, BARREL_OF_LEAGUE )

                if ( barrelOfLeagueCount === 0 ) {
                    break
                }

                await QuestHelper.rewardSingleItem( player, BILL_OF_IASON_HEINE, barrelOfLeagueCount )
                await QuestHelper.giveAdena( player, 3880, true )
                await QuestHelper.takeSingleItem( player, BARREL_OF_LEAGUE, -1 )

                state.setConditionWithSound( 2 )
                return this.getPath( '30969-06.html' )

            case '30969-07.html':
                if ( QuestHelper.hasQuestItems( player, BARREL_OF_LEAGUE, LIZARD_FANG ) ) {
                    return this.getPath( '30969-08.html' )
                }

                break

            case '30969-09.html':
                await state.exitQuest( true, true )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case GOSTA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30916-01.htm' : '30916-04.html' )

                    case QuestStateValues.STARTED:
                        return this.getPath( '30916-05.html' )
                }

                break

            case IASON_HEINE:
                if ( state.isStarted() ) {
                    return this.getPath( '30969-01.html' )
                }

                break

            case ROMAN:
                let companionState = QuestStateCache.getQuestState( data.playerId, 'Q00345_MethodToRaiseTheDead', false )
                if ( state.isStarted() || ( companionState && companionState.isStarted() ) ) {
                    return this.getPath( QuestHelper.hasQuestItems( player, BILL_OF_IASON_HEINE ) ? '30897-01.html' : '30897-02.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}