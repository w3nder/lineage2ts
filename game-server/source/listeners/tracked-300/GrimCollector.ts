import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const GUARD_CURTIZ = 30336
const VARSAK = 30342
const SAMED = 30434
const ANATOMY_DIAGRAM = 1349
const ZOMBIE_HEAD = 1350
const ZOMBIE_HEART = 1351
const ZOMBIE_LIVER = 1352
const SKULL = 1353
const RIB_BONE = 1354
const SPINE = 1355
const ARM_BONE = 1356
const THIGH_BONE = 1357
const COMPLETE_SKELETON = 1358
const minimumLevel = 15

type MonsterReward = [ number, number ] // itemId, chance
const monsterRewardItems: { [ npcId: number ]: Array<MonsterReward> } = {
    20026: [ [ ZOMBIE_HEAD, 30 ], [ ZOMBIE_HEART, 50 ], [ ZOMBIE_LIVER, 75 ] ],
    20029: [ [ ZOMBIE_HEAD, 30 ], [ ZOMBIE_HEART, 52 ], [ ZOMBIE_LIVER, 75 ] ],
    20035: [ [ SKULL, 5 ], [ RIB_BONE, 15 ], [ SPINE, 29 ], [ THIGH_BONE, 79 ] ],
    20042: [ [ SKULL, 6 ], [ RIB_BONE, 19 ], [ ARM_BONE, 69 ], [ THIGH_BONE, 86 ] ],
    20045: [ [ SKULL, 9 ], [ SPINE, 59 ], [ ARM_BONE, 77 ], [ THIGH_BONE, 97 ] ],
    20051: [ [ SKULL, 9 ], [ RIB_BONE, 59 ], [ SPINE, 79 ], [ ARM_BONE, 100 ] ],
    20457: [ [ ZOMBIE_HEAD, 40 ], [ ZOMBIE_HEART, 60 ], [ ZOMBIE_LIVER, 80 ] ],
    20458: [ [ ZOMBIE_HEAD, 40 ], [ ZOMBIE_HEART, 70 ], [ ZOMBIE_LIVER, 100 ] ],
    20514: [ [ SKULL, 6 ], [ RIB_BONE, 21 ], [ SPINE, 30 ], [ ARM_BONE, 31 ], [ THIGH_BONE, 64 ] ],
    20515: [ [ SKULL, 5 ], [ RIB_BONE, 20 ], [ SPINE, 31 ], [ ARM_BONE, 33 ], [ THIGH_BONE, 69 ] ],
}

export class GrimCollector extends ListenerLogic {
    constructor() {
        super( 'Q00325_GrimCollector', 'listeners/tracked-300/GrimCollector.ts' )
        this.questId = 325
        this.questItemIds = [
            ANATOMY_DIAGRAM,
            ZOMBIE_HEAD,
            ZOMBIE_HEART,
            ZOMBIE_LIVER,
            SKULL,
            RIB_BONE,
            SPINE,
            ARM_BONE,
            THIGH_BONE,
            COMPLETE_SKELETON,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardItems ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00325_GrimCollector'
    }

    getQuestStartIds(): Array<number> {
        return [ GUARD_CURTIZ ]
    }

    getTalkIds(): Array<number> {
        return [
            GUARD_CURTIZ,
            VARSAK,
            SAMED,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true )
                || !QuestHelper.hasQuestItem( player, ANATOMY_DIAGRAM ) ) {
            return
        }

        let chance = _.random( 100 )
        let reward: MonsterReward = _.find( monsterRewardItems[ data.npcId ], ( item: MonsterReward ): boolean => {
            let [ itemId, dropChance ] = item
            return chance < QuestHelper.getAdjustedChance( itemId, dropChance, data.isChampion )
        } )

        if ( reward ) {
            let [ itemId ] = reward
            await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30336-03.htm':
                state.startQuest()
                break

            case 'assembleSkeleton':
                if ( !QuestHelper.hasQuestItems( player, SPINE, ARM_BONE, SKULL, RIB_BONE, THIGH_BONE ) ) {
                    return this.getPath( '30342-02.html' )
                }

                await QuestHelper.takeMultipleItems( player, 1, SPINE, ARM_BONE, SKULL, RIB_BONE, THIGH_BONE )

                if ( _.random( 4 ) < 4 ) {
                    await QuestHelper.rewardSingleItem( player, COMPLETE_SKELETON, 1 )
                    return this.getPath( '30342-03.html' )
                }

                return this.getPath( '30342-04.html' )

            case '30434-02.htm':
                break

            case '30434-03.html':
                await QuestHelper.giveSingleItem( player, ANATOMY_DIAGRAM, 1 )
                break

            case '30434-06.html':
            case '30434-07.html':
                const head = QuestHelper.getQuestItemsCount( player, ZOMBIE_HEAD )
                const heart = QuestHelper.getQuestItemsCount( player, ZOMBIE_HEART )
                const liver = QuestHelper.getQuestItemsCount( player, ZOMBIE_LIVER )
                const skull = QuestHelper.getQuestItemsCount( player, SKULL )
                const rib = QuestHelper.getQuestItemsCount( player, RIB_BONE )
                const spine = QuestHelper.getQuestItemsCount( player, SPINE )
                const arm = QuestHelper.getQuestItemsCount( player, ARM_BONE )
                const thigh = QuestHelper.getQuestItemsCount( player, THIGH_BONE )
                const completeSkeleton = QuestHelper.getQuestItemsCount( player, COMPLETE_SKELETON )
                const totalCount = ( head + heart + liver + skull + rib + spine + arm + thigh + completeSkeleton )

                if ( totalCount > 0 ) {
                    let amount = ( head * 30 ) + ( heart * 20 ) + ( liver * 20 ) + ( skull * 100 ) + ( rib * 40 ) + ( spine * 14 ) + ( arm * 14 ) + ( thigh * 14 )

                    if ( totalCount >= 10 ) {
                        amount += 1629
                    }

                    if ( completeSkeleton > 0 ) {
                        amount += 543 + ( completeSkeleton * 341 )
                    }

                    await QuestHelper.giveAdena( player, amount, true )
                }

                await QuestHelper.takeMultipleItems( player, -1, ZOMBIE_HEAD, ZOMBIE_HEART, ZOMBIE_LIVER, SKULL, RIB_BONE, SPINE, ARM_BONE, THIGH_BONE, COMPLETE_SKELETON )

                if ( data.eventName === '30434-06.html' ) {
                    await state.exitQuest( true, true )
                }

                break

            case '30434-09.html':
                let total = QuestHelper.getQuestItemsCount( player, COMPLETE_SKELETON )
                if ( total > 0 ) {
                    let amount = ( total * 341 ) + 543
                    await QuestHelper.giveAdena( player, amount, true )
                    await QuestHelper.takeSingleItem( player, COMPLETE_SKELETON, -1 )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case GUARD_CURTIZ:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30336-02.htm' : '30336-01.htm' )

                    case QuestStateValues.STARTED:
                        return this.getPath( QuestHelper.hasQuestItem( player, ANATOMY_DIAGRAM ) ? '30336-05.html' : '30336-04.html' )
                }

                break

            case VARSAK:
                if ( state.isStarted() && QuestHelper.hasQuestItem( player, ANATOMY_DIAGRAM ) ) {
                    return this.getPath( '30342-01.html' )
                }

                break

            case SAMED:
                if ( !state.isStarted() ) {
                    break
                }

                if ( !QuestHelper.hasQuestItem( player, ANATOMY_DIAGRAM ) ) {
                    return this.getPath( '30434-01.html' )
                }

                if ( !QuestHelper.hasAtLeastOneQuestItem( player, ...this.questItemIds ) ) {
                    return this.getPath( '30434-04.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, COMPLETE_SKELETON ) ) {
                    return this.getPath( '30434-05.html' )
                }

                return this.getPath( '30434-08.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}