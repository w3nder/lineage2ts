import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { DataManager } from '../../data/manager'

import _ from 'lodash'

const CLIFF = 30182
const WAREHOUSE_CHIEF_BAXT = 30685
const DUST_WIND = 20242
const INNERSEN = 20950
const CONGERER = 20774
const CARINKAIN = 20635
const CONNABI = 20947
const HUNTER_GARGOYLE = 20241
const NIGHTMARE_GUIDE = 20942
const DRAGON_BEARER_WARRIOR = 20759
const DRAGON_BEARER_CHIEF = 20758
const DUST_WIND_HOLD = 20281
const WEIRD_DRAKE = 20605
const THUNDER_WYRM_HOLD = 20282
const CADEINE = 20945
const CONGERER_LORD = 20773
const DRAGON_BEARER_ARCHER = 20760
const NIGHTMARE_LORD = 20944
const SANHIDRO = 20946
const GIANT_MONSTEREYE = 20556
const BARTAL = 20948
const HUNTER_GARGOYLE_HOLD = 20286
const ROT_GOLEM = 20559
const GRAVE_GUARD = 20668
const TULBEN = 20677
const NIGHTMARE_KEEPER = 20943
const LUMINUN = 20949
const THUNDER_WYRM = 20243
const Q_IRONGATE_MEDAL = 5964
const MOONSTONE_EARING = 852
const DRAKE_LEATHER_BOOTS = 2437
const DRAKE_LEATHER_MAIL = 401
const MOLD_HARDENER = 4041
const NECKLACE_OF_MERMAID = 917
const SCRL_OF_ENCH_AM_C = 952
const BLACKSMITH_S_FRAME = 1892
const SCRL_OF_ENCH_WP_C = 951
const ORIHARUKON = 1893
const SAMURAI_LONGSWORD = 135
const AQUASTONE_RING = 883
const SYNTHESIS_COKES = 1888
const MITHIRL_ALLOY = 1890
const GREAT_HELMET = 500
const VARNISH_OF_PURITY = 1887
const BLESSED_GLOVES = 2463
const CRAFTED_LEATHER = 1894

const minimumLevel = 40
const variableNames = {
    selectedNumbers: 'sn',
    generatedNumbers: 'gn',
}

const monsterRewardChances: { [ npcId: number ]: number } = {
    [ HUNTER_GARGOYLE ]: 0.328,
    [ DUST_WIND ]: 0.35,
    [ THUNDER_WYRM ]: 0.312,
    [ DUST_WIND_HOLD ]: 0.35,
    [ THUNDER_WYRM_HOLD ]: 0.312,
    [ HUNTER_GARGOYLE_HOLD ]: 0.328,
    [ GIANT_MONSTEREYE ]: 0.176,
    [ ROT_GOLEM ]: 0.226,
    [ WEIRD_DRAKE ]: 0.218,
    [ CARINKAIN ]: 0.216,
    [ GRAVE_GUARD ]: 0.312,
    [ TULBEN ]: 0.522,
    [ DRAGON_BEARER_CHIEF ]: 0.38,
    [ DRAGON_BEARER_WARRIOR ]: 0.39,
    [ DRAGON_BEARER_ARCHER ]: 0.372,
    [ CONGERER_LORD ]: 0.802,
    [ CONGERER ]: 0.844,
    [ NIGHTMARE_GUIDE ]: 0.118,
    [ NIGHTMARE_KEEPER ]: 0.17,
    [ NIGHTMARE_LORD ]: 0.144,
    [ CADEINE ]: 0.162,
    [ SANHIDRO ]: 0.25,
    [ CONNABI ]: 0.272,
    [ BARTAL ]: 0.27,
    [ LUMINUN ]: 0.32,
    [ INNERSEN ]: 0.346,
}

export class WarehouseKeepersPastime extends ListenerLogic {
    constructor() {
        super( 'Q00384_WarehouseKeepersPastime', 'listeners/tracked-300/WarehouseKeepersPastime.ts' )
        this.questId = 384
        this.questItemIds = [
            Q_IRONGATE_MEDAL,
        ]
    }

    async beforeReward( player: L2PcInstance, state: QuestState, value: number, npcId: number ): Promise<string> {
        if ( !this.isSelectedBingoNumber( state, value ) ) {
            this.selectBingoNumber( state, value )

            let matchedLines: number = this.getMatchedBingoLineCount( state )
            let isCorrectCount: boolean = this.getBingoSelectCount( state ) === 6

            if ( matchedLines === 3 && isCorrectCount ) {
                await this.rewardPlayer( player, state, matchedLines )
                return this.colorBoard( state, `${ npcId }-22.html` )
            }

            if ( matchedLines === 0 && isCorrectCount ) {
                await this.rewardPlayer( player, state, matchedLines )
                return this.colorBoard( state, `${ npcId }-24.html` )
            }

            return this.colorBoard( state, `${ npcId }-23.html` )
        }

        return this.fillBoard( state, `${ npcId }-25.html` )
    }

    colorBoard( state: QuestState, htmlPath: string ): string {
        let html = DataManager.getHtmlData().getItem( this.getPath( htmlPath ) )
        let quest = this

        _.each( state.getVariable( variableNames.generatedNumbers ), ( value: number, index: number ) => {
            let color: string = quest.isSelectedBingoNumber( state, value ) ? 'ff0000' : 'ffffff'
            html = html
                    .replace( `<?FontColor${ index + 1 }?>`, color )
                    .replace( `<?Cell${ index + 1 }?>`, value.toString() )
        } )

        return html
    }

    createBingoBoard( state: QuestState ): void {
        state.setVariable( variableNames.selectedNumbers, _.times( 9, _.constant( 0 ) ) )
        state.setVariable( variableNames.generatedNumbers, _.shuffle( [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ] ) )
    }

    fillBoard( state: QuestState, htmlPath: string ): string {
        let html = DataManager.getHtmlData().getItem( this.getPath( htmlPath ) )
        let quest = this

        _.each( state.getVariable( variableNames.generatedNumbers ), ( value: number, index: number ) => {
            let outcome: string = quest.isSelectedBingoNumber( state, value ) ? value.toString() : '?'
            html = html.replace( `<?Cell${ index + 1 }?>`, outcome )
        } )

        return html
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00384_WarehouseKeepersPastime'
    }

    getQuestStartIds(): Array<number> {
        return [ CLIFF ]
    }

    getTalkIds(): Array<number> {
        return [ CLIFF, WAREHOUSE_CHIEF_BAXT ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( Q_IRONGATE_MEDAL, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 2, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, Q_IRONGATE_MEDAL, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        if ( data.eventName.endsWith( '.htm' ) ) {
            return this.getPath( data.eventName )
        }

        let player = L2World.getPlayer( data.playerId )
        let value: number = _.parseInt( data.eventName )

        switch ( data.characterNpcId ) {
            case CLIFF:
                if ( data.eventName === 'QUEST_ACCEPTED' ) {
                    state.startQuest()
                    state.setMemoState( 384 )
                    state.showQuestionMark( 384 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ACCEPT )
                    return '30182-05.htm'
                }

                switch ( value ) {
                    case 3:
                        return '30182-09a.htm'

                    case 4:
                        return '30182-02.htm'

                    case 5:
                        return '30182-03.htm'

                    case 6:
                        await state.exitQuest( true )
                        return '30182-08.html'

                    case 9:
                        return '30182-09.html'

                    case 7:
                        if ( QuestHelper.getQuestItemsCount( player, Q_IRONGATE_MEDAL ) >= 10 ) {
                            await QuestHelper.takeSingleItem( player, Q_IRONGATE_MEDAL, 10 )
                            state.setMemoState( 10 )

                            this.createBingoBoard( state )

                            return '30182-10.html'
                        }

                        return '30182-11.html'

                    case 8:
                        if ( QuestHelper.getQuestItemsCount( player, Q_IRONGATE_MEDAL ) >= 100 ) {
                            await QuestHelper.takeSingleItem( player, Q_IRONGATE_MEDAL, 100 )
                            state.setMemoState( 20 )

                            this.createBingoBoard( state )

                            return '30182-12.html'
                        }

                        return '30182-11.html'

                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                        this.selectBingoNumber( state, ( value - 10 ) + 1 )
                        return this.fillBoard( state, '30182-13.html' )

                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                        return this.takeHtml( state, ( value - 18 ), CLIFF )

                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                        return this.beforeReward( player, state, ( value - 54 ), CLIFF )
                }

                break

            case WAREHOUSE_CHIEF_BAXT:

                switch ( value ) {
                    case 3:
                        return '30685-09a.html'
                    case 6:
                        await state.exitQuest( true )
                        return '30685-08.html'

                    case 9:
                        return '30685-09.html'

                    case 7:
                        if ( QuestHelper.getQuestItemsCount( player, Q_IRONGATE_MEDAL ) >= 10 ) {
                            await QuestHelper.takeSingleItem( player, Q_IRONGATE_MEDAL, 10 )
                            state.setMemoState( 10 )

                            this.createBingoBoard( state )

                            return '30685-10.html'
                        }
                        return '30685-11.html'
                    case 8:
                        if ( QuestHelper.getQuestItemsCount( player, Q_IRONGATE_MEDAL ) >= 100 ) {
                            await QuestHelper.takeSingleItem( player, Q_IRONGATE_MEDAL, 100 )
                            state.setMemoState( 20 )

                            this.createBingoBoard( state )

                            return '30685-12.html'
                        }

                        return '30685-11.html'

                    case 10:
                    case 11:
                    case 12:
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                    case 18:
                        this.selectBingoNumber( state, ( value - 9 ) )
                        return this.fillBoard( state, '30685-13.html' )

                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                        return this.takeHtml( state, ( value - 18 ), WAREHOUSE_CHIEF_BAXT )

                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                    case 60:
                    case 61:
                    case 62:
                    case 63:
                        return this.beforeReward( player, state, ( value - 54 ), WAREHOUSE_CHIEF_BAXT )
                }
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case CLIFF:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() >= minimumLevel ) {
                            return this.getPath( '30182-01.htm' )
                        }

                        return this.getPath( '30182-04.html' )

                    case QuestStateValues.STARTED:
                        if ( QuestHelper.getQuestItemsCount( player, Q_IRONGATE_MEDAL ) < 10 ) {
                            return this.getPath( '30182-06.html' )
                        }

                        return this.getPath( '30182-07.html' )
                }

                break

            case WAREHOUSE_CHIEF_BAXT:
                if ( state.getMemoState() > 0 ) {
                    if ( QuestHelper.getQuestItemsCount( player, Q_IRONGATE_MEDAL ) < 10 ) {
                        return this.getPath( '30685-06.html' )
                    }

                    return this.getPath( '30685-07.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getBingoSelectCount( state: QuestState ): number {
        return _.reduce( state.getVariable( variableNames.selectedNumbers ), ( amount: number, currentNumber: number ): number => {
            if ( currentNumber !== 0 ) {
                amount++
            }

            return amount
        }, 0 )
    }

    getMatchedBingoLineCount( state: QuestState ): number {
        let found: number = 0
        let grid: Array<number> = state.getVariable( variableNames.selectedNumbers )

        /*
            3 x 3 grid
            First match rows to see if all of them contain non-zero values.
            Second match is for columns, again for non-zero values.
            Third match is diagonal...
         */

        if ( !grid.slice( 0, 3 ).includes( 0 ) ) {
            found++
        }

        if ( !grid.slice( 3, 6 ).includes( 0 ) ) {
            found++
        }

        if ( !grid.slice( 6, 9 ).includes( 0 ) ) {
            found++
        }

        if ( ![ grid[ 0 ], grid[ 3 ], grid[ 6 ] ].includes( 0 ) ) {
            found++
        }

        if ( ![ grid[ 1 ], grid[ 4 ], grid[ 7 ] ].includes( 0 ) ) {
            found++
        }

        if ( ![ grid[ 2 ], grid[ 5 ], grid[ 8 ] ].includes( 0 ) ) {
            found++
        }

        if ( ![ grid[ 0 ], grid[ 4 ], grid[ 8 ] ].includes( 0 ) ) {
            found++
        }

        if ( ![ grid[ 2 ], grid[ 4 ], grid[ 6 ] ].includes( 0 ) ) {
            found++
        }

        return found
    }

    isSelectedBingoNumber( state: QuestState, value: number ): boolean {
        return state.getVariable( variableNames.selectedNumbers ).includes( value )
    }

    async rewardPlayer( player: L2PcInstance, state: QuestState, matchedLines: number ): Promise<void> {
        switch ( matchedLines ) {
            case 3:
                if ( state.getMemoState() === 10 ) {
                    let chance = _.random( 100 )
                    if ( chance < 16 ) {
                        return QuestHelper.rewardSingleItem( player, SYNTHESIS_COKES, 1 )
                    }

                    if ( chance < 32 ) {
                        return QuestHelper.rewardSingleItem( player, VARNISH_OF_PURITY, 1 )
                    }

                    if ( chance < 50 ) {
                        return QuestHelper.rewardSingleItem( player, CRAFTED_LEATHER, 1 )
                    }

                    if ( chance < 80 ) {
                        return QuestHelper.rewardSingleItem( player, SCRL_OF_ENCH_AM_C, 1 )
                    }

                    if ( chance < 89 ) {
                        return QuestHelper.rewardSingleItem( player, MITHIRL_ALLOY, 1 )
                    }

                    if ( chance < 98 ) {
                        return QuestHelper.rewardSingleItem( player, ORIHARUKON, 1 )
                    }

                    return QuestHelper.rewardSingleItem( player, SCRL_OF_ENCH_WP_C, 1 )
                }

                if ( state.getMemoState() === 20 ) {
                    let chance = _.random( 100 )

                    if ( chance < 50 ) {
                        return QuestHelper.rewardSingleItem( player, AQUASTONE_RING, 1 )
                    }

                    if ( chance < 80 ) {
                        return QuestHelper.rewardSingleItem( player, SCRL_OF_ENCH_WP_C, 1 )
                    }

                    if ( chance < 98 ) {
                        return QuestHelper.rewardSingleItem( player, MOONSTONE_EARING, 1 )
                    }

                    return QuestHelper.rewardSingleItem( player, DRAKE_LEATHER_MAIL, 1 )
                }

                return

            case 0:
                if ( state.getMemoState() === 10 ) {
                    let chance = _.random( 100 )

                    if ( chance < 50 ) {
                        return QuestHelper.rewardSingleItem( player, MOLD_HARDENER, 1 )
                    }

                    if ( chance < 80 ) {
                        return QuestHelper.rewardSingleItem( player, SCRL_OF_ENCH_AM_C, 1 )
                    }

                    if ( chance < 98 ) {
                        return QuestHelper.rewardSingleItem( player, BLACKSMITH_S_FRAME, 1 )
                    }

                    return QuestHelper.rewardSingleItem( player, NECKLACE_OF_MERMAID, 1 )
                }

                if ( state.getMemoState() === 20 ) {
                    let chance = _.random( 100 )

                    if ( chance < 50 ) {
                        return QuestHelper.rewardSingleItem( player, SCRL_OF_ENCH_WP_C, 1 )
                    }

                    if ( chance < 80 ) {
                        return QuestHelper.rewardSingleItem( player, GREAT_HELMET, 1 )
                    }

                    if ( chance < 98 ) {
                        return QuestHelper.rewardMultipleItems( player, 1, DRAKE_LEATHER_BOOTS, BLESSED_GLOVES )
                    }

                    return QuestHelper.rewardSingleItem( player, SAMURAI_LONGSWORD, 1 )
                }

                return
        }
    }

    selectBingoNumber( state: QuestState, value: number ): void {
        let position = _.findIndex( state.getVariable( variableNames.generatedNumbers ), value )

        if ( position < 0 ) {
            position = 0
        }

        state.getVariable( variableNames.selectedNumbers )[ position ] = value
    }

    takeHtml( state: QuestState, value: number, npcId: number ): string {
        if ( !this.isSelectedBingoNumber( state, value ) ) {
            this.selectBingoNumber( state, value )
            let countValue: number = this.getBingoSelectCount( state )

            switch ( countValue ) {
                case 2:
                    return this.fillBoard( state, `${ npcId }-14.html` )

                case 3:
                    return this.fillBoard( state, `${ npcId }-16.html` )

                case 4:
                    return this.fillBoard( state, `${ npcId }-18.html` )

                case 5:
                    return this.fillBoard( state, `${ npcId }-20.html` )
            }

            return
        }

        let countValue: number = this.getBingoSelectCount( state )
        switch ( countValue ) {
            case 1:
                return this.fillBoard( state, `${ npcId }-15.html` )

            case 2:
                return this.fillBoard( state, `${ npcId }-17.html` )

            case 3:
                return this.fillBoard( state, `${ npcId }-19.html` )

            case 4:
                return this.fillBoard( state, `${ npcId }-21.html` )
        }

        return
    }
}