import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const SARIEN = 30436
const MONSTER_EYE_CARCASS = 1347
const MONSTER_EYE_LENS = 1366
const BASILISK_GIZZARD = 1348
const MONSTER_EYE_CARCASS_ADENA = 25
const MONSTER_EYE_LENS_ADENA = 1000
const BASILISK_GIZZARD_ADENA = 60
const BONUS = 618
const BONUS_COUNT = 10
const minimumLevel = 21

type MonsterReward = [ number, number ] // chance, itemId
const monsterRewardChances: { [ npcId: number ]: Array<MonsterReward> } = {
    20055: [ [ 61, MONSTER_EYE_CARCASS ], [ 62, MONSTER_EYE_LENS ] ],
    20059: [ [ 61, MONSTER_EYE_CARCASS ], [ 62, MONSTER_EYE_LENS ] ],
    20067: [ [ 72, MONSTER_EYE_CARCASS ], [ 74, MONSTER_EYE_LENS ] ],
    20068: [ [ 78, MONSTER_EYE_CARCASS ], [ 79, MONSTER_EYE_LENS ] ],
    20070: [ [ 60, BASILISK_GIZZARD ] ],
    20072: [ [ 63, BASILISK_GIZZARD ] ],
}

export class SenseForBusiness extends ListenerLogic {
    constructor() {
        super( 'Q00328_SenseForBusiness', 'listeners/tracked-300/SenseForBusiness.ts' )
        this.questId = 328
        this.questItemIds = [
            MONSTER_EYE_CARCASS,
            MONSTER_EYE_LENS,
            BASILISK_GIZZARD,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00328_SenseForBusiness'
    }

    getQuestStartIds(): Array<number> {
        return [ SARIEN ]
    }

    getTalkIds(): Array<number> {
        return [ SARIEN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let chance = _.random( 100 )
        let reward: MonsterReward = _.find( monsterRewardChances[ data.npcId ], ( item: MonsterReward ): boolean => {
            let [ monsterChance, itemId ] = item
            return chance < QuestHelper.getAdjustedChance( itemId, monsterChance, data.isChampion )
        } )

        if ( !reward ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, reward[ 1 ], 1, data.isChampion )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30436-03.htm':
                state.startQuest()
                break

            case '30436-06.html':
                await state.exitQuest( true, true )
                break

        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '30436-01.htm' : '30436-02.htm' )

            case QuestStateValues.STARTED:
                const carcass = QuestHelper.getQuestItemsCount( player, MONSTER_EYE_CARCASS )
                const lens = QuestHelper.getQuestItemsCount( player, MONSTER_EYE_LENS )
                const gizzards = QuestHelper.getQuestItemsCount( player, BASILISK_GIZZARD )
                const totalAmount = carcass + lens + gizzards

                if ( totalAmount === 0 ) {
                    return this.getPath( '30436-04.html' )
                }

                let adena = ( carcass * MONSTER_EYE_CARCASS_ADENA ) + ( lens * MONSTER_EYE_LENS_ADENA ) + ( gizzards * BASILISK_GIZZARD_ADENA ) + ( totalAmount >= BONUS_COUNT ? BONUS : 0 )
                await QuestHelper.giveAdena( player, adena, true )
                await QuestHelper.takeMultipleItems( player, -1, ...this.questItemIds )
                return this.getPath( '30436-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}