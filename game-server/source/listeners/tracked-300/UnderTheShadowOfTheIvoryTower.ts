import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const MAGIC_TRADER_CEMA = 30834
const LICH_KING_ICARUS = 30835
const COLLECTOR_MARSHA = 30934
const COLLECTOR_TRUMPIN = 30935
const NEBULITE_ORB = 4364
const TOWER_SHIELD = 103
const NICKLACE_OF_MAGIC = 118
const SAGES_BLOOD = 316
const SQUARE_SHIELD = 630
const SCROLL_OF_ESCAPE = 736
const RING_OF_AGES = 885
const NICKLACE_OF_MERMAID = 917
const SCROLL_ENCHANT_WEAPON_C_GRADE = 951
const SCROLL_ENCHANT_WEAPON_D_GRADE = 955
const SPIRITSHOT_D_GRADE = 2510
const SPIRITSHOT_C_GRADE = 2511
const ECTOPLASM_LIQUEUR = 4365
const MANASHEN_GARGOYLE = 20563
const ENCHANTED_MONSTEREYE = 20564
const ENCHANTED_STONE_GOLEM = 20565
const ENCHANTED_IRON_GOLEM = 20566
const minimumLevel = 40

const variableNames = {
    one: 'o',
    two: 't',
}

export class UnderTheShadowOfTheIvoryTower extends ListenerLogic {
    constructor() {
        super( 'Q00343_UnderTheShadowOfTheIvoryTower', 'listeners/tracked-300/UnderTheShadowOfTheIvoryTower.ts' )
        this.questId = 343
    }

    getAttackableKillIds(): Array<number> {
        return [
            MANASHEN_GARGOYLE,
            ENCHANTED_MONSTEREYE,
            ENCHANTED_STONE_GOLEM,
            ENCHANTED_IRON_GOLEM,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00343_UnderTheShadowOfTheIvoryTower'
    }

    getQuestStartIds(): Array<number> {
        return [ MAGIC_TRADER_CEMA ]
    }

    getTalkIds(): Array<number> {
        return [
            MAGIC_TRADER_CEMA,
            LICH_KING_ICARUS,
            COLLECTOR_MARSHA,
            COLLECTOR_TRUMPIN,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case MANASHEN_GARGOYLE:
            case ENCHANTED_MONSTEREYE:
                await this.onMonsterKilled( player, state, 0.63, 0.12, data.isChampion )
                return

            case ENCHANTED_STONE_GOLEM:
                await this.onMonsterKilled( player, state, 0.65, 0.12, data.isChampion )
                return

            case ENCHANTED_IRON_GOLEM:
                await this.onMonsterKilled( player, state, 0.68, 0.13, data.isChampion )
                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30834-05.htm':
                state.setMemoState( 1 )
                state.setMemoStateEx( 1, 0 )
                state.startQuest()
                break

            case '30834-04.htm':
                if ( player.isInCategory( CategoryType.WIZARD_GROUP ) && player.getLevel() >= minimumLevel ) {
                    break
                }

                return

            case '30834-08.html':
                let adenaRewardMultiplier: number = QuestHelper.getQuestItemsCount( player, NEBULITE_ORB )
                if ( adenaRewardMultiplier > 0 ) {
                    await QuestHelper.giveAdena( player, ( adenaRewardMultiplier * 120 ), true )
                    await QuestHelper.takeSingleItem( player, NEBULITE_ORB, -1 )

                    break
                }

                return this.getPath( '30834-08a.html' )

            case '30834-11.html':
                await state.exitQuest( true, true )
                break

            case '30835-02.html':
                if ( !QuestHelper.hasQuestItem( player, ECTOPLASM_LIQUEUR ) ) {
                    break
                }

                await this.rewardPlayerForEctoplasm( player )
                await QuestHelper.takeSingleItem( player, ECTOPLASM_LIQUEUR, 1 )

                return this.getPath( '30835-03.html' )

            case '30934-05.html':
                if ( state.isMemoState( 1 ) ) {
                    if ( state.getMemoStateEx( 1 ) >= 25 ) {
                        break
                    }

                    if ( QuestHelper.getQuestItemsCount( player, NEBULITE_ORB ) < 10 ) {
                        return this.getPath( '30934-06.html' )
                    }

                    state.setMemoState( 2 )
                    await QuestHelper.takeSingleItem( player, NEBULITE_ORB, 10 )

                    return this.getPath( '30934-07.html' )
                }

                return

            case '30934-08a.html':
                if ( !state.isMemoState( 2 ) ) {
                    return
                }

                let chanceOne = _.random( 99 )
                let chanceTwo = _.random( 2 )

                if ( chanceOne < 20 ) {
                    state.setVariable( variableNames.one, chanceTwo )

                    if ( chanceTwo === 1 ) {
                        return this.getPath( '30934-08b.html' )
                    }

                    if ( chanceTwo === 2 ) {
                        return this.getPath( '30934-08c.html' )
                    }

                    state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) + 4 )
                    break
                }

                if ( chanceOne < 50 ) {
                    if ( chanceTwo === 0 ) {
                        state.setVariable( variableNames.one, _.random( 1 ) )
                        return this.getPath( '30934-09a.html' )
                    }

                    if ( chanceTwo === 1 ) {
                        state.setVariable( variableNames.one, _.random( 1, 2 ) )
                        return this.getPath( '30934-09b.html' )
                    }

                    state.setVariable( variableNames.one, _.random( 1 ) === 0 ? 2 : 0 )
                    return this.getPath( '30934-09c.html' )
                }

                state.setVariable( variableNames.one, _.random( 2 ) )
                return this.getPath( '30934-10.html' )

            case '30934-11a.html':
                if ( !state.isMemoState( 2 ) ) {
                    return
                }

                state.setMemoState( 1 )

                switch ( state.getVariable( variableNames.one ) ) {
                    case 0:
                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 10 )
                        state.setVariable( variableNames.one, 4 )

                        return this.getPath( data.eventName )

                    case 1:
                        return this.getPath( '30934-11b.html' )

                    case 2:
                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 20 )
                        state.setVariable( variableNames.one, 4 )

                        return this.getPath( '30934-11c.html' )
                }

                return

            case '30934-12a.html':
                if ( !state.isMemoState( 2 ) ) {
                    return
                }

                state.setMemoState( 1 )

                switch ( state.getVariable( variableNames.one ) ) {
                    case 0:
                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 20 )
                        state.setVariable( variableNames.one, 4 )

                        return this.getPath( data.eventName )

                    case 1:
                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 10 )
                        state.setVariable( variableNames.one, 4 )

                        return this.getPath( '30934-12b.html' )

                    case 2:
                        return this.getPath( '30934-12c.html' )
                }

                return

            case '30934-13a.html':
                if ( !state.isMemoState( 2 ) ) {
                    return
                }

                state.setMemoState( 1 )

                switch ( state.getVariable( variableNames.one ) ) {
                    case 0:
                        return this.getPath( data.eventName )

                    case 1:
                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 20 )
                        state.setVariable( variableNames.one, 4 )

                        return this.getPath( '30934-13b.html' )

                    case 2:
                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 10 )
                        state.setVariable( variableNames.one, 4 )

                        return this.getPath( '30934-13c.html' )
                }

                return

            case '30935-03.html':
                if ( QuestHelper.getQuestItemsCount( player, NEBULITE_ORB ) < 10 ) {
                    break
                }

                state.setVariable( variableNames.two, _.random( 1 ) )
                return this.getPath( '30935-04.html' )

            case '30935-05.html':

                if ( QuestHelper.getQuestItemsCount( player, NEBULITE_ORB ) < 10 ) {
                    return this.getPath( '30935-03.html' )
                }

                if ( state.getVariable( variableNames.two ) === 1 ) {
                    await QuestHelper.takeSingleItem( player, NEBULITE_ORB, 10 )

                    state.setVariable( variableNames.one, 0 )
                    state.setVariable( variableNames.two, 2 )

                    return this.getPath( '30935-06.html' )
                }

                if ( state.getVariable( variableNames.two ) !== 0 ) {
                    return
                }

                switch ( state.getVariable( variableNames.one ) ) {
                    case 0:
                        state.setVariable( variableNames.one, 1 )
                        state.setVariable( variableNames.two, 2 )

                        return this.getPath( data.eventName )

                    case 1:
                        state.setVariable( variableNames.one, 2 )
                        state.setVariable( variableNames.two, 2 )

                        return this.getPath( '30935-05a.html' )

                    case 2:
                        state.setVariable( variableNames.one, 3 )
                        state.setVariable( variableNames.two, 2 )

                        return this.getPath( '30935-05b.html' )

                    case 3:
                        state.setVariable( variableNames.one, 4 )
                        state.setVariable( variableNames.two, 2 )

                        return this.getPath( '30935-05c.html' )

                    case 4:
                        state.setVariable( variableNames.one, 0 )
                        state.setVariable( variableNames.two, 2 )

                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 310 )
                        return this.getPath( '30935-05d.html' )
                }

                return

            case '30935-07.html':
                if ( QuestHelper.getQuestItemsCount( player, NEBULITE_ORB ) < 10 ) {
                    return this.getPath( '30935-03.html' )
                }

                if ( state.getVariable( variableNames.two ) === 0 ) {
                    await QuestHelper.takeSingleItem( player, NEBULITE_ORB, 10 )
                    state.setVariable( variableNames.one, 0 )
                    state.setVariable( variableNames.two, 2 )
                    break
                }

                if ( state.getVariable( variableNames.two ) !== 1 ) {
                    return
                }

                switch ( state.getVariable( variableNames.one ) ) {
                    case 0:
                        state.setVariable( variableNames.one, 1 )
                        state.setVariable( variableNames.two, 2 )

                        return this.getPath( '30935-08.html' )

                    case 1:
                        state.setVariable( variableNames.one, 2 )
                        state.setVariable( variableNames.two, 2 )

                        return this.getPath( '30935-08a.html' )

                    case 2:
                        state.setVariable( variableNames.one, 3 )
                        state.setVariable( variableNames.two, 2 )

                        return this.getPath( '30935-08b.html' )

                    case 3:
                        state.setVariable( variableNames.one, 4 )
                        state.setVariable( variableNames.two, 2 )

                        return this.getPath( '30935-08c.html' )

                    case 4:
                        state.setVariable( variableNames.one, 0 )
                        state.setVariable( variableNames.two, 2 )

                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 310 )
                        return this.getPath( '30935-08d.html' )
                }

                return

            case '30935-09.html':
                switch ( state.getVariable( variableNames.one ) ) {
                    case 1:
                        state.setVariable( variableNames.one, 0 )
                        state.setVariable( variableNames.two, 2 )

                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 10 )
                        return this.getPath( data.eventName )

                    case 2:
                        state.setVariable( variableNames.one, 0 )
                        state.setVariable( variableNames.two, 2 )

                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 30 )
                        return this.getPath( '30935-09a.html' )

                    case 3:
                        state.setVariable( variableNames.one, 0 )
                        state.setVariable( variableNames.two, 2 )

                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 70 )
                        return this.getPath( '30935-09b.html' )

                    case 4:
                        state.setVariable( variableNames.one, 0 )
                        state.setVariable( variableNames.two, 2 )

                        await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 150 )
                        return this.getPath( '30935-09c.html' )
                }

                return

            case '30935-10.html':
                state.setVariable( variableNames.one, 0 )
                state.setVariable( variableNames.two, 2 )
                break

            case '30834-04a.html':
            case '30834-06a.html':
            case '30834-09a.html':
            case '30834-09b.html':
            case '30834-11a.html':
            case '30834-09.html':
            case '30834-10.html':
            case '30835-04.html':
            case '30835-05.html':
            case '30934-03a.html':
            case '30935-01.html':
            case '30935-01a.html':
            case '30935-01b.html':
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== MAGIC_TRADER_CEMA ) {
                    break
                }

                if ( !player.isInCategory( CategoryType.WIZARD_GROUP ) ) {
                    return this.getPath( '30834-01.htm' )
                }

                if ( player.getLevel() >= minimumLevel ) {
                    return this.getPath( '30834-03.htm' )
                }

                return this.getPath( '30834-02.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MAGIC_TRADER_CEMA:
                        if ( !QuestHelper.hasQuestItems( player, NEBULITE_ORB ) ) {
                            return this.getPath( '30834-06.html' )
                        }

                        return this.getPath( '30834-07.html' )

                    case LICH_KING_ICARUS:
                        return this.getPath( '30835-01.html' )

                    case COLLECTOR_MARSHA:
                        if ( state.getMemoStateEx( 1 ) === 0 ) {
                            state.setMemoStateEx( 1, 1 )
                            return this.getPath( '30934-03.html' )
                        }

                        state.setMemoState( 1 )
                        return this.getPath( '30934-04.html' )

                    case COLLECTOR_TRUMPIN:
                        state.setVariable( variableNames.one, 0 )
                        return this.getPath( '30935-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onMonsterKilled( player: L2PcInstance, state: QuestState, rewardChance: number, stateValueChance: number, isFromChampion: boolean ): Promise<void> {
        if ( Math.random() < QuestHelper.getAdjustedChance( NEBULITE_ORB, rewardChance, isFromChampion ) ) {
            await QuestHelper.rewardSingleItem( player, NEBULITE_ORB, 1 )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        if ( state.getMemoStateEx( 1 ) > 1 && Math.random() <= stateValueChance ) {
            state.setMemoStateEx( 1, state.getMemoStateEx( 1 ) - 1 )
        }
    }

    async rewardPlayerForEctoplasm( player: L2PcInstance ): Promise<void> {
        const chance = _.random( 1000 )

        if ( chance <= 119 ) {
            return QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_WEAPON_D_GRADE, 1 )
        }

        if ( chance <= 169 ) {
            return QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_WEAPON_C_GRADE, 1 )
        }

        if ( chance <= 329 ) {
            return QuestHelper.rewardSingleItem( player, SPIRITSHOT_C_GRADE, _.random( 200 ) + 401 )
        }

        if ( chance <= 559 ) {
            return QuestHelper.rewardSingleItem( player, SPIRITSHOT_D_GRADE, _.random( 200 ) + 401 )
        }

        if ( chance <= 561 ) {
            return QuestHelper.rewardSingleItem( player, SAGES_BLOOD, 1 )
        }

        if ( chance <= 578 ) {
            return QuestHelper.rewardSingleItem( player, SQUARE_SHIELD, 1 )
        }

        if ( chance <= 579 ) {
            return QuestHelper.rewardSingleItem( player, NICKLACE_OF_MAGIC, 1 )
        }

        if ( chance <= 581 ) {
            return QuestHelper.rewardSingleItem( player, RING_OF_AGES, 1 )
        }

        if ( chance <= 582 ) {
            return QuestHelper.rewardSingleItem( player, TOWER_SHIELD, 1 )
        }

        if ( chance <= 584 ) {
            return QuestHelper.rewardSingleItem( player, NICKLACE_OF_MERMAID, 1 )
        }

        return QuestHelper.rewardSingleItem( player, SCROLL_OF_ESCAPE, 1 )
    }
}