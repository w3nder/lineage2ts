import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableAttackedEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2Character } from '../../gameService/models/actor/L2Character'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'

const LORAIN = 30673
const CATHEROK = 21035
const TITAN_LAMP1 = 5875
const TITAN_LAMP2 = 5876
const TITAN_LAMP3 = 5877
const TITAN_LAMP4 = 5878
const TITAN_LAMP5 = 5879
const BROKEN_TITAN_LAMP = 5880
const minimumLevel = 37

const thunderStormSkill = 4072

export class ElectrifyingRecharge extends ListenerLogic {
    constructor() {
        super( 'Q00367_ElectrifyingRecharge', 'listeners/tracked-300/ElectrifyingRecharge.ts' )
        this.questId = 367
        this.questItemIds = [
            TITAN_LAMP1,
            TITAN_LAMP2,
            TITAN_LAMP3,
            TITAN_LAMP4,
            TITAN_LAMP5,
            BROKEN_TITAN_LAMP,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [ CATHEROK ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00367_ElectrifyingRecharge'
    }

    getQuestStartIds(): Array<number> {
        return [ LORAIN ]
    }

    getTalkIds(): Array<number> {
        return [ LORAIN ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        if ( NpcVariablesManager.get( data.targetId, this.getName() ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        NpcVariablesManager.set( data.targetId, this.getName(), true )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        AIEffectHelper.notifyMustCastSpell( npc, L2World.getObjectById( data.attackerId ) as L2Character, thunderStormSkill, 4, false, false )

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.attackerPlayerId ), -1, 1, npc )
        if ( !player ) {
            return
        }

        if ( QuestHelper.hasQuestItem( player, TITAN_LAMP5 ) ) {
            return
        }

        switch ( _.random( 37 ) ) {
            case 0:
                if ( QuestHelper.hasQuestItem( player, TITAN_LAMP1 ) ) {
                    await QuestHelper.takeSingleItem( player, TITAN_LAMP1, -1 )
                    await QuestHelper.giveSingleItem( player, TITAN_LAMP2, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( QuestHelper.hasQuestItem( player, TITAN_LAMP2 ) ) {
                    await QuestHelper.giveSingleItem( player, TITAN_LAMP3, 1 )
                    await QuestHelper.takeSingleItem( player, TITAN_LAMP2, -1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( QuestHelper.hasQuestItem( player, TITAN_LAMP3 ) ) {
                    await QuestHelper.giveSingleItem( player, TITAN_LAMP4, 1 )
                    await QuestHelper.takeSingleItem( player, TITAN_LAMP3, -1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( QuestHelper.hasQuestItem( player, TITAN_LAMP4 ) ) {
                    await QuestHelper.giveSingleItem( player, TITAN_LAMP5, 1 )
                    await QuestHelper.takeSingleItem( player, TITAN_LAMP4, -1 )

                    let otherState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
                    otherState.setConditionWithSound( 2, true )
                }

                return

            case 1:
                if ( !QuestHelper.hasQuestItem( player, BROKEN_TITAN_LAMP ) ) {
                    await QuestHelper.giveSingleItem( player, BROKEN_TITAN_LAMP, 1 )
                    await QuestHelper.takeMultipleItems( player, -1, TITAN_LAMP1, TITAN_LAMP2, TITAN_LAMP3, TITAN_LAMP4 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30673-02.htm':
                state.startQuest()

                let player = L2World.getPlayer( data.playerId )
                await QuestHelper.giveSingleItem( player, TITAN_LAMP1, 1 )
                break

            case '30673-05.html':
                break

            case '30673-06.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30673-01.htm' : '30673-03.html' )

            case QuestStateValues.STARTED:
                if ( !QuestHelper.hasAtLeastOneQuestItem( player, TITAN_LAMP5, BROKEN_TITAN_LAMP ) ) {
                    return this.getPath( '30673-04.html' )
                }

                if ( QuestHelper.hasQuestItem( player, BROKEN_TITAN_LAMP ) ) {
                    await QuestHelper.giveSingleItem( player, TITAN_LAMP1, 1 )
                    await QuestHelper.takeSingleItem( player, BROKEN_TITAN_LAMP, -1 )

                    return this.getPath( '30673-07.html' )
                }

                if ( QuestHelper.hasQuestItem( player, TITAN_LAMP5 ) ) {
                    await QuestHelper.rewardSingleItem( player, this.getRewardItemId(), 1 )
                    await QuestHelper.takeSingleItem( player, TITAN_LAMP5, -1 )
                    await QuestHelper.giveSingleItem( player, TITAN_LAMP1, 1 )

                    return this.getPath( '30673-08.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getRewardItemId(): number {
        switch ( _.random( 13 ) ) {
            case 0:
                return 4553 // Greater Dye of STR <Str+1 Con-1>

            case 1:
                return 4554 // Greater Dye of STR <Str+1 Dex-1>

            case 2:
                return 4555 // Greater Dye of CON <Con+1 Str-1>

            case 3:
                return 4556 // Greater Dye of CON <Con+1 Dex-1>

            case 4:
                return 4557 // Greater Dye of DEX <Dex+1 Str-1>

            case 5:
                return 4558 // Greater Dye of DEX <Dex+1 Con-1>

            case 6:
                return 4559 // Greater Dye of INT <Int+1 Men-1>

            case 7:
                return 4560 // Greater Dye of INT <Int+1 Wit-1>

            case 8:
                return 4561 // Greater Dye of MEN <Men+1 Int-1>

            case 9:
                return 4562 // Greater Dye of MEN <Men+1 Wit-1>

            case 10:
                return 4563 // Greater Dye of WIT <Wit+1 Int-1>

            case 11:
                return 4564 // Greater Dye of WIT <Wit+1 Men-1>
        }

        return 4445 // Dye of STR <Str+1 Con-3>
    }
}