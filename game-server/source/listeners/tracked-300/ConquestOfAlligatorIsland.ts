import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const KLUCK = 30895
const ALLIGATOR_TOOTH = 5863
const MYSTERIOUS_MAP_PIECE = 5864
const PIRATES_TREASURE_MAP = 5915
const minimumLevel = 38

const monsterRewardChances = {
    20804: 84, // crokian_lad
    20805: 91, // dailaon_lad
    20806: 88, // crokian_lad_warrior
    20807: 92, // farhite_lad
}

const monsterAmountChances = {
    22208: 14, // nos_lad
    20991: 69, // tribe_of_swamp
}

export class ConquestOfAlligatorIsland extends ListenerLogic {
    constructor() {
        super( 'Q00354_ConquestOfAlligatorIsland', 'listeners/tracked-300/ConquestOfAlligatorIsland.ts' )
        this.questId = 354
        this.questItemIds = [
            ALLIGATOR_TOOTH,
            MYSTERIOUS_MAP_PIECE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ..._.keys( monsterRewardChances ).map( value => _.parseInt( value ) ),
            ..._.keys( monsterAmountChances ).map( value => _.parseInt( value ) )
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00354_ConquestOfAlligatorIsland'
    }

    getQuestStartIds(): Array<number> {
        return [ KLUCK ]
    }

    getTalkIds(): Array<number> {
        return [ KLUCK ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        switch ( data.npcId ) {
            case 22208:
            case 20991:
                let itemCount = _.random( 100 ) < QuestHelper.getAdjustedChance( ALLIGATOR_TOOTH, monsterAmountChances[ data.npcId ], data.isChampion ) ? 2 : 1
                await QuestHelper.rewardSingleQuestItem( player, ALLIGATOR_TOOTH, itemCount, data.isChampion )
                break

            default:
                if ( _.random( 100 ) < QuestHelper.getAdjustedChance( ALLIGATOR_TOOTH, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, ALLIGATOR_TOOTH, 1, data.isChampion )
                }

                break
        }

        if ( _.random( 100 ) < QuestHelper.getAdjustedChance( MYSTERIOUS_MAP_PIECE, 10, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, MYSTERIOUS_MAP_PIECE, 1, data.isChampion )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30895-04.html':
            case '30895-05.html':
            case '30895-09.html':
                break

            case '30895-02.html':
                state.startQuest()
                break

            case '30895-10.html':
                await state.exitQuest( true, true )
                break

            case 'REWARD':
                let pieceAmount = QuestHelper.getQuestItemsCount( player, MYSTERIOUS_MAP_PIECE )
                if ( pieceAmount >= 10 ) {
                    await QuestHelper.giveSingleItem( player, PIRATES_TREASURE_MAP, 1 )
                    await QuestHelper.takeSingleItem( player, MYSTERIOUS_MAP_PIECE, 10 )

                    return this.getPath( '30895-13.html' )
                }

                if ( pieceAmount > 0 ) {
                    return this.getPath( '30895-12.html' )
                }

                return

            case 'ADENA':
                let toothAmount = QuestHelper.getQuestItemsCount( player, ALLIGATOR_TOOTH )

                if ( toothAmount > 0 ) {
                    let adenaReward = toothAmount * 220
                    await QuestHelper.takeSingleItem( player, ALLIGATOR_TOOTH, -1 )

                    if ( toothAmount >= 100 ) {
                        await QuestHelper.giveAdena( player, adenaReward + 10700, true )
                        return this.getPath( '30895-06.html' )
                    }

                    await QuestHelper.giveAdena( player, adenaReward + 3100, true )
                    return this.getPath( '30895-07.html' )
                }

                return this.getPath( '30895-08.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30895-01.htm' : '30895-03.html' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItem( player, MYSTERIOUS_MAP_PIECE ) ? '30895-11.html' : '30895-04.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}