import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const CHAIREN = 32655
const PROTECTION_SOULS_PENDANT = 14848
const SOUL_CORE_CONTAINING_EVIL_SPIRIT = 14881
const RAGNA_ORCS_AMULET = 14882
const minimumLevel = 80
const SOUL_CORE_COUNT = 10
const RAGNA_ORCS_KILLS_COUNT = 100
const RAGNA_ORCS_AMULET_COUNT = 10

const monsterRewardChances = {
    22691: 0.694, // Ragna Orc
    22692: 0.716, // Ragna Orc Warrior
    22693: 0.736, // Ragna Orc Hero
    22694: 0.712, // Ragna Orc Commander
    22695: 0.698, // Ragna Orc Healer
    22696: 0.692, // Ragna Orc Shaman
    22697: 0.640, // Ragna Orc Seer
    22698: 0.716, // Ragna Orc Archer
    22699: 0.752, // Ragna Orc Sniper
    22701: 0.716, // Varangka's Dre Vanul
    22702: 0.662, // Varangka's Destroyer
}

const variableNames = {
    killCount: 'kc',
}

export class ExpulsionOfEvilSpirits extends ListenerLogic {
    constructor() {
        super( 'Q00311_ExpulsionOfEvilSpirits', 'listeners/tracked-300/ExpulsionOfEvilSpirits.ts' )
        this.questId = 311
        this.questItemIds = [
            SOUL_CORE_CONTAINING_EVIL_SPIRIT,
            RAGNA_ORCS_AMULET,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00311_ExpulsionOfEvilSpirits'
    }

    getQuestStartIds(): Array<number> {
        return [ CHAIREN ]
    }

    getTalkIds(): Array<number> {
        return [ CHAIREN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 2, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        let count: number = state.getVariable( variableNames.killCount ) + 1
        if ( count >= RAGNA_ORCS_KILLS_COUNT
                && _.random( 20 ) < QuestHelper.getAdjustedChance( SOUL_CORE_CONTAINING_EVIL_SPIRIT, count % RAGNA_ORCS_KILLS_COUNT + 1, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, SOUL_CORE_CONTAINING_EVIL_SPIRIT, 1, data.isChampion )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

            count = 0
        }

        state.setVariable( variableNames.killCount, count )

        if ( Math.random() <= QuestHelper.getAdjustedChance( RAGNA_ORCS_AMULET, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, RAGNA_ORCS_AMULET, 1, data.isChampion )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32655-03.htm':
            case '32655-15.html':
                break

            case '32655-04.htm':
                state.startQuest()
                state.setVariable( variableNames.killCount, 0 )
                break

            case '32655-11.html':
                if ( QuestHelper.getQuestItemsCount( player, SOUL_CORE_CONTAINING_EVIL_SPIRIT ) < SOUL_CORE_COUNT ) {
                    return this.getPath( '32655-12.html' )
                }

                await QuestHelper.takeSingleItem( player, SOUL_CORE_CONTAINING_EVIL_SPIRIT, SOUL_CORE_COUNT )
                await QuestHelper.giveSingleItem( player, PROTECTION_SOULS_PENDANT, 1 )
                break

            case '32655-13.html':
                if ( !QuestHelper.hasQuestItem( player, SOUL_CORE_CONTAINING_EVIL_SPIRIT )
                        && QuestHelper.getQuestItemsCount( player, RAGNA_ORCS_AMULET ) >= RAGNA_ORCS_AMULET_COUNT ) {
                    await state.exitQuest( true, true )
                    break
                }

                return this.getPath( '32655-14.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32655-01.htm' : '32655-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItems( player, SOUL_CORE_CONTAINING_EVIL_SPIRIT, RAGNA_ORCS_AMULET ) ? '32655-06.html' : '32655-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}