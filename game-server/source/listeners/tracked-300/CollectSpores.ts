import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const HERBIEL = 30150
const SPORE_SAC = 1118
const minimumLevel = 8
const REQUIRED_SAC_COUNT = 10
const SPORE_FUNGUS = 20509

export class CollectSpores extends ListenerLogic {
    constructor() {
        super( 'Q00313_CollectSpores', 'listeners/tracked-300/CollectSpores.ts' )
        this.questId = 313
        this.questItemIds = [
            SPORE_SAC,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ SPORE_FUNGUS ]
    }

    getQuestStartIds(): Array<number> {
        return [ HERBIEL ]
    }

    getTalkIds(): Array<number> {
        return [ HERBIEL ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00313_CollectSpores'
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( Math.random() >= QuestHelper.getAdjustedChance( SPORE_SAC, 0.4, data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardAndProgressState( player, state, SPORE_SAC, 1, REQUIRED_SAC_COUNT, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30150-05.htm':
                state.startQuest()
                break

            case '30150-04.htm':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30150-03.htm' : '30150-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30150-06.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, SPORE_SAC ) < REQUIRED_SAC_COUNT ) {
                            return this.getPath( '30150-06.html' )
                        }

                        await QuestHelper.giveAdena( player, 3500, true )
                        await state.exitQuest( true, true )

                        return this.getPath( '30150-07.html' )
                }

                break

        }

        return QuestHelper.getNoQuestMessagePath()
    }
}