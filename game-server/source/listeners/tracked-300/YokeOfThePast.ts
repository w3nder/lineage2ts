import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const npcIds = [
    31095, 31096, 31097, 31098, 31099, 31100, 31101,
    31102, 31103, 31104, 31105, 31106, 31107, 31108,
    31109, 31110, 31114, 31115, 31116, 31117, 31118,
    31119, 31120, 31121, 31122, 31123, 31124, 31125,
]

const SCROLL_OF_ANCIENT_MAGIC = 5902
const BLANK_SCROLL = 5965
const minimumLevel = 20

const monsterRewardChances = {
    21144: 0.306, // Catacomb Shadow
    21156: 0.994, // Purgatory Shadow
    21208: 0.146, // Hallowed Watchman
    21209: 0.166, // Hallowed Seer
    21210: 0.202, // Vault Guardian
    21211: 0.212, // Vault Seer
    21213: 0.274, // Hallowed Monk
    21214: 0.342, // Vault Sentinel
    21215: 0.360, // Vault Monk
    21217: 0.460, // Hallowed Priest
    21218: 0.558, // Vault Overlord
    21219: 0.578, // Vault Priest
    21221: 0.710, // Sepulcher Inquisitor
    21222: 0.842, // Sepulcher Archon
    21223: 0.862, // Sepulcher Inquisitor
    21224: 0.940, // Sepulcher Guardian
    21225: 0.970, // Sepulcher Sage
    21226: 0.202, // Sepulcher Guardian
    21227: 0.290, // Sepulcher Sage
    21228: 0.316, // Sepulcher Guard
    21229: 0.426, // Sepulcher Preacher
    21230: 0.646, // Sepulcher Guard
    21231: 0.654, // Sepulcher Preacher
    21236: 0.238, // Barrow Sentinel
    21237: 0.274, // Barrow Monk
    21238: 0.342, // Grave Sentinel
    21239: 0.360, // Grave Monk
    21240: 0.410, // Barrow Overlord
    21241: 0.460, // Barrow Priest
    21242: 0.558, // Grave Overlord
    21243: 0.578, // Grave Priest
    21244: 0.642, // Crypt Archon
    21245: 0.700, // Crypt Inquisitor
    21246: 0.842, // Tomb Archon
    21247: 0.862, // Tomb Inquisitor
    21248: 0.940, // Crypt Guardian
    21249: 0.970, // Crypt Sage
    21250: 0.798, // Tomb Guardian
    21251: 0.710, // Tomb Sage
    21252: 0.684, // Crypt Guard
    21253: 0.574, // Crypt Preacher
    21254: 0.354, // Tomb Guard
    21255: 0.250, // Tomb Preacher
}

export class YokeOfThePast extends ListenerLogic {
    constructor() {
        super( 'Q00385_YokeOfThePast', 'listeners/tracked-300/YokeOfThePast.ts' )
        this.questId = 385
        this.questItemIds = [
            SCROLL_OF_ANCIENT_MAGIC,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00385_YokeOfThePast'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( SCROLL_OF_ANCIENT_MAGIC, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, SCROLL_OF_ANCIENT_MAGIC, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case 'ziggurat-03.htm':
            case 'ziggurat-04.htm':
            case 'ziggurat-06.htm':
            case 'ziggurat-07.htm':
                break

            case 'ziggurat-05.htm':
                state.startQuest()
                break

            case 'ziggurat-10.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? 'ziggurat-01.htm' : 'ziggurat-02.htm' )

            case QuestStateValues.STARTED:
                let amount: number = QuestHelper.getQuestItemsCount( player, SCROLL_OF_ANCIENT_MAGIC )
                if ( amount > 0 ) {
                    await QuestHelper.rewardSingleItem( player, BLANK_SCROLL, amount )
                    await QuestHelper.takeSingleItem( player, SCROLL_OF_ANCIENT_MAGIC, -1 )

                    return this.getPath( 'ziggurat-09.html' )
                }

                return this.getPath( 'ziggurat-08.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}