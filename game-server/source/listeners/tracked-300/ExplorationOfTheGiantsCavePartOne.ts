import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const SOBLING = 31147
const ANCIENT_PARCHMENT = 14841
const BOOK1 = 14836
const BOOK2 = 14837
const BOOK3 = 14838
const BOOK4 = 14839
const BOOK5 = 14840
const minimumLevel = 79

const monsterRewardChances = {
    22670: 0.314, // lord
    22671: 0.302, // gaurdian
    22672: 0.300, // seer
    22673: 0.258, // hirokai
    22674: 0.248, // imagro
    22675: 0.264, // palite
    22676: 0.258, // hamrit
    22677: 0.266, // kranout
}

export class ExplorationOfTheGiantsCavePartOne extends ListenerLogic {
    constructor() {
        super( 'Q00376_ExplorationOfTheGiantsCavePart1', 'listeners/tracked-300/ExplorationOfTheGiantsCavePartOne.ts' )
        this.questId = 376
        this.questItemIds = [
            ANCIENT_PARCHMENT,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00376_ExplorationOfTheGiantsCavePart1'
    }

    getQuestStartIds(): Array<number> {
        return [ SOBLING ]
    }

    getTalkIds(): Array<number> {
        return [ SOBLING ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( ANCIENT_PARCHMENT, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, ANCIENT_PARCHMENT, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31147-02.htm':
                state.startQuest()
                break

            case '31147-04.html':
            case '31147-cont.html':
                break

            case '31147-quit.html':
                await state.exitQuest( true, true )
                break

        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31147-01.htm' : '31147-00.html' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItems( player, BOOK1, BOOK2, BOOK3, BOOK4, BOOK5 ) ? '31147-03.html' : '31147-02a.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}