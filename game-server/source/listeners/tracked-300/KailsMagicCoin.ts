import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

import _ from 'lodash'

const VERGARA = 30687
const FALLEN_ORC = 21017
const FALLEN_ORC_ARCHER = 21019
const FALLEN_ORC_SHAMAN = 21020
const FALLEN_ORC_CAPTAIN = 21022
const ROYAL_MEMBERSHIP = 5898
const silverBasiliskItemId = 5961
const goldGolemItemId = 5962
const bloodDragonItemId = 5963
const minimumLevel = 55

type MonsterReward = [ number, number ] // itemId, chance
const monsterRewards: { [ npcId: number ]: MonsterReward } = {
    [ FALLEN_ORC ]: [ silverBasiliskItemId, 0.073 ],
    [ FALLEN_ORC_ARCHER ]: [ goldGolemItemId, 0.075 ],
    [ FALLEN_ORC_SHAMAN ]: [ bloodDragonItemId, 0.073 ],
    [ FALLEN_ORC_CAPTAIN ]: [ 0, 0.069 ],
}

export class KailsMagicCoin extends ListenerLogic {
    constructor() {
        super( 'Q00382_KailsMagicCoin', 'listeners/tracked-300/KailsMagicCoin.ts' )
        this.questId = 382
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00382_KailsMagicCoin'
    }

    getQuestStartIds(): Array<number> {
        return [ VERGARA ]
    }

    getTalkIds(): Array<number> {
        return [ VERGARA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let [ itemId, chance ]: MonsterReward = monsterRewards[ data.npcId ]

        if ( data.npcId === FALLEN_ORC_CAPTAIN ) {
            itemId = silverBasiliskItemId + _.random( 3 )
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30687-03.htm':
                state.startQuest()
                break

            case '30687-05.htm':
            case '30687-06.htm':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let shouldProceed: boolean = player.getLevel() >= minimumLevel && QuestHelper.hasQuestItems( player, ROYAL_MEMBERSHIP )
                return this.getPath( shouldProceed ? '30687-02.htm' : '30687-01.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( '30687-04.htm' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}