import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { PlayerRadarCache } from '../../gameService/cache/PlayerRadarCache'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ATRA = 32647
const CORRUPTED_MUCROKIAN = 22654
const MUCROKIAN_HIDE = 14873
const FALLEN_MUCROKIAN_HIDE = 14874
const REC_DYNASTY_EARRINGS_70 = 9985
const REC_DYNASTY_NECKLACE_70 = 9986
const REC_DYNASTY_RING_70 = 9987
const REC_DYNASTY_SIGIL_60 = 10115

const MOIRAI_RECIPES = [
    15777,
    15780,
    15783,
    15786,
    15789,
    15790,
    15814,
    15813,
    15812,
]

const MOIRAI_PIECES = [
    15647,
    15650,
    15653,
    15656,
    15659,
    15692,
    15772,
    15773,
    15774,
]

const minimumLevel = 82
const monsterRewardChances = {
    22650: 218, // Mucrokian Fanatic
    22651: 258, // Mucrokian Ascetic
    22652: 248, // Mucrokian Savior
    22653: 290, // Mucrokian Preacher
    22654: 124, // Contaminated Mucrokian
    22655: 220, // Awakened Mucrokian
}

export class ForAGoodCause extends ListenerLogic {
    constructor() {
        super( 'Q00309_ForAGoodCause', 'listeners/tracked-300/ForAGoodCause.ts' )
        this.questId = 309
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00309_ForAGoodCause'
    }

    getQuestStartIds(): Array<number> {
        return [ ATRA ]
    }

    getTalkIds(): Array<number> {
        return [ ATRA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        if ( _.random( 1000 ) >= QuestHelper.getAdjustedChance( MUCROKIAN_HIDE, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let itemId: number = data.npcId === CORRUPTED_MUCROKIAN ? FALLEN_MUCROKIAN_HIDE : MUCROKIAN_HIDE

        let amount = itemId === FALLEN_MUCROKIAN_HIDE ? 2 : 1
        await QuestHelper.rewardSingleQuestItem( player, itemId, amount, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32647-02.htm':
            case '32647-03.htm':
            case '32647-04.htm':
            case '32647-08.html':
            case '32647-10.html':
            case '32647-12.html':
            case '32647-13.html':
                break

            case '32647-05.html':
                state.startQuest()
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 77325, 205773, -3432 )
                break

            case 'claimreward':
                return this.getPath( player.hasQuestCompleted( 'Q00239_WontYouJoinUs' ) ? '32647-11.html' : '32647-09.html' )

            case '100':
            case '120':
                return this.rewardItems( player, _.sample( MOIRAI_PIECES ), _.parseInt( data.eventName ), true )

            case '192':
            case '230':
                return this.rewardItems( player, REC_DYNASTY_EARRINGS_70, _.parseInt( data.eventName ), false )

            case '256':
            case '308':
                return this.rewardItems( player, REC_DYNASTY_NECKLACE_70, _.parseInt( data.eventName ), false )

            case '128':
            case '154':
                return this.rewardItems( player, REC_DYNASTY_RING_70, _.parseInt( data.eventName ), false )

            case '206':
            case '246':
                return this.rewardItems( player, REC_DYNASTY_SIGIL_60, _.parseInt( data.eventName ), false )

            case '180':
            case '216':
                return this.rewardItems( player, _.sample( MOIRAI_RECIPES ), _.parseInt( data.eventName ), false )

            case '32647-14.html':
            case '32647-07.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let requiredState: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00308_ReedFieldMaintenance', false )
        if ( requiredState && requiredState.isStarted() ) {
            return this.getPath( '32647-17.html' )
        }

        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32647-01.htm' : '32647-00.html' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasAtLeastOneQuestItem( player, MUCROKIAN_HIDE, FALLEN_MUCROKIAN_HIDE ) ? '32647-08.html' : '32647-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async needsReward( player: L2PcInstance, amount: number ): Promise<boolean> {
        let mucrokian = QuestHelper.getQuestItemsCount( player, MUCROKIAN_HIDE )
        let fallenHideCount = QuestHelper.getQuestItemsCount( player, FALLEN_MUCROKIAN_HIDE )

        if ( fallenHideCount > 0 ) {
            let adjustedAmount = Math.floor( amount / 2 )
            if ( fallenHideCount >= adjustedAmount ) {
                await QuestHelper.takeSingleItem( player, FALLEN_MUCROKIAN_HIDE, adjustedAmount )
                return true
            }

            let reducedAmount: number = amount - ( fallenHideCount * 2 )
            if ( mucrokian >= reducedAmount ) {
                await QuestHelper.takeSingleItem( player, FALLEN_MUCROKIAN_HIDE, fallenHideCount )
                await QuestHelper.takeSingleItem( player, MUCROKIAN_HIDE, reducedAmount )

                return true
            }

            return false
        }

        if ( mucrokian >= amount ) {
            await QuestHelper.takeSingleItem( player, MUCROKIAN_HIDE, amount )
            return true
        }

        return false
    }

    async rewardItems( player: L2PcInstance, itemId: number, amount: number, allowMultiple: boolean ): Promise<string> {

        let result: boolean = await this.needsReward( player, amount )
        if ( !result ) {
            return this.getPath( '32646-13.html' )
        }

        let amountToGive: number = allowMultiple ? _.random( 1, 3 ) : 1

        await QuestHelper.rewardSingleItem( player, itemId, amountToGive )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_FINISH )

        return this.getPath( '32646-14.html' )
    }
}