import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const TRADER_ROLENTO = 30437
const GOLEM_HEARTSTONE = 1346
const BROKEN_HEARTSTONE = 1365
const minimumLevel = 33

type MonsterReward = [ number, number ] // itemId, chance
const monsterRewards: { [ npcId: number ]: Array<MonsterReward> } = {
    20083: [ [ GOLEM_HEARTSTONE, 3 ], [ BROKEN_HEARTSTONE, 54 ] ], // Granitic Golem
    20085: [ [ GOLEM_HEARTSTONE, 3 ], [ BROKEN_HEARTSTONE, 58 ] ], // Puncher
}

export class CuriosityOfADwarf extends ListenerLogic {
    constructor() {
        super( 'Q00329_CuriosityOfADwarf', 'listeners/tracked-300/CuriosityOfADwarf.ts' )
        this.questId = 329
        this.questItemIds = [ GOLEM_HEARTSTONE, BROKEN_HEARTSTONE ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00329_CuriosityOfADwarf'
    }

    getQuestStartIds(): Array<number> {
        return [ TRADER_ROLENTO ]
    }

    getTalkIds(): Array<number> {
        return [ TRADER_ROLENTO ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let chance = _.random( 100 )
        let reward: MonsterReward = _.find( monsterRewards[ data.npcId ], ( item: MonsterReward ): boolean => {
            let [ itemId, mosterChance ] = item
            return QuestHelper.getAdjustedChance( itemId, mosterChance, data.isChampion ) > chance
        } )

        if ( reward ) {
            let [ itemId ] = reward
            await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30437-03.htm':
                state.startQuest()
                break

            case '30437-06.html':
                await state.exitQuest( true, true )
                break

            case '30437-07.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30437-02.htm' : '30437-01.htm' )

            case QuestStateValues.STARTED:
                const broken = QuestHelper.getQuestItemsCount( player, BROKEN_HEARTSTONE )
                const golem = QuestHelper.getQuestItemsCount( player, GOLEM_HEARTSTONE )
                const totalAmount = broken + golem

                if ( totalAmount === 0 ) {
                    return this.getPath( '30437-04.html' )
                }

                let adena = ( broken * 50 ) + ( golem * 1000 ) + ( totalAmount >= 10 ? 1183 : 0 )
                await QuestHelper.giveAdena( player, adena, true )
                await QuestHelper.takeMultipleItems( player, -1, ...this.questItemIds )

                return this.getPath( '30437-05.html' )

        }

        return QuestHelper.getNoQuestMessagePath()
    }
}