import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { QuestStateValues } from '../../gameService/models/quest/State'

const MAESTRO_NIKOLA = 30621
const RESEARCHER_LORAIN = 30673
const DESTROYED_DEVICE = 32366
const ALARM_OF_GIANT = 32367

const METALLOGRAPH = 10359
const BROKEN_METAL_PIECES = 10360
const NIKOLAS_MAP = 10361

const LORAINES_CERTIFICATE = 10362

const minimumLevel = 40
const maximumLevel = 46

enum VariableNames {
    isSpawned = 'spawned',
    playerId = 'playerId',
    characterId = 'characterId'
}

export class ArtOfPersuasion extends ListenerLogic {
    constructor() {
        super( 'Q00184_ArtOfPersuasion', 'listeners/tracked/ArtOfPersuasion.ts' )
        this.questId = 184
        this.questItemIds = [ METALLOGRAPH, BROKEN_METAL_PIECES, NIKOLAS_MAP ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00184_ArtOfPersuasion'
    }

    getQuestStartIds(): Array<number> {
        return [ MAESTRO_NIKOLA ]
    }

    getTalkIds(): Array<number> {
        return [ MAESTRO_NIKOLA, RESEARCHER_LORAIN, DESTROYED_DEVICE, ALARM_OF_GIANT ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30621-06.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveSingleItem( player, NIKOLAS_MAP, 1 )
                    break
                }

                return

            case '30621-03.htm':
                if ( player.getLevel() >= minimumLevel ) {
                    break
                }

                return this.getPath( '30621-03a.htm' )

            case '30621-04.htm':
            case '30621-05.htm':
                break

            case '30673-02.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '30673-03.html':
                if ( state.isMemoState( 1 ) ) {
                    await QuestHelper.takeSingleItem( player, NIKOLAS_MAP, -1 )

                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '30673-05.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '30673-08.html':
                if ( state.isMemoState( 6 ) ) {
                    break
                }

                return

            case '30673-09.html':
                if ( !state.isMemoState( 6 ) ) {
                    return
                }

                await QuestHelper.giveAdena( player, 72527, true )
                if ( player.getLevel() < maximumLevel ) {
                    await QuestHelper.addExpAndSp( player, 203717, 14032 )
                }

                if ( QuestHelper.hasQuestItem( player, METALLOGRAPH ) ) {
                    await QuestHelper.giveSingleItem( player, LORAINES_CERTIFICATE, 1 )
                    await state.exitQuest( false, true )

                    break
                }

                await state.exitQuest( false, true )
                return this.getPath( '30673-10.htm' )


            case '32366-03.html':
                if ( state.isMemoState( 3 ) && !NpcVariablesManager.get( data.characterId, VariableNames.isSpawned ) ) {
                    NpcVariablesManager.set( data.characterId, VariableNames.isSpawned, true )
                    NpcVariablesManager.set( data.characterId, VariableNames.playerId, data.playerId )

                    let npc = QuestHelper.addGenericSpawn( null, ALARM_OF_GIANT, player.getX() + 80, player.getY() + 60, player.getZ(), 16384, false, 0 )

                    // TODO : determine why variables are set since they are not used within this listener
                    NpcVariablesManager.set( npc.getObjectId(), VariableNames.characterId, data.characterId )
                    NpcVariablesManager.set( npc.getObjectId(), VariableNames.playerId, data.playerId )
                }

                return

            case '32366-06.html':
                if ( state.isMemoState( 4 ) ) {
                    await QuestHelper.giveSingleItem( player, METALLOGRAPH, 1 )

                    state.setMemoState( 6 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '32366-08.html':
                if ( state.isMemoState( 5 ) ) {
                    await QuestHelper.giveSingleItem( player, BROKEN_METAL_PIECES, 1 )

                    state.setMemoState( 6 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MAESTRO_NIKOLA ) {

                    let quest183: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00183_RelicExploration' )
                    let quest184: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00184_ArtOfPersuasion' )
                    let quest185: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00185_NikolasCooperation' )

                    if ( quest183 && quest183.isCompleted() && quest184 && quest185 ) {
                        let player = L2World.getPlayer( data.playerId )

                        return this.getPath( player.getLevel() >= minimumLevel ? '30621-01.htm' : '30621-02.html' )
                    }
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case MAESTRO_NIKOLA:
                        if ( memoState === 1 ) {
                            return this.getPath( '30621-07.html' )
                        }

                        break

                    case RESEARCHER_LORAIN:
                        if ( memoState === 1 ) {
                            return this.getPath( '30673-01.html' )
                        }

                        if ( memoState === 2 ) {
                            return this.getPath( '30673-04.html' )
                        }

                        if ( memoState >= 3 && memoState <= 5 ) {
                            return this.getPath( '30673-06.html' )
                        }

                        if ( memoState === 6 ) {
                            return this.getPath( '30673-07.html' )
                        }

                        break

                    case DESTROYED_DEVICE:
                        if ( memoState === 3 ) {
                            if ( !NpcVariablesManager.get( data.characterId, VariableNames.isSpawned ) ) {
                                return this.getPath( '32366-01.html' )
                            }

                            if ( NpcVariablesManager.get( data.characterId, VariableNames.playerId ) === data.playerId ) {
                                return this.getPath( '32366-03.html' )
                            }

                            return this.getPath( '32366-04.html' )
                        }

                        if ( memoState === 4 ) {
                            return this.getPath( '32366-05.html' )
                        }

                        if ( memoState === 5 ) {
                            return this.getPath( '32366-07.html' )
                        }

                        break

                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}