import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

const ADLER1 = 32497
const ADLER2 = 32510
const SINAI = 32500
const INSPECTOR = 32507
const HILLAS = 18610
const PAPION = 18609
const KINSUS = 18608
const GARGOS = 18607
const ADIANTUM = 18620

const SWORD = 13034
const ENH_SWORD1 = 13035
const ENH_SWORD2 = 13036
const BOOK1 = 13130
const BOOK2 = 13131
const BOOK3 = 13132
const BOOK4 = 13133
const BOOK5 = 13134
const BOOK6 = 13135
const BOOK7 = 13136
const WATER_ESSENCE = 13038
const FIRE_ESSENCE = 13039
const SHIELD_POTION = 13032
const HEAL_POTION = 13033
const FIRE_ENHANCER = 13040
const WATER_ENHANCER = 13041
const REWARDS = [
    13294, // Pailaka Ring
    13293, // Pailaka Earring
    736, // Scroll of Escape
]

const vitalityReplentishingSkill = 5774

const minimumLevel = 36
const maximumLevel = 42
const EXIT_TIME = 5

export class PailakaSongOfIceAndFire extends ListenerLogic {
    constructor() {
        super( 'Q00128_PailakaSongOfIceAndFire', 'listeners/tracked/PailakaSongOfIceAndFire.ts' )
        this.questId = 128
        this.questItemIds = [
            SWORD,
            ENH_SWORD1,
            ENH_SWORD2,
            BOOK1,
            BOOK2,
            BOOK3,
            BOOK4,
            BOOK5,
            BOOK6,
            BOOK7,
            WATER_ESSENCE,
            FIRE_ESSENCE,
            SHIELD_POTION,
            HEAL_POTION,
            FIRE_ENHANCER,
            WATER_ENHANCER,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ HILLAS, PAPION, KINSUS, GARGOS, ADIANTUM ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00128_PailakaSongOfIceAndFire'
    }

    getQuestStartIds(): Array<number> {
        return [ ADLER1 ]
    }

    getTalkIds(): Array<number> {
        return [ ADLER1, ADLER2, SINAI, INSPECTOR ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state || !state.isStarted() ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.npcId ) {
            case HILLAS:
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    await QuestHelper.takeSingleItem( player, BOOK1, -1 )
                    await QuestHelper.giveMultipleItems( player, 1, BOOK2, WATER_ESSENCE )
                }

                QuestHelper.addGenericSpawn( null, PAPION, -53903, 181484, -4555, 30456, false, 0, false, npc.getInstanceId() )
                break

            case PAPION:
                if ( state.isCondition( 4 ) ) {
                    state.setConditionWithSound( 5 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    await QuestHelper.takeSingleItem( player, BOOK3, -1 )
                    await QuestHelper.giveSingleItem( player, BOOK4, 1 )
                }

                QuestHelper.addGenericSpawn( null, KINSUS, -61415, 181418, -4818, 63852, false, 0, false, npc.getInstanceId() )
                break

            case KINSUS:
                if ( state.isCondition( 5 ) ) {
                    state.setConditionWithSound( 6 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    await QuestHelper.takeSingleItem( player, BOOK4, -1 )
                    await QuestHelper.giveMultipleItems( player, 1, BOOK5, FIRE_ESSENCE )
                }

                QuestHelper.addGenericSpawn( null, GARGOS, -61354, 183624, -4821, 63613, false, 0, false, npc.getInstanceId() )
                break

            case GARGOS:
                if ( state.isCondition( 7 ) ) {
                    state.setConditionWithSound( 8 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    await QuestHelper.takeSingleItem( player, BOOK6, -1 )
                    await QuestHelper.giveSingleItem( player, BOOK7, 1 )
                }

                QuestHelper.addGenericSpawn( null, ADIANTUM, -53297, 185027, -4617, 1512, false, 0, false, npc.getInstanceId() )
                break

            case ADIANTUM:
                if ( state.isCondition( 8 ) ) {
                    state.setConditionWithSound( 9 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    await QuestHelper.takeSingleItem( player, BOOK7, -1 )
                    QuestHelper.addGenericSpawn( null, ADLER2, -53297, 185027, -4617, 33486, false, 0, false, npc.getInstanceId() )
                }

                break
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32500-02.htm':
            case '32500-03.htm':
            case '32500-04.htm':
            case '32500-05.htm':
            case '32497-02.htm':
            case '32507-07.htm':
            case '32497-04.htm':
                break

            case '32497-03.htm':
                if ( !state.isStarted() ) {
                    state.startQuest()
                    break
                }

                return

            case '32500-06.htm':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )

                    await QuestHelper.giveSingleItem( player, SWORD, 1 )
                    await QuestHelper.giveSingleItem( player, BOOK1, 1 )

                    break
                }

                return

            case '32507-04.htm':
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )
                    await QuestHelper.takeSingleItem( player, SWORD, -1 )
                    await QuestHelper.takeSingleItem( player, WATER_ESSENCE, -1 )
                    await QuestHelper.takeSingleItem( player, BOOK2, -1 )

                    await QuestHelper.giveSingleItem( player, BOOK3, 1 )
                    await QuestHelper.giveSingleItem( player, ENH_SWORD1, 1 )

                    break
                }

                return

            case '32507-08.htm':
                if ( state.isCondition( 6 ) ) {
                    state.setConditionWithSound( 7, true )

                    await QuestHelper.takeSingleItem( player, ENH_SWORD1, -1 )
                    await QuestHelper.takeSingleItem( player, BOOK5, -1 )
                    await QuestHelper.takeSingleItem( player, FIRE_ESSENCE, -1 )

                    await QuestHelper.giveSingleItem( player, ENH_SWORD2, 1 )
                    await QuestHelper.giveSingleItem( player, BOOK6, 1 )

                    break
                }

                return

            case '32510-02.htm':
                await state.exitQuest( false, true )

                // TODO : implement once instances are working
                // Instance inst = InstanceManager.getInstance().getInstance(npc.getInstanceId());
                // instate.setDuration(EXIT_TIME * 60000);
                // instate.setEmptyDestroyTime(0);
                //
                // if (instate.containsPlayer(player.getObjectId())) {
                //     npc.setTarget(player);
                //     npc.doCast(VITALITY_REPLENISHING);
                //     addExpAndSp(player, 810000, 50000);
                //     for (int id : REWARDS) {
                //         giveItems(player, id, 1);
                //     }
                // }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ADLER1:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '32497-05.htm' )
                        }

                        if ( player.getLevel() > maximumLevel ) {
                            return this.getPath( '32497-06.htm' )
                        }

                        return this.getPath( '32497-01.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.getCondition() > 1 ) {
                            return this.getPath( '32497-00.htm' )
                        }

                        return this.getPath( '32497-03.htm' )

                    case QuestStateValues.COMPLETED:
                        return this.getPath( '32497-07.htm' )

                    default:
                        return this.getPath( '32497-01.htm' )
                }

                break

            case SINAI:
                if ( state.getCondition() > 1 ) {
                    return this.getPath( '32500-00.htm' )
                }

                return this.getPath( '32500-01.htm' )

            case INSPECTOR:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32507-01.htm' )

                    case 2:
                        return this.getPath( '32507-02.htm' )

                    case 3:
                        return this.getPath( '32507-03.htm' )

                    case 4:
                    case 5:
                        return this.getPath( '32507-05.htm' )

                    case 6:
                        return this.getPath( '32507-06.htm' )

                    default:
                        return this.getPath( '32507-09.htm' )
                }

                break

            case ADLER2:
                if ( state.isCompleted() ) {
                    return this.getPath( '32510-00.htm' )
                }

                if ( state.isCondition( 9 ) ) {
                    return this.getPath( '32510-01.htm' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}