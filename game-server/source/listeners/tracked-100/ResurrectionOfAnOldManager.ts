import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcSeePlayerEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const NEWYEAR = 31961
const YUMI = 32041
const STONES = 32046
const WENDY = 32047
const BOX = 32050

const STARSTONE = 8287
const LETTER = 8288
const STARSTONE2 = 8289
const DETCTOR = 8090
const DETCTOR2 = 8091

const GUARDIAN = 27318
const minimumLevel = 70

const variableNames = {
    talk: 't',
    choice: 'c',
    talkSecond: 'ts',
    spawned: 'sp',
}

const eventNames = {
    golemDespawn: 'gd',
}

export class ResurrectionOfAnOldManager extends ListenerLogic {
    golemObjectId: number = 0

    constructor() {
        super( 'Q00114_ResurrectionOfAnOldManager', 'listeners/tracked/ResurrectionOfAnOldManager.ts' )
        this.questId = 114
        this.questItemIds = [ STARSTONE, STARSTONE2, DETCTOR, DETCTOR2, LETTER ]
    }

    getAttackableKillIds(): Array<number> {
        return [ GUARDIAN ]
    }

    getNpcSeePlayerIds(): Array<number> {
        return [ STONES ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00114_ResurrectionOfAnOldManager'
    }

    getQuestStartIds(): Array<number> {
        return [ YUMI ]
    }

    getTalkIds(): Array<number> {
        return [ YUMI, WENDY, BOX, STONES, NEWYEAR ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        if ( state && state.isCondition( 10 ) && state.getVariable( variableNames.spawned ) === 1 ) {
            let npc = L2World.getObjectById( data.targetId )
            BroadcastHelper.broadcastNpcSayStringId( npc as L2Npc,
                    NpcSayType.NpcAll,
                    NpcStringIds.THIS_ENEMY_IS_FAR_TOO_POWERFUL_FOR_ME_TO_FIGHT_I_MUST_WITHDRAW )

            state.setConditionWithSound( 11, true )
            state.unsetVariable( variableNames.spawned )
            this.stopQuestTimer( eventNames.golemDespawn )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32041-04.htm':
                state.startQuest()
                break

            case '32041-08.html':
                state.setVariable( variableNames.talk, 1 )
                break

            case '32041-09.html':
                state.setConditionWithSound( 2, true )
                state.unsetVariable( variableNames.talk )
                break

            case '32041-12.html':
                switch ( state.getCondition() ) {
                    case 4:
                        return this.getPath( '32041-13.html' )
                    case 5:
                        return this.getPath( '32041-14.html' )
                }

                break

            case '32041-15.html':
                state.setVariable( variableNames.talk, 1 )
                break

            case '32041-23.html':
                state.setVariable( variableNames.talk, 2 )
                break

            case '32041-26.html':
                state.setConditionWithSound( 6, true )
                state.unsetVariable( variableNames.talk )
                break

            case '32041-31.html':
                await QuestHelper.giveSingleItem( player, DETCTOR, 1 )
                state.setConditionWithSound( 17, true )
                break

            case '32041-34.html':
                state.setVariable( variableNames.talk, 1 )
                await QuestHelper.takeSingleItem( player, DETCTOR2, 1 )
                break

            case '32041-38.html':
                if ( state.getVariable( variableNames.choice ) === 2 ) {
                    return this.getPath( '32041-37.html' )
                }
                break

            case '32041-39.html':
                state.unsetVariable( variableNames.talk )
                state.setConditionWithSound( 20, true )
                break

            case '32041-40.html':
                state.setConditionWithSound( 21, true )
                state.unsetVariable( variableNames.talk )
                await QuestHelper.giveSingleItem( player, LETTER, 1 )
                break

            case '32046-03.html':
                state.setConditionWithSound( 19, true )
                break

            case '32046-07.html':
                await QuestHelper.addExpAndSp( player, 1846611, 144270 )
                await state.exitQuest( false, true )
                break

            case '32047-02.html':
                if ( !state.getVariable( variableNames.talk ) ) {
                    state.setVariable( variableNames.talk, 1 )
                }

                break

            case '32047-03.html':
                if ( !state.getVariable( variableNames.talkSecond ) ) {
                    state.setVariable( variableNames.talkSecond, '1' )
                }

                break

            case '32047-05.html':
                if ( !state.getVariable( variableNames.talk ) || !state.getVariable( variableNames.talkSecond ) ) {
                    return this.getPath( '32047-04.html' )
                }

                break

            case '32047-06.html':
                state.setVariable( variableNames.choice, 1 )
                state.setConditionWithSound( 3, true )
                state.unsetVariables( variableNames.talkSecond, variableNames.talk )
                break

            case '32047-07.html':
                state.setVariable( variableNames.choice, 2 )
                state.setConditionWithSound( 4, true )

                state.unsetVariables( variableNames.talkSecond, variableNames.talk )
                break

            case '32047-09.html':
                state.setVariable( variableNames.choice, 3 )
                state.setConditionWithSound( 5, true )
                state.unsetVariables( variableNames.talkSecond, variableNames.talk )
                break

            case '32047-14ab.html':
                state.setVariable( variableNames.choice, 3 )
                state.setConditionWithSound( 7, true )
                break

            case '32047-14b.html':
                state.setConditionWithSound( 10, true )
                break

            case '32047-15b.html':
                let deadGolem = this.getGolem()
                if ( deadGolem && deadGolem.isDead() ) {
                    let golem = QuestHelper.addGenericSpawn( null, GUARDIAN, 96977, -110625, -3280 ) as L2Attackable
                    golem.setRunning()

                    AIEffectHelper.notifyAttackedWithTargetId( golem, data.playerId, 0, 999 )

                    BroadcastHelper.broadcastNpcSayStringId( golem,
                            NpcSayType.NpcAll,
                            NpcStringIds.YOU_S1_YOU_ATTACKED_WENDY_PREPARE_TO_DIE,
                            player.getName() )

                    state.setVariable( variableNames.spawned, 1 )

                    this.golemObjectId = golem.getObjectId()
                    this.startQuestTimer( eventNames.golemDespawn, 300000, null, data.playerId )

                    break
                }

                if ( state.getVariable( variableNames.spawned ) === 1 ) {
                    return this.getPath( '32047-17b.html' )
                }

                return this.getPath( '32047-16b.html' )

            case '32047-20a.html':
                state.setConditionWithSound( 8, true )
                break

            case '32047-20b.html':
                state.setConditionWithSound( 12, true )
                break

            case '32047-20c.html':
                state.setConditionWithSound( 13, true )
                break

            case '32047-21a.html':
                state.setConditionWithSound( 9, true )
                break

            case '32047-23a.html':
                state.setConditionWithSound( 23, true )
                break

            case '32047-23c.html':
                await QuestHelper.takeSingleItem( player, STARSTONE, 1 )
                state.setConditionWithSound( 15, true )
                break

            case '32047-29c.html':
                if ( player.getAdena() >= 3000 ) {
                    await QuestHelper.giveSingleItem( player, STARSTONE2, 1 )
                    await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 3000 )

                    state.unsetVariable( variableNames.talk )
                    state.setConditionWithSound( 26, true )
                    break
                }

                return this.getPath( '32047-29ca.html' )

            case '32047-30c.html':
                state.setVariable( variableNames.talk, 1 )
                break

            case '32050-01r.html':
                state.setVariable( variableNames.talk, 1 )
                break

            case '32050-03.html':
                await QuestHelper.giveSingleItem( player, STARSTONE, 1 )
                state.setConditionWithSound( 14, true )
                state.unsetVariable( variableNames.talk )
                break

            case '32050-05.html':
                state.setConditionWithSound( 24, true )
                await QuestHelper.giveSingleItem( player, STARSTONE2, 1 )
                break

            case '31961-02.html':
                await QuestHelper.takeSingleItem( player, LETTER, 1 )
                await QuestHelper.giveSingleItem( player, STARSTONE2, 1 )
                state.setConditionWithSound( 22, true )
                break

            case eventNames.golemDespawn:
                state.unsetVariable( variableNames.spawned )

                let existingGolem = this.getGolem()
                if ( !existingGolem ) {
                    return
                }

                BroadcastHelper.broadcastNpcSayStringId( existingGolem,
                        NpcSayType.NpcAll,
                        NpcStringIds.S1_YOUR_ENEMY_WAS_DRIVEN_OUT_I_WILL_NOW_WITHDRAW_AND_AWAIT_YOUR_NEXT_COMMAND,
                        player.getName() )
                await existingGolem.deleteMe()
                this.golemObjectId = 0

                return

            case '32041-05.html':
            case '32041-06.html':
            case '32041-07.html':
            case '32041-17.html':
            case '32041-18.html':
            case '32041-19.html':
            case '32041-20.html':
            case '32041-21.html':
            case '32041-22.html':
            case '32041-25.html':
            case '32041-29.html':
            case '32041-30.html':
            case '32041-35.html':
            case '32041-36.html':
            case '32046-05.html':
            case '32046-06.html':
            case '32047-06a.html':
            case '32047-12a.html':
            case '32047-12b.html':
            case '32047-12c.html':
            case '32047-13a.html':
            case '32047-14a.html':
            case '32047-13b.html':
            case '32047-13c.html':
            case '32047-14c.html':
            case '32047-15c.html':
            case '32047-17c.html':
            case '32047-13ab.html':
            case '32047-15a.html':
            case '32047-16a.html':
            case '32047-16c.html':
            case '32047-18a.html':
            case '32047-19a.html':
            case '32047-18ab.html':
            case '32047-19ab.html':
            case '32047-18c.html':
            case '32047-17a.html':
            case '32047-19c.html':
            case '32047-21b.html':
            case '32047-27c.html':
            case '32047-28c.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onNpcSeePlayerEvent( data: NpcSeePlayerEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        if ( state && state.isCondition( 17 ) ) {
            let player = L2World.getPlayer( data.playerId )

            await QuestHelper.takeSingleItem( player, DETCTOR, 1 )
            await QuestHelper.giveSingleItem( player, DETCTOR2, 1 )
            state.setConditionWithSound( 18, true )

            player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.THE_RADIO_SIGNAL_DETECTOR_IS_RESPONDING_A_SUSPICIOUS_PILE_OF_STONES_CATCHES_YOUR_EYE, 2, 4500, null ) )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        let talkValue = state.getVariable( variableNames.talk )
        switch ( data.characterNpcId ) {
            case YUMI:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.hasQuestCompleted( 'Q00121_PavelTheGiant' ) ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '32041-02.htm' : '32041-03.htm' )
                        }

                        return this.getPath( '32041-01.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( talkValue === 1 ? '32041-08.html' : '32041-04.htm' )

                            case 2:
                                return this.getPath( '32041-10.html' )

                            case 3:
                            case 4:
                            case 5:
                                switch ( talkValue ) {
                                    case '0':
                                        return this.getPath( '32041-11.html' )

                                    case '1':
                                        return this.getPath( '32041-16.html' )

                                    case '2':
                                        return this.getPath( '32041-24.html' )
                                }

                                break

                            case 6:
                            case 7:
                            case 8:
                            case 10:
                            case 11:
                            case 13:
                            case 14:
                            case 15:
                                return this.getPath( '32041-27.html' )

                            case 9:
                            case 12:
                            case 16:
                                return this.getPath( '32041-28.html' )

                            case 17:
                            case 18:
                                return this.getPath( '32041-32.html' )

                            case 19:
                                return this.getPath( talkValue === 1 ? '32041-34z.html' : '32041-33.html' )

                            case 20:
                                return this.getPath( '32041-39z.html' )

                            case 21:
                                return this.getPath( '32041-40z.html' )

                            case 22:
                            case 25:
                            case 26:
                                state.setConditionWithSound( 27, true )
                                return this.getPath( '32041-41.html' )

                            case 27:
                                return this.getPath( '32041-42.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case WENDY:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 2:
                            return this.getPath( talkValue === 1 && state.getVariable( variableNames.talkSecond ) === 1 ? '32047-05.html' : '32047-01.html' )

                        case 3:
                            return this.getPath( '32047-06b.html' )

                        case 4:
                            return this.getPath( '32047-08.html' )

                        case 5:
                            return this.getPath( '32047-10.html' )

                        case 6:
                            switch ( state.getVariable( variableNames.choice ) ) {
                                case 1:
                                    return this.getPath( '32047-11a.html' )

                                case 2:
                                    return this.getPath( '32047-11b.html' )

                                case 3:
                                    return this.getPath( '32047-11c.html' )
                            }

                            break

                        case 7:
                            return this.getPath( '32047-11c.html' )

                        case 8:
                            return this.getPath( '32047-17a.html' )

                        case 9:
                        case 12:
                        case 16:
                            return this.getPath( '32047-25c.html' )

                        case 10:
                            return this.getPath( '32047-18b.html' )

                        case 11:
                            return this.getPath( '32047-19b.html' )

                        case 13:
                            return this.getPath( '32047-21c.html' )

                        case 14:
                            return this.getPath( '32047-22c.html' )

                        case 15:
                            state.setConditionWithSound( 16, true )
                            return this.getPath( '32047-24c.html' )

                        case 20:
                            if ( state.getVariable( variableNames.choice ) === 1 ) {
                                return this.getPath( '32047-22a.html' )
                            }

                            return this.getPath( talkValue === 1 ? '32047-31c.html' : '32047-26c.html' )

                        case 23:
                            return this.getPath( '32047-23z.html' )

                        case 24:
                            state.setConditionWithSound( 25, true )
                            return this.getPath( '32047-24a.html' )

                        case 25:
                            return this.getPath( '32047-24a.html' )

                        case 26:
                            return this.getPath( '32047-32c.html' )
                    }
                }

                break

            case NEWYEAR:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 21:
                            return this.getPath( '31961-01.html' )

                        case 22:
                            return this.getPath( '31961-03.html' )
                    }
                }

                break

            case BOX:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 13:
                            return this.getPath( talkValue === 1 ? '32050-02.html' : '32050-01.html' )

                        case 14:
                            return this.getPath( '32050-04.html' )

                        case 23:
                            return this.getPath( '32050-04b.html' )

                        case 24:
                            return this.getPath( '32050-05z.html' )
                    }
                }

                break

            case STONES:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 18:
                            return this.getPath( '32046-02.html' )

                        case 19:
                            return this.getPath( '32046-03.html' )

                        case 27:
                            return this.getPath( '32046-04.html' )
                    }
                }
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getGolem(): L2Attackable {
        if ( this.golemObjectId > 0 ) {
            return L2World.getObjectById( this.golemObjectId ) as L2Attackable
        }

        return null
    }
}