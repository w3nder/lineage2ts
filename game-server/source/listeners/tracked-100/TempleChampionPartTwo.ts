import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

const SYLVAIN = 30070
const PUPINA = 30118
const ANGUS = 30474
const SLA = 30666
const mobIds: Array<number> = [
    20176, // Wyrm
    20550, // Guardian Basilisk
    20551, // Road Scavenger
    20552, // Fettered Soul
]

const TEMPLE_MANIFESTO = 10341
const RELICS_OF_THE_DARK_ELF_TRAINEE = 10342
const ANGUS_RECOMMENDATION = 10343
const PUPINAS_RECOMMENDATION = 10344

const maximumLevel = 42
const minimumLevel = 36

const variableNames = {
    talk: 't',
}

export class TempleChampionPartTwo extends ListenerLogic {
    constructor() {
        super( 'Q00138_TempleChampionPart2', 'listeners/tracked/TempleChampionPartTwo.ts' )
        this.questId = 138
        this.questItemIds = [ TEMPLE_MANIFESTO, RELICS_OF_THE_DARK_ELF_TRAINEE, ANGUS_RECOMMENDATION, PUPINAS_RECOMMENDATION ]
    }

    getAttackableKillIds(): Array<number> {
        return mobIds
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00138_TempleChampionPart2'
    }

    getQuestStartIds(): Array<number> {
        return [ SYLVAIN ]
    }

    getTalkIds(): Array<number> {
        return [ SYLVAIN, PUPINA, ANGUS, SLA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() || !state.isCondition( 4 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( QuestHelper.getQuestItemsCount( player, RELICS_OF_THE_DARK_ELF_TRAINEE ) < 10 ) {
            await QuestHelper.rewardSingleQuestItem( player, RELICS_OF_THE_DARK_ELF_TRAINEE, 1, data.isChampion )

            if ( QuestHelper.getQuestItemsCount( player, RELICS_OF_THE_DARK_ELF_TRAINEE ) >= 10 ) {
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30070-02.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, TEMPLE_MANIFESTO, 1 )
                break

            case '30070-05.html':
                if ( player.getLevel() < maximumLevel ) {
                    await QuestHelper.addExpAndSp( player, 187062, 11307 )
                }

                await QuestHelper.giveAdena( player, 84593, true )
                await state.exitQuest( false, true )
                break

            case '30070-03.html':
                state.setConditionWithSound( 2, true )
                break

            case '30118-06.html':
                state.setConditionWithSound( 3, true )
                break

            case '30118-09.html':
                state.setConditionWithSound( 6, true )
                await QuestHelper.giveSingleItem( player, PUPINAS_RECOMMENDATION, 1 )
                break

            case '30474-02.html':
                state.setConditionWithSound( 4, true )
                break

            case '30666-02.html':
                if ( QuestHelper.hasQuestItem( player, PUPINAS_RECOMMENDATION ) ) {
                    state.setVariable( variableNames.talk, 1 )
                    await QuestHelper.takeSingleItem( player, PUPINAS_RECOMMENDATION, -1 )
                }

                break

            case '30666-03.html':
                if ( QuestHelper.hasQuestItem( player, TEMPLE_MANIFESTO ) ) {
                    state.setVariable( variableNames.talk, 2 )
                    await QuestHelper.takeSingleItem( player, TEMPLE_MANIFESTO, -1 )
                }
                break

            case '30666-08.html':
                state.setConditionWithSound( 7, true )
                state.unsetVariable( variableNames.talk )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case SYLVAIN:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30070-02.htm' )

                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        return this.getPath( '30070-03.html' )

                    case 7:
                        return this.getPath( '30070-04.html' )
                }

                if ( state.isCompleted() ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                if ( player.getLevel() >= minimumLevel ) {
                    if ( player.hasQuestCompleted( 'Q00137_TempleChampionPart1' ) ) {
                        return this.getPath( '30070-01.htm' )
                    }

                    return this.getPath( '30070-00a.htm' )
                }

                return this.getPath( '30070-00.htm' )

            case PUPINA:
                switch ( state.getCondition() ) {
                    case 2:
                        return this.getPath( '30118-01.html' )

                    case 3:
                    case 4:
                        return this.getPath( '30118-07.html' )

                    case 5:

                        if ( QuestHelper.hasQuestItem( player, ANGUS_RECOMMENDATION ) ) {
                            await QuestHelper.takeSingleItem( player, ANGUS_RECOMMENDATION, -1 )
                        }

                        return this.getPath( '30118-08.html' )

                    case 6:
                        return this.getPath( '30118-10.html' )
                }

                break

            case ANGUS:
                switch ( state.getCondition() ) {
                    case 3:
                        return this.getPath( '30474-01.html' )

                    case 4:
                        if ( QuestHelper.getQuestItemsCount( player, RELICS_OF_THE_DARK_ELF_TRAINEE ) >= 10 ) {
                            await QuestHelper.takeSingleItem( player, RELICS_OF_THE_DARK_ELF_TRAINEE, -1 )
                            await QuestHelper.giveSingleItem( player, ANGUS_RECOMMENDATION, 1 )
                            state.setConditionWithSound( 5, true )

                            return this.getPath( '30474-04.html' )
                        }

                        return this.getPath( '30474-03.html' )

                    case 5:
                        return this.getPath( '30474-05.html' )
                }

                break

            case SLA:
                switch ( state.getCondition() ) {
                    case 6:
                        switch ( state.getVariable( variableNames.talk ) ) {
                            case 1:
                                return this.getPath( '30666-02.html' )

                            case 2:
                                return this.getPath( '30666-03.html' )
                        }

                        return this.getPath( '30666-01.html' )

                    case 7:
                        return this.getPath( '30666-09.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}