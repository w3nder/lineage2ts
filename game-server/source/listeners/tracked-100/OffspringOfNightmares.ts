import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'
const VLASTY = 30145

const LESSER_DARK_HORROR = 20025
const DARK_HORROR = 20105

const BONE_GAITERS = 31
const CRACKED_SKULL = 1030
const PERFECT_SKULL = 1031

const minimumLevel = 15
const messageData = ExShowScreenMessage.fromNpcMessageId( NpcStringIds.LAST_DUTY_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000, null )

export class OffspringOfNightmares extends ListenerLogic {
    constructor() {
        super( 'Q00169_OffspringOfNightmares', 'listeners/tracked/OffspringOfNightmares.ts' )
        this.questId = 169
        this.questItemIds = [ CRACKED_SKULL, PERFECT_SKULL ]
    }

    getAttackableKillIds(): Array<number> {
        return [ LESSER_DARK_HORROR, DARK_HORROR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00169_OffspringOfNightmares'
    }

    getQuestStartIds(): Array<number> {
        return [ VLASTY ]
    }

    getTalkIds(): Array<number> {
        return [ VLASTY ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let chance: number = QuestHelper.getAdjustedChance( PERFECT_SKULL, _.random( 10 ), data.isChampion )
        if ( chance > 7 && !QuestHelper.hasQuestItem( player, PERFECT_SKULL ) ) {
            await QuestHelper.giveSingleItem( player, PERFECT_SKULL, 1 )
            state.setConditionWithSound( 2, true )

            return
        }

        if ( chance > 4 ) {
            await QuestHelper.rewardSingleQuestItem( player, CRACKED_SKULL, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30145-03.htm':
                state.startQuest()
                break

            case '30145-07.html':
                let player = L2World.getPlayer( data.playerId )
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, PERFECT_SKULL ) ) {
                    await QuestHelper.rewardSingleItem( player, BONE_GAITERS, 1 )
                    await QuestHelper.addExpAndSp( player, 17475, 818 )
                    await QuestHelper.giveAdena( player, 17030 + ( 10 * QuestHelper.getQuestItemsCount( player, CRACKED_SKULL ) ), true )
                    await state.exitQuest( false, true )

                    player.sendCopyData( messageData )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() === Race.DARK_ELF ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30145-02.htm' : '30145-01.htm' )
                }

                return this.getPath( '30145-00.htm' )

            case QuestStateValues.STARTED:
                let hasCracked: boolean = QuestHelper.hasQuestItem( player, CRACKED_SKULL )
                let hasPerfect: boolean = QuestHelper.hasQuestItem( player, PERFECT_SKULL )
                if ( hasCracked && !hasPerfect ) {
                    return this.getPath( '30145-05.html' )
                }

                if ( state.isCondition( 2 ) && hasPerfect ) {
                    return this.getPath( '30145-06.html' )
                }

                if ( !hasCracked && !hasPerfect ) {
                    return this.getPath( '30145-04.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}