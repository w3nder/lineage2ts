import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const GALLADUCCI = 30097
const GENTLER = 30094

const GALLADUCCIS_ORDER = 7563
const MAGIC_SWORD_HILT = 7568
const MARK_OF_TRAVELER = 7570
const SCROLL_OF_ESCAPE_KAMAEL_VILLAGE = 9716

export class ToTheIsleOfSouls extends ListenerLogic {
    constructor() {
        super( 'Q00173_ToTheIsleOfSouls', 'listeners/tracked/ToTheIsleOfSouls.ts' )
        this.questId = 173
        this.questItemIds = [ GALLADUCCIS_ORDER, MAGIC_SWORD_HILT ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00173_ToTheIsleOfSouls'
    }

    getQuestStartIds(): Array<number> {
        return [ GALLADUCCI ]
    }

    getTalkIds(): Array<number> {
        return [ GALLADUCCI, GENTLER ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30097-03.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, GALLADUCCIS_ORDER, 1 )
                break

            case '30097-06.html':
                await QuestHelper.rewardSingleItem( player, SCROLL_OF_ESCAPE_KAMAEL_VILLAGE, 1 )
                await QuestHelper.takeSingleItem( player, MARK_OF_TRAVELER, 1 )
                await state.exitQuest( false, true )
                break

            case '30094-02.html':
                state.setConditionWithSound( 2, true )
                await QuestHelper.takeSingleItem( player, GALLADUCCIS_ORDER, -1 )
                await QuestHelper.giveSingleItem( player, MAGIC_SWORD_HILT, 1 )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case GALLADUCCI:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath(
                                player.hasQuestCompleted( 'Q00172_NewHorizons' )
                                && player.getRace() === Race.KAMAEL
                                && QuestHelper.hasQuestItems( player, MARK_OF_TRAVELER ) ? '30097-01.htm' : '30097-02.htm',
                        )

                    case QuestStateValues.STARTED:
                        return this.getPath( state.isCondition( 1 ) ? '30097-04.html' : '30097-05.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case GENTLER:
                if ( state.isStarted() ) {
                    return this.getPath( state.isCondition( 1 ) ? '30094-01.html' : '30094-03.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}