import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { EventType, NpcGeneralEvent, NpcSeeSkillEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'

const SUSPICIOUS_LOOKING_PILE_OF_STONES = 32046
const WENDY = 32047
const YUMI = 32041
const WEATHERMASTER_1 = 32042
const WEATHERMASTER_2 = 32043
const WEATHERMASTER_3 = 32044
const DOCTOR_CHAOS_SECRET_BOOKSHELF = 32045

const FLOWER_OF_PAVEL = 8290
const HEART_OF_ATLANTA = 8291
const WENDYS_NECKLACE = 8292
const LOCKUP_RESEARCH_REPORT = 8058
const RESEARCH_REPORT = 8059
const KEY_OF_ENIGMA = 8060

const QUEST_TRAP_POWER_SHOT = new SkillHolder( 5073, 5 )
const NPC_DEFAULT = new SkillHolder( 7000 )

const SEALED_PHOENIX_EARRING = 6324

export class PavelsLastResearch extends ListenerLogic {
    constructor() {
        super( 'Q00120_PavelsLastResearch', 'listeners/tracked/PavelsLastResearch.ts' )
        this.questId = 120
        this.questItemIds = [
            FLOWER_OF_PAVEL,
            HEART_OF_ATLANTA,
            WENDYS_NECKLACE,
            LOCKUP_RESEARCH_REPORT,
            RESEARCH_REPORT,
            KEY_OF_ENIGMA,
        ]
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSeeSkill,
                method: this.onNpcSeeSkill.bind( this ),
                ids: [
                    WEATHERMASTER_1,
                    WEATHERMASTER_2,
                    WEATHERMASTER_3,
                ]
            }
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ SUSPICIOUS_LOOKING_PILE_OF_STONES ]
    }

    getTalkIds(): Array<number> {
        return [
            SUSPICIOUS_LOOKING_PILE_OF_STONES,
            WENDY,
            YUMI,
            WEATHERMASTER_1,
            WEATHERMASTER_2,
            WEATHERMASTER_3,
            DOCTOR_CHAOS_SECRET_BOOKSHELF,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32046-03.html':
            case '32046-04.htm':
            case '32046-05.html':
            case '32046-06.html':
                if ( state.isCreated() ) {
                    break
                }

                return

            case 'quest_accept':
                if ( state.isCreated() && player.hasQuestCompleted( 'Q00114_ResurrectionOfAnOldManager' ) ) {
                    if ( player.getLevel() >= 70 ) {
                        state.startQuest()
                        state.setMemoState( 1 )

                        return this.getPath( '32046-08.htm' )
                    }

                    return this.getPath( '32046-07.htm' )
                }

                return

            case '32046-10.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '32046-11.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '32046-14.html':
                if ( state.isMemoState( 3 ) ) {
                    break
                }

                return

            case '32046-15.html':
                if ( state.isMemoState( 3 ) ) {
                    await QuestHelper.giveSingleItem( player, FLOWER_OF_PAVEL, 1 )

                    state.setMemoState( 4 )
                    state.setConditionWithSound( 6, true )

                    break
                }

                return

            case '32046-18.html':
            case '32046-19.html':
            case '32046-20.html':
            case '32046-21.html':
            case '32046-22.html':
            case '32046-23.html':
            case '32046-24.html':
                if ( state.isMemoState( 7 ) ) {
                    break
                }

                return

            case '32046-25.html':
                if ( state.isMemoState( 7 ) ) {
                    state.setMemoState( 8 )
                    state.setConditionWithSound( 10, true )

                    break
                }

                return

            case '32046-26.html':
            case '32046-27.html':
            case '32046-28.html':
                if ( state.isMemoState( 8 ) ) {
                    break
                }

                return

            case '32046-30.html':
            case '32046-31.html':
            case '32046-32.html':
            case '32046-33.html':
            case '32046-34.html':
                if ( state.isMemoState( 11 ) ) {
                    break
                }

                return

            case '32046-35.html':
                if ( state.isMemoState( 11 ) ) {
                    state.setMemoState( 12 )
                    state.setConditionWithSound( 13, true )

                    break
                }

                return

            case '32046-38.html':
            case '32046-39.html':
            case '32046-40.html':
                if ( state.isMemoState( 19 ) ) {
                    break
                }

                return

            case '32046-41.html':
                if ( state.isMemoState( 19 ) ) {
                    state.setMemoState( 20 )
                    state.setConditionWithSound( 20, true )

                    break
                }

                return

            case '32046-44.html':
                if ( state.isMemoState( 22 ) ) {
                    await QuestHelper.giveSingleItem( player, HEART_OF_ATLANTA, 1 )

                    state.setMemoState( 23 )
                    state.setConditionWithSound( 23, true )

                    break
                }

                return

            case '32047-02.html':
            case '32047-03.html':
            case '32047-04.html':
            case '32047-05.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case '32047-06.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            case '32047-09.html':
                if ( state.isMemoState( 4 ) && QuestHelper.hasQuestItem( player, FLOWER_OF_PAVEL ) ) {
                    break
                }

                return

            case '32047-10.html':
                if ( state.isMemoState( 4 ) && QuestHelper.hasQuestItem( player, FLOWER_OF_PAVEL ) ) {
                    await QuestHelper.takeSingleItem( player, FLOWER_OF_PAVEL, -1 )

                    state.setMemoState( 5 )
                    state.setConditionWithSound( 7, true )

                    break
                }

                return

            case '32047-13.html':
            case '32047-14.html':
                if ( state.isMemoState( 6 ) ) {
                    break
                }

                return

            case '32047-15.html':
                if ( state.isMemoState( 6 ) ) {
                    state.setMemoState( 7 )
                    state.setConditionWithSound( 9, true )

                    break
                }

                return

            case '32047-18.html':
                if ( state.isMemoState( 12 ) ) {
                    break
                }

                return

            case '32047-19.html':
                if ( state.isMemoState( 12 ) ) {
                    state.setMemoState( 13 )
                    state.setConditionWithSound( 14, true )
                    break
                }

                return

            case '32047-23.html':
            case '32047-24.html':
            case '32047-25.html':
            case '32047-26.html':
                if ( state.isMemoState( 23 ) && QuestHelper.hasQuestItem( player, HEART_OF_ATLANTA ) ) {
                    break
                }

                return

            case '32047-27.html':
                if ( state.isMemoState( 23 ) && QuestHelper.hasQuestItem( player, HEART_OF_ATLANTA ) ) {
                    await QuestHelper.takeSingleItem( player, HEART_OF_ATLANTA, -1 )
                    state.setMemoState( 24 )
                    state.setConditionWithSound( 24, true )
                    break
                }

                return

            case '32047-28.html':
            case '32047-29.html':
                if ( state.isMemoState( 24 ) ) {
                    break
                }

                return

            case '32047-30.html':
                if ( state.isMemoState( 24 ) ) {
                    state.setMemoState( 25 )
                    break
                }

                return

            case '32047-31.html':
            case '32047-32.html':
                if ( state.isMemoState( 25 ) ) {
                    break
                }

                return

            case '32047-33.html':
                if ( state.isMemoState( 25 ) ) {
                    await QuestHelper.giveSingleItem( player, WENDYS_NECKLACE, 1 )

                    state.setMemoState( 26 )
                    state.setConditionWithSound( 25, true )

                    break
                }

                return

            case '32041-02.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case '32041-03.html':
                if ( state.isMemoState( 2 ) && state.getMemoStateEx( 0 ) === 0 ) {
                    state.setMemoStateEx( 0, 1 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '32041-05.html':
                if ( state.isMemoState( 2 ) && state.getMemoStateEx( 0 ) === 0 ) {
                    state.setMemoStateEx( 0, 2 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '32041-09.html':
            case '32041-10.html':
            case '32041-11.html':
            case '32041-12.html':
                if ( state.isMemoState( 5 ) ) {
                    break
                }

                return

            case '32041-13.html':
                if ( state.isMemoState( 5 ) ) {
                    state.setMemoState( 6 )
                    state.setConditionWithSound( 8, true )

                    break
                }

                return

            case '32041-16.html':
                if ( state.isMemoState( 14 ) && QuestHelper.hasQuestItem( player, LOCKUP_RESEARCH_REPORT ) ) {
                    break
                }

                return

            case '32041-17.html':
                if ( state.isMemoState( 14 ) && QuestHelper.hasQuestItem( player, LOCKUP_RESEARCH_REPORT ) ) {
                    await QuestHelper.giveSingleItem( player, KEY_OF_ENIGMA, 1 )

                    state.setMemoState( 15 )
                    state.setConditionWithSound( 16, true )

                    break
                }

                return
            case '32041-20.html':
                if ( state.isMemoState( 15 ) && QuestHelper.hasQuestItems( player, RESEARCH_REPORT, KEY_OF_ENIGMA ) ) {
                    break
                }

                return

            case 'pavel':
            case 'e=mc2':
                if ( state.isMemoState( 15 ) && QuestHelper.hasQuestItems( player, RESEARCH_REPORT, KEY_OF_ENIGMA ) ) {
                    return this.getPath( '32041-21.html' )
                }

                return

            case 'wdl':
                if ( state.isMemoState( 15 ) && QuestHelper.hasQuestItems( player, RESEARCH_REPORT, KEY_OF_ENIGMA ) ) {
                    return this.getPath( '32041-22.html' )
                }

                return

            case '32041-23.html':
                if ( state.isMemoState( 15 ) && QuestHelper.hasQuestItems( player, RESEARCH_REPORT, KEY_OF_ENIGMA ) ) {
                    await QuestHelper.takeSingleItem( player, KEY_OF_ENIGMA, -1 )
                    state.setMemoState( 16 )
                    state.setConditionWithSound( 17, true )
                    break
                }

                return

            case '32041-24.html':
            case '32041-26.html':
                if ( state.isMemoState( 16 ) && QuestHelper.hasQuestItem( player, RESEARCH_REPORT ) ) {
                    break
                }

                return

            case '32041-29.html':
            case '32041-30.html':
            case '32041-31.html':
            case '32041-32.html':
            case '32041-33.html':
                if ( state.isMemoState( 26 ) && QuestHelper.hasQuestItem( player, WENDYS_NECKLACE ) ) {
                    break
                }

                return

            case '32041-34.html':
                if ( state.isMemoState( 26 ) && QuestHelper.hasQuestItem( player, WENDYS_NECKLACE ) ) {
                    await QuestHelper.takeSingleItem( player, WENDYS_NECKLACE, -1 )
                    await QuestHelper.rewardSingleItem( player, SEALED_PHOENIX_EARRING, 1 )
                    await QuestHelper.giveAdena( player, 783720, true )
                    await QuestHelper.addExpAndSp( player, 3447315, 272615 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '32042-02.html':
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoStateEx( 0, 0 )

                    break
                }

                return

            case 'wm1_1_b':
            case 'wm1_1_c':
            case 'wm1_1_d':
            case 'wm1_1_l':
            case 'wm1_1_m':
            case 'wm1_1_n':
            case 'wm1_1_s':
            case 'wm1_1_t':
            case 'wm1_1_u':
                if ( state.isMemoState( 8 ) ) {
                    return this.getPath( '32042-03.html' )
                }

                return

            case 'wm1_1_a':
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoStateEx( 0, 1 )

                    return this.getPath( '32042-03.html' )
                }

                return

            case 'wm1_2_a':
            case 'wm1_2_b':
            case 'wm1_2_c':
            case 'wm1_2_d':
            case 'wm1_2_l':
            case 'wm1_2_m':
            case 'wm1_2_n':
            case 'wm1_2_s':
            case 'wm1_2_u':
                if ( state.isMemoState( 8 ) ) {
                    return this.getPath( '32042-04.html' )
                }

                return

            case 'wm1_2_t':
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoStateEx( 0, 10 + ( state.getMemoStateEx( 0 ) % 10 ) )

                    return this.getPath( '32042-04.html' )
                }

                return

            case 'wm1_3_a':
            case 'wm1_3_b':
            case 'wm1_3_c':
            case 'wm1_3_d':
            case 'wm1_3_m':
            case 'wm1_3_n':
            case 'wm1_3_s':
            case 'wm1_3_t':
            case 'wm1_3_u':
                if ( state.isMemoState( 8 ) ) {
                    return this.getPath( '32042-05.html' )
                }

                return

            case 'wm1_3_l':
                if ( state.isMemoState( 8 ) ) {
                    if ( state.getMemoStateEx( 0 ) === 11 ) {
                        state.setMemoState( 9 )
                        state.setConditionWithSound( 11, true )
                        state.setMemoStateEx( 0, 0 )

                        return this.getPath( '32042-06.html' )
                    }

                    return this.getPath( '32042-05.html' )
                }

                return

            case '32042-15.html':
            case '32042-06.html':
            case '32042-07.html':
                if ( state.isMemoState( 9 ) ) {
                    break
                }

                return

            case '32042-08.html':
                if ( state.isMemoState( 9 ) ) {
                    state.setMemoState( 10 )
                    player.sendCopyData( SoundPacket.AMBSOUND_PERCUSSION_01 )

                    break
                }

                return

            case 'wm1_return':
                if ( state.isMemoState( 10 ) ) {
                    if ( state.getMemoStateEx( 0 ) === 10101 ) {
                        return this.getPath( '32042-13.html' )
                    }

                    return this.getPath( '32042-09.html' )
                }

                return

            case '32042-10.html':
                if ( state.isMemoState( 10 ) ) {
                    state.setMemoStateEx( 0, state.getMemoStateEx( 0 ) + 1 )

                    break
                }

                return

            case '32042-11.html':
                if ( state.isMemoState( 10 ) ) {
                    let memoStateEx = state.getMemoStateEx( 0 )
                    state.setMemoStateEx( 0, memoStateEx + ( memoStateEx % 100 ) + 100 )

                    break
                }

                return

            case '32042-12.html':
                if ( state.isMemoState( 10 ) ) {
                    state.setMemoStateEx( 0, 10000 + ( state.getMemoStateEx( 0 ) % 10000 ) )

                    break
                }

                return

            case '32042-14.html':
                if ( state.isMemoState( 10 ) && state.getMemoStateEx( 0 ) === 10101 ) {
                    state.setMemoState( 11 )
                    state.setConditionWithSound( 12, true )
                    state.setMemoStateEx( 0, 0 )

                    break
                }

                return

            case '32043-02.html':
                if ( state.isMemoState( 16 ) ) {
                    state.setMemoStateEx( 0, 0 )

                    break
                }

                return

            case 'wm2_1_a':
            case 'wm2_1_b':
            case 'wm2_1_c':
            case 'wm2_1_d':
            case 'wm2_1_l':
            case 'wm2_1_m':
            case 'wm2_1_n':
            case 'wm2_1_v':
            case 'wm2_1_x':
                if ( state.isMemoState( 16 ) ) {
                    return this.getPath( '32043-03.html' )
                }

                return

            case 'wm2_1_w':
                if ( state.isMemoState( 16 ) ) {
                    state.setMemoStateEx( 0, 1 )

                    return this.getPath( '32043-03.html' )
                }

                return

            case 'wm2_2_a':
            case 'wm2_2_b':
            case 'wm2_2_c':
            case 'wm2_2_l':
            case 'wm2_2_m':
            case 'wm2_2_n':
            case 'wm2_2_v':
            case 'wm2_2_w':
            case 'wm2_2_x':
                if ( state.isMemoState( 16 ) ) {
                    return this.getPath( '32043-04.html' )
                }

                return

            case 'wm2_2_d':
                if ( state.isMemoState( 16 ) ) {
                    state.setMemoStateEx( 0, 10 + ( state.getMemoStateEx( 0 ) % 10 ) )

                    return this.getPath( '32043-04.html' )
                }

                return

            case 'wm2_3_a':
            case 'wm2_3_b':
            case 'wm2_3_c':
            case 'wm2_3_d':
            case 'wm2_3_m':
            case 'wm2_3_n':
            case 'wm2_3_v':
            case 'wm2_3_w':
            case 'wm2_3_x':
                if ( state.isMemoState( 8 ) ) {
                    return this.getPath( '32043-05.html' )
                }

                return

            case 'wm2_3_l':
                if ( state.isMemoState( 16 ) ) {
                    if ( state.getMemoStateEx( 0 ) === 11 ) {
                        state.setMemoState( 17 )
                        state.setConditionWithSound( 18, true )
                        state.setMemoStateEx( 0, 0 )

                        return this.getPath( '32043-06.html' )
                    }

                    return this.getPath( '32043-05.html' )
                }

                return

            case '32043-31.html':
            case '32043-30.html':
            case '32043-29.html':
            case '32043-28.html':
            case '32043-06.html':
            case '32043-07.html':
            case '32043-08.html':
                if ( state.isMemoState( 17 ) ) {
                    break
                }

                return

            case '32043-09.html':
                if ( state.isMemoState( 17 ) ) {
                    state.setMemoState( 18 )

                    break
                }

                return

            case '32043-10.html':
            case 'wm2_return':
                if ( state.isMemoState( 18 ) ) {
                    if ( state.getMemoStateEx( 0 ) === 1111 ) {
                        return this.getPath( '32043-12.html' )
                    }

                    return this.getPath( '32043-11.html' )
                }

                return

            case '32043-13.html':
                if ( state.isMemoState( 18 ) ) {
                    state.setMemoStateEx( 0, state.getMemoStateEx( 0 ) + 1 )

                    break
                }

                return

            case '32043-14.html':
                if ( state.isMemoState( 18 ) ) {
                    break
                }

                return

            case 'wm2_output':
                if ( state.isMemoState( 18 ) ) {
                    if ( state.getMemoStateEx( 0 ) < 1000 ) {
                        return this.getPath( '32043-15.html' )
                    }

                    return this.getPath( '32043-18.html' )
                }

                return

            case '32043-16.html':
                if ( state.isMemoState( 18 ) ) {
                    break
                }

                return

            case '32043-17.html':
                if ( state.isMemoState( 18 ) ) {
                    let memoStateEx = state.getMemoStateEx( 0 )
                    state.setMemoStateEx( 0, memoStateEx + ( memoStateEx % 1000 ) + 1000 )

                    player.sendCopyData( SoundPacket.AMBSOUND_DRONE )

                    break
                }

                return

            case '32043-19.html':
            case '32043-20.html':
                if ( state.isMemoState( 18 ) ) {
                    break
                }

                return

            case '32043-21.html':
                if ( state.isMemoState( 18 ) ) {
                    let memoStateEx = state.getMemoStateEx( 0 )
                    state.setMemoStateEx( 0, memoStateEx + ( memoStateEx % 10 ) + 10 )

                    break
                }

                return

            case '32043-22.html':
                if ( state.isMemoState( 18 ) && state.getMemoStateEx( 0 ) === 1111 ) {
                    state.setMemoState( 19 )
                    state.setConditionWithSound( 19, true )
                    state.setMemoStateEx( 0, 0 )

                    break
                }

                return

            case '32043-24.html':
            case '32043-25.html':
                if ( state.isMemoState( 18 ) ) {
                    break
                }

                return

            case '32043-26.html':
                if ( state.isMemoState( 18 ) ) {
                    let memoStateEx = state.getMemoStateEx( 0 )
                    state.setMemoStateEx( 0, memoStateEx + ( memoStateEx % 100 ) + 100 )

                    break
                }

                return

            case '32043-27.html':
                if ( state.isMemoState( 18 ) ) {
                    break
                }

                return

            case '32044-02.html':
                if ( state.isMemoState( 20 ) ) {
                    state.setMemoStateEx( 0, 0 )

                    break
                }

                return

            case 'wm3_1_a':
            case 'wm3_1_b':
            case 'wm3_1_c':
            case 'wm3_1_d':
            case 'wm3_1_l':
            case 'wm3_1_m':
            case 'wm3_1_v':
            case 'wm3_1_w':
            case 'wm3_1_x':
                if ( state.isMemoState( 20 ) ) {
                    return this.getPath( '32044-03.html' )
                }

                return

            case 'wm3_1_n':
                if ( state.isMemoState( 20 ) ) {
                    state.setMemoStateEx( 0, 1 )
                    return this.getPath( '32044-03.html' )
                }

                return

            case 'wm3_2_1':
            case 'wm3_2_2':
            case 'wm3_2_3':
            case 'wm3_2_5':
            case 'wm3_2_6':
            case 'wm3_2_7':
            case 'wm3_2_8':
            case 'wm3_2_9':
            case 'wm3_2_10':
                if ( state.isMemoState( 20 ) ) {
                    return this.getPath( '32044-04.html' )
                }

                return

            case 'wm3_2_4':
                if ( state.isMemoState( 20 ) ) {
                    state.setMemoStateEx( 0, 10 + ( state.getMemoStateEx( 0 ) % 10 ) )

                    return this.getPath( '32044-04.html' )
                }

                return

            case 'wm3_3_1':
            case 'wm3_3_2':
            case 'wm3_3_3':
            case 'wm3_3_4':
            case 'wm3_3_6':
            case 'wm3_3_7':
            case 'wm3_3_8':
            case 'wm3_3_9':
            case 'wm3_3_10':
                if ( state.isMemoState( 20 ) ) {
                    return this.getPath( '32044-05.html' )
                }

                return

            case 'wm3_3_5':
                if ( state.isMemoState( 20 ) ) {
                    if ( state.getMemoStateEx( 0 ) === 11 ) {
                        state.setMemoState( 21 )
                        state.setConditionWithSound( 21, true )
                        state.setMemoStateEx( 0, 0 )

                        player.sendCopyData( SoundPacket.AMBSOUND_PERCUSSION_02 )

                        return this.getPath( '32044-06.html' )
                    }

                    return this.getPath( '32044-05.html' )
                }

                return

            case '32044-07.html':
                if ( state.isMemoState( 21 ) ) {
                    break
                }

                return

            case 'wm3_observe':
                if ( state.isMemoState( 21 ) ) {
                    if ( ( state.getMemoStateEx( 0 ) % 100 ) === 11 ) {
                        return this.getPath( '32044-10.html' )
                    }

                    return this.getPath( '32044-09.html' )
                }

                return

            case '32044-11.html':
                if ( state.isMemoState( 21 ) ) {
                    let memoStateEx = state.getMemoStateEx( 0 )
                    state.setMemoStateEx( 0, memoStateEx + ( memoStateEx % 10 ) + 10 )

                    break
                }

                return

            case 'wm3_fire_of_paagrio':
                if ( state.isMemoState( 21 ) ) {
                    if ( Math.floor( state.getMemoStateEx( 0 ) / 100 ) === 1 ) {
                        return this.getPath( '32044-13.html' )
                    }

                    state.setMemoStateEx( 0, state.getMemoStateEx( 0 ) + 1 )
                    return this.getPath( '32044-12.html' )
                }

                return

            case 'wm3_control':
                if ( state.isMemoState( 21 ) ) {
                    if ( Math.floor( state.getMemoStateEx( 0 ) / 100 ) === 1 ) {
                        return this.getPath( '32044-15.html' )
                    }

                    return this.getPath( '32044-14.html' )
                }

                return

            case '32044-16.html': {
                if ( state.isMemoState( 21 ) && ( Math.floor( state.getMemoStateEx( 0 ) / 100 ) !== 1 ) ) {
                    state.setMemoStateEx( 0, ( state.getMemoStateEx( 0 ) % 100 ) + 100 )

                    break
                }
                return
            }
            case '32044-17.html':
            case '32044-18.html':
            case '32044-19.html':
                if ( state.isMemoState( 21 ) ) {
                    break
                }

                return

            case '32044-20.html':
                if ( state.isMemoState( 21 ) && Math.floor( state.getMemoStateEx( 0 ) / 100 ) === 1 ) {
                    state.setMemoState( 22 )
                    state.setConditionWithSound( 22, true )
                    state.setMemoStateEx( 0, 0 )

                    player.sendCopyData( SoundPacket.AMBSOUND_DRONE )

                    let npc = L2World.getObjectById( data.characterId ) as L2Npc
                    npc.setTarget( player )
                    await npc.doCast( QUEST_TRAP_POWER_SHOT.getSkill() )

                    break
                }

                return

            case '32044-21.html':
                if ( state.isMemoState( 22 ) ) {
                    break
                }

                return

            case '32045-02.html':
                if ( state.isMemoState( 13 ) ) {
                    await QuestHelper.giveSingleItem( player, LOCKUP_RESEARCH_REPORT, 1 )
                    // IMPORTANT!
                    // locked report is exchanged to unlocked by using key of enigma
                    // which is given by Wendy
                    state.setMemoState( 14 )
                    state.setConditionWithSound( 15, true )

                    let npc = L2World.getObjectById( data.characterId ) as L2Npc
                    npc.setTarget( player )
                    await npc.doCast( QUEST_TRAP_POWER_SHOT.getSkill() )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onNpcSeeSkill( data: NpcSeeSkillEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state || !state.isStarted() ) {
            return
        }

        let npc = L2World.getObjectById( data.receiverId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        npc.setTarget( player )
        return npc.doCast( NPC_DEFAULT.getSkill() )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case SUSPICIOUS_LOOKING_PILE_OF_STONES:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.hasQuestCompleted( 'Q00114_ResurrectionOfAnOldManager' ) ) {
                            return this.getPath( '32046-01.htm' )
                        }

                        return this.getPath( '32046-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '32046-09.html' )

                            case 2:
                                return this.getPath( '32046-12.html' )

                            case 3:
                                return this.getPath( '32046-13.html' )

                            case 4:
                                if ( QuestHelper.hasQuestItem( player, FLOWER_OF_PAVEL ) ) {
                                    return this.getPath( '32046-16.html' )
                                }

                                break

                            case 7:
                                return this.getPath( '32046-17.html' )

                            case 8:
                                return this.getPath( '32046-28.html' )

                            case 11:
                                return this.getPath( '32046-29.html' )

                            case 12:
                                return this.getPath( '32046-36.html' )

                            case 19:
                                return this.getPath( '32046-37.html' )

                            case 20:
                                return this.getPath( '32046-42.html' )

                            case 22:
                                return this.getPath( '32046-43.html' )

                            case 23:
                                if ( QuestHelper.hasQuestItem( player, HEART_OF_ATLANTA ) ) {
                                    return this.getPath( '32046-45.html' )
                                }

                                break
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        if ( player.hasQuestCompleted( 'Q00114_ResurrectionOfAnOldManager' ) ) {
                            return QuestHelper.getAlreadyCompletedMessagePath()
                        }

                        break
                }

                break

            case WENDY:
                switch ( state.getMemoState() ) {
                    case 2:
                        return this.getPath( '32047-01.html' )

                    case 3:
                        return this.getPath( '32047-07.html' )

                    case 4:
                        if ( QuestHelper.hasQuestItem( player, FLOWER_OF_PAVEL ) ) {
                            return this.getPath( '32047-08.html' )
                        }

                        break

                    case 5:
                        return this.getPath( '32047-11.html' )

                    case 6:
                        return this.getPath( '32047-12.html' )

                    case 7:
                        return this.getPath( '32047-16.html' )

                    case 12:
                        return this.getPath( '32047-17.html' )

                    case 13:
                        return this.getPath( '32047-20.html' )

                    case 14:
                        if ( QuestHelper.hasQuestItem( player, LOCKUP_RESEARCH_REPORT ) ) {
                            return this.getPath( '32047-21.html' )
                        }

                        break

                    case 23:
                        if ( QuestHelper.hasQuestItem( player, HEART_OF_ATLANTA ) ) {
                            return this.getPath( '32047-22.html' )
                        }

                        break

                    case 24:
                        return this.getPath( '32047-27.html' )

                    case 25:
                        return this.getPath( '32047-30.html' )

                    case 26:
                        if ( QuestHelper.hasQuestItem( player, WENDYS_NECKLACE ) ) {
                            return this.getPath( '32047-34.html' )
                        }

                        break
                }

                break

            case YUMI:
                switch ( state.getMemoState() ) {
                    case 2:
                        switch ( state.getMemoStateEx( 0 ) ) {
                            case 0:
                                return this.getPath( '32041-01.html' )

                            case 1:
                                return this.getPath( '32041-04.html' )

                            case 2:
                                return this.getPath( '32041-06.html' )
                        }

                        break

                    case 5:
                        if ( state.getMemoStateEx( 0 ) > 0 ) {
                            return this.getPath( '32041-07.html' )
                        }

                        return this.getPath( '32041-08.html' )

                    case 6:
                        return this.getPath( '32041-14.html' )

                    case 14:
                        if ( QuestHelper.hasQuestItem( player, LOCKUP_RESEARCH_REPORT ) ) {
                            return this.getPath( '32041-15.html' )
                        }

                        break

                    case 15:
                        if ( QuestHelper.hasQuestItems( player, KEY_OF_ENIGMA, RESEARCH_REPORT ) ) {
                            return this.getPath( '32041-19.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, KEY_OF_ENIGMA, LOCKUP_RESEARCH_REPORT ) ) {
                            return this.getPath( '32041-18.html' )
                        }

                        break

                    case 16:
                        if ( QuestHelper.hasQuestItem( player, RESEARCH_REPORT ) ) {
                            return this.getPath( '32041-27.html' )
                        }

                        break

                    case 26:
                        if ( QuestHelper.hasQuestItem( player, WENDYS_NECKLACE ) ) {
                            return this.getPath( '32041-28.html' )
                        }

                        break
                }

                break

            case WEATHERMASTER_1:
                switch ( state.getMemoState() ) {
                    case 8:
                        player.sendCopyData( SoundPacket.AMBSOUND_CRYSTAL_LOOP )
                        return this.getPath( '32042-01.html' )

                    case 9:
                        return this.getPath( '32042-06.html' )

                    case 10:
                        if ( state.getMemoStateEx( 0 ) === 10101 ) {
                            return this.getPath( '32042-13.html' )
                        }

                        return this.getPath( '32042-09.html' )

                    case 11:
                        return this.getPath( '32042-14.html' )
                }

                break

            case WEATHERMASTER_2:
                switch ( state.getMemoState() ) {
                    case 16:
                        return this.getPath( '32043-01.html' )

                    case 17:
                        return this.getPath( '32043-06.html' )

                    case 18:
                        return this.getPath( '32043-09.html' )

                    case 19:
                        return this.getPath( '32043-23.html' )
                }

                break

            case WEATHERMASTER_3:
                switch ( state.getMemoState() ) {
                    case 20:
                        return this.getPath( '32044-01.html' )

                    case 21:
                        return this.getPath( '32044-08.html' )

                    case 22:
                        return this.getPath( '32044-22.html' )
                }

                break

            case DOCTOR_CHAOS_SECRET_BOOKSHELF:
                switch ( state.getMemoState() ) {
                    case 13: {
                        return this.getPath( '32045-01.html' )
                        break
                    }
                    case 14: {
                        return this.getPath( '32045-03.html' )
                        break
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00120_PavelsLastResearch'
    }
}