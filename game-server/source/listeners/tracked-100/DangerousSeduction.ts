import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const VELLIOR = 30305
const MERKENIS = 27022
const NIGHTMARE_CRYSTAL = 1046
const minimumLevel = 21

export class DangerousSeduction extends ListenerLogic {
    constructor() {
        super( 'Q00170_DangerousSeduction', 'listeners/tracked/DangerousSeduction.ts' )
        this.questId = 170
        this.questItemIds = [ NIGHTMARE_CRYSTAL ]
    }

    getAttackableKillIds(): Array<number> {
        return [ MERKENIS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00170_DangerousSeduction'
    }

    getQuestStartIds(): Array<number> {
        return [ VELLIOR ]
    }

    getTalkIds(): Array<number> {
        return [ VELLIOR ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        state.setConditionWithSound( 2, true )
        await QuestHelper.giveSingleItem( player, NIGHTMARE_CRYSTAL, 1 )
        BroadcastHelper.broadcastNpcSayStringId( L2World.getObjectById( data.targetId ) as L2Npc, NpcSayType.NpcAll, NpcStringIds.SEND_MY_SOUL_TO_LICH_KING_ICARUS )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '30305-04.htm' ) {
            return
        }

        state.startQuest()

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() === Race.DARK_ELF ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30305-01.htm' : '30305-02.htm' )
                }

                return this.getPath( '30305-03.htm' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 1 ) ) {
                    return this.getPath( '30305-05.html' )
                }

                await QuestHelper.giveAdena( player, 102680, true )
                await QuestHelper.addExpAndSp( player, 38607, 4018 )
                await state.exitQuest( false, true )

                return this.getPath( '30305-06.html' )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}