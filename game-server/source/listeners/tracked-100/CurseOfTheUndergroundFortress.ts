import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const UNOREN = 30147

const skullMap = {
    20033: 25, // Shade Horror
    20345: 26, // Dark Terror
    20371: 23, // Mist Terror
}

const boneMap = {
    20463: 25, // Dungeon Skeleton Archer
    20464: 23, // Dungeon Skeleton
    20504: 26, // Dread Soldier
}

const BONE_SHIELD = 625
const BONE_FRAGMENT = 1158
const ELF_SKULL = 1159

const minimumLevel = 12
const REQUIRED_COUNT = 13

export class CurseOfTheUndergroundFortress extends ListenerLogic {
    constructor() {
        super( 'Q00162_CurseOfTheUndergroundFortress', 'listeners/tracked/CurseOfTheUndergroundFortress.ts' )
        this.questId = 162
        this.questItemIds = [ BONE_FRAGMENT, ELF_SKULL ]
    }

    checkConditions( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.getQuestItemsCount( player, ELF_SKULL ) >= 3
                && QuestHelper.getQuestItemsCount( player, BONE_FRAGMENT ) >= 10 ) {
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    getAttackableKillIds(): Array<number> {
        return [ ..._.keys( skullMap ), ..._.keys( boneMap ) ].map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00162_CurseOfTheUndergroundFortress'
    }

    getQuestStartIds(): Array<number> {
        return [ UNOREN ]
    }

    getTalkIds(): Array<number> {
        return [ UNOREN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        if ( QuestHelper.getAdjustedChance( ELF_SKULL, skullMap[ data.npcId ], data.isChampion ) > _.random( 100 ) ) {
            let player = L2World.getPlayer( data.playerId )

            if ( QuestHelper.getQuestItemsCount( player, ELF_SKULL ) < 3 ) {
                await QuestHelper.rewardSingleQuestItem( player, ELF_SKULL, 1, data.isChampion )

                this.checkConditions( player, state )
                return
            }
        }

        if ( QuestHelper.getAdjustedChance( BONE_FRAGMENT, boneMap[ data.npcId ], data.isChampion ) > _.random( 100 ) ) {

            let player = L2World.getPlayer( data.playerId )
            if ( QuestHelper.getQuestItemsCount( player, BONE_FRAGMENT ) < 10 ) {
                await QuestHelper.rewardSingleQuestItem( player, BONE_FRAGMENT, 1, data.isChampion )

                this.checkConditions( player, state )
                return
            }
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30147-03.htm':
                break

            case '30147-04.htm':
                state.startQuest()
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.DARK_ELF ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30147-02.htm' : '30147-01.htm' )
                }

                return this.getPath( '30147-00.htm' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.getQuestItemsCount( player, BONE_FRAGMENT ) + QuestHelper.getQuestItemsCount( player, ELF_SKULL ) >= REQUIRED_COUNT ) {
                    await QuestHelper.rewardSingleItem( player, BONE_SHIELD, 1 )
                    await QuestHelper.addExpAndSp( player, 22652, 1004 )
                    await QuestHelper.giveAdena( player, 24000, true )
                    await state.exitQuest( false, true )

                    return this.getPath( '30147-06.html' )
                }

                return this.getPath( '30147-05.html' )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}