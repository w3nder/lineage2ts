import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { QuestStateValues } from '../../gameService/models/quest/State'

const SIR_GUSTAV_ATHEBALDT = 30760
const CLAUDIA_ATHEBALDT = 31001
const COLIN = 32571
const FROG = 32572
const TESS = 32573
const KUTA = 32574

const ATHEBALDTS_INTRODUCTION = 13818
const NATIVES_GLOVE = 13819
const FROG_KINGS_BEAD = 13820
const GRANDA_TESS_CANDY_POUCH = 13821

const minimumLevel = 79

const TRANSFORMATION_FROG = 6201
const TRANSFORMATION_KID = 6202
const TRANSFORMATION_NATIVE = 6203

export class SevenSignsMammonsContract extends ListenerLogic {
    constructor() {
        super( 'Q00194_SevenSignsMammonsContract', 'listeners/tracked/SevenSignsMammonsContract.ts' )
        this.questId = 194
        this.questItemIds = [ ATHEBALDTS_INTRODUCTION, NATIVES_GLOVE, FROG_KINGS_BEAD, GRANDA_TESS_CANDY_POUCH ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00194_SevenSignsMammonsContract'
    }

    getQuestStartIds(): Array<number> {
        return [ SIR_GUSTAV_ATHEBALDT ]
    }

    getTalkIds(): Array<number> {
        return [ SIR_GUSTAV_ATHEBALDT, COLIN, FROG, TESS, KUTA, CLAUDIA_ATHEBALDT ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case '30760-02.html':
                state.startQuest()
                break

            case '30760-03.html':
                if ( state.isCondition( 1 ) ) {
                    break
                }

                return

            case '30760-04.html':
                if ( state.isCondition( 1 ) ) {
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    break
                }

                return

            case 'showmovie':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    await player.showQuestMovie( 10 )
                }

                return

            case '30760-07.html':
                if ( state.isCondition( 2 ) ) {
                    await QuestHelper.giveSingleItem( player, ATHEBALDTS_INTRODUCTION, 1 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '32571-03.html':
            case '32571-04.html':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, ATHEBALDTS_INTRODUCTION ) ) {
                    break
                }

                return

            case '32571-05.html':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, ATHEBALDTS_INTRODUCTION ) ) {
                    await QuestHelper.takeSingleItem( player, ATHEBALDTS_INTRODUCTION, -1 )
                    npc.setTarget( player )
                    await npc.doCast( SkillCache.getSkill( TRANSFORMATION_FROG, 1 ) )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '32571-07.html':
                if ( state.isCondition( 4 )
                        && player.getTransformationId() !== 111
                        && !QuestHelper.hasQuestItem( player, FROG_KINGS_BEAD ) ) {
                    npc.setTarget( player )
                    await npc.doCast( SkillCache.getSkill( TRANSFORMATION_FROG, 1 ) )

                    break
                }

                break

            case '32571-09.html':
                if ( state.isCondition( 4 )
                        && player.getTransformationId() === 111
                        && !QuestHelper.hasQuestItem( player, FROG_KINGS_BEAD ) ) {
                    await player.stopAllEffects()

                    break
                }

                break

            case '32571-11.html':
                if ( state.isCondition( 5 ) && QuestHelper.hasQuestItem( player, FROG_KINGS_BEAD ) ) {
                    await QuestHelper.takeSingleItem( player, FROG_KINGS_BEAD, -1 )
                    state.setConditionWithSound( 6, true )

                    if ( player.getTransformationId() === 111 ) {
                        await player.stopAllEffects()
                    }

                    break
                }

                break

            case '32571-13.html':
                if ( state.isCondition( 6 ) ) {
                    npc.setTarget( player )

                    await npc.doCast( SkillCache.getSkill( TRANSFORMATION_KID, 1 ) )
                    state.setConditionWithSound( 7, true )

                    break
                }

                return

            case '32571-15.html':
                if ( state.isCondition( 7 )
                        && player.getTransformationId() !== 112
                        && !QuestHelper.hasQuestItem( player, GRANDA_TESS_CANDY_POUCH ) ) {
                    npc.setTarget( player )
                    await npc.doCast( SkillCache.getSkill( TRANSFORMATION_KID, 1 ) )

                    break
                }

                return

            case '32571-17.html':
                if ( state.isCondition( 7 )
                        && player.getTransformationId() === 112
                        && !QuestHelper.hasQuestItem( player, GRANDA_TESS_CANDY_POUCH ) ) {
                    await player.stopAllEffects()

                    break
                }

                return

            case '32571-19.html':
                if ( state.isCondition( 8 ) && QuestHelper.hasQuestItem( player, GRANDA_TESS_CANDY_POUCH ) ) {
                    await QuestHelper.takeSingleItem( player, GRANDA_TESS_CANDY_POUCH, -1 )
                    state.setConditionWithSound( 9, true )

                    if ( player.getTransformationId() === 112 ) {
                        await player.stopAllEffects()
                    }

                    break
                }

                return

            case '32571-21.html':
                if ( state.isCondition( 9 ) ) {
                    npc.setTarget( player )
                    await npc.doCast( SkillCache.getSkill( TRANSFORMATION_NATIVE, 1 ) )

                    state.setConditionWithSound( 10, true )
                    break
                }

                return

            case '32571-23.html':
                if ( state.isCondition( 10 )
                        && player.getTransformationId() !== 124
                        && !QuestHelper.hasQuestItem( player, NATIVES_GLOVE ) ) {
                    npc.setTarget( player )
                    await npc.doCast( SkillCache.getSkill( TRANSFORMATION_NATIVE, 1 ) )

                    break
                }

                return

            case '32571-25.html':
                if ( state.isCondition( 10 )
                        && player.getTransformationId() === 124
                        && !QuestHelper.hasQuestItem( player, NATIVES_GLOVE ) ) {
                    await player.stopAllEffects()

                    break
                }

                return

            case '32571-27.html':
                if ( state.isCondition( 11 ) && QuestHelper.hasQuestItem( player, NATIVES_GLOVE ) ) {
                    await QuestHelper.takeSingleItem( player, NATIVES_GLOVE, -1 )
                    state.setConditionWithSound( 12, true )

                    if ( player.getTransformationId() === 124 ) {
                        await player.stopAllEffects()
                    }

                    break
                }

                return

            case '32572-03.html':
            case '32572-04.html':
                if ( state.isCondition( 4 ) ) {
                    break
                }

                return

            case '32572-05.html':
                if ( state.isCondition( 4 ) ) {
                    await QuestHelper.giveSingleItem( player, FROG_KINGS_BEAD, 1 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            case '32573-03.html':
                if ( state.isCondition( 7 ) ) {
                    break
                }

                return

            case '32573-04.html':
                if ( state.isCondition( 7 ) ) {
                    await QuestHelper.giveSingleItem( player, GRANDA_TESS_CANDY_POUCH, 1 )
                    state.setConditionWithSound( 8, true )

                    break
                }

                return

            case '32574-03.html':
            case '32574-04.html':
                if ( state.isCondition( 10 ) ) {
                    break
                }

                return

            case '32574-05.html':
                if ( state.isCondition( 10 ) ) {
                    await QuestHelper.giveSingleItem( player, NATIVES_GLOVE, 1 )
                    state.setConditionWithSound( 11, true )

                    break
                }

                return

            case '31001-02.html':
                if ( state.isCondition( 12 ) ) {
                    break
                }

                return

            case '31001-03.html':
                if ( state.isCondition( 12 ) ) {
                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( 'level_check.html' )

                    }

                    await QuestHelper.addExpAndSp( player, 52518015, 5817677 )
                    await state.exitQuest( false, true )

                    break
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === SIR_GUSTAV_ATHEBALDT ) {
                    let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00193_SevenSignsDyingMessage' )
                    return this.getPath( canProceed ? '30760-01.htm' : '30760-05.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case SIR_GUSTAV_ATHEBALDT:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30760-02.html' )

                            case 2:
                                return this.getPath( '30760-06.html' )

                            case 3:
                                if ( QuestHelper.hasQuestItem( player, ATHEBALDTS_INTRODUCTION ) ) {
                                    return this.getPath( '30760-08.html' )
                                }

                                break
                        }

                        break

                    case COLIN:
                        switch ( state.getCondition() ) {
                            case 1:
                            case 2:
                                return this.getPath( '32571-01.html' )

                            case 3:
                                if ( QuestHelper.hasQuestItem( player, ATHEBALDTS_INTRODUCTION ) ) {
                                    return this.getPath( '32571-02.html' )
                                }

                                break

                            case 4:
                                if ( !QuestHelper.hasQuestItem( player, FROG_KINGS_BEAD ) ) {
                                    return this.getPath( player.getTransformationId() !== 111 ? '32571-06.html' : '32571-08.html' )
                                }

                                break

                            case 5:
                                if ( QuestHelper.hasQuestItem( player, FROG_KINGS_BEAD ) ) {
                                    return this.getPath( '32571-10.html' )
                                }

                                break

                            case 6:
                                return this.getPath( '32571-12.html' )

                            case 7:
                                if ( !QuestHelper.hasQuestItem( player, GRANDA_TESS_CANDY_POUCH ) ) {
                                    return this.getPath( player.getTransformationId() !== 112 ? '32571-14.html' : '32571-16.html' )
                                }

                                break

                            case 8:
                                if ( QuestHelper.hasQuestItem( player, GRANDA_TESS_CANDY_POUCH ) ) {
                                    return this.getPath( '32571-18.html' )
                                }

                                break

                            case 9:
                                return this.getPath( '32571-20.html' )

                            case 10:
                                if ( !QuestHelper.hasQuestItem( player, NATIVES_GLOVE ) ) {
                                    return this.getPath( player.getTransformationId() !== 124 ? '32571-22.html' : '32571-24.html' )
                                }

                                break

                            case 11:
                                if ( QuestHelper.hasQuestItem( player, NATIVES_GLOVE ) ) {
                                    return this.getPath( '32571-26.html' )
                                }

                                break

                            case 12:
                                return this.getPath( '32571-28.html' )
                        }

                        break

                    case FROG:
                        switch ( state.getCondition() ) {
                            case 1:
                            case 2:
                            case 3:
                                return this.getPath( '32572-01.html' )

                            case 4:
                                return this.getPath( player.getTransformationId() === 111 ? '32572-02.html' : '32572-06.html' )

                            case 5:
                                if ( QuestHelper.hasQuestItem( player, FROG_KINGS_BEAD ) && player.getTransformationId() === 111 ) {
                                    return this.getPath( '32572-07.html' )
                                }

                                break

                        }

                        break

                    case TESS:
                        switch ( state.getCondition() ) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                return this.getPath( '32573-01.html' )

                            case 7:
                                return this.getPath( player.getTransformationId() === 112 ? '32573-02.html' : '32573-05.html' )

                            case 8:
                                if ( QuestHelper.hasQuestItem( player, GRANDA_TESS_CANDY_POUCH ) && player.getTransformationId() === 112 ) {
                                    return this.getPath( '32573-06.html' )
                                }

                                break
                        }

                        break

                    case KUTA:
                        switch ( state.getCondition() ) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                return this.getPath( '32574-01.html' )

                            case 10:
                                return this.getPath( player.getTransformationId() === 124 ? '32574-02.html' : '32574-06.html' )

                            case 11:
                                if ( QuestHelper.hasQuestItem( player, NATIVES_GLOVE ) && player.getTransformationId() === 124 ) {
                                    return this.getPath( '32574-07.html' )
                                }

                                break
                        }

                        break

                    case CLAUDIA_ATHEBALDT:
                        if ( state.isCondition( 12 ) ) {
                            return this.getPath( '31001-01.html' )
                        }

                        break

                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}