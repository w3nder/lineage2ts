import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const HARDIN = 30832
const ERRICKIN = 30701
const CLAYTON = 30464

const GLASS_JAGUAR = 20250
const GHOST1 = 20636
const GHOST2 = 20637
const GHOST3 = 20638
const MIRROR = 20639

const ECTOPLASM = 9787
const STABILIZED_ECTOPLASM = 9786
const ORDER = 9788
const GLASS_JAGUAR_CRYSTAL = 9789
const BOOK_OF_SEAL = 9790
const TRANSFORM_BOOK = 9648

const minimumLevel = 50
const ECTOPLASM_COUNT = 35
const CRYSTAL_COUNT = 5
const CHANCES = [
    0,
    40,
    90,
    290,
]

const variableNames = {
    talked: 't'
}

export class MoreThanMeetsTheEye extends ListenerLogic {
    constructor() {
        super( 'Q00136_MoreThanMeetsTheEye', 'listeners/tracked/MoreThanMeetsTheEye.ts' )
        this.questId = 136
        this.questItemIds = [
            ECTOPLASM, STABILIZED_ECTOPLASM, ORDER, GLASS_JAGUAR_CRYSTAL, BOOK_OF_SEAL,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            GHOST1,
            GHOST2,
            GHOST3,
            GLASS_JAGUAR,
            MIRROR,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00136_MoreThanMeetsTheEye'
    }

    getQuestStartIds(): Array<number> {
        return [ HARDIN ]
    }

    getTalkIds(): Array<number> {
        return [ HARDIN, ERRICKIN, CLAYTON ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( data.npcId !== GLASS_JAGUAR && state.isCondition( 3 ) ) {
            let count = data.npcId === MIRROR && ( QuestHelper.getQuestItemsCount( player, ECTOPLASM ) + 2 ) < ECTOPLASM_COUNT ? 2 : 1
            let index = data.npcId - GHOST1

            if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( ECTOPLASM, CHANCES[ index ], data.isChampion )
                    && ( QuestHelper.getQuestItemsCount( player, ECTOPLASM ) + count ) < ECTOPLASM_COUNT ) {
                await QuestHelper.rewardSingleQuestItem( player, ECTOPLASM, 1, data.isChampion )
            }

            await this.giveItemToPlayer( player, state, ECTOPLASM, count, ECTOPLASM_COUNT, 4, data.isChampion )
            return
        }

        if ( data.npcId === GLASS_JAGUAR && state.isCondition( 7 ) ) {
            await this.giveItemToPlayer( player, state, GLASS_JAGUAR_CRYSTAL, 1, CRYSTAL_COUNT, 8, data.isChampion )
            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30832-05.html':
            case '30832-06.html':
            case '30832-12.html':
            case '30832-13.html':
            case '30832-18.html':
                break

            case '30832-03.htm':
                state.startQuest()
                break

            case '30832-07.html':
                state.setConditionWithSound( 2, true )
                break

            case '30832-11.html':
                state.setVariable( variableNames.talked, 2 )
                break

            case '30832-14.html':
                state.unsetVariable( variableNames.talked )
                await QuestHelper.giveSingleItem( player, ORDER, 1 )
                state.setConditionWithSound( 6, true )
                break

            case '30832-17.html':
                state.setVariable( variableNames.talked, 2 )
                break

            case '30832-19.html':
                await QuestHelper.giveSingleItem( player, TRANSFORM_BOOK, 1 )
                await QuestHelper.giveAdena( player, 67550, true )
                await state.exitQuest( false, true )
                break

            case '30701-03.html':
                state.setConditionWithSound( 3, true )
                break

            case '30464-03.html':
                await QuestHelper.takeSingleItem( player, ORDER, -1 )
                state.setConditionWithSound( 7, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case HARDIN:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30832-01.htm' : '30832-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30832-04.html' )

                            case 2:
                            case 3:
                            case 4:
                                return this.getPath( '30832-08.html' )

                            case 5:
                                if ( state.getVariable( variableNames.talked ) === 1 ) {
                                    return this.getPath( '30832-10.html' )
                                }

                                if ( state.getVariable( variableNames.talked ) === 2 ) {
                                    return this.getPath( '30832-12.html' )
                                }

                                if ( QuestHelper.hasQuestItem( player, STABILIZED_ECTOPLASM ) ) {
                                    await QuestHelper.takeSingleItem( player, STABILIZED_ECTOPLASM, -1 )
                                    state.setVariable( variableNames.talked, 1 )

                                    return this.getPath( '30832-09.html' )
                                }

                                return this.getPath( '30832-08.html' )

                            case 6:
                            case 7:
                            case 8:
                                return this.getPath( '30832-15.html' )

                            case 9:
                                if ( state.getVariable( variableNames.talked ) === 1 ) {
                                    state.setVariable( variableNames.talked, 2 )
                                    return this.getPath( '30832-17.html' )
                                }

                                if ( state.getVariable( variableNames.talked ) === 2 ) {
                                    return this.getPath( '30832-18.html' )
                                }

                                await QuestHelper.takeSingleItem( player, BOOK_OF_SEAL, -1 )
                                state.setVariable( variableNames.talked, 1 )

                                return this.getPath( '30832-16.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case ERRICKIN:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30701-01.html' )

                    case 2:
                        return this.getPath( '30701-02.html' )

                    case 3:
                        return this.getPath( '30701-04.html' )

                    case 4:
                        if ( QuestHelper.getQuestItemsCount( player, ECTOPLASM ) < ECTOPLASM_COUNT ) {
                            await QuestHelper.rewardSingleItem( player, STABILIZED_ECTOPLASM, 1 )
                            state.setConditionWithSound( 5, true )

                            return this.getPath( '30701-06.html' )
                        }

                        await QuestHelper.takeSingleItem( player, ECTOPLASM, -1 )

                        return this.getPath( '30701-05.html' )

                    default:
                        return this.getPath( '30701-07.html' )
                }

                break

            case CLAYTON:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        return this.getPath( '30464-01.html' )

                    case 6:
                        return this.getPath( '30464-02.html' )

                    case 7:
                        return this.getPath( '30464-04.html' )

                    case 8:
                        await QuestHelper.giveSingleItem( player, BOOK_OF_SEAL, 1 )
                        await QuestHelper.takeSingleItem( player, GLASS_JAGUAR_CRYSTAL, -1 )
                        state.setConditionWithSound( 9, true )

                        return this.getPath( '30464-05.html' )

                    default:
                        return this.getPath( '30464-06.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async giveItemToPlayer( player: L2PcInstance, state: QuestState, itemId: number, amount: number, maxAmount: number, conditionValue: number, isFromChampion: boolean ): Promise<void> {
        await QuestHelper.rewardSingleQuestItem( player, itemId, amount, isFromChampion )

        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= maxAmount ) {
            state.setConditionWithSound( conditionValue, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}