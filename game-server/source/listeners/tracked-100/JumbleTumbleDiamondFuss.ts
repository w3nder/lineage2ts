import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import aigle from 'aigle'
import _ from 'lodash'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const COLLECTOR_GOUPH = 30523
const TRADER_REEP = 30516
const CARRIER_TOROCCO = 30555
const MINER_MARON = 30529
const BLACKSMITH_BRUNON = 30526
const WAREHOUSE_KEEPER_MURDOC = 30521
const WAREHOUSE_KEEPER_AIRY = 30522

const GOBLIN_BRIGAND_LEADER = 20323
const GOBLIN_BRIGAND_LIEUTENANT = 20324
const BLADE_BAT = 20480

const GOUPHS_CONTRACT = 1559
const REEPS_CONTRACT = 1560
const ELVEN_WINE = 1561
const BRUNONS_DICE = 1562
const BRUNONS_CONTRACT = 1563
const AQUAMARINE = 1564
const CHRYSOBERYL = 1565
const GEM_BOX = 1566
const COAL_PIECE = 1567
const BRUNONS_LETTER = 1568
const BERRY_TART = 1569
const BAT_DIAGRAM = 1570
const STAR_DIAMOND = 1571

const REWARDS = [
    createItemDefinition( 1060, 100 ), // Lesser Healing Potion
    createItemDefinition( 4412, 10 ), // Echo Crystal - Theme of Battle
    createItemDefinition( 4413, 10 ), // Echo Crystal - Theme of Love
    createItemDefinition( 4414, 10 ), // Echo Crystal - Theme of Solitude
    createItemDefinition( 4415, 10 ), // Echo Crystal - Theme of Feast
    createItemDefinition( 4416, 10 ), // Echo Crystal - Theme of Celebration
]

const SILVERSMITH_HAMMER = 1511
const minimumLevel = 10
const gemCount = 10

const monsterDropChances = {
    GOBLIN_BRIGAND_LEADER: 0.8,
    GOBLIN_BRIGAND_LIEUTENANT: 0.6,
}

export class JumbleTumbleDiamondFuss extends ListenerLogic {
    constructor() {
        super( 'Q00108_JumbleTumbleDiamondFuss', 'listeners/tracked/JumbleTumbleDiamondFuss.ts' )
        this.questId = 108
        this.questItemIds = [
            GOUPHS_CONTRACT,
            REEPS_CONTRACT,
            ELVEN_WINE,
            BRUNONS_DICE,
            BRUNONS_CONTRACT,
            AQUAMARINE,
            CHRYSOBERYL,
            GEM_BOX,
            COAL_PIECE,
            BRUNONS_LETTER,
            BERRY_TART,
            BAT_DIAGRAM,
            STAR_DIAMOND,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            GOBLIN_BRIGAND_LEADER,
            GOBLIN_BRIGAND_LIEUTENANT,
            BLADE_BAT,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00108_JumbleTumbleDiamondFuss'
    }

    getQuestStartIds(): Array<number> {
        return [ COLLECTOR_GOUPH ]
    }

    getTalkIds(): Array<number> {
        return [
            COLLECTOR_GOUPH,
            TRADER_REEP,
            CARRIER_TOROCCO,
            MINER_MARON,
            BLACKSMITH_BRUNON,
            WAREHOUSE_KEEPER_MURDOC,
            WAREHOUSE_KEEPER_AIRY,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case GOBLIN_BRIGAND_LEADER:
            case GOBLIN_BRIGAND_LIEUTENANT:
                if ( state.isCondition( 5 ) && QuestHelper.hasQuestItem( player, BRUNONS_CONTRACT ) ) {
                    let dropChance = monsterDropChances[ data.npcId ]

                    if ( Math.random() > QuestHelper.getAdjustedChance( AQUAMARINE, dropChance, data.isChampion ) ) {
                        await QuestHelper.giveSingleItem( player, AQUAMARINE, _.random( 1, gemCount ) )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                        this.progressState( state, player )
                        return
                    }

                    if ( Math.random() > QuestHelper.getAdjustedChance( CHRYSOBERYL, dropChance, data.isChampion ) ) {
                        await QuestHelper.giveSingleItem( player, CHRYSOBERYL, _.random( 1, gemCount ) )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                        this.progressState( state, player )
                        return
                    }
                }

                return

            case BLADE_BAT:
                if ( state.isCondition( 11 )
                        && QuestHelper.hasQuestItem( player, BAT_DIAGRAM )
                        && Math.random() > QuestHelper.getAdjustedChance( STAR_DIAMOND, 0.2, data.isChampion ) ) {

                    await QuestHelper.giveSingleItem( player, STAR_DIAMOND, 1 )
                    await QuestHelper.takeSingleItem( player, BAT_DIAGRAM, -1 )

                    state.setConditionWithSound( 12 )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        switch ( data.eventName ) {
            case '30523-04.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, GOUPHS_CONTRACT, 1 )

                    return this.getPath( data.eventName )
                }

                return

            case '30555-02.html':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, REEPS_CONTRACT ) ) {
                    await QuestHelper.takeSingleItem( player, REEPS_CONTRACT, -1 )
                    await QuestHelper.giveSingleItem( player, ELVEN_WINE, 1 )
                    state.setConditionWithSound( 3, true )
                    return this.getPath( data.eventName )
                }

                return

            case '30526-02.html':
                if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, BRUNONS_DICE ) ) {
                    await QuestHelper.takeSingleItem( player, BRUNONS_DICE, -1 )
                    await QuestHelper.giveSingleItem( player, BRUNONS_CONTRACT, 1 )
                    state.setConditionWithSound( 5, true )
                    return this.getPath( data.eventName )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case COLLECTOR_GOUPH:

                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() !== Race.DWARF ) {
                            return this.getPath( '30523-01.htm' )
                        }

                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '30523-02.htm' )
                        }

                        return this.getPath( '30523-03.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, GOUPHS_CONTRACT ) ) {
                                    return this.getPath( '30523-05.html' )
                                }

                                break

                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                                if ( QuestHelper.hasAtLeastOneQuestItem( player, REEPS_CONTRACT, ELVEN_WINE, BRUNONS_DICE, BRUNONS_CONTRACT ) ) {
                                    return this.getPath( '30523-06.html' )
                                }

                                break

                            case 7:
                                if ( QuestHelper.hasQuestItem( player, GEM_BOX ) ) {
                                    await QuestHelper.takeSingleItem( player, GEM_BOX, -1 )
                                    await QuestHelper.giveSingleItem( player, COAL_PIECE, 1 )
                                    state.setConditionWithSound( 8, true )

                                    return this.getPath( '30523-07.html' )
                                }

                                break

                            case 8:
                            case 9:
                            case 10:
                            case 11:
                                if ( QuestHelper.hasAtLeastOneQuestItem( player, COAL_PIECE, BRUNONS_LETTER, BERRY_TART, BAT_DIAGRAM ) ) {
                                    return this.getPath( '30523-08.html' )
                                }

                                break

                            case 12:
                                if ( QuestHelper.hasQuestItem( player, STAR_DIAMOND ) ) {
                                    await NewbieRewardsHelper.giveNewbieReward( player )
                                    await QuestHelper.addExpAndSp( player, 34565, 2962 )
                                    await QuestHelper.giveAdena( player, 14666, true )

                                    await aigle.resolve( REWARDS ).each( ( item: ItemDefinition ) => {
                                        return QuestHelper.rewardSingleItem( player, item.id, item.count )
                                    } )

                                    await QuestHelper.rewardSingleItem( player, SILVERSMITH_HAMMER, 1 )
                                    await state.exitQuest( false, true )

                                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                                    return this.getPath( '30523-09.html' )
                                }

                                break
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case TRADER_REEP:
                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.hasQuestItem( player, GOUPHS_CONTRACT ) ) {
                            await QuestHelper.takeSingleItem( player, GOUPHS_CONTRACT, -1 )
                            await QuestHelper.giveSingleItem( player, REEPS_CONTRACT, 1 )
                            state.setConditionWithSound( 2, true )

                            return this.getPath( '30516-01.html' )
                        }

                        break

                    case 2:
                        if ( QuestHelper.hasQuestItem( player, REEPS_CONTRACT ) ) {
                            return this.getPath( '30516-02.html' )
                        }

                        break

                    default:
                        if ( state.getCondition() > 2 ) {
                            return this.getPath( '30516-02.html' )
                        }

                        break
                }

                break

            case CARRIER_TOROCCO:
                switch ( state.getCondition() ) {
                    case 2:
                        if ( QuestHelper.hasQuestItem( player, REEPS_CONTRACT ) ) {
                            return this.getPath( '30555-01.html' )
                        }

                        break

                    case 3:
                        if ( QuestHelper.hasQuestItem( player, ELVEN_WINE ) ) {
                            return this.getPath( '30555-03.html' )
                        }

                        break

                    case 7:
                        if ( QuestHelper.hasQuestItem( player, GEM_BOX ) ) {
                            return this.getPath( '30555-04.html' )
                        }

                        break

                    default:
                        if ( state.isStarted() ) {
                            return this.getPath( '30555-05.html' )
                        }

                        break
                }

                break

            case MINER_MARON:
                switch ( state.getCondition() ) {
                    case 3:
                        if ( QuestHelper.hasQuestItem( player, ELVEN_WINE ) ) {
                            await QuestHelper.takeSingleItem( player, ELVEN_WINE, -1 )
                            await QuestHelper.giveSingleItem( player, BRUNONS_DICE, 1 )

                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30529-01.html' )
                        }

                        break

                    case 4:
                        if ( QuestHelper.hasQuestItem( player, BRUNONS_DICE ) ) {
                            return this.getPath( '30529-02.html' )
                        }

                        break

                    default:
                        if ( state.getCondition() > 4 ) {
                            return this.getPath( '30529-03.html' )
                        }

                        break
                }

                break

            case BLACKSMITH_BRUNON:
                switch ( state.getCondition() ) {
                    case 4:
                        if ( QuestHelper.hasQuestItem( player, BRUNONS_DICE ) ) {
                            return this.getPath( '30526-01.html' )
                        }

                        break

                    case 5:
                        if ( QuestHelper.hasQuestItem( player, BRUNONS_CONTRACT ) ) {
                            return this.getPath( '30526-03.html' )
                        }

                        break

                    case 6:
                        if ( QuestHelper.hasQuestItem( player, BRUNONS_CONTRACT )
                                && QuestHelper.getQuestItemsCount( player, AQUAMARINE ) >= gemCount
                                && QuestHelper.getQuestItemsCount( player, CHRYSOBERYL ) >= gemCount ) {
                            await QuestHelper.takeMultipleItems( player, -1, BRUNONS_CONTRACT, AQUAMARINE, CHRYSOBERYL )
                            await QuestHelper.giveSingleItem( player, GEM_BOX, 1 )

                            state.setConditionWithSound( 7, true )
                            return this.getPath( '30526-04.html' )
                        }

                        break

                    case 7:
                        if ( QuestHelper.hasQuestItem( player, GEM_BOX ) ) {
                            return this.getPath( '30526-05.html' )
                        }
                        break

                    case 8:
                        if ( QuestHelper.hasQuestItem( player, COAL_PIECE ) ) {
                            await QuestHelper.takeSingleItem( player, COAL_PIECE, -1 )
                            await QuestHelper.giveSingleItem( player, BRUNONS_LETTER, 1 )

                            state.setConditionWithSound( 9, true )
                            return this.getPath( '30526-06.html' )
                        }

                        break

                    case 9:
                        if ( QuestHelper.hasQuestItem( player, BRUNONS_LETTER ) ) {
                            return this.getPath( '30526-07.html' )
                        }

                        break

                    case 10:
                    case 11:
                    case 12:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, BERRY_TART, BAT_DIAGRAM, STAR_DIAMOND ) ) {
                            return this.getPath( '30526-08.html' )
                        }

                        break
                }

                break

            case WAREHOUSE_KEEPER_MURDOC:
                switch ( state.getCondition() ) {
                    case 9:
                        if ( QuestHelper.hasQuestItem( player, BRUNONS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, BRUNONS_LETTER, -1 )
                            await QuestHelper.giveSingleItem( player, BERRY_TART, 1 )

                            state.setConditionWithSound( 10, true )
                            return this.getPath( '30521-01.html' )
                        }

                        break

                    case 10:
                        if ( QuestHelper.hasQuestItem( player, BERRY_TART ) ) {
                            return this.getPath( '30521-02.html' )
                        }
                        break

                    case 11:
                    case 12:
                        return this.getPath( '30521-03.html' )
                }

                break

            case WAREHOUSE_KEEPER_AIRY:
                switch ( state.getCondition() ) {
                    case 10:
                        if ( QuestHelper.hasQuestItem( player, BERRY_TART ) ) {
                            await QuestHelper.takeSingleItem( player, BERRY_TART, -1 )
                            await QuestHelper.giveSingleItem( player, BAT_DIAGRAM, 1 )
                            state.setConditionWithSound( 11, true )

                            return this.getPath( '30522-01.html' )
                        }

                        break

                    case 11:
                        if ( QuestHelper.hasQuestItem( player, BAT_DIAGRAM ) ) {
                            return this.getPath( '30522-02.html' )
                        }

                        break

                    case 12:
                        if ( QuestHelper.hasQuestItem( player, STAR_DIAMOND ) ) {
                            return this.getPath( '30522-03.html' )
                        }
                        break

                    default:
                        if ( state.isStarted() ) {
                            return this.getPath( '30522-04.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    progressState( state: QuestState, player: L2PcInstance ): void {
        if ( QuestHelper.getQuestItemsCount( player, AQUAMARINE ) >= gemCount
                || QuestHelper.getQuestItemsCount( player, CHRYSOBERYL ) >= gemCount ) {
            state.setConditionWithSound( 6, true )
        }
    }
}