import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { Race } from '../../gameService/enums/Race'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'
import aigle from 'aigle'
import _ from 'lodash'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const BLACKSMITH_KAROYD = 30307
const CECON = 30132
const HARNE = 30144

const KAROYDS_LETTER = 968
const CECKTINONS_VOUCHER1 = 969
const CECKTINONS_VOUCHER2 = 970
const SOUL_CATCHER = 971
const PRESERVE_OIL = 972
const ZOMBIE_HEAD = 973
const STEELBENDERS_HEAD = 974
const BONE_FRAGMENT = 1107

const MARSH_ZOMBIE = 20015
const DOOM_SOLDIER = 20455
const SKELETON_HUNTER = 20517
const SKELETON_HUNTER_ARCHER = 20518

const BLOODSABER = 975
const minimumLevel = 9

const itemRewards = [
    createItemDefinition( 1060, 100 ), // Lesser Healing Potion
    createItemDefinition( 4412, 10 ), // Echo Crystal - Theme of Battle
    createItemDefinition( 4413, 10 ), // Echo Crystal - Theme of Love
    createItemDefinition( 4414, 10 ), // Echo Crystal - Theme of Solitude
    createItemDefinition( 4415, 10 ), // Echo Crystal - Theme of Feast
    createItemDefinition( 4416, 10 ), // Echo Crystal - Theme of Celebration
]

export class SpiritOfCraftsman extends ListenerLogic {
    constructor() {
        super( 'Q00103_SpiritOfCraftsman', 'listeners/tracked/SpiritOfCraftsman.ts' )
        this.questId = 103
        this.questItemIds = [
            KAROYDS_LETTER,
            CECKTINONS_VOUCHER1,
            CECKTINONS_VOUCHER2,
            SOUL_CATCHER,
            PRESERVE_OIL,
            ZOMBIE_HEAD,
            STEELBENDERS_HEAD,
            BONE_FRAGMENT,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            MARSH_ZOMBIE,
            DOOM_SOLDIER,
            SKELETON_HUNTER,
            SKELETON_HUNTER_ARCHER,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00103_SpiritOfCraftsman'
    }

    getQuestStartIds(): Array<number> {
        return [ BLACKSMITH_KAROYD ]
    }

    getTalkIds(): Array<number> {
        return [
            BLACKSMITH_KAROYD,
            CECON,
            HARNE,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let state = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, npc )

        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        switch ( data.npcId ) {
            case MARSH_ZOMBIE:
                if ( QuestHelper.hasQuestItem( player, PRESERVE_OIL )
                        && _.random( 10 ) < QuestHelper.getAdjustedChance( ZOMBIE_HEAD, 5, data.isChampion )
                        && GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {

                    await QuestHelper.giveSingleItem( player, ZOMBIE_HEAD, 1 )
                    await QuestHelper.takeSingleItem( player, PRESERVE_OIL, -1 )
                    state.setConditionWithSound( 7, true )
                }

                break

            case DOOM_SOLDIER:
            case SKELETON_HUNTER:
            case SKELETON_HUNTER_ARCHER:
                if ( QuestHelper.hasQuestItem( player, CECKTINONS_VOUCHER2 ) ) {
                    await QuestHelper.rewardAndProgressState( state.getPlayer(), state, BONE_FRAGMENT, 1, 10, data.isChampion, 4 )
                }

                break
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return null
        }

        switch ( data.eventName ) {
            case '30307-04.htm':
                return this.getPath( data.eventName )

            case '30307-05.htm':
                if ( state.isCreated() ) {
                    state.startQuest()

                    let player = L2World.getPlayer( data.playerId )
                    await QuestHelper.giveSingleItem( player, KAROYDS_LETTER, 1 )

                    return this.getPath( data.eventName )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case BLACKSMITH_KAROYD:
                if ( state.isCreated() ) {
                    if ( player.getRace() !== Race.DARK_ELF ) {
                        return this.getPath( '30307-01.htm' )
                    }

                    if ( player.getLevel() <= minimumLevel ) {
                        return this.getPath( '30307-02.htm' )
                    }

                    return this.getPath( '30307-03.htm' )
                }

                if ( state.isStarted() ) {
                    if ( QuestHelper.hasAtLeastOneQuestItem( player, KAROYDS_LETTER, CECKTINONS_VOUCHER1, CECKTINONS_VOUCHER2 ) ) {
                        return this.getPath( '30307-06.html' )
                    }

                    if ( QuestHelper.hasQuestItems( player, STEELBENDERS_HEAD ) ) {
                        await NewbieRewardsHelper.giveNewbieReward( player )
                        await QuestHelper.addExpAndSp( player, 46663, 3999 )
                        await QuestHelper.giveAdena( player, 19799, true )

                        await aigle.resolve( itemRewards ).each( ( item: ItemDefinition ) => {
                            return QuestHelper.rewardSingleItem( player, item.id, item.count )
                        } )

                        await QuestHelper.giveSingleItem( player, BLOODSABER, 1 )
                        await state.exitQuest( false, true )
                        player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                        return this.getPath( '30307-07.html' )
                    }

                    break
                }

                if ( state.isCompleted() ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case CECON:
                if ( state.isStarted() ) {
                    if ( QuestHelper.hasQuestItem( player, KAROYDS_LETTER ) ) {
                        state.setConditionWithSound( 2, true )
                        await QuestHelper.takeSingleItem( player, KAROYDS_LETTER, -1 )
                        await QuestHelper.giveSingleItem( player, CECKTINONS_VOUCHER1, 1 )

                        return this.getPath( '30132-01.html' )
                    }

                    if ( QuestHelper.hasAtLeastOneQuestItem( player, CECKTINONS_VOUCHER1, CECKTINONS_VOUCHER2 ) ) {
                        return this.getPath( '30132-02.html' )
                    }

                    if ( QuestHelper.hasQuestItem( player, SOUL_CATCHER ) ) {
                        state.setConditionWithSound( 6, true )
                        await QuestHelper.takeSingleItem( player, SOUL_CATCHER, -1 )
                        await QuestHelper.giveSingleItem( player, PRESERVE_OIL, 1 )

                        return this.getPath( '30132-03.html' )
                    }

                    if ( QuestHelper.hasQuestItem( player, PRESERVE_OIL ) && !QuestHelper.hasQuestItems( player, ZOMBIE_HEAD, STEELBENDERS_HEAD ) ) {
                        return this.getPath( '30132-04.html' )
                    }

                    if ( QuestHelper.hasQuestItem( player, ZOMBIE_HEAD ) ) {
                        state.setConditionWithSound( 8, true )
                        await QuestHelper.takeSingleItem( player, ZOMBIE_HEAD, -1 )
                        await QuestHelper.giveSingleItem( player, STEELBENDERS_HEAD, 1 )

                        return this.getPath( '30132-05.html' )
                    }

                    if ( QuestHelper.hasQuestItem( player, STEELBENDERS_HEAD ) ) {
                        return this.getPath( '30132-06.html' )
                    }
                }

                break

            case HARNE:
                if ( state.isStarted() ) {
                    if ( QuestHelper.hasQuestItem( player, CECKTINONS_VOUCHER1 ) ) {
                        state.setConditionWithSound( 3, true )
                        await QuestHelper.takeSingleItem( player, CECKTINONS_VOUCHER1, -1 )
                        await QuestHelper.giveSingleItem( player, CECKTINONS_VOUCHER2, 1 )

                        return this.getPath( '30144-01.html' )
                    }

                    if ( QuestHelper.hasQuestItem( player, CECKTINONS_VOUCHER2 ) ) {
                        if ( QuestHelper.getQuestItemsCount( player, BONE_FRAGMENT ) >= 10 ) {
                            state.setConditionWithSound( 5, true )
                            await QuestHelper.takeSingleItem( player, CECKTINONS_VOUCHER2, -1 )
                            await QuestHelper.takeSingleItem( player, BONE_FRAGMENT, 10 )
                            await QuestHelper.giveSingleItem( player, SOUL_CATCHER, 1 )

                            return this.getPath( '30144-03.html' )
                        }

                        return this.getPath( '30144-02.html' )
                    }

                    if ( QuestHelper.hasQuestItem( player, SOUL_CATCHER ) ) {
                        return this.getPath( '30144-04.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}