import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const NATOOLS = 30894

const mobChanceMap = {
    20135: 53, // Alligator
    20791: 100, // Crokian Warrior
    20792: 92, // Farhite
}

const PREDECESSORS_REPORT = 10350

const minimumLevel = 37
const maximumLevel = 42
const REPORT_COUNT = 30

const variableNames = {
    talk: 't'
}

export class ShadowFoxPartThree extends ListenerLogic {
    constructor() {
        super( 'Q00141_ShadowFoxPart3', 'listeners/tracked/ShadowFoxPartThree.ts' )
        this.questId = 141
        this.questItemIds = [ PREDECESSORS_REPORT ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( mobChanceMap ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00141_ShadowFoxPart3'
    }

    getQuestStartIds(): Array<number> {
        return [ NATOOLS ]
    }

    getTalkIds(): Array<number> {
        return [ NATOOLS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 3 )
        if ( !player ) {
            return
        }

        if ( _.random( 100 ) < QuestHelper.getAdjustedChance( PREDECESSORS_REPORT, mobChanceMap[ data.npcId ], data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, PREDECESSORS_REPORT, 1, data.isChampion )

            if ( QuestHelper.getQuestItemsCount( player, PREDECESSORS_REPORT ) >= REPORT_COUNT ) {
                let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )
                state.setConditionWithSound( 3, true )
                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30894-05.html':
            case '30894-10.html':
            case '30894-11.html':
            case '30894-12.html':
            case '30894-13.html':
            case '30894-14.html':
            case '30894-16.html':
            case '30894-17.html':
            case '30894-19.html':
            case '30894-20.html':
                break

            case '30894-03.htm':
                state.startQuest()
                break

            case '30894-06.html':
                state.setConditionWithSound( 2, true )
                break

            case '30894-15.html':
                state.setVariable( variableNames.talk, 2 )
                break

            case '30894-18.html':
                state.setConditionWithSound( 4, true )
                state.unsetVariable( variableNames.talk )
                break

            case '30894-21.html':
                let player = L2World.getPlayer( data.playerId )

                await QuestHelper.giveAdena( player, 88888, true )
                if ( player.getLevel() <= maximumLevel ) {
                    await QuestHelper.addExpAndSp( player, 278005, 17058 )
                }

                await state.exitQuest( false, true )

                let companionState: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00998_FallenAngelSelect', true )
                if ( companionState ) {
                    companionState.startQuest()
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() >= minimumLevel ) {
                    return this.getPath( player.hasQuestCompleted( 'Q00140_ShadowFoxPart2' ) ? '30894-01.htm' : '30894-00.html' )
                }

                return this.getPath( '30894-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30894-04.html' )

                    case 2:
                        return this.getPath( '30894-07.html' )

                    case 3:
                        if ( state.getVariable( variableNames.talk ) === 1 ) {
                            return this.getPath( '30894-09.html' )
                        }

                        if ( state.getVariable( variableNames.talk ) === 2 ) {
                            return this.getPath( '30894-16.html' )
                        }

                        await QuestHelper.takeSingleItem( player, PREDECESSORS_REPORT, -1 )
                        state.setVariable( variableNames.talk, 1 )

                        return this.getPath( '30894-08.html' )

                    case 4:
                        return this.getPath( '30894-19.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}