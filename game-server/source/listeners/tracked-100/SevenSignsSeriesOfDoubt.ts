import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { teleportCharacterToGeometryCoordinates } from '../../gameService/helpers/TeleportHelper'
import { GeometryId } from '../../gameService/enums/GeometryId'

const HOLLINT = 30191
const HECTOR = 30197
const STAN = 30200
const CROOP = 30676
const UNIDENTIFIED_BODY = 32568

const CROOPS_INTRODUCTION = 13813
const JACOBS_NECKLACE = 13814
const CROOPS_LETTER = 13815

const minimumLevel = 79

export class SevenSignsSeriesOfDoubt extends ListenerLogic {
    constructor() {
        super( 'Q00192_SevenSignsSeriesOfDoubt', 'listeners/tracked/SevenSignsSeriesOfDoubt.ts' )
        this.questId = 192
        this.questItemIds = [ CROOPS_INTRODUCTION, JACOBS_NECKLACE, CROOPS_LETTER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00192_SevenSignsSeriesOfDoubt'
    }

    getQuestStartIds(): Array<number> {
        return [ CROOP, UNIDENTIFIED_BODY ]
    }

    getTalkIds(): Array<number> {
        return [ CROOP, STAN, UNIDENTIFIED_BODY, HECTOR, HOLLINT ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30676-02.htm':
                break

            case '30676-03.html':
                state.startQuest()
                break

            case 'video':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    await player.showQuestMovie( 8 )
                    this.startQuestTimer( 'back', 32000, data.characterId, data.playerId )
                }

                return

            case 'back':
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 81654, 54851, -1513 )
                return

            case '30676-10.html':
            case '30676-11.html':
            case '30676-12.html':
            case '30676-13.html':
                if ( state.isCondition( 6 ) && QuestHelper.hasQuestItem( player, JACOBS_NECKLACE ) ) {
                    break
                }

                return

            case '30676-14.html':
                if ( state.isCondition( 6 ) && QuestHelper.hasQuestItem( player, JACOBS_NECKLACE ) ) {
                    await QuestHelper.takeMultipleItems( player, -1, CROOPS_LETTER, JACOBS_NECKLACE )

                    state.setConditionWithSound( 7, true )
                    break
                }

                return

            case '30200-02.html':
            case '30200-03.html':
                if ( state.isCondition( 4 ) ) {
                    break
                }

                return

            case '30200-04.html':
                if ( state.isCondition( 4 ) ) {
                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '32568-02.html':
                if ( state.isCondition( 5 ) ) {
                    await QuestHelper.giveSingleItem( player, JACOBS_NECKLACE, 1 )

                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '30197-02.html':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, CROOPS_INTRODUCTION ) ) {
                    break
                }

                return

            case '30197-03.html':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, CROOPS_INTRODUCTION ) ) {
                    await QuestHelper.takeSingleItem( player, CROOPS_INTRODUCTION, -1 )

                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '30191-02.html':
                if ( state.isCondition( 7 ) && QuestHelper.hasQuestItem( player, CROOPS_LETTER ) ) {
                    break
                }

                return

            case 'reward':
                if ( state.isCondition( 7 ) && QuestHelper.hasQuestItem( player, CROOPS_LETTER ) ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        await QuestHelper.addExpAndSp( player, 52518015, 5817677 )
                        await state.exitQuest( false, true )

                        return this.getPath( '30191-03.html' )
                    }

                    return this.getPath( 'level_check.html' )
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === CROOP ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30676-01.htm' : '30676-04.html' )
                }

                if ( data.characterNpcId === UNIDENTIFIED_BODY ) {
                    return this.getPath( '32568-04.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case CROOP:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30676-06.html' )

                            case 2:
                                await QuestHelper.giveSingleItem( player, CROOPS_INTRODUCTION, 1 )

                                state.setConditionWithSound( 3, true )
                                return this.getPath( '30676-07.html' )

                            case 3:
                            case 4:
                            case 5:
                                return this.getPath( '30676-08.html' )


                            case 6:
                                if ( QuestHelper.hasQuestItem( player, JACOBS_NECKLACE ) ) {
                                    return this.getPath( '30676-09.html' )
                                }

                                break

                        }

                        break

                    case HECTOR:
                        if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, CROOPS_INTRODUCTION ) ) {
                            return this.getPath( '30197-01.html' )
                        }

                        if ( state.getCondition() > 3 ) {
                            return this.getPath( '30197-04.html' )
                        }

                        break

                    case STAN:
                        if ( state.isCondition( 4 ) ) {
                            return this.getPath( '30200-01.html' )
                        }

                        if ( state.getCondition() > 4 ) {
                            return this.getPath( '30200-05.html' )
                        }

                        break

                    case UNIDENTIFIED_BODY:
                        if ( state.getCondition() < 5 ) {
                            return this.getPath( '32568-03.html' )
                        }

                        if ( state.isCondition( 5 ) ) {
                            return this.getPath( '32568-01.html' )
                        }

                        break

                    case HOLLINT:
                        if ( state.isCondition( 7 ) && QuestHelper.hasQuestItem( player, CROOPS_LETTER ) ) {
                            return this.getPath( '30191-01.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === CROOP ) {
                    return this.getPath( '30676-05.html' )
                }

                if ( data.characterNpcId === UNIDENTIFIED_BODY ) {
                    return this.getPath( '32568-04.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}