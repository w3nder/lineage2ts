import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'

const LIVINA = 30572
const KARUDA = 32017

const SCROLL_ENCHANT_ARMOR_D_GRADE = 956
const minimumLevel = 20

export class WalkOfFate extends ListenerLogic {
    constructor() {
        super( 'Q00112_WalkOfFate', 'listeners/tracked/WalkOfFate.ts' )
        this.questId = 112
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00112_WalkOfFate'
    }

    getQuestStartIds(): Array<number> {
        return [ LIVINA ]
    }

    getTalkIds(): Array<number> {
        return [ LIVINA, KARUDA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30572-04.htm':
                state.startQuest()
                return this.getPath( data.eventName )

            case '32017-02.html':
                let player = L2World.getPlayer( data.playerId )
                await QuestHelper.giveAdena( player, 22308, true )
                await QuestHelper.addExpAndSp( player, 112876, 5774 )
                await QuestHelper.rewardSingleItem( player, SCROLL_ENCHANT_ARMOR_D_GRADE, 1 )

                await state.exitQuest( false, true )

                return this.getPath( data.eventName )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '30572-03.html' : '30572-01.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case LIVINA:
                        return this.getPath( '30572-05.html' )

                    case KARUDA:
                        return this.getPath( '32017-01.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}