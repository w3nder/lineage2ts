import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const BIOTIN = 30031

const NERKAS = 27016

const ENCHANT_ARMOR_D = 956
const CLAY_TABLET = 1025

const minimumLevel = 21

export class SeedOfEvil extends ListenerLogic {
    constructor() {
        super( 'Q00158_SeedOfEvil', 'listeners/tracked/SeedOfEvil.ts' )
        this.questId = 158
        this.questItemIds = [ CLAY_TABLET ]
    }

    getAttackableAttackIds(): Array<number> {
        return [ NERKAS ]
    }

    getAttackableKillIds(): Array<number> {
        return [ NERKAS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00158_SeedOfEvil'
    }

    getQuestStartIds(): Array<number> {
        return [ BIOTIN ]
    }

    getTalkIds(): Array<number> {
        return [ BIOTIN ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let value: boolean = NpcVariablesManager.get( data.targetId, this.getName() ) as boolean
        if ( !value ) {
            NpcVariablesManager.set( data.targetId, this.getName(), true )

            let npc = L2World.getObjectById( data.targetId ) as L2Npc
            return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds._HOW_DARE_YOU_CHALLENGE_ME )
        }

        return
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( state ) {
            let player = L2World.getPlayer( data.playerId )

            if ( !QuestHelper.hasQuestItem( player, CLAY_TABLET ) ) {
                await QuestHelper.giveSingleItem( player, CLAY_TABLET, 1 )
                state.setConditionWithSound( 2, true )
            }
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_POWER_OF_LORD_BELETH_RULES_THE_WHOLE_WORLD )

        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '30031-03.htm' ) {
            return
        }

        state.startQuest()

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30031-02.htm' : '30031-01.html' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 1 ) ) {
                    return this.getPath( '30031-04.html' )
                }

                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, CLAY_TABLET ) ) {
                    await QuestHelper.rewardSingleItem( player, ENCHANT_ARMOR_D, 1 )
                    await QuestHelper.addExpAndSp( player, 17818, 927 )
                    await QuestHelper.giveAdena( player, 1495, true )
                    await state.exitQuest( false, true )

                    return this.getPath( '30031-05.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}