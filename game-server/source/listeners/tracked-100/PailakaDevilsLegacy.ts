import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import {
    AttackableKillEvent,
    NpcApproachedForTalkEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

const KAMS = 18629 // Kams (Panuka)
const ALKASO = 18631 // Alkaso (Panuka)
const LEMATAN = 18633 // Lematan
const SURVIVOR = 32498 // Devil's Isle Survivor
const SUPPORTER = 32501 // Devil's Isle Supporter
const ADVENTURER1 = 32508 // Dwarf Adventurer
const ADVENTURER2 = 32511 // Dwarf Adventurer

const SWORD = 13042 // Ancient Legacy Sword
const ENH_SWORD1 = 13043 // Enhanced Ancient Legacy Sword
const ENH_SWORD2 = 13044 // Complete Ancient Legacy Sword
const SCROLL_1 = 13046 // Pailaka Weapon Upgrade Stage 1
const SCROLL_2 = 13047 // Pailaka Weapon Upgrade Stage 2
const SHIELD = 13032 // Pailaka Instant Shield
const HEALING_POTION = 13033 // Quick Healing Potion
const ANTIDOTE_POTION = 13048 // Pailaka Antidote
const DIVINE_POTION = 13049 // Divine Soul
const DEFENCE_POTION = 13059 // Long-Range Defense Increasing Potion
const PAILAKA_KEY = 13150 // Pailaka All-Purpose Key
const BRACELET = 13295 // Pailaka Bracelet
const ESCAPE = 736 // Scroll of Escape

const VITALITY_REPLENISHING = new SkillHolder( 5774, 2 ) // Pailaka Reward Vitality Replenishing

const minimumLevel = 61
const maximumLevel = 67
const EXIT_TIME = 5

export class PailakaDevilsLegacy extends ListenerLogic {
    constructor() {
        super( 'Q00129_PailakaDevilsLegacy', 'listeners/tracked/PailakaDevilsLegacy.ts' )
        this.questId = 129
        this.questItemIds = [
            SWORD,
            ENH_SWORD1,
            ENH_SWORD2,
            SCROLL_1,
            SCROLL_2,
            SHIELD,
            HEALING_POTION,
            ANTIDOTE_POTION,
            DIVINE_POTION,
            DEFENCE_POTION,
            PAILAKA_KEY,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ KAMS, ALKASO, LEMATAN ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ SURVIVOR, SUPPORTER, ADVENTURER1, ADVENTURER2 ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00129_PailakaDevilsLegacy'
    }

    getQuestStartIds(): Array<number> {
        return [ SURVIVOR ]
    }

    getTalkIds(): Array<number> {
        return [ SURVIVOR, SUPPORTER, ADVENTURER1, ADVENTURER2 ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.npcId ) {
            case KAMS:
                if ( QuestHelper.hasQuestItem( player, SWORD ) ) {
                    await QuestHelper.giveSingleItem( player, SCROLL_1, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case ALKASO:
                if ( QuestHelper.hasQuestItem( player, ENH_SWORD1 ) ) {
                    await QuestHelper.giveSingleItem( player, SCROLL_2, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case LEMATAN:
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )
                }

                return
        }
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        if ( data.characterNpcId === ADVENTURER2 ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state || !state.isCompleted() ) {
            return this.getPath( `${ data.characterNpcId }.htm` )
        }

        return this.getPath( '32511-03.htm' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32498-02.htm':
            case '32498-03.htm':
            case '32498-04.htm':
            case '32501-02.htm':
            case '32501-04.htm':
                break

            case '32498-05.htm':
                if ( !state.isStarted() ) {
                    state.startQuest()

                    break
                }

                return

            case '32501-03.htm':
                if ( state.isCondition( 2 ) ) {
                    let player = L2World.getPlayer( data.playerId )
                    await QuestHelper.giveSingleItem( player, SWORD, 1 )

                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case SURVIVOR:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '32498-11.htm' )
                        }

                        if ( player.getLevel() > maximumLevel ) {
                            return this.getPath( '32498-12.htm' )
                        }

                        return this.getPath( '32498-01.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.getCondition() > 1 ) {
                            return this.getPath( '32498-08.htm' )
                        }

                        return this.getPath( '32498-06.htm' )

                    case QuestStateValues.COMPLETED:
                        return this.getPath( '32498-10.htm' )

                    default:
                        return this.getPath( '32498-01.htm' )
                }

                break

            case SUPPORTER:
                if ( state.getCondition() > 2 ) {
                    return this.getPath( '32501-04.htm' )
                }

                return this.getPath( '32501-01.htm' )

            case ADVENTURER1:
                if ( player.hasSummon() ) {
                    return this.getPath( '32508-07.htm' )
                }

                if ( QuestHelper.hasQuestItem( player, SWORD ) ) {
                    if ( QuestHelper.hasQuestItem( player, SCROLL_1 ) ) {
                        await QuestHelper.takeSingleItem( player, SWORD, -1 )
                        await QuestHelper.takeSingleItem( player, SCROLL_1, -1 )
                        await QuestHelper.giveSingleItem( player, ENH_SWORD1, 1 )

                        return this.getPath( '32508-03.htm' )
                    }

                    return this.getPath( '32508-02.htm' )
                }

                if ( QuestHelper.hasQuestItem( player, ENH_SWORD1 ) ) {
                    if ( QuestHelper.hasQuestItem( player, SCROLL_2 ) ) {
                        await QuestHelper.takeSingleItem( player, ENH_SWORD1, -1 )
                        await QuestHelper.takeSingleItem( player, SCROLL_2, -1 )
                        await QuestHelper.giveSingleItem( player, ENH_SWORD2, 1 )

                        return this.getPath( '32508-05.htm' )
                    }

                    return this.getPath( '32508-04.htm' )
                }

                if ( QuestHelper.hasQuestItems( player, ENH_SWORD2 ) ) {
                    return this.getPath( '32508-06.htm' )
                }

                return this.getPath( '32508-00.htm' )

            case ADVENTURER2:
                if ( player.hasSummon() ) {
                    return this.getPath( '32511-02.htm' )
                }

                // TODO : implement when instances are working
                // final Instance inst = InstanceManager.getInstance().getInstance(npc.getInstanceId());
                // state.exitQuest(false, true);
                // inst.setDuration(EXIT_TIME * 60000);
                // inst.setEmptyDestroyTime(0);
                // if (inst.containsPlayer(player.getObjectId())) {
                //     npc.setTarget(player);
                //     npc.doCast(VITALITY_REPLENISHING);
                //     addExpAndSp(player, 10800000, 950000);
                //     rewardItems(player, BRACELET, 1);
                //     rewardItems(player, ESCAPE, 1);
                //
                //     return
                // }

                return this.getPath( '32511-01.htm' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}