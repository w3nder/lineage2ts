import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { CycleStepManager } from '../../gameService/cache/CycleStepManager'
import { CycleType } from '../../gameService/enums/CycleType'

const KANIS = 32264
const GALATE = 32292
const REFINED_CRYSTAL_SAMPLE = 9785
const minimumLevel = 78

export class ThatsBloodyHot extends ListenerLogic {
    constructor() {
        super( 'Q00133_ThatsBloodyHot', 'listeners/tracked/ThatsBloodyHot.ts' )
        this.questId = 133
        this.questItemIds = [ REFINED_CRYSTAL_SAMPLE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00133_ThatsBloodyHot'
    }

    getQuestStartIds(): Array<number> {
        return [ KANIS ]
    }

    getTalkIds(): Array<number> {
        return [ KANIS, GALATE ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32264-04.html':
                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    break
                }

                return

            case '32264-06.html':
            case '32264-07.html':
                if ( state.isCondition( 1 ) ) {
                    break
                }

                return
            case '32264-08.html':
                state.setConditionWithSound( 2 )
                break

            case '32264-10.html':
            case '32264-11.html':
                if ( state.isCondition( 2 ) ) {
                    break
                }

                return

            case '32264-12.html':
                if ( state.isCondition( 2 ) ) {
                    await QuestHelper.giveSingleItem( player, REFINED_CRYSTAL_SAMPLE, 1 )
                    state.setConditionWithSound( 3 )
                    break
                }

                return

            case '32292-03.html':
                if ( state.isCondition( 3 ) ) {
                    break
                }

                return

            case '32292-05.html':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, REFINED_CRYSTAL_SAMPLE ) ) {
                    await QuestHelper.takeSingleItem( player, REFINED_CRYSTAL_SAMPLE, -1 )
                    state.setConditionWithSound( 4 )

                    break
                }

                return

            case '32292-06.html':
                if ( state.isCondition( 4 ) ) {
                    if ( !CycleStepManager.isLocked( CycleType.Hellbound ) ) {
                        await QuestHelper.giveAdena( player, 254247, true )
                        await QuestHelper.addExpAndSp( player, 331457, 32524 )
                        await state.exitQuest( false, true )

                        break
                    }

                    if ( CycleStepManager.getStep( CycleType.Hellbound ) < 1 ) {
                        CycleStepManager.setStep( CycleType.Hellbound, 1 )
                    }

                    await QuestHelper.giveAdena( player, 254247, true )
                    await QuestHelper.addExpAndSp( player, 325881, 32524 )
                    await state.exitQuest( false, true )

                    return this.getPath( '32292-07.html' )
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === KANIS ) {
                    if ( player.hasQuestCompleted( 'Q00131_BirdInACage' ) ) {
                        return this.getPath( player.getLevel() >= minimumLevel ? '32264-01.htm' : '32264-02.html' )
                    }

                    return this.getPath( '32264-03.html' )
                }

                break
            case QuestStateValues.STARTED:
                if ( data.characterNpcId === KANIS ) {
                    switch ( state.getCondition() ) {
                        case 1:
                            return this.getPath( '32264-05.html' )

                        case 2:
                            return this.getPath( '32264-09.html' )

                        case 0:
                            return

                        default:
                            return this.getPath( '32264-13.html' )
                    }
                }

                if ( data.characterNpcId === GALATE ) {
                    switch ( state.getCondition() ) {
                        case 0:
                        case 1:
                        case 2:
                            return this.getPath( '32292-01.html' )

                        case 3:
                            return this.getPath( '32292-02.html' )

                        case 4:
                            return this.getPath( '32292-04.html' )
                    }
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}