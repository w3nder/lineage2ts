import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { MagicSkillUse } from '../../gameService/packets/send/MagicSkillUse'
import { L2Npc } from '../../gameService/models/actor/L2Npc'

import _ from 'lodash'

const MUSHIKA = 32114
const KARAKAWEI = 32117
const ULU_KAIMU = 32119
const BALU_KAIMU = 32120
const CHUTA_KAIMU = 32121

const ORNITHOMIMUS_CLAW = 8779
const DEINONYCHUS_BONE = 8780
const EPITAPH_OF_WISDOM = 8781
const GAZKH_FRAGMENT = 8782

const skillId = 5089
const ornithomimusDrops = {
    22200: 661,
    22201: 330,
    22202: 661,
    22219: 327,
    22224: 327,
}

const deinonychusDrops = {
    22203: 651,
    22204: 326,
    22205: 651,
    22220: 319,
    22225: 319,
}

const minimumLevel = 76

const variableNames = {
    T: 't',
    E: 'e',
    P: 'p',
    U: 'u',
    memo: 'memo',
    O: 'o',
    O2: 'o2',
    N: 'n',
    W: 'w',
    A: 'a',
    G: 'g',
}

export class TheNameOfEvilPartOne extends ListenerLogic {
    constructor() {
        super( 'Q00125_TheNameOfEvilPartOne', 'listeners/tracked/TheNameOfEvilPartOne.ts' )
        this.questId = 125
        this.questItemIds = [
            ORNITHOMIMUS_CLAW,
            DEINONYCHUS_BONE,
            EPITAPH_OF_WISDOM,
            GAZKH_FRAGMENT,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ..._.keys( ornithomimusDrops ).map( value => _.parseInt( value ) ),
            ..._.keys( deinonychusDrops ).map( value => _.parseInt( value ) ),
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00125_TheNameOfEvil1'
    }

    getQuestStartIds(): Array<number> {
        return [ MUSHIKA ]
    }

    getTalkIds(): Array<number> {
        return [
            MUSHIKA,
            KARAKAWEI,
            ULU_KAIMU,
            BALU_KAIMU,
            CHUTA_KAIMU,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 3 )
        if ( !player ) {
            return
        }

        if ( ornithomimusDrops[ data.npcId ] ) {
            if ( QuestHelper.getQuestItemsCount( player, ORNITHOMIMUS_CLAW ) < 2
                    && _.random( 1000 ) < QuestHelper.getAdjustedChance( ORNITHOMIMUS_CLAW, ornithomimusDrops[ data.npcId ], data.isChampion ) ) {
                await QuestHelper.rewardSingleQuestItem( player, ORNITHOMIMUS_CLAW, 1, data.isChampion )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            }
        } else if ( deinonychusDrops[ data.npcId ] ) {
            if ( QuestHelper.getQuestItemsCount( player, DEINONYCHUS_BONE ) < 2
                    && _.random( 1000 ) < QuestHelper.getAdjustedChance( DEINONYCHUS_BONE, deinonychusDrops[ data.npcId ], data.isChampion ) ) {
                await QuestHelper.rewardSingleQuestItem( player, DEINONYCHUS_BONE, 1, data.isChampion )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            }
        }

        if ( QuestHelper.getQuestItemsCount( player, ORNITHOMIMUS_CLAW ) >= 2
                && QuestHelper.getQuestItemsCount( player, DEINONYCHUS_BONE ) >= 2 ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )
            state.setConditionWithSound( 4, true )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32114-05.html':
                state.startQuest()
                break

            case '32114-08.html':
                if ( state.isCondition( 1 ) ) {
                    await QuestHelper.giveSingleItem( player, GAZKH_FRAGMENT, 1 )
                    state.setConditionWithSound( 2, true )
                }
                break

            case '32117-09.html':
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3, true )
                }
                break

            case '32117-15.html':
                if ( state.isCondition( 4 ) ) {
                    state.setConditionWithSound( 5, true )
                }
                break

            case 'T_One':
                state.setVariable( variableNames.T, 1 )
                return this.getPath( '32119-04.html' )

            case 'E_One':
                state.setVariable( variableNames.E, 1 )
                return this.getPath( '32119-05.html' )

            case 'P_One':
                state.setVariable( variableNames.P, 1 )
                return this.getPath( '32119-06.html' )

            case 'U_One':
                state.setVariable( variableNames.U, 1 )
                if ( state.isCondition( 5 )
                        && state.getVariable( variableNames.T )
                        && state.getVariable( variableNames.E )
                        && state.getVariable( variableNames.P )
                        && state.getVariable( variableNames.U ) ) {

                    state.setVariable( variableNames.memo, 1 )
                    return this.getPath( '32119-08.html' )
                }

                state.unsetVariables( variableNames.T, variableNames.E, variableNames.P, variableNames.U )

                return this.getPath( '32119-07.html' )

            case '32119-07.html':
                state.unsetVariables( variableNames.T, variableNames.E, variableNames.P, variableNames.U )

                break

            case '32119-18.html':
                if ( state.isCondition( 5 ) ) {
                    state.setConditionWithSound( 6, true )
                    state.unsetVariable( variableNames.memo )
                }

                break

            case 'T_Two':
                state.setVariable( variableNames.T, 1 )
                return this.getPath( '32120-04.html' )

            case 'O_Two':
                state.setVariable( variableNames.O, 1 )
                return this.getPath( '32120-05.html' )

            case 'O2_Two':
                state.setVariable( variableNames.O2, 1 )
                return this.getPath( '32120-06.html' )

            case 'N_Two':
                state.setVariable( variableNames.N, 1 )
                if ( state.isCondition( 6 )
                        && state.getVariable( variableNames.T )
                        && state.getVariable( variableNames.O )
                        && state.getVariable( variableNames.O2 )
                        && state.getVariable( variableNames.N ) ) {

                    state.setVariable( variableNames.memo, 1 )
                    return this.getPath( '32120-08.html' )
                }

                state.unsetVariables( variableNames.T, variableNames.O, variableNames.O2, variableNames.N )

                return this.getPath( '32120-07.html' )

            case '32120-07.html':
                state.unsetVariables( variableNames.T, variableNames.O, variableNames.O2, variableNames.N )

                break

            case '32120-17.html':
                if ( state.isCondition( 6 ) ) {
                    state.setConditionWithSound( 7, true )
                    state.unsetVariable( variableNames.memo )
                }

                break

            case 'W_Three':
                state.setVariable( variableNames.W, 1 )
                return this.getPath( '32121-04.html' )

            case 'A_Three':
                state.setVariable( variableNames.A, 1 )
                return this.getPath( '32121-05.html' )

            case 'G_Three':
                state.setVariable( variableNames.G, 1 )
                return this.getPath( '32121-06.html' )

            case 'U_Three':
                state.setVariable( variableNames.U, 1 )
                if ( state.isCondition( 7 )
                        && state.getVariable( variableNames.W )
                        && state.getVariable( variableNames.A )
                        && state.getVariable( variableNames.G )
                        && state.getVariable( variableNames.U ) ) {

                    state.setVariable( variableNames.memo, 1 )
                    return this.getPath( '32121-08.html' )
                }

                state.unsetVariables( variableNames.W, variableNames.A, variableNames.G, variableNames.U )

                return this.getPath( '32121-07.html' )

            case '32121-07.html':
                state.unsetVariables( variableNames.W, variableNames.A, variableNames.G, variableNames.U )

                break

            case '32121-11.html':
                state.setVariable( variableNames.memo, 2 )
                break

            case '32121-16.html':
                state.setVariable( variableNames.memo, 3 )
                break

            case '32121-18.html':
                if ( state.isCondition( 7 ) && QuestHelper.hasQuestItem( player, GAZKH_FRAGMENT ) ) {
                    await QuestHelper.giveSingleItem( player, EPITAPH_OF_WISDOM, 1 )
                    await QuestHelper.takeSingleItem( player, GAZKH_FRAGMENT, -1 )

                    state.setConditionWithSound( 8, true )
                    state.unsetVariable( variableNames.memo )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case MUSHIKA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '32114-01a.htm' )
                        }

                        return this.getPath( player.hasQuestCompleted( 'Q00124_MeetingTheElroki' ) ? '32114-01.htm' : '32114-01b.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32114-09.html' )

                            case 2:
                                return this.getPath( '32114-10.html' )

                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7:
                                return this.getPath( '32114-11.html' )

                            case 8:
                                if ( QuestHelper.hasQuestItem( player, EPITAPH_OF_WISDOM ) ) {
                                    await QuestHelper.addExpAndSp( player, 859195, 86603 )
                                    await state.exitQuest( false, true )

                                    return this.getPath( '32114-12.html' )
                                }

                                break
                        }
                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case KARAKAWEI:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                            return this.getPath( '32117-01.html' )

                        case 2:
                            return this.getPath( '32117-02.html' )

                        case 3:
                            return this.getPath( '32117-10.html' )

                        case 4:
                            if ( QuestHelper.getQuestItemsCount( player, ORNITHOMIMUS_CLAW ) >= 2
                                    && QuestHelper.getQuestItemsCount( player, DEINONYCHUS_BONE ) >= 2 ) {
                                await QuestHelper.takeMultipleItems( player, -1, ORNITHOMIMUS_CLAW, DEINONYCHUS_BONE )

                                return this.getPath( '32117-11.html' )
                            }

                            break

                        case 5:
                            return this.getPath( '32117-16.html' )

                        case 6:
                        case 7:
                            return this.getPath( '32117-17.html' )

                        case 8:
                            return this.getPath( '32117-18.html' )
                    }
                }

                break

            case ULU_KAIMU:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            return this.getPath( '32119-01.html' )

                        case 5:
                            if ( !state.getVariable( variableNames.memo ) ) {
                                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                                BroadcastHelper.dataInLocation( npc, MagicSkillUse( data.characterId, data.playerId, skillId, 1, 1000, 0 ), npc.getBroadcastRadius() )

                                state.unsetVariables( variableNames.T, variableNames.E, variableNames.P, variableNames.U )

                                return this.getPath( '32119-02.html' )
                            }

                            return this.getPath( '32119-09.html' )

                        case 6:
                            return this.getPath( '32119-18.html' )

                        default:
                            return this.getPath( '32119-19.html' )
                    }
                }

                break

            case BALU_KAIMU:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            return this.getPath( '32120-01.html' )

                        case 6:
                            if ( !state.getVariable( variableNames.memo ) ) {
                                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                                BroadcastHelper.dataInLocation( npc, MagicSkillUse( data.characterId, data.playerId, skillId, 1, 1000, 0 ), npc.getBroadcastRadius() )

                                state.unsetVariables( variableNames.T, variableNames.O, variableNames.O2, variableNames.N )
                                return this.getPath( '32120-02.html' )
                            }

                            return this.getPath( '32120-09.html' )

                        case 7:
                            return this.getPath( '32120-17.html' )

                        default:
                            return this.getPath( '32119-18.html' )
                    }
                }

                break

            case CHUTA_KAIMU:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                            return this.getPath( '32121-01.html' )

                        case 7:
                            if ( !state.getVariable( variableNames.memo ) ) {
                                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                                BroadcastHelper.dataInLocation( npc, MagicSkillUse( data.characterId, data.playerId, skillId, 1, 1000, 0 ), npc.getBroadcastRadius() )

                                state.unsetVariables( variableNames.W, variableNames.A, variableNames.G, variableNames.U )
                                return this.getPath( '32121-02.html' )
                            }

                            switch ( state.getVariable( variableNames.memo ) ) {
                                case 1:
                                    return this.getPath( '32121-09.html' )

                                case 2:
                                    return this.getPath( '32121-19.html' )

                                case 3:
                                    return this.getPath( '32121-20.html' )
                            }

                            break

                        case 8:
                            return this.getPath( '32121-21.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}