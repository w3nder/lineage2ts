import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const JURIS = 30113
const HEAD_BLACKSMITH_KUSTO = 30512
const MAESTRO_NIKOLA = 30621
const RESEARCHER_LORAIN = 30673

const minimumLevel = 42
const maximumLevel = 48

export class LostDream extends ListenerLogic {
    constructor() {
        super( 'Q00190_LostDream', 'listeners/tracked/LostDream.ts' )
        this.questId = 190
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00190_LostDream'
    }

    getQuestStartIds(): Array<number> {
        return [ HEAD_BLACKSMITH_KUSTO ]
    }

    getTalkIds(): Array<number> {
        return [ HEAD_BLACKSMITH_KUSTO, RESEARCHER_LORAIN, MAESTRO_NIKOLA, JURIS ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30512-03.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    break
                }

                return

            case '30512-06.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '30113-02.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '30113-03.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === HEAD_BLACKSMITH_KUSTO && player.hasQuestCompleted( 'Q00187_NikolasHeart' ) ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30512-01.htm' : '30512-02.htm' )
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case HEAD_BLACKSMITH_KUSTO:
                        if ( memoState === 1 ) {
                            return this.getPath( '30512-04.html' )
                        }

                        if ( memoState === 2 ) {
                            return this.getPath( '30512-05.html' )
                        }

                        if ( ( memoState >= 3 ) && ( memoState <= 4 ) ) {
                            return this.getPath( '30512-07.html' )
                        }

                        if ( memoState === 5 ) {
                            await QuestHelper.giveAdena( player, 109427, true )
                            if ( player.getLevel() < maximumLevel ) {
                                await QuestHelper.addExpAndSp( player, 309467, 20614 )
                            }

                            await state.exitQuest( false, true )
                            return this.getPath( '30512-08.html' )
                        }

                        break

                    case JURIS:
                        if ( memoState === 1 ) {
                            return this.getPath( '30113-01.html' )
                        }

                        if ( memoState === 2 ) {
                            return this.getPath( '30113-04.html' )
                        }

                        break

                    case MAESTRO_NIKOLA:
                        if ( memoState === 4 ) {
                            state.setMemoState( 5 )
                            state.setConditionWithSound( 5, true )

                            return this.getPath( '30621-01.html' )
                        }

                        if ( memoState === 5 ) {
                            return this.getPath( '30621-02.html' )
                        }

                        break

                    case RESEARCHER_LORAIN:
                        if ( memoState === 3 ) {
                            state.setMemoState( 4 )
                            state.setConditionWithSound( 4, true )

                            return this.getPath( '30673-01.html' )
                        }

                        if ( memoState === 4 ) {
                            return this.getPath( '30673-02.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === HEAD_BLACKSMITH_KUSTO ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}