import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const GLYVKA = 30067
const ROUKE = 31418

const GIANTS_EXPERIMENTAL_TOOL_FRAGMENT = 10335
const GIANTS_EXPERIMENTAL_TOOL = 10336
const GIANTS_TECHNOLOGY_REPORT = 10337
const ROUKES_REPOT = 10338
const BADGE_TEMPLE_MISSIONARY = 10339

const CRUMA_MARSHLANDS_TRAITOR = 27339

const mobChanceMap = {
    20157: 78, // Marsh Stakato
    20229: 75, // Stinger Wasp
    20230: 86, // Marsh Stakato Worker
    20231: 83, // Toad Lord
    20232: 81, // Marsh Stakato Soldier
    20233: 95, // Marsh Spider
    20234: 96, // Marsh Stakato Drone
}

const minimumLevel = 35
const MAX_REWARD_LEVEL = 41
const FRAGMENT_COUNT = 10
const REPORT_COUNT = 3

const variableNames = {
    talk: 't'
}

export class TempleMissionary extends ListenerLogic {
    constructor() {
        super( 'Q00134_TempleMissionary', 'listeners/tracked/TempleMissionary.ts' )
        this.questId = 134
        this.questItemIds = [
            GIANTS_EXPERIMENTAL_TOOL_FRAGMENT,
            GIANTS_EXPERIMENTAL_TOOL,
            GIANTS_TECHNOLOGY_REPORT,
            ROUKES_REPOT,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ..._.keys( mobChanceMap ).map( value => _.parseInt( value ) ),
            CRUMA_MARSHLANDS_TRAITOR,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00134_TempleMissionary'
    }

    getQuestStartIds(): Array<number> {
        return [ GLYVKA ]
    }

    getTalkIds(): Array<number> {
        return [ GLYVKA, ROUKE ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 3 )
        if ( !player ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )
        if ( data.npcId === CRUMA_MARSHLANDS_TRAITOR ) {
            await QuestHelper.rewardSingleQuestItem( player, GIANTS_TECHNOLOGY_REPORT, 1, data.isChampion )
            if ( QuestHelper.getQuestItemsCount( player, GIANTS_TECHNOLOGY_REPORT ) >= REPORT_COUNT ) {
                state.setConditionWithSound( 4, true )
                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

            return
        }

        if ( QuestHelper.hasQuestItem( player, GIANTS_EXPERIMENTAL_TOOL ) ) {
            await QuestHelper.takeSingleItem( player, GIANTS_EXPERIMENTAL_TOOL, 1 )
            if ( _.random( 100 ) !== 0 ) {
                let npc = L2World.getObjectById( data.targetId )
                QuestHelper.addGenericSpawn( null, CRUMA_MARSHLANDS_TRAITOR, npc.getX() + 20, npc.getY() + 20, npc.getZ(), npc.getHeading(), false, 60000 )
            }

            return
        }

        if ( _.random( 100 ) < QuestHelper.getAdjustedChance( GIANTS_EXPERIMENTAL_TOOL_FRAGMENT, mobChanceMap[ data.npcId ], data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, GIANTS_EXPERIMENTAL_TOOL_FRAGMENT, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30067-05.html':
            case '30067-09.html':
            case '31418-07.html':
                break

            case '30067-03.htm':
                state.startQuest()
                break

            case '30067-06.html':
                state.setConditionWithSound( 2, true )
                break

            case '31418-03.html':
                state.setConditionWithSound( 3, true )
                break

            case '31418-08.html':
                state.setConditionWithSound( 5, true )
                await QuestHelper.giveSingleItem( player, ROUKES_REPOT, 1 )
                state.unsetVariable( variableNames.talk )
                break

            case '30067-10.html':
                await QuestHelper.giveSingleItem( player, BADGE_TEMPLE_MISSIONARY, 1 )
                await QuestHelper.giveAdena( player, 15100, true )

                if ( player.getLevel() < MAX_REWARD_LEVEL ) {
                    await QuestHelper.addExpAndSp( player, 30000, 2000 )
                }

                await state.exitQuest( false, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case GLYVKA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30067-01.htm' : '30067-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30067-04.html' )

                            case 2:
                            case 3:
                            case 4:
                                return this.getPath( '30067-07.html' )

                            case 5:
                                if ( state.getVariable( variableNames.talk ) ) {
                                    return this.getPath( '30067-09.html' )
                                }

                                await QuestHelper.takeSingleItem( player, ROUKES_REPOT, -1 )
                                state.setVariable( variableNames.talk, 1 )

                                return this.getPath( '30067-08.html' )
                        }
                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case ROUKE:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '31418-01.html' )

                    case 2:
                        return this.getPath( '31418-02.html' )

                    case 3:
                        let fragmentCount: number = QuestHelper.getQuestItemsCount( player, GIANTS_EXPERIMENTAL_TOOL_FRAGMENT )
                        if ( fragmentCount < FRAGMENT_COUNT
                                && QuestHelper.getQuestItemsCount( player, GIANTS_TECHNOLOGY_REPORT ) < REPORT_COUNT ) {
                            return this.getPath( '31418-04.html' )
                        }

                        if ( fragmentCount >= FRAGMENT_COUNT ) {
                            let count = Math.floor( QuestHelper.getQuestItemsCount( player, GIANTS_EXPERIMENTAL_TOOL_FRAGMENT ) / 10 )
                            await QuestHelper.takeSingleItem( player, GIANTS_EXPERIMENTAL_TOOL_FRAGMENT, count * 10 )
                            await QuestHelper.giveSingleItem( player, GIANTS_EXPERIMENTAL_TOOL, count )

                            return this.getPath( '31418-05.html' )
                        }

                        break

                    case 4:
                        if ( state.getVariable( variableNames.talk ) ) {
                            return this.getPath( '31418-07.html' )
                        }

                        if ( QuestHelper.getQuestItemsCount( player, GIANTS_TECHNOLOGY_REPORT ) >= REPORT_COUNT ) {
                            await QuestHelper.takeMultipleItems( player, -1, GIANTS_EXPERIMENTAL_TOOL_FRAGMENT, GIANTS_EXPERIMENTAL_TOOL, GIANTS_TECHNOLOGY_REPORT )

                            state.setVariable( variableNames.talk, 1 )

                            return this.getPath( '31418-06.html' )
                        }

                        break

                    case 5:
                        return this.getPath( '31418-09.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}