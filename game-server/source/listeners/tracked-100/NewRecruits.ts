import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const KEKROPUS = 32138
const MENACING_MACHINE = 32258

const minimumLevel = 17
const maximumLevel = 21

const RED_CRESCENT_EARRING = 10122
const RING_OF_DEVOTION = 10124

export class NewRecruits extends ListenerLogic {
    constructor() {
        super( 'Q00182_NewRecruits', 'listeners/tracked/NewRecruits.ts' )
        this.questId = 182
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00182_NewRecruits'
    }

    getQuestStartIds(): Array<number> {
        return [ KEKROPUS ]
    }

    getTalkIds(): Array<number> {
        return [ KEKROPUS, MENACING_MACHINE ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32138-03.htm':
                if ( player.getLevel() >= minimumLevel
                        && player.getLevel() <= maximumLevel
                        && player.isInCategory( CategoryType.FIRST_CLASS_GROUP ) ) {
                    break
                }

                return

            case '32138-04.htm':
                if ( player.getLevel() >= minimumLevel
                        && player.getLevel() <= maximumLevel
                        && player.isInCategory( CategoryType.FIRST_CLASS_GROUP ) ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    break
                }

                return

            case '32258-02.html':
            case '32258-03.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '32258-04.html':
                if ( state.isMemoState( 1 ) ) {
                    await QuestHelper.rewardSingleItem( player, RED_CRESCENT_EARRING, 2 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '32258-05.html':
                if ( state.isMemoState( 1 ) ) {
                    await QuestHelper.rewardSingleItem( player, RING_OF_DEVOTION, 2 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() === Race.KAMAEL ) {
                    return this.getPath( '32138-01.htm' )
                }

                if ( player.getLevel() >= minimumLevel
                        && player.getLevel() <= maximumLevel
                        && player.isInCategory( CategoryType.FIRST_CLASS_GROUP ) ) {
                    return this.getPath( '32138-02.htm' )
                }

                return this.getPath( '32138-05.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case KEKROPUS:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32138-06.html' )
                        }

                        break

                    case MENACING_MACHINE:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32258-01.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === KEKROPUS ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}