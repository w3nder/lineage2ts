import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2MonsterInstance } from '../../gameService/models/actor/instance/L2MonsterInstance'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const SHILENS_EVIL_THOUGHTS = 27396
const ORVEN = 30857
const WOOD = 32593
const LEOPARD = 32594
const LAWRENCE = 32595
const SOPHIA = 32596

const MYSTERIOUS_HAND_WRITTEN_TEXT = 13829
const SCULPTURE_OF_DOUBT = 14354

const minimumLevel = 79
const eventNames = {
    despawn: 'despawn',
}

export class SevenSignsTheSacredBookOfSeal extends ListenerLogic {
    isBusy: boolean = false

    constructor() {
        super( 'Q00197_SevenSignsTheSacredBookOfSeal', 'listeners/tracked/SevenSignsTheSacredBookOfSeal.ts' )
        this.questId = 197
        this.questItemIds = [ MYSTERIOUS_HAND_WRITTEN_TEXT, SCULPTURE_OF_DOUBT ]
    }

    getAttackableKillIds(): Array<number> {
        return [ SHILENS_EVIL_THOUGHTS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00197_SevenSignsTheSacredBookOfSeal'
    }

    getQuestStartIds(): Array<number> {
        return [ WOOD ]
    }

    getTalkIds(): Array<number> {
        return [ WOOD, ORVEN, LEOPARD, LAWRENCE, SOPHIA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 3 )
        if ( !player ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        if ( npc.isInsideRadius( player, 1500, true ) ) {
            await QuestHelper.giveSingleItem( player, SCULPTURE_OF_DOUBT, 1 )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_FINISH )
            state.setConditionWithSound( 4 )
        }

        this.isBusy = false
        this.stopQuestTimers( eventNames.despawn )

        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.S1_YOU_MAY_HAVE_WON_THIS_TIME_BUT_NEXT_TIME_I_WILL_SURELY_CAPTURE_YOU, player.getName() )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.characterNpcId === SHILENS_EVIL_THOUGHTS && data.eventName === eventNames.despawn ) {

            let npc = L2World.getObjectById( data.characterId ) as L2Npc

            if ( !npc.isDead() ) {
                this.isBusy = false
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_ANCIENT_PROMISE_TO_THE_EMPEROR_HAS_BEEN_FULFILLED )
                await npc.deleteMe()
            }

            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32593-02.htm':
            case '32593-03.htm':
                break

            case '32593-04.html':
                state.startQuest()
                break

            case '32593-08.html':
                if ( state.isCondition( 6 ) && QuestHelper.hasQuestItems( player, MYSTERIOUS_HAND_WRITTEN_TEXT, SCULPTURE_OF_DOUBT ) ) {
                    break
                }

                return

            case '32593-09.html':
                if ( state.isCondition( 6 ) ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        await QuestHelper.addExpAndSp( player, 52518015, 5817677 )
                        await state.exitQuest( false, true )

                        break
                    }

                    return this.getPath( 'level_check.html' )
                }
                return

            case '30857-02.html':
            case '30857-03.html':
                if ( state.isCondition( 1 ) ) {
                    break
                }

                return

            case '30857-04.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '32594-02.html':
                if ( state.isCondition( 2 ) ) {
                    break
                }

                return

            case '32594-03.html':
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '32595-02.html':
            case '32595-03.html':
                if ( state.isCondition( 3 ) ) {
                    break
                }

                return

            case '32595-04.html':
                if ( state.isCondition( 3 ) ) {
                    this.isBusy = true

                    let npc = L2World.getObjectById( data.characterId ) as L2Npc
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.S1_THAT_STRANGER_MUST_BE_DEFEATED_HERE_IS_THE_ULTIMATE_HELP, player.getName() )

                    let monster: L2MonsterInstance = QuestHelper.addGenericSpawn( null, SHILENS_EVIL_THOUGHTS, 152520, -57502, -3408, 0, false, 0, false ) as L2MonsterInstance
                    BroadcastHelper.broadcastNpcSayStringId( monster, NpcSayType.NpcAll, NpcStringIds.YOU_ARE_NOT_THE_OWNER_OF_THAT_ITEM )

                    monster.setRunning()

                    AIEffectHelper.notifyAttackedWithTargetId( monster, data.playerId, 0, 999 )

                    this.startQuestTimer( eventNames.despawn, 300000, monster.getObjectId(), 0 )
                }

                return

            case '32595-06.html':
            case '32595-07.html':
            case '32595-08.html':
                if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT ) ) {
                    break
                }

                return

            case '32595-09.html':
                if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT ) ) {
                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '32596-02.html':
            case '32596-03.html':
                if ( state.isCondition( 5 ) && QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT ) ) {
                    break
                }

                return

            case '32596-04.html':
                if ( state.isCondition( 5 ) && QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT ) ) {
                    await QuestHelper.giveSingleItem( player, MYSTERIOUS_HAND_WRITTEN_TEXT, 1 )
                    state.setConditionWithSound( 6, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === WOOD ) {
                    let shouldProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00196_SevenSignsSealOfTheEmperor' )
                    return this.getPath( shouldProceed ? '32593-01.htm' : '32593-05.html' )
                }
                break

            case QuestStateValues.STARTED:
                let condition = state.getCondition()

                switch ( data.characterNpcId ) {
                    case WOOD:
                        if ( condition > 0 && condition < 6 ) {
                            return this.getPath( '32593-06.html' )
                        }

                        if ( condition === 6 && QuestHelper.hasQuestItems( player, MYSTERIOUS_HAND_WRITTEN_TEXT, SCULPTURE_OF_DOUBT ) ) {
                            return this.getPath( '32593-07.html' )
                        }

                        break

                    case ORVEN:
                        if ( condition === 1 ) {
                            return this.getPath( '30857-01.html' )
                        }

                        if ( condition >= 2 ) {
                            return this.getPath( '30857-05.html' )
                        }

                        break

                    case LEOPARD:
                        if ( condition === 2 ) {
                            return this.getPath( '32594-01.html' )
                        }

                        if ( condition >= 3 ) {
                            return this.getPath( '32594-04.html' )
                        }

                        break

                    case LAWRENCE:
                        if ( condition === 3 ) {
                            return this.getPath( this.isBusy ? '32595-05.html' : '32595-01.html' )
                        }

                        let hasSculpture: boolean = QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT )
                        if ( condition === 4 && hasSculpture ) {
                            return this.getPath( '32595-06.html' )
                        }

                        if ( condition >= 5 && hasSculpture ) {
                            return this.getPath( '32595-10.html' )
                        }

                        break

                    case SOPHIA:
                        if ( condition === 5 && QuestHelper.hasQuestItem( player, SCULPTURE_OF_DOUBT ) ) {
                            return this.getPath( '32596-01.html' )
                        }

                        if ( condition >= 6 && QuestHelper.hasQuestItems( player, SCULPTURE_OF_DOUBT, MYSTERIOUS_HAND_WRITTEN_TEXT ) ) {
                            return this.getPath( '32596-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()

        }

        return QuestHelper.getNoQuestMessagePath()
    }
}