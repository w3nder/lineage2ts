import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcSpawnEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const TRADER_ARODIN = 30207
const GUARD_ALVAH = 30381
const TYRA = 30420
const NETI = 30425
const TRADER_ROLENTO = 30437
const TUREK_CHIEF_BURAI = 30617

const BLADE_MOLD = 4239
const TYRAS_BILL = 4240
const RANGERS_REPORT1 = 4241
const RANGERS_REPORT2 = 4242
const RANGERS_REPORT3 = 4243
const RANGERS_REPORT4 = 4244
const WEAPONS_TRADE_CONTRACT = 4245
const ATTACK_DIRECTIVES = 4246
const CERTIFICATE_OF_THE_SILVER_GUILD = 4247
const ROLENTOS_CARGOBOX = 4248
const OL_MAHUM_CAPTAINS_HEAD = 4249

const TUMRAN_BUGBEAR = 20062
const TUMRAN_BUGBEAR_WARRIOR = 20064
const OL_MAHUM_CAPTAIN = 20066
const OL_MAHUM_GENERAL = 20438
const TUREK_ORC_ARCHER = 20496
const TUREK_ORC_SKIRMISHER = 20497
const TUREK_ORC_SUPPLIER = 20498
const TUREK_ORC_FOOTMAN = 20499

const OL_MAHUM_SUPPORT_TROOP = 27190

const minimumLevel = 27

export class ActsOfEvil extends ListenerLogic {
    constructor() {
        super( 'Q00171_ActsOfEvil', 'listeners/tracked/ActsOfEvil.ts' )
        this.questId = 171
        this.questItemIds = [
            BLADE_MOLD,
            TYRAS_BILL,
            RANGERS_REPORT1,
            RANGERS_REPORT2,
            RANGERS_REPORT3,
            RANGERS_REPORT4,
            WEAPONS_TRADE_CONTRACT,
            ATTACK_DIRECTIVES,
            CERTIFICATE_OF_THE_SILVER_GUILD,
            ROLENTOS_CARGOBOX,
            OL_MAHUM_CAPTAINS_HEAD,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            TUMRAN_BUGBEAR,
            TUMRAN_BUGBEAR_WARRIOR,
            OL_MAHUM_CAPTAIN,
            OL_MAHUM_GENERAL,
            TUREK_ORC_ARCHER,
            TUREK_ORC_SKIRMISHER,
            TUREK_ORC_SUPPLIER,
            TUREK_ORC_FOOTMAN,
            OL_MAHUM_SUPPORT_TROOP,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00171_ActsOfEvil'
    }

    getQuestStartIds(): Array<number> {
        return [ GUARD_ALVAH ]
    }

    getSpawnIds(): Array<number> {
        return [ OL_MAHUM_SUPPORT_TROOP ]
    }

    getTalkIds(): Array<number> {
        return [ GUARD_ALVAH, TRADER_ARODIN, TYRA, NETI, TRADER_ROLENTO, TUREK_CHIEF_BURAI ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId )
        let player = L2World.getPlayer( data.playerId )

        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case TUMRAN_BUGBEAR:
            case TUMRAN_BUGBEAR_WARRIOR:
                if ( state.isMemoState( 5 ) ) {
                    let hasReportOne = QuestHelper.hasQuestItem( player, RANGERS_REPORT1 )
                    if ( !hasReportOne ) {
                        await QuestHelper.giveSingleItem( player, RANGERS_REPORT1, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                        return
                    }

                    let hasReportTwo = QuestHelper.hasQuestItem( player, RANGERS_REPORT2 )
                    if ( hasReportOne
                            && !hasReportTwo
                            && _.random( 100 ) <= QuestHelper.getAdjustedChance( RANGERS_REPORT2, 19, data.isChampion ) ) {

                        await QuestHelper.giveSingleItem( player, RANGERS_REPORT2, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                        return
                    }

                    let hasReportThree = QuestHelper.hasQuestItem( player, RANGERS_REPORT3 )
                    if ( hasReportOne
                            && hasReportTwo
                            && !hasReportThree
                            && _.random( 100 ) <= QuestHelper.getAdjustedChance( RANGERS_REPORT3, 19, data.isChampion ) ) {

                        await QuestHelper.giveSingleItem( player, RANGERS_REPORT3, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                        return
                    }

                    let hasReportFour = QuestHelper.hasQuestItem( player, RANGERS_REPORT4 )
                    if ( hasReportOne
                            && hasReportTwo
                            && hasReportThree
                            && !hasReportFour
                            && _.random( 100 ) <= QuestHelper.getAdjustedChance( RANGERS_REPORT4, 19, data.isChampion ) ) {

                        await QuestHelper.giveSingleItem( player, RANGERS_REPORT4, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                        return
                    }
                }

                break

            case OL_MAHUM_CAPTAIN:
                if ( state.isMemoState( 10 )
                        && QuestHelper.getQuestItemsCount( player, OL_MAHUM_CAPTAINS_HEAD ) < 30
                        && _.random( 100 ) <= QuestHelper.getAdjustedChance( OL_MAHUM_CAPTAINS_HEAD, 49, data.isChampion ) ) {

                    await QuestHelper.rewardSingleQuestItem( player, OL_MAHUM_CAPTAINS_HEAD, 1, data.isChampion )
                    if ( QuestHelper.getQuestItemsCount( player, OL_MAHUM_CAPTAINS_HEAD ) >= 30 ) {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                break

            case OL_MAHUM_GENERAL:
                if ( state.isMemoState( 6 )
                        && _.random( 100 ) <= QuestHelper.getAdjustedChance( WEAPONS_TRADE_CONTRACT, 9, data.isChampion ) ) {

                    if ( !QuestHelper.hasQuestItems( player, WEAPONS_TRADE_CONTRACT ) ) {
                        await QuestHelper.giveSingleItem( player, WEAPONS_TRADE_CONTRACT, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                        return
                    }

                    if ( !QuestHelper.hasQuestItems( player, ATTACK_DIRECTIVES ) ) {
                        await QuestHelper.giveSingleItem( player, ATTACK_DIRECTIVES, 1 )

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                        return
                    }
                }

                break

            case TUREK_ORC_ARCHER:
                await this.onOrcKilled( state, player, 100, 53, data )
                return

            case TUREK_ORC_SKIRMISHER:
                await this.onOrcKilled( state, player, 100, 55, data )
                return

            case TUREK_ORC_SUPPLIER:
                await this.onOrcKilled( state, player, 100, 51, data )
                return

            case TUREK_ORC_FOOTMAN:
                await this.onOrcKilled( state, player, 100, 50, data )
                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === 'despawn' ) {
            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            if ( npc ) {
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.YOU_SHOULD_CONSIDER_GOING_BACK )
                await npc.deleteMe()
            }

            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30381-03.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    break
                }

                break

            case '30381-07.html':
                state.setMemoState( 5 )
                state.setConditionWithSound( 5, true )
                break

            case '30381-12.html':
                state.setMemoState( 7 )
                state.setConditionWithSound( 7, true )

                break

            case '30437-04.html':
                await QuestHelper.takeSingleItem( player, WEAPONS_TRADE_CONTRACT, 1 )
                await QuestHelper.giveSingleItem( player, CERTIFICATE_OF_THE_SILVER_GUILD, 1 )
                await QuestHelper.giveSingleItem( player, ROLENTOS_CARGOBOX, 1 )

                state.setMemoState( 9 )
                state.setConditionWithSound( 9, true )

                break
            case '30207-01a.html':
            case '30437-02.html':
            case '30437-03.html':
            case '30617-03.html':
            case '30617-04.html':
                break

            case '30617-05.html':
                await QuestHelper.takeMultipleItems( player, 1, ATTACK_DIRECTIVES, CERTIFICATE_OF_THE_SILVER_GUILD, ROLENTOS_CARGOBOX )

                state.setMemoState( 10 )
                state.setConditionWithSound( 10, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        this.startQuestTimer( 'despawn', 200000, data.characterId, 0, false )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === GUARD_ALVAH ) {
                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30381-01.htm' )
                    }

                    return this.getPath( '30381-02.htm' )
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case GUARD_ALVAH:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30381-04.html' )

                            case 2:
                            case 3:
                                return this.getPath( '30381-05.html' )

                            case 4:
                                return this.getPath( '30381-06.html' )

                            case 5:
                                if ( QuestHelper.hasQuestItems( player, RANGERS_REPORT1, RANGERS_REPORT2, RANGERS_REPORT3, RANGERS_REPORT4 ) ) {
                                    await QuestHelper.takeMultipleItems( player, 1, RANGERS_REPORT1, RANGERS_REPORT2, RANGERS_REPORT3, RANGERS_REPORT4 )

                                    state.setMemoState( 6 )
                                    state.setConditionWithSound( 6, true )

                                    return this.getPath( '30381-09.html' )
                                }

                                return this.getPath( '30381-08.html' )

                            case 6:
                                if ( QuestHelper.hasQuestItems( player, WEAPONS_TRADE_CONTRACT, ATTACK_DIRECTIVES ) ) {
                                    return this.getPath( '30381-11.html' )
                                }

                                return this.getPath( '30381-10.html' )

                            case 7:
                                return this.getPath( '30381-13.html' )

                            case 8:
                                return this.getPath( '30381-14.html' )

                            case 9:
                                return this.getPath( '30381-15.html' )

                            case 10:
                                return this.getPath( '30381-16.html' )

                            case 11:
                                await QuestHelper.giveAdena( player, 95000, true )
                                await QuestHelper.addExpAndSp( player, 159820, 9182 )
                                await state.exitQuest( false, true )

                                return this.getPath( '30381-17.html' )
                        }

                        break

                    case TRADER_ARODIN:
                        switch ( memoState ) {
                            case 1:
                                state.setMemoState( 2 )
                                state.setConditionWithSound( 2, true )

                                return this.getPath( '30207-01.html' )

                            case 2:
                                if ( QuestHelper.getQuestItemsCount( player, BLADE_MOLD ) < 20 ) {
                                    return this.getPath( '30207-02.html' )
                                }

                                return this.getPath( '30207-03.html' )

                            case 3:
                                await QuestHelper.takeSingleItem( player, TYRAS_BILL, 1 )

                                state.setMemoState( 4 )
                                state.setConditionWithSound( 4, true )

                                return this.getPath( '30207-04.html' )

                            default:
                                return this.getPath( '30207-05.html' )
                        }

                        break

                    case TYRA:
                        switch ( memoState ) {
                            case 2:
                                if ( QuestHelper.getQuestItemsCount( player, BLADE_MOLD ) < 20 ) {
                                    return this.getPath( '30420-01.html' )
                                }

                                await QuestHelper.takeSingleItem( player, BLADE_MOLD, -1 )
                                await QuestHelper.giveSingleItem( player, TYRAS_BILL, 1 )

                                state.setMemoState( 3 )
                                state.setConditionWithSound( 3, true )

                                return this.getPath( '30420-02.html' )

                            case 3:
                                return this.getPath( '30420-03.html' )
                        }

                        if ( memoState >= 4 ) {
                            return this.getPath( '30420-04.html' )
                        }

                        break

                    case NETI:
                        switch ( memoState ) {
                            case 7:
                                state.setMemoState( 8 )
                                state.setConditionWithSound( 8, true )

                                return this.getPath( '30425-01.html' )

                            case 8:
                                return this.getPath( '30425-02.html' )
                        }

                        if ( memoState >= 9 ) {
                            return this.getPath( '30425-03.html' )
                        }

                        break

                    case TRADER_ROLENTO:
                        switch ( memoState ) {
                            case 8:
                                return this.getPath( '30437-02.html' )

                            case 9:
                                return this.getPath( '30437-05.html' )
                        }

                        if ( memoState >= 10 ) {
                            return this.getPath( '30437-06.html' )
                        }

                        break

                    case TUREK_CHIEF_BURAI:

                        if ( memoState < 9 ) {
                            return this.getPath( '30617-01.html' )
                        }

                        switch ( memoState ) {
                            case 9:
                                return this.getPath( '30617-02.html' )

                            case 10:
                                if ( QuestHelper.getQuestItemsCount( player, OL_MAHUM_CAPTAINS_HEAD ) < 30 ) {
                                    return this.getPath( '30617-06.html' )
                                }

                                await QuestHelper.takeSingleItem( player, OL_MAHUM_CAPTAINS_HEAD, -1 )
                                await QuestHelper.giveAdena( player, 8000, true )

                                state.setMemoState( 11 )
                                state.setConditionWithSound( 11, true )

                                return this.getPath( '30617-07.html' )

                            case 11:
                                return this.getPath( '30617-08.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === GUARD_ALVAH ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onOrcKilled( state: QuestState, player: L2PcInstance, chanceRange: number, chanceTarget: number, data: AttackableKillEvent ): Promise<void> {
        if ( state.isMemoState( 2 )
                && QuestHelper.getQuestItemsCount( player, BLADE_MOLD ) < 20 ) {
            if ( _.random( chanceRange ) < QuestHelper.getAdjustedChance( BLADE_MOLD, chanceTarget, data.isChampion ) ) {
                await QuestHelper.rewardSingleQuestItem( player, BLADE_MOLD, 1, data.isChampion )

                if ( QuestHelper.getQuestItemsCount( player, BLADE_MOLD ) >= 20 ) {
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    return
                }

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            }

            let updatedCount = QuestHelper.getQuestItemsCount( player, BLADE_MOLD )
            if ( updatedCount === 5
                    || ( updatedCount >= 10
                            && _.random( 100 ) <= QuestHelper.getAdjustedChance( BLADE_MOLD, 24, data.isChampion ) ) ) {
                let npc = L2World.getObjectById( data.targetId )
                let troopNpc = QuestHelper.addSpawnAtLocation( OL_MAHUM_SUPPORT_TROOP, npc, true, 0 )
                QuestHelper.addAttackDesire( troopNpc, player )
            }
        }
    }
}