import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const HEAD_BLACKSMITH_NEWYEAR = 31961

const CRYSTAL_D = 1458
const BRUIN_LIZARDMAN_BLOOD = 8549
const PICOT_ARANEIDS_LEG = 8550

const CLAN_OATH_HELM = 7850
const CLAN_OATH_ARMOR = 7851
const CLAN_OATH_GAUNTLETS_HEAVY_ARMOR = 7852
const CLAN_OATH_SABATON_HEAVY_ARMOR = 7853
const CLAN_OATH_BRIGANDINE = 7854
const CLAN_OATH_LEATHER_GLOVES_LIGHT_ARMOR = 7855
const CLAN_OATH_BOOTS_LIGHT_ARMOR = 7856
const CLAN_OATH_AKETON = 7857
const CLAN_OATH_PADDED_GLOVES_ROBE = 7858
const CLAN_OATH_SANDALS_ROBE = 7859

const BRUIN_LIZARDMAN = 27321
const PICOT_ARANEID = 27322

const minimumLevel = 19
const CRYSTAL_COUNT_1 = 922
const CRYSTAL_COUNT_2 = 771

export class TheLeaderAndTheFollower extends ListenerLogic {
    constructor() {
        super( 'Q00123_TheLeaderAndTheFollower', 'listeners/tracked/TheLeaderAndTheFollower.ts' )
        this.questId = 123
        this.questItemIds = [ BRUIN_LIZARDMAN_BLOOD, PICOT_ARANEIDS_LEG ]
    }

    async checkForApprentice( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        let apprentice: L2PcInstance = L2World.getPlayer( player.getApprentice() )
        if ( !apprentice ) {
            return
        }

        let currentQuestState: QuestState = QuestStateCache.getQuestState( player.getApprentice(), 'Q00123_TheLeaderAndTheFollower' )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case 'sponsor':
                if ( !GeneralHelper.checkIfInRange( 1500, npc, apprentice, true ) ) {
                    return this.getPath( '31961-09.html' )
                }

                if ( !currentQuestState || ( !currentQuestState.isMemoState( 2 ) && !currentQuestState.isMemoState( 3 ) ) ) {
                    return this.getPath( '31961-14.html' )
                }

                if ( currentQuestState.isMemoState( 2 ) ) {
                    return this.getPath( '31961-08.html' )
                }

                if ( currentQuestState.isMemoState( 3 ) ) {
                    return this.getPath( '31961-12.html' )
                }

                return

            case '31961-10.html':
                if ( GeneralHelper.checkIfInRange( 1500, npc, apprentice, true )
                        && currentQuestState
                        && currentQuestState.isMemoState( 2 ) ) {

                    switch ( currentQuestState.getMemoStateEx( 1 ) ) {
                        case 1:
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_D ) >= CRYSTAL_COUNT_1 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_D, CRYSTAL_COUNT_1 )

                                currentQuestState.setMemoState( 3 )
                                currentQuestState.setConditionWithSound( 6, true )

                                break
                            }

                            return this.getPath( '31961-11.html' )

                        case 2:
                        case 3:
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_D ) >= CRYSTAL_COUNT_2 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_D, CRYSTAL_COUNT_2 )

                                currentQuestState.setMemoState( 3 )
                                currentQuestState.setConditionWithSound( 6, true )

                                break
                            }

                            return this.getPath( '31961-11a.html' )
                    }
                }

                return
        }
    }

    getAttackableKillIds(): Array<number> {
        return [ BRUIN_LIZARDMAN, PICOT_ARANEID ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00123_TheLeaderAndTheFollower'
    }

    getQuestStartIds(): Array<number> {
        return [ HEAD_BLACKSMITH_NEWYEAR ]
    }

    getTalkIds(): Array<number> {
        return [ HEAD_BLACKSMITH_NEWYEAR ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        switch ( npc.getId() ) {
            case BRUIN_LIZARDMAN:
                if ( state.isMemoState( 1 ) && Math.random() < QuestHelper.getAdjustedChance( BRUIN_LIZARDMAN_BLOOD, 0.7, data.isChampion ) ) {
                    await QuestHelper.rewardAndProgressState( player, state, BRUIN_LIZARDMAN_BLOOD, 1, 10, data.isChampion, 2 )
                }

                return

            case PICOT_ARANEID:
                if ( state.isMemoState( 4 ) && player.getSponsor() > 0 ) {

                    let sponsor: L2PcInstance = L2World.getPlayer( player.getSponsor() )
                    if ( sponsor
                            && Math.random() < QuestHelper.getAdjustedChance( PICOT_ARANEIDS_LEG, 0.7, data.isChampion )
                            && GeneralHelper.checkIfInRange( 1500, npc, sponsor, true ) ) {
                        await QuestHelper.rewardAndProgressState( player, state, PICOT_ARANEIDS_LEG, 1, 8, data.isChampion, 8 )
                    }
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( [ 'sponsor', '31961-10.html' ].includes( data.eventName ) ) {
            return this.checkForApprentice( data )
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31961-03.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )
                    break
                }

                return

            case '31961-05a.html':
            case '31961-05b.html':
            case '31961-05c.html':
            case '31961-05g.html':
                break

            case '31961-05d.html':
                if ( state.isMemoState( 1 ) && QuestHelper.getQuestItemsCount( player, BRUIN_LIZARDMAN_BLOOD ) >= 10 ) {
                    await QuestHelper.takeSingleItem( player, BRUIN_LIZARDMAN_BLOOD, -1 )

                    state.setMemoState( 2 )
                    state.setMemoStateEx( 1, 1 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '31961-05e.html':
                if ( state.isMemoState( 1 ) && QuestHelper.getQuestItemsCount( player, BRUIN_LIZARDMAN_BLOOD ) >= 10 ) {
                    await QuestHelper.takeSingleItem( player, BRUIN_LIZARDMAN_BLOOD, -1 )

                    state.setMemoState( 2 )
                    state.setMemoStateEx( 1, 2 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '31961-05f.html':
                if ( state.isMemoState( 1 ) && QuestHelper.getQuestItemsCount( player, BRUIN_LIZARDMAN_BLOOD ) >= 10 ) {
                    await QuestHelper.takeSingleItem( player, BRUIN_LIZARDMAN_BLOOD, -1 )

                    state.setMemoState( 2 )
                    state.setMemoStateEx( 1, 3 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let otherState: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00118_ToLeadAndBeLed' )

        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( otherState && otherState.isStarted() ) {
                    return this.getPath( '31961-02b.htm' )
                }

                if ( otherState && otherState.isCompleted() ) {
                    return this.getPath( '31961-02a.html' )
                }

                if ( player.getLevel() >= minimumLevel
                        && player.getPledgeType() === -1
                        && player.getSponsor() > 0 ) {
                    return this.getPath( '31961-01.htm' )
                }

                return this.getPath( '31961-02.htm' )

            case QuestStateValues.STARTED:
                if ( state.isMemoState( 1 ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, BRUIN_LIZARDMAN_BLOOD ) < 10 ) {
                        return this.getPath( '31961-04.html' )
                    }

                    return this.getPath( '31961-05.html' )
                }

                if ( state.isMemoState( 2 ) ) {
                    if ( player.getSponsor() === 0 ) {
                        if ( state.getMemoStateEx( 1 ) === 1 ) {
                            return this.getPath( '31961-06a.html' )
                        }

                        if ( state.getMemoStateEx( 1 ) === 2 ) {
                            return this.getPath( '31961-06b.html' )
                        }

                        if ( state.getMemoStateEx( 1 ) === 3 ) {
                            return this.getPath( '31961-06c.html' )
                        }

                        break
                    }

                    let sponsor: L2PcInstance = L2World.getPlayer( player.getSponsor() )
                    let npc = L2World.getObjectById( data.characterId )
                    if ( sponsor && GeneralHelper.checkIfInRange( 1500, npc, sponsor, true ) ) {
                        return this.getPath( '31961-07.html' )
                    }

                    switch ( state.getMemoStateEx( 1 ) ) {
                        case 1:
                            return this.getPath( '31961-06.html' )

                        case 2:
                            return this.getPath( '31961-06d.html' )

                        case 3:
                            return this.getPath( '31961-06e.html' )
                    }

                    break

                }

                if ( state.isMemoState( 3 ) ) {
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 7, true )

                    return this.getPath( '31961-15.html' )
                }

                if ( state.isMemoState( 4 ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, PICOT_ARANEIDS_LEG ) < 8 ) {
                        return this.getPath( '31961-16.html' )
                    }

                    if ( state.getMemoStateEx( 1 ) === 1 ) {
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_HELM, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_ARMOR, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_GAUNTLETS_HEAVY_ARMOR, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_SABATON_HEAVY_ARMOR, 1 )
                        await QuestHelper.takeSingleItem( player, PICOT_ARANEIDS_LEG, -1 )
                    }

                    if ( state.getMemoStateEx( 1 ) === 2 ) {
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_HELM, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_BRIGANDINE, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_LEATHER_GLOVES_LIGHT_ARMOR, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_BOOTS_LIGHT_ARMOR, 1 )
                        await QuestHelper.takeSingleItem( player, PICOT_ARANEIDS_LEG, -1 )
                    }

                    if ( state.getMemoStateEx( 1 ) === 3 ) {
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_HELM, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_AKETON, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_PADDED_GLOVES_ROBE, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_SANDALS_ROBE, 1 )
                        await QuestHelper.takeSingleItem( player, PICOT_ARANEIDS_LEG, -1 )
                    }

                    await state.exitQuest( false, true )
                    return this.getPath( '31961-17.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}