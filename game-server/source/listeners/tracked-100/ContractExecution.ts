import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

import _ from 'lodash'

const MAESTRO_NIKOLA = 30621
const RESEARCHER_LORAIN = 30673
const BLUEPRINT_SELLER_LUKA = 31437

const mobDropChances = {
    20577: 40, // Leto Lizardman
    20578: 44, // Leto Lizardman Archer
    20579: 46, // Leto Lizardman Soldier
    20580: 88, // Leto Lizardman Warrior
    20581: 50, // Leto Lizardman Shaman
    20582: 100, // Leto Lizardman Overlord
}

const LORAINES_CERTIFICATE = 10362
const METALLOGRAPH_RESEARCH_REPORT = 10366
const LETO_LIZARDMAN_ACCESSORY = 10367

const minimumLevel = 41
const maximumLevel = 47

export class ContractExecution extends ListenerLogic {
    constructor() {
        super( 'Q00186_ContractExecution', 'listeners/tracked/ContractExecution.ts' )
        this.questId = 186
        this.questItemIds = [ METALLOGRAPH_RESEARCH_REPORT, LETO_LIZARDMAN_ACCESSORY ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( mobDropChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00186_ContractExecution'
    }

    getQuestStartIds(): Array<number> {
        return [ RESEARCHER_LORAIN ]
    }

    getTalkIds(): Array<number> {
        return [ RESEARCHER_LORAIN, BLUEPRINT_SELLER_LUKA, MAESTRO_NIKOLA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( state.isMemoState( 2 )
                && _.random( 100 ) < QuestHelper.getAdjustedChance( LETO_LIZARDMAN_ACCESSORY, mobDropChances[ data.npcId ], data.isChampion )
                && !QuestHelper.hasQuestItem( player, LETO_LIZARDMAN_ACCESSORY )
                && GeneralHelper.checkIfInRange( 1500, L2World.getObjectById( data.targetId ), player, false ) ) {

            await QuestHelper.giveSingleItem( player, LETO_LIZARDMAN_ACCESSORY, 1 )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30673-03.htm':
                if ( player.getLevel() >= minimumLevel && QuestHelper.hasQuestItem( player, LORAINES_CERTIFICATE ) ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveSingleItem( player, METALLOGRAPH_RESEARCH_REPORT, 1 )
                    await QuestHelper.takeSingleItem( player, LORAINES_CERTIFICATE, -1 )
                    break
                }

                return

            case '30621-02.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '30621-03.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '31437-03.html':
                if ( state.isMemoState( 2 ) && QuestHelper.hasQuestItem( player, LETO_LIZARDMAN_ACCESSORY ) ) {
                    break
                }

                return

            case '31437-04.html':
                if ( state.isMemoState( 2 ) && QuestHelper.hasQuestItems( player, LETO_LIZARDMAN_ACCESSORY ) ) {
                    state.setMemoState( 3 )

                    break
                }

                return

            case '31437-06.html':
                if ( state.isMemoState( 3 ) ) {
                    await QuestHelper.giveAdena( player, 105083, true )

                    if ( player.getLevel() < maximumLevel ) {
                        await QuestHelper.addExpAndSp( player, 285935, 18711 )
                    }

                    await state.exitQuest( false, true )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === RESEARCHER_LORAIN ) {
                    if ( player.hasQuestCompleted( 'Q00184_ArtOfPersuasion' ) && QuestHelper.hasQuestItem( player, LORAINES_CERTIFICATE ) ) {
                        return this.getPath( player.getLevel() >= minimumLevel ? '30673-01.htm' : '30673-02.htm' )
                    }
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case RESEARCHER_LORAIN:
                        if ( memoState >= 1 ) {
                            return this.getPath( '30673-04.html' )
                        }

                        break

                    case MAESTRO_NIKOLA:
                        if ( memoState === 1 ) {
                            return this.getPath( '30621-01.html' )
                        }

                        if ( memoState === 2 ) {
                            return this.getPath( '30621-04.html' )
                        }

                        break

                    case BLUEPRINT_SELLER_LUKA:
                        if ( memoState === 2 ) {
                            if ( QuestHelper.hasQuestItem( player, LETO_LIZARDMAN_ACCESSORY ) ) {
                                return this.getPath( '31437-02.html' )
                            }

                            return this.getPath( '31437-01.html' )
                        }

                        if ( memoState == 3 ) {
                            return this.getPath( '31437-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === RESEARCHER_LORAIN ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}