import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const RAYMOND = 30289
const CASIAN = 30612
const NATOOLS = 30894
const ROCK = 32368

const FALLEN_ANGEL = 27338
const mobChanceMap = {
    20079: 338, // Ant
    20080: 363, // Ant Captain
    20081: 611, // Ant Overseer
    20082: 371, // Ant Recruit
    20084: 421, // Ant Patrol
    20086: 371, // Ant Guard
    20087: 900, // Ant Soldier
    20088: 1000,// Ant Warrior Captain
    20089: 431, // Noble Ant
    20090: 917, // Noble Ant Leader
}

const CRYPTOGRAM_OF_THE_ANGEL_SEARCH = 10351
const PROPHECY_FRAGMENT = 10352
const FALLEN_ANGEL_BLOOD = 10353

const maximumLevel = 43
const FRAGMENT_COUNT = 30
let isAngelSpawned: boolean = false

const variableNames = {
    talk: 't'
}

export class FallenAngelRequestOfDawn extends ListenerLogic {
    constructor() {
        super( 'Q00142_FallenAngelRequestOfDawn', 'listeners/tracked/FallenAngelRequestOfDawn.ts' )
        this.questId = 142
        this.questItemIds = [
            CRYPTOGRAM_OF_THE_ANGEL_SEARCH,
            PROPHECY_FRAGMENT,
            FALLEN_ANGEL_BLOOD,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ..._.keys( mobChanceMap ).map( value => _.parseInt( value ) ),
            FALLEN_ANGEL,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00142_FallenAngelRequestOfDawn'
    }

    getTalkIds(): Array<number> {
        return [ NATOOLS, RAYMOND, CASIAN, ROCK ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( data.npcId === FALLEN_ANGEL ) {
            let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

            if ( state.isCondition( 5 ) ) {
                let player = L2World.getPlayer( data.playerId )
                await QuestHelper.giveSingleItem( player, FALLEN_ANGEL_BLOOD, 1 )
                state.setConditionWithSound( 6, true )
                isAngelSpawned = false
            }

            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 4 )
        if ( !player ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )

        if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( PROPHECY_FRAGMENT, mobChanceMap[ data.npcId ], data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, PROPHECY_FRAGMENT, 1, data.isChampion )

            if ( QuestHelper.getQuestItemsCount( player, PROPHECY_FRAGMENT ) >= FRAGMENT_COUNT ) {
                await QuestHelper.takeSingleItem( player, PROPHECY_FRAGMENT, -1 )
                state.setConditionWithSound( 5, true )
                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30894-02.html':
            case '30289-03.html':
            case '30289-04.html':
            case '30612-03.html':
            case '30612-04.html':
            case '30612-06.html':
            case '30612-07.html':
                break

            case '30894-01.html':
                state.startQuest()
                break

            case '30894-03.html':
                await QuestHelper.giveSingleItem( player, CRYPTOGRAM_OF_THE_ANGEL_SEARCH, 1 )
                state.setConditionWithSound( 2, true )
                break

            case '30289-05.html':
                state.unsetVariable( variableNames.talk )
                state.setConditionWithSound( 3, true )
                break

            case '30612-05.html':
                state.setVariable( variableNames.talk, 2 )
                break

            case '30612-08.html':
                state.unsetVariable( variableNames.talk )
                state.setConditionWithSound( 4, true )
                break

            case '32368-04.html':
                if ( isAngelSpawned ) {
                    return '32368-03.html'
                }

                let npc = L2World.getObjectById( data.characterId )
                QuestHelper.addGenericSpawn( null, FALLEN_ANGEL, npc.getX() + 100, npc.getY() + 100, npc.getZ(), 0, false, 120000 )
                isAngelSpawned = true
                this.startQuestTimer( 'despawn', 120000, null, data.playerId )
                break

            case 'despawn':
                if ( isAngelSpawned ) {
                    isAngelSpawned = false
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case NATOOLS:
                switch ( state.getState() ) {
                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30894-01.html' )
                        }

                        return this.getPath( '30894-04.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case RAYMOND:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30289-01.html' )

                    case 2:
                        if ( state.getVariable( variableNames.talk ) ) {
                            return this.getPath( '30289-03.html' )
                        }

                        await QuestHelper.takeSingleItem( player, CRYPTOGRAM_OF_THE_ANGEL_SEARCH, -1 )
                        state.setVariable( variableNames.talk, 1 )

                        return this.getPath( '30289-02.html' )

                    case 3:
                    case 4:
                    case 5:
                        return this.getPath( '30289-06.html' )

                    case 6:
                        await QuestHelper.giveAdena( player, 92676, true )
                        if ( player.getLevel() <= maximumLevel ) {
                            await QuestHelper.addExpAndSp( player, 223036, 13091 )
                        }

                        await state.exitQuest( false, true )
                        return this.getPath( '30289-07.html' )
                }

                break

            case CASIAN:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                    case 2:
                        return this.getPath( '30612-01.html' )

                    case 3:
                        if ( state.getVariable( variableNames.talk ) === 1 ) {
                            return this.getPath( '30612-03.html' )
                        }

                        if ( state.getVariable( variableNames.talk ) === 2 ) {
                            return this.getPath( '30612-06.html' )
                        }

                        state.setVariable( variableNames.talk, 1 )
                        return this.getPath( '30612-02.html' )

                    case 4:
                    case 5:
                    case 6:
                        return this.getPath( '30612-09.html' )
                }

                break

            case ROCK:
                if ( state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 5:
                        return this.getPath( '32368-02.html' )

                    case 6:
                        return this.getPath( '32368-05.html' )

                    default:
                        return this.getPath( '32368-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}