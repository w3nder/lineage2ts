import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { createItemDefinition } from '../../gameService/interface/ItemDefinition'

const FILAUR = 30535
const OBI = 32052

const THIEF_KEY = createItemDefinition( 1661, 10 )
const BANDAGE = createItemDefinition( 1833, 20 )
const ENERGY_STONE = createItemDefinition( 5589, 5 )
const SUPPLYING_GOODS = 8098

const SOULSHOT_D = 1463
const minimumLevel = 30

export class BeyondTheHillsOfWinter extends ListenerLogic {
    constructor() {
        super( 'Q00116_BeyondTheHillsOfWinter', 'listeners/tracked/BeyondTheHillsOfWinter.ts' )
        this.questId = 116
        this.questItemIds = [ SUPPLYING_GOODS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00116_BeyondTheHillsOfWinter'
    }

    getQuestStartIds(): Array<number> {
        return [ FILAUR ]
    }

    getTalkIds(): Array<number> {
        return [ FILAUR, OBI ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30535-02.htm':
                state.startQuest()
                state.setMemoState( 1 )
                break

            case '30535-05.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )
                    await QuestHelper.giveSingleItem( player, SUPPLYING_GOODS, 1 )

                    break
                }

                return

            case '32052-02.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case 'MATERIAL':
                if ( state.isMemoState( 2 ) ) {
                    await QuestHelper.rewardSingleItem( player, SOULSHOT_D, 1740 )
                    await QuestHelper.addExpAndSp( player, 82792, 4981 )
                    await state.exitQuest( false, true )

                    return this.getPath( '32052-03.html' )
                }

                return

            case 'ADENA':
                if ( state.isMemoState( 2 ) ) {
                    await QuestHelper.giveAdena( player, 17387, true )
                    await QuestHelper.addExpAndSp( player, 82792, 4981 )
                    await state.exitQuest( false, true )

                    return this.getPath( '32052-03.html' )
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === FILAUR ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30535-01.htm' : '30535-03.htm' )
                }

                return

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case FILAUR:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( QuestHelper.hasEachItem( player, [ THIEF_KEY, BANDAGE, ENERGY_STONE ] ) ? '30535-04.html' : '30535-06.html' )
                        }

                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '30535-07.html' )
                        }

                        break

                    case OBI:
                        if ( state.isMemoState( 2 ) && QuestHelper.hasQuestItem( player, SUPPLYING_GOODS ) ) {
                            return this.getPath( '32052-01.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === FILAUR ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}