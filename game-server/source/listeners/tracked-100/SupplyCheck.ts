import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { PacketHelper } from '../../gameService/packets/PacketVariables'
import aigle from 'aigle'

const NIKA = 32167
const BENIS = 32170
const MARCELA = 32173

const WAREHOUSE_MANIFEST = 9792
const GROCERY_STORE_MANIFEST = 9793

const rewards : Array<number> = [
    23, // Wooden Breastplate
    43, // Wooden Helmet
    49, // Gloves
    2386, // Wooden Gaiters
    37, // Leather Shoes
]

const minimumLevel = 2
const messageData : Buffer = PacketHelper.preservePacket( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.DELIVERY_DUTY_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000, null ) )

export class SupplyCheck extends ListenerLogic {
    constructor() {
        super( 'Q00174_SupplyCheck', 'listeners/tracked/SupplyCheck.ts' )
        this.questId = 174
        this.questItemIds = [ WAREHOUSE_MANIFEST, GROCERY_STORE_MANIFEST ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00174_SupplyCheck'
    }

    getQuestStartIds(): Array<number> {
        return [ MARCELA ]
    }

    getTalkIds(): Array<number> {
        return [ MARCELA, BENIS, NIKA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '32173-03.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case MARCELA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '32173-01.htm' : '32173-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '32173-04.html' )

                            case 2:
                                state.setConditionWithSound( 3, true )
                                await QuestHelper.takeSingleItem( player, WAREHOUSE_MANIFEST, -1 )
                                return this.getPath( '32173-05.html' )

                            case 3:
                                return this.getPath( '32173-06.html' )

                            case 4:
                                await aigle.resolve( rewards ).each( ( itemId: number ) : Promise<void> => {
                                    return QuestHelper.rewardSingleItem( player, itemId, 1 )
                                } )

                                await QuestHelper.giveAdena( player, 2466, true )
                                await QuestHelper.addExpAndSp( player, 5672, 446 )
                                await state.exitQuest( false, true )

                                player.sendCopyData( messageData )
                                return this.getPath( '32173-07.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case BENIS:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                            state.setConditionWithSound( 2, true )
                            await QuestHelper.giveSingleItem( player, WAREHOUSE_MANIFEST, 1 )

                            return this.getPath( '32170-01.html' )

                        case 2:
                            return this.getPath( '32170-02.html' )

                        default:
                            return this.getPath( '32170-03.html' )
                    }
                }

                break

            case NIKA:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                        case 2:
                            return this.getPath( '32167-01.html' )

                        case 3:
                            state.setConditionWithSound( 4, true )
                            await QuestHelper.giveSingleItem( player, GROCERY_STORE_MANIFEST, 1 )

                            return this.getPath( '32167-02.html' )

                        case 4:
                            return this.getPath( '32167-03.html' )
                    }
                }
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}