import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const BLACKSMITH_PINTER = 30298

const CRYSTAL_D = 1458
const BLOOD_OF_MAILLE_LIZARDMAN = 8062
const LEG_OF_KING_ARANEID = 8063

const CLAN_OATH_HELM = 7850
const CLAN_OATH_ARMOR = 7851
const CLAN_OATH_GAUNTLETS_HEAVY_ARMOR = 7852
const CLAN_OATH_SABATON_HEAVY_ARMOR = 7853
const CLAN_OATH_BRIGANDINE = 7854
const CLAN_OATH_LEATHER_GLOVES_LIGHT_ARMOR = 7855
const CLAN_OATH_BOOTS_LIGHT_ARMOR = 7856
const CLAN_OATH_AKETON = 7857
const CLAN_OATH_PADDED_GLOVES_ROBE = 7858
const CLAN_OATH_SANDALS_ROBE = 7859

const MAILLE_LIZARDMAN = 20919
const MAILLE_LIZARDMAN_SCOUT = 20920
const MAILLE_LIZARDMAN_GUARD = 20921
const KING_OF_THE_ARANEID = 20927

const minimumLevel = 19
const CRYSTAL_COUNT_1 = 922
const CRYSTAL_COUNT_2 = 771

export class ToLeadAndBeLed extends ListenerLogic {
    constructor() {
        super( 'Q00118_ToLeadAndBeLed', 'listeners/tracked/ToLeadAndBeLed.ts' )
        this.questId = 118
        this.questItemIds = [ LEG_OF_KING_ARANEID, BLOOD_OF_MAILLE_LIZARDMAN ]
    }

    async checkForApprentice( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        let apprentice: L2PcInstance = L2World.getPlayer( player.getApprentice() )
        if ( !apprentice ) {
            return
        }

        let currentQuestState: QuestState = QuestStateCache.getQuestState( player.getApprentice(), 'Q00118_ToLeadAndBeLed' )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        switch ( data.eventName ) {
            case 'sponsor':
                if ( !GeneralHelper.checkIfInRange( 1500, npc, apprentice, true ) ) {
                    return this.getPath( '30298-09.html' )
                }

                if ( !currentQuestState || ( !currentQuestState.isMemoState( 2 ) && !currentQuestState.isMemoState( 3 ) ) ) {
                    return this.getPath( '30298-14.html' )
                }

                if ( currentQuestState.isMemoState( 2 ) ) {
                    return this.getPath( '30298-08.html' )
                }

                if ( currentQuestState.isMemoState( 3 ) ) {
                    return this.getPath( '30298-12.html' )
                }

                return

            case '30298-10.html':
                if ( GeneralHelper.checkIfInRange( 1500, npc, apprentice, true ) && currentQuestState && currentQuestState.isMemoState( 2 ) ) {
                    switch ( currentQuestState.getMemoStateEx( 1 ) ) {
                        case 1:
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_D ) >= CRYSTAL_COUNT_1 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_D, CRYSTAL_COUNT_1 )
                                currentQuestState.setMemoState( 3 )
                                currentQuestState.setConditionWithSound( 6, true )

                                return this.getPath( data.eventName )
                            }

                            return this.getPath( '30298-11.html' )

                        case 2:
                        case 3:
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_D ) >= CRYSTAL_COUNT_2 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_D, CRYSTAL_COUNT_2 )
                                currentQuestState.setMemoState( 3 )
                                currentQuestState.setConditionWithSound( 6, true )

                                return this.getPath( data.eventName )
                            }

                            return this.getPath( '30298-11a.html' )

                    }
                }

                return
        }
    }

    getAttackableKillIds(): Array<number> {
        return [ MAILLE_LIZARDMAN, MAILLE_LIZARDMAN_SCOUT, MAILLE_LIZARDMAN_GUARD, KING_OF_THE_ARANEID ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00118_ToLeadAndBeLed'
    }

    getQuestStartIds(): Array<number> {
        return [ BLACKSMITH_PINTER ]
    }

    getTalkIds(): Array<number> {
        return [ BLACKSMITH_PINTER ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        switch ( data.npcId ) {
            case MAILLE_LIZARDMAN:
            case MAILLE_LIZARDMAN_SCOUT:
            case MAILLE_LIZARDMAN_GUARD:
                if ( state.isMemoState( 1 )
                        && Math.random() < QuestHelper.getAdjustedChance( BLOOD_OF_MAILLE_LIZARDMAN, 0.7, data.isChampion ) ) {
                    await QuestHelper.rewardAndProgressState( player, state, BLOOD_OF_MAILLE_LIZARDMAN, 1, 10, data.isChampion, 2 )
                }

                return

            case KING_OF_THE_ARANEID:
                if ( state.isMemoState( 4 ) && player.getSponsor() > 0 ) {
                    let sponsor: L2PcInstance = L2World.getPlayer( player.getSponsor() )
                    if ( sponsor
                            && Math.random() < QuestHelper.getAdjustedChance( LEG_OF_KING_ARANEID, 0.7, data.isChampion )
                            && GeneralHelper.checkIfInRange( 1500, npc, sponsor, true ) ) {
                        await QuestHelper.rewardAndProgressState( player, state, LEG_OF_KING_ARANEID, 1, 8, data.isChampion, 8 )
                    }
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        if ( [ 'sponsor', '30298-10.html' ].includes( data.eventName ) ) {
            return this.checkForApprentice( data )
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30298-03.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    break
                }

                return

            case '30298-05a.html':
            case '30298-05b.html':
            case '30298-05c.html':
            case '30298-05g.html':
                break

            case '30298-05d.html':
                if ( state.isMemoState( 1 ) && QuestHelper.getQuestItemsCount( player, BLOOD_OF_MAILLE_LIZARDMAN ) >= 10 ) {
                    await QuestHelper.takeSingleItem( player, BLOOD_OF_MAILLE_LIZARDMAN, -1 )

                    state.setMemoState( 2 )
                    state.setMemoStateEx( 1, 1 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '30298-05e.html':
                if ( state.isMemoState( 1 ) && QuestHelper.getQuestItemsCount( player, BLOOD_OF_MAILLE_LIZARDMAN ) >= 10 ) {
                    await QuestHelper.takeSingleItem( player, BLOOD_OF_MAILLE_LIZARDMAN, -1 )

                    state.setMemoState( 2 )
                    state.setMemoStateEx( 1, 2 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '30298-05f.html':
                if ( state.isMemoState( 1 ) && QuestHelper.getQuestItemsCount( player, BLOOD_OF_MAILLE_LIZARDMAN ) >= 10 ) {
                    await QuestHelper.takeSingleItem( player, BLOOD_OF_MAILLE_LIZARDMAN, -1 )

                    state.setMemoState( 2 )
                    state.setMemoStateEx( 1, 3 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let otherState: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00123_TheLeaderAndTheFollower' )

        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( otherState && otherState.isStarted() ) {
                    return this.getPath( '30298-02b.html' )
                }

                if ( otherState && otherState.isCompleted() ) {
                    return this.getPath( '30298-02a.htm' )
                }

                if ( player.getLevel() >= minimumLevel && player.getPledgeType() == -1 && player.getSponsor() > 0 ) {
                    return this.getPath( '30298-01.htm' )
                }

                return this.getPath( '30298-02.htm' )

            case QuestStateValues.STARTED:
                if ( state.isMemoState( 1 ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, BLOOD_OF_MAILLE_LIZARDMAN ) < 10 ) {
                        return this.getPath( '30298-04.html' )
                    }

                    return this.getPath( '30298-05.html' )
                }

                if ( state.isMemoState( 2 ) ) {
                    if ( player.getSponsor() === 0 ) {
                        if ( state.getMemoStateEx( 1 ) === 1 ) {
                            return this.getPath( '30298-06a.html' )
                        }

                        if ( state.getMemoStateEx( 1 ) === 2 ) {
                            return this.getPath( '30298-06b.html' )
                        }

                        if ( state.getMemoStateEx( 1 ) === 3 ) {
                            return this.getPath( '30298-06c.html' )
                        }

                        break
                    }

                    let sponsor: L2PcInstance = L2World.getPlayer( player.getSponsor() )
                    let npc = L2World.getObjectById( data.characterId )
                    if ( sponsor && GeneralHelper.checkIfInRange( 1500, npc, sponsor, true ) ) {
                        return this.getPath( '30298-07.html' )
                    }

                    if ( state.getMemoStateEx( 1 ) == 1 ) {
                        return this.getPath( '30298-06.html' )
                    }

                    if ( state.getMemoStateEx( 1 ) === 2 ) {
                        return this.getPath( '30298-06d.html' )
                    }

                    if ( state.getMemoStateEx( 1 ) === 3 ) {
                        return this.getPath( '30298-06e.html' )
                    }

                    break
                }

                if ( state.isMemoState( 3 ) ) {
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 7, true )

                    return this.getPath( '30298-15.html' )
                }

                if ( state.isMemoState( 4 ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, LEG_OF_KING_ARANEID ) < 8 ) {
                        return this.getPath( '30298-16.html' )
                    }

                    if ( state.getMemoStateEx( 1 ) === 1 ) {
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_HELM, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_ARMOR, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_GAUNTLETS_HEAVY_ARMOR, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_SABATON_HEAVY_ARMOR, 1 )
                        await QuestHelper.takeSingleItem( player, LEG_OF_KING_ARANEID, -1 )
                    }

                    if ( state.getMemoStateEx( 1 ) === 2 ) {
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_HELM, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_BRIGANDINE, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_LEATHER_GLOVES_LIGHT_ARMOR, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_BOOTS_LIGHT_ARMOR, 1 )
                        await QuestHelper.takeSingleItem( player, LEG_OF_KING_ARANEID, -1 )
                    }

                    if ( state.getMemoStateEx( 1 ) === 3 ) {
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_HELM, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_AKETON, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_PADDED_GLOVES_ROBE, 1 )
                        await QuestHelper.giveSingleItem( player, CLAN_OATH_SANDALS_ROBE, 1 )
                        await QuestHelper.takeSingleItem( player, LEG_OF_KING_ARANEID, -1 )
                    }

                    await state.exitQuest( false, true )

                    return this.getPath( '30298-17.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}