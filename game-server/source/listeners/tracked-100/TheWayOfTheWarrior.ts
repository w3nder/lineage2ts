import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'
import _ from 'lodash'
import aigle from 'aigle'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const PERWAN = 32133
const KEKROPUS = 32138

const WOLF_TAIL = createItemDefinition( 9807, 5 )
const MUERTOS_CLAW = createItemDefinition( 9808, 10 )

const minimumLevel = 10

const UNSEALED_ALTAR = new SkillHolder( 4549 )
const WARRIORS_SWORD = 9720
const rewards: Array<ItemDefinition> = [
    createItemDefinition( 1060, 100 ), // Lesser Healing Potion
    createItemDefinition( 4412, 10 ), // Echo Crystal - Theme of Battle
    createItemDefinition( 4413, 10 ), // Echo Crystal - Theme of Love
    createItemDefinition( 4414, 10 ), // Echo Crystal - Theme of Solitude
    createItemDefinition( 4415, 10 ), // Echo Crystal - Theme of Feast
    createItemDefinition( 4416, 10 ), // Echo Crystal - Theme of Celebration
]

const MOUNTAIN_WEREWOLF = 22235
const mobIds: Array<number> = [
    22236, // Muertos Archer
    22239, // Muertos Guard
    22240, // Muertos Scout
    22242, // Muertos Warrior
    22243, // Muertos Captain
    22245, // Muertos Lieutenant
    22246, // Muertos Commander
]

export class TheWayOfTheWarrior extends ListenerLogic {
    constructor() {
        super( 'Q00175_TheWayOfTheWarrior', 'listeners/tracked/TheWayOfTheWarrior.ts' )
        this.questId = 175
        this.questItemIds = [ WOLF_TAIL.id, MUERTOS_CLAW.id ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            MOUNTAIN_WEREWOLF,
            ...mobIds,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00175_TheWayOfTheWarrior'
    }

    getQuestStartIds(): Array<number> {
        return [ KEKROPUS ]
    }

    getTalkIds(): Array<number> {
        return [ KEKROPUS, PERWAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let isWerewolf: boolean = data.npcId === MOUNTAIN_WEREWOLF
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), isWerewolf ? 2 : 7, 3, npc )

        if ( !state ) {
            return
        }

        let player: L2PcInstance = state.getPlayer()

        if ( isWerewolf ) {
            if ( Math.random() < QuestHelper.getAdjustedChance( WOLF_TAIL.id, 0.5, data.isChampion ) ) {
                await QuestHelper.rewardAndProgressState( player, state, WOLF_TAIL.id, _.random( 1, WOLF_TAIL.count ), WOLF_TAIL.count, data.isChampion, 3 )
                return
            }

            return
        }

        await QuestHelper.rewardAndProgressState( player, state, MUERTOS_CLAW.id, 1, MUERTOS_CLAW.count, data.isChampion, 8 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32138-02.htm':
                break

            case '32138-05.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    break
                }

                return

            case '32138-10.html':
                state.setMemoState( 6 )
                state.setConditionWithSound( 7, true )
                break

            case '32138-13.html':
                if ( !QuestHelper.hasItem( player, MUERTOS_CLAW ) ) {
                    return
                }

                await QuestHelper.takeSingleItem( player, MUERTOS_CLAW.id, -1 )
                await QuestHelper.giveAdena( player, 8799, true )

                await aigle.resolve( rewards ).each( async ( item : ItemDefinition ) => {
                    return QuestHelper.rewardSingleItem( player, item.id, item.count )
                } )

                await NewbieRewardsHelper.giveNewbieReward( player )

                await QuestHelper.rewardSingleItem( player, WARRIORS_SWORD, 1 )
                await QuestHelper.addExpAndSp( player, 20739, 1777 )

                await state.exitQuest( false, true )
                player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                break

            case '32133-06.html':
                state.setMemoState( 5 )
                state.setConditionWithSound( 6, true )

                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                npc.setTarget( player )
                await npc.doCastWithHolder( UNSEALED_ALTAR )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case KEKROPUS:

                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() !== Race.KAMAEL ) {
                            return this.getPath( '32138-04.htm' )
                        }

                        if ( player.getLevel() >= minimumLevel ) {
                            return this.getPath( '32138-01.htm' )
                        }

                        return this.getPath( '32138-03.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                            case 2:
                            case 3:
                                return this.getPath( '32138-06.html' )

                            case 4:
                                state.setMemoState( 4 )
                                state.setConditionWithSound( 5, true )

                                return this.getPath( '32138-07.html' )

                            case 5:
                                return this.getPath( '32138-08.html' )

                            case 6:
                                return this.getPath( '32138-09.html' )

                            case 7:
                                return this.getPath( '32138-11.html' )

                            case 8:
                                if ( QuestHelper.hasItem( player, MUERTOS_CLAW ) ) {
                                    return this.getPath( '32138-12.html' )
                                }

                                break
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case PERWAN:
                switch ( state.getCondition() ) {
                    case 1:
                        state.setMemoState( 2 )
                        state.setConditionWithSound( 2, true )

                        return this.getPath( '32133-01.html' )

                    case 2:
                        return this.getPath( '32133-02.html' )

                    case 3:
                        if ( QuestHelper.hasItem( player, WOLF_TAIL ) ) {
                            await QuestHelper.takeSingleItem( player, WOLF_TAIL.id, WOLF_TAIL.count )

                            state.setMemoState( 3 )
                            state.setConditionWithSound( 4, true )

                            return this.getPath( '32133-03.html' )
                        }

                        break

                    case 4:
                        return this.getPath( '32133-04.html' )

                    case 5:
                        return this.getPath( '32133-05.html' )

                    case 6:
                        return this.getPath( '32133-07.html' )

                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}