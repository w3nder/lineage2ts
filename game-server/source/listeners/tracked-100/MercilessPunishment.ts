import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'
import aigle from 'aigle'
import { createItemDefinition, ItemDefinition } from '../../gameService/interface/ItemDefinition'

const URUTU_CHIEF_HATOS = 30568
const CENTURION_PARUGON = 30580

const HATOSS_ORDER_1 = 1553
const HATOSS_ORDER_2 = 1554
const HATOSS_ORDER_3 = 1555
const LETTER_TO_DARK_ELF = 1556
const LETTER_TO_HUMAN = 1557
const LETTER_TO_ELF = 1558

const BARANKA_MESSENGER = 27041

const BUTCHER = 1510

let rewards = [
    createItemDefinition( 1060, 100 ), // Lesser Healing Potion
    createItemDefinition( 4412, 10 ), // Echo Crystal - Theme of Battle
    createItemDefinition( 4413, 10 ), // Echo Crystal - Theme of Love
    createItemDefinition( 4414, 10 ), // Echo Crystal - Theme of Solitude
    createItemDefinition( 4415, 10 ), // Echo Crystal - Theme of Feast
    createItemDefinition( 4416, 10 ), // Echo Crystal - Theme of Celebration
]

const minimumLevel = 10

export class MercilessPunishment extends ListenerLogic {
    constructor() {
        super( 'Q00107_MercilessPunishment', 'listeners/tracked/MercilessPunishment.ts' )
        this.questId = 107
        this.questItemIds = [
            HATOSS_ORDER_1,
            HATOSS_ORDER_2,
            HATOSS_ORDER_3,
            LETTER_TO_DARK_ELF,
            LETTER_TO_HUMAN,
            LETTER_TO_ELF,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ BARANKA_MESSENGER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00107_MercilessPunishment'
    }

    getQuestStartIds(): Array<number> {
        return [ URUTU_CHIEF_HATOS ]
    }

    getTalkIds(): Array<number> {
        return [ URUTU_CHIEF_HATOS, CENTURION_PARUGON ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
            return
        }

        switch ( state.getCondition() ) {
            case 2:
                if ( QuestHelper.hasQuestItem( player, HATOSS_ORDER_1 ) ) {
                    await QuestHelper.giveSingleItem( player, LETTER_TO_HUMAN, 1 )
                    state.setConditionWithSound( 3, true )
                }

                return

            case 4:
                if ( QuestHelper.hasQuestItem( player, HATOSS_ORDER_2 ) ) {
                    await QuestHelper.giveSingleItem( player, LETTER_TO_DARK_ELF, 1 )
                    state.setConditionWithSound( 5, true )
                }

                return

            case 6:
                if ( QuestHelper.hasQuestItem( player, HATOSS_ORDER_3 ) ) {
                    await QuestHelper.giveSingleItem( player, LETTER_TO_ELF, 1 )
                    state.setConditionWithSound( 7, true )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return null
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30568-04.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, HATOSS_ORDER_1, 1 )

                    break
                }

                return

            case '30568-07.html':
                await QuestHelper.giveAdena( player, 200, true )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_GIVEUP )

                await state.exitQuest( true )

                break

            case '30568-08.html':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, HATOSS_ORDER_1 ) ) {
                    state.setConditionWithSound( 4 )

                    await QuestHelper.takeSingleItem( player, HATOSS_ORDER_1, -1 )
                    await QuestHelper.giveSingleItem( player, HATOSS_ORDER_2, 1 )

                    break
                }

                return

            case '30568-10.html':
                if ( state.isCondition( 5 ) && QuestHelper.hasQuestItem( player, HATOSS_ORDER_2 ) ) {
                    state.setConditionWithSound( 6 )

                    await QuestHelper.takeSingleItem( player, HATOSS_ORDER_2, -1 )
                    await QuestHelper.giveSingleItem( player, HATOSS_ORDER_3, 1 )

                    break
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case URUTU_CHIEF_HATOS:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() !== Race.ORC ) {
                            return this.getPath( '30568-01.htm' )
                        }

                        return this.getPath( player.getLevel() < minimumLevel ? '30568-02.htm' : '30568-03.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                            case 2:
                                if ( QuestHelper.hasQuestItem( player, HATOSS_ORDER_1 ) ) {
                                    return this.getPath( '30568-05.html' )
                                }

                                break

                            case 3:
                                if ( QuestHelper.hasQuestItems( player, HATOSS_ORDER_1, LETTER_TO_HUMAN ) ) {
                                    return this.getPath( '30568-06.html' )
                                }

                                break

                            case 4:
                                if ( QuestHelper.hasQuestItems( player, HATOSS_ORDER_2, LETTER_TO_HUMAN ) ) {
                                    return this.getPath( '30568-08.html' )
                                }

                                break

                            case 5:
                                if ( QuestHelper.hasQuestItems( player, HATOSS_ORDER_2, LETTER_TO_HUMAN, LETTER_TO_DARK_ELF ) ) {
                                    return this.getPath( '30568-09.html' )
                                }

                                break

                            case 6:
                                if ( QuestHelper.hasQuestItems( player, HATOSS_ORDER_3, LETTER_TO_HUMAN, LETTER_TO_DARK_ELF ) ) {
                                    return this.getPath( '30568-10.html' )
                                }

                                break

                            case 7:
                                if ( QuestHelper.hasQuestItems( player, HATOSS_ORDER_3, LETTER_TO_HUMAN, LETTER_TO_DARK_ELF, LETTER_TO_ELF ) ) {
                                    await NewbieRewardsHelper.giveNewbieReward( player )
                                    await QuestHelper.addExpAndSp( player, 34565, 2962 )
                                    await QuestHelper.giveAdena( player, 14666, true )

                                    await aigle.resolve( rewards ).each( ( item: ItemDefinition ) => {
                                        return QuestHelper.rewardSingleItem( player, item.id, item.count )
                                    } )

                                    await QuestHelper.rewardSingleItem( player, BUTCHER, 1 )
                                    await state.exitQuest( false, true )

                                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                                    return this.getPath( '30568-11.html' )
                                }

                                break
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case CENTURION_PARUGON:
                if ( state.isStarted() && state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, HATOSS_ORDER_1 ) ) {
                    state.setConditionWithSound( 2, true )

                    return this.getPath( '30580-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}