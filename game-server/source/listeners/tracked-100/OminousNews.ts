import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'

const MOIRA = 31979
const KARUDA = 32017
const minimumLevel = 20

export class OminousNews extends ListenerLogic {
    constructor() {
        super( 'Q00122_OminousNews', 'listeners/tracked/OminousNews.ts' )
        this.questId = 122
    }

    getQuestStartIds(): Array<number> {
        return [ MOIRA ]
    }

    getTalkIds(): Array<number> {
        return [ MOIRA, KARUDA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        switch ( data.eventName ) {
            case '31979-02.htm':
                state.startQuest()
                break

            case '32017-02.html':
                let player = L2World.getPlayer( data.playerId )
                await QuestHelper.giveAdena( player, 8923, true )
                await QuestHelper.addExpAndSp( player, 45151, 2310 )
                await state.exitQuest( false, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case MOIRA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '31979-01.htm' : '31979-00.htm' )

                    case QuestStateValues.STARTED:
                        return this.getPath( '31979-03.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case KARUDA:
                if ( state.isStarted() ) {
                    return this.getPath( '32017-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00122_OminousNews'
    }
}