import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const HARRYS = 30035
const ALTRAN = 30283

const STONE_GOLEM = 20016

const WOODEN_BREASTPLATE = 23
const HARRYS_1ST_RECIEPT = 1008
const HARRYS_2ND_RECIEPT = 1009
const GOLEM_SHARD = 1010
const TOOL_BOX = 1011

const minimumLevel = 10

export class ShardsOfGolem extends ListenerLogic {
    constructor() {
        super( 'Q00152_ShardsOfGolem', 'listeners/tracked/ShardsOfGolem.ts' )
        this.questId = 152
        this.questItemIds = [ HARRYS_1ST_RECIEPT, HARRYS_2ND_RECIEPT, GOLEM_SHARD, TOOL_BOX ]
    }

    getAttackableKillIds(): Array<number> {
        return [ STONE_GOLEM ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00152_ShardsOfGolem'
    }

    getQuestStartIds(): Array<number> {
        return [ HARRYS ]
    }

    getTalkIds(): Array<number> {
        return [ HARRYS, ALTRAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )
        let player = L2World.getPlayer( data.playerId )

        if ( state
                && state.isCondition( 2 )
                && _.random( 100 ) < QuestHelper.getAdjustedChance( GOLEM_SHARD, 30, data.isChampion )
                && QuestHelper.getQuestItemsCount( player, GOLEM_SHARD ) < 5 ) {

            await QuestHelper.rewardSingleQuestItem( player, GOLEM_SHARD, 1, data.isChampion )

            if ( QuestHelper.getQuestItemsCount( player, GOLEM_SHARD ) >= 5 ) {
                state.setConditionWithSound( 3, true )
                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return this.getPath( data.eventName )
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30035-03.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, HARRYS_1ST_RECIEPT, 1 )

                break

            case '30283-02.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, HARRYS_1ST_RECIEPT ) ) {
                    await QuestHelper.takeSingleItem( player, HARRYS_1ST_RECIEPT, -1 )
                    await QuestHelper.giveSingleItem( player, HARRYS_2ND_RECIEPT, 1 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case HARRYS:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30035-02.htm' : '30035-01.htm' )

                    case QuestStateValues.STARTED: {
                        switch ( state.getCondition() ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, HARRYS_1ST_RECIEPT ) ) {
                                    return this.getPath( '30035-04a.html' )
                                }

                                break

                            case 2:
                            case 3:
                                if ( QuestHelper.hasQuestItem( player, HARRYS_2ND_RECIEPT ) ) {
                                    return this.getPath( '30035-04.html' )
                                }

                                break

                            case 4:
                                if ( QuestHelper.hasQuestItems( player, HARRYS_2ND_RECIEPT, TOOL_BOX ) ) {
                                    await QuestHelper.rewardSingleItem( player, WOODEN_BREASTPLATE, 1 )
                                    await QuestHelper.addExpAndSp( player, 5000, 0 )
                                    await state.exitQuest( false, true )

                                    return this.getPath( '30035-05.html' )
                                }

                                break
                        }
                        break
                    }
                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case ALTRAN:
                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.hasQuestItem( player, HARRYS_1ST_RECIEPT ) ) {
                            return this.getPath( '30283-01.html' )
                        }

                        break

                    case 2:
                        if ( QuestHelper.hasQuestItem( player, HARRYS_2ND_RECIEPT ) && QuestHelper.getQuestItemsCount( player, GOLEM_SHARD ) < 5 ) {
                            return this.getPath( '30283-03.html' )
                        }

                        break

                    case 3:
                        if ( QuestHelper.hasQuestItem( player, HARRYS_2ND_RECIEPT ) && QuestHelper.getQuestItemsCount( player, GOLEM_SHARD ) >= 5 ) {
                            await QuestHelper.takeSingleItem( player, GOLEM_SHARD, -1 )
                            await QuestHelper.giveSingleItem( player, TOOL_BOX, 1 )

                            state.setConditionWithSound( 4, true )

                            return this.getPath( '30283-04.html' )
                        }

                        break

                    case 4:
                        if ( QuestHelper.hasQuestItems( player, HARRYS_2ND_RECIEPT, TOOL_BOX ) ) {
                            return this.getPath( '30283-05.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}