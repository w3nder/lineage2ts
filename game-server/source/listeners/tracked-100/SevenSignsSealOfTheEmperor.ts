import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { QuestHelper } from '../helpers/QuestHelper'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const IASON_HEINE = 30969
const MERCHANT_OF_MAMMON = 32584
const SHUNAIMAN = 32586
const WOOD = 32593
const COURT_MAGICIAN = 32598

const ELMOREDEN_HOLY_WATER = 13808
const COURT_MAGICIANS_MAGIC_STAFF = 13809
const SEAL_OF_BINDING = 13846
const SACRED_SWORD_OF_EINHASAD = 15310

const minimumLevel = 79
let isBusy: boolean = false

const variableNames = {
    despawn: 'despawn',
    playerId: 'playerId',
}

export class SevenSignsSealOfTheEmperor extends ListenerLogic {
    constructor() {
        super( 'Q00196_SevenSignsSealOfTheEmperor', 'listeners/tracked/SevenSignsSealOfTheEmperor.ts' )
        this.questId = 196
        this.questItemIds = [
            ELMOREDEN_HOLY_WATER,
            COURT_MAGICIANS_MAGIC_STAFF,
            SEAL_OF_BINDING,
            SACRED_SWORD_OF_EINHASAD,
        ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ MERCHANT_OF_MAMMON ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00196_SevenSignsSealOfTheEmperor'
    }

    getQuestStartIds(): Array<number> {
        return [ IASON_HEINE ]
    }

    getTalkIds(): Array<number> {
        return [ IASON_HEINE, MERCHANT_OF_MAMMON, SHUNAIMAN, WOOD, COURT_MAGICIAN ]
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( '32584.htm' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.characterNpcId === MERCHANT_OF_MAMMON && data.eventName === variableNames.despawn ) {
            isBusy = false

            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_ANCIENT_PROMISE_TO_THE_EMPEROR_HAS_BEEN_FULFILLED )
            await npc.deleteMe()

            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case '30969-02.htm':
            case '30969-03.htm':
            case '30969-04.htm':
                break

            case '30969-05.html':
                state.startQuest()
                break

            case 'ssq_mammon':
                if ( state.isCondition( 1 ) ) {
                    if ( !isBusy ) {
                        isBusy = true

                        NpcVariablesManager.set( data.characterId, variableNames.playerId, 1 )
                        let merchant: L2Npc = QuestHelper.addGenericSpawn( null, MERCHANT_OF_MAMMON, 109743, 219975, -3512, 0, false, 0, false )

                        BroadcastHelper.broadcastNpcSayStringId( merchant, NpcSayType.NpcAll, NpcStringIds.WHO_DARES_SUMMON_THE_MERCHANT_OF_MAMMON )
                        this.startQuestTimer( variableNames.despawn, 120000, merchant.getObjectId(), 0 )

                        return this.getPath( '30969-06.html' )
                    }

                    return this.getPath( '30969-07.html' )
                }

                return

            case '30969-13.html':
                if ( state.isCondition( 5 ) ) {
                    break
                }
                return

            case '30969-14.html':
                if ( state.isCondition( 5 ) ) {
                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '32584-02.html':
            case '32584-03.html':
            case '32584-04.html':
                if ( state.isCondition( 1 ) ) {
                    break
                }

                return

            case '32584-05.html':
                if ( state.isCondition( 1 ) ) {
                    this.stopQuestTimers( variableNames.despawn )

                    await npc.deleteMe()
                    isBusy = false

                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '32586-02.html':
            case '32586-03.html':
            case '32586-04.html':
            case '32586-06.html':
                if ( state.isCondition( 3 ) ) {
                    break
                }

                return

            case '32586-07.html':
                if ( state.isCondition( 3 ) ) {
                    await QuestHelper.giveMultipleItems( player, 1, ELMOREDEN_HOLY_WATER, SACRED_SWORD_OF_EINHASAD )

                    state.setConditionWithSound( 4, true )

                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.BY_USING_THE_SKILL_OF_EINHASAD_S_HOLY_SWORD_DEFEAT_THE_EVIL_LILIMS ) )
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.USING_EINHASAD_HOLY_WATER_TO_OPEN_DOOR ) )
                    break
                }

                return

            case '32586-11.html':
            case '32586-12.html':
            case '32586-13.html':
                if ( state.isCondition( 4 ) && QuestHelper.getQuestItemsCount( player, SEAL_OF_BINDING ) >= 4 ) {
                    break
                }

                return

            case '32586-14.html':
                if ( state.isCondition( 4 ) && QuestHelper.getQuestItemsCount( player, SEAL_OF_BINDING ) >= 4 ) {
                    await QuestHelper.takeMultipleItems( player, -1, ELMOREDEN_HOLY_WATER, COURT_MAGICIANS_MAGIC_STAFF, SEAL_OF_BINDING, SACRED_SWORD_OF_EINHASAD )
                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case 'finish':
                if ( state.isCondition( 6 ) ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        await QuestHelper.addExpAndSp( player, 52518015, 5817677 )
                        await state.exitQuest( false, true )

                        return this.getPath( '32593-02.html' )
                    }

                    return this.getPath( 'level_check.html' )
                }

                return

            case '32598-02.html':
                if ( [ 3, 4 ].includes( state.getCondition() ) ) {
                    await QuestHelper.giveSingleItem( player, COURT_MAGICIANS_MAGIC_STAFF, 1 )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === IASON_HEINE ) {
                    let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00195_SevenSignsSecretRitualOfThePriests' )
                    return this.getPath( canProceed ? '30969-01.htm' : '30969-08.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case IASON_HEINE:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30969-09.html' )

                            case 2:
                                state.setConditionWithSound( 3, true )
                                NpcVariablesManager.set( data.characterId, variableNames.playerId, 0 )

                                return this.getPath( '30969-10.html' )

                            case 3:
                            case 4:
                                return this.getPath( '30969-11.html' )

                            case 5:
                                return this.getPath( '30969-12.html' )

                            case 6:
                                return this.getPath( '30969-15.html' )
                        }

                        break

                    case MERCHANT_OF_MAMMON:
                        if ( state.isCondition( 1 ) ) {
                            if ( !NpcVariablesManager.get( data.characterId, variableNames.playerId ) ) {
                                NpcVariablesManager.set( data.characterId, variableNames.playerId, data.playerId )
                            }

                            return this.getPath( NpcVariablesManager.get( data.characterId, variableNames.playerId ) === data.playerId ? '32584-01.html' : '32584-06.html' )
                        }

                        break

                    case SHUNAIMAN:
                        switch ( state.getCondition() ) {
                            case 3:
                                return this.getPath( '32586-01.html' )

                            case 4:
                                if ( QuestHelper.getQuestItemsCount( player, SEAL_OF_BINDING ) < 4 ) {
                                    let hasHolyWater: boolean = QuestHelper.hasQuestItem( player, ELMOREDEN_HOLY_WATER )
                                    let hasSword: boolean = QuestHelper.hasQuestItem( player, SACRED_SWORD_OF_EINHASAD )

                                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.BY_USING_THE_SKILL_OF_EINHASAD_S_HOLY_SWORD_DEFEAT_THE_EVIL_LILIMS ) )
                                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.USING_EINHASAD_HOLY_WATER_TO_OPEN_DOOR ) )

                                    if ( hasHolyWater && hasSword ) {
                                        return this.getPath( '32586-08.html' )
                                    }

                                    if ( !hasHolyWater && hasSword ) {
                                        await QuestHelper.giveSingleItem( player, ELMOREDEN_HOLY_WATER, 1 )
                                        return this.getPath( '32586-09.html' )
                                    }

                                    if ( hasHolyWater && !hasSword ) {
                                        await QuestHelper.giveSingleItem( player, SACRED_SWORD_OF_EINHASAD, 1 )
                                        return this.getPath( '32586-09.html' )
                                    }

                                    break
                                }

                                return this.getPath( '32586-10.html' )

                            case 5:
                                return this.getPath( '32586-15.html' )
                        }

                        break

                    case WOOD:
                        if ( state.isCondition( 6 ) ) {
                            return this.getPath( '32593-01.html' )
                        }

                        break

                    case COURT_MAGICIAN:
                        if ( [ 3, 4 ].includes( state.getCondition() ) ) {
                            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.USING_COURT_MAGICIANS_STAFF_TO_OPEN_DOOR ) )

                            return this.getPath( QuestHelper.hasQuestItem( player, COURT_MAGICIANS_MAGIC_STAFF ) ? '32598-03.html' : '32598-01.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}