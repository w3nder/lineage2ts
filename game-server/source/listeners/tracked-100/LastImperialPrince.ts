import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const NAMELESS_SPIRIT = 31453
const DEVORIN = 32009
const ANTIQUE_BROOCH = 7262

const minimumLevel = 74

export class LastImperialPrince extends ListenerLogic {
    constructor() {
        super( 'Q00119_LastImperialPrince', 'listeners/tracked/LastImperialPrince.ts' )
        this.questId = 119
    }

    getQuestStartIds(): Array<number> {
        return [ NAMELESS_SPIRIT ]
    }

    getTalkIds(): Array<number> {
        return [ NAMELESS_SPIRIT, DEVORIN ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31453-02.htm':
            case '31453-03.htm':
            case '31453-10.html':
                break

            case '31453-04.html':
                state.startQuest()
                break

            case '31453-11.html':
                if ( state.isCondition( 2 ) ) {
                    await QuestHelper.giveAdena( player, 150292, true )
                    await QuestHelper.addExpAndSp( player, 902439, 90067 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case 'brooch':
                return this.getPath( QuestHelper.hasQuestItem( player, ANTIQUE_BROOCH ) ? '32009-02.html' : '32009-03.html' )

            case '32009-04.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, ANTIQUE_BROOCH ) ) {
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel && QuestHelper.hasQuestItem( player, ANTIQUE_BROOCH ) ? '31453-01.htm' : '31453-05.html' )

            case QuestStateValues.STARTED:
                if ( data.characterNpcId === NAMELESS_SPIRIT ) {
                    if ( state.isCondition( 1 ) ) {
                        if ( QuestHelper.hasQuestItem( player, ANTIQUE_BROOCH ) ) {
                            return this.getPath( '31453-07.html' )
                        }

                        await state.exitQuest( true )

                        return this.getPath( '31453-08.html' )
                    }

                    if ( state.isCondition( 2 ) ) {
                        return this.getPath( '31453-09.html' )
                    }

                    break
                }

                if ( data.characterNpcId === DEVORIN ) {
                    if ( state.isCondition( 1 ) ) {
                        return this.getPath( '32009-01.html' )
                    }

                    if ( state.isCondition( 2 ) ) {
                        return this.getPath( '32009-05.html' )
                    }

                    break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === NAMELESS_SPIRIT ) {
                    return this.getPath( '31453-06.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessage()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00119_LastImperialPrince'
    }
}