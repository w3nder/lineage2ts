import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { EchoCrystalRewards, NewbieItemIds } from '../helpers/NewbieRewardsHelper'
import _ from 'lodash'

const THIFIELL = 30358
const KARTA = 30133

const TUMRAN_ORC_BRIGAND = 27070

const ONYX_TALISMAN1 = 984
const ONYX_TALISMAN2 = 985
const ANCIENT_SCROLL = 986
const ANCIENT_CLAY_TABLET = 987
const KARTAS_TRANSLATION = 988

const minimumLevel = 10
const EldritchDagger = 989

export class ForgottenTruth extends ListenerLogic {
    constructor() {
        super( 'Q00106_ForgottenTruth', 'listeners/tracked/ForgottenTruth.ts' )
        this.questId = 106
        this.questItemIds = [
            KARTAS_TRANSLATION,
            ONYX_TALISMAN1,
            ONYX_TALISMAN2,
            ANCIENT_SCROLL,
            ANCIENT_CLAY_TABLET,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ TUMRAN_ORC_BRIGAND ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00106_ForgottenTruth'
    }

    getQuestStartIds(): Array<number> {
        return [ THIFIELL ]
    }

    getTalkIds(): Array<number> {
        return [ THIFIELL, KARTA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state || !state.isCondition( 2 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
            return
        }

        if ( _.random( 100 ) < QuestHelper.getAdjustedChance( ANCIENT_SCROLL, 20, data.isChampion )
                && QuestHelper.hasQuestItem( player, ONYX_TALISMAN2 ) ) {
            if ( !QuestHelper.hasQuestItem( player, ANCIENT_SCROLL ) ) {
                await QuestHelper.giveSingleItem( player, ANCIENT_SCROLL, 1 )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                return
            }

            if ( !QuestHelper.hasQuestItem( player, ANCIENT_CLAY_TABLET ) ) {
                state.setConditionWithSound( 3, true )
                await QuestHelper.giveSingleItem( player, ANCIENT_CLAY_TABLET, 1 )

                return
            }
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return null
        }

        switch ( data.eventName ) {
            case '30358-04.htm':
                return this.getPath( data.eventName )

            case '30358-05.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( L2World.getPlayer( data.playerId ), ONYX_TALISMAN1, 1 )

                    return this.getPath( data.eventName )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case THIFIELL:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() === Race.DARK_ELF ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '30358-03.htm' : '30358-02.htm' )
                        }

                        return this.getPath( '30358-01.htm' )

                    case QuestStateValues.STARTED:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, ONYX_TALISMAN1, ONYX_TALISMAN2 )
                                && !QuestHelper.hasQuestItem( player, KARTAS_TRANSLATION ) ) {
                            return this.getPath( '30358-06.html' )
                        }

                        if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, KARTAS_TRANSLATION ) ) {
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            await QuestHelper.rewardSingleItem( player, NewbieItemIds.LesserHealingPotion, 100 )
                            await QuestHelper.rewardMultipleItems( player, 10, ...EchoCrystalRewards )
                            await QuestHelper.rewardSingleItem( player, EldritchDagger, 1 )

                            if ( player.isMageClass() ) {
                                await QuestHelper.rewardSingleItem( player, NewbieItemIds.SpiritshotNoGrade, 500 )
                            } else {
                                await QuestHelper.rewardSingleItem( player, NewbieItemIds.SoulshotNoGrade, 500 )
                            }

                            await QuestHelper.giveAdena( player, 10266, true )
                            await QuestHelper.addExpAndSp( player, 24195, 2074 )
                            await state.exitQuest( false, true )

                            return this.getPath( '30358-07.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case KARTA:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                            if ( QuestHelper.hasQuestItem( player, ONYX_TALISMAN1 ) ) {
                                state.setConditionWithSound( 2, true )
                                await QuestHelper.takeSingleItem( player, ONYX_TALISMAN1, -1 )
                                await QuestHelper.giveSingleItem( player, ONYX_TALISMAN2, 1 )

                                return this.getPath( '30133-01.html' )
                            }

                            break

                        case 2:
                            if ( QuestHelper.hasQuestItem( player, ONYX_TALISMAN2 ) ) {
                                return this.getPath( '30133-02.html' )
                            }
                            break

                        case 3:
                            if ( QuestHelper.hasQuestItems( player, ANCIENT_SCROLL, ANCIENT_CLAY_TABLET ) ) {
                                state.setConditionWithSound( 4, true )
                                await QuestHelper.takeMultipleItems( player, -1, ANCIENT_SCROLL, ANCIENT_CLAY_TABLET, ONYX_TALISMAN2 )
                                await QuestHelper.giveSingleItem( player, KARTAS_TRANSLATION, 1 )

                                return this.getPath( '30133-03.html' )
                            }

                            break

                        case 4:
                            if ( QuestHelper.hasQuestItem( player, KARTAS_TRANSLATION ) ) {
                                return this.getPath( '30133-04.html' )
                            }

                            break
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}