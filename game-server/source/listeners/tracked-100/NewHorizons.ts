import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const ZENYA = 32140
const RAGARA = 32163

const SCROLL_OF_ESCAPE_GIRAN = 7559
const MARK_OF_TRAVELER = 7570

const minimumLevel = 3

export class NewHorizons extends ListenerLogic {
    constructor() {
        super( 'Q00172_NewHorizons', 'listeners/tracked/NewHorizons.ts' )
        this.questId = 172
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00172_NewHorizons'
    }

    getQuestStartIds(): Array<number> {
        return [ ZENYA ]
    }

    getTalkIds(): Array<number> {
        return [ ZENYA, RAGARA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32140-04.htm':
                state.startQuest()
                break

            case '32163-02.html':
                let player = L2World.getPlayer( data.playerId )
                await QuestHelper.rewardSingleItem( player, SCROLL_OF_ESCAPE_GIRAN, 1 )
                await QuestHelper.giveSingleItem( player, MARK_OF_TRAVELER, 1 )

                await state.exitQuest( false, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ZENYA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() === Race.KAMAEL ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '32140-01.htm' : '32140-02.htm' )
                        }

                        return this.getPath( '32140-03.htm' )

                    case QuestStateValues.STARTED:
                        return this.getPath( '32140-05.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case RAGARA:
                if ( state.isStarted() ) {
                    return this.getPath( '32163-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}