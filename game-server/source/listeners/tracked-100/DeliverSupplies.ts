import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const JENNA = 30349
const ROSELYN = 30355
const KRISTIN = 30357
const HARANT = 30360

const JENNAS_LETTER = 1153
const SENTRY_BLADE1 = 1154
const SENTRY_BLADE2 = 1155
const SENTRY_BLADE3 = 1156
const OLD_BRONZE_SWORD = 1157

const minimumLevel = 3
const npcMap = {
    KRISTIN: SENTRY_BLADE3,
    ROSELYN: SENTRY_BLADE2,
}

export class DeliverSupplies extends ListenerLogic {
    constructor() {
        super( 'Q00168_DeliverSupplies', 'listeners/tracked/DeliverSupplies.ts' )
        this.questId = 168
        this.questItemIds = [ JENNAS_LETTER, SENTRY_BLADE1, SENTRY_BLADE2, SENTRY_BLADE3, OLD_BRONZE_SWORD ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00168_DeliverSupplies'
    }

    getQuestStartIds(): Array<number> {
        return [ JENNA ]
    }

    getTalkIds(): Array<number> {
        return [ JENNA, ROSELYN, KRISTIN, HARANT ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '30349-03.htm' ) {
            return
        }

        state.startQuest()

        await QuestHelper.giveSingleItem( L2World.getPlayer( data.playerId ), JENNAS_LETTER, 1 )
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case JENNA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() === Race.DARK_ELF ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '30349-02.htm' : '30349-01.htm' )
                        }

                        return this.getPath( '30349-00.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, JENNAS_LETTER ) ) {
                                    return this.getPath( '30349-04.html' )
                                }

                                break

                            case 2:
                                if ( QuestHelper.hasQuestItems( player, SENTRY_BLADE1, SENTRY_BLADE2, SENTRY_BLADE3 ) ) {
                                    await QuestHelper.takeSingleItem( player, SENTRY_BLADE1, -1 )
                                    state.setConditionWithSound( 3, true )

                                    return this.getPath( '30349-05.html' )
                                }

                                break

                            case 3:
                                if ( QuestHelper.hasAtLeastOneQuestItem( player, SENTRY_BLADE2, SENTRY_BLADE3 ) ) {
                                    return this.getPath( '30349-07.html' )
                                }

                                break

                            case 4:
                                if ( QuestHelper.getQuestItemsCount( player, OLD_BRONZE_SWORD ) >= 2 ) {
                                    await QuestHelper.giveAdena( player, 820, true )
                                    await state.exitQuest( false, true )

                                    return this.getPath( '30349-06.html' )
                                }

                                break
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case HARANT:
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, JENNAS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, JENNAS_LETTER, -1 )
                    await QuestHelper.giveMultipleItems( player, 1, SENTRY_BLADE1, SENTRY_BLADE2, SENTRY_BLADE3 )

                    state.setConditionWithSound( 2, true )

                    return this.getPath( '30360-01.html' )
                }

                if ( state.isCondition( 2 ) ) {
                    return this.getPath( '30360-02.html' )
                }

                break

            case ROSELYN:
            case KRISTIN:
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItems( npcMap[ data.characterNpcId ] ) ) {
                    await QuestHelper.takeSingleItem( player, npcMap[ data.characterNpcId ], -1 )
                    await QuestHelper.rewardSingleItem( player, OLD_BRONZE_SWORD, 1 )

                    if ( QuestHelper.getQuestItemsCount( player, OLD_BRONZE_SWORD ) >= 2 ) {
                        state.setConditionWithSound( 4, true )
                    }

                    return this.getPath( `${ data.characterNpcId }-01.html` )
                }

                if ( !QuestHelper.hasQuestItems( npcMap[ data.characterNpcId ] ) && QuestHelper.hasQuestItem( player, OLD_BRONZE_SWORD ) ) {
                    return this.getPath( `${ data.characterNpcId }-02.html` )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}