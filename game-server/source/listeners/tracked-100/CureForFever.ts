import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'

import _ from 'lodash'

const ELLIAS = 30050
const YOHANES = 30032

const mobIds = [
    20103, // Giant Spider
    20106, // Talon Spider
    20108, // Blade Spider
]

const ROUND_SHIELD = 102
const POISON_SAC = 703
const FEVER_MEDICINE = 704

const minimumLevel = 15
const poisonSackThreshold = 0

export class CureForFever extends ListenerLogic {
    constructor() {
        super( 'Q00151_CureForFever', 'listeners/tracked/CureForFever.ts' )
        this.questId = 151
        this.questItemIds = [ POISON_SAC, FEVER_MEDICINE ]
    }

    getAttackableKillIds(): Array<number> {
        return mobIds
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00151_CureForFever'
    }

    getQuestStartIds(): Array<number> {
        return [ ELLIAS ]
    }

    getTalkIds(): Array<number> {
        return [ ELLIAS, YOHANES ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )
        if ( !state
                || !state.isCondition( 1 )
                || _.random( 5 ) > QuestHelper.getAdjustedChance( POISON_SAC, poisonSackThreshold, data.isChampion ) ) {
            // TODO : implement me
        }

        await QuestHelper.rewardSingleQuestItem( L2World.getPlayer( data.playerId ), POISON_SAC, 1, data.isChampion )
        state.setConditionWithSound( 2, true )

        return
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( state && data.eventName === '30050-03.htm' ) {
            state.startQuest()
            return this.getPath( data.eventName )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ELLIAS:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30050-02.htm' : '30050-01.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, FEVER_MEDICINE ) ) {
                            await QuestHelper.rewardSingleItem( player, ROUND_SHIELD, 1 )
                            await QuestHelper.addExpAndSp( player, 13106, 613 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.LAST_DUTY_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000, null ) )

                            return this.getPath( '30050-06.html' )
                        }

                        if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, POISON_SAC ) ) {
                            return this.getPath( '30050-05.html' )
                        }

                        return this.getPath( '30050-04.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case YOHANES:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, POISON_SAC ) ) {
                        state.setConditionWithSound( 3, true )
                        await QuestHelper.takeSingleItem( player, POISON_SAC, -1 )
                        await QuestHelper.giveSingleItem( player, FEVER_MEDICINE, 1 )

                        return this.getPath( '30032-01.html' )
                    }

                    if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, FEVER_MEDICINE ) ) {
                        return this.getPath( '30032-02.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}