import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const KANIS = 32264
const PARME = 32271

const ECHO_CRYSTAL_OF_FREE_THOUGHT = 9783
const PARMES_LETTER = 9784
const FIRE_STONE = 9546

const INSTANCE_EXIT = new Location( 143281, 148843, -12004 )
const minimumLevel = 78

export class BirdInACage extends ListenerLogic {
    constructor() {
        super( 'Q00131_BirdInACage', 'listeners/tracked/BirdInACage.ts' )
        this.questId = 131
        this.questItemIds = [ ECHO_CRYSTAL_OF_FREE_THOUGHT, PARMES_LETTER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00131_BirdInACage'
    }

    getQuestStartIds(): Array<number> {
        return [ KANIS ]
    }

    getTalkIds(): Array<number> {
        return [ KANIS, PARME ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32264-04.html':
                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    break
                }

                return

            case '32264-06.html':
                if ( state.isCondition( 1 ) ) {
                    break
                }

                return

            case '32264-07.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2 )
                    break
                }

                break

            case '32264-09.html':
            case '32264-10.html':
            case '32264-11.html':
                if ( state.isCondition( 2 ) ) {
                    break
                }

                return

            case '32264-12.html':
                if ( state.isCondition( 2 ) ) {
                    await QuestHelper.giveSingleItem( player, ECHO_CRYSTAL_OF_FREE_THOUGHT, 1 )
                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '32264-14.html':
            case '32264-15.html':
                if ( state.isCondition( 3 ) ) {
                    break
                }

                return

            case '32264-17.html':
                if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, PARMES_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, PARMES_LETTER, -1 )
                    state.setConditionWithSound( 5 )

                    break
                }

                return

            case '32264-19.html':
                if ( state.isCondition( 5 ) && QuestHelper.hasQuestItem( player, ECHO_CRYSTAL_OF_FREE_THOUGHT ) ) {
                    await QuestHelper.addExpAndSp( player, 250677, 25019 )
                    await QuestHelper.rewardSingleItem( player, FIRE_STONE + _.random( 4 ), 4 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case '32271-03.html':
                if ( state.isCondition( 3 ) ) {
                    break
                }

                return

            case '32271-04.html':
                if ( state.isCondition( 3 ) ) {
                    await QuestHelper.giveSingleItem( player, PARMES_LETTER, 1 )

                    state.setConditionWithSound( 4, true )
                    player.setInstanceId( 0 )
                    await player.teleportToLocation( INSTANCE_EXIT, true )

                    break
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === KANIS ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '32264-01.htm' : '32264-02.html' )
                }

                break

            case QuestStateValues.STARTED:
                if ( data.characterNpcId === KANIS ) {
                    switch ( state.getCondition() ) {
                        case 1:
                            return this.getPath( '32264-05.html' )

                        case 2:
                            return this.getPath( '32264-08.html' )

                        case 3:
                            return this.getPath( '32264-13.html' )

                        case 4:
                            return this.getPath( '32264-16.html' )

                        case 5:
                            return this.getPath( '32264-18.html' )
                    }

                    return
                }

                if ( data.characterNpcId === PARME ) {
                    let value = state.getCondition()
                    if ( value < 3 ) {
                        return this.getPath( '32271-01.html' )
                    }

                    if ( value === 3 ) {
                        return this.getPath( '32271-02.html' )
                    }

                    return
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}