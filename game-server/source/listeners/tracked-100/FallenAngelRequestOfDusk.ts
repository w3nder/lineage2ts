import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

const TOBIAS = 30297
const CASIAN = 30612
const NATOOLS = 30894
const ROCK = 32368
const ANGEL = 32369

const SEALED_PROPHECY_PATH_OF_THE_GOD = 10354
const PROPHECY_PATH_OF_THE_GOD = 10355
const EMPTY_SOUND_CRYSTAL = 10356
const ANGEL_MEDICINE = 10357
const ANGELS_MESSAGE = 10358

const maximumLevel = 43
let isAngelSpawned = false

const variableNames = {
    talk: 't'
}

export class FallenAngelRequestOfDusk extends ListenerLogic {
    constructor() {
        super( 'Q00143_FallenAngelRequestOfDusk', 'listeners/tracked/FallenAngelRequestOfDusk.ts' )
        this.questId = 143
        this.questItemIds = [
            SEALED_PROPHECY_PATH_OF_THE_GOD,
            PROPHECY_PATH_OF_THE_GOD,
            EMPTY_SOUND_CRYSTAL,
            ANGEL_MEDICINE,
            ANGELS_MESSAGE,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00143_FallenAngelRequestOfDusk'
    }

    getTalkIds(): Array<number> {
        return [
            NATOOLS,
            TOBIAS,
            CASIAN,
            ROCK,
            ANGEL,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case '30894-02.html':
            case '30297-04.html':
            case '30612-05.html':
            case '30612-06.html':
            case '30612-07.html':
            case '30612-08.html':
            case '32369-04.html':
            case '32369-05.html':
            case '32369-07.html':
            case '32369-08.html':
            case '32369-09.html':
            case '32369-10.html':
                break

            case '30894-01.html':
                state.startQuest()
                break

            case '30894-03.html':
                state.setConditionWithSound( 2, true )
                await QuestHelper.giveSingleItem( player, SEALED_PROPHECY_PATH_OF_THE_GOD, 1 )
                break

            case '30297-03.html':
                await QuestHelper.takeSingleItem( player, SEALED_PROPHECY_PATH_OF_THE_GOD, -1 )
                state.setVariable( variableNames.talk, 1 )
                break

            case '30297-05.html':
                state.unsetVariable( variableNames.talk )
                state.setConditionWithSound( 3, true )

                await QuestHelper.giveMultipleItems( player, 1, PROPHECY_PATH_OF_THE_GOD, EMPTY_SOUND_CRYSTAL )

                break

            case '30612-03.html':
                await QuestHelper.giveSingleItem( player, PROPHECY_PATH_OF_THE_GOD, -1 )
                state.setVariable( variableNames.talk, 1 )
                break

            case '30612-09.html':
                state.unsetVariable( variableNames.talk )
                state.setConditionWithSound( 4, true )

                await QuestHelper.giveSingleItem( player, ANGEL_MEDICINE, 1 )
                break

            case '32368-04.html':
                if ( isAngelSpawned ) {
                    return this.getPath( '32368-03.html' )
                }

                QuestHelper.addGenericSpawn( null, ANGEL, npc.getX() + 100, npc.getY() + 100, npc.getZ(), 0, false, 120000 )
                this.startQuestTimer( 'despawn', 120000, null, data.playerId )
                isAngelSpawned = true
                break

            case '32369-03.html':
                await QuestHelper.takeSingleItem( player, ANGEL_MEDICINE, -1 )
                state.setVariable( variableNames.talk, 1 )
                break

            case '32369-06.html':
                state.setVariable( variableNames.talk, 2 )
                break

            case '32369-11.html':
                state.unsetVariable( variableNames.talk )

                await QuestHelper.takeSingleItem( player, EMPTY_SOUND_CRYSTAL, -1 )
                await QuestHelper.giveSingleItem( player, ANGELS_MESSAGE, 1 )

                state.setConditionWithSound( 5, true )
                await npc.deleteMe()
                isAngelSpawned = false

                break

            case 'despawn':
                if ( isAngelSpawned ) {
                    isAngelSpawned = false
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case NATOOLS:
                switch ( state.getState() ) {
                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30894-01.html' )
                        }

                        return this.getPath( '30894-04.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case TOBIAS:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30297-01.html' )

                    case 2:
                        return this.getPath( state.getVariable( variableNames.talk ) ? '30297-04.html' : '30297-02.html' )

                    case 3:
                    case 4:
                        return this.getPath( '30297-06.html' )

                    case 5:
                        await QuestHelper.giveAdena( player, 89046, true )
                        if ( player.getLevel() <= maximumLevel ) {
                            await QuestHelper.addExpAndSp( player, 223036, 13901 )
                        }

                        await state.exitQuest( false, true )

                        return this.getPath( '30297-07.html' )
                }

                break

            case CASIAN:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                    case 2:
                        return this.getPath( '30612-01.html' )

                    case 3:
                        return this.getPath( state.getVariable( variableNames.talk ) ? '30612-04.html' : '30612-02.html' )
                }

                return this.getPath( '30612-10.html' )

            case ROCK:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                    case 2:
                    case 3:
                        return this.getPath( '32368-01.html' )

                    case 4:
                        return this.getPath( '32368-02.html' )

                    case 5:
                        return this.getPath( '32368-05.html' )
                }

                break

            case ANGEL:
                if ( !state.isStarted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                    case 2:
                    case 3:
                        return this.getPath( '32369-01.html' )

                    case 4:
                        switch ( state.getVariable( variableNames.talk ) ) {
                            case 1:
                                return this.getPath( '32369-04.html' )

                            case 2:
                                return this.getPath( '32369-07.html' )

                            default:
                                return this.getPath( '32369-02.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}