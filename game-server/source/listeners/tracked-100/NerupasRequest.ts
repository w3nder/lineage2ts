import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const NERUPA = 30370
const UNOREN = 30147
const CREAMEES = 30149
const JULIA = 30152

const SILVERY_SPIDERSILK = 1026
const UNOS_RECEIPT = 1027
const CELS_TICKET = 1028
const NIGHTSHADE_LEAF = 1029

const LESSER_HEALING_POTION = 1060

const minimumLevel = 3

export class NerupasRequest extends ListenerLogic {
    constructor() {
        super( 'Q00160_NerupasRequest', 'listeners/tracked/NerupasRequest.ts' )
        this.questId = 160
        this.questItemIds = [
            SILVERY_SPIDERSILK,
            UNOS_RECEIPT,
            CELS_TICKET,
            NIGHTSHADE_LEAF,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00160_NerupasRequest'
    }

    getQuestStartIds(): Array<number> {
        return [ NERUPA ]
    }

    getTalkIds(): Array<number> {
        return [ NERUPA, UNOREN, CREAMEES, JULIA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || data.eventName !== '30370-04.htm' ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        state.startQuest()
        if ( !QuestHelper.hasQuestItem( player, SILVERY_SPIDERSILK ) ) {
            await QuestHelper.giveSingleItem( player, SILVERY_SPIDERSILK, 1 )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === NERUPA ) {
                    if ( player.getRace() !== Race.ELF ) {
                        return this.getPath( '30370-01.htm' )
                    }

                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30370-02.htm' )
                    }

                    return this.getPath( '30370-03.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case NERUPA:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, SILVERY_SPIDERSILK, UNOS_RECEIPT, CELS_TICKET ) ) {
                            return this.getPath( '30370-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, NIGHTSHADE_LEAF ) ) {
                            await QuestHelper.rewardSingleItem( player, LESSER_HEALING_POTION, 5 )
                            await QuestHelper.addExpAndSp( player, 1000, 0 )
                            await state.exitQuest( false, true )

                            return this.getPath( '30370-06.html' )
                        }

                        break

                    case UNOREN:
                        if ( QuestHelper.hasQuestItem( player, SILVERY_SPIDERSILK ) ) {
                            await QuestHelper.takeSingleItem( player, SILVERY_SPIDERSILK, -1 )
                            if ( !QuestHelper.hasQuestItem( player, UNOS_RECEIPT ) ) {
                                await QuestHelper.giveSingleItem( player, UNOS_RECEIPT, 1 )
                            }

                            state.setConditionWithSound( 2, true )
                            return this.getPath( '30147-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, UNOS_RECEIPT ) ) {
                            return this.getPath( '30147-02.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, NIGHTSHADE_LEAF ) ) {
                            return this.getPath( '30147-03.html' )
                        }

                        break

                    case CREAMEES:
                        if ( QuestHelper.hasQuestItem( player, UNOS_RECEIPT ) ) {
                            await QuestHelper.takeSingleItem( player, UNOS_RECEIPT, -1 )

                            if ( !QuestHelper.hasQuestItem( player, CELS_TICKET ) ) {
                                await QuestHelper.giveSingleItem( player, CELS_TICKET, 1 )
                            }

                            state.setConditionWithSound( 3, true )
                            return this.getPath( '30149-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, CELS_TICKET ) ) {
                            return this.getPath( '30149-02.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, NIGHTSHADE_LEAF ) ) {
                            return this.getPath( '30149-03.html' )
                        }

                        break

                    case JULIA:
                        if ( QuestHelper.hasQuestItem( player, CELS_TICKET ) ) {
                            await QuestHelper.takeSingleItem( player, CELS_TICKET, -1 )

                            if ( !QuestHelper.hasQuestItem( player, NIGHTSHADE_LEAF ) ) {
                                await QuestHelper.giveSingleItem( player, NIGHTSHADE_LEAF, 1 )
                            }


                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30152-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, NIGHTSHADE_LEAF ) ) {
                            return this.getPath( '30152-02.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}