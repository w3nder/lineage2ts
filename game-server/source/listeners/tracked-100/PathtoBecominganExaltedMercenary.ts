import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const npcIds = [
    36481,
    36482,
    36483,
    36484,
    36485,
    36486,
    36487,
    36488,
    36489,
]

const ELITE_CERTIFICATE = 13767
const TOP_ELITE_CERTIFICATE = 13768

export class PathtoBecominganExaltedMercenary extends ListenerLogic {
    constructor() {
        super( 'Q00148_PathtoBecominganExaltedMercenary', 'listeners/tracked/PathtoBecominganExaltedMercenary.ts' )
        this.questId = 148
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00148_PathtoBecominganExaltedMercenary'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return this.getPath( data.eventName )
        }

        if ( data.eventName === 'exalted-00b.htm' ) {
            let player = L2World.getPlayer( data.playerId )
            await QuestHelper.giveSingleItem( player, ELITE_CERTIFICATE, 1 )
        }

        if ( data.eventName === 'exalted-03.htm' ) {
            state.startQuest()
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getClan() && player.getClan().getCastleId() > 0 ) {
                    return this.getPath( 'castle.htm' )
                }

                if ( await QuestHelper.hasQuestItem( player, ELITE_CERTIFICATE ) ) {
                    return this.getPath( 'exalted-01.htm' )
                }

                return this.getPath( player.hasQuestCompleted( 'Q00147_PathtoBecominganEliteMercenary' ) ? 'exalted-00a.htm' : 'exalted-00.htm' )

            case QuestStateValues.STARTED:
                if ( state.getCondition() < 4 ) {
                    return this.getPath( 'exalted-04.htm' )
                }

                if ( state.isCondition( 4 ) ) {
                    await QuestHelper.takeSingleItem( player, ELITE_CERTIFICATE, -1 )
                    await QuestHelper.giveSingleItem( player, TOP_ELITE_CERTIFICATE, 1 )
                    await state.exitQuest( false )

                    return this.getPath( 'exalted-05.htm' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}