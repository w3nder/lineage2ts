import { NpcRouteListener } from '../../helpers/NpcRouteListener'

export class TradekeeperJaradine extends NpcRouteListener {
    constructor() {
        super( 'TradekeeperJaradine', 'listeners/npcs/routes/TradekeeperJaradine.ts' )
    }

    stringIdMap: Record<number, number> = {
        3: 1010208,
        4: 1010209
    }

    getNpcIds(): Array<number> {
        return [ 31359 ]
    }

    isRunningRoute(): boolean {
        return false
    }
}