import { NpcRouteListener } from '../../helpers/NpcRouteListener'

export class PorterTate extends NpcRouteListener {
    constructor() {
        super( 'PorterTate', 'listeners/npcs/routes/PorterTate.ts' )
    }

    stringIdMap: Record<number, number> = {
        1: 1010218,
        6: 1010219,
        11: 1010220
    }

    getNpcIds(): Array<number> {
        return [ 31362 ]
    }

    isRunningRoute(): boolean {
        return false
    }
}