import { NpcRouteListener } from '../../helpers/NpcRouteListener'

export class ProphetGiran extends NpcRouteListener {
    constructor() {
        super( 'ProphetGiran', 'listeners/npcs/routes/ProphetGiran.ts' )
    }

    stringIdMap: Record<number, number> = {
        1: 1010224,
        2: 1010225,
        4: 1010226
    }

    getNpcIds(): Array<number> {
        return [ 4310 ]
    }

    isRunningRoute(): boolean {
        return false
    }
}