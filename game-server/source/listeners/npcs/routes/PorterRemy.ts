import { NpcRouteListener } from '../../helpers/NpcRouteListener'
import { NpcRouteNodeArrivedEvent } from '../../../gameService/models/events/EventType'
import Timeout = NodeJS.Timeout
import { L2World } from '../../../gameService/L2World'
import { L2Npc } from '../../../gameService/models/actor/L2Npc'

export class PorterRemy extends NpcRouteListener {
    stringIdMap : Record<number, number> = {
        3: 1010202,
        7: 1010203,
        12: 1010204,
        15: 1010201
    }

    counter: number = 0
    repeatTimer: Timeout
    npcObjectId: number

    constructor() {
        super( 'PorterRemy', 'listeners/npcs/routes/PorterRemy.ts' )
    }

    getNpcIds(): Array<number> {
        return [
            31356
        ]
    }

    isRunningRoute(): boolean {
        return true
    }

    onNpcRouteNodeArrived( data: NpcRouteNodeArrivedEvent ): void {
        this.npcObjectId = data.characterId
        this.broadcastNodeMessage( data )

        if ( data.nodeIndex === 12 ) {
            this.counter++

            if ( this.repeatTimer ) {
                this.repeatTimer.refresh()
                return
            }

            this.repeatTimer = setTimeout( this.onRepeatTimer.bind( this ), 3000 )
        }
    }

    onRepeatTimer() : void {
        if ( this.counter < 3 ) {
            let npc = L2World.getObjectById( this.npcObjectId ) as L2Npc

            if ( !npc ) {
                this.counter = 0

                return
            }

            this.broadcastNpcSayId( npc, 1010204 )

            this.counter++
            this.repeatTimer.refresh()

            return
        }

        this.counter = 0
    }
}