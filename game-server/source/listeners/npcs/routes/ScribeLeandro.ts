import { NpcRouteListener } from '../../helpers/NpcRouteListener'

export class ScribeLeandro extends NpcRouteListener {
    stringIdMap : Record<number, number> = {
        5: 1010205,
        7: 1010206,
        10: 1010207
    }

    constructor() {
        super( 'ScribeLeandro', 'listeners/npcs/routes/ScribeLeandro.ts' )
    }

    getNpcIds(): Array<number> {
        return [ 31357 ]
    }

    isRunningRoute(): boolean {
        return false
    }
}