import { NpcRouteListener } from '../../helpers/NpcRouteListener'

export class ProphetGludio extends NpcRouteListener {
    constructor() {
        super( 'ProphetGludio', 'listeners/npcs/routes/ProphetGludio.ts' )
    }

    stringIdMap: Record<number, number> = {
        1: 1010221,
        3: 1010222,
        6: 1010223
    }

    getNpcIds(): Array<number> {
        return [ 4309 ]
    }

    isRunningRoute(): boolean {
        return false
    }
}