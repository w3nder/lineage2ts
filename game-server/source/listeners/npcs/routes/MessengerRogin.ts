import { NpcRouteListener } from '../../helpers/NpcRouteListener'

export class MessengerRogin extends NpcRouteListener {
    constructor() {
        super( 'MessengerRogin', 'listeners/npcs/routes/MessengerRogin.ts' )
    }

    stringIdMap: Record<number, number> = {
        3: 1010215,
        6: 1010216,
        7: 1010217
    }

    getNpcIds(): Array<number> {
        return [ 31363 ]
    }

    isRunningRoute(): boolean {
        return true
    }
}