import { NpcRouteListener } from '../../helpers/NpcRouteListener'

export class TetrarchAgentAlhena extends NpcRouteListener {
    constructor() {
        super( 'TetrarchAgentAlhena', 'listeners/npcs/routes/TetrarchAgentAlhena.ts' )
    }

    stringIdMap: Record<number, number> = {
        3: 1010212,
        9: 1010213
    }

    getNpcIds(): Array<number> {
        return [ 31360 ]
    }

    isRunningRoute(): boolean {
        return false
    }
}