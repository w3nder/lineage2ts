import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { HennaEquipList } from '../../gameService/packets/send/HennaEquipList'
import { HennaRemoveList } from '../../gameService/packets/send/HennaRemoveList'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'

const npcIds: Array<number> = [
    31046, // Marsden
    31047, // Kell
    31048, // McDermott
    31049, // Pepper
    31050, // Thora
    31051, // Keach
    31052, // Heid
    31053, // Kidder
    31264, // Olsun
    31308, // Achim
    31953, // Rankar
]

export class SymbolMaker extends ListenerLogic {
    constructor() {
        super( 'SymbolMaker', 'listeners/npcs/SymbolMaker.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/SymbolMaker'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( 'symbol_maker.htm' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        switch ( data.eventName ) {
            case 'symbol_maker.htm':
            case 'symbol_maker-1.htm':
            case 'symbol_maker-2.htm':
            case 'symbol_maker-3.htm':
                return this.getPath( data.eventName )

            case 'Draw':
                PacketDispatcher.sendOwnedData( data.playerId, HennaEquipList( data.playerId ) )
                return

            case 'Remove':
                PacketDispatcher.sendOwnedData( data.playerId, HennaRemoveList( data.playerId ) )
                return
        }

        return null
    }
}