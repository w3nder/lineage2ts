import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { ClassId } from '../../gameService/models/base/ClassId'
import { ConfigManager } from '../../config/ConfigManager'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { L2SkillLearn } from '../../gameService/models/L2SkillLearn'
import { Skill } from '../../gameService/models/Skill'
import { DataManager } from '../../data/manager'
import { AcquireSkillList } from '../../gameService/packets/send/builder/AcquireSkillList'
import { AcquireSkillType } from '../../gameService/enums/AcquireSkillType'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { InventoryAction } from '../../gameService/enums/InventoryAction'
import aigle from 'aigle'
import _ from 'lodash'
import { ItemDefinition } from '../../gameService/interface/ItemDefinition'

const trainers : Array<number> = [
    30022, 30030, 30032, 30036, 30067, 30068, 30116, 30117, 30118, 30119,
    30144, 30145, 30188, 30194, 30293, 30330, 30375, 30377, 30464, 30473,
    30476, 30680, 30701, 30720, 30721, 30858, 30859, 30860, 30861, 30864,
    30906, 30908, 30912, 31280, 31281, 31287, 31329, 31330, 31335, 31969,
    31970, 31976, 32155, 32162
]

const minimumLevel = 76
const minimumClassLevel = 3
const classIdMap : { [ classId: number ] : number } = {
    97: 15307,
    105: 15308,
    112: 15309
}

export class HealerTrainer extends ListenerLogic {
    constructor() {
        super( 'HealerTrainer', 'listeners/npcs/HealerTrainer.ts' )
    }

    getQuestStartIds(): Array<number> {
        return trainers
    }

    getTalkIds(): Array<number> {
        return trainers
    }

    getApproachedForTalkIds(): Array<number> {
        return trainers
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30864.html':
            case '30864-1.html':
                return this.getPath( data.eventName )

            case 'SkillTransfer':
                return this.getPath( 'main.html' )

            case 'SkillTransferLearn':
                if ( !DataManager.getNpcData().getTemplate( data.characterNpcId ).canTeach( ClassId.getClassIdByIdentifier( player.getClassId() ) ) ) {
                    return this.getPath( `${npc.getId()}-noteach.html` )
                }

                if ( player.getLevel() < minimumLevel || ClassId.getLevelByClassId( player.getClassId() ) < minimumClassLevel ) {
                    return this.getPath( 'learn-lowlevel.html' )
                }

                let skillList : AcquireSkillList = new AcquireSkillList( AcquireSkillType.Transfer )

                SkillCache.getAvailableTransferSkills( player ).forEach( ( skill: L2SkillLearn ) => {
                    if ( SkillCache.getSkill( skill.getSkillId(), skill.getSkillLevel() ) ) {
                        skillList.addSkill( skill.getSkillId(), skill.getSkillLevel(), skill.getLevelUpSp(), 0 )
                    }
                } )

                if ( skillList.getCount() > 0 ) {
                    player.sendOwnedData( skillList.getBuffer() )
                    return
                }

                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_MORE_SKILLS_TO_LEARN ) )
                return

            case 'SkillTransferCleanse':
                if ( !DataManager.getNpcData().getTemplate( data.characterNpcId ).canTeach( ClassId.getClassIdByIdentifier( player.getClassId() ) ) ) {
                    return this.getPath( 'cleanse-no.html' )
                }

                if ( player.getLevel() < minimumClassLevel || ClassId.getLevelByClassId( player.getClassId() ) < minimumClassLevel ) {
                    return this.getPath( 'cleanse-no.html' )
                }

                if ( player.getAdena() < ConfigManager.character.getFeeDeleteTransferSkills() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_RESET_SKILL_LINK_BECAUSE_NOT_ENOUGH_ADENA ) )
                    return
                }

                if ( this.hasTransferSkillItems( player ) ) {
                    // Come back when you have used all transfer skill items for this class.
                    return this.getPath( 'cleanse-no_skills.html' )
                }

                let hasSkills = false
                let skillMap : { [key: number]: L2SkillLearn } = DataManager.getSkillTreesData().getTransferSkillTrees()[ player.getClassId() ]

                await aigle.resolve( skillMap ).eachSeries( async ( skillLearn: L2SkillLearn ) => {
                    let skill : Skill = player.getKnownSkill( skillLearn.getSkillId() )
                    if ( skill ) {
                        await player.removeSkill( skill )

                        await aigle.resolve( skillLearn.getRequiredItems() ).eachSeries( async ( item: ItemDefinition ) => {
                            return player.addItem( item.id, item.count, -1, npc.getObjectId(), InventoryAction.QuestReward )
                        } )

                        hasSkills = true
                    }
                } )

                if ( hasSkills ) {
                    await player.reduceAdena( ConfigManager.character.getFeeDeleteTransferSkills(), true, 'Quest:HealerTrainer' )
                }

                return
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Trainers/HealerTrainer'
    }

    hasTransferSkillItems( player: L2PcInstance ) : boolean {
        let itemId = _.get( classIdMap, player.getClassId(), -1 )
        return player.getInventory().getInventoryItemCount( itemId, -1 ) > 0
    }
}