import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestHelper } from '../helpers/QuestHelper'
import { DataManager } from '../../data/manager'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { AbstractVariablesMap } from '../../gameService/variables/AbstractVariablesManager'
import _ from 'lodash'

const enum ListenerValues {
    NpcId = 32783,
    NevitVoiceId = 17094,
    AdenaPrice = 100000,
}

const hourglasses: Array<Array<number>> = [
    [ 17095, 17096, 17097, 17098, 17099 ],
    [ 17100, 17101, 17102, 17103, 17104 ],
    [ 17105, 17106, 17107, 17108, 17109 ],
    [ 17110, 17111, 17112, 17113, 17114 ],
    [ 17115, 17116, 17117, 17118, 17119 ],
    [ 17120, 17121, 17122, 17123, 17124 ],
    [ 17125, 17126, 17127, 17128, 17129 ],
]

const hourglassPrice: Array<number> = [
    4000,
    30000,
    110000,
    310000,
    970000,
    2160000,
    5000000,
]

interface PriestOfBlessingData extends AbstractVariablesMap {
    voiceTime: number
    hourglassTimes: Array<number>
}

const enum EventNames {
    BuyVoice = 'buy_voice',
    BuyHourglass = 'buy_hourglass'
}

export class PriestOfBlessing extends ListenerLogic {

    constructor() {
        super( 'PriestOfBlessing', 'listeners/npcs/PriestOfBlessing.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ ListenerValues.NpcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/PriestOfBlessing'
    }

    getQuestStartIds(): Array<number> {
        return [ ListenerValues.NpcId ]
    }

    getTalkIds(): Array<number> {
        return [ ListenerValues.NpcId ]
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let html: string = DataManager.getHtmlData().getItem( this.getPath( '32783.htm' ) )
        return html.replace( /%donate%/g, hourglassPrice[ this.getIndex( player.getLevel() ) ].toString() )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName.toLowerCase() ) {
            case EventNames.BuyVoice:
                if ( player.getAdena() >= ListenerValues.AdenaPrice ) {
                    let playerData: PriestOfBlessingData = this.getData( data.playerId )
                    let { voiceTime } = playerData

                    if ( Date.now() > voiceTime ) {
                        await QuestHelper.takeSingleItem( player, ItemTypes.Adena, ListenerValues.AdenaPrice )
                        await QuestHelper.giveSingleItem( player, ListenerValues.NevitVoiceId, 1 )

                        playerData.voiceTime = Date.now() + GeneralHelper.hoursToMillis( 20 )
                        PlayerVariablesManager.set( data.playerId, this.getName(), playerData )

                        return
                    }

                    let [ hours, minutes ] = GeneralHelper.getTimeDifference( voiceTime - Date.now() )

                    let message = new SystemMessageBuilder( SystemMessageIds.AVAILABLE_AFTER_S1_S2_HOURS_S3_MINUTES )
                        .addString( 'Nevit\'s Hourglass' )
                        .addNumber( hours )
                        .addNumber( minutes )
                        .getBuffer()

                    player.sendOwnedData( message )

                    return
                }

                return this.getPath( '32783-adena.htm' )

            case EventNames.BuyHourglass:
                let index = this.getIndex( player.getLevel() )
                let price = hourglassPrice[ index ]

                if ( player.getAdena() >= price ) {
                    let playerData: PriestOfBlessingData = this.getData( data.playerId )
                    let hourglassTime = playerData.hourglassTimes[ index ]

                    if ( Date.now() > hourglassTime ) {
                        let hourglassItemId = _.sample( hourglasses[ index ] )
                        await QuestHelper.takeSingleItem( player, ItemTypes.Adena, price )
                        await QuestHelper.giveSingleItem( player, hourglassItemId, 1 )

                        playerData.hourglassTimes[ index ] = Date.now() + GeneralHelper.hoursToMillis( 20 )
                        PlayerVariablesManager.set( data.playerId, this.getName(), playerData )
                        return
                    }

                    let remainingTime = ( hourglassTime - Date.now() ) / 1000
                    let hours = Math.floor( remainingTime / 3600 )
                    let minutes = Math.floor( ( remainingTime % 3600 ) / 60 )

                    let message = new SystemMessageBuilder( SystemMessageIds.AVAILABLE_AFTER_S1_S2_HOURS_S3_MINUTES )
                        .addString( 'Nevit\'s Hourglass' )
                        .addNumber( hours )
                        .addNumber( minutes )
                        .getBuffer()

                    player.sendOwnedData( message )

                    return
                }

                return this.getPath( '32783-adena.htm' )
        }

        return this.getPath( event )
    }

    getIndex( level: number ): number {
        if ( level < 20 ) {
            return 0
        }

        if ( level < 40 ) {
            return 1
        }

        if ( level < 52 ) {
            return 2
        }

        if ( level < 61 ) {
            return 3
        }

        if ( level < 76 ) {
            return 4
        }

        if ( level < 80 ) {
            return 5
        }

        if ( level < 86 ) {
            return 6
        }

        return 6
    }

    getData( playerId: number ): PriestOfBlessingData {
        let value = PlayerVariablesManager.get( playerId, this.getName() ) as PriestOfBlessingData
        if ( !value ) {
            return {
                hourglassTimes: [ 0, 0, 0, 0, 0, 0, 0 ],
                voiceTime: 0
            }
        }

        return value
    }
}