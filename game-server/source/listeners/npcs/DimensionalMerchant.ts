import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { ExGetDimensionalItemList } from '../../gameService/packets/send/ExGetDimensionalItemList'
import { DimensionalTransferManager } from '../../gameService/cache/DimensionalTransferManager'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'

const nameToCouponId: { [ eventName: string ]: number } = {
    'whiteWeasel': 13017,
    'fairyPrincess': 13018,
    'wildBeast': 13019,
    'foxShaman': 13020,
    'toyKnight': 14061,
    'spiritShaman': 14062,
    'turtleAscetic': 14064,
    'desheloph': 20915,
    'hyum': 20916,
    'lekang': 20917,
    'lilias': 20918,
    'lapham': 20919,
    'mafum': 20920,
}

const npcId: number = 32478

export class DimensionalMerchant extends ListenerLogic {
    constructor() {
        super( 'DimensionalMerchant', 'listeners/npcs/DimensionalMerchant.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [
            npcId
        ]
    }

    getPathPrefix(): string {
        return 'overrides/html/npc/dimensionalMerchant'
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( '32478.htm' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case 'getDimensonalItem':
                if ( !DimensionalTransferManager.hasItems( data.playerId ) ) {
                    PacketDispatcher.sendOwnedData( data.playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.THERE_ARE_NO_MORE_D_ITEMS_TO_BE_FOUND ) )
                    return
                }

                PacketDispatcher.sendOwnedData( data.playerId, ExGetDimensionalItemList( data.playerId ) )
                return

            case 'whiteWeasel':
            case 'fairyPrincess':
            case 'wildBeast':
            case 'foxShaman':
                return this.rewardItem( data, 13273, 13383 )

            case 'toyKnight':
            case 'spiritShaman':
            case 'turtleAscetic':
                return this.rewardItem( data, 14065, 14074 )

            case 'desheloph':
            case 'hyum':
            case 'lekang':
            case 'lilias':
            case 'lapham':
            case 'mafum':
                return this.rewardItem( data, 20914, 22240 )
        }

        return this.getPath( data.eventName )
    }

    async rewardItem( data: NpcGeneralEvent, couponId: number, eventCouponId: number ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let [ hasCoupon, hasEventCoupon ]: Array<boolean> = QuestHelper.hasEachQuestItem( player, couponId, eventCouponId )

        if ( !hasCoupon && !hasEventCoupon ) {
            return this.getPath( '32478-07.htm' )
        }

        let itemIdToRemove: number = hasCoupon ? couponId : eventCouponId

        await QuestHelper.takeSingleItem( player, itemIdToRemove, 1 )
        await QuestHelper.giveSingleItem( player, nameToCouponId[ data.eventName ], 1 )

        return this.getPath( '32478-08.htm' )
    }
}