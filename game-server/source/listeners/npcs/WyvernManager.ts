import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { DataManager } from '../../data/manager'
import { SevenSigns } from '../../gameService/directives/SevenSigns'
import { SevenSignsSeal, SevenSignsSide, SevenSignsValues } from '../../gameService/values/SevenSignsValues'
import { Fort } from '../../gameService/models/entity/Fort'
import { QuestHelper } from '../helpers/QuestHelper'
import { SiegableHall } from '../../gameService/models/entity/clanhall/SiegableHall'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const enum ManagerType {
    CASTLE,
    CLAN_HALL,
    FORT,
}

const CRYSTAL_B_GRADE = 1460
const WYVERN = 12621
const WYVERN_FEE = 25
const STRIDER_LVL = 55
const striders: Array<number> = [
    12526,
    12527,
    12528,
    16038,
    16039,
    16040,
    16068,
    13197,
]

const managerMap: { [ npcId: number ]: ManagerType } = {
    35101: ManagerType.CASTLE,
    35143: ManagerType.CASTLE,
    35185: ManagerType.CASTLE,
    35227: ManagerType.CASTLE,
    35275: ManagerType.CASTLE,
    35317: ManagerType.CASTLE,
    35364: ManagerType.CASTLE,
    35510: ManagerType.CASTLE,
    35536: ManagerType.CASTLE,
    35419: ManagerType.CLAN_HALL,
    35638: ManagerType.CLAN_HALL,
    36457: ManagerType.FORT,
    36458: ManagerType.FORT,
    36459: ManagerType.FORT,
    36460: ManagerType.FORT,
    36461: ManagerType.FORT,
    36462: ManagerType.FORT,
    36463: ManagerType.FORT,
    36464: ManagerType.FORT,
    36465: ManagerType.FORT,
    36466: ManagerType.FORT,
    36467: ManagerType.FORT,
    36468: ManagerType.FORT,
    36469: ManagerType.FORT,
    36470: ManagerType.FORT,
    36471: ManagerType.FORT,
    36472: ManagerType.FORT,
    36473: ManagerType.FORT,
    36474: ManagerType.FORT,
    36475: ManagerType.FORT,
    36476: ManagerType.FORT,
    36477: ManagerType.FORT,
}

const npcIds: Array<number> = _.keys( managerMap ).map( value => _.parseInt( value ) )

export class WyvernManager extends ListenerLogic {
    constructor() {
        super( 'WyvernManager', 'listeners/npcs/WyvernManager.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/WyvernManager'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( !this.isOwnerClan( npc, player ) ) {
            return this.getPath( 'wyvernmanager-02.html' )
        }

        if ( ConfigManager.castle.allowRideWyvernAlways() ) {
            return this.getFullHtmlForResidence( npc )
        }

        if ( managerMap[ npc.getId() ] === ManagerType.CASTLE
                && SevenSigns.isSealValidationPeriod()
                && SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) === SevenSignsSide.Dusk ) {
            return this.getPath( 'wyvernmanager-dusk.html' )
        }

        return this.getFullHtmlForResidence( npc )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        switch ( event ) {
            case 'Return':
                if ( !this.isOwnerClan( npc, player ) ) {
                    return this.getPath( 'wyvernmanager-02.html' )
                }

                if ( ConfigManager.castle.allowRideWyvernAlways() ) {
                    return this.getFullHtmlForResidence( npc )
                }

                if ( managerMap[ npc.getId() ] === ManagerType.CASTLE
                        && SevenSigns.isSealValidationPeriod()
                        && SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) === SevenSignsSide.Dusk ) {
                    return this.getPath( 'wyvernmanager-dusk.html' )
                }

                return this.getFullHtmlForResidence( npc )

            case 'Help':
                if ( managerMap[ npc.getId() ] === ManagerType.CASTLE ) {
                    return this.getFullHtmlWithFees( 'wyvernmanager-03.html' )
                }

                return this.getFullHtmlWithFees( 'wyvernmanager-03b.html' )

            case 'RideWyvern':
                if ( !ConfigManager.castle.allowRideWyvernAlways() ) {
                    if ( !ConfigManager.castle.allowRideWyvernDuringSiege() && ( this.isInSiege( npc ) || player.isInSiege() ) ) {
                        player.sendMessage( 'You cannot summon wyvern while in siege.' )
                        return
                    }

                    if ( managerMap[ npc.getId() ] === ManagerType.CASTLE
                            && SevenSigns.isSealValidationPeriod()
                            && SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) === SevenSignsSide.Dusk ) {
                        return this.getPath( 'wyvernmanager-dusk.html' )
                    }
                }

                return this.mountWyvern( npc, player )
        }
    }

    getFullHtmlForResidence( npc: L2Npc ): string {
        return this.getFullHtmlWithFees( 'wyvernmanager-01.html' ).replace( /%residence_name%/g, this.getResidenceName( npc ) )
    }

    getFullHtmlWithFees( path: string ): string {
        return DataManager.getHtmlData().getItem( this.getPath( path ) )
                .replace( /%wyvern_fee%/g, WYVERN_FEE.toString() )
                .replace( /%strider_level%/g, STRIDER_LVL.toString() )
    }

    getResidenceName( npc: L2Npc ): string {
        switch ( managerMap[ npc.getId() ] ) {
            case ManagerType.CASTLE:
                return npc.getCastle().getName()

            case ManagerType.CLAN_HALL:
                return npc.getConquerableHall().getName()

            case ManagerType.FORT:
                return npc.getFort().getName()
        }

        return ''
    }

    isInSiege( npc: L2Npc ): boolean {
        switch ( managerMap[ npc.getId() ] ) {
            case ManagerType.CASTLE: {
                return npc.getCastle().isSiegeActive()
            }
            case ManagerType.CLAN_HALL:
                let hall: SiegableHall = npc.getConquerableHall()
                return hall ? hall.isInSiege() : npc.getCastle().getSiege().isInProgress()

            case ManagerType.FORT:
                return npc.getFort().isSiegeActive()
        }

        return false
    }

    isOwnerClan( npc: L2Npc, player: L2PcInstance ): boolean {
        if ( !player.isClanLeader() ) {
            return false
        }

        switch ( managerMap[ npc.getId() ] ) {
            case ManagerType.CASTLE:
                if ( npc.getCastle() ) {
                    return player.getClanId() === npc.getCastle().getOwnerId()
                }

                return false

            case ManagerType.CLAN_HALL:
                if ( npc.getConquerableHall() ) {
                    return player.getClanId() === npc.getConquerableHall().getOwnerId()
                }

                return false

            case ManagerType.FORT:
                let fort: Fort = npc.getFort()
                if ( fort && fort.getOwnerClan() ) {
                    return player.getClanId() === npc.getFort().getOwnerClan().getId()
                }

                return false
        }

        return false
    }

    async mountWyvern( npc: L2Npc, player: L2PcInstance ): Promise<string> {
        if ( player.isMounted()
                && player.getMountLevel() >= STRIDER_LVL
                && striders.includes( player.getMountNpcId() ) ) {

            if ( this.isOwnerClan( npc, player ) && ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B_GRADE ) >= WYVERN_FEE ) ) {
                await QuestHelper.takeMultipleItems( player, CRYSTAL_B_GRADE, WYVERN_FEE )
                player.dismount()
                await player.mountWithId( WYVERN, 0, true )

                return this.getPath( 'wyvernmanager-04.html' )
            }
            return this.getFullHtmlWithFees( 'wyvernmanager-06.html' )
        }
        return this.getFullHtmlWithFees( 'wyvernmanager-05.html' )
    }
}