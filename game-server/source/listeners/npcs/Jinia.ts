import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

const npcId: number = 32781
const FROZEN_CORE = 15469
const BLACK_FROZEN_CORE = 15470
const minimumLevel = 82

export class Jinia extends ListenerLogic {
    constructor() {
        super( 'Jinia', 'listeners/npcs/Jinia.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Jinia'
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q10286_ReunionWithSirra' )
        if ( state && ( player.getLevel() >= minimumLevel ) ) {
            if ( state.isCompleted() ) {
                return this.getPath( '32781-02.html' )
            }

            if ( state.isCondition( 5 ) || state.isCondition( 6 ) ) {
                return this.getPath( '32781-09.html' )
            }
        }

        return this.getPath( '32781-01.html' )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        if ( event === 'check' ) {
            if ( QuestHelper.hasAtLeastOneQuestItem( player, FROZEN_CORE, BLACK_FROZEN_CORE ) ) {
                return this.getPath( '32781-03.html' )
            }

            let state: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q10286_ReunionWithSirra' )
            if ( state && state.isCompleted() ) {
                await QuestHelper.giveSingleItem( player, FROZEN_CORE, 1 )
            } else {
                await QuestHelper.giveSingleItem( player, BLACK_FROZEN_CORE, 1 )
            }


            return this.getPath( '32781-04.html' )
        }

        return this.getPath( event )
    }
}