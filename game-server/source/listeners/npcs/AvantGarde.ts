import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { Skill } from '../../gameService/models/Skill'
import { AcquireSkillType } from '../../gameService/enums/AcquireSkillType'
import { canTransform, RequestAcquireSkillVariables } from '../../gameService/packets/receive/RequestAcquireSkill'
import { MultisellCache } from '../../gameService/cache/MultisellCache'
import { ConfigManager } from '../../config/ConfigManager'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { L2SkillLearn } from '../../gameService/models/L2SkillLearn'
import { AcquireSkillList } from '../../gameService/packets/send/builder/AcquireSkillList'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { DataManager } from '../../data/manager'
import { L2World } from '../../gameService/L2World'
import aigle from 'aigle'
import _ from 'lodash'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'

const npcId: number = 32323
const itemIds: Array<number> = [
    10280, 10281, 10282, 10283, 10284, 10285, 10286, 10287, 10288, 10289, 10290, 10291, 10292, 10293, 10294, 10612,
]

export class AvantGarde extends ListenerLogic {
    constructor() {
        super( 'AvantGarde', 'listeners/npcs/AvantGarde.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.PlayerLearnSkill,
                method: this.onPlayerSkillLearnEvent.bind( this ),
                ids: [ npcId ],
            },
        ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/AvantGarde'
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( '32323-01.html' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( event ) {
            case '32323-02.html':
            case '32323-02a.html':
            case '32323-02b.html':
            case '32323-02c.html':
            case '32323-05.html':
            case '32323-05a.html':
            case '32323-05no.html':
            case '32323-06.html':
            case '32323-06no.html':
                return this.getPath( event )

            case 'LearnTransformationSkill':
                if ( canTransform( player ) ) {
                    this.showTransformSkillList( player )
                    return
                }

                return this.getPath( '32323-03.html' )

            case 'BuyTransformationItems':
                if ( canTransform( player ) ) {
                    MultisellCache.separateAndSend( 32323001, player, npc, false )
                    return
                }

                return this.getPath( '32323-04.html' )

            case 'LearnSubClassSkill':
                if ( !canTransform( player ) ) {
                    return this.getPath( '32323-04.html' )
                }
                if ( player.isSubClassActive() ) {
                    return this.getPath( '32323-08.html' )
                }

                let hasItems = _.some( itemIds, ( itemId: number ) => {
                    return !!player.getInventory().getItemByItemId( itemId )
                } )

                if ( hasItems ) {
                    this.showSubClassSkillList( player )
                    return
                }

                return this.getPath( '32323-08.html' )

            case 'CancelCertification':
                if ( _.isEmpty( player.getSubClasses() ) ) {
                    return this.getPath( '32323-07.html' )
                }

                if ( player.isSubClassActive() ) {
                    return this.getPath( '32323-08.html' )
                }

                if ( player.getAdena() < ConfigManager.character.getFeeDeleteSubClassSkills() ) {
                    return this.getPath( '32323-08no.html' )
                }

                let questVariableNames: Array<string> = RequestAcquireSkillVariables.flatMap( ( variablePrefix: string ) => {
                    return _.times( ConfigManager.character.getMaxSubclass(), ( index: number ) => {
                        return `${ variablePrefix }${ index + 1 }`
                    } )
                } )

                let variableValues = _.pick( PlayerVariablesManager.getData( player.getObjectId() ), questVariableNames ) as Record<string, string>
                let certifications: Record<string, string> = _.omitBy( variableValues, ( value: string ) => {
                    return _.isEmpty( value ) || value === '0'
                } )

                if ( _.isEmpty( certifications ) ) {
                    return this.getPath( '32323-10no.html' )
                }

                await aigle.resolve( certifications ).eachSeries( async ( variableValue: string, variableName: string ) => {
                    if ( variableValue.startsWith( '!' ) ) {
                        let skillId: number = _.parseInt( variableValue.substring( 1 ) )
                        let skill: Skill = SkillCache.getSkill( skillId, 1 )
                        if ( skill ) {
                            await player.removeSkill( skill )
                            PlayerVariablesManager.remove( player.getObjectId(), variableName )
                        }

                        return
                    }

                    let objectId = _.parseInt( variableValue )
                    let itemInstance: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
                    if ( itemInstance ) {
                        await player.destroyItemByObjectId( objectId, 1, false, 'Quest:AvantGarde:CancelCertification' )
                        PlayerVariablesManager.remove( player.getObjectId(), variableName )
                        return
                    }

                    await player.initializeWarehouse()
                    itemInstance = player.getWarehouse().getItemByObjectId( objectId )
                    if ( itemInstance ) {
                        await player.getWarehouse().destroyItem( itemInstance, 1, 0, 'Quest:AvantGarde:CancelCertification' )
                    }

                    PlayerVariablesManager.remove( player.getObjectId(), variableName )
                } )

                if ( ConfigManager.character.getFeeDeleteSubClassSkills() > 0 ) {
                    await player.reduceAdena( ConfigManager.character.getFeeDeleteSubClassSkills(), true, 'Quest:AvantGarde:CancelCertification' )
                }

                player.sendSkillList()

                await aigle.resolve( itemIds ).each( async ( itemId: number ) => {
                    let item: L2ItemInstance = player.getInventory().getItemByItemId( itemId )
                    if ( item ) {
                        await player.destroyItem( item, false, 'Quest:AvantGarde:CancelCertificationExtraBooks' )
                    }
                } )

                return this.getPath( '32323-09no.html' )
        }
    }

    async onTalkEvent(): Promise<string> {
        return this.getPath( '32323-01.html' )
    }

    async onPlayerSkillLearnEvent( npc: L2Npc, player: L2PcInstance, skill: Skill, type: AcquireSkillType ): Promise<void> {
        switch ( type ) {
            case AcquireSkillType.Transform:
                return this.showTransformSkillList( player )

            case AcquireSkillType.Subclass:
                return this.showSubClassSkillList( player )
        }
    }

    showSubClassSkillList( player: L2PcInstance ): void {
        let subClassSkills: Array<L2SkillLearn> = SkillCache.getAvailableSubClassSkills( player )
        let skillList: AcquireSkillList = new AcquireSkillList( AcquireSkillType.Subclass )

        subClassSkills.forEach( ( skill: L2SkillLearn ) => {
            if ( SkillCache.getSkill( skill.getSkillId(), skill.getSkillLevel() ) ) {
                skillList.addSkill( skill.getSkillId(), skill.getSkillLevel(), 0, 0 )
            }
        } )

        if ( skillList.getCount() > 0 ) {
            return player.sendOwnedData( skillList.getBuffer() )
        }


    }

    showTransformSkillList( player: L2PcInstance ): void {
        let skills: Array<L2SkillLearn> = SkillCache.getAvailableTransformSkills( player )
        let skillList: AcquireSkillList = new AcquireSkillList( AcquireSkillType.Transform )

        skills.forEach( ( skill: L2SkillLearn ) => {
            if ( SkillCache.getSkill( skill.getSkillId(), skill.getSkillLevel() ) ) {
                skillList.addSkill( skill.getSkillId(), skill.getSkillLevel(), 0, 0 )
            }
        } )

        if ( skillList.getCount() > 0 ) {
            return player.sendOwnedData( skillList.getBuffer() )
        }

        let level = SkillCache.getMinimumLevelForNewSkill( player, DataManager.getSkillTreesData().getTransformSkillTree() )
        if ( level > 0 ) {
            let message = new SystemMessageBuilder( SystemMessageIds.DO_NOT_HAVE_FURTHER_SKILLS_TO_LEARN_S1 )
                    .addNumber( level )
                    .getBuffer()

            return player.sendOwnedData( message )
        }

        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_MORE_SKILLS_TO_LEARN ) )
    }
}