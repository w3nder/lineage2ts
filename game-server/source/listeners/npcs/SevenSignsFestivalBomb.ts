import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AITraitCache } from '../../gameService/cache/AITraitCache'
import { AIIntent } from '../../gameService/aicontroller/enums/AIIntent'
import { BaseMoveAction } from '../../gameService/aicontroller/traits/actions/BaseMoveAction'
import { IAITrait, TraitSettings } from '../../gameService/aicontroller/interface/IAITrait'
import { AIEffect, AIEventData, AIMoveToLocationEvent } from '../../gameService/aicontroller/enums/AIEffect'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SkillCache } from '../../gameService/cache/SkillCache'

const npcIds : Array<number> = [
    18317, 18315, 18313, 18311, 18309, // dawn side
    18307, 18305, 18303, 18301, 18299, // dusk side
]

class FestivalBombMovement extends BaseMoveAction implements IAITrait {
    canProcessEffect( effect: AIEffect ): boolean {
        return effect === AIEffect.MoveToLocation || effect === AIEffect.MoveFinished
    }

    canSwitchIntent(): boolean {
        return false
    }

    describeState( objectId: number, playerId: number ): void {
        let npc = L2World.getObjectById( objectId ) as L2Npc
        return this.describeMovementAction( npc, playerId )
    }

    getName(): string {
        return 'FestivalBombMovement'
    }

    onEnd( objectId: number ): void {}

    onStart( objectId: number ): Promise<void> {
        return Promise.resolve( undefined )
    }

    processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        let npc = L2World.getObjectById( objectId ) as L2Npc
        switch ( effect ) {
            case AIEffect.MoveToLocation:
                let moveParameters = parameters as AIMoveToLocationEvent
                this.moveToCoordinates( npc, moveParameters.x, moveParameters.y, moveParameters.z, moveParameters.heading, moveParameters.instanceId )
                break

            case AIEffect.MoveFinished:
                let skillData = npc.getTemplate().getSkillParameters()[ 'DDMagic' ]
                if ( !skillData ) {
                    break
                }

                return npc.doCast( SkillCache.getSkill( skillData.id, skillData.level ) )
        }

        return Promise.resolve( undefined )
    }

    switchIntent(): AIIntent {
        return AIIntent.WAITING
    }
}

export class SevenSignsFestivalBomb extends ListenerLogic {
    constructor() {
        super( 'SevenSignsFestivalBomb', 'listeners/npcs/SevenSignsFestivalBomb.ts' )
    }

    async initialize(): Promise<void> {
        let traits : TraitSettings = {
            ...AITraitCache.createEmptyTraits(),
            [ AIIntent.WAITING ]: new FestivalBombMovement()
        }

        AITraitCache.setManyNpcTraits( npcIds, traits )
    }
}