import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2Clan } from '../../gameService/models/L2Clan'
import { Fort } from '../../gameService/models/entity/Fort'
import { Castle } from '../../gameService/models/entity/Castle'
import { DataManager } from '../../data/manager'
import { ConfigManager } from '../../config/ConfigManager'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { FortSiegeManager } from '../../gameService/instancemanager/FortSiegeManager'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'
import { ClanPrivilege } from '../../gameService/enums/ClanPriviledge'

const npcIds: Array<number> = [
    35659, // Shanty Fortress
    35690, // Southern Fortress
    35728, // Hive Fortress
    35759, // Valley Fortress
    35797, // Ivory Fortress
    35828, // Narsell Fortress
    35859, // Bayou Fortress
    35897, // White Sands Fortress
    35928, // Borderland Fortress
    35966, // Swamp Fortress
    36004, // Archaic Fortress
    36035, // Floran Fortress
    36073, // Cloud Mountain
    36111, // Tanor Fortress
    36142, // Dragonspine Fortress
    36173, // Antharas's Fortress
    36211, // Western Fortress
    36249, // Hunter's Fortress
    36287, // Aaru Fortress
    36318, // Demon Fortress
    36356, // Monastic Fortress
]

export class FortressSiegeManager extends ListenerLogic {
    constructor() {
        super( 'FortressSiegeManager', 'listeners/npcs/FortressSiegeManager.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/FortressSiegeManager'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc
        let fortress: Fort = npc.getFort()
        let fortOwner = fortress.getOwnerClan() ? fortress.getOwnerClan().getId() : 0
        if ( fortOwner === 0 ) {
            return this.getPath( 'FortressSiegeManager.html' )
        }

        return DataManager.getHtmlData().getItem( this.getPath( 'FortressSiegeManager-01.html' ) )
                .replace( /%clanName%/g, fortress.getOwnerClan().getName() )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        switch ( event ) {
            case 'FortressSiegeManager-11.html':
            case 'FortressSiegeManager-13.html':
            case 'FortressSiegeManager-14.html':
            case 'FortressSiegeManager-15.html':
            case 'FortressSiegeManager-16.html':
                return this.getPath( event )

            case 'register':
                if ( !player.getClan() ) {
                    return this.getPath( 'FortressSiegeManager-02.html' )
                }

                let clan: L2Clan = player.getClan()
                let fortress: Fort = npc.getFort()
                let castle: Castle = npc.getCastle()

                if ( clan.getFortId() === fortress.getResidenceId() ) {
                    return DataManager.getHtmlData().getItem( this.getPath( 'FortressSiegeManager-12.html' ) )
                            .replace( /%clanName%/g, fortress.getOwnerClan().getName() )
                }

                if ( !player.hasClanPrivilege( ClanPrivilege.CastleSiege ) ) {
                    return this.getPath( 'FortressSiegeManager-10.html' )
                }

                if ( ( clan.getLevel() < ConfigManager.fortSiege.getSiegeClanMinLevel() ) ) {
                    return this.getPath( 'FortressSiegeManager-04.html' )
                }

                if ( ( player.getClan().getCastleId() === castle.getResidenceId() ) && ( fortress.getFortState() === 2 ) ) {
                    return this.getPath( 'FortressSiegeManager-18.html' )
                }

                if ( clan.getCastleId() !== 0
                        && clan.getCastleId() !== castle.getResidenceId()
                        && ConfigManager.fortSiege.justToTerritory() ) {
                    return this.getPath( 'FortressSiegeManager-17.html' )
                }

                if ( ( fortress.getTimeTillRebelArmy() > 0 ) && ( fortress.getTimeTillRebelArmy() <= 7200 ) ) {
                    return this.getPath( 'FortressSiegeManager-19.html' )
                }

                switch ( await npc.getFort().getSiege().addAttackerPlayer( player, true ) ) {
                    case 1:
                        return this.getPath( 'FortressSiegeManager-03.html' )

                    case 2:
                        return this.getPath( 'FortressSiegeManager-07.html' )

                    case 3:
                        return this.getPath( 'FortressSiegeManager-06.html' )

                    case 4:
                        let message = new SystemMessageBuilder( SystemMessageIds.REGISTERED_TO_S1_FORTRESS_BATTLE )
                                .addString( npc.getFort().getName() )
                                .getBuffer()
                        player.sendOwnedData( message )
                        return this.getPath( 'FortressSiegeManager-05.html' )
                }

                break

            case 'cancel':
                if ( !player.getClan() ) {
                    return this.getPath( 'FortressSiegeManager-02.html' )
                }

                let cancelClan: L2Clan = player.getClan()
                let cancelFortress: Fort = npc.getFort()

                if ( cancelClan.getFortId() === cancelFortress.getResidenceId() ) {
                    return DataManager.getHtmlData().getItem( this.getPath( 'FortressSiegeManager-12.html' ) )
                            .replace( /%clanName%/g, cancelFortress.getOwnerClan().getName() )
                }

                if ( !player.hasClanPrivilege( ClanPrivilege.CastleSiege ) ) {
                    return this.getPath( 'FortressSiegeManager-10.html' )
                }

                if ( !await FortSiegeManager.checkIsRegistered( cancelClan, cancelFortress.getResidenceId() ) ) {
                    return this.getPath( 'FortressSiegeManager-09.html' )
                }

                await cancelFortress.getSiege().removeAttackerClan( player.getClan() )
                return this.getPath( 'FortressSiegeManager-08.html' )

            case 'warInfo':
                return this.getPath( _.isEmpty( npc.getFort().getSiege().getAttackerClans() ) ? 'FortressSiegeManager-20.html' : 'FortressSiegeManager-21.html' )
        }
    }
}