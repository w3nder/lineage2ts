import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import _ from 'lodash'

const npcId: number = 32871

const EMERALD_HORN = 25718
const DUST_RIDER = 25719
const BLEEDING_FLY = 25720
const BLACKDAGGER_WING = 25721
const SHADOW_SUMMONER = 25722
const SPIKE_SLASHER = 25723
const MUSCLE_BOMBER = 25724

const LARGE_DRAGON_BONE = 17248

export class DragonVortex extends ListenerLogic {
    constructor() {
        super( 'DragonVortex', 'listeners/npcs/DragonVortex.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/DragonVortex'
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case 'RAIDBOSS':
                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                let player = L2World.getPlayer( data.playerId )

                if ( QuestHelper.hasQuestItems( player, LARGE_DRAGON_BONE ) ) {
                    let state = NpcVariablesManager.get( data.characterId, this.name ) || false
                    if ( !state ) {
                        await QuestHelper.takeSingleItem( player, LARGE_DRAGON_BONE, 1 )
                        NpcVariablesManager.set( data.characterId, this.name, true )

                        if ( [ 92225,110116, 121172, 108924 ].includes( npc.getX() ) ) {
                            QuestHelper.addGenericSpawn( null, this.getRandomNpcId(), npc.getX() + _.random( -600, 600 ), npc.getY() + _.random( -600, 600 ), npc.getZ(), 0, false, 0, true, npc.getInstanceId() )
                        }

                        this.startQuestTimer( 'CANSPAWN', 60000, npc.getObjectId() )
                        return
                    }

                    return this.getPath( '32871-02.html' )
                }

                return this.getPath( '32871-01.html' )

            case 'CANSPAWN':
                NpcVariablesManager.set( data.characterId, this.name, false )
                break
        }
    }

    getRandomNpcId(): number {
        let random = _.random( 100 )
        if ( random < 3 ) {
            return MUSCLE_BOMBER
        }

        if ( random < 8 ) {
            return SHADOW_SUMMONER
        }

        if ( random < 15 ) {
            return SPIKE_SLASHER
        }

        if ( random < 25 ) {
            return BLACKDAGGER_WING
        }

        if ( random < 45 ) {
            return BLEEDING_FLY
        }

        if ( random < 67 ) {
            return DUST_RIDER
        }

        return EMERALD_HORN
    }
}