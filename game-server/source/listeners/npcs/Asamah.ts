import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

const npcId : number = 32115

export class Asamah extends ListenerLogic {
    constructor() {
        super( 'Asamah', 'listeners/npcs/Asamah.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let state : QuestState = QuestStateCache.getQuestState( data.playerId,'Q00111_ElrokianHuntersProof' )
        return this.getPath( ( state && state.isCompleted() ) ? '32115-01.htm' : '32115-02.htm' )
    }

    getPathPrefix(): string {
        return 'data/l2jserver/datapack/ai/npc/Asamah'
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {
        if ( [ '32115-03.htm', '32115-04.htm' ].includes( data.eventName ) ) {
            return this.getPath( data.eventName )
        }

        return
    }
}