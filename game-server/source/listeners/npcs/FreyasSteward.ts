import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const npcId: number = 32029
const location: Location = new Location( 103045, -124361, -2768 )
const minimumLevel = 82

export class FreyasSteward extends ListenerLogic {
    constructor() {
        super( 'FreyasSteward', 'listeners/npcs/FreyasSteward.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/FreyasSteward'
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onApproachedForTalk(): Promise<string> {
        return '32029.html'
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        if ( player.getLevel() >= minimumLevel ) {
            await player.teleportToLocation( location )
            return
        }

        return this.getPath( '32029-1.html' )
    }
}