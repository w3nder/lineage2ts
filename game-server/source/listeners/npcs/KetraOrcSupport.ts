import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const KADUN = 31370 // Hierarch
const WAHKAN = 31371 // Messenger
const ASEFA = 31372 // Soul Guide
const ATAN = 31373 // Grocer
const JAFF = 31374 // Warehouse Keeper
const JUMARA = 31375 // Trader
const KURFA = 31376 // Gate Keeper

const HORN = 7186
const marks : Array<number> = [
    7211, // Mark of Ketra's Alliance - Level 1
    7212, // Mark of Ketra's Alliance - Level 2
    7213, // Mark of Ketra's Alliance - Level 3
    7214, // Mark of Ketra's Alliance - Level 4
    7215, // Mark of Ketra's Alliance - Level 5
]

const buffs : { [ key: number ] : [ number, number ]} = {
    1: [ 4359, 2 ], // Focus: Requires 2 Buffalo Horns
    2: [ 4360, 2 ], // Death Whisper: Requires 2 Buffalo Horns
    3: [ 4345, 3 ], // Might: Requires 3 Buffalo Horns
    4: [ 4355, 3 ], // Acumen: Requires 3 Buffalo Horns
    5: [ 4352, 3 ], // Berserker: Requires 3 Buffalo Horns
    6: [ 4354, 3 ], // Vampiric Rage: Requires 3 Buffalo Horns
    7: [ 4356, 6 ], // Empower: Requires 6 Buffalo Horns
    8: [ 4357, 6 ], // Haste: Requires 6 Buffalo Horns
}

export class KetraOrcSupport extends ListenerLogic {
    constructor() {
        super( 'KetraOrcSupport', 'listeners/npcs/KetraOrcSupport.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ KURFA, JAFF ]
    }

    getTalkIds(): Array<number> {
        return [ ASEFA, KURFA, JAFF ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ KADUN, WAHKAN, ASEFA, ATAN, JAFF, JUMARA, KURFA ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/KetraOrcSupport'
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let level : number = this.getAllianceLevel( player )
        switch ( data.characterNpcId ) {
            case KADUN:
                return this.getPath( ( level > 0 ) ? '31370-friend.html' : '31370-no.html' )

            case WAHKAN:
                return this.getPath( ( level > 0 ) ? '31371-friend.html' : '31371-no.html' )

            case ASEFA:
                return this.getPath( ( level > 0 ) ? ( level < 3 ) ? '31372-01.html' : '31372-04.html' : '31372-03.html' )

            case ATAN:
                return this.getPath( ( level > 0 ) ? '31373-friend.html' : '31373-no.html' )

            case JAFF:
                return this.getPath( ( level > 0 ) ? ( level === 1 ? '31374-01.html' : '31374-02.html' ) : '31374-no.html' )

            case JUMARA:
                switch ( level ) {
                    case 1:
                    case 2:
                        return this.getPath( '31375-01.html' )

                    case 3:
                    case 4:
                        return this.getPath( '31375-02.html' )

                    case 5:
                        return this.getPath( '31375-03.html' )
                }

                return this.getPath( '31375-no.html' )

            case KURFA:
                switch ( level ) {
                    case 1:
                    case 2:
                    case 3:
                        return this.getPath( '31376-01.html' )

                    case 4:
                        return this.getPath( '31376-02.html' )

                    case 5:
                        return this.getPath( '31376-03.html' )
                }

                return this.getPath( '31376-no.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getAllianceLevel( player: L2PcInstance ) {
        let index = marks.findIndex( ( itemId: number ) => {
            return QuestHelper.hasQuestItems( player, itemId )
        } )

        return _.clamp( index, 0, marks.length )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        if ( buffs[ event ] ) {
            let [ skillId, cost ] = buffs[ event ]
            if ( QuestHelper.getQuestItemsCount( player, HORN ) >= cost ) {
                await QuestHelper.takeSingleItem( player, HORN, cost )

                npc.setTarget( player )
                await npc.doCast( SkillCache.getSkill( skillId, 1 ) )
                npc.setMaxStats()

                return
            }

            return this.getPath( '31372-02.html' )
        }

        if ( event === 'Teleport' ) {
            let level : number = this.getAllianceLevel( player )
            if ( level === 4 ) {
                return this.getPath( '31376-04.html' )
            }

            if ( level === 5 ) {
                return this.getPath( '31376-05.html' )
            }
        }
    }
}