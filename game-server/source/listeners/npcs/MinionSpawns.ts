import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { MinionSpawnNpcIds } from './data/MinionSpawnData'
import { AttackableAttackedEvent, NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2MonsterInstance } from '../../gameService/models/actor/instance/L2MonsterInstance'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const sayOnAttack : Array<number> = [
    NpcStringIds.COME_OUT_YOU_CHILDREN_OF_DARKNESS,
    NpcStringIds.SHOW_YOURSELVES,
    NpcStringIds.DESTROY_THE_ENEMY_MY_BROTHERS,
    NpcStringIds.FORCES_OF_DARKNESS_FOLLOW_ME
]

/*
    TODO : review usage or remove
    - all spawns must be initialized through spawn makers
    - minions should be spawned in same way
 */
export class MinionSpawns extends ListenerLogic {
    constructor() {
        super( 'MinionSpawns', 'listeners/npcs/MinionSpawns.ts' )
    }

    getSpawnIds(): Array<number> {
        return MinionSpawnNpcIds
    }

    getAttackableAttackIds(): Array<number> {
        return [ 20767 ] // Timak Orc Troop Leader
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2MonsterInstance

        if ( !npc.getTemplate().getParameters()[ 'SummonPrivateRate' ] ) {
            //npc.getMinionList().spawnMinions( npc.getTemplate().getParameters()[ 'Privates' ] )
            NpcVariablesManager.set( data.characterId, this.name, true )
            return
        }

        NpcVariablesManager.set( data.characterId, this.name, false )
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.targetId ) as L2MonsterInstance
        if ( npc && !npc.isTeleporting() ) {
            let chance = npc.getTemplate().getParameters()[ 'SummonPrivateRate' ] as number ?? 0
            if ( _.random( 1, 100 ) <= chance && NpcVariablesManager.get( data.targetId, this.name ) ) {

                // TODO : add spawns if necessary

                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, _.sample( sayOnAttack ) )
                NpcVariablesManager.set( data.targetId, this.name, true )
            }
        }

        return
    }
}