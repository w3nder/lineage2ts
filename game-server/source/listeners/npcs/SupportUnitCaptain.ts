import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { RequestShowSubUnitSkillList } from '../../gameService/packets/send/RequestShowSubUnitSkillList'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'
import { ClanPrivilege } from '../../gameService/enums/ClanPriviledge'

const npcIds: Array<number> = [
    35662, // Shanty Fortress
    35694, // Southern Fortress
    35731, // Hive Fortress
    35763, // Valley Fortress
    35800, // Ivory Fortress
    35831, // Narsell Fortress
    35863, // Bayou Fortress
    35900, // White Sands Fortress
    35932, // Borderland Fortress
    35970, // Swamp Fortress
    36007, // Archaic Fortress
    36039, // Floran Fortress
    36077, // Cloud Mountain
    36114, // Tanor Fortress
    36145, // Dragonspine Fortress
    36177, // Antharas's Fortress
    36215, // Western Fortress
    36253, // Hunter's Fortress
    36290, // Aaru Fortress
    36322, // Demon Fortress
    36360, // Monastic Fortress
]

const EPAULETTE = 9912 // Knight's Epaulette
const RED_MEDITATION = 9931 // Red Talisman of Meditation
const BLUE_DIV_PROTECTION = 9932 // Blue Talisman - Divine Protection
const BLUE_EXPLOSION = 10416 // Blue Talisman - Explosion
const BLUE_M_EXPLOSION = 10417 // Blue Talisman - Magic Explosion
const RED_MIN_CLARITY = 9917 // Red Talisman of Minimum Clarity
const RED_MAX_CLARITY = 9918 // Red Talisman of Maximum Clarity
const RED_MENTAL_REG = 9928 // Red Talisman of Mental Regeneration
const BLUE_PROTECTION = 9929 // Blue Talisman of Protection
const BLUE_INVIS = 9920 // Blue Talisman of Invisibility
const BLUE_DEFENSE = 9916 // Blue Talisman of Defense
const BLACK_ESCAPE = 9923 // Black Talisman - Escape
const BLUE_HEALING = 9924 // Blue Talisman of Healing
const RED_RECOVERY = 9925 // Red Talisman of Recovery
const BLUE_DEFENSE2 = 9926 // Blue Talisman of Defense
const BLUE_M_DEFENSE = 9927 // Blue Talisman of Magic Defense
const RED_LIFE_FORCE = 10518 // Red Talisman - Life Force
const BLUE_GREAT_HEALING = 10424 // Blue Talisman - Greater Healing
const WHITE_FIRE = 10421 // White Talisman - Fire

const talismans: Array<number> = [
    9914, // Blue Talisman of Power
    9915, // Blue Talisman of Wild Magic
    9920, // Blue Talisman of Invisibility
    9921, // Blue Talisman - Shield Protection
    9922, // Black Talisman - Mending
    9933, // Yellow Talisman of Power
    9934, // Yellow Talisman of Violent Haste
    9935, // Yellow Talisman of Arcane Defense
    9936, // Yellow Talisman of Arcane Power
    9937, // Yellow Talisman of Arcane Haste
    9938, // Yellow Talisman of Accuracy
    9939, // Yellow Talisman of Defense
    9940, // Yellow Talisman of Alacrity
    9941, // Yellow Talisman of Speed
    9942, // Yellow Talisman of Critical Reduction
    9943, // Yellow Talisman of Critical Damage
    9944, // Yellow Talisman of Critical Dodging
    9945, // Yellow Talisman of Evasion
    9946, // Yellow Talisman of Healing
    9947, // Yellow Talisman of CP Regeneration
    9948, // Yellow Talisman of Physical Regeneration
    9949, // Yellow Talisman of Mental Regeneration
    9950, // Grey Talisman of Weight Training
    9952, // Orange Talisman - Hot Springs CP Potion
    9953, // Orange Talisman - Elixir of Life
    9954, // Orange Talisman - Elixir of Mental Strength
    9955, // Black Talisman - Vocalization
    9956, // Black Talisman - Arcane Freedom
    9957, // Black Talisman - Physical Freedom
    9958, // Black Talisman - Rescue
    9959, // Black Talisman - Free Speech
    9960, // White Talisman of Bravery
    9961, // White Talisman of Motion
    9962, // White Talisman of Grounding
    9963, // White Talisman of Attention
    9964, // White Talisman of Bandages
    9965, // White Talisman of Protection
    10418, // White Talisman - Storm
    10420, // White Talisman - Water
    10519, // White Talisman - Earth
    10422, // White Talisman - Light
    10423, // Blue Talisman - Self-Destruction
]

export class SupportUnitCaptain extends ListenerLogic {
    constructor() {
        super( 'SupportUnitCaptain', 'listeners/npcs/SupportUnitCaptain.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/SupportUnitCaptain'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        let fortOwner = npc.getFort().getOwnerClan() ? npc.getFort().getOwnerClan().getId() : 0
        if ( !player.getClan() || player.getClanId() !== fortOwner ) {
            return this.getPath( 'unitcaptain-04.htm' )
        }

        switch ( event ) {
            case 'unitcaptain.html':
            case 'unitcaptain-01.html':
                return this.getPath( event )

            case 'giveTalisman':
                if ( QuestHelper.getQuestItemsCount( player, EPAULETTE ) < 10 ) {
                    return this.getPath( 'unitcaptain-05.html' )
                }

                let itemId = this.getTalismanId()

                await QuestHelper.takeSingleItem( player, EPAULETTE, 10 )
                await QuestHelper.giveSingleItem( player, itemId, 1 )
                return this.getPath( 'unitcaptain-02.html' )

            case 'squadSkill': {
                if ( player.isClanLeader() || player.hasClanPrivilege( ClanPrivilege.NameTroops ) ) {
                    player.sendOwnedData( RequestShowSubUnitSkillList( player ) )
                    return
                }

                return this.getPath( 'unitcaptain-03.html' )
            }
        }
    }

    getTalismanId(): number {
        let categoryChance = _.random( 100 )
        if ( categoryChance <= 5 ) {
            let chance = _.random( 100 )
            if ( chance <= 25 ) {
                return RED_MEDITATION
            }

            if ( chance <= 50 ) {
                return BLUE_DIV_PROTECTION
            }

            if ( chance <= 75 ) {
                return BLUE_EXPLOSION
            }

            return BLUE_M_EXPLOSION
        }

        if ( categoryChance <= 15 ) {
            let chance = _.random( 100 )
            if ( chance <= 20 ) {
                return RED_MIN_CLARITY
            }

            if ( chance <= 40 ) {
                return RED_MAX_CLARITY
            }

            if ( chance <= 60 ) {
                return RED_MENTAL_REG
            }

            if ( chance <= 80 ) {
                return BLUE_PROTECTION
            }

            return BLUE_INVIS
        }

        if ( categoryChance <= 30 ) {
            let chance = _.random( 100 )
            if ( chance <= 12 ) {
                return BLUE_DEFENSE
            }

            if ( chance <= 25 ) {
                return BLACK_ESCAPE
            }

            if ( chance <= 37 ) {
                return BLUE_HEALING
            }

            if ( chance <= 50 ) {
                return RED_RECOVERY
            }

            if ( chance <= 62 ) {
                return BLUE_DEFENSE2
            }

            if ( chance <= 75 ) {
                return BLUE_M_DEFENSE
            }

            if ( chance <= 87 ) {
                return RED_LIFE_FORCE
            }

            return BLUE_GREAT_HEALING
        }

        let chance = _.random( 46 )
        if ( chance <= 41 ) {
            return talismans[ chance ]
        }

        return WHITE_FIRE
    }
}