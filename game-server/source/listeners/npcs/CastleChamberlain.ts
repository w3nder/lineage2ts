import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { PlayerActionOverride } from '../../gameService/values/PlayerConditions'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { CastleManorManager } from '../../gameService/instancemanager/CastleManorManager'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { ExShowSeedInfo } from '../../gameService/packets/send/ExShowSeedInfo'
import { ExShowCropInfo } from '../../gameService/packets/send/ExShowCropInfo'
import { ExShowManorDefaultInfo } from '../../gameService/packets/send/ExShowManorDefaultInfo'
import { ExShowSeedSetting } from '../../gameService/packets/send/ExShowSeedSetting'
import { ExShowCropSetting } from '../../gameService/packets/send/ExShowCropSetting'
import { FortManager } from '../../gameService/instancemanager/FortManager'
import { Fort } from '../../gameService/models/entity/Fort'
import { DataManager } from '../../data/manager'
import { NpcHtmlMessage, NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { ConfigManager } from '../../config/ConfigManager'
import { Castle, CastleFunction } from '../../gameService/models/entity/Castle'
import { SevenSigns } from '../../gameService/directives/SevenSigns'
import { L2DoorInstance } from '../../gameService/models/actor/instance/L2DoorInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { ClanCache } from '../../gameService/cache/ClanCache'
import { L2Clan } from '../../gameService/models/L2Clan'
import { SevenSignsPeriod, SevenSignsSeal, SevenSignsSide } from '../../gameService/values/SevenSignsValues'
import { SeedProduction } from '../../gameService/models/manor/SeedProduction'
import { TerritoryWarManager } from '../../gameService/instancemanager/TerritoryWarManager'
import { TeleportLocationManager } from '../../gameService/cache/TeleportLocationManager'
import { ExShowDominionRegistry } from '../../gameService/packets/send/ExShowDominionRegistry'
import { L2MerchantInstance } from '../../gameService/models/actor/instance/L2MerchantInstance'
import { L2World } from '../../gameService/L2World'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import aigle from 'aigle'
import _ from 'lodash'
import moment from 'moment'
import { getMaxAdena } from '../../gameService/helpers/ConfigurationHelper'
import { CastleFunctions } from '../../gameService/enums/CastleFunctions'
import { ClanPrivilege } from '../../gameService/enums/ClanPriviledge'

const npcIds: Array<number> = [
    35100, // Sayres
    35142, // Crosby
    35184, // Saul
    35226, // Brasseur
    35274, // Logan
    35316, // Neurath
    35363, // Alfred
    35509, // Frederick
    35555, // August
]

const CROWN = 6841
const fortressMap: { [ key: number ]: Array<number> } = {
    1: [ 101, 102, 112, 113 ], // Gludio Castle
    2: [ 103, 112, 114, 115 ], // Dion Castle
    3: [ 104, 114, 116, 118, 119 ], // Giran Castle
    4: [ 105, 113, 115, 116, 117 ], // Oren Castle
    5: [ 106, 107, 117, 118 ], // Aden Castle
    6: [ 108, 119 ], // Innadril Castle
    7: [ 109, 117, 120 ], // Goddard Castle
    8: [ 110, 120, 121 ], // Rune Castle
    9: [ 111, 121 ], // Schuttgart Castle
}

const buffs: Array<SkillHolder> = [
    new SkillHolder( 4342, 2 ), // Wind Walk Lv.2
    new SkillHolder( 4343, 3 ), // Decrease Weight Lv.3
    new SkillHolder( 4344, 3 ), // Shield Lv.3
    new SkillHolder( 4346, 4 ), // Mental Shield Lv.4
    new SkillHolder( 4345, 3 ), // Might Lv.3
    new SkillHolder( 4347, 2 ), // Bless the Body Lv.2
    new SkillHolder( 4349, 1 ), // Magic Barrier Lv.1
    new SkillHolder( 4350, 1 ), // Resist Shock Lv.1
    new SkillHolder( 4348, 2 ), // Bless the Soul Lv.2
    new SkillHolder( 4351, 2 ), // Concentration Lv.2
    new SkillHolder( 4352, 1 ), // Berserker Spirit Lv.1
    new SkillHolder( 4353, 2 ), // Bless Shield Lv.2
    new SkillHolder( 4358, 1 ), // Guidance Lv.1
    new SkillHolder( 4354, 1 ), // Vampiric Rage Lv.1
    new SkillHolder( 4347, 6 ), // Bless the Body Lv.6
    new SkillHolder( 4349, 2 ), // Magic Barrier Lv.2
    new SkillHolder( 4350, 4 ), // Resist Shock Lv.4
    new SkillHolder( 4348, 6 ), // Bless the Soul Lv.6
    new SkillHolder( 4351, 6 ), // Concentration Lv.6
    new SkillHolder( 4352, 2 ), // Berserker Spirit Lv.2
    new SkillHolder( 4353, 6 ), // Bless Shield Lv.6
    new SkillHolder( 4358, 3 ), // Guidance Lv.3
    new SkillHolder( 4354, 4 ), // Vampiric Rage Lv.4
    new SkillHolder( 4355, 1 ), // Acumen Lv.1
    new SkillHolder( 4356, 1 ), // Empower Lv.1
    new SkillHolder( 4357, 1 ), // Haste Lv.1
    new SkillHolder( 4359, 1 ), // Focus Lv.1
    new SkillHolder( 4360, 1 ), // Death Whisper Lv.1
]

export class CastleChamberlain extends ListenerLogic {
    constructor() {
        super( 'CastleChamberlain', 'listeners/npcs/CastleChamberlain.ts' )
    }

    formatCastleFunction( castle: Castle, html: string, functionType: number, pattern: string ): void {
        let currentFunction: CastleFunction = castle.getFunction( functionType )
        if ( currentFunction ) {
            html = html
                    .replace( `%${ pattern }Depth%`, '<fstring>4</fstring>' )
                    .replace( `%${ pattern }Cost%`, '' )
                    .replace( `%${ pattern }Expire%`, '<fstring>4</fstring>' )
                    .replace( `%${ pattern }Reset%`, '' )

            return
        }

        let fstring = [ CastleFunctions.SupportBuffs, CastleFunctions.Teleport ].includes( functionType ) ? '9' : '10'
        let endDate = moment( currentFunction.endDate )

        let rate = Math.floor( currentFunction.rate / 86400000 )
        html = html
                .replace( `%${ pattern }Depth%`, `<fstring p1="${ currentFunction.getLevel() }">${ fstring }</fstring>` )
                .replace( `%${ pattern }Cost%`, `<fstring p1="${ currentFunction.fee }" p2="${ rate }">6</fstring>` )
                .replace( `%${ pattern }Expire%`, `<fstring p1="${ endDate.date() }" p2="${ endDate.month() + 1 }" p3="${ endDate.year() }">5</fstring>` )
                .replace( `%${ pattern }Reset%`, `[<a action="bypass -h Quest CastleChamberlain ${ pattern } 0">Deactivate</a>]` )

    }

    getConfirmationHtml( player: L2PcInstance, npc: L2Npc, castle: Castle, functionType: number, level: number ): string {
        if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {

            let type: string = ( functionType === CastleFunctions.Teleport ) ? '9' : '10'
            let html: string
            if ( level === 0 ) {
                html = DataManager.getHtmlData().getItem( this.getPath( 'castleresetdeco.html' ) )
                        .replace( /%AgitDecoSubmit%/g, functionType.toString() )

            } else if ( ( castle.getFunction( functionType ) != null ) && ( castle.getFunction( functionType ).getLevel() === level ) ) {
                html = DataManager.getHtmlData().getItem( this.getPath( 'castledecoalreadyset.html' ) )
                        .replace( /%AgitDecoEffect%/g, `<fstring p1="${ level }">${ type }</fstring>` )
            } else {
                let fee = this.getFunctionFee( functionType, level )
                let ratio = Math.floor( this.getFunctionRatio( functionType ) / 86400000 )
                html = DataManager.getHtmlData().getItem( this.getPath( `castledeco-0${ functionType }.html` ) )
                        .replace( '%AgitDecoCost%', `<fstring p1="${ fee }" p2="${ ratio }">6</fstring>` )
                        .replace( '%AgitDecoEffect%', `<fstring p1="${ level }">${ type }</fstring>` )
                        .replace( '%AgitDecoSubmit%', `${ functionType } ${ level }` )
            }

            player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
            return
        }

        return this.getPath( 'chamberlain-21.html' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcManorBypass,
                method: this.onNpcManorBypassEvent.bind( this ),
                ids: [ 35100, 35142, 35184, 35226, 35274, 35316, 35363, 35509, 35555 ],
            },
        ]
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/CastleChamberlain'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        return this.getPath( this.isOwner( player, npc ) ? 'chamberlain-01.html' : 'chamberlain-04.html' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        let chunks: Array<string> = _.split( event, ' ' )
        let eventId = _.head( chunks )
        let castle: Castle = npc.getCastle()

        switch ( eventId ) {
            case 'chamberlain-01.html':
            case 'manor-help-01.html':
            case 'manor-help-02.html':
            case 'manor-help-03.html':
            case 'manor-help-04.html':
                return this.getPath( eventId )

            case 'fort_status':
                if ( npc.isLord( player ) ) {
                    let fortList: Array<string> = _.map( fortressMap[ castle.getResidenceId() ], ( id: number ) => {
                        let fortress: Fort = FortManager.getFortById( id )
                        let fortId = fortress.getResidenceId()
                        let fortType = ( fortId < 112 ) ? '1300133' : '1300134'
                        let fortStatus = this.getFortStatus( fortress.getFortState() )

                        return `<fstring>1300${ fortId }</fstring> (<fstring>${ fortType }</fstring>)  : <font color="00FFFF"><fstring>${ fortStatus }</fstring></font><br>`
                    } )

                    let html = DataManager.getHtmlData().getItem( this.getPath( 'chamberlain-28.html' ) )
                            .replace( /%list%/g, fortList.join( '' ) )
                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'siege_functions':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {
                    if ( castle.getSiege().isInProgress() ) {
                        return this.getPath( 'chamberlain-08.html' )
                    }

                    if ( !this.isDomainFortressInContractStatus( castle.getResidenceId() ) ) {
                        return this.getPath( 'chamberlain-27.html' )
                    }

                    if ( !SevenSigns.isCompareResultsPeriod() ) {
                        return this.getPath( 'chamberlain-26.html' )
                    }

                    return this.getPath( 'chamberlain-12.html' )
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'manage_doors':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {
                    if ( chunks.length > 1 ) {
                        let [ type, ...doors ] = _.tail( chunks )
                        let html = DataManager.getHtmlData().getItem( this.getPath( 'chamberlain-13.html' ) )
                                .replace( /%type%/g, type )

                        html.replace( '%doors%', doors.join( ' ' ) )
                        player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                        return
                    }

                    return this.getPath( `${ npc.getId() }-du.html` )
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'upgrade_doors':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {
                    let type = _.parseInt( _.nth( chunks, 1 ) )
                    let level = _.parseInt( _.nth( chunks, 2 ) )
                    let html = DataManager.getHtmlData().getItem( this.getPath( 'chamberlain-14.html' ) )
                            .replace( /%gate_price%/g, this.getDoorUpgradePrice( type, level ).toString() )
                            .replace( /%event%/g, event.substring( 'upgrade_doors'.length + 1 ) )
                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'upgrade_doors_confirm':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {
                    if ( castle.getSiege().isInProgress() ) {
                        return this.getPath( 'chamberlain-08.html' )
                    }

                    let [ typeValue, levelValue, ...doorValues ] = _.tail( chunks )
                    let level: number = _.parseInt( levelValue )
                    let price: number = this.getDoorUpgradePrice( _.parseInt( typeValue ), level )
                    let doorIds: Array<number> = _.map( doorValues, value => _.parseInt( value ) )

                    let door: L2DoorInstance = castle.getDoor( _.head( doorIds ) )
                    if ( door ) {
                        let currentLevel: number = door.getStat().getUpgradeHpRatio()
                        if ( currentLevel >= level ) {
                            let html = DataManager.getHtmlData().getItem( this.getPath( 'chamberlain-15.html' ) )
                                    .replace( /%doorlevel%/g, currentLevel.toString() )
                            player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                            return
                        }

                        if ( player.getAdena() >= price ) {
                            await QuestHelper.takeSingleItem( player, ItemTypes.Adena, price )

                            await aigle.resolve( doorIds ).each( ( doorId: number ) => {
                                return castle.setDoorUpgrade( doorId, level, true )
                            } )

                            return this.getPath( 'chamberlain-16.html' )
                        }

                        return this.getPath( 'chamberlain-09.html' )
                    }
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'manage_trap':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {
                    if ( chunks.length > 1 ) {
                        let fileName = _.toLower( castle.getName() ) === 'aden' ? 'chamberlain-17a.html' : 'chamberlain-17.html'
                        let html = DataManager.getHtmlData().getItem( this.getPath( fileName ) )
                                .replace( /%trapIndex%/g, _.nth( chunks, 1 ) )
                        player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                        return
                    }

                    return this.getPath( `${ npc.getId() }-tu.html` )
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'upgrade_trap':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {
                    let [ indexValue, levelValue ] = _.tail( chunks )
                    let level = _.parseInt( levelValue )
                    let html = DataManager.getHtmlData().getItem( 'chamberlain-18.html' )
                            .replace( /%trapIndex%/g, indexValue )
                            .replace( /%level%/g, levelValue )
                            .replace( /%dmgzone_price%/g, this.getTrapUpgradePrice( level ).toString() )

                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'upgrade_trap_confirm':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {
                    if ( castle.getSiege().isInProgress() ) {
                        return this.getPath( 'chamberlain-08.html' )
                    }

                    let [ indexValue, levelValue ] = _.tail( chunks )
                    let trapIndex = _.parseInt( indexValue )
                    let level = _.parseInt( levelValue )
                    let price: number = this.getTrapUpgradePrice( level )
                    let currentLevel: number = castle.getTrapUpgradeLevel( trapIndex )

                    if ( currentLevel >= level ) {
                        let html = DataManager.getHtmlData().getItem( this.getPath( 'chamberlain-19.html' ) )
                                .replace( /%dmglevel%/g, currentLevel.toString() )
                        player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                        return
                    }

                    if ( player.getAdena() >= price ) {
                        await QuestHelper.takeSingleItem( player, ItemTypes.Adena, price )
                        await castle.setTrapUpgrade( trapIndex, level, true )

                        return this.getPath( 'chamberlain-20.html' )
                    }

                    return this.getPath( 'chamberlain-09.html' )
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'receive_report':
                if ( npc.isLord( player ) ) {
                    if ( castle.getSiege().isInProgress() ) {
                        return this.getPath( 'chamberlain-07.html' )
                    }

                    let clan: L2Clan = ClanCache.getClan( castle.getOwnerId() )
                    let html = DataManager.getHtmlData().getItem( this.getPath( 'chamberlain-02.html' ) )
                            .replace( /%clanleadername%/g, clan.getLeaderName() )
                            .replace( /%clanname%/g, clan.getName() )
                            .replace( /%castlename%/g, ( 1001000 + castle.getResidenceId() ).toString() )
                            .replace( /%ss_avarice%/g, this.getSealOwner( 1 ) )
                            .replace( /%ss_gnosis%/g, this.getSealOwner( 2 ) )
                            .replace( /%ss_strife%/g, this.getSealOwner( 3 ) )

                    switch ( SevenSigns.getCurrentPeriod() ) {
                        case SevenSignsPeriod.Recruiting:
                            html = html.replace( /%ss_event%/g, '1000509' )
                            break

                        case SevenSignsPeriod.Competing:
                            html = html.replace( /%ss_event%/g, '1000507' )
                            break

                        case SevenSignsPeriod.SealValidation:
                        case SevenSignsPeriod.CompetitionResults:
                            html.replace( /%ss_event%/g, '1000508' )
                            break
                    }

                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'manage_tax':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleTaxes ) ) {
                    if ( castle.getSiege().isInProgress() ) {
                        return this.getPath( 'chamberlain-08.html' )
                    }

                    let html = DataManager.getHtmlData().getItem( this.getPath( 'castlesettaxrate.html' ) )
                            .replace( /%tax_rate%/g, castle.getTaxPercent().toString() )
                            .replace( /%next_tax_rate%/g, '0' )
                            .replace( /%tax_limit%/g, this.getTaxLimit().toString() )

                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                if ( this.isOwner( player, npc ) ) {
                    let html = DataManager.getHtmlData().getItem( this.getPath( 'chamberlain-03.html' ) )
                            .replace( /%tax_rate%/g, castle.getTaxPercent().toString() )
                            .replace( /%next_tax_rate%/g, '0' )

                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'set_tax':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleTaxes ) ) {
                    if ( castle.getSiege().isInProgress() ) {
                        return this.getPath( 'chamberlain-08.html' )
                    }

                    let tax: number = _.parseInt( _.nth( chunks, 1 ) )
                    let taxLimit: number = this.getTaxLimit()
                    let html: string
                    if ( tax > taxLimit ) {
                        html = DataManager.getHtmlData().getItem( this.getPath( 'castletoohightaxrate.html' ) )
                                .replace( /%tax_limit%/g, taxLimit.toString() )
                    } else {
                        await castle.setTaxPercent( tax )
                        html = DataManager.getHtmlData().getItem( this.getPath( 'castleaftersettaxrate.html' ) )
                                .replace( /%next_tax_rate%/g, tax.toString() )
                    }

                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'manage_vault':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleTaxes ) ) {
                    let seedIncome: number = 0
                    if ( ConfigManager.general.allowManor() ) {
                        seedIncome = _.reduce( CastleManorManager.getSeedProduction( castle.getResidenceId(), false ), ( totals: number, production: SeedProduction ) => {
                            let finalAmount = production.getStartAmount() - production.getAmount()
                            if ( finalAmount !== 0 ) {
                                totals += finalAmount * production.getPrice()
                            }

                            return totals
                        }, 0 )
                    }

                    let html = DataManager.getHtmlData().getItem( this.getPath( 'castlemanagevault.html' ) )
                            .replace( '%tax_income%', castle.getTreasury().toString() )
                            .replace( '%tax_income_reserved%', '0' )
                            .replace( '%seed_income%', seedIncome.toString() )

                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'deposit':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleTaxes ) ) {
                    let amount: number = _.parseInt( _.nth( chunks, 1 ) )

                    if ( amount > 0 && amount < getMaxAdena() ) {
                        if ( player.getAdena() >= amount ) {
                            await QuestHelper.takeSingleItem( player, ItemTypes.Adena, amount )
                            castle.addToTreasuryNoTax( amount )
                            return
                        }

                        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                        return
                    }

                    return this.getPath( 'chamberlain-01.html' )
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'withdraw':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleTaxes ) ) {
                    let amount: number = _.parseInt( _.nth( chunks, 1 ) )

                    if ( amount <= castle.getTreasury() ) {
                        castle.addToTreasuryNoTax( ( -1 ) * amount )
                        await QuestHelper.giveAdena( player, amount, false )
                        return this.getPath( 'chamberlain-01.html' )
                    }
                    let html = DataManager.getHtmlData().getItem( this.getPath( 'castlenotenoughbalance.html' ) )
                            .replace( /%tax_income%/g, castle.getTreasury().toString() )
                            .replace( /%withdraw_amount%/g, amount.toString() )
                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'manage_functions':
                if ( !this.isOwner( player, npc ) ) {
                    return this.getPath( 'chamberlain-21.html' )
                }

                if ( castle.getSiege().isInProgress() ) {
                    return this.getPath( 'chamberlain-08.html' )
                }

                return this.getPath( 'chamberlain-23.html' )

            case 'banish_foreigner_show':
                if ( !this.isOwner( player, npc ) || !player.hasClanPrivilege( ClanPrivilege.CastleDismiss ) ) {
                    return this.getPath( 'chamberlain-21.html' )
                }

                if ( castle.getSiege().isInProgress() ) {
                    return this.getPath( 'chamberlain-08.html' )
                }

                return this.getPath( 'chamberlain-10.html' )

            case 'banish_foreigner':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleDismiss ) ) {
                    if ( castle.getSiege().isInProgress() || TerritoryWarManager.isTWInProgress ) {
                        return this.getPath( 'chamberlain-08.html' )
                    }

                    await castle.banishForeigners()
                    return this.getPath( 'chamberlain-11.html' )
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'doors':
                if ( !this.isOwner( player, npc ) || !player.hasClanPrivilege( ClanPrivilege.CastleOpenDoors ) ) {
                    return this.getPath( 'chamberlain-21.html' )
                }

                if ( castle.getSiege().isInProgress() ) {
                    return this.getPath( 'chamberlain-08.html' )
                }

                return this.getPath( `${ npc.getId() }-d.html` )

            case 'operate_door':
                if ( !this.isOwner( player, npc ) || !player.hasClanPrivilege( ClanPrivilege.CastleOpenDoors ) ) {
                    return this.getPath( 'chamberlain-21.html' )
                }

                if ( castle.getSiege().isInProgress() ) {
                    return this.getPath( 'chamberlain-08.html' )
                }

                let [ openValue, ...doorIds ] = _.tail( chunks )
                let isOpen: boolean = _.parseInt( openValue ) === 1

                _.each( doorIds, ( doorValue: string ) => {
                    castle.openCloseDoor( player, _.parseInt( doorValue ), isOpen )
                } )

                return this.getPath( isOpen ? 'chamberlain-05.html' : 'chamberlain-06.html' )

            case 'additional_functions':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {
                    return this.getPath( 'castletdecomanage.html' )
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'recovery':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {
                    let html = DataManager.getHtmlData().getItem( this.getPath( 'castledeco-AR01.html' ) )

                    this.formatCastleFunction( castle, html, CastleFunctions.RegenerateHp, 'HP' )
                    this.formatCastleFunction( castle, html, CastleFunctions.RegenerateMp, 'MP' )
                    this.formatCastleFunction( castle, html, CastleFunctions.RestoreExpOnDeath, 'XP' )

                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'other':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {
                    let html = DataManager.getHtmlData().getItem( this.getPath( 'castledeco-AE01.html' ) )

                    this.formatCastleFunction( castle, html, CastleFunctions.Teleport, 'TP' )
                    this.formatCastleFunction( castle, html, CastleFunctions.SupportBuffs, 'BF' )

                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'HP':
                return this.getConfirmationHtml( player, npc, castle, CastleFunctions.RegenerateHp, _.parseInt( _.nth( chunks, 1 ) ) )

            case 'MP':
                return this.getConfirmationHtml( player, npc, castle, CastleFunctions.RegenerateMp, _.parseInt( _.nth( chunks, 1 ) ) )

            case 'XP':
                return this.getConfirmationHtml( player, npc, castle, CastleFunctions.RestoreExpOnDeath, _.parseInt( _.nth( chunks, 1 ) ) )

            case 'TP':
                return this.getConfirmationHtml( player, npc, castle, CastleFunctions.Teleport, _.parseInt( _.nth( chunks, 1 ) ) )

            case 'BF':
                return this.getConfirmationHtml( player, npc, castle, CastleFunctions.SupportBuffs, _.parseInt( _.nth( chunks, 1 ) ) )

            case 'set_func':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSetFunctions ) ) {
                    let [ functionValue, levelValue ] = _.tail( chunks )
                    let currentFunction: number = _.parseInt( functionValue )
                    let level = _.parseInt( levelValue )

                    if ( level === 0 ) {
                        await castle.updateFunctions( player, currentFunction, level, 0, 0, false )
                        return
                    }

                    let fee: number = this.getFunctionFee( currentFunction, level )
                    let ratio: number = this.getFunctionRatio( currentFunction )
                    if ( !await castle.updateFunctions( player, currentFunction, level, fee, ratio, !castle.getFunction( currentFunction ) ) ) {
                        return this.getPath( 'chamberlain-09.html' )
                    }

                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'functions':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleUseFunctions ) ) {
                    let HP: CastleFunction = castle.getFunction( CastleFunctions.RegenerateHp )
                    let MP: CastleFunction = castle.getFunction( CastleFunctions.RegenerateMp )
                    let XP: CastleFunction = castle.getFunction( CastleFunctions.RestoreExpOnDeath )

                    let html = DataManager.getHtmlData().getItem( this.getPath( 'castledecofunction.html' ) )
                            .replace( /%HPDepth%/g, !HP ? '0' : HP.getLevel().toString() )
                            .replace( /%MPDepth%/g, !MP ? '0' : MP.getLevel().toString() )
                            .replace( /%XPDepth%/g, !XP ? '0' : XP.getLevel().toString() )
                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'teleport':
                if ( !this.isOwner( player, npc ) || !player.hasClanPrivilege( ClanPrivilege.CastleUseFunctions ) ) {
                    return this.getPath( 'chamberlain-21.html' )
                }

                if ( !castle.getFunction( CastleFunctions.Teleport ) ) {
                    return this.getPath( 'castlefuncdisabled.html' )
                }

                return this.getPath( `${ npc.getId() }-t${ castle.getFunction( CastleFunctions.Teleport ).getLevel() }.html` )

            case 'goto':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleUseFunctions ) ) {
                    let locationId = _.parseInt( _.nth( chunks, 1 ) )

                    await TeleportLocationManager.teleportPlayer( player, locationId )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'buffer':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleUseFunctions ) ) {
                    if ( !castle.getFunction( CastleFunctions.SupportBuffs ) ) {
                        return this.getPath( 'castlefuncdisabled.html' )
                    }

                    let path = this.getPath( `castlebuff-0${ castle.getFunction( CastleFunctions.SupportBuffs ).getLevel() }.html` )
                    let html = DataManager.getHtmlData().getItem( path )
                            .replace( /%MPLeft%/g, Math.floor( npc.getCurrentMp() ).toString() )
                    player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'cast_buff':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleUseFunctions ) ) {
                    if ( !castle.getFunction( CastleFunctions.SupportBuffs ) ) {
                        return this.getPath( 'castlefuncdisabled.html' )
                    }

                    let holder = _.nth( buffs, _.parseInt( _.nth( chunks, 1 ) ) )
                    if ( holder ) {
                        let path: string

                        if ( holder.getSkill().getFinishCostMp() < npc.getCurrentMp() ) {
                            npc.setTarget( player )
                            await npc.doCastWithHolder( holder )
                            path = this.getPath( 'castleafterbuff.html' )
                        } else {
                            path = this.getPath( 'castlenotenoughmp.html' )
                        }

                        let html = DataManager.getHtmlData().getItem( path )
                                .replace( /%MPLeft%/g, Math.floor( npc.getCurrentMp() ).toString() )
                        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), npc.getObjectId() ) )
                    }

                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'list_siege_clans':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSiege ) ) {
                    castle.getSiege().listRegisterClan( player )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'list_territory_clans':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleSiege ) ) {
                    player.sendOwnedData( ExShowDominionRegistry( castle.getResidenceId(), player ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'manor':
                if ( ConfigManager.general.allowManor() ) {
                    if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleManor ) ) {
                        return this.getPath( 'manor.html' )
                    }


                    return this.getPath( 'chamberlain-21.html' )
                }

                player.sendMessage( 'Manor system is deactivated.' )
                return

            case 'products':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleUseFunctions ) ) {
                    let html = DataManager.getHtmlData().getItem( this.getPath( 'chamberlain-22.html' ) )
                            .replace( /%npcId%/g, npc.getId().toString() )
                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'buy':
                if ( this.isOwner( player, npc ) && player.hasClanPrivilege( ClanPrivilege.CastleUseFunctions ) ) {
                    ( npc as L2MerchantInstance ).showBuyWindow( player, _.parseInt( _.nth( chunks, 1 ) ) )
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'give_crown':
                if ( castle.getSiege().isInProgress() ) {
                    return this.getPath( 'chamberlain-08.html' )
                }

                if ( npc.isLord( player ) ) {
                    if ( QuestHelper.hasQuestItems( player, CROWN ) ) {
                        return this.getPath( 'chamberlain-24.html' )
                    }

                    let html = DataManager.getHtmlData().getItem( this.getPath( 'chamberlain-25.html' ) )
                            .replace( /%owner_name%/g, player.getName() )
                            .replace( /%feud_name%/g, ( 1001000 + castle.getResidenceId() ).toString() )
                    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )

                    await QuestHelper.giveSingleItem( player, CROWN, 1 )
                    return
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'manors_cert':
                if ( npc.isLord( player ) ) {
                    if ( castle.getSiege().isInProgress() ) {
                        return this.getPath( 'chamberlain-08.html' )
                    }

                    if ( SevenSigns.getPlayerSide( player.getObjectId() ) === SevenSignsSide.Dawn
                            && SevenSigns.isCompetitionPeriod() ) {
                        let ticketCount = castle.getTicketBuyCount()
                        if ( ticketCount < ( ConfigManager.sevenSigns.getSevenSignsDawnTicketQuantity() / ConfigManager.sevenSigns.getSevenSignsDawnTicketBundle() ) ) {

                            let html = DataManager.getHtmlData().getItem( this.getPath( 'ssq_selldawnticket.html' ) )
                                    .replace( /%DawnTicketLeft%/g, ( ConfigManager.sevenSigns.getSevenSignsDawnTicketQuantity() - ( ticketCount * ConfigManager.sevenSigns.getSevenSignsDawnTicketBundle() ) ).toString() )
                                    .replace( /%DawnTicketBundle%/g, ConfigManager.sevenSigns.getSevenSignsDawnTicketBundle().toString() )
                                    .replace( /%DawnTicketPrice%/g, ( ConfigManager.sevenSigns.getSevenSignsDawnTicketPrice() * ConfigManager.sevenSigns.getSevenSignsDawnTicketBundle() ).toString() )
                            player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
                            return
                        }

                        return this.getPath( 'ssq_notenoughticket.html' )
                    }

                    return this.getPath( 'ssq_notdawnorevent.html' )
                }

                return this.getPath( 'chamberlain-21.html' )

            case 'manors_cert_confirm':
                if ( npc.isLord( player ) ) {
                    if ( castle.getSiege().isInProgress() ) {
                        return this.getPath( 'chamberlain-08.html' )
                    }

                    if ( SevenSigns.getPlayerSide( player.getObjectId() ) === SevenSignsSide.Dawn
                            && SevenSigns.isCompetitionPeriod() ) {

                        let ticketCount = castle.getTicketBuyCount()
                        if ( ticketCount < ( ConfigManager.sevenSigns.getSevenSignsDawnTicketQuantity() / ConfigManager.sevenSigns.getSevenSignsDawnTicketBundle() ) ) {
                            let totalCost = ConfigManager.sevenSigns.getSevenSignsDawnTicketPrice() * ConfigManager.sevenSigns.getSevenSignsDawnTicketBundle()
                            if ( player.getAdena() >= totalCost ) {
                                await QuestHelper.takeSingleItem( player, ItemTypes.Adena, totalCost )
                                await QuestHelper.giveSingleItem( player, ConfigManager.sevenSigns.getSevenSignsManorsAgreementId(), ConfigManager.sevenSigns.getSevenSignsDawnTicketBundle() )
                                castle.setTicketBuyCount( ticketCount + 1 )
                            }

                            return this.getPath( 'chamberlain-09.html' )
                        }

                        return this.getPath( 'ssq_notenoughticket.html' )
                    }

                    return this.getPath( 'ssq_notdawnorevent.html' )
                }

                return this.getPath( 'chamberlain-21.html' )
            default:
                break
        }
    }

    getDoorUpgradePrice( type: number, level: number ): number {
        let multiplier: number = 1
        switch ( SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) ) {
            case SevenSignsSide.Dusk:
                multiplier = 3
                break

            case SevenSignsSide.Dawn:
                multiplier = 0.8
                break
        }

        switch ( type ) {
            case 1: // Outer Door
                switch ( level ) {
                    case 2:
                        return ConfigManager.castle.getOuterDoorUpgradePriceLvl2() * multiplier

                    case 3:
                        return ConfigManager.castle.getOuterDoorUpgradePriceLvl3() * multiplier

                    case 5:
                        return ConfigManager.castle.getOuterDoorUpgradePriceLvl5() * multiplier
                }
                break


            case 2: // Inner Door
                switch ( level ) {
                    case 2:
                        return ConfigManager.castle.getInnerDoorUpgradePriceLvl2() * multiplier

                    case 3:
                        return ConfigManager.castle.getInnerDoorUpgradePriceLvl3() * multiplier

                    case 5:
                        return ConfigManager.castle.getInnerDoorUpgradePriceLvl5() * multiplier
                }
                break

            case 3: // Wall
                switch ( level ) {
                    case 2:
                        return ConfigManager.castle.getWallUpgradePriceLvl2() * multiplier

                    case 3:
                        return ConfigManager.castle.getWallUpgradePriceLvl3() * multiplier

                    case 5:
                        return ConfigManager.castle.getWallUpgradePriceLvl5() * multiplier

                }
                break
        }

        return 0
    }

    getFortStatus( fortState: number ): string {
        switch ( fortState ) {
            case 1:
                return '1300122'

            case 2:
                return '1300124'
        }

        return '1300123'
    }

    getFunctionFee( type: number, level: number ): number {
        switch ( type ) {
            case CastleFunctions.RestoreExpOnDeath:
                return ( level === 45 ) ? ConfigManager.castle.getExpRegenerationFeeLvl1() : ConfigManager.castle.getExpRegenerationFeeLvl2()

            case CastleFunctions.RegenerateHp:
                return ( level === 300 ) ? ConfigManager.castle.getHpRegenerationFeeLvl1() : ConfigManager.castle.getHpRegenerationFeeLvl2()

            case CastleFunctions.RegenerateMp:
                return ( level === 40 ) ? ConfigManager.castle.getMpRegenerationFeeLvl1() : ConfigManager.castle.getMpRegenerationFeeLvl2()

            case CastleFunctions.SupportBuffs:
                return ( level === 5 ) ? ConfigManager.castle.getSupportFeeLvl1() : ConfigManager.castle.getSupportFeeLvl2()

            case CastleFunctions.Teleport:
                return ( level === 1 ) ? ConfigManager.castle.getTeleportFunctionFeeLvl1() : ConfigManager.castle.getTeleportFunctionFeeLvl2()
        }

        return 0
    }

    getFunctionRatio( type: number ): number {
        switch ( type ) {
            case CastleFunctions.RestoreExpOnDeath:
                return ConfigManager.castle.getExpRegenerationFunctionFeeRatio()

            case CastleFunctions.RegenerateHp:
                return ConfigManager.castle.getHpRegenerationFunctionFeeRatio()

            case CastleFunctions.RegenerateMp:
                return ConfigManager.castle.getMpRegenerationFunctionFeeRatio()

            case CastleFunctions.SupportBuffs:
                return ConfigManager.castle.getSupportFunctionFeeRatio()

            case CastleFunctions.Teleport:
                return ConfigManager.castle.getTeleportFunctionFeeRatio()
        }

        return 0
    }

    getSealOwner( id: number ): string {
        switch ( SevenSigns.getWinningSealSide( id ) ) {
            case SevenSignsSide.Dawn:
                return '1000511'
            case SevenSignsSide.Dusk:
                return '1000510'
        }

        return '1000512'
    }

    getTaxLimit(): number {
        switch ( SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) ) {
            case SevenSignsSide.Dawn:
                return 25

            case SevenSignsSide.Dusk:
                return 5
        }

        return 15
    }

    getTrapUpgradePrice( level: number ): number {
        let multiplier: number = 1
        switch ( SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) ) {
            case SevenSignsSide.Dusk:
                multiplier = 3
                break

            case SevenSignsSide.Dawn:
                multiplier = 0.8
                break
        }

        switch ( level ) {
            case 1:
                return ConfigManager.castle.getTrapUpgradePriceLvl1() * multiplier

            case 2:
                return ConfigManager.castle.getTrapUpgradePriceLvl2() * multiplier

            case 3:
                return ConfigManager.castle.getTrapUpgradePriceLvl3() * multiplier

            case 4:
                return ConfigManager.castle.getTrapUpgradePriceLvl4() * multiplier
        }

        return 0
    }

    isDomainFortressInContractStatus( castleId: number ): boolean {
        let amount = ( ( castleId === 1 ) || ( castleId === 5 ) ) ? 2 : 1

        return _.some( _.take( fortressMap[ castleId ], amount ), ( fortId: number ) => {
            return FortManager.getFortById( fortId ).getFortState() === 2
        } )
    }

    isOwner( player: L2PcInstance, npc: L2Npc ): boolean {
        return player.hasActionOverride( PlayerActionOverride.CastleRights )
                || ( player.getClan() && ( player.getClanId() === npc.getCastle().getOwnerId() ) )

    }

    async onNpcManorBypassEvent( npc: L2Npc, player: L2PcInstance, requestId: number, manorId: number, isNextPeriod: boolean ): Promise<void> {
        if ( this.isOwner( player, npc ) ) {
            if ( CastleManorManager.isUnderMaintenance() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_MANOR_SYSTEM_IS_CURRENTLY_UNDER_MAINTENANCE ) )
                return
            }

            let castleId = ( manorId === -1 ) ? npc.getCastle().getResidenceId() : manorId
            switch ( requestId ) {
                case 3: // Seed info
                    return player.sendOwnedData( ExShowSeedInfo( castleId, isNextPeriod, true ) )

                case 4: // Crop info
                    return player.sendOwnedData( ExShowCropInfo( castleId, isNextPeriod, true ) )

                case 5: // Basic info
                    return player.sendOwnedData( ExShowManorDefaultInfo( true ) )

                case 7: // Seed settings
                    if ( CastleManorManager.isManorApproved() ) {
                        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.A_MANOR_CANNOT_BE_SET_UP_BETWEEN_4_30_AM_AND_8_PM ) )
                    }

                    return player.sendOwnedData( ExShowSeedSetting( castleId ) )

                case 8: // Crop settings
                    if ( CastleManorManager.isManorApproved() ) {
                        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.A_MANOR_CANNOT_BE_SET_UP_BETWEEN_4_30_AM_AND_8_PM ) )
                    }

                    return player.sendOwnedData( ExShowCropSetting( castleId ) )
            }
        }
    }
}