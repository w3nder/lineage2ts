import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2World } from '../../gameService/L2World'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { NpcApproachedForTalkEvent, NpcGeneralEvent, NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const npcId: number = 32367

const ART_OF_PERSUASION_ID = 184
const NIKOLAS_COOPERATION_ID = 185

// TODO : convert event strings to enum

export class Alarm extends ListenerLogic {
    constructor() {
        super( 'Alarm', 'listeners/npcs/Alarm.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Alarm'
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getSpawnIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( this.verifyMemoState( player, ART_OF_PERSUASION_ID, 3 ) || this.verifyMemoState( player, NIKOLAS_COOPERATION_ID, 3 ) ) {
            let [ currentPlayer ] = this.getVariableValues( npc )
            if ( player.getObjectId() === currentPlayer.getObjectId() ) {
                return this.getPath( '32367-01.html' )
            }

            return this.getPath( '32367-02.html' )
        }
        return QuestHelper.getNoQuestMessagePath()
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        switch ( event ) {
            case 'SELF_DESTRUCT_IN_60':
                this.startQuestTimer( 'SELF_DESTRUCT_IN_30', 30000, data.characterId )
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_ALARM_WILL_SELF_DESTRUCT_IN_60_SECONDS_ENTER_PASSCODE_TO_OVERRIDE )
                return

            case 'SELF_DESTRUCT_IN_30':
                this.startQuestTimer( 'SELF_DESTRUCT_IN_10', 20000, data.characterId )
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_ALARM_WILL_SELF_DESTRUCT_IN_30_SECONDS_ENTER_PASSCODE_TO_OVERRIDE )
                return

            case 'SELF_DESTRUCT_IN_10':
                this.startQuestTimer( 'RECORDER_CRUSHED', 10000, data.characterId )
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_ALARM_WILL_SELF_DESTRUCT_IN_10_SECONDS_ENTER_PASSCODE_TO_OVERRIDE )
                return

            case 'RECORDER_CRUSHED':
                let [ recorderPlayer, recorderNpc ] = this.getVariableValues( npc )
                if ( recorderNpc ) {
                    if ( NpcVariablesManager.get( recorderNpc.getObjectId(), 'SPAWNED' ) ) {
                        NpcVariablesManager.set( recorderNpc.getObjectId(), 'SPAWNED', false )

                        if ( recorderPlayer ) {
                            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.RECORDER_CRUSHED )

                            if ( this.verifyMemoState( recorderPlayer, ART_OF_PERSUASION_ID, -1 ) ) {
                                this.setMemoState( recorderPlayer, ART_OF_PERSUASION_ID, 5 )
                            } else if ( this.verifyMemoState( recorderPlayer, NIKOLAS_COOPERATION_ID, -1 ) ) {
                                this.setMemoState( recorderPlayer, NIKOLAS_COOPERATION_ID, 5 )
                            }
                        }
                    }
                }
                await npc.deleteMe()
                return

            case '32367-184_04.html':
            case '32367-184_06.html':
            case '32367-184_08.html':
                return this.getPath( event )

            case '2':
                let [ currentPlayer ] = this.getVariableValues( npc )
                if ( currentPlayer.getObjectId() === player.getObjectId() ) {
                    if ( this.verifyMemoState( player, ART_OF_PERSUASION_ID, 3 ) ) {
                        return this.getPath( '32367-184_02.html' )
                    }

                    if ( this.verifyMemoState( player, NIKOLAS_COOPERATION_ID, 3 ) ) {
                        return this.getPath( '32367-185_02.html' )
                    }
                }
                return

            case '3':
                if ( this.verifyMemoState( player, ART_OF_PERSUASION_ID, 3 ) ) {
                    this.setMemoStateEx( player, ART_OF_PERSUASION_ID, 1, 1 )
                    return this.getPath( '32367-184_04.html' )
                }

                if ( this.verifyMemoState( player, NIKOLAS_COOPERATION_ID, 3 ) ) {
                    this.setMemoStateEx( player, NIKOLAS_COOPERATION_ID, 1, 1 )
                    return this.getPath( '32367-185_04.html' )
                }

                return

            case '4':
                if ( this.verifyMemoState( player, ART_OF_PERSUASION_ID, 3 ) ) {
                    this.setMemoStateEx( player, ART_OF_PERSUASION_ID, 1, this.getMemoStateEx( player, ART_OF_PERSUASION_ID, 1 ) + 1 )
                    return this.getPath( '32367-184_06.html' )
                }

                if ( this.verifyMemoState( player, NIKOLAS_COOPERATION_ID, 3 ) ) {
                    this.setMemoStateEx( player, NIKOLAS_COOPERATION_ID, 1, this.getMemoStateEx( player, NIKOLAS_COOPERATION_ID, 1 ) + 1 )
                    return this.getPath( '32367-185_06.html' )
                }
                return

            case '5':
                if ( this.verifyMemoState( player, ART_OF_PERSUASION_ID, 3 ) ) {
                    this.setMemoStateEx( player, ART_OF_PERSUASION_ID, 1, this.getMemoStateEx( player, ART_OF_PERSUASION_ID, 1 ) + 1 )
                    return this.getPath( '32367-184_08.html' )
                }

                if ( this.verifyMemoState( player, NIKOLAS_COOPERATION_ID, 3 ) ) {
                    this.setMemoStateEx( player, NIKOLAS_COOPERATION_ID, 1, this.getMemoStateEx( player, NIKOLAS_COOPERATION_ID, 1 ) + 1 )
                    return this.getPath( '32367-185_08.html' )
                }

                return

            case '6':
                if ( this.verifyMemoState( player, ART_OF_PERSUASION_ID, 3 ) ) {
                    let state = this.getMemoStateEx( player, ART_OF_PERSUASION_ID, 1 )
                    if ( state >= 3 ) {
                        let [ currentPlayer, currentNpc ] = this.getVariableValues( npc )
                        if ( currentNpc && NpcVariablesManager.get( currentNpc.getObjectId(), 'SPAWNED' ) ) {
                            NpcVariablesManager.set( currentNpc.getObjectId(), 'SPAWNED', false )
                        }
                        await npc.deleteMe()
                        this.setMemoState( player, ART_OF_PERSUASION_ID, 4 )
                        return this.getPath( '32367-184_09.html' )
                    }

                    this.setMemoStateEx( player, ART_OF_PERSUASION_ID, 1, 0 )
                    return this.getPath( '32367-184_10.html' )
                }

                if ( this.verifyMemoState( player, NIKOLAS_COOPERATION_ID, 3 ) ) {
                    let state = this.getMemoStateEx( player, NIKOLAS_COOPERATION_ID, 1 )
                    if ( state >= 3 ) {
                        let [ currentPlayer, currentNpc ] = this.getVariableValues( npc )
                        if ( currentNpc && NpcVariablesManager.get( currentNpc.getObjectId(), 'SPAWNED' ) ) {
                            NpcVariablesManager.set( currentNpc.getObjectId(), 'SPAWNED', false )
                        }

                        await npc.deleteMe()
                        this.setMemoState( player, NIKOLAS_COOPERATION_ID, 4 )
                        return this.getPath( '32367-185_09.html' )
                    }

                    this.setMemoStateEx( player, NIKOLAS_COOPERATION_ID, 1, 0 )
                    return this.getPath( '32367-185_10.html' )
                }
                return
        }
    }

    async onSpawnEvent( data : NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        this.startQuestTimer( 'SELF_DESTRUCT_IN_60', 60000, data.characterId )
        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.INTRUDER_ALERT_THE_ALARM_WILL_SELF_DESTRUCT_IN_2_MINUTES )
        let [ currentPlayer ] = this.getVariableValues( npc )
        if ( currentPlayer ) {
            currentPlayer.sendCopyData( SoundPacket.ITEMSOUND_SIREN )
        }
    }

    getMemoStateEx( player: L2PcInstance, questId: number, slot: number ): number {
        const getValue = ( state: QuestState ): number => {
            return state ? state.getMemoStateEx( slot ) : -1
        }

        switch ( questId ) {
            case ART_OF_PERSUASION_ID:
                return getValue( QuestStateCache.getQuestState( player.getObjectId(), 'Q00184_ArtOfPersuasion' ) )

            case NIKOLAS_COOPERATION_ID:
                return getValue( QuestStateCache.getQuestState( player.getObjectId(), 'Q00185_NikolasCooperation' ) )
        }

        return -1
    }

    getVariableValues( currentNpc: L2Npc ): [ L2PcInstance, L2Npc ] {
        let player: L2PcInstance = L2World.getPlayer( NpcVariablesManager.get( currentNpc.getObjectId(), 'player0' ) as number )
        let npc: L2Npc = L2World.getObjectById( NpcVariablesManager.get( currentNpc.getObjectId(), 'npc0' ) as number ) as L2Npc

        return [ player, npc ]
    }

    setMemoState( player: L2PcInstance, questId: number, value: number ): void {
        const setState = ( state: QuestState ) => {
            return state && state.setMemoState( value )
        }

        switch ( questId ) {
            case ART_OF_PERSUASION_ID:
                return setState( QuestStateCache.getQuestState( player.getObjectId(), 'Q00184_ArtOfPersuasion' ) )

            case NIKOLAS_COOPERATION_ID:
                return setState( QuestStateCache.getQuestState( player.getObjectId(), 'Q00185_NikolasCooperation' ) )
        }
    }

    setMemoStateEx( player: L2PcInstance, questId: number, slot: number, value: number ): void {
        const setState = ( state: QuestState ) => {
            return state && state.setMemoStateEx( slot, value )
        }

        switch ( questId ) {
            case ART_OF_PERSUASION_ID:
                return setState( QuestStateCache.getQuestState( player.getObjectId(), 'Q00184_ArtOfPersuasion' ) )

            case NIKOLAS_COOPERATION_ID:
                return setState( QuestStateCache.getQuestState( player.getObjectId(), 'Q00185_NikolasCooperation' ) )
        }
    }

    verifyMemoState( player: L2PcInstance, questId: number, desiredValue: number ): boolean {
        const isValid = ( state: QuestState ): boolean => {
            return state && ( ( desiredValue < 0 ) || state.isMemoState( desiredValue ) )
        }

        switch ( questId ) {
            case ART_OF_PERSUASION_ID:
                return isValid( QuestStateCache.getQuestState( player.getObjectId(), 'Q00184_ArtOfPersuasion' ) )

            case NIKOLAS_COOPERATION_ID:
                return isValid( QuestStateCache.getQuestState( player.getObjectId(), 'Q00185_NikolasCooperation' ) )
        }

        return false
    }
}