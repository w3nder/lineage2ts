import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { DataManager } from '../../data/manager'
import { GameTime } from '../../gameService/cache/GameTime'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestStateLimits } from '../../gameService/models/quest/State'
import { L2World } from '../../gameService/L2World'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { CharacterNamesCache } from '../../gameService/cache/CharacterNamesCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { DimensionalRiftManager } from '../../gameService/instancemanager/DimensionalRiftManager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { ShowHtml } from '../helpers/ShowHtml'
import { SevenSigns } from '../../gameService/directives/SevenSigns'
import { SevenSignsSide } from '../../gameService/values/SevenSignsValues'
import _ from 'lodash'
import { Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    31488,
    31489,
    31490,
    31491,
    31492,
    31493
]

const npcToItemAmount : Record<number, number> = {
    31488: 21,
    31489: 24,
    31490: 27,
    31491: 30,
    31492: 33,
    31493: 36,
}

const npcToRoomType : Record<number, number> = {
    31488: 1,
    31489: 2,
    31490: 3,
    31491: 4,
    31492: 5,
    31493: 6,
}

const DIMENSIONAL_FRAGMENT = 7079

const duskTeleportLocations : Array<Location> = [
    new Location( -81328, 86536, -5152 ),
    new Location( -81262, 86468, -5152 ),
    new Location( -81248, 86582, -5152 )
]

const dawnTeleportLocations : Array<Location> = [
    new Location( -80316, 111356, -4896 ),
    new Location( -80226, 111290, -4896 ),
    new Location( -80217, 111435, -4896 )
]

type TeleportIndex = 0 | 1

const dualTeleportLocations : Array<[ Location, Location ]> = [
        [ new Location( -80542, 150315, -3040 ), new Location( -82340, 151575, -3120 ) ],
        [ new Location( -13996, 121413, -2984 ), new Location( -14727, 124002, -3112 ) ],
        [ new Location( 16320, 142915, -2696 ), new Location( 18501, 144673, -3056 ) ],
        [ new Location( 83312, 149236, -3400 ), new Location( 81572, 148580, -3464 ) ],
        [ new Location( 111359, 220959, -3544 ), new Location( 112441, 220149, -3544 ) ],
        [ new Location( 83057, 53983, -1488 ), new Location( 82842, 54613, -1520 ) ],
        [ new Location( 146955, 26690, -2200 ), new Location( 147528, 28899, -2264 ) ],
        [ new Location( 115206, 74775, -2600 ), new Location( 116651, 77512, -2688 ) ],
        [ new Location( 148326, -55533, -2776 ), new Location( 149968, -56645, -2976 ) ],
        [ new Location( 45605, -50360, -792 ), new Location( 44505, -48331, -792 ) ],
        [ new Location( 86730, -143148, -1336 ), new Location( 85048, -142046, -1536 ) ]
]

const singleTeleportLocation : Array<Location> = [
        new Location( -41443, 210030, -5080 ),
        new Location( -53034, -250421, -7935 ),
        new Location( 45160, 123605, -5408 ),
        new Location( 46488, 170184, -4976 ),
        new Location( 111521, 173905, -5432 ),
        new Location( -20395, -250930, -8191 ),
        new Location( -21482, 77253, -5168 ),
        new Location( 140688, 79565, -5424 ),
        new Location( -52007, 78986, -4736 ),
        new Location( 118547, 132669, -4824 ),
        new Location( 172562, -17730, -4896 ),
        new Location( 83344, 209110, -5432 ),
        new Location( -19154, 13415, -4896 ),
        new Location( 12747, -248614, -9607 ),
        new Location( -41559, 209140, -5080 ),
        new Location( 42448, 143943, -5376 ),
        new Location( 45239, 124522, -5408 ),
        new Location( 45680, 170299, -4976 ),
        new Location( 110659, 174008, -5432 ),
        new Location( 77132, 78399, -5120 ),
        new Location( -22408, 77375, -5168 ),
        new Location( 139807, 79675, -5424 ),
        new Location( -53177, 79100, -4736 ),
        new Location( 117647, 132801, -4824 ),
        new Location( 171684, -17602, -4896 ),
        new Location( 82456, 209218, -5432 ),
        new Location( -20105, 13505, -4896 ),
        new Location( 113299, 84547, -6536 )
]

export class RiftWatcher extends ListenerLogic {
    constructor() {
        super( 'RiftWatcher', 'listeners/npcs/RiftWatcher.ts' )
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( 'rift_watcher_1001.htm' )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/RiftWatcher'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName.endsWith( '.htm' ) ) {
            return this.getPath( `rift_watcher_${data.eventName}` )
        }

        let [ type, choice ] = data.eventName.split( ';' )
        if ( type !== '635' ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        if ( !player.isInventoryUnder80( false ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INVENTORY_LESS_THAN_80_PERCENT ) )
            return
        }

        switch ( choice ) {
            case '1':
                return this.onStartRift( data )

            case '2':
                return this.onExitRift( data )

            case '3':
                return this.onSevenSignsTeleport( data )
        }
    }

    private async onStartRift( data: NpcGeneralEvent ) : Promise<string> {
        let party = PlayerGroupCache.getParty( data.playerId )
        if ( !party ) {
            return this.getPath( 'rift_watcher_1_q0635_01.htm' )
        }

        if ( party.getLeaderObjectId() !== data.playerId ) {
            let path = this.getPath( 'rift_watcher_1_q0635_02.htm' )
            return DataManager.getHtmlData().getItem( path ).replace( '<?name?>', party.getLeader().getName() )
        }

        let time = GameTime.getGameTime()
        let amount = npcToItemAmount[ data.characterNpcId ]

        for ( const playerId of party.getMembers() ) {
            if ( QuestStateCache.getAllActiveStatesSize( playerId ) < ( QuestStateLimits.MaximumQuestsAllowed - 1 ) && !QuestStateCache.hasQuestState( playerId, 'Q00635_IntoTheDimensionalRift' ) ) {
                let path = this.getPath( 'rift_watcher_1_q0635_03a.htm' )
                return DataManager.getHtmlData().getItem( path ).replace( '<?name?>', CharacterNamesCache.getCachedNameById( playerId ) )
            }

            let player = L2World.getPlayer( playerId )
            if ( !QuestHelper.hasQuestItemCount( player, DIMENSIONAL_FRAGMENT, amount ) ) {
                let state = QuestStateCache.getQuestState( playerId, 'Q00635_IntoTheDimensionalRift', true )
                let memoValue = state.getMemoStateEx( 1 ) ?? 0

                if ( !state.isCompleted() || ( time - memoValue ) > 3600 ) {
                    let path = this.getPath( 'rift_watcher_1_q0635_03.htm' )
                    return DataManager.getHtmlData().getItem( path ).replace( '<?name?>', CharacterNamesCache.getCachedNameById( playerId ) )
                }
            }
        }

        let roomType = npcToRoomType[ data.characterNpcId ]
        if ( !DimensionalRiftManager.isAllowedToEnter( roomType ) ) {
            return this.getPath( 'rift_watcher_1_q0635_04.htm' )
        }

        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        for ( const playerId of party.getMembers() ) {
            let player = L2World.getPlayer( playerId )
            let distance = npc.calculateDistance( player, true )

            if ( distance > 1500 ) {
                continue
            }

            let state = QuestStateCache.getQuestState( playerId, 'Q00635_IntoTheDimensionalRift', true )
            if ( state.getMemoState() !== 2 ) {
                await QuestHelper.takeSingleItem( player, DIMENSIONAL_FRAGMENT, amount )
            }

            state.setMemoState( time )
            state.setMemoStateEx( 1, party.getLeaderObjectId() === playerId ? 1 : 0 )
        }

        let partyLeader = L2World.getPlayer( data.playerId )
        await DimensionalRiftManager.start( partyLeader, roomType, npc )

        ShowHtml.showPath( partyLeader, this.getPath( 'rift_watcher_1_q0635_05.htm' ) )
    }

    private async onSevenSignsTeleport( data: NpcGeneralEvent ) : Promise<string> {
        if ( !SevenSigns.isCompetitionPeriod() ) {
            return this.getPath( 'rift_watcher_1_q0635_12.htm' )
        }

        let side = SevenSigns.getPlayerSide( data.playerId )
        let player = L2World.getPlayer( data.playerId )

        switch ( side ) {
            case SevenSignsSide.None:
                return this.getPath( 'rift_watcher_1_q0635_11.htm' )

            case SevenSignsSide.Dusk:
                await player.teleportToLocation( _.sample( duskTeleportLocations ) )
                return this.getPath( 'rift_watcher_1_q0635_10.htm' )

            case SevenSignsSide.Dawn:
                await player.teleportToLocation( _.sample( dawnTeleportLocations ) )
                return this.getPath( 'rift_watcher_1_q0635_10.htm' )
        }
    }

    getTeleportIndex( playerId: number ) : TeleportIndex {
        return SevenSigns.isCompetitionPeriod() && SevenSigns.getPlayerSide( playerId ) === SevenSignsSide.Dawn ? 0 : 1
    }

    getDualTeleportIndex( value: number ) : number {
        return Math.floor( ( value - 95 ) / 100 )
    }

    getExitTeleportLocation( value: number, playerId: number ) : Location {
        let dualTeleportValue = value % 10000

        if ( dualTeleportValue >= 95 ) {
            let dualTeleportData = dualTeleportLocations[ this.getDualTeleportIndex( dualTeleportValue ) ]

            if ( dualTeleportData ) {
                return dualTeleportData[ this.getTeleportIndex( playerId ) ]
            }
        }

        let teleportValue = ( ( value - dualTeleportValue ) + 5 ) / 10000
        return singleTeleportLocation[ teleportValue - 1 ] ?? _.sample( singleTeleportLocation )
    }

    private async onExitRift( data: NpcGeneralEvent ) : Promise<string> {
        let tutorialState = QuestStateCache.getQuestState( data.playerId, 'Q00255_Tutorial', true )
        let riftState = QuestStateCache.getQuestState( data.playerId, 'Q00635_IntoTheDimensionalRift' )

        if ( riftState ) {
            await riftState.exitQuest( true, false )
        }

        let location = this.getExitTeleportLocation( tutorialState.getMemoStateEx( 1 ), data.playerId )
        let player = L2World.getPlayer( data.playerId )

        await player.teleportToLocation( location )

        return this.getPath( 'rift_watcher_1_q0635_06.htm' )
    }
}