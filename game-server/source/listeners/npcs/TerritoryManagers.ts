import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { ClassId } from '../../gameService/models/base/ClassId'
import { DataManager } from '../../data/manager'
import { ConfigManager } from '../../config/ConfigManager'
import { NpcHtmlMessage, NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { MultisellCache } from '../../gameService/cache/MultisellCache'
import { UserInfo } from '../../gameService/packets/send/UserInfo'
import { ExBrExtraUserInfo } from '../../gameService/packets/send/ExBrExtraUserInfo'
import { ListenerManager } from '../../gameService/instancemanager/ListenerManager'
import { Race } from '../../gameService/enums/Race'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { TerritoryWarItems, TerritoryWarManager } from '../../gameService/instancemanager/TerritoryWarManager'
import { CastleManager } from '../../gameService/instancemanager/CastleManager'
import { ActionFailed } from '../../gameService/packets/send/ActionFailed'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { InventoryAction } from '../../gameService/enums/InventoryAction'
import _ from 'lodash'
import aigle from 'aigle'

const PRECIOUS_SOUL_1_ITEM_IDS = [
    7587,
    7588,
    7589,
    7597,
    7598,
    7599,
]

const PRECIOUS_SOUL_2_ITEM_IDS = [
    7595,
]

const PRECIOUS_SOUL_3_ITEM_IDS = [
    7678,
    7591,
    7592,
    7593,
]

const npcIds: Array<number> = _.times( 9, ( index ) => index + 36490 )
const itemIds: Array<number> = [
    7678, // Caradine's Letter
    7679, // Caradine's Letter
    5011, // Star of Destiny
    1239, // Virgil's Letter
    1246, // Arkenia's Letter
]

export class TerritoryManagers extends ListenerLogic {
    constructor() {
        super( 'TerritoryManagers', 'listeners/npcs/TerritoryManagers.ts' )
    }

    async deleteIfExist( player: L2PcInstance, itemId: number ): Promise<void> {
        let item: L2ItemInstance = player.getInventory().getItemByItemId( itemId )
        if ( item ) {
            await player.destroyItem( item, true, 'Quest:TerritoryManagers' )
        }
    }

    getCalulatedRewardHtml( reward: [ number, number ], territoryId: number ): string {
        if ( TerritoryWarManager.isTWInProgress || reward[ 0 ] === 0 ) {
            return DataManager.getHtmlData().getItem( this.getPath( 'reward-0a.html' ) )
        }

        if ( reward[ 0 ] !== territoryId ) {
            return DataManager.getHtmlData().getItem( this.getPath( 'reward-0b.html' ) )
                              .replace( /%castle%/g, CastleManager.getCastleById( reward[ 0 ] - 80 ).getName() )
        }

        if ( reward[ 1 ] === 0 ) {
            return DataManager.getHtmlData().getItem( this.getPath( 'reward-0a.html' ) )
        }

        return DataManager.getHtmlData().getItem( this.getPath( 'reward-1.html' ) )
                          .replace( /%castle%/g, CastleManager.getCastleById( reward[ 0 ] - 80 ).getName() )
                          .replace( /%badge%/g, reward[ 1 ].toString() )
                          .replace( /%adena%/, ( reward[ 1 ] * 5000 ).toString() )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/TerritoryManagers'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( ClassId.getLevelByClassId( player.getClassId() ) < 2
                || player.getLevel() < 40 ) {
            return this.getPath( '36490-08.html' )
        }

        return this.getPath( `${ data.characterNpcId }.html` )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        let npcId = npc.getId()
        let itemId = 13757 + ( npcId - 36490 )
        let territoryId = 81 + ( npcId - 36490 )

        switch ( event ) {
            case '36490-04.html':
                let htmlPath = this.getPath( event )
                let html = DataManager.getHtmlData().getItem( htmlPath )
                                      .replace( /%badge%/g, ConfigManager.territoryWar.getMinTerritoryBadgeForNobless().toString() )
                player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), npc.getObjectId() ) )
                return

            case 'BuyProducts':
                if ( player.getInventory().getItemByItemId( itemId ) ) {
                    // If the player has at least one Territory Badges then show the multisell.
                    let multiSellId = 364900001 + ( ( npcId - 36490 ) * 10000 )
                    MultisellCache.separateAndSend( multiSellId, player, npc, false )
                    return
                }

                return this.getPath( '36490-02.html' )

            case 'MakeMeNoble':
                if ( player.getInventory().getInventoryItemCount( itemId, -1 ) < ConfigManager.territoryWar.getMinTerritoryBadgeForNobless() ) {
                    // If the player does not have enough Territory Badges, it cannot continue.
                    return this.getPath( '36490-02.html' )
                }

                if ( player.isNoble() ) {
                    // If the player is already Noblesse, it cannot continue.
                    return this.getPath( '36490-05.html' )
                }

                if ( player.getLevel() < 75 ) {
                    // If the player is not level 75 or greater, it cannot continue.
                    return this.getPath( '36490-06.html' )
                }

                await Promise.all( [
                    // Complete the Noblesse related quests.
                    // Possessor of a Precious Soul - 1 (241)
                    this.processNoblesseQuest( player, 241, PRECIOUS_SOUL_1_ITEM_IDS ),
                    // Possessor of a Precious Soul - 2 (242)
                    this.processNoblesseQuest( player, 242, PRECIOUS_SOUL_2_ITEM_IDS ),
                    // Possessor of a Precious Soul - 3 (246)
                    this.processNoblesseQuest( player, 246, PRECIOUS_SOUL_3_ITEM_IDS ),
                    // Possessor of a Precious Soul - 4 (247)
                    this.processNoblesseQuest( player, 247, null ),
                    player.destroyItemByItemId( itemId, ConfigManager.territoryWar.getMinTerritoryBadgeForNobless(), true, 'Quest:TerritoryManagers' ),
                    player.addItem( 7694, 1, -1, npc.getObjectId(), InventoryAction.Other ),
                    player.setNoble( true ),
                ] )

                player.sendDebouncedPacket( UserInfo )
                player.sendDebouncedPacket( ExBrExtraUserInfo )

                /*
                    Complete the sub-class related quest.
                    Complete quest Seeds of Chaos (236) for Kamael characters.
                    Complete quest Mimir's Elixir (235) for other races characters.
                 */
                let quest: ListenerLogic = ListenerManager.getQuest( ( player.getRace() === Race.KAMAEL ) ? 236 : 235 )
                if ( quest ) {
                    let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), quest.getName(), true )
                    await state.exitQuest( false )
                }

                await QuestHelper.takeMultipleItems( player, -1, ...itemIds )

                return

            case 'CalcRewards':
                let reward: [ number, number ] = TerritoryWarManager.calculateReward( player )
                let rewardHtml: string = this.getCalulatedRewardHtml( reward, territoryId )
                                             .replace( /%territoryId%/g, territoryId.toString() )

                player.sendOwnedData( NpcHtmlMessage( rewardHtml, player.getObjectId(), npc.getObjectId() ) )
                player.sendOwnedData( ActionFailed() )
                return

            case 'ReceiveRewards':
                let badgeId = _.get( TerritoryWarItems, territoryId, 57 )
                let receiveReward: [ number, number ] = TerritoryWarManager.calculateReward( player )
                let receiveHtml: string = await this.getReceiveRewardHtml( receiveReward, territoryId, player, badgeId )

                player.sendOwnedData( NpcHtmlMessage( receiveHtml, player.getObjectId(), npc.getObjectId() ) )
                player.sendOwnedData( ActionFailed() )
                return
        }

        return this.getPath( event )
    }

    async getReceiveRewardHtml( reward: [ number, number ], territoryId: number, player: L2PcInstance, badgeId: number ): Promise<string> {
        if ( TerritoryWarManager.isTWInProgress || reward[ 0 ] === 0 ) {
            return DataManager.getHtmlData().getItem( this.getPath( 'reward-0a.html' ) )
        }

        if ( reward[ 0 ] !== territoryId ) {
            return DataManager.getHtmlData().getItem( this.getPath( 'reward-0b.html' ) )
                              .replace( /%castle%/g, CastleManager.getCastleById( reward[ 0 ] - 80 ).getName() )
        }

        if ( reward[ 1 ] === 0 ) {
            return DataManager.getHtmlData().getItem( this.getPath( 'reward-0a.html' ) )
        }

        await player.addItem( badgeId, reward[ 1 ], -1, player.getObjectId(), InventoryAction.Other )
        await player.addAdena( reward[ 1 ] * 5000, 'Quest:TerritoryManagers', null, true )
        TerritoryWarManager.resetReward( player )

        return DataManager.getHtmlData().getItem( this.getPath( 'reward-2.html' ) )
    }

    async processNoblesseQuest( player: L2PcInstance, questId: number, itemIds: Array<number> ): Promise<void> {
        let quest: ListenerLogic = ListenerManager.getQuest( questId )
        if ( !quest ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), quest.getName(), true )

        if ( !state.isCompleted() ) {
            await aigle.resolve( itemIds ).eachSeries( ( itemId: number ) => {
                return QuestHelper.takeSingleItem( player, itemId, -1 )
            } )

            return state.exitQuest( false )
        }
    }
}