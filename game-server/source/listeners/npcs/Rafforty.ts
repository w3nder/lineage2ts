import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const npcId: number = 32020
const NECKLACE = 16025
const BLESSED_NECKLACE = 16026
const BOTTLE = 16027

export class Rafforty extends ListenerLogic {
    constructor() {
        super( 'Rafforty', 'listeners/npcs/Rafforty.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Rafforty'
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        switch ( event ) {
            case '32020-01.html':
                if ( !QuestHelper.hasQuestItems( player, NECKLACE ) ) {
                    return this.getPath( '32020-02.html' )
                }
                break

            case '32020-04.html':
                if ( !QuestHelper.hasQuestItems( player, BOTTLE ) ) {
                    return this.getPath( '32020-05.html' )
                }
                break

            case '32020-07.html':
                if ( !QuestHelper.hasQuestItems( player, BOTTLE, NECKLACE ) ) {
                    return this.getPath( '32020-08.html' )
                }

                await QuestHelper.takeSingleItem( player, NECKLACE, 1 )
                await QuestHelper.takeSingleItem( player, BOTTLE, 1 )
                await QuestHelper.giveSingleItem( player, BLESSED_NECKLACE, 1 )
                break
        }

        return this.getPath( event )
    }
}