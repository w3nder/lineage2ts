import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { Siege } from '../../gameService/models/entity/Siege'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { NpcSay } from '../../gameService/packets/send/builder/NpcSay'
import { L2World } from '../../gameService/L2World'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const npcIds : Array<number> = [
    35095, // Mass Gatekeeper (Gludio)
    35137, // Mass Gatekeeper (Dion)
    35179, // Mass Gatekeeper (Giran)
    35221, // Mass Gatekeeper (Oren)
    35266, // Mass Gatekeeper (Aden)
    35311, // Mass Gatekeeper (Innadril)
    35355, // Mass Gatekeeper (Goddard)
    35502, // Mass Gatekeeper (Rune)
    35547, // Mass Gatekeeper (Schuttgart)
]

export class CastleTeleporter extends ListenerLogic {
    constructor() {
        super( 'CastleTeleporter', 'listeners/npcs/CastleTeleporter.ts' )
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/CastleTeleporter'
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        if ( !NpcVariablesManager.has( data.characterId, this.name ) ) {
            let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc
            let siege : Siege = npc.getCastle().getSiege()

            if ( siege.isInProgress() && ( siege.getControlTowerCount() === 0 ) ) {
                return this.getPath( 'teleporter-02.html' )
            }

            return this.getPath( 'teleporter-01.html' )
        }

        return this.getPath( 'teleporter-03.html' )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName.toLowerCase()

        if ( event === 'teleporter-03.html' ) {
            if ( NpcVariablesManager.get( data.characterId, this.name ) === false ) {
                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                let siege : Siege = npc.getCastle().getSiege()
                let time = ( siege.isInProgress() && siege.getControlTowerCount() === 0 ) ? 480000 : 30000

                this.startQuestTimer( 'teleport', time, data.characterId )
                NpcVariablesManager.set( data.characterId, this.name, true )
            }

            return event
        }

        if ( event === 'teleport' ) {
            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            let message = NpcSay.fromNpcString( npc.getObjectId(),
                    NpcSayType.NpcShout,
                    npc.getId(),
                    NpcStringIds.THE_DEFENDERS_OF_S1_CASTLE_WILL_BE_TELEPORTED_TO_THE_INNER_CASTLE,
                    npc.getCastle().getName() )
                    .getBuffer()

            await npc.getCastle().removeAllPlayers()
            NpcVariablesManager.set( data.characterId, this.name, false )


            BroadcastHelper.dataInLocation( npc.getCastle().residenceArea.getCenterPointLocation(), message, 10000 )
        }
    }
}