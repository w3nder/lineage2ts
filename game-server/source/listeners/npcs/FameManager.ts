import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ClassId } from '../../gameService/models/base/ClassId'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { UserInfo } from '../../gameService/packets/send/UserInfo'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const npcIds: Array<number> = [
    36479, // Rapidus
    36480, // Scipio
]

const minimumLevel = 40
const minimumFameCost = 5000
const reputationCost = 1000
const minimumClanLevel = 5
const classLevel = 2

export class FameManager extends ListenerLogic {
    constructor() {
        super( 'FameManager', 'listeners/npcs/FameManager.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/FameManager'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        return this.getPath( ( ( player.getFame() > 0 )
                && ( player.getLevel() >= minimumLevel )
                && ClassId.getLevelByClassId( player.getClassId() ) >= classLevel ) ? `${ data.characterNpcId }.html` : `${ data.characterNpcId }-01.html` )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        switch ( event ) {
            case '36479.html':
            case '36479-02.html':
            case '36479-07.html':
            case '36480.html':
            case '36480-02.html':
            case '36480-07.html':
                return this.getPath( event )

            case 'decreasePk':
                if ( player.getPkKills() > 0 ) {
                    if ( ( player.getFame() >= minimumFameCost )
                            && ( player.getLevel() >= minimumLevel )
                            && ClassId.getLevelByClassId( player.getClassId() ) >= classLevel ) {
                        player.setFame( player.getFame() - minimumFameCost )
                        player.setPkKills( player.getPkKills() - 1 )
                        player.sendDebouncedPacket( UserInfo )

                        return this.getPath( `${ npc.getId() }-06.html` )
                    }

                    return this.getPath( `${ npc.getId() }-01.html` )
                }

                return this.getPath( `${ npc.getId() }-05.html` )

            case 'clanRep': {
                if ( player.getClan() && player.getClan().getLevel() >= minimumClanLevel ) {
                    if ( player.getFame() >= reputationCost && player.getLevel() >= minimumLevel && ClassId.getLevelByClassId( player.getClassId() ) >= classLevel ) {
                        player.setFame( player.getFame() - reputationCost )
                        await player.getClan().addReputationScore( 50, true )
                        player.sendDebouncedPacket( UserInfo )
                        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ACQUIRED_50_CLAN_FAME_POINTS ) )

                        return this.getPath( `${ npc.getId() }-04.html` )
                    }

                    return this.getPath( `${ npc.getId() }-01.html` )
                }

                return this.getPath( `${ npc.getId() }-03.html` )
            }
        }
    }
}