import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { InstanceManager } from '../../gameService/instancemanager/InstanceManager'
import { InstanceWorld } from '../../gameService/models/instancezone/InstanceWorld'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcApproachedForTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const npcId: number = 32762
const normalTemplateId = 139
const hardTemplateId = 144

export class Sirra extends ListenerLogic {
    constructor() {
        super( 'Sirra', 'listeners/npcs/Sirra.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Sirra'
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc
        let world: InstanceWorld = InstanceManager.getWorld( npc.getInstanceId() )

        if ( world && world.getTemplateId() == normalTemplateId ) {
            return this.getPath( world.isStatus( 0 ) ? '32762-easy.html' : '32762-easyfight.html' )
        }

        if ( world && world.getTemplateId() == hardTemplateId ) {
            return this.getPath( world.isStatus( 0 ) ? '32762-hard.html' : '32762-hardfight.html' )
        }

        return this.getPath( '32762.html' )
    }
}