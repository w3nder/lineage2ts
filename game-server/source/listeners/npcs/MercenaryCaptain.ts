import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { TerritoryWarManager } from '../../gameService/instancemanager/TerritoryWarManager'
import { Territory } from '../../gameService/instancemanager/territory/Territory'
import { TerritoryNPCSpawn } from '../../gameService/instancemanager/territory/TerritoryNPCSpawn'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcHtmlMessagePath } from '../../gameService/packets/send/NpcHtmlMessage'
import { ExShowDominionRegistry } from '../../gameService/packets/send/ExShowDominionRegistry'
import { DataManager } from '../../data/manager'
import { ConfigManager } from '../../config/ConfigManager'
import { QuestHelper } from '../helpers/QuestHelper'
import { MultisellCache } from '../../gameService/cache/MultisellCache'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { ClassId } from '../../gameService/models/base/ClassId'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const npcItemMap: { [ npcId: number ]: number } = {
    36481: 13757, // Mercenary Captain (Gludio)
    36482: 13758, // Mercenary Captain (Dion)
    36483: 13759, // Mercenary Captain (Giran)
    36484: 13760, // Mercenary Captain (Oren)
    36485: 13761, // Mercenary Captain (Aden)
    36486: 13762, // Mercenary Captain (Innadril)
    36487: 13763, // Mercenary Captain (Goddard)
    36488: 13764, // Mercenary Captain (Rune)
    36489: 13765, // Mercenary Captain (Schuttgart)
}

const npcIds: Array<number> = _.keys( npcItemMap ).map( value => _.parseInt( value ) )
const timerName: string = 'Quest:MercenaryCaptain'

const STRIDER_WIND = 4422
const STRIDER_STAR = 4423
const STRIDER_TWILIGHT = 4424
const GUARDIAN_STRIDER = 14819
const ELITE_MERCENARY_CERTIFICATE = 13767
const TOP_ELITE_MERCENARY_CERTIFICATE = 13768

const striders: Array<number> = [
    4422,
    4423,
    4424,
    14819,
]

const minimumLevel = 40
const classLevel = 2

// TODO : review and implement what is missing
export class MercenaryCaptain extends ListenerLogic {
    constructor() {
        super( 'MercenaryCaptain', 'npcs/MercenaryCaptain.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/MercenaryCaptain'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async initialize(): Promise<void> {
        let quest = this
        _.each( TerritoryWarManager.getAllTerritories(), ( territory: Territory ) => {
            _.each( territory.getSpawns(), ( spawn: TerritoryNPCSpawn ) => {
                if ( npcIds.includes( spawn.getId() ) ) {
                    quest.startQuestTimer( timerName, 3600000, spawn.getNpc().getObjectId(), undefined, true )
                }
            } )
        } )
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( player.getLevel() < minimumLevel || ClassId.getLevelByClassId( player.getClassId() ) < classLevel ) {
            return this.getPath( '36481-08.html' )
        }

        if ( npc.isLord( player ) ) {
            return this.getPath( ( npc.getCastle().getSiege().isInProgress() || TerritoryWarManager.isTWInProgress ) ? '36481-05.html' : '36481-04.html' )
        }

        return this.getPath( ( npc.getCastle().getSiege().isInProgress() || TerritoryWarManager.isTWInProgress ) ? '36481-06.html' : `${ npc.getId() }-01.html` )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        if ( player ) {
            let eventChunks: Array<string> = _.split( event, ' ' )
            let keyword = _.nth( eventChunks, 0 )
            switch ( keyword ) {
                case '36481-02.html':
                    return this.getPath( keyword )

                case '36481-03.html':
                    let htmlPath = this.getPath( '36481-03.html' )
                    let html = DataManager.getHtmlData().getItem( htmlPath )
                            .replace( /%strider%/g, ConfigManager.territoryWar.getMinTerritoryBadgeForStriders().toString() )
                            .replace( /%gstrider%/g, ConfigManager.territoryWar.getMinTerritoryBadgeForBigStrider().toString() )
                    player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), npc.getObjectId() ) )
                    return

                case 'territory':
                    player.sendOwnedData( ExShowDominionRegistry( npc.getCastle().getResidenceId(), player ) )
                    return

                case 'strider':
                    let type: number = _.parseInt( _.nth( eventChunks, 1 ) )
                    let price: number = type === 3 ? ConfigManager.territoryWar.getMinTerritoryBadgeForBigStrider() : ConfigManager.territoryWar.getMinTerritoryBadgeForStriders()
                    let badgeId = npcItemMap[ npc.getId() ]
                    if ( QuestHelper.getQuestItemsCount( player, badgeId ) < price ) {
                        return this.getPath( '36481-07.html' )
                    }

                    let striderId: number = _.nth( striders, type )

                    if ( !_.isNumber( striderId ) ) {
                        return
                    }

                    await QuestHelper.takeSingleItem( player, badgeId, price )
                    await QuestHelper.giveSingleItem( player, striderId, 1 )

                    return this.getPath( '36481-09.html' )

                case 'elite':
                    if ( !QuestHelper.hasQuestItems( player, ELITE_MERCENARY_CERTIFICATE ) ) {
                        return this.getPath( '36481-10.html' )
                    }

                    let eliteListId = 676 + npc.getCastle().getResidenceId()
                    MultisellCache.separateAndSend( eliteListId, player, npc, false )
                    return

                case 'top-elite':
                    if ( !QuestHelper.hasQuestItems( player, TOP_ELITE_MERCENARY_CERTIFICATE ) ) {
                        return this.getPath( '36481-10.html' )
                    }

                    let listId = 685 + npc.getCastle().getResidenceId()
                    MultisellCache.separateAndSend( listId, player, npc, false )
                    return

            }

            return
        }

        if ( event === timerName && !npc.isDecayed() ) {
            if ( TerritoryWarManager.isTWInProgress ) {
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcShout, NpcStringIds.CHARGE_CHARGE_CHARGE )
                return
            }

            if ( _.random( 2 ) === 0 ) {
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcShout, NpcStringIds.COURAGE_AMBITION_PASSION_MERCENARIES_WHO_WANT_TO_REALIZE_THEIR_DREAM_OF_FIGHTING_IN_THE_TERRITORY_WAR_COME_TO_ME_FORTUNE_AND_GLORY_ARE_WAITING_FOR_YOU )
                return
            }

            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcShout, NpcStringIds.DO_YOU_WISH_TO_FIGHT_ARE_YOU_AFRAID_NO_MATTER_HOW_HARD_YOU_TRY_YOU_HAVE_NOWHERE_TO_RUN_BUT_IF_YOU_FACE_IT_HEAD_ON_OUR_MERCENARY_TROOP_WILL_HELP_YOU_OUT )
        }
    }
}