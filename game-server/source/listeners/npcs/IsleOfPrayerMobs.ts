import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { ConfigManager } from '../../config/ConfigManager'
import _ from 'lodash'

const YELLOW_SEED_OF_EVIL_SHARD = 9593
const GREEN_SEED_OF_EVIL_SHARD = 9594
const BLUE_SEED_OF_EVIL_SHARD = 9595
const RED_SEED_OF_EVIL_SHARD = 9596

type DropItem = [ number, number ] // itemId, chance (1-10000)
const dropMap: { [ npcId: number ]: DropItem } = {
    22257: [ YELLOW_SEED_OF_EVIL_SHARD, 2087 ], // Island Guardian
    22258: [ YELLOW_SEED_OF_EVIL_SHARD, 2147 ], // White Sand Mirage
    22259: [ YELLOW_SEED_OF_EVIL_SHARD, 2642 ], // Muddy Coral
    22260: [ YELLOW_SEED_OF_EVIL_SHARD, 2292 ], // Kleopora
    22261: [ GREEN_SEED_OF_EVIL_SHARD, 1171 ], // Seychelles
    22262: [ GREEN_SEED_OF_EVIL_SHARD, 1173 ], // Naiad
    22263: [ GREEN_SEED_OF_EVIL_SHARD, 1403 ], // Sonneratia
    22264: [ GREEN_SEED_OF_EVIL_SHARD, 1207 ], // Castalia
    22265: [ RED_SEED_OF_EVIL_SHARD, 575 ], // Chrysocolla
    22266: [ RED_SEED_OF_EVIL_SHARD, 493 ], // Pythia
    22267: [ RED_SEED_OF_EVIL_SHARD, 770 ], // Dark Water Dragon
    22268: [ BLUE_SEED_OF_EVIL_SHARD, 987 ], // Shade
    22269: [ BLUE_SEED_OF_EVIL_SHARD, 995 ], // Shade
    22270: [ BLUE_SEED_OF_EVIL_SHARD, 1008 ], // Water Dragon Detractor
    22271: [ BLUE_SEED_OF_EVIL_SHARD, 1008 ], // Water Dragon Detractor
}

const npcIds : Array<number> = _.keys( dropMap ).map( value => _.parseInt( value ) )

export class IsleOfPrayerMobs extends ListenerLogic {
    constructor() {
        super( 'IsleOfPrayerMobs', 'listeners/npcs/IsleOfPrayerMobs.ts' )
    }

    getAttackableKillIds(): Array<number> {
        return npcIds
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ itemId, chance ] = dropMap[ data.npcId ]

        if ( _.random( 10000 ) <= ( chance * ConfigManager.rates.getDeathDropChanceMultiplier() ) ) {
            let npc = L2World.getObjectById( data.targetId ) as L2Npc
            let amount = _.random( ConfigManager.rates.getDeathDropAmountMinMultiplier(), ConfigManager.rates.getDeathDropAmountMaxMultiplier(), true )
            npc.dropSingleItem( itemId, Math.max( Math.floor( amount ), 1 ), data.playerId )
        }

        return
    }
}