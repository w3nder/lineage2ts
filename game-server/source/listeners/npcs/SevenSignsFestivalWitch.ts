import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcApproachedForTalkEvent, NpcGeneralEvent, NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { DataManager } from '../../data/manager'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { SevenSignsFestival } from '../../gameService/directives/SevenSignsFestival'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { SevenSignsSide } from '../../gameService/values/SevenSignsValues'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SpawnMakerCache } from '../../gameService/cache/SpawnMakerCache'
import { L2FestivalPhase, L2FestivalRoom, L2FestivalSpawn } from '../../gameService/models/L2DarknessFestival'
import { SevenSignsFestivalLevel } from '../../gameService/values/SevenSignsFestivalValues'
import {
    SSFDawnPhaseOneSpawns,
    SSFDawnPhaseTwoSpawns,
    SSFDuskPhaseOneSpawns,
    SSFDuskPhaseTwoSpawns
} from './data/SSFSpawnData'
import { QuestHelper } from '../helpers/QuestHelper'
import {
    teleportAreaPlayersToCoordinates,
    teleportCharacterToGeometryCoordinates
} from '../../gameService/helpers/TeleportHelper'
import { AreaCache } from '../../gameService/cache/AreaCache'
import { GeometryId } from '../../gameService/enums/GeometryId'
import { MusicPacket } from '../../gameService/packets/send/Music'

const dawnNpcIds: Array<number> = [
    31132,
    31133,
    31134,
    31135,
    31136
]

const duskNpcIds: Array<number> = [
    31142,
    31143,
    31144,
    31145,
    31146
]

const allNpcIds: Array<number> = [
    ...dawnNpcIds,
    ...duskNpcIds
]

/*
    Areas will be dynamically created from available spawn territory.
 */
const npcAreaIds: Record<number, string> = {
    31136: 'ssqe1_npc1721_01',
    31135: 'ssqe1_npc1721_02',
    31134: 'ssqe1_npc1721_03',
    31133: 'ssqe1_npc1721_04',
    31132: 'ssqe1_npc1721_05',
    31146: 'ssqe2_npc1720_01',
    31145: 'ssqe2_npc1720_02',
    31144: 'ssqe2_npc1720_03',
    31143: 'ssqe2_npc1720_04',
    31142: 'ssqe2_npc1720_05'
}

const npcTalkHtml: Record<number, string> = {
    31132: 'dawn_sibyl1001.htm',
    31133: 'dawn_sibyl2001.htm',
    31134: 'dawn_sibyl3001.htm',
    31135: 'dawn_sibyl4001.htm',
    31136: 'dawn_sibyl5001.htm',
    31142: 'dusk_sibyl1001.htm',
    31143: 'dusk_sibyl2001.htm',
    31144: 'dusk_sibyl3001.htm',
    31145: 'dusk_sibyl4001.htm',
    31146: 'dusk_sibyl5001.htm',
}

const enum NpcVariable {
    AskHigherDifficulty = 'ssf.difficulty',
    AskHigherDifficultyTime = 'ssf.difficultyT',
    BombTimeCheck = 'ssf.bombTime'
}

const enum TimerEvent {
    Exit = 'ex',
    RoomExpiration = 're',
    StartInitialSpawn = 'si',
    SpawnBomb = 'sb',
    StartBombSpawn = 'sbs',
    SpawnTreasureChest = 'stc'
}

/*
    Maximum duration of party being in room is 17.83 minutes, expressed in millis
 */

// TODO : add packets for playing music based on progression
const maxRoomDuration = ( ( 60 * 17 ) + 50 ) * 1000

export class SevenSignsFestivalWitch extends ListenerLogic {
    onExitTimeout: NodeJS.Timeout

    constructor() {
        super( 'SevenSignsFestivalWitch', 'listeners/npcs/SevenSignsFestivalWitch.ts' )
    }

    getSpawnIds(): Array<number> {
        return allNpcIds
    }

    getQuestStartIds(): Array<number> {
        return allNpcIds
    }

    getApproachedForTalkIds(): Array<number> {
        return allNpcIds
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId )
        if ( !npc ) {
            return
        }

        BroadcastHelper.dataInLocation( npc, MusicPacket.B06_S01_2000, 2000 )

        this.startQuestTimer( TimerEvent.RoomExpiration, maxRoomDuration, data.characterId )
        this.startQuestTimer( TimerEvent.StartInitialSpawn, 10, data.characterId )
        this.startQuestTimer( TimerEvent.SpawnBomb, 300000, data.characterId )
        this.startQuestTimer( TimerEvent.SpawnTreasureChest, 300000, data.characterId )
    }

    getPathPrefix(): string {
        return 'overrides/html/npc/festivalWitch'
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        return this.getPath( npcTalkHtml[ data.characterNpcId ] )
    }

    async onPartyCheck( data: NpcGeneralEvent ) : Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        if ( !player ) {
            return
        }

        let party = player.getParty()
        if ( !party ) {
            let template = DataManager.getNpcData().getTemplate( data.characterNpcId )
            if ( !template ) {
                return
            }

            let x = template.getParameters()[ 'escape_x' ] as number
            let y = template.getParameters()[ 'escape_y' ] as number
            let z = template.getParameters()[ 'coord_z' ] as number

            if ( !Number.isInteger( x ) || !Number.isInteger( y ) || !Number.isInteger( z ) ) {
                return
            }

            await teleportCharacterToGeometryCoordinates( GeometryId.PartyTeleport, player, x, y, z )
            return
        }

        if ( !party.isLeader( player ) ) {
            return this.getPath( 'ssq_main_event_sibyl_q0505_04.htm' )
        }

        return this.getPath( 'ssq_main_event_sibyl_q0505_01.htm' )
    }

    onAskDifficulty( data: NpcGeneralEvent ) : string {
        let party = PlayerGroupCache.getParty( data.playerId )
        if ( !party || party.getLeaderObjectId() !== data.playerId ) {
            return this.getPath( 'ssq_main_event_sibyl_q0505_04.htm' )
        }

        return this.getPath( 'ssq_main_event_sibyl_q0505_02.htm' )
    }

    onConfirmedExit( data: NpcGeneralEvent ) : string {
        this.startQuestTimer( TimerEvent.Exit, 7000, data.characterId )
        return this.getPath( 'ssq_main_event_sibyl_q0505_05.htm' )
    }

    onConfirmedHigherDifficulty( data: NpcGeneralEvent ) : string {
        if ( this.onExitTimeout ) {
            return
        }

        let difficultyTime : number = NpcVariablesManager.get( data.characterId, NpcVariable.AskHigherDifficultyTime ) as number ?? 0
        if ( ( Date.now() - difficultyTime ) < 10000 ) {
            return this.getPath( 'ssq_main_event_sibyl_q0505_03.htm' )
        }

        let difficultyAskAmount : number = NpcVariablesManager.get( data.characterId, NpcVariable.AskHigherDifficulty ) as number ?? 0
        if ( difficultyAskAmount >= 5 ) {
            return this.getPath( 'ssq_main_event_sibyl_q0505_03.htm' )
        }

        let room = this.getFestivalRoomForNpcId( data.characterNpcId )
        room.spawnPhase( L2FestivalPhase.AdditionalDifficulty )

        NpcVariablesManager.set( data.characterId, NpcVariable.AskHigherDifficultyTime, Date.now() )
        NpcVariablesManager.set( data.characterId, NpcVariable.AskHigherDifficulty, difficultyAskAmount + 1 )
    }

    onRoomExpiration( data: NpcGeneralEvent ) : Promise<void> {
        this.stopTimers( data.characterId )

        let spawn = SpawnMakerCache.getFirstSpawn( data.characterNpcId )
        if ( spawn ) {
            spawn.startDespawn()
        }

        return this.teleportOutOfZone( data.characterId )
    }

    onStartInitialSpawn( data: NpcGeneralEvent ) : void {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        let npcId = npc.getTemplate().getId()

        let expectedSide = DataManager.getNpcData().getTemplate( npcId ).getParameters()[ 'part_type' ] === 'DAWN' ? SevenSignsSide.Dawn : SevenSignsSide.Dusk
        let roomLevel = DataManager.getNpcData().getTemplate( npcId ).getParameters()[ 'RoomIndex' ] as number - 1
        let room = SevenSignsFestival.getRoomFestival( expectedSide, roomLevel )
        if ( !room.isInitialized() ) {
            room.initialize( this.getRoomSpawns( expectedSide, roomLevel ) )
        }

        room.spawnPhase( L2FestivalPhase.Initial )
    }

    onSpawnBomb( data: NpcGeneralEvent ) : void {
        let hasSpawnedBefore = !!NpcVariablesManager.get( data.characterId, NpcVariable.BombTimeCheck )
        let chance = hasSpawnedBefore ? 0.3 : 0.1

        if ( Math.random() < chance ) {
            this.spawnBombNpc( data.characterId )
        }

        if ( !hasSpawnedBefore ) {
            NpcVariablesManager.set( data.characterId, NpcVariable.BombTimeCheck, Date.now() )
        }
    }

    private spawnBombNpc( objectId: number ) : void {
        let npc = L2World.getObjectById( objectId ) as L2Npc
        if ( !npc ) {
            return
        }

        let bombNpcId = npc.getTemplate().getParameters()[ 'battle_bomb_man_name' ] as number
        if ( !bombNpcId ) {
            return
        }

        let x = npc.getX() + 250 + Math.floor( Math.random() + 100 )
        let y = npc.getY() + 250 + Math.floor( Math.random() + 100 )
        QuestHelper.addGenericSpawn( null, bombNpcId, x, y, npc.getZ(), 0 )
    }

    private onSpawnTreasureChest( data: NpcGeneralEvent ) : void {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        if ( !npc ) {
            return
        }

        let treasureNpcId = npc.getTemplate().getParameters()[ 'battle_present_name' ] as number
        if ( !treasureNpcId ) {
            return
        }

        let x = npc.getX() + ( 200 * ( Math.random() > 0.5 ? 1 : -1 ) ) + 50 + Math.floor( Math.random() + 100 ) * ( Math.random() > 0.5 ? 1 : -1 )
        let y = npc.getY() + ( 200 * ( Math.random() > 0.5 ? 1 : -1 ) ) + 50 + Math.floor( Math.random() + 100 ) * ( Math.random() > 0.5 ? 1 : -1 )
        QuestHelper.addGenericSpawn( null, treasureNpcId, x, y, npc.getZ(), 0 )
    }

    onTimerEvent( data: NpcGeneralEvent ) : Promise<void> {
        switch ( data.eventName ) {
            case TimerEvent.Exit:
                return this.teleportOutOfZone( data.characterId )

            case TimerEvent.RoomExpiration:
                return this.onRoomExpiration( data )

            case TimerEvent.StartInitialSpawn:
                this.onStartInitialSpawn( data )
                break

            case TimerEvent.SpawnBomb:
                this.onSpawnBomb( data )
                break

            case TimerEvent.StartBombSpawn:
                this.onSpawnBomb( data )
                this.startQuestTimer( TimerEvent.SpawnBomb, 3000, data.characterId, 0, true )
                break

            case TimerEvent.SpawnTreasureChest:
                this.onSpawnTreasureChest( data )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.isTimer ) {
            await this.onTimerEvent( data )

            return
        }

        switch ( data.eventName ) {
            case '1':
                return this.onPartyCheck( data )

            case '2':
                return this.onAskDifficulty( data )

            case '3':
                return this.onConfirmedExit( data )

            case '4':
                return this.onConfirmedHigherDifficulty( data )
        }
    }

    private async teleportOutOfZone( npcObjectId: number ) : Promise<void> {
        let npc = L2World.getObjectById( npcObjectId ) as L2Npc
        if ( !npc ) {
            return
        }

        let template = npc.getTemplate()
        let x = template.getParameters()[ 'escape_x' ] as number
        let y = template.getParameters()[ 'escape_y' ] as number
        let z = template.getParameters()[ 'coord_z' ] as number

        if ( !Number.isInteger( x ) || !Number.isInteger( y ) || !Number.isInteger( z ) ) {
            return
        }

        let area = AreaCache.getTerritoryArea( npcAreaIds[ template.getId() ] )
        if ( !area ) {
            return
        }

        return teleportAreaPlayersToCoordinates( area, x, y, z )
    }

    private getFestivalRoomForNpcId( npcId: number ) : L2FestivalRoom {
        let expectedSide = DataManager.getNpcData().getTemplate( npcId ).getParameters()[ 'part_type' ] === 'DAWN' ? SevenSignsSide.Dawn : SevenSignsSide.Dusk
        let roomLevel = DataManager.getNpcData().getTemplate( npcId ).getParameters()[ 'RoomIndex' ] as number - 1

        return SevenSignsFestival.getRoomFestival( expectedSide, roomLevel )
    }

    private getRoomSpawns( side: SevenSignsSide, festivalId: SevenSignsFestivalLevel ) : Record<L2FestivalPhase, Array<L2FestivalSpawn>> {
        return {
            [ L2FestivalPhase.Initial ]: side === SevenSignsSide.Dawn ? SSFDawnPhaseOneSpawns[ festivalId ] : SSFDuskPhaseOneSpawns[ festivalId ],
            [ L2FestivalPhase.AdditionalDifficulty ]: side === SevenSignsSide.Dawn ? SSFDawnPhaseTwoSpawns[ festivalId ] : SSFDuskPhaseTwoSpawns[ festivalId ],
            [ L2FestivalPhase.Final ]: side === SevenSignsSide.Dawn ? SSFDawnPhaseTwoSpawns[ festivalId ] : SSFDuskPhaseTwoSpawns[ festivalId ],
        }
    }

    private stopTimers( objectId: number ) : void {
        this.stopQuestTimer( TimerEvent.Exit, objectId )
        this.stopQuestTimer( TimerEvent.RoomExpiration, objectId )
        this.stopQuestTimer( TimerEvent.StartInitialSpawn, objectId )
        this.stopQuestTimer( TimerEvent.SpawnBomb, objectId )
        this.stopQuestTimer( TimerEvent.StartBombSpawn, objectId )
        this.stopQuestTimer( TimerEvent.SpawnTreasureChest, objectId )
    }
}