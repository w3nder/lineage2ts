import {
    PersistedConfigurationLogic,
    PersistedConfigurationLogicType
} from '../../gameService/models/listener/PersistedConfigurationLogic'
import { AttackableKillEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { QuestHelper } from '../helpers/QuestHelper'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2World } from '../../gameService/L2World'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { AbstractVariablesMap } from '../../gameService/variables/AbstractVariablesManager'
import { ListenerHelper } from '../helpers/ListenerHelper'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2Object } from '../../gameService/models/L2Object'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2ItemInstance } from '../../gameService/models/items/instance/L2ItemInstance'
import { MagicSkillLaunched } from '../../gameService/packets/send/MagicSkillLaunched'
import { Skill } from '../../gameService/models/Skill'
import { SkillCache } from '../../gameService/cache/SkillCache'
import _ from 'lodash'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import { AITraitCache } from '../../gameService/cache/AITraitCache'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

interface LuckyPigConfiguration extends PersistedConfigurationLogicType {
    spawnChanceOnKill: number
    level52AdenaLimit: number
    level70AdenaLimit: number
    level80AdenaLimit: number
    isEnabled: boolean
    isSingleInstancePig: boolean
    despawnTimeSeconds: number
    neolithicCrystalChance: number
    respawnTimeSeconds: number
}

const level52NpcIds: Array<number> = [
    // Enchanted Valley
    20589,
    20590,
    20591,
    20592,
    20593,
    20594,
    20595,
    20596,
    20597,
    20598,
    20599,
]

const level70NpcIds: Array<number> = [
    // Forest of the Dead
    18119,
    21555,
    21556,
    21547,
    21553,
    21548,
    21557,
    21559,
    21560,
    21561,
    21562,
    21563,
    21564,
    21565,
    21566,
    21568,
    21567,
    21596,
    21572,
    21573,
    21571,
    21570,
    21574,
    21576,
    21599,
    21580,
    21581,
    21579,
    21582,
    21578,
    21586,
    21587,
    21583,
    21585,
    21590,
    21593,
    21588,

    // Valley of Saints
    21520,
    21521,
    21524,
    21523,
    21526,
    21529,
    21541,
    21531,
    21530,
    21533,
    21532,
    21536,
    21535,
    21537,
    21539,
    21544,
]

const level80NpcIds: Array<number> = [
    // Beast Farm
    18873,
    18880,
    18887,
    18894,
    18906,
    18907,
    18874,
    18875,
    18876,
    18877,
    18878,
    18879,
    18881,
    18882,
    18883,
    18884,
    18885,
    18886,
    18888,
    18889,
    18890,
    18891,
    18892,
    18893,
    18895,
    18896,
    18897,
    18898,
    18899,
    18900,

    // Plains of the Lizardmen
    22768,
    22769,
    22773,
    22772,
    22771,
    22770,
    22774,

    // Sel Mahum Training Grounds
    18908,
    22780,
    22782,
    22784,
    22781,
    22783,
    22785,
    22776,
    22786,
    22787,
    22788,
    22775,
    22777,
    22778,

    // Fields of Silence & Fields of Whispers
    22651,
    22654,
    22650,
    22655,
    22652,
    22658,
    22659,

    // Crypts of Disgrace
    22704,
    22703,
    22705,

    // Den of Evil
    22701,
    22691,
    22698,
    22695,
    22694,
    22696,
    22692,
    22693,
    22699,
    22698,
    22697,
    18807,
    22702,

    // Primeval Island
    22196,
    22197,
    22198,
    22218,
    22223,
    22203,
    22204,
    22205,
    22220,
    22225,
    22743,
    22745,
    22200,
    22201,
    22202,
    22219,
    22224,
    22742,
    22744,
    22199,
    22212,
    22213,
    22222,
    22211,
    22227,
    22208,
    22209,
    22210,
    22221,
    22226,
    22214,

    // Dragon Valley
    22815,
    22822,
    22823,
    22824,
    22862,
    22818,
    22819,
    22860,
    22829,
    22858,
    22830,
    22828,
    22827,
    22826,
    22861,
    22825,
]

enum LuckyPigInstance {
    level52,
    level70,
    level80
}

type ItemDrop = [ number, number, number ] // itemId, amount, chance
const itemDrops: { [ pigLevel: number ]: Array<ItemDrop> } = {
    [ LuckyPigInstance.level52 ]: [
        [ 8745, 1, 0.5 ], // High-Grade Life Stone - Level 52
        [ 8745, 2, 0.25 ], // High-Grade Life Stone - Level 52
        [ 8755, 1, 0.5 ], // Top-Grade Life Stone - Level 52
        [ 8755, 2, 0.25 ], // Top-Grade Life Stone - Level 52
    ],

    [ LuckyPigInstance.level70 ]: [
        [ 5577, 1, 0.33 ], // Red Soul Crystal - Stage 11
        [ 5578, 1, 0.33 ], // Green Soul Crystal - Stage 11
        [ 5579, 1, 0.66 ], // Blue Soul Crystal - Stage 11
    ],

    [ LuckyPigInstance.level80 ]: [
        [ 9552, 1, 0.20 ], // Fire Crystal
        [ 9553, 1, 0.20 ], // Water Crystal
        [ 9554, 1, 0.20 ], // Earth Crystal
        [ 9555, 1, 0.20 ], // Wind Crystal
        [ 9556, 1, 0.20 ], // Dark Crystal
        [ 9557, 1, 0.40 ], // Holy Crystal

        [ 9552, 2, 0.20 ], // Fire Crystal
        [ 9553, 2, 0.20 ], // Water Crystal
        [ 9554, 2, 0.20 ], // Earth Crystal
        [ 9555, 2, 0.20 ], // Wind Crystal
        [ 9556, 2, 0.20 ], // Dark Crystal
        [ 9557, 2, 0.40 ], // Holy Crystal
    ],
}

const goldPigItemDrops: { [ pigLevel: number ]: number } = {
    [ LuckyPigInstance.level52 ]: 14678, // Neolithic Crystal - B
    [ LuckyPigInstance.level70 ]: 14679, // Neolithic Crystal - A
    [ LuckyPigInstance.level80 ]: 14680, // Neolithic Crystal - S
}

interface LuckyPigVariables extends AbstractVariablesMap {
    originalInstance: LuckyPigInstance
    adenaTries: number
    stage: number
    isFromChampion: boolean
}

const mobToPigType: { [ npcId: number ]: LuckyPigInstance } = {}

function attachType( type: LuckyPigInstance, npcId: number ): void {
    mobToPigType[ npcId ] = type
}

_.each( level52NpcIds, attachType.bind( null, LuckyPigInstance.level52 ) )
_.each( level70NpcIds, attachType.bind( null, LuckyPigInstance.level70 ) )
_.each( level80NpcIds, attachType.bind( null, LuckyPigInstance.level80 ) )

const eventNames = {
    despawn: 'ds',
    sayText: 'say',
    feed: 'fd',
}

const untransfromedPigIds: Array<number> = [
    18664,
    18665,
    18666,
]

const pinkWinglessPig = 2502
const goldWinglessPig = 2503
const transformedPigIds: Array<number> = [
    pinkWinglessPig,
    goldWinglessPig,
]

const goldPigSpawnChances : Array<number> = _.times( 20, ( index : number ) : number => {
    if ( index > 2 ) {
        return index * 0.03
    }

    return 0
} )

const spawnedNpcIds: Array<number> = [
    18664,
    18665,
    18666
]

/**
 * TODO : current logic vs original
 * Consider that there are Neolithic Crystal S80 (14681) and S84 (21579), Dimensional Merchant has items for these
 * Original:
 * - spawn locations are static, total 25, multiple pigs can appear at same time
 * - eat all objects nearby, not just adena, but adena adds satience levels
 * - ability to drop adena
 * Current logic:
 * - more rewarding, though based on kill/feed chance
 */
export class LuckyPig extends PersistedConfigurationLogic<LuckyPigConfiguration> {

    pigInstances: Set<number> = new Set<number>()
    pigInstanceTimeout: { [ type: number ]: number } = {}

    constructor() {
        super( 'LuckyPig', 'listeners/npcs/LuckyPig.ts' )
    }

    getDefaultConfiguration(): LuckyPigConfiguration {
        return {
            isEnabled: true,
            level52AdenaLimit: 10000,
            level70AdenaLimit: 50000,
            level80AdenaLimit: 70000,
            spawnChanceOnKill: 0.05,
            isSingleInstancePig: false, // only one pig of specific level/type is permitted to exist in the world at a time
            despawnTimeSeconds: 60,
            neolithicCrystalChance: 0.5,
            respawnTimeSeconds: 6000, // time between respawns
        }
    }

    async initialize(): Promise<void> {
        AITraitCache.setManyNpcTraits( spawnedNpcIds, AITraitCache.createEmptyTraits() )
    }

    getAttackableKillIds(): Array<number> {
        return [
            ...level52NpcIds,
            ...level70NpcIds,
            ...level80NpcIds,
            ...untransfromedPigIds,
            ...transformedPigIds,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case eventNames.despawn:
                this.stopQuestTimersForIds( data.characterId, 0, [ eventNames.sayText, eventNames.feed ] )

                if ( npc ) {
                    await npc.deleteMe()
                }

                return

            case eventNames.sayText:
                if ( !npc ) {
                    return
                }

                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.All, this.getTextToSay( data.characterId ) )
                this.startQuestTimer( eventNames.sayText, _.random( 10000, 20000 ), data.characterId, 0, false )
                return

            case eventNames.feed:
                /*
                    TODO : Convert whole behavior to use AIController:
                    - scan area for closest adena (pick range as default local region of 2048)
                    - go to adena
                    - upon interaction invoke consumption & transformation

                    Current implementation is grossly inefficient due to polling mechanic
                 */
                let adenaItem: L2Object = L2World.getFirstObjectByPredicate( npc, 300, ( object: L2Object ) => {
                    return object.isItem() && object.getId() === ItemTypes.Adena
                }, false )

                if ( !adenaItem ) {
                    return
                }

                if ( GeneralHelper.checkIfInShortRadius( 30, npc, adenaItem, false ) ) {
                    await this.consumeAdena( npc, adenaItem as L2ItemInstance )
                    return
                }

                AIEffectHelper.notifyInteractWithTarget( npc, adenaItem.getObjectId() )
                return
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( !this.configuration.isEnabled ) {
            return
        }

        if ( transformedPigIds.includes( data.npcId ) ) {
            this.processTransformedPigDrop( data )
            return
        }

        if ( untransfromedPigIds.includes( data.npcId ) ) {
            await this.processNormalPigDeath( data )
            return
        }

        let type: LuckyPigInstance = mobToPigType[ data.npcId ]
        if ( this.pigInstances.has( type ) || ( this.configuration.isSingleInstancePig && this.pigInstances.size > 0 ) ) {
            return
        }

        let respawnTime = _.get( this.pigInstanceTimeout, type, 0 )
        if ( Date.now() < respawnTime ) {
            return
        }

        if ( Math.random() > ListenerHelper.getOnDeathAdjustedChance( this.configuration.spawnChanceOnKill, data.isChampion ) ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId )
        let luckyPig: L2Npc = QuestHelper.addSpawnAtLocation( this.getLuckyPigNpcId( type ), npc, true, 0, true )

        let variables: LuckyPigVariables = {
            adenaTries: 0,
            stage: 0,
            originalInstance: type,
            isFromChampion: data.isChampion,
        }

        NpcVariablesManager.set( luckyPig.getObjectId(), this.getName(), variables )
        this.pigInstanceTimeout[ type ] = Date.now() + this.configuration.respawnTimeSeconds * 1000
        this.pigInstances.add( type )

        this.startQuestTimer( eventNames.despawn, this.getDespawnInterval(), luckyPig.getObjectId(), 0, false )
        this.startQuestTimer( eventNames.sayText, 1000, luckyPig.getObjectId(), 0, false )
        this.startQuestTimer( eventNames.feed, 6000, luckyPig.getObjectId(), 0, true )
    }

    getAdenaLimit( type: LuckyPigInstance ): number {
        switch ( type ) {
            case LuckyPigInstance.level52:
                return this.configuration.level52AdenaLimit

            case LuckyPigInstance.level70:
                return this.configuration.level70AdenaLimit

            case LuckyPigInstance.level80:
                return this.configuration.level80AdenaLimit
        }

        return 0
    }

    getLuckyPigNpcId( type: LuckyPigInstance ): number {
        switch ( type ) {
            case LuckyPigInstance.level52:
                return 18664

            case LuckyPigInstance.level70:
                return 18665

            case LuckyPigInstance.level80:
                return 18666
        }
    }

    getTextToSay( characterId: number ): number {
        let variables: LuckyPigVariables = NpcVariablesManager.get( characterId, this.getName() ) as LuckyPigVariables
        if ( !variables ) {
            return
        }

        switch ( variables.stage ) {
            case 0:
                return _.sample( [
                    NpcStringIds.IF_YOU_HAVE_ITEMS_PLEASE_GIVE_THEM_TO_ME,
                    NpcStringIds.MY_STOMACH_IS_EMPTY,
                ] )

            case 1:
                return _.sample( [
                    NpcStringIds.IM_HUNGRY_IM_HUNGRY,
                    NpcStringIds.IM_STILL_NOT_FULL,
                ] )

            case 2:
                return _.sample( [
                    NpcStringIds.IM_STILL_HUNGRY,
                    NpcStringIds.I_FEEL_A_LITTLE_WOOZY,
                ] )

            case 3:
                return _.sample( [
                    NpcStringIds.GIVE_ME_SOMETHING_TO_EAT,
                    NpcStringIds.NOW_ITS_TIME_TO_EAT,
                ] )

            case 4:
                return _.sample( [
                    NpcStringIds.I_ALSO_NEED_A_DESSERT,
                    1800288, // TODO : add new named id here
                ] )

            case 5:
                return NpcStringIds.IM_FULL_NOW_I_DONT_WANT_TO_EAT_ANYMORE
        }
    }

    processTransformedPigDrop( data: AttackableKillEvent ): void {
        let variables: LuckyPigVariables = NpcVariablesManager.get( data.targetId, this.getName() ) as LuckyPigVariables
        if ( !variables ) {
            return
        }

        this.pigInstances.delete( variables.originalInstance )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        switch ( data.npcId ) {
            case goldWinglessPig:
                let goldItemId: number = goldPigItemDrops[ variables.originalInstance ]
                if ( Math.random() > QuestHelper.getAdjustedChance( goldItemId, this.configuration.neolithicCrystalChance, variables.isFromChampion ) ) {
                    return
                }

                npc.dropSingleItem( goldItemId, QuestHelper.getAdjustedAmount( goldItemId, 1, variables.isFromChampion ) )
                return

            case pinkWinglessPig:
                let droppedItemIds: Set<number> = new Set<number>()
                _.each( itemDrops[ variables.originalInstance ], ( drop: ItemDrop ) => {
                    let [ itemId, amount, chance ] = drop
                    if ( droppedItemIds.has( itemId ) || Math.random() > QuestHelper.getAdjustedChance( itemId, chance, variables.isFromChampion ) ) {
                        return
                    }

                    npc.dropSingleItem( itemId, QuestHelper.getAdjustedAmount( itemId, amount, variables.isFromChampion ) )
                    droppedItemIds.add( itemId )
                } )

                return
        }
    }


    async processNormalPigDeath( data: AttackableKillEvent ): Promise<void> {
        this.stopQuestTimersForIds( data.targetId, 0, [ eventNames.sayText, eventNames.feed, eventNames.despawn ] )

        let variables: LuckyPigVariables = NpcVariablesManager.get( data.targetId, this.getName() ) as LuckyPigVariables
        if ( !variables ) {
            return
        }

        this.pigInstances.delete( variables.originalInstance )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        switch ( variables.stage ) {
            case 0:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.All, NpcStringIds.I_HAVENT_EATEN_ANYTHING_IM_SO_WEAK )
                return

            case 1:
                let stageOneAdena: number = _.random( this.getAdenaLimit( variables.originalInstance ) / 2, this.getAdenaLimit( variables.originalInstance ) )
                npc.dropSingleItem( ItemTypes.Adena, QuestHelper.getAdjustedAmount( ItemTypes.Adena, stageOneAdena, variables.isFromChampion ) )

                return

            case 2:
                let stageTwoAdena: number = _.random( this.getAdenaLimit( variables.originalInstance ) / 2, this.getAdenaLimit( variables.originalInstance ) * 2 )
                npc.dropSingleItem( ItemTypes.Adena, QuestHelper.getAdjustedAmount( ItemTypes.Adena, stageTwoAdena, variables.isFromChampion ) )

                return

            case 3:
                let stageThreeAdena: number = _.random( this.getAdenaLimit( variables.originalInstance ), this.getAdenaLimit( variables.originalInstance ) * 5 )
                npc.dropSingleItem( ItemTypes.Adena, QuestHelper.getAdjustedAmount( ItemTypes.Adena, stageThreeAdena, variables.isFromChampion ) )

                return

            case 4:
            case 5:
                let dropIterations: number = variables.stage === 4 ? 1 : 2
                _.times( dropIterations, () => {
                    let [ itemId, amount, chance ]: ItemDrop = _.sample( itemDrops[ variables.originalInstance ] )
                    if ( Math.random() > QuestHelper.getAdjustedChance( itemId, chance, variables.isFromChampion ) ) {
                        return
                    }

                    npc.dropSingleItem( itemId, QuestHelper.getAdjustedAmount( itemId, amount, variables.isFromChampion ) )
                } )

                return
        }
    }

    async consumeAdena( npc: L2Npc, adenaItem: L2ItemInstance ): Promise<void> {
        let variables: LuckyPigVariables = NpcVariablesManager.get( npc.getObjectId(), this.getName() ) as LuckyPigVariables
        if ( !variables ) {
            return
        }

        let enoughAdena : boolean = this.getAdenaLimit( variables.originalInstance ) <= QuestHelper.getAdjustedAmount( ItemTypes.Adena, adenaItem.getCount(), variables.isFromChampion )
        if ( variables.stage === 5 && enoughAdena ) {
            await adenaItem.pickupMeBy( npc )
            return this.transformPig( npc, variables )
        }

        if ( variables.stage < 5 && !enoughAdena ) {
            variables.stage = Math.max( 1, variables.stage - 1 )
        }

        this.stopQuestTimer( eventNames.despawn, npc.getObjectId(), 0 )
        this.startQuestTimer( eventNames.despawn, this.getDespawnInterval(), npc.getObjectId(), 0, false )

        switch ( variables.stage ) {
            case 0:
                variables.stage = 1
                break

            case 1:

                if ( Math.random() < QuestHelper.getAdjustedChance( ItemTypes.Adena, 0.1, variables.isFromChampion ) ) {
                    variables.stage = 2
                    break
                }

                if ( Math.random() < QuestHelper.getAdjustedChance( ItemTypes.Adena, 0.15, variables.isFromChampion ) ) {
                    variables.stage = 3
                    break
                }

                break

            case 2:
                if ( Math.random() < QuestHelper.getAdjustedChance( ItemTypes.Adena, 0.1, variables.isFromChampion ) ) {
                    variables.stage = 3
                    break
                }

                if ( Math.random() < QuestHelper.getAdjustedChance( ItemTypes.Adena, 0.15, variables.isFromChampion ) ) {
                    variables.stage = 4
                    break
                }

                break

            case 3:
                if ( Math.random() < QuestHelper.getAdjustedChance( ItemTypes.Adena, 0.1, variables.isFromChampion ) ) {
                    variables.stage = 4
                }

                break

            case 4:
                if ( Math.random() < QuestHelper.getAdjustedChance( ItemTypes.Adena, 0.1, variables.isFromChampion ) ) {
                    variables.stage = 5
                }

                break
        }

        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.All, NpcStringIds.YUM_YUM_YUM_YUM )
        await adenaItem.pickupMeBy( npc )

        let skill: Skill = SkillCache.getSkill( 6045, 1 )
        BroadcastHelper.dataInLocation( npc, MagicSkillLaunched( npc.getObjectId(), skill.getDisplayId(), skill.getDisplayLevel(), [ npc ] ), npc.getBroadcastRadius() )

        variables.adenaTries = variables.adenaTries + 1
        NpcVariablesManager.set( npc.getObjectId(), this.getName(), variables )
    }

    getDespawnInterval() : number {
        return this.configuration.despawnTimeSeconds * 1000
    }

    async transformPig( npc: L2Npc, variables: LuckyPigVariables ) {
        this.stopQuestTimersForIds( npc.getObjectId(), 0, [ eventNames.sayText, eventNames.feed, eventNames.despawn ] )
        await npc.deleteMe()

        NpcVariablesManager.remove( npc.getObjectId(), this.getName() )

        let pig : L2Npc = QuestHelper.addSummonedSpawnAtLocation( null, this.getNextPigId( variables ), npc, false, this.getDespawnInterval(), true )
        NpcVariablesManager.set( pig.getObjectId(), this.getName(), variables )

        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.All, _.sample( [
            NpcStringIds.OH_MY_WINGS_DISAPPEARED_ARE_YOU_GONNA_HIT_ME_IF_YOU_HIT_ME_ILL_THROW_UP_EVERYTHING_THAT_I_ATE,
            NpcStringIds.OH_MY_WINGS_ACK_ARE_YOU_GONNA_HIT_ME_SCARY_SCARY_IF_YOU_HIT_ME_SOMETHING_BAD_WILL_HAPPEN
        ] ) )
    }

    getNextPigId( variables: LuckyPigVariables ) : number {
        let chance : number = _.defaultTo( _.nth( goldPigSpawnChances, variables.adenaTries ), 0.5 )
        return Math.random() > ListenerHelper.getOnDeathAdjustedChance( chance, variables.isFromChampion ) ? goldWinglessPig : pinkWinglessPig
    }
}