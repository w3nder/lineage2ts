import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { NpcGeneralEvent, NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const npcId : number = 30556
const teleportLocations : Array<Location> = [
    new Location( 151680, -174891, -1782 ),
    new Location( 154153, -220105, -3402 ),
    new Location( 178834, -184336, -355, 41400 )
]

const scheduledDelay = GeneralHelper.minutesToMillis( 30 )
const opportunisticDelay = GeneralHelper.minutesToMillis( 1 )
const eventNames = {
    scheduledTeleport: 'st',
    delayedTeleport: 'dt'
}

export class Toma extends ListenerLogic {
    objectId: number
    lastLocation: Location

    constructor() {
        super( 'Toma', 'listeners/npcs/Toma.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getSpawnIds(): Array<number> {
        return [ npcId ]
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        this.objectId = data.characterId
        this.startQuestTimer( eventNames.scheduledTeleport, scheduledDelay, 0, 0, true )

        return this.teleportToRandomLocation( this.getNpc() )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let npc : L2Npc = L2World.getObjectById( this.objectId ) as L2Npc

        switch ( data.eventName ) {
            case eventNames.scheduledTeleport:
                /*
                    AIController is present only in player visible npcs, having player nearby.
                    Toma will not immediately teleport if player is nearby, allowing easier way to complete quest.
                 */
                if ( npc.hasAIController() ) {
                    this.startQuestTimer( eventNames.delayedTeleport, opportunisticDelay, 0, 0, false )
                    return
                }

            case eventNames.delayedTeleport:
                await this.teleportToRandomLocation( npc )
                return
        }
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( '30556.htm' )
    }

    getPathPrefix(): string {
        return 'overrides/html/npc/toma'
    }

    teleportToRandomLocation( npc: L2Npc ) : Promise<void> {
        this.lastLocation = _.sample( _.without( teleportLocations, this.lastLocation ) )
        return npc.teleportToLocation( this.lastLocation, false )
    }

    getNpc() : L2Npc {
        return L2World.getObjectById( this.objectId ) as L2Npc
    }
}