import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { EventType, NpcApproachedForTalkEvent, NpcFinishedSkillEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'

const npcId: number = 31556
const GOLDEN_RAM_BADGE_RECRUIT = 7246
const GOLDEN_RAM_BADGE_SOLDIER = 7247
const GOLDEN_RAM_COIN = 7251

const allBuffs: { [ skillId: number ]: [ number, number ] } = {
    4359: [ 2, 2 ], // Focus
    4360: [ 2, 2 ], // Death Whisper
    4345: [ 3, 3 ], // Might
    4355: [ 2, 3 ], // Acumen
    4352: [ 1, 3 ], // Berserker Spirit
    4354: [ 2, 3 ], // Vampiric Rage
    4356: [ 1, 6 ], // Empower
    4357: [ 2, 6 ], // Haste
}

export class Selina extends ListenerLogic {
    constructor() {
        super( 'Selina', 'listeners/npcs/Selina.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSkillFinished,
                method: this.onNpcSkillFinishedEvent.bind( this ),
                ids: [ npcId ],
                triggers: {
                    targetIds: new Set( Object.keys( allBuffs ).map( id => _.parseInt( id ) ) )
                }
            }
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Selina'
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        if ( QuestHelper.hasQuestItems( player, GOLDEN_RAM_BADGE_SOLDIER ) ) {
            return this.getPath( '31556-08.html' )
        }

        if ( QuestHelper.hasQuestItems( player, GOLDEN_RAM_BADGE_RECRUIT ) ) {
            return this.getPath( '31556-01.html' )
        }

        return this.getPath( '31556-09.html' )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        let [ level, cost ] = allBuffs[ event ]

        if ( cost > 0 && QuestHelper.getQuestItemsCount( player, GOLDEN_RAM_COIN ) >= cost ) {
            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            await QuestHelper.castSkill( npc, player, SkillCache.getSkill( _.parseInt( event ), level ) )
            return
        }

        return this.getPath( '31556-02.html' )
    }

    async onNpcSkillFinishedEvent( data : NpcFinishedSkillEvent ): Promise<void> {
        let [ level, cost ] = allBuffs[ data.skillId ]
        if ( cost > 0 ) {
            await QuestHelper.takeSingleItem( L2World.getPlayer( data.attackerId ), GOLDEN_RAM_COIN, cost )
        }

        return
    }
}