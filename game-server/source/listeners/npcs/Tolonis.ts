import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { EventType, NpcApproachedForTalkEvent, NpcTeachesSkillsEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { ShowHtml } from '../helpers/ShowHtml'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2SkillLearn } from '../../gameService/models/L2SkillLearn'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { AcquireSkillList } from '../../gameService/packets/send/builder/AcquireSkillList'
import { AcquireSkillType } from '../../gameService/enums/AcquireSkillType'
import { Skill } from '../../gameService/models/Skill'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'

const npcIds: Array<number> = [
    32611
]

const minimumLevel = 75

export class Tolonis extends ListenerLogic {
    constructor() {
        super( 'Tolonis', 'listeners/npcs/Tolonis.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcTeachesSkills,
                method: this.onNpcTeachesSkills.bind( this ),
                ids: npcIds
            }
        ]
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        if ( player.getKarma() > 0 ) {
            return this.getPath( 'denied.html' )
        }

        return this.getPath( 'welcome.html' )
    }

    getPathPrefix(): string {
        return 'overrides/html/npc/tolonis'
    }

    onNpcTeachesSkills( data: NpcTeachesSkillsEvent ): void {
        let player = L2World.getPlayer( data.playerId )

        if ( data.playerLevel >= minimumLevel ) {
            return this.showEtcSkills( player )
        }

        ShowHtml.showPath( player, this.getPath( 'rejected.html' ) )
    }

    showEtcSkills( player: L2PcInstance ): void {
        let skills: Array<L2SkillLearn> = SkillCache.getAvailableCollectSkills( player )
        let skillBuilder: AcquireSkillList = new AcquireSkillList( AcquireSkillType.Collect )

        skills.forEach( ( skillLearn: L2SkillLearn ) => {
            let skill: Skill = SkillCache.getSkill( skillLearn.getSkillId(), skillLearn.getSkillLevel() )

            if ( skill ) {
                skillBuilder.addSkill( skillLearn.getSkillId(), skillLearn.getSkillLevel(), 0, 1 )
            }
        } )

        if ( skillBuilder.getCount() === 0 ) {
            let minLevel = SkillCache.getMinimumLevelForNewSkill( player, SkillCache.getCollectSkillTree() )
            if ( minLevel > 0 ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.DO_NOT_HAVE_FURTHER_SKILLS_TO_LEARN_S1 )
                    .addNumber( minLevel )
                    .getBuffer()
                return player.sendOwnedData( packet )
            }

            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_MORE_SKILLS_TO_LEARN ) )
        }

        return player.sendOwnedData( skillBuilder.getBuffer() )
    }
}