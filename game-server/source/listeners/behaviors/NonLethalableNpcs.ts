import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'

export class NonLethalableNpcs extends ListenerLogic {
    constructor() {
        super( 'NonLethalableNpcs', 'listeners/behaviors/NonLethalableNpcs.ts' )
    }

    getSpawnIds(): Array<number> {
        return [
            22854, // Bloody Karik
            22855, // Bloody Berserker
            22856, // Bloody Karinness
            22857, // Knoriks
            35062, // Headquarters
        ]
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        npc.setLethalable( false )
    }
}