import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Attackable } from '../../gameService/models/actor/L2Attackable'

export class SeeThroughSilentMove extends ListenerLogic {
    constructor() {
        super( 'SeeThroughSilentMove', 'listeners/behaviors/SeeThroughSilentMove.ts' )
    }

    getSpawnIds(): Array<number> {
        return [
            18001, 18002, 22199, 22215, 22216, 22217, 22327, 22746, 22747, 22748,
            22749, 22750, 22751, 22752, 22753, 22754, 22755, 22756, 22757, 22758,
            22759, 22760, 22761, 22762, 22763, 22764, 22765, 22794, 22795, 22796,
            22797, 22798, 22799, 22800, 22843, 22857, 25725, 25726, 25727, 29009,
            29010, 29011, 29012, 29013,
        ]
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Attackable
        npc.setSeeThroughSilentMove( true )
    }
}