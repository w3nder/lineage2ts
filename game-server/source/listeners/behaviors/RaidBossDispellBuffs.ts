import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableAttackedEvent, EventType, NpcSeeSkillEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _, { DebouncedFunc } from 'lodash'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'

const npcIds: Array<number> = [
    25019, // Pan Dryad
    25050, // Verfa
    25063, // Chertuba of Great Soul
    25088, // Crazy Mechanic Golem
    25098, // Sejarr's Servitor
    25102, // Shacram
    25118, // Guilotine, Warden of the Execution Grounds
    25125, // Fierce Tiger King Angel
    25126, // Longhorn Golkonda
    25127, // Langk Matriarch Rashkos
    25158, // King Tarlk
    25162, // Giant Marpanak
    25163, // Roaring Skylancer
    25169, // Ragraman
    25188, // Apepi
    25198, // Fafurion's Herald Lokness
    25229, // Storm Winged Naga
    25233, // Spirit of Andras, the Betrayer
    25234, // Ancient Drake
    25244, // Last Lesser Giant Olkuth
    25248, // Doom Blade Tanatos
    25255, // Gargoyle Lord Tiphon
    25259, // Zaken's Butcher Krantz
    25272, // Partisan Leader Talakin
    25276, // Death Lord Ipos
    25280, // Pagan Watcher Cerberon
    25281, // Anakim's Nemesis Zakaron
    25282, // Death Lord Shax
    25305, // Ketra's Chief Brakki
    25315, // Varka's Chief Horus
    25333, // Anakazel
    25334, // Anakazel
    25335, // Anakazel
    25336, // Anakazel
    25337, // Anakazel
    25338, // Anakazel
    25365, // Patriarch Kuroboros
    25372, // Discarded Guardian
    25391, // Nurka's Messenger
    25394, // Premo Prime
    25437, // Timak Orc Gosmos
    25512, // Gigantic Chaos Golem
    25523, // Plague Golem
    25527, // Uruka
    25552, // Soul Hunter Chakundel
    25553, // Durango the Crusher
    25578, // Jakard
    25588, // Immortal Muus
    25592, // Commander Koenig
    25616, // Lost Warden
    25617, // Lost Warden
    25618, // Lost Warden
    25619, // Lost Warden
    25620, // Lost Warden
    25621, // Lost Warden
    25622, // Lost Warden
    25680, // Giant Marpanak
    25709, // Lost Warden
    25753, // Guillotine Warden
    25766, // Ancient Drake
    29036, // Fenril Hound Uruz
    29040, // Wings of Flame, Ixion
    29060, // Captain of the Ice Queen's Royal Guard
    29065, // Sailren
    29095, // Gordon
]

export class RaidBossDispellBuffs extends ListenerLogic {
    debounceSkillCast: DebouncedFunc<( player: L2PcInstance, npc: L2Npc ) => void>

    constructor() {
        super( 'RaidBossDispellBuffs', 'listeners/behaviors/RaidBossDispellBuffs.ts' )
        this.debounceSkillCast = _.debounce( this.attemptSkillCast.bind( this ), 5000, {
            leading: true
        } )
    }

    getAttackableAttackIds(): Array<number> {
        return npcIds
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSeeSkill,
                method: this.onNpcSeeSkill.bind( this ),
                ids: npcIds
            }
        ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        if ( _.random( 750 ) !== 0 ) {
            return
        }

        let player = L2World.getPlayer( data.attackerPlayerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        this.debounceSkillCast( player, npc )
    }

    async onNpcSeeSkill( data: NpcSeeSkillEvent ): Promise<void> {
        if ( _.random( 750 ) !== 0 ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.receiverId ) as L2Npc

        this.debounceSkillCast( player, npc )
    }

    attemptSkillCast( player: L2PcInstance, npc: L2Npc ): void {
        if ( !GeneralHelper.checkIfInRange( 150, npc, player, true ) ) {
            return
        }

        let skillData = npc.getTemplate().getSkillParameters()[ 'SelfRangeCancel_a' ]
        if ( !skillData || !skillData.id || !skillData.level ) {
            return
        }

        return AIEffectHelper.notifyMustCastSpell( npc, player, skillData.id, skillData.level, false, false )
    }
}