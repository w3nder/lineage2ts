import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { EventType, NpcSeeSkillEvent, NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2Character } from '../../gameService/models/actor/L2Character'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'

const npcIds: Array<number> = [
    18463,
    18464,
    18465,
]

const SKILL_HOLY_WATER : number = 2358

export class RemnantsDie extends ListenerLogic {
    constructor() {
        super( 'RemnantsDie', 'listeners/behaviors/RemnantsDie.ts' )
    }

    getSpawnIds(): Array<number> {
        return npcIds
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSeeSkill,
                method: this.onNpcSeeSkill.bind( this ),
                ids: npcIds,
                triggers: {
                    targetIds: new Set( [ SKILL_HOLY_WATER ] )
                }
            }
        ]
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        npc.setIsMortal( false )
    }

    async onNpcSeeSkill( data: NpcSeeSkillEvent ): Promise<void> {
        if ( data.skillId !== SKILL_HOLY_WATER ) {
            return
        }

        if ( !data.targetIds.includes( data.receiverId ) ) {
            return
        }

        let npc = L2World.getObjectById( data.receiverId ) as L2Npc
        if ( npc.isDead() || npc.getCurrentHp() > ( npc.getMaxHp() * 0.02 ) ) {
            return
        }

        await npc.doDie( L2World.getObjectById( data.originatorId ) as L2Character )
    }
}