import { PersistedEventConfiguration, PersistedEventLogic } from '../../gameService/models/listener/PersistedEventLogic'
import { DataManager } from '../../data/manager'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'
import aigle from 'aigle'

const npcIds: Array<number> = [
    31854,
    31855,
    31856,
    31857,
    31858,
]

const letterMap = {
    A: 3875,
    C: 3876,
    E: 3877,
    F: 3878,
    G: 3879,
    H: 3880,
    I: 3881,
    L: 3882,
    N: 3883,
    O: 3884,
    R: 3885,
    S: 3886,
    T: 3887,
    Y: 13417,
    5: 13418,
    2: 3888,
}

type L2DayWordRewards = Array<[ number, number, number ]>

const firstRewards: L2DayWordRewards = [ // itemId, chance, amount
    [ 10260, 90, 3 ], // Alacrity Juice
    [ 10261, 85, 3 ], // Accuracy Juice
    [ 10262, 80, 3 ], // Critical Hit Juice
    [ 10263, 75, 3 ], // Critical Rate Juice
    [ 10264, 70, 3 ], // Casting Spd. Juice
    [ 10265, 65, 3 ], // Evasion Juice
    [ 10266, 60, 3 ], // M. Atk. Juice
    [ 10267, 55, 3 ], // Power Juice
    [ 10268, 50, 3 ], // Speed Juice
    [ 10269, 45, 3 ], // Defense Juice
    [ 10270, 40, 3 ], // MP Consumption Juice
    [ 9546, 37, 2 ], // Fire Stone
    [ 9547, 34, 2 ], // Water Stone
    [ 9548, 31, 2 ], // Earth Stone
    [ 9549, 28, 2 ], // Wind Stone
    [ 9550, 25, 2 ], // Dark Stone
    [ 9551, 22, 2 ], // Holy Stone
    [ 8947, 19, 1 ], // L2day - Rabbit Ears
    [ 8948, 16, 1 ], // L2day - Little Angel Wings
    [ 8949, 13, 1 ], // L2day - Fairy Antennae
    [ 3959, 10, 2 ], // Blessed Scroll of Resurrection (Event)
    [ 3958, 7, 2 ], // Blessed Scroll of Escape (Event)
    [ 8752, 4, 2 ], // High-Grade Life Stone - Level 76
    [ 8762, 1, 1 ], // Top-Grade Life Stone - Level 76
    [ 6660, 0, 1 ], // Ring of Queen Ant
]

const secondRewards: L2DayWordRewards = [ // itemId, chance, amount
    [ 10260, 90, 2 ], // Alacrity Juice
    [ 10261, 85, 2 ], // Accuracy Juice
    [ 10262, 80, 2 ], // Critical Hit Juice
    [ 10263, 75, 2 ], // Critical Rate Juice
    [ 10264, 70, 2 ], // Casting Spd. Juice
    [ 10265, 65, 2 ], // Evasion Juice
    [ 10266, 60, 2 ], // M. Atk. Juice
    [ 10267, 55, 2 ], // Power Juice
    [ 10268, 50, 2 ], // Speed Juice
    [ 10269, 45, 2 ], // Defense Juice
    [ 10270, 40, 2 ], // MP Consumption Juice
    [ 9546, 37, 1 ], // Fire Stone
    [ 9547, 34, 1 ], // Water Stone
    [ 9548, 31, 1 ], // Earth Stone
    [ 9549, 28, 1 ], // Wind Stone
    [ 9550, 25, 1 ], // Dark Stone
    [ 9551, 22, 1 ], // Holy Stone
    [ 8948, 19, 1 ], // L2day - Little Angel Wings
    [ 8949, 16, 1 ], // L2day - Fairy Antennae
    [ 8950, 13, 1 ], // L2day - Feathered Hat
    [ 3959, 10, 1 ], // Blessed Scroll of Resurrection (Event)
    [ 3958, 7, 1 ], // Blessed Scroll of Escape (Event)
    [ 8742, 4, 2 ], // Mid-Grade Life Stone - Level 76
    [ 8752, 1, 1 ], // High-Grade Life Stone - Level 76
    [ 6661, 0, 1 ], // Earring of Orfen
]

const thirdRewards: L2DayWordRewards = [ // itemId, chance, amount
    [ 10260, 90, 1 ], // Alacrity Juice
    [ 10261, 85, 1 ], // Accuracy Juice
    [ 10262, 80, 1 ], // Critical Hit Juice
    [ 10263, 75, 1 ], // Critical Rate Juice
    [ 10264, 70, 1 ], // Casting Spd. Juice
    [ 10265, 65, 1 ], // Evasion Juice
    [ 10266, 60, 1 ], // M. Atk. Juice
    [ 10267, 55, 1 ], // Power Juice
    [ 10268, 50, 1 ], // Speed Juice
    [ 10269, 45, 1 ], // Defense Juice
    [ 10270, 40, 1 ], // MP Consumption Juice
    [ 9546, 37, 1 ], // Fire Stone
    [ 9547, 34, 1 ], // Water Stone
    [ 9548, 31, 1 ], // Earth Stone
    [ 9549, 28, 1 ], // Wind Stone
    [ 9550, 25, 1 ], // Dark Stone
    [ 9551, 22, 1 ], // Holy Stone
    [ 8949, 19, 1 ], // L2day - Fairy Antennae
    [ 8950, 16, 1 ], // L2day - Feathered Hat
    [ 8951, 13, 1 ], // L2day - Artisan's Goggles
    [ 3959, 10, 1 ], // Blessed Scroll of Resurrection (Event)
    [ 3958, 7, 1 ], // Blessed Scroll of Escape (Event)
    [ 8742, 4, 1 ], // Mid-Grade Life Stone - Level 76
    [ 8752, 1, 1 ], // High-Grade Life Stone - Level 76
    [ 6662, 0, 1 ], // Ring of Core
]

const optionTemplate = '<a action="bypass -h Quest L2Day collect_%index%">I\'ve collected "%word%"!</a>'

type L2DayNpcRewards = { [ npcId: number ]: number }

interface L2DayConfiguration extends PersistedEventConfiguration {
    npcRewards: L2DayNpcRewards
    words: Array<string>
}

interface L2DayRequiredItem {
    itemId: number
    amount: number
}

export class L2Day extends PersistedEventLogic<L2DayConfiguration> {
    optionsHtml: string
    explanationHtml: string
    requiredItems: Array<Array<L2DayRequiredItem>>

    constructor() {
        super( 'L2Day', 'listeners/events/L2Day.ts' )
    }

    getDefaultConfiguration(): L2DayConfiguration {
        return {
            isEnabled: false,
            eventName: 'L2 Day',
            startDate: 0,
            endDate: 0,
            npcRewards: {
                31854: 7117,
                31855: 7118,
                31856: 7119,
                31857: 7121,
                31858: 7120,
            },
            words: [
                'CHRONICLE',
                'LINEAGE 2',
                'FREYA HI5',
            ],
            drops: [
                {
                    itemId: 3875,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3876,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3877,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3878,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3879,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3880,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3881,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3882,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3883,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3884,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3885,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3886,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3887,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 3888,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 13417,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
                {
                    itemId: 13418,
                    minCount: 1,
                    maxCount: 1,
                    chance: 1,
                },
            ],
            npcs: [
                {
                    npc: 31854,
                    x: -84128,
                    y: 243258,
                    z: -3735,
                    heading: 9707,
                },
                {
                    npc: 31855,
                    x: 45510,
                    y: 48364,
                    z: -3065,
                    heading: 49859,
                },
                {
                    npc: 31856,
                    x: 11861,
                    y: 16173,
                    z: -4568,
                    heading: 18289,
                },
                {
                    npc: 31857,
                    x: 115639,
                    y: -178066,
                    z: -921,
                    heading: 27485,
                },
                {
                    npc: 31858,
                    x: -44810,
                    y: -113388,
                    z: -192,
                    heading: 15527,
                },
            ],
            onStartMessage: 'L2 Day Event: Collect letters from monsters. Create the various words and turn these letters to Event Managers in villages and win great prizes!',
            onEndMessage: 'L2 Day: Event has ended.',
        }
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'overrides/html/events/l2Day'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async initialize(): Promise<void> {
        await super.initialize()

        if ( _.size( this.configuration.words ) !== 3 ) {
            throw new Error( 'Need three words to use with event!' )
        }

        let options: string = _.map( this.configuration.words, ( word: string, index: number ) => {
            return optionTemplate
                    .replace( '%word%', word )
                    .replace( '%index%', index.toString() )
        } ).join( '<br>' )

        this.optionsHtml = DataManager.getHtmlData().getItem( this.getPath( 'manager-1.htm' ) )
                .replace( '%options%', options )

        this.explanationHtml = DataManager.getHtmlData().getItem( this.getPath( 'manager-2.htm' ) )
                .replace( /%word_0%/g, this.configuration.words[ 0 ] )
                .replace( /%word_1%/g, this.configuration.words[ 1 ] )
                .replace( /%word_2%/g, this.configuration.words[ 2 ] )

        this.requiredItems = this.configuration.words.map( this.getRequiredWordCount.bind( this ) )
    }

    async onApproachedForTalk(): Promise<string> {
        return this.optionsHtml
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'collect_0':
                return this.processRewards( player, data.characterNpcId, 0, firstRewards )

            case 'collect_1':
                return this.processRewards( player, data.characterNpcId, 1, secondRewards )

            case 'collect_2':
                return this.processRewards( player, data.characterNpcId, 2, thirdRewards )

            case 'manager-2.htm':
                return this.explanationHtml

            case 'manager-1.htm':
                return this.optionsHtml

            case 'manager-3.htm':
                return this.getPath( data.eventName )
        }
    }

    getRequiredWordCount( word: string ): Array<L2DayRequiredItem> {
        let countMap = _.reduce( _.toUpper( word ).replace( /\s/, '' ).split( '' ), ( finalMap: { [ letter: string ]: number }, currentLetter: string ) => {
            if ( !finalMap[ currentLetter ] ) {
                finalMap[ currentLetter ] = 0
            }

            finalMap[ currentLetter ]++

            return finalMap
        }, {} )

        return _.compact( _.map( countMap, ( amount: number, letter: string ): L2DayRequiredItem => {
            if ( !letterMap[ letter ] ) {
                return null
            }

            return {
                amount,
                itemId: letterMap[ letter ],
            }
        } ) )
    }

    async giveRewards( player: L2PcInstance, rewards: L2DayWordRewards, npcId: number ): Promise<void> {
        let value = _.random( 100 )
        if ( value >= 95 && this.configuration.npcRewards[ npcId ] > 0 ) {
            return QuestHelper.rewardSingleItem( player, this.configuration.npcRewards[ npcId ] )
        }

        await aigle.resolve( rewards ).each( async ( reward: [ number, number, number ] ) => {
            let [ itemId, chance, amount ] = reward
            if ( value >= chance ) {
                await QuestHelper.rewardSingleItem( player, itemId, amount )
            }
        } )
    }

    hasRequiredItems( player: L2PcInstance, requiredItemIndex: number ): boolean {
        return _.every( this.requiredItems[ requiredItemIndex ], ( item: L2DayRequiredItem ): boolean => {
            return player.getInventory().getInventoryItemCount( item.itemId, -1, false ) === item.amount
        } )
    }

    async processRewards( player: L2PcInstance, npcId: number, index: number, rewards: L2DayWordRewards ): Promise<string> {
        if ( this.hasRequiredItems( player, index ) ) {
            await this.takeRequiredItems( player, index )
            await this.giveRewards( player, rewards, npcId )

            return this.getPath( 'manager-1.htm' )
        }

        return this.getPath( 'manager-no.htm' )
    }

    async takeRequiredItems( player: L2PcInstance, requiredItemIndex: number ): Promise<void> {
        await aigle.resolve( this.requiredItems[ requiredItemIndex ] ).each( ( item: L2DayRequiredItem ) : Promise<void> => {
            return QuestHelper.takeSingleItem( player, item.itemId, item.amount )
        } )
    }
}