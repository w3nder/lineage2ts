import { PersistedEventConfiguration, PersistedEventLogic } from '../../gameService/models/listener/PersistedEventLogic'
import { NpcApproachedForTalkEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import _ from 'lodash'

const npcIds: Array<number> = [
    4301,
]

interface ValentineConfiguration extends PersistedEventConfiguration {
    rewardItemIds: Array<number>
}

export class Valentine extends PersistedEventLogic<ValentineConfiguration> {
    constructor() {
        super( 'TheValentineEvent', 'listeners/events/Valentine.ts' )
    }

    getDefaultConfiguration(): ValentineConfiguration {
        return {
            isEnabled: false,
            eventName: 'Valentine Event',
            rewardItemIds: [ 20191 ],
            startDate: 0,
            endDate: 0,
            drops: [
                {
                    itemId: 20192,
                    minCount: 1,
                    maxCount: 1,
                    chance: 5,
                },
                {
                    itemId: 20193,
                    minCount: 1,
                    maxCount: 1,
                    chance: 5,
                },
                {
                    itemId: 20194,
                    minCount: 1,
                    maxCount: 1,
                    chance: 5,
                },
            ],
            npcs: [
                {
                    npc: 4301,
                    x: 87792,
                    y: -142240,
                    z: -1343,
                    heading: 44000,
                },
                {
                    npc: 4301,
                    x: 87616,
                    y: -140688,
                    z: -1542,
                    heading: 16500,
                },
                {
                    npc: 4301,
                    x: 114733,
                    y: -178691,
                    z: -821,
                    heading: 0,
                },
                {
                    npc: 4301,
                    x: 115708,
                    y: -182362,
                    z: -1449,
                    heading: 0,
                },
                {
                    npc: 4301,
                    x: -44337,
                    y: -113669,
                    z: -224,
                    heading: 0,
                },
                {
                    npc: 4301,
                    x: -44628,
                    y: -115409,
                    z: -240,
                    heading: 22500,
                },
                {
                    npc: 4301,
                    x: -13073,
                    y: 122801,
                    z: -3117,
                    heading: 0,
                },
                {
                    npc: 4301,
                    x: -13949,
                    y: 121934,
                    z: -2988,
                    heading: 32768,
                },
                {
                    npc: 4301,
                    x: -14822,
                    y: 123708,
                    z: -3117,
                    heading: 8192,
                },
                {
                    npc: 4301,
                    x: -80762,
                    y: 151118,
                    z: -3043,
                    heading: 28672,
                },
                {
                    npc: 4301,
                    x: -84049,
                    y: 150176,
                    z: -3129,
                    heading: 4096,
                },
                {
                    npc: 4301,
                    x: -82623,
                    y: 151666,
                    z: -3129,
                    heading: 49152,
                },
                {
                    npc: 4301,
                    x: -84516,
                    y: 242971,
                    z: -3730,
                    heading: 34000,
                },
                {
                    npc: 4301,
                    x: -86003,
                    y: 243205,
                    z: -3730,
                    heading: 60000,
                },
                {
                    npc: 4301,
                    x: 11281,
                    y: 15652,
                    z: -4584,
                    heading: 25000,
                },
                {
                    npc: 4301,
                    x: 11303,
                    y: 17732,
                    z: -4574,
                    heading: 57344,
                },
                {
                    npc: 4301,
                    x: 47151,
                    y: 49436,
                    z: -3059,
                    heading: 32000,
                },
                {
                    npc: 4301,
                    x: 79806,
                    y: 55570,
                    z: -1560,
                    heading: 0,
                },
                {
                    npc: 4301,
                    x: 83328,
                    y: 55824,
                    z: -1525,
                    heading: 32768,
                },
                {
                    npc: 4301,
                    x: 80986,
                    y: 54504,
                    z: -1525,
                    heading: 32768,
                },
                {
                    npc: 4301,
                    x: 18178,
                    y: 145149,
                    z: -3054,
                    heading: 7400,
                },
                {
                    npc: 4301,
                    x: 19208,
                    y: 144380,
                    z: -3097,
                    heading: 32768,
                },
                {
                    npc: 4301,
                    x: 19508,
                    y: 145775,
                    z: -3086,
                    heading: 48000,
                },
                {
                    npc: 4301,
                    x: 17396,
                    y: 170259,
                    z: -3507,
                    heading: 30000,
                },
                {
                    npc: 4301,
                    x: 83332,
                    y: 149160,
                    z: -3405,
                    heading: 49152,
                },
                {
                    npc: 4301,
                    x: 82277,
                    y: 148598,
                    z: -3467,
                    heading: 0,
                },
                {
                    npc: 4301,
                    x: 81621,
                    y: 148725,
                    z: -3467,
                    heading: 32768,
                },
                {
                    npc: 4301,
                    x: 81680,
                    y: 145656,
                    z: -3533,
                    heading: 32768,
                },
                {
                    npc: 4301,
                    x: 117498,
                    y: 76630,
                    z: -2695,
                    heading: 38000,
                },
                {
                    npc: 4301,
                    x: 115914,
                    y: 76449,
                    z: -2711,
                    heading: 59000,
                },
                {
                    npc: 4301,
                    x: 119536,
                    y: 76988,
                    z: -2275,
                    heading: 40960,
                },
                {
                    npc: 4301,
                    x: 147120,
                    y: 27312,
                    z: -2192,
                    heading: 40960,
                },
                {
                    npc: 4301,
                    x: 147920,
                    y: 25664,
                    z: -2000,
                    heading: 16384,
                },
                {
                    npc: 4301,
                    x: 111776,
                    y: 221104,
                    z: -3543,
                    heading: 16384,
                },
                {
                    npc: 4301,
                    x: 107904,
                    y: 218096,
                    z: -3675,
                    heading: 0,
                },
                {
                    npc: 4301,
                    x: 114920,
                    y: 220020,
                    z: -3632,
                    heading: 32768,
                },
                {
                    npc: 4301,
                    x: 147888,
                    y: -58048,
                    z: -2979,
                    heading: 49000,
                },
                {
                    npc: 4301,
                    x: 147285,
                    y: -56461,
                    z: -2776,
                    heading: 11500,
                },
                {
                    npc: 4301,
                    x: 44176,
                    y: -48732,
                    z: -800,
                    heading: 33000,
                },
                {
                    npc: 4301,
                    x: 44294,
                    y: -47642,
                    z: -792,
                    heading: 50000,
                },
                {
                    npc: 4301,
                    x: -116677,
                    y: 46824,
                    z: 360,
                    heading: 34828,
                },
            ],
            onEndMessage: 'Valentine : Event has ended.',
            onStartMessage: 'Valentine\'s Event is currently active. Collect all items and make your Valentine Cake!',
        }
    }

    getApproachedForTalkIds(): Array<number> {
        return npcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/events/TheValentineEvent'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        return this.getPath( `${ data.characterNpcId }.htm` )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === '4301-3.htm' ) {
            let hasCompletedEvent: boolean = _.defaultTo( PlayerVariablesManager.get( data.playerId, this.name ) as boolean, false )
            if ( hasCompletedEvent ) {
                return this.getPath( '4301-4.htm' )
            }

            let player = L2World.getPlayer( data.playerId )
            await QuestHelper.rewardMultipleItems( player, 1, ...this.configuration.rewardItemIds )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            PlayerVariablesManager.set( data.playerId, this.name, true )
        }

        return this.getPath( data.eventName )
    }
}