import {
    PersistedConfigurationLogic,
    PersistedConfigurationLogicType,
} from '../../gameService/models/listener/PersistedConfigurationLogic'
import { ListenerDescription } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, PlayerChangedLevelEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { DataManager } from '../../data/manager'
import { L2PlayerLevelupRewardData, L2PlayerLevelupRewardItem } from '../../data/interface/PlayerLevelupRewardsDataApi'
import { Race } from '../../gameService/enums/Race'
import { QuestHelper } from '../helpers/QuestHelper'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { MailManager } from '../../gameService/instancemanager/MailManager'
import { MailMessage } from '../../gameService/models/mail/MailMessage'
import { SendBySystem } from '../../gameService/models/mail/SendBySystem'
import { ConfigManager } from '../../config/ConfigManager'
import { DimensionalTransferManager } from '../../gameService/cache/DimensionalTransferManager'
import { ExNotifyDimensionalItem } from '../../gameService/packets/send/ExNotifyPremiumItem'
import _ from 'lodash'
import aigle from 'aigle'

interface PlayerLevelupSettings extends PersistedConfigurationLogicType {
    isEnabled: boolean
    useMailSystem: boolean
    useRace: boolean
    playerMessageTemplate: string
}

export class PlayerLevelupRewards extends PersistedConfigurationLogic<PlayerLevelupSettings> {

    constructor() {
        super( 'PlayerLevelupRewards', 'listeners/events/PlayerLevelupRewards.ts' )
    }

    getCustomListeners(): Array<ListenerDescription> {
        if ( !this.configuration.isEnabled ) {
            return
        }

        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerChangedLevel,
                method: this.onPlayerLevelup.bind( this ),
                ids: null,
            }
        ]
    }

    getDefaultConfiguration(): PlayerLevelupSettings {
        return {
            isEnabled: true,
            useMailSystem: false,
            useRace: true,
            playerMessageTemplate: 'Upon reaching %level% level you have earned %rewardAmount% reward(s)!',
        }
    }

    async onPlayerLevelup( data: PlayerChangedLevelEvent ): Promise<void> {
        let rewardedLevels = _.defaultTo( PlayerVariablesManager.get( data.playerId, this.getName() ) as Array<number>, [] )
        if ( rewardedLevels.includes( data.newLevelValue ) ) {
            return
        }

        let rewards: Array<L2PlayerLevelupRewardData> = DataManager.getPlayerLevelupRewards().getRewards( data.newLevelValue )
        let { useRace } = this.configuration

        let player = L2World.getPlayer( data.playerId )

        let currentReward: L2PlayerLevelupRewardData = _.find( rewards, ( reward: L2PlayerLevelupRewardData ): boolean => {
            return useRace ? Race[ reward.race ] === player.getRace() : reward.race === 'ALL'
        } )

        if ( !currentReward ) {
            currentReward = _.find( rewards, ( reward: L2PlayerLevelupRewardData ): boolean => {
                return reward.race === 'ALL'
            } )
        }

        if ( !currentReward || currentReward.items.length === 0 ) {
            return
        }

        rewardedLevels.push( data.newLevelValue )
        PlayerVariablesManager.set( data.playerId, this.getName(), rewardedLevels )

        const text: string = this.configuration.playerMessageTemplate
                .replace( /%level%/, data.newLevelValue.toString() )
                .replace( /%rewardAmount%/, currentReward.items.length.toString() )

        if ( this.configuration.useMailSystem ) {
            let message: MailMessage = MailMessage.asSystemMessage( data.playerId, 'Player LevelUp Rewards', text, SendBySystem.None )
            let attachments = message.createAttachments()

            await aigle.resolve( currentReward.items ).each( async ( item: L2PlayerLevelupRewardItem ) => {
                return attachments.addItem( item.itemId, item.amount, 0, 'PlayerLevelupRewards.onPlayerLevelup' )
            } )

            return MailManager.sendMessage( message )
        }

        player.sendMessage( text )

        currentReward.items.forEach( ( item: L2PlayerLevelupRewardItem ) => {
            DimensionalTransferManager.addItem( player.getObjectId(), item.itemId, item.amount, 'Player Levelup Rewards' )
        } )

        player.sendOwnedData( ExNotifyDimensionalItem() )
    }
}