import { PersistedEventConfiguration, PersistedEventLogic } from '../../gameService/models/listener/PersistedEventLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import aigle from 'aigle'
import _ from 'lodash'

const npcId : number = 4306
const GIFT_OF_VITALITY = new SkillHolder( 23179, 1 )
const JOY_OF_VITALITY = new SkillHolder( 23180, 1 )

const fighterBuffs : Array<SkillHolder> = [
    new SkillHolder( 5627 ), // Wind Walk
    new SkillHolder( 5628 ), // Shield
    new SkillHolder( 5637 ), // Magic Barrier
    new SkillHolder( 5629 ), // Bless the Body
    new SkillHolder( 5630 ), // Vampiric Rage
    new SkillHolder( 5631 ), // Regeneration
    new SkillHolder( 5632 ), // Haste
]

const mageBuffs : Array<SkillHolder> = [
    new SkillHolder( 5627 ), // Wind Walk
    new SkillHolder( 5628 ), // Shield
    new SkillHolder( 5637 ), // Magic Barrier
    new SkillHolder( 5633 ), // Bless the Soul
    new SkillHolder( 5634 ), // Acumen
    new SkillHolder( 5635 ), // Concentration
    new SkillHolder( 5636 ), // Empower
]

const servitorBuffs : Array<SkillHolder> = [
    new SkillHolder( 5627 ), // Wind Walk
    new SkillHolder( 5628 ), // Shield
    new SkillHolder( 5637 ), // Magic Barrier
    new SkillHolder( 5629 ), // Bless the Body
    new SkillHolder( 5633 ), // Bless the Soul
    new SkillHolder( 5630 ), // Vampiric Rage
    new SkillHolder( 5634 ), // Acumen
    new SkillHolder( 5631 ), // Regeneration
    new SkillHolder( 5635 ), // Concentration
    new SkillHolder( 5632 ), // Haste
    new SkillHolder( 5636 ), // Empower
]

interface GiftOfVitalityConfiguration extends PersistedEventConfiguration{
    minimumLevel: number
    reuseIntervalHours: number
}

export class GiftOfVitality extends PersistedEventLogic<GiftOfVitalityConfiguration> {
    constructor() {
        super( 'GiftOfVitality', 'listeners/events/GiftOfVitality.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    getDefaultConfiguration(): GiftOfVitalityConfiguration {
        return {
            isEnabled: false,
            eventName: 'Gift Of Vitality Event',
            endDate: 0,
            startDate: 0,
            minimumLevel: 75,
            reuseIntervalHours: 5,
            onEndMessage: 'Gift of Vitality: Event has ended.',
            onStartMessage: 'Gift of Vitality: Event ongoing!',
            npcs: [
                {
                    npc: npcId,
                    x: 87116,
                    y: -141332,
                    z: -1336,
                    heading: 52193
                },
                {
                    npc: npcId,
                    x: -13869,
                    y: 122063,
                    z: -2984,
                    heading: 18270
                },
                {
                    npc: npcId,
                    x: 17203,
                    y: 144949,
                    z: -3024,
                    heading: 18166
                },
                {
                    npc: npcId,
                    x: 82766,
                    y: 149438,
                    z: -3464,
                    heading: 33865
                },
                {
                    npc: npcId,
                    x: -83161,
                    y: 150915,
                    z: -3120,
                    heading: 17311
                },
                {
                    npc: npcId,
                    x: -84037,
                    y: 243194,
                    z: -3728,
                    heading: 8992
                },
                {
                    npc: npcId,
                    x: 45402,
                    y: 48355,
                    z: -3056,
                    heading: 49153
                },
                {
                    npc: npcId,
                    x: 12084,
                    y: 16576,
                    z: -4584,
                    heading: 57345
                },
                {
                    npc: npcId,
                    x: 82286,
                    y: 53291,
                    z: -1488,
                    heading: 15250
                },
                {
                    npc: npcId,
                    x: 147060,
                    y: 25943,
                    z: -2008,
                    heading: 46171
                },
                {
                    npc: npcId,
                    x: 116868,
                    y: 77309,
                    z: -2688,
                    heading: 40353
                },
                {
                    npc: npcId,
                    x: -119690,
                    y: 44583,
                    z: 360,
                    heading: 29289
                },
                {
                    npc: npcId,
                    x: 111164,
                    y: 221062,
                    z: -3544,
                    heading: 2714
                },
                {
                    npc: npcId,
                    x: -44928,
                    y: -113608,
                    z: -192,
                    heading: 30212
                },
                {
                    npc: npcId,
                    x: 115616,
                    y: -177941,
                    z: -896,
                    heading: 30708
                },
                {
                    npc: npcId,
                    x: 148096,
                    y: -55466,
                    z: -2728,
                    heading: 40541
                },
                {
                    npc: npcId,
                    x: 43521,
                    y: -47542,
                    z: -792,
                    heading: 31655
                }
            ]
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/events/GiftOfVitality'
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( '4306.htm' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case 'vitality':
                let lastDateUsed = _.defaultTo( PlayerVariablesManager.get( data.playerId, this.name ), 0 ) as number
                let currentDate = Date.now()
                let timeDifference = lastDateUsed - currentDate

                if ( timeDifference > 0 ) {
                    let [ hours, minutes ] = GeneralHelper.getTimeDifference( lastDateUsed - currentDate )
                    let message = new SystemMessageBuilder( SystemMessageIds.AVAILABLE_AFTER_S1_S2_HOURS_S3_MINUTES )
                            .addSkillNameById( GIFT_OF_VITALITY.getId() )
                            .addNumber( hours )
                            .addNumber( minutes )
                            .getBuffer()

                    player.sendOwnedData( message )
                    return this.getPath( '4306-notime.htm' )
                }

                await player.doCast( GIFT_OF_VITALITY.getSkill() )
                await player.doSimultaneousCast( JOY_OF_VITALITY.getSkill() )
                PlayerVariablesManager.set( data.playerId, this.name, currentDate + GeneralHelper.hoursToMillis( this.configuration.reuseIntervalHours ) )

                return this.getPath( '4306-okvitality.htm' )

            case 'memories_player':
                if ( player.getLevel() <= this.configuration.minimumLevel ) {
                    return this.getPath( '4306-nolevel.htm' )
                }

                npc.setTarget( player )

                await aigle.resolve( player.isMageClass() ? mageBuffs : fighterBuffs ).each( ( holder: SkillHolder ) : Promise<void> => {
                    return npc.doCast( holder.getSkill() )
                } )

                return this.getPath( '4306-okbuff.htm' )

            case 'memories_summon': {
                if ( player.getLevel() <= this.configuration.minimumLevel ) {
                    return this.getPath( '4306-nolevel.htm' )
                }

                if ( !player.hasServitor() ) {
                    return this.getPath( '4306-nosummon.htm' )
                }

                npc.setTarget( player.getSummon() )
                await aigle.resolve( servitorBuffs ).each( ( holder: SkillHolder ) : Promise<void> => {
                    return npc.doCast( holder.getSkill() )
                } )

                return this.getPath( '4306-okbuff.htm' )
            }
        }

        return this.getPath( data.eventName )
    }
}