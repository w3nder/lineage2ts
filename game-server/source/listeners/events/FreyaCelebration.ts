import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { EventType, NpcGeneralEvent, NpcSeeSkillEvent } from '../../gameService/models/events/EventType'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { CreatureSay } from '../../gameService/packets/send/builder/CreatureSay'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { DataManager } from '../../data/manager'
import { PersistedEventConfiguration, PersistedEventLogic } from '../../gameService/models/listener/PersistedEventLogic'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import _ from 'lodash'
import { ListenerDescription } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const FREYA = 13296

const FREYA_POTION = 15440
const FREYA_GIFT = 17138
const skillIds: Set<number> = new Set( [
    9150,
    9151,
    9152,
    9153,
    9154,
    9155,
    9156,
] )

const textLines: Array<number> = [
    NpcStringIds.EVEN_THOUGH_YOU_BRING_SOMETHING_CALLED_A_GIFT_AMONG_YOUR_HUMANS_IT_WOULD_JUST_BE_PROBLEMATIC_FOR_ME,
    NpcStringIds.I_JUST_DONT_KNOW_WHAT_EXPRESSION_I_SHOULD_HAVE_IT_APPEARED_ON_ME_ARE_HUMANS_EMOTIONS_LIKE_THIS_FEELING,
    NpcStringIds.THE_FEELING_OF_THANKS_IS_JUST_TOO_MUCH_DISTANT_MEMORY_FOR_ME,
    NpcStringIds.BUT_I_KIND_OF_MISS_IT_LIKE_I_HAD_FELT_THIS_FEELING_BEFORE,
    NpcStringIds.I_AM_ICE_QUEEN_FREYA_THIS_FEELING_AND_EMOTION_ARE_NOTHING_BUT_A_PART_OF_MELISSAA_MEMORIES,
]

interface FreyaCelebrationConfiguration extends PersistedEventConfiguration {
    reuseIntervalHours: number
    adenaCost: number
}

export class FreyaCelebration extends PersistedEventLogic<FreyaCelebrationConfiguration> {
    html: string

    constructor() {
        super( 'FreyaCelebration', 'listeners/events/FreyaCelebration.ts' )
    }

    getDefaultConfiguration(): FreyaCelebrationConfiguration {
        return {
            isEnabled: false,
            eventName: 'Freya Celebration Event',
            startDate: 0,
            endDate: 0,
            reuseIntervalHours: 20,
            adenaCost: 1,
            npcs: [
                {
                    npc: FREYA,
                    x: -119494,
                    y: 44882,
                    z: 360,
                    heading: 24576,
                },
                {
                    npc: FREYA,
                    x: -117239,
                    y: 46842,
                    z: 360,
                    heading: 49151,
                },
                {
                    npc: FREYA,
                    x: -84023,
                    y: 243051,
                    z: -3728,
                    heading: 4096,
                },
                {
                    npc: FREYA,
                    x: -84411,
                    y: 244813,
                    z: -3728,
                    heading: 57343,
                },
                {
                    npc: FREYA,
                    x: 46908,
                    y: 50856,
                    z: -2992,
                    heading: 8192,
                },
                {
                    npc: FREYA,
                    x: 45538,
                    y: 48357,
                    z: -3056,
                    heading: 18000,
                },
                {
                    npc: FREYA,
                    x: -45372,
                    y: -114104,
                    z: -240,
                    heading: 16384,
                },
                {
                    npc: FREYA,
                    x: -45278,
                    y: -112766,
                    z: -240,
                    heading: 0,
                },
                {
                    npc: FREYA,
                    x: 9929,
                    y: 16324,
                    z: -4568,
                    heading: 62999,
                },
                {
                    npc: FREYA,
                    x: 11546,
                    y: 17599,
                    z: -4584,
                    heading: 46900,
                },
                {
                    npc: FREYA,
                    x: 115096,
                    y: -178370,
                    z: -880,
                    heading: 0,
                },
                {
                    npc: FREYA,
                    x: -13727,
                    y: 122117,
                    z: -2984,
                    heading: 16384,
                },
                {
                    npc: FREYA,
                    x: -14129,
                    y: 123869,
                    z: -3112,
                    heading: 40959,
                },
                {
                    npc: FREYA,
                    x: -83156,
                    y: 150994,
                    z: -3120,
                    heading: 0,
                },
                {
                    npc: FREYA,
                    x: -81031,
                    y: 150038,
                    z: -3040,
                    heading: 0,
                },
                {
                    npc: FREYA,
                    x: 16111,
                    y: 142850,
                    z: -2696,
                    heading: 16000,
                },
                {
                    npc: FREYA,
                    x: 17275,
                    y: 145000,
                    z: -3032,
                    heading: 25000,
                },
                {
                    npc: FREYA,
                    x: 111004,
                    y: 218928,
                    z: -3536,
                    heading: 16384,
                },
                {
                    npc: FREYA,
                    x: 81755,
                    y: 146487,
                    z: -3528,
                    heading: 32768,
                },
                {
                    npc: FREYA,
                    x: 82145,
                    y: 148609,
                    z: -3464,
                    heading: 0,
                },
                {
                    npc: FREYA,
                    x: 83037,
                    y: 149324,
                    z: -3464,
                    heading: 44000,
                },
                {
                    npc: FREYA,
                    x: 81987,
                    y: 53723,
                    z: -1488,
                    heading: 0,
                },
                {
                    npc: FREYA,
                    x: 147200,
                    y: 25614,
                    z: -2008,
                    heading: 16384,
                },
                {
                    npc: FREYA,
                    x: 148557,
                    y: 26806,
                    z: -2200,
                    heading: 32768,
                },
                {
                    npc: FREYA,
                    x: 147421,
                    y: -55435,
                    z: -2728,
                    heading: 49151,
                },
                {
                    npc: FREYA,
                    x: 148206,
                    y: -55786,
                    z: -2776,
                    heading: 61439,
                },
                {
                    npc: FREYA,
                    x: 85584,
                    y: -142490,
                    z: -1336,
                    heading: 0,
                },
                {
                    npc: FREYA,
                    x: 86865,
                    y: -142915,
                    z: -1336,
                    heading: 26000,
                },
                {
                    npc: FREYA,
                    x: 43966,
                    y: -47709,
                    z: -792,
                    heading: 49999,
                },
                {
                    npc: FREYA,
                    x: 43165,
                    y: -48461,
                    z: -792,
                    heading: 17000,
                },
            ],

            onEndMessage: 'Freya Celebration: Event has ended.',
            onStartMessage: 'Freya Celebration: Event ongoing!',
        }
    }

    getApproachedForTalkIds(): Array<number> {
        return [ FREYA ]
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSeeSkill,
                method: this.onNpcSeeSkill.bind( this ),
                ids: [ FREYA ],
                triggers: {
                    targetIds: skillIds
                }
            }
        ]
    }

    getPathPrefix(): string {
        return 'overrides/html/events/freyaCelebration'
    }

    getQuestStartIds(): Array<number> {
        return [ FREYA ]
    }

    getTalkIds(): Array<number> {
        return [ FREYA ]
    }

    async onApproachedForTalk(): Promise<string> {
        if ( !this.html ) {
            this.html = DataManager.getHtmlData().getItem( this.getPath( '13296.htm' ) )
                    .replace( '%timeInterval%', this.configuration.reuseIntervalHours.toString() )
                    .replace( '%adenaAmount%', this.configuration.adenaCost.toString() )
        }

        return this.html
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === 'give_potion' ) {

            let previousDate: number = _.defaultTo( PlayerVariablesManager.get( data.playerId, this.name ) as number, 0 )
            let reuseInterval = GeneralHelper.hoursToMillis( this.configuration.reuseIntervalHours )

            let currentDate = Date.now()
            if ( currentDate > previousDate ) {
                let player = L2World.getPlayer( data.playerId )

                if ( player.getAdena() >= this.configuration.adenaCost ) {
                    await player.getInventory().reduceAdena( this.configuration.adenaCost )
                    await QuestHelper.giveSingleItem( player, FREYA_POTION, 1 )
                    PlayerVariablesManager.set( data.playerId, this.name, currentDate + reuseInterval )

                    return
                }

                let message = new SystemMessageBuilder( SystemMessageIds.S2_UNIT_OF_THE_ITEM_S1_REQUIRED )
                        .addItemNameWithId( ItemTypes.Adena )
                        .addNumber( this.configuration.adenaCost )
                        .getBuffer()
                player.sendOwnedData( message )

                return
            }

            let [ hours, minutes ] = GeneralHelper.getTimeDifference( previousDate - currentDate )
            let message = new SystemMessageBuilder( SystemMessageIds.AVAILABLE_AFTER_S1_S2_HOURS_S3_MINUTES )
                    .addItemNameWithId( FREYA_POTION )
                    .addNumber( hours )
                    .addNumber( minutes )
                    .getBuffer()
            PacketDispatcher.sendOwnedData( data.playerId, message )

            return
        }
    }

    async onNpcSeeSkill( data: NpcSeeSkillEvent ): Promise<void> {
        if ( data.targetIds.includes( data.receiverId ) ) {
            if ( _.random( 100 ) < 5 ) {

                let player = L2World.getPlayer( data.playerId )
                let npc = L2World.getObjectById( data.receiverId ) as L2Npc

                let packet: Buffer = CreatureSay.fromNpcName( data.receiverId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.DEAR_S1_THINK_OF_THIS_AS_MY_APPRECIATION_FOR_THE_GIFT_TAKE_THIS_WITH_YOU_THERES_NOTHING_STRANGE_ABOUT_IT_ITS_JUST_A_BIT_OF_MY_CAPRICIOUSNESS )
                        .addParameter( player.getName() )
                        .getBuffer()

                BroadcastHelper.dataBasedOnVisibility( npc, packet )
                await QuestHelper.giveSingleItem( player, FREYA_GIFT, 1, 1 )

                return
            }

            if ( _.random( 10 ) < 2 ) {
                let npc = L2World.getObjectById( data.receiverId ) as L2Npc
                let packet: Buffer = CreatureSay.fromNpcName( data.receiverId, NpcSayType.NpcAll, npc.getName(), _.sample( textLines ) ).getBuffer()
                BroadcastHelper.dataBasedOnVisibility( npc, packet )

                return
            }
        }
    }
}