import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const FERRIS = 30847

const ENCHANTED_IRON_GOLEM = 20566

const ARTISANS_FRAME = 1891
const ORIHARUKON = 1893
const SEWING_KIT = 7078
const ENCHANTED_IRON = 7163

const minimumLevel = 60
const IRON_COUNT = 5
const COUNT = 10

export class MakeASewingKit extends ListenerLogic {
    constructor() {
        super( 'Q00036_MakeASewingKit', 'listeners/tracked/MakeASewingKit.ts' )
        this.questId = 36
        this.questItemIds = [ ENCHANTED_IRON ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ENCHANTED_IRON_GOLEM ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00036_MakeASewingKit'
    }

    getQuestStartIds(): Array<number> {
        return [ FERRIS ]
    }

    getTalkIds(): Array<number> {
        return [ FERRIS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player || QuestHelper.getAdjustedChance( ENCHANTED_IRON, Math.random(), data.isChampion ) < 0.5 ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, ENCHANTED_IRON, 1, data.isChampion )

        if ( QuestHelper.hasQuestItemCount( player, ENCHANTED_IRON, IRON_COUNT ) ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.name )
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30847-03.htm':
                state.startQuest()
                break

            case '30847-06.html':
                if ( QuestHelper.getQuestItemsCount( player, ENCHANTED_IRON ) < IRON_COUNT ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeSingleItem( player, ENCHANTED_IRON, -1 )
                state.setConditionWithSound( 3, true )
                break

            case '30847-09.html':
                if ( QuestHelper.hasQuestItemCount( player, ARTISANS_FRAME, COUNT )
                        && QuestHelper.hasQuestItemCount( player, ORIHARUKON, COUNT ) ) {
                    await QuestHelper.takeSingleItem( player, ARTISANS_FRAME, 10 )
                    await QuestHelper.takeSingleItem( player, ORIHARUKON, 10 )

                    await QuestHelper.giveSingleItem( player, SEWING_KIT, 1 )
                    await state.exitQuest( false, true )

                    break
                }

                return this.getPath( '30847-10.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30847-01.htm' : '30847-02.html' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30847-04.html' )

                    case 2:
                        return this.getPath( '30847-05.html' )

                    case 3:
                        if ( QuestHelper.hasQuestItemCount( player, ARTISANS_FRAME, COUNT ) && QuestHelper.hasQuestItemCount( player, ORIHARUKON, COUNT ) ) {
                            return this.getPath( '30847-07.html' )
                        }

                        return this.getPath( '30847-08.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getNoQuestMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}