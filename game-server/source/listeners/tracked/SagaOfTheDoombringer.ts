import { SagaLogic } from '../helpers/SagaLogic'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    32138,
    31627,
    32223,
    32227,
    32254,
    31646,
    31647,
    31650,
    31654,
    31655,
    31656,
    32227,
]

const itemIds: Array<number> = [
    7080,
    9721,
    7081,
    9740,
    9722,
    9725,
    9728,
    9731,
    9734,
    9737,
    9717,
    0,
]

const mobIds: Array<number> = [
    27324,
    27325,
    27326,
]

const spawnLocations: Array<ILocational> = [
    new Location( 191046, -40640, -3042 ),
    new Location( 47429, -56923, -2383 ),
    new Location( 47391, -56929, -2370 ),
]

export class SagaOfTheDoombringer extends SagaLogic {
    constructor() {
        super( 'Q00067_SagaOfTheDoombringer', 'listeners/tracked-100/SagaOfTheDoombringer.ts' )
        this.questId = 67
    }

    getClassId( player: L2PcInstance ): number {
        return 131
    }

    getPreviousClassId( player: L2PcInstance ): number {
        return 0x7f
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00067_SagaOfTheDoombringer'
    }
}