import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    31581,
    31626,
    31588,
    31287,
    31621,
    31646,
    31647,
    31651,
    31654,
    31655,
    31658,
    31287,
]

const itemIds: Array<number> = [
    7080,
    7525,
    7081,
    7513,
    7296,
    7327,
    7358,
    7389,
    7420,
    7451,
    7090,
    0,
]

const mobIds: Array<number> = [
    27270,
    27247,
    27277,
]

const spawnLocations: Array<ILocational> = [
    new Location( 119518, -28658, -3811 ),
    new Location( 181215, 36676, -4812 ),
    new Location( 181227, 36703, -4816 ),
]

export class SagaOfTheShillienSaint extends SagaLogic {
    constructor() {
        super( 'Q00098_SagaOfTheShillienSaint', 'listeners/tracked/SagaOfTheShillienSaint.ts' )
        this.questId = 98
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 112
    }

    getPreviousClassId(): number {
        return 0x2b
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00098_SagaOfTheShillienSaint'
    }
}