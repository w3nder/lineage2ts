import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'

const ISAEL_SILVERSHADOW = 30655
const KITZKA = 31045

const DELIVERY_BOX = 17281
const rewards = {
    '31045-10.html': 17248, // Large Dragon Bone
    '31045-11.html': 17266, // Will of Antharas
    '31045-12.html': 17267, // Sealed Blood Crystal
}

const minimumLevel = 80

export class TiredOfWaiting extends ListenerLogic {
    constructor() {
        super( 'Q00026_TiredOfWaiting', 'listeners/tracked/TiredOfWaiting.ts' )
        this.questId = 26
        this.questItemIds = [ DELIVERY_BOX ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00026_TiredOfWaiting'
    }

    getQuestStartIds(): Array<number> {
        return [ ISAEL_SILVERSHADOW ]
    }

    getTalkIds(): Array<number> {
        return [ ISAEL_SILVERSHADOW, KITZKA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30655-02.htm':
            case '30655-03.htm':
            case '30655-05.html':
            case '30655-06.html':
            case '31045-02.html':
            case '31045-03.html':
            case '31045-05.html':
            case '31045-06.html':
            case '31045-07.html':
            case '31045-08.html':
            case '31045-09.html':
                return this.getPath( data.eventName )

            case '30655-04.html':
                if ( state.isCreated() ) {
                    await QuestHelper.giveSingleItem( player, DELIVERY_BOX, 1 )
                    state.startQuest()
                    return this.getPath( data.eventName )
                }

                return

            case '31045-04.html':
                if ( state.isStarted() ) {
                    await QuestHelper.takeSingleItem( player, DELIVERY_BOX, -1 )
                    return this.getPath( data.eventName )
                }

                return

            case '31045-10.html':
            case '31045-11.html':
            case '31045-12.html':
                if ( state.isStarted() ) {
                    await QuestHelper.giveSingleItem( player, rewards[ data.eventName ], 1 )
                    await state.exitQuest( false, true )
                    return this.getPath( data.eventName )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ISAEL_SILVERSHADOW:
                if ( state.isCreated() ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30655-01.htm' : '30655-00.html' )
                }

                if ( state.isStarted() ) {
                    return this.getPath( '30655-07.html' )
                }

                return this.getPath( '30655-08.html' )

            case KITZKA:
                if ( state.isStarted() ) {
                    return this.getPath( QuestHelper.hasQuestItem( player, DELIVERY_BOX ) ? '31045-01.html' : '31045-09.html' )
                }

                return
        }
    }
}