import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const HIERARCH = 31517
const saintAltarN1 = 31508
const saintAltarN2 = 31509
const saintAltarN3 = 31510
const saintAltarN4 = 31511

const BLOOD_OF_SAINT = 7168

export class LightAndDarkness extends ListenerLogic {
    constructor() {
        super( 'Q00017_LightAndDarkness', 'listeners/tracked/LightAndDarkness.ts' )
        this.questId = 17
        this.questItemIds = [ BLOOD_OF_SAINT ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00017_LightAndDarkness'
    }

    getQuestStartIds(): Array<number> {
        return [ HIERARCH ]
    }

    getTalkIds(): Array<number> {
        return [ HIERARCH, saintAltarN1, saintAltarN2, saintAltarN3, saintAltarN4 ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, false )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31517-02.html':
                if ( player.getLevel() < 61 ) {
                    return this.getPath( '31517-02a.html' )
                }

                state.startQuest()
                await QuestHelper.giveSingleItem( player, BLOOD_OF_SAINT, 4 )

                break

            case '31508-02.html':
            case '31509-02.html':
            case '31510-02.html':
            case '31511-02.html':
                let conditionValue = state.getCondition()
                let npcId = parseInt( data.eventName.replace( '-02.html', '' ) )
                if ( ( conditionValue === ( npcId - 31507 ) ) && QuestHelper.hasQuestItems( player, BLOOD_OF_SAINT ) ) {
                    await QuestHelper.takeSingleItem( player, BLOOD_OF_SAINT, 1 )
                    state.setConditionWithSound( conditionValue + 1, true )

                    return this.getPath( `${ npcId }-01.html` )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.hasQuestCompleted( 'Q00015_SweetWhispers' ) ? '31517-00.htm' : '31517-06.html' )

            case QuestStateValues.STARTED:
                let itemCount = QuestHelper.getQuestItemsCount( player, BLOOD_OF_SAINT )

                switch ( data.characterNpcId ) {
                    case HIERARCH:
                        if ( state.getCondition() < 5 ) {
                            return this.getPath( itemCount >= 5 ? '31517-05.html' : '31517-04.html' )
                        }

                        await QuestHelper.addExpAndSp( player, 697040, 54887 )
                        await state.exitQuest( false, true )
                        return this.getPath( '31517-03.html' )

                    case saintAltarN1:
                    case saintAltarN2:
                    case saintAltarN3:
                    case saintAltarN4:
                        let npcIdValue = data.characterNpcId - 31507
                        let conditionValue = state.getCondition()
                        if ( npcIdValue === conditionValue ) {
                            let suffix = itemCount > 0 ? '00' : '02'
                            return this.getPath( `${data.characterNpcId}-${suffix}.html` )
                        }

                        if ( conditionValue > npcIdValue ) {
                            return this.getPath( `${data.characterNpcId}-03.html` )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}