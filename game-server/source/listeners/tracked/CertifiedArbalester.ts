import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { QuestVariables } from '../helpers/QuestVariables'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import _ from 'lodash'

const WAREHOUSE_KEEPER_HOLVAS = 30058
const MAGISTER_GAIUS = 30171
const BLACKSMITH_POITAN = 30458
const MAGISTER_CLAYTON = 30464
const MAGISTER_GAUEN = 30717
const MAGISTER_KAIENA = 30720
const MASTER_RINDY = 32201
const GRAND_MASTER_MELDINA = 32214
const MASTER_SELSIA = 32220

const ENMITY_CRYSTAL = 9773
const ENMITY_CRYSTAL_CORE = 9774
const MANUSCRIPT_PAGE = 9775
const ENCODED_PAGE_ON_THE_ANCIENT_RACE = 9776
const KAMAEL_INQUISITOR_TRAINEE_MARK = 9777
const FRAGMENT_OF_ATTACK_ORDERS = 9778
const GRANDIS_ATTACK_ORDERS = 9779
const MANASHENS_TALISMAN = 9780
const RESEARCH_ON_THE_GIANTS_AND_THE_ANCIENT_RACE = 9781

const DIMENSIONAL_DIAMOND = 7562
const KAMAEL_INQUISITOR_MARK = 9782

const GRANITIC_GOLEM = 20083
const HANGMAN_TREE = 20144
const AMBER_BASILISK = 20199
const STRAIN = 20200
const GHOUL = 20201
const DEAD_SEEKER = 20202
const GRANDIS = 20554
const MANASHEN_GARGOYLE = 20563
const TIMAK_ORC = 20583
const TIMAK_ORC_ARCHER = 20584
const DELU_LIZARDMAN_SHAMAN = 20781
const WATCHMAN_OF_THE_PLAINS = 21102
const ROUGHLY_HEWN_ROCK_GOLEM = 21103
const DELU_LIZARDMAN_SUPPLIER = 21104
const DELU_LIZARDMAN_AGENT = 21105
const CURSED_SEER = 21106
const DELU_LIZARDMAN_COMMANDER = 21107

const CRIMSON_LADY = 27336

const minimumLevel = 39

export class CertifiedArbalester extends ListenerLogic {
    constructor() {
        super( 'Q00066_CertifiedArbalester', 'listeners/tracked/CertifiedArbalester.ts' )
        this.questId = 66
        this.questItemIds = [
            ENMITY_CRYSTAL,
            ENMITY_CRYSTAL_CORE,
            MANUSCRIPT_PAGE,
            ENCODED_PAGE_ON_THE_ANCIENT_RACE,
            KAMAEL_INQUISITOR_TRAINEE_MARK,
            FRAGMENT_OF_ATTACK_ORDERS,
            GRANDIS_ATTACK_ORDERS,
            MANASHENS_TALISMAN,
            RESEARCH_ON_THE_GIANTS_AND_THE_ANCIENT_RACE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            GRANITIC_GOLEM,
            HANGMAN_TREE,
            AMBER_BASILISK,
            STRAIN, GHOUL,
            DEAD_SEEKER,
            GRANDIS,
            MANASHEN_GARGOYLE,
            TIMAK_ORC,
            TIMAK_ORC_ARCHER,
            DELU_LIZARDMAN_SHAMAN,
            WATCHMAN_OF_THE_PLAINS,
            ROUGHLY_HEWN_ROCK_GOLEM,
            DELU_LIZARDMAN_SUPPLIER,
            DELU_LIZARDMAN_AGENT,
            CURSED_SEER,
            DELU_LIZARDMAN_COMMANDER,
            CRIMSON_LADY,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00066_CertifiedArbalester'
    }

    getQuestStartIds(): Array<number> {
        return [ MASTER_RINDY ]
    }

    getTalkIds(): Array<number> {
        return [
            MASTER_RINDY,
            WAREHOUSE_KEEPER_HOLVAS,
            MAGISTER_GAIUS,
            BLACKSMITH_POITAN,
            MAGISTER_CLAYTON,
            MAGISTER_GAUEN,
            MAGISTER_KAIENA,
            GRAND_MASTER_MELDINA,
            MASTER_SELSIA,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
            return
        }

        let manuscriptCount = QuestHelper.getQuestItemsCount( player, MANUSCRIPT_PAGE )
        let crystalCount = QuestHelper.getQuestItemsCount( player, ENMITY_CRYSTAL )

        switch ( data.npcId ) {
            case GRANITIC_GOLEM:
            case HANGMAN_TREE:
                if ( state.isMemoState( 8 ) && manuscriptCount < 30 ) {
                    await QuestHelper.rewardSingleQuestItem( player, MANUSCRIPT_PAGE, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, MANUSCRIPT_PAGE ) < 29
                            && _.random( 100 ) < QuestHelper.getAdjustedChance( MANUSCRIPT_PAGE, 10, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, MANUSCRIPT_PAGE, 1, data.isChampion )
                    }

                    await this.tryToProgressQuest( player, state )
                }

                return

            case AMBER_BASILISK:
                if ( state.isMemoState( 8 )
                        && manuscriptCount < 30
                        && _.random( 100 ) < QuestHelper.getAdjustedChance( MANUSCRIPT_PAGE, 98, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, MANUSCRIPT_PAGE, 1, data.isChampion )

                    await this.tryToProgressQuest( player, state )
                }

                return

            case STRAIN:
                if ( state.isMemoState( 8 )
                        && manuscriptCount < 30
                        && _.random( 100 ) < QuestHelper.getAdjustedChance( MANUSCRIPT_PAGE, 86, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, MANUSCRIPT_PAGE, 1, data.isChampion )

                    await this.tryToProgressQuest( player, state )
                }

                return

            case GHOUL:
            case DEAD_SEEKER:
                if ( state.isMemoState( 8 ) && manuscriptCount < 30 ) {
                    await QuestHelper.rewardSingleQuestItem( player, MANUSCRIPT_PAGE, 1, data.isChampion )

                    if ( ( manuscriptCount + 1 ) < 29
                            && _.random( 1000 ) < QuestHelper.getAdjustedChance( MANUSCRIPT_PAGE, 20, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, MANUSCRIPT_PAGE, 1, data.isChampion )
                    }

                    await this.tryToProgressQuest( player, state )
                }

                return

            case GRANDIS:
                let attackOrdersCount = QuestHelper.getQuestItemsCount( player, FRAGMENT_OF_ATTACK_ORDERS )
                if ( ( state.isMemoState( 21 ) || ( state.isMemoState( 22 ) && attackOrdersCount < 10 ) )
                        && _.random( 100 ) < QuestHelper.getAdjustedChance( FRAGMENT_OF_ATTACK_ORDERS, 78, data.isChampion ) ) {

                    if ( state.isMemoState( 21 ) && attackOrdersCount === 0 ) {
                        state.setMemoState( 22 )
                        state.setConditionWithSound( 12, true )

                        await QuestHelper.rewardSingleQuestItem( player, FRAGMENT_OF_ATTACK_ORDERS, 1, data.isChampion )
                        return
                    }

                    if ( state.isMemoState( 22 ) && attackOrdersCount === 9 ) {
                        state.setMemoState( 23 )
                        state.setConditionWithSound( 13, true )

                        await QuestHelper.takeSingleItem( player, FRAGMENT_OF_ATTACK_ORDERS, -1 )
                        await QuestHelper.giveSingleItem( player, GRANDIS_ATTACK_ORDERS, 1 )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    await QuestHelper.rewardSingleQuestItem( player, FRAGMENT_OF_ATTACK_ORDERS, 1, data.isChampion )

                    return
                }

                return

            case MANASHEN_GARGOYLE:
                let talismanCount: number = QuestHelper.getQuestItemsCount( player, MANASHENS_TALISMAN )
                if ( ( state.isMemoState( 25 ) || ( state.isMemoState( 26 ) && talismanCount < 10 ) )
                        && _.random( 1000 ) < QuestHelper.getAdjustedChance( MANASHENS_TALISMAN, 840, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, MANASHENS_TALISMAN, 1, data.isChampion )

                    if ( state.isMemoState( 25 ) && talismanCount === 0 ) {
                        state.setMemoState( 26 )
                        state.setConditionWithSound( 15, true )
                        return
                    }

                    if ( state.isMemoState( 26 ) && ( talismanCount === 9 ) ) {
                        state.setMemoState( 27 )
                        state.setConditionWithSound( 16, true )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    return
                }

                return

            case TIMAK_ORC:
            case TIMAK_ORC_ARCHER:
                if ( state.isMemoState( 32 ) ) {
                    let exValue = state.getMemoStateEx( 1 )
                    if ( exValue < 5 ) {
                        state.setMemoStateEx( 1, exValue + 1 )
                        return
                    }

                    state.setMemoStateEx( 1, 0 )
                    QuestHelper.addSpawnAtLocation( CRIMSON_LADY, npc, true, 0 )
                }

                return

            case DELU_LIZARDMAN_SHAMAN:
            case DELU_LIZARDMAN_SUPPLIER:
                if ( state.isMemoState( 3 ) && crystalCount < 30 ) {
                    await QuestHelper.rewardSingleQuestItem( player, ENMITY_CRYSTAL, 1, data.isChampion )

                    if ( ( crystalCount + 1 ) < 29
                            && _.random( 1000 ) < QuestHelper.getAdjustedChance( ENMITY_CRYSTAL, 80, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, ENMITY_CRYSTAL, 1, data.isChampion )
                    }

                    await this.tryToProgressQuestWithCrystals( player, state )
                }

                return

            case WATCHMAN_OF_THE_PLAINS:
                if ( state.isMemoState( 3 )
                        && crystalCount < 30
                        && _.random( 100 ) < QuestHelper.getAdjustedChance( ENMITY_CRYSTAL, 84, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, ENMITY_CRYSTAL, 1, data.isChampion )

                    await this.tryToProgressQuestWithCrystals( player, state )
                }

                return

            case ROUGHLY_HEWN_ROCK_GOLEM:
                if ( state.isMemoState( 3 )
                        && crystalCount < 30
                        && _.random( 100 ) < QuestHelper.getAdjustedChance( ENMITY_CRYSTAL, 86, data.isChampion ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, ENMITY_CRYSTAL, 1, data.isChampion )

                    await this.tryToProgressQuestWithCrystals( player, state )
                }

                return

            case DELU_LIZARDMAN_AGENT:
                if ( state.isMemoState( 3 ) && crystalCount < 30 ) {
                    await QuestHelper.rewardSingleQuestItem( player, ENMITY_CRYSTAL, 1, data.isChampion )

                    if ( ( crystalCount + 1 ) < 29 && _.random( 1000 ) < QuestHelper.getAdjustedChance( ENMITY_CRYSTAL, 240, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, ENMITY_CRYSTAL, 1, data.isChampion )
                    }

                    await this.tryToProgressQuestWithCrystals( player, state )
                }

                return

            case CURSED_SEER:
                if ( state.isMemoState( 3 ) && crystalCount < 30 ) {
                    await QuestHelper.rewardSingleQuestItem( player, ENMITY_CRYSTAL, 1, data.isChampion )

                    if ( crystalCount === 29 ) {
                        state.setConditionWithSound( 4, true )
                    } else {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    }


                    if ( ( crystalCount + 1 ) < 29 && _.random( 1000 ) < QuestHelper.getAdjustedChance( ENMITY_CRYSTAL, 40, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, ENMITY_CRYSTAL, 1, data.isChampion )
                    }

                    await this.tryToProgressQuestWithCrystals( player, state )
                }

                return

            case DELU_LIZARDMAN_COMMANDER:
                if ( state.isMemoState( 3 ) && crystalCount < 30 ) {
                    let amountToGive = crystalCount <= 28 ? 2 : 1
                    await QuestHelper.rewardSingleQuestItem( player, ENMITY_CRYSTAL, amountToGive, data.isChampion )

                    if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( ENMITY_CRYSTAL, 220, data.isChampion )
                            && QuestHelper.getQuestItemsCount( player, ENMITY_CRYSTAL ) < 28 ) {
                        await QuestHelper.rewardSingleQuestItem( player, ENMITY_CRYSTAL, 1, data.isChampion )
                    }

                    await this.tryToProgressQuestWithCrystals( player, state )
                }

                return

            case CRIMSON_LADY:
                if ( state.isMemoState( 32 ) ) {
                    await QuestHelper.giveSingleItem( player, RESEARCH_ON_THE_GIANTS_AND_THE_ANCIENT_RACE, 1 )

                    state.setMemoState( 32 )
                    state.setConditionWithSound( 20, true )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getLevel() >= minimumLevel
                        && player.getClassId() === ClassIdValues.warder.id
                        && !QuestHelper.hasQuestItem( player, KAMAEL_INQUISITOR_MARK ) ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 64 )
                        PlayerVariablesManager.set( data.playerId, QuestVariables.secondClassDiamondReward, true )

                        return this.getPath( '32201-07a.htm' )
                    }

                    return this.getPath( '32201-07.htm' )
                }

                return

            case '32201-08.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '30058-03.html':
            case '30058-04.html':
                if ( state.isMemoState( 7 ) ) {
                    break
                }

                return

            case '30058-05.html':
                if ( state.isMemoState( 7 ) ) {
                    state.setMemoState( 8 )
                    state.setConditionWithSound( 7, true )

                    break
                }

                return
            case '30058-08.html':
                if ( state.isMemoState( 9 ) ) {
                    await QuestHelper.giveSingleItem( player, ENCODED_PAGE_ON_THE_ANCIENT_RACE, 1 )
                    state.setMemoState( 10 )
                    state.setConditionWithSound( 9, true )

                    break
                }

                return

            case '30171-03.html':
                if ( state.isMemoState( 23 ) ) {
                    break
                }

                return

            case '30171-05.html':
                if ( state.isMemoState( 23 ) ) {
                    await QuestHelper.takeSingleItem( player, GRANDIS_ATTACK_ORDERS, -1 )
                    state.setMemoState( 24 )

                    break
                }

                return

            case '30171-06.html':
            case '30171-07.html':
                if ( state.isMemoState( 24 ) ) {
                    break
                }

                return

            case '30171-08.html':
                if ( state.isMemoState( 24 ) ) {
                    state.setMemoState( 25 )
                }

                state.setConditionWithSound( 14, true )

                break

            case '30458-03.html':
                if ( state.isMemoState( 5 ) ) {
                    await QuestHelper.takeSingleItem( player, ENMITY_CRYSTAL_CORE, 1 )
                    state.setMemoState( 6 )
                    break
                }

                return

            case '30458-05.html':
            case '30458-06.html':
            case '30458-07.html':
            case '30458-08.html':
                if ( state.isMemoState( 6 ) ) {
                    break
                }

                return

            case '30458-09.html':
                if ( state.isMemoState( 6 ) ) {
                    state.setMemoState( 7 )
                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '30464-03.html':
            case '30464-04.html':
            case '30464-05.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case '30464-06.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '30464-09.html':
                if ( state.isMemoState( 4 ) ) {
                    await QuestHelper.giveSingleItem( player, ENMITY_CRYSTAL_CORE, 1 )
                    state.setMemoState( 5 )
                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '30717-03.html':
            case '30717-05.html':
            case '30717-06.html':
            case '30717-07.html':
            case '30717-08.html':
                if ( state.isMemoState( 28 ) ) {
                    break
                }

                return

            case '30717-09.html':
                if ( state.isMemoState( 28 ) ) {
                    state.setMemoState( 29 )
                    state.setConditionWithSound( 17, true )
                    break
                }

                return

            case '30720-03.html':
                if ( state.isMemoState( 29 ) ) {
                    break
                }

                return

            case '30720-04.html':
                if ( state.isMemoState( 29 ) ) {
                    state.setMemoState( 30 )
                    state.setConditionWithSound( 18, true )
                    break
                }

                return

            case '32214-03.html':
                if ( state.isMemoState( 10 ) ) {
                    break
                }

                return

            case '32214-04.html':
                if ( state.isMemoState( 10 ) ) {
                    await QuestHelper.takeSingleItem( player, ENCODED_PAGE_ON_THE_ANCIENT_RACE, 1 )
                    await QuestHelper.giveSingleItem( player, KAMAEL_INQUISITOR_TRAINEE_MARK, 1 )
                    state.setMemoState( 11 )
                    state.setConditionWithSound( 10, true )

                    break
                }

                return

            case '32220-03.html':
                if ( state.isMemoState( 11 ) ) {
                    await QuestHelper.takeSingleItem( player, KAMAEL_INQUISITOR_TRAINEE_MARK, -1 )
                    state.setMemoState( 12 )

                    break
                }

                return

            case '32220-05.html':
                if ( state.isMemoState( 12 ) ) {
                    state.setMemoState( 13 )
                    break
                }

                return

            case '32220-06.html':
                if ( state.isMemoState( 13 ) ) {
                    state.setMemoStateEx( 1, 0 )
                    break
                }

                return

            case '32220-09.html':
            case '32220-10.html':
                if ( state.isMemoState( 13 ) ) {
                    break
                }

                return

            case '32220-11.html':
            case '32220-12.html':
            case '32220-13.html':
                if ( state.isMemoState( 13 ) ) {
                    state.setMemoState( 13 )
                    state.setMemoStateEx( 1, 1 )
                    break
                }

                return

            case '32220-13a.html':
                if ( state.isMemoState( 13 ) ) {
                    state.setMemoState( 20 )
                    state.setMemoStateEx( 1, 0 )
                    break
                }

                return

            case '32220-13b.html':
                if ( state.isMemoState( 20 ) ) {
                    state.setMemoState( 21 )
                    state.setConditionWithSound( 11, true )
                    break
                }

                return

            case '32220-19.html':
            case '32220-21.html':
            case '32220-22.html':
            case '32220-23.html':
            case '32220-24.html':
            case '32220-25.html':
                if ( state.isMemoState( 31 ) ) {
                    break
                }

                return

            case '32220-26.html':
                if ( state.isMemoState( 31 ) ) {
                    state.setMemoStateEx( 1, 0 )
                    state.setMemoState( 32 )
                    state.setConditionWithSound( 19, true )
                    break
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MASTER_RINDY ) {
                    if ( player.getClassId() === ClassIdValues.warder.id
                            && !QuestHelper.hasQuestItem( player, KAMAEL_INQUISITOR_MARK ) ) {
                        if ( player.getLevel() >= minimumLevel ) {
                            return this.getPath( '32201-01.htm' )
                        }

                        return this.getPath( '32201-02.html' )
                    }

                    return this.getPath( '32201-03.html' )
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case MASTER_RINDY:
                        if ( memoState === 1 ) {
                            state.setMemoState( 2 )
                            state.setConditionWithSound( 2, true )

                            return this.getPath( '32201-09.html' )
                        }

                        if ( memoState === 2 ) {
                            return this.getPath( '32201-10' )
                        }

                        if ( memoState > 2 && memoState < 11 ) {
                            return this.getPath( '32201-11' )
                        }

                        if ( memoState >= 11 ) {
                            return this.getPath( '32201-12' )
                        }

                        break

                    case WAREHOUSE_KEEPER_HOLVAS:
                        if ( memoState < 7 ) {
                            return this.getPath( '30058-01' )
                        }

                        if ( memoState === 7 ) {
                            return this.getPath( '30058-02' )
                        }

                        if ( memoState === 8 ) {
                            if ( QuestHelper.getQuestItemsCount( player, MANUSCRIPT_PAGE ) < 30 ) {
                                return this.getPath( '30058-06' )
                            }

                            await QuestHelper.takeSingleItem( player, MANUSCRIPT_PAGE, -1 )
                            state.setMemoState( 9 )

                            return this.getPath( '30058-07' )
                        }

                        if ( memoState === 9 ) {
                            await QuestHelper.giveSingleItem( player, ENCODED_PAGE_ON_THE_ANCIENT_RACE, 1 )

                            state.setMemoState( 10 )
                            state.setConditionWithSound( 9, true )

                            return this.getPath( '30058-09' )
                        }

                        if ( memoState > 9 ) {
                            return this.getPath( '30058-10' )
                        }

                        break

                    case MAGISTER_GAIUS:
                        if ( memoState < 23 ) {
                            return this.getPath( '30171-01' )
                        }

                        if ( memoState === 23 ) {
                            return this.getPath( '30171-02' )
                        }

                        if ( memoState === 24 ) {
                            return this.getPath( '30171-06' )
                        }

                        if ( memoState === 25 ) {
                            return this.getPath( '30171-09' )
                        }

                        if ( memoState === 26 ) {
                            return this.getPath( '30171-10' )
                        }

                        if ( memoState === 27 ) {
                            return this.getPath( '30171-11' )
                        }

                        if ( memoState === 28 ) {
                            return this.getPath( '30171-12' )
                        }

                        if ( memoState === 29 ) {
                            return this.getPath( '30171-13' )
                        }

                        break

                    case BLACKSMITH_POITAN:
                        if ( memoState < 5 ) {
                            return this.getPath( '30458-01' )
                        }

                        if ( memoState === 5 ) {
                            return this.getPath( '30458-02' )
                        }

                        if ( memoState === 6 ) {
                            return this.getPath( '30458-04' )
                        }

                        if ( memoState === 7 ) {
                            return this.getPath( '30458-10' )
                        }

                        break

                    case MAGISTER_CLAYTON:
                        if ( memoState < 2 ) {
                            return this.getPath( '30464-01' )
                        }

                        if ( memoState === 2 ) {
                            state.setMemoState( 2 )
                            return this.getPath( '30464-02' )
                        }

                        if ( memoState === 3 ) {
                            if ( QuestHelper.getQuestItemsCount( player, ENMITY_CRYSTAL ) < 30 ) {
                                return this.getPath( '30464-07' )
                            }

                            await QuestHelper.takeSingleItem( player, ENMITY_CRYSTAL, -1 )
                            state.setMemoState( 4 )

                            return this.getPath( '30464-08' )
                        }

                        if ( memoState === 4 ) {
                            await QuestHelper.giveSingleItem( player, ENMITY_CRYSTAL_CORE, 1 )

                            state.setMemoState( 5 )
                            state.setConditionWithSound( 5, true )

                            return this.getPath( '30464-10' )
                        }

                        if ( memoState === 5 ) {
                            return this.getPath( '30464-12' )
                        }

                        if ( memoState > 5 ) {
                            return this.getPath( '30464-13' )
                        }

                        break

                    case MAGISTER_GAUEN:
                        if ( memoState < 27 ) {
                            return this.getPath( '30717-01' )
                        }

                        if ( memoState === 27 ) {
                            await QuestHelper.takeMultipleItems( player, MANASHENS_TALISMAN, -1 )
                            state.setMemoState( 28 )

                            return this.getPath( '30717-02' )
                        }

                        if ( memoState === 28 ) {
                            return this.getPath( '30717-04' )
                        }

                        if ( memoState >= 29 ) {
                            return this.getPath( '30717-10' )
                        }

                        break

                    case MAGISTER_KAIENA:
                        if ( memoState < 29 ) {
                            return this.getPath( '30720-01' )
                        }

                        if ( memoState === 29 ) {
                            return this.getPath( '30720-02' )
                        }

                        if ( memoState >= 30 ) {
                            return this.getPath( '30720-05' )
                        }

                        break

                    case GRAND_MASTER_MELDINA:
                        if ( memoState < 10 ) {
                            return this.getPath( '32214-01' )
                        }

                        if ( memoState === 10 ) {
                            return this.getPath( '32214-02' )
                        }

                        if ( memoState === 11 ) {
                            return this.getPath( '32214-05' )
                        }

                        if ( memoState > 11 ) {
                            return this.getPath( '32214-06' )
                        }

                        break

                    case MASTER_SELSIA:
                        if ( memoState < 11 ) {
                            return this.getPath( '32220-01' )
                        }

                        if ( memoState === 11 ) {
                            return this.getPath( '32220-02' )
                        }

                        if ( memoState === 12 ) {
                            return this.getPath( '32220-04' )
                        }

                        if ( memoState === 13 ) {
                            let stateExValue = state.getMemoStateEx( 1 )
                            if ( stateExValue === 0 ) {
                                state.setMemoStateEx( 1, 0 )
                                return this.getPath( '32220-07' )
                            }

                            if ( stateExValue === 1 ) {
                                state.setMemoStateEx( 1, 0 )
                                return this.getPath( '32220-08' )
                            }

                            break
                        }

                        if ( memoState === 20 ) {
                            state.setMemoState( 21 )
                            state.setConditionWithSound( 11, true )
                            return this.getPath( '32220-14' )
                        }

                        if ( memoState === 21 ) {
                            return this.getPath( '32220-15' )
                        }

                        if ( memoState === 22 ) {
                            return this.getPath( '32220-16' )
                        }

                        if ( ( memoState >= 23 ) && ( memoState < 30 ) ) {
                            return this.getPath( '32220-17' )
                        }

                        if ( memoState === 30 ) {
                            state.setMemoState( 31 )
                            return this.getPath( '32220-18' )
                        }

                        if ( memoState === 31 ) {
                            return this.getPath( '32220-20' )
                        }

                        if ( memoState === 32 ) {
                            if ( !QuestHelper.hasQuestItem( player, RESEARCH_ON_THE_GIANTS_AND_THE_ANCIENT_RACE ) ) {
                                return this.getPath( '32220-27' )
                            }

                            await QuestHelper.giveAdena( player, 77666, true )
                            await QuestHelper.giveSingleItem( player, KAMAEL_INQUISITOR_MARK, 1 )
                            await QuestHelper.addExpAndSp( player, 429546, 29476 )

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '32220-28' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MASTER_RINDY ) {
                    if ( player.getClassId() === ClassIdValues.arbalester.id ) {
                        return this.getPath( '32201-05.html' )
                    }

                    return this.getPath( '32201-06.html' )
                }

                return
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async tryToProgressQuest( player: L2PcInstance, state: QuestState ): Promise<void> {
        if ( QuestHelper.getQuestItemsCount( player, MANUSCRIPT_PAGE ) >= 30 ) {
            state.setConditionWithSound( 8, true )

            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async tryToProgressQuestWithCrystals( player: L2PcInstance, state: QuestState ): Promise<void> {
        if ( QuestHelper.getQuestItemsCount( player, ENMITY_CRYSTAL ) >= 30 ) {
            state.setConditionWithSound( 4, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}