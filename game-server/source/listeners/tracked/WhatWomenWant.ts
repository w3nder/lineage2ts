import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const ARUJIEN = 30223
const MIRABEL = 30146
const HERBIEL = 30150
const GREENIS = 30157

const ARUJIENS_LETTER1 = 1092
const ARUJIENS_LETTER2 = 1093
const ARUJIENS_LETTER3 = 1094
const POETRY_BOOK = 689
const GREENIS_LETTER = 693
const EARRING = 113

const minimumLevel = 2

export class WhatWomenWant extends ListenerLogic {
    constructor() {
        super( 'Q00002_WhatWomenWant', 'listeners/tracked/WhatWomenWant.ts' )
        this.questItemIds = [ ARUJIENS_LETTER1, ARUJIENS_LETTER2, ARUJIENS_LETTER3, POETRY_BOOK, GREENIS_LETTER ]
        this.questId = 2
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00002_WhatWomenWant'
    }

    getQuestStartIds(): Array<number> {
        return [ ARUJIEN ]
    }

    getTalkIds(): Array<number> {
        return [ ARUJIEN, MIRABEL, HERBIEL, GREENIS ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = this.getQuestState( data.playerId, false )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30223-04.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, ARUJIENS_LETTER1, 1 )
                break

            case '30223-08.html':
                await QuestHelper.takeSingleItem( player, ARUJIENS_LETTER3, -1 )
                await QuestHelper.giveSingleItem( player, POETRY_BOOK, 1 )
                state.setConditionWithSound( 4, true )
                break

            case '30223-09.html':
                await QuestHelper.giveAdena( player, 450, true )
                await state.exitQuest( false, true )

                player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.DELIVERY_DUTY_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000, null ) )

                await QuestHelper.addExpAndSp( player, 4254, 335 )
                await QuestHelper.giveAdena( player, 1850, true )
                break

            default:
                return null
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = this.getQuestState( data.playerId, true )

        switch ( data.characterNpcId ) {
            case ARUJIEN:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( ( player.getRace() !== Race.ELF ) && ( player.getRace() !== Race.HUMAN ) ) {
                            return this.getPath( '30223-00.htm"' )
                        }

                        return this.getPath( player.getLevel() >= minimumLevel ? '30223-02.htm' : '30223-01.html' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30223-05.html' )

                            case 2:
                                return this.getPath( '30223-06.html' )

                            case 3:
                                return this.getPath( '30223-07.html' )

                            case 4:
                                return this.getPath( '30223-10.html' )

                            case 5:
                                await QuestHelper.rewardSingleItem( player, EARRING, 1 )
                                await state.exitQuest( false, true )

                                player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.DELIVERY_DUTY_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000, null ) )
                                await QuestHelper.addExpAndSp( player, 4254, 335 )
                                await QuestHelper.giveAdena( player, 1850, true )

                                return this.getPath( '30223-11.html' )
                        }
                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                return QuestHelper.getNoQuestMessagePath()

            case MIRABEL:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 1 ) ) {
                        state.setConditionWithSound( 2, true )

                        await QuestHelper.takeSingleItem( player, ARUJIENS_LETTER1, -1 )
                        await QuestHelper.giveSingleItem( player, ARUJIENS_LETTER2, 1 )

                        return this.getPath( '30146-01.html' )
                    }

                    return this.getPath( '30146-02.html' )
                }

                return QuestHelper.getNoQuestMessagePath()

            case HERBIEL:
                if ( state.isStarted() && ( state.getCondition() > 1 ) ) {
                    if ( state.isCondition( 2 ) ) {
                        state.setConditionWithSound( 3, true )

                        await QuestHelper.takeSingleItem( player, ARUJIENS_LETTER2, -1 )
                        await QuestHelper.giveSingleItem( player, ARUJIENS_LETTER3, 1 )

                        return this.getPath( '30150-01.html' )
                    }

                    return this.getPath( '30150-02.html' )
                }

                return QuestHelper.getNoQuestMessagePath()

            case GREENIS:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 4 ) ) {
                        state.setConditionWithSound( 5, true )
                        await QuestHelper.takeMultipleItems( player, POETRY_BOOK, -1 )
                        await QuestHelper.giveSingleItem( player, GREENIS_LETTER, 1 )

                        return this.getPath( '30157-02.html' )
                    }

                    if ( state.isCondition( 5 ) ) {
                        return this.getPath( '30157-03.html' )
                    }

                    return this.getPath( '30157-01.html' )
                }

                return QuestHelper.getNoQuestMessagePath()
        }

        return
    }
}