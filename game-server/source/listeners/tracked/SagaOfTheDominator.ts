import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    31336,
    31624,
    31371,
    31290,
    31636,
    31646,
    31648,
    31653,
    31654,
    31655,
    31656,
    31290,
]

const itemIds: Array<number> = [
    7080,
    7539,
    7081,
    7492,
    7275,
    7306,
    7337,
    7368,
    7399,
    7430,
    7100,
    0,
]

const mobIds: Array<number> = [
    27294,
    27226,
    27262,
]

const spawnLocations: Array<ILocational> = [
    new Location( 162898, -76492, -3096 ),
    new Location( 47429, -56923, -2383 ),
    new Location( 47391, -56929, -2370 ),
]

export class SagaOfTheDominator extends SagaLogic {
    constructor() {
        super( 'Q00077_SagaOfTheDominator', 'listeners/tracked-100/SagaOfTheDominator.ts' )
        this.questId = 77
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 115
    }

    getPreviousClassId(): number {
        return 0x33
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00077_SagaOfTheDominator'
    }
}