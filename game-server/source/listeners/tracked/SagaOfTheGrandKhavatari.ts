import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    31339,
    31624,
    31589,
    31290,
    31637,
    31646,
    31647,
    31652,
    31654,
    31655,
    31659,
    31290,
]

const itemIds: Array<number> = [
    7080,
    7539,
    7081,
    7491,
    7274,
    7305,
    7336,
    7367,
    7398,
    7429,
    7099,
    0,
]

const mobIds: Array<number> = [
    27293,
    27226,
    27284,
]

const spawnLocations: Array<ILocational> = [
    new Location( 161719, -92823, -1893 ),
    new Location( 124355, 82155, -2803 ),
    new Location( 124376, 82127, -2796 ),
]

export class SagaOfTheGrandKhavatari extends SagaLogic {
    constructor() {
        super( 'Q00076_SagaOfTheGrandKhavatari', 'listeners/tracked-100/SagaOfTheGrandKhavatari.ts' )
        this.questId = 76
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 114
    }

    getPreviousClassId(): number {
        return 0x30
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00076_SagaOfTheGrandKhavatari'
    }
}