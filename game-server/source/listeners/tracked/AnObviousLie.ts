import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { createItemDefinition } from '../../gameService/interface/ItemDefinition'

const MAXIMILIAN = 30120
const GENTLER = 30094
const MIKI_THE_CAT = 31706

const ALLIGATOR = 20135

const MAP_OF_GENTLER = 7165
const MEDICINAL_HERB = createItemDefinition( 7166, 20 )
const SPIRIT_ORE = createItemDefinition( 3031, 500 )
const THREAD = createItemDefinition( 1868, 1000 )
const SUEDE = createItemDefinition( 1866, 500 )

const minimumLevel = 45
const rewards = {
    'cat': 6843, // Cat Ears
    'raccoon': 7680, // Raccoon ears
    'rabbit': 7683, // Rabbit ears
}

export class AnObviousLie extends ListenerLogic {

    constructor() {
        super( 'Q00032_AnObviousLie', 'listeners/tracked/AnObviousLie.ts' )
        this.questId = 32
        this.questItemIds = [ MAP_OF_GENTLER, MEDICINAL_HERB.id ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ALLIGATOR ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00032_AnObviousLie'
    }

    getQuestStartIds(): Array<number> {
        return [ MAXIMILIAN ]
    }

    getTalkIds(): Array<number> {
        return [ MAXIMILIAN, GENTLER, MIKI_THE_CAT ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 3, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        await QuestHelper.rewardAndProgressState( player, state, MEDICINAL_HERB.id, 1, MEDICINAL_HERB.count, data.isChampion, 4 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30120-02.html':
                if ( state.isCreated() ) {
                    state.startQuest()
                    return this.getPath( data.eventName )
                }

                break

            case '30094-02.html':
                if ( state.isCondition( 1 ) ) {
                    await QuestHelper.giveSingleItem( player, MAP_OF_GENTLER, 1 )
                    state.setConditionWithSound( 2, true )
                    return this.getPath( data.eventName )
                }

                break

            case '31706-02.html':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, MAP_OF_GENTLER ) ) {
                    await QuestHelper.takeSingleItem( player, MAP_OF_GENTLER, -1 )
                    state.setConditionWithSound( 3, true )
                    return this.getPath( data.eventName )
                }

                break

            case '30094-06.html':
                if ( state.isCondition( 4 ) && QuestHelper.hasQuestItemCount( player, MEDICINAL_HERB.id, MEDICINAL_HERB.count ) ) {
                    await QuestHelper.takeSingleItem( player, MEDICINAL_HERB.id, MEDICINAL_HERB.count )
                    state.setConditionWithSound( 5, true )
                    return this.getPath( data.eventName )
                }

                break

            case '30094-09.html':
                if ( state.isCondition( 5 ) && QuestHelper.hasQuestItemCount( player, SPIRIT_ORE.id, SPIRIT_ORE.count ) ) {
                    await QuestHelper.takeSingleItem( player, SPIRIT_ORE.id, SPIRIT_ORE.count )
                    state.setConditionWithSound( 6, true )
                    return this.getPath( data.eventName )
                }

                break

            case '30094-12.html':
                if ( state.isCondition( 7 ) ) {
                    state.setConditionWithSound( 8, true )
                    return this.getPath( data.eventName )
                }

                break

            case '30094-15.html':
                return this.getPath( data.eventName )

            case '31706-05.html':
                if ( state.isCondition( 6 ) ) {
                    state.setConditionWithSound( 7, true )
                    return this.getPath( data.eventName )
                }

                break

            case 'cat':
            case 'raccoon':
            case 'rabbit':
                if ( state.isCondition( 8 ) && await QuestHelper.removeAllItems( player, [ THREAD, SUEDE ] ) ) {
                    await QuestHelper.rewardSingleItem( player, rewards[ data.eventName ], 1 )
                    await state.exitQuest( false, true )
                    return this.getPath( '30094-16.html' )
                }

                return this.getPath( '30094-17.html' )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case MAXIMILIAN:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30120-01.htm' : '30120-03.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30120-04.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case GENTLER:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30094-01.html' )

                    case 2:
                        return this.getPath( '30094-03.html' )

                    case 4:
                        return this.getPath( QuestHelper.hasItem( player, MEDICINAL_HERB ) ? '30094-04.html' : '30094-05.html' )

                    case 5:
                        return this.getPath( QuestHelper.hasItem( player, SPIRIT_ORE ) ? '30094-07.html' : '30094-08.html' )

                    case 6:
                        return this.getPath( '30094-10.html' )

                    case 7:
                        return this.getPath( '30094-11.html' )

                    case 8:
                        if ( QuestHelper.hasEachItem( player, [ THREAD, SUEDE ] ) ) {
                            return this.getPath( '30094-13.html' )
                        }

                        return this.getPath( '30094-14.html' )
                }

                break

            case MIKI_THE_CAT:
                switch ( state.getCondition() ) {
                    case 2:
                        if ( QuestHelper.hasQuestItem( player, MAP_OF_GENTLER ) ) {
                            return this.getPath( '31706-01.html' )
                        }

                        break

                    case 3:
                    case 4:
                    case 5:
                        return this.getPath( '31706-03.html' )

                    case 6:
                        return this.getPath( '31706-04.html' )

                    case 7:
                        return this.getPath( '31706-06.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}