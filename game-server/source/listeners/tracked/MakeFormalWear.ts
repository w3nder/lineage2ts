import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ALEXIS = 30842
const LEIKAR = 31520
const JEREMY = 31521
const MIST = 31627

const FORMAL_WEAR = 6408
const MYSTERIOUS_CLOTH = 7076
const JEWEL_BOX = 7077
const SEWING_KIT = 7078
const DRESS_SHOES_BOX = 7113
const BOX_OF_COOKIES = 7159
const ICE_WINE = 7160
const SIGNET_RING = 7164

const minimumLevel = 60

export class MakeFormalWear extends ListenerLogic {
    constructor() {
        super( 'Q00037_MakeFormalWear', 'listeners/tracked/MakeFormalWear.ts' )
        this.questId = 37
        this.questItemIds = [ SIGNET_RING, ICE_WINE, BOX_OF_COOKIES ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00037_MakeFormalWear'
    }

    getQuestStartIds(): Array<number> {
        return [ ALEXIS ]
    }

    getTalkIds(): Array<number> {
        return [ ALEXIS ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30842-03.htm':
                state.startQuest()
                break

            case '31520-02.html':
                await QuestHelper.giveSingleItem( player, SIGNET_RING, 1 )
                state.setConditionWithSound( 2, true )
                break

            case '31521-02.html':
                await QuestHelper.giveSingleItem( player, ICE_WINE, 1 )
                state.setConditionWithSound( 3, true )
                break

            case '31627-02.html':
                if ( !QuestHelper.hasQuestItem( player, ICE_WINE ) ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeSingleItem( player, ICE_WINE, 1 )
                state.setConditionWithSound( 4, true )
                break

            case '31521-05.html':
                await QuestHelper.giveSingleItem( player, BOX_OF_COOKIES, 1 )
                state.setConditionWithSound( 5, true )
                break

            case '31520-05.html':
                if ( !QuestHelper.hasQuestItem( player, BOX_OF_COOKIES ) ) {
                    return QuestHelper.getNoQuestMessagePath()
                }

                await QuestHelper.takeSingleItem( player, BOX_OF_COOKIES, 1 )
                state.setConditionWithSound( 6, true )
                break

            case '31520-08.html':
                if ( !QuestHelper.hasQuestItems( player, SEWING_KIT, JEWEL_BOX, MYSTERIOUS_CLOTH ) ) {
                    return this.getPath( '31520-09.html' )
                }

                await QuestHelper.takeSingleItem( player, SEWING_KIT, 1 )
                await QuestHelper.takeSingleItem( player, JEWEL_BOX, 1 )
                await QuestHelper.takeSingleItem( player, MYSTERIOUS_CLOTH, 1 )
                state.setConditionWithSound( 7, true )
                break

            case '31520-12.html':
                if ( !QuestHelper.hasQuestItem( player, DRESS_SHOES_BOX ) ) {
                    return this.getPath( '31520-13.html' )
                }

                await QuestHelper.takeSingleItem( player, DRESS_SHOES_BOX, 1 )
                await QuestHelper.giveSingleItem( player, FORMAL_WEAR, 1 )
                await state.exitQuest( false, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ALEXIS:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30842-01.htm' : '30842-02.html' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30842-04.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getNoQuestMessagePath()
                }

                break

            case LEIKAR:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 1:
                            return this.getPath( '31520-01.html' )

                        case 2:
                            return this.getPath( '31520-03.html' )

                        case 5:
                            return this.getPath( '31520-04.html' )

                        case 6:
                            return this.getPath( QuestHelper.hasQuestItems( player, SEWING_KIT, JEWEL_BOX, MYSTERIOUS_CLOTH ) ? '31520-06.html' : '31520-07.html' )

                        case 7:
                            return this.getPath( QuestHelper.hasQuestItem( player, DRESS_SHOES_BOX ) ? '31520-10.html' : '31520-11.html' )

                    }
                }

                break

            case JEREMY:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 2:
                            return this.getPath( '31521-01.html' )

                        case 3:
                            return this.getPath( '31521-03.html' )

                        case 4:
                            return this.getPath( '31521-04.html' )

                        case 5:
                            return this.getPath( '31521-06.html' )
                    }
                }

                break

            case MIST:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 3:
                            return this.getPath( '31627-01.html' )

                        case 4:
                            return this.getPath( '31627-03.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}