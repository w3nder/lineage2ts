import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const LINNAEUS = 31577
const CRIMSON_DRAKE = 20670

const CRIMSON_DRAKE_HEART = 7624
const FLAMING_FISHING_LURE = 7613
const minimumLevel = 59

export class LinnaeusSpecialBait extends ListenerLogic {
    constructor() {
        super( 'Q00053_LinnaeusSpecialBait', 'listeners/tracked/OFullesSpecialBait.ts' )
        this.questId = 53
        this.questItemIds = [ CRIMSON_DRAKE_HEART ]
    }

    getAttackableKillIds(): Array<number> {
        return [ CRIMSON_DRAKE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00053_LinnaeusSpecialBait'
    }

    getQuestStartIds(): Array<number> {
        return [ LINNAEUS ]
    }

    getTalkIds(): Array<number> {
        return [ LINNAEUS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )

        if ( !player ) {
            return
        }

        if ( QuestHelper.getQuestItemsCount( player, CRIMSON_DRAKE_HEART ) < 100
                && _.random( 100 ) < QuestHelper.getAdjustedChance( CRIMSON_DRAKE_HEART, 33, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, CRIMSON_DRAKE_HEART, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        if ( QuestHelper.getQuestItemsCount( player, CRIMSON_DRAKE_HEART ) >= 100 ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.name )
            state.setConditionWithSound( 2, true )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31577-1.htm':
                state.startQuest()
                break

            case '31577-3.htm':
                let player = L2World.getPlayer( data.playerId )
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, CRIMSON_DRAKE_HEART ) >= 100 ) {
                    await QuestHelper.rewardSingleItem( player, FLAMING_FISHING_LURE, 4 )
                    await state.exitQuest( false, true )

                    break
                }

                return this.getPath( '31577-5.html' )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let player = L2World.getPlayer( data.playerId )
                return this.getPath( player.getLevel() > minimumLevel ? '31577-0.htm' : '31577-0a.html' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 1 ) ? '31577-4.html' : '31577-2.html' )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}