import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ExShowScreenMessage } from '../../gameService/packets/send/ExShowScreenMessage'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

const DARIN = 30048
const ROXXY = 30006
const BAULRO = 30033

const DARINS_LETTER = 687
const ROXXYS_KERCHIEF = 688
const DARINS_RECEIPT = 1079
const BAULROS_POTION = 1080
const NECKLACE_OF_KNOWLEDGE = 906

const minimumLevel = 2

export class LettersOfLove extends ListenerLogic {
    constructor() {
        super( 'Q00001_LettersOfLove', 'listeners/tracked/LettersOfLove.ts' )

        this.questItemIds = [
            DARINS_LETTER,
            ROXXYS_KERCHIEF,
            DARINS_RECEIPT,
            BAULROS_POTION,
        ]

        this.questId = 1
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00001_LettersOfLove'
    }

    getQuestStartIds(): Array<number> {
        return [ DARIN ]
    }

    getTalkIds(): Array<number> {
        return [ DARIN, ROXXY, BAULRO ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30048-03.html':
            case '30048-04.html':
            case '30048-05.html':
                return this.getPath( data.eventName )

            case '30048-06.htm':
                let player = L2World.getPlayer( data.playerId )

                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, DARINS_LETTER, 1 )

                    return this.getPath( data.eventName )
                }

                break
        }

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() < minimumLevel ? '30048-01.html' : '30048-02.html' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        switch ( data.characterNpcId ) {
                            case DARIN:
                                return this.getPath( '30048-07.html' )

                            case ROXXY:
                                if ( QuestHelper.hasQuestItem( player, DARINS_LETTER ) && !QuestHelper.hasQuestItem( player, ROXXYS_KERCHIEF ) ) {
                                    await QuestHelper.takeSingleItem( player, DARINS_LETTER, -1 )
                                    await QuestHelper.giveSingleItem( player, ROXXYS_KERCHIEF, 1 )

                                    state.setConditionWithSound( 2, true )
                                    return this.getPath( '30006-01.html' )
                                }

                                break
                        }

                        break

                    case 2:
                        switch ( data.characterNpcId ) {
                            case DARIN:
                                if ( QuestHelper.hasQuestItems( player, ROXXYS_KERCHIEF ) ) {

                                    await QuestHelper.takeSingleItem( player, ROXXYS_KERCHIEF, -1 )
                                    await QuestHelper.giveSingleItem( player, DARINS_RECEIPT, 1 )
                                    state.setConditionWithSound( 3, true )

                                    return this.getPath( '30048-08.html' )
                                }

                                break

                            case ROXXY:
                                if ( QuestHelper.hasQuestItems( player, ROXXYS_KERCHIEF ) ) {
                                    return this.getPath( '30006-02.html' )
                                }

                                break
                        }

                        return QuestHelper.getNoQuestMessagePath()

                    case 3:
                        switch ( data.characterNpcId ) {
                            case DARIN:
                                if ( QuestHelper.hasAtLeastOneQuestItem( player, DARINS_RECEIPT, BAULROS_POTION ) ) {
                                    return this.getPath( '30048-09.html' )
                                }

                                break

                            case ROXXY:
                                if ( QuestHelper.hasAtLeastOneQuestItem( player, DARINS_RECEIPT, BAULROS_POTION ) ) {
                                    return this.getPath( '30006-03.html' )
                                }

                                break

                            case BAULRO:
                                if ( QuestHelper.hasQuestItems( player, DARINS_RECEIPT ) ) {
                                    await QuestHelper.takeSingleItem( player, DARINS_RECEIPT, -1 )
                                    await QuestHelper.giveSingleItem( player, BAULROS_POTION, 1 )

                                    state.setConditionWithSound( 4, true )
                                    return this.getPath( '30033-01.html' )
                                }

                                if ( QuestHelper.hasQuestItems( player, BAULROS_POTION ) ) {
                                    return this.getPath( '30033-02.html' )
                                }

                                break
                        }

                        return QuestHelper.getNoQuestMessagePath()

                    case 4:
                        switch ( data.characterNpcId ) {
                            case DARIN:
                                player.sendOwnedData( ExShowScreenMessage.fromNpcMessageId( NpcStringIds.DELIVERY_DUTY_COMPLETE_N_GO_FIND_THE_NEWBIE_GUIDE, 2, 5000, null ) )

                                await QuestHelper.rewardSingleItem( player, NECKLACE_OF_KNOWLEDGE, 1 )
                                await QuestHelper.addExpAndSp( player, 5672, 446 )
                                await QuestHelper.giveAdena( player, 2466, false )

                                await state.exitQuest( false, true )

                                return this.getPath( '30048-10.html' )

                            case BAULRO:
                                if ( QuestHelper.hasQuestItems( player, BAULROS_POTION ) ) {
                                    return this.getPath( '30033-02.html' )
                                }

                                break

                            case ROXXY:
                                if ( QuestHelper.hasQuestItems( player, BAULROS_POTION ) ) {
                                    return this.getPath( '30006-03.html' )
                                }

                                break
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}