import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

const npcIds: Array<number> = [
    30849,
    31624,
    31277,
    30849,
    31631,
    31646,
    31647,
    31650,
    31654,
    31655,
    31657,
    31277,
]

const itemIds: Array<number> = [
    7080,
    7534,
    7081,
    7485,
    7268,
    7299,
    7330,
    7361,
    7392,
    7423,
    7093,
    6482,
]

const mobIds: Array<number> = [
    27286,
    27219,
    27278,
]

const spawnLocations: Array<ILocational> = [
    new Location( 191046, -40640, -3042 ),
    new Location( 46087, -36372, -1685 ),
    new Location( 46066, -36396, -1685 ),
]

export class SagaOfThePhoenixKnight extends SagaLogic {
    constructor() {
        super( 'Q00070_SagaOfThePhoenixKnight', 'listeners/tracked-100/SagaOfThePhoenixKnight.ts' )
        this.questId = 70
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId( player: L2PcInstance ): number {
        return 90
    }

    getPreviousClassId( player: L2PcInstance ): number {
        return 0x05
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00070_SagaOfThePhoenixKnight'
    }
}