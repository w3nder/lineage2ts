import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    30175,
    31627,
    31287,
    31287,
    31598,
    31646,
    31649,
    31652,
    31654,
    31655,
    31659,
    31287,
]

const itemIds: Array<number> = [
    7080,
    7531,
    7081,
    7505,
    7288,
    7319,
    7350,
    7381,
    7412,
    7443,
    7084,
    0,
]

const mobIds: Array<number> = [
    27252,
    27239,
    27256,
]

const spawnLocations: Array<ILocational> = [
    new Location( 161719, -92823, -1893 ),
    new Location( 124376, 82127, -2796 ),
    new Location( 124355, 82155, -2803 ),
]

export class SagaOfTheStormScreamer extends SagaLogic {
    constructor() {
        super( 'Q00090_SagaOfTheStormScreamer', 'listeners/tracked/SagaOfTheStormScreamer.ts' )
        this.questId = 90
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 110
    }

    getPreviousClassId(): number {
        return 0x28
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00090_SagaOfTheStormScreamer'
    }
}