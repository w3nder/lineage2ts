import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const HELMUT = 31258
const CADMON = 31296
const NARAN_ASHANUK = 31378

const MUNITIONS_BOX = 7232

const minimumLevel = 74

export class SecretMeetingWithVarkaSilenos extends ListenerLogic {
    constructor() {
        super( 'Q00012_SecretMeetingWithVarkaSilenos', 'listeners/tracked/SecretMeetingWithVarkaSilenos.ts' )
        this.questId = 12
        this.questItemIds = [ MUNITIONS_BOX ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00012_SecretMeetingWithVarkaSilenos'
    }

    getQuestStartIds(): Array<number> {
        return [ CADMON ]
    }

    getTalkIds(): Array<number> {
        return [ CADMON, HELMUT, NARAN_ASHANUK ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return null
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31296-03.htm':
                state.startQuest()
                state.setMemoState( 11 )

                return this.getPath( data.eventName )

            case '31258-02.html':
                await QuestHelper.giveSingleItem( player, MUNITIONS_BOX, 1 )
                state.setMemoState( 21 )
                state.setConditionWithSound( 2, true )

                return this.getPath( data.eventName )

            case '31378-02.html':
                if ( !QuestHelper.hasQuestItems( player, MUNITIONS_BOX ) ) {
                    return this.getPath( '31378-03.html' )
                }

                await QuestHelper.addExpAndSp( player, 233125, 18142 )
                await state.exitQuest( false, true )

                return this.getPath( data.eventName )
        }

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case CADMON:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '31296-01.htm' : '31296-02.html' )

                    case QuestStateValues.STARTED:
                        if ( state.isMemoState( 11 ) ) {
                            return this.getPath( '31296-04.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case HELMUT:
                if ( state.isStarted() ) {
                    if ( state.isMemoState( 11 ) ) {
                        return this.getPath( '31258-01.html' )
                    }

                    if ( state.isMemoState( 21 ) ) {
                        return this.getPath( '31258-03.html' )
                    }
                }

                break

            case NARAN_ASHANUK:
                if ( state.isStarted() && QuestHelper.hasQuestItems( player, MUNITIONS_BOX ) && state.isMemoState( 21 ) ) {
                    return this.getPath( '31378-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}