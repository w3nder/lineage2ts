import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    30176,
    31627,
    31282,
    31282,
    31590,
    31646,
    31647,
    31650,
    31654,
    31655,
    31657,
    31282
]

const itemIds : Array<number> = [
    7080,
    7529,
    7081,
    7503,
    7286,
    7317,
    7348,
    7379,
    7410,
    7441,
    7082,
    0
]

const mobIds : Array<number> = [
    27250,
    27237,
    27254
]

const spawnLocations : Array<ILocational> = [
    new Location( 191046, -40640, -3042 ),
    new Location( 46066, -36396, -1685 ),
    new Location( 46087, -36372, -1685 )
]

export class SagaOfTheArchmage extends SagaLogic {
    constructor() {
        super( 'Q00088_SagaOfTheArchmage', 'listeners/tracked/SagaOfTheArchmage.ts' )
        this.questId = 88
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 94
    }

    getPreviousClassId(): number {
        return 0x0c
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00088_SagaOfTheArchmage'
    }
}