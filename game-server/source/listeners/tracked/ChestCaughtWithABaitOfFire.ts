import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const LINNAEUS = 31577
const RUKAL = 30629

const treasureBox = 6511
const score = 7628
const necklace = 916
const minimumLevel = 61

export class ChestCaughtWithABaitOfFire extends ListenerLogic {
    constructor() {
        super( 'Q00030_ChestCaughtWithABaitOfFire', 'listeners/tracked/ChestCaughtWithABaitOfFire.ts' )
        this.questId = 30
        this.questItemIds = [ treasureBox, score ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00030_ChestCaughtWithABaitOfFire'
    }

    getQuestStartIds(): Array<number> {
        return [ LINNAEUS ]
    }

    getTalkIds(): Array<number> {
        return [ LINNAEUS, RUKAL ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31577-02.htm':
                state.startQuest()
                break

            case '31577-04a.htm':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, treasureBox ) ) {
                    await QuestHelper.giveSingleItem( player, score, 1 )
                    await QuestHelper.takeSingleItem( player, treasureBox, 1 )
                    state.setConditionWithSound( 2, true )

                    return this.getPath( '31577-04.htm' )
                }

                break

            case '30629-02.htm':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, score ) ) {
                    await QuestHelper.rewardSingleItem( player, necklace, 1 )
                    await state.exitQuest( false, true )

                    return this.getPath( '30629-03.htm' )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                return QuestHelper.getNoQuestMessagePath()

            case QuestStateValues.CREATED:
                if ( data.characterNpcId === LINNAEUS ) {
                    if ( player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00053_LinnaeusSpecialBait' ) ) {
                        return this.getPath( '31577-01.htm' )
                    }

                    return this.getPath( '31577-00.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case LINNAEUS:
                        switch ( state.getCondition() ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, treasureBox ) ) {
                                    return this.getPath( '31577-03.htm' )
                                }

                                return this.getPath( '31577-03a.htm' )

                            case 2:
                                return this.getPath( '31577-05.htm' )
                        }

                        break

                    case RUKAL:
                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '30629-01.htm' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}