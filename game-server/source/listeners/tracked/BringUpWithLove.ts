import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

const npcIds: Array<number> = [
    31537,
]

const WATER_CRYSTAL = 9553
const INNOCENCE_JEWEL = 15533
const minimumLevel = 82

export class BringUpWithLove extends ListenerLogic {
    constructor() {
        super( 'Q00020_BringUpWithLove', 'listeners/tracked/BringUpWithLove.ts' )
        this.questId = 20
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00020_BringUpWithLove'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31537-11.html':
                state.startQuest()
                break

            case '31537-16.html': {
                let player = L2World.getPlayer( data.playerId )
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, INNOCENCE_JEWEL ) ) {
                    await QuestHelper.giveSingleItem( player, WATER_CRYSTAL, 1 )
                    await QuestHelper.takeSingleItem( player, INNOCENCE_JEWEL, -1 )

                    await state.exitQuest( false, true )
                    break
                }

                return
            }
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31537-01.htm' : '31537-13.html' )

            case QuestStateValues.STARTED:
                if ( state.getCondition() === 1 ) {
                    return this.getPath( '31537-14.html' )
                }

                if ( state.getCondition() === 2 ) {
                    return this.getPath( QuestHelper.hasQuestItem( player, INNOCENCE_JEWEL ) ? '31537-14.html' : '31537-15.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}