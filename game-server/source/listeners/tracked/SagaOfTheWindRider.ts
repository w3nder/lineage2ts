import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    31603,
    31624,
    31284,
    31615,
    31612,
    31646,
    31648,
    31652,
    31654,
    31655,
    31659,
    31616
]

const itemIds : Array<number> = [
    7080,
    7517,
    7081,
    7495,
    7278,
    7309,
    7340,
    7371,
    7402,
    7433,
    7103,
    0
]

const mobIds : Array<number> = [
    27300,
    27229,
    27303
]

const spawnLocations : Array<ILocational> = [
    new Location( 161719, -92823, -1893 ),
    new Location( 124314, 82155, -2803 ),
    new Location( 124355, 82155, -2803 )
]

export class SagaOfTheWindRider extends SagaLogic {
    constructor() {
        super( 'Q00080_SagaOfTheWindRider', 'listeners/tracked-100/SagaOfTheWindRider.ts' )
        this.questId = 80
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 101
    }

    getPreviousClassId(): number {
        return 0x17
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00080_SagaOfTheWindRider'
    }
}