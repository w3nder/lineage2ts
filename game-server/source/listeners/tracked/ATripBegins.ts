import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const MIRABEL = 30146
const ARIEL = 30148
const ASTERIOS = 30154

const ARIELS_RECOMMENDATION = 7572
const SCROLL_OF_ESCAPE_GIRAN = 7559
const MARK_OF_TRAVELER = 7570

const minimumLevel = 3

export class ATripBegins extends ListenerLogic {
    constructor() {
        super( 'Q00007_ATripBegins', 'listeners/tracked/ATripBegins.ts' )
        this.questItemIds = [ ARIELS_RECOMMENDATION ]
        this.questId = 7
    }

    getQuestStartIds(): Array<number> {
        return [ MIRABEL ]
    }

    getTalkIds(): Array<number> {
        return [ MIRABEL, ARIEL, ASTERIOS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00007_ATripBegins'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        switch ( data.eventName ) {
            case '30146-03.htm':
                state.startQuest()
                break

            case '30146-06.html':
                await QuestHelper.rewardSingleItem( player, SCROLL_OF_ESCAPE_GIRAN, 1 )
                await QuestHelper.giveSingleItem( player, MARK_OF_TRAVELER, 1 )
                await state.exitQuest( false, true )
                break

            case '30148-02.html':
                state.setConditionWithSound( 2, true )
                await QuestHelper.giveSingleItem( player, ARIELS_RECOMMENDATION, 1 )
                break

            case '30154-02.html':
                if ( !QuestHelper.hasQuestItems( player, ARIELS_RECOMMENDATION ) ) {
                    return this.getPath( '30154-03.html' )
                }

                await QuestHelper.giveSingleItem( player, ARIELS_RECOMMENDATION, -1 )
                state.setConditionWithSound( 3, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case MIRABEL:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( ( player.getRace() === Race.ELF ) && ( player.getLevel() >= minimumLevel ) ? '30146-01.htm' : '30146-02.html' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30146-04.html' )
                        }

                        if ( state.isCondition( 3 ) ) {
                            return this.getPath( '30146-05.html' )
                        }
                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case ARIEL:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 1 ) ) {
                        return this.getPath( '30148-01.html' )
                    }

                    if ( state.isCondition( 2 ) ) {
                        return this.getPath( '30148-03.html' )
                    }
                }

                break

            case ASTERIOS:
                if ( state.isStarted() ) {
                    if ( state.isCondition( 2 ) ) {
                        return this.getPath( '30154-01.html' )
                    }

                    if ( state.isCondition( 3 ) ) {
                        return this.getPath( '30154-04.html' )
                    }
                }

                break
        }
    }
}