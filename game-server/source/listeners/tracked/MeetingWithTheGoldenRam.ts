import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const DONAL = 31314
const DAISY = 31315
const ABERCROMBIE = 31555

const BOX = 7245

export class MeetingWithTheGoldenRam extends ListenerLogic {
    constructor() {
        super( 'Q00018_MeetingWithTheGoldenRam', 'listeners/tracked/MeetingWithTheGoldenRam.ts' )
        this.questId = 18
        this.questItemIds = [ BOX ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00018_MeetingWithTheGoldenRam'
    }

    getQuestStartIds(): Array<number> {
        return [ DONAL ]
    }

    getTalkIds(): Array<number> {
        return [ DONAL, DAISY, ABERCROMBIE ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, false )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31314-03.html':
                if ( player.getLevel() < 66 ) {
                    return this.getPath( '31314-02.html' )
                }

                state.startQuest()
                break

            case '31315-02.html':
                state.setConditionWithSound( 2, true )
                await QuestHelper.giveSingleItem( player, BOX, 1 )
                break

            case '31555-02.html':
                if ( QuestHelper.hasQuestItems( player, BOX ) ) {
                    await QuestHelper.giveAdena( player, 40000, true )
                    await QuestHelper.addExpAndSp( player, 126668, 11731 )
                    await state.exitQuest( false, true )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === DONAL ) {
                    return this.getPath( '31314-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case DONAL:
                        return this.getPath( '31314-04.html' )

                    case DAISY:
                        return this.getPath( state.getCondition() < 2 ? '31315-01.html' : '31315-03.html' )

                    case ABERCROMBIE:
                        if ( state.isCondition( 2 ) && QuestHelper.hasQuestItems( player, BOX ) ) {
                            return this.getPath( '31555-01.html' )
                        }
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}