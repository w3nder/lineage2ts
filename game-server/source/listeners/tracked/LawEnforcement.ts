import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { DataManager } from '../../data/manager'
import { ClassIdValues } from '../../gameService/models/base/ClassId'

const LIANE = 32222
const KEKROPUS = 32138
const EINDBURGH = 32469

const minimumLevel = 76

export class LawEnforcement extends ListenerLogic {
    constructor() {
        super( 'Q00061_LawEnforcement', 'listeners/tracked/LawEnforcement.ts' )
        this.questId = 61
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00061_LawEnforcement'
    }

    getQuestStartIds(): Array<number> {
        return [ LIANE ]
    }

    getTalkIds(): Array<number> {
        return [ LIANE, KEKROPUS, EINDBURGH ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32222-02.htm':
                return this.getPath( data.eventName )

            case '32222-03.htm':
                state.setMemoState( 1 )
                state.startQuest()
                return this.getPath( data.eventName )

            case '32138-01.html':
            case '32138-02.html':
                if ( state.isMemoState( 1 ) ) {
                    return this.getPath( data.eventName )
                }

                return

            case '32138-03.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )

                    return this.getPath( data.eventName )
                }

                return

            case '32138-04.html':
            case '32138-05.html':
            case '32138-06.html':
            case '32138-07.html':
                if ( state.isMemoState( 2 ) || state.isMemoState( 3 ) ) {
                    return this.getPath( data.eventName )
                }

                return

            case '32138-08.html':
                if ( state.isMemoState( 2 ) || state.isMemoState( 3 ) ) {
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 2, true )

                    return this.getPath( data.eventName )
                }

                return

            case '32138-09.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 3 )
                    return this.getPath( data.eventName )
                }

                return

            case '32469-02.html':
                if ( state.isMemoState( 4 ) ) {
                    state.setMemoState( 5 )
                    return this.getPath( data.eventName )
                }

                return

            case '32469-03.html':
            case '32469-04.html':
            case '32469-05.html':
            case '32469-06.html':
            case '32469-07.html':
                if ( state.isMemoState( 5 ) ) {
                    return this.getPath( data.eventName )
                }

                return

            case '32469-08.html':
            case '32469-09.html':
                if ( state.isMemoState( 5 ) ) {
                    let player = L2World.getPlayer( data.playerId )

                    await player.setClassId( 136 )
                    player.broadcastUserInfo()

                    await QuestHelper.giveAdena( player, 26000, true )
                    await state.exitQuest( false, true )

                    return this.getPath( data.eventName )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )

        switch ( state.getState() ) {
            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case LIANE:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32222-06.html' )
                        }

                        break

                    case KEKROPUS:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '32138-01.html' )

                            case 2:
                                return this.getPath( '32138-03.html' )

                            case 3:
                                return this.getPath( '32138-10.html' )

                            case 4:
                                return this.getPath( '32138-10.html' )
                        }

                        break

                    case EINDBURGH:
                        if ( state.isMemoState( 4 ) ) {
                            let player = L2World.getPlayer( data.playerId )
                            return DataManager.getHtmlData().getItem( this.getPath( '32469-01.html' ) )
                                    .replace( '%name%', player.getName() )
                        }

                        if ( state.isMemoState( 5 ) ) {
                            return this.getPath( '32469-02.html' )
                        }

                        break
                }

                break

            case QuestStateValues.CREATED:
                let player = L2World.getPlayer( data.playerId )
                if ( player.getLevel() >= minimumLevel ) {

                    if ( player.getClassId() === ClassIdValues.inspector.id ) {
                        return DataManager.getHtmlData().getItem( this.getPath( '32222-01.htm' ) )
                                .replace( '%name%', player.getName() )
                    }

                    return this.getPath( '32222-04.htm' )
                }

                return this.getPath( '32222-05.htm' )

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId == LIANE ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}