import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    30832,
    31623,
    31279,
    31279,
    31645,
    31646,
    31648,
    31650,
    31654,
    31655,
    31657,
    31279,
]

const itemIds: Array<number> = [
    7080,
    7533,
    7081,
    7509,
    7292,
    7323,
    7354,
    7385,
    7416,
    7447,
    7085,
    0,
]

const mobIds: Array<number> = [
    27257,
    27243,
    27265,
]

const spawnLocations: Array<ILocational> = [
    new Location( 191046, -40640, -3042 ),
    new Location( 46066, -36396, -1685 ),
    new Location( 46087, -36372, -1685 ),
]

export class SagaOfTheSoultaker extends SagaLogic {
    constructor() {
        super( 'Q00094_SagaOfTheSoultaker', 'listeners/tracked/SagaOfTheSoultaker.ts' )
        this.questId = 94
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 95
    }

    getPreviousClassId(): number {
        return 0x0d
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00094_SagaOfTheSoultaker'
    }
}