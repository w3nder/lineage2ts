import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    30191,
    31626,
    31588,
    31280,
    31591,
    31646,
    31648,
    31652,
    31654,
    31655,
    31659,
    31280
]

const itemIds : Array<number> = [
    7080,
    7523,
    7081,
    7501,
    7284,
    7315,
    7346,
    7377,
    7408,
    7439,
    7089,
    0
]

const mobIds : Array<number> = [
    27269,
    27235,
    27275
]

const spawnLocations : Array<ILocational> = [
    new Location( 161719, -92823, -1893 ),
    new Location( 124355, 82155, -2803 ),
    new Location( 124376, 82127, -2796 )
]

export class SagaOfTheHierophant extends SagaLogic {
    constructor() {
        super( 'Q00086_SagaOfTheHierophant', 'listeners/tracked/SagaOfTheHierophant.ts' )
        this.questId = 86
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 98
    }

    getPreviousClassId(): number {
        return 0x11
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00086_SagaOfTheHierophant'
    }
}