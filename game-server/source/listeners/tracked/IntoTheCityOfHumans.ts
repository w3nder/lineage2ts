import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

const PETUKAI = 30583
const TANAPI = 30571
const TAMIL = 30576
// Items
const SCROLL_OF_ESCAPE_GIRAN = 7559
const MARK_OF_TRAVELER = 7570
// Misc
const minimumLevel = 3

export class IntoTheCityOfHumans extends ListenerLogic {
    constructor() {
        super( 'Q00009_IntoTheCityOfHumans', 'listeners/tracked/IntoTheCityOfHumans.ts' )
        this.questId = 9
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00009_IntoTheCityOfHumans'
    }

    getQuestStartIds(): Array<number> {
        return [ PETUKAI ]
    }

    getTalkIds(): Array<number> {
        return [ PETUKAI, TANAPI, TAMIL ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return null
        }

        let player = L2World.getPlayer( data.playerId )
        switch ( data.eventName ) {
            case '30583-04.htm':
                state.startQuest()
                break

            case '30576-02.html':
                await QuestHelper.giveSingleItem( player, MARK_OF_TRAVELER, 1 )
                await QuestHelper.rewardSingleItem( player, SCROLL_OF_ESCAPE_GIRAN, 1 )
                await state.exitQuest( false, true )
                break

            case '30571-02.html':
                state.setConditionWithSound( 2, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case PETUKAI:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() >= minimumLevel ) {
                            return this.getPath( player.getRace() === Race.ORC ? '30583-01.htm' : '30583-02.html' )
                        }

                        return this.getPath( '30583-03.html' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '30583-05.html' )
                        }
                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case TANAPI:
                if ( state.isStarted() ) {
                    return this.getPath( state.isCondition( 1 ) ? '30571-01.html' : '30571-03.html' )
                }

                break

            case TAMIL:
                if ( state.isStarted() && state.isCondition( 2 ) ) {
                    return this.getPath( '30576-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}