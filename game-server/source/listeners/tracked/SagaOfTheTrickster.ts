import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

const npcIds : Array<number> = [
    32138,
    31270,
    31282,
    32228,
    32263,
    31646,
    31649,
    31653,
    31654,
    31655,
    31659,
    31283
]

const itemIds : Array<number> = [
    7080,
    9761,
    7081,
    9742,
    9724,
    9727,
    9730,
    9733,
    9736,
    9739,
    9718,
    0
]

const mobIds : Array<number> = [
    27333,
    27334,
    27335
]

const spawnLocations : Array<ILocational> = [
    new Location( 164014, -74733, -3093 ),
    new Location( 124355, 82155, -2803 ),
    new Location( 124376, 82127, -2796 )
]

const textLines : Array<string> = [
    `${SagaPlaceholders.playerName}! Pursued to here! However, I jumped out of the Banshouren boundaries! You look at the giant as the sign of power!`,
    '... Oh ... good! So it was ... let\'s begin!',
    'I do not have the patience ..! I have been a giant force ...! Cough chatter ah ah ah!',
    `Paying homage to those who disrupt the orderly will be ${SagaPlaceholders.playerName}'s death!`,
    'Now, my soul freed from the shackles of the millennium, Halixia, to the back side I come ...',
    'Why do you interfere others\' battles?',
    'This is a waste of time.. Say goodbye...!',
    '...That is the enemy',
    `...Goodness! ${SagaPlaceholders.playerName} you are still looking?`,
    `${SagaPlaceholders.playerName} ... Not just to whom the victory. Only personnel involved in the fighting are eligible to share in the victory.`,
    `Your sword is not an ornament. Don't you think, ${SagaPlaceholders.playerName}?`,
    'Goodness! I no longer sense a battle there now.',
    'let...',
    'Only engaged in the battle to bar their choice. Perhaps you should regret.',
    'The human nation was foolish to try and fight a giant\'s strength.',
    'Must...Retreat... Too...Strong.',
    `${SagaPlaceholders.playerName}. Defeat...by...retaining...and...Mo...Hacker`,
    '....! Fight...Defeat...It...Fight...Defeat...It...'
]

export class SagaOfTheTrickster extends SagaLogic {
    constructor() {
        super( 'Q00069_SagaOfTheTrickster', 'listeners/tracked-100/SagaOfTheTrickster.ts' )
        this.questId = 69
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId( player: L2PcInstance ): number {
        return 134
    }

    getPreviousClassId( player: L2PcInstance ): number {
        return 0x82
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00069_SagaOfTheTrickster'
    }
}