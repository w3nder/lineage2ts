import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'
import { NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'

const npcIds: Array<number> = [
    30849,
    31624,
    31226,
    31331,
    31639,
    31646,
    31647,
    31653,
    31654,
    31655,
    31656,
    31277,
]

const itemIds: Array<number> = [
    7080,
    7537,
    7081,
    7488,
    7271,
    7302,
    7333,
    7364,
    7395,
    7426,
    7096,
    7546,
]

const mobIds: Array<number> = [
    27289,
    27222,
    27281,
]

const spawnLocations: Array<ILocational> = [
    new Location( 164650, -74121, -2871 ),
    new Location( 47429, -56923, -2383 ),
    new Location( 47391, -56929, -2370 ),
]

const TUNATUN = 31537
const TOPQUALITYMEAT = 7546

export class SagaOfTheDuelist extends SagaLogic {
    constructor() {
        super( 'Q00073_SagaOfTheDuelist', 'listeners/tracked-100/SagaOfTheDuelist.ts' )
        this.questId = 73
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 88
    }

    getPreviousClassId(): number {
        return 0x02
    }

    getTalkIds(): Array<number> {
        return [
            ...super.getTalkIds(),
            TUNATUN,
        ]
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        if ( data.characterNpcId === TUNATUN ) {
            let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )
            if ( !state || !state.isCondition( 2 ) ) {
                return QuestHelper.getNoQuestMessagePath()
            }

            let player = L2World.getPlayer( data.playerId )
            if ( !QuestHelper.hasQuestItem( player, TOPQUALITYMEAT ) ) {
                await QuestHelper.giveSingleItem( player, TOPQUALITYMEAT, 1 )

                return this.getPath( 'tunatun_01.htm' )
            }

            return this.getPath( 'tunatun_02.htm' )
        }

        return super.onTalkEvent( data )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00073_SagaOfTheDuelist'
    }
}