import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    30191,
    31626,
    31588,
    31280,
    31644,
    31646,
    31647,
    31651,
    31654,
    31655,
    31658,
    31280
]

const itemIds : Array<number> = [
    7080,
    7522,
    7081,
    7500,
    7283,
    7314,
    7345,
    7376,
    7407,
    7438,
    7087,
    0
]

const mobIds : Array<number> = [
    27267,
    27234,
    27274
]

const spawnLocations : Array<ILocational> = [
    new Location( 119518, -28658, -3811 ),
    new Location( 181215, 36676, -4812 ),
    new Location( 181227, 36703, -4816 )
]

export class SagaOfTheCardinal extends SagaLogic {
    constructor() {
        super( 'Q00085_SagaOfTheCardinal', 'listeners/tracked/SagaOfTheCardinal.ts' )
        this.questId = 85
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 97
    }

    getPreviousClassId(): number {
        return 0x10
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00085_SagaOfTheCardinal'
    }
}