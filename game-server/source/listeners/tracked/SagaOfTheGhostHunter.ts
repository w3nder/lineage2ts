import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    31603,
    31624,
    31286,
    31615,
    31617,
    31646,
    31649,
    31653,
    31654,
    31655,
    31656,
    31616
]

const itemIds : Array<number> = [
    7080,
    7518,
    7081,
    7496,
    7279,
    7310,
    7341,
    7372,
    7403,
    7434,
    7104,
    0
]

const mobIds : Array<number> = [
    27301,
    27230,
    27304
]

const spawnLocations : Array<ILocational> = [
    new Location( 164650, -74121, -2871 ),
    new Location( 47391, -56929, -2370 ),
    new Location( 47429, -56923, -2383 )
]

export class SagaOfTheGhostHunter extends SagaLogic {
    constructor() {
        super( 'Q00081_SagaOfTheGhostHunter', 'listeners/tracked-100/SagaOfTheGhostHunter.ts' )
        this.questId = 81
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 108
    }

    getPreviousClassId(): number {
        return 0x24
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00081_SagaOfTheGhostHunter'
    }
}