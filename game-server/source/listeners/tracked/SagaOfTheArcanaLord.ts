import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    31605,
    31622,
    31585,
    31608,
    31586,
    31646,
    31647,
    31651,
    31654,
    31655,
    31658,
    31608,
]

const itemIds: Array<number> = [
    7080,
    7604,
    7081,
    7506,
    7289,
    7320,
    7351,
    7382,
    7413,
    7444,
    7110,
    0,
]

const mobIds: Array<number> = [
    27313,
    27240,
    27310,
]

const spawnLocations: Array<ILocational> = [
    new Location( 119518, -28658, -3811 ),
    new Location( 181215, 36676, -4812 ),
    new Location( 181227, 36703, -4816 ),
]

export class SagaOfTheArcanaLord extends SagaLogic {
    constructor() {
        super( 'Q00091_SagaOfTheArcanaLord', 'listeners/tracked/SagaOfTheArcanaLord.ts' )
        this.questId = 91
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 96
    }

    getPreviousClassId(): number {
        return 0x0e
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00091_SagaOfTheArcanaLord'
    }
}