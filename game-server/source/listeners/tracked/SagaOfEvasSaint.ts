import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    30191,
    31626,
    31588,
    31280,
    31620,
    31646,
    31649,
    31653,
    31654,
    31655,
    31657,
    31280
]

const itemIds : Array<number> = [
    7080,
    7524,
    7081,
    7502,
    7285,
    7316,
    7347,
    7378,
    7409,
    7440,
    7088,
    0
]

const mobIds : Array<number> = [
    27266,
    27236,
    27276
]

const spawnLocations : Array<ILocational> = [
    new Location( 164650, -74121, -2871 ),
    new Location( 46087, -36372, -1685 ),
    new Location( 46066, -36396, -1685 )
]

export class SagaOfEvasSaint extends SagaLogic {
    constructor() {
        super( 'Q00087_SagaOfEvasSaint', 'listeners/tracked/SagaOfEvasSaint.ts' )
        this.questId = 87
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 105
    }

    getPreviousClassId(): number {
        return 0x1e
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00087_SagaOfEvasSaint'
    }
}