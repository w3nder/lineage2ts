import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const OFULLE = 31572
const FETTERED_SOUL = 20552

const LOST_BAIT = 7622
const ICY_AIR_LURE = 7611
const minimumLevel = 36

export class OFullesSpecialBait extends ListenerLogic {
    constructor() {
        super( 'Q00051_OFullesSpecialBait', 'listeners/tracked/OFullesSpecialBait.ts' )
        this.questId = 51
        this.questItemIds = [ LOST_BAIT ]
    }

    getAttackableKillIds(): Array<number> {
        return [ FETTERED_SOUL ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00051_OFullesSpecialBait'
    }

    getQuestStartIds(): Array<number> {
        return [ OFULLE ]
    }

    getTalkIds(): Array<number> {
        return [ OFULLE ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )

        if ( !player ) {
            return
        }

        if ( QuestHelper.getQuestItemsCount( player, LOST_BAIT ) < 100
                && _.random( 100 ) < QuestHelper.getAdjustedChance( LOST_BAIT, 33, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, LOST_BAIT, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        if ( QuestHelper.getQuestItemsCount( player, LOST_BAIT ) >= 100 ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.name )
            state.setConditionWithSound( 2, true )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31572-03.htm':
                state.startQuest()
                break

            case '31572-07.html':
                let player = L2World.getPlayer( data.playerId )
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, LOST_BAIT ) >= 100 ) {
                    await QuestHelper.rewardSingleItem( player, ICY_AIR_LURE, 4 )
                    await state.exitQuest( false, true )

                    return this.getPath( '31572-06.htm' )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let player = L2World.getPlayer( data.playerId )
                return this.getPath( player.getLevel() >= minimumLevel ? '31572-01.htm' : '31572-02.html' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 1 ) ? '31572-05.html' : '31572-04.html' )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}