import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'

const MASTER_SIONE = 32195
const MASTER_GOBIE = 32198
const MASTER_TOBIAS = 30297
const CAPTAIN_BATHIS = 30332

const ORDERS = 9762
const ORGANIZATION_CHART = 9763
const GOBIES_ORDERS = 9764
const LETTER_TO_HUMANS = 9765
const HUMANS_REOLY = 9766
const LETTER_TO_THE_DARKELVES = 9767
const DARK_ELVES_REPLY = 9768
const REPORT_TO_SIONE = 9769
const EMPTY_SOUL_CRYSTAL = 9770
const TAKS_CAPTURED_SOUL = 9771

const STEELRAZOR_EVALUTION = 9772

const OL_MAHUM_PATROL = 20053
const OL_MAHUM_NOVICE = 20782
const MAILLE_LIZARDMAN = 20919
const MAILLE_LIZARDMAN_SCOUT = 20920
const MAILLE_LIZARDMAN_GUARD = 20921

const OL_MAHUM_OFFICER_TAK = 27337

const minimumLevel = 18
const lizardmanCounter = 'count'

export class PathOfTheWarder extends ListenerLogic {
    constructor() {
        super( 'Q00063_PathOfTheWarder', 'listeners/tracked/PathOfTheWarder.ts' )
        this.questId = 63
        this.questItemIds = [
            ORDERS,
            ORGANIZATION_CHART,
            GOBIES_ORDERS,
            LETTER_TO_HUMANS,
            HUMANS_REOLY,
            LETTER_TO_THE_DARKELVES,
            DARK_ELVES_REPLY,
            REPORT_TO_SIONE,
            EMPTY_SOUL_CRYSTAL,
            TAKS_CAPTURED_SOUL,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            OL_MAHUM_PATROL,
            OL_MAHUM_NOVICE,
            MAILLE_LIZARDMAN,
            MAILLE_LIZARDMAN_SCOUT,
            MAILLE_LIZARDMAN_GUARD,
            OL_MAHUM_OFFICER_TAK,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00063_PathOfTheWarder'
    }

    getQuestStartIds(): Array<number> {
        return [ MASTER_SIONE ]
    }

    getTalkIds(): Array<number> {
        return [ MASTER_SIONE, MASTER_GOBIE, MASTER_TOBIAS, CAPTAIN_BATHIS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case OL_MAHUM_PATROL:
                if ( state.isMemoState( 2 ) && QuestHelper.getQuestItemsCount( player, ORGANIZATION_CHART ) < 5 ) {
                    await QuestHelper.rewardSingleQuestItem( player, ORGANIZATION_CHART, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, ORDERS ) >= 10
                            && QuestHelper.getQuestItemsCount( player, ORGANIZATION_CHART ) >= 5 ) {
                        state.setConditionWithSound( 3, true )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case OL_MAHUM_NOVICE:
                if ( state.isMemoState( 2 ) && QuestHelper.getQuestItemsCount( player, ORDERS ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, ORDERS, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, ORDERS ) >= 10
                            && QuestHelper.getQuestItemsCount( player, ORGANIZATION_CHART ) >= 5 ) {
                        state.setConditionWithSound( 3, true )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case MAILLE_LIZARDMAN:
            case MAILLE_LIZARDMAN_SCOUT:
            case MAILLE_LIZARDMAN_GUARD:
                if ( state.isMemoState( 16 ) && !QuestHelper.hasQuestItem( player, TAKS_CAPTURED_SOUL ) ) {
                    let count: number = state.getVariable( lizardmanCounter )
                    if ( count < 4 ) {
                        state.setVariable( lizardmanCounter, count + 1 )
                        return
                    }

                    state.setVariable( lizardmanCounter, 0 )
                    let killedNpc = L2World.getObjectById( data.targetId )
                    let npc: L2Npc = QuestHelper.addSpawnAtLocation( OL_MAHUM_OFFICER_TAK, killedNpc, true, 0 )

                    await AIEffectHelper.notifyAttackedWithTargetId( npc, data.playerId, 1, 1 )
                }

                return

            case OL_MAHUM_OFFICER_TAK:
                if ( state.isMemoState( 16 ) && !QuestHelper.hasQuestItem( player, TAKS_CAPTURED_SOUL ) ) {
                    await QuestHelper.takeSingleItem( player, EMPTY_SOUL_CRYSTAL, 1 )
                    await QuestHelper.giveSingleItem( player, TAKS_CAPTURED_SOUL, 1 )
                    state.setConditionWithSound( 12, true )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    return this.getPath( '32195-05.htm' )
                }

                return

            case '32195-06.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case '32195-08.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '32198-03.html':
                if ( state.isMemoState( 3 ) ) {
                    await QuestHelper.takeSingleItem( player, GOBIES_ORDERS, 1 )
                    await QuestHelper.giveSingleItem( player, LETTER_TO_HUMANS, 1 )
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            case '32198-07.html':
                if ( state.isMemoState( 7 ) ) {
                    break
                }

                return

            case '32198-08.html':
                if ( state.isMemoState( 7 ) ) {
                    await QuestHelper.giveSingleItem( player, LETTER_TO_THE_DARKELVES, 1 )
                    state.setMemoState( 8 )
                    state.setConditionWithSound( 7, true )

                    break
                }

                return

            case '32198-12.html':
                if ( state.isMemoState( 12 ) ) {
                    await QuestHelper.giveSingleItem( player, REPORT_TO_SIONE, 1 )
                    state.setMemoState( 13 )
                    state.setConditionWithSound( 9, true )

                    break
                }

                return

            case '32198-15.html':
                if ( state.isMemoState( 14 ) ) {
                    state.setMemoState( 15 )

                    break
                }

                return

            case '32198-16.html':
                if ( state.isMemoState( 15 ) ) {
                    await QuestHelper.giveSingleItem( player, EMPTY_SOUL_CRYSTAL, 1 )
                    state.setMemoState( 16 )
                    state.setConditionWithSound( 11, true )

                    break
                }

                return

            case '30332-03.html':
                if ( state.isMemoState( 4 ) ) {
                    await QuestHelper.takeSingleItem( player, LETTER_TO_HUMANS, 1 )
                    await QuestHelper.giveSingleItem( player, HUMANS_REOLY, 1 )
                    state.setMemoState( 5 )

                    break
                }

                return

            case '30332-05.html':
                if ( state.isMemoState( 5 ) ) {
                    break
                }

                return

            case '30332-06.html':
                if ( state.isMemoState( 5 ) ) {
                    state.setMemoState( 6 )
                    state.setConditionWithSound( 6, true )

                    break
                }

                return

            case '30297-03.html':
                if ( state.isMemoState( 9 ) ) {
                    break
                }

                return

            case '30297-04.html':
                if ( state.isMemoState( 9 ) ) {
                    state.setMemoState( 10 )

                    break
                }

                return

            case '30297-06.html':
                if ( state.isMemoState( 10 ) ) {
                    await QuestHelper.giveSingleItem( player, DARK_ELVES_REPLY, 1 )
                    state.setMemoState( 11 )
                    state.setConditionWithSound( 8, true )

                    break
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== MASTER_SIONE ) {
                    break
                }

                if ( ( player.getClassId() === ClassIdValues.femaleSoldier.id ) && !QuestHelper.hasQuestItem( player, STEELRAZOR_EVALUTION ) ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        return this.getPath( '32195-01.htm' )
                    }

                    return this.getPath( '32195-02.html' )
                }

                if ( QuestHelper.hasQuestItem( player, STEELRAZOR_EVALUTION ) ) {
                    return this.getPath( '32195-03.html' )
                }

                return this.getPath( '32195-04.html' )

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case MASTER_SIONE:
                        if ( memoState === 1 ) {
                            return this.getPath( '32195-07.html' )
                        }

                        if ( memoState === 2 ) {
                            if ( QuestHelper.getQuestItemsCount( player, ORDERS ) < 10
                                    || QuestHelper.getQuestItemsCount( player, ORGANIZATION_CHART ) < 5 ) {
                                return this.getPath( '32195-09.html' )
                            }

                            await QuestHelper.takeSingleItem( player, ORDERS, -1 )
                            await QuestHelper.takeSingleItem( player, ORGANIZATION_CHART, -1 )
                            await QuestHelper.giveSingleItem( player, GOBIES_ORDERS, 1 )

                            state.setMemoState( 3 )
                            state.setConditionWithSound( 4, true )

                            return this.getPath( '32195-10.html' )
                        }

                        if ( memoState === 3 ) {
                            return this.getPath( '32195-11.html' )
                        }

                        if ( memoState === 13 ) {
                            await QuestHelper.takeSingleItem( player, REPORT_TO_SIONE, 1 )

                            state.setMemoState( 14 )
                            state.setConditionWithSound( 10, true )

                            return this.getPath( '32195-13.html' )
                        }

                        if ( memoState > 3 ) {
                            return this.getPath( '32195-12.html' )
                        }

                        break

                    case MASTER_GOBIE:
                        if ( memoState < 3 ) {
                            return this.getPath( '32198-01.html' )
                        }

                        if ( memoState === 3 ) {
                            return this.getPath( '32198-02.html' )
                        }

                        if ( memoState === 4 || memoState === 5 ) {
                            return this.getPath( '32198-04.html' )
                        }

                        if ( memoState === 6 ) {
                            await QuestHelper.takeSingleItem( player, HUMANS_REOLY, 1 )
                            state.setMemoState( 7 )

                            return this.getPath( '32198-05.html' )
                        }

                        if ( memoState === 7 ) {
                            return this.getPath( '32198-06.html' )
                        }

                        if ( memoState === 8 ) {
                            return this.getPath( '32198-09.html' )
                        }

                        if ( memoState === 11 ) {
                            await QuestHelper.takeSingleItem( player, DARK_ELVES_REPLY, 1 )
                            state.setMemoState( 12 )

                            return this.getPath( '32198-10.html' )
                        }

                        if ( memoState === 12 ) {
                            await QuestHelper.giveSingleItem( player, REPORT_TO_SIONE, 1 )
                            state.setMemoState( 13 )

                            return this.getPath( '32198-11.html' )
                        }

                        if ( memoState === 13 ) {
                            return this.getPath( '32198-13.html' )
                        }

                        if ( memoState === 14 ) {
                            return this.getPath( '32198-14.html' )
                        }

                        if ( memoState === 15 ) {
                            await QuestHelper.giveSingleItem( player, EMPTY_SOUL_CRYSTAL, 1 )

                            state.setMemoState( 16 )
                            state.setVariable( lizardmanCounter, 0 )
                            state.setConditionWithSound( 11, true )

                            return this.getPath( '32198-17.html' )
                        }

                        if ( memoState === 16 ) {
                            if ( !QuestHelper.hasQuestItem( player, TAKS_CAPTURED_SOUL ) ) {
                                state.setVariable( lizardmanCounter, 0 )

                                return this.getPath( '32198-18.html' )
                            }

                            await QuestHelper.giveAdena( player, 163800, true )
                            await QuestHelper.takeSingleItem( player, TAKS_CAPTURED_SOUL, 1 )
                            await QuestHelper.giveSingleItem( player, STEELRAZOR_EVALUTION, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 22046 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 28744 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 591724, 35442 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )


                            return this.getPath( '32198-19.html' )
                        }

                        break

                    case CAPTAIN_BATHIS:
                        if ( memoState < 4 ) {
                            return this.getPath( '30332-02.html' )
                        }

                        if ( memoState === 4 ) {
                            return this.getPath( '30332-01.html' )
                        }

                        if ( memoState === 5 ) {
                            return this.getPath( '30332-04.html' )
                        }

                        if ( memoState === 6 ) {
                            return this.getPath( '30332-07.html' )
                        }

                        if ( memoState > 6 ) {
                            return this.getPath( '30332-08.html' )
                        }

                        break

                    case MASTER_TOBIAS:
                        if ( memoState === 8 && QuestHelper.hasQuestItem( player, LETTER_TO_THE_DARKELVES ) ) {
                            await QuestHelper.takeSingleItem( player, LETTER_TO_THE_DARKELVES, 1 )
                            state.setMemoState( 9 )

                            return this.getPath( '30297-01.html' )
                        }

                        if ( memoState === 9 ) {
                            return this.getPath( '30297-02.html' )
                        }

                        if ( memoState === 10 ) {
                            await QuestHelper.giveSingleItem( player, DARK_ELVES_REPLY, 1 )
                            state.setMemoState( 11 )
                            state.setConditionWithSound( 8, true )

                            return this.getPath( '30297-05.html' )
                        }

                        if ( memoState >= 11 ) {
                            return this.getPath( '30297-07.html' )
                        }

                        break

                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MASTER_GOBIE ) {
                    if ( QuestHelper.hasQuestItem( player, STEELRAZOR_EVALUTION ) ) {
                        return this.getPath( '32198-20.html' )
                    }
                }

                break

        }


        return QuestHelper.getNoQuestMessagePath()
    }
}