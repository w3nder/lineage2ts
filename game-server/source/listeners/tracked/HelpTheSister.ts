import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const COOPER = 30829
const GALLADUCCI = 30097

const SPECTER = 20171
const SORROW_MAIDEN = 20197

const CRAFTED_DAGGER = 220
const MAP_PIECE = 7550
const MAP = 7551
const PET_TICKET = 7584

const minimumLevel = 26

export class HelpTheSister extends ListenerLogic {
    constructor() {
        super( 'Q00043_HelpTheSister', 'listeners/tracked/HelpTheSister.ts' )
        this.questId = 43
        this.questItemIds = [ MAP, MAP_PIECE ]
    }

    getAttackableKillIds(): Array<number> {
        return [ SORROW_MAIDEN, SPECTER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00043_HelpTheSister'
    }

    getQuestStartIds(): Array<number> {
        return [ COOPER ]
    }

    getTalkIds(): Array<number> {
        return [ COOPER, GALLADUCCI ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( !state || !state.isCondition( 2 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, MAP_PIECE, 1, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, MAP_PIECE ) >= 30 ) {
            state.setConditionWithSound( 3, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30829-01.htm':
                state.startQuest()
                break

            case '30829-03.html':
                if ( QuestHelper.hasQuestItem( player, CRAFTED_DAGGER ) ) {
                    await QuestHelper.takeSingleItem( player, CRAFTED_DAGGER, 1 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return QuestHelper.getNoQuestMessagePath()

            case '30829-06.html':
                if ( QuestHelper.getQuestItemsCount( player, MAP_PIECE ) >= 30 ) {
                    await QuestHelper.takeSingleItem( player, MAP_PIECE, -1 )
                    await QuestHelper.giveSingleItem( player, MAP, 1 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return this.getPath( '30829-06a.html' )

            case '30097-02.html':
                if ( QuestHelper.hasQuestItem( player, MAP ) ) {
                    await QuestHelper.takeSingleItem( player, MAP, -1 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return this.getPath( '30097-02a.html' )

            case '30829-09.html':
                await QuestHelper.giveSingleItem( player, PET_TICKET, 1 )
                await state.exitQuest( false, true )
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case COOPER:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30829-00.htm' : '30829-00a.html' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( QuestHelper.hasQuestItem( player, CRAFTED_DAGGER ) ? '30829-02.html' : '30829-02a.html' )

                            case 2:
                                return this.getPath( '30829-04.html' )

                            case 3:
                                return this.getPath( '30829-05.html' )

                            case 4:
                                return this.getPath( '30829-07.html' )

                            case 5:
                                return this.getPath( '30829-08.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case GALLADUCCI:
                if ( state.isStarted() ) {
                    switch ( state.getCondition() ) {
                        case 4:
                            return this.getPath( '30097-01.html' )
                        case 5:
                            return this.getPath( '30097-03.html' )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}