import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import {
    EventType,
    NpcGeneralEvent,
    NpcRouteFinishedEvent,
    NpcSeePlayerEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const INNOCENTIN = 31328
const AGRIPEL = 31348
const BENEDICT = 31349
const DOMINIC = 31350
const MYSTERIOUS_WIZARD = 31522
const TOMBSTONE = 31523
const GHOST_OF_VON_HELLMAN = 31524
const GHOST_OF_VON_HELLMANS_PAGE = 31525
const BROKEN_BOOKSHELF = 31526

const GHOST_LOC = new Location( 51432, -54570, -3136, 0 )
const PAGE_LOC = new Location( 51446, -54514, -3136, 0 )

const CROSS_OF_EINHASAD = 7140
const CROSS_OF_EINHASAD2 = 7141
const minimumLevel = 63
const walkingRouteName = 'rune_ghost1b'

const variableNames = {
    AGRIPEL: 'a',
    DOMINIC: 'd',
    BENEDICT: 'b',
}

const eventNames = {
    despawnGhost: 'despawnGhost',
    despawnPage: 'despawnPage',
}

export class HiddenTruth extends ListenerLogic {
    isGhostSpawned: boolean = false
    isRouteFinished: boolean = false
    ghostPageCounter: number = 0
    isPageSpawned: boolean = false

    constructor() {
        super( 'Q00021_HiddenTruth', 'listeners/tracked/HiddenTruth.ts' )
        this.questItemIds = [ CROSS_OF_EINHASAD ]
        this.questId = 21
    }

    computeCommonValues( player: L2PcInstance, state: QuestState, setVariableName: string ): string {
        if ( QuestHelper.hasQuestItems( player, CROSS_OF_EINHASAD ) && state.isCondition( 6 ) ) {
            state.setVariable( setVariableName, 1 )

            if ( state.getVariable( variableNames.AGRIPEL ) === 1
                    && state.getVariable( variableNames.DOMINIC ) === 1
                    && state.getVariable( variableNames.BENEDICT ) === 1 ) {

                state.setConditionWithSound( 7, false )
                return this.getPath( '31348-03.html' )
            }

            if ( state.getVariable( variableNames.DOMINIC ) === 1 || state.getVariable( variableNames.BENEDICT ) === 1 ) {
                return this.getPath( '31348-02.html' )
            }

            return this.getPath( '31348-01.html' )
        }

        if ( state.isCondition( 7 ) ) {
            return this.getPath( '31348-03.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getNpcSeePlayerIds(): Array<number> {
        return [ GHOST_OF_VON_HELLMANS_PAGE ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00021_HiddenTruth'
    }

    getQuestStartIds(): Array<number> {
        return [ MYSTERIOUS_WIZARD ]
    }

    getRouteFinishedIds(): Array<number> {
        return [ GHOST_OF_VON_HELLMANS_PAGE ]
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcRouteFinished,
                method: this.onNpcRouteFinished.bind( this ),
                ids: [
                    GHOST_OF_VON_HELLMANS_PAGE
                ]
            }
        ]
    }

    getTalkIds(): Array<number> {
        return [ MYSTERIOUS_WIZARD, TOMBSTONE, GHOST_OF_VON_HELLMAN, GHOST_OF_VON_HELLMANS_PAGE, BROKEN_BOOKSHELF, AGRIPEL, BENEDICT, DOMINIC, INNOCENTIN ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        switch ( data.eventName ) {
            case '31328-02.html':
            case '31328-03.html':
            case '31328-04.html':
            case '31522-01.htm':
            case '31522-04.html':
            case '31523-02.html':
            case '31524-02.html':
            case '31524-03.html':
            case '31524-04.html':
            case '31524-05.html':
            case '31526-01.html':
            case '31526-02.html':
            case '31526-04.html':
            case '31526-05.html':
            case '31526-06.html':
            case '31526-12.html':
            case '31526-13.html':
                return this.getPath( data.eventName )

            case '31328-05.html':
                if ( state.isCondition( 7 ) ) {
                    await QuestHelper.giveSingleItem( player, CROSS_OF_EINHASAD2, 1 )
                    await player.addExpAndSp( 131228, 11978 )
                    await state.exitQuest( false, true )
                    return this.getPath( data.eventName )
                }

                return

            case '31522-02.htm':
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31522-03.htm' )
                }

                state.startQuest()
                return this.getPath( data.eventName )

            case '31523-03.html':
                if ( this.isGhostSpawned ) {
                    player.sendCopyData( SoundPacket.SKILLSOUND_HORROR_2 )
                    return this.getPath( '31523-04.html' )
                }

                let ghost: L2Npc = QuestHelper.addSpawnAtLocation( GHOST_OF_VON_HELLMAN, GHOST_LOC, false, 0 )
                BroadcastHelper.broadcastNpcSayStringId( ghost, NpcSayType.All, NpcStringIds.WHO_AWOKE_ME )

                this.isGhostSpawned = true
                this.startQuestTimer( eventNames.despawnGhost, GeneralHelper.minutesToMillis( 5 ), ghost.getObjectId() )
                state.setConditionWithSound( 2, false )
                player.sendCopyData( SoundPacket.SKILLSOUND_HORROR_2 )

                return this.getPath( data.eventName )

            case '31524-06.html':
                if ( this.ghostPageCounter < 5 ) {
                    let page: L2Npc = QuestHelper.addSpawnAtLocation( GHOST_OF_VON_HELLMANS_PAGE, PAGE_LOC, false, 0 )
                    await page.startWalkingRoute( walkingRouteName )

                    NpcVariablesManager.set( page.getObjectId(), this.name, data.playerId )
                    BroadcastHelper.broadcastNpcSayStringId( page, NpcSayType.NpcAll, NpcStringIds.MY_MASTER_HAS_INSTRUCTED_ME_TO_BE_YOUR_GUIDE_S1, player.getName() )

                    this.ghostPageCounter++
                    state.setConditionWithSound( 3, false )
                    return this.getPath( data.eventName )
                }

                return this.getPath( '31524-06a.html' )

            case '31526-03.html':
                player.sendCopyData( SoundPacket.ITEMSOUND_ARMOR_CLOTH )
                return this.getPath( data.eventName )

            case '31526-07.html':
                state.setConditionWithSound( 4, false )
                return this.getPath( data.eventName )

            case '31526-08.html':
                if ( !state.isCondition( 5 ) ) {
                    player.sendCopyData( SoundPacket.AMDSOUND_ED_CHIMES )
                    state.setConditionWithSound( 5, false )
                    return this.getPath( data.eventName )
                }

                return this.getPath( '31526-09.html' )

            case '31526-14.html':
                await QuestHelper.giveSingleItem( player, CROSS_OF_EINHASAD, 1 )
                state.setConditionWithSound( 6, false )
                return this.getPath( '31526-09.html' )

            case eventNames.despawnGhost:
                let ghostNpc = L2World.getObjectById( data.characterId ) as L2Npc
                await ghostNpc.deleteMe()
                this.isGhostSpawned = false

                return

            case eventNames.despawnPage:
                this.ghostPageCounter--
                let pageNpc = L2World.getObjectById( data.characterId ) as L2Npc
                await pageNpc.deleteMe()

                if ( this.ghostPageCounter === 0 ) {
                    this.isRouteFinished = false
                    this.isPageSpawned = false
                }

                return
        }
    }

    async onNpcRouteFinished( data: NpcRouteFinishedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( NpcVariablesManager.get( data.characterId, this.getName() ) as number, this.getName() )
        if ( state ) {
            this.startQuestTimer( eventNames.despawnPage, 15000, data.characterId )
            this.isRouteFinished = true
        }
    }

    async onNpcSeePlayerEvent( data: NpcSeePlayerEvent ): Promise<void> {
        PacketDispatcher.sendCopyData( data.playerId, SoundPacket.HORROR_01 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case MYSTERIOUS_WIZARD:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( '31522-01.htm' )

                    case QuestStateValues.STARTED:
                        return this.getPath( '31522-05.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case TOMBSTONE:
                return this.getPath( '31523-01.html' )

            case GHOST_OF_VON_HELLMAN:
                switch ( state.getCondition() ) {
                    case 2:
                        return this.getPath( '31524-01.html' )

                    case 3:
                        if ( this.isPageSpawned ) {
                            return this.getPath( '31524-07b.html' )
                        }

                        if ( this.ghostPageCounter < 5 ) {
                            let page: L2Npc = QuestHelper.addSpawnAtLocation( GHOST_OF_VON_HELLMANS_PAGE, PAGE_LOC, false, 0 )
                            await page.startWalkingRoute( walkingRouteName )

                            NpcVariablesManager.set( page.getObjectId(), this.name, data.playerId )
                            this.isPageSpawned = true

                            return this.getPath( '31524-07.html' )
                        }

                        return this.getPath( '31524-07a.html' )

                    case 4:
                        return this.getPath( '31524-07c.html' )
                }

                break

            case GHOST_OF_VON_HELLMANS_PAGE:
                if ( state.isCondition( 3 ) ) {
                    if ( this.isRouteFinished ) {
                        this.startQuestTimer( eventNames.despawnPage, 3000, data.characterId )
                        return this.getPath( '31525-02.html' )
                    }

                    return this.getPath( '31525-01.html' )
                }

                break

            case BROKEN_BOOKSHELF:
                switch ( state.getCondition() ) {
                    case 3:
                        return this.getPath( '31526-01.html' )

                    case 4:
                        state.setConditionWithSound( 5, false )
                        player.sendCopyData( SoundPacket.AMDSOUND_ED_CHIMES )
                        return this.getPath( '31526-10.html' )

                    case 5:
                        return this.getPath( '31526-11.html' )

                    case 6:
                        return this.getPath( '31526-15.html' )
                }

                break

            case AGRIPEL:
                return this.computeCommonValues( player, state, variableNames.AGRIPEL )

            case BENEDICT:
                return this.computeCommonValues( player, state, variableNames.BENEDICT )

            case DOMINIC:
                return this.computeCommonValues( player, state, variableNames.DOMINIC )

            case INNOCENTIN: {
                if ( state.isCondition( 7 ) && QuestHelper.hasQuestItems( player, CROSS_OF_EINHASAD ) ) {
                    return this.getPath( '31328-01.html' )
                }

                if ( state.isCompleted() && !QuestStateCache.getQuestState( data.playerId, 'Q00022_TragedyInVonHellmannForest' ) ) {
                    return this.getPath( '31328-06.html' )
                }

                break
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}