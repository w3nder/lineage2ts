import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    30174,
    31627,
    31283,
    31283,
    31643,
    31646,
    31648,
    31651,
    31654,
    31655,
    31658,
    31283,
]

const itemIds: Array<number> = [
    7080,
    7530,
    7081,
    7504,
    7287,
    7318,
    7349,
    7380,
    7411,
    7442,
    7083,
    0,
]

const mobIds: Array<number> = [
    27251,
    27238,
    27255,
]

const spawnLocations: Array<ILocational> = [
    new Location( 119518, -28658, -3811 ),
    new Location( 181227, 36703, -4816 ),
    new Location( 181215, 36676, -4812 ),
]

export class SagaOfTheMysticMuse extends SagaLogic {
    constructor() {
        super( 'Q00089_SagaOfTheMysticMuse', 'listeners/tracked/SagaOfTheMysticMuse.ts' )
        this.questId = 89
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 103
    }

    getPreviousClassId(): number {
        return 0x1b
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00089_SagaOfTheMysticMuse'
    }
}