import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const VLADIMIR = 31302
const HIERARCH = 31517
const M_NECROMANCER = 31518

export class SweetWhispers extends ListenerLogic {
    constructor() {
        super( 'Q00015_SweetWhispers', 'listeners/tracked/SweetWhispers.ts' )
        this.questId = 15
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00015_SweetWhispers'
    }

    getQuestStartIds(): Array<number> {
        return [ VLADIMIR ]
    }

    getTalkIds(): Array<number> {
        return [ VLADIMIR, HIERARCH, M_NECROMANCER ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, false )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31302-01.html':
                state.startQuest()
                break

            case '31518-01.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, false )
                }

                break

            case '31517-01.html':
                if ( state.isCondition( 2 ) ) {
                    let player = L2World.getPlayer( data.playerId )
                    await QuestHelper.addExpAndSp( player, 350531, 28204 )
                    await state.exitQuest( false, true )
                }
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === VLADIMIR ) {
                    let player = L2World.getPlayer( data.playerId )
                    return this.getPath( player.getLevel() >= 60 ? '31302-00.htm' : '31302-00a.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case VLADIMIR:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '31302-01a.html' )
                        }

                        break

                    case M_NECROMANCER:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '31518-00.html' )
                        }

                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '31518-01a.html' )
                        }

                        break

                    case HIERARCH:
                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '31517-00.html' )
                        }

                        break
                }
                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}