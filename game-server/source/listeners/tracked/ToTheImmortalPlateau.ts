import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const GALLADUCCI = 30097
const GENTLER = 30094
const SANDRA = 30090
const DUSTIN = 30116

const MARK_OF_TRAVELER = 7570
const GALLADUCCIS_ORDER_1 = 7563
const GALLADUCCIS_ORDER_2 = 7564
const GALLADUCCIS_ORDER_3 = 7565
const PURIFIED_MAGIC_NECKLACE = 7566
const GEMSTONE_POWDER = 7567
const MAGIC_SWORD_HILT = 7568

const minimumLevel = 3
const SCROLL_OF_ESCAPE_ORC_VILLAGE = 7557
const itemMap = {
    [ GENTLER ]: [ 1, GALLADUCCIS_ORDER_1 ], // condition state, itemId
    [ SANDRA ]: [ 3, GALLADUCCIS_ORDER_2 ],
    [ DUSTIN ]: [ 5, GALLADUCCIS_ORDER_3 ],
}

export class ToTheImmortalPlateau extends ListenerLogic {
    constructor() {
        super( 'Q00048_ToTheImmortalPlateau', 'listeners/tracked/ToTheImmortalPlateau.ts' )
        this.questId = 48
        this.questItemIds = [ GALLADUCCIS_ORDER_1, GALLADUCCIS_ORDER_2, GALLADUCCIS_ORDER_3, PURIFIED_MAGIC_NECKLACE, GEMSTONE_POWDER, MAGIC_SWORD_HILT ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00048_ToTheImmortalPlateau'
    }

    getQuestStartIds(): Array<number> {
        return [ GALLADUCCI ]
    }

    getTalkIds(): Array<number> {
        return [ GALLADUCCI, GENTLER, SANDRA, DUSTIN ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30097-04.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, GALLADUCCIS_ORDER_1, 1 )

                    break
                }

                return

            case '30094-02.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, GALLADUCCIS_ORDER_1 ) ) {
                    await QuestHelper.takeSingleItem( player, GALLADUCCIS_ORDER_1, 1 )
                    await QuestHelper.giveSingleItem( player, MAGIC_SWORD_HILT, 1 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return this.getPath( '30094-03.html' )

            case '30097-07.html':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, MAGIC_SWORD_HILT ) ) {
                    await QuestHelper.takeSingleItem( player, MAGIC_SWORD_HILT, 1 )
                    await QuestHelper.giveSingleItem( player, GALLADUCCIS_ORDER_2, 1 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return this.getPath( '30097-08.html' )

            case '30090-02.html':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItem( player, GALLADUCCIS_ORDER_2 ) ) {
                    await QuestHelper.takeSingleItem( player, GALLADUCCIS_ORDER_2, 1 )
                    await QuestHelper.giveSingleItem( player, GEMSTONE_POWDER, 1 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return this.getPath( '30090-03.html' )

            case '30097-11.html':
                if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, GEMSTONE_POWDER ) ) {
                    await QuestHelper.takeSingleItem( player, GEMSTONE_POWDER, 1 )
                    await QuestHelper.giveSingleItem( player, GALLADUCCIS_ORDER_3, 1 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return this.getPath( '30097-12.html' )

            case '30116-02.html':
                if ( state.isCondition( 5 ) && QuestHelper.hasQuestItem( player, GALLADUCCIS_ORDER_3 ) ) {
                    await QuestHelper.takeSingleItem( player, GALLADUCCIS_ORDER_3, 1 )
                    await QuestHelper.giveSingleItem( player, PURIFIED_MAGIC_NECKLACE, 1 )
                    state.setConditionWithSound( 6, true )

                    break
                }

                return this.getPath( '30116-03.html' )

            case '30097-15.html':
                if ( state.isCondition( 6 ) && QuestHelper.hasQuestItem( player, PURIFIED_MAGIC_NECKLACE ) ) {
                    await QuestHelper.rewardSingleItem( player, SCROLL_OF_ESCAPE_ORC_VILLAGE, 1 )
                    await state.exitQuest( false, true )

                    break
                }

                return this.getPath( '30097-16.html' )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case GALLADUCCI:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '30097-03.html' )
                        }

                        if ( player.hasQuestCompleted( 'Q00009_IntoTheCityOfHumans' ) && QuestHelper.hasQuestItem( player, MARK_OF_TRAVELER ) ) {
                            return this.getPath( '30097-01.htm' )
                        }

                        return this.getPath( '30097-02.html' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30097-05.html' )

                            case 2:
                                if ( QuestHelper.hasQuestItem( player, MAGIC_SWORD_HILT ) ) {
                                    return this.getPath( '30097-06.html' )
                                }

                                break

                            case 3:
                                return this.getPath( '30097-09.html' )

                            case 4:
                                if ( QuestHelper.hasQuestItem( player, GEMSTONE_POWDER ) ) {
                                    return this.getPath( '30097-10.html' )
                                }

                                break

                            case 5:
                                return this.getPath( '30097-13.html' )

                            case 6:
                                if ( QuestHelper.hasQuestItem( player, PURIFIED_MAGIC_NECKLACE ) ) {
                                    return this.getPath( '30097-14.html' )
                                }

                                break
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case GENTLER:
            case SANDRA:
            case DUSTIN:
                if ( state.isStarted() ) {
                    let [ conditionValue, itemId ] = itemMap[ data.characterNpcId ]
                    if ( state.isCondition( conditionValue ) ) {
                        if ( QuestHelper.hasQuestItem( player, itemId ) ) {
                            return this.getPath( `${ data.characterNpcId }-01.html` )
                        }

                        break
                    }

                    if ( state.isCondition( conditionValue + 1 ) ) {
                        return this.getPath( `${ data.characterNpcId }-04.html` )
                    }
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}