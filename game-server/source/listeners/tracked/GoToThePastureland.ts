import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const VLADIMIR = 31302
const TUNATUN = 31537

const VEAL = 15532
const YOUNG_WILD_BEAST_MEAT = 7547

export class GoToThePastureland extends ListenerLogic {
    constructor() {
        super( 'Q00019_GoToThePastureland', 'listeners/tracked/GoToThePastureland.ts' )
        this.questId = 19
        this.questItemIds = [ VEAL, YOUNG_WILD_BEAST_MEAT ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00019_GoToThePastureland'
    }

    getQuestStartIds(): Array<number> {
        return [ VLADIMIR ]
    }

    getTalkIds(): Array<number> {
        return [ VLADIMIR, TUNATUN ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, false )
        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName.toLowerCase() ) {
            case '31302-02.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, VEAL, 1 )
                break

            case '31537-02.html':
                if ( QuestHelper.hasQuestItems( player, YOUNG_WILD_BEAST_MEAT ) ) {
                    await QuestHelper.giveAdena( player, 50000, true )
                    await QuestHelper.addExpAndSp( player, 136766, 12688 )
                    await state.exitQuest( false, true )

                    return this.getPath( '31537-02.html' )
                }

                if ( QuestHelper.hasQuestItems( player, VEAL ) ) {
                    await QuestHelper.giveAdena( player, 147200, true )
                    await QuestHelper.addExpAndSp( player, 385040, 75250 )
                    await state.exitQuest( false, true )

                    return this.getPath( '31537-02.html' )
                }

                return this.getPath( '31537-03.html' )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case VLADIMIR:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getLevel() >= 82 ) {
                            return this.getPath( '31302-01.htm' )
                        }

                        return this.getPath( '31302-03.html' )

                    case QuestStateValues.STARTED:
                        return this.getPath( '31302-04.html' )

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case TUNATUN:
                if ( state.isCondition( 1 ) ) {
                    return this.getPath( '31537-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}