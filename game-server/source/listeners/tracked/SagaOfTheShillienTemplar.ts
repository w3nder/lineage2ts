import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    31580,
    31623,
    31285,
    31285,
    31610,
    31646,
    31648,
    31652,
    31654,
    31655,
    31659,
    31285,
]

const itemIds: Array<number> = [
    7080,
    7526,
    7081,
    7512,
    7295,
    7326,
    7357,
    7388,
    7419,
    7450,
    7091,
    0,
]

const mobIds: Array<number> = [
    27271,
    27246,
    27273,
]

const spawnLocations: Array<ILocational> = [
    new Location( 161719, -92823, -1893 ),
    new Location( 124355, 82155, -2803 ),
    new Location( 124376, 82127, -2796 ),
]

export class SagaOfTheShillienTemplar extends SagaLogic {
    constructor() {
        super( 'Q00097_SagaOfTheShillienTemplar', 'listeners/tracked/SagaOfTheShillienTemplar.ts' )
        this.questId = 97
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 106
    }

    getPreviousClassId(): number {
        return 0x21
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00097_SagaOfTheShillienTemplar'
    }
}