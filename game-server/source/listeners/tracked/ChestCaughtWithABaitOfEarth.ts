import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const WILLIE = 31574
const ANABEL = 30909

const treasureBox = 6507
const glassBox = 7627
const gloves = 2455
const minimumLevel = 48

export class ChestCaughtWithABaitOfEarth extends ListenerLogic {
    constructor() {
        super( 'Q00029_ChestCaughtWithABaitOfEarth', 'listeners/tracked/ChestCaughtWithABaitOfEarth.ts' )
        this.questId = 29
        this.questItemIds = [ glassBox, treasureBox ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00029_ChestCaughtWithABaitOfEarth'
    }

    getQuestStartIds(): Array<number> {
        return [ WILLIE ]
    }

    getTalkIds(): Array<number> {
        return [ WILLIE, ANABEL ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31574-04.htm':
                state.startQuest()
                break

            case '31574-08.htm':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, treasureBox ) ) {
                    await QuestHelper.giveSingleItem( player, glassBox, 1 )
                    await QuestHelper.takeSingleItem( player, treasureBox, 1 )

                    state.setConditionWithSound( 2, true )
                    return this.getPath( '31574-07.htm' )
                }

                break

            case '30909-03.htm':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, glassBox ) ) {
                    await QuestHelper.rewardSingleItem( player, gloves, 1 )
                    await state.exitQuest( false, true )

                    return this.getPath( '30909-02.htm' )
                }

                break

        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                return QuestHelper.getNoQuestMessagePath()

            case QuestStateValues.CREATED:
                if ( data.characterNpcId === WILLIE ) {
                    if ( player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00052_WilliesSpecialBait' ) ) {
                        return this.getPath( '31574-01.htm' )
                    }

                    return this.getPath( '31574-02.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case WILLIE:
                        switch ( state.getCondition() ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, treasureBox ) ) {
                                    return this.getPath( '31574-05.htm' )
                                }

                                return this.getPath( '31574-06.htm' )

                            case 2:
                                return this.getPath( '31574-09.htm' )
                        }

                        break

                    case ANABEL:
                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '30909-01.htm' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}