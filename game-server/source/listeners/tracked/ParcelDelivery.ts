import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const FUNDIN = 31274
const VULCAN = 31539

const PACKAGE_TO_VULCAN = 7263

const minimumLevel = 74

export class ParcelDelivery extends ListenerLogic {
    constructor() {
        super( 'Q00013_ParcelDelivery', 'listeners/tracked/ParcelDelivery.ts' )
        this.questId = 13
        this.questItemIds = [ PACKAGE_TO_VULCAN ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00013_ParcelDelivery'
    }

    getQuestStartIds(): Array<number> {
        return [ FUNDIN ]
    }

    getTalkIds(): Array<number> {
        return [ FUNDIN, VULCAN ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        if ( !state ) {
            return null
        }

        let player = L2World.getPlayer( data.playerId )
        switch ( data.eventName ) {
            case '31274-03.htm':
                state.startQuest()
                state.setMemoState( 11 )

                await QuestHelper.giveSingleItem( player, PACKAGE_TO_VULCAN, 1 )
                return this.getPath( data.eventName )

            case '31539-02.html': {
                if ( !QuestHelper.hasQuestItems( player, PACKAGE_TO_VULCAN ) ) {
                    return this.getPath( '31539-03.html' )
                }

                await QuestHelper.giveAdena( player, 157834, true )
                await QuestHelper.addExpAndSp( player, 589092, 58794 )
                await state.exitQuest( false, true )

                return this.getPath( data.eventName )
            }
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case FUNDIN:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '31274-01.htm' : '31274-02.html' )

                    case QuestStateValues.STARTED:
                        if ( state.isMemoState( 11 ) ) {
                            return this.getPath( '31274-04.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case VULCAN:
                if ( state.isStarted() && QuestHelper.hasQuestItems( player, PACKAGE_TO_VULCAN ) && state.isMemoState( 11 ) ) {
                    return this.getPath( '31539-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}