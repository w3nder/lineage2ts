import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    31327,
    31624,
    31289,
    31290,
    31607,
    31646,
    31649,
    31651,
    31654,
    31655,
    31658,
    31290,
]

const itemIds: Array<number> = [
    7080,
    7539,
    7081,
    7490,
    7273,
    7304,
    7335,
    7366,
    7397,
    7428,
    7098,
    0,
]

const mobIds: Array<number> = [
    27292,
    27224,
    27283,
]

const spawnLocations: Array<ILocational> = [
    new Location( 119518, -28658, -3811 ),
    new Location( 181215, 36676, -4812 ),
    new Location( 181227, 36703, -4816 ),
]

const textLines: Array<string> = [
    `${ SagaPlaceholders.playerName }! Pursued to here! However, I jumped out of the Banshouren boundaries! You look at the giant as the sign of power!`,
    '... Oh ... good! So it was ... let\'s begin!',
    'I do not have the patience ..! I have been a giant force ...! Cough chatter ah ah ah!',
    `Paying homage to those who disrupt the orderly will be ${ SagaPlaceholders.playerName }'s death!`,
    'Now, my soul freed from the shackles of the millennium, Halixia, to the back side I come ...',
    'Why do you interfere others\' battles?',
    'This is a waste of time.. Say goodbye...!',
    '...That is the enemy',
    `...Goodness! ${ SagaPlaceholders.playerName } you are still looking?`,
    `${ SagaPlaceholders.playerName } ... Not just to whom the victory. Only personnel involved in the fighting are eligible to share in the victory.`,
    `Your sword is not an ornament. Don't you think, ${ SagaPlaceholders.playerName }?`,
    'Goodness! I no longer sense a battle there now.',
    'let...',
    'Only engaged in the battle to bar their choice. Perhaps you should regret.',
    'The human nation was foolish to try and fight a giant\'s strength.',
    'Must...Retreat... Too...Strong.',
    `${ SagaPlaceholders.playerName }. Defeat...by...retaining...and...Mo...Hacker`,
    '....! Fight...Defeat...It...Fight...Defeat...It...',
]

export class SagaOfTheTitan extends SagaLogic {
    constructor() {
        super( 'Q00075_SagaOfTheTitan', 'listeners/tracked-100/SagaOfTheTitan.ts' )
        this.questId = 75
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 113
    }

    getPreviousClassId(): number {
        return 0x2e
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00075_SagaOfTheTitan'
    }
}