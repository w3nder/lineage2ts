import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestHelper } from '../helpers/QuestHelper'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { PlayerRadarCache } from '../../gameService/cache/PlayerRadarCache'
import { QuestVariables } from '../helpers/QuestVariables'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const MASTER_ENTIENS = 32200
const MASTER_ORKURUS = 32207
const MASTER_TENAIN = 32215
const CARAVANER_GORT = 32252
const HARKILGAMED = 32253

const BREKA_ORC_HEAD = 9754
const MESSAGE_PLATE = 9755
const REPORT_EAST = 9756
const REPORT_NORTH = 9757
const HARKILGAMEDS_LETTER = 9758
const TENAINS_RECOMMENDATION = 9759

const DIMENSIONAL_DIAMOND = 7562
const ORKURUS_RECOMMENDATION = 9760

const DEAD_SEEKER = 20202
const MARSH_STAKATO_DRONE = 20234
const BREKA_ORC = 20267
const BREKA_ORC_ARCHER = 20268
const BREKA_ORC_SHAMAN = 20269
const BREKA_ORC_OVERLORD = 20270
const BREKA_ORC_WARRIOR = 20271
const ROAD_SCAVENGER = 20551

const DIVINE_EMISSARY = 27323

const minimumLevel = 39

export class CertifiedBerserker extends ListenerLogic {
    constructor() {
        super( 'Q00064_CertifiedBerserker', 'listeners/tracked/CertifiedBerserker.ts' )
        this.questId = 64
        this.questItemIds = [ BREKA_ORC_HEAD, MESSAGE_PLATE, REPORT_EAST, REPORT_NORTH, HARKILGAMEDS_LETTER, TENAINS_RECOMMENDATION ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            DEAD_SEEKER,
            MARSH_STAKATO_DRONE,
            BREKA_ORC,
            BREKA_ORC_ARCHER,
            BREKA_ORC_SHAMAN,
            BREKA_ORC_OVERLORD,
            BREKA_ORC_WARRIOR,
            ROAD_SCAVENGER,
            DIVINE_EMISSARY,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00064_CertifiedBerserker'
    }

    getQuestStartIds(): Array<number> {
        return [ MASTER_ORKURUS ]
    }

    getTalkIds(): Array<number> {
        return [ MASTER_ORKURUS, MASTER_ENTIENS, MASTER_TENAIN, CARAVANER_GORT, HARKILGAMED ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )

        if ( state && state.isStarted() ) {
            let player = L2World.getPlayer( data.playerId )
            let npc = L2World.getObjectById( data.targetId ) as L2Npc

            if ( !GeneralHelper.checkIfInRange( 1500, npc, player, true ) ) {
                return
            }

            switch ( npc.getId() ) {
                case DEAD_SEEKER:
                    if ( state.isMemoState( 7 )
                            && !QuestHelper.hasQuestItem( player, REPORT_EAST )
                            && _.random( 100 ) < QuestHelper.getAdjustedChance( REPORT_EAST, 20, data.isChampion ) ) {

                        await QuestHelper.giveSingleItem( player, REPORT_EAST, 1 )

                        if ( QuestHelper.hasQuestItem( player, REPORT_NORTH ) ) {
                            state.setConditionWithSound( 10, true )
                            return
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                        return
                    }

                    return

                case MARSH_STAKATO_DRONE:
                    if ( state.isMemoState( 7 )
                            && !QuestHelper.hasQuestItem( player, REPORT_NORTH )
                            && _.random( 100 ) < QuestHelper.getAdjustedChance( REPORT_NORTH, 20, data.isChampion ) ) {

                        await QuestHelper.giveSingleItem( player, REPORT_NORTH, 1 )

                        if ( QuestHelper.hasQuestItem( player, REPORT_EAST ) ) {
                            state.setConditionWithSound( 10, true )
                            return
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                        return
                    }

                    return

                case BREKA_ORC:
                case BREKA_ORC_ARCHER:
                case BREKA_ORC_SHAMAN:
                case BREKA_ORC_OVERLORD:
                case BREKA_ORC_WARRIOR:
                    if ( state.isMemoState( 2 ) && QuestHelper.getQuestItemsCount( player, BREKA_ORC_HEAD ) < 20 ) {
                        await QuestHelper.rewardSingleQuestItem( player, BREKA_ORC_HEAD, 1, data.isChampion )

                        if ( QuestHelper.getQuestItemsCount( player, BREKA_ORC_HEAD ) >= 20 ) {
                            state.setConditionWithSound( 3, true )
                            return
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                        return
                    }

                    return

                case ROAD_SCAVENGER:
                    if ( state.isMemoState( 4 )
                            && !QuestHelper.hasQuestItems( player, MESSAGE_PLATE )
                            && _.random( 100 ) < QuestHelper.getAdjustedChance( MESSAGE_PLATE, 20, data.isChampion ) ) {
                        await QuestHelper.giveSingleItem( player, MESSAGE_PLATE, 1 )
                        state.setConditionWithSound( 6, true )
                    }

                    return

                case DIVINE_EMISSARY:
                    if ( state.isMemoState( 9 ) && _.random( 100 ) < 20 ) {
                        let kamael: L2Npc = QuestHelper.addSpawnAtLocation( HARKILGAMED, npc, true, 60000 )
                        BroadcastHelper.broadcastNpcSayStringId(
                                kamael,
                                NpcSayType.NpcAll,
                                NpcStringIds.S1_DID_YOU_COME_TO_HELP_ME,
                                player.getAppearance().getVisibleName() )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    }

                    return
            }
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( data.playerId, QuestVariables.secondClassDiamondReward, true )
                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 48 )

                        return this.getPath( '32207-06.htm' )
                    }

                    return this.getPath( '32207-06a.htm' )
                }

                return

            case '32207-10.html':
                if ( state.isMemoState( 11 ) ) {
                    break
                }

                return

            case '32207-11.html':
                if ( state.isMemoState( 11 ) ) {
                    await QuestHelper.giveAdena( player, 63104, true )
                    await QuestHelper.giveSingleItem( player, ORKURUS_RECOMMENDATION, 1 )
                    await QuestHelper.addExpAndSp( player, 349006, 23948 )

                    await state.exitQuest( false, true )
                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                    break
                }

                return

            case '32215-02.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '32215-07.html':
            case '32215-08.html':
            case '32215-09.html':
                if ( state.isMemoState( 5 ) ) {
                    break
                }

                return

            case '32215-10.html':
                if ( state.isMemoState( 5 ) ) {
                    await QuestHelper.takeSingleItem( player, MESSAGE_PLATE, 1 )
                    state.setMemoState( 6 )
                    state.setConditionWithSound( 8, true )

                    break
                }

                return
            case '32215-15.html':
                if ( state.isMemoState( 10 ) ) {
                    await QuestHelper.takeSingleItem( player, HARKILGAMEDS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, TENAINS_RECOMMENDATION, 1 )

                    state.setMemoState( 11 )
                    state.setConditionWithSound( 14, true )

                    break
                }

                return

            case '32252-02.html':
                if ( state.isMemoState( 3 ) ) {
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            case '32253-02.html':
                if ( state.isMemoState( 9 ) ) {
                    await QuestHelper.giveSingleItem( player, HARKILGAMEDS_LETTER, 1 )
                    state.setMemoState( 10 )
                    state.setConditionWithSound( 13, true )

                    break
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MASTER_ORKURUS ) {
                    if ( player.getRace() === Race.KAMAEL ) {
                        if ( player.getClassId() === ClassIdValues.trooper.id ) {
                            if ( player.getLevel() >= minimumLevel ) {
                                return this.getPath( '32207-01.htm' )
                            }

                            return this.getPath( '32207-02.html' )
                        }

                        return this.getPath( '32207-03.html' )
                    }

                    return this.getPath( '32207-04.html' )
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()
                switch ( data.characterNpcId ) {
                    case MASTER_ORKURUS:
                        if ( memoState === 1 ) {
                            return this.getPath( '32207-07.html' )
                        }

                        if ( memoState >= 2 && memoState < 11 ) {
                            return this.getPath( '32207-08.html' )
                        }

                        if ( memoState === 11 ) {
                            return this.getPath( '32207-09.html' )
                        }

                        break

                    case MASTER_ENTIENS:
                        if ( memoState === 6 ) {
                            state.setMemoState( 7 )
                            state.setConditionWithSound( 9, true )

                            PlayerRadarCache.getRadar( data.playerId ).addMarker( 27956, 106003, -3831 )
                            PlayerRadarCache.getRadar( data.playerId ).addMarker( 50568, 152408, -2656 )

                            return this.getPath( '32200-01.html' )
                        }

                        if ( memoState === 7 ) {
                            if ( !QuestHelper.hasQuestItems( player, REPORT_EAST, REPORT_NORTH ) ) {
                                return this.getPath( '32200-02.html' )
                            }

                            await QuestHelper.takeSingleItem( player, REPORT_EAST, -1 )
                            await QuestHelper.takeSingleItem( player, REPORT_NORTH, -1 )

                            state.setMemoState( 8 )
                            state.setConditionWithSound( 11, true )

                            return this.getPath( '32200-03.html' )
                        }

                        if ( memoState === 8 ) {
                            return this.getPath( '32200-04.html' )
                        }

                        break

                    case MASTER_TENAIN:
                        switch ( memoState ) {
                            case 1:
                                return this.getPath( '32215-01.html' )

                            case 2:
                                if ( QuestHelper.getQuestItemsCount( player, BREKA_ORC_HEAD ) < 20 ) {
                                    return this.getPath( '32215-03.html' )
                                }

                                await QuestHelper.takeSingleItem( player, BREKA_ORC_HEAD, -1 )

                                state.setMemoState( 3 )
                                state.setConditionWithSound( 4, true )

                                return this.getPath( '32215-04.html' )

                            case 3:
                                return this.getPath( '32215-05.html' )

                            case 5:
                                return this.getPath( '32215-06.html' )

                            case 6:
                                return this.getPath( '32215-11.html' )

                            case 8:
                                state.setMemoState( 9 )
                                state.setConditionWithSound( 12, true )

                                return this.getPath( '32215-12.html' )

                            case 9:
                                return this.getPath( '32215-13.html' )

                            case 10:
                                return this.getPath( '32215-14.html' )

                            case 11:
                                return this.getPath( '32215-16.html' )
                        }

                        break

                    case CARAVANER_GORT:
                        if ( memoState === 3 ) {
                            return this.getPath( '32252-01.html' )
                        }

                        if ( memoState === 4 ) {
                            if ( !QuestHelper.hasQuestItem( player, MESSAGE_PLATE ) ) {
                                return this.getPath( '32252-03.html' )
                            }

                            state.setMemoState( 5 )
                            state.setConditionWithSound( 7, true )

                            return this.getPath( '32252-04.html' )
                        }

                        if ( memoState === 5 ) {
                            return this.getPath( '32252-05.html' )
                        }

                        break

                    case HARKILGAMED:
                        if ( memoState === 9 ) {
                            return this.getPath( '32253-01.html' )
                        }

                        if ( memoState === 10 ) {
                            return this.getPath( '32253-03.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MASTER_ORKURUS ) {
                    return this.getPath( '32207-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}