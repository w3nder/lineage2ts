import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const HIERARCH = 31517
const EVIL_ALTAR_1 = 31512
const EVIL_ALTAR_2 = 31513
const EVIL_ALTAR_3 = 31514
const EVIL_ALTAR_4 = 31515
const EVIL_ALTAR_5 = 31516

const CRYSTAL_OF_SEAL = 7167

export class TheComingDarkness extends ListenerLogic {
    constructor() {
        super( 'Q00016_TheComingDarkness', 'listeners/tracked/TheComingDarkness.ts' )
        this.questId = 16
        this.questItemIds = [ CRYSTAL_OF_SEAL ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00016_TheComingDarkness'
    }

    getQuestStartIds(): Array<number> {
        return [ HIERARCH ]
    }

    getTalkIds(): Array<number> {
        return [ HIERARCH, EVIL_ALTAR_1, EVIL_ALTAR_2, EVIL_ALTAR_3, EVIL_ALTAR_4, EVIL_ALTAR_5 ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, false )
        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        switch ( data.eventName ) {
            case '31517-02.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, CRYSTAL_OF_SEAL, 5 )
                break

            case '31512-01.html':
            case '31513-01.html':
            case '31514-01.html':
            case '31515-01.html':
            case '31516-01.html':
                let npcId: number = _.parseInt( data.eventName.replace( '-01.html', '' ) )
                let conditionValue: number = state.getCondition()
                if ( ( conditionValue === ( npcId - 31511 ) ) && QuestHelper.hasQuestItems( player, CRYSTAL_OF_SEAL ) ) {
                    await QuestHelper.takeSingleItem( player, CRYSTAL_OF_SEAL, 1 )
                    state.setConditionWithSound( conditionValue + 1, true )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let nextState: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00017_LightAndDarkness' )
        if ( nextState && !nextState.isCompleted() ) {
            return this.getPath( '31517-04.html' )
        }

        let state: QuestState = this.getQuestState( data.playerId, true )
        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let player = L2World.getPlayer( data.playerId )
                return this.getPath( player.getLevel() >= 62 ? '31517-00.htm' : '31517-05.html' )

            case QuestStateValues.STARTED:
                if ( data.characterNpcId === HIERARCH ) {
                    if ( state.isCondition( 6 ) ) {
                        let player = L2World.getPlayer( data.playerId )
                        await QuestHelper.addExpAndSp( player, 865187, 69172 )
                        await state.exitQuest( false, true )

                        return this.getPath( '31517-03.html' )
                    }

                    return this.getPath( '31517-02a.html' )
                }

                if ( ( data.characterNpcId - 31511 ) === state.getCondition() ) {
                    return this.getPath( `${ data.characterNpcId }-00.html` )
                }

                return this.getPath( `${ data.characterNpcId }-01.html` )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}