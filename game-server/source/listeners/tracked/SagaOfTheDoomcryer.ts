import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    31336,
    31624,
    31589,
    31290,
    31642,
    31646,
    31649,
    31650,
    31654,
    31655,
    31657,
    31290
]

const itemIds : Array<number> = [
    7080,
    7539,
    7081,
    7493,
    7276,
    7307,
    7338,
    7369,
    7400,
    7431,
    7101,
    0
]

const mobIds : Array<number> = [
    27295,
    27227,
    27285
]

const spawnLocations : Array<ILocational> = [
    new Location( 191046, -40640, -3042 ),
    new Location( 46087, -36372, -1685 ),
    new Location( 46066, -36396, -1685 )
]

export class SagaOfTheDoomcryer extends SagaLogic {
    constructor() {
        super( 'Q00078_SagaOfTheDoomcryer', 'listeners/tracked-100/SagaOfTheDoomcryer.ts' )
        this.questId = 78
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 116
    }

    getPreviousClassId(): number {
        return 0x34
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00078_SagaOfTheDoomcryer'
    }
}