import { SagaLogic } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds: Array<number> = [
    31582,
    31623,
    31284,
    31284,
    31611,
    31646,
    31649,
    31653,
    31654,
    31655,
    31656,
    31284,
]

const itemIds: Array<number> = [
    7080,
    7527,
    7081,
    7511,
    7294,
    7325,
    7356,
    7387,
    7418,
    7449,
    7092,
    0,
]

const mobIds: Array<number> = [
    27272,
    27245,
    27264,
]

const spawnLocations: Array<ILocational> = [
    new Location( 164650, -74121, -2871 ),
    new Location( 47429, -56923, -2383 ),
    new Location( 47391, -56929, -2370 ),
]

export class SagaOfTheSpectralDancer extends SagaLogic {
    constructor() {
        super( 'Q00096_SagaOfTheSpectralDancer', 'listeners/tracked/SagaOfTheSpectralDancer.ts' )
        this.questId = 96
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 107
    }

    getPreviousClassId(): number {
        return 0x22
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00096_SagaOfTheSpectralDancer'
    }
}