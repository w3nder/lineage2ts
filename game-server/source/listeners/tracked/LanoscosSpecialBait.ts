import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const LANOSCO = 31570
const SINGING_WIND = 21026

const ESSENCE_OF_WIND = 7621
const WIND_FISHING_LURE = 7610
const minimumLevel = 27

export class LanoscosSpecialBait extends ListenerLogic {
    constructor() {
        super( 'Q00050_LanoscosSpecialBait', 'listeners/tracked/LanoscosSpecialBait.ts' )
        this.questId = 50
        this.questItemIds = [ ESSENCE_OF_WIND ]
    }

    getAttackableKillIds(): Array<number> {
        return [ SINGING_WIND ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00050_LanoscosSpecialBait'
    }

    getQuestStartIds(): Array<number> {
        return [ LANOSCO ]
    }

    getTalkIds(): Array<number> {
        return [ LANOSCO ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )

        if ( !player ) {
            return
        }

        if ( QuestHelper.getQuestItemsCount( player, ESSENCE_OF_WIND ) < 100
                && _.random( 100 ) < QuestHelper.getAdjustedChance( ESSENCE_OF_WIND, 33, data.isChampion ) ) {
            await QuestHelper.rewardSingleItem( player, ESSENCE_OF_WIND, 1 )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        if ( QuestHelper.getQuestItemsCount( player, ESSENCE_OF_WIND ) >= 100 ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.name )
            state.setConditionWithSound( 2, true )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '31570-03.htm':
                state.startQuest()
                break

            case '31570-07.html':
                let player = L2World.getPlayer( data.playerId )
                if ( state.isCondition( 2 ) && QuestHelper.getQuestItemsCount( player, ESSENCE_OF_WIND ) >= 100 ) {
                    await QuestHelper.rewardSingleItem( player, WIND_FISHING_LURE, 4 )
                    await state.exitQuest( false, true )

                    return this.getPath( '31570-06.htm' )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name, true )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let player = L2World.getPlayer( data.playerId )
                return this.getPath( player.getLevel() >= minimumLevel ? '31570-01.htm' : '31570-02.html' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 1 ) ? '31570-05.html' : '31570-04.html' )

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}