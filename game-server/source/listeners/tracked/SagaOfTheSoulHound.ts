import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

const npcIds: Array<number> = [
    32138,
    31272,
    31269,
    31317,
    32235,
    31646,
    31648,
    31652,
    31654,
    31655,
    31657,
    32241,
]

const itemIds: Array<number> = [
    7080,
    9802,
    7081,
    9741,
    9723,
    9726,
    9729,
    9732,
    9735,
    9738,
    9719,
    0,
]

const mobIds: Array<number> = [
    27327,
    27329,
    27328,
]

const spawnLocations: Array<ILocational> = [
    new Location( 161719, -92823, -1893 ),
    new Location( 46087, -36372, -1685 ),
    new Location( 46066, -36396, -1685 ),
]

const textLines: Array<string> = [
    `${ SagaPlaceholders.playerName }! Pursued to here! However, I jumped out of the Banshouren boundaries! You look at the giant as the sign of power!`,
    '... Oh ... good! So it was ... let\'s begin!',
    'I do not have the patience ..! I have been a giant force ...! Cough chatter ah ah ah!',
    `Paying homage to those who disrupt the orderly will be ${ SagaPlaceholders.playerName }'s death!`,
    'Now, my soul freed from the shackles of the millennium, Halixia, to the back side I come ...',
    'Why do you interfere others\' battles?',
    'This is a waste of time.. Say goodbye...!',
    '...That is the enemy',
    `...Goodness! ${ SagaPlaceholders.playerName } you are still looking?`,
    `${ SagaPlaceholders.playerName } ... Not just to whom the victory. Only personnel involved in the fighting are eligible to share in the victory.`,
    `Your sword is not an ornament. Don't you think, ${ SagaPlaceholders.playerName }?`,
    'Goodness! I no longer sense a battle there now.',
    'let...',
    'Only engaged in the battle to bar their choice. Perhaps you should regret.',
    'The human nation was foolish to try and fight a giant\'s strength.',
    'Must...Retreat... Too...Strong.',
    `${ SagaPlaceholders.playerName }. Defeat...by...retaining...and...Mo...Hacker`,
    '....! Fight...Defeat...It...Fight...Defeat...It...',
]

export class SagaOfTheSoulHound extends SagaLogic {
    constructor() {
        super( 'Q00068_SagaOfTheSoulHound', 'listeners/tracked-100/SagaOfTheSoulHound.ts' )
        this.questId = 68
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId( player: L2PcInstance ): number {
        if ( player.getClassId() === 0x81 ) {
            return 133
        }

        return 132
    }

    getPreviousClassId( player: L2PcInstance ): number {
        if ( player.getClassId() === 0x81 ) {
            return 0x81
        }

        return 0x80
    }

    getState( player: L2PcInstance ): QuestState {
        if ( ![ 0x80, 0x81 ].includes( player.getClassId() ) ) {
            return
        }

        return QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00068_SagaOfTheSoulHound'
    }
}