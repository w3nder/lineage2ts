import { SagaLogic, SagaPlaceholders } from '../helpers/SagaLogic'
import { ILocational, Location } from '../../gameService/models/Location'

const npcIds : Array<number> = [
    30702,
    31627,
    31604,
    31640,
    31634,
    31646,
    31648,
    31652,
    31654,
    31655,
    31658,
    31641
]

const itemIds : Array<number> = [
    7080,
    7520,
    7081,
    7498,
    7281,
    7312,
    7343,
    7374,
    7405,
    7436,
    7106,
    0
]

const mobIds : Array<number> = [
    27297,
    27232,
    27306
]

const spawnLocations : Array<ILocational> = [
    new Location( 161719, -92823, -1893 ),
    new Location( 181227, 36703, -4816 ),
    new Location( 181215, 36676, -4812 )
]

export class SagaOfTheMoonlightSentinel extends SagaLogic {
    constructor() {
        super( 'Q00083_SagaOfTheMoonlightSentinel', 'listeners/tracked/SagaOfTheMoonlightSentinel.ts' )
        this.questId = 83
    }

    getAllItemIds(): Array<number> {
        return itemIds
    }

    getAllMobIds(): Array<number> {
        return mobIds
    }

    getAllNpcIds(): Array<number> {
        return npcIds
    }

    getAllSpawnLocations(): Array<ILocational> {
        return spawnLocations
    }

    getClassId(): number {
        return 102
    }

    getPreviousClassId(): number {
        return 0x18
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00083_SagaOfTheMoonlightSentinel'
    }
}