import {
    PersistedConfigurationLogic,
    PersistedConfigurationLogicType,
} from '../../gameService/models/listener/PersistedConfigurationLogic'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { DataManager } from '../../data/manager'

interface DelevelManagerConfiguration extends PersistedConfigurationLogicType {
    isEnabled: boolean
    managerNpcId: number
    requiredItemId: number
    requiredItemCount: number
    minimumLevelRange: number
    maximumLevelRange: number
}

export class DelevelManager extends PersistedConfigurationLogic<DelevelManagerConfiguration> {

    constructor() {
        super( 'DelevelManager', 'listeners/addons/DelevelManager.ts' )
    }

    getDefaultConfiguration(): DelevelManagerConfiguration {
        return {
            isEnabled: false,
            managerNpcId: 1002000,
            requiredItemId: 4356, // Gold Einhasad
            requiredItemCount: 2,
            minimumLevelRange: 20,
            maximumLevelRange: 60
        }
    }

    getQuestStartIds(): Array<number> {
        if ( this.configuration.isEnabled ) {
            return [ this.configuration.managerNpcId ]
        }
    }

    getTalkIds(): Array<number> {
        if ( this.configuration.isEnabled ) {
            return [ this.configuration.managerNpcId ]
        }
    }

    getApproachedForTalkIds(): Array<number> {
        if ( this.configuration.isEnabled ) {
            return [ this.configuration.managerNpcId ]
        }
    }

    async onApproachedForTalk(): Promise<string> {
        return DataManager.getHtmlData().getItem( this.getPath( '1002000.htm' ) )
                .replace( '%amount%', this.configuration.requiredItemCount.toString() )
                .replace( '%name%', DataManager.getItems().getTemplate( this.configuration.requiredItemId ).getName() )
    }

    getPathPrefix(): string {
        return 'overrides/html/addons/delevelManager'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( !this.configuration.isEnabled ) {
            return
        }

        if ( data.eventName === 'delevel' ) {
            let player = L2World.getPlayer( data.playerId )
            if ( player.getLevel() > this.configuration.maximumLevelRange || player.getLevel() <= this.configuration.minimumLevelRange ) {
                return DataManager.getHtmlData().getItem( this.getPath( '1002000-2.htm' ) )
                        .replace( '%min%', this.configuration.minimumLevelRange.toString() )
                        .replace( '%max%', this.configuration.maximumLevelRange.toString() )
            }

            if ( QuestHelper.getQuestItemsCount( player, this.configuration.requiredItemId ) >= this.configuration.requiredItemCount ) {
                await QuestHelper.takeSingleItem( player, this.configuration.requiredItemId, this.configuration.requiredItemCount )

                let nextLevel = player.getLevel() - 1
                player.setLevel( nextLevel )
                player.setExp( DataManager.getExperienceData().getExpForLevel( Math.min( nextLevel, player.getMaxExpLevel() ) ) )
                await player.onLevelChange( false )
                player.broadcastInfo()
            }

            return DataManager.getHtmlData().getItem( this.getPath( '1002000-1.htm' ) )
                    .replace( '%name%', DataManager.getItems().getTemplate( this.configuration.requiredItemId ).getName() )
        }
    }
}