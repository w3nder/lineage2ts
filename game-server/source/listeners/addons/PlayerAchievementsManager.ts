import {
    PersistedConfigurationLogic,
    PersistedConfigurationLogicType,
} from '../../gameService/models/listener/PersistedConfigurationLogic'

// TODO : implement data loader for player achievements
// TODO : implement player achievements system, state would be stored in player variables
interface PlayerAchievementsConfiguration extends PersistedConfigurationLogicType {

}

export class PlayerAchievementsManager extends PersistedConfigurationLogic<PlayerAchievementsConfiguration> {
    getDefaultConfiguration(): PlayerAchievementsConfiguration {
        return
    }
}