import {
    PersistedConfigurationLogic,
    PersistedConfigurationLogicType,
} from '../../gameService/models/listener/PersistedConfigurationLogic'
import { AdditionalDropManager, CustomDrop } from '../../gameService/cache/AdditionalDropManager'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import _ from 'lodash'

interface DropManagerConfiguration extends PersistedConfigurationLogicType {
    isEnabled: boolean
    dropItems: Array<CustomDrop>
    spoilItems: Array<CustomDrop>
}

export class DropManager extends PersistedConfigurationLogic<DropManagerConfiguration> {
    dropIds : Array<number> = []
    spoilIds : Array<number> = []

    constructor() {
        super( 'DropManager', 'listeners/addons/DropManager.ts' )
    }

    getDefaultConfiguration(): DropManagerConfiguration {
        return {
            dropItems: [
                {
                    itemId: ItemTypes.Adena,
                    maxCount: 100,
                    minCount: 10,
                    chance: 10
                }
            ],
            isEnabled: true,
            spoilItems: [
                {
                    itemId: ItemTypes.Adena,
                    maxCount: 100,
                    minCount: 10,
                    chance: 10
                }
            ]
        }
    }

    async initialize(): Promise<void> {
        this.dropIds = _.map( this.configuration.dropItems, ( drop: CustomDrop ) : number => {
            if ( drop.templateId > 0 ) {
                return AdditionalDropManager.addTemplateDrop( drop.itemId, drop.minCount, drop.maxCount, drop.chance, drop.templateId )
            }

            return AdditionalDropManager.addGenericDrop( drop.itemId, drop.minCount, drop.maxCount, drop.chance )
        } )

        this.spoilIds = _.map( this.configuration.spoilItems, ( drop: CustomDrop ) : number => {
            if ( drop.templateId > 0 ) {
                return AdditionalDropManager.addTemplateSpoil( drop.itemId, drop.minCount, drop.maxCount, drop.chance, drop.templateId )
            }

            return AdditionalDropManager.addGenericSpoil( drop.itemId, drop.minCount, drop.maxCount, drop.chance )
        } )
    }

    removeAllItems() {
        AdditionalDropManager.removeGenericDrops( this.dropIds )
        AdditionalDropManager.removeTemplateDrops( this.dropIds )

        this.dropIds = null

        AdditionalDropManager.removeGenericSpoils( this.spoilIds )
        AdditionalDropManager.removeTemplateSpoils( this.spoilIds )

        this.spoilIds = null
    }
}