import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ClanHallSiegeManager } from '../../gameService/instancemanager/ClanHallSiegeManager'
import { SiegableHall } from '../../gameService/models/entity/clanhall/SiegableHall'
import { L2Clan } from '../../gameService/models/L2Clan'
import { DataManager } from '../../data/manager'
import _ from 'lodash'
import moment from 'moment'

const MESSENGER = 35437
const TARLK_AMULET = 4332
const CONTEST_CERTIFICATE = 4333
const TROPHY_OF_ALLIANCE = 5009

const monsterRewardChances = {
    20570: 0.6, // Tarlk Bugbear
    20571: 0.7, // Tarlk Bugbear Warrior
    20572: 0.8, // Tarlk Bugbear High Warrior
    20573: 0.9, // Tarlk Basilisk
    20574: 0.7, // Elder Tarlk Basilisk
}

export class CompetitionForTheBanditStronghold extends ListenerLogic {
    constructor() {
        super( 'Q00504_CompetitionForTheBanditStronghold', 'listeners/tracked-500/CompetitionForTheBanditStronghold.ts' )
        this.questId = 504
    }

    getQuestStartIds(): Array<number> {
        return [ MESSENGER ]
    }

    getTalkIds(): Array<number> {
        return [ MESSENGER ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        if ( data.eventName === '35437-02.htm' ) {
            let player = L2World.getPlayer( data.playerId )

            state.startQuest()
            await QuestHelper.giveSingleItem( player, CONTEST_CERTIFICATE, 1 )

            return this.getPath( '35437-02.htm' )
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( TARLK_AMULET, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( !QuestHelper.hasQuestItem( player, CONTEST_CERTIFICATE ) ) {
            return
        }

        await QuestHelper.rewardUpToLimit( player, TARLK_AMULET, 1, 30, data.isChampion )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )
        let banditStronghold: SiegableHall = ClanHallSiegeManager.getSiegableHall( 35 )

        if ( !banditStronghold.isWaitingBattle() ) {
            let time: string = moment()
                    .millisecond( banditStronghold.getNextSiegeTime() )
                    .format( 'yyyy-MM-dd HH:mm:ss' )
            return DataManager.getHtmlData().getItem( this.getPath( '35437-09.html' ) )
                    .replace( '%nextSiege%', time )
        }

        let clan: L2Clan = player.getClan()
        if ( !clan || clan.getLevel() < 4 ) {
            return this.getPath( '35437-04.html' )
        }

        if ( !player.isClanLeader() ) {
            return this.getPath( '35437-05.html' )
        }

        if ( clan.getHideoutId() > 0
                || clan.getFortId() > 0
                || clan.getCastleId() > 0 ) {
            return this.getPath( '35437-10.html' )
        }

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( !banditStronghold.isWaitingBattle() ) {
                    let time: string = moment()
                            .millisecond( banditStronghold.getNextSiegeTime() )
                            .format( 'yyyy-MM-dd HH:mm:ss' )
                    return DataManager.getHtmlData().getItem( this.getPath( '35437-09.html' ) )
                            .replace( '%nextSiege%', time )
                }

                return this.getPath( '35437-01.htm' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.getQuestItemsCount( player, TARLK_AMULET ) < 30 ) {
                    return this.getPath( '35437-07.html' )
                }

                await QuestHelper.takeSingleItem( player, TARLK_AMULET, -1 )
                await QuestHelper.rewardSingleItem( player, TROPHY_OF_ALLIANCE, 1 )
                await state.exitQuest( true )

                return this.getPath( '35437-08.html' )

            case QuestStateValues.COMPLETED:
                return this.getPath( '35437-07a.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00504_CompetitionForTheBanditStronghold'
    }
}