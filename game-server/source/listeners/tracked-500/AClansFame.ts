import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { PlayerRadarCache } from '../../gameService/cache/PlayerRadarCache'
import { L2World } from '../../gameService/L2World'
import { L2Clan } from '../../gameService/models/L2Clan'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { PledgeShowInfoUpdate } from '../../gameService/packets/send/PledgeShowInfoUpdate'

const VALDIS = 31331

type RaidRewardData = [ number, number, number ] // npcId, itemId, reputation score amount
const raidRewards: { [ raidId: number ]: RaidRewardData } = {
    1: [ 25290, 8489, 1378 ], // Daimon The White-Eyed
    2: [ 25293, 8490, 1378 ], // Hestia, Guardian Deity Of The Hot Springs
    3: [ 25523, 8491, 1070 ], // Plague Golem
    4: [ 25322, 8492, 782 ], // Demon's Agent Falston
}

const variableNames = {
    raidId: 'ri',
}

export class AClansFame extends ListenerLogic {
    constructor() {
        super( 'Q00509_AClansFame', 'listeners/tracked-500/AClansFame.ts' )
        this.questId = 509
    }

    getQuestStartIds(): Array<number> {
        return [ VALDIS ]
    }

    getTalkIds(): Array<number> {
        return [ VALDIS ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            25290,
            25293,
            25523,
            25322,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        switch ( data.eventName ) {
            case '31331-0.html':
                state.startQuest()
                break

            case '31331-1.html':
                state.setVariable( variableNames.raidId, 1 )
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 186304, -43744, -3193 )
                break

            case '31331-2.html':
                state.setVariable( variableNames.raidId, 2 )
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 134672, -115600, -1216 )
                break

            case '31331-3.html':
                state.setVariable( variableNames.raidId, 3 )
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 170000, -60000, -3500 )
                break

            case '31331-4.html':
                state.setVariable( variableNames.raidId, 4 )
                PlayerRadarCache.getRadar( data.playerId ).addNpcMarker( 93296, -75104, -1824 )
                break

            case '31331-5.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        let clan: L2Clan = player.getClan()
        if ( !clan ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( clan.getLeaderId(), this.getName(), false )
        if ( !state || !state.isStarted() ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let leader: L2PcInstance = clan.getLeader().getPlayerInstance()

        if ( !GeneralHelper.checkIfInRange( 1500, leader, npc, true ) ) {
            return
        }

        let raidData: RaidRewardData = raidRewards[ state.getVariable( variableNames.raidId ) ]
        if ( !raidData ) {
            return
        }

        let [ npcId, itemId ] = raidData

        if ( npcId !== data.npcId || QuestHelper.hasQuestItem( leader, itemId ) ) {
            return
        }

        await QuestHelper.giveSingleItem( leader, itemId, 1 )
        leader.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )
        let clan: L2Clan = player.getClan()

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let isNotValid: boolean = !clan || !player.isClanLeader() || clan.getLevel() < 6
                return this.getPath( isNotValid ? '31331-0a.htm' : '31331-0b.htm' )

            case QuestStateValues.STARTED:
                if ( !clan || !player.isClanLeader() ) {
                    await state.exitQuest( true )

                    return this.getPath( '31331-6.html' )
                }

                let raidId = state.getVariable( variableNames.raidId )
                let raidData: RaidRewardData = raidRewards[ raidId ]
                if ( !raidData ) {
                    return this.getPath( '31331-0.html' )
                }

                let [ , itemId, points ] = raidData

                if ( !QuestHelper.hasQuestItem( player, itemId ) ) {
                    return this.getPath( `31331-${ raidId }a.html` )
                }

                await QuestHelper.takeSingleItem( player, itemId, -1 )
                await clan.addReputationScore( points, true )

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_FANFARE_1 )

                let message: Buffer = new SystemMessageBuilder( SystemMessageIds.CLAN_QUEST_COMPLETED_AND_S1_POINTS_GAINED )
                        .addNumber( points )
                        .getBuffer()
                player.sendOwnedData( message )
                clan.broadcastDataToOnlineMembers( PledgeShowInfoUpdate( clan ) )

                return this.getPath( `31331-${ raidId }b.html` )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00509_AClansFame'
    }
}