import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ILocational, Location } from '../../gameService/models/Location'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { ClanCache } from '../../gameService/cache/ClanCache'
import { L2Clan } from '../../gameService/models/L2Clan'
import { L2Character } from '../../gameService/models/actor/L2Character'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { AbnormalType } from '../../gameService/models/skills/AbnormalType'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const SIR_KRISTOF_RODEMAI = 30756
const STATUE_OF_OFFERING = 30757
const ATHREA = 30758
const KALIS = 30759
const OEL_MAHUM_WITCH_DOCTOR = 20576
const HARIT_LIZARDMAN_SHAMAN = 20644
const VANOR_SILENOS_SHAMAN = 20685
const BOX_OF_ATHREA_1 = 27173
const BOX_OF_ATHREA_2 = 27174
const BOX_OF_ATHREA_3 = 27175
const BOX_OF_ATHREA_4 = 27176
const BOX_OF_ATHREA_5 = 27177
const HERB_OF_HARIT = 3832
const HERB_OF_VANOR = 3833
const HERB_OF_OEL_MAHUM = 3834
const BLOOD_OF_EVA = 3835
const ATHREAS_COIN = 3836
const SYMBOL_OF_LOYALTY = 3837
const ANTIDOTE_RECIPE_LIST = 3872
const VOUCHER_OF_FAITH = 3873
const ALLIANCE_MANIFESTO = 3874
const POTION_OF_RECOVERY = 3889
const POISON_OF_DEATH = 4082
const DIE_YOU_FOOL = 4083
const minimumClanLevel = 3
const clanMememberLevel = 40
const requiredAdenaForGame = 10000

const locations: Array<Location> = [
    new Location( 102273, 103433, -3512 ),
    new Location( 102190, 103379, -3524 ),
    new Location( 102107, 103325, -3533 ),
    new Location( 102024, 103271, -3500 ),
    new Location( 102327, 103350, -3511 ),
    new Location( 102244, 103296, -3518 ),
    new Location( 102161, 103242, -3529 ),
    new Location( 102078, 103188, -3500 ),
    new Location( 102381, 103267, -3538 ),
    new Location( 102298, 103213, -3532 ),
    new Location( 102215, 103159, -3520 ),
    new Location( 102132, 103105, -3513 ),
    new Location( 102435, 103184, -3515 ),
    new Location( 102352, 103130, -3522 ),
    new Location( 102269, 103076, -3533 ),
    new Location( 102186, 103022, -3541 ),
]

const variableNames = {
    flag: 'fg',
}

const eventNames = {
    symbolOfLoyalty: 'sol',
}

export class ProofOfClanAlliance extends ListenerLogic {
    constructor() {
        super( 'Q00501_ProofOfClanAlliance', 'listeners/tracked-500/ProofOfClanAlliance.ts' )
        this.questId = 501
        this.questItemIds = [
            ANTIDOTE_RECIPE_LIST,
            VOUCHER_OF_FAITH,
            HERB_OF_HARIT,
            HERB_OF_VANOR,
            HERB_OF_OEL_MAHUM,
            BLOOD_OF_EVA,
            ATHREAS_COIN,
            SYMBOL_OF_LOYALTY,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ SIR_KRISTOF_RODEMAI, STATUE_OF_OFFERING ]
    }

    getTalkIds(): Array<number> {
        return [ SIR_KRISTOF_RODEMAI, STATUE_OF_OFFERING, ATHREA, KALIS ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            OEL_MAHUM_WITCH_DOCTOR,
            HARIT_LIZARDMAN_SHAMAN,
            VANOR_SILENOS_SHAMAN,
            BOX_OF_ATHREA_1,
            BOX_OF_ATHREA_2,
            BOX_OF_ATHREA_3,
            BOX_OF_ATHREA_4,
            BOX_OF_ATHREA_5,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case '30756-06.html':
            case '30756-08.html':
            case '30757-05.html':
            case '30758-02.html':
            case '30758-04.html':
            case '30759-02.html':
            case '30759-04.html':
                break

            case '30756-07.html':
                if ( state.isCreated()
                        && player.isClanLeader()
                        && player.getClan().getLevel() === minimumClanLevel ) {
                    state.startQuest()
                    state.setMemoState( 1 )
                    break
                }

                return

            case '30757-04.html':
                if ( Math.random() > 0.5 ) {
                    if ( state.getVariable( variableNames.flag ) !== 2501 ) {
                        await QuestHelper.giveSingleItem( player, SYMBOL_OF_LOYALTY, 1 )
                        state.setVariable( variableNames.flag, 2501 )
                    }

                    break
                }

                npc.setTarget( player )
                await npc.doCast( SkillCache.getSkill( DIE_YOU_FOOL, 1 ) )

                this.startQuestTimer( eventNames.symbolOfLoyalty, 4000, data.characterId, data.playerId )
                return this.getPath( '30757-03.html' )

            case '30758-03.html':
                let leaderState: QuestState = this.getLeaderQuestState( player )
                if ( !leaderState ) {
                    return
                }

                if ( npc.getSummonedNpcCount() < 4 ) {
                    leaderState.setMemoState( 4 )
                    leaderState.setVariable( variableNames.flag, 0 )

                    NpcVariablesManager.set( data.characterId, this.getName(), 0 )

                    locations.forEach( ( location: ILocational ) => {
                        QuestHelper.addSpawnAtLocation( _.random( BOX_OF_ATHREA_1, BOX_OF_ATHREA_5 ), location, false, 300000 )
                    } )

                    break
                }

                return this.getPath( '30758-03a.html' )

            case '30758-07.html':
                if ( player.getAdena() >= requiredAdenaForGame ) {
                    if ( npc.getSummonedNpcCount() < 4 ) {
                        await QuestHelper.takeSingleItem( player, ItemTypes.Adena, requiredAdenaForGame )
                    }

                    break
                }

                return this.getPath( '30758-06.html' )

            case '30759-03.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    state.setMemoState( 2 )

                    break
                }

                return

            case '30759-07.html':
                if ( state.isMemoState( 2 )
                        && QuestHelper.getQuestItemsCount( player, SYMBOL_OF_LOYALTY ) >= 3 ) {
                    await QuestHelper.takeSingleItem( player, SYMBOL_OF_LOYALTY, -1 )
                    await QuestHelper.giveSingleItem( player, ANTIDOTE_RECIPE_LIST, 1 )

                    npc.setTarget( player )
                    await npc.doCast( SkillCache.getSkill( POISON_OF_DEATH, 1 ) )

                    state.setConditionWithSound( 3, true )
                    state.setMemoState( 3 )

                    break
                }

                return

            case eventNames.symbolOfLoyalty:
                if ( player.isDead() && state.getVariable( variableNames.flag ) !== 2501 ) {
                    await QuestHelper.giveSingleItem( player, SYMBOL_OF_LOYALTY, 1 )
                    state.setVariable( variableNames.flag, 2501 )
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    getLeaderQuestState( player: L2PcInstance ): QuestState {
        let clan: L2Clan = ClanCache.getClan( player.getClanId() )

        if ( !clan ) {
            return
        }

        return QuestStateCache.getQuestState( clan.getLeaderId(), this.getName(), false )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getLeaderQuestState( L2World.getPlayer( data.playerId ) )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        switch ( data.npcId ) {
            case OEL_MAHUM_WITCH_DOCTOR:
                await this.onMobKill( player, state, HERB_OF_OEL_MAHUM, data.isChampion )
                return

            case HARIT_LIZARDMAN_SHAMAN:
                await this.onMobKill( player, state, HERB_OF_HARIT, data.isChampion )
                return

            case VANOR_SILENOS_SHAMAN:
                await this.onMobKill( player, state, HERB_OF_VANOR, data.isChampion )
                return

            case BOX_OF_ATHREA_1:
            case BOX_OF_ATHREA_2:
            case BOX_OF_ATHREA_3:
            case BOX_OF_ATHREA_4:
            case BOX_OF_ATHREA_5:
                let npc = L2World.getObjectById( data.targetId ) as L2Npc
                let summoner: L2Character = L2World.getObjectById( npc.getSummonerId() ) as L2Character

                if ( !summoner || !summoner.isNpc() || !state.isMemoState( 4 ) ) {
                    return
                }

                switch ( state.getVariable( variableNames.flag ) ) {
                    case 3:
                        if ( Math.random() < 0.25 || NpcVariablesManager.get( npc.getSummonerId(), this.getName() ) === 15 ) {
                            state.incrementVariable( variableNames.flag, 1 )
                            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.BINGO )
                        }

                        break

                    case 2:
                        if ( Math.random() < 0.25 || NpcVariablesManager.get( npc.getSummonerId(), this.getName() ) === 14 ) {
                            state.incrementVariable( variableNames.flag, 1 )
                            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.BINGO )
                        }

                        break

                    case 1:
                        if ( Math.random() < 0.25 || NpcVariablesManager.get( npc.getSummonerId(), this.getName() ) === 13 ) {
                            state.incrementVariable( variableNames.flag, 1 )
                            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.BINGO )
                        }

                        break

                    case 0:
                        if ( Math.random() < 0.25 || NpcVariablesManager.get( npc.getSummonerId(), this.getName() ) === 12 ) {
                            state.incrementVariable( variableNames.flag, 1 )
                            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.BINGO )
                        }

                        break
                }

                let existingValue: number = NpcVariablesManager.get( npc.getSummonerId(), this.getName() ) as number || 0
                NpcVariablesManager.set( npc.getSummonerId(), this.getName(), existingValue + 1 )

                return
        }
    }

    onMobKill( player: L2PcInstance, state: QuestState, itemId: number, isFromChampion: boolean ): Promise<void> {
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, 0.1, isFromChampion ) ) {
            return
        }

        if ( state.getMemoState() >= 3 && state.getMemoState() < 6 ) {
            return QuestHelper.rewardSingleQuestItem( player, HERB_OF_OEL_MAHUM, 1, isFromChampion )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player: L2PcInstance = L2World.getPlayer( data.playerId )
        let leaderState: QuestState = this.getLeaderQuestState( player )
        let leaderPlayer: L2PcInstance = leaderState.getPlayer()

        switch ( data.characterNpcId ) {
            case SIR_KRISTOF_RODEMAI:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.isClanLeader() ) {
                            let clan: L2Clan = player.getClan()
                            if ( clan.getLevel() < minimumClanLevel ) {
                                return this.getPath( '30756-01.html' )
                            }

                            if ( clan.getLevel() === minimumClanLevel ) {
                                if ( QuestHelper.hasQuestItem( player, ALLIANCE_MANIFESTO ) ) {
                                    return this.getPath( '30756-03.html' )
                                }

                                return this.getPath( '30756-04.html' )
                            }

                            return this.getPath( '30756-02.html' )
                        }

                        return this.getPath( '30756-05.html' )

                    case QuestStateValues.STARTED:
                        if ( state.isMemoState( 6 ) && QuestHelper.hasQuestItem( player, VOUCHER_OF_FAITH ) ) {
                            await QuestHelper.takeSingleItem( player, VOUCHER_OF_FAITH, -1 )
                            await QuestHelper.giveSingleItem( player, ALLIANCE_MANIFESTO, 1 )
                            await QuestHelper.addExpAndSp( player, 0, 120000 )
                            await state.exitQuest( false )

                            return this.getPath( '30756-09.html' )
                        }

                        return this.getPath( '30756-10.html' )
                }

                break

            case STATUE_OF_OFFERING:
                if ( leaderState && leaderState.isMemoState( 2 ) ) {
                    if ( !player.isClanLeader() ) {
                        if ( player.getLevel() >= clanMememberLevel ) {
                            return this.getPath( state.getVariable( variableNames.flag ) !== 2501 ? '30757-01.html' : '30757-01b.html' )
                        }

                        return this.getPath( '30757-02.html' )
                    }

                    return this.getPath( '30757-01a.html' )
                }

                return this.getPath( '30757-06.html' )

            case ATHREA:
                if ( !leaderState ) {
                    break
                }

                switch ( leaderState.getMemoState() ) {
                    case 3:
                        if ( QuestHelper.hasQuestItem( leaderPlayer, ANTIDOTE_RECIPE_LIST )
                                && !QuestHelper.hasQuestItem( leaderPlayer, BLOOD_OF_EVA ) ) {
                            leaderState.setVariable( variableNames.flag, 0 )

                            return this.getPath( '30758-01.html' )
                        }

                        break

                    case 4:
                        if ( leaderState.getVariable( variableNames.flag ) < 4 ) {
                            return this.getPath( '30758-05.html' )
                        }

                        await QuestHelper.giveSingleItem( player, BLOOD_OF_EVA, 1 )
                        leaderState.setMemoState( 5 )

                        return this.getPath( '30758-08.html' )


                    case 5:
                        return this.getPath( '30758-09.html' )
                }

                break

            case KALIS:
                if ( state.isMemoState( 1 ) && !QuestHelper.hasQuestItem( player, SYMBOL_OF_LOYALTY ) ) {
                    return this.getPath( '30759-01.html' )
                }

                if ( state.isMemoState( 2 ) && QuestHelper.getQuestItemsCount( player, SYMBOL_OF_LOYALTY ) < 3 ) {
                    return this.getPath( '30759-05.html' )
                }

                let hasEffect: boolean = this.hasAbnormalEffect( player )
                if ( QuestHelper.getQuestItemsCount( player, SYMBOL_OF_LOYALTY ) >= 3 && !hasEffect ) {
                    return this.getPath( '30759-06.html' )
                }

                if ( state.isMemoState( 5 )
                        && QuestHelper.hasQuestItems( player, BLOOD_OF_EVA, HERB_OF_VANOR, HERB_OF_HARIT, HERB_OF_OEL_MAHUM )
                        && hasEffect ) {

                    await QuestHelper.giveMultipleItems( player, 1, VOUCHER_OF_FAITH, POTION_OF_RECOVERY )
                    await QuestHelper.takeMultipleItems( player, -1, BLOOD_OF_EVA, ANTIDOTE_RECIPE_LIST, HERB_OF_OEL_MAHUM, HERB_OF_HARIT, HERB_OF_VANOR )

                    state.setConditionWithSound( 4, true )
                    state.setMemoState( 6 )

                    return this.getPath( '30759-08.html' )
                }

                if ( [ 3, 4, 5 ].includes( state.getMemoState() ) && !hasEffect ) {
                    await QuestHelper.takeSingleItem( player, ANTIDOTE_RECIPE_LIST, -1 )
                    state.setMemoState( 1 )

                    return this.getPath( '30759-09.html' )
                }

                if ( state.getMemoState() < 6
                        && QuestHelper.getQuestItemsCount( player, SYMBOL_OF_LOYALTY ) >= 3
                        && !QuestHelper.hasAtLeastOneQuestItem( player, BLOOD_OF_EVA, HERB_OF_VANOR, HERB_OF_HARIT, HERB_OF_OEL_MAHUM )
                        && hasEffect ) {
                    return this.getPath( '30759-10.html' )
                }

                if ( state.isMemoState( 6 ) ) {
                    return this.getPath( '30759-11.html' )
                }

                if ( leaderState && !player.isClanLeader() ) {
                    return this.getPath( '30759-12.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    hasAbnormalEffect( player: L2PcInstance ): boolean {
        return !!player.getEffectList().getBuffInfoByAbnormalType( AbnormalType.FATAL_POISON )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00501_ProofOfClanAlliance'
    }
}