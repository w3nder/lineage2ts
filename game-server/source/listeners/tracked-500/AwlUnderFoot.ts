import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { AttackableAttackedEvent, AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { InstanceManager } from '../../gameService/instancemanager/InstanceManager'
import { InstanceWorld } from '../../gameService/models/instancezone/InstanceWorld'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { Fort } from '../../gameService/models/entity/Fort'
import { L2Clan } from '../../gameService/models/L2Clan'
import { L2Party } from '../../gameService/models/L2Party'
import { DataManager } from '../../data/manager'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { Instance } from '../../gameService/models/entity/Instance'
import { Location } from '../../gameService/models/Location'
import { QuestHelper } from '../helpers/QuestHelper'
import { InstanceType } from '../../gameService/enums/InstanceType'
import { L2RaidBossInstance } from '../../gameService/models/actor/instance/L2RaidBossInstance'
import { L2Playable } from '../../gameService/models/actor/L2Playable'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import aigle from 'aigle'
import _ from 'lodash'
import { teleportCharacterToGeometryCoordinates } from '../../gameService/helpers/TeleportHelper'
import { GeometryId } from '../../gameService/enums/GeometryId'

const reEnterDuration = 14400000
const RAID_SPAWN_DELAY = 120000
const DL_MARK = 9797
const KNIGHT_EPALUETTE = 9912
const RAID_CURSE = new SkillHolder( 5456 )

interface FortDungeon {
    instanceId: number
    futureReEnterTime: number
}

function createDungeon( instanceId: number ): FortDungeon {
    return {
        futureReEnterTime: 0,
        instanceId,
    }
}

const npcDungeons = {
    35666: createDungeon( 22 ),
    35698: createDungeon( 23 ),
    35735: createDungeon( 24 ),
    35767: createDungeon( 25 ),
    35804: createDungeon( 26 ),
    35835: createDungeon( 27 ),
    35867: createDungeon( 28 ),
    35904: createDungeon( 29 ),
    35936: createDungeon( 30 ),
    35974: createDungeon( 31 ),
    36011: createDungeon( 32 ),
    36043: createDungeon( 33 ),
    36081: createDungeon( 34 ),
    36118: createDungeon( 35 ),
    36149: createDungeon( 36 ),
    36181: createDungeon( 37 ),
    36219: createDungeon( 38 ),
    36257: createDungeon( 39 ),
    36294: createDungeon( 40 ),
    36326: createDungeon( 41 ),
    36364: createDungeon( 42 ),
}

const phaseOneIds = [
    25572,
    25575,
    25578,
]

const phaseTwoIds = [
    25579,
    25582,
    25585,
    25588,
]

const phaseThreeIds = [
    25589,
    25592,
    25593,
]

const raidNpcIds = [
    phaseOneIds,
    phaseTwoIds,
    phaseThreeIds,
]

const instanceNameId = 'fortdungeon'
const npcIds = _.keys( npcDungeons ).map( value => _.parseInt( value ) )

export class AwlUnderFoot extends ListenerLogic {
    constructor() {
        super( 'Q00511_AwlUnderFoot', 'listeners/tracked-500/AwlUnderFoot.ts' )
        this.questId = 511
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    getAttackableKillIds(): Array<number> {
        return [
            ...phaseOneIds,
            ...phaseTwoIds,
            ...phaseThreeIds,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return _.range( 25572, 25596 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case 'FortressWarden-10.htm':
                state.startQuest()
                break

            case 'FortressWarden-15.htm':
                await state.exitQuest( true, true )
                break

            case 'enter':
                return this.teleportPlayerToInstance( data )
        }

        return this.getPath( data.eventName )
    }

    sendToInstance( playerId: number, instanceId: number ): Promise<void> {
        let player = L2World.getPlayer( playerId )
        return teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, 53322, 246380, -6580, 0, instanceId )
    }

    async teleportPlayerToInstance( data: NpcGeneralEvent ): Promise<string> {
        let world: InstanceWorld = InstanceManager.getPlayerWorld( data.playerId )
        if ( world ) {
            if ( world.getInstanceName() === instanceNameId ) {
                PacketDispatcher.sendOwnedData( data.playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_ENTERED_ANOTHER_INSTANT_ZONE_THEREFORE_YOU_CANNOT_ENTER_CORRESPONDING_DUNGEON ) )
                return
            }

            await this.sendToInstance( data.playerId, world.getInstanceId() )
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let conditionResponse: string = this.getFortConditionResponse( player, L2World.getObjectById( data.characterId ) as L2Npc, true )
                || this.getConditionResponseForPlayer( player )
        if ( conditionResponse ) {
            return conditionResponse
        }

        let instanceId = InstanceManager.createDynamicInstance( instanceNameId )
        let dungeon: FortDungeon = npcDungeons[ data.characterNpcId ]
        let playerInstance: Instance = InstanceManager.getInstance( instanceId )

        playerInstance.exitLocation = Location.fromObject( player )

        world = new InstanceWorld()
        world.instanceId = instanceId
        world.templateId = dungeon.instanceId
        world.status = 0

        dungeon.futureReEnterTime = Date.now() + reEnterDuration
        InstanceManager.addWorld( world )

        setTimeout( this.spawnRaid, instanceId, RAID_SPAWN_DELAY )

        let party: L2Party = player.getParty()
        let playerIds: Array<number> = party ? party.getMembers() : [ data.playerId ]

        let listener = this
        await aigle.resolve( playerIds ).each( ( playerId: number ) : Promise<void> => {
            world.addAllowedId( playerId )
            return listener.sendToInstance( playerId, instanceId )
        } )

        return DataManager.getHtmlData().getItem( this.getPath( 'FortressWarden-08.htm' ) )
                          .replace( '%clan%', player.getClan().getName() )
    }

    spawnRaid( instanceId: number ): void {
        let world: InstanceWorld = InstanceManager.getWorld( instanceId )

        let spawnIds: Array<number> = raidNpcIds[ world.getStatus() ]
        if ( !spawnIds ) {
            return
        }

        let raidBossNpc: L2Npc = QuestHelper.addGenericSpawn( null, _.sample( spawnIds ), 53319, 245814, -6576, 0, false, 0, false, instanceId )
        if ( raidBossNpc.getInstanceType() === InstanceType.L2RaidBossInstance ) {
            ( raidBossNpc as L2RaidBossInstance ).useRaidCurse = false
        }
    }

    getFortConditionResponse( player: L2PcInstance, npc: L2Npc, isEntering: boolean ): string {
        let fortress: Fort = npc.getFort()
        let dungeon: FortDungeon = npcDungeons[ npc.getId() ]
        if ( !player || !fortress || !dungeon ) {
            return this.getPath( 'FortressWarden-01.htm' )
        }

        let clan: L2Clan = player.getClan()
        if ( !clan || clan.getFortId() !== fortress.getResidenceId() ) {
            return this.getPath( 'FortressWarden-01.htm' )
        }

        if ( fortress.getFortState() === 0 ) {
            return this.getPath( 'FortressWarden-02a.htm' )
        }

        if ( fortress.getFortState() === 2 ) {
            return this.getPath( 'FortressWarden-02b.htm' )
        }

        if ( isEntering && dungeon.futureReEnterTime > Date.now() ) {
            return this.getPath( 'FortressWarden-07.htm' )
        }

        let party: L2Party = player.getParty()
        if ( !party ) {
            return this.getPath( 'FortressWarden-03.htm' )
        }

        let response: string
        let getPath: Function = this.getPath

        _.each( party.getMembers(), ( playerId: number ) => {
            let partyMember = L2World.getPlayer( playerId )

            if ( !partyMember ) {
                return
            }

            let partyMemeberClan: L2Clan = partyMember.getClan()
            let shouldSendResponse: boolean = !partyMemeberClan
                    || partyMemeberClan.getFortId() === 0
                    || partyMemeberClan.getFortId() !== fortress.getResidenceId()

            if ( shouldSendResponse ) {
                response = DataManager.getHtmlData().getItem( getPath( 'FortressWarden-05.htm' ) )
                                      .replace( '%player%', partyMember.getName() )

                return false
            }
        } )

        return response
    }

    getConditionResponseForPlayer( player: L2PcInstance ): string {
        let party: L2Party = player.getParty()
        if ( !party ) {
            return this.getPath( 'FortressWarden-03.htm' )
        }
        if ( player.getObjectId() !== party.getLeaderObjectId() ) {
            return DataManager.getHtmlData().getItem( this.getPath( 'FortressWarden-04.htm' ) )
                              .replace( '%leader%', party.getLeader().getName() )
        }

        let response: string
        let questName = this.getName()
        let getPath: Function = this.getPath

        _.each( party.getMembers(), ( playerId: number ) => {
            let partyMember = L2World.getPlayer( playerId )

            if ( !partyMember ) {
                return
            }

            let state: QuestState = QuestStateCache.getQuestState( playerId, questName, false )
            if ( !state || state.getCondition() === 0 ) {
                response = DataManager.getHtmlData().getItem( getPath( 'FortressWarden-05.htm' ) )
                                      .replace( '%player%', partyMember.getName() )

                return false
            }

            if ( !GeneralHelper.checkIfInRange( 1000, player, partyMember, true ) ) {
                response = DataManager.getHtmlData().getItem( getPath( 'FortressWarden-06.htm' ) )
                                      .replace( '%player%', partyMember.getName() )

                return false
            }
        } )

        return response
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let attacker: L2Playable = L2World.getObjectById( data.attackerId ) as L2Playable
        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        if ( ( attacker.getLevel() - npc.getLevel() ) < 9 ) {
            return
        }

        if ( attacker.getBuffCount() > 0 || attacker.getDanceCount() > 0 ) {
            npc.setTarget( attacker )
            await npc.doSimultaneousCastWithHolder( RAID_CURSE )

            return
        }

        let party: L2Party = PlayerGroupCache.getParty( data.attackerPlayerId )
        if ( !party ) {
            return
        }

        _.each( party.getMembers(), ( playerId: number ) => {
            let player = L2World.getPlayer( playerId )
            if ( player.getBuffCount() > 0 || player.getDanceCount() > 0 ) {
                npc.setTarget( player )
                npc.doSimultaneousCastWithHolder( RAID_CURSE )
            }
        } )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let world: InstanceWorld = InstanceManager.getWorld( data.instanceId )
        if ( world.getInstanceName() !== instanceNameId ) {
            return
        }

        if ( phaseThreeIds.includes( data.npcId ) ) {
            let party: L2Party = PlayerGroupCache.getParty( data.playerId )
            let questName = this.getName()

            await aigle.resolve( party ? party.getMembers() : [ data.playerId ] ).each( async ( playerId: number ) => {
                let state: QuestState = QuestStateCache.getQuestState( playerId, questName, false )

                if ( !state || !state.isCondition( 1 ) ) {
                    return
                }

                let player = L2World.getPlayer( playerId )
                await QuestHelper.rewardSingleQuestItem( player, DL_MARK, 140, data.isChampion )

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            } )

            let instanceObj: Instance = InstanceManager.getInstance( data.instanceId )
            instanceObj.setDuration( 360000 )
            instanceObj.removeNpcs()

            return
        }

        world.status = world.status + 1
        setTimeout( this.spawnRaid, data.instanceId, RAID_SPAWN_DELAY )
    }

    async rewardPlayer( playerId: number ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( playerId, this.getName(), false )

        if ( !state ) {
            return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        let response: string = this.getFortConditionResponse( player, npc, false )
        if ( response ) {
            return response
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )

        if ( state.getCondition() === 0 ) {
            if ( player.getLevel() >= 60 ) {
                return this.getPath( 'FortressWarden-09.htm' )
            }

            await state.exitQuest( true )
            return this.getPath( 'FortressWarden-00.htm' )
        }

        if ( state.getState() !== QuestStateValues.STARTED || state.getCondition() !== 1 ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        let count = QuestHelper.getQuestItemsCount( player, DL_MARK )
        if ( count > 0 ) {
            await QuestHelper.takeSingleItem( player, DL_MARK, -1 )
            await QuestHelper.rewardSingleItem( player, KNIGHT_EPALUETTE, count )

            return this.getPath( 'FortressWarden-14.htm' )
        }

        return this.getPath( 'FortressWarden-10.htm' )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00511_AwlUnderFoot'
    }
}