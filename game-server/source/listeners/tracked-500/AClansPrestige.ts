import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Clan } from '../../gameService/models/L2Clan'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { PledgeShowInfoUpdate } from '../../gameService/packets/send/PledgeShowInfoUpdate'

const VALDIS = 31331
const TYRANNOSAURUS_CLAW = 8767

export class AClansPrestige extends ListenerLogic {
    constructor() {
        super( 'Q00510_AClansPrestige', 'listeners/tracked-500/AClansPrestige.ts' )
        this.questId = 510
    }

    getQuestStartIds(): Array<number> {
        return [ VALDIS ]
    }

    getTalkIds(): Array<number> {
        return [ VALDIS ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            22215,
            22216,
            22217
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        switch ( data.eventName ) {
            case '31331-3.html':
                state.startQuest()
                break

            case '31331-6.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        let clan: L2Clan = player.getClan()
        if ( !clan ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( clan.getLeaderId(), this.getName(), false )
        if ( !state || !state.isStarted() ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let leader: L2PcInstance = clan.getLeader().getPlayerInstance()

        if ( !GeneralHelper.checkIfInRange( 1500, leader, npc, true ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, TYRANNOSAURUS_CLAW, 1, data.isChampion )
        leader.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )
        let clan: L2Clan = player.getClan()

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let isNotValid: boolean = !clan || !player.isClanLeader() || clan.getLevel() < 5
                return this.getPath( isNotValid ? '31331-0.htm' : '31331-1.htm' )

            case QuestStateValues.STARTED:
                if ( !clan || !player.isClanLeader() ) {
                    await state.exitQuest( true )

                    return this.getPath( '31331-8.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, TYRANNOSAURUS_CLAW ) ) {
                    return this.getPath( '31331-4.html' )
                }

                let count : number = QuestHelper.getQuestItemsCount( player, TYRANNOSAURUS_CLAW )
                let points : number = 30 * count + ( count < 10 ? 0 : 59 )

                await QuestHelper.takeSingleItem( player, TYRANNOSAURUS_CLAW, -1 )
                await clan.addReputationScore( points, true )

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_FANFARE_1 )

                let message: Buffer = new SystemMessageBuilder( SystemMessageIds.CLAN_QUEST_COMPLETED_AND_S1_POINTS_GAINED )
                        .addNumber( points )
                        .getBuffer()
                player.sendOwnedData( message )
                clan.broadcastDataToOnlineMembers( PledgeShowInfoUpdate( clan ) )

                return this.getPath( '31331-7.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00510_AClansPrestige'
    }
}