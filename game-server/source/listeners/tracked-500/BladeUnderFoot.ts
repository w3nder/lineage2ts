import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2Party } from '../../gameService/models/L2Party'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const npcIds: Array<number> = [
    36403, // Gludio
    36404, // Dion
    36405, // Giran
    36406, // Oren
    36407, // Aden
    36408, // Innadril
    36409, // Goddard
    36410, // Rune
    36411, // Schuttgart
]

const minimumLevel = 70
const FRAGMENT_OF_THE_DUNGEON_LEADER_MARK = 9798
const KNIGHTS_EPAULETTE = 9912

type MonsterDropData = [ number, number ] // no party amount, party amount
const monsterDropChances: { [ npcId: number ]: MonsterDropData } = {
    25563: [ 175, 1443 ], // Beautiful Atrielle
    25566: [ 176, 1447 ], // Nagen the Tomboy
    25569: [ 177, 1450 ], // Jax the Destroyer
}

export class BladeUnderFoot extends ListenerLogic {
    constructor() {
        super( 'Q00512_BladeUnderFoot', 'listeners/tracked-500/BladeUnderFoot.ts' )
        this.questId = 512
        this.questItemIds = [ FRAGMENT_OF_THE_DUNGEON_LEADER_MARK ]
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterDropChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '36403-02.htm':
                state.startQuest()
                break

            case '36403-04.html':
            case '36403-05.html':
            case '36403-06.html':
            case '36403-07.html':
            case '36403-10.html':
                break

            case '36403-11.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance, npc: L2Npc ): Promise<void> => {
            if ( !state ) {
                return
            }

            if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
                return
            }

            let party: L2Party = PlayerGroupCache.getParty( player.getObjectId() )
            let [ singlePlayerAmount, partyAmount ] = monsterDropChances[ npc.getId() ]
            let amount: number = party && party.getMemberCount() > 1 ? ( partyAmount / party.getMemberCount() ) : singlePlayerAmount

            await QuestHelper.rewardSingleQuestItem( player, FRAGMENT_OF_THE_DUNGEON_LEADER_MARK, amount, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        } )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                let hasValidResidence: boolean = npc.isLord( player )
                        || ( npc.getCastle().getResidenceId() === player.getClan().getCastleId()
                                && ( player.getClan().getCastleId() > 0 ) )

                if ( player.getLevel() >= minimumLevel ) {
                    return this.getPath( hasValidResidence ? '36403-01.htm' : '36403-03.htm' )
                }

                return this.getPath( '36403-08.htm' )

            case QuestStateValues.STARTED:
                let amount = QuestHelper.getQuestItemsCount( player, FRAGMENT_OF_THE_DUNGEON_LEADER_MARK )
                if ( amount > 0 ) {
                    await QuestHelper.takeSingleItem( player, FRAGMENT_OF_THE_DUNGEON_LEADER_MARK, -1 )
                    await QuestHelper.rewardSingleItem( player, KNIGHTS_EPAULETTE, amount )

                    return this.getPath( '36403-09.html' )
                }

                return this.getPath( '36403-12.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00512_BladeUnderFoot'
    }
}