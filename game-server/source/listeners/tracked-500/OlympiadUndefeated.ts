import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import {
    EventType,
    NpcGeneralEvent,
    NpcTalkEvent,
    OlympiadMatchResultEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestType } from '../../gameService/enums/QuestType'
import { QuestStateValues } from '../../gameService/models/quest/State'
import aigle from 'aigle'

const MANAGER = 31688
const WIN_CONF_2 = 17244
const WIN_CONF_5 = 17245
const WIN_CONF_10 = 17246
const OLY_CHEST = 17169
const MEDAL_OF_GLORY = 21874
const minimumLevel = 75

const variableNames = {
    undefeatedCount: 'uc',
}

export class OlympiadUndefeated extends ListenerLogic {
    constructor() {
        super( 'Q00553_OlympiadUndefeated', 'listeners/tracked-500/OlympiadUndefeated.ts' )
        this.questId = 553
        this.questItemIds = [
            WIN_CONF_2,
            WIN_CONF_5,
            WIN_CONF_10,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ MANAGER ]
    }

    getTalkIds(): Array<number> {
        return [ MANAGER ]
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.OlympiadMatchResult,
                method: this.processOlympiadMatchResult.bind( this ),
                ids: null,
            },
        ]
    }

    async processOlympiadMatchResult( data: OlympiadMatchResultEvent ): Promise<void> {
        await aigle.resolve( data.playerIds ).each( ( id: number ) => this.processEachOlympiadPlayer( data, id ) )
    }

    async processEachOlympiadPlayer( data: OlympiadMatchResultEvent, playerId: number ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( playerId, this.getName(), false )

        if ( !state || !state.isStarted() || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( playerId )

        if ( data.winnerPlayerId !== playerId ) {
            state.unsetVariable( variableNames.undefeatedCount )
            await QuestHelper.takeMultipleItems( player, -1, WIN_CONF_2, WIN_CONF_5, WIN_CONF_10 )

            return
        }

        let matches = state.getVariable( variableNames.undefeatedCount ) || 0
        matches++

        switch ( matches ) {
            case 2:
                if ( !QuestHelper.hasQuestItem( player, WIN_CONF_2 ) ) {
                    await QuestHelper.giveSingleItem( player, WIN_CONF_2, 1 )
                }

                return

            case 5:
                if ( !QuestHelper.hasQuestItem( player, WIN_CONF_5 ) ) {
                    await QuestHelper.giveSingleItem( player, WIN_CONF_5, 1 )
                }

                return

            case 10:
                if ( !QuestHelper.hasQuestItem( player, WIN_CONF_10 ) ) {
                    await QuestHelper.giveSingleItem( player, WIN_CONF_10, 1 )
                    state.setConditionWithSound( 2 )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31688-03.html':
                state.startQuest()
                break

            case '31688-04.html':
                let amount: number = QuestHelper.getItemsSumCount( player, WIN_CONF_2, WIN_CONF_5 )

                if ( amount > 0 ) {
                    await QuestHelper.rewardSingleItem( player, OLY_CHEST, amount )
                    if ( amount === 2 ) {
                        await QuestHelper.rewardSingleItem( player, MEDAL_OF_GLORY, 3 )
                    }

                    await state.exitQuestWithType( QuestType.DAILY, true )
                }

                return QuestHelper.getNoQuestMessagePath()
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        if ( player.getLevel() < minimumLevel || !player.isNoble() ) {
            return this.getPath( '31688-00.htm' )
        }

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( '31688-01.htm' )

            case QuestStateValues.STARTED:
                let amount: number = QuestHelper.getItemsSumCount( player, WIN_CONF_2, WIN_CONF_5, WIN_CONF_10 )
                if ( amount === 3 && state.isCondition( 2 ) ) {
                    await QuestHelper.rewardSingleItem( player, OLY_CHEST, 4 )
                    await QuestHelper.rewardSingleItem( player, MEDAL_OF_GLORY, 5 )
                    await state.exitQuestWithType( QuestType.DAILY, true )

                    return this.getPath( '31688-04.html' )
                }

                return this.getPath( `31688-w${ amount }.html` )

            case QuestStateValues.COMPLETED:
                if ( state.isNowAvailable() ) {
                    state.setState( QuestStateValues.CREATED )

                    return this.getPath( '31688-01.htm' )
                }

                return this.getPath( '31688-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00553_OlympiadUndefeated'
    }
}