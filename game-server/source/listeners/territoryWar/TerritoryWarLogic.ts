import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { TWPlayerLoginEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

export const enum TerritoryWarVariableNames {
    Kills = 'TW.kills',
    Catapults = 'TW.catapult',
    KillsRequired = 'TW.killMax',
    FinishTime = 'TW.endTime'
}

export abstract class TerritoryWarLogic extends ListenerLogic {
    async onPlayerLogin( data: TWPlayerLoginEvent ): Promise<void> {
        let state = QuestStateCache.getQuestState( data.playerId, this.name, true )
        if ( !state.isStarted() ) {
            state.startQuest( false, 1 )
        }
    }

    processStepsForHonor( playerId: number ): void {
        let state = QuestStateCache.getQuestState( playerId, 'Q00176_StepsForHonor', false )
        if ( !state || !state.isStarted() ) {
            return
        }

        let value = state.getCondition()
        if ( value === 1 || value === 3 || value === 5 || value === 7 ) {
            state.incrementVariable( TerritoryWarVariableNames.Kills, 1 )

            let kills = state.getVariable( TerritoryWarVariableNames.Kills )

            if ( value === 1 && kills >= 9 ) {
                state.setConditionWithSound( 2 )
                state.unsetVariable( TerritoryWarVariableNames.Kills )

                return
            }

            if ( value === 3 && kills >= 18 ) {
                state.setConditionWithSound( 4 )
                state.unsetVariable( TerritoryWarVariableNames.Kills )

                return
            }

            if ( value === 5 && kills >= 27 ) {
                state.setConditionWithSound( 6 )
                state.unsetVariable( TerritoryWarVariableNames.Kills )

                return
            }

            if ( value === 7 && kills >= 36 ) {
                state.setConditionWithSound( 8 )
                state.unsetVariable( TerritoryWarVariableNames.Kills )

                return
            }
        }
    }

    processBecomeMercenaryQuest( playerId: number, useCatapult: boolean ): void {
        let state = QuestStateCache.getQuestState( playerId, 'Q00147_PathtoBecominganEliteMercenary', false )
        if ( !state ) {
            return
        }

        let requireKills = 10
        let requireCapaults = 1

        if ( state.isCompleted() ) {
            state = QuestStateCache.getQuestState( playerId, 'Q00148_PathtoBecominganExaltedMercenary', false )
            requireKills = 30
            requireCapaults = 2
        }

        if ( !state || !state.isStarted() ) {
            return
        }

        let value = state.getCondition()

        if ( useCatapult ) {
            if ( value === 1 || value === 2 ) {
                state.incrementVariable( TerritoryWarVariableNames.Catapults, 1 )

                let catapults = state.getVariable( TerritoryWarVariableNames.Catapults )
                if ( catapults > requireCapaults ) {
                    state.setConditionWithSound( value === 1 ? 3 : 4 )
                }
            }

            return
        }

        if ( value === 1 || value === 3 ) {
            state.incrementVariable( TerritoryWarVariableNames.Kills, 1 )

            let kills = state.getVariable( TerritoryWarVariableNames.Kills )
            if ( kills > requireKills ) {
                state.setConditionWithSound( value === 1 ? 2 : 4 )
            }
        }
    }

    getPathPrefix() : string {
        return 'no-path'
    }

    abstract getDescription(): string
}