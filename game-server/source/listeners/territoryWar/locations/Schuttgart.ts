import { TWLocationLogic } from '../TWLocationLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'

export class Schuttgart extends TWLocationLogic {
    catapultId: number = 36507
    guardNpcIds: Array<number> = [
        36557,
        36559,
        36560,
    ]

    leaderIds: Set<number> = new Set<number>( [
        36556,
        36558,
        36561,
        36599,
    ] )

    territoryId: number = 89

    constructor() {
        super( 'Q00725_ForTheSakeOfTheTerritorySchuttgart', 'listeners/territoryWar/locations/Schuttgart.ts' )
        this.questId = 725
    }

    getMessageId(): number {
        return NpcStringIds.THE_CATAPULT_OF_SCHUTTGART_HAS_BEEN_DESTROYED
    }

    getDescription(): string {
        return 'For the Sake of the Territory - Schuttgart'
    }
}