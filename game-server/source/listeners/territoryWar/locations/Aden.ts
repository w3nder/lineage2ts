import { TWLocationLogic } from '../TWLocationLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'

export class Aden extends TWLocationLogic {
    catapultId: number = 36503
    guardNpcIds: Array<number> = [
        36533,
        36535,
        36536
    ]

    leaderIds: Set<number> = new Set<number>( [
        36532,
        36534,
        36537,
        36595
    ] )

    territoryId: number = 85

    constructor() {
        super( 'Q00721_ForTheSakeOfTheTerritoryAden', 'listeners/territoryWar/locations/Aden.ts' )
        this.questId = 721
    }

    getMessageId(): number {
        return NpcStringIds.THE_CATAPULT_OF_ADEN_HAS_BEEN_DESTROYED
    }

    getDescription(): string {
        return 'For the Sake of the Territory - Aden'
    }
}