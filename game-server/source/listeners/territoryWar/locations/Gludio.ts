import { TWLocationLogic } from '../TWLocationLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'

export class Gludio extends TWLocationLogic {
    catapultId: 36499
    guardNpcIds: Array<number> = [
        36509,
        36511,
        36512
    ]

    leaderIds: Set<number> = new Set<number>( [
        36508,
        36510,
        36513,
        36591
    ] )

    territoryId: number = 81

    constructor() {
        super( 'Q00717_ForTheSakeOfTheTerritoryGludio', 'listeners/territoryWar/Gludio.ts' )
        this.questId = 717
    }

    getMessageId(): number {
        return NpcStringIds.THE_CATAPULT_OF_GLUDIO_HAS_BEEN_DESTROYED
    }

    getDescription(): string {
        return 'For the Sake of the Territory - Gludio'
    }
}