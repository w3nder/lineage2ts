import { TWLocationLogic } from '../TWLocationLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'

export class Giran extends TWLocationLogic {
    catapultId: number = 36500
    guardNpcIds: Array<number> = [
        36515,
        36517,
        36518
    ]

    leaderIds: Set<number> = new Set<number>( [
        36514,
        36516,
        36519,
        36592
    ] )

    territoryId: number = 82

    constructor() {
        super( 'Q00719_ForTheSakeOfTheTerritoryGiran', 'listeners/territoryWar/locations/Giran.ts' )
        this.questId = 719
    }

    getMessageId(): number {
        return NpcStringIds.THE_CATAPULT_OF_DION_HAS_BEEN_DESTROYED
    }

    getDescription(): string {
        return 'For the Sake of the Territory - Giran'
    }
}