import { TWLocationLogic } from '../TWLocationLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'

export class Goddard extends TWLocationLogic {
    catapultId: number = 36505
    guardNpcIds: Array<number> = [
        36545,
        36547,
        36548
    ]

    leaderIds: Set<number> = new Set<number>( [
        36544,
        36546,
        36549,
        36597
    ] )

    territoryId: number = 87

    constructor() {
        super( 'Q00723_ForTheSakeOfTheTerritoryGoddard', 'listeners/territoryWar/locations/Goddard.ts' )
        this.questId = 723
    }

    getMessageId(): number {
        return NpcStringIds.THE_CATAPULT_OF_GODDARD_HAS_BEEN_DESTROYED
    }

    getDescription(): string {
        return 'For the Sake of the Territory - Goddard'
    }
}