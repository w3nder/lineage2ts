import { TerritoryWarLogic } from './TerritoryWarLogic'
import { ListenerDescription } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { AttackableInitialAttackEvent, EventType } from '../../gameService/models/events/EventType'
import { InstanceType } from '../../gameService/enums/InstanceType'
import { TerritoryWarManager } from '../../gameService/instancemanager/TerritoryWarManager'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'

export abstract class TWProtectionLogic extends TerritoryWarLogic {
    abstract protectNpcIds : Array<number>

    async load(): Promise<void> {
        this.protectNpcIds.forEach( ( value : number ) => {
            let territoryId = this.getTerritoryId( value )
            if ( territoryId < 81 || territoryId > 89 ) {
                throw new Error( `Territory War Protection quest ${this.name} has wrong npc ids!` )
            }
        } )
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.AttackableInitialAttack,
                method: this.onInitialAttack.bind( this ),
                ids: this.protectNpcIds,
                triggers: {
                    targetIds: new Set<number>( [ InstanceType.L2PcInstance ] )
                }
            }
        ]
    }

    async onInitialAttack( data: AttackableInitialAttackEvent ) : Promise<void> {
        let territoryId = this.getTerritoryId( data.targetNpcId )
        TerritoryWarManager.getTerritoryPlayerIds( territoryId ).forEach( ( objectId: number ) => {
            let state = QuestStateCache.getQuestState( objectId, this.name, true )
            if ( !state.isStarted() ) {
                state.startQuest( false )
            }
        } )
    }

    abstract getTerritoryId( npcId: number ) : number
}