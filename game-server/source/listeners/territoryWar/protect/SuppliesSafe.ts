import { TWProtectionLogic } from '../TWProtectionLogic'

export class SuppliesSafe extends TWProtectionLogic {
    protectNpcIds: Array<number> = [
        36591,
        36592,
        36593,
        36594,
        36595,
        36596,
        36597,
        36598,
        36599
    ]

    constructor() {
        super( 'Q00730_ProtectTheSuppliesSafe', 'listeners/territoryWar/protect/SuppliesSafe.ts' )
        this.questId = 730
    }

    getTerritoryId( npcId: number ): number {
        return npcId - 36510
    }

    getDescription(): string {
        return 'Protect the Supplies Safe'
    }
}