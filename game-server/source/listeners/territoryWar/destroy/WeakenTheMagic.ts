import { TWDestructionLogic } from '../TWDestructionLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'
import _ from 'lodash'

export class WeakenTheMagic extends TWDestructionLogic {
    classIds: Set<number> = new Set<number>( [
        12,
        13,
        14,
        27,
        28,
        40,
        41,
        94,
        95,
        96,
        103,
        104,
        110,
        111,
    ] )

    constructor() {
        super( 'Q00736_WeakenTheMagic', 'listeners/territoryWar/destroy/WeakenTheMagic.ts' )
        this.questId = 736
    }

    getContinueMessageId(): number {
        return NpcStringIds.YOU_HAVE_DEFEATED_S2_OF_S1_ENEMIES
    }

    getFinishMessageId(): number {
        return NpcStringIds.YOU_WEAKENED_THE_ENEMYS_MAGIC
    }

    getMaxKills(): number {
        return _.random( 10, 15 )
    }

    getDescription(): string {
        return 'Weaken the magic!'
    }
}