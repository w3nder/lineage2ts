import { TWDestructionLogic } from '../TWDestructionLogic'
import { NpcStringIds } from '../../../gameService/packets/NpcStringIds'
import _ from 'lodash'

export class MakeSpearsDull extends TWDestructionLogic {
    classIds: Set<number> = new Set<number>( [
        2,
        3,
        8,
        9,
        21,
        23,
        24,
        34,
        36,
        37,
        46,
        48,
        55,
        88,
        89,
        92,
        93,
        100,
        101,
        102,
        107,
        108,
        109,
        113,
        114,
        117,
        127,
        128,
        129,
        130,
        131,
        132,
        133,
        134,
        135,
        136,
    ] )

    constructor() {
        super( 'Q00735_MakeSpearsDull', 'listeners/territoryWar/destroy/MakeSpearsDull.ts' )
        this.questId = 735
    }

    getContinueMessageId(): number {
        return NpcStringIds.YOU_HAVE_DEFEATED_S2_OF_S1_WARRIORS_AND_ROGUES
    }

    getFinishMessageId(): number {
        return NpcStringIds.YOU_WEAKENED_THE_ENEMYS_ATTACK
    }

    getMaxKills(): number {
        return _.random( 15, 20 )
    }

    getDescription(): string {
        return 'Make Spears Dull!'
    }
}