import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import _ from 'lodash'

const PREFECT_KARUKIA = 30570
const PREFRCT_KASMAN = 30501
const PREFRCT_TAZEER = 31978
const GREEN_BLOOD = 1578
const GOBLIN_DWELLING_MAP = 1579
const KURUKA_RATMAN_TOOTH = 1580
const BETRAYER_UMBAR_REPORT = 1589
const BETRAYER_ZAKAN_REPORT = 1590
const HEAD_OF_BETRAYER = 1591
const TIMORA_ORC_HEAD = 8544
const MARK_OF_RAIDER = 1592
const KURUKA_RATMAN_LEADER = 27045
const UMBAR_ORC = 27054
const TIMORA_ORC = 27320
const GOBLIN_TOMB_RAIDER_LEADER = 20320
const minimumLevel = 18

export class PathOfTheOrcRaider extends ListenerLogic {
    constructor() {
        super( 'Q00414_PathOfTheOrcRaider', 'listeners/tracked-400/PathOfTheOrcRaider.ts' )
        this.questId = 414
        this.questItemIds = [
            GREEN_BLOOD,
            GOBLIN_DWELLING_MAP,
            KURUKA_RATMAN_TOOTH,
            BETRAYER_UMBAR_REPORT,
            BETRAYER_ZAKAN_REPORT,
            HEAD_OF_BETRAYER,
            TIMORA_ORC_HEAD,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ PREFECT_KARUKIA ]
    }

    getTalkIds(): Array<number> {
        return [ PREFECT_KARUKIA, PREFRCT_KASMAN, PREFRCT_TAZEER ]
    }

    getAttackableKillIds(): Array<number> {
        return [ KURUKA_RATMAN_LEADER, UMBAR_ORC, TIMORA_ORC, GOBLIN_TOMB_RAIDER_LEADER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00414_PathOfTheOrcRaider'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.orcFighter.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, MARK_OF_RAIDER ) ) {
                            return this.getPath( '30570-04.htm' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, GOBLIN_DWELLING_MAP ) ) {
                            await QuestHelper.giveSingleItem( player, GOBLIN_DWELLING_MAP, 1 )
                        }

                        state.startQuest()
                        return this.getPath( '30570-05.htm' )
                    }

                    return this.getPath( '30570-02.htm' )
                }

                if ( player.getClassId() === ClassIdValues.orcRaider.id ) {
                    return this.getPath( '30570-02a.htm' )
                }

                return this.getPath( '30570-03.htm' )

            case '30570-07a.html':
                if ( QuestHelper.hasQuestItem( player, GOBLIN_DWELLING_MAP )
                        && QuestHelper.getQuestItemsCount( player, KURUKA_RATMAN_TOOTH ) >= 10 ) {
                    await QuestHelper.takeMultipleItems( player, -1, GOBLIN_DWELLING_MAP, KURUKA_RATMAN_TOOTH )
                    await QuestHelper.giveMultipleItems( player, -1, BETRAYER_UMBAR_REPORT, BETRAYER_ZAKAN_REPORT )

                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '30570-07b.html':
                if ( QuestHelper.hasQuestItem( player, GOBLIN_DWELLING_MAP )
                        && QuestHelper.getQuestItemsCount( player, KURUKA_RATMAN_TOOTH ) >= 10 ) {
                    await QuestHelper.takeMultipleItems( player, -1, GOBLIN_DWELLING_MAP, KURUKA_RATMAN_TOOTH )

                    state.setConditionWithSound( 5, true )
                    state.setMemoState( 2 )
                    break
                }

                return

            case '31978-04.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case '31978-02.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 6, true )

                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case GOBLIN_TOMB_RAIDER_LEADER:
                if ( QuestHelper.hasQuestItem( player, GOBLIN_DWELLING_MAP )
                        && QuestHelper.getQuestItemsCount( player, KURUKA_RATMAN_TOOTH ) < 10
                        && ( QuestHelper.getQuestItemsCount( player, GREEN_BLOOD ) <= 20 ) ) {
                    if ( _.random( 100 ) < QuestHelper.getQuestItemsCount( player, GREEN_BLOOD ) * 5 ) {
                        await QuestHelper.takeSingleItem( player, GREEN_BLOOD, -1 )
                        QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( KURUKA_RATMAN_LEADER, npc, true, 0, true ), player )

                        return
                    }

                    await QuestHelper.rewardSingleQuestItem( player, GREEN_BLOOD, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case KURUKA_RATMAN_LEADER:
                if ( QuestHelper.hasQuestItem( player, GOBLIN_DWELLING_MAP )
                        && QuestHelper.getQuestItemsCount( player, KURUKA_RATMAN_TOOTH ) < 10 ) {
                    await QuestHelper.takeSingleItem( player, GREEN_BLOOD, -1 )
                    await QuestHelper.rewardAndProgressState( player, state, KURUKA_RATMAN_TOOTH, 1, 10, data.isChampion, 2 )
                }

                return

            case UMBAR_ORC:
                if ( Math.random() < QuestHelper.getAdjustedChance( HEAD_OF_BETRAYER, 0.2, data.isChampion )
                        && QuestHelper.getQuestItemsCount( player, HEAD_OF_BETRAYER ) < 2 ) {

                    let hasZakanReport: boolean = QuestHelper.hasQuestItem( player, BETRAYER_ZAKAN_REPORT )
                    let hasUmbarReport: boolean = QuestHelper.hasQuestItem( player, BETRAYER_UMBAR_REPORT )

                    if ( !hasZakanReport && !hasUmbarReport ) {
                        return
                    }

                    if ( hasZakanReport ) {
                        await QuestHelper.takeSingleItem( player, BETRAYER_ZAKAN_REPORT, 1 )
                    } else if ( hasUmbarReport ) {
                        await QuestHelper.takeSingleItem( player, BETRAYER_UMBAR_REPORT, 1 )
                    }

                    await QuestHelper.rewardAndProgressState( player, state, HEAD_OF_BETRAYER, 1, 2, data.isChampion, 4 )
                }

                return

            case TIMORA_ORC:
                if ( Math.random() < QuestHelper.getAdjustedChance( TIMORA_ORC_HEAD, 0.6, data.isChampion )
                        && state.isMemoState( 3 )
                        && !QuestHelper.hasQuestItem( player, TIMORA_ORC_HEAD ) ) {
                    await QuestHelper.giveSingleItem( player, TIMORA_ORC_HEAD, 1 )
                    state.setConditionWithSound( 7, true )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === PREFECT_KARUKIA ) {
                    return this.getPath( '30570-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case PREFECT_KARUKIA:
                        if ( QuestHelper.hasQuestItem( player, GOBLIN_DWELLING_MAP ) ) {
                            let toothAmount: number = QuestHelper.getQuestItemsCount( player, KURUKA_RATMAN_TOOTH )
                            if ( toothAmount < 10 ) {
                                return this.getPath( '30570-06.html' )
                            }

                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, BETRAYER_UMBAR_REPORT, BETRAYER_ZAKAN_REPORT ) ) {
                                return this.getPath( '30570-07.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, HEAD_OF_BETRAYER )
                                || QuestHelper.hasAtLeastOneQuestItem( player, BETRAYER_UMBAR_REPORT, BETRAYER_ZAKAN_REPORT ) ) {
                            return this.getPath( '30570-08.html' )
                        }

                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '30570-07b.html' )
                        }

                        break

                    case PREFRCT_KASMAN:
                        switch ( QuestHelper.getQuestItemsCount( player, HEAD_OF_BETRAYER ) ) {
                            case 0:
                                if ( QuestHelper.hasQuestItems( player, BETRAYER_UMBAR_REPORT, BETRAYER_ZAKAN_REPORT ) ) {
                                    return this.getPath( '30501-01.html' )
                                }

                                break

                            case 1:
                                return this.getPath( '30501-02.html' )

                            default:
                                await QuestHelper.giveAdena( player, 163800, true )
                                await QuestHelper.giveSingleItem( player, MARK_OF_RAIDER, 1 )

                                if ( player.getLevel() >= 20 ) {
                                    await QuestHelper.addExpAndSp( player, 320534, 21312 )
                                } else if ( player.getLevel() === 19 ) {
                                    await QuestHelper.addExpAndSp( player, 456128, 28010 )
                                } else {
                                    await QuestHelper.addExpAndSp( player, 591724, 34708 )
                                }

                                await state.exitQuest( false, true )
                                player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                                return this.getPath( '30501-03.html' )
                        }

                        break

                    case PREFRCT_TAZEER:
                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '31978-01.html' )
                        }

                        if ( state.isMemoState( 3 ) ) {
                            if ( !QuestHelper.hasQuestItem( player, TIMORA_ORC_HEAD ) ) {
                                return this.getPath( '31978-03.html' )
                            }

                            await QuestHelper.giveAdena( player, 81900, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_RAIDER, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 160267, 10656 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 228064, 14005 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 295862, 17354 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '31978-05.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}