import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const MASTER_VIRGIL = 30329
const KALINTA = 30422
const PALLUS_TALISMAN = 1237
const LYCANTHROPE_SKULL = 1238
const VIRGILS_LETTER = 1239
const MORTE_TALISMAN = 1240
const VENOMOUS_SPIDERS_CARAPACE = 1241
const ARACHNID_TRACKER_SILK = 1242
const COFFIN_OF_ETERNAL_REST = 1243
const GAZE_OF_ABYSS = 1244
const VENOMOUS_SPIDER = 20038
const ARACHNID_TRACKER = 20043
const LYCANTHROPE = 20049
const MIN_LEVEL = 18

export class PathOfThePalusKnight extends ListenerLogic {
    constructor() {
        super( 'Q00410_PathOfThePalusKnight', 'listeners/tracked-400/PathOfThePalusKnight.ts' )
        this.questId = 410
        this.questItemIds = [
            PALLUS_TALISMAN,
            LYCANTHROPE_SKULL,
            VIRGILS_LETTER,
            MORTE_TALISMAN,
            VENOMOUS_SPIDERS_CARAPACE,
            ARACHNID_TRACKER_SILK,
            COFFIN_OF_ETERNAL_REST,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00410_PathOfThePalusKnight'
    }

    getQuestStartIds(): Array<number> {
        return [ MASTER_VIRGIL ]
    }

    getTalkIds(): Array<number> {
        return [ MASTER_VIRGIL, KALINTA ]
    }

    getAttackableKillIds(): Array<number> {
        return [ VENOMOUS_SPIDER, ARACHNID_TRACKER, LYCANTHROPE ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.darkFighter.id ) {
                    if ( player.getLevel() >= MIN_LEVEL ) {
                        if ( QuestHelper.hasQuestItem( player, GAZE_OF_ABYSS ) ) {
                            return this.getPath( '30329-04.htm' )
                        }

                        return this.getPath( '30329-05.htm' )
                    }

                    return this.getPath( '30329-02.htm' )
                }

                if ( player.getClassId() === ClassIdValues.palusKnight.id ) {
                    return this.getPath( '30329-02a.htm' )
                }

                return this.getPath( '30329-03.htm' )

            case '30329-06.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, PALLUS_TALISMAN, 1 )
                break

            case '30329-10.html':
                if ( QuestHelper.hasQuestItems( player, PALLUS_TALISMAN, LYCANTHROPE_SKULL ) ) {
                    await QuestHelper.takeMultipleItems( player, -1, PALLUS_TALISMAN, LYCANTHROPE_SKULL )
                    await QuestHelper.giveSingleItem( player, VIRGILS_LETTER, 1 )

                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '30422-02.html':
                if ( QuestHelper.hasQuestItem( player, VIRGILS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, VIRGILS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, MORTE_TALISMAN, 1 )

                    state.setConditionWithSound( 4, true )
                    break
                }
                return

            case '30422-06.html':
                if ( QuestHelper.hasQuestItems( player, MORTE_TALISMAN, ARACHNID_TRACKER_SILK, VENOMOUS_SPIDERS_CARAPACE ) ) {
                    await QuestHelper.takeMultipleItems( player, -1, MORTE_TALISMAN, VENOMOUS_SPIDERS_CARAPACE, ARACHNID_TRACKER_SILK )
                    await QuestHelper.giveSingleItem( player, COFFIN_OF_ETERNAL_REST, 1 )

                    state.setConditionWithSound( 6, true )
                    break
                }
                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case VENOMOUS_SPIDER: {
                if ( QuestHelper.hasQuestItem( player, MORTE_TALISMAN ) && !QuestHelper.hasQuestItem( player, VENOMOUS_SPIDERS_CARAPACE ) ) {
                    await QuestHelper.giveSingleItem( player, VENOMOUS_SPIDERS_CARAPACE, 1 )
                    this.progressQuest( player, state )
                }
                return
            }
            case ARACHNID_TRACKER:
                if ( QuestHelper.hasQuestItem( player, MORTE_TALISMAN )
                        && QuestHelper.getQuestItemsCount( player, ARACHNID_TRACKER_SILK ) < 5 ) {
                    await QuestHelper.rewardSingleQuestItem( player, ARACHNID_TRACKER_SILK, 1, data.isChampion )

                    if ( !this.progressQuest( player, state ) ) {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    }
                }

                return

            case LYCANTHROPE:
                if ( QuestHelper.hasQuestItem( player, PALLUS_TALISMAN )
                        && QuestHelper.getQuestItemsCount( player, LYCANTHROPE_SKULL ) < 13 ) {
                    await QuestHelper.rewardAndProgressState( player, state, LYCANTHROPE_SKULL, 1, 13, data.isChampion, 2 )
                }

                return
        }
    }

    progressQuest( player: L2PcInstance, state: QuestState ): boolean {
        if ( QuestHelper.hasQuestItem( player, VENOMOUS_SPIDERS_CARAPACE )
                && QuestHelper.getQuestItemsCount( player, ARACHNID_TRACKER_SILK ) >= 5 ) {
            state.setConditionWithSound( 5, true )
            return true
        }

        return false
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MASTER_VIRGIL ) {
                    return this.getPath( '30329-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MASTER_VIRGIL:
                        if ( QuestHelper.hasQuestItem( player, PALLUS_TALISMAN ) ) {
                            if ( !QuestHelper.hasQuestItem( player, LYCANTHROPE_SKULL ) ) {
                                return this.getPath( '30329-07.html' )
                            }

                            if ( QuestHelper.getQuestItemsCount( player, LYCANTHROPE_SKULL ) < 13 ) {
                                return this.getPath( '30329-08.html' )
                            }

                            return this.getPath( '30329-09.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, COFFIN_OF_ETERNAL_REST ) ) {
                            await QuestHelper.giveAdena( player, 163800, true )
                            await QuestHelper.giveSingleItem( player, GAZE_OF_ABYSS, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 26212 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 32910 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 591724, 39608 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30329-11.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, VIRGILS_LETTER, MORTE_TALISMAN ) ) {
                            return this.getPath( '30329-12.html' )
                        }

                        break

                    case KALINTA:
                        if ( QuestHelper.hasQuestItem( player, VIRGILS_LETTER ) ) {
                            return this.getPath( '30422-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MORTE_TALISMAN ) ) {
                            let silkAmount: number = QuestHelper.getQuestItemsCount( player, ARACHNID_TRACKER_SILK )

                            if ( silkAmount === 0 ) {
                                if ( !QuestHelper.hasQuestItem( player, VENOMOUS_SPIDERS_CARAPACE ) ) {
                                    return this.getPath( '30422-03.html' )
                                }

                                return this.getPath( '30422-04.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, VENOMOUS_SPIDERS_CARAPACE ) ) {
                                if ( silkAmount >= 5 ) {
                                    return this.getPath( '30422-05.html' )
                                }

                                return this.getPath( '30422-04.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, COFFIN_OF_ETERNAL_REST ) ) {
                            return this.getPath( '30422-06.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}