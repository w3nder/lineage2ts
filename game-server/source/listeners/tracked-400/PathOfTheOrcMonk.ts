import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { WeaponType } from '../../gameService/models/items/type/WeaponType'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const PREFECT_KASMAN = 30501
const GANTAKI_ZU_URUTU = 30587
const KHAVATARI_ROSHEEK = 30590
const KHAVATARI_TORUKU = 30591
const SEER_MOIRA = 31979
const KHAVATARI_AREN = 32056
const POMEGRANATE = 1593
const LEATHER_POUCH_1ST = 1594
const LEATHER_POUCH_2ND = 1595
const LEATHER_POUCH_3RD = 1596
const LEATHER_POUCH_1ST_FULL = 1597
const LEATHER_POUCH_2ND_FULL = 1598
const LEATHER_POUCH_3RD_FULL = 1599
const KASHA_BEAR_CLAW = 1600
const KASHA_BLADE_SPIDER_TALON = 1601
const SCARLET_SALAMANDER_SCALE = 1602
const FIERY_SPIRIT_SCROLL = 1603
const ROSHEEKS_LETTER = 1604
const GANTAKIS_LETTRT_OF_RECOMMENDATION = 1605
const FIG = 1606
const LEATHER_POUCH_4TF = 1607
const LEATHER_POUCH_4TF_FULL = 1608
const VUKU_ORK_TUSK = 1609
const RATMAN_FANG = 1610
const LANGK_LIZARDMAN_TOOTH = 1611
const FELIM_LIZARDMAN_TOOTH = 1612
const IRON_WILL_SCROLL = 1613
const TORUKUS_LETTER = 1614
const KASHA_SPIDERS_TOOTH = 8545
const HORN_OF_BAAR_DRE_VANUL = 8546
const KHAVATARI_TOTEM = 1615
const FELIM_LIZARDMAN_WARRIOR = 20014
const VUKU_ORC_FIGHTER = 20017
const LANGK_LIZZARDMAN_WARRIOR = 20024
const RATMAN_WARRIOR = 20359
const SCARLET_SALAMANDER = 20415
const KASHA_FANG_SPIDER = 20476
const KASHA_BLADE_SPIDER = 20478
const KASHA_BEAR = 20479
const BAAR_DRE_VANUL = 21118
const minimumLevel = 18

const monsterIds: Array<number> = [
    FELIM_LIZARDMAN_WARRIOR,
    VUKU_ORC_FIGHTER,
    LANGK_LIZZARDMAN_WARRIOR,
    RATMAN_WARRIOR,
    SCARLET_SALAMANDER,
    KASHA_FANG_SPIDER,
    KASHA_BLADE_SPIDER,
    KASHA_BEAR,
    BAAR_DRE_VANUL,
]

const variableNames = {
    lastAttacker: '415.la',
}

export class PathOfTheOrcMonk extends ListenerLogic {
    constructor() {
        super( 'Q00415_PathOfTheOrcMonk', 'listeners/tracked-400/PathOfTheOrcMonk.ts' )
        this.questId = 415
        this.questItemIds = [
            POMEGRANATE,
            LEATHER_POUCH_1ST,
            LEATHER_POUCH_2ND,
            LEATHER_POUCH_3RD,
            LEATHER_POUCH_1ST_FULL,
            LEATHER_POUCH_2ND_FULL,
            LEATHER_POUCH_3RD_FULL,
            KASHA_BEAR_CLAW,
            KASHA_BLADE_SPIDER_TALON,
            SCARLET_SALAMANDER_SCALE,
            FIERY_SPIRIT_SCROLL,
            ROSHEEKS_LETTER,
            GANTAKIS_LETTRT_OF_RECOMMENDATION,
            FIG,
            LEATHER_POUCH_4TF,
            LEATHER_POUCH_4TF_FULL,
            VUKU_ORK_TUSK,
            RATMAN_FANG,
            LANGK_LIZARDMAN_TOOTH,
            FELIM_LIZARDMAN_TOOTH,
            IRON_WILL_SCROLL,
            TORUKUS_LETTER,
            KASHA_SPIDERS_TOOTH,
            HORN_OF_BAAR_DRE_VANUL,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00415_PathOfTheOrcMonk'
    }

    getQuestStartIds(): Array<number> {
        return [ GANTAKI_ZU_URUTU ]
    }

    getTalkIds(): Array<number> {
        return [
            GANTAKI_ZU_URUTU,
            PREFECT_KASMAN,
            KHAVATARI_ROSHEEK,
            KHAVATARI_TORUKU,
            SEER_MOIRA,
            KHAVATARI_AREN,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return monsterIds
    }

    getAttackableAttackIds(): Array<number> {
        return monsterIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.orcFighter.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, KHAVATARI_TOTEM ) ) {
                            return this.getPath( '30587-04.htm' )
                        }

                        return this.getPath( '30587-05.htm' )
                    }

                    return this.getPath( '30587-03.htm' )
                }

                if ( player.getClassId() === ClassIdValues.orcMonk.id ) {
                    return this.getPath( '30587-02a.htm' )
                }

                return this.getPath( '30587-02.htm' )

            case '30587-06.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, POMEGRANATE, 1 )
                break

            case '30587-09b.html':
                if ( QuestHelper.hasQuestItems( player, FIERY_SPIRIT_SCROLL, ROSHEEKS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, ROSHEEKS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, GANTAKIS_LETTRT_OF_RECOMMENDATION, 1 )

                    state.setConditionWithSound( 9 )
                    break
                }

                return

            case '30587-09c.html':
                if ( QuestHelper.hasQuestItems( player, FIERY_SPIRIT_SCROLL, ROSHEEKS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, ROSHEEKS_LETTER, 1 )

                    state.setMemoState( 2 )
                    state.setConditionWithSound( 14 )
                    break
                }

                return

            case '31979-02.html':
                if ( state.isMemoState( 5 ) ) {
                    break
                }

                return

            case '31979-03.html':
                if ( state.isMemoState( 5 ) ) {
                    await QuestHelper.giveAdena( player, 81900, true )
                    await QuestHelper.giveSingleItem( player, KHAVATARI_TOTEM, 1 )

                    if ( player.getLevel() >= 20 ) {
                        await QuestHelper.addExpAndSp( player, 160267, 12646 )
                    } else if ( player.getLevel() === 19 ) {
                        await QuestHelper.addExpAndSp( player, 228064, 15995 )
                    } else {
                        await QuestHelper.addExpAndSp( player, 295862, 19344 )
                    }

                    await state.exitQuest( false, true )
                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                    break
                }

                return

            case '31979-04.html':
                if ( state.isMemoState( 5 ) ) {
                    state.setConditionWithSound( 20 )

                    break
                }

                return

            case '32056-02.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case '32056-03.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 15 )

                    break
                }

                return

            case '32056-08.html':
                if ( state.isMemoState( 4 ) && QuestHelper.getQuestItemsCount( player, HORN_OF_BAAR_DRE_VANUL ) >= 1 ) {
                    await QuestHelper.takeSingleItem( player, HORN_OF_BAAR_DRE_VANUL, -1 )

                    state.setMemoState( 5 )
                    state.setConditionWithSound( 19 )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        switch ( NpcVariablesManager.get( data.targetId, this.getName() ) ) {
            case 1:
                if ( NpcVariablesManager.get( data.targetId, variableNames.lastAttacker ) !== data.attackerPlayerId
                        || !this.isWeaponValid( data.attackerPlayerId ) ) {
                    NpcVariablesManager.set( data.targetId, this.getName(), 2 )
                }

                return

            case 2:
                return

            default:
                if ( !this.isWeaponValid( data.attackerPlayerId ) ) {
                    NpcVariablesManager.set( data.targetId, this.getName(), 2 )
                    return
                }

                NpcVariablesManager.set( data.targetId, this.getName(), 1 )
                NpcVariablesManager.set( data.targetId, variableNames.lastAttacker, data.attackerPlayerId )

                return
        }
    }

    isWeaponValid( playerId: number ): boolean {
        let player = L2World.getPlayer( playerId )
        let weapon = player.getActiveWeaponInstance()

        return !weapon || [ WeaponType.FIST, WeaponType.DUALFIST ].includes( weapon.getItemType() )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( NpcVariablesManager.get( data.targetId, this.getName() ) !== 1 ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case FELIM_LIZARDMAN_WARRIOR:
                if ( QuestHelper.getQuestItemsCount( player, FELIM_LIZARDMAN_TOOTH ) < 3 ) {
                    await this.processWarriorMonsterKill( player, state, FELIM_LIZARDMAN_TOOTH, data.isChampion )
                }

                return

            case VUKU_ORC_FIGHTER:
                if ( QuestHelper.getQuestItemsCount( player, VUKU_ORK_TUSK ) < 3 ) {
                    await this.processWarriorMonsterKill( player, state, VUKU_ORK_TUSK, data.isChampion )
                }

                return

            case LANGK_LIZZARDMAN_WARRIOR:
                if ( QuestHelper.getQuestItemsCount( player, LANGK_LIZARDMAN_TOOTH ) < 3 ) {
                    await this.processWarriorMonsterKill( player, state, LANGK_LIZARDMAN_TOOTH, data.isChampion )
                }

                return

            case RATMAN_WARRIOR:
                if ( QuestHelper.getQuestItemsCount( player, RATMAN_FANG ) < 3 ) {
                    await this.processWarriorMonsterKill( player, state, RATMAN_FANG, data.isChampion )
                }

                return

            case SCARLET_SALAMANDER:
                if ( QuestHelper.hasQuestItem( player, LEATHER_POUCH_3RD ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, SCARLET_SALAMANDER_SCALE ) >= 4 ) {
                        await QuestHelper.takeMultipleItems( player, -1, LEATHER_POUCH_3RD, SCARLET_SALAMANDER_SCALE )
                        await QuestHelper.giveSingleItem( player, LEATHER_POUCH_3RD_FULL, 1 )

                        state.setConditionWithSound( 7, true )
                        return
                    }

                    await QuestHelper.rewardSingleQuestItem( player, SCARLET_SALAMANDER_SCALE, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }
                return

            case KASHA_FANG_SPIDER:
                if ( Math.random() < QuestHelper.getAdjustedChance( KASHA_SPIDERS_TOOTH, 0.7, data.isChampion )
                        && state.isMemoState( 3 )
                        && QuestHelper.getQuestItemsCount( player, KASHA_SPIDERS_TOOTH ) < 6 ) {

                    await QuestHelper.rewardAndProgressState( player, state, KASHA_SPIDERS_TOOTH, 1, 6, data.isChampion, 16 )
                }

                return

            case KASHA_BLADE_SPIDER:
                if ( QuestHelper.hasQuestItem( player, LEATHER_POUCH_2ND ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, KASHA_BLADE_SPIDER_TALON ) >= 4 ) {
                        await QuestHelper.takeMultipleItems( player, -1, LEATHER_POUCH_2ND, KASHA_BLADE_SPIDER_TALON )
                        await QuestHelper.giveSingleItem( player, LEATHER_POUCH_2ND_FULL, 1 )

                        state.setConditionWithSound( 5, true )
                        return
                    }

                    await QuestHelper.rewardSingleQuestItem( player, KASHA_BLADE_SPIDER_TALON, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                    return
                }

                if ( Math.random() < QuestHelper.getAdjustedChance( KASHA_SPIDERS_TOOTH, 0.7, data.isChampion )
                        && state.isMemoState( 3 )
                        && QuestHelper.getQuestItemsCount( player, KASHA_SPIDERS_TOOTH ) < 6 ) {
                    await QuestHelper.rewardAndProgressState( player, state, KASHA_SPIDERS_TOOTH, 1, 6, data.isChampion, 16 )
                }

                return

            case KASHA_BEAR:
                if ( QuestHelper.hasQuestItem( player, LEATHER_POUCH_1ST ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, KASHA_BEAR_CLAW ) >= 4 ) {
                        await QuestHelper.takeMultipleItems( player, -1, LEATHER_POUCH_1ST, KASHA_BEAR_CLAW )
                        await QuestHelper.giveSingleItem( player, LEATHER_POUCH_1ST_FULL, 1 )

                        state.setConditionWithSound( 3, true )
                        return
                    }

                    await QuestHelper.rewardSingleQuestItem( player, KASHA_BLADE_SPIDER_TALON, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case BAAR_DRE_VANUL:
                if ( Math.random() < QuestHelper.getAdjustedChance( HORN_OF_BAAR_DRE_VANUL, 0.9, data.isChampion )
                        && state.isMemoState( 4 )
                        && !QuestHelper.hasQuestItem( player, HORN_OF_BAAR_DRE_VANUL ) ) {
                    await QuestHelper.giveSingleItem( player, HORN_OF_BAAR_DRE_VANUL, 1 )

                    state.setConditionWithSound( 18, true )
                }

                return
        }
    }

    async processWarriorMonsterKill( player: L2PcInstance, state: QuestState, givenItemId: number, isFromChampion: boolean ): Promise<void> {
        if ( !QuestHelper.hasQuestItem( player, LEATHER_POUCH_4TF ) ) {
            return
        }

        let itemAmount: number = QuestHelper.getItemsSumCount( player, RATMAN_FANG, LANGK_LIZARDMAN_TOOTH, FELIM_LIZARDMAN_TOOTH, VUKU_ORK_TUSK )
        if ( itemAmount >= 11 ) {
            await QuestHelper.takeMultipleItems( player, -1, LEATHER_POUCH_4TF, VUKU_ORK_TUSK, RATMAN_FANG, LANGK_LIZARDMAN_TOOTH, FELIM_LIZARDMAN_TOOTH )
            await QuestHelper.giveSingleItem( player, LEATHER_POUCH_4TF_FULL, 1 )

            state.setConditionWithSound( 12, true )
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, FELIM_LIZARDMAN_TOOTH, 1, isFromChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === GANTAKI_ZU_URUTU ) {
                    return this.getPath( '30587-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case GANTAKI_ZU_URUTU:
                        if ( state.getMemoState() === 2 ) {
                            return this.getPath( '30587-09c.html' )
                        }

                        switch ( QuestHelper.getItemsSumCount( player,
                                LEATHER_POUCH_1ST,
                                LEATHER_POUCH_2ND,
                                LEATHER_POUCH_3RD,
                                LEATHER_POUCH_1ST_FULL,
                                LEATHER_POUCH_2ND_FULL,
                                LEATHER_POUCH_3RD_FULL ) ) {

                            case 0:
                                let [ hasPomegranate, hasScroll, hasRecommendation, hasLetter ]: Array<boolean> = QuestHelper.hasEachQuestItem( player, POMEGRANATE, FIERY_SPIRIT_SCROLL, GANTAKIS_LETTRT_OF_RECOMMENDATION, ROSHEEKS_LETTER )
                                if ( hasPomegranate
                                        && !hasScroll
                                        && !hasRecommendation
                                        && !hasLetter ) {
                                    return this.getPath( '30587-07.html' )
                                }

                                if ( hasScroll
                                        && hasLetter
                                        && !hasPomegranate
                                        && !hasRecommendation ) {
                                    return this.getPath( '30587-09a.html' )
                                }

                                if ( state.getMemoState() < 2 ) {
                                    if ( hasScroll
                                            && hasRecommendation
                                            && !hasPomegranate
                                            && !hasLetter ) {
                                        return this.getPath( '30587-10.html' )
                                    }

                                    if ( hasScroll
                                            && !hasRecommendation
                                            && !hasPomegranate
                                            && !hasLetter ) {
                                        return this.getPath( '30587-11.html' )
                                    }
                                }

                                break

                            case 1:
                                if ( !QuestHelper.hasAtLeastOneQuestItem( player, FIERY_SPIRIT_SCROLL, POMEGRANATE, GANTAKIS_LETTRT_OF_RECOMMENDATION, ROSHEEKS_LETTER ) ) {
                                    return this.getPath( '30587-08.html' )
                                }

                                break
                        }

                        break

                    case PREFECT_KASMAN:
                        if ( QuestHelper.hasQuestItem( player, GANTAKIS_LETTRT_OF_RECOMMENDATION ) ) {
                            await QuestHelper.takeSingleItem( player, GANTAKIS_LETTRT_OF_RECOMMENDATION, 1 )
                            await QuestHelper.giveSingleItem( player, FIG, 1 )

                            state.setConditionWithSound( 10 )
                            return this.getPath( '30501-01.html' )
                        }

                        let hasFig: boolean = QuestHelper.hasQuestItem( player, FIG )
                        let hasPouchFour: boolean = QuestHelper.hasAtLeastOneQuestItem( player, LEATHER_POUCH_4TF, LEATHER_POUCH_4TF_FULL )

                        if ( hasFig && !hasPouchFour ) {
                            return this.getPath( '30501-02.html' )
                        }

                        if ( !hasFig && hasPouchFour ) {
                            return this.getPath( '30501-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, IRON_WILL_SCROLL ) ) {
                            await QuestHelper.giveAdena( player, 163800, true )
                            await QuestHelper.giveSingleItem( player, KHAVATARI_TOTEM, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 25292 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 31990 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 591724, 38688 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30501-04.html' )
                        }

                        break

                    case KHAVATARI_ROSHEEK:
                        if ( QuestHelper.hasQuestItem( player, POMEGRANATE ) ) {
                            await QuestHelper.takeSingleItem( player, POMEGRANATE, 1 )
                            await QuestHelper.giveSingleItem( player, LEATHER_POUCH_1ST, 1 )

                            state.setConditionWithSound( 2 )
                            return this.getPath( '30590-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LEATHER_POUCH_1ST )
                                && !QuestHelper.hasQuestItem( player, LEATHER_POUCH_1ST_FULL ) ) {
                            return this.getPath( '30590-02.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, LEATHER_POUCH_1ST )
                                && QuestHelper.hasQuestItem( player, LEATHER_POUCH_1ST_FULL ) ) {
                            await QuestHelper.giveSingleItem( player, LEATHER_POUCH_2ND, 1 )
                            await QuestHelper.takeSingleItem( player, LEATHER_POUCH_1ST_FULL, 1 )

                            state.setConditionWithSound( 4 )
                            return this.getPath( '30590-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LEATHER_POUCH_2ND )
                                && !QuestHelper.hasQuestItem( player, LEATHER_POUCH_2ND_FULL ) ) {
                            return this.getPath( '30590-04.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, LEATHER_POUCH_2ND )
                                && QuestHelper.hasQuestItem( player, LEATHER_POUCH_2ND_FULL ) ) {
                            await QuestHelper.takeSingleItem( player, LEATHER_POUCH_2ND_FULL, 1 )
                            await QuestHelper.giveSingleItem( player, LEATHER_POUCH_3RD, 1 )

                            state.setConditionWithSound( 6 )
                            return this.getPath( '30590-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LEATHER_POUCH_3RD )
                                && !QuestHelper.hasQuestItem( player, LEATHER_POUCH_3RD_FULL ) ) {
                            return this.getPath( '30590-06.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, LEATHER_POUCH_3RD )
                                && QuestHelper.hasQuestItem( player, LEATHER_POUCH_3RD_FULL ) ) {
                            await QuestHelper.takeSingleItem( player, LEATHER_POUCH_3RD_FULL, 1 )
                            await QuestHelper.giveMultipleItems( player, 1, FIERY_SPIRIT_SCROLL, ROSHEEKS_LETTER )

                            state.setConditionWithSound( 8 )
                            return this.getPath( '30590-07.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ROSHEEKS_LETTER, FIERY_SPIRIT_SCROLL ) ) {
                            return this.getPath( '30590-08.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, ROSHEEKS_LETTER )
                                && QuestHelper.hasQuestItem( player, FIERY_SPIRIT_SCROLL ) ) {
                            return this.getPath( '30590-09.html' )
                        }

                        break

                    case KHAVATARI_TORUKU:
                        if ( QuestHelper.hasQuestItem( player, FIG ) ) {
                            await QuestHelper.takeSingleItem( player, FIG, -1 )
                            await QuestHelper.giveSingleItem( player, LEATHER_POUCH_4TF, 1 )

                            state.setConditionWithSound( 11 )
                            return this.getPath( '30591-01.html' )
                        }

                        let hasNormalPouch: boolean = QuestHelper.hasQuestItem( player, LEATHER_POUCH_4TF )
                        let hasFullPouch: boolean = QuestHelper.hasQuestItem( player, LEATHER_POUCH_4TF_FULL )

                        if ( hasNormalPouch && !hasFullPouch ) {
                            return this.getPath( '30591-02.html' )
                        }

                        if ( !hasNormalPouch && hasFullPouch ) {
                            await QuestHelper.takeSingleItem( player, LEATHER_POUCH_4TF_FULL, 1 )
                            await QuestHelper.giveMultipleItems( player, 1, IRON_WILL_SCROLL, TORUKUS_LETTER )

                            state.setConditionWithSound( 13 )
                            return this.getPath( '30591-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, IRON_WILL_SCROLL, TORUKUS_LETTER ) ) {
                            return this.getPath( '30591-04.html' )
                        }

                        break

                    case SEER_MOIRA:
                        if ( state.getMemoState() === 5 ) {
                            return this.getPath( '31979-01.html' )
                        }

                        break

                    case KHAVATARI_AREN:
                        switch ( state.getMemoState() ) {
                            case 2:
                                return this.getPath( '32056-01.html' )

                            case 3:
                                if ( QuestHelper.getQuestItemsCount( player, KASHA_SPIDERS_TOOTH ) < 6 ) {
                                    return this.getPath( '32056-04.html' )
                                }

                                await QuestHelper.takeSingleItem( player, KASHA_SPIDERS_TOOTH, -1 )

                                state.setMemoState( 4 )
                                state.setConditionWithSound( 17 )

                                return this.getPath( '32056-05.html' )

                            case 4:
                                if ( !QuestHelper.hasQuestItem( player, HORN_OF_BAAR_DRE_VANUL ) ) {
                                    return this.getPath( '32056-06.html' )
                                }

                                return this.getPath( '32056-07.html' )

                            case 5:
                                return this.getPath( '32056-09.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}