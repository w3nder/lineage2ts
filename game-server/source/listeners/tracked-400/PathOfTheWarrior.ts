import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const MASTER_AURON = 30010
const TRADER_SIMPLON = 30253
const AURONS_LETTER = 1138
const WARRIOR_GUILD_MARK = 1139
const RUSTED_BRONZE_SWORD1 = 1140
const RUSTED_BRONZE_SWORD2 = 1141
const RUSTED_BRONZE_SWORD3 = 1142
const SIMPLONS_LETTER = 1143
const VENOMOUS_SPIDERS_LEG = 1144
const MEDALLION_OF_WARRIOR = 1145
const TRACKER_SKELETON = 20035
const VENOMOUS_SPIDERS = 20038
const TRACKER_SKELETON_LIDER = 20042
const ARACHNID_TRACKER = 20043
const MIN_LEVEL = 18

const variableNames = {
    lastAttacker: '401.la',
}

export class PathOfTheWarrior extends ListenerLogic {
    constructor() {
        super( 'Q00401_PathOfTheWarrior', 'listeners/tracked-400/PathOfTheWarrior.ts' )
        this.questId = 401
        this.questItemIds = [
            AURONS_LETTER,
            WARRIOR_GUILD_MARK,
            RUSTED_BRONZE_SWORD1,
            RUSTED_BRONZE_SWORD2,
            RUSTED_BRONZE_SWORD3,
            SIMPLONS_LETTER,
            VENOMOUS_SPIDERS_LEG,
        ]
    }

    checkWeapon( playerId: number ): boolean {
        let player = L2World.getPlayer( playerId )
        let weapon = player.getActiveWeaponInstance()
        return weapon && weapon.getId() === RUSTED_BRONZE_SWORD3
    }

    getAttackableAttackIds(): Array<number> {
        return [
            VENOMOUS_SPIDERS,
            ARACHNID_TRACKER,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            TRACKER_SKELETON,
            VENOMOUS_SPIDERS,
            TRACKER_SKELETON_LIDER,
            ARACHNID_TRACKER,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00401_PathOfTheWarrior'
    }

    getQuestStartIds(): Array<number> {
        return [ MASTER_AURON ]
    }

    getTalkIds(): Array<number> {
        return [
            MASTER_AURON,
            TRADER_SIMPLON,
        ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        switch ( NpcVariablesManager.get( data.targetId, this.getName() ) ) {
            default:
                NpcVariablesManager.set( data.targetId, variableNames.lastAttacker, data.attackerPlayerId )
                let value: number = this.checkWeapon( data.attackerPlayerId ) ? 1 : 2
                NpcVariablesManager.set( data.targetId, this.getName(), value )

                return

            case 2:
                return

            case 1:
                if ( !this.checkWeapon( data.attackerPlayerId ) || NpcVariablesManager.get( data.targetId, variableNames.lastAttacker ) !== data.attackerPlayerId ) {
                    NpcVariablesManager.set( data.targetId, this.getName(), 2 )
                }

                return
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case TRACKER_SKELETON:
            case TRACKER_SKELETON_LIDER:
                if ( !state.isCondition( 3 )
                        && Math.random() < QuestHelper.getAdjustedChance( RUSTED_BRONZE_SWORD1, 0.4, data.isChampion )
                        && QuestHelper.hasQuestItem( player, WARRIOR_GUILD_MARK )
                        && QuestHelper.getQuestItemsCount( player, RUSTED_BRONZE_SWORD1 ) < 10 ) {
                    await QuestHelper.rewardAndProgressState( player, state, RUSTED_BRONZE_SWORD1, 1, 10, data.isChampion, 3 )
                }

                return

            case VENOMOUS_SPIDERS:
            case ARACHNID_TRACKER:
                if ( !state.isCondition( 6 )
                        && NpcVariablesManager.get( data.targetId, this.getName() ) === 1
                        && QuestHelper.getQuestItemsCount( player, VENOMOUS_SPIDERS_LEG ) < 20 ) {
                    await QuestHelper.rewardAndProgressState( player, state, VENOMOUS_SPIDERS_LEG, 1, 20, data.isChampion, 6 )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.fighter.id ) {
                    if ( player.getLevel() >= MIN_LEVEL ) {
                        if ( QuestHelper.hasQuestItem( player, MEDALLION_OF_WARRIOR ) ) {
                            return this.getPath( '30010-04.htm' )
                        }

                        return this.getPath( '30010-05.htm' )
                    }

                    return this.getPath( '30010-02.htm' )
                }

                if ( player.getClassId() === ClassIdValues.warrior.id ) {
                    return this.getPath( '30010-02a.htm' )
                }

                return this.getPath( '30010-03.htm' )

            case '30010-06.htm':
                if ( !QuestHelper.hasQuestItem( player, AURONS_LETTER ) ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, AURONS_LETTER, 1 )

                    break
                }

                return

            case '30010-10.html':
                break

            case '30010-11.html':
                if ( QuestHelper.hasQuestItems( player, SIMPLONS_LETTER, RUSTED_BRONZE_SWORD2 ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, RUSTED_BRONZE_SWORD2, SIMPLONS_LETTER )
                    await QuestHelper.giveSingleItem( player, RUSTED_BRONZE_SWORD3, 1 )

                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '30253-02.html':
                if ( QuestHelper.hasQuestItem( player, AURONS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, AURONS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, WARRIOR_GUILD_MARK, 1 )

                    state.setConditionWithSound( 2, true )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MASTER_AURON ) {
                    return this.getPath( '30010-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MASTER_AURON:
                        if ( QuestHelper.hasQuestItem( player, AURONS_LETTER ) ) {
                            return this.getPath( '30010-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, WARRIOR_GUILD_MARK ) ) {
                            return this.getPath( '30010-08.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, SIMPLONS_LETTER, RUSTED_BRONZE_SWORD2 ) ) {
                            return this.getPath( '30010-09.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, RUSTED_BRONZE_SWORD3 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, VENOMOUS_SPIDERS_LEG ) < 20 ) {
                                return this.getPath( '30010-12.html' )
                            }

                            await QuestHelper.giveAdena( player, 163800, true )
                            await QuestHelper.giveSingleItem( player, MEDALLION_OF_WARRIOR, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 21012 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 27710 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 160267, 34408 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30010-13.html' )
                        }

                        break

                    case TRADER_SIMPLON:
                        if ( QuestHelper.hasQuestItem( player, AURONS_LETTER ) ) {
                            return this.getPath( '30253-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, WARRIOR_GUILD_MARK ) ) {
                            if ( !QuestHelper.hasQuestItem( player, RUSTED_BRONZE_SWORD1 ) ) {
                                return this.getPath( '30253-03.html' )
                            }

                            if ( QuestHelper.getQuestItemsCount( player, RUSTED_BRONZE_SWORD1 ) < 10 ) {
                                return this.getPath( '30253-04.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, WARRIOR_GUILD_MARK, RUSTED_BRONZE_SWORD1 )
                            await QuestHelper.giveMultipleItems( player, 1, RUSTED_BRONZE_SWORD2, SIMPLONS_LETTER )

                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30253-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SIMPLONS_LETTER ) ) {
                            return this.getPath( '30253-06.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}