import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const OCTAVIA = 31043
const GOLEM = 21103
const RED_CRYSTAL = 7541
const ECHO_CRYSTAL = 7061
const minimumLevel = 31
const requiredCrystalAmount = 50

export class BirthdayPartySong extends ListenerLogic {
    constructor() {
        super( 'Q00432_BirthdayPartySong', 'listeners/tracked-400/BirthdayPartySong.ts' )
        this.questId = 432
        this.questItemIds = [ RED_CRYSTAL ]
    }

    getQuestStartIds(): Array<number> {
        return [ OCTAVIA ]
    }

    getTalkIds(): Array<number> {
        return [ OCTAVIA ]
    }

    getAttackableKillIds(): Array<number> {
        return [ GOLEM ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00432_BirthdayPartySong'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31043-02.htm':
                state.startQuest()
                break

            case '31043-05.html':
                if ( QuestHelper.getQuestItemsCount( player, RED_CRYSTAL ) < requiredCrystalAmount ) {
                    return this.getPath( '31043-06.html' )
                }

                await QuestHelper.rewardSingleItem( player, ECHO_CRYSTAL, 25 )
                await state.exitQuest( true, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state
                || !state.isCondition( 1 )
                || Math.random() > QuestHelper.getAdjustedChance( RED_CRYSTAL, 0.5, data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardAndProgressState( L2World.getPlayer( data.playerId ), state, RED_CRYSTAL, 1, requiredCrystalAmount, data.isChampion, 2 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '31043-01.htm' : '31043-00.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( state.isCondition( 1 ) ? '31043-03.html' : '31043-04.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}