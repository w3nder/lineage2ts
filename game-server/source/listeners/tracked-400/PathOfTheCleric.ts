import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

const GALLINT = 30017
const ZIGAUNT = 30022
const VIVYAN = 30030
const TRADER_SIMPLON = 30253
const GUARD_PRAGA = 30333
const LIONEL = 30408
const LETTER_OF_ORDER_1ST = 1191
const LETTER_OF_ORDER_2ND = 1192
const LIONELS_BOOK = 1193
const BOOK_OF_VIVYAN = 1194
const BOOK_OF_SIMPLON = 1195
const BOOK_OF_PRAGA = 1196
const CERTIFICATE_OF_GALLINT = 1197
const PENDANT_OF_MOTHER = 1198
const NECKLACE_OF_MOTHER = 1199
const LEMONIELLS_COVENANT = 1200
const MARK_OF_FAITH = 1201
const RUIN_ZOMBIE = 20026
const RUIN_ZOMBIE_LEADER = 20029
const minimumLevel = 18

export class PathOfTheCleric extends ListenerLogic {
    constructor() {
        super( 'Q00405_PathOfTheCleric', 'listeners/tracked-400/PathOfTheCleric.ts' )
        this.questId = 405
        this.questItemIds = [
            LETTER_OF_ORDER_1ST,
            LETTER_OF_ORDER_2ND,
            LIONELS_BOOK,
            BOOK_OF_VIVYAN,
            BOOK_OF_SIMPLON,
            BOOK_OF_PRAGA,
            CERTIFICATE_OF_GALLINT,
            PENDANT_OF_MOTHER,
            NECKLACE_OF_MOTHER,
            LEMONIELLS_COVENANT,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ ZIGAUNT ]
    }

    getTalkIds(): Array<number> {
        return [
            ZIGAUNT,
            GALLINT,
            VIVYAN,
            TRADER_SIMPLON,
            GUARD_PRAGA,
            LIONEL,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00405_PathOfTheCleric'
    }

    getAttackableKillIds(): Array<number> {
        return [
            RUIN_ZOMBIE,
            RUIN_ZOMBIE_LEADER,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.mage.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, MARK_OF_FAITH ) ) {
                            return this.getPath( '30022-04.htm' )
                        }

                        state.startQuest()
                        await QuestHelper.giveSingleItem( player, LETTER_OF_ORDER_1ST, 1 )

                        return this.getPath( '30022-05.htm' )
                    }

                    return this.getPath( '30022-03.htm' )
                }

                if ( player.getClassId() === ClassIdValues.cleric.id ) {
                    return this.getPath( '30022-02a.htm' )
                }

                return this.getPath( '30022-02.htm' )
        }

        return
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( QuestHelper.hasQuestItem( player, NECKLACE_OF_MOTHER ) && !QuestHelper.hasQuestItem( player, PENDANT_OF_MOTHER ) ) {
            await QuestHelper.giveSingleItem( player, PENDANT_OF_MOTHER, 1 )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === ZIGAUNT ) {
                    if ( !QuestHelper.hasQuestItem( player, MARK_OF_FAITH ) ) {
                        return this.getPath( '30022-01.htm' )
                    }

                    return this.getPath( '30022-04.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case ZIGAUNT:
                        if ( QuestHelper.hasQuestItem( player, LETTER_OF_ORDER_2ND ) ) {
                            if ( !QuestHelper.hasQuestItem( player, LEMONIELLS_COVENANT ) ) {
                                return this.getPath( '30022-07.html' )
                            }

                            await QuestHelper.giveAdena( player, 163800, true )
                            await QuestHelper.takeMultipleItems( player, -1, LETTER_OF_ORDER_2ND, LEMONIELLS_COVENANT )
                            await QuestHelper.giveSingleItem( player, MARK_OF_FAITH, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 23152 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 28630 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 591724, 35328 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30022-09.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LETTER_OF_ORDER_1ST ) ) {
                            if ( QuestHelper.hasQuestItems( player, BOOK_OF_VIVYAN, BOOK_OF_SIMPLON, BOOK_OF_PRAGA ) ) {
                                await QuestHelper.takeMultipleItems( player, -1, LETTER_OF_ORDER_1ST, BOOK_OF_VIVYAN, BOOK_OF_SIMPLON, BOOK_OF_PRAGA )
                                await QuestHelper.giveSingleItem( player, LETTER_OF_ORDER_2ND, 1 )

                                state.setConditionWithSound( 3, true )
                                return this.getPath( '30022-08.html' )
                            }

                            return this.getPath( '30022-06.html' )
                        }

                        break

                    case GALLINT:
                        if ( !QuestHelper.hasQuestItem( player, LEMONIELLS_COVENANT )
                                && QuestHelper.hasQuestItem( player, LETTER_OF_ORDER_2ND ) ) {
                            if ( !QuestHelper.hasQuestItem( player, CERTIFICATE_OF_GALLINT )
                                    && QuestHelper.hasQuestItem( player, LIONELS_BOOK ) ) {
                                await QuestHelper.takeSingleItem( player, LIONELS_BOOK, 1 )
                                await QuestHelper.giveSingleItem( player, CERTIFICATE_OF_GALLINT, 1 )

                                state.setConditionWithSound( 5, true )
                                return this.getPath( '30017-01.html' )
                            }

                            return this.getPath( '30017-02.html' )
                        }

                        break

                    case VIVYAN:
                        if ( QuestHelper.hasQuestItem( player, LETTER_OF_ORDER_1ST ) ) {
                            if ( !QuestHelper.hasQuestItem( player, BOOK_OF_VIVYAN ) ) {
                                await QuestHelper.giveSingleItem( player, BOOK_OF_VIVYAN, 1 )

                                this.progressState( player, state )
                                return this.getPath( '30030-01.html' )
                            }

                            return this.getPath( '30030-02.html' )
                        }

                        break

                    case TRADER_SIMPLON:
                        if ( QuestHelper.hasQuestItem( player, LETTER_OF_ORDER_1ST ) ) {
                            if ( !QuestHelper.hasQuestItem( player, BOOK_OF_SIMPLON ) ) {
                                await QuestHelper.giveSingleItem( player, BOOK_OF_SIMPLON, 3 )
                                this.progressState( player, state )

                                return this.getPath( '30253-01.html' )
                            }

                            return this.getPath( '30253-02.html' )
                        }

                        break

                    case GUARD_PRAGA:
                        if ( QuestHelper.hasQuestItem( player, LETTER_OF_ORDER_1ST ) ) {
                            if ( QuestHelper.hasQuestItem( player, BOOK_OF_PRAGA ) ) {
                                return this.getPath( '30333-04.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, NECKLACE_OF_MOTHER ) ) {
                                if ( !QuestHelper.hasQuestItem( player, PENDANT_OF_MOTHER ) ) {
                                    return this.getPath( '30333-02.html' )
                                }

                                await QuestHelper.giveSingleItem( player, BOOK_OF_PRAGA, 1 )
                                await QuestHelper.takeMultipleItems( player, -1, PENDANT_OF_MOTHER, NECKLACE_OF_MOTHER )

                                this.progressState( player, state )
                                return this.getPath( '30333-03.html' )
                            }

                            await QuestHelper.giveSingleItem( player, NECKLACE_OF_MOTHER, 1 )
                            return this.getPath( '30333-01.html' )
                        }

                        break

                    case LIONEL:
                        if ( !QuestHelper.hasQuestItem( player, LETTER_OF_ORDER_2ND ) ) {
                            return this.getPath( '30408-02.html' )
                        }

                        let hasBook : boolean = QuestHelper.hasQuestItem( player, LIONELS_BOOK )
                        let hasCovenant : boolean = QuestHelper.hasQuestItem( player, LEMONIELLS_COVENANT )
                        let hasCertificate : boolean = QuestHelper.hasQuestItem( player, CERTIFICATE_OF_GALLINT )

                        if ( hasBook && !( hasCovenant || hasCertificate ) ) {
                            return this.getPath( '30408-03.html' )
                        }

                        if ( hasCertificate && !( hasBook || hasCovenant ) ) {
                            await QuestHelper.takeSingleItem( player, CERTIFICATE_OF_GALLINT, -1 )
                            await QuestHelper.giveSingleItem( player, LEMONIELLS_COVENANT, 1 )

                            state.setConditionWithSound( 6, true )
                            return this.getPath( '30408-04.html' )
                        }

                        if ( hasCovenant && !( hasBook || hasCertificate ) ) {
                            return this.getPath( '30408-05.html' )
                        }

                        if ( !hasBook && !hasCertificate && !hasCovenant ) {
                            await QuestHelper.giveSingleItem( player, LIONELS_BOOK, 1 )

                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30408-01.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    progressState( player : L2PcInstance, state : QuestState ) : void {
        if ( QuestHelper.getQuestItemsCount( player, BOOK_OF_SIMPLON ) >= 3
                && QuestHelper.hasQuestItem( player, BOOK_OF_VIVYAN )
                && QuestHelper.hasQuestItem( player, BOOK_OF_PRAGA ) ) {
            state.setConditionWithSound( 2, true )
        }
    }
}