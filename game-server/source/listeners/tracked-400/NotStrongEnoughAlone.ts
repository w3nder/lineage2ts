import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import * as Buffer from 'buffer'
import { ExQuestNpcLogList } from '../../gameService/packets/send/builder/ExQuestNpcLogList'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestType } from '../../gameService/enums/QuestType'

import _ from 'lodash'
import aigle from 'aigle'

const KLEMIS = 32734
const monsterConditionRequirements: { [ npcId: number ]: number } = {
    22746: 2,
    22747: 2,
    22748: 2,
    22749: 2,
    22750: 2,
    22751: 2,
    22752: 2,
    22753: 2,
    22754: 3,
    22755: 3,
    22756: 3,
    22757: 3,
    22758: 3,
    22759: 3,
    22760: 4,
    22761: 4,
    22762: 4,
    22763: 4,
    22764: 4,
    22765: 4,
}

const rewardItems: Array<number> = [
    15815,
    15816,
    15817,
    15818,
    15819,
    15820,
    15821,
    15822,
    15823,
    15824,
    15825,
    15634,
    15635,
    15636,
    15637,
    15638,
    15639,
    15640,
    15641,
    15642,
    15643,
    15644,
]

const variableNames = {
    npcData: 'nd',
}

const minimumLevel = 84

export class NotStrongEnoughAlone extends ListenerLogic {
    constructor() {
        super( 'Q00453_NotStrongEnoughAlone', 'listeners/tracked-400/NotStrongEnoughAlone.ts' )
        this.questId = 453
    }

    getQuestStartIds(): Array<number> {
        return [ KLEMIS ]
    }

    getTalkIds(): Array<number> {
        return [ KLEMIS ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterConditionRequirements ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32734-06.htm':
                state.startQuest()
                break

            case '32734-07.html':
                state.setConditionWithSound( 2, true )
                break

            case '32734-08.html':
                state.setConditionWithSound( 3, true )
                break

            case '32734-09.html':
                state.setConditionWithSound( 4, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let party = PlayerGroupCache.getParty( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        await aigle.resolve( party ? party.getMembers() : [ data.playerId ] ).eachLimit( 10, ( playerId: number ) : Promise<void> => {
            let state: QuestState = QuestStateCache.getQuestState( playerId, this.getName(), false )

            if ( !state ) {
                return
            }

            let player = L2World.getPlayer( playerId )

            if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
                return
            }

            return this.increaseKill( player, state, data )
        } )

        return
    }

    async increaseKill( player: L2PcInstance, state: QuestState, data: AttackableKillEvent ): Promise<void> {
        if ( !state.isCondition( monsterConditionRequirements[ data.npcId ] ) ) {
            return
        }

        if ( !state.getVariable( variableNames.npcData ) ) {
            state.setVariable( variableNames.npcData, {} )
        }

        switch ( state.getCondition() ) {
            case 2:
                player.sendOwnedData( this.processCondition( data.npcId, state, player, 22750, 22753, 4, 15, [ 22746, 22747, 22748, 22749 ] ) )
                break

            case 3:
                player.sendOwnedData( this.processCondition( data.npcId, state, player, 22757, 22759, 3, 20, [ 22754, 22755, 22756 ] ) )
                break

            case 4:
                player.sendOwnedData( this.processCondition( data.npcId, state, player, 22763, 22765, 3, 20, [ 22760, 22761, 22762 ] ) )
                break
        }
    }

    processCondition( currentNpcId: number,
            state: QuestState,
            player: L2PcInstance,
            minRange: number,
            maxRange: number,
            differenceOffset: number,
            valueMax: number,
            npcIds: Array<number> ): Buffer {

        let npcId: number = currentNpcId
        if ( npcId >= minRange && npcId <= maxRange ) {
            npcId = npcId - differenceOffset
        }

        let value = _.get( state.getVariable( variableNames.npcData ), npcId, 0 )
        if ( value < valueMax ) {
            state.getVariable( variableNames.npcData )[ npcId ] = value + 1
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }

        this.checkProgress( state, valueMax, npcIds )

        let logPacket: ExQuestNpcLogList = new ExQuestNpcLogList( this.getId() )

        _.times( 4, ( index: number ) => {
            let id = index + npcIds[ 0 ]
            logPacket.addRecord( id, _.get( state.getVariable( variableNames.npcData ), id, 0 ) )
        } )

        return logPacket.getBuffer()
    }

    checkProgress( state: QuestState, amount: number, npcIds: Array<number> ): void {
        let shouldExit = _.some( npcIds, ( npcId: number ) => {
            let value = _.get( state.getVariable( variableNames.npcData ), npcId, 0 )
            return value < amount
        } )

        if ( shouldExit ) {
            return
        }

        state.setConditionWithSound( 5, true )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00453_NotStrongEnoughAlone'
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '32734-02.htm' )
                }

            case QuestStateValues.CREATED:
                if ( player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q10282_ToTheSeedOfAnnihilation' ) ) {
                    return this.getPath( '32734-01.htm' )
                }

                return this.getPath( '32734-03.html' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32734-10.html' )

                    case 2:
                        return this.getPath( '32734-11.html' )

                    case 3:
                        return this.getPath( '32734-12.html' )

                    case 4:
                        return this.getPath( '32734-13.html' )

                    case 5:
                        await QuestHelper.rewardSingleItem( player, _.sample( rewardItems ), 1 )
                        await state.exitQuestWithType( QuestType.DAILY, true )

                        return this.getPath( '32734-14.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}