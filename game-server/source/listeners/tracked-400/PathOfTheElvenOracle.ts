import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const PRIEST_MANUEL = 30293
const ALLANA = 30424
const PERRIN = 30428
const CRYSTAL_MEDALLION = 1231
const SWINDLERS_MONEY = 1232
const ALLANA_OF_DAIRY = 1233
const LIZARD_CAPTAIN_ORDER = 1234
const HALF_OF_DAIRY = 1236
const TAMIL_NECKLACE = 1275
const LEAF_OF_ORACLE = 1235
const minimumLevel = 18
const LIZARDMAN_WARRIOR = 27032
const LIZARDMAN_SCOUT = 27033
const LIZARDMAN_SOLDIER = 27034
const TAMIL = 27035

const variableNames = {
    firstAttacker: '409.fa',
}

export class PathOfTheElvenOracle extends ListenerLogic {
    constructor() {
        super( 'Q00409_PathOfTheElvenOracle', 'listeners/tracked-400/PathOfTheElvenOracle.ts' )
        this.questId = 409
        this.questItemIds = [
            CRYSTAL_MEDALLION,
            SWINDLERS_MONEY,
            ALLANA_OF_DAIRY,
            LIZARD_CAPTAIN_ORDER,
            HALF_OF_DAIRY,
            TAMIL_NECKLACE,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00409_PathOfTheElvenOracle'
    }

    getQuestStartIds(): Array<number> {
        return [ PRIEST_MANUEL ]
    }

    getTalkIds(): Array<number> {
        return [ PRIEST_MANUEL, ALLANA, PERRIN ]
    }

    getAttackableKillIds(): Array<number> {
        return [ TAMIL, LIZARDMAN_WARRIOR, LIZARDMAN_SCOUT, LIZARDMAN_SOLDIER ]
    }

    getAttackableAttackIds(): Array<number> {
        return [ TAMIL, LIZARDMAN_WARRIOR, LIZARDMAN_SCOUT, LIZARDMAN_SOLDIER ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.elvenMage.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, LEAF_OF_ORACLE ) ) {
                            return this.getPath( '30293-04.htm' )
                        }

                        state.startQuest()
                        state.setMemoState( 1 )

                        await QuestHelper.giveSingleItem( player, CRYSTAL_MEDALLION, 1 )
                        return this.getPath( '30293-05.htm' )
                    }

                    return this.getPath( '30293-03.htm' )
                }

                if ( player.getClassId() === ClassIdValues.oracle.id ) {
                    return this.getPath( '30293-02a.htm' )
                }

                return this.getPath( '30293-02.htm' )

            case '30424-08.html':
            case '30424-09.html':
                break

            case '30424-07.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            case 'replay_1':
                state.setMemoState( 2 )
                let npc = L2World.getObjectById( data.characterId )
                _.each( [ LIZARDMAN_WARRIOR, LIZARDMAN_SCOUT, LIZARDMAN_SOLDIER ], ( id: number ) => {
                    QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( id, npc, true, 0 ), player )
                } )

                return

            case '30428-02.html':
            case '30428-03.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case 'replay_2':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )

                    let location = L2World.getObjectById( data.characterId )
                    let npc = QuestHelper.addGenericSpawn( null, TAMIL, location.getX(), location.getY(), location.getZ(), location.getHeading(), true, 0, true, location.getInstanceId() )

                    QuestHelper.addAttackDesire( npc, player )
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case LIZARDMAN_WARRIOR:
                if ( !QuestHelper.hasQuestItem( player, LIZARD_CAPTAIN_ORDER ) ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.ARRGHHWE_SHALL_NEVER_SURRENDER )
                    await QuestHelper.giveSingleItem( player, LIZARD_CAPTAIN_ORDER, 1 )

                    state.setConditionWithSound( 3, true )
                }

                return

            case LIZARDMAN_SCOUT:
            case LIZARDMAN_SOLDIER:
                if ( !QuestHelper.hasQuestItem( player, LIZARD_CAPTAIN_ORDER ) ) {
                    await QuestHelper.giveSingleItem( player, LIZARD_CAPTAIN_ORDER, 1 )

                    state.setConditionWithSound( 3, true )
                }

                return

            case TAMIL: {
                if ( !QuestHelper.hasQuestItem( player, TAMIL_NECKLACE ) ) {
                    await QuestHelper.giveSingleItem( player, TAMIL_NECKLACE, 1 )

                    state.setConditionWithSound( 5, true )
                }

                return
            }
        }
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        switch ( NpcVariablesManager.get( data.targetId, this.getName() ) ) {
            case 1:
                if ( NpcVariablesManager.get( data.targetId, variableNames.firstAttacker ) !== data.attackerPlayerId ) {
                    NpcVariablesManager.set( data.targetId, this.getName(), 2 )
                }

                return

            case 2:
                return

            default:
                let npc = L2World.getObjectById( data.targetId ) as L2Npc
                switch ( data.targetNpcId ) {
                    case LIZARDMAN_WARRIOR:
                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_SACRED_FLAME_IS_OURS )
                        break

                    case LIZARDMAN_SCOUT:
                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_SACRED_FLAME_IS_OURS )
                        break

                    case LIZARDMAN_SOLDIER:
                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_SACRED_FLAME_IS_OURS )
                        break

                    case TAMIL:
                        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.AS_YOU_WISH_MASTER )
                        break
                }

                NpcVariablesManager.set( data.targetId, this.getName(), 1 )
                NpcVariablesManager.set( data.targetId, variableNames.firstAttacker, data.attackerPlayerId )

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === PRIEST_MANUEL ) {
                    if ( !QuestHelper.hasQuestItem( player, LEAF_OF_ORACLE ) ) {
                        return this.getPath( '30293-01.htm' )
                    }

                    return this.getPath( '30293-04.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case PRIEST_MANUEL:
                        if ( !QuestHelper.hasQuestItem( player, CRYSTAL_MEDALLION ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItems( player, SWINDLERS_MONEY, ALLANA_OF_DAIRY, LIZARD_CAPTAIN_ORDER ) ) {
                            if ( !QuestHelper.hasQuestItem( player, HALF_OF_DAIRY ) ) {
                                await QuestHelper.giveAdena( player, 163800, true )
                                await QuestHelper.giveSingleItem( player, LEAF_OF_ORACLE, 1 )

                                if ( player.getLevel() >= 20 ) {
                                    await QuestHelper.addExpAndSp( player, 320534, 20392 )
                                } else if ( player.getLevel() === 19 ) {
                                    await QuestHelper.addExpAndSp( player, 456128, 27090 )
                                } else {
                                    await QuestHelper.addExpAndSp( player, 591724, 33788 )
                                }

                                await state.exitQuest( false, true )
                                player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                                return this.getPath( '30293-08.html' )
                            }

                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, SWINDLERS_MONEY, ALLANA_OF_DAIRY, LIZARD_CAPTAIN_ORDER, HALF_OF_DAIRY ) ) {
                            if ( state.isMemoState( 2 ) ) {

                                state.setMemoState( 1 )
                                state.setConditionWithSound( 8 )

                                return this.getPath( '30293-09.html' )
                            }

                            state.setMemoState( 1 )
                            return this.getPath( '30293-06.html' )
                        }

                        return this.getPath( '30293-07.html' )

                    case ALLANA:
                        if ( !QuestHelper.hasQuestItem( player, CRYSTAL_MEDALLION ) ) {
                            break
                        }

                        let [ hasHalfDiary, hasMoney, hasAllanaDiary, hasOrder ]: Array<boolean> = QuestHelper.hasEachQuestItem( player, HALF_OF_DAIRY, SWINDLERS_MONEY, ALLANA_OF_DAIRY, LIZARD_CAPTAIN_ORDER )

                        if ( !hasOrder && !hasHalfDiary && !hasMoney && !hasAllanaDiary ) {
                            if ( state.isMemoState( 2 ) ) {
                                return this.getPath( '30424-05.html' )
                            }

                            if ( state.isMemoState( 1 ) ) {
                                state.setConditionWithSound( 2, true )
                                return this.getPath( '30424-01.html' )
                            }

                            break
                        }

                        if ( !hasOrder ) {
                            break
                        }

                        if ( !hasMoney && !hasAllanaDiary && !hasHalfDiary ) {
                            state.setMemoState( 2 )
                            state.setConditionWithSound( 4, true )

                            await QuestHelper.giveSingleItem( player, HALF_OF_DAIRY, 1 )
                            return this.getPath( '30424-02.html' )
                        }

                        if ( !hasMoney && !hasAllanaDiary && hasHalfDiary ) {
                            if ( state.isMemoState( 3 ) && !QuestHelper.hasQuestItem( player, TAMIL_NECKLACE ) ) {
                                state.setMemoState( 2 )
                                state.setConditionWithSound( 4, true )

                                return this.getPath( '30424-06.html' )
                            }

                            return this.getPath( '30424-03.html' )
                        }

                        if ( hasMoney && hasHalfDiary && !hasAllanaDiary ) {
                            await QuestHelper.takeSingleItem( player, HALF_OF_DAIRY, 1 )
                            await QuestHelper.giveSingleItem( player, ALLANA_OF_DAIRY, 1 )

                            state.setConditionWithSound( 9, true )
                            return this.getPath( '30424-04.html' )
                        }

                        if ( hasMoney && hasAllanaDiary ) {
                            state.setConditionWithSound( 7, true )
                            return this.getPath( '30424-05.html' )
                        }

                        break

                    case PERRIN:
                        if ( !QuestHelper.hasQuestItems( player, CRYSTAL_MEDALLION, LIZARD_CAPTAIN_ORDER, HALF_OF_DAIRY ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, TAMIL_NECKLACE ) ) {
                            await QuestHelper.giveSingleItem( player, SWINDLERS_MONEY, 1 )
                            await QuestHelper.takeSingleItem( player, TAMIL_NECKLACE, 1 )

                            state.setConditionWithSound( 6, true )
                            return this.getPath( '30428-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SWINDLERS_MONEY ) ) {
                            return this.getPath( '30428-05.html' )
                        }

                        if ( state.isMemoState( 3 ) ) {
                            return this.getPath( '30428-06.html' )
                        }

                        return this.getPath( '30428-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}