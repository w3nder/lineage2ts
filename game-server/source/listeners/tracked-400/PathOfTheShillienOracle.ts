import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const MAGISTER_SIDRA = 30330
const PRIEST_ADONIUS = 30375
const MAGISTER_TALBOT = 30377
const SIDRAS_LETTER = 1262
const BLANK_SHEET = 1263
const BLOODY_RUNE = 1264
const GARMIELS_BOOK = 1265
const PRAYER_OF_ADONIUS = 1266
const PENITENTS_MARK = 1267
const ASHEN_BONES = 1268
const ANDARIEL_BOOK = 1269
const ORB_OF_ABYSS = 1270
const ZOMBIE_SOLDIER = 20457
const ZOMBIE_WARRIOR = 20458
const SHIELD_SKELETON = 20514
const SKELETON_INFANTRYMAN = 20515
const DARK_SUCCUBUS = 20776
const minimumLevel = 18

export class PathOfTheShillienOracle extends ListenerLogic {
    constructor() {
        super( 'Q00413_PathOfTheShillienOracle', 'listeners/tracked-400/PathOfTheShillienOracle.ts' )
        this.questId = 413
        this.questItemIds = [
            SIDRAS_LETTER,
            BLANK_SHEET,
            BLOODY_RUNE,
            GARMIELS_BOOK,
            PRAYER_OF_ADONIUS,
            PENITENTS_MARK,
            ASHEN_BONES,
            ANDARIEL_BOOK,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00413_PathOfTheShillienOracle'
    }

    getQuestStartIds(): Array<number> {
        return [ MAGISTER_SIDRA ]
    }

    getTalkIds(): Array<number> {
        return [ MAGISTER_SIDRA, PRIEST_ADONIUS, MAGISTER_TALBOT ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ZOMBIE_SOLDIER,
            ZOMBIE_WARRIOR,
            SHIELD_SKELETON,
            SKELETON_INFANTRYMAN,
            DARK_SUCCUBUS,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.darkMage.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, ORB_OF_ABYSS ) ) {
                            return this.getPath( '30330-04.htm' )
                        }

                        return this.getPath( '30330-05.htm' )
                    }

                    return this.getPath( '30330-02.htm' )
                }

                if ( player.getClassId() === ClassIdValues.shillienOracle.id ) {
                    return this.getPath( '30330-02a.htm' )
                }

                return this.getPath( '30330-03.htm' )

            case '30330-06.htm':
                if ( !QuestHelper.hasQuestItem( player, SIDRAS_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, SIDRAS_LETTER, 1 )
                }

                state.startQuest()
                break

            case '30330-06a.html':
            case '30375-02.html':
            case '30375-03.html':
                break

            case '30375-04.html':
                if ( QuestHelper.hasQuestItem( player, PRAYER_OF_ADONIUS ) ) {
                    await QuestHelper.takeSingleItem( player, PRAYER_OF_ADONIUS, 1 )
                    await QuestHelper.giveSingleItem( player, PENITENTS_MARK, 1 )

                    state.setConditionWithSound( 5, true )
                }

                break

            case '30377-02.html':
                if ( QuestHelper.hasQuestItem( player, SIDRAS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, SIDRAS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, BLANK_SHEET, 5 )
                    state.setConditionWithSound( 2, true )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( npc.getId() ) {
            case ZOMBIE_SOLDIER:
            case ZOMBIE_WARRIOR:
            case SHIELD_SKELETON:
            case SKELETON_INFANTRYMAN:
                if ( QuestHelper.hasQuestItem( player, PENITENTS_MARK )
                        && QuestHelper.getQuestItemsCount( player, ASHEN_BONES ) < 10 ) {
                    await QuestHelper.rewardAndProgressState( player, state, ASHEN_BONES, 1, 10, data.isChampion, 6 )
                }

                return

            case DARK_SUCCUBUS:
                if ( QuestHelper.hasQuestItem( player, BLANK_SHEET ) ) {
                    await QuestHelper.takeSingleItem( player, BLANK_SHEET, 1 )
                    await QuestHelper.rewardAndProgressState( player, state, BLOODY_RUNE, 1, 5, data.isChampion, 3 )
                }

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MAGISTER_SIDRA ) {
                    return this.getPath( '30330-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MAGISTER_SIDRA:
                        if ( QuestHelper.hasQuestItem( player, SIDRAS_LETTER ) ) {
                            return this.getPath( '30330-07.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, BLANK_SHEET, BLOODY_RUNE ) ) {
                            return this.getPath( '30330-08.html' )
                        }

                        let hasAndarielBook: boolean = QuestHelper.hasQuestItem( player, ANDARIEL_BOOK )

                        if ( !hasAndarielBook
                                && QuestHelper.hasAtLeastOneQuestItem( player, PRAYER_OF_ADONIUS, GARMIELS_BOOK, PENITENTS_MARK, ASHEN_BONES ) ) {
                            return this.getPath( '30330-09.html' )
                        }

                        if ( hasAndarielBook || QuestHelper.hasQuestItem( player, GARMIELS_BOOK ) ) {
                            await QuestHelper.giveAdena( player, 163800, true )
                            await QuestHelper.giveSingleItem( player, ORB_OF_ABYSS, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 26532 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 33230 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 591724, 39928 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30330-10.html' )
                        }

                        break

                    case PRIEST_ADONIUS:
                        if ( QuestHelper.hasQuestItem( player, PRAYER_OF_ADONIUS ) ) {
                            return this.getPath( '30375-01.html' )
                        }

                        let ashenBonesAmount: number = QuestHelper.getQuestItemsCount( player, ASHEN_BONES )

                        if ( QuestHelper.hasQuestItem( player, PENITENTS_MARK )
                                && !QuestHelper.hasQuestItem( player, ANDARIEL_BOOK )
                                && ashenBonesAmount === 0 ) {
                            return this.getPath( '30375-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PENITENTS_MARK ) ) {
                            if ( ashenBonesAmount > 0 && ashenBonesAmount < 10 ) {
                                return this.getPath( '30375-06.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, PENITENTS_MARK, ASHEN_BONES )
                            await QuestHelper.giveSingleItem( player, ANDARIEL_BOOK, 1 )

                            state.setConditionWithSound( 7, true )
                            return this.getPath( '30375-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ANDARIEL_BOOK ) ) {
                            return this.getPath( '30375-08.html' )
                        }

                        break

                    case MAGISTER_TALBOT:
                        if ( QuestHelper.hasQuestItem( player, SIDRAS_LETTER ) ) {
                            return this.getPath( '30377-01.html' )
                        }

                        let runeAmount: number = QuestHelper.getQuestItemsCount( player, BLOODY_RUNE )

                        if ( runeAmount === 0 && QuestHelper.getQuestItemsCount( player, BLANK_SHEET ) >= 5 ) {
                            return this.getPath( '30377-03.html' )
                        }

                        if ( runeAmount > 0 && runeAmount < 5 ) {
                            return this.getPath( '30377-04.html' )
                        }

                        if ( runeAmount >= 5 ) {
                            await QuestHelper.takeSingleItem( player, BLOODY_RUNE, -1 )
                            await QuestHelper.giveMultipleItems( player, 1, GARMIELS_BOOK, PRAYER_OF_ADONIUS )

                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30377-05.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, PRAYER_OF_ADONIUS, PENITENTS_MARK, ASHEN_BONES ) ) {
                            return this.getPath( '30377-06.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ANDARIEL_BOOK, GARMIELS_BOOK ) ) {
                            return this.getPath( '30377-07.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}