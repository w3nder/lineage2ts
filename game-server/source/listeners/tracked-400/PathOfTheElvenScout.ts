import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const MASTER_REORIA = 30328
const GUARD_BABENCO = 30334
const GUARD_MORETTI = 30337
const PRIAS = 30426
const REISAS_LETTER = 1207
const PRIASS_1ND_TORN_LETTER = 1208
const PRIASS_2ND_TORN_LETTER = 1209
const PRIASS_3ND_TORN_LETTER = 1210
const PRIASS_4ND_TORN_LETTER = 1211
const MORETTIES_HERB = 1212
const MORETTIS_LETTER = 1214
const PRIASS_LETTER = 1215
const HONORARY_GUARD = 1216
const REISAS_RECOMMENDATION = 1217
const RUSTED_KEY = 1293
const OL_MAHUM_PATROL = 20053
const OL_MAHUM_SENTRY = 27031
const minimumLevel = 18

export class PathOfTheElvenScout extends ListenerLogic {
    constructor() {
        super( 'Q00407_PathOfTheElvenScout', 'listeners/tracked-400/PathOfTheElvenScout.ts' )
        this.questId = 407
        this.questItemIds = [
            REISAS_LETTER,
            PRIASS_1ND_TORN_LETTER,
            PRIASS_2ND_TORN_LETTER,
            PRIASS_3ND_TORN_LETTER,
            PRIASS_4ND_TORN_LETTER,
            MORETTIES_HERB,
            MORETTIS_LETTER,
            PRIASS_LETTER,
            HONORARY_GUARD,
            RUSTED_KEY,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ MASTER_REORIA ]
    }

    getTalkIds(): Array<number> {
        return [ MASTER_REORIA, GUARD_BABENCO, GUARD_MORETTI, PRIAS ]
    }

    getAttackableKillIds(): Array<number> {
        return [ OL_MAHUM_PATROL, OL_MAHUM_SENTRY ]
    }

    getAttackableAttackIds(): Array<number> {
        return [ OL_MAHUM_PATROL, OL_MAHUM_SENTRY ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00407_PathOfTheElvenScout'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.elvenFighter.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, REISAS_RECOMMENDATION ) ) {
                            return this.getPath( '30328-04.htm' )
                        }

                        state.startQuest()
                        state.setMemoState( 0 )

                        await QuestHelper.giveSingleItem( player, REISAS_LETTER, 1 )
                        return this.getPath( '30328-05.htm' )
                    }

                    return this.getPath( '30328-03.htm' )
                }

                if ( player.getClassId() === ClassIdValues.elvenScout.id ) {
                    return this.getPath( '30328-02a.htm' )
                }

                return this.getPath( '30328-02.htm' )

            case '30337-02.html':
                break

            case '30337-03.html':
                if ( QuestHelper.hasQuestItem( player, REISAS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, REISAS_LETTER, -1 )

                    state.setMemoState( 1 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        NpcVariablesManager.set( data.targetId, this.getName(), data.attackerPlayerId )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( NpcVariablesManager.get( data.targetId, this.getName() ) !== data.playerId ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( data.npcId === OL_MAHUM_SENTRY ) {
            if ( Math.random() > QuestHelper.getAdjustedChance( RUSTED_KEY, 0.6, data.isChampion ) ) {
                return
            }

            if ( state.isCondition( 5 )
                    && QuestHelper.hasQuestItems( player, MORETTIES_HERB, MORETTIS_LETTER )
                    && !QuestHelper.hasQuestItem( player, RUSTED_KEY ) ) {
                await QuestHelper.giveSingleItem( player, RUSTED_KEY, 1 )
                state.setConditionWithSound( 6, true )
            }

            return
        }

        if ( !state.isCondition( 2 ) ) {
            return
        }

        let itemId: number = [
            PRIASS_1ND_TORN_LETTER,
            PRIASS_2ND_TORN_LETTER,
            PRIASS_3ND_TORN_LETTER,
            PRIASS_4ND_TORN_LETTER ].find( ( itemId: number ): boolean => {
            return !QuestHelper.hasQuestItem( player, itemId )
        } )

        await QuestHelper.giveSingleItem( player, itemId, 1 )

        if ( QuestHelper.hasQuestItems( player, PRIASS_1ND_TORN_LETTER, PRIASS_2ND_TORN_LETTER, PRIASS_3ND_TORN_LETTER, PRIASS_4ND_TORN_LETTER ) ) {
            state.setConditionWithSound( 3, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MASTER_REORIA ) {
                    return this.getPath( '30328-01.htm' )
                }

                return

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MASTER_REORIA:
                        if ( QuestHelper.hasQuestItem( player, REISAS_LETTER ) ) {
                            return this.getPath( '30328-06.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, HONORARY_GUARD ) ) {
                            await QuestHelper.takeSingleItem( player, HONORARY_GUARD, -1 )
                            await QuestHelper.giveSingleItem( player, REISAS_RECOMMENDATION, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 19932 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 26630 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 591724, 33328 )
                            }

                            await QuestHelper.giveAdena( player, 163800, true )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30328-07.html' )
                        }

                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '30328-08.html' )
                        }

                        break

                    case GUARD_BABENCO:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '30334-01.html' )
                        }

                        break

                    case GUARD_MORETTI:
                        let hasItems: boolean = QuestHelper.hasQuestItems( player, PRIASS_1ND_TORN_LETTER, PRIASS_2ND_TORN_LETTER, PRIASS_3ND_TORN_LETTER, PRIASS_4ND_TORN_LETTER )
                        if ( QuestHelper.hasQuestItem( player, REISAS_LETTER ) && !hasItems ) {
                            return this.getPath( '30337-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PRIASS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, PRIASS_LETTER, -1 )
                            await QuestHelper.giveSingleItem( player, HONORARY_GUARD, 1 )

                            state.setConditionWithSound( 8, true )
                            return this.getPath( '30337-07.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, MORETTIES_HERB, MORETTIS_LETTER ) ) {
                            return this.getPath( '30337-09.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, HONORARY_GUARD ) ) {
                            return this.getPath( '30337-08.html' )
                        }

                        if ( state.isMemoState( 1 ) && !QuestHelper.hasQuestItem( player, MORETTIS_LETTER ) ) {
                            if ( !hasItems ) {
                                return this.getPath( '30337-04.html' )
                            }

                            if ( QuestHelper.hasAtLeastOneQuestItem( player, PRIASS_1ND_TORN_LETTER, PRIASS_2ND_TORN_LETTER, PRIASS_3ND_TORN_LETTER, PRIASS_4ND_TORN_LETTER ) ) {
                                return this.getPath( '30337-05.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, PRIASS_1ND_TORN_LETTER, PRIASS_2ND_TORN_LETTER, PRIASS_3ND_TORN_LETTER, PRIASS_4ND_TORN_LETTER )
                            await QuestHelper.giveMultipleItems( player, 1, MORETTIES_HERB, MORETTIS_LETTER )

                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30337-06.html' )
                        }

                        break

                    case PRIAS:
                        if ( QuestHelper.hasQuestItems( player, MORETTIS_LETTER, MORETTIES_HERB ) ) {
                            if ( !QuestHelper.hasQuestItem( player, RUSTED_KEY ) ) {
                                state.setConditionWithSound( 5, true )
                                return this.getPath( '30426-01.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, RUSTED_KEY, MORETTIES_HERB, MORETTIS_LETTER )
                            await QuestHelper.giveSingleItem( player, PRIASS_LETTER, 1 )

                            state.setConditionWithSound( 7, true )
                            return this.getPath( '30426-02.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PRIASS_LETTER ) ) {
                            return this.getPath( '30426-04.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}