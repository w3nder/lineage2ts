import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const ROSSELA = 30414
const GREENIS = 30157
const THALIA = 30371
const NORTHWIND = 30423
const ROSELLAS_LETTER = 1218
const RED_DOWN = 1219
const MAGICAL_POWERS_RUBY = 1220
const PURE_AQUAMARINE = 1221
const APPETIZING_APPLE = 1222
const GOLD_LEAVES = 1223
const IMMORTAL_LOVE = 1224
const AMETHYST = 1225
const NOBILITY_AMETHYST = 1226
const FERTILITY_PERIDOT = 1229
const GREENISS_CHARM = 1272
const SAP_OF_THE_MOTHER_TREE = 1273
const LUCKY_POTPOURRI = 1274
const ETERNITY_DIAMOND = 1230
const DRYAD_ELDER = 20019
const SUKAR_WERERAT_LEADER = 20047
const PINCER_SPIDER = 20466
const minimumLevel = 18

export class PathOfTheElvenWizard extends ListenerLogic {
    constructor() {
        super( 'Q00408_PathOfTheElvenWizard', 'listeners/tracked-400/PathOfTheElvenWizard.ts' )
        this.questId = 408
        this.questItemIds = [
            ROSELLAS_LETTER,
            RED_DOWN,
            MAGICAL_POWERS_RUBY,
            PURE_AQUAMARINE,
            APPETIZING_APPLE,
            GOLD_LEAVES,
            IMMORTAL_LOVE,
            AMETHYST,
            NOBILITY_AMETHYST,
            FERTILITY_PERIDOT,
            GREENISS_CHARM,
            SAP_OF_THE_MOTHER_TREE,
            LUCKY_POTPOURRI,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00408_PathOfTheElvenWizard'
    }

    getQuestStartIds(): Array<number> {
        return [ ROSSELA ]
    }

    getTalkIds(): Array<number> {
        return [ ROSSELA, GREENIS, THALIA, NORTHWIND ]
    }

    getAttackableKillIds(): Array<number> {
        return [ DRYAD_ELDER, SUKAR_WERERAT_LEADER, PINCER_SPIDER ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() !== ClassIdValues.elvenMage.id ) {
                    if ( player.getClassId() === ClassIdValues.elvenWizard.id ) {
                        return this.getPath( '30414-02a.htm' )
                    }

                    return this.getPath( '30414-03.htm' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30414-04.htm' )
                }

                if ( QuestHelper.hasQuestItem( player, ETERNITY_DIAMOND ) ) {
                    return this.getPath( '30414-05.htm' )
                }

                if ( !QuestHelper.hasQuestItem( player, FERTILITY_PERIDOT ) ) {
                    await QuestHelper.giveSingleItem( player, FERTILITY_PERIDOT, 1 )
                }

                state.startQuest()
                return this.getPath( '30414-06.htm' )

            case '30414-02.htm':
                break

            case '30414-10.html':
                if ( QuestHelper.hasQuestItem( player, MAGICAL_POWERS_RUBY ) ) {
                    break
                }

                if ( !QuestHelper.hasQuestItem( player, MAGICAL_POWERS_RUBY )
                        && QuestHelper.hasQuestItem( player, FERTILITY_PERIDOT ) ) {
                    if ( !QuestHelper.hasQuestItem( player, ROSELLAS_LETTER ) ) {
                        await QuestHelper.giveSingleItem( player, ROSELLAS_LETTER, 1 )
                    }

                    return this.getPath( '30414-07.html' )
                }

                return

            case '30414-12.html':
                if ( QuestHelper.hasQuestItem( player, PURE_AQUAMARINE ) ) {
                    break
                }

                if ( !QuestHelper.hasQuestItem( player, PURE_AQUAMARINE )
                        && QuestHelper.hasQuestItem( player, FERTILITY_PERIDOT ) ) {
                    if ( !QuestHelper.hasQuestItem( player, APPETIZING_APPLE ) ) {
                        await QuestHelper.giveSingleItem( player, APPETIZING_APPLE, 1 )
                    }

                    return this.getPath( '30414-13.html' )
                }

                return

            case '30414-16.html':
                if ( QuestHelper.hasQuestItem( player, NOBILITY_AMETHYST ) ) {
                    break
                }

                if ( !QuestHelper.hasQuestItem( player, NOBILITY_AMETHYST )
                        && QuestHelper.hasQuestItem( player, FERTILITY_PERIDOT ) ) {
                    if ( !QuestHelper.hasQuestItem( player, IMMORTAL_LOVE ) ) {
                        await QuestHelper.giveSingleItem( player, IMMORTAL_LOVE, 1 )
                    }

                    return this.getPath( '30414-17.html' )
                }

                return

            case '30157-02.html':
                if ( QuestHelper.hasQuestItem( player, ROSELLAS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, ROSELLAS_LETTER, 1 )

                    if ( !QuestHelper.hasQuestItem( player, GREENISS_CHARM ) ) {
                        await QuestHelper.giveSingleItem( player, GREENISS_CHARM, 1 )
                    }
                }

                break

            case '30371-02.html':
                if ( QuestHelper.hasQuestItem( player, APPETIZING_APPLE ) ) {
                    await QuestHelper.takeSingleItem( player, APPETIZING_APPLE, 1 )

                    if ( !QuestHelper.hasQuestItem( player, SAP_OF_THE_MOTHER_TREE ) ) {
                        await QuestHelper.giveSingleItem( player, SAP_OF_THE_MOTHER_TREE, 1 )
                    }
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case DRYAD_ELDER:
                await this.onMobKilled( player, SAP_OF_THE_MOTHER_TREE, GOLD_LEAVES, 5, 0.4, data.isChampion )
                return

            case SUKAR_WERERAT_LEADER:
                await this.onMobKilled( player, LUCKY_POTPOURRI, AMETHYST, 2, 0.4, data.isChampion )
                return

            case PINCER_SPIDER:
                await this.onMobKilled( player, GREENISS_CHARM, RED_DOWN, 5, 0.7, data.isChampion )
                return
        }
    }

    async onMobKilled( player: L2PcInstance, requiredItemId: number, givenItemId: number, limit: number, chance: number, isFromChampion: boolean ): Promise<void> {
        if ( Math.random() > QuestHelper.getAdjustedChance( givenItemId, chance, isFromChampion ) ) {
            return
        }

        if ( QuestHelper.hasQuestItem( player, requiredItemId )
                && QuestHelper.getQuestItemsCount( player, givenItemId ) < limit ) {
            await QuestHelper.rewardSingleQuestItem( player, givenItemId, 1, isFromChampion )

            if ( QuestHelper.getQuestItemsCount( player, GOLD_LEAVES ) >= limit ) {
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === ROSSELA ) {
                    return this.getPath( '30414-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case ROSSELA:
                        if ( QuestHelper.hasQuestItem( player, ROSELLAS_LETTER ) ) {
                            return this.getPath( '30414-08.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, GREENISS_CHARM ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, RED_DOWN ) < 5 ) {
                                return this.getPath( '30414-09.html' )
                            }

                            return this.getPath( '30414-21.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, APPETIZING_APPLE ) ) {
                            return this.getPath( '30414-14.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SAP_OF_THE_MOTHER_TREE ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, GOLD_LEAVES ) < 5 ) {
                                return this.getPath( '30414-15.html' )
                            }

                            return this.getPath( '30414-22.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, IMMORTAL_LOVE ) ) {
                            return this.getPath( '30414-18.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LUCKY_POTPOURRI ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, AMETHYST ) < 2 ) {
                                return this.getPath( '30414-19.html' )
                            }

                            return this.getPath( '30414-23.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, FERTILITY_PERIDOT ) ) {
                            if ( !QuestHelper.hasQuestItems( player, MAGICAL_POWERS_RUBY, NOBILITY_AMETHYST, PURE_AQUAMARINE ) ) {
                                return this.getPath( '30414-11.html' )
                            }

                            await QuestHelper.giveAdena( player, 163800, true )
                            if ( !QuestHelper.hasQuestItem( player, ETERNITY_DIAMOND ) ) {
                                await QuestHelper.giveSingleItem( player, ETERNITY_DIAMOND, 1 )
                            }

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 22532 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 29230 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 591724, 35928 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30414-20.html' )
                        }

                        break

                    case GREENIS:
                        if ( QuestHelper.hasQuestItem( player, ROSELLAS_LETTER ) ) {
                            return this.getPath( '30157-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, GREENISS_CHARM ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, RED_DOWN ) < 5 ) {
                                return this.getPath( '30157-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, RED_DOWN, GREENISS_CHARM )

                            if ( !QuestHelper.hasQuestItem( player, MAGICAL_POWERS_RUBY ) ) {
                                await QuestHelper.giveSingleItem( player, MAGICAL_POWERS_RUBY, 1 )
                            }

                            return this.getPath( '30157-04.html' )
                        }

                        break

                    case THALIA:
                        if ( QuestHelper.hasQuestItem( player, APPETIZING_APPLE ) ) {
                            return this.getPath( '30371-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SAP_OF_THE_MOTHER_TREE ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, GOLD_LEAVES ) < 5 ) {
                                return this.getPath( '30371-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, GOLD_LEAVES, SAP_OF_THE_MOTHER_TREE )

                            if ( !QuestHelper.hasQuestItem( player, PURE_AQUAMARINE ) ) {
                                await QuestHelper.giveSingleItem( player, PURE_AQUAMARINE, 1 )
                            }

                            return this.getPath( '30371-04.html' )
                        }

                        break

                    case NORTHWIND:
                        if ( QuestHelper.hasQuestItem( player, IMMORTAL_LOVE ) ) {
                            await QuestHelper.takeSingleItem( player, IMMORTAL_LOVE, 1 )

                            if ( !QuestHelper.hasQuestItem( player, LUCKY_POTPOURRI ) ) {
                                await QuestHelper.giveSingleItem( player, LUCKY_POTPOURRI, 1 )
                            }

                            return this.getPath( '30423-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LUCKY_POTPOURRI ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, AMETHYST ) < 2 ) {
                                return this.getPath( '30423-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, AMETHYST, LUCKY_POTPOURRI )

                            if ( !QuestHelper.hasQuestItem( player, NOBILITY_AMETHYST ) ) {
                                await QuestHelper.giveSingleItem( player, NOBILITY_AMETHYST, 1 )
                            }

                            return this.getPath( '30423-03.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}