import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const BATRACOS = 32740
const JOHNNY = 32744
const TANTA_GUARD = 18862
const SEER_UGOROS_PASS = 15496
const minimumLevel = 82

export class TakeYourBestShot extends ListenerLogic {
    constructor() {
        super( 'Q00423_TakeYourBestShot', 'listeners/tracked-400/TakeYourBestShot.ts' )
        this.questId = 423
    }

    getQuestStartIds(): Array<number> {
        return [ JOHNNY, BATRACOS ]
    }

    getTalkIds(): Array<number> {
        return [ JOHNNY, BATRACOS ]
    }

    getAttackableKillIds(): Array<number> {
        return [ TANTA_GUARD ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00423_TakeYourBestShot'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32744-06.htm':
                if ( state.isCreated() && player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    state.setMemoState( 1 )
                    break
                }

                return

            case '32744-04.html':
            case '32744-05.htm':
                if ( !QuestHelper.hasQuestItems( player, SEER_UGOROS_PASS ) && player.getLevel() >= minimumLevel ) {
                    break
                }

                return

            case '32744-07.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() || !state.isMemoState( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( !QuestHelper.hasQuestItem( player, SEER_UGOROS_PASS ) ) {
            state.setMemoState( 2 )
            state.setConditionWithSound( 2, true )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === JOHNNY ) {
                    if ( QuestHelper.hasQuestItem( player, SEER_UGOROS_PASS ) ) {
                        return this.getPath( '32744-02.htm' )
                    }

                    let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00249_PoisonedPlainsOfTheLizardmen' )
                    return this.getPath( canProceed ? '32744-03.htm' : '32744-01.htm' )
                }

                if ( data.characterNpcId === BATRACOS ) {
                    return this.getPath( QuestHelper.hasQuestItem( player, SEER_UGOROS_PASS ) ? '32740-04.html' : '32740-01.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case JOHNNY:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32744-08.html' )
                        }

                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '32744-09.html' )
                        }

                        break

                    case BATRACOS:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32740-02.html' )
                        }

                        if ( state.isMemoState( 2 ) ) {
                            await QuestHelper.giveSingleItem( player, SEER_UGOROS_PASS, 1 )
                            await state.exitQuest( true, true )

                            return this.getPath( '32740-03.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}