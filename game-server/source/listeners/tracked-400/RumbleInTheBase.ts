import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestType } from '../../gameService/enums/QuestType'

import _ from 'lodash'

const STAN = 30200
const SHINY_SALMON = 15503
const SHOES_STRING_OF_SEL_MAHUM = 16382

const monsterRewardChances: { [ npcId: number ]: number } = {
    22780: 0.581,
    22781: 0.772,
    22782: 0.581,
    22783: 0.563,
    22784: 0.581,
    22785: 0.271,
    18908: 0.782,
}

const minimumLevel = 82

export class RumbleInTheBase extends ListenerLogic {
    constructor() {
        super( 'Q00461_RumbleInTheBase', 'listeners/tracked-400/RumbleInTheBase.ts' )
        this.questId = 461
        this.questItemIds = [
            SHINY_SALMON,
            SHOES_STRING_OF_SEL_MAHUM,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ STAN ]
    }

    getTalkIds(): Array<number> {
        return [ STAN ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30200-05.htm':
                state.startQuest()
                break

            case '30200-04.htm':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > QuestHelper.getAdjustedChance( SHINY_SALMON, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        if ( data.npcId === 18908 ) {
            let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )
            let player = L2World.getPlayer( data.playerId )

            if ( !state || !state.isCondition( 1 ) || QuestHelper.getQuestItemsCount( player, SHINY_SALMON ) >= 5 ) {
                return
            }

            await QuestHelper.rewardSingleQuestItem( player, SHINY_SALMON, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            this.progressQuest( player, state )

            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }


        if ( QuestHelper.getQuestItemsCount( player, SHOES_STRING_OF_SEL_MAHUM ) >= 10 ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, SHOES_STRING_OF_SEL_MAHUM, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
        this.progressQuest( player, state )
    }

    progressQuest( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.getQuestItemsCount( player, SHINY_SALMON ) >= 5 && QuestHelper.getQuestItemsCount( player, SHOES_STRING_OF_SEL_MAHUM ) >= 10 ) {
            state.setConditionWithSound( 2, true )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '30200-03.htm' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                let shouldProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00252_ItSmellsDelicious' )
                return this.getPath( shouldProceed ? '30200-01.htm' : '30200-02.htm' )

            case QuestStateValues.STARTED:
                if ( state.isCondition( 1 ) ) {
                    return this.getPath( '30200-06.html' )
                }

                await QuestHelper.addExpAndSp( player, 224784, 342528 )
                await state.exitQuestWithType( QuestType.DAILY, true )

                return this.getPath( '30200-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00461_RumbleInTheBase'
    }
}