import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestType } from '../../gameService/enums/QuestType'

const JAKAN = 32773
const TAG_ID = 15513
const minimumLevel = 84

export class FindingtheLostSoldiers extends ListenerLogic {
    constructor() {
        super( 'Q00452_FindingtheLostSoldiers', 'listeners/tracked-400/FindingtheLostSoldiers.ts' )
        this.questId = 452
        this.questItemIds = [ TAG_ID ]
    }

    getTalkIds(): Array<number> {
        return [
            32769,
            32770,
            32771,
            32772,
            JAKAN,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ JAKAN ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00452_FindingtheLostSoldiers'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( data.characterNpcId === JAKAN ) {
            if ( data.eventName === '32773-3.htm' ) {
                state.startQuest()
                return this.getPath( data.eventName )
            }

            return
        }

        if ( state.isCondition( 1 ) ) {
            if ( Math.random() < 0.5 ) {
                return this.getPath( 'corpse-3.html' )
            }

            await QuestHelper.giveSingleItem( player, TAG_ID, 1 )

            state.setConditionWithSound( 2, true )
            await ( L2World.getObjectById( data.characterId ) as L2Npc ).deleteMe()

            return this.getPath( data.eventName )
        }

        return this.getPath( 'corpse-3.html' )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        if ( data.characterNpcId === JAKAN ) {
            switch ( state.getState() ) {
                case QuestStateValues.CREATED:
                    return this.getPath( player.getLevel() < minimumLevel ? '32773-0.html' : '32773-1.htm' )

                case QuestStateValues.STARTED:
                    if ( state.isCondition( 1 ) ) {
                        return this.getPath( '32773-4.html' )
                    }

                    if ( state.isCondition( 2 ) ) {
                        await QuestHelper.takeSingleItem( player, TAG_ID, -1 )
                        await QuestHelper.giveAdena( player, 95200, true )
                        await QuestHelper.addExpAndSp( player, 435024, 50366 )
                        await state.exitQuestWithType( QuestType.DAILY, true )

                        return this.getPath( '32773-5.html' )
                    }

                    break

                case QuestStateValues.COMPLETED:
                    if ( state.isNowAvailable() ) {
                        state.setState( QuestStateValues.CREATED )

                        return this.getPath( player.getLevel() < minimumLevel ? '32773-0.html' : '32773-1.htm' )
                    }

                    return this.getPath( '32773-6.html' )
            }

            return QuestHelper.getNoQuestMessagePath()
        }

        if ( state.isCondition( 1 ) ) {
            return this.getPath( 'corpse-1.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}