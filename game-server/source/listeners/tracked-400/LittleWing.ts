import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const MARIA = 30608
const CRONOS = 30610
const BYRON = 30711
const MIMYU = 30747
const EXARION = 30748
const ZWOV = 30749
const KALIBRAN = 30750
const SUZET = 30751
const SHAMHAI = 30752
const COOPER = 30829
const COAL = 1870
const CHARCOAL = 1871
const SILVER_NUGGET = 1873
const STONE_OF_PURITY = 1875
const GEMSTONE_D = 2130
const GEMSTONE_C = 2131
const FAIRY_DUST = 3499
const FAIRY_STONE = 3816
const DELUXE_FAIRY_STONE = 3817
const FAIRY_STONE_LIST = 3818
const DELUXE_STONE_LIST = 3819
const TOAD_SKIN = 3820
const MONKSHOOD_JUICE = 3821
const EXARION_SCALE = 3822
const EXARION_EGG = 3823
const ZWOV_SCALE = 3824
const ZWOV_EGG = 3825
const KALIBRAN_SCALE = 3826
const KALIBRAN_EGG = 3827
const SUZET_SCALE = 3828
const SUZET_EGG = 3829
const SHAMHAI_SCALE = 3830
const SHAMHAI_EGG = 3831
const DEAD_SEEKER = 20202
const TOAD_LORD = 20231
const MARSH_SPIDER = 20233
const BREKA_OVERLORD = 20270
const ROAD_SCAVENGER = 20551
const LETO_WARRIOR = 20580
const DRAGONFLUTE_OF_WIND = 3500
const DRAGONFLUTE_OF_STAR = 3501
const DRAGONFLUTE_OF_TWILIGHT = 3502
const HATCHLING_ARMOR = 3912
const HATCHLING_FOOD = 4038
const minimumLevel = 35

const eggItems: Array<number> = [
    EXARION_EGG,
    SUZET_EGG,
    KALIBRAN_EGG,
    SHAMHAI_EGG,
    ZWOV_EGG,
]

const eggDrops = {
    [ DEAD_SEEKER ]: SHAMHAI_EGG,
    [ MARSH_SPIDER ]: ZWOV_EGG,
    [ BREKA_OVERLORD ]: SUZET_EGG,
    [ ROAD_SCAVENGER ]: KALIBRAN_EGG,
    [ LETO_WARRIOR ]: EXARION_EGG,
}

const variableNames = {
    oldStone: 'os',
    fairyStone: 'fs',
    chosenNpcId: 'cn',
}

export class LittleWing extends ListenerLogic {
    constructor() {
        super( 'Q00420_LittleWing', 'listeners/tracked-400/LittleWing.ts' )
        this.questId = 420
        this.questItemIds = [
            FAIRY_DUST,
            FAIRY_STONE,
            DELUXE_FAIRY_STONE,
            FAIRY_STONE_LIST,
            DELUXE_STONE_LIST,
            TOAD_SKIN,
            MONKSHOOD_JUICE,
            EXARION_SCALE,
            EXARION_EGG,
            ZWOV_SCALE,
            ZWOV_EGG,
            KALIBRAN_SCALE,
            KALIBRAN_EGG,
            SUZET_SCALE,
            SUZET_EGG,
            SHAMHAI_SCALE,
            SHAMHAI_EGG,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00420_LittleWing'
    }

    getQuestStartIds(): Array<number> {
        return [ COOPER ]
    }

    getTalkIds(): Array<number> {
        return [
            MARIA,
            CRONOS,
            BYRON,
            MIMYU,
            EXARION,
            ZWOV,
            KALIBRAN,
            SUZET,
            SHAMHAI,
            COOPER,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [
            20589, // Fline
            20590, // Liele
            20591, // Valley Treant
            20592, // Satyr
            20593, // Unicorn
            20594, // Forest Runner
            20595, // Fline Elder
            20596, // Liele Elder
            20597, // Valley Treant Elder
            20598, // Satyr Elder
            20599, // Unicorn Elder
            27185, // Fairy Tree of Wind (Quest Monster)
            27186, // Fairy Tree of Star (Quest Monster)
            27187, // Fairy Tree of Twilight (Quest Monster)
            27188, // Fairy Tree of Abyss (Quest Monster)
            27189, // Soul of Tree Guardian (Quest Monster)
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            TOAD_LORD,
            DEAD_SEEKER,
            MARSH_SPIDER,
            BREKA_OVERLORD,
            ROAD_SCAVENGER,
            LETO_WARRIOR,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30610-02.html':
            case '30610-03.html':
            case '30610-04.html':
            case '30711-02.html':
            case '30747-05.html':
            case '30747-06.html':
            case '30751-02.html':
                break

            case '30829-02.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    break
                }

                return

            case '30610-05.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    state.setVariable( variableNames.oldStone, 0 )
                    state.setVariable( variableNames.fairyStone, 1 )

                    await QuestHelper.giveSingleItem( player, FAIRY_STONE_LIST, 1 )
                    break
                }

                return

            case '30610-06.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    state.setVariable( variableNames.oldStone, 0 )
                    state.setVariable( variableNames.fairyStone, 2 )

                    await QuestHelper.giveSingleItem( player, DELUXE_STONE_LIST, 1 )
                    break
                }

                return

            case '30610-12.html':
                if ( state.isCondition( 5 ) ) {
                    state.setConditionWithSound( 2, true )
                    state.setVariable( variableNames.oldStone, state.getVariable( variableNames.fairyStone ) )
                    state.setVariable( variableNames.fairyStone, 1 )

                    await QuestHelper.giveSingleItem( player, FAIRY_STONE_LIST, 1 )
                    break
                }

                return

            case '30610-13.html':
                if ( state.isCondition( 5 ) ) {
                    state.setConditionWithSound( 2, true )
                    state.setVariable( variableNames.oldStone, state.getVariable( variableNames.fairyStone ) )
                    state.setVariable( variableNames.fairyStone, 2 )

                    await QuestHelper.giveSingleItem( player, DELUXE_STONE_LIST, 1 )
                    break
                }

                return

            case '30608-03.html':
                if ( state.isCondition( 2 ) ) {
                    if ( state.getVariable( variableNames.fairyStone ) === 1
                            && QuestHelper.getQuestItemsCount( player, COAL ) >= 10
                            && QuestHelper.getQuestItemsCount( player, CHARCOAL ) >= 10
                            && QuestHelper.getQuestItemsCount( player, GEMSTONE_D ) >= 1
                            && QuestHelper.getQuestItemsCount( player, SILVER_NUGGET ) >= 3
                            && ( QuestHelper.getQuestItemsCount( player, TOAD_SKIN ) >= 10 ) ) {
                        await QuestHelper.takeMultipleItems( player, -1, FAIRY_STONE_LIST, TOAD_SKIN )
                        await QuestHelper.takeMultipleItems( player, 10, COAL, CHARCOAL )
                        await QuestHelper.takeSingleItem( player, GEMSTONE_D, 1 )
                        await QuestHelper.takeSingleItem( player, SILVER_NUGGET, 3 )
                        await QuestHelper.giveSingleItem( player, FAIRY_STONE, 1 )
                    }

                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '30608-05.html':
                if ( state.isCondition( 2 ) ) {
                    if ( state.getVariable( variableNames.fairyStone ) === 2
                            && QuestHelper.getQuestItemsCount( player, COAL ) >= 10
                            && QuestHelper.getQuestItemsCount( player, CHARCOAL ) >= 10
                            && QuestHelper.getQuestItemsCount( player, GEMSTONE_C ) >= 1
                            && QuestHelper.getQuestItemsCount( player, STONE_OF_PURITY ) >= 1
                            && QuestHelper.getQuestItemsCount( player, SILVER_NUGGET ) >= 5
                            && QuestHelper.getQuestItemsCount( player, TOAD_SKIN ) >= 20 ) {
                        await QuestHelper.takeMultipleItems( player, -1, DELUXE_STONE_LIST, TOAD_SKIN )
                        await QuestHelper.takeMultipleItems( player, 10, COAL, CHARCOAL )
                        await QuestHelper.takeSingleItem( player, GEMSTONE_C, 1 )
                        await QuestHelper.takeSingleItem( player, SILVER_NUGGET, 5 )
                        await QuestHelper.giveSingleItem( player, STONE_OF_PURITY, 1 )
                        await QuestHelper.giveSingleItem( player, DELUXE_FAIRY_STONE, 1 )
                    }

                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '30711-03.html':
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )

                    if ( state.getVariable( variableNames.fairyStone ) === 2 ) {
                        return this.getPath( '30711-04.html' )
                    }

                    break
                }

                return

            case '30747-02.html':
            case '30747-04.html':
                if ( state.isCondition( 4 )
                        && QuestHelper.hasAtLeastOneQuestItem( player, FAIRY_STONE, DELUXE_FAIRY_STONE ) ) {
                    await QuestHelper.takeMultipleItems( player, -1, FAIRY_STONE, DELUXE_FAIRY_STONE )
                    if ( state.getVariable( variableNames.fairyStone ) === 2 ) {
                        await QuestHelper.giveSingleItem( player, FAIRY_DUST, 1 )
                    }

                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '30747-07.html':
            case '30747-08.html':
                if ( state.isCondition( 5 ) && !QuestHelper.hasQuestItem( player, MONKSHOOD_JUICE ) ) {
                    await QuestHelper.giveSingleItem( player, MONKSHOOD_JUICE, 1 )
                    break
                }

                return

            case '30747-12.html':
                if ( state.isCondition( 7 ) ) {
                    if ( state.getVariable( variableNames.fairyStone ) === 1
                            || !QuestHelper.hasQuestItem( player, FAIRY_DUST ) ) {
                        await this.giveReward( player )
                        await state.exitQuest( true, true )

                        return this.getPath( '30747-16.html' )
                    }

                    state.setConditionWithSound( 8, false )
                    break
                }

                if ( state.isCondition( 8 ) ) {
                    break
                }

                return

            case '30747-13.html':
                if ( state.isCondition( 8 ) ) {
                    await this.giveReward( player )
                    await state.exitQuest( true, true )
                    break
                }

                return

            case '30747-15.html':
                if ( state.isCondition( 8 ) && QuestHelper.hasQuestItem( player, FAIRY_DUST ) ) {

                    await QuestHelper.takeSingleItem( player, FAIRY_DUST, -1 )
                    await this.giveReward( player )

                    if ( Math.random() < 0.05 ) {
                        await QuestHelper.rewardSingleItem( player, HATCHLING_ARMOR, 1 )
                        await state.exitQuest( true, true )

                        return this.getPath( '30747-14.html' )
                    }

                    await QuestHelper.rewardSingleItem( player, HATCHLING_FOOD, 20 )
                    await state.exitQuest( true, true )
                    break
                }

                return

            case '30748-02.html':
                if ( state.isCondition( 5 ) ) {
                    await QuestHelper.takeSingleItem( player, MONKSHOOD_JUICE, -1 )
                    await QuestHelper.giveSingleItem( player, EXARION_SCALE, 1 )

                    state.setConditionWithSound( 6, true )
                    state.setVariable( variableNames.chosenNpcId, LETO_WARRIOR )

                    break
                }

                return

            case '30749-02.html':
                if ( state.isCondition( 5 ) ) {
                    await QuestHelper.takeSingleItem( player, MONKSHOOD_JUICE, -1 )
                    await QuestHelper.giveSingleItem( player, ZWOV_SCALE, 1 )

                    state.setConditionWithSound( 6, true )
                    state.setVariable( variableNames.chosenNpcId, MARSH_SPIDER )

                    break
                }

                return

            case '30750-02.html':
                if ( state.isCondition( 5 ) ) {
                    await QuestHelper.takeSingleItem( player, MONKSHOOD_JUICE, -1 )
                    await QuestHelper.giveSingleItem( player, KALIBRAN_SCALE, 1 )

                    state.setConditionWithSound( 6, true )
                    state.setVariable( variableNames.chosenNpcId, ROAD_SCAVENGER )

                    break
                }

                return

            case '30750-05.html':
                if ( state.isCondition( 6 )
                        && QuestHelper.getQuestItemsCount( player, KALIBRAN_EGG ) >= 20 ) {
                    await QuestHelper.takeMultipleItems( player, -1, KALIBRAN_SCALE, KALIBRAN_EGG )
                    await QuestHelper.giveSingleItem( player, KALIBRAN_EGG, 1 )

                    state.setConditionWithSound( 7, true )
                    break
                }

                return

            case '30751-03.html':
                if ( state.isCondition( 5 ) ) {
                    await QuestHelper.takeSingleItem( player, MONKSHOOD_JUICE, -1 )
                    await QuestHelper.giveSingleItem( player, SUZET_SCALE, 1 )

                    state.setConditionWithSound( 6, true )
                    state.setVariable( variableNames.chosenNpcId, BREKA_OVERLORD )

                    break
                }

                return

            case '30752-02.html':
                if ( state.isCondition( 5 ) ) {
                    await QuestHelper.takeSingleItem( player, MONKSHOOD_JUICE, -1 )
                    await QuestHelper.giveSingleItem( player, SHAMHAI_SCALE, 1 )

                    state.setConditionWithSound( 6, true )
                    state.setVariable( variableNames.chosenNpcId, DEAD_SEEKER )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || Math.random() > QuestHelper.getAdjustedChance( DELUXE_FAIRY_STONE, 0.3, data.isChampion ) ) {
            return
        }

        let player = L2World.getPlayer( data.attackerPlayerId )
        if ( !QuestHelper.hasQuestItem( player, DELUXE_FAIRY_STONE ) ) {
            return
        }

        await QuestHelper.takeSingleItem( player, DELUXE_FAIRY_STONE, -1 )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_STONE_THE_ELVEN_STONE_BROKE )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === COOPER ) {
                    return this.getPath( player.getLevel() >= minimumLevel ? '30829-01.htm' : '30829-03.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case COOPER:
                        return this.getPath( '30829-04.html' )

                    case CRONOS:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30610-01.html' )

                            case 2:
                                return this.getPath( '30610-07.html' )

                            case 3:
                                if ( state.getVariable( variableNames.oldStone ) > 0 ) {
                                    return this.getPath( '30610-14.html' )
                                }

                                return this.getPath( '30610-08.html' )

                            case 4:
                                return this.getPath( '30610-09.html' )

                            case 5:
                                if ( !QuestHelper.hasQuestItems( player, FAIRY_STONE, DELUXE_FAIRY_STONE ) ) {
                                    return this.getPath( '30610-10.html' )
                                }

                                return this.getPath( '30610-11.html' )

                        }

                        break

                    case MARIA:
                        switch ( state.getCondition() ) {
                            case 2:
                                if ( state.getVariable( variableNames.fairyStone ) === 1
                                        && QuestHelper.getQuestItemsCount( player, COAL ) >= 10
                                        && QuestHelper.getQuestItemsCount( player, CHARCOAL ) >= 10
                                        && QuestHelper.getQuestItemsCount( player, GEMSTONE_D ) >= 1
                                        && QuestHelper.getQuestItemsCount( player, SILVER_NUGGET ) >= 3
                                        && QuestHelper.getQuestItemsCount( player, TOAD_SKIN ) >= 10 ) {
                                    return this.getPath( '30608-02.html' )
                                }

                                if ( state.getVariable( variableNames.fairyStone ) === 2
                                        && QuestHelper.getQuestItemsCount( player, COAL ) >= 10
                                        && QuestHelper.getQuestItemsCount( player, CHARCOAL ) >= 10
                                        && QuestHelper.getQuestItemsCount( player, GEMSTONE_C ) >= 1
                                        && QuestHelper.getQuestItemsCount( player, STONE_OF_PURITY ) >= 1
                                        && QuestHelper.getQuestItemsCount( player, SILVER_NUGGET ) >= 5
                                        && QuestHelper.getQuestItemsCount( player, TOAD_SKIN ) >= 20 ) {
                                    return this.getPath( '30608-04.html' )
                                }

                                return this.getPath( '30608-01.html' )

                            case 3:
                                return this.getPath( '30608-06.html' )
                        }

                        break

                    case BYRON:
                        switch ( state.getCondition() ) {
                            case 2:
                                return this.getPath( '30711-10.html' )

                            case 3:
                                if ( state.getVariable( variableNames.oldStone ) === 0 ) {
                                    return this.getPath( '30711-01.html' )
                                }

                                if ( state.getVariable( variableNames.oldStone ) === 1 ) {
                                    state.setConditionWithSound( 5, true )
                                    return this.getPath( '30711-05.html' )
                                }

                                state.setConditionWithSound( 4, true )
                                return this.getPath( '30711-06.html' )

                            case 4:
                                if ( !QuestHelper.hasQuestItem( player, FAIRY_STONE ) ) {
                                    if ( !QuestHelper.hasQuestItem( player, DELUXE_FAIRY_STONE ) ) {
                                        return this.getPath( '30711-09.html' )
                                    }

                                    return this.getPath( '30711-08.html' )
                                }

                                return this.getPath( '30711-07.html' )
                        }

                        break

                    case MIMYU:
                        switch ( state.getCondition() ) {
                            case 4:
                                if ( QuestHelper.hasQuestItem( player, FAIRY_STONE ) ) {
                                    return this.getPath( '30747-01.html' )
                                }

                                if ( QuestHelper.hasQuestItem( player, DELUXE_FAIRY_STONE ) ) {
                                    return this.getPath( '30747-03.html' )
                                }

                                break

                            case 5:
                                if ( QuestHelper.hasQuestItem( player, MONKSHOOD_JUICE ) ) {
                                    return this.getPath( '30747-09.html' )
                                }

                                if ( state.getVariable( variableNames.fairyStone ) === 1 ) {
                                    return this.getPath( '30747-05.html' )
                                }

                                return this.getPath( '30747-06.html' )

                            case 6:
                                if ( QuestHelper.getQuestItemsCount( player, EXARION_EGG ) >= 20
                                        || QuestHelper.getQuestItemsCount( player, ZWOV_EGG ) >= 20
                                        || QuestHelper.getQuestItemsCount( player, KALIBRAN_EGG ) >= 20
                                        || QuestHelper.getQuestItemsCount( player, SUZET_EGG ) >= 20
                                        || QuestHelper.getQuestItemsCount( player, SHAMHAI_EGG ) >= 20 ) {
                                    return this.getPath( '30747-10.html' )
                                }

                                return this.getPath( '30747-09.html' )

                            case 7:
                                return this.getPath( '30747-11.html' )

                            case 8:
                                return this.getPath( '30747-12.html' )
                        }

                        break

                    case EXARION:
                        switch ( state.getCondition() ) {
                            case 5:
                                if ( QuestHelper.hasQuestItem( player, MONKSHOOD_JUICE ) ) {
                                    return this.getPath( '30748-01.html' )
                                }

                                break

                            case 6:
                                if ( QuestHelper.getQuestItemsCount( player, EXARION_EGG ) >= 20 ) {
                                    await QuestHelper.takeMultipleItems( player, -1, EXARION_SCALE, EXARION_EGG )
                                    await QuestHelper.giveSingleItem( player, EXARION_EGG, 1 )

                                    state.setConditionWithSound( 7, true )
                                    return this.getPath( '30748-04.html' )
                                }

                                return this.getPath( '30748-03.html' )

                            case 7:
                                return this.getPath( '30748-05.html' )
                        }

                        break

                    case ZWOV:
                        switch ( state.getCondition() ) {
                            case 5:
                                if ( QuestHelper.hasQuestItem( player, MONKSHOOD_JUICE ) ) {
                                    return this.getPath( '30749-01.html' )
                                }

                                break

                            case 6:
                                if ( QuestHelper.getQuestItemsCount( player, ZWOV_EGG ) >= 20 ) {
                                    await QuestHelper.takeMultipleItems( player, -1, ZWOV_SCALE, ZWOV_EGG )
                                    await QuestHelper.giveSingleItem( player, ZWOV_EGG, 1 )

                                    state.setConditionWithSound( 7, true )
                                    return this.getPath( '30749-04.html' )
                                }

                                return this.getPath( '30749-03.html' )

                            case 7:
                                return this.getPath( '30749-05.html' )
                        }

                        break

                    case KALIBRAN:
                        switch ( state.getCondition() ) {
                            case 5:
                                if ( QuestHelper.hasQuestItem( player, MONKSHOOD_JUICE ) ) {
                                    return this.getPath( '30750-01.html' )
                                }

                                break

                            case 6:
                                if ( QuestHelper.getQuestItemsCount( player, KALIBRAN_EGG ) >= 20 ) {
                                    return this.getPath( '30750-04.html' )
                                }

                                return this.getPath( '30750-03.html' )

                            case 7:
                                return this.getPath( '30750-06.html' )
                        }

                        break

                    case SUZET:
                        switch ( state.getCondition() ) {
                            case 5:
                                if ( QuestHelper.hasQuestItem( player, MONKSHOOD_JUICE ) ) {
                                    return this.getPath( '30751-01.html' )
                                }

                                break

                            case 6:
                                if ( QuestHelper.getQuestItemsCount( player, SUZET_EGG ) >= 20 ) {
                                    await QuestHelper.takeMultipleItems( player, -1, SUZET_SCALE, SUZET_EGG )
                                    await QuestHelper.giveSingleItem( player, SUZET_EGG, 1 )

                                    state.setConditionWithSound( 7, true )
                                    return this.getPath( '30751-05.html' )
                                }

                                return this.getPath( '30751-04.html' )

                            case 7:
                                return this.getPath( '30751-06.html' )
                        }

                        break

                    case SHAMHAI:
                        switch ( state.getCondition() ) {
                            case 5:
                                if ( QuestHelper.hasQuestItem( player, MONKSHOOD_JUICE ) ) {
                                    return this.getPath( '30752-01.html' )
                                }

                                break

                            case 6:
                                if ( QuestHelper.getQuestItemsCount( player, SHAMHAI_EGG ) >= 20 ) {
                                    await QuestHelper.takeMultipleItems( player, -1, SHAMHAI_SCALE, SHAMHAI_EGG )
                                    await QuestHelper.giveSingleItem( player, SHAMHAI_EGG, 1 )

                                    state.setConditionWithSound( 7, true )
                                    return this.getPath( '30752-04.html' )
                                }

                                return this.getPath( '30752-03.html' )

                            case 7:
                                return this.getPath( '30752-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    checkPartyMember( state: QuestState ): boolean {
        return [ 2, 6 ].includes( state.getCondition() )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) )

        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        switch ( state.getCondition() ) {
            case 2:
                if ( data.npcId !== TOAD_LORD || Math.random() > QuestHelper.getAdjustedChance( TOAD_SKIN, 0.3, data.isChampion ) ) {
                    return
                }

                await QuestHelper.rewardUpToLimit( player, TOAD_SKIN, 1, state.getVariable( variableNames.fairyStone ) === 1 ? 10 : 20, data.isChampion )
                return

            case 6:
                let itemId = eggDrops[ data.npcId ]
                if ( data.npcId !== state.getVariable( variableNames.chosenNpcId )
                        || Math.random() > QuestHelper.getAdjustedChance( itemId, 0.5, data.isChampion ) ) {
                    return
                }

                await QuestHelper.rewardUpToLimit( player, itemId, 1, 20, data.isChampion )
                return
        }
    }

    async giveReward( player: L2PcInstance ): Promise<void> {
        let random = _.random( 100 )

        let itemId = _.find( eggItems, ( eggId: number ): boolean => {
            return QuestHelper.hasQuestItem( player, eggId )
        } )

        if ( !itemId ) {
            return
        }

        let modifier = eggItems.indexOf( itemId ) * 5
        if ( QuestHelper.hasQuestItem( player, FAIRY_DUST ) ) {
            if ( random < ( 45 + modifier ) ) {
                await QuestHelper.rewardSingleItem( player, DRAGONFLUTE_OF_WIND, 1 )
            } else if ( random < ( 75 + modifier ) ) {
                await QuestHelper.rewardSingleItem( player, DRAGONFLUTE_OF_STAR, 1 )
            } else {
                await QuestHelper.rewardSingleItem( player, DRAGONFLUTE_OF_TWILIGHT, 1 )
            }
        }

        if ( random < ( 50 + modifier ) ) {
            await QuestHelper.rewardSingleItem( player, DRAGONFLUTE_OF_WIND, 1 )
        } else if ( random < ( 85 + modifier ) ) {
            await QuestHelper.rewardSingleItem( player, DRAGONFLUTE_OF_STAR, 1 )
        } else {
            await QuestHelper.rewardSingleItem( player, DRAGONFLUTE_OF_TWILIGHT, 1 )
        }

        await QuestHelper.takeSingleItem( player, itemId, -1 )
    }
}