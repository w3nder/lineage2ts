import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { NpcSay } from '../../gameService/packets/send/builder/NpcSay'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const CAPTAIN_BEZIQUE = 30379
const NETI = 30425
const BEZIQUES_LETTER = 1180
const NETIS_BOW = 1181
const NETIS_DAGGER = 1182
const SPARTOIS_BONES = 1183
const HORSESHOE_OF_LIGHT = 1184
const MOST_WANTED_LIST = 1185
const STOLEN_JEWELRY = 1186
const STOLEN_TOMES = 1187
const STOLEN_RING = 1188
const STOLEN_NECKLACE = 1189
const STOLEN_ITEMS = [
    STOLEN_JEWELRY,
    STOLEN_TOMES,
    STOLEN_RING,
    STOLEN_NECKLACE,
]

const BEZIQUES_RECOMMENDATION = 1190
const minimumLevel = 18
const requireItemAmount = 10
const CATS_EYE_BANDIT = 27038

const monsterRewardChances: { [ npcId: number ]: number } = {
    20035: 0.2, // Tracker Skeleton
    20042: 0.3, // Tracker Skeleton Leader
    20045: 0.2, // Skeleton Scout
    20051: 0.2, // Skeleton Bowman
    20054: 0.8, // Ruin Spartoi
    20060: 0.8, // Raging Spartoi
}

const monsterIds: Array<number> = [ CATS_EYE_BANDIT, ..._.keys( monsterRewardChances ).map( value => _.parseInt( value ) ) ]

const variableNames = {
    lastAttacker: '403.la',
}

export class PathOfTheRogue extends ListenerLogic {
    constructor() {
        super( 'Q00403_PathOfTheRogue', 'listeners/tracked-400/PathOfTheRogue.ts' )
        this.questId = 403
        this.questItemIds = [
            BEZIQUES_LETTER,
            NETIS_BOW,
            NETIS_DAGGER,
            SPARTOIS_BONES,
            HORSESHOE_OF_LIGHT,
            MOST_WANTED_LIST,
            STOLEN_JEWELRY,
            STOLEN_TOMES,
            STOLEN_RING,
            STOLEN_NECKLACE,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00403_PathOfTheRogue'
    }

    checkWeapon( playerId: number ): boolean {
        let player = L2World.getPlayer( playerId )
        let weapon = player.getActiveWeaponInstance()
        return weapon && [ NETIS_BOW, NETIS_DAGGER ].includes( weapon.getId() )
    }

    getAttackableAttackIds(): Array<number> {
        return monsterIds
    }

    getAttackableKillIds(): Array<number> {
        return monsterIds
    }

    getQuestStartIds(): Array<number> {
        return [ CAPTAIN_BEZIQUE ]
    }

    getTalkIds(): Array<number> {
        return [ CAPTAIN_BEZIQUE, NETI ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        switch ( NpcVariablesManager.get( data.targetId, this.getName() ) ) {
            case 1:
                if ( !this.checkWeapon( data.attackerPlayerId )
                        || NpcVariablesManager.get( data.targetId, variableNames.lastAttacker ) !== data.attackerPlayerId ) {
                    NpcVariablesManager.set( data.targetId, this.getName(), 2 )
                }

                return

            case 2:
                return

            default:
                NpcVariablesManager.set( data.targetId, variableNames.lastAttacker, data.attackerPlayerId )

                if ( !this.checkWeapon( data.attackerPlayerId ) ) {
                    NpcVariablesManager.set( data.targetId, this.getName(), 2 )
                    return
                }

                if ( data.targetNpcId === CATS_EYE_BANDIT ) {
                    PacketDispatcher.sendOwnedData( data.attackerPlayerId,
                            NpcSay.fromNpcString( data.targetId, NpcSayType.NpcAll, data.targetNpcId,
                            NpcStringIds.YOU_CHILDISH_FOOL_DO_YOU_THINK_YOU_CAN_CATCH_ME ).getBuffer() )
                }

                NpcVariablesManager.set( data.targetId, this.getName(), 1 )
                return
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() || NpcVariablesManager.get( data.targetId, this.getName() ) !== 1 ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( data.npcId === CATS_EYE_BANDIT ) {
            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.I_MUST_DO_SOMETHING_ABOUT_THIS_SHAMEFUL_INCIDENT )

            if ( QuestHelper.hasQuestItem( player, MOST_WANTED_LIST ) ) {
                let randomItem = _.sample( STOLEN_ITEMS )
                if ( !QuestHelper.hasQuestItem( player, randomItem ) ) {
                    await QuestHelper.giveSingleItem( player, randomItem, 1 )

                    if ( QuestHelper.hasQuestItems( player, ...STOLEN_ITEMS ) ) {
                        state.setConditionWithSound( 6, true )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }
            }

            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( SPARTOIS_BONES, monsterRewardChances[ data.npcId ], data.isChampion )
                || QuestHelper.getQuestItemsCount( player, SPARTOIS_BONES ) < requireItemAmount ) {
            return
        }

        await QuestHelper.rewardAndProgressState( player, state, SPARTOIS_BONES, 1, requireItemAmount, data.isChampion, 3 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( player.getClassId() === ClassIdValues.fighter.id ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        if ( QuestHelper.hasQuestItem( player, BEZIQUES_RECOMMENDATION ) ) {
                            return this.getPath( '30379-04.htm' )
                        }

                        return this.getPath( '30379-05.htm' )
                    }

                    return this.getPath( '30379-03.htm' )
                }

                if ( player.getClassId() === ClassIdValues.rogue.id ) {
                    return this.getPath( '30379-02a.htm' )
                }

                return this.getPath( '30379-02.htm' )

            case '30379-06.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, BEZIQUES_LETTER, 1 )
                break

            case '30425-02.html':
            case '30425-03.html':
            case '30425-04.html':
                break

            case '30425-05.html':
                if ( QuestHelper.hasQuestItem( player, BEZIQUES_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, BEZIQUES_LETTER, 1 )
                    if ( !QuestHelper.hasQuestItem( player, NETIS_BOW ) ) {
                        await QuestHelper.takeSingleItem( player, NETIS_BOW, 1 )
                    }

                    if ( !QuestHelper.hasQuestItem( player, NETIS_DAGGER ) ) {
                        await QuestHelper.takeSingleItem( player, NETIS_DAGGER, 1 )
                    }

                    state.setConditionWithSound( 2, true )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === CAPTAIN_BEZIQUE ) {
                    return this.getPath( '30379-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case CAPTAIN_BEZIQUE:
                        if ( QuestHelper.hasQuestItems( player, STOLEN_JEWELRY, STOLEN_TOMES, STOLEN_RING, STOLEN_NECKLACE ) ) {
                            await QuestHelper.takeMultipleItems( player, -1,
                                    NETIS_BOW,
                                    NETIS_DAGGER,
                                    MOST_WANTED_LIST,
                                    STOLEN_JEWELRY,
                                    STOLEN_TOMES,
                                    STOLEN_RING,
                                    STOLEN_NECKLACE )

                            await QuestHelper.giveAdena( player, 163800, true )
                            await QuestHelper.giveSingleItem( player, BEZIQUES_RECOMMENDATION, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 320534, 20232 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 456128, 26930 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 591724, 33628 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '30379-09.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, HORSESHOE_OF_LIGHT, BEZIQUES_LETTER ) ) {
                            return this.getPath( '30379-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, HORSESHOE_OF_LIGHT ) ) {
                            await QuestHelper.takeSingleItem( player, HORSESHOE_OF_LIGHT, -1 )
                            await QuestHelper.giveSingleItem( player, MOST_WANTED_LIST, 1 )

                            state.setConditionWithSound( 5, true )
                            return this.getPath( '30379-08.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, NETIS_BOW, NETIS_DAGGER )
                                && !QuestHelper.hasQuestItem( player, MOST_WANTED_LIST ) ) {
                            return this.getPath( '30379-10.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MOST_WANTED_LIST ) ) {
                            return this.getPath( '30379-11.html' )
                        }

                        break

                    case NETI:
                        if ( QuestHelper.hasQuestItem( player, BEZIQUES_LETTER ) ) {
                            return this.getPath( '30425-01.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, HORSESHOE_OF_LIGHT, BEZIQUES_LETTER ) ) {
                            if ( QuestHelper.hasQuestItem( player, MOST_WANTED_LIST ) ) {
                                return this.getPath( '30425-08.html' )
                            }

                            if ( QuestHelper.getQuestItemsCount( player, SPARTOIS_BONES ) < requireItemAmount ) {
                                return this.getPath( '30425-06.html' )
                            }

                            await QuestHelper.takeSingleItem( player, SPARTOIS_BONES, -1 )
                            await QuestHelper.giveSingleItem( player, HORSESHOE_OF_LIGHT, 1 )

                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30425-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, HORSESHOE_OF_LIGHT ) ) {
                            return this.getPath( '30425-08.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}