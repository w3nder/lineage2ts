import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcApproachedForTalkEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestType } from '../../gameService/enums/QuestType'
import { NpcSay } from '../../gameService/packets/send/builder/NpcSay'
import { CreatureSay } from '../../gameService/packets/send/builder/CreatureSay'
import { AIIntent } from '../../gameService/aicontroller/enums/AIIntent'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'
import { SpawnMakerCache } from '../../gameService/cache/SpawnMakerCache'
import { ISpawnLogic } from '../../gameService/models/spawns/ISpawnLogic'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const GUMIEL = 32759
const ESCORT_CHECKER = 32764
const solinaClanNpcIds = [
    22789, // Guide Solina
    22790, // Seeker Solina
    22791, // Savior Solina
    22793, // Ascetic Solina
]

const PACKAGED_BOOK = 15716
const gumielSpawnChance = 0.01
const minimumLevel = 82

const eventNames = {
    check: 'ck',
    timeLimit: 'tl',
    talkTimeOne: 'tto',
    talkTimeTwo: 'ttt',
    talkTimeThree: 'tttr',
    stop: 'st',
    cleanUp: 'cu',
}

export class LostAndFound extends ListenerLogic {
    constructor() {
        super( 'Q00457_LostAndFound', 'listeners/tracked-400/LostAndFound.ts' )
        this.questId = 457
    }

    getQuestStartIds(): Array<number> {
        return [ GUMIEL ]
    }

    getSpawnIds(): Array<number> {
        return [ ESCORT_CHECKER ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ GUMIEL ]
    }

    getTalkIds(): Array<number> {
        return [ GUMIEL ]
    }

    getAttackableKillIds(): Array<number> {
        return solinaClanNpcIds
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00457_LostAndFound'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case '32759-06.html':
                state.startQuest()
                npc.setTarget( player )
                npc.setWalking()

                AIEffectHelper.notifyFollow( npc, data.playerId )

                this.startQuestTimer( eventNames.check, 1000, data.characterId, data.playerId, true )
                this.startQuestTimer( eventNames.timeLimit, 600000, data.characterId, data.playerId )
                this.startQuestTimer( eventNames.talkTimeOne, 120000, data.characterId, data.playerId )
                this.startQuestTimer( eventNames.talkTimeTwo, 30000, data.characterId, data.playerId )

                return

            case eventNames.talkTimeOne:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.All, NpcStringIds.AH_I_THINK_I_REMEMBER_THIS_PLACE )
                return

            case eventNames.talkTimeTwo:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.All, NpcStringIds.WHAT_WERE_YOU_DOING_HERE )
                this.startQuestTimer( eventNames.talkTimeThree, 10 * 1000, data.characterId, data.playerId )
                return

            case eventNames.talkTimeThree:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.All, NpcStringIds.I_GUESS_YOURE_THE_SILENT_TYPE_THEN_ARE_YOU_LOOKING_FOR_TREASURE_LIKE_ME )
                return

            case eventNames.timeLimit:
                this.startQuestTimer( eventNames.stop, 2000, data.characterId, data.playerId )
                await state.exitQuestWithType( QuestType.DAILY, false )
                return

            case eventNames.check:
                let shouldProceed: boolean = _.some( SpawnMakerCache.getAllSpawns( ESCORT_CHECKER ), ( spawn: ISpawnLogic ) => {
                    return spawn.hasSpawned() && npc.isInsideRadius( spawn, 1000 )
                } )

                if ( shouldProceed ) {
                    this.startQuestTimer( eventNames.stop, 1000, data.characterId, data.playerId )
                    this.startQuestTimer( eventNames.cleanUp, 3000, data.characterId, data.playerId )
                    this.stopQuestTimer( eventNames.check, data.characterId, data.playerId )

                    BroadcastHelper.dataInLocation( npc, CreatureSay.fromNpcName( data.characterId, NpcSayType.NpcAll, npc.getName(), NpcStringIds.AH_FRESH_AIR ).getBuffer(), npc.getBroadcastRadius() )
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.All, NpcStringIds.AH_FRESH_AIR )

                    await QuestHelper.giveSingleItem( player, PACKAGED_BOOK, 1 )
                    await state.exitQuestWithType( QuestType.DAILY, true )

                    return
                }

                /*
                    TODO : L2J has these blocks reversed, so it is possible that current implementation is missing certain things
                 */
                let distance = npc.calculateDistance( player, true )
                if ( distance > 1000 ) {
                    if ( distance > 5000 ) {
                        this.startQuestTimer( eventNames.stop, 2000, data.characterId, data.playerId )
                        await state.exitQuestWithType( QuestType.DAILY, false )
                        return
                    }

                    switch ( NpcVariablesManager.get( data.characterId, this.getName() ) ) {
                        case 2:
                            this.startQuestTimer( eventNames.stop, 2000, data.characterId, data.playerId )
                            await state.exitQuestWithType( QuestType.DAILY, false )
                            break

                        case 1:
                            player.sendOwnedData( NpcSay.fromNpcString( data.characterId, NpcSayType.Tell, data.characterNpcId, NpcStringIds.ITS_HARD_TO_FOLLOW ).getBuffer() )
                            NpcVariablesManager.set( data.characterId, this.getName(), 2 )
                            break

                        default:
                            player.sendOwnedData( NpcSay.fromNpcString( data.characterId, NpcSayType.Tell, data.characterNpcId, NpcStringIds.HEY_DONT_GO_SO_FAST ).getBuffer() )
                            NpcVariablesManager.set( data.characterId, this.getName(), 1 )
                            break
                    }
                }

                return

            case eventNames.stop:
                npc.setTarget( null )
                await AIEffectHelper.setNextIntent( npc, AIIntent.WAITING )

                this.stopQuestTimersForIds( data.characterId, data.playerId, [ eventNames.check, eventNames.timeLimit, eventNames.talkTimeOne, eventNames.talkTimeTwo ] )
                return

            case eventNames.cleanUp:
                await npc.deleteMe()
                return
        }

        return this.getPath( data.eventName )
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        if ( npc.getTargetId() > 0 ) {
            return this.getPath( npc.getTargetId() === data.playerId ? '32759-08.html' : '32759-01a.html' )
        }

        return this.getPath( '32759.html' )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( Math.random() > gumielSpawnChance ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        await QuestHelper.addSpawnAtLocation( GUMIEL, L2World.getObjectById( data.targetId ) )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '32759-02.html' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32759-01.htm' : '32759-03.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}