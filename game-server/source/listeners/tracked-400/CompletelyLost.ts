import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import {
    CharacterStoppedMovingEvent,
    EventTerminationResult,
    EventType,
    NpcGeneralEvent,
    NpcReceivingEvent,
    NpcSeePlayerEvent,
    NpcSpawnEvent,
    NpcTalkEvent,
    TerminatedNpcStartsAttackEvent,
} from '../../gameService/models/events/EventType'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { DataManager } from '../../data/manager'
import { QuestType } from '../../gameService/enums/QuestType'
import { AIIntent } from '../../gameService/aicontroller/enums/AIIntent'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { NpcSay } from '../../gameService/packets/send/builder/NpcSay'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { L2Party } from '../../gameService/models/L2Party'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import { L2Object } from '../../gameService/models/L2Object'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const INJURED_SOLDIER = 32738
const ERMIAN = 32736
const minimumLevel = 84
const moveToLocation = new Location( -180219, 186341, -10600 )

const variableNames = {
    state: 'st',
    ermian: 'erm',
    questEscort: 'qs',
    leaderObjectId: 'loi',
    whisper: 'wr',
    partyLeaderId: 'pli',
}

const eventNames = {
    questTimer: 'qt',
    sayTimerOne: 'sto',
    sayTimerTwo: 'stt',
    iAmErmian: 'iae',
    expiredTimer: 'eti',
    checkTimer: 'cti',
    escortFailure: 'eqf',
    escortStart: 'eqs',
    escortSuccess: 'eqss',
    timeLimitOne: 'tlo',
    timeLimitTwo: 'tlt',
    timeLimitThree: 'tltr',
    timeLimitFour: 'tlf',
    timeLimitFive: 'tlfv',
}

/*
    TODO : review event sequences since there appears to be unnecessary looping logic that needs review
    consider using AIController to make follow logic happen without need for manual event hand-holding
 */
export class CompletelyLost extends ListenerLogic {
    constructor() {
        super( 'Q00454_CompletelyLost', 'listeners/tracked-400/CompletelyLost.ts' )
        this.questId = 454
    }

    getQuestStartIds(): Array<number> {
        return [ INJURED_SOLDIER ]
    }

    getTalkIds(): Array<number> {
        return [ INJURED_SOLDIER, ERMIAN ]
    }

    getSpawnIds(): Array<number> {
        return [ ERMIAN ]
    }

    getNpcSeePlayerIds(): Array<number> {
        return [ INJURED_SOLDIER ]
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcReceivingEvent,
                ids: [ INJURED_SOLDIER ],
                method: this.onNpcEventReceived.bind( this ),
            },
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.CharacterStoppedMoving,
                ids: [ INJURED_SOLDIER ],
                method: this.onNpcStoppedMoving.bind( this ),
            },
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.TerminatedNpcStartsAttack,
                ids: [ INJURED_SOLDIER ],
                method: this.onCreatureAttacked.bind( this ),
            },
        ]
    }

    async onNpcSeePlayerEvent( data: NpcSeePlayerEvent ): Promise<void> {
        if ( !NpcVariablesManager.get( data.npcObjectId, variableNames.state ) ) {
            QuestHelper.addAttackDesire( L2World.getObjectById( data.npcObjectId ) as L2Npc, L2World.getPlayer( data.playerId ), 10 )
        }
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        // TODO: convert to fire event on AIController being turned on
        // problem is that questTimer event goes into very short loop that gets triggered in 100 ms intervals in
        //  this.startQuestTimer( eventNames.questTimer, 1000, data.characterId, 0 )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00454_CompletelyLost'
    }

    async onNpcStoppedMoving( data: CharacterStoppedMovingEvent ): Promise<void> {
        let objectId: number = NpcVariablesManager.get( data.characterId, variableNames.ermian ) as number
        let npc = L2World.getObjectById( objectId ) as L2Npc
        if ( !npc ) {
            return
        }

        npc.setHeading( GeneralHelper.calculateHeadingFromLocations( L2World.getObjectById( data.characterId ), npc ) )
        this.startQuestTimer( eventNames.sayTimerTwo, 2000, data.characterId, 0 )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '32738-02.htm' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '32738-03.htm' )
                }

                let value: number = NpcVariablesManager.get( data.characterId, variableNames.questEscort ) as number || 0
                if ( value === 0 ) {
                    return this.getPath( '32738-01.htm' )
                }

                if ( value === 99 ) {
                    return this.getPath( '32738-01c.htm' )
                }

                let playerId: number = NpcVariablesManager.get( data.characterId, variableNames.leaderObjectId ) as number
                let leader: L2PcInstance = L2World.getPlayer( playerId )

                if ( !leader ) {
                    NpcVariablesManager.remove( data.characterId, variableNames.leaderObjectId )
                    break
                }

                let party = PlayerGroupCache.getParty( playerId )
                if ( party && party.getMembers().includes( data.playerId ) ) {
                    return DataManager.getHtmlData().getItem( this.getPath( '32738-01a.htm' ) )
                            .replace( /%leader%/g, leader.getName() )
                            .replace( '%name%', player.getName() )
                }

                return DataManager.getHtmlData().getItem( this.getPath( '32738-01b.htm' ) )
                        .replace( '%leader%', leader.getName() )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case INJURED_SOLDIER:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32738-05.html' )
                        }

                        if ( state.isMemoState( 2 ) ) {
                            return this.getPath( '32738-08.html' )
                        }

                        break

                    case ERMIAN:
                        switch ( state.getMemoState() ) {
                            case 1:
                            case 2:
                                return this.getPath( '32736-01.html' )

                            case 3:
                                await state.exitQuestWithType( QuestType.DAILY, true )
                                return this.getPath( '32736-02.html' )

                            case 4:
                                let [ itemId, amount ] = this.getPlayerRewards()
                                await QuestHelper.rewardSingleItem( player, itemId, amount )

                                await state.exitQuestWithType( QuestType.DAILY, true )
                                return this.getPath( '32736-03.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getPlayerRewards(): [ number, number ] { // itemId, amount
        let group = _.random( 2 )
        let chance = _.random( 100 )

        switch ( group ) {
            case 0:
                if ( Math.random() < 0.5 ) {
                    if ( chance < 11 ) {
                        return [ 15792, 1 ] // Recipe - Sealed Vesper Helmet (60%)
                    }

                    if ( chance < 22 ) {
                        return [ 15798, 1 ] // Recipe - Sealed Vesper Gaiter (60%)
                    }

                    if ( chance < 33 ) {
                        return [ 15795, 1 ] // Recipe - Sealed Vesper Breastplate (60%)
                    }

                    if ( chance < 44 ) {
                        return [ 15801, 1 ] // Recipe - Sealed Vesper Gauntlet (60%)
                    }

                    if ( chance < 55 ) {
                        return [ 15808, 1 ] // Recipe - Sealed Vesper Shield (60%)
                    }

                    if ( chance < 66 ) {
                        return [ 15804, 1 ] // Recipe - Sealed Vesper Boots (60%)
                    }

                    if ( chance < 77 ) {
                        return [ 15809, 1 ] // Recipe - Sealed Vesper Ring (70%)
                    }

                    if ( chance < 88 ) {
                        return [ 15810, 1 ] // Recipe - Sealed Vesper Earring (70%)
                    }

                    return [ 15811, 1 ] // Recipe - Sealed Vesper Necklace (70%)
                }

                if ( chance < 11 ) {
                    return [ 15660, 3 ] // Sealed Vesper Helmet Piece
                }

                if ( chance < 22 ) {
                    return [ 15666, 3 ] // Sealed Vesper Gaiter Piece
                }

                if ( chance < 33 ) {
                    return [ 15663, 3 ] // Sealed Vesper Breastplate Piece
                }

                if ( chance < 44 ) {
                    return [ 15667, 3 ] // Sealed Vesper Gauntlet Piece
                }

                if ( chance < 55 ) {
                    return [ 15669, 3 ] // Sealed Vesper Verteidiger Piece
                }

                if ( chance < 66 ) {
                    return [ 15668, 3 ] // Sealed Vesper Boots Piece
                }

                if ( chance < 77 ) {
                    return [ 15769, 3 ] // Sealed Vesper Ring Gem
                }

                if ( chance < 88 ) {
                    return [ 15770, 3 ] // Sealed Vesper Earring Gem
                }

                return [ 15771, 3 ] // Sealed Vesper Necklace Gem

            case 1:
                if ( Math.random() < 0.5 ) {
                    if ( chance < 12 ) {
                        return [ 15805, 1 ] // Recipe - Sealed Vesper Leather Boots (60%)
                    }

                    if ( chance < 24 ) {
                        return [ 15796, 1 ] // Recipe - Sealed Vesper Leather Breastplate (60%)
                    }

                    if ( chance < 36 ) {
                        return [ 15793, 1 ] // Recipe - Sealed Vesper Leather Helmet (60%)
                    }

                    if ( chance < 48 ) {
                        return [ 15799, 1 ] // Recipe - Sealed Vesper Leather Legging (60%)
                    }

                    if ( chance < 60 ) {
                        return [ 15802, 1 ] // Recipe - Sealed Vesper Leather Gloves (60%)
                    }

                    if ( chance < 72 ) {
                        return [ 15809, 1 ] // Recipe - Sealed Vesper Ring (70%)
                    }

                    if ( chance < 84 ) {
                        return [ 15810, 1 ] // Recipe - Sealed Vesper Earring (70%)
                    }

                    return [ 15811, 1 ] // Recipe - Sealed Vesper Necklace (70%)
                }

                if ( chance < 12 ) {
                    return [ 15672, 3 ] // Sealed Vesper Leather Boots Piece
                }

                if ( chance < 24 ) {
                    return [ 15664, 3 ] // Sealed Vesper Leather Breastplate Piece
                }

                if ( chance < 36 ) {
                    return [ 15661, 3 ] // Sealed Vesper Leather Helmet Piece
                }

                if ( chance < 48 ) {
                    return [ 15670, 3 ] // Sealed Vesper Leather Legging Piece
                }

                if ( chance < 60 ) {
                    return [ 15671, 3 ] // Sealed Vesper Leather Gloves Piece
                }

                if ( chance < 72 ) {
                    return [ 15769, 3 ] // Sealed Vesper Ring Gem
                }

                if ( chance < 84 ) {
                    return [ 15770, 3 ] // Sealed Vesper Earring Gem
                }

                return [ 15771, 3 ] // Sealed Vesper Necklace Gem

            default:
                if ( Math.random() < 0.5 ) {
                    if ( chance < 11 ) {
                        return [ 15800, 1 ] // Recipe - Sealed Vesper Stockings (60%)
                    }

                    if ( chance < 22 ) {
                        return [ 15803, 1 ] // Recipe - Sealed Vesper Gloves (60%)
                    }

                    if ( chance < 33 ) {
                        return [ 15806, 1 ] // Recipe - Sealed Vesper Shoes (60%)
                    }

                    if ( chance < 44 ) {
                        return [ 15807, 1 ] // Recipe - Sealed Vesper Sigil (60%)
                    }

                    if ( chance < 55 ) {
                        return [ 15797, 1 ] // Recipe - Sealed Vesper Tunic (60%)
                    }

                    if ( chance < 66 ) {
                        return [ 15794, 1 ] // Recipe - Sealed Vesper Circlet (60%)
                    }

                    if ( chance < 77 ) {
                        return [ 15809, 1 ] // Recipe - Sealed Vesper Ring (70%)
                    }

                    if ( chance < 88 ) {
                        return [ 15810, 1 ] // Recipe - Sealed Vesper Earring (70%)
                    }

                    return [ 15811, 1 ] // Recipe - Sealed Vesper Necklace (70%)
                }

                if ( chance < 11 ) {
                    return [ 15673, 3 ] // Sealed Vesper Stockings Piece
                }

                if ( chance < 22 ) {
                    return [ 15674, 3 ] // Sealed Vesper Gloves Piece
                }

                if ( chance < 33 ) {
                    return [ 15675, 3 ] // Sealed Vesper Shoes Piece
                }

                if ( chance < 44 ) {
                    return [ 15691, 3 ] // Sealed Vesper Sigil Piece
                }

                if ( chance < 55 ) {
                    return [ 15665, 3 ] // Sealed Vesper Tunic Piece
                }

                if ( chance < 66 ) {
                    return [ 15662, 3 ] // Sealed Vesper Circlet Piece
                }

                if ( chance < 77 ) {
                    return [ 15769, 3 ] // Sealed Vesper Ring Gem
                }

                if ( chance < 88 ) {
                    return [ 15770, 3 ] // Sealed Vesper Earring Gem
                }

                return [ 15771, 3 ] // Sealed Vesper Necklace Gem
        }
    }

    async onCreatureAttacked( data: TerminatedNpcStartsAttackEvent ): Promise<EventTerminationResult> {
        NpcVariablesManager.set( data.targetId, variableNames.state, 1 )
        this.startQuestTimer( eventNames.sayTimerOne, 2000, data.targetId, 0 )

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        await AIEffectHelper.setNextIntent( npc, AIIntent.WAITING )

        return {
            terminate: true,
            priority: 0,
            message: null,
        }
    }

    getLeader( objectId: number ): L2PcInstance {
        return L2World.getPlayer( this.getLeaderId( objectId ) )
    }

    getLeaderId( objectId: number ): number {
        return NpcVariablesManager.get( objectId, variableNames.leaderObjectId ) as number
    }


    processTimeLimit( data: NpcGeneralEvent, nextEvent: string, stringId: number ): void {
        let player: L2PcInstance = this.getLeader( data.characterId )
        if ( player ) {
            this.startQuestTimer( nextEvent, 150000, data.characterId, 0 )
            player.sendOwnedData( NpcSay.fromNpcString( data.characterId, NpcSayType.Tell, data.characterNpcId, stringId ).getBuffer() )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        switch ( data.eventName ) {
            case eventNames.questTimer:
                QuestHelper.broadcastEvent( npc, eventNames.iAmErmian, 300, null, this.isCorrectNpc )
                // TODO : review if this still needs to be triggered this.startQuestTimer( eventNames.questTimer, 1000, data.characterId, 0, false )

                return

            case eventNames.sayTimerOne:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.GASP )
                return

            case eventNames.sayTimerTwo:
                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.SOB_TO_SEE_ERMIAN_AGAIN_CAN_I_GO_TO_MY_FAMILY_NOW )
                this.startQuestTimer( eventNames.expiredTimer, 2000, data.characterId, 0, false )
                return

            case eventNames.checkTimer:
                this.startQuestTimer( eventNames.checkTimer, 2000, data.characterNpcId, 0, false )

                let checkTimerLeader: L2PcInstance = this.getLeader( data.characterId )
                if ( checkTimerLeader ) {
                    let distance = npc.calculateDistance( checkTimerLeader )
                    if ( distance > 1000 ) {
                        if ( ( distance > 5000 && distance < 6900 )
                                || ( distance > 31000 && distance < 32000 ) ) {
                            await npc.teleportToLocation( checkTimerLeader, true )
                            return
                        }

                        switch ( NpcVariablesManager.get( data.characterId, variableNames.whisper ) ) {
                            case 1:
                                NpcVariablesManager.set( data.characterId, variableNames.whisper, 2 )
                                checkTimerLeader.sendOwnedData( NpcSay.fromNpcString( data.characterId, NpcSayType.Tell, data.characterNpcId, NpcStringIds.WHERE_ARE_YOU_REALLY_I_CANT_FOLLOW_YOU_LIKE_THIS ).getBuffer() )
                                return

                            case 2:
                                checkTimerLeader.sendOwnedData( NpcSay.fromNpcString( data.characterId, NpcSayType.Tell, data.characterNpcId, NpcStringIds.IM_SORRY_THIS_IS_IT_FOR_ME ).getBuffer() )
                                await QuestHelper.sendEvent( data.characterId, eventNames.escortFailure, npc, null )
                                break

                            default:
                                NpcVariablesManager.set( data.characterId, variableNames.whisper, 1 )
                                checkTimerLeader.sendOwnedData( NpcSay.fromNpcString( data.characterId, NpcSayType.Tell, data.characterNpcId, NpcStringIds.WHERE_ARE_YOU_I_CANT_SEE_ANYTHING ).getBuffer() )
                                return
                        }
                    }
                }

                return

            case eventNames.timeLimitOne:
                this.processTimeLimit( data, eventNames.timeLimitTwo, NpcStringIds.IS_IT_STILL_LONG_OFF )
                return

            case eventNames.timeLimitTwo:
                this.processTimeLimit( data, eventNames.timeLimitThree, NpcStringIds.IS_ERMIAN_WELL_EVEN_I_CANT_BELIEVE_THAT_I_SURVIVED_IN_A_PLACE_LIKE_THIS )
                return

            case eventNames.timeLimitThree:
                this.processTimeLimit( data, eventNames.timeLimitFour, NpcStringIds.I_DONT_KNOW_HOW_LONG_ITS_BEEN_SINCE_I_PARTED_COMPANY_WITH_YOU_TIME_DOESNT_SEEM_TO_MOVE_IT_JUST_FEELS_TOO_LONG )
                return

            case eventNames.timeLimitFour:
                this.processTimeLimit( data, eventNames.timeLimitFive, NpcStringIds.SORRY_TO_SAY_THIS_BUT_THE_PLACE_YOU_STRUCK_ME_BEFORE_NOW_HURTS_GREATLY )
                return

            case eventNames.timeLimitFive:
                let playerId: number = this.getLeaderId( data.characterId )
                PacketDispatcher.sendOwnedData( playerId, NpcSay.fromNpcString( data.characterId, NpcSayType.Tell, data.characterNpcId, NpcStringIds.UGH_IM_SORRY_IT_LOOKS_LIKE_THIS_IS_IT_FOR_ME_I_WANTED_TO_LIVE_AND_SEE_MY_FAMILY ).getBuffer() )

                this.startQuestTimer( eventNames.expiredTimer, 2000, data.characterId, 0 )
                await this.onEscortFailure( data.characterId )

                return

            case eventNames.expiredTimer:
                await npc.deleteMe()
                return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32738-04.htm':
                if ( state.isCreated()
                        && state.isNowAvailable()
                        && player.getLevel() >= minimumLevel ) {
                    if ( !NpcVariablesManager.get( data.characterId, variableNames.questEscort ) ) {
                        NpcVariablesManager.set( data.characterId, variableNames.leaderObjectId, data.playerId )
                        NpcVariablesManager.set( data.characterId, variableNames.questEscort, 1 )

                        if ( player.isInParty() ) {
                            NpcVariablesManager.set( data.characterId, variableNames.partyLeaderId, player.getParty().getLeaderObjectId() )
                        }

                        state.setMemoState( 1 )
                        state.startQuest()

                        break
                    }

                    let leader = this.getLeader( data.characterId )
                    if ( leader.isInParty() && leader.getParty().getMembers().includes( data.playerId ) ) {
                        state.startQuest()
                        state.setMemoState( 1 )

                        return DataManager.getHtmlData().getItem( this.getPath( '32738-04a.htm' ) )
                                .replace( '%leader%', leader.getName() )
                    }

                    return DataManager.getHtmlData().getItem( this.getPath( '32738-01b.htm' ) )
                            .replace( '%leader%', leader.getName() )
                }
                return

            case 'agree1':
                if ( state.isMemoState( 1 ) ) {
                    let playerId: number = this.getLeaderId( data.characterId )
                    if ( playerId > 0 ) {
                        if ( PlayerGroupCache.getParty( playerId ) ) {
                            state.setMemoState( 2 )

                            await this.onEscortStart( data.characterId )
                            return this.getPath( '32738-06.html' )
                        }

                        return this.getPath( '32738-05a.html' )
                    }
                }

                return

            case 'agree2':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    await this.onEscortStart( data.characterId )

                    let party: L2Party = PlayerGroupCache.getParty( this.getLeaderId( data.characterId ) )
                    if ( party && NpcVariablesManager.get( data.characterId, variableNames.partyLeaderId ) === party.getLeaderObjectId() ) {
                        let questName = this.getName()

                        party.getMembers().forEach( ( playerId: number ) => {
                            let playerState: QuestState = QuestStateCache.getQuestState( playerId, questName, false )
                            if ( playerState && playerState.isMemoState( 1 ) ) {
                                playerState.setMemoState( 2 )
                            }
                        } )
                    }

                    return this.getPath( '32738-06.html' )
                }

                return

            case '32738-07.html':
                if ( state.isMemoState( 1 ) ) {
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onNpcEventReceived( data: NpcReceivingEvent ): Promise<void> {
        switch ( data.eventName ) {
            case eventNames.iAmErmian:
                if ( NpcVariablesManager.get( data.receiverId, variableNames.state ) === 2 ) {
                    NpcVariablesManager.set( data.receiverId, variableNames.state, 3 )
                    NpcVariablesManager.set( data.receiverId, variableNames.ermian, data.senderId )

                    let npc = L2World.getObjectById( data.receiverId ) as L2Npc
                    AIEffectHelper.notifyMove( npc, moveToLocation )

                    this.onEscortSuccess( data.receiverId )
                }

                return

            case eventNames.escortStart:
                return this.onEscortStart( data.receiverId )

            case eventNames.escortSuccess:
                this.onEscortSuccess( data.receiverId )
                return

            case eventNames.escortFailure:
                await this.onEscortFailure( data.receiverId )
                return
        }
    }

    onEscortSuccess( receiverId: number ) : void {
        this.processEscortEvent( receiverId, 4 )
    }

    onEscortFailure( receiverId: number ) : Promise<any> {
        this.processEscortEvent( receiverId, 3 )
        return ( L2World.getObjectById( receiverId ) as L2Npc ).deleteMe()
    }

    onEscortStart( receiverId: number ) : void {
        this.startQuestTimer( eventNames.checkTimer, 1000, receiverId, 0 )
        this.startQuestTimer( eventNames.timeLimitOne, 60000, receiverId, 0 )

        NpcVariablesManager.set( receiverId, variableNames.state, 2 )
        NpcVariablesManager.set( receiverId, variableNames.questEscort, 99 )

        AIEffectHelper.notifyFollow( L2World.getObjectById( receiverId ) as L2Npc, this.getLeaderId( receiverId ) )
    }

    processEscortEvent( receiverId: number, nextMemoState: number ): void {
        let playerId: number = this.getLeaderId( receiverId )
        let party: L2Party = PlayerGroupCache.getParty( playerId )
        let playerIds : Array<number> = party ? party.getMembers() : [ playerId ]
        let questName = this.getName()

        playerIds.forEach( ( objectId: number ) => {
            let state: QuestState = QuestStateCache.getQuestState( objectId, questName, false )

            if ( !state || state.isMemoState( 2 ) ) {
                return
            }

            state.setMemoState( nextMemoState )
        } )

        this.stopQuestTimer( eventNames.checkTimer, receiverId, 0 )
        this.stopQuestTimer( eventNames.timeLimitOne, receiverId, 0 )
        this.stopQuestTimer( eventNames.timeLimitTwo, receiverId, 0 )
        this.stopQuestTimer( eventNames.timeLimitThree, receiverId, 0 )
        this.stopQuestTimer( eventNames.timeLimitFour, receiverId, 0 )
        this.stopQuestTimer( eventNames.timeLimitFive, receiverId, 0 )
    }

    isCorrectNpc( object : L2Object ) : boolean {
        return INJURED_SOLDIER === object.getId()
    }
}