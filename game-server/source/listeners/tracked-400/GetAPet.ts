import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2World } from '../../gameService/L2World'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { Race } from '../../gameService/enums/Race'
import { QuestHelper } from '../helpers/QuestHelper'
import { DataManager } from '../../data/manager'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

import _ from 'lodash'

const GUARD_METTY = 30072
const ACCESSORY_MERCHANT_ELICE = 30091
const GATEKEEPER_BELLA = 30256
const PET_MENAGER_MARTIN = 30731
const ANIMAL_LOVERS_LIST = 3417
const ANIMAL_SLAYERS_1ST_LIST = 3418
const ANIMAL_SLAYERS_2ND_LIST = 3419
const ANIMAL_SLAYERS_3RD_LIST = 3420
const ANIMAL_SLAYERS_4TH_LIST = 3421
const ANIMAL_SLAYERS_5TH_LIST = 3422
const BLOODY_FANG = 3423
const BLOODY_CLAW = 3424
const BLOODY_NAIL = 3425
const BLOODY_KASHA_FANG = 3426
const BLOODY_TARANTULA_NAIL = 3427
const ANIMAL_SLAYERS_LIST = 10164
const BLOODY_RED_CLAW = 10165
const WOLF_COLLAR = 2375
const LESSER_DARK_HORROR = 20025
const PROWLER = 20034
const GIANT_SPIDER = 20103
const DARK_HORROR = 20105
const TALON_SPIDER = 20106
const BLADE_SPIDER = 20108
const HOOK_SPIDER = 20308
const HUNTER_TARANTULA = 20403
const CRIMSON_SPIDER = 20460
const PINCER_SPIDER = 20466
const KASHA_SPIDER = 20474
const KASHA_FANG_SPIDER = 20476
const KASHA_BLADE_SPIDER = 20478
const PLUNDER_TARANTULA = 20508
const CRIMSON_SPIDER2 = 22244
const minimumLevel = 15

const linkData = {
    1110001: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Can be used for item transportation.</a><br>',
    1110002: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Can help during hunting by assisting in attacks.</a><br>',
    1110003: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">Can be sent to the village to buy items.</a><br>',
    1110004: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Can be traded or sold to a new owner for adena.</a><br>',
    1110005: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
    1110006: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">When taking down a monster, always have a pet\'s company.</a><br>',
    1110007: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Tell your pet to pick up items.</a><br>',
    1110008: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Tell your pet to attack monsters first.</a><br>',
    1110009: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Let your pet do what it wants.</a><br>',
    1110010: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
    1110011: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">10 hours</a><br>',
    1110012: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">15 hours</a><br>',
    1110013: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">It won\'t disappear.</a><br>',
    1110014: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">25 hours</a><br>',
    1110015: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
    1110016: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">Dire Wolf</a><br>',
    1110017: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Air Wolf</a><br>',
    1110018: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Turek Wolf</a><br>',
    1110019: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Kasha Wolf</a><br>',
    1110020: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
    1110021: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">It\'s tail is always pointing straight down.</a><br>',
    1110022: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">It\'s tail is always curled up.</a><br>',
    1110023: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">It\'s tail is always wagging back and forth.</a><br>',
    1110024: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">What are you talking about?! A wolf doesn\'t have a tail.</a><br>',
    1110025: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
    1110026: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Raccoon</a><br>',
    1110027: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Jackal</a><br>',
    1110028: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Fox</a><br>',
    1110029: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Shepherd Dog</a><br>',
    1110030: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">None of the above.</a><br>',
    1110031: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">1.4 km</a><br>',
    1110032: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">2.4 km</a><br>',
    1110033: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">3.4 km</a><br>',
    1110034: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">4.4 km</a><br>',
    1110035: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
    1110036: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">Male</a><br>',
    1110037: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Female</a><br>',
    1110038: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">A baby that was born last year</a><br>',
    1110039: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">A baby that was born two years ago</a><br>',
    1110040: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
    1110041: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Goat</a><br>',
    1110042: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Meat of a dead animal</a><br>',
    1110043: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Berries</a><br>',
    1110044: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Wild Bird</a><br>',
    1110045: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">None of the above.</a><br>',
    1110046: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Breeding season is January-February.</a><br>',
    1110047: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">Pregnancy is nine months.</a><br>',
    1110048: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Babies are born in April-June.</a><br>',
    1110049: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Has up to ten offspring at one time.</a><br>',
    1110050: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
    1110051: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">3-6 years</a><br>',
    1110052: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">6-9 years</a><br>',
    1110053: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">9-12 years</a><br>',
    1110054: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">12-15 years</a><br>',
    1110055: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
    1110056: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Wolves gather and move in groups of 7-13 animals.</a><br>',
    1110057: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Wolves can eat a whole calf in one sitting.</a><br>',
    1110058: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">If they have water, wolves can live for 5-6 days without eating anything.</a><br>',
    1110059: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">A pregnant wolf makes its home in a wide open place to have its babies.</a><br>',
    1110060: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
    1110061: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">A grown wolf is still not as heavy as a fully-grown male adult human.</a><br>',
    1110062: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">A wolf changes into a werewolf during a full-moon.</a><br>',
    1110063: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">The color of a wolf\'s fur is the same as the place where it lives.</a><br>',
    1110064: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">A wolf enjoys eating Dwarves.</a><br>',
    1110065: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
    1110066: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Talking Island - Wolf</a><br>',
    1110067: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Dark Forest - Ashen Wolf</a><br>',
    1110068: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">Elven Forest - Gray Wolf</a><br>',
    1110069: '<a action="bypass -h Quest Q00419_GetAPet QUESTIONS">Orc - Black Wolf</a><br>',
    1110070: '<a action="bypass -h Quest Q00419_GetAPet 30731-14.html">None of the above.</a><br>',
}

type StartingData = [ number, string, number ] // given itemId, path, required itemId
const startingItems: { [ playerRace: number ]: StartingData } = {
    [ Race.HUMAN ]: [ ANIMAL_SLAYERS_1ST_LIST, '30731-04.htm', BLOODY_FANG ],
    [ Race.ELF ]: [ ANIMAL_SLAYERS_2ND_LIST, '30731-05.htm', BLOODY_CLAW ],
    [ Race.DARK_ELF ]: [ ANIMAL_SLAYERS_3RD_LIST, '30731-06.htm', BLOODY_NAIL ],
    [ Race.ORC ]: [ ANIMAL_SLAYERS_4TH_LIST, '30731-07.htm', BLOODY_KASHA_FANG ],
    [ Race.DWARF ]: [ ANIMAL_SLAYERS_5TH_LIST, '30731-08.htm', BLOODY_TARANTULA_NAIL ],
    [ Race.KAMAEL ]: [ ANIMAL_SLAYERS_LIST, '30731-08a.htm', BLOODY_RED_CLAW ],
}

type MonsterReward = [ number, number, number ] // chance, requiredItemId, countedItemId
const monsterRewards: { [ npcId: number ]: MonsterReward } = {
    [ LESSER_DARK_HORROR ]: [ 0.6, ANIMAL_SLAYERS_3RD_LIST, BLOODY_NAIL ],
    [ PROWLER ]: [ 1, ANIMAL_SLAYERS_3RD_LIST, BLOODY_NAIL ],
    [ GIANT_SPIDER ]: [ 0.6, ANIMAL_SLAYERS_1ST_LIST, BLOODY_FANG ],
    [ DARK_HORROR ]: [ 0.75, ANIMAL_SLAYERS_3RD_LIST, BLOODY_NAIL ],
    [ TALON_SPIDER ]: [ 0.75, ANIMAL_SLAYERS_1ST_LIST, BLOODY_FANG ],
    [ BLADE_SPIDER ]: [ 1, ANIMAL_SLAYERS_1ST_LIST, BLOODY_FANG ],
    [ HOOK_SPIDER ]: [ 1, ANIMAL_SLAYERS_2ND_LIST, BLOODY_CLAW ],
    [ HUNTER_TARANTULA ]: [ 0.75, ANIMAL_SLAYERS_5TH_LIST, BLOODY_TARANTULA_NAIL ],
    [ CRIMSON_SPIDER ]: [ 0.5, ANIMAL_SLAYERS_2ND_LIST, BLOODY_CLAW ],
    [ PINCER_SPIDER ]: [ 1, ANIMAL_SLAYERS_2ND_LIST, BLOODY_CLAW ],
    [ KASHA_SPIDER ]: [ 0.6, ANIMAL_SLAYERS_4TH_LIST, BLOODY_KASHA_FANG ],
    [ KASHA_FANG_SPIDER ]: [ 0.5, ANIMAL_SLAYERS_4TH_LIST, BLOODY_KASHA_FANG ],
    [ KASHA_BLADE_SPIDER ]: [ 1, ANIMAL_SLAYERS_4TH_LIST, BLOODY_KASHA_FANG ],
    [ PLUNDER_TARANTULA ]: [ 1, ANIMAL_SLAYERS_5TH_LIST, BLOODY_TARANTULA_NAIL ],
    [ CRIMSON_SPIDER2 ]: [ 0.75, ANIMAL_SLAYERS_LIST, BLOODY_RED_CLAW ],
}

export class GetAPet extends ListenerLogic {
    constructor() {
        super( 'Q00419_GetAPet', 'listeners/tracked-400/GetAPet.ts' )
        this.questId = 419
        this.questItemIds = [
            ANIMAL_LOVERS_LIST,
            ANIMAL_SLAYERS_1ST_LIST,
            ANIMAL_SLAYERS_2ND_LIST,
            ANIMAL_SLAYERS_3RD_LIST,
            ANIMAL_SLAYERS_4TH_LIST,
            ANIMAL_SLAYERS_5TH_LIST,
            BLOODY_FANG,
            BLOODY_CLAW,
            BLOODY_NAIL,
            BLOODY_KASHA_FANG,
            BLOODY_TARANTULA_NAIL,
            ANIMAL_SLAYERS_LIST,
            BLOODY_RED_CLAW,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ PET_MENAGER_MARTIN ]
    }

    getTalkIds(): Array<number> {
        return [
            PET_MENAGER_MARTIN,
            GUARD_METTY,
            ACCESSORY_MERCHANT_ELICE,
            GATEKEEPER_BELLA,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            LESSER_DARK_HORROR,
            PROWLER,
            GIANT_SPIDER,
            DARK_HORROR,
            TALON_SPIDER,
            BLADE_SPIDER,
            HOOK_SPIDER,
            HUNTER_TARANTULA,
            CRIMSON_SPIDER,
            PINCER_SPIDER,
            KASHA_SPIDER,
            KASHA_FANG_SPIDER,
            KASHA_BLADE_SPIDER,
            PLUNDER_TARANTULA,
            CRIMSON_SPIDER2,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00419_GetAPet'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()

                    if ( !startingItems[ player.getRace() ] ) {
                        return
                    }

                    let [ itemId, path ]: StartingData = startingItems[ player.getRace() ]
                    await QuestHelper.giveSingleItem( player, itemId, 1 )
                    return this.getPath( path )
                }

                return

            case '30731-03.htm':
            case '30072-02.html':
            case '30091-02.html':
            case '30256-02.html':
            case '30256-03.html':
                break

            case '30731-12.html':
                if ( !startingItems[ player.getRace() ] ) {
                    return
                }

                let [ givenItemId, path, requiredItemId ]: StartingData = startingItems[ player.getRace() ]

                if ( QuestHelper.hasQuestItem( player, givenItemId ) && QuestHelper.getQuestItemsCount( player, requiredItemId ) >= 50 ) {
                    await QuestHelper.takeMultipleItems( player, -1, givenItemId, requiredItemId )
                    await QuestHelper.giveSingleItem( player, ANIMAL_LOVERS_LIST, 1 )
                }

                state.setMemoState( 0 )
                break


            case 'QUESTIONS':
                if ( ( state.getMemoState() & 15 ) === 10 && QuestHelper.hasQuestItem( player, ANIMAL_LOVERS_LIST ) ) {
                    await QuestHelper.takeSingleItem( player, ANIMAL_LOVERS_LIST, -1 )
                    await QuestHelper.giveSingleItem( player, WOLF_COLLAR, 1 )
                    await state.exitQuest( true, true )

                    return this.getPath( '30731-15.html' )
                }

                return this.composeQuestionsHtml( state )

            case '30731-14.html':
                state.setMemoState( 0 )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    composeQuestionsHtml( state: QuestState ): string {
        let fileName: string
        let linkId: number = 0

        while ( !fileName ) {
            let randomLinkOffset = _.random( 13 )
            let value: number = Math.pow( 2, randomLinkOffset + 4 )

            if ( ( value & state.getMemoState() ) === 0 ) {
                state.setMemoState( ( state.getMemoState() + 1 ) | value )
                linkId = 1110000 + ( 5 * randomLinkOffset )
                fileName = `30731-${ 20 + randomLinkOffset }.htm`
            }
        }

        let offsets: Array<number> = []
        let rollingValue = 0
        while ( offsets.length < 4 ) {
            let randomReplyOffset = _.random( 3 ) + 1
            let value = Math.pow( 2, randomReplyOffset )

            if ( ( value & rollingValue ) === 0 ) {
                offsets.push( randomReplyOffset )
                rollingValue = rollingValue | value
            }
        }

        return DataManager.getHtmlData().getItem( this.getPath( fileName ) )
                .replace( '<?reply1?>', linkData[ linkId + offsets[ 0 ] ] )
                .replace( '<?reply2?>', linkData[ linkId + offsets[ 1 ] ] )
                .replace( '<?reply3?>', linkData[ linkId + offsets[ 2 ] ] )
                .replace( '<?reply4?>', linkData[ linkId + offsets[ 3 ] ] )
                .replace( '<?reply5?>', linkData[ linkId + 5 ] )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ chance, requiredItemId, countedItemId ]: MonsterReward = monsterRewards[ data.npcId ]

        if ( chance !== 1 && Math.random() > QuestHelper.getAdjustedChance( countedItemId, chance, data.isChampion ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        if ( !QuestHelper.hasQuestItem( player, requiredItemId ) || QuestHelper.getQuestItemsCount( player, countedItemId ) >= 50 ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, countedItemId, 1, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, countedItemId ) >= 50 ) {
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== PET_MENAGER_MARTIN ) {
                    break
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30731-01.htm' )
                }

                return this.getPath( '30731-02.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case PET_MENAGER_MARTIN:
                        if ( !startingItems[ player.getRace() ] ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, ANIMAL_LOVERS_LIST ) ) {
                            if ( state.getMemoState() !== 14 && state.getMemoState() !== 1879048192 ) {
                                return this.getPath( '30731-16.html' )
                            }

                            state.setMemoState( 1879048192 )
                            return this.getPath( '30731-13.html' )
                        }

                        let [ requiredItemId, path, countedItemId ]: StartingData = startingItems[ player.getRace() ]

                        if ( !QuestHelper.hasQuestItem( player, requiredItemId ) ) {
                            break
                        }

                        return this.processItemCount( player, countedItemId, '30731-09.html', '30731-10.html', '30731-11.html' )

                    case GUARD_METTY:
                        if ( QuestHelper.hasQuestItem( player, ANIMAL_LOVERS_LIST ) ) {
                            state.setMemoState( state.getMemoState() | 4 )

                            return this.getPath( '30072-01.html' )
                        }

                        break

                    case ACCESSORY_MERCHANT_ELICE:
                        if ( QuestHelper.hasQuestItem( player, ANIMAL_LOVERS_LIST ) ) {
                            state.setMemoState( state.getMemoState() | 8 )

                            return this.getPath( '30091-01.html' )
                        }

                        break

                    case GATEKEEPER_BELLA:
                        if ( QuestHelper.hasQuestItem( player, ANIMAL_LOVERS_LIST ) ) {
                            state.setMemoState( state.getMemoState() | 2 )

                            return this.getPath( '30256-01.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    processItemCount( player: L2PcInstance,
            countedItemId: number,
            noItemsPath: string,
            someItemsPath: string,
            fullItemsPath: string ): string {
        let amount: number = QuestHelper.getQuestItemsCount( player, BLOODY_RED_CLAW )

        if ( amount === 0 ) {
            return this.getPath( noItemsPath )
        }

        if ( amount < 50 ) {
            return this.getPath( someItemsPath )
        }

        return this.getPath( fullItemsPath )
    }
}