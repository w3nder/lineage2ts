import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const UMOS = 30502
const TATARU_ZU_HESTUI = 30585
const HESTUI_TOTEM_SPIRIT = 30592
const DUDA_MARA_TOTEM_SPIRIT = 30593
const MOIRA = 31979
const TOTEM_SPIRIT_OF_GANDI = 32057
const DEAD_LEOPARDS_CARCASS = 32090
const FIRE_CHARM = 1616
const KASHA_BEAR_PELT = 1617
const KASHA_BLADE_SPIDER_HUSK = 1618
const FIRST_FIERY_EGG = 1619
const HESTUI_MASK = 1620
const SECOND_FIERY_EGG = 1621
const TOTEM_SPIRIT_CLAW = 1622
const TATARUS_LETTER = 1623
const FLAME_CHARM = 1624
const GRIZZLY_BLOOD = 1625
const BLOOD_CAULDRON = 1626
const SPIRIT_NET = 1627
const BOUND_DURKA_SPIRIT = 1628
const DURKA_PARASITE = 1629
const TOTEM_SPIRIT_BLOOD = 1630
const MASK_OF_MEDIUM = 1631
const DURKA_SPIRIT = 27056
const BLACK_LEOPARD = 27319
const minimumLevel = 18

type MonsterCondition = [ number, number ] // itemId, required state condition
const monsterData: { [ npcId: number ]: MonsterCondition } = {
    20415: [ FIRST_FIERY_EGG, 1 ], // scarlet salamander
    20478: [ KASHA_BLADE_SPIDER_HUSK, 1 ], // kasha blade spider
    20479: [ KASHA_BEAR_PELT, 1 ], // kasha bear
    20335: [ GRIZZLY_BLOOD, 6 ], // grizzly bear
    20038: [ DURKA_PARASITE, 9 ], // poison spider
    20043: [ DURKA_PARASITE, 9 ], // bind poison spider
    27056: [ DURKA_PARASITE, 9 ], // durka spirit
}

export class PathOfTheOrcShaman extends ListenerLogic {
    constructor() {
        super( 'Q00416_PathOfTheOrcShaman', 'listeners/tracked-400/PathOfTheOrcShaman.ts' )
        this.questId = 416
        this.questItemIds = [
            FIRE_CHARM,
            KASHA_BEAR_PELT,
            KASHA_BLADE_SPIDER_HUSK,
            FIRST_FIERY_EGG,
            HESTUI_MASK,
            SECOND_FIERY_EGG,
            TOTEM_SPIRIT_CLAW,
            TATARUS_LETTER,
            FLAME_CHARM,
            GRIZZLY_BLOOD,
            BLOOD_CAULDRON,
            SPIRIT_NET,
            BOUND_DURKA_SPIRIT,
            DURKA_PARASITE,
            TOTEM_SPIRIT_BLOOD,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00416_PathOfTheOrcShaman'
    }

    getQuestStartIds(): Array<number> {
        return [ TATARU_ZU_HESTUI ]
    }

    getTalkIds(): Array<number> {
        return [
            TATARU_ZU_HESTUI,
            UMOS,
            MOIRA,
            DEAD_LEOPARDS_CARCASS,
            DUDA_MARA_TOTEM_SPIRIT,
            HESTUI_TOTEM_SPIRIT,
            TOTEM_SPIRIT_OF_GANDI,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ..._.keys( monsterData ).map( value => _.parseInt( value ) ),
            BLACK_LEOPARD,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'START':
                if ( player.getClassId() !== ClassIdValues.orcMage.id ) {
                    if ( player.getClassId() === ClassIdValues.orcShaman.id ) {
                        return this.getPath( '30585-02.htm' )
                    }

                    return this.getPath( '30585-03.htm' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30585-04.htm' )
                }

                if ( QuestHelper.hasQuestItem( player, MASK_OF_MEDIUM ) ) {
                    return this.getPath( '30585-05.htm' )
                }

                return this.getPath( '30585-06.htm' )

            case '30585-07.htm':
                state.startQuest()
                state.setMemoState( 1 )

                await QuestHelper.giveSingleItem( player, FIRE_CHARM, 1 )
                break

            case '30585-12.html':
                if ( QuestHelper.hasQuestItem( player, TOTEM_SPIRIT_CLAW ) ) {
                    break
                }

                return

            case '30585-13.html':
                if ( QuestHelper.hasQuestItem( player, TOTEM_SPIRIT_CLAW ) ) {
                    await QuestHelper.takeSingleItem( player, TOTEM_SPIRIT_CLAW, -1 )
                    await QuestHelper.giveSingleItem( player, TATARUS_LETTER, 1 )

                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '30585-14.html':
                if ( QuestHelper.hasQuestItem( player, TOTEM_SPIRIT_CLAW ) ) {
                    await QuestHelper.takeSingleItem( player, TOTEM_SPIRIT_CLAW, -1 )
                    state.setConditionWithSound( 12, true )
                    state.setMemoState( 100 )
                    break
                }

                return

            case '30502-07.html':
                if ( QuestHelper.hasQuestItem( player, TOTEM_SPIRIT_BLOOD ) ) {
                    await QuestHelper.takeSingleItem( player, TOTEM_SPIRIT_BLOOD, -1 )
                    await QuestHelper.giveSingleItem( player, MASK_OF_MEDIUM, 1 )

                    if ( player.getLevel() >= 20 ) {
                        await QuestHelper.addExpAndSp( player, 320534, 22992 )
                    } else if ( player.getLevel() === 19 ) {
                        await QuestHelper.addExpAndSp( player, 456128, 29690 )
                    } else {
                        await QuestHelper.addExpAndSp( player, 591724, 36388 )
                    }

                    await QuestHelper.giveAdena( player, 163800, true )
                    await state.exitQuest( false, true )
                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                    break
                }

                return

            case '32090-05.html':
                if ( state.isMemoState( 106 ) ) {
                    break
                }

                return

            case '32090-06.html':
                if ( state.isMemoState( 106 ) ) {
                    state.setMemoState( 107 )
                    state.setConditionWithSound( 18, true )

                    break
                }

                return

            case '30593-02.html':
                if ( QuestHelper.hasQuestItem( player, BLOOD_CAULDRON ) ) {
                    break
                }

                return

            case '30593-03.html':
                if ( QuestHelper.hasQuestItem( player, BLOOD_CAULDRON ) ) {
                    await QuestHelper.takeSingleItem( player, BLOOD_CAULDRON, -1 )
                    await QuestHelper.giveSingleItem( player, SPIRIT_NET, 1 )

                    state.setConditionWithSound( 9, true )
                    break
                }

                return

            case '30592-02.html':
                if ( QuestHelper.hasQuestItems( player, HESTUI_MASK, SECOND_FIERY_EGG ) ) {
                    break
                }

                return

            case '30592-03.html':
                if ( QuestHelper.hasQuestItems( player, HESTUI_MASK, SECOND_FIERY_EGG ) ) {
                    await QuestHelper.takeMultipleItems( player, -1, HESTUI_MASK, SECOND_FIERY_EGG )
                    await QuestHelper.giveSingleItem( player, TOTEM_SPIRIT_CLAW, 1 )

                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '32057-02.html':
                if ( state.isMemoState( 101 ) ) {
                    state.setMemoState( 102 )
                    state.setConditionWithSound( 14, true )

                    break
                }

                return

            case '32057-05.html':
                if ( state.isMemoState( 109 ) ) {
                    state.setMemoState( 110 )
                    state.setConditionWithSound( 21, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId )
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 3, npc )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()

        if ( data.npcId === BLACK_LEOPARD ) {
            switch ( state.getMemoState() ) {
                case 102:
                    state.setMemoState( 103 )
                    return

                case 103:
                    state.setMemoState( 104 )
                    state.setConditionWithSound( 15, true )

                    if ( Math.random() < 0.66 ) {
                        BroadcastHelper.broadcastNpcSayStringId( npc as L2Npc, NpcSayType.NpcAll, NpcStringIds.MY_DEAR_FRIEND_OF_S1_WHO_HAS_GONE_ON_AHEAD_OF_ME, player.getName() )
                    }

                    return

                case 105:
                    state.setMemoState( 106 )
                    state.setConditionWithSound( 17, true )

                    if ( Math.random() < 0.66 ) {
                        BroadcastHelper.broadcastNpcSayStringId( npc as L2Npc, NpcSayType.NpcAll, NpcStringIds.LISTEN_TO_TEJAKAR_GANDI_YOUNG_OROKA_THE_SPIRIT_OF_THE_SLAIN_LEOPARD_IS_CALLING_YOU_S1, player.getName() )
                    }

                    return

                case 107:
                    state.setMemoState( 108 )
                    state.setConditionWithSound( 19, true )

                    return
            }

            return
        }

        let [ itemId, conditionValue ] = monsterData[ data.npcId ]
        if ( conditionValue !== state.getCondition() ) {
            return
        }

        switch ( state.getCondition() ) {
            case 1:
                if ( !QuestHelper.hasQuestItem( player, FIRE_CHARM ) ) {
                    return
                }

                await QuestHelper.giveSingleItem( player, itemId, 1 )
                if ( QuestHelper.hasQuestItems( player, FIRST_FIERY_EGG, KASHA_BLADE_SPIDER_HUSK, KASHA_BEAR_PELT ) ) {
                    state.setConditionWithSound( 2, true )
                }

                return

            case 6:
                if ( !QuestHelper.hasQuestItem( player, FLAME_CHARM ) ) {
                    return
                }

                await QuestHelper.giveSingleItem( player, itemId, 1 )
                state.setConditionWithSound( 7 )

                return

            case 9:
                if ( !QuestHelper.hasQuestItem( player, SPIRIT_NET ) || QuestHelper.hasQuestItem( player, BOUND_DURKA_SPIRIT ) ) {
                    return
                }

                if ( ![ 20038, 20043 ].includes( data.npcId ) ) {
                    await QuestHelper.takeMultipleItems( player, -1, DURKA_PARASITE, SPIRIT_NET )
                    await QuestHelper.giveSingleItem( player, BOUND_DURKA_SPIRIT, 1 )

                    return
                }

                let chance: number = Math.random()
                let parasiteAmount: number = QuestHelper.getQuestItemsCount( player, DURKA_PARASITE )
                if ( ( parasiteAmount === 5 && chance < 0.1 )
                        || ( parasiteAmount === 6 && chance < 0.2 )
                        || ( parasiteAmount === 7 && chance < 0.2 )
                        || parasiteAmount >= 8 ) {
                    await QuestHelper.takeSingleItem( player, DURKA_PARASITE, -1 )
                    QuestHelper.addSpawnAtLocation( DURKA_SPIRIT, npc, true, 0, false )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_BEFORE_BATTLE )

                    return
                }

                await QuestHelper.rewardSingleQuestItem( player, DURKA_PARASITE, 1, data.isChampion )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === TATARU_ZU_HESTUI ) {
                    return this.getPath( '30585-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case TATARU_ZU_HESTUI:
                        if ( state.isMemoState( 1 ) ) {
                            if ( QuestHelper.hasQuestItem( player, FIRE_CHARM ) ) {
                                if ( !QuestHelper.hasQuestItems( player, KASHA_BEAR_PELT, KASHA_BLADE_SPIDER_HUSK, FIRST_FIERY_EGG ) ) {
                                    return this.getPath( '30585-08.html' )
                                }

                                await QuestHelper.takeMultipleItems( player, -1, FIRE_CHARM, KASHA_BEAR_PELT, KASHA_BLADE_SPIDER_HUSK, FIRST_FIERY_EGG )
                                await QuestHelper.giveMultipleItems( player, 1, HESTUI_MASK, SECOND_FIERY_EGG )

                                state.setConditionWithSound( 3, true )
                                return this.getPath( '30585-09.html' )
                            }

                            if ( QuestHelper.hasQuestItems( player, HESTUI_MASK, SECOND_FIERY_EGG ) ) {
                                return this.getPath( '30585-10.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, TOTEM_SPIRIT_CLAW ) ) {
                                return this.getPath( '30585-11.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, TATARUS_LETTER ) ) {
                                return this.getPath( '30585-15.html' )
                            }

                            if ( QuestHelper.hasAtLeastOneQuestItem( player, GRIZZLY_BLOOD, FLAME_CHARM, BLOOD_CAULDRON, SPIRIT_NET, BOUND_DURKA_SPIRIT, TOTEM_SPIRIT_BLOOD ) ) {
                                return this.getPath( '30585-16.html' )
                            }
                        }

                        if ( state.isMemoState( 100 ) ) {
                            return this.getPath( '30585-14.html' )
                        }

                        break

                    case UMOS:
                        if ( !state.isMemoState( 1 ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, TATARUS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, TATARUS_LETTER, -1 )
                            await QuestHelper.giveSingleItem( player, FLAME_CHARM, 1 )

                            state.setConditionWithSound( 6, true )
                            return this.getPath( '30502-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, FLAME_CHARM ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, GRIZZLY_BLOOD ) < 3 ) {
                                return this.getPath( '30502-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, FLAME_CHARM, GRIZZLY_BLOOD )
                            await QuestHelper.giveSingleItem( player, BLOOD_CAULDRON, 1 )

                            state.setConditionWithSound( 8, true )
                            return this.getPath( '30502-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BLOOD_CAULDRON ) ) {
                            return this.getPath( '30502-04.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, BOUND_DURKA_SPIRIT, SPIRIT_NET ) ) {
                            return this.getPath( '30502-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TOTEM_SPIRIT_BLOOD ) ) {
                            return this.getPath( '30502-06.html' )
                        }

                        break

                    case MOIRA:
                        let memoState = state.getMemoState()
                        if ( memoState === 100 ) {
                            state.setMemoState( 101 )
                            state.setConditionWithSound( 13, true )

                            return this.getPath( '31979-01.html' )
                        }

                        if ( memoState >= 101 && memoState < 108 ) {
                            return this.getPath( '31979-02.html' )
                        }

                        if ( memoState === 110 ) {
                            await QuestHelper.giveAdena( player, 81900, true )
                            await QuestHelper.giveSingleItem( player, MASK_OF_MEDIUM, 1 )

                            if ( player.getLevel() >= 20 ) {
                                await QuestHelper.addExpAndSp( player, 160267, 11496 )
                            } else if ( player.getLevel() === 19 ) {
                                await QuestHelper.addExpAndSp( player, 228064, 14845 )
                            } else {
                                await QuestHelper.addExpAndSp( player, 295862, 18194 )
                            }

                            await state.exitQuest( false, true )
                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )

                            return this.getPath( '31979-03.html' )
                        }

                        break

                    case DEAD_LEOPARDS_CARCASS:
                        switch ( state.getMemoState() ) {
                            case 102:
                            case 103:
                                return this.getPath( '32090-01.html' )

                            case 104:
                                state.setMemoState( 105 )
                                state.setConditionWithSound( 16, true )

                                return this.getPath( '32090-03.html' )

                            case 105:
                                return this.getPath( '32090-01.html' )

                            case 106:
                                return this.getPath( '32090-04.html' )

                            case 107:
                                return this.getPath( '32090-07.html' )

                            case 108:
                                state.setMemoState( 109 )
                                state.setConditionWithSound( 20, true )

                                return this.getPath( '32090-08.html' )
                        }

                        break

                    case DUDA_MARA_TOTEM_SPIRIT:
                        if ( !state.isMemoState( 1 ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, BLOOD_CAULDRON ) ) {
                            return this.getPath( '30593-01.html' )
                        }

                        let hasNet: boolean = QuestHelper.hasQuestItem( player, SPIRIT_NET )
                        let hasSpirit: boolean = QuestHelper.hasQuestItem( player, BOUND_DURKA_SPIRIT )

                        if ( hasNet && !hasSpirit ) {
                            return this.getPath( '30593-04.html' )
                        }

                        if ( !hasNet && hasSpirit ) {
                            await QuestHelper.takeSingleItem( player, BOUND_DURKA_SPIRIT, -1 )
                            await QuestHelper.giveSingleItem( player, TOTEM_SPIRIT_BLOOD, 1 )

                            state.setConditionWithSound( 11, true )
                            return this.getPath( '30593-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TOTEM_SPIRIT_BLOOD ) ) {
                            return this.getPath( '30593-06.html' )
                        }

                        break

                    case HESTUI_TOTEM_SPIRIT:
                        if ( !state.isMemoState( 1 ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItems( player, HESTUI_MASK, SECOND_FIERY_EGG ) ) {
                            return this.getPath( '30592-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TOTEM_SPIRIT_CLAW ) ) {
                            return this.getPath( '30592-04.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, GRIZZLY_BLOOD, FLAME_CHARM, BLOOD_CAULDRON, SPIRIT_NET, BOUND_DURKA_SPIRIT, TOTEM_SPIRIT_BLOOD, TATARUS_LETTER ) ) {
                            return this.getPath( '30592-05.html' )
                        }

                        break

                    case TOTEM_SPIRIT_OF_GANDI:
                        switch ( state.getMemoState() ) {
                            case 101:
                                return this.getPath( '32057-01.html' )

                            case 102:
                                return this.getPath( '32057-03.html' )

                            case 109:
                                return this.getPath( '32057-04.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}