import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestType } from '../../gameService/enums/QuestType'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'

import _ from 'lodash'

const DAICHIR = 30537
const REPLENISHED_BEAD = 14877
const DISCHARGED_BEAD = 14878
const minimumLevel = 80

const variableNames = {
    visitedAltars: 'va',
}

export class LuciensAltar extends ListenerLogic {
    constructor() {
        super( 'Q00451_LuciensAltar', 'listeners/tracked-400/LuciensAltar.ts' )
        this.questId = 451
        this.questItemIds = [
            REPLENISHED_BEAD,
            DISCHARGED_BEAD,
        ]
    }

    getTalkIds(): Array<number> {
        return [
            32706,
            32707,
            32708,
            32709,
            32710,
            DAICHIR,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00451_LuciensAltar'
    }

    getQuestStartIds(): Array<number> {
        return [ DAICHIR ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30537-04.htm':
                break

            case '30537-05.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( L2World.getPlayer( data.playerId ), REPLENISHED_BEAD, 5 )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        if ( data.characterNpcId === DAICHIR ) {
            switch ( state.getState() ) {
                case QuestStateValues.COMPLETED:
                    if ( !state.isNowAvailable() ) {
                        return this.getPath( '30537-03.html' )
                    }
                    state.setState( QuestStateValues.CREATED )

                case QuestStateValues.CREATED:
                    return this.getPath( player.getLevel() >= minimumLevel ? '30537-01.htm' : '30537-02.htm' )

                case QuestStateValues.STARTED:
                    if ( state.isCondition( 1 ) ) {
                        let hasVisitedAltar: boolean = _.some( state.getVariable( variableNames.visitedAltars ), ( id: number ) => {
                            return [ 32706, 32707, 32708, 32709, 32710 ].includes( id )
                        } )

                        return this.getPath( hasVisitedAltar ? '30537-10.html' : '30537-09.html' )
                    }

                    await QuestHelper.giveAdena( player, 255380, true )
                    await state.exitQuestWithType( QuestType.DAILY, true )

                    return this.getPath( '30537-08.html' )
            }

            return QuestHelper.getNoQuestMessagePath()
        }

        if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, REPLENISHED_BEAD ) ) {
            if ( _.includes( state.getVariable( variableNames.visitedAltars ), data.characterNpcId ) ) {
                return this.getPath( 'findother.html' )
            }

            if ( !state.getVariable( variableNames.visitedAltars ) ) {
                state.setVariable( variableNames.visitedAltars, [] )
            }

            state.getVariable( variableNames.visitedAltars ).push( data.characterNpcId )
            await QuestHelper.takeSingleItem( player, REPLENISHED_BEAD, 1 )
            await QuestHelper.giveSingleItem( player, DISCHARGED_BEAD, 1 )

            if ( QuestHelper.getQuestItemsCount( player, DISCHARGED_BEAD ) >= 5 ) {
                state.setConditionWithSound( 2, true )
            } else {
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            }

            return this.getPath( 'recharge.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}