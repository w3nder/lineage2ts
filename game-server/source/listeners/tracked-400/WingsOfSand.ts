import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestType } from '../../gameService/enums/QuestType'
import _ from 'lodash'
import aigle from 'aigle'

const separatedSoulNpcIds = [
    32864,
    32865,
    32866,
    32867,
    32868,
    32869,
    32870,
    32891,
]

const EMERALD_HORN = 25718
const DUST_RIDER = 25719
const BLEEDING_FLY = 25720
const BLACK_DAGGER_WING = 25721
const SHADOW_SUMMONER = 25722
const SPIKE_SLASHER = 25723
const MUSCLE_BOMBER = 25724
const LARGE_BABY_DRAGON = 17250
const minimumLevel = 80

const armorPartItemIds = [
    15660,
    15661,
    15662,
    15663,
    15664,
    15665,
    15666,
    15667,
    15668,
    15669,
    15670,
    15671,
    15672,
    15673,
    15674,
    15675,
    15691,
]

const rewardAmountRange: Array<number> = [ 1, 2 ]

export class WingsOfSand extends ListenerLogic {
    constructor() {
        super( 'Q00455_WingsOfSand', 'listeners/tracked-400/WingsOfSand.ts' )
        this.questId = 455
        this.questItemIds = [ LARGE_BABY_DRAGON ]
    }

    getQuestStartIds(): Array<number> {
        return separatedSoulNpcIds
    }

    getTalkIds(): Array<number> {
        return separatedSoulNpcIds
    }

    getAttackableAttackIds(): Array<number> {
        return [
            EMERALD_HORN,
            DUST_RIDER,
            BLEEDING_FLY,
            BLACK_DAGGER_WING,
            SHADOW_SUMMONER,
            SPIKE_SLASHER,
            MUSCLE_BOMBER,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '32864-02.htm':
            case '32864-03.htm':
            case '32864-04.htm':
                break

            case '32864-05.htm':
                state.startQuest()
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance, npc: L2Npc ): Promise<void> => {
            if ( !state || state.getCondition() < 3 ) {
                return
            }

            if ( Math.random() > QuestHelper.getAdjustedChance( LARGE_BABY_DRAGON, 0.35, data.isChampion ) ) {
                return
            }

            if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
                return
            }

            await QuestHelper.giveSingleItem( player, LARGE_BABY_DRAGON, 1 )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

            let amount = QuestHelper.getQuestItemsCount( player, LARGE_BABY_DRAGON )
            if ( amount === 1 ) {
                state.setConditionWithSound( 2, true )
                return
            }

            if ( amount === 2 ) {
                state.setConditionWithSound( 3, true )
                return
            }
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '32864-08.html' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                if ( player.getLevel() >= minimumLevel ) {
                    return this.getPath( '32864-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32864-06.html' )

                    case 2:
                        await this.rewardItems( player, state )
                        return this.getPath( '32864-07.html' )

                    case 3:
                        await this.rewardItems( player, state )
                        return this.getPath( '32864-07.html' )
                }
                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardItems( player: L2PcInstance, state: QuestState ): Promise<void> {
        await aigle.resolve( state.getCondition() - 1 ).timesSeries( () : Promise<void> => {
            let chance = _.random( 1000 )
            let amount = _.sample( rewardAmountRange )

            if ( chance < 50 ) {
                return QuestHelper.rewardSingleItem( player, _.random( 15815, 15825 ), 1 ) // Weapon Recipes
            }

            if ( chance < 100 ) {
                return QuestHelper.rewardSingleItem( player, _.random( 15792, 15808 ), amount ) // Armor Recipes
            }

            if ( chance < 150 ) {
                return QuestHelper.rewardSingleItem( player, _.random( 15809, 15811 ), amount ) // Jewelry Recipes
            }

            if ( chance < 250 ) {
                return QuestHelper.rewardSingleItem( player, _.sample( armorPartItemIds ), amount ) // Armor Parts
            }

            if ( chance < 500 ) {
                return QuestHelper.rewardSingleItem( player, _.random( 15634, 15644 ), amount ) // Weapon Parts
            }

            if ( chance < 750 ) {
                return QuestHelper.rewardSingleItem( player, _.random( 15769, 15771 ), amount ) // Jewelry Parts
            }

            if ( chance < 900 ) {
                return QuestHelper.rewardSingleItem( player, _.random( 9552, 9557 ), 1 ) // Crystals
            }

            if ( chance < 970 ) {
                return QuestHelper.rewardSingleItem( player, 6578, 1 ) // Blessed Scroll: Enchant Armor (S-Grade)
            }

            return QuestHelper.rewardSingleItem( player, 6577, 1 ) // Blessed Scroll: Enchant Weapon (S-Grade)
        } )

        return state.exitQuestWithType( QuestType.DAILY, true )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00455_WingsOfSand'
    }
}