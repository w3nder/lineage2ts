import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Party } from '../../gameService/models/L2Party'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import aigle from 'aigle'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestType } from '../../gameService/enums/QuestType'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

const THEODRIC = 30755
const BEHEMOTH_DRAGON = 29069
const TARASK_DRAGON = 29190
const TARASK_DRAGONS_LEATHER_FRAGMENT = 21991
const BEHEMOTH_DRAGON_LEATHER = 21992
const SCROLL_ANTHARAS_CALL = 21897
const PORTAL_STONE = 3865
const minimumLevel = 83

export class TheCallOfAntharas extends ListenerLogic {
    constructor() {
        super( 'Q00903_TheCallOfAntharas', 'listeners/tracked-900/TheCallOfAntharas.ts' )
        this.questId = 903
        this.questItemIds = [
            TARASK_DRAGONS_LEATHER_FRAGMENT,
            BEHEMOTH_DRAGON_LEATHER,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ THEODRIC ]
    }

    getTalkIds(): Array<number> {
        return [ THEODRIC ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            BEHEMOTH_DRAGON,
            TARASK_DRAGON,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00903_TheCallOfAntharas'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( player.getLevel() < minimumLevel || !QuestHelper.hasQuestItem( player, PORTAL_STONE ) ) {
            return
        }

        switch ( data.eventName ) {
            case '30755-05.htm':
                break

            case '30755-06.html':
                state.startQuest()
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let itemId: number = data.npcId === BEHEMOTH_DRAGON ? BEHEMOTH_DRAGON_LEATHER : TARASK_DRAGONS_LEATHER_FRAGMENT
        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state : QuestState, player : L2PcInstance ) : Promise<void> => {
            await QuestHelper.giveSingleItem( player, itemId, 1 )

            if ( QuestHelper.hasQuestItem( player, BEHEMOTH_DRAGON_LEATHER )
                    && QuestHelper.hasQuestItem( player, TARASK_DRAGONS_LEATHER_FRAGMENT ) ) {
                state.setConditionWithSound( 2, true )
                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '30755-02.html' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30755-03.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, PORTAL_STONE ) ) {
                    return this.getPath( '30755-04.html' )
                }

                return this.getPath( '30755-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30755-07.html' )

                    case 2:
                        await QuestHelper.rewardSingleItem( player, SCROLL_ANTHARAS_CALL, 1 )
                        await state.exitQuestWithType( QuestType.DAILY, true )

                        return this.getPath( '30755-08.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}