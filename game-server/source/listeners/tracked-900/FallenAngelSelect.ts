import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { ListenerManager } from '../../gameService/instancemanager/ListenerManager'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2World } from '../../gameService/L2World'

const NATOOLS = 30894
const minimumLevel = 38

export class FallenAngelSelect extends ListenerLogic {
    constructor() {
        super( 'Q00998_FallenAngelSelect', 'listeners/tracked-900/FallenAngelSelect.ts' )
        this.questId = 998
    }

    getQuestStartIds(): Array<number> {
        return [ NATOOLS ]
    }

    getTalkIds(): Array<number> {
        return [ NATOOLS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00998_FallenAngelSelect'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30894-01.html':
            case '30894-02.html':
            case '30894-03.html':
                break

            case 'dawn':
                state.setState( QuestStateValues.COMPLETED )
                return this.startQuest( 'Q00142_FallenAngelRequestOfDawn', data.playerId )

            case 'dusk':
                state.setState( QuestStateValues.COMPLETED )
                return this.startQuest( 'Q00143_FallenAngelRequestOfDusk', data.playerId )
        }

        return this.getPath( data.eventName )
    }

    startQuest( name: string, playerId: number ): Promise<string> {
        let listener: ListenerLogic = ListenerManager.getListenerByName( name )
        if ( !listener ) {
            return
        }

        QuestStateCache.createQuestState( playerId, name )
        return listener.onNpcEvent( {
            characterId: 0,
            characterNpcId: NATOOLS,
            eventName: '30894-01.html',
            playerId,
            isTimer: false
        } )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let companionState: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q00141_ShadowFoxPartThree', false )
        if ( !companionState || !companionState.isCompleted() ) {
            return this.getPath( '30894-00.html' )
        }

        let player = L2World.getPlayer( data.playerId )
        return this.getPath( player.getLevel() > minimumLevel ? '30894-01.html' : '30894-00.html' )
    }
}