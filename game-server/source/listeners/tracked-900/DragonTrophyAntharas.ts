import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { QuestType } from '../../gameService/enums/QuestType'

const THEODRIC = 30755
const ANTHARAS = 29068
const MEDAL_OF_GLORY = 21874
const PORTAL_STONE = 3865
const minimumLevel = 84

export class DragonTrophyAntharas extends ListenerLogic {
    constructor() {
        super( 'Q00904_DragonTrophyAntharas', 'listeners/tracked-900/DragonTrophyAntharas.ts' )
        this.questId = 904
    }

    getQuestStartIds(): Array<number> {
        return [ THEODRIC ]
    }

    getTalkIds(): Array<number> {
        return [ THEODRIC ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ANTHARAS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00904_DragonTrophyAntharas'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( player.getLevel() < minimumLevel || QuestHelper.hasQuestItem( player, PORTAL_STONE ) ) {
            return
        }

        switch ( data.eventName ) {
            case '30755-05.htm':
            case '30755-06.htm':
                break

            case '30755-07.html':
                await state.startQuest()
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForCommandChannel( data.playerId, data.targetId, this.getName(), ( state: QuestState ): Promise<void> => {
            if ( state.isCondition( 1 ) ) {
                state.setConditionWithSound( 2, true )
            }

            return
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.COMPLETED:
                if ( !state.isNowAvailable() ) {
                    return this.getPath( '30755-03.html' )
                }

                state.setState( QuestStateValues.CREATED )

            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30755-02.html' )
                }

                if ( !QuestHelper.hasQuestItem( player, PORTAL_STONE ) ) {
                    return this.getPath( '30755-04.html' )
                }

                return this.getPath( '30755-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30755-08.html' )

                    case 2:
                        await QuestHelper.rewardSingleItem( player, MEDAL_OF_GLORY, 30 )
                        await state.exitQuestWithType( QuestType.DAILY, true )

                        return this.getPath( '30755-09.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}