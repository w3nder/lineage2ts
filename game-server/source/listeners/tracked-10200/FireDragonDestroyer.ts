import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

const KLEIN = 31540
const VALAKAS = 29028
const FLOATING_STONE = 7267
const POOR_NECKLACE = 15524
const VALOR_NECKLACE = 15525
const VALAKAS_SLAYER_CIRCLET = 8567
const minimumLevel = 83

export class FireDragonDestroyer extends ListenerLogic {
    constructor() {
        super( 'Q10291_FireDragonDestroyer', 'listeners/tracked-10200/FireDragonDestroyer.ts' )
        this.questId = 10291
        this.questItemIds = [ POOR_NECKLACE, VALOR_NECKLACE ]
    }

    getQuestStartIds(): Array<number> {
        return [ KLEIN ]
    }

    getTalkIds(): Array<number> {
        return [ KLEIN ]
    }

    getAttackableKillIds(): Array<number> {
        return [ VALAKAS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10291_FireDragonDestroyer'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        if ( data.eventName === '31540-05.htm' ) {
            let player = L2World.getPlayer( data.playerId )

            state.startQuest()
            await QuestHelper.giveSingleItem( player, POOR_NECKLACE, 1 )

            return this.getPath( data.eventName )
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForCommandChannel( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance ): Promise<void> => {
            if ( !state.isCondition( 1 ) || !QuestHelper.hasQuestItem( player, POOR_NECKLACE ) ) {
                return
            }

            await QuestHelper.takeSingleItem( player, POOR_NECKLACE, -1 )
            await QuestHelper.giveSingleItem( player, VALOR_NECKLACE, 1 )

            state.setConditionWithSound( 2, true )
        }, 8000 )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31540-00.htm' )
                }

                return this.getPath( QuestHelper.hasQuestItem( player, FLOATING_STONE ) ? '31540-02.htm' : '31540-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.hasQuestItem( player, POOR_NECKLACE ) ) {
                            return this.getPath( '31540-06.html' )
                        }

                        await QuestHelper.giveSingleItem( player, POOR_NECKLACE, 1 )
                        return this.getPath( '31540-07.html' )

                    case 2:
                        if ( !QuestHelper.hasQuestItem( player, VALOR_NECKLACE ) ) {
                            break
                        }

                        await QuestHelper.giveAdena( player, 126549, true )
                        await QuestHelper.addExpAndSp( player, 717291, 77397 )
                        await QuestHelper.giveSingleItem( player, VALAKAS_SLAYER_CIRCLET, 1 )
                        await state.exitQuest( false, true )

                        return this.getPath( '31540-08.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '31540-09.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}