import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const HARDIN = 30832
const WOOD = 32593
const FRANZ = 32597
const ELCADIA = 32784
const ELCADIA_2 = 32787
const ERISS_EVIL_THOUGHTS = 32792
const ODD_GLOBE = 32815
const CERTIFICATE_OF_DAWN = 17265
const minimumLevel = 81

export class SevenSignsOneWhoSeeksThePowerOfTheSeal extends ListenerLogic {
    constructor() {
        super( 'Q10296_SevenSignsOneWhoSeeksThePowerOfTheSeal', 'listeners/tracked-10200/SevenSignsOneWhoSeeksThePowerOfTheSeal.ts' )
        this.questId = 10296
    }

    getQuestStartIds(): Array<number> {
        return [
            ERISS_EVIL_THOUGHTS,
            ODD_GLOBE,
        ]
    }

    getTalkIds(): Array<number> {
        return [
            ERISS_EVIL_THOUGHTS,
            ODD_GLOBE,
            HARDIN,
            WOOD,
            FRANZ,
            ELCADIA,
            ELCADIA_2,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10296_SevenSignsOneWhoSeeksThePowerOfTheSeal'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32792-02.htm':
                break

            case '32792-03.htm':
                state.startQuest()

                break

            case '30832-03.html':
                if ( state.isCondition( 4 ) ) {
                    break
                }

                return

            case '30832-04.html':
                if ( state.isCondition( 4 ) ) {
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            case '32593-03.html':
                if ( state.isCondition( 5 ) ) {
                    break
                }

                return

            case '32597-02.html':
                if ( state.isCondition( 5 ) ) {
                    break
                }

                return

            case '32597-03.html':
                if ( state.isCondition( 5 ) ) {
                    if ( player.isSubClassActive() ) {
                        break
                    }

                    await QuestHelper.addExpAndSp( player, 125000000, 12500000 )
                    await QuestHelper.giveSingleItem( player, CERTIFICATE_OF_DAWN, 1 )
                    await state.exitQuest( false, true )

                    return this.getPath( '32597-04.html' )
                }

                return

            case '32784-02.html':
                if ( state.isCondition( 3 ) ) {
                    break
                }

                return

            case '32784-03.html':
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === ELCADIA_2 ) {
                    return this.getPath( '32787-01.html' )
                }

                if ( !player.hasQuestCompleted( 'Q10295_SevenSignsSolinasTomb' ) ) {
                    break
                }

                switch ( data.characterNpcId ) {
                    case ERISS_EVIL_THOUGHTS:
                        if ( player.getLevel() >= minimumLevel ) {
                            return this.getPath( '32792-01.htm' )
                        }

                        break

                    case ODD_GLOBE:
                        return this.getPath( '32815-01.html' )
                }

                break

            case QuestStateValues.STARTED:
                let conditionValue = state.getCondition()

                switch ( data.characterNpcId ) {
                    case ERISS_EVIL_THOUGHTS:
                        switch ( conditionValue ) {
                            case 1:
                                state.setConditionWithSound( 2, true )
                                return this.getPath( '32792-05.html' )

                            case 2:
                                return this.getPath( '32792-06.html' )
                        }

                        break

                    case ODD_GLOBE:
                        if ( conditionValue <= 2 ) {
                            return this.getPath( '32815-01.html' )
                        }

                        return this.getPath( '32815-02.html' )

                    case HARDIN:
                        if ( conditionValue < 4 ) {
                            return this.getPath( '30832-01.html' )
                        }

                        if ( conditionValue === 4 ) {
                            return this.getPath( '30832-02.html' )
                        }

                        return this.getPath( '30832-04.html' )

                    case WOOD:
                        if ( conditionValue < 5 ) {
                            return this.getPath( '32593-01.html' )
                        }

                        if ( conditionValue === 5 ) {
                            return this.getPath( '32593-02.html' )
                        }

                        return this.getPath( '32593-04.html' )

                    case FRANZ:
                        if ( state.isCondition( 5 ) ) {
                            return this.getPath( '32597-01.html' )
                        }

                        break

                    case ELCADIA:
                        if ( conditionValue === 3 ) {
                            return this.getPath( '32784-01.html' )
                        }

                        if ( conditionValue > 3 ) {
                            return this.getPath( '32784-04.html' )
                        }

                        break

                    case ELCADIA_2:
                        if ( conditionValue < 2 ) {
                            return this.getPath( '32787-02.html' )
                        }

                        if ( conditionValue === 2 ) {
                            return this.getPath( '32787-03.html' )
                        }

                        state.setConditionWithSound( 3, true )
                        return this.getPath( '32787-04.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === ERISS_EVIL_THOUGHTS ) {
                    return this.getPath( '32792-04.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}