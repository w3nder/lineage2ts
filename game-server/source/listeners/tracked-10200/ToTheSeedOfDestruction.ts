import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const KEUCEREUS = 32548
const ALLENOS = 32526
const INTRODUCTION = 13812
const minimumLevel = 75

export class ToTheSeedOfDestruction extends ListenerLogic {
    constructor() {
        super( 'Q10269_ToTheSeedOfDestruction', 'listeners/tracked-10200/ToTheSeedOfDestruction.ts' )
        this.questId = 10269
        this.questItemIds = [ INTRODUCTION ]
    }

    getQuestStartIds(): Array<number> {
        return [ KEUCEREUS ]
    }

    getTalkIds(): Array<number> {
        return [ KEUCEREUS, ALLENOS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10269_ToTheSeedOfDestruction'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        if ( data.eventName === '32548-05.html' ) {
            let player = L2World.getPlayer( data.playerId )

            state.startQuest()
            await QuestHelper.giveSingleItem( player, INTRODUCTION, 1 )

            return this.getPath( data.eventName )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== KEUCEREUS ) {
                    break
                }

                return this.getPath( player.getLevel() < minimumLevel ? '32548-00.html' : '32548-01.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case KEUCEREUS:
                        return this.getPath( '32548-06.html' )

                    case ALLENOS:
                        await QuestHelper.giveAdena( player, 29174, true )
                        await QuestHelper.addExpAndSp( player, 176121, 7671 )
                        await state.exitQuest( false, true )

                        return this.getPath( '32526-01.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case KEUCEREUS:
                        return this.getPath( '32548-0a.html' )

                    case ALLENOS:
                        return this.getPath( '32526-02.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}