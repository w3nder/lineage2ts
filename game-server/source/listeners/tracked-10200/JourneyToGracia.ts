import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ORVEN = 30857
const KEUCEREUS = 32548
const PAPIKU = 32564
const LETTER = 13810
const minimumLevel = 75

export class JourneyToGracia extends ListenerLogic {
    constructor() {
        super( 'Q10267_JourneyToGracia', 'listeners/tracked-10200/JourneyToGracia.ts' )
        this.questId = 10267
        this.questItemIds = [ LETTER ]
    }

    getQuestStartIds(): Array<number> {
        return [ ORVEN ]
    }

    getTalkIds(): Array<number> {
        return [ ORVEN, KEUCEREUS, PAPIKU ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10267_JourneyToGracia'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30857-06.html':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, LETTER, 1 )
                break

            case '32564-02.html':
                state.setConditionWithSound( 2, true )
                break

            case '32548-02.html':
                await QuestHelper.giveAdena( player, 92500, true )
                await QuestHelper.addExpAndSp( player, 75480, 7570 )
                await state.exitQuest( false, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== ORVEN ) {
                    break
                }

                return this.getPath( player.getLevel() < minimumLevel ? '30857-00.html' : '30857-01.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case ORVEN:
                        return this.getPath( '30857-07.html' )

                    case PAPIKU:
                        return this.getPath( state.isCondition( 1 ) ? '32564-01.html' : '32564-03.html' )

                    case KEUCEREUS:
                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '32548-01.html' )
                        }

                        break
                }
                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case KEUCEREUS:
                        return this.getPath( '32548-03.html' )

                    case ORVEN:
                        return this.getPath( '30857-0a.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}