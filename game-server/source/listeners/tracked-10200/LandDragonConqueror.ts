import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

const THEODRIC = 30755
const ANTHARAS = 29068
const PORTAL_STONE = 3865
const SHABBY_NECKLACE = 15522
const MIRACLE_NECKLACE = 15523
const ANTHARAS_SLAYER_CIRCLET = 8568
const minimumLevel = 83

export class LandDragonConqueror extends ListenerLogic {
    constructor() {
        super( 'Q10290_LandDragonConqueror', 'listeners/tracked-10200/LandDragonConqueror.ts' )
        this.questId = 10290
        this.questItemIds = [ MIRACLE_NECKLACE, SHABBY_NECKLACE ]
    }

    getQuestStartIds(): Array<number> {
        return [ THEODRIC ]
    }

    getTalkIds(): Array<number> {
        return [ THEODRIC ]
    }

    getAttackableKillIds(): Array<number> {
        return [ ANTHARAS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10290_LandDragonConqueror'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return QuestHelper.getNoQuestMessagePath()
        }

        if ( data.eventName === '30755-05.htm' ) {
            let player = L2World.getPlayer( data.playerId )

            state.startQuest()
            await QuestHelper.giveSingleItem( player, SHABBY_NECKLACE, 1 )

            return this.getPath( data.eventName )
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForCommandChannel( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance ): Promise<void> => {
            if ( !state.isCondition( 1 ) || !QuestHelper.hasQuestItem( player, SHABBY_NECKLACE ) ) {
                return
            }

            await QuestHelper.takeSingleItem( player, SHABBY_NECKLACE, -1 )
            await QuestHelper.giveSingleItem( player, MIRACLE_NECKLACE, 1 )

            state.setConditionWithSound( 2, true )
        }, 8000 )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30755-00.htm' )
                }

                return this.getPath( QuestHelper.hasQuestItem( player, PORTAL_STONE ) ? '30755-02.htm' : '30755-01.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        if ( QuestHelper.hasQuestItem( player, SHABBY_NECKLACE ) ) {
                            return this.getPath( '30755-06.html' )
                        }

                        await QuestHelper.giveSingleItem( player, SHABBY_NECKLACE, 1 )
                        return this.getPath( '30755-07.html' )

                    case 2:
                        if ( !QuestHelper.hasQuestItem( player, MIRACLE_NECKLACE ) ) {
                            break
                        }

                        await QuestHelper.giveAdena( player, 131236, true )
                        await QuestHelper.addExpAndSp( player, 702557, 76334 )
                        await QuestHelper.giveSingleItem( player, ANTHARAS_SLAYER_CIRCLET, 1 )
                        await state.exitQuest( false, true )

                        return this.getPath( '30755-08.html' )
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '30755-09.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}