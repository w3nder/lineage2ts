import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const BATHIS = 30332
const ROHMER = 30344
const TOMLAN_KAMOS = 18554
const OL_ARIOSH = 18555
const TISSUE_TK = 13830
const TISSUE_OA = 13831
const minimumLevel = 17

export class MutatedKaneusGludio extends ListenerLogic {
    constructor() {
        super( 'Q10276_MutatedKaneusGludio', 'listeners/tracked-10200/MutatedKaneusGludio.ts' )
        this.questId = 10276
        this.questItemIds = [ TISSUE_TK, TISSUE_OA ]
    }

    getQuestStartIds(): Array<number> {
        return [ BATHIS ]
    }

    getTalkIds(): Array<number> {
        return [ BATHIS, ROHMER ]
    }

    getAttackableKillIds(): Array<number> {
        return [ TOMLAN_KAMOS, OL_ARIOSH ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10276_MutatedKaneusGludio'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30332-03.htm':
                state.startQuest()
                break

            case '30344-03.htm':
                let player = L2World.getPlayer( data.playerId )
                if ( !QuestHelper.hasQuestItems( player, TISSUE_TK, TISSUE_OA ) ) {
                    return
                }

                await QuestHelper.giveAdena( player, 8500, true )
                await state.exitQuest( false, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 1, L2World.getObjectById( data.targetId ) )
        if ( !player ) {
            return
        }

        let itemId: number = data.npcId === TOMLAN_KAMOS ? TISSUE_TK : TISSUE_OA
        await QuestHelper.giveSingleItem( player, itemId, 1 )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    checkPartyMemberConditions( state: QuestState, conditionValue: number, target: L2Npc ): boolean {
        if ( !state.isStarted() ) {
            return false
        }

        let player = state.getPlayer()
        let itemId: number = target.getId() === TOMLAN_KAMOS ? TISSUE_TK : TISSUE_OA

        return !QuestHelper.hasQuestItem( player, itemId )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== BATHIS ) {
                    break
                }

                return this.getPath( player.getLevel() > minimumLevel ? '30332-01.htm' : '30332-00.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case BATHIS:
                        return this.getPath( QuestHelper.hasQuestItems( player, TISSUE_TK, TISSUE_OA ) ? '30332-05.htm' : '30332-04.htm' )

                    case ROHMER:
                        return this.getPath( QuestHelper.hasQuestItems( player, TISSUE_TK, TISSUE_OA ) ? '30344-02.htm' : '30344-01.htm' )
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case BATHIS:
                        return this.getPath( '30332-06.htm' )

                    case ROHMER:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}