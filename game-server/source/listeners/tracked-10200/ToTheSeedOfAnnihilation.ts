import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const KBALDIR = 32733
const KLEMIS = 32734
const SOA_ORDERS = 15512
const minimumLevel = 84

export class ToTheSeedOfAnnihilation extends ListenerLogic {
    constructor() {
        super( 'Q10282_ToTheSeedOfAnnihilation', 'listeners/tracked-10200/ToTheSeedOfAnnihilation.ts' )
        this.questId = 10282
        this.questItemIds = [ SOA_ORDERS ]
    }

    getQuestStartIds(): Array<number> {
        return [ KBALDIR ]
    }

    getTalkIds(): Array<number> {
        return [ KBALDIR, KLEMIS ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10282_ToTheSeedOfAnnihilation'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32733-07.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, SOA_ORDERS, 1 )

                break

            case '32734-02.htm':
                await QuestHelper.addExpAndSp( player, 1148480, 99110 )
                await state.exitQuest( false )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== KBALDIR ) {
                    break
                }

                return this.getPath( player.getLevel() < minimumLevel ? '32733-00.htm' : '32733-01.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case KBALDIR:
                        return this.getPath( '32733-08.htm' )

                    case KLEMIS:
                        return this.getPath( '32734-01.htm' )
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case KBALDIR:
                        return this.getPath( '32733-09.htm' )

                    case KLEMIS:
                        return this.getPath( '32734-03.htm' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}