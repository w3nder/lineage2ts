import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { InventorySlot } from '../../gameService/values/InventoryValues'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'
import { ElementalType } from '../../gameService/enums/ElementalType'

const HOLLY = 30839
const WEBER = 31307
const YIN = 32325
const YANG = 32326
const WATER = 27380
const AIR = 27381
const YINSWORD = 13845
const YANGSWORD = 13881
const SOULPIECEWATER = 13861
const SOULPIECEAIR = 13862
const BLESSING_OF_FIRE = new SkillHolder( 2635, 1 )
const BLESSING_OF_EARTH = new SkillHolder( 2636, 1 )
const minimumLevel = 75

export class ContainingTheAttributePower extends ListenerLogic {
    constructor() {
        super( 'Q10275_ContainingTheAttributePower', 'listeners/tracked-10200/ContainingTheAttributePower.ts' )
        this.questId = 10275
        this.questItemIds = [
            YINSWORD,
            YANGSWORD,
            SOULPIECEWATER,
            SOULPIECEAIR,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ HOLLY, WEBER ]
    }

    getTalkIds(): Array<number> {
        return [ HOLLY, WEBER, YIN, YANG ]
    }

    getAttackableKillIds(): Array<number> {
        return [ AIR, WATER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10275_ContainingTheAttributePower'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( data.eventName.length === 1 ) {
            let index = _.parseInt( data.eventName )
            if ( !_.isNumber( index ) ) {
                return
            }

            await QuestHelper.rewardSingleItem( player, 10520 + index, 2 )
            await QuestHelper.addExpAndSp( player, 202160, 20375 )
            await state.exitQuest( false, true )

            return this.getPath( `${ data.characterNpcId }-1${ data.eventName }.html` )
        }

        switch ( data.eventName ) {
            case '30839-02.html':
            case '31307-02.html':
                state.startQuest()
                break

            case '30839-05.html':
                state.setConditionWithSound( 2, true )
                break

            case '31307-05.html':
                state.setConditionWithSound( 7, true )
                break

            case '32325-03.html':
                state.setConditionWithSound( 3, true )
                await QuestHelper.giveSingleItemWithAttribute( player, YINSWORD, 1, ElementalType.FIRE, 10 )
                break

            case '32326-03.html':
                state.setConditionWithSound( 8, true )
                await QuestHelper.giveSingleItemWithAttribute( player, YANGSWORD, 1, ElementalType.EARTH, 10 )
                break

            case '32325-06.html':
                if ( QuestHelper.hasQuestItem( player, YINSWORD ) ) {
                    await QuestHelper.takeSingleItem( player, YINSWORD, 1 )

                    return this.getPath( '32325-07.html' )
                }

                await QuestHelper.giveSingleItemWithAttribute( player, YINSWORD, 1, ElementalType.FIRE, 10 )
                break

            case '32326-06.html':
                if ( QuestHelper.hasQuestItem( player, YANGSWORD ) ) {
                    await QuestHelper.takeSingleItem( player, YANGSWORD, 1 )

                    return this.getPath( '32326-07.html' )
                }

                await QuestHelper.giveSingleItemWithAttribute( player, YANGSWORD, 1, ElementalType.EARTH, 10 )
                break

            case '32325-09.html':
                state.setConditionWithSound( 5, true )
                await BLESSING_OF_FIRE.getSkill().applyEffects( player, player )

                await QuestHelper.giveSingleItemWithAttribute( player, YINSWORD, 1, ElementalType.FIRE, 10 )
                break

            case '32326-09.html':
                state.setConditionWithSound( 10, true )
                await BLESSING_OF_EARTH.getSkill().applyEffects( player, player )

                await QuestHelper.giveSingleItemWithAttribute( player, YANGSWORD, 1, ElementalType.EARTH, 10 )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let itemId = data.npcId === AIR ? SOULPIECEAIR : SOULPIECEWATER
        if ( Math.random() > QuestHelper.getAdjustedChance( itemId, 0.3, data.isChampion ) ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let applicableConditions: Array<number> = data.npcId === AIR ? [ 8, 10 ] : [ 3, 4, 5 ]
        if ( !applicableConditions.includes( state.getCondition() ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= 6 ) {
            return
        }

        let requiredItemId: number = data.npcId === AIR ? YANGSWORD : YINSWORD
        if ( requiredItemId !== player.getInventory().getPaperdollItemId( InventorySlot.RightHand ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= 6 ) {
            state.setConditionWithSound( state.getCondition() + 1, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                switch ( data.characterNpcId ) {
                    case HOLLY:
                        return this.getPath( player.getLevel() > minimumLevel ? '30839-01.htm' : '30839-00.html' )

                    case WEBER:
                        return this.getPath( player.getLevel() > minimumLevel ? '31307-01.htm' : '31307-00.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case HOLLY:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30839-03.html' )

                            case 2:
                                return this.getPath( '30839-05.html' )
                        }

                        break

                    case WEBER:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31307-03.html' )

                            case 7:
                                return this.getPath( '31307-05.html' )
                        }

                        break

                    case YIN:
                        switch ( state.getCondition() ) {
                            case 2:
                                return this.getPath( '32325-01.html' )

                            case 3:
                            case 5:
                                return this.getPath( '32325-04.html' )

                            case 4:
                                await QuestHelper.takeMultipleItems( player, -1, YINSWORD, SOULPIECEWATER )
                                return this.getPath( '32325-08.html' )

                            case 6:
                                return this.getPath( '32325-10.html' )
                        }

                        break

                    case YANG:
                        switch ( state.getCondition() ) {
                            case 7:
                                return this.getPath( '32326-01.html' )

                            case 8:
                            case 10:
                                return this.getPath( '32326-04.html' )

                            case 9:
                                await QuestHelper.takeMultipleItems( player, -1, YANGSWORD, SOULPIECEAIR )
                                return this.getPath( '32326-08.html' )

                            case 11:
                                return this.getPath( '32326-10.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case HOLLY:
                        return this.getPath( '30839-0a.html' )

                    case WEBER:
                        return this.getPath( '31307-0a.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}