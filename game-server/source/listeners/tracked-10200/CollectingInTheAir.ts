import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { EventType, NpcGeneralEvent, NpcSeeSkillEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'

const LEKON = 32557
const SCROLL = 13844
const RED = 13858
const BLUE = 13859
const GREEN = 13860
const minimumLevel = 75
const npcIds: Array<number> = [
    18684, // Red Star Stone
    18685, // Red Star Stone
    18686, // Red Star Stone
    18687, // Blue Star Stone
    18688, // Blue Star Stone
    18689, // Blue Star Stone
    18690, // Green Star Stone
    18691, // Green Star Stone
    18692, // Green Star Stone
]

export class CollectingInTheAir extends ListenerLogic {
    constructor() {
        super( 'Q10274_CollectingInTheAir', 'listeners/tracked-10200/CollectingInTheAir.ts' )
        this.questId = 10274
        this.questItemIds = [
            SCROLL,
            RED,
            BLUE,
            GREEN,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ LEKON ]
    }

    getTalkIds(): Array<number> {
        return [ LEKON ]
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSeeSkill,
                method: this.onNpcSeeSkill.bind( this ),
                ids: npcIds,
                triggers: {
                    targetIds: new Set( [ 2630 ] )
                }
            }
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10274_CollectingInTheAir'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        if ( data.eventName === '32557-03.html' ) {
            let player = L2World.getPlayer( data.playerId )

            state.startQuest()
            await QuestHelper.giveSingleItem( player, SCROLL, 9 )
        }

        return this.getPath( data.eventName )
    }

    async onNpcSeeSkill( data: NpcSeeSkillEvent ): Promise<void> {
        if ( data.skillId !== 2630 ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.receiverNpcId ) {
            case 18684:
            case 18685:
            case 18686:
                await QuestHelper.rewardSingleQuestItem( player, RED, 1, data.receiverIsChampion )
                break

            case 18687:
            case 18688:
            case 18689:
                await QuestHelper.rewardSingleQuestItem( player, BLUE, 1, data.receiverIsChampion )
                break

            case 18690:
            case 18691:
            case 18692:
                await QuestHelper.rewardSingleQuestItem( player, GREEN, 1, data.receiverIsChampion )
                break
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

        let npc = L2World.getObjectById( data.receiverId ) as L2Npc
        await npc.doDie( player )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q10273_GoodDayToFly' )
                return this.getPath( canProceed ? '32557-01.htm' : '32557-00.html' )

            case QuestStateValues.STARTED:
                if ( QuestHelper.getItemsSumCount( player, RED, BLUE, GREEN ) >= 8 ) {
                    await QuestHelper.rewardSingleItem( player, 13728, 1 )
                    await QuestHelper.addExpAndSp( player, 25160, 2525 )
                    await state.exitQuest( false, true )

                    return this.getPath( '32557-05.html' )
                }

                return this.getPath( '32557-04.html' )

            case QuestStateValues.COMPLETED:
                return this.getPath( '32557-0a.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}