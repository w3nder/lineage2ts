import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const ELCADIA = 32784
const ELCADIA_INSTANCE = 32787
const ERIS_EVIL_THOUGHTS = 32792
const SOLINAS_EVIL_THOUGHTS = 32793
const JUDE_VAN_ETINA = 32797
const RELIC_GUARDIAN = 32803
const RELIC_WATCHER1 = 32804
const RELIC_WATCHER2 = 32805
const RELIC_WATCHER3 = 32806
const RELIC_WATCHER4 = 32807
const ODD_GLOBE = 32815
const READING_DESK1 = 32821
const READING_DESK2 = 32822
const READING_DESK3 = 32823
const READING_DESK4 = 32824
const READING_DESK5 = 32825
const READING_DESK6 = 32826
const READING_DESK7 = 32827
const READING_DESK8 = 32828
const READING_DESK9 = 32829
const READING_DESK10 = 32830
const READING_DESK11 = 32831
const READING_DESK12 = 32832
const READING_DESK13 = 32833
const READING_DESK14 = 32834
const READING_DESK15 = 32835
const READING_DESK16 = 32836
const JUDE_EVIL_THOUGHTS = 32888
const SOLINA_LAY_BROTHER = 22125
const GUIDE_SOLINA = 27415
const minimumLevel = 81

const VAMPIRIC_RAGE = 6727
const RESIST_HOLY = 6729

const mageBuffIds: Array<number> = [
    6714, // Wind Walk of Elcadia
    6721, // Empower of Elcadia
    6722, // Acumen of Elcadia
    6717, // Berserker Spirit of Elcadia
]

const warriorBuffIds: Array<number> = [
    6714, // Wind Walk of Elcadia
    6715, // Haste of Elcadia
    6716, // Might of Elcadia
    6717, // Berserker Spirit of Elcadia
]

const variableNames = {
    one: 'one',
    two: 'two',
    three: 'three',
    four: 'four',
}

const eventNames = {
    spawnMobs: 'spm',
}

export class SevenSignsToTheMonasteryOfSilence extends ListenerLogic {
    constructor() {
        super( 'Q10294_SevenSignsToTheMonasteryOfSilence', 'listeners/tracked-10200/SevenSignsToTheMonasteryOfSilence.ts' )
        this.questId = 10294
    }

    getQuestStartIds(): Array<number> {
        return [
            ELCADIA,
            ODD_GLOBE,
            ELCADIA_INSTANCE,
            RELIC_GUARDIAN,
        ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ ELCADIA_INSTANCE ]
    }

    getTalkIds(): Array<number> {
        return [
            ELCADIA,
            ELCADIA_INSTANCE,
            ERIS_EVIL_THOUGHTS,
            RELIC_GUARDIAN,
            ODD_GLOBE,
            READING_DESK1,
            READING_DESK2,
            READING_DESK3,
            READING_DESK4,
            READING_DESK5,
            READING_DESK6,
            READING_DESK7,
            READING_DESK8,
            READING_DESK9,
            READING_DESK10,
            READING_DESK11,
            READING_DESK12,
            READING_DESK13,
            READING_DESK14,
            READING_DESK15,
            READING_DESK16,
            JUDE_VAN_ETINA,
            SOLINAS_EVIL_THOUGHTS,
            RELIC_WATCHER1,
            RELIC_WATCHER2,
            RELIC_WATCHER3,
            RELIC_WATCHER4,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10294_SevenSignsToTheMonasteryOfSilence'
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( '32787.html' )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( data.eventName === eventNames.spawnMobs ) {
            QuestHelper.addGenericSpawn( null, JUDE_EVIL_THOUGHTS, 88655, -250591, -8320, 144, false, 0, false, player.getInstanceId() )
            QuestHelper.addGenericSpawn( null, GUIDE_SOLINA, 88655, -250591, -8320, 144, false, 0, false, player.getInstanceId() )
            QuestHelper.addGenericSpawn( null, SOLINA_LAY_BROTHER, 88655, -250591, -8320, 144, false, 0, false, player.getInstanceId() )
            QuestHelper.addGenericSpawn( null, SOLINA_LAY_BROTHER, 88655, -250591, -8320, 144, false, 0, false, player.getInstanceId() )

            return
        }

        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case '32784-03.htm':
            case '32784-04.htm':
                break

            case '32784-05.html':
                state.startQuest()
                break

            case '32792-02.html':
                if ( state.isCondition( 1 ) ) {
                    break
                }

                return

            case '32792-03.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '32792-04.html':
            case '32792-05.html':
            case '32792-06.html':
            case '32803-02.html':
            case '32803-03.html':
            case '32822-02.html':
            case '32804-02.html':
            case '32804-04.html':
            case '32804-06.html':
            case '32804-07.html':
            case '32804-08.html':
            case '32804-09.html':
            case '32804-10.html':
            case '32805-02.html':
            case '32805-04.html':
            case '32805-06.html':
            case '32805-07.html':
            case '32805-08.html':
            case '32805-09.html':
            case '32805-10.html':
            case '32806-02.html':
            case '32806-04.html':
            case '32806-06.html':
            case '32806-07.html':
            case '32806-08.html':
            case '32806-09.html':
            case '32806-10.html':
            case '32807-02.html':
            case '32807-04.html':
            case '32807-06.html':
            case '32807-07.html':
            case '32807-08.html':
            case '32807-09.html':
            case '32807-10.html':
                if ( state.isCondition( 2 ) ) {
                    break
                }

                return

            case '32792-08.html':
                if ( state.isCondition( 3 ) ) {
                    await QuestHelper.addExpAndSp( player, 25000000, 2500000 )
                    await state.exitQuest( false, true )

                    break
                }

                return

            case 'BUFF':
                npc.setTarget( player )

                _.each( player.isMageClass() ? mageBuffIds : warriorBuffIds, ( skillId: number ) => {
                    npc.doSimultaneousCast( SkillCache.getSkill( skillId, 1 ) )
                } )

                return

            case 'RIGHT_BOOK1':
                state.setVariable( variableNames.one, true )
                npc.setDisplayEffect( 1 )

                this.startQuestTimer( eventNames.spawnMobs, 22000, data.characterId, data.playerId )
                if ( this.hasAllVariablesSet( state ) ) {
                    await player.showQuestMovie( 25 )
                }

                return this.getPath( '32821-02.html' )


            case 'RIGHT_BOOK2':
                state.setVariable( variableNames.two, true )
                npc.setDisplayEffect( 1 )
                npc.setTarget( player )

                await npc.doCast( SkillCache.getSkill( VAMPIRIC_RAGE, 1 ) )
                if ( this.hasAllVariablesSet( state ) ) {
                    await player.showQuestMovie( 25 )
                }

                return this.getPath( '32821-02.html' )


            case 'RIGHT_BOOK3':
                state.setVariable( variableNames.three, true )

                npc.setDisplayEffect( 1 )
                let jude: L2Npc = QuestHelper.addGenericSpawn( null, JUDE_VAN_ETINA, 85783, -253471, -8320, 65, false, 0, false, player.getInstanceId() )
                jude.setTarget( player )
                await jude.doCast( SkillCache.getSkill( RESIST_HOLY, 1 ) )

                if ( this.hasAllVariablesSet( state ) ) {
                    await player.showQuestMovie( 25 )
                }

                return this.getPath( '32821-02.html' )

            case 'RIGHT_BOOK4':
                state.setVariable( variableNames.four, true )
                npc.setDisplayEffect( 1 )

                let solina: L2Npc = QuestHelper.addGenericSpawn( null, SOLINAS_EVIL_THOUGHTS, 85793, -247581, -8320, 0, false, 0, false, player.getInstanceId() )
                solina.setTarget( player )
                await solina.doCast( SkillCache.getSkill( RESIST_HOLY, 1 ) )

                if ( this.hasAllVariablesSet( state ) ) {
                    await player.showQuestMovie( 25 )
                }

                return this.getPath( '32821-02.html' )

            case 'DONE1':
                return this.getPath( state.getVariable( variableNames.one ) ? '32804-05.html' : '32804-03.html' )

            case 'DONE2':
                return this.getPath( state.getVariable( variableNames.two ) ? '32805-05.html' : '32805-03.html' )

            case 'DONE3':
                return this.getPath( state.getVariable( variableNames.three ) ? '32806-05.html' : '32806-03.html' )

            case 'DONE4':
                return this.getPath( state.getVariable( variableNames.four ) ? '32807-05.html' : '32807-03.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    hasAllVariablesSet( state: QuestState ): boolean {
        return _.every( _.values( variableNames ), ( name: string ): boolean => {
            return state.getVariable( name )
        } )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ELCADIA:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q10293_SevenSignsForbiddenBookOfTheElmoreAdenKingdom' )
                        return this.getPath( canProceed ? '32784-01.htm' : '32784-07.htm' )

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '32784-06.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return this.getPath( '32784-02.html' )
                }

                break

            case ERIS_EVIL_THOUGHTS:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '32792-01.html' )

                    case 2:
                        return this.getPath( '32792-04.html' )

                    case 3:
                        return this.getPath( player.isSubClassActive() ? '32792-09.html' : '32792-07.html' )
                }

                break

            case RELIC_GUARDIAN:
                if ( state.isCondition( 2 ) ) {
                    if ( this.hasAllVariablesSet( state ) ) {
                        state.setConditionWithSound( 3, true )

                        return this.getPath( '32803-04.html' )
                    }

                    return this.getPath( '32803-01.html' )
                }

                if ( state.isCondition( 3 ) ) {
                    return this.getPath( '32803-05.html' )
                }

                break

            case ODD_GLOBE:
                if ( state.getCondition() < 3 ) {
                    return this.getPath( '32815-01.html' )
                }

                break

            case ELCADIA_INSTANCE:
                if ( state.isCondition( 1 ) ) {
                    return this.getPath( '32787-01.html' )
                }

                if ( state.isCondition( 2 ) ) {
                    return this.getPath( '32787-02.html' )
                }

                break

            case READING_DESK2:
            case READING_DESK3:
            case READING_DESK4:
            case READING_DESK6:
            case READING_DESK7:
            case READING_DESK8:
            case READING_DESK10:
            case READING_DESK11:
            case READING_DESK12:
            case READING_DESK14:
            case READING_DESK15:
            case READING_DESK16:
                if ( state.isCondition( 2 ) ) {
                    return this.getPath( '32822-01.html' )
                }

                break

            case READING_DESK1:
                return this.getPath( state.getVariable( variableNames.one ) ? '32821-03.html' : '32821-01.html' )

            case READING_DESK5:
                return this.getPath( state.getVariable( variableNames.two ) ? '32821-03.html' : '32825-01.html' )

            case READING_DESK9:
                return this.getPath( state.getVariable( variableNames.three ) ? '32821-03.html' : '32829-01.html' )

            case READING_DESK13:
                return this.getPath( state.getVariable( variableNames.four ) ? '32821-03.html' : '32833-01.html' )

            case SOLINAS_EVIL_THOUGHTS:
            case JUDE_VAN_ETINA:
            case RELIC_WATCHER1:
            case RELIC_WATCHER2:
            case RELIC_WATCHER3:
            case RELIC_WATCHER4:
                if ( state.isCondition( 2 ) ) {
                    return this.getPath( `${ data.characterNpcId }-01.html` )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}