import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestHelper } from '../helpers/QuestHelper'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

const ARTIUS = 32559
const PLENOS = 32563
const GINBY = 32566
const LELRIKIA = 32567
const COHEMENES = 25634
const YEHAN_KLODEKUS = 25665
const YEHAN_KLANIKUS = 25666
const YEHAN_KLODEKUS_BADGE = 13868
const YEHAN_KLANIKUS_BADGE = 13869
const LICH_CRYSTAL = 13870
const minimumLevel = 75
const instanceExit = new Location( -185057, 242821, 1576, 0, 0 )

export class BirthOfTheSeed extends ListenerLogic {
    constructor() {
        super( 'Q10270_BirthOfTheSeed', 'listeners/tracked-10200/BirthOfTheSeed.ts' )
        this.questId = 10270
        this.questItemIds = [
            YEHAN_KLODEKUS_BADGE,
            YEHAN_KLANIKUS_BADGE,
            LICH_CRYSTAL,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ PLENOS ]
    }

    getTalkIds(): Array<number> {
        return [
            PLENOS,
            GINBY,
            LELRIKIA,
            ARTIUS,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10270_BirthOfTheSeed'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32563-02.htm':
                break

            case '32563-03.htm':
                state.startQuest( false )
                state.setMemoState( 1 )

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                break

            case '32566-02.html':
                if ( state.isMemoState( 4 ) ) {
                    let companionState: QuestState = this.getCompanionState( data.playerId )

                    if ( !companionState
                            || !companionState.isStarted()
                            || ( companionState.isStarted() && ( companionState.getMemoState() < 10 ) ) ) {
                        break
                    }

                    return this.getPath( '32566-03.html' )
                }

                return

            case '32566-04.html':
                if ( state.isMemoState( 4 ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, ItemTypes.Adena ) < 10000 ) {
                        break
                    }

                    await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 10000 )
                    state.setMemoState( 5 )

                    return this.getPath( '32566-05.html' )
                }

                return

            case '32566-06.html':
                if ( state.isMemoState( 5 ) ) {
                    break
                }

                return

            case '32567-02.html':
            case '32567-03.html':
                if ( state.isMemoState( 10 ) ) {
                    break
                }

                return

            case '32567-04.html':
                if ( state.isMemoState( 10 ) ) {
                    state.setMemoState( 11 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            case '32567-05.html':
                if ( state.isMemoState( 11 ) ) {
                    state.setMemoState( 20 )
                    player.setInstanceId( 0 )

                    await player.teleportToLocation( instanceExit, true )
                    break
                }

                return

            case '32559-02.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '32559-08.html':
                if ( state.isMemoState( 3 ) ) {
                    let companionState: QuestState = this.getCompanionState( data.playerId )
                    if ( !companionState || ( companionState.isStarted() && companionState.getMemoState() < 10 ) ) {
                        state.setMemoState( 4 )
                        state.setConditionWithSound( 4, true )

                        break
                    }
                }

                return

            case '32559-10.html':
                if ( state.isMemoState( 3 ) ) {
                    let companionState: QuestState = this.getCompanionState( data.playerId )
                    if ( companionState
                            && ( ( companionState.isStarted() && ( companionState.getMemoState() >= 10 ) ) || companionState.isCompleted() ) ) {
                        state.setMemoState( 4 )
                        state.setConditionWithSound( 4, true )

                        break
                    }
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    getCompanionState( playerId: number ): QuestState {
        return QuestStateCache.getQuestState( playerId, 'Q10272_LightFragment', false )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        await QuestHelper.runStateActionForParty( data.playerId, data.targetId, this.getName(), async ( state: QuestState, player: L2PcInstance ): Promise<void> => {
            if ( !state.isMemoState( 2 ) ) {
                return
            }

            switch ( data.npcId ) {
                case YEHAN_KLODEKUS:
                    if ( !QuestHelper.hasQuestItem( player, YEHAN_KLODEKUS_BADGE ) ) {
                        await QuestHelper.giveSingleItem( player, YEHAN_KLODEKUS_BADGE, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    }

                    return

                case YEHAN_KLANIKUS:
                    if ( !QuestHelper.hasQuestItem( player, YEHAN_KLANIKUS_BADGE ) ) {
                        await QuestHelper.giveSingleItem( player, YEHAN_KLANIKUS_BADGE, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    }

                    return

                case COHEMENES:
                    if ( !QuestHelper.hasQuestItem( player, LICH_CRYSTAL ) ) {
                        await QuestHelper.giveSingleItem( player, LICH_CRYSTAL, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    }

                    return
            }
        } )

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== PLENOS ) {
                    break
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '32563-01.htm' : '32563-04.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case PLENOS:
                        if ( state.isMemoState( 1 ) ) {
                            return this.getPath( '32563-06.html' )
                        }

                        break

                    case GINBY:
                        let memoState = state.getMemoState()

                        if ( memoState < 4 ) {
                            return this.getPath( '32566-07.html' )
                        }

                        if ( memoState === 5 ) {
                            return this.getPath( '32566-06.html' )
                        }

                        if ( memoState >= 10 && memoState < 20 ) {
                            return this.getPath( '32566-08.html' )
                        }

                        if ( memoState === 20 ) {
                            return this.getPath( '32566-09.html' )
                        }

                        break

                    case LELRIKIA:
                        switch ( state.getMemoState() ) {
                            case 10:
                                return this.getPath( '32567-01.html' )

                            case 11:
                                return this.getPath( '32567-06.html' )
                        }

                        break

                    case ARTIUS: {
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '32559-01.html' )

                            case 2:
                                if ( QuestHelper.hasQuestItems( player, YEHAN_KLODEKUS_BADGE, YEHAN_KLANIKUS_BADGE, LICH_CRYSTAL ) ) {
                                    await QuestHelper.takeMultipleItems( player, -1, YEHAN_KLODEKUS_BADGE, YEHAN_KLANIKUS_BADGE, LICH_CRYSTAL )

                                    state.setMemoState( 3 )
                                    state.setConditionWithSound( 3, true )

                                    return this.getPath( '32559-04.html' )
                                }

                                if ( QuestHelper.hasAtLeastOneQuestItem( player, YEHAN_KLODEKUS_BADGE, YEHAN_KLANIKUS_BADGE, LICH_CRYSTAL ) ) {
                                    return this.getPath( '32559-05.html' )
                                }

                                return this.getPath( '32559-06.html' )

                            case 3:
                                let companionState: QuestState = this.getCompanionState( data.playerId )
                                if ( !companionState
                                        || !companionState.isStarted()
                                        || ( companionState.isStarted() && ( companionState.getMemoState() < 10 ) ) ) {
                                    return this.getPath( '32559-07.html' )
                                }

                                return this.getPath( '32559-09.html' )

                            case 20:
                                if ( player.getLevel() >= minimumLevel ) {
                                    await QuestHelper.giveAdena( player, 133590, true )
                                    await QuestHelper.addExpAndSp( player, 625343, 48222 )
                                    await state.exitQuest( false, true )

                                    return this.getPath( '32559-11.html' )
                                }

                                break
                        }
                        break
                    }
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case PLENOS:
                        return this.getPath( '32563-05.html' )

                    case ARTIUS:
                        return this.getPath( '32559-03.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}