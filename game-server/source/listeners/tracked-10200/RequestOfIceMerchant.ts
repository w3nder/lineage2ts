import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const RAFFORTY = 32020
const KIER = 32022
const JINIA = 32760
const minimumLevel = 82
const eventNames = {
    despawn: 'ds',
}

export class RequestOfIceMerchant extends ListenerLogic {
    lastSeenPlayerId: number
    isBusy: boolean = false

    constructor() {
        super( 'Q10283_RequestOfIceMerchant', 'listeners/tracked-10200/RequestOfIceMerchant.ts' )
        this.questId = 10283
    }

    getQuestStartIds(): Array<number> {
        return [ RAFFORTY ]
    }

    getTalkIds(): Array<number> {
        return [ RAFFORTY, KIER, JINIA ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10283_RequestOfIceMerchant'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === eventNames.despawn ) {
            this.isBusy = false
            this.lastSeenPlayerId = 0

            await ( L2World.getObjectById( data.characterId ) as L2Npc ).deleteMe()

            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32020-03.htm':
                break

            case '32020-04.htm':
                state.startQuest()
                break

            case '32020-05.html':
            case '32020-06.html':

                break

            case '32020-07.html':
                state.setConditionWithSound( 2 )
                break

            case '32022-02.html':
                if ( !this.isBusy ) {
                    this.isBusy = true
                    this.lastSeenPlayerId = data.playerId

                    state.setConditionWithSound( 3 )
                    QuestHelper.addGenericSpawn( null, JINIA, 104476, -107535, -3688, 44954, false, 0, false )

                    return
                }

                return this.getPath( this.lastSeenPlayerId === data.playerId ? data.eventName : '32022-03.html' )

            case '32760-02.html':
            case '32760-03.html':
                break

            case '32760-04.html':
                await QuestHelper.giveAdena( player, 190000, true )
                await QuestHelper.addExpAndSp( player, 627000, 50300 )
                await state.exitQuest( false, true )

                this.startQuestTimer( eventNames.despawn, 2000, data.characterId, 0 )
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== RAFFORTY ) {
                    break
                }

                let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00115_TheOtherSideOfTruth' )
                return this.getPath( canProceed ? '32020-01.htm' : '32020-08.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case RAFFORTY:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '32020-09.html' )
                        }

                        return this.getPath( '32020-10.html' )

                    case KIER:
                        if ( state.isCondition( 1 ) ) {
                            break
                        }

                        return this.getPath( '32022-01.html' )

                    case JINIA: {
                        if ( state.isCondition( 1 ) ) {
                            break
                        }

                        return this.getPath( this.lastSeenPlayerId === data.playerId ? '32760-01.html' : '32760-05.html' )
                    }
                }

                break

            case QuestStateValues.COMPLETED:
                switch ( data.characterNpcId ) {
                    case RAFFORTY:
                        return this.getPath( '32020-02.html' )

                    case JINIA:
                        return this.getPath( '32760-06.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}