import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { NpcApproachedForTalkEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const DOMINIC = 31350
const AQUILANI = 32780
const GREYMORE = 32757
const LETTER = 15529
const TELEPORT = new Location( 118833, -80589, -2688 )
const minimumLevel = 82

export class SecretMission extends ListenerLogic {
    constructor() {
        super( 'Q10288_SecretMission', 'listeners/tracked-10200/SecretMission.ts' )
        this.questId = 10288
        this.questItemIds = [ LETTER ]
    }

    getQuestStartIds(): Array<number> {
        return [ AQUILANI, DOMINIC ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ AQUILANI ]
    }

    getTalkIds(): Array<number> {
        return [ DOMINIC, GREYMORE, AQUILANI ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q10288_SecretMission'
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '31350-03.html':
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31350-02b.html' )
                }

                break

            case '31350-05.htm':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, LETTER, 1 )
                break

            case '32780-03.html':
                if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, LETTER ) ) {
                    state.setConditionWithSound( 2, true )
                }

                break

            case '32757-03.html':
                if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, LETTER ) ) {
                    await QuestHelper.giveAdena( player, 106583, true )
                    await QuestHelper.addExpAndSp( player, 417788, 46320 )
                    await state.exitQuest( false, true )
                }

                break

            case 'teleport':
                if ( data.characterNpcId === AQUILANI && state.isCompleted() ) {
                    await player.teleportToLocation( TELEPORT )

                    return
                }

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCompleted() ) {
            return 'data/html/default/32780.htm'
        }

        return this.getPath( '32780-05.html' )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== DOMINIC ) {
                    break
                }

                return this.getPath( '31350-01.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case DOMINIC:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '31350-06.html' )
                        }

                        break

                    case AQUILANI:
                        if ( state.isCondition( 1 ) && QuestHelper.hasQuestItem( player, LETTER ) ) {
                            return this.getPath( '32780-01.html' )
                        }

                        if ( state.isCondition( 2 ) ) {
                            return this.getPath( '32780-04.html' )
                        }

                        break
                    case GREYMORE:
                        if ( state.isCondition( 2 ) && QuestHelper.hasQuestItem( player, LETTER ) ) {
                            return this.getPath( '32757-01.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId !== DOMINIC ) {
                    break
                }

                return this.getPath( '31350-07.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}