import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const KUNAI = 30559
const CONTAMINATED_KASHA_SPIDER_VENOM = 10869
const minimumLevel = 15

export class TracesOfEvil extends ListenerLogic {
    constructor() {
        super( 'Q00268_TracesOfEvil', 'listeners/tracked-200/TracesOfEvil.ts' )
        this.questId = 268
        this.questItemIds = [ CONTAMINATED_KASHA_SPIDER_VENOM ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20474, // Kasha Spider
            20476, // Kasha Fang Spider
            20478, // Kasha Blade Spider
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00268_TracesOfEvil'
    }

    getQuestStartIds(): Array<number> {
        return [ KUNAI ]
    }

    getTalkIds(): Array<number> {
        return [ KUNAI ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, CONTAMINATED_KASHA_SPIDER_VENOM, 1, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, CONTAMINATED_KASHA_SPIDER_VENOM ) >= 30 ) {
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '31852-04.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30559-02.htm' : '30559-01.htm' )

            case QuestStateValues.STARTED: {
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( QuestHelper.hasQuestItem( player, CONTAMINATED_KASHA_SPIDER_VENOM ) ? '30559-05.html' : '30559-04.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, CONTAMINATED_KASHA_SPIDER_VENOM ) < 30 ) {
                            return this.getPath( '30559-05.html' )
                        }

                        await QuestHelper.giveAdena( player, 2474, true )
                        await QuestHelper.addExpAndSp( player, 8738, 409 )
                        await state.exitQuest( true, true )

                        return this.getPath( '30559-06.html' )
                }

                break
            }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}