import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { ILocational } from '../../gameService/models/Location'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SkillCache } from '../../gameService/cache/SkillCache'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import _ from 'lodash'

const JOAN = 30718
const LADD = 30721
const ALCHEMISTS_MIXING_URN = 31149

const STAR_OF_DESTINY = 5011
const MAGISTERS_MIXING_STONE = 5905
const BLOOD_FIRE = 6318
const MIMIRS_ELIXIR = 6319
const PURE_SILVER = 6320
const TRUE_GOLD = 6321
const SAGES_STONE = 6322

const ENCHANT_WEAPON_A = 729

const MIN_LEVEL = 75
const QUEST_MIMIRS_ELIXIR = 4339

const mobIdMap = {
    20965: [ SAGES_STONE, 4, 1 ], // chimera_piece
    21090: [ BLOOD_FIRE, 7, 1 ], // bloody_guardian
}

export class MimirsElixir extends ListenerLogic {
    constructor() {
        super( 'Q00235_MimirsElixir', 'listeners/tracked-200/MimirsElixir.ts' )
        this.questId = 235
        this.questItemIds = [
            MAGISTERS_MIXING_STONE,
            BLOOD_FIRE,
            MIMIRS_ELIXIR,
            TRUE_GOLD,
            SAGES_STONE,
        ]
    }

    checkPartyMemberForLocation( location: ILocational, playerId: number ): boolean {
        let state: QuestState = QuestStateCache.getQuestState( playerId, this.getName() )
        return state && [ 3, 6 ].includes( state.getMemoState() )
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( mobIdMap ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00235_MimirsElixir'
    }

    getQuestStartIds(): Array<number> {
        return [ LADD ]
    }

    getTalkIds(): Array<number> {
        return [ LADD, JOAN, ALCHEMISTS_MIXING_URN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( _.random( 4 ) !== 0 ) {
            return
        }

        let player: L2PcInstance = this.getRandomPartyMemberNearLocation( L2World.getPlayer( data.playerId ), L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        let [ itemId, stateValue, amount ] = mobIdMap[ data.npcId ]
        await QuestHelper.giveSingleItem( player, itemId, amount )

        let state = this.getQuestState( player.getObjectId() )
        state.setMemoState( stateValue )
        state.setConditionWithSound( stateValue )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30721-02.htm':
            case '30721-03.htm':
            case '30721-04.htm':
            case '30721-05.htm':
                break

            case '30721-06.htm':
                state.setMemoState( 1 )
                state.startQuest()
                break

            case '30721-12.html':
                if ( state.isMemoState( 1 ) ) {
                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2 )

                    break
                }

                return

            case '30721-15.html':
                if ( state.isMemoState( 5 ) ) {
                    await QuestHelper.giveSingleItem( player, MAGISTERS_MIXING_STONE, 1 )

                    state.setMemoState( 6 )
                    state.setConditionWithSound( 6 )

                    break
                }

                return

            case '30721-18.html':
                if ( state.isMemoState( 8 ) ) {
                    break
                }

                return

            case '30721-19.html':
                if ( state.isMemoState( 8 ) && QuestHelper.hasQuestItems( player, MAGISTERS_MIXING_STONE, MIMIRS_ELIXIR ) ) {
                    let npc = L2World.getObjectById( data.characterId ) as L2Npc
                    npc.setTarget( player )

                    await npc.doCast( SkillCache.getSkill( QUEST_MIMIRS_ELIXIR, 1 ) )
                    await QuestHelper.takeSingleItem( player, STAR_OF_DESTINY, -1 )
                    await QuestHelper.rewardSingleItem( player, ENCHANT_WEAPON_A, 1 )
                    await state.exitQuest( false, true )

                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                    break
                }

                return

            case '30718-02.html':
                if ( state.isMemoState( 2 ) ) {
                    break
                }

                return

            case '30718-03.html':
                if ( state.isMemoState( 2 ) ) {
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return

            case '30718-06.html':
                if ( state.isMemoState( 4 ) && QuestHelper.hasQuestItem( player, SAGES_STONE ) ) {
                    await QuestHelper.giveSingleItem( player, TRUE_GOLD, 1 )
                    await QuestHelper.takeSingleItem( player, SAGES_STONE, -1 )

                    state.setMemoState( 5 )
                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '31149-02.html':
            case '31149-05.html':
            case '31149-07.html':
            case '31149-09.html':
            case '31149-10.html':
                if ( state.isMemoState( 7 ) ) {
                    break
                }

                return

            case 'PURE_SILVER':
                if ( state.isMemoState( 7 ) ) {
                    return this.getPath( QuestHelper.hasQuestItem( player, PURE_SILVER ) ? '31149-04.html' : '31149-03.html' )
                }

                return

            case 'TRUE_GOLD':
                if ( state.isMemoState( 7 ) ) {
                    return this.getPath( QuestHelper.hasQuestItem( player, TRUE_GOLD ) ? '31149-06.html' : '31149-03.html' )
                }

                return

            case 'BLOOD_FIRE':
                if ( state.isMemoState( 7 ) ) {
                    return this.getPath( QuestHelper.hasQuestItem( player, BLOOD_FIRE ) ? '31149-08.html' : '31149-03.html' )
                }

                return

            case '31149-11.html':
                if ( state.isMemoState( 7 ) && QuestHelper.hasQuestItems( player, BLOOD_FIRE, PURE_SILVER, TRUE_GOLD ) ) {
                    await QuestHelper.giveSingleItem( player, MIMIRS_ELIXIR, 1 )
                    await QuestHelper.takeMultipleItems( player, -1, BLOOD_FIRE, PURE_SILVER, TRUE_GOLD )

                    state.setMemoState( 8 )
                    state.setConditionWithSound( 8, true )

                    break
                }
                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== LADD ) {
                    break
                }

                if ( player.getRace() === Race.KAMAEL ) {
                    return this.getPath( '30721-09.html' )
                }

                if ( player.getLevel() < MIN_LEVEL ) {
                    return this.getPath( '30721-08.html' )
                }

                return this.getPath( QuestHelper.hasQuestItem( player, STAR_OF_DESTINY ) ? '30721-01.htm' : '30721-07.html' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case LADD:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( QuestHelper.hasQuestItems( player, PURE_SILVER ) ? '30721-11.html' : '30721-10.html' )

                            case 2:
                            case 3:
                            case 4:
                                return this.getPath( '30721-13.html' )

                            case 5:
                                return this.getPath( '30721-14.html' )

                            case 6:
                            case 7:
                                return this.getPath( '30721-16.html' )

                            case 8:
                                return this.getPath( '30721-17.html' )
                        }

                        break

                    case JOAN:
                        switch ( state.getMemoState() ) {
                            case 2:
                                return this.getPath( '30718-01.html' )

                            case 3:
                                return this.getPath( '30718-04.html' )

                            case 4:
                                return this.getPath( '30718-05.html' )
                        }

                        break

                    case ALCHEMISTS_MIXING_URN:
                        if ( state.isMemoState( 7 ) && QuestHelper.hasQuestItem( player, MAGISTERS_MIXING_STONE ) ) {
                            return this.getPath( '31149-01.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === LADD ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}