import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const TAMIL = 30576
const GREYSTONE_GOLEM = 20333
const STARSTONE = 1572
const GATEKEEPER_CHARM = 1658
const minimumLevel = 15
const STARSTONE_COUT = 20

export class GatekeepersOffering extends ListenerLogic {
    constructor() {
        super( 'Q00277_GatekeepersOffering', 'listeners/tracked-200/GatekeepersOffering.ts' )
        this.questId = 277
        this.questItemIds = [
            STARSTONE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ GREYSTONE_GOLEM ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00277_GatekeepersOffering'
    }

    getQuestStartIds(): Array<number> {
        return [ TAMIL ]
    }

    getTalkIds(): Array<number> {
        return [ TAMIL ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( QuestHelper.getQuestItemsCount( player, STARSTONE ) >= STARSTONE_COUT ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, STARSTONE, 1, data.isChampion )
        if ( QuestHelper.getQuestItemsCount( player, STARSTONE ) >= STARSTONE_COUT ) {
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30576-03.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30576-01.htm' )
                }

                return this.getPath( '30576-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30576-04.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, STARSTONE ) < STARSTONE_COUT ) {
                            return this.getPath( '30576-04.html' )
                        }

                        await QuestHelper.rewardSingleItem( player, GATEKEEPER_CHARM, 2 )
                        await state.exitQuest( true, true )

                        return this.getPath( '30576-05.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}