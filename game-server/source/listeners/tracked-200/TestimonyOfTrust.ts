import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import _ from 'lodash'

const HIGH_PRIEST_BIOTIN = 30031
const HIERARCH_ASTERIOS = 30154
const HIGH_PRIEST_HOLLINT = 30191
const TETRARCH_THIFIELL = 30358
const MAGISTER_CLAYTON = 30464
const SEER_MANAKIA = 30515
const IRON_GATES_LOCKIRIN = 30531
const FLAME_LORD_KAKAI = 30565
const MAESTRO_NIKOLA = 30621
const CARDINAL_SERESIN = 30657

const LETTER_TO_ELF = 2735
const LETTER_TO_DARKELF = 2736
const LETTER_TO_DWARF = 2737
const LETTER_TO_ORC = 2738
const LETTER_TO_SERESIN = 2739
const SCROLL_OF_DARKELF_TRUST = 2740
const SCROLL_OF_ELF_TRUST = 2741
const SCROLL_OF_DWARF_TRUST = 2742
const SCROLL_OF_ORC_TRUST = 2743
const RECOMMENDATION_OF_HOLLIN = 2744
const ORDER_OF_ASTERIOS = 2745
const BREATH_OF_WINDS = 2746
const SEED_OF_VERDURE = 2747
const LETTER_OF_THIFIELL = 2748
const BLOOD_OF_GUARDIAN_BASILISK = 2749
const GIANT_APHID = 2750
const STAKATOS_FLUIDS = 2751
const BASILISK_PLASMA = 2752
const HONEY_DEW = 2753
const STAKATO_ICHOR = 2754
const ORDER_OF_CLAYTON = 2755
const PARASITE_OF_LOTA = 2756
const LETTER_TO_MANAKIA = 2757
const LETTER_OF_MANAKIA = 2758
const LETTER_TO_NICHOLA = 2759
const ORDER_OF_NICHOLA = 2760
const HEART_OF_PORTA = 2761

const MARK_OF_TRUST = 2734
const DIMENSIONAL_DIAMOND = 7562

const DRYAD = 20013
const DRYAD_ELDER = 20019
const LIREIN = 20036
const LIREIN_ELDER = 20044
const ANT_RECRUIT = 20082
const ANT_PATROL = 20084
const ANT_GUARD = 20086
const ANT_SOLDIER = 20087
const ANT_WARRIOR_CAPTAIN = 20088
const MARSH_STAKATO = 20157
const PORTA = 20213
const MARSH_STAKATO_WORKER = 20230
const MARSH_STAKATO_SOLDIER = 20232
const MARSH_STAKATO_DRONE = 20234
const GUARDIAN_BASILISK = 20550
const WINDSUS = 20553

const LUELL_OF_ZEPHYR_WINDS = 27120
const ACTEA_OF_VERDANT_WILDS = 27121

const minimumLevel = 37
const variableNames = {
    chance: 'c',
}

export class TestimonyOfTrust extends ListenerLogic {
    constructor() {
        super( 'Q00217_TestimonyOfTrust', 'listeners/tracked-200/TestimonyOfTrust.ts' )
        this.questId = 217
        this.questItemIds = [
            LETTER_TO_ELF,
            LETTER_TO_DARKELF,
            LETTER_TO_DWARF,
            LETTER_TO_ORC,
            LETTER_TO_SERESIN,
            SCROLL_OF_DARKELF_TRUST,
            SCROLL_OF_ELF_TRUST,
            SCROLL_OF_DWARF_TRUST,
            SCROLL_OF_ORC_TRUST,
            RECOMMENDATION_OF_HOLLIN,
            ORDER_OF_ASTERIOS,
            BREATH_OF_WINDS,
            SEED_OF_VERDURE,
            LETTER_OF_THIFIELL,
            BLOOD_OF_GUARDIAN_BASILISK,
            GIANT_APHID,
            STAKATOS_FLUIDS,
            BASILISK_PLASMA,
            HONEY_DEW,
            STAKATO_ICHOR,
            ORDER_OF_CLAYTON,
            PARASITE_OF_LOTA,
            LETTER_TO_MANAKIA,
            LETTER_OF_MANAKIA,
            LETTER_TO_NICHOLA,
            ORDER_OF_NICHOLA,
            HEART_OF_PORTA,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            DRYAD,
            DRYAD_ELDER,
            LIREIN,
            LIREIN_ELDER,
            ANT_RECRUIT,
            ANT_PATROL,
            ANT_GUARD,
            ANT_SOLDIER,
            ANT_WARRIOR_CAPTAIN,
            MARSH_STAKATO,
            PORTA,
            MARSH_STAKATO_WORKER,
            MARSH_STAKATO_SOLDIER,
            MARSH_STAKATO_DRONE,
            GUARDIAN_BASILISK,
            WINDSUS,
            LUELL_OF_ZEPHYR_WINDS,
            ACTEA_OF_VERDANT_WILDS,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00217_TestimonyOfTrust'
    }

    getQuestStartIds(): Array<number> {
        return [ HIGH_PRIEST_HOLLINT ]
    }

    getTalkIds(): Array<number> {
        return [
            HIGH_PRIEST_HOLLINT,
            HIGH_PRIEST_BIOTIN,
            HIERARCH_ASTERIOS,
            TETRARCH_THIFIELL,
            MAGISTER_CLAYTON,
            SEER_MANAKIA,
            IRON_GATES_LOCKIRIN,
            FLAME_LORD_KAKAI,
            MAESTRO_NIKOLA,
            CARDINAL_SERESIN,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case DRYAD:
            case DRYAD_ELDER:
                if ( state.isMemoState( 2 ) ) {
                    let chanceValue = state.getVariable( variableNames.chance )
                    if ( _.random( 100 ) < ( chanceValue * 33 ) ) {
                        QuestHelper.addSpawnAtLocation( ACTEA_OF_VERDANT_WILDS, npc, true, 200000 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_BEFORE_BATTLE )
                    }
                }

                return

            case LIREIN:
            case LIREIN_ELDER:
                if ( state.isMemoState( 2 ) ) {
                    let chanceValue = state.getVariable( variableNames.chance )

                    if ( _.random( 100 ) < ( chanceValue * 33 ) ) {
                        QuestHelper.addSpawnAtLocation( LUELL_OF_ZEPHYR_WINDS, npc, true, 200000 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_BEFORE_BATTLE )
                    }
                }

                return

            case ANT_RECRUIT:
            case ANT_GUARD:
                if ( state.isMemoState( 6 )
                        && QuestHelper.getQuestItemsCount( player, GIANT_APHID ) < 5
                        && QuestHelper.hasQuestItem( player, ORDER_OF_CLAYTON )
                        && !QuestHelper.hasQuestItem( player, HONEY_DEW ) ) {

                    if ( QuestHelper.getQuestItemsCount( player, GIANT_APHID ) >= 4 ) {
                        await QuestHelper.rewardSingleQuestItem( player, HONEY_DEW, 1, data.isChampion )
                        await QuestHelper.takeSingleItem( player, GIANT_APHID, -1 )

                        if ( QuestHelper.hasQuestItems( player, BASILISK_PLASMA, STAKATO_ICHOR ) ) {
                            state.setConditionWithSound( 7 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        return
                    }

                    await QuestHelper.rewardSingleQuestItem( player, GIANT_APHID, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case ANT_PATROL:
            case ANT_SOLDIER:
            case ANT_WARRIOR_CAPTAIN:
                if ( state.isMemoState( 6 )
                        && ( QuestHelper.getQuestItemsCount( player, GIANT_APHID ) < 10 )
                        && QuestHelper.hasQuestItem( player, ORDER_OF_CLAYTON )
                        && !QuestHelper.hasQuestItem( player, HONEY_DEW ) ) {

                    if ( QuestHelper.getQuestItemsCount( player, GIANT_APHID ) >= 4 ) {
                        await QuestHelper.rewardSingleQuestItem( player, HONEY_DEW, 1, data.isChampion )
                        await QuestHelper.takeSingleItem( player, GIANT_APHID, -1 )

                        if ( QuestHelper.hasQuestItems( player, BASILISK_PLASMA, STAKATO_ICHOR ) ) {
                            state.setConditionWithSound( 7 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        return
                    }

                    await QuestHelper.rewardSingleQuestItem( player, GIANT_APHID, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case MARSH_STAKATO:
            case MARSH_STAKATO_WORKER:
                if ( state.isMemoState( 6 )
                        && QuestHelper.getQuestItemsCount( player, STAKATOS_FLUIDS ) < 10
                        && QuestHelper.hasQuestItem( player, ORDER_OF_CLAYTON )
                        && !QuestHelper.hasQuestItem( player, STAKATO_ICHOR ) ) {

                    if ( QuestHelper.getQuestItemsCount( player, STAKATOS_FLUIDS ) >= 4 ) {
                        await QuestHelper.rewardSingleQuestItem( player, STAKATO_ICHOR, 1, data.isChampion )
                        await QuestHelper.takeSingleItem( player, STAKATOS_FLUIDS, -1 )

                        if ( QuestHelper.hasQuestItems( player, BASILISK_PLASMA, HONEY_DEW ) ) {
                            state.setConditionWithSound( 7 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        return
                    }

                    await QuestHelper.rewardSingleQuestItem( player, STAKATOS_FLUIDS, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case MARSH_STAKATO_SOLDIER:
            case MARSH_STAKATO_DRONE:
                if ( state.isMemoState( 6 )
                        && QuestHelper.getQuestItemsCount( player, STAKATOS_FLUIDS ) < 5
                        && QuestHelper.hasQuestItem( player, ORDER_OF_CLAYTON )
                        && !QuestHelper.hasQuestItem( player, STAKATO_ICHOR ) ) {

                    if ( QuestHelper.getQuestItemsCount( player, STAKATOS_FLUIDS ) >= 4 ) {
                        await QuestHelper.rewardSingleQuestItem( player, STAKATO_ICHOR, 1, data.isChampion )
                        await QuestHelper.takeSingleItem( player, STAKATOS_FLUIDS, -1 )

                        if ( QuestHelper.hasQuestItems( player, BASILISK_PLASMA, HONEY_DEW ) ) {
                            state.setConditionWithSound( 7 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        return
                    }

                    await QuestHelper.rewardSingleQuestItem( player, STAKATOS_FLUIDS, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case PORTA:
                if ( state.isMemoState( 16 ) && !QuestHelper.hasQuestItem( player, HEART_OF_PORTA ) ) {
                    await QuestHelper.giveSingleItem( player, HEART_OF_PORTA, 1 )
                    state.setConditionWithSound( 20, true )
                }

                return

            case GUARDIAN_BASILISK:
                if ( state.isMemoState( 6 )
                        && QuestHelper.getQuestItemsCount( player, BLOOD_OF_GUARDIAN_BASILISK ) < 10
                        && QuestHelper.hasQuestItem( player, ORDER_OF_CLAYTON )
                        && !QuestHelper.hasQuestItem( player, BASILISK_PLASMA ) ) {

                    if ( QuestHelper.getQuestItemsCount( player, BLOOD_OF_GUARDIAN_BASILISK ) >= 4 ) {
                        await QuestHelper.rewardSingleQuestItem( player, BASILISK_PLASMA, 1, data.isChampion )
                        await QuestHelper.takeSingleItem( player, BLOOD_OF_GUARDIAN_BASILISK, -1 )

                        if ( QuestHelper.hasQuestItems( player, STAKATO_ICHOR, HONEY_DEW ) ) {
                            state.setConditionWithSound( 7 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        return
                    }

                    await QuestHelper.rewardSingleQuestItem( player, BLOOD_OF_GUARDIAN_BASILISK, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case WINDSUS:
                if ( state.isMemoState( 11 ) && QuestHelper.getQuestItemsCount( player, PARASITE_OF_LOTA ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, PARASITE_OF_LOTA, 2, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, PARASITE_OF_LOTA ) >= 10 ) {
                        state.setMemoState( 12 )
                        state.setConditionWithSound( 15, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case LUELL_OF_ZEPHYR_WINDS:
                if ( state.isMemoState( 2 ) && !QuestHelper.hasQuestItem( player, BREATH_OF_WINDS ) ) {
                    await QuestHelper.giveSingleItem( player, BREATH_OF_WINDS, 1 )

                    if ( QuestHelper.hasQuestItem( player, SEED_OF_VERDURE ) ) {
                        state.setMemoState( 3 )
                        state.setConditionWithSound( 3, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case ACTEA_OF_VERDANT_WILDS:
                if ( state.isMemoState( 2 ) && !QuestHelper.hasQuestItem( player, SEED_OF_VERDURE ) ) {
                    if ( QuestHelper.hasQuestItem( player, BREATH_OF_WINDS ) ) {
                        await QuestHelper.giveSingleItem( player, SEED_OF_VERDURE, 1 )

                        state.setMemoState( 3 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        return
                    }

                    await QuestHelper.giveSingleItem( player, SEED_OF_VERDURE, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveSingleItem( player, LETTER_TO_ELF, 1 )
                    await QuestHelper.giveSingleItem( player, LETTER_TO_DARKELF, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 96 )

                        return this.getPath( '30191-04a.htm' )
                    }

                    return this.getPath( '30191-04.htm' )
                }

                return

            case '30154-02.html':
            case '30657-02.html':
                break

            case '30154-03.html':
                if ( QuestHelper.hasQuestItem( player, LETTER_TO_ELF ) ) {
                    await QuestHelper.takeSingleItem( player, LETTER_TO_ELF, 1 )
                    await QuestHelper.giveSingleItem( player, ORDER_OF_ASTERIOS, 1 )

                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )

                    break
                }

                return

            case '30358-02.html':
                if ( QuestHelper.hasQuestItem( player, LETTER_TO_DARKELF ) ) {
                    await QuestHelper.takeSingleItem( player, LETTER_TO_DARKELF, 1 )
                    await QuestHelper.giveSingleItem( player, LETTER_OF_THIFIELL, 1 )

                    state.setMemoState( 5 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            case '30515-02.html':
                if ( QuestHelper.hasQuestItem( player, LETTER_TO_MANAKIA ) ) {
                    await QuestHelper.takeSingleItem( player, LETTER_TO_MANAKIA, 1 )

                    state.setMemoState( 11 )
                    state.setConditionWithSound( 14, true )

                    break
                }

                return

            case '30531-02.html':
                if ( QuestHelper.hasQuestItem( player, LETTER_TO_DWARF ) ) {
                    await QuestHelper.takeSingleItem( player, LETTER_TO_DWARF, 1 )
                    await QuestHelper.giveSingleItem( player, LETTER_TO_NICHOLA, 1 )

                    state.setMemoState( 15 )
                    state.setConditionWithSound( 18, true )

                    break
                }

                return

            case '30565-02.html':
                if ( QuestHelper.hasQuestItem( player, LETTER_TO_ORC ) ) {
                    await QuestHelper.takeSingleItem( player, LETTER_TO_ORC, 1 )
                    await QuestHelper.giveSingleItem( player, LETTER_TO_MANAKIA, 1 )

                    state.setMemoState( 10 )
                    state.setConditionWithSound( 13, true )

                    break
                }

                return

            case '30621-02.html':
                if ( QuestHelper.hasQuestItem( player, LETTER_TO_NICHOLA ) ) {
                    await QuestHelper.takeSingleItem( player, LETTER_TO_NICHOLA, 1 )
                    await QuestHelper.giveSingleItem( player, ORDER_OF_NICHOLA, 1 )

                    state.setMemoState( 16 )
                    state.setConditionWithSound( 19, true )

                    break
                }

                return

            case '30657-03.html':
                if ( state.isMemoState( 8 ) && QuestHelper.hasQuestItem( player, LETTER_TO_SERESIN ) ) {
                    await QuestHelper.giveMultipleItems( player, 1, LETTER_TO_DWARF, LETTER_TO_ORC )
                    await QuestHelper.takeSingleItem( player, LETTER_TO_SERESIN, 1 )

                    state.setMemoState( 9 )
                    state.setConditionWithSound( 12, true )

                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === HIGH_PRIEST_HOLLINT ) {
                    if ( player.getRace() !== Race.HUMAN ) {
                        return this.getPath( '30191-02.html' )
                    }

                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30191-01.html' )
                    }

                    if ( player.isInCategory( CategoryType.HUMAN_2ND_GROUP ) ) {
                        return this.getPath( '30191-03.htm' )
                    }

                    if ( player.isInCategory( CategoryType.FIRST_CLASS_GROUP ) ) {
                        return this.getPath( '30191-01a.html' )
                    }
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case HIGH_PRIEST_HOLLINT:
                        switch ( memoState ) {
                            case 1:
                                return this.getPath( '30191-08.html' )

                            case 7:
                                if ( QuestHelper.hasQuestItems( player, SCROLL_OF_ELF_TRUST, SCROLL_OF_DARKELF_TRUST ) ) {
                                    await QuestHelper.giveSingleItem( player, LETTER_TO_SERESIN, 1 )
                                    await QuestHelper.takeMultipleItems( player, 1, SCROLL_OF_DARKELF_TRUST, SCROLL_OF_ELF_TRUST )

                                    state.setMemoState( 8 )
                                    state.setConditionWithSound( 10, true )

                                    return this.getPath( '30191-05.html' )
                                }

                                break

                            case 8:
                                return this.getPath( '30191-09.html' )

                            case 18:
                                if ( QuestHelper.hasQuestItems( player, SCROLL_OF_DWARF_TRUST, SCROLL_OF_ORC_TRUST ) ) {
                                    await QuestHelper.takeMultipleItems( player, 1, SCROLL_OF_DWARF_TRUST, SCROLL_OF_ORC_TRUST )
                                    await QuestHelper.giveSingleItem( player, RECOMMENDATION_OF_HOLLIN, 1 )

                                    state.setMemoState( 19 )
                                    state.setConditionWithSound( 23, true )

                                    return this.getPath( '30191-06.html' )
                                }

                                break

                            case 19:
                                return this.getPath( '30191-07.html' )
                        }

                        break

                    case HIGH_PRIEST_BIOTIN:
                        if ( memoState === 19 && QuestHelper.hasQuestItem( player, RECOMMENDATION_OF_HOLLIN ) ) {
                            await QuestHelper.giveAdena( player, 252212, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_TRUST, 1 )
                            await QuestHelper.addExpAndSp( player, 1390298, 92782 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                            return this.getPath( '30031-01.html' )
                        }

                        break

                    case HIERARCH_ASTERIOS:
                        switch ( memoState ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, LETTER_TO_ELF ) ) {
                                    return this.getPath( '30154-01.html' )
                                }

                                break

                            case 2:
                                if ( QuestHelper.hasQuestItem( player, ORDER_OF_ASTERIOS ) ) {
                                    return this.getPath( '30154-04.html' )
                                }

                                break

                            case 3:
                                if ( QuestHelper.hasQuestItems( player, BREATH_OF_WINDS, SEED_OF_VERDURE ) ) {
                                    await QuestHelper.giveSingleItem( player, SCROLL_OF_ELF_TRUST, 1 )
                                    await QuestHelper.takeMultipleItems( player, 1, ORDER_OF_ASTERIOS, BREATH_OF_WINDS, SEED_OF_VERDURE )

                                    state.setMemoState( 4 )
                                    state.setConditionWithSound( 4, true )
                                    return this.getPath( '30154-05.html' )
                                }

                                break

                            case 4:
                                return this.getPath( '30154-06.html' )
                        }

                        break

                    case TETRARCH_THIFIELL:
                        switch ( memoState ) {
                            case 4:
                                if ( QuestHelper.hasQuestItem( player, LETTER_TO_DARKELF ) ) {
                                    return this.getPath( '30358-01.html' )
                                }

                                break

                            case 5:
                                return this.getPath( '30358-05.html' )

                            case 6:
                                let totalCount = QuestHelper.getQuestItemsCount( player, STAKATO_ICHOR ) + QuestHelper.getQuestItemsCount( player, HONEY_DEW ) + QuestHelper.getQuestItemsCount( player, BASILISK_PLASMA )
                                if ( QuestHelper.hasQuestItem( player, ORDER_OF_CLAYTON )
                                        && totalCount >= 3 ) {
                                    await QuestHelper.giveSingleItem( player, SCROLL_OF_DARKELF_TRUST, 1 )
                                    await QuestHelper.takeMultipleItems( player, -1, BASILISK_PLASMA, HONEY_DEW, STAKATO_ICHOR, ORDER_OF_CLAYTON )

                                    state.setMemoState( 7 )
                                    state.setConditionWithSound( 9, true )
                                    return this.getPath( '30358-03.html' )
                                }

                                break

                            case 7:
                                return this.getPath( '30358-04.html' )
                        }

                        break

                    case MAGISTER_CLAYTON:
                        switch ( memoState ) {
                            case 5:
                                if ( QuestHelper.hasQuestItem( player, LETTER_OF_THIFIELL ) ) {
                                    await QuestHelper.takeSingleItem( player, LETTER_OF_THIFIELL, 1 )
                                    await QuestHelper.giveSingleItem( player, ORDER_OF_CLAYTON, 1 )

                                    state.setMemoState( 6 )
                                    state.setConditionWithSound( 6, true )

                                    return this.getPath( '30464-01.html' )
                                }

                                break

                            case 6:
                                let totalCount = QuestHelper.getQuestItemsCount( player, STAKATO_ICHOR ) +
                                        QuestHelper.getQuestItemsCount( player, HONEY_DEW ) +
                                        QuestHelper.getQuestItemsCount( player, BASILISK_PLASMA )
                                if ( QuestHelper.hasQuestItem( player, ORDER_OF_CLAYTON ) && totalCount < 3 ) {
                                    return this.getPath( '30464-02.html' )
                                }

                                state.setConditionWithSound( 8, true )
                                return this.getPath( '30464-03.html' )
                        }

                        break

                    case SEER_MANAKIA:
                        if ( QuestHelper.hasQuestItem( player, LETTER_TO_MANAKIA ) ) {
                            return this.getPath( '30515-01.html' )
                        }

                        switch ( memoState ) {
                            case 11:
                                return this.getPath( '30515-03.html' )

                            case 12:
                                if ( QuestHelper.getQuestItemsCount( player, PARASITE_OF_LOTA ) == 10 ) {
                                    await QuestHelper.takeSingleItem( player, PARASITE_OF_LOTA, -1 )
                                    await QuestHelper.giveSingleItem( player, LETTER_OF_MANAKIA, 1 )

                                    state.setMemoState( 13 )
                                    state.setConditionWithSound( 16, true )

                                    return this.getPath( '30515-04.html' )
                                }

                                break

                            case 13:
                                return this.getPath( '30515-05.html' )
                        }

                        break

                    case IRON_GATES_LOCKIRIN:
                        switch ( memoState ) {
                            case 14:
                                if ( QuestHelper.hasQuestItem( player, LETTER_TO_DWARF ) ) {
                                    return this.getPath( '30531-01.html' )
                                }

                                break

                            case 15:
                                return this.getPath( '30531-03.html' )

                            case 17:
                                await QuestHelper.giveSingleItem( player, SCROLL_OF_DWARF_TRUST, 1 )

                                state.setMemoState( 18 )
                                state.setConditionWithSound( 22, true )

                                return this.getPath( '30531-04.html' )

                            case 18:
                                return this.getPath( '30531-05.html' )
                        }

                        break

                    case FLAME_LORD_KAKAI:
                        switch ( memoState ) {
                            case 9:
                                if ( QuestHelper.hasQuestItem( player, LETTER_TO_ORC ) ) {
                                    return this.getPath( '30565-01.html' )
                                }

                                break

                            case 10:
                                return this.getPath( '30565-03.html' )

                            case 13:
                                await QuestHelper.giveSingleItem( player, SCROLL_OF_ORC_TRUST, 1 )
                                await QuestHelper.takeSingleItem( player, LETTER_OF_MANAKIA, 1 )

                                state.setMemoState( 14 )
                                state.setConditionWithSound( 17, true )

                                return this.getPath( '30565-04.html' )

                            case 14:
                                return this.getPath( '30565-05.html' )
                        }

                        break

                    case MAESTRO_NIKOLA:
                        switch ( memoState ) {
                            case 15:
                                if ( QuestHelper.hasQuestItem( player, LETTER_TO_NICHOLA ) ) {
                                    return this.getPath( '30621-01.html' )
                                }

                                break

                            case 16:
                                if ( !QuestHelper.hasQuestItem( player, HEART_OF_PORTA ) ) {
                                    return this.getPath( '30621-03.html' )
                                }

                                await QuestHelper.takeMultipleItems( player, 1, ORDER_OF_NICHOLA, HEART_OF_PORTA )

                                state.setMemoState( 17 )
                                state.setConditionWithSound( 21, true )

                                return this.getPath( '30621-04.html' )

                            case 17:
                                return this.getPath( '30621-05.html' )
                        }

                        break

                    case CARDINAL_SERESIN:
                        switch ( memoState ) {
                            case 8:
                                if ( QuestHelper.hasQuestItem( player, LETTER_TO_SERESIN ) ) {
                                    return this.getPath( '30657-01.html' )
                                }

                                break

                            case 9:
                                return this.getPath( '30657-04.html' )

                            case 18:
                                return this.getPath( '30657-05.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === HIGH_PRIEST_HOLLINT ) {
                    return this.getPath( '30191-01b.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}