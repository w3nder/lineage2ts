import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { QuestHelper } from '../helpers/QuestHelper'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { Race } from '../../gameService/enums/Race'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const DUELIST_KAIEN = 30623

const ORDER_GLUDIO = 2763
const ORDER_DION = 2764
const ORDER_GIRAN = 2765
const ORDER_OREN = 2766
const ORDER_ADEN = 2767
const PUNCHERS_SHARD = 2768
const NOBLE_ANTS_FEELER = 2769
const DRONES_CHITIN = 2770
const DEAD_SEEKER_FANG = 2771
const OVERLORD_NECKLACE = 2772
const FETTERED_SOULS_CHAIN = 2773
const CHIEDS_AMULET = 2774
const ENCHANTED_EYE_MEAT = 2775
const TAMRIN_ORCS_RING = 2776
const TAMRIN_ORCS_ARROW = 2777
const FINAL_ORDER = 2778
const EXCUROS_SKIN = 2779
const KRATORS_SHARD = 2780
const GRANDIS_SKIN = 2781
const TIMAK_ORCS_BELT = 2782
const LAKINS_MACE = 2783

const MARK_OF_DUELIST = 2762
const DIMENSIONAL_DIAMOND = 7562

const PUNCHER = 20085
const NOBLE_ANT_LEADER = 20090
const DEAD_SEEKER = 20202
const EXCURO = 20214
const KRATOR = 20217
const MARSH_STAKATO_DRONE = 20234
const BREKA_ORC_OVERLORD = 20270
const FETTERED_SOUL = 20552
const GRANDIS = 20554
const ENCHANTED_MONSTEREYE = 20564
const LETO_LIZARDMAN_OVERLORD = 20582
const TIMAK_ORC_OVERLORD = 20588
const TAMLIN_ORC = 20601
const TAMLIN_ORC_ARCHER = 20602
const LAKIN = 20604

const minimumLevel = 39

export class TestOfTheDuelist extends ListenerLogic {
    constructor() {
        super( 'Q00222_TestOfTheDuelist', 'listeners/tracked-200/TestOfTheDuelist.ts' )
        this.questId = 222
        this.questItemIds = [
            ORDER_GLUDIO,
            ORDER_DION,
            ORDER_GIRAN,
            ORDER_OREN,
            ORDER_ADEN,
            PUNCHERS_SHARD,
            NOBLE_ANTS_FEELER,
            DRONES_CHITIN,
            DEAD_SEEKER_FANG,
            OVERLORD_NECKLACE,
            FETTERED_SOULS_CHAIN,
            CHIEDS_AMULET,
            ENCHANTED_EYE_MEAT,
            TAMRIN_ORCS_RING,
            TAMRIN_ORCS_ARROW,
            FINAL_ORDER,
            EXCUROS_SKIN,
            KRATORS_SHARD,
            GRANDIS_SKIN,
            TIMAK_ORCS_BELT,
            LAKINS_MACE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            PUNCHER,
            NOBLE_ANT_LEADER,
            DEAD_SEEKER,
            EXCURO,
            KRATOR,
            MARSH_STAKATO_DRONE,
            BREKA_ORC_OVERLORD,
            FETTERED_SOUL,
            GRANDIS,
            ENCHANTED_MONSTEREYE,
            LETO_LIZARDMAN_OVERLORD,
            TIMAK_ORC_OVERLORD,
            TAMLIN_ORC,
            TAMLIN_ORC_ARCHER,
            LAKIN,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00222_TestOfTheDuelist'
    }

    getQuestStartIds(): Array<number> {
        return [ DUELIST_KAIEN ]
    }

    getTalkIds(): Array<number> {
        return [ DUELIST_KAIEN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( npc.getId() ) {
            case PUNCHER:
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, ORDER_GLUDIO ) ) {
                    await this.tryToProgressFirstTask( player, state, PUNCHERS_SHARD, 10, data.isChampion )
                }

                return

            case NOBLE_ANT_LEADER:
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, ORDER_GLUDIO ) ) {
                    await this.tryToProgressFirstTask( player, state, NOBLE_ANTS_FEELER, 10, data.isChampion )
                }

                return

            case DEAD_SEEKER:
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, ORDER_DION ) ) {
                    await this.tryToProgressFirstTask( player, state, DEAD_SEEKER_FANG, 10, data.isChampion )
                }

                return

            case EXCURO:
                if ( state.isMemoState( 2 ) && QuestHelper.hasQuestItem( player, FINAL_ORDER ) ) {
                    await this.tryToProgressSecondTask( player, state, EXCUROS_SKIN, 3, data.isChampion )
                }

                return

            case KRATOR:
                if ( state.isMemoState( 2 ) && QuestHelper.hasQuestItem( player, FINAL_ORDER ) ) {
                    await this.tryToProgressSecondTask( player, state, KRATORS_SHARD, 3, data.isChampion )
                }

                return

            case MARSH_STAKATO_DRONE:
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, ORDER_DION ) ) {
                    await this.tryToProgressFirstTask( player, state, DRONES_CHITIN, 10, data.isChampion )
                }

                return

            case BREKA_ORC_OVERLORD:
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, ORDER_GIRAN ) ) {
                    await this.tryToProgressFirstTask( player, state, OVERLORD_NECKLACE, 10, data.isChampion )
                }

                return

            case FETTERED_SOUL:
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, ORDER_GIRAN ) ) {
                    await this.tryToProgressFirstTask( player, state, FETTERED_SOULS_CHAIN, 10, data.isChampion )
                }

                return

            case GRANDIS:
                if ( state.isMemoState( 2 ) && QuestHelper.hasQuestItem( player, FINAL_ORDER ) ) {
                    await this.tryToProgressSecondTask( player, state, GRANDIS_SKIN, 3, data.isChampion )
                }

                return

            case ENCHANTED_MONSTEREYE:
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, ORDER_OREN ) ) {
                    await this.tryToProgressFirstTask( player, state, ENCHANTED_EYE_MEAT, 10, data.isChampion )
                }

                return

            case LETO_LIZARDMAN_OVERLORD:
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, ORDER_OREN ) ) {
                    await this.tryToProgressFirstTask( player, state, CHIEDS_AMULET, 10, data.isChampion )
                }

                return

            case TIMAK_ORC_OVERLORD:
                if ( state.isMemoState( 2 ) && QuestHelper.hasQuestItem( player, FINAL_ORDER ) ) {
                    await this.tryToProgressSecondTask( player, state, TIMAK_ORCS_BELT, 3, data.isChampion )
                }

                return

            case TAMLIN_ORC:
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, ORDER_ADEN ) ) {
                    await this.tryToProgressFirstTask( player, state, TAMRIN_ORCS_RING, 10, data.isChampion )
                }

                return

            case TAMLIN_ORC_ARCHER:
                if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, ORDER_ADEN ) ) {
                    await this.tryToProgressFirstTask( player, state, TAMRIN_ORCS_ARROW, 10, data.isChampion )
                }

                return

            case LAKIN:
                if ( state.isMemoState( 2 ) && QuestHelper.hasQuestItem( player, FINAL_ORDER ) ) {
                    await this.tryToProgressSecondTask( player, state, LAKINS_MACE, 3, data.isChampion )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveMultipleItems( player, 1, ORDER_GLUDIO, ORDER_DION, ORDER_GIRAN, ORDER_OREN, ORDER_ADEN )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        let amount: number = player.getClassId() === ClassIdValues.palusKnight.id ? 104 : 72
                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, amount )

                        return this.getPath( '30623-07a.htm' )
                    }

                    return this.getPath( '30623-07.htm' )
                }
                return

            case '30623-04.htm':
                if ( player.getRace() === Race.ORC ) {
                    return this.getPath( '30623-05.htm' )
                }

                break

            case '30623-06.htm':
            case '30623-07.html':
            case '30623-09.html':
            case '30623-10.html':
            case '30623-11.html':
            case '30623-12.html':
            case '30623-15.html':
                break

            case '30623-08.html':
                state.setConditionWithSound( 2, true )
                break

            case '30623-16.html':
                await QuestHelper.takeMultipleItems( player, -1,
                        PUNCHERS_SHARD,
                        DEAD_SEEKER_FANG,
                        NOBLE_ANTS_FEELER,
                        DRONES_CHITIN,
                        OVERLORD_NECKLACE,
                        FETTERED_SOULS_CHAIN,
                        CHIEDS_AMULET,
                        ENCHANTED_EYE_MEAT,
                        TAMRIN_ORCS_RING,
                        TAMRIN_ORCS_ARROW,
                        ORDER_GLUDIO,
                        ORDER_DION,
                        ORDER_GIRAN,
                        ORDER_OREN,
                        ORDER_ADEN )

                await QuestHelper.giveSingleItem( player, FINAL_ORDER, 1 )

                state.setMemoState( 2 )
                state.setConditionWithSound( 4, true )

                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( [ ClassIdValues.warrior.id, ClassIdValues.elvenKnight.id, ClassIdValues.palusKnight.id, ClassIdValues.orcMonk.id ].includes( player.getClassId() ) ) {
                    if ( player.getLevel() >= minimumLevel ) {
                        return this.getPath( '30623-03.htm' )
                    }

                    return this.getPath( '30623-01.html' )
                }

                return this.getPath( '30623-02.html' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 5:
                        if ( QuestHelper.hasQuestItems( player, ORDER_GLUDIO, ORDER_DION, ORDER_GIRAN, ORDER_OREN, ORDER_ADEN ) ) {
                            if ( QuestHelper.getItemsSumCount( player, PUNCHERS_SHARD, NOBLE_ANTS_FEELER, DRONES_CHITIN, DEAD_SEEKER_FANG, OVERLORD_NECKLACE, FETTERED_SOULS_CHAIN, CHIEDS_AMULET, ENCHANTED_EYE_MEAT, TAMRIN_ORCS_RING, TAMRIN_ORCS_ARROW ) >= 100 ) {
                                return this.getPath( '30623-13.html' )
                            }

                            return this.getPath( '30623-14.html' )
                        }

                        break

                    case 3:
                        if ( QuestHelper.hasQuestItems( player, FINAL_ORDER ) ) {
                            if ( QuestHelper.getItemsSumCount( player, EXCUROS_SKIN, KRATORS_SHARD, LAKINS_MACE, GRANDIS_SKIN, TIMAK_ORCS_BELT ) >= 15 ) {
                                await QuestHelper.giveAdena( player, 161806, true )
                                await QuestHelper.giveSingleItem( player, MARK_OF_DUELIST, 1 )
                                await QuestHelper.addExpAndSp( player, 894888, 61408 )
                                await state.exitQuest( false, true )

                                player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                                return this.getPath( '30623-18.html' )
                            }

                            return this.getPath( '30623-17.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return QuestHelper.getAlreadyCompletedMessagePath()
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async tryToProgressFirstTask( player: L2PcInstance, state: QuestState, itemId: number, limit: number, isFromChampion: boolean ): Promise<void> {
        let outcome: boolean = await QuestHelper.rewardUpToLimit( player, itemId, 1, limit, isFromChampion )
        if ( outcome && QuestHelper.getItemsSumCount( player, PUNCHERS_SHARD, NOBLE_ANTS_FEELER, DRONES_CHITIN, DEAD_SEEKER_FANG, OVERLORD_NECKLACE, FETTERED_SOULS_CHAIN, CHIEDS_AMULET, ENCHANTED_EYE_MEAT, TAMRIN_ORCS_RING, TAMRIN_ORCS_ARROW ) >= 100 ) {
            state.setConditionWithSound( 3 )

            return
        }
    }

    async tryToProgressSecondTask( player: L2PcInstance, state: QuestState, itemId: number, limit: number, isFromChampion: boolean ): Promise<void> {
        let outcome: boolean = await QuestHelper.rewardUpToLimit( player, itemId, 1, limit, isFromChampion )
        if ( outcome && QuestHelper.getItemsSumCount( player, EXCUROS_SKIN, KRATORS_SHARD, LAKINS_MACE, GRANDIS_SKIN, TIMAK_ORCS_BELT ) >= 15 ) {
            state.setConditionWithSound( 5 )
            return
        }
    }
}