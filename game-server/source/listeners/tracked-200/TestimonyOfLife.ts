import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { Race } from '../../gameService/enums/Race'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const HIERARCH_ASTERIOS = 30154
const BLACKSMITH_PUSHKIN = 30300
const THALIA = 30371
const PRIEST_ADONIUS = 30375
const ARKENIA = 30419
const MASTER_CARDIEN = 30460
const ISAEL_SILVERSHADOW = 30655

const TALINS_SPEAR = 3026
const CARDIENS_LETTER = 3141
const CAMOMILE_CHARM = 3142
const HIERARCHS_LETTER = 3143
const MOONFLOWER_CHARM = 3144
const GRAIL_DIAGRAM = 3145
const THALIAS_1ST_LETTER = 3146
const THALIAS_2ND_LETTER = 3147
const THALIAS_INSTRUCTIONS = 3148
const PUSHKINS_LIST = 3149
const PURE_MITHRIL_CUP = 3150
const ARKENIAS_CONTRACT = 3151
const ARKENIAS_INSTRUCTIONS = 3152
const ADONIUS_LIST = 3153
const ANDARIEL_SCRIPTURE_COPY = 3154
const STARDUST = 3155
const ISAELS_INSTRUCTIONS = 3156
const ISAELS_LETTER = 3157
const GRAIL_OF_PURITY = 3158
const TEARS_OF_UNICORN = 3159
const WATER_OF_LIFE = 3160
const PURE_MITHRIL_ORE = 3161
const ANT_SOLDIER_ACID = 3162
const WYRMS_TALON = 3163
const SPIDER_ICHOR = 3164
const HARPYS_DOWN = 3165
const TALINS_SPEAR_BLADE = 3166
const TALINS_SPEAR_SHAFT = 3167
const TALINS_RUBY = 3168
const TALINS_AQUAMARINE = 3169
const TALINS_AMETHYST = 3170
const TALINS_PERIDOT = 3171

const MARK_OF_LIFE = 3140
const DIMENSIONAL_DIAMOND = 7562

const ANT_RECRUIT = 20082
const ANT_PATROL = 20084
const ANT_GUARD = 20086
const ANT_SOLDIER = 20087
const ANT_WARRIOR_CAPTAIN = 20088
const HARPY = 20145
const WYRM = 20176
const MARSH_SPIDER = 20233
const GUARDIAN_BASILISK = 20550
const LETO_LIZARDMAN_SHAMAN = 20581
const LETO_LIZARDMAN_OVERLORD = 20582

const UNICORN_OF_EVA = 27077
const minimumLevel = 37
const baseLevel = 38

export class TestimonyOfLife extends ListenerLogic {
    constructor() {
        super( 'Q00218_TestimonyOfLife', 'listeners/tracked-200/TestimonyOfLife.ts' )
        this.questId = 218
        this.questItemIds = [
            TALINS_SPEAR,
            CARDIENS_LETTER,
            CAMOMILE_CHARM,
            HIERARCHS_LETTER,
            MOONFLOWER_CHARM,
            GRAIL_DIAGRAM,
            THALIAS_1ST_LETTER,
            THALIAS_2ND_LETTER,
            THALIAS_INSTRUCTIONS,
            PUSHKINS_LIST,
            PURE_MITHRIL_CUP,
            ARKENIAS_CONTRACT,
            ARKENIAS_INSTRUCTIONS,
            ADONIUS_LIST,
            ANDARIEL_SCRIPTURE_COPY,
            STARDUST,
            ISAELS_INSTRUCTIONS,
            ISAELS_LETTER,
            GRAIL_OF_PURITY,
            TEARS_OF_UNICORN,
            WATER_OF_LIFE,
            PURE_MITHRIL_ORE,
            ANT_SOLDIER_ACID,
            WYRMS_TALON,
            SPIDER_ICHOR,
            HARPYS_DOWN,
            TALINS_SPEAR_BLADE,
            TALINS_SPEAR_SHAFT,
            TALINS_RUBY,
            TALINS_AQUAMARINE,
            TALINS_AMETHYST,
            TALINS_PERIDOT,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ANT_RECRUIT,
            ANT_PATROL,
            ANT_GUARD,
            ANT_SOLDIER,
            ANT_WARRIOR_CAPTAIN,
            HARPY,
            WYRM,
            MARSH_SPIDER,
            GUARDIAN_BASILISK,
            LETO_LIZARDMAN_SHAMAN,
            LETO_LIZARDMAN_OVERLORD,
            UNICORN_OF_EVA,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00218_TestimonyOfLife'
    }

    getQuestStartIds(): Array<number> {
        return [ MASTER_CARDIEN ]
    }

    getTalkIds(): Array<number> {
        return [
            MASTER_CARDIEN,
            HIERARCH_ASTERIOS,
            BLACKSMITH_PUSHKIN,
            THALIA,
            PRIEST_ADONIUS,
            ARKENIA,
            ISAEL_SILVERSHADOW,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case ANT_RECRUIT:
            case ANT_PATROL:
            case ANT_GUARD:
            case ANT_SOLDIER:
            case ANT_WARRIOR_CAPTAIN:
                if ( QuestHelper.hasQuestItems( player, MOONFLOWER_CHARM, PUSHKINS_LIST )
                        && QuestHelper.getQuestItemsCount( player, ANT_SOLDIER_ACID ) < 20 ) {
                    await QuestHelper.rewardSingleQuestItem( player, ANT_SOLDIER_ACID, 2, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, ANT_SOLDIER_ACID ) >= 20 ) {
                        if ( QuestHelper.getQuestItemsCount( player, PURE_MITHRIL_ORE ) >= 10
                                && QuestHelper.getQuestItemsCount( player, WYRMS_TALON ) >= 20 ) {
                            state.setConditionWithSound( 5 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case HARPY:
                if ( QuestHelper.hasQuestItems( player, MOONFLOWER_CHARM, ADONIUS_LIST )
                        && QuestHelper.getQuestItemsCount( player, HARPYS_DOWN ) < 20 ) {
                    await QuestHelper.rewardSingleQuestItem( player, HARPYS_DOWN, 4, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, HARPYS_DOWN ) >= 20 ) {
                        if ( QuestHelper.getQuestItemsCount( player, SPIDER_ICHOR ) >= 20 ) {
                            state.setConditionWithSound( 10 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case WYRM:
                if ( QuestHelper.hasQuestItems( player, MOONFLOWER_CHARM, PUSHKINS_LIST )
                        && QuestHelper.getQuestItemsCount( player, WYRMS_TALON ) < 20 ) {
                    await QuestHelper.rewardSingleQuestItem( player, WYRMS_TALON, 4, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, WYRMS_TALON ) >= 20 ) {
                        if ( QuestHelper.getQuestItemsCount( player, PURE_MITHRIL_ORE ) >= 10
                                && QuestHelper.getQuestItemsCount( player, ANT_SOLDIER_ACID ) >= 20 ) {
                            state.setConditionWithSound( 5 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case MARSH_SPIDER:
                if ( QuestHelper.hasQuestItems( player, MOONFLOWER_CHARM, ADONIUS_LIST )
                        && QuestHelper.getQuestItemsCount( player, SPIDER_ICHOR ) < 20 ) {
                    await QuestHelper.rewardSingleQuestItem( player, SPIDER_ICHOR, 4, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, SPIDER_ICHOR ) >= 20 ) {
                        if ( QuestHelper.getQuestItemsCount( player, HARPYS_DOWN ) >= 20 ) {
                            state.setConditionWithSound( 10 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case GUARDIAN_BASILISK:
                if ( QuestHelper.hasQuestItems( player, MOONFLOWER_CHARM, PUSHKINS_LIST )
                        && QuestHelper.getQuestItemsCount( player, PURE_MITHRIL_ORE ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, PURE_MITHRIL_ORE, 2, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, PURE_MITHRIL_ORE ) == 10 ) {
                        if ( ( QuestHelper.getQuestItemsCount( player, WYRMS_TALON ) >= 20 ) && ( QuestHelper.getQuestItemsCount( player, ANT_SOLDIER_ACID ) >= 20 ) ) {
                            state.setConditionWithSound( 5 )
                        }

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case LETO_LIZARDMAN_SHAMAN:
            case LETO_LIZARDMAN_OVERLORD:
                if ( !QuestHelper.hasQuestItem( player, ISAELS_INSTRUCTIONS ) ) {
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, TALINS_SPEAR_BLADE ) ) {
                    await QuestHelper.giveSingleItem( player, TALINS_SPEAR_BLADE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, TALINS_SPEAR_SHAFT ) ) {
                    await QuestHelper.giveSingleItem( player, TALINS_SPEAR_SHAFT, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, TALINS_RUBY ) ) {
                    await QuestHelper.giveSingleItem( player, TALINS_RUBY, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, TALINS_AQUAMARINE ) ) {
                    await QuestHelper.giveSingleItem( player, TALINS_AQUAMARINE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, TALINS_AMETHYST ) ) {
                    await QuestHelper.giveSingleItem( player, TALINS_AMETHYST, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, TALINS_PERIDOT ) ) {
                    await QuestHelper.giveSingleItem( player, TALINS_PERIDOT, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                return

            case UNICORN_OF_EVA:
                if ( !QuestHelper.hasQuestItem( player, TEARS_OF_UNICORN )
                        && QuestHelper.hasQuestItems( player, MOONFLOWER_CHARM, TALINS_SPEAR, GRAIL_OF_PURITY ) ) {

                    let npc = L2World.getObjectById( data.targetId ) as L2Npc
                    if ( npc.getKillingBlowWeapon() === TALINS_SPEAR ) {
                        await QuestHelper.takeMultipleItems( player, 1, TALINS_SPEAR, GRAIL_OF_PURITY )
                        await QuestHelper.giveSingleItem( player, TEARS_OF_UNICORN, 1 )

                        state.setConditionWithSound( 19, true )
                    }
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT': {
                if ( state.isCreated() ) {
                    state.startQuest()
                    if ( !QuestHelper.hasQuestItem( player, CARDIENS_LETTER ) ) {
                        await QuestHelper.giveSingleItem( player, CARDIENS_LETTER, 1 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 102 )

                        return this.getPath( '30460-04a.htm' )
                    }

                    return this.getPath( '30460-04.htm' )
                }
                return
            }
            case '30154-02.html':
            case '30154-03.html':
            case '30154-04.html':
            case '30154-05.html':
            case '30154-06.html':
            case '30300-02.html':
            case '30300-03.html':
            case '30300-04.html':
            case '30300-05.html':
            case '30300-09.html':
            case '30300-07a.html':
            case '30371-02.html':
            case '30371-10.html':
            case '30419-02.html':
            case '30419-03.html':
                break

            case '30154-07.html':
                if ( QuestHelper.hasQuestItem( player, CARDIENS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, CARDIENS_LETTER, 1 )
                    await QuestHelper.giveMultipleItems( player, 1, HIERARCHS_LETTER, MOONFLOWER_CHARM )

                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '30300-06.html':
                if ( QuestHelper.hasQuestItem( player, GRAIL_DIAGRAM ) ) {
                    await QuestHelper.takeSingleItem( player, GRAIL_DIAGRAM, 1 )
                    await QuestHelper.giveSingleItem( player, PUSHKINS_LIST, 1 )

                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '30300-10.html':
                if ( QuestHelper.hasQuestItem( player, PUSHKINS_LIST ) ) {
                    await QuestHelper.giveSingleItem( player, PURE_MITHRIL_CUP, 1 )
                    await QuestHelper.takeSingleItem( player, PUSHKINS_LIST, 1 )
                    await QuestHelper.takeMultipleItems( player, -1, PURE_MITHRIL_ORE, ANT_SOLDIER_ACID, WYRMS_TALON )

                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '30371-03.html':
                if ( QuestHelper.hasQuestItem( player, HIERARCHS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, HIERARCHS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, GRAIL_DIAGRAM, 1 )

                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '30371-11.html':
                if ( QuestHelper.hasQuestItem( player, STARDUST ) ) {
                    await QuestHelper.giveSingleItem( player, THALIAS_2ND_LETTER, 1 )
                    await QuestHelper.takeSingleItem( player, STARDUST, 1 )

                    state.setConditionWithSound( 14, true )
                    break
                }

                return

            case '30419-04.html':
                if ( QuestHelper.hasQuestItem( player, THALIAS_1ST_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, THALIAS_1ST_LETTER, 1 )
                    await QuestHelper.giveMultipleItems( player, 1, ARKENIAS_CONTRACT, ARKENIAS_INSTRUCTIONS )

                    state.setConditionWithSound( 8, true )
                    break
                }

                return

            case '30375-02.html':
                if ( QuestHelper.hasQuestItem( player, ARKENIAS_INSTRUCTIONS ) ) {
                    await QuestHelper.takeSingleItem( player, ARKENIAS_INSTRUCTIONS, 1 )
                    await QuestHelper.giveSingleItem( player, ADONIUS_LIST, 1 )

                    state.setConditionWithSound( 9, true )
                    break
                }

                return

            case '30655-02.html':
                if ( QuestHelper.hasQuestItem( player, THALIAS_2ND_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, THALIAS_2ND_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, ISAELS_INSTRUCTIONS, 1 )

                    state.setConditionWithSound( 15, true )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MASTER_CARDIEN ) {
                    if ( player.getRace() !== Race.ELF ) {
                        return this.getPath( '30460-01.html' )
                    }

                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30460-02.html' )
                    }

                    if ( player.isInCategory( CategoryType.ELF_2ND_GROUP ) ) {
                        return this.getPath( '30460-03.htm' )
                    }

                    return this.getPath( '30460-01a.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MASTER_CARDIEN:
                        if ( QuestHelper.hasQuestItem( player, CARDIENS_LETTER ) ) {
                            return this.getPath( '30460-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MOONFLOWER_CHARM ) ) {
                            return this.getPath( '30460-06.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, CAMOMILE_CHARM ) ) {
                            await QuestHelper.giveAdena( player, 342288, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_LIFE, 1 )
                            await QuestHelper.addExpAndSp( player, 1886832, 125918 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                            return this.getPath( '30460-07.html' )
                        }

                        break

                    case HIERARCH_ASTERIOS:
                        if ( QuestHelper.hasQuestItem( player, CARDIENS_LETTER ) ) {
                            return this.getPath( '30154-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MOONFLOWER_CHARM ) ) {
                            if ( !QuestHelper.hasQuestItem( player, WATER_OF_LIFE ) ) {
                                return this.getPath( '30154-08.html' )
                            }

                            await QuestHelper.giveSingleItem( player, CAMOMILE_CHARM, 1 )
                            await QuestHelper.takeMultipleItems( player, 1, MOONFLOWER_CHARM, WATER_OF_LIFE )

                            state.setConditionWithSound( 21, true )
                            return this.getPath( '30154-09.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, CAMOMILE_CHARM ) ) {
                            return this.getPath( '30154-10.html' )
                        }

                        break

                    case BLACKSMITH_PUSHKIN:
                        if ( !QuestHelper.hasQuestItem( player, MOONFLOWER_CHARM ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, GRAIL_DIAGRAM ) ) {
                            return this.getPath( '30300-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PUSHKINS_LIST ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, PURE_MITHRIL_ORE ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, ANT_SOLDIER_ACID ) >= 20
                                    && QuestHelper.getQuestItemsCount( player, WYRMS_TALON ) >= 20 ) {
                                return this.getPath( '30300-08.html' )
                            }

                            return this.getPath( '30300-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PURE_MITHRIL_CUP ) ) {
                            return this.getPath( '30300-11.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, GRAIL_DIAGRAM, PUSHKINS_LIST, PURE_MITHRIL_CUP ) ) {
                            return this.getPath( '30300-12.html' )
                        }

                        break

                    case THALIA:
                        if ( !QuestHelper.hasQuestItem( player, MOONFLOWER_CHARM ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, HIERARCHS_LETTER ) ) {
                            return this.getPath( '30371-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, GRAIL_DIAGRAM ) ) {
                            return this.getPath( '30371-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PUSHKINS_LIST ) ) {
                            return this.getPath( '30371-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PURE_MITHRIL_CUP ) ) {
                            await QuestHelper.giveSingleItem( player, THALIAS_1ST_LETTER, 1 )
                            await QuestHelper.takeSingleItem( player, PURE_MITHRIL_CUP, 1 )

                            state.setConditionWithSound( 7, true )
                            return this.getPath( '30371-06.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, THALIAS_1ST_LETTER ) ) {
                            return this.getPath( '30371-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ARKENIAS_CONTRACT ) ) {
                            return this.getPath( '30371-08.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, STARDUST ) ) {
                            return this.getPath( '30371-09.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, THALIAS_INSTRUCTIONS ) ) {
                            if ( player.getLevel() >= baseLevel ) {
                                await QuestHelper.takeSingleItem( player, THALIAS_INSTRUCTIONS, 1 )
                                await QuestHelper.giveSingleItem( player, THALIAS_2ND_LETTER, 1 )

                                state.setConditionWithSound( 14, true )
                                return this.getPath( '30371-13.html' )
                            }

                            return this.getPath( '30371-12.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, THALIAS_2ND_LETTER ) ) {
                            return this.getPath( '30371-14.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ISAELS_INSTRUCTIONS ) ) {
                            return this.getPath( '30371-15.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, TALINS_SPEAR, ISAELS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, ISAELS_LETTER, 1 )
                            await QuestHelper.giveSingleItem( player, GRAIL_OF_PURITY, 1 )

                            state.setConditionWithSound( 18, true )
                            return this.getPath( '30371-16.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, TALINS_SPEAR, GRAIL_OF_PURITY ) ) {
                            return this.getPath( '30371-17.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TEARS_OF_UNICORN ) ) {
                            await QuestHelper.takeSingleItem( player, TEARS_OF_UNICORN, 1 )
                            await QuestHelper.giveSingleItem( player, WATER_OF_LIFE, 1 )

                            state.setConditionWithSound( 20, true )
                            return this.getPath( '30371-18.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, CAMOMILE_CHARM, WATER_OF_LIFE ) ) {
                            return this.getPath( '30371-19.html' )
                        }

                        break

                    case ARKENIA:
                        if ( !QuestHelper.hasQuestItem( player, MOONFLOWER_CHARM ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, THALIAS_1ST_LETTER ) ) {
                            return this.getPath( '30419-01.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, ARKENIAS_INSTRUCTIONS, ADONIUS_LIST ) ) {
                            return this.getPath( '30419-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ANDARIEL_SCRIPTURE_COPY ) ) {
                            await QuestHelper.takeMultipleItems( player, 1, ARKENIAS_CONTRACT, ANDARIEL_SCRIPTURE_COPY )
                            await QuestHelper.giveSingleItem( player, STARDUST, 1 )

                            state.setConditionWithSound( 12, true )
                            return this.getPath( '30419-06.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, STARDUST ) ) {
                            return this.getPath( '30419-07.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, THALIAS_1ST_LETTER, ARKENIAS_CONTRACT, ANDARIEL_SCRIPTURE_COPY, STARDUST ) ) {
                            return this.getPath( '30419-08.html' )
                        }

                        break

                    case PRIEST_ADONIUS:
                        if ( !QuestHelper.hasQuestItem( player, MOONFLOWER_CHARM ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, ARKENIAS_INSTRUCTIONS ) ) {
                            return this.getPath( '30375-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ADONIUS_LIST ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, SPIDER_ICHOR ) >= 20
                                    && QuestHelper.getQuestItemsCount( player, HARPYS_DOWN ) >= 20 ) {
                                await QuestHelper.giveSingleItem( player, ANDARIEL_SCRIPTURE_COPY, 1 )
                                await QuestHelper.takeMultipleItems( player, -1, ADONIUS_LIST, SPIDER_ICHOR, HARPYS_DOWN )

                                state.setConditionWithSound( 11, true )
                                return this.getPath( '30375-04.html' )
                            }

                            return this.getPath( '30375-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ANDARIEL_SCRIPTURE_COPY ) ) {
                            return this.getPath( '30375-05.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, ARKENIAS_INSTRUCTIONS, ADONIUS_LIST, ANDARIEL_SCRIPTURE_COPY ) ) {
                            return this.getPath( '30375-06.html' )
                        }

                        break

                    case ISAEL_SILVERSHADOW:
                        if ( !QuestHelper.hasQuestItem( player, MOONFLOWER_CHARM ) ) {
                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, THALIAS_2ND_LETTER ) ) {
                            return this.getPath( '30655-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ISAELS_INSTRUCTIONS ) ) {
                            if ( QuestHelper.hasQuestItems( player, TALINS_SPEAR_BLADE, TALINS_SPEAR_SHAFT, TALINS_RUBY, TALINS_AQUAMARINE, TALINS_AMETHYST, TALINS_PERIDOT ) ) {
                                await QuestHelper.giveMultipleItems( player, 1, TALINS_SPEAR, ISAELS_LETTER )
                                await QuestHelper.takeMultipleItems( player, 1, ISAELS_INSTRUCTIONS, TALINS_SPEAR_BLADE, TALINS_SPEAR_SHAFT, TALINS_RUBY, TALINS_AQUAMARINE, TALINS_AMETHYST, TALINS_PERIDOT )

                                state.setConditionWithSound( 17, true )
                                return this.getPath( '30655-04.html' )
                            }

                            return this.getPath( '30655-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, TALINS_SPEAR, ISAELS_LETTER ) ) {
                            return this.getPath( '30655-05.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, GRAIL_OF_PURITY, WATER_OF_LIFE, CAMOMILE_CHARM ) ) {
                            return this.getPath( '30655-06.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MASTER_CARDIEN ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}