import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const STEDMIEL = 30692
const GABRIELLE = 30753
const GILMORE = 30754
const KANTABILON = 31042
const RAHORAKTI = 31336
const TALIEN = 31739
const CARADINE = 31740
const VIRGIL = 31742
const KASSANDRA = 31743
const OGMAR = 31744

const BARAHAM = 27113
const MALRUK_SUCCUBUS_1 = 20244
const MALRUK_SUCCUBUS_TUREN_1 = 20245
const MALRUK_SUCCUBUS_2 = 20283
const MALRUK_SUCCUBUS_TUREN_2 = 20284
const TAIK_ORC_SUPPLY_LEADER = 20669

const LEGEND_OF_SEVENTEEN = 7587
const MALRUK_SUCCUBUS_CLAW = 7597
const ECHO_CRYSTAL = 7589
const POETRY_BOOK = 7588
const CRIMSON_MOSS = 7598
const RAHORAKTIS_MEDICINE = 7599
const VIRGILS_LETTER = 7677

const CRIMSON_MOSS_CHANCE = 30
const MALRUK_SUCCUBUS_CLAW_CHANCE = 60
const minimumLevel = 50

export class PossessorOfAPreciousSoulPartOne extends ListenerLogic {
    constructor() {
        super( 'Q00241_PossessorOfAPreciousSoulPartOne', 'listeners/tracked-200/PossessorOfAPreciousSoulPartOne.ts' )
        this.questId = 241
        this.questItemIds = [
            LEGEND_OF_SEVENTEEN,
            MALRUK_SUCCUBUS_CLAW,
            ECHO_CRYSTAL,
            POETRY_BOOK,
            CRIMSON_MOSS,
            RAHORAKTIS_MEDICINE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            BARAHAM,
            MALRUK_SUCCUBUS_1,
            MALRUK_SUCCUBUS_TUREN_1,
            MALRUK_SUCCUBUS_2,
            MALRUK_SUCCUBUS_TUREN_2,
            TAIK_ORC_SUPPLY_LEADER,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00241_PossessorOfAPreciousSoul1'
    }

    getQuestStartIds(): Array<number> {
        return [ TALIEN ]
    }

    getTalkIds(): Array<number> {
        return [
            TALIEN,
            STEDMIEL,
            GABRIELLE,
            GILMORE,
            KANTABILON,
            RAHORAKTI,
            CARADINE,
            KASSANDRA,
            VIRGIL,
            OGMAR,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        switch ( data.npcId ) {
            case BARAHAM:
                await this.onBaharamKilled( data )
                return

            case MALRUK_SUCCUBUS_1:
            case MALRUK_SUCCUBUS_TUREN_1:
            case MALRUK_SUCCUBUS_2:
            case MALRUK_SUCCUBUS_TUREN_2:
                await this.processOnKilledEvent( data, 6, MALRUK_SUCCUBUS_CLAW_CHANCE, MALRUK_SUCCUBUS_CLAW, 10 )
                return

            case TAIK_ORC_SUPPLY_LEADER:
                await this.processOnKilledEvent( data, 14, CRIMSON_MOSS_CHANCE, CRIMSON_MOSS, 5 )
                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( !player.isSubClassActive() ) {
            return this.getPath( 'no_sub.html' )
        }

        switch ( data.eventName ) {
            case '31739-02.html':
                state.startQuest()
                break

            case '30753-02.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                }

                break

            case '30754-02.html':
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3, true )
                }

                break

            case '31739-05.html':
                if ( state.isCondition( 4 ) && QuestHelper.hasQuestItem( player, LEGEND_OF_SEVENTEEN ) ) {
                    await QuestHelper.takeSingleItem( player, LEGEND_OF_SEVENTEEN, -1 )

                    state.setConditionWithSound( 5, true )
                }

                break

            case '31042-02.html':
                if ( state.isCondition( 5 ) ) {
                    state.setConditionWithSound( 6, true )
                }

                break

            case '31042-05.html':
                if ( state.isCondition( 7 ) && QuestHelper.getQuestItemsCount( player, MALRUK_SUCCUBUS_CLAW ) >= 10 ) {
                    await QuestHelper.takeSingleItem( player, MALRUK_SUCCUBUS_CLAW, -1 )
                    await QuestHelper.giveSingleItem( player, ECHO_CRYSTAL, 1 )

                    state.setConditionWithSound( 8, true )
                }

                break

            case '31739-08.html':
                if ( state.isCondition( 8 ) && QuestHelper.hasQuestItem( player, ECHO_CRYSTAL ) ) {
                    await QuestHelper.takeSingleItem( player, ECHO_CRYSTAL, -1 )

                    state.setConditionWithSound( 9, true )
                }

                break

            case '30692-02.html':
                if ( state.isCondition( 9 ) && !QuestHelper.hasQuestItem( player, POETRY_BOOK ) ) {
                    await QuestHelper.giveSingleItem( player, POETRY_BOOK, 1 )

                    state.setConditionWithSound( 10, true )
                }

                break

            case '31739-11.html':
                if ( state.isCondition( 10 ) && QuestHelper.hasQuestItem( player, POETRY_BOOK ) ) {
                    await QuestHelper.takeSingleItem( player, POETRY_BOOK, -1 )

                    state.setConditionWithSound( 11, true )
                }

                break

            case '31742-02.html':
                if ( state.isCondition( 11 ) ) {
                    state.setConditionWithSound( 12, true )
                }

                break

            case '31744-02.html':
                if ( state.isCondition( 12 ) ) {
                    state.setConditionWithSound( 13, true )
                }

                break

            case '31336-02.html':
                if ( state.isCondition( 13 ) ) {
                    state.setConditionWithSound( 14, true )
                }

                break

            case '31336-05.html':
                if ( state.isCondition( 15 ) && ( QuestHelper.getQuestItemsCount( player, CRIMSON_MOSS ) >= 5 ) ) {
                    await QuestHelper.takeSingleItem( player, CRIMSON_MOSS, -1 )
                    await QuestHelper.giveSingleItem( player, RAHORAKTIS_MEDICINE, 1 )

                    state.setConditionWithSound( 16, true )
                }

                break

            case '31743-02.html':
                if ( state.isCondition( 16 ) && QuestHelper.hasQuestItem( player, RAHORAKTIS_MEDICINE ) ) {
                    await QuestHelper.takeSingleItem( player, RAHORAKTIS_MEDICINE, -1 )

                    state.setConditionWithSound( 17, true )
                }

                break

            case '31742-05.html':
                if ( state.isCondition( 17 ) ) {
                    state.setConditionWithSound( 18, true )
                }

                break

            case '31740-05.html':
                if ( state.getCondition() >= 18 ) {
                    await QuestHelper.giveSingleItem( player, VIRGILS_LETTER, 1 )
                    await QuestHelper.addExpAndSp( player, 263043, 0 )
                    await state.exitQuest( false, true )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        if ( state.isStarted() && !player.isSubClassActive() ) {
            return this.getPath( 'no_sub.html' )
        }

        switch ( data.characterNpcId ) {
            case TALIEN:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '31739-01.htm' : '31739-00.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31739-03.html' )

                            case 4:
                                if ( QuestHelper.hasQuestItem( player, LEGEND_OF_SEVENTEEN ) ) {
                                    return this.getPath( '31739-04.html' )
                                }

                                break

                            case 5:
                                return this.getPath( '31739-06.html' )

                            case 8:
                                if ( QuestHelper.hasQuestItem( player, ECHO_CRYSTAL ) ) {
                                    return this.getPath( '31739-07.html' )
                                }

                                break

                            case 9:
                                return this.getPath( '31739-09.html' )

                            case 10:
                                if ( QuestHelper.hasQuestItem( player, POETRY_BOOK ) ) {
                                    return this.getPath( '31739-10.html' )
                                }

                                break

                            case 11:
                                return this.getPath( '31739-12.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case GABRIELLE:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30753-01.html' )

                    case 2:
                        return this.getPath( '30753-03.html' )
                }

                break

            case GILMORE:
                switch ( state.getCondition() ) {
                    case 2:
                        return this.getPath( '30754-01.html' )

                    case 3:
                        return this.getPath( '30754-03.html' )
                }

                break

            case KANTABILON:
                switch ( state.getCondition() ) {
                    case 5:
                        return this.getPath( '31042-01.html' )

                    case 6:
                        return this.getPath( '31042-04.html' )

                    case 7:
                        if ( QuestHelper.getQuestItemsCount( player, MALRUK_SUCCUBUS_CLAW ) >= 10 ) {
                            return this.getPath( '31042-03.html' )
                        }

                        break

                    case 8:
                        return this.getPath( '31042-06.html' )
                }

                break

            case STEDMIEL:
                switch ( state.getCondition() ) {
                    case 9:
                        return this.getPath( '30692-01.html' )

                    case 10:
                        return this.getPath( '30692-03.html' )
                }

                break

            case VIRGIL:
                switch ( state.getCondition() ) {
                    case 11:
                        return this.getPath( '31742-01.html' )

                    case 12:
                        return this.getPath( '31742-03.html' )

                    case 17:
                        return this.getPath( '31742-04.html' )

                    case 18:
                        return this.getPath( '31742-06.html' )
                }

                break

            case OGMAR:
                switch ( state.getCondition() ) {
                    case 12:
                        return this.getPath( '31744-01.html' )

                    case 13:
                        return this.getPath( '31744-03.html' )
                }

                break

            case RAHORAKTI:
                switch ( state.getCondition() ) {
                    case 13:
                        return this.getPath( '31336-01.html' )

                    case 14:
                        return this.getPath( '31336-04.html' )

                    case 15:
                        if ( QuestHelper.getQuestItemsCount( player, CRIMSON_MOSS ) >= 5 ) {
                            return this.getPath( '31336-03.html' )
                        }

                        break

                    case 16:
                        return this.getPath( '31336-06.html' )
                }

                break

            case KASSANDRA:
                switch ( state.getCondition() ) {
                    case 16:
                        if ( QuestHelper.hasQuestItem( player, RAHORAKTIS_MEDICINE ) ) {
                            return this.getPath( '31743-01.html' )
                        }

                        break

                    case 17:
                        return this.getPath( '31743-03.html' )
                }

                break

            case CARADINE:
                if ( state.getCondition() >= 18 ) {
                    return this.getPath( '31740-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onBaharamKilled( data: AttackableKillEvent ): Promise<void> {
        let player = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 3 )
        if ( !player ) {
            return
        }

        await QuestHelper.giveSingleItem( player, LEGEND_OF_SEVENTEEN, 1 )

        let state = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
        state.setConditionWithSound( 4, true )
    }

    async processOnKilledEvent( data: AttackableKillEvent, startCondition: number, chance: number, itemId: number, maximumCount: number ): Promise<void> {
        let player = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), startCondition )
        if ( !player
                || QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) < _.random( 100 )
                || QuestHelper.getQuestItemsCount( player, itemId ) >= maximumCount ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= maximumCount ) {
            let state = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
            return state.setConditionWithSound( startCondition + 1, true )
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}