import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const BRUKURSE = 30569
const NECKLACE_OF_COURAGE = 1506
const NECKLACE_OF_VALOR = 1507
const WEREWOLF_HEAD = 1477
const WEREWOLF_TOTEM = 1501
const minimumLevel = 9

const headRewardRate = 30
const totemRewardRate = 600

export class SkirmishWithTheWerewolves extends ListenerLogic {
    constructor() {
        super( 'Q00274_SkirmishWithTheWerewolves', 'listeners/tracked-200/SkirmishWithTheWerewolves.ts' )
        this.questId = 274
        this.questItemIds = [
            WEREWOLF_HEAD,
            WEREWOLF_TOTEM,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20363, // Maraku Werewolf
            20364, // Maraku Werewolf Chieftain
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00274_SkirmishWithTheWerewolves'
    }

    getQuestStartIds(): Array<number> {
        return [ BRUKURSE ]
    }

    getTalkIds(): Array<number> {
        return [ BRUKURSE ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, WEREWOLF_HEAD, 1, data.isChampion )
        if ( _.random( 100 ) <= QuestHelper.getAdjustedChance( WEREWOLF_TOTEM, 5, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, WEREWOLF_TOTEM, 1, data.isChampion )
        }

        if ( QuestHelper.getQuestItemsCount( player, WEREWOLF_HEAD ) >= 40 ) {
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '30569-04.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( !QuestHelper.hasAtLeastOneQuestItem( player, NECKLACE_OF_VALOR, NECKLACE_OF_COURAGE ) ) {
                    return this.getPath( '30569-08.html' )
                }

                if ( player.getRace() !== Race.ORC ) {
                    return this.getPath( '30569-01.html' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30569-03.htm' : '30569-02.html' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30569-05.html' )

                    case 2:
                        let heads = QuestHelper.getQuestItemsCount( player, WEREWOLF_HEAD )
                        if ( heads < 40 ) {
                            return this.getPath( '30569-05.html' )
                        }

                        let totems = QuestHelper.getQuestItemsCount( player, WEREWOLF_TOTEM )
                        let amount = ( heads * headRewardRate ) + ( totems * totemRewardRate ) + 2300

                        await QuestHelper.giveAdena( player, amount, true )
                        await state.exitQuest( true, true )

                        return this.getPath( totems > 0 ? '30569-07.html' : '30569-06.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}