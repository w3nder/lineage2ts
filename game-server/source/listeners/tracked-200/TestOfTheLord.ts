import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const SEER_SOMAK = 30510
const SEER_MANAKIA = 30515
const TRADER_JAKAL = 30558
const BLACKSMITH_SUMARI = 30564
const FLAME_LORD_KAKAI = 30565
const ATUBA_CHIEF_VARKEES = 30566
const NERUGA_CHIEF_TANTUS = 30567
const URUTU_CHIEF_HATOS = 30568
const DUDA_MARA_CHIEF_TAKUNA = 30641
const GANDI_CHIEF_CHIANTA = 30642
const FIRST_ORC = 30643
const ANCESTOR_MARTANKUS = 30649

const ADENA = 57
const BONE_ARROW = 1341
const ORDEAL_NECKLACE = 3391
const VARKEES_CHARM = 3392
const TANTUS_CHARM = 3393
const HATOS_CHARM = 3394
const TAKUNA_CHARM = 3395
const CHIANTA_CHARM = 3396
const MANAKIAS_ORDERS = 3397
const BREKA_ORC_FANG = 3398
const MANAKIAS_AMULET = 3399
const HUGE_ORC_FANG = 3400
const SUMARIS_LETTER = 3401
const URUTU_BLADE = 3402
const TIMAK_ORC_SKULL = 3403
const SWORD_INTO_SKULL = 3404
const NERUGA_AXE_BLADE = 3405
const AXE_OF_CEREMONY = 3406
const MARSH_SPIDER_FEELER = 3407
const MARSH_SPIDER_FEET = 3408
const HANDIWORK_SPIDER_BROOCH = 3409
const ENCHANTED_MONSTER_CORNEA = 3410
const MONSTER_EYE_WOODCARVING = 3411
const BEAR_FANG_NECKLACE = 3412
const MARTANKUS_CHARM = 3413
const RAGNA_ORC_HEAD = 3414
const RAGNA_CHIEF_NOTICE = 3415
const IMMORTAL_FLAME = 3416

const MARK_OF_LORD = 3390
const DIMENSIONAL_DIAMOND = 7562

const MARSH_SPIDER = 20233
const BREKA_ORC_SHAMAN = 20269
const BREKA_ORC_OVERLORD = 20270
const ENCHANTED_MONSTEREYE = 20564
const TIMAK_ORC = 20583
const TIMAK_ORC_ARCHER = 20584
const TIMAK_ORC_SOLDIER = 20585
const TIMAK_ORC_WARRIOR = 20586
const TIMAK_ORC_SHAMAN = 20587
const TIMAK_ORC_OVERLORD = 20588
const RAGNA_ORC_OVERLORD = 20778
const RAGNA_ORC_SEER = 20779

const minimumLevel = 39
const orcSpawnLocation = new Location( 21036, -107690, -3038 )


export class TestOfTheLord extends ListenerLogic {
    constructor() {
        super( 'Q00232_TestOfTheLord', 'listeners/tracked-200/TestOfTheLord.ts' )
        this.questId = 232
        this.questItemIds = [
            ORDEAL_NECKLACE,
            VARKEES_CHARM,
            TANTUS_CHARM,
            HATOS_CHARM,
            TAKUNA_CHARM,
            CHIANTA_CHARM,
            MANAKIAS_ORDERS,
            BREKA_ORC_FANG,
            MANAKIAS_AMULET,
            HUGE_ORC_FANG,
            SUMARIS_LETTER,
            URUTU_BLADE,
            TIMAK_ORC_SKULL,
            SWORD_INTO_SKULL,
            NERUGA_AXE_BLADE,
            AXE_OF_CEREMONY,
            MARSH_SPIDER_FEELER,
            MARSH_SPIDER_FEET,
            HANDIWORK_SPIDER_BROOCH,
            ENCHANTED_MONSTER_CORNEA,
            MONSTER_EYE_WOODCARVING,
            BEAR_FANG_NECKLACE,
            MARTANKUS_CHARM,
            RAGNA_ORC_HEAD,
            RAGNA_CHIEF_NOTICE,
            IMMORTAL_FLAME,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            MARSH_SPIDER,
            BREKA_ORC_SHAMAN,
            BREKA_ORC_OVERLORD,
            ENCHANTED_MONSTEREYE,
            TIMAK_ORC,
            TIMAK_ORC_ARCHER,
            TIMAK_ORC_SOLDIER,
            TIMAK_ORC_SOLDIER,
            TIMAK_ORC_WARRIOR,
            TIMAK_ORC_SHAMAN,
            TIMAK_ORC_OVERLORD,
            RAGNA_ORC_OVERLORD,
            RAGNA_ORC_SEER,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00232_TestOfTheLord'
    }

    getQuestStartIds(): Array<number> {
        return [ FLAME_LORD_KAKAI ]
    }

    getTalkIds(): Array<number> {
        return [
            FLAME_LORD_KAKAI,
            SEER_SOMAK,
            SEER_MANAKIA,
            TRADER_JAKAL,
            BLACKSMITH_SUMARI,
            ATUBA_CHIEF_VARKEES,
            NERUGA_CHIEF_TANTUS,
            URUTU_CHIEF_HATOS,
            DUDA_MARA_CHIEF_TAKUNA,
            GANDI_CHIEF_CHIANTA,
            FIRST_ORC,
            ANCESTOR_MARTANKUS,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case MARSH_SPIDER:
                if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, TAKUNA_CHARM )
                        && !QuestHelper.hasQuestItem( player, HANDIWORK_SPIDER_BROOCH ) ) {

                    if ( QuestHelper.getQuestItemsCount( player, MARSH_SPIDER_FEELER ) < 10 ) {
                        await this.rewardItems( player, MARSH_SPIDER_FEELER, 2, 10, data.isChampion )
                        return
                    }

                    if ( QuestHelper.getQuestItemsCount( player, MARSH_SPIDER_FEET ) < 10 ) {
                        await this.rewardItems( player, MARSH_SPIDER_FEET, 2, 10, data.isChampion )
                        return
                    }
                }

                return

            case BREKA_ORC_SHAMAN:
            case BREKA_ORC_OVERLORD:
                if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, VARKEES_CHARM, MANAKIAS_ORDERS )
                        && !QuestHelper.hasAtLeastOneQuestItem( player, HUGE_ORC_FANG, MANAKIAS_AMULET )
                        && QuestHelper.getQuestItemsCount( player, BREKA_ORC_FANG ) < 20 ) {
                    await this.rewardItems( player, BREKA_ORC_FANG, 2, 20, data.isChampion )
                }

                return

            case ENCHANTED_MONSTEREYE:
                if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, CHIANTA_CHARM )
                        && !QuestHelper.hasQuestItem( player, MONSTER_EYE_WOODCARVING )
                        && QuestHelper.getQuestItemsCount( player, ENCHANTED_MONSTER_CORNEA ) < 20 ) {
                    await this.rewardItems( player, ENCHANTED_MONSTER_CORNEA, 1, 20, data.isChampion )
                }

                return

            case TIMAK_ORC:
            case TIMAK_ORC_ARCHER:
            case TIMAK_ORC_SOLDIER:
            case TIMAK_ORC_WARRIOR:
            case TIMAK_ORC_SHAMAN:
            case TIMAK_ORC_OVERLORD:
                if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, HATOS_CHARM )
                        && !QuestHelper.hasQuestItem( player, SWORD_INTO_SKULL )
                        && QuestHelper.getQuestItemsCount( player, TIMAK_ORC_SKULL ) < 10 ) {
                    await this.rewardItems( player, TIMAK_ORC_SKULL, 1, 10, data.isChampion )
                }

                return

            case RAGNA_ORC_OVERLORD:
            case RAGNA_ORC_SEER:
                if ( !QuestHelper.hasQuestItem( player, MARTANKUS_CHARM ) ) {
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, RAGNA_CHIEF_NOTICE ) ) {
                    await QuestHelper.giveSingleItem( player, RAGNA_CHIEF_NOTICE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return
                }

                if ( !QuestHelper.hasQuestItem( player, RAGNA_ORC_HEAD ) ) {
                    await QuestHelper.giveSingleItem( player, RAGNA_ORC_HEAD, 1 )

                    state.setConditionWithSound( 5, true )
                    return
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, ORDEAL_NECKLACE, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 92 )

                        return this.getPath( '30565-05b.htm' )
                    }

                    return this.getPath( '30565-05.htm' )
                }

                return

            case '30565-05a.html':
            case '30558-03a.html':
            case '30643-02.html':
            case '30643-03.html':
            case '30649-02.html':
            case '30649-03.html':
                break

            case '30565-08.html':
                if ( QuestHelper.hasQuestItem( player, HUGE_ORC_FANG ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, ORDEAL_NECKLACE, HUGE_ORC_FANG, SWORD_INTO_SKULL, AXE_OF_CEREMONY, HANDIWORK_SPIDER_BROOCH, MONSTER_EYE_WOODCARVING )
                    await QuestHelper.giveSingleItem( player, BEAR_FANG_NECKLACE, 1 )

                    state.setConditionWithSound( 3, true )
                    break
                }
                return

            case '30558-02.html':
                if ( player.getAdena() >= 1000 ) {
                    await QuestHelper.takeSingleItem( player, ADENA, 1000 )
                    await QuestHelper.giveSingleItem( player, NERUGA_AXE_BLADE, 1 )

                    break
                }

                return

            case '30566-02.html':
                await QuestHelper.giveSingleItem( player, VARKEES_CHARM, 1 )
                break

            case '30567-02.html':
                await QuestHelper.giveSingleItem( player, TANTUS_CHARM, 1 )
                break

            case '30568-02.html':
                await QuestHelper.giveSingleItem( player, HATOS_CHARM, 1 )
                break

            case '30641-02.html':
                await QuestHelper.giveSingleItem( player, TAKUNA_CHARM, 1 )
                break

            case '30642-02.html':
                await QuestHelper.giveSingleItem( player, CHIANTA_CHARM, 1 )
                break

            case '30649-04.html':
                if ( QuestHelper.hasQuestItem( player, BEAR_FANG_NECKLACE ) ) {
                    await QuestHelper.takeSingleItem( player, BEAR_FANG_NECKLACE, 1 )
                    await QuestHelper.giveSingleItem( player, MARTANKUS_CHARM, 1 )

                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '30649-07.html':
                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                if ( npc.getSummonedNpcCount() < 1 ) {
                    QuestHelper.addSummonedSpawnAtLocation( npc, FIRST_ORC, orcSpawnLocation, false, 10000 )
                }

                break


            default:
                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== FLAME_LORD_KAKAI ) {
                    break
                }

                if ( player.getRace() !== Race.ORC ) {
                    return this.getPath( '30565-01.html' )
                }

                if ( player.getClassId() !== ClassIdValues.orcShaman.id ) {
                    return this.getPath( '30565-02.html' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30565-03.html' )
                }

                return this.getPath( '30565-04.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case FLAME_LORD_KAKAI:
                        if ( QuestHelper.hasQuestItem( player, ORDEAL_NECKLACE ) ) {
                            if ( QuestHelper.hasQuestItems( player, HUGE_ORC_FANG, SWORD_INTO_SKULL, AXE_OF_CEREMONY, MONSTER_EYE_WOODCARVING, HANDIWORK_SPIDER_BROOCH ) ) {
                                return this.getPath( '30565-07.html' )
                            }

                            return this.getPath( '30565-06.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, BEAR_FANG_NECKLACE ) ) {
                            return this.getPath( '30565-09.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MARTANKUS_CHARM ) ) {
                            return this.getPath( '30565-10.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, IMMORTAL_FLAME ) ) {
                            await QuestHelper.giveAdena( player, 161806, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_LORD, 1 )
                            await QuestHelper.addExpAndSp( player, 894888, 61408 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                            return this.getPath( '30565-11.html' )
                        }

                        break

                    case SEER_SOMAK:
                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, HATOS_CHARM, SUMARIS_LETTER )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, SWORD_INTO_SKULL, URUTU_BLADE ) ) {
                            await QuestHelper.takeSingleItem( player, SUMARIS_LETTER, 1 )
                            await QuestHelper.giveSingleItem( player, URUTU_BLADE, 1 )
                            return this.getPath( '30510-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, HATOS_CHARM, URUTU_BLADE )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, SWORD_INTO_SKULL, SUMARIS_LETTER ) ) {
                            return this.getPath( '30510-02.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, SWORD_INTO_SKULL )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, HATOS_CHARM, URUTU_BLADE, SUMARIS_LETTER ) ) {
                            return this.getPath( '30510-03.html' )
                        }

                        break

                    case SEER_MANAKIA:
                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, VARKEES_CHARM )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, HUGE_ORC_FANG, MANAKIAS_AMULET, MANAKIAS_ORDERS ) ) {
                            await QuestHelper.giveSingleItem( player, MANAKIAS_ORDERS, 1 )
                            return this.getPath( '30515-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, VARKEES_CHARM, ORDEAL_NECKLACE, MANAKIAS_ORDERS )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, HUGE_ORC_FANG, MANAKIAS_AMULET ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, BREKA_ORC_FANG ) < 20 ) {
                                return this.getPath( '30515-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, MANAKIAS_ORDERS, BREKA_ORC_FANG )
                            await QuestHelper.giveSingleItem( player, MANAKIAS_AMULET, 1 )

                            return this.getPath( '30515-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, VARKEES_CHARM, MANAKIAS_AMULET )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, HUGE_ORC_FANG, MANAKIAS_ORDERS ) ) {
                            return this.getPath( '30515-04.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, HUGE_ORC_FANG )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, VARKEES_CHARM, MANAKIAS_AMULET, MANAKIAS_ORDERS ) ) {
                            return this.getPath( '30515-05.html' )
                        }

                        break

                    case TRADER_JAKAL:
                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, TANTUS_CHARM )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, AXE_OF_CEREMONY, NERUGA_AXE_BLADE ) ) {
                            if ( player.getAdena() >= 1000 ) {
                                return this.getPath( '30558-01.html' )
                            }

                            return this.getPath( '30558-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, TANTUS_CHARM, NERUGA_AXE_BLADE )
                                && !QuestHelper.hasQuestItem( player, AXE_OF_CEREMONY ) ) {
                            return this.getPath( '30558-04.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, AXE_OF_CEREMONY )
                                && !QuestHelper.hasQuestItem( player, TANTUS_CHARM ) ) {
                            return this.getPath( '30558-05.html' )
                        }

                        break

                    case BLACKSMITH_SUMARI:
                        if ( QuestHelper.hasQuestItems( player, HATOS_CHARM, ORDEAL_NECKLACE )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, SWORD_INTO_SKULL, URUTU_BLADE, SUMARIS_LETTER ) ) {
                            await QuestHelper.giveSingleItem( player, SUMARIS_LETTER, 1 )
                            return this.getPath( '30564-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, HATOS_CHARM, SUMARIS_LETTER )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, SWORD_INTO_SKULL, URUTU_BLADE ) ) {
                            return this.getPath( '30564-02.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, HATOS_CHARM, URUTU_BLADE )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, SUMARIS_LETTER, SWORD_INTO_SKULL ) ) {
                            return this.getPath( '30564-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, SWORD_INTO_SKULL )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, HATOS_CHARM, URUTU_BLADE, SUMARIS_LETTER ) ) {
                            return this.getPath( '30564-04.html' )
                        }

                        break

                    case ATUBA_CHIEF_VARKEES:
                        if ( QuestHelper.hasQuestItem( player, ORDEAL_NECKLACE )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, HUGE_ORC_FANG, VARKEES_CHARM ) ) {
                            return this.getPath( '30566-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, VARKEES_CHARM )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, HUGE_ORC_FANG, MANAKIAS_AMULET ) ) {
                            return this.getPath( '30566-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, VARKEES_CHARM, MANAKIAS_AMULET )
                                && !QuestHelper.hasQuestItem( player, HUGE_ORC_FANG ) ) {
                            await QuestHelper.takeMultipleItems( player, 1, VARKEES_CHARM, MANAKIAS_AMULET )
                            await QuestHelper.giveSingleItem( player, HUGE_ORC_FANG, 1 )

                            if ( QuestHelper.hasQuestItems( player, AXE_OF_CEREMONY, SWORD_INTO_SKULL, HANDIWORK_SPIDER_BROOCH, MONSTER_EYE_WOODCARVING ) ) {
                                state.setConditionWithSound( 2, true )
                            }

                            return this.getPath( '30566-04.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, HUGE_ORC_FANG )
                                && !QuestHelper.hasQuestItem( player, VARKEES_CHARM ) ) {
                            return this.getPath( '30566-05.html' )
                        }

                        break

                    case NERUGA_CHIEF_TANTUS:
                        if ( QuestHelper.hasQuestItem( player, ORDEAL_NECKLACE ) && !QuestHelper.hasAtLeastOneQuestItem( player, AXE_OF_CEREMONY, TANTUS_CHARM ) ) {
                            return this.getPath( '30567-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, TANTUS_CHARM )
                                && !QuestHelper.hasQuestItem( player, AXE_OF_CEREMONY ) ) {
                            if ( !QuestHelper.hasQuestItem( player, NERUGA_AXE_BLADE )
                                    || QuestHelper.getQuestItemsCount( player, BONE_ARROW ) < 1000 ) {
                                return this.getPath( '30567-03.html' )
                            }

                            await QuestHelper.takeSingleItem( player, BONE_ARROW, 1000 )
                            await QuestHelper.takeMultipleItems( player, -1, TANTUS_CHARM, NERUGA_AXE_BLADE )
                            await QuestHelper.giveSingleItem( player, AXE_OF_CEREMONY, 1 )

                            if ( QuestHelper.hasQuestItems( player, HUGE_ORC_FANG, SWORD_INTO_SKULL, HANDIWORK_SPIDER_BROOCH, MONSTER_EYE_WOODCARVING ) ) {
                                state.setConditionWithSound( 2, true )
                            }

                            return this.getPath( '30567-04.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, AXE_OF_CEREMONY )
                                && !QuestHelper.hasQuestItem( player, TANTUS_CHARM ) ) {
                            return this.getPath( '30567-05.html' )
                        }

                        break

                    case URUTU_CHIEF_HATOS:
                        if ( QuestHelper.hasQuestItem( player, ORDEAL_NECKLACE )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, SWORD_INTO_SKULL, HATOS_CHARM ) ) {
                            return this.getPath( '30568-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, HATOS_CHARM )
                                && !QuestHelper.hasQuestItem( player, SWORD_INTO_SKULL ) ) {
                            if ( QuestHelper.hasQuestItem( player, URUTU_BLADE )
                                    && QuestHelper.getQuestItemsCount( player, TIMAK_ORC_SKULL ) >= 10 ) {
                                await QuestHelper.takeMultipleItems( player, -1, HATOS_CHARM, URUTU_BLADE, TIMAK_ORC_SKULL )
                                await QuestHelper.giveSingleItem( player, SWORD_INTO_SKULL, 1 )

                                if ( QuestHelper.hasQuestItems( player, HUGE_ORC_FANG, AXE_OF_CEREMONY, HANDIWORK_SPIDER_BROOCH, MONSTER_EYE_WOODCARVING ) ) {
                                    state.setConditionWithSound( 2, true )
                                }

                                return this.getPath( '30568-04.html' )
                            }

                            return this.getPath( '30568-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, SWORD_INTO_SKULL )
                                && !QuestHelper.hasQuestItem( player, HATOS_CHARM ) ) {
                            return this.getPath( '30568-05.html' )
                        }

                        break

                    case DUDA_MARA_CHIEF_TAKUNA:
                        if ( QuestHelper.hasQuestItem( player, ORDEAL_NECKLACE )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, HANDIWORK_SPIDER_BROOCH, TAKUNA_CHARM ) ) {
                            return this.getPath( '30641-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, TAKUNA_CHARM )
                                && !QuestHelper.hasQuestItem( player, HANDIWORK_SPIDER_BROOCH ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, MARSH_SPIDER_FEELER ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, MARSH_SPIDER_FEET ) >= 10 ) {
                                await QuestHelper.takeMultipleItems( player, -1, TAKUNA_CHARM, MARSH_SPIDER_FEELER, MARSH_SPIDER_FEET )
                                await QuestHelper.giveSingleItem( player, HANDIWORK_SPIDER_BROOCH, 1 )

                                if ( QuestHelper.hasQuestItems( player, HUGE_ORC_FANG, AXE_OF_CEREMONY, SWORD_INTO_SKULL, MONSTER_EYE_WOODCARVING ) ) {
                                    state.setConditionWithSound( 2, true )
                                }

                                return this.getPath( '30641-04.html' )
                            }

                            return this.getPath( '30641-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, HANDIWORK_SPIDER_BROOCH )
                                && !QuestHelper.hasQuestItem( player, TAKUNA_CHARM ) ) {
                            return this.getPath( '30641-05.html' )
                        }
                        break

                    case GANDI_CHIEF_CHIANTA:
                        if ( QuestHelper.hasQuestItem( player, ORDEAL_NECKLACE )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, MONSTER_EYE_WOODCARVING, CHIANTA_CHARM ) ) {
                            return this.getPath( '30642-01.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, CHIANTA_CHARM )
                                && !QuestHelper.hasQuestItem( player, MONSTER_EYE_WOODCARVING ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, ENCHANTED_MONSTER_CORNEA ) < 20 ) {
                                return this.getPath( '30642-03.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, CHIANTA_CHARM, ENCHANTED_MONSTER_CORNEA )
                            await QuestHelper.giveSingleItem( player, MONSTER_EYE_WOODCARVING, 1 )

                            if ( QuestHelper.hasQuestItems( player, HUGE_ORC_FANG, AXE_OF_CEREMONY, SWORD_INTO_SKULL, HANDIWORK_SPIDER_BROOCH ) ) {
                                state.setConditionWithSound( 2, true )
                            }

                            return this.getPath( '30642-04.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, ORDEAL_NECKLACE, MONSTER_EYE_WOODCARVING )
                                && !QuestHelper.hasQuestItem( player, CHIANTA_CHARM ) ) {
                            return this.getPath( '30642-05.html' )
                        }

                        break

                    case FIRST_ORC:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, MARTANKUS_CHARM, IMMORTAL_FLAME ) ) {
                            state.setConditionWithSound( 7, true )
                            return this.getPath( '30643-01.html' )
                        }

                        break

                    case ANCESTOR_MARTANKUS:
                        if ( QuestHelper.hasQuestItem( player, BEAR_FANG_NECKLACE ) ) {
                            return this.getPath( '30649-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MARTANKUS_CHARM )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, RAGNA_CHIEF_NOTICE, RAGNA_ORC_HEAD ) ) {
                            return this.getPath( '30649-05.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, MARTANKUS_CHARM, RAGNA_CHIEF_NOTICE, RAGNA_ORC_HEAD ) ) {
                            await QuestHelper.takeMultipleItems( player, 1, MARTANKUS_CHARM, RAGNA_ORC_HEAD, RAGNA_CHIEF_NOTICE )
                            await QuestHelper.giveSingleItem( player, IMMORTAL_FLAME, 1 )

                            state.setConditionWithSound( 6, true )
                            return this.getPath( '30649-06.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, IMMORTAL_FLAME ) ) {
                            let npc = L2World.getObjectById( data.characterId ) as L2Npc
                            if ( npc.getSummonedNpcCount() < 1 ) {
                                QuestHelper.addSummonedSpawnAtLocation( npc, FIRST_ORC, orcSpawnLocation, false, 10000 )
                            }

                            return this.getPath( '30649-08.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === FLAME_LORD_KAKAI ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardItems( player: L2PcInstance, itemId: number, amount: number, maximumCount: number, isFromChampion: boolean ): Promise<void> {
        await QuestHelper.rewardSingleQuestItem( player, itemId, amount, isFromChampion )

        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= maximumCount ) {
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}