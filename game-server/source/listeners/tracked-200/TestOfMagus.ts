import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const PARINA = 30391
const EARTH_SNAKE = 30409
const FLAME_SALAMANDER = 30411
const WIND_SYLPH = 30412
const WATER_UNDINE = 30413
const ELDER_CASIAN = 30612
const BARD_RUKAL = 30629

const RUKALS_LETTER = 2841
const PARINAS_LETTER = 2842
const LILAC_CHARM = 2843
const GOLDEN_SEED_1ST = 2844
const GOLDEN_SEED_2ND = 2845
const GOLDEN_SEED_3RD = 2846
const SCORE_OF_ELEMENTS = 2847
const DAZZLING_DROP = 2848
const FLAME_CRYSTAL = 2849
const HARPYS_FEATHER = 2850
const WYRMS_WINGBONE = 2851
const WINDSUS_MANE = 2852
const ENCHANTED_MONSTER_EYE_SHELL = 2853
const ENCHANTED_GOLEM_POWDER = 2854
const ENCHANTED_IRON_GOLEM_SCRAP = 2855
const TONE_OF_WATER = 2856
const TONE_OF_FIRE = 2857
const TONE_OF_WIND = 2858
const TONE_OF_EARTH = 2859
const SALAMANDER_CHARM = 2860
const SYLPH_CHARM = 2861
const UNDINE_CHARM = 2862
const SERPENT_CHARM = 2863

const MARK_OF_MAGUS = 2840
const DIMENSIONAL_DIAMOND = 7562

const HARPY = 20145
const MARSH_STAKATO = 20157
const WYRM = 20176
const MARSH_STAKATO_WORKER = 20230
const TOAD_LORD = 20231
const MARSH_STAKATO_SOLDIER = 20232
const MARSH_STAKATO_DRONE = 20234
const WINDSUS = 20553
const ENCHANTED_MONSTEREYE = 20564
const ENCHANTED_STOLEN_GOLEM = 20565
const ENCHANTED_IRON_GOLEM = 20566

const SINGING_FLOWER_PHANTASM = 27095
const SINGING_FLOWER_NIGTMATE = 27096
const SINGING_FLOWER_DARKLING = 27097
const GHOST_FIRE = 27098

const minimumLevel = 39

export class TestOfMagus extends ListenerLogic {
    constructor() {
        super( 'Q00228_TestOfMagus', 'listeners/tracked-200/TestOfMagus.ts' )
        this.questId = 228
        this.questItemIds = [
            RUKALS_LETTER,
            PARINAS_LETTER,
            LILAC_CHARM,
            GOLDEN_SEED_1ST,
            GOLDEN_SEED_2ND,
            GOLDEN_SEED_3RD,
            SCORE_OF_ELEMENTS,
            DAZZLING_DROP,
            FLAME_CRYSTAL,
            HARPYS_FEATHER,
            WYRMS_WINGBONE,
            WINDSUS_MANE,
            ENCHANTED_MONSTER_EYE_SHELL,
            ENCHANTED_GOLEM_POWDER,
            ENCHANTED_IRON_GOLEM_SCRAP,
            TONE_OF_WATER,
            TONE_OF_FIRE,
            TONE_OF_WIND,
            TONE_OF_EARTH,
            SALAMANDER_CHARM,
            SYLPH_CHARM,
            UNDINE_CHARM,
            SERPENT_CHARM,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            HARPY,
            MARSH_STAKATO,
            WYRM,
            MARSH_STAKATO_WORKER,
            TOAD_LORD,
            MARSH_STAKATO_SOLDIER,
            MARSH_STAKATO_DRONE,
            WINDSUS,
            ENCHANTED_MONSTEREYE,
            ENCHANTED_STOLEN_GOLEM,
            ENCHANTED_IRON_GOLEM,
            SINGING_FLOWER_PHANTASM,
            SINGING_FLOWER_NIGTMATE,
            SINGING_FLOWER_DARKLING,
            GHOST_FIRE,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00228_TestOfMagus'
    }

    getQuestStartIds(): Array<number> {
        return [ BARD_RUKAL ]
    }

    getTalkIds(): Array<number> {
        return [ BARD_RUKAL, PARINA, EARTH_SNAKE, FLAME_SALAMANDER, WIND_SYLPH, WATER_UNDINE, ELDER_CASIAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( npc.getId() ) {
            case HARPY:
                if ( QuestHelper.hasQuestItems( player, SCORE_OF_ELEMENTS, SYLPH_CHARM )
                        && QuestHelper.getQuestItemsCount( player, HARPYS_FEATHER ) < 20 ) {
                    await this.rewardItems( player, HARPYS_FEATHER, 20, data.isChampion )
                }

                return

            case MARSH_STAKATO:
            case MARSH_STAKATO_WORKER:
            case TOAD_LORD:
            case MARSH_STAKATO_SOLDIER:
            case MARSH_STAKATO_DRONE:
                if ( QuestHelper.hasQuestItems( player, SCORE_OF_ELEMENTS, UNDINE_CHARM )
                        && QuestHelper.getQuestItemsCount( player, DAZZLING_DROP ) < 20 ) {
                    await this.rewardItems( player, DAZZLING_DROP, 20, data.isChampion )
                }

                return

            case WYRM:
                if ( QuestHelper.hasQuestItems( player, SCORE_OF_ELEMENTS, SYLPH_CHARM )
                        && QuestHelper.getQuestItemsCount( player, WYRMS_WINGBONE ) < 10
                        && Math.random() < 0.5 ) {
                    await this.rewardItems( player, WYRMS_WINGBONE, 10, data.isChampion )
                }

                return

            case WINDSUS:
                if ( QuestHelper.hasQuestItems( player, SCORE_OF_ELEMENTS, SYLPH_CHARM )
                        && QuestHelper.getQuestItemsCount( player, WINDSUS_MANE ) < 10
                        && Math.random() < 0.5 ) {
                    await this.rewardItems( player, WINDSUS_MANE, 10, data.isChampion )
                }

                return

            case ENCHANTED_MONSTEREYE:
                if ( QuestHelper.hasQuestItems( player, SCORE_OF_ELEMENTS, SERPENT_CHARM )
                        && QuestHelper.getQuestItemsCount( player, ENCHANTED_MONSTER_EYE_SHELL ) < 10 ) {
                    await this.rewardItems( player, ENCHANTED_MONSTER_EYE_SHELL, 10, data.isChampion )
                }

                return

            case ENCHANTED_STOLEN_GOLEM:
                if ( QuestHelper.hasQuestItems( player, SCORE_OF_ELEMENTS, SERPENT_CHARM )
                        && QuestHelper.getQuestItemsCount( player, ENCHANTED_GOLEM_POWDER ) < 10 ) {
                    await this.rewardItems( player, ENCHANTED_GOLEM_POWDER, 10, data.isChampion )
                }

                return

            case ENCHANTED_IRON_GOLEM:
                if ( QuestHelper.hasQuestItems( player, SCORE_OF_ELEMENTS, SERPENT_CHARM )
                        && QuestHelper.getQuestItemsCount( player, ENCHANTED_IRON_GOLEM_SCRAP ) < 10 ) {
                    await this.rewardItems( player, ENCHANTED_IRON_GOLEM_SCRAP, 10, data.isChampion )
                }

                return

            case SINGING_FLOWER_PHANTASM:
                if ( QuestHelper.hasQuestItem( player, LILAC_CHARM ) && !QuestHelper.hasQuestItem( player, GOLDEN_SEED_1ST ) ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.I_AM_A_TREE_OF_NOTHING_A_TREE_THAT_KNOWS_WHERE_TO_RETURN )
                    await QuestHelper.giveSingleItem( player, GOLDEN_SEED_1ST, 1 )

                    this.tryToProgressQuest( player, state )
                }

                return

            case SINGING_FLOWER_NIGTMATE:
                if ( QuestHelper.hasQuestItem( player, LILAC_CHARM ) && !QuestHelper.hasQuestItem( player, GOLDEN_SEED_2ND ) ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.I_AM_A_CREATURE_THAT_SHOWS_THE_TRUTH_OF_THE_PLACE_DEEP_IN_MY_HEART )
                    await QuestHelper.giveSingleItem( player, GOLDEN_SEED_2ND, 1 )

                    this.tryToProgressQuest( player, state )
                }

                return

            case SINGING_FLOWER_DARKLING:
                if ( QuestHelper.hasQuestItem( player, LILAC_CHARM ) && !QuestHelper.hasQuestItem( player, GOLDEN_SEED_3RD ) ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.I_AM_A_MIRROR_OF_DARKNESS_A_VIRTUAL_IMAGE_OF_DARKNESS )
                    await QuestHelper.giveSingleItem( player, GOLDEN_SEED_3RD, 1 )

                    this.tryToProgressQuest( player, state )
                }

                return

            case GHOST_FIRE:
                if ( QuestHelper.hasQuestItems( player, SCORE_OF_ELEMENTS, SALAMANDER_CHARM )
                        && QuestHelper.getQuestItemsCount( player, FLAME_CRYSTAL ) < 5
                        && Math.random() < 0.5 ) {
                    await this.rewardItems( player, FLAME_CRYSTAL, 5, data.isChampion )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, RUKALS_LETTER, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 122 )

                        return this.getPath( '30629-04a.htm' )
                    }

                    return this.getPath( '30629-04.htm' )
                }

                return

            case '30629-09.html':
            case '30409-02.html':
                break

            case '30629-10.html':
                if ( QuestHelper.hasQuestItem( player, GOLDEN_SEED_3RD ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, LILAC_CHARM, GOLDEN_SEED_1ST, GOLDEN_SEED_2ND, GOLDEN_SEED_3RD )
                    await QuestHelper.giveSingleItem( player, SCORE_OF_ELEMENTS, 1 )

                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '30391-02.html':
                if ( QuestHelper.hasQuestItem( player, RUKALS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, RUKALS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, PARINAS_LETTER, 1 )
                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '30409-03.html':
                await QuestHelper.giveSingleItem( player, SERPENT_CHARM, 1 )
                break

            case '30412-02.html':
                await QuestHelper.giveSingleItem( player, SYLPH_CHARM, 1 )
                break

            case '30612-02.html':
                await QuestHelper.takeSingleItem( player, PARINAS_LETTER, 1 )
                await QuestHelper.giveSingleItem( player, LILAC_CHARM, 1 )

                state.setConditionWithSound( 3, true )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === BARD_RUKAL ) {
                    if ( ![ ClassIdValues.wizard.id, ClassIdValues.elvenWizard.id, ClassIdValues.darkWizard.id ].includes( player.getClassId() ) ) {
                        return this.getPath( '30629-01.html' )
                    }

                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30629-02.html' )
                    }

                    return this.getPath( '30629-03.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case BARD_RUKAL:
                        if ( QuestHelper.hasQuestItem( player, RUKALS_LETTER ) ) {
                            return this.getPath( '30629-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PARINAS_LETTER ) ) {
                            return this.getPath( '30629-06.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LILAC_CHARM ) ) {
                            if ( QuestHelper.hasQuestItems( player, GOLDEN_SEED_1ST, GOLDEN_SEED_2ND, GOLDEN_SEED_3RD ) ) {
                                return this.getPath( '30629-08.html' )
                            }

                            return this.getPath( '30629-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SCORE_OF_ELEMENTS ) ) {
                            if ( QuestHelper.hasQuestItems( player, TONE_OF_WATER, TONE_OF_FIRE, TONE_OF_WIND, TONE_OF_EARTH ) ) {
                                await QuestHelper.giveAdena( player, 372154, true )
                                await QuestHelper.giveSingleItem( player, MARK_OF_MAGUS, 1 )
                                await QuestHelper.addExpAndSp( player, 2058244, 141240 )
                                await state.exitQuest( false, true )

                                player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                                return this.getPath( '30629-12.html' )
                            }

                            return this.getPath( '30629-11.html' )
                        }

                        break

                    case PARINA:
                        if ( QuestHelper.hasQuestItem( player, RUKALS_LETTER ) ) {
                            return this.getPath( '30391-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PARINAS_LETTER ) ) {
                            return this.getPath( '30391-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LILAC_CHARM ) ) {
                            return this.getPath( '30391-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SCORE_OF_ELEMENTS ) ) {
                            return this.getPath( '30391-05.html' )
                        }

                        break

                    case EARTH_SNAKE:
                        if ( !QuestHelper.hasQuestItem( player, SCORE_OF_ELEMENTS ) ) {
                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, TONE_OF_EARTH, SERPENT_CHARM ) ) {
                            return this.getPath( '30409-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SERPENT_CHARM ) ) {
                            if ( ( QuestHelper.getQuestItemsCount( player, ENCHANTED_MONSTER_EYE_SHELL ) >= 10 ) && ( QuestHelper.getQuestItemsCount( player, ENCHANTED_GOLEM_POWDER ) >= 10 ) && ( QuestHelper.getQuestItemsCount( player, ENCHANTED_IRON_GOLEM_SCRAP ) >= 10 ) ) {
                                await QuestHelper.takeMultipleItems( player, -1, ENCHANTED_MONSTER_EYE_SHELL, ENCHANTED_GOLEM_POWDER, ENCHANTED_IRON_GOLEM_SCRAP, SERPENT_CHARM )
                                await QuestHelper.giveSingleItem( player, TONE_OF_EARTH, 1 )

                                if ( QuestHelper.hasQuestItems( player, TONE_OF_FIRE, TONE_OF_WATER, TONE_OF_WIND ) ) {
                                    state.setConditionWithSound( 6, true )
                                }

                                return this.getPath( '30409-05.html' )
                            }

                            return this.getPath( '30409-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TONE_OF_EARTH ) && !QuestHelper.hasQuestItem( player, SERPENT_CHARM ) ) {
                            return this.getPath( '30409-06.html' )
                        }

                        break

                    case FLAME_SALAMANDER:
                        if ( !QuestHelper.hasQuestItem( player, SCORE_OF_ELEMENTS ) ) {
                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, TONE_OF_FIRE, SALAMANDER_CHARM ) ) {
                            await QuestHelper.giveSingleItem( player, SALAMANDER_CHARM, 1 )
                            return this.getPath( '30411-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SALAMANDER_CHARM ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, FLAME_CRYSTAL ) < 5 ) {
                                return this.getPath( '30411-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, FLAME_CRYSTAL, SALAMANDER_CHARM )
                            await QuestHelper.giveSingleItem( player, TONE_OF_FIRE, 1 )

                            if ( QuestHelper.hasQuestItems( player, TONE_OF_WATER, TONE_OF_WIND, TONE_OF_EARTH ) ) {
                                state.setConditionWithSound( 6, true )
                            }

                            return this.getPath( '30411-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TONE_OF_FIRE ) && !QuestHelper.hasQuestItem( player, SALAMANDER_CHARM ) ) {
                            return this.getPath( '30411-04.html' )
                        }

                        break

                    case WIND_SYLPH:
                        if ( !QuestHelper.hasQuestItem( player, SCORE_OF_ELEMENTS ) ) {
                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, TONE_OF_WIND, SYLPH_CHARM ) ) {
                            return this.getPath( '30412-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SYLPH_CHARM ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, HARPYS_FEATHER ) >= 20
                                    && QuestHelper.getQuestItemsCount( player, WYRMS_WINGBONE ) >= 10
                                    && QuestHelper.getQuestItemsCount( player, WINDSUS_MANE ) >= 10 ) {
                                await QuestHelper.takeMultipleItems( player, -1, HARPYS_FEATHER, WYRMS_WINGBONE, WINDSUS_MANE, SYLPH_CHARM )
                                await QuestHelper.giveSingleItem( player, TONE_OF_WIND, 1 )

                                if ( QuestHelper.hasQuestItems( player, TONE_OF_WATER, TONE_OF_FIRE, TONE_OF_EARTH ) ) {
                                    state.setConditionWithSound( 6, true )
                                }

                                return this.getPath( '30412-04.html' )
                            }

                            return this.getPath( '30412-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TONE_OF_WIND ) && !QuestHelper.hasQuestItem( player, SYLPH_CHARM ) ) {
                            return this.getPath( '30412-05.html' )
                        }

                        break

                    case WATER_UNDINE:
                        if ( !QuestHelper.hasQuestItem( player, SCORE_OF_ELEMENTS ) ) {
                            break
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, TONE_OF_WATER, UNDINE_CHARM ) ) {
                            await QuestHelper.giveSingleItem( player, UNDINE_CHARM, 1 )
                            return this.getPath( '30413-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, UNDINE_CHARM ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, DAZZLING_DROP ) < 20 ) {
                                return this.getPath( '30413-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, DAZZLING_DROP, UNDINE_CHARM )
                            await QuestHelper.giveSingleItem( player, TONE_OF_WATER, 1 )

                            if ( QuestHelper.hasQuestItems( player, TONE_OF_FIRE, TONE_OF_WIND, TONE_OF_EARTH ) ) {
                                state.setConditionWithSound( 6, true )
                            }

                            return this.getPath( '30413-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TONE_OF_WATER ) && !QuestHelper.hasQuestItem( player, UNDINE_CHARM ) ) {
                            return this.getPath( '30413-04.html' )
                        }

                        break

                    case ELDER_CASIAN:
                        if ( QuestHelper.hasQuestItem( player, PARINAS_LETTER ) ) {
                            return this.getPath( '30612-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LILAC_CHARM ) ) {
                            if ( QuestHelper.hasQuestItems( player, GOLDEN_SEED_1ST, GOLDEN_SEED_2ND, GOLDEN_SEED_3RD ) ) {
                                return this.getPath( '30612-04.html' )
                            }

                            return this.getPath( '30612-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SCORE_OF_ELEMENTS ) ) {
                            return this.getPath( '30612-05.html' )
                        }

                        break

                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === BARD_RUKAL ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardItems( player: L2PcInstance, itemId: number, maxQuantity: number, isFromChampion: boolean ): Promise<void> {
        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, isFromChampion )
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= maxQuantity ) {
            return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }

        return player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    tryToProgressQuest( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.hasQuestItems( player, GOLDEN_SEED_1ST, GOLDEN_SEED_2ND, GOLDEN_SEED_3RD ) ) {
            state.setConditionWithSound( 4 )
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
    }
}