import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'
import aigle from 'aigle'

const LADD = 30721
const CARADINE = 31740
const OSSIAN = 31741
const PILGRIM_OF_SPLENDOR = 21541
const JUDGE_OF_SPLENDOR = 21544
const BARAKIEL = 25325

const CARADINE_LETTER = 7678
const CARADINE_LETTER_LAST = 7679
const WATERBINDER = 7591
const EVERGREEN = 7592
const RAIN_SONG = 7593
const RELIC_BOX = 7594
const FRAGMENTS = 21725

const CHANCE_FOR_DROP = 30
const CHANCE_FOR_DROP_FRAGMENTS = 60
const variableNames = {
    awaitsWaterbinder: 'aw',
    awaitsEvergreen: 'ae',
}
const minimumLevel = 65

export class PossessorOfAPreciousSoulPartThree extends ListenerLogic {
    constructor() {
        super( 'Q00246_PossessorOfAPreciousSoulPartThree', 'listeners/tracked-200/PossessorOfAPreciousSoulPartThree.ts' )
        this.questId = 246
        this.questItemIds = [
            WATERBINDER,
            EVERGREEN,
            FRAGMENTS,
            RAIN_SONG,
            RELIC_BOX,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            21535, // Signet of Splendor
            21536, // Crown of Splendor
            21537, // Fang of Splendor
            21538, // Fang of Splendor
            21539, // Wailing of Splendor
            21540, // Wailing of Splendor
            PILGRIM_OF_SPLENDOR,
            JUDGE_OF_SPLENDOR,
            BARAKIEL,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00246_PossessorOfAPreciousSoul3'
    }

    getQuestStartIds(): Array<number> {
        return [ CARADINE ]
    }

    getTalkIds(): Array<number> {
        return [ LADD, CARADINE, OSSIAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        switch ( data.npcId ) {
            case PILGRIM_OF_SPLENDOR:
                await this.tryToProgressQuest( data, variableNames.awaitsWaterbinder, WATERBINDER, EVERGREEN )
                return

            case JUDGE_OF_SPLENDOR:
                await this.tryToProgressQuest( data, variableNames.awaitsEvergreen, EVERGREEN, WATERBINDER )
                return

            case BARAKIEL:
                await this.onBarakielKilled( data )
                return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( !state.isCondition( 4 ) || QuestHelper.getQuestItemsCount( player, FRAGMENTS ) >= 100 || _.random( 100 ) >= CHANCE_FOR_DROP_FRAGMENTS ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, FRAGMENTS, 1, data.isChampion )
        if ( QuestHelper.getQuestItemsCount( player, FRAGMENTS ) >= 100 ) {
            state.setConditionWithSound( 5, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( !player.isSubClassActive() ) {
            return this.getPath( 'no_sub.html' )
        }

        switch ( data.eventName ) {
            case '31740-4.html':
                if ( state.isCreated() ) {
                    await QuestHelper.takeSingleItem( player, CARADINE_LETTER, -1 )
                    state.startQuest()
                }

                break

            case '31741-2.html':
                if ( state.isStarted() && state.isCondition( 1 ) ) {
                    state.setVariable( variableNames.awaitsWaterbinder, true )
                    state.setVariable( variableNames.awaitsEvergreen, true )
                    state.setConditionWithSound( 2, true )
                }

                break

            case '31741-5.html':
                if ( state.isCondition( 3 ) && QuestHelper.hasQuestItems( player, WATERBINDER, EVERGREEN ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, WATERBINDER, EVERGREEN )
                    state.setConditionWithSound( 4, true )
                }

                break

            case '31741-9.html':
                if ( state.isCondition( 5 )
                        && ( QuestHelper.hasQuestItem( player, RAIN_SONG ) || QuestHelper.getQuestItemsCount( player, FRAGMENTS ) >= 100 ) ) {
                    await QuestHelper.takeMultipleItems( player, -1, RAIN_SONG, FRAGMENTS )
                    await QuestHelper.giveSingleItem( player, RELIC_BOX, 1 )
                    state.setConditionWithSound( 6, true )

                    break
                }

                return this.getPath( '31741-8.html' )

            case '30721-2.html':
                if ( state.isCondition( 6 ) && QuestHelper.hasQuestItem( player, RELIC_BOX ) ) {
                    await QuestHelper.takeSingleItem( player, RELIC_BOX, -1 )
                    await QuestHelper.giveSingleItem( player, CARADINE_LETTER_LAST, 1 )
                    await QuestHelper.addExpAndSp( player, 719843, 0 )
                    await state.exitQuest( false, true )
                }

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        if ( state.isStarted() && !player.isSubClassActive() ) {
            return this.getPath( 'no_sub.html' )
        }

        switch ( data.characterNpcId ) {
            case CARADINE:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00242_PossessorOfAPreciousSoulPartTwo' ) ? '31740-1.htm' : '31740-2.html' )

                    case QuestStateValues.STARTED:
                        return this.getPath( '31740-5.html' )
                }

                break

            case OSSIAN:
                switch ( state.getState() ) {
                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31741-1.html' )

                            case 2:
                                return this.getPath( '31741-4.html' )

                            case 3:
                                if ( QuestHelper.hasQuestItems( player, WATERBINDER, EVERGREEN ) ) {
                                    return this.getPath( '31741-3.html' )
                                }

                                break

                            case 4:
                                return this.getPath( '31741-8.html' )

                            case 5:
                                if ( QuestHelper.hasQuestItem( player, RAIN_SONG ) || ( QuestHelper.getQuestItemsCount( player, FRAGMENTS ) >= 100 ) ) {
                                    return this.getPath( '31741-7.html' )
                                }

                                return this.getPath( '31741-8.html' )

                            case 6:
                                if ( QuestHelper.hasQuestItem( player, RELIC_BOX ) ) {
                                    return this.getPath( '31741-11.html' )
                                }

                                break
                        }
                }

                break

            case LADD:
                switch ( state.getState() ) {
                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 6 ) ) {
                            return this.getPath( '30721-1.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onBarakielKilled( data: AttackableKillEvent ): Promise<void> {
        let player = L2World.getPlayer( data.playerId )
        let members = player.getParty() ? player.getParty().getMembers() : [ data.playerId ]
        let questName = this.getName()

        await aigle.resolve( members ).each( async ( playerId: number ) => {
            let state = QuestStateCache.getQuestState( playerId, questName, false )
            if ( !state ) {
                return
            }

            let currentPlayer = L2World.getPlayer( playerId )
            if ( state.isCondition( 4 ) && !QuestHelper.hasQuestItem( currentPlayer, RAIN_SONG ) ) {
                await QuestHelper.giveSingleItem( currentPlayer, RAIN_SONG, 1 )
                state.setConditionWithSound( 5, true )
            }
        } )
    }

    async tryToProgressQuest( data: AttackableKillEvent, variableName: string, giveItemId: number, progressItemId: number ): Promise<void> {
        let player = this.getRandomPartyMemberForVariable( L2World.getPlayer( data.playerId ), variableName, true )
        if ( _.random( 100 ) >= QuestHelper.getAdjustedChance( giveItemId, CHANCE_FOR_DROP, data.isChampion ) ) {
            return
        }

        await QuestHelper.giveSingleItem( player, giveItemId, 1 )
        let state = QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )

        state.unsetVariable( variableName )

        if ( QuestHelper.hasQuestItem( player, progressItemId ) ) {
            state.setConditionWithSound( 3, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}