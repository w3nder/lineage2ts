import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { PlayerRadarCache } from '../../gameService/cache/PlayerRadarCache'

const CAPTAIN_ALEX = 30291
const TYRA = 30420
const TREE = 30627
const STRONG_WOODEN_CHEST = 30628
const MASTER_LUTHER = 30690
const MILITIAMAN_LEIRYNN = 30728
const DRUNKARD_BORYS = 30729
const BODYGUARD_JAX = 30730

const LUTHERS_LETTER = 2784
const ALEXS_WARRANT = 2785
const LEIRYNNS_1ST_ORDER = 2786
const DELU_TOTEM = 2787
const LEIRYNNS_2ND_ORDER = 2788
const CHIEF_KALKIS_FANG = 2789
const LEIRYNNS_REPORT = 2790
const STRINGE_MAP = 2791
const LAMBERTS_MAP = 2792
const ALEXS_LETTER = 2793
const ALEXS_ORDER = 2794
const WINE_CATALOG = 2795
const TYRAS_CONTRACT = 2796
const RED_SPORE_DUST = 2797
const MALRUKIAN_WINE = 2798
const OLD_ORDER = 2799
const JAXS_DIARY = 2800
const TORN_MAP_PIECE_1ST = 2801
const TORN_MAP_PIECE_2ND = 2802
const SOLTS_MAP = 2803
const MAKELS_MAP = 2804
const COMBINED_MAP = 2805
const RUSTED_KEY = 2806
const GOLD_BAR = 2807
const ALEXS_RECOMMEND = 2808

const MARK_OF_SEARCHER = 2809
const DIMENSIONAL_DIAMOND = 7562

const HANGMAN_TREE = 20144
const ROAD_SCAVENGER = 20551
const GIANT_FUNGUS = 20555
const DELU_LIZARDMAN_SHAMAN = 20781

const NEER_BODYGUARD = 27092
const DELU_CHIEF_KALKIS = 27093

const minimumLevel = 39

export class TestOfTheSearcher extends ListenerLogic {
    constructor() {
        super( 'Q00225_TestOfTheSearcher', 'listeners/tracked-200/TestOfTheSearcher.ts' )
        this.questId = 225
        this.questItemIds = [
            LUTHERS_LETTER,
            ALEXS_WARRANT,
            LEIRYNNS_1ST_ORDER,
            DELU_TOTEM,
            LEIRYNNS_2ND_ORDER,
            CHIEF_KALKIS_FANG,
            LEIRYNNS_REPORT,
            STRINGE_MAP,
            LAMBERTS_MAP,
            ALEXS_LETTER,
            ALEXS_ORDER,
            WINE_CATALOG,
            TYRAS_CONTRACT,
            RED_SPORE_DUST,
            MALRUKIAN_WINE,
            OLD_ORDER,
            JAXS_DIARY,
            TORN_MAP_PIECE_1ST,
            TORN_MAP_PIECE_2ND,
            SOLTS_MAP,
            MAKELS_MAP,
            COMBINED_MAP,
            RUSTED_KEY,
            GOLD_BAR,
            ALEXS_RECOMMEND,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [ DELU_LIZARDMAN_SHAMAN ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            HANGMAN_TREE,
            ROAD_SCAVENGER,
            GIANT_FUNGUS,
            DELU_LIZARDMAN_SHAMAN,
            NEER_BODYGUARD,
            DELU_CHIEF_KALKIS,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00225_TestOfTheSearcher'
    }

    getQuestStartIds(): Array<number> {
        return [ MASTER_LUTHER ]
    }

    getTalkIds(): Array<number> {
        return [
            MASTER_LUTHER,
            CAPTAIN_ALEX,
            TYRA,
            TREE,
            STRONG_WOODEN_CHEST,
            MILITIAMAN_LEIRYNN,
            DRUNKARD_BORYS,
            BODYGUARD_JAX,
        ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.attackerPlayerId )

        if ( !NpcVariablesManager.get( data.targetId, this.getName() ) && QuestHelper.hasQuestItem( player, LEIRYNNS_1ST_ORDER ) ) {
            NpcVariablesManager.set( data.targetId, this.getName(), true )

            let npc = L2World.getObjectById( data.targetId ) as L2Npc
            QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( NEER_BODYGUARD, npc, true, 200000 ), player )
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case HANGMAN_TREE:
                if ( QuestHelper.hasQuestItem( player, JAXS_DIARY )
                        && !QuestHelper.hasQuestItem( player, MAKELS_MAP )
                        && QuestHelper.hasQuestItem( player, TORN_MAP_PIECE_2ND ) ) {
                    let receivedItem = false
                    if ( QuestHelper.getQuestItemsCount( player, TORN_MAP_PIECE_2ND ) < 3
                            && Math.random() < QuestHelper.getAdjustedChance( TORN_MAP_PIECE_2ND, 0.5, data.isChampion ) ) {
                        await QuestHelper.rewardSingleQuestItem( player, TORN_MAP_PIECE_2ND, 1, data.isChampion )
                        receivedItem = true
                    }

                    if ( Math.random() < QuestHelper.getAdjustedChance( MAKELS_MAP, 0.5, data.isChampion ) ) {
                        await QuestHelper.takeSingleItem( player, TORN_MAP_PIECE_2ND, -1 )
                        await QuestHelper.giveSingleItem( player, MAKELS_MAP, 1 )

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        if ( QuestHelper.getQuestItemsCount( player, SOLTS_MAP ) >= 1 ) {
                            state.setConditionWithSound( 15 )
                        }

                        return
                    }

                    if ( receivedItem ) {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    }
                }
                return

            case ROAD_SCAVENGER:
                if ( QuestHelper.hasQuestItem( player, JAXS_DIARY )
                        && !QuestHelper.hasQuestItem( player, SOLTS_MAP )
                        && QuestHelper.hasQuestItem( player, TORN_MAP_PIECE_1ST ) ) {
                    if ( QuestHelper.getQuestItemsCount( player, TORN_MAP_PIECE_1ST ) < 3 ) {
                        await QuestHelper.rewardSingleQuestItem( player, TORN_MAP_PIECE_1ST, 1, data.isChampion )
                    }

                    if ( QuestHelper.getQuestItemsCount( player, TORN_MAP_PIECE_1ST ) >= 4 ) {
                        await QuestHelper.takeSingleItem( player, TORN_MAP_PIECE_1ST, -1 )
                        await QuestHelper.giveSingleItem( player, SOLTS_MAP, 1 )

                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        if ( QuestHelper.getQuestItemsCount( player, MAKELS_MAP ) >= 1 ) {
                            state.setConditionWithSound( 15 )
                        }

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }
                return

            case GIANT_FUNGUS:
                if ( QuestHelper.hasQuestItem( player, TYRAS_CONTRACT )
                        && QuestHelper.getQuestItemsCount( player, RED_SPORE_DUST ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, RED_SPORE_DUST, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, RED_SPORE_DUST ) >= 10 ) {
                        state.setConditionWithSound( 11, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }
                return

            case DELU_LIZARDMAN_SHAMAN:
                if ( QuestHelper.hasQuestItem( player, LEIRYNNS_1ST_ORDER )
                        && QuestHelper.getQuestItemsCount( player, DELU_TOTEM ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, DELU_TOTEM, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, RED_SPORE_DUST ) >= 10 ) {
                        state.setConditionWithSound( 4, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case DELU_CHIEF_KALKIS:
                if ( QuestHelper.hasQuestItem( player, LEIRYNNS_2ND_ORDER )
                        && !QuestHelper.hasAtLeastOneQuestItem( player, CHIEF_KALKIS_FANG, STRINGE_MAP ) ) {
                    await QuestHelper.giveMultipleItems( player, 1, CHIEF_KALKIS_FANG, STRINGE_MAP )
                    state.setConditionWithSound( 6, true )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveSingleItem( player, LUTHERS_LETTER, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        let amount: number = player.getClassId() === ClassIdValues.scavenger.id ? 82 : 96
                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, amount )

                        return this.getPath( '30690-05a.htm' )
                    }

                    return this.getPath( '30690-05.htm' )
                }
                return

            case '30291-05.html':
            case '30291-01t.html':
            case '30291-06.html':
            case '30730-01a.html':
            case '30730-01b.html':
            case '30730-01c.html':
            case '30730-02.html':
            case '30730-02a.html':
            case '30730-02b.html':
                break

            case '30291-07.html':
                if ( QuestHelper.hasQuestItems( player, LEIRYNNS_REPORT, STRINGE_MAP ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, LEIRYNNS_REPORT, STRINGE_MAP )
                    await QuestHelper.giveMultipleItems( player, 1, LAMBERTS_MAP, ALEXS_LETTER, ALEXS_ORDER )

                    state.setConditionWithSound( 8, true )
                    break
                }

                return

            case '30420-01a.html':
                if ( QuestHelper.hasQuestItem( player, WINE_CATALOG ) ) {
                    await QuestHelper.takeSingleItem( player, WINE_CATALOG, 1 )
                    await QuestHelper.giveSingleItem( player, TYRAS_CONTRACT, 1 )

                    state.setConditionWithSound( 10, true )
                    break
                }

                return

            case '30627-01a.html':
                if ( npc.getSummonedNpcCount() < 5 ) {
                    await QuestHelper.giveSingleItem( player, RUSTED_KEY, 1 )

                    QuestHelper.addSummonedSpawnAtLocation( npc, STRONG_WOODEN_CHEST, npc, true, 0 )
                    state.setConditionWithSound( 17, true )
                    break
                }

                return

            case '30628-01a.html':
                await QuestHelper.takeSingleItem( player, RUSTED_KEY, 1 )
                await QuestHelper.giveSingleItem( player, GOLD_BAR, 20 )

                state.setConditionWithSound( 18, true )
                await npc.deleteMe()
                break

            case '30730-01d.html':
                if ( QuestHelper.hasQuestItem( player, OLD_ORDER ) ) {
                    await QuestHelper.takeSingleItem( player, OLD_ORDER, 1 )
                    await QuestHelper.giveSingleItem( player, JAXS_DIARY, 1 )

                    state.setConditionWithSound( 14, true )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MASTER_LUTHER ) {
                    if ( ![ ClassIdValues.rogue.id,
                           ClassIdValues.elvenScout.id,
                           ClassIdValues.assassin.id,
                           ClassIdValues.scavenger.id ].includes( player.getClassId() ) ) {
                        return this.getPath( '30690-01.html' )
                    }

                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30690-02.html' )
                    }

                    if ( player.getClassId() === ClassIdValues.scavenger.id ) {
                        return this.getPath( '30690-04.htm' )
                    }

                    return this.getPath( '30690-03.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MASTER_LUTHER:
                        if ( QuestHelper.hasQuestItem( player, LUTHERS_LETTER ) && !QuestHelper.hasQuestItem( player, ALEXS_RECOMMEND ) ) {
                            return this.getPath( '30690-06.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, LUTHERS_LETTER, ALEXS_RECOMMEND ) ) {
                            return this.getPath( '30690-07.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, LUTHERS_LETTER ) && QuestHelper.hasQuestItem( player, ALEXS_RECOMMEND ) ) {
                            await QuestHelper.giveAdena( player, 161806, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_SEARCHER, 1 )
                            await QuestHelper.addExpAndSp( player, 894888, 61408 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                            return this.getPath( '30690-08.html' )
                        }

                        break

                    case CAPTAIN_ALEX:
                        if ( QuestHelper.hasQuestItem( player, LUTHERS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, LUTHERS_LETTER, 1 )
                            await QuestHelper.giveSingleItem( player, ALEXS_WARRANT, 1 )

                            state.setConditionWithSound( 2, true )
                            return this.getPath( '30291-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ALEXS_WARRANT ) ) {
                            return this.getPath( '30291-02.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, LEIRYNNS_1ST_ORDER, LEIRYNNS_2ND_ORDER ) ) {
                            return this.getPath( '30291-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LEIRYNNS_REPORT ) ) {
                            return this.getPath( '30291-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ALEXS_ORDER ) ) {
                            if ( QuestHelper.hasQuestItem( player, ALEXS_LETTER ) ) {
                                return this.getPath( '30291-08.html' )
                            }

                            if ( QuestHelper.hasAtLeastOneQuestItem( player, OLD_ORDER, JAXS_DIARY ) ) {
                                return this.getPath( '30291-09.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, COMBINED_MAP ) ) {
                                if ( QuestHelper.getQuestItemsCount( player, GOLD_BAR ) >= 20 ) {
                                    await QuestHelper.takeMultipleItems( player, -1, ALEXS_ORDER, COMBINED_MAP, GOLD_BAR )
                                    await QuestHelper.giveSingleItem( player, ALEXS_RECOMMEND, 1 )

                                    PlayerRadarCache.getRadar( data.playerId ).removeMarker( 10133, 157155, -2383 )

                                    state.setConditionWithSound( 19, true )
                                    return this.getPath( '30291-11.html' )
                                }

                                return this.getPath( '30291-10.html' )
                            }
                        }

                        if ( QuestHelper.hasQuestItem( player, ALEXS_RECOMMEND ) ) {
                            return this.getPath( '30291-12.html' )
                        }

                        break

                    case TYRA:
                        if ( QuestHelper.hasQuestItem( player, WINE_CATALOG ) ) {
                            return this.getPath( '30420-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TYRAS_CONTRACT ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, RED_SPORE_DUST ) < 10 ) {
                                return this.getPath( '30420-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, TYRAS_CONTRACT, RED_SPORE_DUST )
                            await QuestHelper.giveSingleItem( player, MALRUKIAN_WINE, 1 )

                            state.setConditionWithSound( 12, true )
                            return this.getPath( '30420-03.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, JAXS_DIARY, OLD_ORDER, COMBINED_MAP, ALEXS_RECOMMEND, MALRUKIAN_WINE ) ) {
                            return this.getPath( '30420-04.html' )
                        }

                        break

                    case TREE:
                        if ( QuestHelper.hasQuestItem( player, COMBINED_MAP ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, RUSTED_KEY, GOLD_BAR ) ) {
                                return this.getPath( '30627-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, RUSTED_KEY )
                                    && QuestHelper.getQuestItemsCount( player, GOLD_BAR ) >= 20 ) {
                                return this.getPath( '30627-01.html' )
                            }
                        }

                        break

                    case STRONG_WOODEN_CHEST:
                        if ( QuestHelper.hasQuestItem( player, RUSTED_KEY ) ) {
                            return this.getPath( '30628-01.html' )
                        }

                        break

                    case MILITIAMAN_LEIRYNN:
                        if ( QuestHelper.hasQuestItem( player, ALEXS_WARRANT ) ) {
                            await QuestHelper.takeSingleItem( player, ALEXS_WARRANT, 1 )
                            await QuestHelper.giveSingleItem( player, LEIRYNNS_1ST_ORDER, 1 )

                            state.setConditionWithSound( 3, true )
                            return this.getPath( '30728-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LEIRYNNS_1ST_ORDER ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, DELU_TOTEM ) < 10 ) {
                                return this.getPath( '30728-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, LEIRYNNS_1ST_ORDER, DELU_TOTEM )
                            await QuestHelper.giveSingleItem( player, LEIRYNNS_2ND_ORDER, 1 )

                            state.setConditionWithSound( 5, true )
                            return this.getPath( '30728-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LEIRYNNS_2ND_ORDER ) ) {
                            if ( !QuestHelper.hasQuestItem( player, CHIEF_KALKIS_FANG ) ) {
                                return this.getPath( '30728-04.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, LEIRYNNS_2ND_ORDER, CHIEF_KALKIS_FANG )
                            await QuestHelper.giveSingleItem( player, LEIRYNNS_REPORT, 1 )

                            state.setConditionWithSound( 7, true )
                            return this.getPath( '30728-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LEIRYNNS_REPORT ) ) {
                            return this.getPath( '30728-06.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, ALEXS_RECOMMEND, ALEXS_ORDER ) ) {
                            return this.getPath( '30728-07.html' )
                        }

                        break

                    case DRUNKARD_BORYS:
                        if ( QuestHelper.hasQuestItem( player, ALEXS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, ALEXS_LETTER, 1 )
                            await QuestHelper.giveSingleItem( player, WINE_CATALOG, 1 )

                            state.setConditionWithSound( 9, true )
                            return this.getPath( '30729-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, WINE_CATALOG ) && !QuestHelper.hasQuestItem( player, MALRUKIAN_WINE ) ) {
                            return this.getPath( '30729-02.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MALRUKIAN_WINE ) && !QuestHelper.hasQuestItem( player, WINE_CATALOG ) ) {
                            await QuestHelper.takeSingleItem( player, MALRUKIAN_WINE, 1 )
                            await QuestHelper.giveSingleItem( player, OLD_ORDER, 1 )

                            state.setConditionWithSound( 13, true )
                            return this.getPath( '30729-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, OLD_ORDER ) ) {
                            return this.getPath( '30729-04.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, JAXS_DIARY, COMBINED_MAP, ALEXS_RECOMMEND ) ) {
                            return this.getPath( '30729-05.html' )
                        }

                        break

                    case BODYGUARD_JAX:
                        if ( QuestHelper.hasQuestItem( player, OLD_ORDER ) ) {
                            return this.getPath( '30730-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, JAXS_DIARY ) ) {
                            let itemCount = QuestHelper.getQuestItemsCount( player, SOLTS_MAP ) + QuestHelper.getQuestItemsCount( player, MAKELS_MAP )
                            if ( itemCount < 2 ) {
                                return this.getPath( '30730-02.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, LAMBERTS_MAP, JAXS_DIARY, SOLTS_MAP, MAKELS_MAP )
                            await QuestHelper.giveSingleItem( player, COMBINED_MAP, 1 )

                            state.setConditionWithSound( 16, true )
                            return this.getPath( '30730-03.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, COMBINED_MAP, ALEXS_RECOMMEND ) ) {
                            return this.getPath( '30730-04.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MASTER_LUTHER ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}