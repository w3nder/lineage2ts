import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'

const FLAUEN = 30899
const IASON = 30969
const ROMAN = 30897
const MORELYN = 30925
const HELVETICA = 32641
const ATHENIA = 32643

const FLAUENS_LETTER = 14862
const DOSKOZER_LETTER = 14863
const ATHENIA_LETTER = 14864
const VICINITY_OF_FOS = 14865
const SUPPORT_CERTIFICATE = 14866

const minimumLevel = 82

export class WindsOfChange extends ListenerLogic {
    constructor() {
        super( 'Q00237_WindsOfChange', 'listeners/tracked-200/WindsOfChange.ts' )
        this.questId = 237
        this.questItemIds = [ FLAUENS_LETTER, DOSKOZER_LETTER, ATHENIA_LETTER ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00237_WindsOfChange'
    }

    getQuestStartIds(): Array<number> {
        return [ FLAUEN ]
    }

    getTalkIds(): Array<number> {
        return [ FLAUEN, IASON, ROMAN, MORELYN, HELVETICA, ATHENIA ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30899-02.htm':
            case '30899-03.htm':
            case '30899-04.htm':
            case '30899-05.htm':
            case '30969-03.html':
            case '30969-03a.html':
            case '30969-03b.html':
            case '30969-04.html':
            case '30969-08.html':
            case '30969-08a.html':
            case '30969-08b.html':
            case '30969-08c.html':
            case '30897-02.html':
            case '30925-02.html':
                break

            case '30899-06.html':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, FLAUENS_LETTER, 1 )
                break

            case '30969-02.html':
                await QuestHelper.takeSingleItem( player, FLAUENS_LETTER, -1 )
                break

            case '30969-05.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '30897-03.html':
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '30925-03.html':
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '30969-09.html':
                if ( state.isCondition( 4 ) ) {
                    await QuestHelper.giveSingleItem( player, DOSKOZER_LETTER, 1 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return
            case '30969-10.html':
                if ( state.isCondition( 4 ) ) {
                    await QuestHelper.giveSingleItem( player, ATHENIA_LETTER, 1 )
                    state.setConditionWithSound( 6, true )

                    break
                }
                return
            case '32641-02.html':
                await QuestHelper.giveAdena( player, 213876, true )
                await QuestHelper.giveSingleItem( player, VICINITY_OF_FOS, 1 )
                await QuestHelper.addExpAndSp( player, 892773, 60012 )
                await state.exitQuest( false, true )

                break

            case '32643-02.html':
                await QuestHelper.giveAdena( player, 213876, true )
                await QuestHelper.giveSingleItem( player, SUPPORT_CERTIFICATE, 1 )
                await QuestHelper.addExpAndSp( player, 892773, 60012 )
                await state.exitQuest( false, true )

                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case FLAUEN:
                switch ( state.getState() ) {
                    case QuestStateValues.COMPLETED:
                        return this.getPath( '30899-09.html' )

                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30899-01.htm' : '30899-00.html' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                            case 4:
                                return this.getPath( '30899-07.html' )

                            case 2:
                                return this.getPath( '30899-10.html' )

                            case 3:
                                return this.getPath( '30899-11.html' )

                            case 5:
                            case 6:
                                return this.getPath( '30899-08.html' )
                        }
                }

                break

            case IASON:
                if ( state.isCompleted() ) {
                    break
                }

                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30969-01.html' )

                    case 2:
                        return this.getPath( '30969-06.html' )

                    case 4:
                        return this.getPath( '30969-07.html' )

                    case 5:
                    case 6:
                        return this.getPath( '30969-11.html' )
                }

                break

            case ROMAN:
                switch ( state.getCondition() ) {
                    case 2:
                        return this.getPath( '30897-01.html' )

                    case 3:
                    case 4:
                        return this.getPath( '30897-04.html' )
                }

                break

            case MORELYN:
                switch ( state.getCondition() ) {
                    case 3:
                        return this.getPath( '30925-01.html' )

                    case 4:
                        return this.getPath( '30925-04.html' )
                }

                break

            case HELVETICA:
                if ( state.isCompleted() ) {
                    let canProceed: boolean = QuestHelper.hasQuestItem( player, VICINITY_OF_FOS ) || player.hasQuestCompleted( 'Q00238_SuccessFailureOfBusiness' )
                    return this.getPath( canProceed ? '32641-03.html' : '32641-05.html' )
                }

                if ( state.isCondition( 5 ) ) {
                    return this.getPath( '32641-01.html' )
                }

                if ( state.isCondition( 6 ) ) {
                    return this.getPath( '32641-04.html' )
                }

                break

            case ATHENIA:
                if ( state.isCompleted() ) {
                    let canProceed: boolean = QuestHelper.hasQuestItem( player, SUPPORT_CERTIFICATE ) || player.hasQuestCompleted( 'Q00239_WontYouJoinUs' )
                    return this.getPath( canProceed ? '32643-03.html' : '32643-05.html' )
                }

                if ( state.isCondition( 5 ) ) {
                    return this.getPath( '32643-04.html' )
                }

                if ( state.isCondition( 6 ) ) {
                    return this.getPath( '32643-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}