import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const HIGH_PRIEST_SYLVAIN = 30070
const CAPTAIN_LUCAS = 30071
const WAREHOUSE_KEEPER_VALKON = 30103
const MAGISTER_DIETER = 30111
const GRAND_MAGISTER_JUREK = 30115
const TRADER_EDROC = 30230
const WAREHOUSE_KEEPER_RAUT = 30316
const BLACKSMITH_POITAN = 30458
const MAGISTER_MIRIEN = 30461
const MARIA = 30608
const ASTROLOGER_CRETA = 30609
const ELDER_CRONOS = 30610
const DRUNKARD_TRIFF = 30611
const ELDER_CASIAN = 30612

const MIRIENS_1ST_SIGIL = 2675
const MIRIENS_2ND_SIGIL = 2676
const MIRIENS_3RD_SIGIL = 2677
const MIRIENS_INSTRUCTION = 2678
const MARIAS_1ST_LETTER = 2679
const MARIAS_2ND_LETTER = 2680
const LUCASS_LETTER = 2681
const LUCILLAS_HANDBAG = 2682
const CRETAS_1ST_LETTER = 2683
const CRERAS_PAINTING1 = 2684
const CRERAS_PAINTING2 = 2685
const CRERAS_PAINTING3 = 2686
const BROWN_SCROLL_SCRAP = 2687
const CRYSTAL_OF_PURITY1 = 2688
const HIGH_PRIESTS_SIGIL = 2689
const GRAND_MAGISTER_SIGIL = 2690
const CRONOS_SIGIL = 2691
const SYLVAINS_LETTER = 2692
const SYMBOL_OF_SYLVAIN = 2693
const JUREKS_LIST = 2694
const MONSTER_EYE_DESTROYER_SKIN = 2695
const SHAMANS_NECKLACE = 2696
const SHACKLES_SCALP = 2697
const SYMBOL_OF_JUREK = 2698
const CRONOS_LETTER = 2699
const DIETERS_KEY = 2700
const CRETAS_2ND_LETTER = 2701
const DIETERS_LETTER = 2702
const DIETERS_DIARY = 2703
const RAUTS_LETTER_ENVELOPE = 2704
const TRIFFS_RING = 2705
const SCRIPTURE_CHAPTER_1 = 2706
const SCRIPTURE_CHAPTER_2 = 2707
const SCRIPTURE_CHAPTER_3 = 2708
const SCRIPTURE_CHAPTER_4 = 2709
const VALKONS_REQUEST = 2710
const POITANS_NOTES = 2711
const STRONG_LIGUOR = 2713
const CRYSTAL_OF_PURITY2 = 2714
const CASIANS_LIST = 2715
const GHOULS_SKIN = 2716
const MEDUSAS_BLOOD = 2717
const FETTERED_SOULS_ICHOR = 2718
const ENCHANTED_GARGOYLES_NAIL = 2719
const SYMBOL_OF_CRONOS = 2720

const MARK_OF_SCHOLAR = 2674
const DIMENSIONAL_DIAMOND = 7562

const MONSTER_EYE_DESTREOYER = 20068
const MEDUSA = 20158
const GHOUL = 20201
const SHACKLE1 = 20235
const BREKA_ORC_SHAMAN = 20269
const SHACKLE2 = 20279
const FETTERED_SOUL = 20552
const GRANDIS = 20554
const ENCHANTED_GARGOYLE = 20567
const LETO_LIZARDMAN_WARRIOR = 20580

const minimumLevel = 35
const maximumLevel = 36

export class TrialOfTheScholar extends ListenerLogic {
    constructor() {
        super( 'Q00214_TrialOfTheScholar', 'listeners/tracked-200/TrialOfTheScholar.ts' )
        this.questId = 214
        this.questItemIds = [
            MIRIENS_1ST_SIGIL,
            MIRIENS_2ND_SIGIL,
            MIRIENS_3RD_SIGIL,
            MIRIENS_INSTRUCTION,
            MARIAS_1ST_LETTER,
            MARIAS_2ND_LETTER,
            LUCASS_LETTER,
            LUCILLAS_HANDBAG,
            CRETAS_1ST_LETTER,
            CRERAS_PAINTING1,
            CRERAS_PAINTING1,
            CRERAS_PAINTING3,
            BROWN_SCROLL_SCRAP,
            CRYSTAL_OF_PURITY1,
            HIGH_PRIESTS_SIGIL,
            GRAND_MAGISTER_SIGIL,
            CRONOS_SIGIL,
            SYLVAINS_LETTER,
            SYMBOL_OF_SYLVAIN,
            JUREKS_LIST,
            MONSTER_EYE_DESTROYER_SKIN,
            SHAMANS_NECKLACE,
            SHACKLES_SCALP,
            SYMBOL_OF_JUREK,
            CRONOS_LETTER,
            DIETERS_KEY,
            CRETAS_2ND_LETTER,
            DIETERS_LETTER,
            DIETERS_DIARY,
            RAUTS_LETTER_ENVELOPE,
            TRIFFS_RING,
            SCRIPTURE_CHAPTER_1,
            SCRIPTURE_CHAPTER_2,
            SCRIPTURE_CHAPTER_3,
            SCRIPTURE_CHAPTER_4,
            VALKONS_REQUEST,
            POITANS_NOTES,
            STRONG_LIGUOR,
            CRYSTAL_OF_PURITY2,
            CASIANS_LIST,
            GHOULS_SKIN,
            MEDUSAS_BLOOD,
            FETTERED_SOULS_ICHOR,
            ENCHANTED_GARGOYLES_NAIL,
            SYMBOL_OF_CRONOS,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            MONSTER_EYE_DESTREOYER,
            MEDUSA,
            GHOUL,
            SHACKLE1,
            BREKA_ORC_SHAMAN,
            SHACKLE2,
            FETTERED_SOUL,
            GRANDIS,
            ENCHANTED_GARGOYLE,
            LETO_LIZARDMAN_WARRIOR,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00214_TrialOfTheScholar'
    }

    getQuestStartIds(): Array<number> {
        return [ MAGISTER_MIRIEN ]
    }

    getTalkIds(): Array<number> {
        return [
            MAGISTER_MIRIEN,
            HIGH_PRIEST_SYLVAIN,
            CAPTAIN_LUCAS,
            WAREHOUSE_KEEPER_VALKON,
            MAGISTER_DIETER,
            GRAND_MAGISTER_JUREK,
            TRADER_EDROC,
            WAREHOUSE_KEEPER_RAUT,
            BLACKSMITH_POITAN,
            MARIA,
            ASTROLOGER_CRETA,
            ELDER_CRONOS,
            DRUNKARD_TRIFF,
            ELDER_CASIAN,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case MONSTER_EYE_DESTREOYER:
                if ( QuestHelper.hasQuestItems( player, MIRIENS_2ND_SIGIL, GRAND_MAGISTER_SIGIL, JUREKS_LIST )
                        && QuestHelper.getQuestItemsCount( player, MONSTER_EYE_DESTROYER_SKIN ) < 5 ) {
                    await QuestHelper.rewardSingleQuestItem( player, MONSTER_EYE_DESTROYER_SKIN, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, MONSTER_EYE_DESTROYER_SKIN ) >= 5
                            && QuestHelper.getQuestItemsCount( player, SHAMANS_NECKLACE ) >= 5
                            && QuestHelper.getQuestItemsCount( player, SHACKLES_SCALP ) >= 2 ) {
                        state.setConditionWithSound( 17, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case MEDUSA:
                if ( QuestHelper.hasQuestItems( player, TRIFFS_RING, POITANS_NOTES, CASIANS_LIST )
                        && QuestHelper.getQuestItemsCount( player, MEDUSAS_BLOOD ) < 12 ) {
                    await QuestHelper.rewardSingleQuestItem( player, MEDUSAS_BLOOD, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, MEDUSAS_BLOOD ) >= 12 ) {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case GHOUL:
                if ( QuestHelper.hasQuestItems( player, TRIFFS_RING, POITANS_NOTES, CASIANS_LIST )
                        && QuestHelper.getQuestItemsCount( player, GHOULS_SKIN ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, GHOULS_SKIN, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, GHOULS_SKIN ) >= 10 ) {
                        state.setConditionWithSound( 29, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case SHACKLE1:
            case SHACKLE2:
                if ( QuestHelper.hasQuestItems( player, MIRIENS_2ND_SIGIL, GRAND_MAGISTER_SIGIL, JUREKS_LIST )
                        && QuestHelper.getQuestItemsCount( player, SHACKLES_SCALP ) < 2 ) {
                    await QuestHelper.rewardSingleQuestItem( player, SHACKLES_SCALP, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, MONSTER_EYE_DESTROYER_SKIN ) >= 5
                            && QuestHelper.getQuestItemsCount( player, SHAMANS_NECKLACE ) >= 5
                            && QuestHelper.getQuestItemsCount( player, SHACKLES_SCALP ) >= 2 ) {
                        state.setConditionWithSound( 17, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case BREKA_ORC_SHAMAN:
                if ( QuestHelper.hasQuestItems( player, MIRIENS_2ND_SIGIL, GRAND_MAGISTER_SIGIL, JUREKS_LIST )
                        && QuestHelper.getQuestItemsCount( player, SHAMANS_NECKLACE ) < 5 ) {
                    await QuestHelper.rewardSingleQuestItem( player, SHAMANS_NECKLACE, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, MONSTER_EYE_DESTROYER_SKIN ) >= 5
                            && QuestHelper.getQuestItemsCount( player, SHAMANS_NECKLACE ) >= 5
                            && QuestHelper.getQuestItemsCount( player, SHACKLES_SCALP ) >= 2 ) {
                        state.setConditionWithSound( 17, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case FETTERED_SOUL:
                if ( QuestHelper.hasQuestItems( player, TRIFFS_RING, POITANS_NOTES, CASIANS_LIST )
                        && QuestHelper.getQuestItemsCount( player, FETTERED_SOULS_ICHOR ) < 5 ) {
                    await QuestHelper.rewardSingleQuestItem( player, FETTERED_SOULS_ICHOR, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, FETTERED_SOULS_ICHOR ) >= 5 ) {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case GRANDIS:
                if ( QuestHelper.hasQuestItems( player, MIRIENS_3RD_SIGIL, CRONOS_SIGIL, TRIFFS_RING )
                        && !QuestHelper.hasQuestItems( player, SCRIPTURE_CHAPTER_3 ) ) {
                    await QuestHelper.giveSingleItem( player, SCRIPTURE_CHAPTER_3, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case ENCHANTED_GARGOYLE:
                if ( QuestHelper.hasQuestItems( player, TRIFFS_RING, POITANS_NOTES, CASIANS_LIST )
                        && QuestHelper.getQuestItemsCount( player, ENCHANTED_GARGOYLES_NAIL ) < 5 ) {
                    await QuestHelper.giveSingleItem( player, ENCHANTED_GARGOYLES_NAIL, 1 )

                    if ( QuestHelper.getQuestItemsCount( player, ENCHANTED_GARGOYLES_NAIL ) >= 5 ) {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case LETO_LIZARDMAN_WARRIOR:
                if ( QuestHelper.hasQuestItems( player, MIRIENS_1ST_SIGIL, HIGH_PRIESTS_SIGIL, CRERAS_PAINTING3 )
                        && QuestHelper.getQuestItemsCount( player, BROWN_SCROLL_SCRAP ) < 5 ) {
                    await QuestHelper.rewardSingleQuestItem( player, BROWN_SCROLL_SCRAP, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, BROWN_SCROLL_SCRAP ) >= 5 ) {
                        state.setConditionWithSound( 12, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    if ( !QuestHelper.hasQuestItem( player, MIRIENS_1ST_SIGIL ) ) {
                        await QuestHelper.giveSingleItem( player, MIRIENS_1ST_SIGIL, 1 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 168 )

                        return this.getPath( '30461-04a.htm' )
                    }

                    return this.getPath( '30461-04.htm' )
                }

                return

            case '30103-02.html':
            case '30103-03.html':
            case '30111-02.html':
            case '30111-03.html':
            case '30111-04.html':
            case '30111-08.html':
            case '30111-14.html':
            case '30115-02.html':
            case '30316-03.html':
            case '30461-09.html':
            case '30608-07.html':
            case '30609-02.html':
            case '30609-03.html':
            case '30609-04.html':
            case '30609-08.html':
            case '30609-13.html':
            case '30610-02.html':
            case '30610-03.html':
            case '30610-04.html':
            case '30610-05.html':
            case '30610-06.html':
            case '30610-07.html':
            case '30610-08.html':
            case '30610-09.html':
            case '30610-13.html':
            case '30611-02.html':
            case '30611-03.html':
            case '30611-06.html':
            case '30612-03.html':
                break

            case '30461-10.html':
                if ( QuestHelper.hasQuestItems( player, MIRIENS_2ND_SIGIL, SYMBOL_OF_JUREK ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, MIRIENS_2ND_SIGIL, SYMBOL_OF_JUREK )
                    await QuestHelper.giveSingleItem( player, MIRIENS_3RD_SIGIL, 1 )

                    state.setConditionWithSound( 19, true )
                    break
                }

                return

            case '30070-02.html':
                await QuestHelper.giveMultipleItems( player, 1, HIGH_PRIESTS_SIGIL, SYLVAINS_LETTER )

                state.setConditionWithSound( 2, true )
                break

            case '30071-04.html':
                if ( QuestHelper.hasQuestItem( player, CRERAS_PAINTING2 ) ) {
                    await QuestHelper.takeSingleItem( player, CRERAS_PAINTING2, 1 )
                    await QuestHelper.giveSingleItem( player, CRERAS_PAINTING3, 1 )

                    state.setConditionWithSound( 10, true )
                    break
                }

                return

            case '30103-04.html':
                await QuestHelper.giveSingleItem( player, VALKONS_REQUEST, 1 )
                break

            case '30111-05.html':
                if ( QuestHelper.hasQuestItem( player, CRONOS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, CRONOS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, DIETERS_KEY, 1 )

                    state.setConditionWithSound( 21, true )
                    break
                }

                return

            case '30111-09.html':
                if ( QuestHelper.hasQuestItem( player, CRETAS_2ND_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, CRETAS_2ND_LETTER, 1 )
                    await QuestHelper.giveMultipleItems( player, 1, DIETERS_LETTER, DIETERS_DIARY )

                    state.setConditionWithSound( 23, true )
                    break
                }

                return

            case '30115-03.html':
                await QuestHelper.giveMultipleItems( player, 1, JUREKS_LIST, GRAND_MAGISTER_SIGIL )

                state.setConditionWithSound( 16, true )
                break

            case '30230-02.html':
                if ( QuestHelper.hasQuestItem( player, DIETERS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, DIETERS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, RAUTS_LETTER_ENVELOPE, 1 )

                    state.setConditionWithSound( 24, true )
                    break
                }

                return

            case '30316-02.html':
                if ( QuestHelper.hasQuestItem( player, RAUTS_LETTER_ENVELOPE ) ) {
                    await QuestHelper.takeSingleItem( player, RAUTS_LETTER_ENVELOPE, 1 )
                    await QuestHelper.giveMultipleItems( player, 1, SCRIPTURE_CHAPTER_1, STRONG_LIGUOR )

                    state.setConditionWithSound( 25, true )
                    break
                }

                return

            case '30608-02.html':
                if ( QuestHelper.hasQuestItem( player, SYLVAINS_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, MARIAS_1ST_LETTER, 1 )
                    await QuestHelper.takeSingleItem( player, SYLVAINS_LETTER, 1 )

                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            case '30608-08.html':
                if ( QuestHelper.hasQuestItem( player, CRETAS_1ST_LETTER ) ) {
                    await QuestHelper.giveSingleItem( player, LUCILLAS_HANDBAG, 1 )
                    await QuestHelper.takeSingleItem( player, CRETAS_1ST_LETTER, 1 )

                    state.setConditionWithSound( 7, true )
                    break
                }

                return

            case '30608-14.html':
                if ( QuestHelper.hasQuestItem( player, CRERAS_PAINTING3 ) ) {
                    await QuestHelper.takeSingleItem( player, CRERAS_PAINTING3, 1 )
                    await QuestHelper.takeSingleItem( player, BROWN_SCROLL_SCRAP, -1 )
                    await QuestHelper.giveSingleItem( player, CRYSTAL_OF_PURITY1, 1 )

                    state.setConditionWithSound( 13, true )
                    break
                }

                return

            case '30609-05.html':
                if ( QuestHelper.hasQuestItem( player, MARIAS_2ND_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, MARIAS_2ND_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, CRETAS_1ST_LETTER, 1 )

                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '30609-09.html':
                if ( QuestHelper.hasQuestItem( player, LUCILLAS_HANDBAG ) ) {
                    await QuestHelper.takeSingleItem( player, LUCILLAS_HANDBAG, 1 )
                    await QuestHelper.giveSingleItem( player, CRERAS_PAINTING1, 1 )

                    state.setConditionWithSound( 8, true )
                    break
                }

                return

            case '30609-14.html':
                if ( QuestHelper.hasQuestItem( player, DIETERS_KEY ) ) {
                    await QuestHelper.takeSingleItem( player, DIETERS_KEY, 1 )
                    await QuestHelper.giveSingleItem( player, CRETAS_2ND_LETTER, 1 )

                    state.setConditionWithSound( 22, true )
                    break
                }

                return

            case '30610-10.html':
                await QuestHelper.giveMultipleItems( player, 1, CRONOS_SIGIL, CRONOS_LETTER )

                state.setConditionWithSound( 20, true )
                break

            case '30610-14.html':
                if ( QuestHelper.hasQuestItems( player, SCRIPTURE_CHAPTER_1, SCRIPTURE_CHAPTER_2, SCRIPTURE_CHAPTER_3, SCRIPTURE_CHAPTER_4 ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, CRONOS_SIGIL, DIETERS_DIARY, TRIFFS_RING, SCRIPTURE_CHAPTER_1, SCRIPTURE_CHAPTER_2, SCRIPTURE_CHAPTER_3, SCRIPTURE_CHAPTER_4 )
                    await QuestHelper.giveSingleItem( player, SYMBOL_OF_CRONOS, 1 )

                    state.setConditionWithSound( 31, true )
                    break
                }

                return

            case '30611-04.html':
                if ( QuestHelper.hasQuestItem( player, STRONG_LIGUOR ) ) {
                    await QuestHelper.giveSingleItem( player, TRIFFS_RING, 1 )
                    await QuestHelper.takeSingleItem( player, STRONG_LIGUOR, 1 )

                    state.setConditionWithSound( 26, true )
                    break
                }

                return

            case '30612-04.html':
                await QuestHelper.giveSingleItem( player, CASIANS_LIST, 1 )

                state.setConditionWithSound( 28, true )
                break

            case '30612-07.html':
                await QuestHelper.giveSingleItem( player, SCRIPTURE_CHAPTER_4, 1 )
                await QuestHelper.takeMultipleItems( player, 1, POITANS_NOTES, CASIANS_LIST )
                await QuestHelper.takeMultipleItems( player, -1, GHOULS_SKIN, MEDUSAS_BLOOD, FETTERED_SOULS_ICHOR, ENCHANTED_GARGOYLES_NAIL )

                state.setConditionWithSound( 30, true )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MAGISTER_MIRIEN ) {
                    if ( [ ClassIdValues.wizard.id, ClassIdValues.elvenWizard.id, ClassIdValues.darkWizard.id ].includes( player.getClassId() ) ) {
                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '30461-02.html' )
                        }
                            return this.getPath( '30461-03.htm' )

                    }
                        return this.getPath( '30461-01.html' )

                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MAGISTER_MIRIEN:
                        if ( QuestHelper.hasQuestItem( player, MIRIENS_1ST_SIGIL ) ) {
                            if ( !QuestHelper.hasQuestItem( player, SYMBOL_OF_SYLVAIN ) ) {
                                return this.getPath( '30461-05.html' )
                            }

                            await QuestHelper.takeSingleItem( player, MIRIENS_1ST_SIGIL, 1 )
                            await QuestHelper.giveSingleItem( player, MIRIENS_2ND_SIGIL, 1 )
                            await QuestHelper.takeSingleItem( player, SYMBOL_OF_SYLVAIN, 1 )
                            state.setConditionWithSound( 15, true )

                            return this.getPath( '30461-06.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MIRIENS_2ND_SIGIL ) ) {
                            if ( !QuestHelper.hasQuestItem( player, SYMBOL_OF_JUREK ) ) {
                                return this.getPath( '30461-07.html' )
                            }

                            return this.getPath( '30461-08.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MIRIENS_INSTRUCTION ) ) {
                            if ( player.getLevel() < maximumLevel ) {
                                return this.getPath( '30461-11.html' )
                            }

                            await QuestHelper.takeSingleItem( player, MIRIENS_INSTRUCTION, 1 )
                            await QuestHelper.giveSingleItem( player, MIRIENS_3RD_SIGIL, 1 )
                            state.setConditionWithSound( 19, true )

                            return this.getPath( '30461-12.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MIRIENS_3RD_SIGIL ) ) {
                            if ( !QuestHelper.hasQuestItem( player, SYMBOL_OF_CRONOS ) ) {
                                return this.getPath( '30461-13.html' )
                            }

                            await QuestHelper.giveAdena( player, 319628, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_SCHOLAR, 1 )
                            await QuestHelper.addExpAndSp( player, 1753926, 113754 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                            return this.getPath( '30461-14.html' )
                        }

                        break

                    case HIGH_PRIEST_SYLVAIN:
                        if ( QuestHelper.hasQuestItem( player, MIRIENS_1ST_SIGIL )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, HIGH_PRIESTS_SIGIL, SYMBOL_OF_SYLVAIN ) ) {
                            return this.getPath( '30070-01.html' )
                        }

                        if ( !QuestHelper.hasQuestItem( player, CRYSTAL_OF_PURITY1 ) && QuestHelper.hasQuestItems( player, HIGH_PRIESTS_SIGIL, MIRIENS_1ST_SIGIL ) ) {
                            return this.getPath( '30070-03.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, HIGH_PRIESTS_SIGIL, MIRIENS_1ST_SIGIL, CRYSTAL_OF_PURITY1 ) ) {
                            await QuestHelper.takeSingleItem( player, CRYSTAL_OF_PURITY1, 1 )
                            await QuestHelper.takeSingleItem( player, HIGH_PRIESTS_SIGIL, 1 )
                            await QuestHelper.giveSingleItem( player, SYMBOL_OF_SYLVAIN, 1 )
                            state.setConditionWithSound( 14, true )
                            return this.getPath( '30070-04.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, MIRIENS_1ST_SIGIL, SYMBOL_OF_SYLVAIN ) && !QuestHelper.hasQuestItem( player, HIGH_PRIESTS_SIGIL ) ) {
                            return this.getPath( '30070-05.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, MIRIENS_2ND_SIGIL, MIRIENS_3RD_SIGIL ) ) {
                            return this.getPath( '30070-06.html' )
                        }

                        break

                    case CAPTAIN_LUCAS:
                        if ( QuestHelper.hasQuestItems( player, MIRIENS_1ST_SIGIL, HIGH_PRIESTS_SIGIL ) ) {
                            if ( QuestHelper.hasQuestItem( player, MARIAS_1ST_LETTER ) ) {
                                await QuestHelper.takeSingleItem( player, MARIAS_1ST_LETTER, 1 )
                                await QuestHelper.giveSingleItem( player, LUCASS_LETTER, 1 )

                                state.setConditionWithSound( 4, true )
                                return this.getPath( '30071-01.html' )
                            }

                            if ( QuestHelper.hasAtLeastOneQuestItem( player, MARIAS_2ND_LETTER, CRETAS_1ST_LETTER, LUCILLAS_HANDBAG, CRERAS_PAINTING1, LUCASS_LETTER ) ) {
                                return this.getPath( '30071-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRERAS_PAINTING2 ) ) {
                                return this.getPath( '30071-03.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRERAS_PAINTING3 ) ) {
                                if ( QuestHelper.getQuestItemsCount( player, BROWN_SCROLL_SCRAP ) < 5 ) {
                                    return this.getPath( '30071-05.html' )
                                }

                                return this.getPath( '30071-06.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, SYMBOL_OF_SYLVAIN, MIRIENS_2ND_SIGIL, MIRIENS_3RD_SIGIL, CRYSTAL_OF_PURITY1 ) ) {
                            return this.getPath( '30071-07.html' )
                        }

                        break

                    case WAREHOUSE_KEEPER_VALKON:
                        if ( QuestHelper.hasQuestItem( player, TRIFFS_RING ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, VALKONS_REQUEST, CRYSTAL_OF_PURITY2, SCRIPTURE_CHAPTER_2 ) ) {
                                return this.getPath( '30103-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, VALKONS_REQUEST ) && !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_PURITY2, SCRIPTURE_CHAPTER_2 ) ) {
                                return this.getPath( '30103-05.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRYSTAL_OF_PURITY2 ) && !QuestHelper.hasAtLeastOneQuestItem( player, VALKONS_REQUEST, SCRIPTURE_CHAPTER_2 ) ) {
                                await QuestHelper.giveSingleItem( player, SCRIPTURE_CHAPTER_2, 1 )
                                await QuestHelper.takeSingleItem( player, CRYSTAL_OF_PURITY2, 1 )
                                return this.getPath( '30103-06.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, SCRIPTURE_CHAPTER_2 ) && !QuestHelper.hasAtLeastOneQuestItem( player, VALKONS_REQUEST, CRYSTAL_OF_PURITY2 ) ) {
                                return this.getPath( '30103-07.html' )
                            }
                        }

                        break

                    case MAGISTER_DIETER:
                        if ( QuestHelper.hasQuestItems( player, MIRIENS_3RD_SIGIL, CRONOS_SIGIL ) ) {
                            if ( QuestHelper.hasQuestItem( player, CRONOS_LETTER ) ) {
                                return this.getPath( '30111-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, DIETERS_KEY ) ) {
                                return this.getPath( '30111-06.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRETAS_2ND_LETTER ) ) {
                                return this.getPath( '30111-07.html' )
                            }

                            if ( QuestHelper.hasQuestItems( player, DIETERS_DIARY, DIETERS_LETTER ) ) {
                                return this.getPath( '30111-10.html' )
                            }

                            if ( QuestHelper.hasQuestItems( player, DIETERS_DIARY, RAUTS_LETTER_ENVELOPE ) ) {
                                return this.getPath( '30111-11.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, DIETERS_DIARY )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, DIETERS_LETTER, RAUTS_LETTER_ENVELOPE ) ) {
                                if ( QuestHelper.hasQuestItems( player, SCRIPTURE_CHAPTER_1, SCRIPTURE_CHAPTER_2, SCRIPTURE_CHAPTER_3, SCRIPTURE_CHAPTER_4 ) ) {
                                    return this.getPath( '30111-13.html' )
                                }

                                return this.getPath( '30111-12.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, SYMBOL_OF_CRONOS ) ) {
                            return this.getPath( '30111-15.html' )
                        }

                        break

                    case GRAND_MAGISTER_JUREK:
                        if ( QuestHelper.hasQuestItem( player, MIRIENS_2ND_SIGIL ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, GRAND_MAGISTER_SIGIL, SYMBOL_OF_JUREK ) ) {
                                return this.getPath( '30115-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, JUREKS_LIST ) ) {
                                let totalItems = QuestHelper.getQuestItemsCount( player, MONSTER_EYE_DESTROYER_SKIN ) + QuestHelper.getQuestItemsCount( player, SHAMANS_NECKLACE ) + QuestHelper.getQuestItemsCount( player, SHACKLES_SCALP )
                                if ( totalItems < 12 ) {
                                    return this.getPath( '30115-04.html' )
                                }

                                await QuestHelper.takeMultipleItems( player, 1, GRAND_MAGISTER_SIGIL, JUREKS_LIST )
                                await QuestHelper.takeMultipleItems( player, -1, MONSTER_EYE_DESTROYER_SKIN, SHAMANS_NECKLACE, SHACKLES_SCALP )
                                await QuestHelper.giveSingleItem( player, SYMBOL_OF_JUREK, 1 )

                                state.setConditionWithSound( 18, true )
                                return this.getPath( '30115-05.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, SYMBOL_OF_JUREK ) && !QuestHelper.hasQuestItem( player, GRAND_MAGISTER_SIGIL ) ) {
                                return this.getPath( '30115-06.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, MIRIENS_1ST_SIGIL, MIRIENS_3RD_SIGIL ) ) {
                            return this.getPath( '30115-07.html' )
                        }

                        break

                    case TRADER_EDROC:
                        if ( QuestHelper.hasQuestItem( player, DIETERS_DIARY ) ) {
                            if ( QuestHelper.hasQuestItem( player, DIETERS_LETTER ) ) {
                                return this.getPath( '30230-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, RAUTS_LETTER_ENVELOPE ) ) {
                                return this.getPath( '30230-03.html' )
                            }

                            if ( QuestHelper.hasAtLeastOneQuestItem( player, STRONG_LIGUOR, TRIFFS_RING ) ) {
                                return this.getPath( '30230-04.html' )
                            }
                        }

                        break

                    case WAREHOUSE_KEEPER_RAUT:
                        if ( QuestHelper.hasQuestItem( player, DIETERS_DIARY ) ) {
                            if ( QuestHelper.hasQuestItem( player, RAUTS_LETTER_ENVELOPE ) ) {
                                return this.getPath( '30316-01.html' )
                            }

                            if ( QuestHelper.hasQuestItems( player, SCRIPTURE_CHAPTER_1, STRONG_LIGUOR ) ) {
                                return this.getPath( '30316-04.html' )
                            }

                            if ( QuestHelper.hasQuestItems( player, SCRIPTURE_CHAPTER_1, TRIFFS_RING ) ) {
                                return this.getPath( '30316-05.html' )
                            }
                        }

                        break

                    case BLACKSMITH_POITAN:
                        if ( QuestHelper.hasQuestItem( player, TRIFFS_RING ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, POITANS_NOTES, CASIANS_LIST, SCRIPTURE_CHAPTER_4 ) ) {
                                await QuestHelper.giveSingleItem( player, POITANS_NOTES, 1 )

                                return this.getPath( '30458-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, POITANS_NOTES ) && !QuestHelper.hasAtLeastOneQuestItem( player, CASIANS_LIST, SCRIPTURE_CHAPTER_4 ) ) {
                                return this.getPath( '30458-02.html' )
                            }

                            if ( QuestHelper.hasQuestItems( player, POITANS_NOTES, CASIANS_LIST )
                                    && !QuestHelper.hasQuestItem( player, SCRIPTURE_CHAPTER_4 ) ) {
                                return this.getPath( '30458-03.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, SCRIPTURE_CHAPTER_4 )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, POITANS_NOTES, CASIANS_LIST ) ) {
                                return this.getPath( '30458-04.html' )
                            }
                        }

                        break

                    case MARIA:
                        if ( QuestHelper.hasQuestItems( player, MIRIENS_1ST_SIGIL, HIGH_PRIESTS_SIGIL ) ) {
                            if ( QuestHelper.hasQuestItem( player, SYLVAINS_LETTER ) ) {
                                return this.getPath( '30608-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, MARIAS_1ST_LETTER ) ) {
                                return this.getPath( '30608-03.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, LUCASS_LETTER ) ) {
                                await QuestHelper.giveSingleItem( player, MARIAS_2ND_LETTER, 1 )
                                await QuestHelper.takeSingleItem( player, LUCASS_LETTER, 1 )

                                state.setConditionWithSound( 5, true )
                                return this.getPath( '30608-04.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, MARIAS_2ND_LETTER ) ) {
                                return this.getPath( '30608-05.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRETAS_1ST_LETTER ) ) {
                                return this.getPath( '30608-06.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, LUCILLAS_HANDBAG ) ) {
                                return this.getPath( '30608-09.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRERAS_PAINTING1 ) ) {
                                await QuestHelper.takeSingleItem( player, CRERAS_PAINTING1, 1 )
                                await QuestHelper.giveSingleItem( player, CRERAS_PAINTING2, 1 )

                                state.setConditionWithSound( 9, true )
                                return this.getPath( '30608-10.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRERAS_PAINTING2 ) ) {
                                return this.getPath( '30608-11.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRERAS_PAINTING3 ) ) {
                                if ( QuestHelper.getQuestItemsCount( player, BROWN_SCROLL_SCRAP ) < 5 ) {
                                    state.setConditionWithSound( 11, true )

                                    return this.getPath( '30608-12.html' )
                                }

                                return this.getPath( '30608-13.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRYSTAL_OF_PURITY1 ) ) {
                                return this.getPath( '30608-15.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, SYMBOL_OF_SYLVAIN, MIRIENS_2ND_SIGIL ) ) {
                            return this.getPath( '30608-16.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MIRIENS_3RD_SIGIL ) ) {
                            if ( !QuestHelper.hasQuestItem( player, VALKONS_REQUEST ) ) {
                                return this.getPath( '30608-17.html' )
                            }

                            await QuestHelper.takeSingleItem( player, VALKONS_REQUEST, 1 )
                            await QuestHelper.giveSingleItem( player, CRYSTAL_OF_PURITY2, 1 )

                            return this.getPath( '30608-18.html' )
                        }

                        break

                    case ASTROLOGER_CRETA:
                        if ( QuestHelper.hasQuestItems( player, MIRIENS_1ST_SIGIL, HIGH_PRIESTS_SIGIL ) ) {
                            if ( QuestHelper.hasQuestItem( player, MARIAS_2ND_LETTER ) ) {
                                return this.getPath( '30609-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRETAS_1ST_LETTER ) ) {
                                return this.getPath( '30609-06.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, LUCILLAS_HANDBAG ) ) {
                                return this.getPath( '30609-07.html' )
                            }

                            if ( QuestHelper.hasAtLeastOneQuestItem( player, CRERAS_PAINTING1, CRERAS_PAINTING2, CRERAS_PAINTING3 ) ) {
                                return this.getPath( '30609-10.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_PURITY1, SYMBOL_OF_SYLVAIN, MIRIENS_2ND_SIGIL ) ) {
                            return this.getPath( '30609-11.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MIRIENS_3RD_SIGIL ) ) {
                            if ( QuestHelper.hasQuestItem( player, DIETERS_KEY ) ) {
                                return this.getPath( '30609-12.html' )
                            }

                            return this.getPath( '30609-15.html' )
                        }

                        break

                    case ELDER_CRONOS:
                        if ( QuestHelper.hasQuestItem( player, MIRIENS_3RD_SIGIL ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRONOS_SIGIL, SYMBOL_OF_CRONOS ) ) {
                                return this.getPath( '30610-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRONOS_SIGIL ) ) {
                                if ( QuestHelper.hasQuestItems( player, SCRIPTURE_CHAPTER_1, SCRIPTURE_CHAPTER_2, SCRIPTURE_CHAPTER_3, SCRIPTURE_CHAPTER_4 ) ) {
                                    return this.getPath( '30610-12.html' )
                                }

                                return this.getPath( '30610-11.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, SYMBOL_OF_CRONOS ) && !QuestHelper.hasQuestItem( player, CRONOS_SIGIL ) ) {
                                return this.getPath( '30610-15.html' )
                            }
                        }
                        break

                    case DRUNKARD_TRIFF:
                        if ( QuestHelper.hasQuestItems( player, DIETERS_DIARY, SCRIPTURE_CHAPTER_1, STRONG_LIGUOR ) ) {
                            return this.getPath( '30611-01.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, TRIFFS_RING, SYMBOL_OF_CRONOS ) ) {
                            return this.getPath( '30611-05.html' )
                        }

                        break

                    case ELDER_CASIAN:
                        if ( QuestHelper.hasQuestItems( player, TRIFFS_RING, POITANS_NOTES ) ) {
                            if ( !QuestHelper.hasQuestItem( player, CASIANS_LIST ) ) {
                                if ( QuestHelper.hasQuestItems( player, SCRIPTURE_CHAPTER_1, SCRIPTURE_CHAPTER_2, SCRIPTURE_CHAPTER_3 ) ) {
                                    return this.getPath( '30612-02.html' )
                                }

                                return this.getPath( '30612-01.html' )
                            }

                            let totalItems = QuestHelper.getQuestItemsCount( player, GHOULS_SKIN ) +
                                    QuestHelper.getQuestItemsCount( player, MEDUSAS_BLOOD ) +
                                    QuestHelper.getQuestItemsCount( player, FETTERED_SOULS_ICHOR ) +
                                    QuestHelper.getQuestItemsCount( player, ENCHANTED_GARGOYLES_NAIL )
                            if ( totalItems < 32 ) {
                                return this.getPath( '30612-05.html' )
                            }

                            return this.getPath( '30612-06.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, TRIFFS_RING, SCRIPTURE_CHAPTER_1, SCRIPTURE_CHAPTER_2, SCRIPTURE_CHAPTER_3, SCRIPTURE_CHAPTER_4 )
                                && !QuestHelper.hasAtLeastOneQuestItem( player, POITANS_NOTES, CASIANS_LIST ) ) {
                            return this.getPath( '30612-08.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MAGISTER_MIRIEN ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}