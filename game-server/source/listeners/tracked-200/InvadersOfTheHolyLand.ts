import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'

import _ from 'lodash'

const VARKEES = 30566
const BLACK_SOULSTONE = 1475
const RED_SOULSTONE = 1476
const minimumLevel = 6

const monsterRewardChange = {
    20311: 90, // Rakeclaw Imp
    20312: 87, // Rakeclaw Imp Hunter
    20313: 77, // Rakeclaw Imp Chieftain
}

export class InvadersOfTheHolyLand extends ListenerLogic {
    constructor() {
        super( 'Q00273_InvadersOfTheHolyLand', 'listeners/tracked-200/InvadersOfTheHolyLand.ts' )
        this.questId = 273
        this.questItemIds = [ BLACK_SOULSTONE, RED_SOULSTONE ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChange ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00273_InvadersOfTheHolyLand'
    }

    getQuestStartIds(): Array<number> {
        return [ VARKEES ]
    }

    getTalkIds(): Array<number> {
        return [ VARKEES ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let itemId: number = _.random( 100 ) > monsterRewardChange[ data.npcId ] ? RED_SOULSTONE : BLACK_SOULSTONE

        await QuestHelper.rewardSingleQuestItem( player, itemId, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30566-04.htm':
                state.startQuest()
                break

            case '30566-08.html':
                await state.exitQuest( true, true )
                break

            case '30566-09.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.ORC ) {
                    return this.getPath( '30566-01.htm' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30566-03.htm' : '30566-02.htm' )

            case QuestStateValues.STARTED:
                let blackAmount: number = QuestHelper.getQuestItemsCount( player, BLACK_SOULSTONE )
                let redAmount: number = QuestHelper.getQuestItemsCount( player, RED_SOULSTONE )
                let total = blackAmount + redAmount

                if ( total === 0 ) {
                    return this.getPath( '30566-05.html' )
                }

                let amount = ( redAmount * 10 ) + ( blackAmount * 3 ) + ( redAmount > 0 ? ( total >= 10 ? 1800 : 0 ) : ( blackAmount >= 10 ? 1500 : 0 ) )
                await QuestHelper.giveAdena( player, amount, true )
                await QuestHelper.takeMultipleItems( player, -1, BLACK_SOULSTONE, RED_SOULSTONE )
                await NewbieRewardsHelper.giveNewbieReward( player )

                return this.getPath( redAmount > 0 ? '30566-07.html' : '30566-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}