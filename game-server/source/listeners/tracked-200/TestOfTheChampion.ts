import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import _ from 'lodash'

const TRADER_GROOT = 30093
const CAPTAIN_MOUEN = 30196
const VETERAN_ASCALON = 30624
const MASON = 30625

const ASCALONS_1ST_LETTER = 3277
const MASONS_LETTER = 3278
const IRON_ROSE_RING = 3279
const ASCALONS_2ND_LETTER = 3280
const WHITE_ROSE_INSIGNIA = 3281
const GROOTS_LETTER = 3282
const ASCALONS_3RD_LETTER = 3283
const MOUENS_1ST_ORDER = 3284
const MOUENS_2ND_ORDER = 3285
const MOUENS_LETTER = 3286
const HARPYS_EGG = 3287
const MEDUSA_VENOM = 3288
const WINDSUS_BILE = 3289
const BLOODY_AXE_HEAD = 3290
const ROAD_RATMAN_HEAD = 3291
const LETO_LIZARDMAN_FANG = 3292

const MARK_OF_CHAMPION = 3276
const DIMENSIONAL_DIAMOND = 7562

const HARPY = 20145
const MEDUSA = 20158
const ROAD_SCAVENGER = 20551
const WINDSUS = 20553
const LETO_LIZARDMAN = 20577
const LETO_LIZARDMAN_ARCHER = 20578
const LETO_LIZARDMAN_SOLDIER = 20579
const LETO_LIZARDMAN_WARRIOR = 20580
const LETO_LIZARDMAN_SHAMAN = 20581
const LETO_LIZARDMAN_OCERLORD = 20582
const BLOODY_AXE_ELITE = 20780

const HARPY_MATRIARCH = 27088
const ROAD_COLLECTOR = 27089

const minimumLevel = 39

export class TestOfTheChampion extends ListenerLogic {
    constructor() {
        super( 'Q00223_TestOfTheChampion', 'listeners/tracked-200/TestOfTheChampion.ts' )
        this.questId = 223
        this.questItemIds = [
            ASCALONS_1ST_LETTER,
            MASONS_LETTER,
            IRON_ROSE_RING,
            ASCALONS_2ND_LETTER,
            WHITE_ROSE_INSIGNIA,
            GROOTS_LETTER,
            ASCALONS_3RD_LETTER,
            MOUENS_1ST_ORDER,
            MOUENS_2ND_ORDER,
            MOUENS_LETTER,
            HARPYS_EGG,
            MEDUSA_VENOM,
            WINDSUS_BILE,
            BLOODY_AXE_HEAD,
            ROAD_RATMAN_HEAD,
            LETO_LIZARDMAN_FANG,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [ HARPY, ROAD_SCAVENGER, BLOODY_AXE_ELITE ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            HARPY,
            MEDUSA,
            WINDSUS,
            ROAD_SCAVENGER,
            LETO_LIZARDMAN,
            LETO_LIZARDMAN_ARCHER,
            LETO_LIZARDMAN_SOLDIER,
            LETO_LIZARDMAN_WARRIOR,
            LETO_LIZARDMAN_SHAMAN,
            LETO_LIZARDMAN_OCERLORD,
            BLOODY_AXE_ELITE,
            HARPY_MATRIARCH,
            ROAD_COLLECTOR,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00223_TestOfTheChampion'
    }

    getQuestStartIds(): Array<number> {
        return [ VETERAN_ASCALON ]
    }

    getTalkIds(): Array<number> {
        return [ VETERAN_ASCALON, TRADER_GROOT, CAPTAIN_MOUEN, MASON ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName() )

        if ( !state || state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.attackerPlayerId )

        switch ( data.targetNpcId ) {
            case HARPY:
                if ( !NpcVariablesManager.get( data.targetId, this.getName() ) ) {

                    if ( QuestHelper.hasQuestItem( player, WHITE_ROSE_INSIGNIA )
                            && QuestHelper.getQuestItemsCount( player, HARPYS_EGG ) < 30 && Math.random() > 0.5 ) {
                        let count = _.random( 10 ) < 7 ? 1 : 2
                        let npc = L2World.getObjectById( data.targetId ) as L2Npc

                        _.times( count, () => {
                            QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( HARPY_MATRIARCH, npc, true, 0 ), player )
                        } )
                    }

                    NpcVariablesManager.set( data.targetId, this.getName(), true )
                }

                return

            case ROAD_SCAVENGER:
                if ( !NpcVariablesManager.get( data.targetId, this.getName() ) ) {
                    if ( QuestHelper.hasQuestItem( player, MOUENS_1ST_ORDER )
                            && QuestHelper.getQuestItemsCount( player, ROAD_RATMAN_HEAD ) < 10
                            && Math.random() > 0.5 ) {

                        let count = _.random( 10 ) < 7 ? 1 : 2
                        let npc = L2World.getObjectById( data.targetId ) as L2Npc

                        _.times( count, () => {
                            QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( ROAD_COLLECTOR, npc, true, 0 ), player )
                        } )
                    }

                    NpcVariablesManager.set( data.targetId, this.getName(), true )
                }

                return

            case BLOODY_AXE_ELITE:
                if ( !NpcVariablesManager.get( data.targetId, this.getName() ) ) {
                    if ( QuestHelper.hasQuestItem( player, IRON_ROSE_RING )
                            && QuestHelper.getQuestItemsCount( player, BLOODY_AXE_HEAD ) < 10
                            && Math.random() > 0.5 ) {

                        let npc = L2World.getObjectById( data.targetId ) as L2Npc
                        QuestHelper.addAttackDesire( QuestHelper.addSpawnAtLocation( BLOODY_AXE_ELITE, npc, true, 0 ), player )

                    }

                    NpcVariablesManager.set( data.targetId, this.getName(), true )
                }

                return
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.npcId ) {
            case HARPY:
            case HARPY_MATRIARCH:
                if ( QuestHelper.hasQuestItem( player, WHITE_ROSE_INSIGNIA )
                        && QuestHelper.getQuestItemsCount( player, HARPYS_EGG ) < 30 ) {
                    await this.tryToProgressQuest( player, state, HARPYS_EGG, 2, 30, data.isChampion )
                }

                return

            case MEDUSA:
                if ( QuestHelper.hasQuestItem( player, WHITE_ROSE_INSIGNIA )
                        && QuestHelper.getQuestItemsCount( player, MEDUSA_VENOM ) < 30 ) {
                    await this.tryToProgressQuest( player, state, MEDUSA_VENOM, 3, 30, data.isChampion )
                }

                return

            case WINDSUS:
                if ( QuestHelper.hasQuestItem( player, WHITE_ROSE_INSIGNIA )
                        && QuestHelper.getQuestItemsCount( player, WINDSUS_BILE ) < 30 ) {
                    await this.tryToProgressQuest( player, state, WINDSUS_BILE, 3, 30, data.isChampion )
                }

                return

            case ROAD_SCAVENGER:
            case ROAD_COLLECTOR:
                if ( QuestHelper.hasQuestItem( player, MOUENS_1ST_ORDER )
                        && QuestHelper.getQuestItemsCount( player, ROAD_RATMAN_HEAD ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, ROAD_RATMAN_HEAD, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, ROAD_RATMAN_HEAD ) >= 10 ) {
                        state.setConditionWithSound( 11, true )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case LETO_LIZARDMAN:
            case LETO_LIZARDMAN_ARCHER:
            case LETO_LIZARDMAN_SOLDIER:
            case LETO_LIZARDMAN_WARRIOR:
            case LETO_LIZARDMAN_SHAMAN:
            case LETO_LIZARDMAN_OCERLORD:
                if ( QuestHelper.hasQuestItem( player, MOUENS_2ND_ORDER )
                        && QuestHelper.getQuestItemsCount( player, LETO_LIZARDMAN_FANG ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, LETO_LIZARDMAN_FANG, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, LETO_LIZARDMAN_FANG ) >= 10 ) {
                        state.setConditionWithSound( 13, true )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case BLOODY_AXE_ELITE:
                if ( QuestHelper.hasQuestItem( player, IRON_ROSE_RING )
                        && QuestHelper.getQuestItemsCount( player, BLOODY_AXE_HEAD ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, BLOODY_AXE_HEAD, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, BLOODY_AXE_HEAD ) >= 10 ) {
                        state.setConditionWithSound( 3, true )
                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    await QuestHelper.giveSingleItem( player, ASCALONS_1ST_LETTER, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        let amount: number = player.getClassId() === ClassIdValues.warrior.id ? 72 : 64
                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, amount )

                        return this.getPath( '30624-06a.htm' )
                    }

                    return this.getPath( '30624-06.htm' )
                }

                return

            case '30624-05.htm':
            case '30196-02.html':
            case '30625-02.html':
                break

            case '30624-10.html':
                if ( QuestHelper.hasQuestItem( player, MASONS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, MASONS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, ASCALONS_2ND_LETTER, 1 )

                    state.setConditionWithSound( 5, true )
                    break
                }

                return

            case '30624-14.html':
                if ( QuestHelper.hasQuestItem( player, GROOTS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, GROOTS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, ASCALONS_3RD_LETTER, 1 )

                    state.setConditionWithSound( 9, true )
                    break
                }

                return

            case '30093-02.html':
                if ( QuestHelper.hasQuestItem( player, ASCALONS_2ND_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, ASCALONS_2ND_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, WHITE_ROSE_INSIGNIA, 1 )

                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '30196-03.html':
                if ( QuestHelper.hasQuestItem( player, ASCALONS_3RD_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, ASCALONS_3RD_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, MOUENS_1ST_ORDER, 1 )

                    state.setConditionWithSound( 10, true )
                    break
                }

                return

            case '30196-06.html':
                if ( QuestHelper.getQuestItemsCount( player, ROAD_RATMAN_HEAD ) >= 10 ) {
                    await QuestHelper.takeMultipleItems( player, -1, MOUENS_1ST_ORDER, ROAD_RATMAN_HEAD )
                    await QuestHelper.giveSingleItem( player, MOUENS_2ND_ORDER, 1 )

                    state.setConditionWithSound( 12, true )
                    break
                }

                return

            case '30625-03.html':
                if ( QuestHelper.hasQuestItem( player, ASCALONS_1ST_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, ASCALONS_1ST_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, IRON_ROSE_RING, 1 )

                    state.setConditionWithSound( 2, true )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === VETERAN_ASCALON ) {
                    if ( ![ ClassIdValues.warrior.id, ClassIdValues.orcRaider.id ].includes( player.getClassId() ) ) {
                        return this.getPath( '30624-02.html' )
                    }

                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30624-01.html' )
                    }

                    if ( player.getClassId() === ClassIdValues.warrior.id ) {
                        return this.getPath( '30624-03.htm' )
                    }

                    return this.getPath( '30624-04.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case VETERAN_ASCALON:
                        if ( QuestHelper.hasQuestItem( player, ASCALONS_1ST_LETTER ) ) {
                            return this.getPath( '30624-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, IRON_ROSE_RING ) ) {
                            return this.getPath( '30624-08.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MASONS_LETTER ) ) {
                            return this.getPath( '30624-09.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ASCALONS_2ND_LETTER ) ) {
                            return this.getPath( '30624-11.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, WHITE_ROSE_INSIGNIA ) ) {
                            return this.getPath( '30624-12.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, GROOTS_LETTER ) ) {
                            return this.getPath( '30624-13.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ASCALONS_3RD_LETTER ) ) {
                            return this.getPath( '30624-15.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, MOUENS_1ST_ORDER, MOUENS_2ND_ORDER ) ) {
                            return this.getPath( '30624-16.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MOUENS_LETTER ) ) {
                            await QuestHelper.giveAdena( player, 229764, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_CHAMPION, 1 )
                            await QuestHelper.addExpAndSp( player, 1270742, 87200 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                            return this.getPath( '30624-17.html' )
                        }

                        break

                    case TRADER_GROOT:
                        if ( QuestHelper.hasQuestItem( player, ASCALONS_2ND_LETTER ) ) {
                            return this.getPath( '30093-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, WHITE_ROSE_INSIGNIA ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, HARPYS_EGG ) >= 30
                                    && QuestHelper.getQuestItemsCount( player, MEDUSA_VENOM ) >= 30
                                    && QuestHelper.getQuestItemsCount( player, WINDSUS_BILE ) >= 30 ) {
                                await QuestHelper.giveSingleItem( player, GROOTS_LETTER, 1 )
                                await QuestHelper.takeMultipleItems( player, -1, WHITE_ROSE_INSIGNIA, HARPYS_EGG, MEDUSA_VENOM, WINDSUS_BILE )

                                state.setConditionWithSound( 8, true )
                                return this.getPath( '30093-04.html' )
                            }

                            return this.getPath( '30093-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, GROOTS_LETTER ) ) {
                            return this.getPath( '30093-05.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, ASCALONS_3RD_LETTER, MOUENS_1ST_ORDER, MOUENS_2ND_ORDER, MOUENS_LETTER ) ) {
                            return this.getPath( '30093-06.html' )
                        }

                        break

                    case CAPTAIN_MOUEN:
                        if ( QuestHelper.hasQuestItem( player, ASCALONS_3RD_LETTER ) ) {
                            return this.getPath( '30196-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MOUENS_1ST_ORDER ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, ROAD_RATMAN_HEAD ) < 10 ) {
                                return this.getPath( '30196-04.html' )
                            }

                            return this.getPath( '30196-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MOUENS_2ND_ORDER ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, LETO_LIZARDMAN_FANG ) < 10 ) {
                                return this.getPath( '30196-07.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, -1, MOUENS_2ND_ORDER, LETO_LIZARDMAN_FANG )
                            await QuestHelper.giveSingleItem( player, MOUENS_LETTER, 1 )

                            state.setConditionWithSound( 14, true )
                            return this.getPath( '30196-08.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MOUENS_LETTER ) ) {
                            return this.getPath( '30196-09.html' )
                        }

                        break

                    case MASON:
                        if ( QuestHelper.hasQuestItem( player, ASCALONS_1ST_LETTER ) ) {
                            return this.getPath( '30625-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, IRON_ROSE_RING ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, BLOODY_AXE_HEAD ) < 10 ) {
                                return this.getPath( '30625-04.html' )
                            }

                            await QuestHelper.giveSingleItem( player, MASONS_LETTER, 1 )
                            await QuestHelper.takeMultipleItems( player, -1, IRON_ROSE_RING, BLOODY_AXE_HEAD )

                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30625-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MASONS_LETTER ) ) {
                            return this.getPath( '30625-06.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, ASCALONS_2ND_LETTER, WHITE_ROSE_INSIGNIA, GROOTS_LETTER, ASCALONS_3RD_LETTER, MOUENS_1ST_ORDER, MOUENS_2ND_ORDER, MOUENS_LETTER ) ) {
                            return this.getPath( '30625-07.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === VETERAN_ASCALON ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async tryToProgressQuest( player: L2PcInstance, state: QuestState, itemId: number, count: number, limit: number, isFromChampion: boolean ): Promise<void> {
        await QuestHelper.rewardSingleQuestItem( player, itemId, count, isFromChampion )
        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= limit ) {

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
            if ( QuestHelper.getQuestItemsCount( player, HARPYS_EGG ) >= 30
                    && QuestHelper.getQuestItemsCount( player, MEDUSA_VENOM ) >= 30
                    && QuestHelper.getQuestItemsCount( player, WINDSUS_BILE ) >= 30 ) {
                state.setConditionWithSound( 7 )
            }
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}