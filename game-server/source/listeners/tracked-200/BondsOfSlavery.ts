import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'

import _ from 'lodash'

const IMP_SHACKLES = 1368
const KRISTIN = 30357
const minimumLevel = 6

const monsterRewardChances = {
    20004: 5, // Imp
    20005: 6, // Imp Elder
}

export class BondsOfSlavery extends ListenerLogic {
    constructor() {
        super( 'Q00265_BondsOfSlavery', 'listeners/tracked-200/BondsOfSlavery.ts' )
        this.questId = 265
        this.questItemIds = [ IMP_SHACKLES ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00265_BondsOfSlavery'
    }

    getQuestStartIds(): Array<number> {
        return [ KRISTIN ]
    }

    getTalkIds(): Array<number> {
        return [ KRISTIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || _.random( 10 ) <= QuestHelper.getAdjustedChance( IMP_SHACKLES, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, IMP_SHACKLES, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30357-04.htm':
                state.startQuest()
                break

            case '30357-07.html':
                await state.exitQuest( true, true )
                break

            case '30357-08.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.DARK_ELF ) {
                    return this.getPath( '30357-01.html' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30357-03.htm' : '30357-02.html' )

            case QuestStateValues.STARTED:
                let shackles = QuestHelper.getQuestItemsCount( player, IMP_SHACKLES )
                if ( shackles === 0 ) {
                    return this.getPath( '30357-05.html' )
                }

                let amount: number = ( shackles * 12 ) + ( shackles >= 10 ? 500 : 0 )

                await QuestHelper.giveAdena( player, amount, true )
                await QuestHelper.takeSingleItem( player, IMP_SHACKLES, -1 )
                await NewbieRewardsHelper.giveNewbieReward( player )

                return this.getPath( '30357-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}