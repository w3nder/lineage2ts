import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ConfigManager } from '../../config/ConfigManager'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import {
    AttackableKillEvent,
    EventType,
    NpcApproachedForTalkEvent,
    NpcGeneralEvent,
    PlayerChangedLevelEvent,
    PlayerLoginEvent,
    PlayerPickupItemEvent,
    PlayerSitEvent,
    PlayerStartsAttackEvent,
    PlayerTutorialClientEvent,
    PlayerTutorialCommandEvent,
    PlayerTutorialLinkEvent,
    PlayerTutorialQuestionMarkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { Voice } from '../../gameService/enums/audio/Voice'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { ClassId, ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import { Race } from '../../gameService/enums/Race'
import { ListenerCache } from '../../gameService/cache/ListenerCache'
import { EventListenerData } from '../../gameService/models/events/listeners/EventListenerData'
import { TutorialEnableClientEvent } from '../../gameService/packets/send/TutorialEnableClientEvent'
import { TutorialCloseHtml } from '../../gameService/packets/send/TutorialCloseHtml'
import { Location } from '../../gameService/models/Location'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { ShowHtml } from '../helpers/ShowHtml'
import { TerritoryWarManager } from '../../gameService/instancemanager/TerritoryWarManager'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { PlayerRadarCache } from '../../gameService/cache/PlayerRadarCache'
import _ from 'lodash'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'

const ROIEN = 30008
const NEWBIE_HELPER_HUMAN_FIGHTER = 30009
const GALLINT = 30017
const NEWBIE_HELPER_HUMAN_MAGE = 30019
const MITRAELL = 30129
const NEWBIE_HELPER_DARK_ELF = 30131
const NERUPA = 30370
const NEWBIE_HELPER_ELF = 30400
const LAFERON = 30528
const NEWBIE_HELPER_DWARF = 30530
const VULKUS = 30573
const NEWBIE_HELPER_ORC = 30575
const PERWAN = 32133
const NEWBIE_HELPER_KAMAEL = 32134

const npcIds: Array<number> = [
    ROIEN,
    NEWBIE_HELPER_HUMAN_FIGHTER,
    GALLINT,
    NEWBIE_HELPER_HUMAN_MAGE,
    MITRAELL,
    NEWBIE_HELPER_DARK_ELF,
    NERUPA,
    NEWBIE_HELPER_ELF,
    LAFERON,
    NEWBIE_HELPER_DWARF,
    VULKUS,
    NEWBIE_HELPER_ORC,
    PERWAN,
    NEWBIE_HELPER_KAMAEL,
]

const TUTORIAL_GREMLIN = 18342

const SOULSHOT_NO_GRADE_FOR_BEGINNERS = 5789
const SPIRITSHOT_NO_GRADE_FOR_BEGINNERS = 5790
const BLUE_GEMSTONE = 6353
const TUTORIAL_GUIDE = 5588

const RECOMMENDATION_1 = 1067
const RECOMMENDATION_2 = 1068
const LEAF_OF_THE_MOTHER_TREE = 1069
const BLOOD_OF_MITRAELL = 1070
const LICENSE_OF_MINER = 1498
const VOUCHER_OF_FLAME = 1496
const DIPLOMA = 9881

// Connected quests
const Q10276_MUTATED_KANEUS_GLUDIO = 10276
const Q10277_MUTATED_KANEUS_DION = 10277
const Q10278_MUTATED_KANEUS_HEINE = 10278
const Q10279_MUTATED_KANEUS_OREN = 10279
const Q10280_MUTATED_KANEUS_SCHUTTGART = 10280
const Q10281_MUTATED_KANEUS_RUNE = 10281
const Q192_SEVEN_SIGNS_SERIES_OF_DOUBT = 192
const Q10292_SEVEN_SIGNS_GIRL_OF_DOUBT = 10292
const Q234_FATES_WHISPER = 234
const Q128_PAILAKA_SONG_OF_ICE_AND_FIRE = 128
const Q129_PAILAKA_DEVILS_LEGACY = 129
const Q144_PAIRAKA_WOUNDED_DRAGON = 144

const Q729_PROTECT_THE_TERRITORY_CATAPULT = 729
const Q730_PROTECT_THE_SUPPLIES_SAFE = 730
const Q731_PROTECT_THE_MILITARY_ASSOCIATION_LEADER = 731
const Q732_PROTECT_THE_RELIGIOUS_ASSOCIATION_LEADER = 732
const Q733_PROTECT_THE_ECONOMIC_ASSOCIATION_LEADER = 733

const Q201_TUTORIAL_HUMAN_FIGHTER = 201
const Q202_TUTORIAL_HUMAN_MAGE = 202
const Q203_TUTORIAL_ELF = 203
const Q204_TUTORIAL_DARK_ELF = 204
const Q205_TUTORIAL_ORC = 205
const Q206_TUTORIAL_DWARF = 206

const Q717_FOR_THE_SAKE_OF_THE_TERRITORY_GLUDIO = 717
const Q718_FOR_THE_SAKE_OF_THE_TERRITORY_DION = 718
const Q719_FOR_THE_SAKE_OF_THE_TERRITORY_GIRAN = 719
const Q720_FOR_THE_SAKE_OF_THE_TERRITORY_OREN = 720
const Q721_FOR_THE_SAKE_OF_THE_TERRITORY_ADEN = 721
const Q722_FOR_THE_SAKE_OF_THE_TERRITORY_INNADRIL = 722
const Q723_FOR_THE_SAKE_OF_THE_TERRITORY_GODDARD = 723
const Q724_FOR_THE_SAKE_OF_THE_TERRITORY_RUNE = 724
const Q725_FOR_THE_SAKE_OF_THE_TERRITORY_SCHUTTGART = 725
const Q728_TERRITORY_WAR = 728

const minimumLevel = 6
const tutorialEvent = 'tutorial'
const teleportLocation = new Location( -120050, 44500, 360 )

export class Tutorial extends ListenerLogic {
    constructor() {
        super( 'Q00255_Tutorial', 'listeners/tracked-200/Tutorial.ts' )
        this.questId = 255
    }

    enableTutorialEvent( state: QuestState, status: number ): void {
        if ( ( ( status & ( 1048576 | 2097152 ) ) !== 0 ) ) {
            this.addPlayerItemEvent( state.getPlayerId() )
        } else if ( ListenerCache.hasObjectListener( state.getPlayerId(), EventType.PlayerPickupItem, this ) ) {
            this.removeListener( state.getPlayerId(), EventType.PlayerPickupItem )
        }

        if ( ( status & 8388608 ) !== 0 ) {
            this.addPlayerSitEvent( state.getPlayerId() )
        } else if ( ListenerCache.hasObjectListener( state.getPlayerId(), EventType.PlayerSit, this ) ) {
            this.removeListener( state.getPlayerId(), EventType.PlayerSit )
        }

        if ( ( status & 256 ) !== 0 ) {
            this.addPlayerAttackEvent( state.getPlayerId() )
        } else {
            this.removeListener( state.getPlayerId(), EventType.PlayerStartsAttack )
        }

        PacketDispatcher.sendOwnedData( state.getPlayerId(), TutorialEnableClientEvent( status ) )
    }

    async eventForemanLaferon( eventId: number, player: L2PcInstance, npcId: number, npcObjectId: number, state: QuestState ): Promise<void> {
        switch ( eventId ) {
            case 31:
                if ( QuestHelper.hasQuestItems( player, LICENSE_OF_MINER ) ) {
                    let needSoulshots = QuestHelper.getQuestItemsCount( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS ) <= 200
                    if ( !player.isMageClass() && needSoulshots ) {

                        player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                        await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                        await QuestHelper.addExpAndSp( player, 0, 50 )
                    }

                    let needSpiritshots = QuestHelper.getQuestItemsCount( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS )
                    if ( player.isMageClass() && needSpiritshots <= 100 ) {
                        player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
                        await QuestHelper.rewardSingleItem( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS, 100 )
                        await QuestHelper.addExpAndSp( player, 0, 50 )
                    }

                    await QuestHelper.takeSingleItem( player, LICENSE_OF_MINER, 1 )
                    this.startQuestTimer( npcId.toString(), 60000, npcObjectId, player.getObjectId() )

                    if ( state.getMemoStateEx( 1 ) <= 3 ) {
                        state.setMemoStateEx( 1, 4 )
                    }

                    return ShowHtml.showPath( player, this.getPath( '30528-002.htm' ) )
                }

                return

            case 41:
                await QuestHelper.teleportPlayer( player, teleportLocation, 0 )
                state.addRadar( -119692, 44504, 380 )
                return ShowHtml.showPath( player, this.getPath( '30528-005.htm' ) )

            case 42:
                state.addRadar( 115632, -177996, -905 )
                return ShowHtml.showPath( player, this.getPath( '30528-006.htm' ) )
        }
    }

    async eventGallin( eventId: number, player: L2PcInstance, npcId: number, npcObjectId: number, state: QuestState ): Promise<void> {
        switch ( eventId ) {
            case 31:
                if ( QuestHelper.hasQuestItems( player, RECOMMENDATION_2 ) ) {
                    let needSouldshots = QuestHelper.getQuestItemsCount( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS ) <= 200
                    if ( !player.isMageClass() && needSouldshots ) {
                        player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                        await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                        await QuestHelper.addExpAndSp( player, 0, 50 )
                    }

                    let needSpiritshots = QuestHelper.getQuestItemsCount( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS ) <= 100
                    if ( player.isMageClass()
                            && needSouldshots
                            && needSpiritshots ) {

                        if ( player.getClassId() === ClassIdValues.orcMage.id ) {
                            player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                            await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                        } else {
                            player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
                            await QuestHelper.rewardSingleItem( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                        }

                        await QuestHelper.addExpAndSp( player, 0, 50 )
                    }

                    await QuestHelper.takeSingleItem( player, RECOMMENDATION_2, 1 )
                    this.startQuestTimer( npcId.toString(), 60000, npcObjectId, player.getObjectId() )

                    if ( state.getMemoStateEx( 1 ) <= 3 ) {
                        state.setMemoStateEx( 1, 4 )
                    }

                    return ShowHtml.showPath( player, this.getPath( '30017-002.htm' ) )
                }

                return

            case 41:
                await QuestHelper.teleportPlayer( player, teleportLocation, 0 )
                state.addRadar( -119692, 44504, 380 )
                return ShowHtml.showPath( player, this.getPath( '30017-005.htm' ) )

            case 42:
                state.addRadar( -84081, 243277, -3723 )
                return ShowHtml.showPath( player, this.getPath( '30017-006.htm' ) )
        }
    }

    async eventGuardianVullkus( eventId: number, player: L2PcInstance, npcId: number, npcObjectId: number, state: QuestState ): Promise<void> {
        switch ( eventId ) {
            case 31:
                if ( QuestHelper.hasQuestItems( player, VOUCHER_OF_FLAME ) ) {
                    await QuestHelper.takeSingleItem( player, VOUCHER_OF_FLAME, 1 )
                    this.startQuestTimer( npcId.toString(), 60000, npcObjectId, player.getObjectId() )

                    if ( state.getMemoStateEx( 1 ) <= 3 ) {
                        state.setMemoStateEx( 1, 4 )
                    }

                    if ( QuestHelper.getQuestItemsCount( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS ) <= 200 ) {
                        await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                        await QuestHelper.addExpAndSp( player, 0, 50 )
                    }

                    player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                    return ShowHtml.showPath( player, this.getPath( '30573-002.htm' ) )
                }

                return

            case 41:
                await QuestHelper.teleportPlayer( player, teleportLocation, 0 )
                state.addRadar( -119692, 44504, 380 )
                return ShowHtml.showPath( player, this.getPath( '30573-005.htm' ) )

            case 42:
                state.addRadar( -45032, -113598, -192 )
                return ShowHtml.showPath( player, this.getPath( '30573-006.htm' ) )
        }
    }

    async eventJundin( eventId: number, player: L2PcInstance, npcId: number, npcObjectId: number, state: QuestState ): Promise<void> {
        switch ( eventId ) {
            case 31:
                if ( QuestHelper.hasQuestItems( player, BLOOD_OF_MITRAELL ) ) {

                    let needSoulshots = QuestHelper.getQuestItemsCount( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS ) <= 200
                    if ( !player.isMageClass() && needSoulshots ) {
                        player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                        await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                        await QuestHelper.addExpAndSp( player, 0, 50 )
                    }

                    let needSpiritshots = QuestHelper.getQuestItemsCount( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS ) <= 100
                    if ( player.isMageClass() && needSoulshots && needSpiritshots ) {
                        if ( player.getClassId() === ClassIdValues.orcMage.id ) {
                            player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                            await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                        } else {
                            player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
                            await QuestHelper.rewardSingleItem( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                        }

                        await QuestHelper.addExpAndSp( player, 0, 50 )
                    }

                    await QuestHelper.takeSingleItem( player, BLOOD_OF_MITRAELL, 1 )
                    this.startQuestTimer( npcId.toString(), 60000, npcObjectId, player.getObjectId() )

                    if ( state.getMemoStateEx( 1 ) <= 3 ) {
                        state.setMemoStateEx( 1, 4 )
                    }

                    return ShowHtml.showPath( player, this.getPath( '30129-002.htm' ) )
                }

                return

            case 41:
                await QuestHelper.teleportPlayer( player, teleportLocation, 0 )
                state.addRadar( -119692, 44504, 380 )
                return ShowHtml.showPath( player, this.getPath( '30129-005.htm' ) )

            case 42:
                state.addRadar( 17024, 13296, -3744 )
                return ShowHtml.showPath( player, this.getPath( '30129-006.htm' ) )
        }
    }

    async eventNerupa( eventId: number, player: L2PcInstance, npcId: number, npcObjectId: number, state: QuestState ): Promise<void> {
        switch ( eventId ) {
            case 31:
                if ( QuestHelper.hasQuestItems( player, LEAF_OF_THE_MOTHER_TREE ) ) {
                    await QuestHelper.takeSingleItem( player, LEAF_OF_THE_MOTHER_TREE, 1 )

                    let needSoulshots = QuestHelper.getQuestItemsCount( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS ) <= 200
                    if ( !player.isMageClass() && needSoulshots ) {
                        player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                        await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                        await QuestHelper.addExpAndSp( player, 0, 50 )
                    }

                    let needSpiritshots = QuestHelper.getQuestItemsCount( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS ) <= 100
                    if ( player.isMageClass() && needSoulshots && needSpiritshots ) {
                        player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
                        await QuestHelper.rewardSingleItem( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                        await QuestHelper.addExpAndSp( player, 0, 50 )
                    }

                    this.startQuestTimer( npcId.toString(), 60000, npcObjectId, player.getObjectId() )

                    if ( state.getMemoStateEx( 1 ) <= 3 ) {
                        state.setMemoStateEx( 1, 4 )
                    }

                    return ShowHtml.showPath( player, this.getPath( '30370-002.htm' ) )
                }

                return

            case 41:
                await QuestHelper.teleportPlayer( player, teleportLocation, 0 )
                state.addRadar( -119692, 44504, 380 )
                return ShowHtml.showPath( player, this.getPath( '30370-005.htm' ) )

            case 42:
                state.addRadar( 45475, 48359, -3060 )
                return ShowHtml.showPath( player, this.getPath( '30370-006.htm' ) )
        }
    }

    async eventRoien( eventId: number, player: L2PcInstance, npcId: number, npcObjectId: number, state: QuestState ): Promise<void> {
        switch ( eventId ) {
            case 31:
                if ( QuestHelper.hasQuestItems( player, RECOMMENDATION_1 ) ) {
                    let hasCorrectState = state.getMemoStateEx( 1 ) <= 3
                    if ( !player.isMageClass() && hasCorrectState ) {
                        await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                        player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )

                        await QuestHelper.addExpAndSp( player, 0, 50 )
                        state.setMemoStateEx( 1, 4 )
                    }

                    if ( player.isMageClass() && hasCorrectState ) {
                        if ( player.getClassId() === ClassIdValues.orcMage.id ) {
                            await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                            player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                        } else {
                            await QuestHelper.rewardSingleItem( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS, 100 )
                            player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
                        }

                        await QuestHelper.addExpAndSp( player, 0, 50 )
                        state.setMemoStateEx( 1, 4 )
                    }

                    this.startQuestTimer( npcId.toString(), 60000, npcObjectId, player.getObjectId() )
                    await QuestHelper.takeSingleItem( player, RECOMMENDATION_1, 1 )
                    return ShowHtml.showPath( player, this.getPath( '30008-002.htm' ) )
                }

                return

            case 41:
                await QuestHelper.teleportPlayer( player, teleportLocation, 0 )
                return ShowHtml.showPath( player, this.getPath( '30008-005.htm' ) )

            case 42:
                state.addRadar( -84081, 243277, -3723 )
                return ShowHtml.showPath( player, this.getPath( '30008-006.htm' ) )
        }
    }

    async eventSubelderPerwan( eventId: number, player: L2PcInstance, npcId: number, npcObjectId: number, state: QuestState ): Promise<void> {
        if ( eventId === 31 && QuestHelper.hasQuestItems( player, DIPLOMA ) ) {
            if ( player.getRace() === Race.KAMAEL
                    && ClassId.getLevelByClassId( player.getClassId() ) === 0
                    && ( state.getMemoStateEx( 1 ) <= 3 ) ) {

                await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )

                await QuestHelper.addExpAndSp( player, 0, 50 )
                state.setMemoStateEx( 1, 4 )
            }

            await QuestHelper.takeSingleItem( player, DIPLOMA, -1 )
            this.startQuestTimer( npcId.toString(), 60000, npcObjectId, player.getObjectId() )
            state.addRadar( -119692, 44504, 380 )
            return ShowHtml.showPath( player, this.getPath( '32133-002.htm' ) )
        }
    }

    async fireEvent( player: L2PcInstance ): Promise<void> {
        if ( !player || player.isDead() || !player.isPlayer() ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.name )

        switch ( state.getMemoStateEx( 1 ) ) {
            case -2:
                switch ( player.getClassId() ) {
                    case ClassIdValues.fighter.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_001A_2000 )
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-fighter-001.htm' ) )
                        break

                    case ClassIdValues.mage.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_001B_2000 )
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-mage-001.htm' ) )
                        break

                    case ClassIdValues.elvenFighter.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_001C_2000 )
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-elven-fighter-001.htm' ) )
                        break

                    case ClassIdValues.elvenMage.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_001D_2000 )
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-elven-mage-001.htm' ) )
                        break

                    case ClassIdValues.darkFighter.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_001E_2000 )
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-delf-fighter-001.htm' ) )
                        break

                    case ClassIdValues.darkMage.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_001F_2000 )
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-delf-mage-001.htm' ) )
                        break

                    case ClassIdValues.orcFighter.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_001G_2000 )
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-orc-fighter-001.htm' ) )
                        break

                    case ClassIdValues.orcMage.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_001H_2000 )
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-orc-mage-001.htm' ) )
                        break

                    case ClassIdValues.dwarvenFighter.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_001I_2000 )
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-dwarven-fighter-001.htm' ) )
                        break

                    case ClassIdValues.maleSoldier.id:
                    case ClassIdValues.femaleSoldier.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_001K_2000 )
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-kamael-001.htm' ) )
                        break
                }

                if ( !QuestHelper.hasQuestItems( player, TUTORIAL_GUIDE ) ) {
                    await QuestHelper.giveSingleItem( player, TUTORIAL_GUIDE, 1 )
                }

                this.startQuestTimer( tutorialEvent, 30000, undefined, player.getObjectId() )
                state.setMemoStateEx( 1, -3 )
                break

            case -3:
                player.sendCopyData( Voice.TUTORIAL_VOICE_002_1000 )
                break

            case -4:
                player.sendCopyData( Voice.TUTORIAL_VOICE_008_1000 )
                state.setMemoStateEx( 1, -5 )
                break

        }
    }

    getAttackableKillIds(): Array<number> {
        if ( ConfigManager.character.tutorial() ) {
            return [ TUTORIAL_GREMLIN ]
        }
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerLogin,
                method: this.onEnterWorldEvent.bind( this ),
                ids: null,
            },
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerTutorialLink,
                method: this.onPlayerTutorialLink.bind( this ),
                ids: null,
            },
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerTutorialClient,
                method: this.onPlayerTutorialClient.bind( this ),
                ids: null,
            },
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerTutorialQuestionMark,
                method: this.onPlayerTutorialQuestionMark.bind( this ),
                ids: null,
            },
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerTutorialCommand,
                method: this.onPlayerTutorialCommand.bind( this ),
                ids: null,
            },
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerChangedLevel,
                method: this.onLevelUp.bind( this ),
                ids: null,
            },
        ]
    }

    getApproachedForTalkIds(): Array<number> {
        if ( ConfigManager.character.tutorial() ) {
            return npcIds
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00255_Tutorial'
    }

    getQuestStartIds(): Array<number> {
        if ( ConfigManager.character.tutorial() ) {
            return npcIds
        }
    }

    getTalkIds(): Array<number> {
        if ( ConfigManager.character.tutorial() ) {
            return npcIds
        }
    }

    isVisibleInQuestWindow(): boolean {
        return false
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = this.getQuestState( data.playerId, false )

        if ( [ 0, 1 ].includes( state.getMemoStateEx( 1 ) ) ) {
            player.sendCopyData( Voice.TUTORIAL_VOICE_011_1000 )
            state.showQuestionMark( 3 )
            state.setMemoStateEx( 1, 2 )
        }

        if ( [ 0, 1, 2 ].includes( state.getMemoStateEx( 1 ) )
                && !QuestHelper.hasQuestItem( player, BLUE_GEMSTONE )
                && Math.random() < QuestHelper.getAdjustedChance( BLUE_GEMSTONE, 0.5, data.isChampion ) ) {
            let npc: L2Npc = L2World.getObjectById( data.targetId ) as L2Npc
            npc.dropSingleItem( BLUE_GEMSTONE, 1, data.playerId )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
        }

        return
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = this.getQuestState( data.playerId, true )

        switch ( data.characterNpcId ) {
            case ROIEN:
                this.talkRoien( player, state )
                return

            case NEWBIE_HELPER_HUMAN_FIGHTER:
                await this.talkCarl( data.characterNpcId, data.characterId, player, state )
                return

            case GALLINT:
                this.talkGallin( player, state )
                return

            case NEWBIE_HELPER_HUMAN_MAGE:
                await this.talkDoff( data.characterNpcId, data.characterId, player, state )
                return

            case MITRAELL:
                this.talkJundin( player, state )
                return

            case NEWBIE_HELPER_DARK_ELF:
                await this.talkPoeny( data.characterNpcId, data.characterId, player, state )
                return

            case NERUPA:
                this.talkNerupa( player, state )
                return

            case NEWBIE_HELPER_ELF:
                await this.talkMotherTemp( data.characterNpcId, data.characterId, player, state )
                return

            case LAFERON:
                this.talkForemanLaferon( player, state )
                return

            case NEWBIE_HELPER_DWARF:
                await this.talkMinerMai( data.characterNpcId, data.characterId, player, state )
                return

            case VULKUS:
                this.talkGuardianVullkus( player, state )
                return

            case NEWBIE_HELPER_ORC:
                await this.talkShelaPriestess( data.characterNpcId, data.characterId, player, state )
                return

            case PERWAN:
                this.talkSubelderPerwan( player, state )
                return

            case NEWBIE_HELPER_KAMAEL:
                await this.talkHelperKrenisk( data.characterNpcId, data.characterId, player, state )
                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( data.eventName === tutorialEvent ) {
            await this.fireEvent( player )
            return
        }

        if ( player.isDead() ) {
            return
        }

        let state: QuestState = this.getQuestState( data.playerId, true )
        let eventId = _.parseInt( data.eventName )

        switch ( data.characterNpcId ) {
            case NEWBIE_HELPER_HUMAN_FIGHTER:
                switch ( state.getMemoStateEx( 1 ) ) {
                    case 0:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_009A )
                        state.setMemoStateEx( 1, 1 )
                        break

                    case 3:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_010A )
                        break
                }

                break

            case NEWBIE_HELPER_HUMAN_MAGE:
                switch ( state.getMemoStateEx( 1 ) ) {
                    case 0:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_009B )
                        state.setMemoStateEx( 1, 1 )
                        break

                    case 3:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_010B )
                        break
                }

                break

            case NEWBIE_HELPER_DARK_ELF:
                switch ( state.getMemoStateEx( 1 ) ) {
                    case 0:
                        if ( !player.isMageClass() ) {
                            player.sendCopyData( Voice.TUTORIAL_VOICE_009A )
                        } else {
                            player.sendCopyData( Voice.TUTORIAL_VOICE_009B )
                        }

                        state.setMemoStateEx( 1, 1 )
                        break

                    case 3:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_010D )
                        break
                }

                break

            case NEWBIE_HELPER_ELF:
                switch ( state.getMemoStateEx( 1 ) ) {
                    case 0:
                        if ( !player.isMageClass() ) {
                            player.sendCopyData( Voice.TUTORIAL_VOICE_009A )
                        } else {
                            player.sendCopyData( Voice.TUTORIAL_VOICE_009B )
                        }

                        state.setMemoStateEx( 1, 1 )
                        break

                    case 3:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_010C )
                        break
                }

                break

            case NEWBIE_HELPER_DWARF:
                switch ( state.getMemoStateEx( 1 ) ) {
                    case 0:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_009A )
                        state.setMemoStateEx( 1, 1 )
                        break

                    case 3:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_010F )
                        break
                }

                break

            case NEWBIE_HELPER_ORC:
                switch ( state.getMemoStateEx( 1 ) ) {
                    case 0:
                        if ( !player.isMageClass() ) {
                            player.sendCopyData( Voice.TUTORIAL_VOICE_009A )
                        } else {
                            player.sendCopyData( Voice.TUTORIAL_VOICE_009C )
                        }

                        state.setMemoStateEx( 1, 1 )
                        break

                    case 3:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_010E )
                        break
                }

                break

            case NEWBIE_HELPER_KAMAEL:
                switch ( state.getMemoStateEx( 1 ) ) {
                    case 0:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_009A )
                        state.setMemoStateEx( 1, 1 )
                        break

                    case 3:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_010G )
                        break
                }

                break

            case ROIEN:
                if ( eventId === ROIEN ) {
                    if ( state.getMemoStateEx( 1 ) >= 4 ) {
                        state.showQuestionMark( 7 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                        player.sendCopyData( Voice.TUTORIAL_VOICE_025_1000 )
                    }

                    return
                }

                await this.eventRoien( eventId, player, data.characterNpcId, data.characterId, state )
                break

            case GALLINT:
                await this.eventGallin( eventId, player, data.characterNpcId, data.characterId, state )
                break

            case MITRAELL:
                if ( eventId === MITRAELL ) {
                    if ( state.getMemoStateEx( 1 ) >= 4 ) {
                        state.showQuestionMark( 7 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                        player.sendCopyData( Voice.TUTORIAL_VOICE_025_1000 )
                    }

                    return
                }

                await this.eventJundin( eventId, player, data.characterNpcId, data.characterId, state )
                break

            case NERUPA:
                if ( eventId === NERUPA ) {
                    if ( state.getMemoStateEx( 1 ) >= 4 ) {
                        state.showQuestionMark( 7 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                        player.sendCopyData( Voice.TUTORIAL_VOICE_025_1000 )
                    }

                    return
                }

                await this.eventNerupa( eventId, player, data.characterNpcId, data.characterId, state )
                break

            case LAFERON:
                if ( eventId === LAFERON ) {
                    if ( state.getMemoStateEx( 1 ) >= 4 ) {
                        state.showQuestionMark( 7 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                        player.sendCopyData( Voice.TUTORIAL_VOICE_025_1000 )
                    }

                    return
                }

                await this.eventForemanLaferon( eventId, player, data.characterNpcId, data.characterId, state )
                break

            case VULKUS:
                if ( eventId === VULKUS ) {
                    if ( state.getMemoStateEx( 1 ) >= 4 ) {
                        state.showQuestionMark( 7 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                        player.sendCopyData( Voice.TUTORIAL_VOICE_025_1000 )
                    }

                    return
                }

                await this.eventGuardianVullkus( eventId, player, data.characterNpcId, data.characterId, state )
                break

            case PERWAN:
                if ( eventId === PERWAN ) {
                    if ( state.getMemoStateEx( 1 ) >= 4 ) {
                        state.showQuestionMark( 7 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                        player.sendCopyData( Voice.TUTORIAL_VOICE_025_1000 )
                    }

                    return
                }

                await this.eventSubelderPerwan( eventId, player, data.characterNpcId, data.characterId, state )
                break
        }
    }

    async onEnterWorldEvent( data: PlayerLoginEvent ): Promise<string> {
        await this.onUserConnected( L2World.getPlayer( data.playerId ) )
        return
    }

    async onLevelUp( data: PlayerChangedLevelEvent ): Promise<void> {
        let player = L2World.getPlayer( data.playerId )
        let level = data.newLevelValue

        switch ( level ) {
            case 5:
                return this.tutorialEvent( player, 1024 )

            case 6:
                return this.tutorialEvent( player, 134217728 )

            case 7:
                return this.tutorialEvent( player, 2048 )

            case 9:
                return this.tutorialEvent( player, 268435456 )

            case 10:
                return this.tutorialEvent( player, 536870912 )

            case 12:
                return this.tutorialEvent( player, 1073741824 )

            case 15:
                return this.tutorialEvent( player, 67108864 )

            case 18:
                this.tutorialEvent( player, 4096 )

                if ( !QuestHelper.haveMemo( player.getObjectId(), Q10276_MUTATED_KANEUS_GLUDIO )
                        || QuestHelper.getOneTimeQuestFlag( data.playerId, Q10276_MUTATED_KANEUS_GLUDIO ) === 0 ) {
                    ShowHtml.showTutorialHTML( player, 'tw-gludio.htm' )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( -13900, 123822, -3112 )
                }

                return

            case 28:
                if ( !QuestHelper.haveMemo( player.getObjectId(), Q10277_MUTATED_KANEUS_DION ) || QuestHelper.getOneTimeQuestFlag( data.playerId, Q10277_MUTATED_KANEUS_DION ) === 0 ) {
                    ShowHtml.showTutorialHTML( player, 'tw-dion.htm' )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( 18199, 146081, -3080 )
                }

                return

            case 35:
                return this.tutorialEvent( player, 16777216 )

            case 36:
                return this.tutorialEvent( player, 32 )

            case 38:
                if ( !QuestHelper.haveMemo( player.getObjectId(), Q10278_MUTATED_KANEUS_HEINE ) || QuestHelper.getOneTimeQuestFlag( data.playerId, Q10278_MUTATED_KANEUS_HEINE ) === 0 ) {
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tw-heine.htm' ) )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( 108384, 221563, -3592 )
                }

                return

            case 39:
                if ( player.getRace() === Race.KAMAEL ) {
                    this.tutorialEvent( player, 16384 )
                }

                return

            case 48:
                if ( !QuestHelper.haveMemo( player.getObjectId(), Q10279_MUTATED_KANEUS_OREN ) || QuestHelper.getOneTimeQuestFlag( data.playerId, Q10279_MUTATED_KANEUS_OREN ) === 0 ) {
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tw-oren.htm' ) )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( 81023, 56456, -1552 )
                }

                return

            case 58:
                if ( !QuestHelper.haveMemo( player.getObjectId(), Q10280_MUTATED_KANEUS_SCHUTTGART ) || QuestHelper.getOneTimeQuestFlag( data.playerId, Q10280_MUTATED_KANEUS_SCHUTTGART ) === 0 ) {
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tw-schuttgart.htm' ) )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( 85868, -142164, -1342 )
                }

                return

            case 61:
                this.tutorialEvent( player, 64 )
                return

            case 68:
                if ( !QuestHelper.haveMemo( player.getObjectId(), Q10281_MUTATED_KANEUS_RUNE )
                        || ( QuestHelper.getOneTimeQuestFlag( data.playerId, Q10281_MUTATED_KANEUS_RUNE ) === 0 ) ) {
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tw-rune.htm' ) )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( 42596, -47988, -800 )
                }

                return

            case 73:
                this.tutorialEvent( player, 128 )
                return

            case 79:
                if ( !QuestHelper.haveMemo( player.getObjectId(), Q192_SEVEN_SIGNS_SERIES_OF_DOUBT )
                        || ( QuestHelper.getOneTimeQuestFlag( data.playerId, Q192_SEVEN_SIGNS_SERIES_OF_DOUBT ) === 0 ) ) {
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-ss-79.htm' ) )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( 81655, 54736, -1509 )
                }

                return

            case 81:
                if ( !QuestHelper.haveMemo( player.getObjectId(), Q10292_SEVEN_SIGNS_GIRL_OF_DOUBT )
                        || ( QuestHelper.getOneTimeQuestFlag( data.playerId, Q10292_SEVEN_SIGNS_GIRL_OF_DOUBT ) === 0 ) ) {
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-ss-81.htm' ) )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    PlayerRadarCache.getRadar( data.playerId ).addMarker( 146995, 23755, -1984 )
                }

                return
        }
    }

    async onPlayerTutorialClient( data: PlayerTutorialClientEvent ): Promise<void> {
        this.tutorialEvent( L2World.getPlayer( data.playerId ), data.eventId )
    }

    async onPlayerTutorialCommand( data: PlayerTutorialCommandEvent ): Promise<void> {
        let player: L2PcInstance = L2World.getPlayer( data.playerId )
        let reply: number = _.parseInt( data.command )

        switch ( reply ) {
            case 1:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22g.htm' ) )

            case 2:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22w.htm' ) )

            case 3:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22ap.htm' ) )

            case 4:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22ad.htm' ) )

            case 5:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22bt.htm' ) )

            case 6:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22bh.htm' ) )

            case 7:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22cs.htm' ) )

            case 8:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22cn.htm' ) )

            case 9:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22cw.htm' ) )

            case 10:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22db.htm' ) )

            case 11:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22dp.htm' ) )

            case 12:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22et.htm' ) )

            case 13:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22es.htm' ) )

            case 14:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22fp.htm' ) )

            case 15:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22fs.htm' ) )

            case 16:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22gs.htm' ) )

            case 17:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22ge.htm' ) )

            case 18:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22ko.htm' ) )

            case 19:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22kw.htm' ) )

            case 20:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22ns.htm' ) )

            case 21:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22nb.htm' ) )

            case 22:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22oa.htm' ) )

            case 23:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22op.htm' ) )

            case 24:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22ps.htm' ) )

            case 25:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22pp.htm' ) )

            case 26:
                switch ( player.getClassId() ) {
                    case ClassIdValues.warrior.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22.htm' ) )

                    case ClassIdValues.knight.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22a.htm' ) )

                    case ClassIdValues.rogue.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22b.htm' ) )

                    case ClassIdValues.wizard.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22c.htm' ) )

                    case ClassIdValues.cleric.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22d.htm' ) )

                    case ClassIdValues.elvenKnight.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22e.htm' ) )

                    case ClassIdValues.elvenScout.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22f.htm' ) )

                    case ClassIdValues.elvenWizard.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22g.htm' ) )

                    case ClassIdValues.oracle.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22h.htm' ) )

                    case ClassIdValues.orcRaider.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22i.htm' ) )

                    case ClassIdValues.orcMonk.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22j.htm' ) )

                    case ClassIdValues.orcShaman.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22k.htm' ) )

                    case ClassIdValues.scavenger.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22l.htm' ) )

                    case ClassIdValues.artisan.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22m.htm' ) )

                    case ClassIdValues.palusKnight.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22n.htm' ) )

                    case ClassIdValues.assassin.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22o.htm' ) )

                    case ClassIdValues.darkWizard.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22p.htm' ) )

                    case ClassIdValues.shillienOracle.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22q.htm' ) )

                    default:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22qe.htm' ) )
                }

            case 27:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-29.htm' ) )

            case 28:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-28.htm' ) )

            case 29:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-07a.htm' ) )

            case 30:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-07b.htm' ) )

            case 31:
                switch ( player.getClassId() ) {
                    case ClassIdValues.trooper.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-28a.htm' ) )

                    case ClassIdValues.warder.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-28b.htm' ) )

                }

            case 32:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22qa.htm' ) )

            case 33:
                switch ( player.getClassId() ) {
                    case ClassIdValues.trooper.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22qb.htm' ) )

                    case ClassIdValues.warder.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22qc.htm' ) )

                }

            case 34:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-22qd.htm' ) )

        }
    }

    async onPlayerTutorialLink( data: PlayerTutorialLinkEvent ): Promise<void> {
        if ( _.startsWith( data.command, 'CO' ) ) {
            return
        }

        let pass = _.parseInt( data.command.substring( 15 ) )
        if ( pass < 302 ) {
            pass = -pass
        }

        this.tutorialEvent( L2World.getPlayer( data.playerId ), pass )
    }

    async onPlayerTutorialQuestionMark( data: PlayerTutorialQuestionMarkEvent ): Promise<void> {
        let player: L2PcInstance = L2World.getPlayer( data.playerId )
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )
        let memoFlag = state.getMemoState() & 2147483392

        switch ( data.responseId ) {
            case 1:
                player.sendCopyData( Voice.TUTORIAL_VOICE_007_3500 )

                if ( state.getMemoStateEx( 1 ) < 0 ) {
                    state.setMemoStateEx( 1, -5 )
                }

                switch ( player.getClassId() ) {
                    case ClassIdValues.fighter.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-fighter-007.htm' ) )
                        state.addRadar( -71424, 258336, -3109 )
                        break

                    case ClassIdValues.mage.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-fighter-007.htm' ) )
                        state.addRadar( -91036, 248044, -3568 )
                        break

                    case ClassIdValues.elvenFighter.id:
                    case ClassIdValues.elvenMage.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-fighter-007.htm' ) )
                        state.addRadar( -91036, 248044, -3568 )
                        break

                    case ClassIdValues.darkFighter.id:
                    case ClassIdValues.darkMage.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-fighter-007.htm' ) )
                        state.addRadar( 28384, 11056, -4233 )
                        break

                    case ClassIdValues.orcFighter.id:
                    case ClassIdValues.orcMage.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-fighter-007.htm' ) )
                        state.addRadar( -56736, -113680, -672 )
                        break

                    case ClassIdValues.dwarvenFighter.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-fighter-007.htm' ) )
                        state.addRadar( 108567, -173994, -406 )
                        break

                    case ClassIdValues.maleSoldier.id:
                    case ClassIdValues.femaleSoldier.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-fighter-007.htm' ) )
                        state.addRadar( -125872, 38016, 1251 )
                        break
                }

                return state.setMemoState( memoFlag | 2 )

            case 2:
                switch ( player.getClassId() ) {
                    case ClassIdValues.fighter.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-fighter-008.htm' ) )
                        break
                    case ClassIdValues.mage.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-mage-008.htm' ) )
                        break
                    case ClassIdValues.elvenFighter.id:
                    case ClassIdValues.elvenMage.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-elf-008.htm' ) )
                        break
                    case ClassIdValues.darkFighter.id:
                    case ClassIdValues.darkMage.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-delf-008.htm' ) )
                        break
                    case ClassIdValues.orcFighter.id:
                    case ClassIdValues.orcMage.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-orc-008.htm' ) )
                        break
                    case ClassIdValues.dwarvenFighter.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-dwarven-fighter-008.htm' ) )
                        break
                    case ClassIdValues.maleSoldier.id:
                    case ClassIdValues.femaleSoldier.id:
                        ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-kamael-008.htm' ) )
                        break
                }

                return state.setMemoState( memoFlag | 2 )

            case 3:
                this.enableTutorialEvent( state, memoFlag | 1048576 )
                state.setMemoState( state.getMemoState() | 1048576 )
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-09.htm' ) )

            case 4:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-10.htm' ) )

            case 5:
                switch ( player.getClassId() ) {
                    case ClassIdValues.fighter.id:
                        state.addRadar( -71424, 258336, -3109 )
                        break

                    case ClassIdValues.mage.id:
                        state.addRadar( -91036, 248044, -3568 )
                        break

                    case ClassIdValues.elvenFighter.id:
                    case ClassIdValues.elvenMage.id:
                        state.addRadar( 46112, 41200, -3504 )
                        break

                    case ClassIdValues.darkFighter.id:
                    case ClassIdValues.darkMage.id:
                        state.addRadar( 28384, 11056, -4233 )
                        break

                    case ClassIdValues.orcFighter.id:
                    case ClassIdValues.orcMage.id:
                        state.addRadar( -56736, -113680, -672 )
                        break

                    case ClassIdValues.dwarvenFighter.id:
                        state.addRadar( 108567, -173994, -406 )
                        break

                    case ClassIdValues.maleSoldier.id:
                    case ClassIdValues.femaleSoldier.id:
                        state.addRadar( -125872, 38016, 1251 )
                        break
                }

                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-11.htm' ) )

            case 7:
                state.setMemoState( memoFlag | 5 )
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-15.htm' ) )

            case 8:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-18.htm' ) )

            case 9:
                if ( !player.isMageClass() ) {
                    switch ( player.getRace() ) {
                        case Race.HUMAN:
                        case Race.ELF:
                        case Race.DARK_ELF:
                            return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-fighter-017.htm' ) )

                        case Race.DWARF:
                            return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-fighter-dwarf-017.htm' ) )

                        case Race.ORC:
                            return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-fighter-orc-017.htm' ) )

                        case Race.KAMAEL:
                            return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-kamael-017.htm' ) )

                    }
                }

                return

            case 10:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-19.htm' ) )

            case 11:
                switch ( player.getRace() ) {
                    case Race.HUMAN:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-mage-020.htm' ) )

                    case Race.ELF:
                    case Race.DARK_ELF:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-mage-elf-020.htm' ) )

                    case Race.ORC:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-mage-orc-020.htm' ) )
                }

                return

            case 12:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-15.htm' ) )

            case 13:
                switch ( player.getClassId() ) {
                    case ClassIdValues.fighter.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21.htm' ) )

                    case ClassIdValues.mage.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21a.htm' ) )

                    case ClassIdValues.elvenFighter.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21b.htm' ) )

                    case ClassIdValues.elvenMage.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21c.htm' ) )

                    case ClassIdValues.orcFighter.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21d.htm' ) )

                    case ClassIdValues.orcMage.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21e.htm' ) )

                    case ClassIdValues.dwarvenFighter.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21f.htm' ) )

                    case ClassIdValues.darkFighter.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21g.htm' ) )

                    case ClassIdValues.darkMage.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21h.htm' ) )

                    case ClassIdValues.maleSoldier.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21i.htm' ) )

                    case ClassIdValues.femaleSoldier.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21j.htm' ) )
                }

                return

            case 15:
                if ( player.getRace() !== Race.KAMAEL ) {
                    return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-28.htm' ) )
                }

                if ( player.getClassId() === ClassIdValues.trooper.id ) {
                    return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-28a.htm' ) )
                }

                if ( player.getClassId() === ClassIdValues.warder.id ) {
                    return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-28b.htm' ) )
                }

                return

            case 16:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-30.htm' ) )

            case 17:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-27.htm' ) )

            case 19:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-07.htm' ) )

            case 20:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-14.htm' ) )

            case 21:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-001.htm' ) )

            case 22:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-14.htm' ) )

            case 23:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-24.htm' ) )

            case 24:
                switch ( player.getRace() ) {
                    case Race.HUMAN:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-003a.htm' ) )

                    case Race.ELF:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-003b.htm' ) )

                    case Race.DARK_ELF:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-003c.htm' ) )

                    case Race.ORC:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-003d.htm' ) )

                    case Race.DWARF:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-003e.htm' ) )

                    case Race.KAMAEL:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-003f.htm' ) )
                }

                return

            case 25:
                switch ( player.getClassId() ) {
                    case ClassIdValues.fighter.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-002a.htm' ) )

                    case ClassIdValues.mage.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-002b.htm' ) )

                    case ClassIdValues.elvenFighter.id:
                    case ClassIdValues.elvenMage.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-002c.htm' ) )

                    case ClassIdValues.darkMage.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-002d.htm' ) )

                    case ClassIdValues.darkFighter.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-002e.htm' ) )

                    case ClassIdValues.dwarvenFighter.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-002g.htm' ) )

                    case ClassIdValues.orcFighter.id:
                    case ClassIdValues.orcMage.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-002f.htm' ) )

                    case ClassIdValues.maleSoldier.id:
                    case ClassIdValues.femaleSoldier.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-002i.htm' ) )

                }

                return

            case 26:
                if ( !player.isMageClass() || player.getClassId() === ClassIdValues.orcMage.id ) {
                    return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-004a.htm' ) )
                }

                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-004b.htm' ) )

            case 27:
                switch ( player.getClassId() ) {
                    case ClassIdValues.fighter.id:
                    case ClassIdValues.orcMage.id:
                    case ClassIdValues.orcFighter.id:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-newbie-002h.htm' ) )
                }

                return

            case 28:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-31.htm' ) )

            case 29:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-32.htm' ) )

            case 30:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-33.htm' ) )

            case 31:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-34.htm' ) )

            case 32:
                return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-35.htm' ) )

            case 33:
                switch ( player.getLevel() ) {
                    case 18:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tw-gludio.htm' ) )

                    case 28:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tw-dion.htm' ) )

                    case 38:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tw-heine.htm' ) )

                    case 48:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tw-oren.htm' ) )

                    case 58:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tw-shuttgart.htm' ) )

                    case 68:
                        return ShowHtml.showTutorialHTML( player, this.getPath( 'tw-rune.htm' ) )
                }

                return

            case 34:
                if ( player.getLevel() === 79 ) {
                    return ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-ss-79.htm' ) )
                }

                return
        }
    }

    async onUserConnected( player: L2PcInstance ): Promise<void> {
        let state: QuestState = this.getQuestState( player.getObjectId(), true )

        if ( !state ) {
            return
        }

        if ( !state.isStarted() ) {
            state.startQuest( false )
        }

        if ( player.getLevel() < minimumLevel ) {
            if ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), 255 ) !== 0 ) {
                return
            }

            let memoState = state.getMemoState()
            let memoFlag

            if ( memoState === -1 ) {
                memoState = 0
                memoFlag = 0
            } else {
                memoFlag = memoState & 255
                memoState = memoState & 2147483392
            }

            switch ( memoFlag ) {
                case 0:
                    this.startQuestTimer( tutorialEvent, 10000 )
                    memoState = 2147483392 & ~( 8388608 | 1048576 )
                    state.setMemoState( 1 | memoState )

                    if ( state.getMemoStateEx( 1 ) < 0 ) {
                        state.setMemoStateEx( 1, -2 )
                    }
                    break

                case 1:
                    state.showQuestionMark( 1 )
                    player.sendCopyData( Voice.TUTORIAL_VOICE_006_1000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    break

                case 2:
                    if ( QuestHelper.haveMemo( player.getObjectId(), Q201_TUTORIAL_HUMAN_FIGHTER )
                            || QuestHelper.haveMemo( player.getObjectId(), Q202_TUTORIAL_HUMAN_MAGE )
                            || QuestHelper.haveMemo( player.getObjectId(), Q203_TUTORIAL_ELF )
                            || QuestHelper.haveMemo( player.getObjectId(), Q204_TUTORIAL_DARK_ELF )
                            || QuestHelper.haveMemo( player.getObjectId(), Q205_TUTORIAL_ORC )
                            || QuestHelper.haveMemo( player.getObjectId(), Q206_TUTORIAL_DWARF ) ) {
                        state.showQuestionMark( 6 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    } else {
                        state.showQuestionMark( 2 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    }

                    break

                case 3:
                    let stateMark = 1
                    if ( QuestHelper.getQuestItemsCount( player, BLUE_GEMSTONE ) === 1 ) {
                        stateMark = 3
                    } else if ( state.getMemoStateEx( 1 ) === 2 ) {
                        stateMark = 2
                    }

                    switch ( stateMark ) {
                        case 1:
                            state.showQuestionMark( 3 )
                            break
                        case 2:
                            state.showQuestionMark( 4 )
                            break
                        case 3:
                            state.showQuestionMark( 5 )
                            break
                    }
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    break
                case 4:
                    state.showQuestionMark( 12 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    break
            }
            return this.enableTutorialEvent( state, memoState )
        }

        switch ( player.getLevel() ) {
            case 18:
                if ( QuestHelper.haveMemo( player.getObjectId(), 10276 ) && ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), 10276 ) === 0 ) ) {
                    state.showQuestionMark( 33 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                }
                break

            case 28:
                if ( QuestHelper.haveMemo( player.getObjectId(), 10277 ) && ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), 10277 ) === 0 ) ) {
                    state.showQuestionMark( 33 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                }
                break

            case 38:
                if ( QuestHelper.haveMemo( player.getObjectId(), 10278 ) && ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), 10278 ) === 0 ) ) {
                    state.showQuestionMark( 33 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                }
                break

            case 48:
                if ( QuestHelper.haveMemo( player.getObjectId(), 10279 ) && ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), 10279 ) === 0 ) ) {
                    state.showQuestionMark( 33 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                }
                break

            case 58:
                if ( QuestHelper.haveMemo( player.getObjectId(), 10280 ) && ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), 10280 ) === 0 ) ) {
                    state.showQuestionMark( 33 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                }
                break

            case 68:
                if ( QuestHelper.haveMemo( player.getObjectId(), 10281 ) && ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), 10281 ) === 0 ) ) {
                    state.showQuestionMark( 33 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                }
                break

            case 79:
                if ( QuestHelper.haveMemo( player.getObjectId(), 192 ) && ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), 192 ) === 0 ) ) {
                    state.showQuestionMark( 34 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                }
                break
        }

        let territoryWarId = TerritoryWarManager.getRegisteredTerritoryId( player )
        let territoryWarState = state.getNRMemoStateEx( 728 )
        let isPreparingForWar = TerritoryWarManager.isTWInProgress
        let prepareQuest: Function = ( questId: number ): void => {
            if ( isPreparingForWar ) {
                if ( !state.haveNRMemo( questId ) ) {
                    state.setNRMemo( questId )
                    state.setNRMemoState( questId, 0 )
                    state.setNRFlagJournal( 1 )
                }

                state.showQuestionMark( questId )

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
            }
        }

        if ( territoryWarId > 0 && isPreparingForWar ) {
            if ( !state.haveNRMemo( 728 ) ) {
                state.setNRMemo( 728 )
                state.setNRMemoState( 728, 0 )
                state.setNRMemoStateEx( 728, territoryWarId )
            } else if ( territoryWarId !== territoryWarState ) {
                state.setNRMemoState( 728, 0 )
                state.setNRMemoStateEx( 728, territoryWarId )
            }

            switch ( territoryWarId ) {
                case 81:
                    return prepareQuest( Q717_FOR_THE_SAKE_OF_THE_TERRITORY_GLUDIO )

                case 82:
                    return prepareQuest( Q718_FOR_THE_SAKE_OF_THE_TERRITORY_DION )

                case 83:
                    return prepareQuest( Q719_FOR_THE_SAKE_OF_THE_TERRITORY_GIRAN )

                case 84:
                    return prepareQuest( Q720_FOR_THE_SAKE_OF_THE_TERRITORY_OREN )

                case 85:
                    return prepareQuest( Q721_FOR_THE_SAKE_OF_THE_TERRITORY_ADEN )

                case 86:
                    return prepareQuest( Q722_FOR_THE_SAKE_OF_THE_TERRITORY_INNADRIL )

                case 87:
                    return prepareQuest( Q723_FOR_THE_SAKE_OF_THE_TERRITORY_GODDARD )

                case 88:
                    return prepareQuest( Q724_FOR_THE_SAKE_OF_THE_TERRITORY_RUNE )

                case 89:
                    return prepareQuest( Q725_FOR_THE_SAKE_OF_THE_TERRITORY_SCHUTTGART )
            }

            return
        }

        if ( state.haveNRMemo( Q728_TERRITORY_WAR ) ) {
            if ( ( territoryWarState >= 81 ) && ( territoryWarState <= 89 ) ) {
                let twNRState = state.getNRMemoState( Q728_TERRITORY_WAR )
                let twNRStateForCurrentWar = state.getNRMemoState( 636 + territoryWarState )
                if ( twNRStateForCurrentWar >= 0 ) {
                    state.setNRMemoState( Q728_TERRITORY_WAR, twNRStateForCurrentWar + twNRState )
                    state.removeNRMemo()
                }
            }
        }

        if ( state.haveNRMemo( 739 ) && ( state.getNRMemoState( 739 ) > 0 ) ) {
            state.setNRMemoState( 739, 0 )
        }

        if ( state.haveNRMemo( Q729_PROTECT_THE_TERRITORY_CATAPULT ) ) {
            state.removeNRMemo()
        }

        if ( state.haveNRMemo( Q730_PROTECT_THE_SUPPLIES_SAFE ) ) {
            state.removeNRMemo()
        }

        if ( state.haveNRMemo( Q731_PROTECT_THE_MILITARY_ASSOCIATION_LEADER ) ) {
            state.removeNRMemo()
        }

        if ( state.haveNRMemo( Q732_PROTECT_THE_RELIGIOUS_ASSOCIATION_LEADER ) ) {
            state.removeNRMemo()
        }

        if ( state.haveNRMemo( Q733_PROTECT_THE_ECONOMIC_ASSOCIATION_LEADER ) ) {
            state.removeNRMemo()
        }
    }

    async talkCarl( npcId: number, npcObjectId: number, player: L2PcInstance, state: QuestState ): Promise<void> {
        if ( state.getMemoStateEx( 1 ) < 0 ) {
            if ( player.getClassId() === ClassIdValues.fighter.id && player.getRace() === Race.HUMAN ) {
                state.removeRadar( -71424, 258336, -3109 )
                state.setMemoStateEx( 1, 0 )

                this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )
                this.enableTutorialEvent( state, ( state.getMemoState() & 2147483392 ) | 1048576 )
                return ShowHtml.showPath( player, this.getPath( '30009-001.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30009-006.htm' ) )
        }

        let hasGemstone = QuestHelper.hasQuestItems( player, BLUE_GEMSTONE )
        let hasState = [ 0, 1, 2 ].includes( state.getMemoStateEx( 1 ) )

        if ( hasState && !hasGemstone ) {
            return ShowHtml.showPath( player, this.getPath( '30009-002.htm' ) )
        }

        if ( hasState && hasGemstone ) {
            await QuestHelper.takeSingleItem( player, BLUE_GEMSTONE, -1 )
            await QuestHelper.giveSingleItem( player, RECOMMENDATION_1, 1 )

            state.setMemoStateEx( 1, 3 )
            state.setMemoState( ( state.getMemoState() & 2147483392 ) | 4 )
            this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )

            if ( !player.isMageClass() && !QuestHelper.hasQuestItems( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS ) ) {
                await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )

            }
            if ( player.isMageClass()
                    && !QuestHelper.hasQuestItems( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS )
                    && !QuestHelper.hasQuestItems( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS ) ) {
                if ( player.getClassId() === ClassIdValues.orcMage.id ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                    await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                } else {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
                    await QuestHelper.rewardSingleItem( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS, 100 )
                }
            }

            return ShowHtml.showPath( player, this.getPath( '30009-003.htm' ) )
        }

        if ( state.getMemoStateEx( 1 ) === 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30009-004.htm' ) )
        }

        if ( state.getMemoStateEx( 1 ) > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30009-005.htm' ) )
        }
    }

    async talkDoff( npcId: number, npcObjectId: number, player: L2PcInstance, state: QuestState ): Promise<void> {
        if ( state.getMemoStateEx( 1 ) < 0 ) {
            if ( player.getClassId() === ClassIdValues.mage.id && player.getRace() === Race.HUMAN ) {
                state.removeRadar( -91036, 248044, -3568 )
                state.setMemoStateEx( 1, 0 )

                this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )
                this.enableTutorialEvent( state, ( state.getMemoState() & 2147483392 ) | 1048576 )

                return ShowHtml.showPath( player, this.getPath( '30019-001.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30009-006.htm' ) )
        }

        let hasState: boolean = [ 0, 1, 2 ].includes( state.getMemoStateEx( 1 ) )
        let hasGemstone: boolean = QuestHelper.hasQuestItems( player, BLUE_GEMSTONE )

        if ( hasState && !hasGemstone ) {
            return ShowHtml.showPath( player, this.getPath( '30019-002.htm' ) )
        }

        if ( hasState && hasGemstone ) {
            await QuestHelper.takeSingleItem( player, BLUE_GEMSTONE, -1 )
            await QuestHelper.giveSingleItem( player, RECOMMENDATION_2, 1 )

            this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )
            state.setMemoStateEx( 1, 3 )
            state.setMemoState( ( state.getMemoState() & 2147483392 ) | 4 )

            let hasSoulshots = QuestHelper.hasQuestItems( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS )
            if ( !player.isMageClass() && !hasSoulshots ) {
                await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
            }

            if ( player.isMageClass()
                    && !hasSoulshots
                    && !QuestHelper.hasQuestItems( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS ) ) {

                if ( player.getClassId() === ClassIdValues.orcMage.id ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                    await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                } else {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
                    await QuestHelper.rewardSingleItem( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS, 100 )
                }
            }

            return ShowHtml.showPath( player, this.getPath( '30019-003.htm' ) )
        }

        if ( state.getMemoStateEx( 1 ) === 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30019-004.htm' ) )
        }

        if ( state.getMemoStateEx( 1 ) > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30009-005.htm' ) )
        }
    }

    talkForemanLaferon( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.hasQuestItems( player, LICENSE_OF_MINER ) ) {
            return ShowHtml.showPath( player, this.getPath( '30528-001.htm' ) )
        }

        if ( state.getMemoStateEx( 1 ) > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30528-004.htm' ) )
        }

        return ShowHtml.showPath( player, this.getPath( '30528-003.htm' ) )
    }

    talkGallin( player: L2PcInstance, state: QuestState ) {
        if ( QuestHelper.hasQuestItems( player, RECOMMENDATION_2 ) ) {
            return ShowHtml.showPath( player, this.getPath( '30017-001.htm' ) )
        }

        if ( state.getMemoStateEx( 1 ) > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30017-004.htm' ) )
        }

        return ShowHtml.showPath( player, this.getPath( '30017-003.htm' ) )
    }

    talkGuardianVullkus( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.hasQuestItems( player, VOUCHER_OF_FLAME ) ) {
            return ShowHtml.showPath( player, this.getPath( '30573-001.htm' ) )
        }

        if ( state.getMemoStateEx( 1 ) > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30573-004.htm' ) )
        }

        return ShowHtml.showPath( player, this.getPath( '30573-003.htm' ) )
    }

    async talkHelperKrenisk( npcId: number, npcObjectId: number, player: L2PcInstance, state: QuestState ): Promise<void> {
        if ( state.getMemoStateEx( 1 ) < 0 ) {
            if ( player.getRace() === Race.KAMAEL ) {
                state.removeRadar( -125872, 38016, 1251 )
                state.setMemoStateEx( 1, 0 )
                this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )
                this.enableTutorialEvent( state, ( state.getMemoState() & 2147483392 ) | 1048576 )
                ShowHtml.showPath( player, this.getPath( '32134-001.htm' ) )

                return
            }

            return ShowHtml.showPath( player, this.getPath( '30009-006.htm' ) )
        }

        if ( [ 0, 1, 2 ].includes( state.getMemoStateEx( 1 ) ) && !QuestHelper.hasQuestItems( player, BLUE_GEMSTONE ) ) {
            return ShowHtml.showPath( player, this.getPath( '32134-002.htm' ) )
        }

        if ( [ 0, 1, 2 ].includes( state.getMemoStateEx( 1 ) ) && QuestHelper.hasQuestItems( player, BLUE_GEMSTONE ) ) {
            ShowHtml.showPath( player, this.getPath( '32134-003.htm' ) )
            await QuestHelper.takeSingleItem( player, BLUE_GEMSTONE, -1 )
            state.setMemoStateEx( 1, 3 )
            await QuestHelper.giveSingleItem( player, DIPLOMA, 1 )

            this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )
            state.setMemoState( ( state.getMemoState() & 2147483392 ) | 4 )

            if ( player.getRace() === Race.KAMAEL
                    && ClassId.getLevelByClassId( player.getClassId() ) === 0
                    && !QuestHelper.hasQuestItems( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS ) ) {
                await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
            }

            return
        }

        if ( state.getMemoStateEx( 1 ) === 3 ) {
            return ShowHtml.showPath( player, this.getPath( '32134-004.htm' ) )
        }

        if ( state.getMemoStateEx( 1 ) > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '32134-005.htm' ) )
        }
    }

    talkJundin( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.hasQuestItems( player, BLOOD_OF_MITRAELL ) ) {
            return ShowHtml.showPath( player, this.getPath( '30129-001.htm' ) )
        }

        if ( state.getMemoStateEx( 1 ) > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30129-004.htm' ) )
        }

        return ShowHtml.showPath( player, this.getPath( '30129-003.htm' ) )
    }

    async talkMinerMai( npcId: number, npcObjectId: number, player: L2PcInstance, state: QuestState ): Promise<void> {
        let currentState = state.getMemoStateEx( 1 )
        if ( currentState < 0 ) {
            if ( player.getRace() === Race.DWARF ) {
                state.removeRadar( 108567, -173994, -406 )
                state.setMemoStateEx( 1, 0 )

                this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )
                this.enableTutorialEvent( state, ( state.getMemoState() & 2147483392 ) | 1048576 )

                if ( !player.isMageClass() ) {
                    return ShowHtml.showPath( player, this.getPath( '30009-001.htm' ) )
                }

                return ShowHtml.showPath( player, this.getPath( '30019-001.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30009-006.htm' ) )
        }

        let hasAllStates = [ 0, 1, 2 ].includes( currentState )
        let hasGemstone = QuestHelper.hasQuestItems( player, BLUE_GEMSTONE )

        if ( hasAllStates && !hasGemstone ) {
            return ShowHtml.showPath( player, this.getPath( '30009-002.htm' ) )
        }

        if ( hasAllStates && hasGemstone ) {
            await QuestHelper.takeSingleItem( player, BLUE_GEMSTONE, -1 )
            await QuestHelper.giveSingleItem( player, LICENSE_OF_MINER, 1 )

            this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )
            state.setMemoStateEx( 1, 3 )
            state.setMemoState( ( state.getMemoState() & 2147483392 ) | 4 )

            let hasSoulshots = QuestHelper.hasQuestItems( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS )
            if ( !player.isMageClass() && !hasSoulshots ) {
                player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
            }

            if ( player.isMageClass() && !hasSoulshots && !QuestHelper.hasQuestItems( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS ) ) {
                if ( player.getClassId() === ClassIdValues.orcMage.id ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                    await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                } else {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
                    await QuestHelper.rewardSingleItem( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS, 100 )
                }

                return
            }

            return ShowHtml.showPath( player, this.getPath( '30530-003.htm' ) )
        }

        if ( currentState === 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30530-004.htm' ) )
        }

        if ( currentState > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30009-005.htm' ) )
        }
    }

    async talkMotherTemp( npcId: number, npcObjectId: number, player: L2PcInstance, state: QuestState ): Promise<void> {
        let currentState = state.getMemoStateEx( 1 )
        if ( currentState < 0 ) {
            if ( player.getRace() === Race.ELF ) {
                this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )

                state.removeRadar( 46112, 41200, -3504 )
                state.setMemoStateEx( 1, 0 )

                this.enableTutorialEvent( state, ( state.getMemoState() & 2147483392 ) | 1048576 )

                if ( !player.isMageClass() ) {
                    return ShowHtml.showPath( player, this.getPath( '30009-001.htm' ) )
                }

                return ShowHtml.showPath( player, this.getPath( '30019-001.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30009-006.htm' ) )
        }

        let hasState = [ 0, 1, 2 ].includes( currentState )
        let hasGemstone = QuestHelper.hasQuestItems( player, BLUE_GEMSTONE )
        if ( hasState && !hasGemstone ) {
            if ( !player.isMageClass() ) {
                return ShowHtml.showPath( player, this.getPath( '30009-002.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30019-002.htm' ) )
        }

        if ( hasState && hasGemstone ) {
            await QuestHelper.takeSingleItem( player, BLUE_GEMSTONE, -1 )
            await QuestHelper.rewardSingleItem( player, LEAF_OF_THE_MOTHER_TREE, 1 )

            this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )

            state.setMemoStateEx( 1, 3 )
            state.setMemoState( ( state.getMemoState() & 2147483392 ) | 4 )

            let hasSoulshots = QuestHelper.hasQuestItems( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS )
            if ( !player.isMageClass() && !hasSoulshots ) {
                player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
            }

            if ( player.isMageClass() && !hasSoulshots && !QuestHelper.hasQuestItems( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS ) ) {
                if ( player.getClassId() === ClassIdValues.orcMage.id ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                    await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
                } else {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
                    await QuestHelper.rewardSingleItem( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS, 100 )
                }
            }

            if ( !player.isMageClass() ) {
                return ShowHtml.showPath( player, this.getPath( '30400-003f.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30400-003m.htm' ) )
        }

        if ( currentState === 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30400-004.htm' ) )
        }

        if ( currentState > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30009-005.htm' ) )
        }
    }

    talkNerupa( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.hasQuestItems( player, LEAF_OF_THE_MOTHER_TREE ) ) {
            return ShowHtml.showPath( player, this.getPath( '30370-001.htm' ) )
        }

        if ( state.getMemoStateEx( 1 ) > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30370-004.htm' ) )
        }

        return ShowHtml.showPath( player, this.getPath( '30370-003.htm' ) )
    }

    async talkPoeny( npcId: number, npcObjectId: number, player: L2PcInstance, state: QuestState ): Promise<void> {
        let currentState = state.getMemoStateEx( 1 )
        if ( currentState < 0 ) {
            if ( player.getRace() === Race.DARK_ELF ) {
                state.removeRadar( 28384, 11056, -4233 )
                state.setMemoStateEx( 1, 0 )

                this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )
                this.enableTutorialEvent( state, ( state.getMemoState() & 2147483392 ) | 1048576 )

                if ( !player.isMageClass() ) {
                    return ShowHtml.showPath( player, this.getPath( '30009-001.htm' ) )
                }

                return ShowHtml.showPath( player, this.getPath( '30019-001.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30009-006.htm' ) )
        }

        let hasAllValues = [ 0, 1, 2 ].includes( currentState )
        let hasGemstone = QuestHelper.hasQuestItems( player, BLUE_GEMSTONE )
        if ( hasAllValues && !hasGemstone ) {
            if ( !player.isMageClass() ) {
                return ShowHtml.showPath( player, this.getPath( '30009-002.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30019-002.htm' ) )
        }

        if ( hasAllValues && hasGemstone ) {
            await QuestHelper.takeSingleItem( player, BLUE_GEMSTONE, -1 )
            await QuestHelper.giveSingleItem( player, BLOOD_OF_MITRAELL, 1 )

            this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )

            state.setMemoStateEx( 1, 3 )
            state.setMemoState( ( state.getMemoState() & 2147483392 ) | 4 )

            let hasSoulshots = QuestHelper.hasQuestItems( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS )
            if ( !player.isMageClass() && !hasSoulshots ) {
                player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
            }

            if ( player.isMageClass()
                    && !hasSoulshots
                    && !QuestHelper.hasQuestItems( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS ) ) {

                if ( player.getClassId() === ClassIdValues.orcMage.id ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )
                    await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 100 )
                } else {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_027_1000 )
                    await QuestHelper.rewardSingleItem( player, SPIRITSHOT_NO_GRADE_FOR_BEGINNERS, 100 )
                }
            }

            if ( !player.isMageClass() ) {
                return ShowHtml.showPath( player, this.getPath( '30131-003f.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30131-003m.htm' ) )
        }

        if ( currentState === 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30131-004.htm' ) )
        }

        if ( currentState > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30009-005.htm' ) )
        }
    }

    talkRoien( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.hasQuestItems( player, RECOMMENDATION_1 ) ) {
            return ShowHtml.showPath( player, this.getPath( '30008-001.htm' ) )
        }

        if ( state.getMemoStateEx( 1 ) > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30008-004.htm' ) )
        }

        return ShowHtml.showPath( player, this.getPath( '30008-003.htm' ) )
    }

    async talkShelaPriestess( npcId: number, npcObjectId: number, player: L2PcInstance, state: QuestState ): Promise<void> {
        let currentState = state.getMemoStateEx( 1 )
        if ( currentState < 0 ) {
            if ( player.getRace() === Race.ORC ) {
                state.removeRadar( -56736, -113680, -672 )
                state.setMemoStateEx( 1, 0 )

                this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )
                this.enableTutorialEvent( state, ( state.getMemoState() & 2147483392 ) | 1048576 )
                if ( !player.isMageClass() ) {
                    return ShowHtml.showPath( player, this.getPath( '30009-001.htm' ) )
                }

                return ShowHtml.showPath( player, this.getPath( '30575-001.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30009-006.htm' ) )
        }

        let hasAllStates = [ 0, 1, 2 ].includes( currentState )
        let hasGemstone = QuestHelper.hasQuestItems( player, BLUE_GEMSTONE )
        if ( hasAllStates && !hasGemstone ) {
            if ( !player.isMageClass() ) {
                return ShowHtml.showPath( player, this.getPath( '30009-002.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30575-002.htm' ) )
        }

        if ( hasAllStates && hasGemstone ) {
            await QuestHelper.takeSingleItem( player, BLUE_GEMSTONE, -1 )
            await QuestHelper.giveSingleItem( player, VOUCHER_OF_FLAME, 1 )

            state.setMemoStateEx( 1, 3 )
            state.setMemoState( ( state.getMemoState() & 2147483392 ) | 4 )

            this.startQuestTimer( npcId.toString(), 30000, npcObjectId, player.getObjectId() )

            if ( !QuestHelper.hasQuestItems( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS ) ) {
                await QuestHelper.rewardSingleItem( player, SOULSHOT_NO_GRADE_FOR_BEGINNERS, 200 )
            }

            player.sendCopyData( Voice.TUTORIAL_VOICE_026_1000 )

            if ( !player.isMageClass() ) {
                return ShowHtml.showPath( player, this.getPath( '30575-003f.htm' ) )
            }

            return ShowHtml.showPath( player, this.getPath( '30575-003m.htm' ) )
        }

        if ( currentState === 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30575-004.htm' ) )
        }

        if ( currentState > 3 ) {
            return ShowHtml.showPath( player, this.getPath( '30009-005.htm' ) )
        }
    }

    talkSubelderPerwan( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.hasQuestItems( player, DIPLOMA ) ) {
            return ShowHtml.showPath( player, this.getPath( '32133-001.htm' ) )
        }

        if ( ( state.getMemoStateEx( 1 ) > 3 ) ) {
            return ShowHtml.showPath( player, this.getPath( '32133-004.htm' ) )
        }

        return ShowHtml.showPath( player, this.getPath( '32133-003.htm' ) )
    }

    tutorialEvent( player: L2PcInstance, id: number ): void {
        let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), this.getName() )
        if ( !state ) {
            return
        }

        if ( id === 0 ) {
            player.sendOwnedData( TutorialCloseHtml( player.getObjectId() ) )
            return
        }

        let memoState = state.getMemoState()
        let memoFlag = memoState & 2147483632

        if ( id < 0 ) {
            switch ( Math.abs( id ) ) {
                case 1:
                    player.sendCopyData( Voice.TUTORIAL_VOICE_006_3500 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )
                    player.sendOwnedData( TutorialCloseHtml( player.getObjectId() ) )

                    state.showQuestionMark( 1 )
                    this.startQuestTimer( tutorialEvent, 30000, 0, player.getObjectId() )

                    if ( state.getMemoStateEx( 1 ) < 0 ) {
                        state.setMemoStateEx( 1, -4 )
                    }

                    break

                case 2:
                    player.sendCopyData( Voice.TUTORIAL_VOICE_003_2000 )
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-02.htm' ) )
                    this.enableTutorialEvent( state, memoFlag | 1 )

                    if ( state.getMemoStateEx( 1 ) < 0 ) {
                        state.setMemoStateEx( 1, -5 )
                    }

                    break

                case 3:
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-03.htm' ) )
                    this.enableTutorialEvent( state, memoFlag | 2 )
                    break

                case 4:
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-04.htm' ) )
                    this.enableTutorialEvent( state, memoFlag | 4 )
                    break

                case 5:
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-05.htm' ) )
                    this.enableTutorialEvent( state, memoFlag | 8 )
                    break

                case 6:
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-06.htm' ) )
                    this.enableTutorialEvent( state, memoFlag | 16 )
                    break

                case 7:
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-100.htm' ) )
                    this.enableTutorialEvent( state, memoFlag )
                    break

                case 8:
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-101.htm' ) )
                    this.enableTutorialEvent( state, memoFlag )
                    break

                case 9:
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-102.htm' ) )
                    this.enableTutorialEvent( state, memoFlag )
                    break

                case 10:
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-103.htm' ) )
                    this.enableTutorialEvent( state, memoFlag )
                    break

                case 11:
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-104.htm' ) )
                    this.enableTutorialEvent( state, memoFlag )
                    break

                case 12:
                    player.sendOwnedData( TutorialCloseHtml( player.getObjectId() ) )
                    break
            }

            return
        }

        switch ( id ) {
            case 1:
                if ( player.getLevel() < minimumLevel ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_004_5000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-03.htm' ) )
                    this.enableTutorialEvent( state, memoFlag | 2 )
                }

                break

            case 2:
                if ( player.getLevel() < minimumLevel ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_005_5000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-05.htm' ) )
                    this.enableTutorialEvent( state, memoFlag | 8 )
                }

                break

            case 8:
                if ( player.getLevel() < minimumLevel ) {
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-human-fighter-007.htm' ) )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    switch ( player.getClassId() ) {
                        case ClassIdValues.fighter.id:
                            state.addRadar( -71424, 258336, -3109 )
                            break

                        case ClassIdValues.mage.id:
                            state.addRadar( -91036, 248044, -3568 )
                            break

                        case ClassIdValues.elvenFighter.id:
                        case ClassIdValues.elvenMage.id:
                            state.addRadar( 46112, 41200, -3504 )
                            break

                        case ClassIdValues.darkFighter.id:
                        case ClassIdValues.darkMage.id:
                            state.addRadar( 28384, 11056, -4233 )
                            break

                        case ClassIdValues.orcFighter.id:
                        case ClassIdValues.orcMage.id:
                            state.addRadar( -56736, -113680, -672 )
                            break

                        case ClassIdValues.dwarvenFighter.id:
                            state.addRadar( 108567, -173994, -406 )
                            break

                        case ClassIdValues.maleSoldier.id:
                        case ClassIdValues.femaleSoldier.id:
                            state.addRadar( -125872, 38016, 1251 )
                            break
                    }

                    player.sendCopyData( Voice.TUTORIAL_VOICE_007_3500 )
                    state.setMemoState( memoFlag | 2 )

                    if ( state.getMemoStateEx( 1 ) < 0 ) {
                        state.setMemoStateEx( 1, -5 )
                    }
                }

                break

            case 256:
                if ( player.getLevel() < minimumLevel ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_017_1000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 10 )
                    state.setMemoState( memoState & ~256 )
                    this.enableTutorialEvent( state, ( memoFlag & ~256 ) | 8388608 )
                }

                break

            case 512:
                player.sendCopyData( Voice.TUTORIAL_VOICE_016_1000 )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                state.showQuestionMark( 8 )
                state.setMemoState( memoState & ~512 )
                break

            case 1024:
                state.setMemoState( memoState & ~1024 )

                switch ( player.getClassId() ) {
                    case ClassIdValues.fighter.id:
                        state.addRadar( -83020, 242553, -3718 )
                        break

                    case ClassIdValues.elvenFighter.id:
                        state.addRadar( 45061, 52468, -2796 )
                        break

                    case ClassIdValues.darkFighter.id:
                        state.addRadar( 10447, 14620, -4242 )
                        break

                    case ClassIdValues.orcFighter.id:
                        state.addRadar( -46389, -113905, -21 )
                        break

                    case ClassIdValues.dwarvenFighter.id:
                        state.addRadar( 115271, -182692, -1445 )
                        break

                    case ClassIdValues.maleSoldier.id:
                    case ClassIdValues.femaleSoldier.id:
                        state.addRadar( -118132, 42788, 723 )
                        break
                }

                if ( !player.isMageClass() ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_014_1000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 9 )
                }

                this.enableTutorialEvent( state, memoFlag | 134217728 )
                state.setMemoState( memoState & ~1024 )
                break

            case 134217728:
                player.sendCopyData( Voice.TUTORIAL_VOICE_020_1000 )
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                state.showQuestionMark( 24 )
                state.setMemoState( memoState & ~134217728 )

                this.enableTutorialEvent( state, memoFlag & ~134217728 )
                this.enableTutorialEvent( state, memoFlag | 2048 )
                break

            case 2048:
                if ( player.isMageClass() ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_019_1000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 11 )

                    switch ( player.getClassId() ) {
                        case ClassIdValues.mage.id:
                            state.addRadar( -84981, 244764, -3726 )
                            break

                        case ClassIdValues.elvenMage.id:
                            state.addRadar( 45701, 52459, -2796 )
                            break

                        case ClassIdValues.darkMage.id:
                            state.addRadar( 10344, 14445, -4242 )
                            break

                        case ClassIdValues.orcMage.id:
                            state.addRadar( -46225, -113312, -21 )
                            break
                    }

                    state.setMemoState( memoState & ~2048 )
                }

                this.enableTutorialEvent( state, memoFlag | 268435456 )
                break

            case 268435456:
                if ( player.getClassId() === ClassIdValues.fighter.id ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_021_1000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 25 )
                    state.setMemoState( memoState & ~268435456 )
                }

                this.enableTutorialEvent( state, memoFlag | 536870912 )
                break

            case 536870912:
                switch ( player.getClassId() ) {
                    case ClassIdValues.dwarvenFighter.id:
                    case ClassIdValues.mage.id:
                    case ClassIdValues.elvenFighter.id:
                    case ClassIdValues.elvenMage.id:
                    case ClassIdValues.darkMage.id:
                    case ClassIdValues.darkFighter.id:
                    case ClassIdValues.maleSoldier.id:
                    case ClassIdValues.femaleSoldier.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_021_1000 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                        state.showQuestionMark( 25 )
                        state.setMemoState( memoState & ~536870912 )
                        break

                    default:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_030_1000 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                        state.showQuestionMark( 27 )
                        state.setMemoState( memoState & ~536870912 )
                        break
                }

                this.enableTutorialEvent( state, memoFlag | 1073741824 )
                break

            case 1073741824:
                switch ( player.getClassId() ) {
                    case ClassIdValues.orcFighter.id:
                    case ClassIdValues.orcMage.id:
                        player.sendCopyData( Voice.TUTORIAL_VOICE_021_1000 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                        state.showQuestionMark( 25 )
                        state.setMemoState( memoState & ~1073741824 )
                        break
                }

                this.enableTutorialEvent( state, memoFlag | 67108864 )
                break

            case 67108864:
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                state.showQuestionMark( 17 )
                state.setMemoState( memoState & ~67108864 )

                this.enableTutorialEvent( state, memoFlag | 4096 )
                break

            case 4096:
                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                state.showQuestionMark( 13 )
                state.setMemoState( memoState & ~4096 )

                this.enableTutorialEvent( state, memoFlag | 16777216 )
                break

            case 16777216:
                if ( ClassId.getClassIdByIdentifier( player.getClassId() ).race !== Race.KAMAEL ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_023_1000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 15 )
                    state.setMemoState( memoState & ~16777216 )
                }

                this.enableTutorialEvent( state, memoFlag | 32 )
                break

            case 16384:
                let classIdData = ClassId.getClassIdByIdentifier( player.getClassId() )
                if ( classIdData.race === Race.KAMAEL && classIdData.level === 1 ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_028_1000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 15 )
                    state.setMemoState( memoState & ~16384 )
                }

                this.enableTutorialEvent( state, memoFlag | 64 )
                break

            case 33554432:
                if ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), Q234_FATES_WHISPER ) === 0 ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_024_1000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 16 )
                    state.setMemoState( memoState & ~33554432 )
                }

                this.enableTutorialEvent( state, memoFlag | 32768 )
                break

            case 32768:
                if ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), Q234_FATES_WHISPER ) === 1 ) {
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 29 )
                    state.setMemoState( memoState & ~32768 )
                }

                break

            case 32:
                if ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), Q128_PAILAKA_SONG_OF_ICE_AND_FIRE ) === 0 ) {
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 30 )
                    state.setMemoState( memoState & ~32 )
                }

                this.enableTutorialEvent( state, memoFlag | 16384 )
                break

            case 64:
                if ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), Q129_PAILAKA_DEVILS_LEGACY ) === 0 ) {
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 31 )
                    state.setMemoState( memoState & ~64 )
                }

                this.enableTutorialEvent( state, memoFlag | 128 )
                break

            case 128:
                if ( QuestHelper.getOneTimeQuestFlag( player.getObjectId(), Q144_PAIRAKA_WOUNDED_DRAGON ) === 0 ) {
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 32 )
                    state.setMemoState( memoState & ~128 )
                }

                this.enableTutorialEvent( state, memoFlag | 33554432 )
                break

            case 2097152:
                if ( player.getLevel() < minimumLevel ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_012_1000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 23 )
                    state.setMemoState( memoState & ~2097152 )
                }

                break

            case 1048576:
                if ( player.getLevel() < minimumLevel ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_013_1000 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_TUTORIAL )

                    state.showQuestionMark( 5 )
                    state.setMemoState( memoState & ~1048576 )
                }

                break

            case 8388608:
                if ( player.getLevel() < minimumLevel ) {
                    player.sendCopyData( Voice.TUTORIAL_VOICE_018_1000 )

                    state.setMemoState( memoState & ~8388608 )
                    ShowHtml.showTutorialHTML( player, this.getPath( 'tutorial-21z.htm' ) )
                    this.enableTutorialEvent( state, ( memoFlag & ~8388608 ) )
                }

                break
        }
    }

    onPlayerPicksItem( data: PlayerPickupItemEvent ) : Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.name )
        if ( !state ) {
            return
        }

        if ( data.itemId === BLUE_GEMSTONE && ( ( state.getMemoState() & 1048576 ) !== 0 ) ) {
            this.tutorialEvent( L2World.getPlayer( data.playerId ), 1048576 )
        }

        if ( data.itemId === ItemTypes.Adena && ( ( state.getMemoState() & 2097152 ) !== 0 ) ) {
            this.tutorialEvent( L2World.getPlayer( data.playerId ), 2097152 )
        }

        return
    }

    onPlayerStartsAttack( data: PlayerStartsAttackEvent ) : Promise<void> {
        let player = L2World.getPlayer( data.playerId )
        if ( player && ( player.getCurrentHp() <= ( player.getStat().getMaxHp() * 0.3 ) ) ) {
            this.tutorialEvent( player, 256 )
        }

        return
    }

    onPlayerSits( data: PlayerSitEvent ) : void {
        let player = L2World.getPlayer( data.playerId )
        if ( !player ) {
            return
        }

        this.tutorialEvent( player, 8388608 )
    }

    removeListener( objectId: number, eventType: EventType ) : void {
        ListenerCache.removeObjectListener( objectId, eventType, ( data: EventListenerData ) => {
            return data.owner === this
        } )
    }

    addPlayerAttackEvent( objectId: number ) : void {
        if ( !ListenerCache.hasObjectListener( objectId, EventType.PlayerStartsAttack, this ) ) {
            ListenerCache.addObjectListener( objectId, EventType.PlayerStartsAttack, this, this.onPlayerStartsAttack.bind( this ) )
        }
    }

    addPlayerSitEvent( objectId: number ) : void {
        if ( !ListenerCache.hasObjectListener( objectId, EventType.PlayerSit, this ) ) {
            ListenerCache.addObjectListener( objectId, EventType.PlayerSit, this, this.onPlayerSits.bind( this ) )
        }
    }

    addPlayerItemEvent( objectId: number ) : void {
        if ( !ListenerCache.hasObjectListener( objectId, EventType.PlayerPickupItem, this ) ) {
            ListenerCache.addObjectListener( objectId, EventType.PlayerPickupItem, this, this.onPlayerPicksItem.bind( this ), { targetIds: new Set( [ BLUE_GEMSTONE, ItemTypes.Adena ] ) } )
        }
    }
}