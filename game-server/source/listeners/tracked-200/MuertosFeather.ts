import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

import _ from 'lodash'

const TREVOR = 32166
const minimumLevel = 11
const MUERTOS_FEATHER = 9748
const adenaPerFeather = 45

const monsterRewardChances = {
    22239: 0.500, // Muertos Guard
    22240: 0.533, // Muertos Scout
    22242: 0.566, // Muertos Warrior
    22243: 0.600, // Muertos Captain
    22245: 0.633, // Muertos Lieutenant
    22246: 0.633, // Muertos Commander
}

export class MuertosFeather extends ListenerLogic {
    constructor() {
        super( 'Q00284_MuertosFeather', 'listeners/tracked-200/MuertosFeather.ts' )
        this.questId = 284
        this.questItemIds = [ MUERTOS_FEATHER ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00284_MuertosFeather'
    }

    getQuestStartIds(): Array<number> {
        return [ TREVOR ]
    }

    getTalkIds(): Array<number> {
        return [ TREVOR ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), 1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( MUERTOS_FEATHER, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, MUERTOS_FEATHER, 1, data.isChampion )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32166-03.htm':
                state.startQuest()
                break

            case '32166-06.html':
                break

            case '32166-08.html':
                let amount = QuestHelper.getQuestItemsCount( player, MUERTOS_FEATHER )
                if ( amount === 0 ) {
                    return this.getPath( '32166-07.html' )
                }

                await QuestHelper.giveAdena( player, amount * adenaPerFeather, true )
                await QuestHelper.takeSingleItem( player, MUERTOS_FEATHER, -1 )

                break

            case '32166-09.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32166-01.htm' : '32166-02.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.hasQuestItem( player, MUERTOS_FEATHER ) ? '32166-05.html' : '32166-04.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}