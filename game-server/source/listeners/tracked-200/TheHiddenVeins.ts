import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'

const FILAUR = 30535
const CHICHIRIN = 30539
const CHRYSOLITE_ORE = 1488
const TORN_MAP_FRAGMENT = 1489
const HIDDEN_ORE_MAP = 1490
const minimumLevel = 6
const REQUIRED_TORN_MAP_FRAGMENT = 4

export class TheHiddenVeins extends ListenerLogic {
    constructor() {
        super( 'Q00293_TheHiddenVeins', 'listeners/tracked-200/TheHiddenVeins.ts' )
        this.questId = 293
        this.questItemIds = [
            CHRYSOLITE_ORE,
            TORN_MAP_FRAGMENT,
            HIDDEN_ORE_MAP,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20446,
            20447,
            20448,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00293_TheHiddenVeins'
    }

    getQuestStartIds(): Array<number> {
        return [ FILAUR ]
    }

    getTalkIds(): Array<number> {
        return [ FILAUR, CHICHIRIN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let chance = Math.random()

        if ( chance > 50 ) {
            await QuestHelper.rewardSingleQuestItem( player, CHRYSOLITE_ORE, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            return
        }

        if ( chance < QuestHelper.getAdjustedChance( TORN_MAP_FRAGMENT, 5, data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, TORN_MAP_FRAGMENT, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30535-04.htm':
                state.startQuest()
                break

            case '30535-07.html':
                await state.exitQuest( true, true )
                break

            case '30535-08.html':
                break

            case '30539-03.html':
                let player = L2World.getPlayer( data.playerId )
                if ( QuestHelper.getQuestItemsCount( player, TORN_MAP_FRAGMENT ) >= REQUIRED_TORN_MAP_FRAGMENT ) {
                    await QuestHelper.giveSingleItem( player, HIDDEN_ORE_MAP, 1 )
                    await QuestHelper.takeSingleItem( player, TORN_MAP_FRAGMENT, -1 )

                    break
                }

                return this.getPath( '30539-02.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case FILAUR:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.getRace() !== Race.DWARF ) {
                            return this.getPath( '30535-01.htm' )
                        }

                        return this.getPath( player.getLevel() >= minimumLevel ? '30535-03.htm' : '30535-02.htm' )

                    case QuestStateValues.STARTED:
                        let ores = QuestHelper.getQuestItemsCount( player, CHRYSOLITE_ORE )
                        let maps = QuestHelper.getQuestItemsCount( player, HIDDEN_ORE_MAP )
                        let total = ores + maps

                        if ( total === 0 ) {
                            return this.getPath( '30535-05.html' )
                        }

                        let amount: number = ( ores * 5 ) + ( maps * 500 ) + ( total >= 10 ? 2000 : 0 )

                        await QuestHelper.giveAdena( player, amount, true )
                        await QuestHelper.takeMultipleItems( player, -1, CHRYSOLITE_ORE, HIDDEN_ORE_MAP )
                        await NewbieRewardsHelper.giveNewbieReward( player )

                        if ( ores === 0 ) {
                            return this.getPath( '30535-09.html' )
                        }

                        return this.getPath( maps > 0 ? '30535-10.html' : '30535-06.html' )
                }

                break

            case CHICHIRIN:
                return this.getPath( '30539-01.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}