import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

const STAN = 30200
const DIARY = 15500
const COOKBOOK_PAGE = 15501

const CHEF = 18908
const DIARY_CHANCE = 0.599
const DIARY_MAX_COUNT = 10
const COOKBOOK_PAGE_CHANCE = 0.36
const COOKBOOK_PAGE_MAX_COUNT = 5
const minimumLevel = 82

export class ItSmellsDelicious extends ListenerLogic {
    constructor() {
        super( 'Q00252_ItSmellsDelicious', 'listeners/tracked-200/ItSmellsDelicious.ts' )
        this.questId = 252
        this.questItemIds = [
            DIARY,
            COOKBOOK_PAGE,
        ]
    }

    checkPartyMember( state: QuestState ): boolean {
        return !this.hasAllDiaries( state.getPlayer() )
    }

    getAttackableKillIds(): Array<number> {
        return [
            22786,
            22787,
            22788,
            CHEF,
        ]
    }

    getQuestStartIds(): Array<number> {
        return [ STAN ]
    }

    getTalkIds(): Array<number> {
        return [ STAN ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00252_ItSmellsDelicious'
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( data.npcId === CHEF ) {
            let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

            if ( !state || !state.isCondition( 1 ) ) {
                return
            }

            if ( Math.random() > QuestHelper.getAdjustedChance( COOKBOOK_PAGE, COOKBOOK_PAGE_CHANCE, data.isChampion ) ) {
                return
            }

            await QuestHelper.rewardUpToLimit( player, COOKBOOK_PAGE, 1, COOKBOOK_PAGE_MAX_COUNT, data.isChampion )

            if ( this.hasAllDiaries( player ) ) {
                state.setConditionWithSound( 2, true )
            }

            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( DIARY, DIARY_CHANCE, data.isChampion ) ) {
            return
        }

        let state = this.getRandomPartyMemberState( player, 1, 3, npc )
        if ( !state ) {
            return
        }

        let otherPlayer = state.getPlayer()
        await QuestHelper.rewardUpToLimit( otherPlayer, DIARY, 1, DIARY_MAX_COUNT, data.isChampion )

        if ( this.hasAllPages( otherPlayer ) ) {
            state.setConditionWithSound( 2, true )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30200-04.htm':
                break

            case '30200-05.htm':
                if ( state.isCreated() ) {
                    state.startQuest()
                    break
                }

                return

            case '30200-08.html':
                if ( state.isCondition( 2 ) ) {
                    let player = L2World.getPlayer( data.playerId )

                    await QuestHelper.giveAdena( player, 147656, true )
                    await QuestHelper.addExpAndSp( player, 716238, 78324 )
                    await state.exitQuest( false, true )

                    break
                }

                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '30200-01.htm' : '30200-02.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '30200-06.html' )

                    case 2:
                        if ( this.hasAllDiaries( player ) && this.hasAllPages( player ) ) {
                            return this.getPath( '30200-07.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                return this.getPath( '30200-03.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    hasAllDiaries( player: L2PcInstance ): boolean {
        return QuestHelper.getQuestItemsCount( player, DIARY ) >= DIARY_MAX_COUNT
    }

    hasAllPages( player: L2PcInstance ): boolean {
        return QuestHelper.getQuestItemsCount( player, COOKBOOK_PAGE ) >= COOKBOOK_PAGE_MAX_COUNT
    }
}