import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const PRIEST_PETRON = 30036
const PRIEST_PRIMOS = 30117
const ANDELLIA = 30362
const GAURI_TWINKLEROCK = 30550
const SEER_TANAPI = 30571
const ELDER_CASIAN = 30612
const HERMIT_SANTIAGO = 30648
const ANCESTOR_MARTANKUS = 30649
const PRIEST_OF_THE_EARTH_GERALD = 30650
const WANDERER_DORF = 30651
const URUHA = 30652

const ADENA = 57
const BOOK_OF_SAGE = 2722
const VOUCHER_OF_TRIAL = 2723
const SPIRIT_OF_FLAME = 2724
const ESSENSE_OF_FLAME = 2725
const BOOK_OF_GERALD = 2726
const GREY_BADGE = 2727
const PICTURE_OF_NAHIR = 2728
const HAIR_OF_NAHIR = 2729
const STATUE_OF_EINHASAD = 2730
const BOOK_OF_DARKNESS = 2731
const DEBRIS_OF_WILLOW = 2732
const TAG_OF_RUMOR = 2733

const MARK_OF_PILGRIM = 2721
const DIMENSIONAL_DIAMOND = 7562

const LAVA_SALAMANDER = 27116
const NAHIR = 27117
const BLACK_WILLOW = 27118

const minimumLevel = 35

export class TrialOfThePilgrim extends ListenerLogic {
    constructor() {
        super( 'Q00215_TrialOfThePilgrim', 'listeners/tracked-200/TrialOfThePilgrim.ts' )
        this.questId = 215
        this.questItemIds = [
            BOOK_OF_SAGE,
            VOUCHER_OF_TRIAL,
            SPIRIT_OF_FLAME,
            ESSENSE_OF_FLAME,
            BOOK_OF_GERALD,
            GREY_BADGE,
            PICTURE_OF_NAHIR,
            HAIR_OF_NAHIR,
            STATUE_OF_EINHASAD,
            BOOK_OF_DARKNESS,
            DEBRIS_OF_WILLOW,
            TAG_OF_RUMOR,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ LAVA_SALAMANDER, NAHIR, BLACK_WILLOW ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00215_TrialOfThePilgrim'
    }

    getQuestStartIds(): Array<number> {
        return [ HERMIT_SANTIAGO ]
    }

    getTalkIds(): Array<number> {
        return [
            HERMIT_SANTIAGO,
            PRIEST_PETRON,
            PRIEST_PRIMOS,
            ANDELLIA,
            GAURI_TWINKLEROCK,
            SEER_TANAPI,
            ELDER_CASIAN,
            ANCESTOR_MARTANKUS,
            PRIEST_OF_THE_EARTH_GERALD,
            WANDERER_DORF,
            URUHA,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case LAVA_SALAMANDER:
                if ( state.isMemoState( 3 ) && !QuestHelper.hasQuestItem( player, ESSENSE_OF_FLAME ) ) {
                    state.setMemoState( 4 )
                    state.setConditionWithSound( 4, true )

                    await QuestHelper.giveSingleItem( player, ESSENSE_OF_FLAME, 1 )
                }

                return

            case NAHIR:
                if ( state.isMemoState( 10 ) && !QuestHelper.hasQuestItem( player, HAIR_OF_NAHIR ) ) {
                    state.setMemoState( 11 )
                    state.setConditionWithSound( 11, true )

                    await QuestHelper.giveSingleItem( player, HAIR_OF_NAHIR, 1 )
                }

                return

            case BLACK_WILLOW:
                if ( state.isMemoState( 13 ) && !QuestHelper.hasQuestItem( player, DEBRIS_OF_WILLOW ) ) {
                    state.setMemoState( 14 )
                    state.setConditionWithSound( 14, true )

                    await QuestHelper.giveSingleItem( player, DEBRIS_OF_WILLOW, 1 )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveSingleItem( player, VOUCHER_OF_TRIAL, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( data.playerId, QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 49 )

                        return this.getPath( '30648-04a.htm' )
                    }

                    return this.getPath( '30648-04.htm' )
                }

                return

            case '30648-05.html':
            case '30648-06.html':
            case '30648-07.html':
            case '30648-08.html':
                break

            case '30362-05.html':
                if ( state.isMemoState( 15 ) && QuestHelper.hasQuestItem( player, BOOK_OF_DARKNESS ) ) {
                    await QuestHelper.takeSingleItem( player, BOOK_OF_DARKNESS, 1 )

                    state.setMemoState( 16 )
                    state.setConditionWithSound( 16, true )

                    break
                }

                return

            case '30362-04.html':
                if ( state.isMemoState( 15 ) && QuestHelper.hasQuestItem( player, BOOK_OF_DARKNESS ) ) {
                    state.setMemoState( 16 )
                    state.setConditionWithSound( 16, true )

                    break
                }
                return

            case '30649-04.html':
                if ( state.isMemoState( 4 ) && QuestHelper.hasQuestItem( player, ESSENSE_OF_FLAME ) ) {
                    await QuestHelper.giveSingleItem( player, SPIRIT_OF_FLAME, 1 )
                    await QuestHelper.takeSingleItem( player, ESSENSE_OF_FLAME, 1 )

                    state.setMemoState( 5 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return

            case '30650-02.html':
                if ( state.isMemoState( 6 ) && QuestHelper.hasQuestItem( player, TAG_OF_RUMOR ) ) {
                    if ( player.getAdena() >= 100000 ) {
                        await QuestHelper.giveSingleItem( player, BOOK_OF_GERALD, 1 )
                        await QuestHelper.takeSingleItem( player, ADENA, 100000 )

                        state.setMemoState( 7 )

                        break
                    }

                    return this.getPath( '30650-03.html' )
                }

                return

            case '30650-03.html':
                if ( state.isMemoState( 6 ) && QuestHelper.hasQuestItem( player, TAG_OF_RUMOR ) ) {
                    break
                }

                return

            case '30652-02.html':
                if ( state.isMemoState( 14 ) && QuestHelper.hasQuestItem( player, DEBRIS_OF_WILLOW ) ) {
                    await QuestHelper.giveSingleItem( player, BOOK_OF_DARKNESS, 1 )
                    await QuestHelper.takeSingleItem( player, DEBRIS_OF_WILLOW, 1 )

                    state.setMemoState( 15 )
                    state.setConditionWithSound( 15, true )

                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === HERMIT_SANTIAGO ) {
                    if ( !player.isInCategory( CategoryType.HEAL_GROUP ) ) {
                        return this.getPath( '30648-02.html' )
                    } else if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30648-01.html' )
                    }
                        return this.getPath( '30648-03.htm' )

                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case HERMIT_SANTIAGO:
                        if ( memoState >= 1 ) {
                            if ( !QuestHelper.hasQuestItem( player, BOOK_OF_SAGE ) ) {
                                return this.getPath( '30648-09.html' )
                            }

                            await QuestHelper.giveAdena( player, 229298, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_PILGRIM, 1 )
                            await QuestHelper.addExpAndSp( player, 1258250, 81606 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                            return this.getPath( '30648-10.html' )
                        }

                        break

                    case PRIEST_PETRON:
                        if ( memoState === 9 ) {
                            await QuestHelper.giveSingleItem( player, PICTURE_OF_NAHIR, 1 )

                            state.setMemoState( 10 )
                            state.setConditionWithSound( 10, true )

                            return this.getPath( '30036-01.html' )
                        }

                        if ( memoState === 10 ) {
                            return this.getPath( '30036-02.html' )
                        }

                        if ( memoState === 11 ) {
                            await QuestHelper.takeMultipleItems( player, 1, PICTURE_OF_NAHIR, HAIR_OF_NAHIR )
                            await QuestHelper.giveSingleItem( player, STATUE_OF_EINHASAD, 1 )

                            state.setMemoState( 12 )
                            state.setConditionWithSound( 12, true )

                            return this.getPath( '30036-03.html' )
                        }

                        if ( memoState === 12 && QuestHelper.hasQuestItem( player, STATUE_OF_EINHASAD ) ) {
                            return this.getPath( '30036-04.html' )
                        }

                        break

                    case PRIEST_PRIMOS:
                        if ( memoState === 8 ) {
                            state.setMemoState( 9 )
                            state.setConditionWithSound( 9, true )

                            return this.getPath( '30117-01.html' )
                        }

                        if ( memoState === 9 ) {
                            state.setMemoState( 9 )
                            state.setConditionWithSound( 9, true )

                            return this.getPath( '30117-02.html' )
                        }

                        break

                    case ANDELLIA:
                        if ( memoState === 12 ) {
                            if ( player.getLevel() >= 0 ) {
                                state.setMemoState( 13 )
                                state.setConditionWithSound( 13, true )

                                return this.getPath( '30362-01.html' )
                            }

                            return this.getPath( '30362-01a.html' )
                        }

                        if ( memoState === 13 ) {
                            return this.getPath( '30362-02.html' )
                        }

                        if ( memoState === 14 ) {
                            return this.getPath( '30362-02a.html' )
                        }

                        if ( memoState === 15 ) {
                            if ( QuestHelper.hasQuestItem( player, BOOK_OF_DARKNESS ) ) {
                                return this.getPath( '30362-03.html' )
                            }

                            return this.getPath( '30362-07.html' )
                        }

                        if ( memoState === 16 ) {
                            return this.getPath( '30362-06.html' )
                        }

                        break

                    case GAURI_TWINKLEROCK:
                        if ( memoState === 5 && QuestHelper.hasQuestItem( player, SPIRIT_OF_FLAME ) ) {
                            await QuestHelper.takeSingleItem( player, SPIRIT_OF_FLAME, 1 )
                            await QuestHelper.giveSingleItem( player, TAG_OF_RUMOR, 1 )

                            state.setMemoState( 6 )
                            state.setConditionWithSound( 7, true )

                            return this.getPath( '30550-01.html' )
                        }

                        if ( memoState === 6 ) {
                            return this.getPath( '30550-02.html' )
                        }

                        break

                    case SEER_TANAPI:
                        if ( memoState === 1 && QuestHelper.hasQuestItem( player, VOUCHER_OF_TRIAL ) ) {
                            await QuestHelper.takeSingleItem( player, VOUCHER_OF_TRIAL, 1 )

                            state.setMemoState( 2 )
                            state.setConditionWithSound( 2, true )

                            return this.getPath( '30571-01.html' )
                        }

                        if ( memoState === 2 ) {
                            return this.getPath( '30571-02.html' )
                        }

                        if ( memoState === 5 && QuestHelper.hasQuestItem( player, SPIRIT_OF_FLAME ) ) {
                            state.setConditionWithSound( 6, true )
                            return this.getPath( '30571-03.html' )
                        }

                        break

                    case ELDER_CASIAN:
                        if ( memoState === 16 ) {
                            state.setMemoState( 17 )

                            if ( !QuestHelper.hasQuestItem( player, BOOK_OF_SAGE ) ) {
                                await QuestHelper.giveSingleItem( player, BOOK_OF_SAGE, 1 )
                            }

                            await QuestHelper.takeMultipleItems( player, 1, GREY_BADGE, SPIRIT_OF_FLAME, STATUE_OF_EINHASAD )

                            if ( QuestHelper.hasQuestItem( player, BOOK_OF_DARKNESS ) ) {
                                await QuestHelper.addExpAndSp( player, 5000, 500 )
                                await QuestHelper.takeSingleItem( player, BOOK_OF_DARKNESS, 1 )
                            }

                            return this.getPath( '30612-01.html' )
                        }

                        if ( memoState === 17 ) {
                            state.setConditionWithSound( 17, true )
                            return this.getPath( '30612-02.html' )
                        }

                        break

                    case ANCESTOR_MARTANKUS:
                        if ( memoState === 2 ) {
                            state.setMemoState( 3 )
                            state.setConditionWithSound( 3, true )

                            return this.getPath( '30649-01.html' )
                        }

                        if ( memoState === 3 ) {
                            return this.getPath( '30649-02.html' )
                        }

                        if ( memoState === 4 && QuestHelper.hasQuestItem( player, ESSENSE_OF_FLAME ) ) {
                            return this.getPath( '30649-03.html' )
                        }

                        break

                    case PRIEST_OF_THE_EARTH_GERALD:
                        if ( memoState === 6 ) {
                            if ( QuestHelper.hasQuestItem( player, TAG_OF_RUMOR ) ) {
                                return this.getPath( '30650-01.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItems( player, GREY_BADGE, BOOK_OF_GERALD ) ) {
                            await QuestHelper.giveAdena( player, 100000, true )
                            await QuestHelper.takeSingleItem( player, BOOK_OF_GERALD, 1 )

                            return this.getPath( '30650-04.html' )
                        }

                        break

                    case WANDERER_DORF:
                        switch ( memoState ) {
                            case 6:
                                if ( QuestHelper.hasQuestItem( player, TAG_OF_RUMOR ) ) {
                                    await QuestHelper.giveSingleItem( player, GREY_BADGE, 1 )
                                    await QuestHelper.takeSingleItem( player, TAG_OF_RUMOR, 1 )

                                    state.setMemoState( 8 )
                                    return this.getPath( '30651-01.html' )
                                }

                                break

                            case 7:
                                if ( QuestHelper.hasQuestItem( player, TAG_OF_RUMOR ) ) {
                                    await QuestHelper.giveSingleItem( player, GREY_BADGE, 1 )
                                    await QuestHelper.takeSingleItem( player, TAG_OF_RUMOR, 1 )

                                    state.setMemoState( 8 )
                                    return this.getPath( '30651-02.html' )
                                }

                                break

                            case 8:
                                state.setConditionWithSound( 8, true )
                                return this.getPath( '30651-03.html' )
                        }

                        break

                    case URUHA:
                        switch ( memoState ) {
                            case 14:
                                if ( QuestHelper.hasQuestItem( player, DEBRIS_OF_WILLOW ) ) {
                                    return this.getPath( '30652-01.html' )
                                }

                                break

                            case 15:
                                if ( QuestHelper.hasQuestItem( player, BOOK_OF_DARKNESS ) ) {
                                    return this.getPath( '30652-03.html' )
                                }

                                break
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === HERMIT_SANTIAGO ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}