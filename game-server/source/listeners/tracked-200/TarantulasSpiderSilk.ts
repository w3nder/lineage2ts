import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { NewbieRewardsHelper } from '../helpers/NewbieRewardsHelper'

import _ from 'lodash'

const TRADER_MION = 30519
const DEFENDER_NATHAN = 30548
const TARANTULA_SPIDER_SILK = 1493
const TARANTULA_SPINNERETTE = 1494
const minimumLevel = 15

export class TarantulasSpiderSilk extends ListenerLogic {
    constructor() {
        super( 'Q00296_TarantulasSpiderSilk', 'listeners/tracked-200/TarantulasSpiderSilk.ts' )
        this.questId = 296
        this.questItemIds = [
            TARANTULA_SPIDER_SILK,
            TARANTULA_SPINNERETTE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20394,
            20403,
            20508,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00296_TarantulasSpiderSilk'
    }

    getQuestStartIds(): Array<number> {
        return [ TRADER_MION ]
    }

    getTalkIds(): Array<number> {
        return [ TRADER_MION, DEFENDER_NATHAN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let chance = QuestHelper.getAdjustedChance( TARANTULA_SPINNERETTE, _.random( 100 ), data.isChampion )
        if ( chance > 95 ) {
            await QuestHelper.rewardSingleQuestItem( player, TARANTULA_SPINNERETTE, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            return
        }

        if ( chance > 45 ) {
            await QuestHelper.rewardSingleQuestItem( player, TARANTULA_SPIDER_SILK, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
            return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30519-03.htm':
                state.startQuest()
                break

            case '30519-06.html':
                await state.exitQuest( true, true )
                break

            case '30519-07.html':
                break

            case '30548-03.html':
                let player = L2World.getPlayer( data.playerId )
                let existingTotal = QuestHelper.getQuestItemsCount( player, TARANTULA_SPINNERETTE )

                if ( existingTotal === 0 ) {
                    return this.getPath( '30548-02.html' )
                }

                let amount: number = ( 15 + _.random( 9 ) ) * existingTotal

                await QuestHelper.rewardSingleItem( player, TARANTULA_SPIDER_SILK, amount )
                await QuestHelper.takeSingleItem( player, TARANTULA_SPINNERETTE, -1 )

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== TRADER_MION ) {
                    break
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30519-02.htm' : '30519-01.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case TRADER_MION:
                        let total = QuestHelper.getQuestItemsCount( player, TARANTULA_SPIDER_SILK )
                        if ( total === 0 ) {
                            return this.getPath( '30519-04.html' )
                        }

                        let amount: number = ( total * 30 ) + ( total >= 10 ? 2000 : 0 )

                        await QuestHelper.giveAdena( player, amount, true )
                        await QuestHelper.takeSingleItem( player, TARANTULA_SPIDER_SILK, -1 )
                        await NewbieRewardsHelper.giveNewbieReward( player )

                        return this.getPath( '30519-05.html' )

                    case DEFENDER_NATHAN:
                        return this.getPath( '30548-01.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}