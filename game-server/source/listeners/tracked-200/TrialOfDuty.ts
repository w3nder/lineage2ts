import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2Weapon } from '../../gameService/models/items/L2Weapon'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import _ from 'lodash'
import { createItemDefinition } from '../../gameService/interface/ItemDefinition'

const HANNAVALT = 30109
const DUSTIN = 30116
const SIR_COLLIN_WINDAWOOD = 30311
const SIR_ARON_TANFORD = 30653
const SIR_KIEL_NIGHTHAWK = 30654
const ISAEL_SILVERSHADOW = 30655
const SPIRIT_OF_SIR_TALIANUS = 30656

const LETTER_OF_DUSTIN = 2634
const KNIGHTS_TEAR = 2635
const MIRROR_OF_ORPIC = 2636
const TEAR_OF_CONFESSION = 2637
const REPORT_PIECE = createItemDefinition( 2638, 10 )
const TALIANUSS_REPORT = 2639
const TEAR_OF_LOYALTY = 2640
const MILITAS_ARTICLE = createItemDefinition( 2641, 20 )
const SAINTS_ASHES_URN = 2641
const ATHEBALDTS_SKULL = 2643
const ATHEBALDTS_RIBS = 2644
const ATHEBALDTS_SHIN = 2645
const LETTER_OF_WINDAWOOD = 2646
const OLD_KNIGHTS_SWORD = 3027

const HANGMAN_TREE = 20144
const SKELETON_MARAUDER = 20190
const SKELETON_RAIDER = 20191
const STRAIN = 20200
const GHOUL = 20201
const BREKA_ORC_OVERLORD = 20270
const LETO_LIZARDMAN = 20577
const LETO_LIZARDMAN_ARCHER = 20578
const LETO_LIZARDMAN_SOLDIER = 20579
const LETO_LIZARDMAN_WARRIOR = 20580
const LETO_LIZARDMAN_SHAMAN = 20581
const LETO_LIZARDMAN_OVERLORD = 20582
const SPIRIT_OF_SIR_HEROD = 27119

const MARK_OF_DUTY = 2633
const DIMENSIONAL_DIAMOND = 7562

const minimumLevel = 35
const variableNames = {
    chance: 'c',
}

export class TrialOfDuty extends ListenerLogic {
    constructor() {
        super( 'Q00212_TrialOfDuty', 'listeners/tracked-200/TrialOfDuty.ts' )
        this.questId = 212
        this.questItemIds = [
            LETTER_OF_DUSTIN,
            KNIGHTS_TEAR,
            MIRROR_OF_ORPIC,
            TEAR_OF_CONFESSION,
            REPORT_PIECE.id,
            TALIANUSS_REPORT,
            TEAR_OF_LOYALTY,
            MILITAS_ARTICLE.id,
            SAINTS_ASHES_URN,
            ATHEBALDTS_SKULL,
            ATHEBALDTS_RIBS,
            ATHEBALDTS_SHIN,
            LETTER_OF_WINDAWOOD,
            OLD_KNIGHTS_SWORD,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            HANGMAN_TREE,
            SKELETON_MARAUDER,
            SKELETON_RAIDER,
            STRAIN,
            GHOUL,
            BREKA_ORC_OVERLORD,
            LETO_LIZARDMAN,
            LETO_LIZARDMAN_ARCHER,
            LETO_LIZARDMAN_SOLDIER,
            LETO_LIZARDMAN_WARRIOR,
            LETO_LIZARDMAN_SHAMAN,
            LETO_LIZARDMAN_OVERLORD,
            SPIRIT_OF_SIR_HEROD,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00212_TrialOfDuty'
    }

    getQuestStartIds(): Array<number> {
        return [ HANNAVALT ]
    }

    getTalkIds(): Array<number> {
        return [
            HANNAVALT,
            DUSTIN,
            SIR_COLLIN_WINDAWOOD,
            SIR_ARON_TANFORD,
            SIR_KIEL_NIGHTHAWK,
            ISAEL_SILVERSHADOW,
            SPIRIT_OF_SIR_TALIANUS,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case SKELETON_MARAUDER:
            case SKELETON_RAIDER:
                if ( state.isMemoState( 2 ) ) {
                    let chance : number = state.getVariable( variableNames.chance )

                    if ( _.random( 100 ) < ( chance * 10 ) ) {
                        QuestHelper.addSpawnAtLocation( SPIRIT_OF_SIR_HEROD, npc )
                        state.setVariable( variableNames.chance, 0 )
                    } else {
                        state.setVariable( variableNames.chance, chance + 1 )
                    }
                }


                return

            case SPIRIT_OF_SIR_HEROD:
                if ( state.isMemoState( 2 ) ) {
                    let weapon: L2Weapon = player.getActiveWeaponItem()

                    if ( weapon && weapon.getId() === OLD_KNIGHTS_SWORD ) {
                        await QuestHelper.giveSingleItem( player, KNIGHTS_TEAR, 1 )

                        state.setMemoState( 3 )
                        state.setConditionWithSound( 3, true )
                    }
                }

                return

            case STRAIN:
            case GHOUL:
                if ( state.isMemoState( 5 )
                        && !QuestHelper.hasQuestItem( player, TALIANUSS_REPORT )
                        && await QuestHelper.rewardAndProgressState( player, state, REPORT_PIECE.id, 1, REPORT_PIECE.count, data.isChampion, 6 ) ) {
                    await QuestHelper.takeSingleItem( player, REPORT_PIECE.id, -1 )
                    await QuestHelper.giveSingleItem( player, TALIANUSS_REPORT, 1 )
                }

                return

            case HANGMAN_TREE:
                if ( state.isMemoState( 6 ) ) {
                    let chance : number = state.getVariable( variableNames.chance )

                    if ( _.random( 100 ) < ( ( chance - 3 ) * 33 ) ) {
                        QuestHelper.addSpawnAtLocation( SPIRIT_OF_SIR_TALIANUS, npc )
                        state.setConditionWithSound( 8, true )
                        state.setVariable( variableNames.chance, 0 )
                    } else {
                        state.setVariable( variableNames.chance, chance + 1 )
                    }
                }

                return

            case LETO_LIZARDMAN:
            case LETO_LIZARDMAN_ARCHER:
            case LETO_LIZARDMAN_SOLDIER:
            case LETO_LIZARDMAN_WARRIOR:
            case LETO_LIZARDMAN_SHAMAN:
            case LETO_LIZARDMAN_OVERLORD:
                if ( state.isMemoState( 9 ) ) {
                    await QuestHelper.rewardAndProgressState( player, state, MILITAS_ARTICLE.id, _.random( 1, MILITAS_ARTICLE.count ), MILITAS_ARTICLE.count, data.isChampion, 12 )
                }

                return

            case BREKA_ORC_OVERLORD:
                if ( state.isMemoState( 11 ) ) {
                    if ( !QuestHelper.hasQuestItem( player, ATHEBALDTS_SKULL ) ) {
                        await QuestHelper.giveSingleItem( player, ATHEBALDTS_SKULL, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                        return
                    }

                    if ( !QuestHelper.hasQuestItem( player, ATHEBALDTS_RIBS ) ) {
                        await QuestHelper.giveSingleItem( player, ATHEBALDTS_RIBS, 1 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )

                        return
                    }

                    if ( !QuestHelper.hasQuestItem( player, ATHEBALDTS_SHIN ) ) {
                        await QuestHelper.giveSingleItem( player, ATHEBALDTS_SHIN, 1 )
                        state.setConditionWithSound( 15, true )

                        return
                    }
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'quest_accept':
                if ( state.isCreated() && player.getLevel() >= minimumLevel && player.isInCategory( CategoryType.KNIGHT_GROUP ) ) {
                    state.startQuest()
                    state.setMemoState( 1 )
                    state.setVariable( variableNames.chance, 0 )

                    if ( await this.rewardDimensionalDiamondsTo( player ) ) {
                        return this.getPath( '30109-04a.htm' )
                    }

                    return this.getPath( '30109-04.htm' )
                }

                return

            case '30116-02.html':
            case '30116-03.html':
            case '30116-04.html':
                if ( state.isMemoState( 10 ) && QuestHelper.hasQuestItem( player, TEAR_OF_LOYALTY ) ) {
                    break
                }

                return

            case '30116-05.html':
                if ( state.isMemoState( 10 ) && QuestHelper.hasQuestItem( player, TEAR_OF_LOYALTY ) ) {
                    await QuestHelper.takeSingleItem( player, TEAR_OF_LOYALTY, -1 )

                    state.setMemoState( 11 )
                    state.setConditionWithSound( 14, true )

                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case HANNAVALT:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( !player.isInCategory( CategoryType.KNIGHT_GROUP ) ) {
                            return this.getPath( '30109-02.html' )
                        }

                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '30109-01.html' )
                        }

                        return this.getPath( '30109-03.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getMemoState() ) {
                            case 1:
                                return this.getPath( '30109-04.htm' )

                            case 14:
                                if ( QuestHelper.hasQuestItems( player, LETTER_OF_DUSTIN ) ) {

                                    await QuestHelper.takeSingleItem( player, LETTER_OF_DUSTIN, -1 )
                                    await QuestHelper.addExpAndSp( player, 762576, 49458 )
                                    await QuestHelper.giveAdena( player, 138968, true )
                                    await QuestHelper.giveSingleItem( player, MARK_OF_DUTY, 1 )

                                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                                    await this.rewardDimensionalDiamondsTo( player )

                                    await state.exitQuest( false, true )
                                    return this.getPath( '30109-05.html' )
                                }

                                break
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case SIR_ARON_TANFORD:
                switch ( state.getMemoState() ) {
                    case 1:
                        if ( !QuestHelper.hasQuestItem( player, OLD_KNIGHTS_SWORD ) ) {
                            await QuestHelper.giveSingleItem( player, OLD_KNIGHTS_SWORD, 1 )
                        }

                        state.setMemoState( 2 )
                        state.setConditionWithSound( 2, true )

                        return this.getPath( '30653-01.html' )

                    case 2:
                        if ( QuestHelper.hasQuestItem( player, OLD_KNIGHTS_SWORD ) ) {
                            return this.getPath( '30653-02.html' )
                        }

                        break

                    case 3:
                        if ( QuestHelper.hasQuestItem( player, KNIGHTS_TEAR ) ) {
                            await QuestHelper.takeMultipleItems( player, -1, KNIGHTS_TEAR, OLD_KNIGHTS_SWORD )

                            state.setMemoState( 4 )
                            state.setConditionWithSound( 4, true )

                            return this.getPath( '30653-03.html' )
                        }

                        break

                    case 4:
                        return this.getPath( '30653-04.html' )
                }

                break

            case SIR_KIEL_NIGHTHAWK:
                switch ( state.getMemoState() ) {
                    case 4:
                        state.setMemoState( 5 )
                        state.setConditionWithSound( 5, true )

                        return this.getPath( '30654-01.html' )

                    case 5:
                        if ( QuestHelper.hasQuestItem( player, TALIANUSS_REPORT ) ) {
                            state.setMemoState( 6 )
                            state.setConditionWithSound( 7, true )

                            await QuestHelper.giveSingleItem( player, MIRROR_OF_ORPIC, 1 )

                            return this.getPath( '30654-03.html' )
                        }

                        return this.getPath( '30654-02.html' )

                    case 6:
                        if ( QuestHelper.hasQuestItem( player, MIRROR_OF_ORPIC ) ) {
                            return this.getPath( '30654-04.html' )
                        }

                        break

                    case 7:
                        if ( QuestHelper.hasQuestItem( player, TEAR_OF_CONFESSION ) ) {
                            await QuestHelper.takeSingleItem( player, TEAR_OF_CONFESSION, -1 )

                            state.setMemoState( 8 )
                            state.setConditionWithSound( 10, true )

                            return this.getPath( '30654-05.html' )
                        }

                        break

                    case 8:
                        return this.getPath( '30654-06.html' )
                }

                break

            case SPIRIT_OF_SIR_TALIANUS:
                if ( state.isMemoState( 6 ) && QuestHelper.hasQuestItems( player, MIRROR_OF_ORPIC, TALIANUSS_REPORT ) ) {
                    await QuestHelper.takeMultipleItems( player, -1, MIRROR_OF_ORPIC, TALIANUSS_REPORT )
                    await QuestHelper.giveSingleItem( player, TEAR_OF_CONFESSION, 1 )

                    state.setMemoState( 7 )
                    state.setConditionWithSound( 9, true )

                    let npc = L2World.getObjectById( data.characterId ) as L2Npc
                    await npc.deleteMe()

                    return this.getPath( '30656-01.html' )
                }

                break

            case ISAEL_SILVERSHADOW:
                switch ( state.getMemoState() ) {
                    case 8:
                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '30655-01.html' )
                        }

                        state.setMemoState( 9 )
                        state.setConditionWithSound( 11, true )

                        return this.getPath( '30655-02.html' )

                    case 9:
                        if ( QuestHelper.hasItem( player, MILITAS_ARTICLE ) ) {
                            await QuestHelper.giveSingleItem( player, TEAR_OF_LOYALTY, 1 )
                            await QuestHelper.takeSingleItem( player, MILITAS_ARTICLE.id, MILITAS_ARTICLE.count )

                            state.setMemoState( 10 )
                            state.setConditionWithSound( 13, true )

                            return this.getPath( '30655-04.html' )
                        }

                        return this.getPath( '30655-03.html' )

                    case 10:
                        if ( QuestHelper.hasQuestItem( player, TEAR_OF_LOYALTY ) ) {
                            return this.getPath( '30655-05.html' )
                        }

                        break
                }

                break

            case DUSTIN:
                switch ( state.getMemoState() ) {
                    case 10:
                        if ( QuestHelper.hasQuestItem( player, TEAR_OF_LOYALTY ) ) {
                            return this.getPath( '30116-01.html' )
                        }
                        break

                    case 11:
                        if ( QuestHelper.hasQuestItems( player, ATHEBALDTS_SKULL, ATHEBALDTS_RIBS, ATHEBALDTS_SHIN ) ) {
                            await QuestHelper.takeMultipleItems( player, -1, ATHEBALDTS_SKULL, ATHEBALDTS_RIBS, ATHEBALDTS_SHIN )
                            await QuestHelper.giveSingleItem( player, SAINTS_ASHES_URN, 1 )

                            state.setMemoState( 12 )
                            state.setConditionWithSound( 16, true )

                            return this.getPath( '30116-07.html' )
                        }

                        return this.getPath( '30116-06.html' )

                    case 12:
                        if ( QuestHelper.hasQuestItem( player, SAINTS_ASHES_URN ) ) {
                            return this.getPath( '30116-09.html' )
                        }

                        break

                    case 13:
                        if ( QuestHelper.hasQuestItem( player, LETTER_OF_WINDAWOOD ) ) {
                            await QuestHelper.takeSingleItem( player, LETTER_OF_WINDAWOOD, -1 )
                            await QuestHelper.giveSingleItem( player, LETTER_OF_DUSTIN, 1 )

                            state.setMemoState( 14 )
                            state.setConditionWithSound( 18, true )

                            return this.getPath( '30116-08.html' )
                        }

                        break

                    case 14:
                        if ( QuestHelper.hasQuestItem( player, LETTER_OF_DUSTIN ) ) {
                            return this.getPath( '30116-10.html' )
                        }

                        break
                }

                break

            case SIR_COLLIN_WINDAWOOD:
                switch ( state.getMemoState() ) {
                    case 12:
                        if ( QuestHelper.hasQuestItem( player, SAINTS_ASHES_URN ) ) {
                            await QuestHelper.takeSingleItem( player, SAINTS_ASHES_URN, -1 )
                            await QuestHelper.giveSingleItem( player, LETTER_OF_WINDAWOOD, 1 )

                            state.setMemoState( 13 )
                            state.setConditionWithSound( 17, true )

                            return this.getPath( '30311-01.html' )
                        }

                        break

                    case 13:
                        if ( QuestHelper.hasQuestItem( player, LETTER_OF_WINDAWOOD ) ) {
                            return this.getPath( '30311-02.html' )
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async rewardDimensionalDiamondsTo( player: L2PcInstance ): Promise<boolean> {
        if ( !PlayerVariablesManager.get( player.getObjectId(), QuestVariables.secondClassDiamondReward ) ) {
            let count: number = player.getClassId() === ClassIdValues.knight.id ? 45 : 61
            await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, count )

            PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )
            return true
        }

        return false
    }
}