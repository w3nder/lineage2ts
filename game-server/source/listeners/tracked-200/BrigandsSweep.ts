import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'

import _ from 'lodash'

const SPIRON = 30532
const BALANKI = 30533
const GOBLIN_NECKLACE = 1483
const GOBLIN_PENDANT = 1484
const GOBLIN_LORD_PENDANT = 1485
const SUSPICIOUS_MEMO = 1486
const SUSPICIOUS_CONTRACT = 1487
const minimumLevel = 5

const monsterRewards = {
    20322: GOBLIN_NECKLACE, // Goblin Brigand
    20323: GOBLIN_PENDANT, // Goblin Brigand Leader
    20324: GOBLIN_NECKLACE, // Goblin Brigand Lieutenant
    20327: GOBLIN_NECKLACE, // Goblin Snooper
    20528: GOBLIN_LORD_PENDANT, // Goblin Lord
}

export class BrigandsSweep extends ListenerLogic {
    constructor() {
        super( 'Q00292_BrigandsSweep', 'listeners/tracked-200/BrigandsSweep.ts' )
        this.questId = 292
        this.questItemIds = [
            GOBLIN_NECKLACE,
            GOBLIN_PENDANT,
            GOBLIN_LORD_PENDANT,
            SUSPICIOUS_MEMO,
            SUSPICIOUS_CONTRACT,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewards ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00292_BrigandsSweep'
    }

    getQuestStartIds(): Array<number> {
        return [ SPIRON ]
    }

    getTalkIds(): Array<number> {
        return [ SPIRON, BALANKI ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        let chance = Math.random()
        if ( chance > 0.5 ) {
            await QuestHelper.rewardSingleQuestItem( player, monsterRewards[ data.npcId ], 1, data.isChampion )
            return
        }

        if ( QuestHelper.getAdjustedChance( SUSPICIOUS_MEMO, chance, data.isChampion ) > 0.4
                && !QuestHelper.hasQuestItem( player, SUSPICIOUS_CONTRACT )
                && QuestHelper.getQuestItemsCount( player, SUSPICIOUS_MEMO ) < 3 ) {
            await QuestHelper.rewardSingleQuestItem( player, SUSPICIOUS_MEMO, 1, data.isChampion )

            if ( QuestHelper.getQuestItemsCount( player, SUSPICIOUS_MEMO ) >= 3 ) {
                await QuestHelper.giveSingleItem( player, SUSPICIOUS_CONTRACT, 1 )
                await QuestHelper.takeSingleItem( player, SUSPICIOUS_MEMO, -1 )

                state.setConditionWithSound( 2, true )

                return
            }

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        switch ( data.eventName ) {
            case '30532-03.htm':
                state.startQuest()
                break

            case '30532-06.html':
                await state.exitQuest( true, true )

            case '30532-07.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== SPIRON ) {
                    break
                }

                if ( player.getRace() !== Race.DWARF ) {
                    return this.getPath( '30532-00.htm' )
                }

                return this.getPath( player.getLevel() >= minimumLevel ? '30532-02.htm' : '30532-01.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case SPIRON:
                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, GOBLIN_NECKLACE, GOBLIN_PENDANT, GOBLIN_LORD_PENDANT ) ) {
                            return this.getPath( '30532-04.html' )
                        }

                        let necklaces = QuestHelper.getQuestItemsCount( player, GOBLIN_NECKLACE )
                        let pendants = QuestHelper.getQuestItemsCount( player, GOBLIN_PENDANT )
                        let lordPendants = QuestHelper.getQuestItemsCount( player, GOBLIN_LORD_PENDANT )
                        let sum = necklaces + pendants + lordPendants

                        if ( sum > 0 ) {
                            let amount = ( necklaces * 12 ) + ( pendants * 36 ) + ( lordPendants * 33 ) + ( sum >= 10 ? 1000 : 0 )
                            await QuestHelper.giveAdena( player, amount, true )
                            await QuestHelper.takeMultipleItems( player, -1, GOBLIN_NECKLACE, GOBLIN_PENDANT, GOBLIN_LORD_PENDANT )
                        }

                        if ( sum > 0 && !QuestHelper.hasAtLeastOneQuestItem( player, SUSPICIOUS_MEMO, SUSPICIOUS_CONTRACT ) ) {
                            return this.getPath( '30532-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, SUSPICIOUS_CONTRACT ) ) {
                            await QuestHelper.giveAdena( player, 1120, true )
                            await QuestHelper.takeSingleItem( player, -1, SUSPICIOUS_CONTRACT )

                            return this.getPath( '30532-10.html' )
                        }

                        if ( QuestHelper.getQuestItemsCount( player, SUSPICIOUS_MEMO ) === 1 ) {
                            return this.getPath( '30532-08.html' )
                        }

                        return this.getPath( '30532-09.html' )

                    case BALANKI:
                        if ( QuestHelper.hasQuestItems( player, SUSPICIOUS_CONTRACT ) ) {
                            await QuestHelper.giveAdena( player, 620, true )
                            await QuestHelper.takeSingleItem( player, SUSPICIOUS_CONTRACT, -1 )

                            return this.getPath( '30533-02.html' )
                        }

                        return this.getPath( '30533-01.html' )
                }
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}