import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import _ from 'lodash'

const PREFECT_VOKIAN = 30514
const SAGITTARIUS_HAMIL = 30626
const SIR_ARON_TANFORD = 30653
const GUILD_PRESIDENT_BERNARD = 30702
const MAGISTER_GAUEN = 30717

const WOODEN_ARROW = 17
const CRESCENT_MOON_BOW = 3028
const BERNARDS_INTRODUCTION = 3294
const HAMILS_1ST_LETTER = 3295
const HAMILS_2ND_LETTER = 3296
const HAMILS_3RD_LETTER = 3297
const HUNTERS_1ST_RUNE = 3298
const HUNTERS_2ND_RUNE = 3299
const TALISMAN_OF_KADESH = 3300
const TALISMAN_OF_SNAKE = 3301
const MITHRIL_CLIP = 3302
const STAKATO_CHITIN = 3303
const REINFORCED_BOWSTRING = 3304
const MANASHENS_HORN = 3305
const BLOOD_OF_LIZARDMAN = 3306

const MARK_OF_SAGITTARIUS = 3293
const DIMENSIONAL_DIAMOND = 7562

const ANT = 20079
const ANT_CAPTAIN = 20080
const ANT_OVERSEER = 20081
const ANT_RECRUIT = 20082
const ANT_PATROL = 20084
const ANT_GUARD = 20086
const NOBLE_ANT = 20089
const NOBLE_ANT_LEADER = 20090
const MARSH_STAKATO_WORKER = 20230
const MARSH_STAKATO_SOLDIER = 20232
const MARSH_SPIDER = 20233
const MARSH_STAKATO_DRONE = 20234
const BREKA_ORC_SHAMAN = 20269
const BREKA_ORC_OVERLORD = 20270
const ROAD_SCAVENGER = 20551
const MANASHEN_GARGOYLE = 20563
const LETO_LIZARDMAN = 20577
const LETO_LIZARDMAN_ARCHER = 20578
const LETO_LIZARDMAN_SOLDIER = 20579
const LETO_LIZARDMAN_WARRIOR = 20580
const LETO_LIZARDMAN_SHAMAN = 20581
const LETO_LIZARDMAN_OVERLORD = 20582

const SERPENT_DEMON_KADESH = 27090

const minimumLevel = 39

export class TestOfSagittarius extends ListenerLogic {
    constructor() {
        super( 'Q00224_TestOfSagittarius', 'listeners/tracked-200/TestOfSagittarius.ts' )
        this.questId = 224
        this.questItemIds = [
            CRESCENT_MOON_BOW,
            BERNARDS_INTRODUCTION,
            HAMILS_1ST_LETTER,
            HAMILS_2ND_LETTER,
            HAMILS_3RD_LETTER,
            HUNTERS_1ST_RUNE,
            HUNTERS_2ND_RUNE,
            TALISMAN_OF_KADESH,
            TALISMAN_OF_SNAKE,
            MITHRIL_CLIP,
            STAKATO_CHITIN,
            REINFORCED_BOWSTRING,
            MANASHENS_HORN,
            BLOOD_OF_LIZARDMAN,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ANT,
            ANT_CAPTAIN,
            ANT_OVERSEER,
            ANT_RECRUIT,
            ANT_PATROL,
            ANT_GUARD,
            NOBLE_ANT,
            NOBLE_ANT_LEADER,
            MARSH_STAKATO_WORKER,
            MARSH_STAKATO_SOLDIER,
            MARSH_SPIDER,
            MARSH_STAKATO_DRONE,
            BREKA_ORC_SHAMAN,
            BREKA_ORC_OVERLORD,
            ROAD_SCAVENGER,
            MANASHEN_GARGOYLE,
            LETO_LIZARDMAN,
            LETO_LIZARDMAN_ARCHER,
            LETO_LIZARDMAN_SOLDIER,
            LETO_LIZARDMAN_WARRIOR,
            LETO_LIZARDMAN_SHAMAN,
            LETO_LIZARDMAN_OVERLORD,
            SERPENT_DEMON_KADESH,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00224_TestOfSagittarius'
    }

    getQuestStartIds(): Array<number> {
        return [ GUILD_PRESIDENT_BERNARD ]
    }

    getTalkIds(): Array<number> {
        return [ GUILD_PRESIDENT_BERNARD, PREFECT_VOKIAN, SAGITTARIUS_HAMIL, SIR_ARON_TANFORD, MAGISTER_GAUEN ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case ANT:
            case ANT_CAPTAIN:
            case ANT_OVERSEER:
            case ANT_RECRUIT:
            case ANT_PATROL:
            case ANT_GUARD:
            case NOBLE_ANT:
            case NOBLE_ANT_LEADER:
                if ( state.isMemoState( 3 ) && QuestHelper.getQuestItemsCount( player, HUNTERS_1ST_RUNE ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, HUNTERS_1ST_RUNE, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, HUNTERS_1ST_RUNE ) >= 10 ) {
                        state.setMemoState( 4 )
                        state.setConditionWithSound( 4, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case MARSH_STAKATO_WORKER:
            case MARSH_STAKATO_SOLDIER:
            case MARSH_STAKATO_DRONE:
                if ( state.isMemoState( 10 ) && !QuestHelper.hasQuestItem( player, STAKATO_CHITIN ) ) {
                    await QuestHelper.giveSingleItem( player, STAKATO_CHITIN, 1 )
                    if ( QuestHelper.hasQuestItems( player, MITHRIL_CLIP, REINFORCED_BOWSTRING, MANASHENS_HORN ) ) {
                        state.setMemoState( 11 )
                        state.setConditionWithSound( 11, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case MARSH_SPIDER:
                if ( state.isMemoState( 10 ) && !QuestHelper.hasQuestItem( player, REINFORCED_BOWSTRING ) ) {
                    await QuestHelper.giveSingleItem( player, REINFORCED_BOWSTRING, 1 )

                    if ( QuestHelper.hasQuestItems( player, MITHRIL_CLIP, MANASHENS_HORN, STAKATO_CHITIN ) ) {
                        await QuestHelper.giveSingleItem( player, REINFORCED_BOWSTRING, 1 )

                        state.setMemoState( 11 )
                        state.setConditionWithSound( 11, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case BREKA_ORC_SHAMAN:
            case BREKA_ORC_OVERLORD:
                if ( state.isMemoState( 6 ) && ( QuestHelper.getQuestItemsCount( player, HUNTERS_2ND_RUNE ) < 10 ) ) {
                    await QuestHelper.rewardSingleQuestItem( player, HUNTERS_2ND_RUNE, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, HUNTERS_2ND_RUNE ) >= 10 ) {
                        await QuestHelper.giveSingleItem( player, TALISMAN_OF_SNAKE, 1 )

                        state.setMemoState( 7 )
                        state.setConditionWithSound( 7, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case ROAD_SCAVENGER:
                if ( state.isMemoState( 10 ) && !QuestHelper.hasQuestItem( player, MITHRIL_CLIP ) ) {
                    if ( QuestHelper.hasQuestItems( player, REINFORCED_BOWSTRING, MANASHENS_HORN, STAKATO_CHITIN ) ) {
                        await QuestHelper.giveSingleItem( player, MITHRIL_CLIP, 1 )

                        state.setMemoState( 11 )
                        state.setConditionWithSound( 11, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case MANASHEN_GARGOYLE:
                if ( state.isMemoState( 10 ) && !QuestHelper.hasQuestItem( player, MANASHENS_HORN ) ) {
                    await QuestHelper.giveSingleItem( player, MANASHENS_HORN, 1 )

                    if ( QuestHelper.hasQuestItems( player, MITHRIL_CLIP, REINFORCED_BOWSTRING, STAKATO_CHITIN ) ) {

                        state.setMemoState( 11 )
                        state.setConditionWithSound( 11, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case LETO_LIZARDMAN:
            case LETO_LIZARDMAN_ARCHER:
            case LETO_LIZARDMAN_SOLDIER:
            case LETO_LIZARDMAN_WARRIOR:
            case LETO_LIZARDMAN_SHAMAN:
            case LETO_LIZARDMAN_OVERLORD:
                if ( state.isMemoState( 13 ) && QuestHelper.hasQuestItem( player, BLOOD_OF_LIZARDMAN ) ) {
                    if ( ( ( QuestHelper.getQuestItemsCount( player, BLOOD_OF_LIZARDMAN ) - 10 ) * 5 ) > _.random( 100 ) ) {
                        await QuestHelper.takeSingleItem( player, BLOOD_OF_LIZARDMAN, -1 )

                        QuestHelper.addSpawnAtLocation( SERPENT_DEMON_KADESH, npc, true, 300000 )
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_BEFORE_BATTLE )

                        return
                    }

                    await QuestHelper.rewardSingleItem( player, BLOOD_OF_LIZARDMAN, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case SERPENT_DEMON_KADESH: {
                if ( state.isMemoState( 13 ) && !QuestHelper.hasQuestItem( player, TALISMAN_OF_KADESH ) ) {
                    if ( npc.getKillingBlowWeapon() === CRESCENT_MOON_BOW ) {
                        await QuestHelper.giveSingleItem( player, TALISMAN_OF_KADESH, 1 )

                        state.setMemoState( 14 )
                        state.setConditionWithSound( 14, true )

                        return
                    }

                    QuestHelper.addSpawnAtLocation( SERPENT_DEMON_KADESH, npc, true, 300000 )
                }

                return
            }
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveSingleItem( player, BERNARDS_INTRODUCTION, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 96 )

                        return this.getPath( '30702-04a.htm' )
                    }

                    return this.getPath( '30702-04.htm' )
                }

                return

            case '30514-02.html':
                if ( QuestHelper.hasQuestItem( player, HAMILS_2ND_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, HAMILS_2ND_LETTER, 1 )
                    state.setMemoState( 6 )
                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '30626-02.html':
            case '30626-06.html':
                break

            case '30626-03.html':
                if ( QuestHelper.hasQuestItem( player, BERNARDS_INTRODUCTION ) ) {
                    await QuestHelper.takeSingleItem( player, BERNARDS_INTRODUCTION, 1 )
                    await QuestHelper.giveSingleItem( player, HAMILS_1ST_LETTER, 1 )

                    state.setMemoState( 2 )
                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '30626-07.html':
                if ( QuestHelper.getQuestItemsCount( player, HUNTERS_1ST_RUNE ) >= 10 ) {
                    await QuestHelper.takeSingleItem( player, HUNTERS_1ST_RUNE, -1 )
                    await QuestHelper.giveSingleItem( player, HAMILS_2ND_LETTER, 1 )

                    state.setMemoState( 5 )
                    state.setConditionWithSound( 5, true )
                    break
                }
                return

            case '30653-02.html':
                if ( QuestHelper.hasQuestItem( player, HAMILS_1ST_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, HAMILS_1ST_LETTER, 1 )

                    state.setMemoState( 3 )
                    state.setConditionWithSound( 3, true )
                    break
                }

                return

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === GUILD_PRESIDENT_BERNARD ) {
                    if ( ![ ClassIdValues.rogue.id, ClassIdValues.elvenScout.id, ClassIdValues.assassin.id ].includes( player.getClassId() ) ) {
                        return this.getPath( '30702-02.html' )
                    }

                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30702-01.html' )
                    }

                    return this.getPath( '30702-03.htm' )
                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()

                switch ( data.characterNpcId ) {
                    case GUILD_PRESIDENT_BERNARD:
                        if ( QuestHelper.hasQuestItem( player, BERNARDS_INTRODUCTION ) ) {
                            return this.getPath( '30702-05.html' )
                        }

                        break

                    case PREFECT_VOKIAN:
                        switch ( memoState ) {
                            case 5:
                                if ( QuestHelper.hasQuestItem( player, HAMILS_2ND_LETTER ) ) {
                                    return this.getPath( '30514-01.html' )
                                }

                                break

                            case 6:
                                return this.getPath( '30514-03.html' )

                            case 7:
                                if ( QuestHelper.hasQuestItem( player, TALISMAN_OF_SNAKE ) ) {
                                    await QuestHelper.takeSingleItem( player, TALISMAN_OF_SNAKE, 1 )

                                    state.setMemoState( 8 )
                                    state.setConditionWithSound( 8, true )

                                    return this.getPath( '30514-04.html' )
                                }

                                break

                            case 8:
                                return this.getPath( '30514-05.html' )
                        }

                        break

                    case SAGITTARIUS_HAMIL:
                        switch ( memoState ) {
                            case 1:
                                if ( QuestHelper.hasQuestItem( player, BERNARDS_INTRODUCTION ) ) {
                                    return this.getPath( '30626-01.html' )
                                }

                                break

                            case 2:
                                if ( QuestHelper.hasQuestItem( player, HAMILS_1ST_LETTER ) ) {
                                    return this.getPath( '30626-04.html' )
                                }

                                break

                            case 4:
                                if ( QuestHelper.getQuestItemsCount( player, HUNTERS_1ST_RUNE ) >= 10 ) {
                                    return this.getPath( '30626-05.html' )
                                }

                                break

                            case 5:
                                if ( QuestHelper.hasQuestItem( player, HAMILS_2ND_LETTER ) ) {
                                    return this.getPath( '30626-08.html' )
                                }

                                break

                            case 8:
                                await QuestHelper.giveSingleItem( player, HAMILS_3RD_LETTER, 1 )
                                await QuestHelper.takeSingleItem( player, HUNTERS_2ND_RUNE, -1 )

                                state.setMemoState( 9 )
                                state.setConditionWithSound( 9, true )

                                return this.getPath( '30626-09.html' )

                            case 9:
                                if ( QuestHelper.hasQuestItem( player, HAMILS_3RD_LETTER ) ) {
                                    return this.getPath( '30626-10.html' )
                                }

                                break

                            case 12:
                                if ( QuestHelper.hasQuestItem( player, CRESCENT_MOON_BOW ) ) {
                                    state.setConditionWithSound( 13, true )
                                    state.setMemoState( 13 )

                                    return this.getPath( '30626-11.html' )
                                }

                                break

                            case 13:
                                return this.getPath( '30626-12.html' )

                            case 14:
                                if ( QuestHelper.hasQuestItem( player, TALISMAN_OF_KADESH ) ) {
                                    await QuestHelper.giveAdena( player, 161806, true )
                                    await QuestHelper.giveSingleItem( player, MARK_OF_SAGITTARIUS, 1 )
                                    await QuestHelper.addExpAndSp( player, 894888, 61408 )
                                    await state.exitQuest( false, true )

                                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                                    return this.getPath( '30626-13.html' )
                                }

                                break
                        }

                        break

                    case SIR_ARON_TANFORD:
                        switch ( memoState ) {
                            case 2:
                                if ( QuestHelper.hasQuestItem( player, HAMILS_1ST_LETTER ) ) {
                                    return this.getPath( '30653-01.html' )
                                }

                                break

                            case 3:
                                return this.getPath( '30653-03.html' )
                        }

                        break

                    case MAGISTER_GAUEN:
                        switch ( memoState ) {
                            case 9:
                                if ( QuestHelper.hasQuestItem( player, HAMILS_3RD_LETTER ) ) {
                                    await QuestHelper.takeSingleItem( player, HAMILS_3RD_LETTER, 1 )

                                    state.setMemoState( 10 )
                                    state.setConditionWithSound( 10, true )

                                    return this.getPath( '30717-01.html' )
                                }

                                break

                            case 10:
                                return this.getPath( '30717-03.html' )

                            case 11:
                                if ( QuestHelper.hasQuestItems( player, STAKATO_CHITIN, MITHRIL_CLIP, REINFORCED_BOWSTRING, MANASHENS_HORN ) ) {
                                    await QuestHelper.giveSingleItem( player, WOODEN_ARROW, 10 )
                                    await QuestHelper.giveSingleItem( player, CRESCENT_MOON_BOW, 1 )
                                    await QuestHelper.takeMultipleItems( player, 1, MITHRIL_CLIP, STAKATO_CHITIN, REINFORCED_BOWSTRING, MANASHENS_HORN )

                                    state.setMemoState( 12 )
                                    state.setConditionWithSound( 12, true )

                                    return this.getPath( '30717-02.html' )
                                }

                                break

                            case 12:
                                return this.getPath( '30717-04.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === GUILD_PRESIDENT_BERNARD ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}