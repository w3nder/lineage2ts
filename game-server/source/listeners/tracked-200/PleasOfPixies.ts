import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import _ from 'lodash'
import aigle from 'aigle'

const PIXY_MURIKA = 31852
const PREDATORS_FANG = 1334
const itemLimit = 100
const minimumLevel = 3

type MonsterReward = [ number, number ] // chance, item amount
const monsterRewardItems: { [ npcId: number ]: Array<MonsterReward> } = {
    20537: [ [ 10, 2 ] ], // Elder Red Keltir
    20525: [ [ 5, 2 ], [ 10, 3 ] ], // Gray Wolf
    20534: [ [ 6, 1 ] ], // Red Keltir
    20530: [ [ 8, 1 ] ], // Young Red Keltir
}

type QuestReward = [ number, number ] // itemId, amount
const questRewards: Array<Array<QuestReward>> = [
    [ [ 1337, 1 ], [ 3032, 1 ] ], // Emerald, Recipe: Spiritshot D
    [ [ 2176, 1 ], [ 1338, 1 ] ], // Recipe: Leather Boots, Blue Onyx
    [ [ 1339, 1 ], [ 1061, 1 ] ], // Onyx, Greater Healing Potion
    [ [ 1336, 1 ], [ 1060, 1 ] ], // Glass Shard, Lesser Healing Potion
]

export class PleasOfPixies extends ListenerLogic {
    constructor() {
        super( 'Q00266_PleasOfPixies', 'listeners/tracked-200/PleasOfPixies.ts' )
        this.questId = 266
        this.questItemIds = [ PREDATORS_FANG ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardItems ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00266_PleasOfPixies'
    }

    getQuestStartIds(): Array<number> {
        return [ PIXY_MURIKA ]
    }

    getTalkIds(): Array<number> {
        return [ PIXY_MURIKA ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || !state.isCondition( 1 ) ) {
            return
        }

        let chanceValue = _.random( 10 )
        let reward: MonsterReward = _.find( monsterRewardItems[ data.npcId ], ( item: MonsterReward ): boolean => {
            return chanceValue < QuestHelper.getAdjustedChance( PREDATORS_FANG, item[ 0 ], data.isChampion )
        } )

        if ( !reward ) {
            return
        }

        let [ , amount ] = reward
        let player = L2World.getPlayer( data.playerId )
        await QuestHelper.rewardSingleQuestItem( player, PREDATORS_FANG, amount, data.isChampion )

        if ( QuestHelper.getQuestItemsCount( player, PREDATORS_FANG ) >= itemLimit ) {
            state.setConditionWithSound( 2, true )
            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state || data.eventName !== '31852-04.htm' ) {
            return
        }

        state.startQuest()
        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( player.getRace() !== Race.ELF ) {
                    return this.getPath( '31852-01.htm' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '31852-02.htm' )
                }

                return this.getPath( '31852-03.htm' )

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '31852-05.html' )

                    case 2:
                        if ( QuestHelper.getQuestItemsCount( player, PREDATORS_FANG ) < itemLimit ) {
                            return this.getPath( '31852-05.html' )
                        }

                        let index = this.getRewardIndex()
                        await aigle.resolve( questRewards[ index ] ).each( async ( reward: QuestReward ) => {
                            let [ itemId, amount ] = reward
                            await QuestHelper.rewardSingleItem( player, itemId, amount )
                        } )

                        if ( index === 0 ) {
                            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_JACKPOT )
                        }

                        await state.exitQuest( true, true )
                        return this.getPath( '31852-06.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    getRewardIndex(): number {
        let chance = _.random( 100 )
        if ( chance < 2 ) {
            return 0
        }

        if ( chance < 20 ) {
            return 1
        }

        if ( chance < 45 ) {
            return 2
        }

        return 3
    }
}