import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { AttackableAttackedEvent, AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2Summon } from '../../gameService/models/actor/L2Summon'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { InstanceType } from '../../gameService/enums/InstanceType'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcSay } from '../../gameService/packets/send/builder/NpcSay'
import { L2Character } from '../../gameService/models/actor/L2Character'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const GROCER_LARA = 30063
const HIGH_SUMMONER_GALATEA = 30634
const SUMMONER_ALMORS = 30635
const SUMMONER_CAMONIELL = 30636
const SUMMONER_BELTHUS = 30637
const SUMMONER_BASILLA = 30638
const SUMMONER_CELESTIEL = 30639
const SUMMONER_BRYNTHEA = 30640

const LETOLIZARDMAN_AMULET = 3337
const SAC_OF_REDSPORES = 3338
const KARULBUGBEAR_TOTEM = 3339
const SHARDS_OF_MANASHEN = 3340
const BREKAORC_TOTEM = 3341
const CRIMSON_BLOODSTONE = 3342
const TALONS_OF_TYRANT = 3343
const WINGS_OF_DRONEANT = 3344
const TUSK_OF_WINDSUS = 3345
const FANGS_OF_WYRM = 3346
const LARAS_1ST_LIST = 3347
const LARAS_2ND_LIST = 3348
const LARAS_3RD_LIST = 3349
const LARAS_4TH_LIST = 3350
const LARAS_5TH_LIST = 3351
const GALATEAS_LETTER = 3352
const BEGINNERS_ARCANA = 3353
const ALMORS_ARCANA = 3354
const CAMONIELL_ARCANA = 3355
const BELTHUS_ARCANA = 3356
const BASILLIA_ARCANA = 3357
const CELESTIEL_ARCANA = 3358
const BRYNTHEA_ARCANA = 3359
const CRYSTAL_OF_STARTING_1ST = 3360
const CRYSTAL_OF_INPROGRESS_1ST = 3361
const CRYSTAL_OF_FOUL_1ST = 3362
const CRYSTAL_OF_DEFEAT_1ST = 3363
const CRYSTAL_OF_VICTORY_1ST = 3364
const CRYSTAL_OF_STARTING_2ND = 3365
const CRYSTAL_OF_INPROGRESS_2ND = 3366
const CRYSTAL_OF_FOUL_2ND = 3367
const CRYSTAL_OF_DEFEAT_2ND = 3368
const CRYSTAL_OF_VICTORY_2ND = 3369
const CRYSTAL_OF_STARTING_3RD = 3370
const CRYSTAL_OF_INPROGRESS_3RD = 3371
const CRYSTAL_OF_FOUL_3RD = 3372
const CRYSTAL_OF_DEFEAT_3RD = 3373
const CRYSTAL_OF_VICTORY_3RD = 3374
const CRYSTAL_OF_STARTING_4TH = 3375
const CRYSTAL_OF_INPROGRESS_4TH = 3376
const CRYSTAL_OF_FOUL_4TH = 3377
const CRYSTAL_OF_DEFEAT_4TH = 3378
const CRYSTAL_OF_VICTORY_4TH = 3379
const CRYSTAL_OF_STARTING_5TH = 3380
const CRYSTAL_OF_INPROGRESS_5TH = 3381
const CRYSTAL_OF_FOUL_5TH = 3382
const CRYSTAL_OF_DEFEAT_5TH = 3383
const CRYSTAL_OF_VICTORY_5TH = 3384
const CRYSTAL_OF_STARTING_6TH = 3385
const CRYSTAL_OF_INPROGRESS_6TH = 3386
const CRYSTAL_OF_FOUL_6TH = 3387
const CRYSTAL_OF_DEFEAT_6TH = 3388
const CRYSTAL_OF_VICTORY_6TH = 3389

const MARK_OF_SUMMONER = 3336
const DIMENSIONAL_DIAMOND = 7562

const NOBLE_ANT = 20089
const NOBLE_ANT_LEADER = 20090
const WYRM = 20176
const TYRANT = 20192
const TYRANT_KINGPIN = 20193
const BREKA_ORC = 20267
const BREKA_ORC_ARCHER = 20268
const BREKA_ORC_SHAMAN = 20269
const BREKA_ORC_OVERLORD = 20270
const BREKA_ORC_WARRIOR = 20271
const FETTERED_SOUL = 20552
const WINDSUS = 20553
const GIANT_FUNGUS = 20555
const MANASHEN_GARGOYLE = 20563
const LETO_LIZARDMAN = 20577
const LETO_LIZARDMAN_ARCHER = 20578
const LETO_LIZARDMAN_SOLDIER = 20579
const LETO_LIZARDMAN_WARRIOR = 20580
const LETO_LIZARDMAN_SHAMAN = 20581
const LETO_LIZARDMAN_OVERLORD = 20582
const KARUL_BUGBEAR = 20600

const PAKO_THE_CAT = 27102
const UNICORN_RACER = 27103
const SHADOW_TUREN = 27104
const MIMI_THE_CAT = 27105
const UNICORN_PHANTASM = 27106
const SILHOUETTE_TILFO = 27107
const REDUCTION_IN_RECOVERY_TIME = 4126

const minimumLevel = 39
const mobIdMap: { [ npcId: number ]: [ number, number, number ] } = {
    [ PAKO_THE_CAT ]: [ CRYSTAL_OF_INPROGRESS_1ST, CRYSTAL_OF_VICTORY_1ST, NpcStringIds.IM_SORRY_LORD ],
    [ UNICORN_RACER ]: [ CRYSTAL_OF_INPROGRESS_3RD, CRYSTAL_OF_VICTORY_3RD, NpcStringIds.I_LOSE ],
    [ SHADOW_TUREN ]: [ CRYSTAL_OF_INPROGRESS_5TH, CRYSTAL_OF_VICTORY_5TH, NpcStringIds.UGH_I_LOST ],
    [ MIMI_THE_CAT ]: [ CRYSTAL_OF_INPROGRESS_2ND, CRYSTAL_OF_VICTORY_2ND, NpcStringIds.LOST_SORRY_LORD ],
    [ UNICORN_PHANTASM ]: [ CRYSTAL_OF_INPROGRESS_4TH, CRYSTAL_OF_VICTORY_4TH, NpcStringIds.I_LOSE ],
    [ SILHOUETTE_TILFO ]: [ CRYSTAL_OF_INPROGRESS_6TH, CRYSTAL_OF_VICTORY_6TH, NpcStringIds.UGH_CAN_THIS_BE_HAPPENING ],
}

const eventNames = {
    despawn: 'd',
    killedAttacker: 'k',
}

const variableNames = {
    attackerId: 'aId',
}

export class TestOfTheSummoner extends ListenerLogic {
    constructor() {
        super( 'Q00230_TestOfTheSummoner', 'listeners/tracked-200/TestOfTheSummoner.ts' )
        this.questId = 230
        this.questItemIds = [
            LETOLIZARDMAN_AMULET,
            SAC_OF_REDSPORES,
            KARULBUGBEAR_TOTEM,
            SHARDS_OF_MANASHEN,
            BREKAORC_TOTEM,
            CRIMSON_BLOODSTONE,
            TALONS_OF_TYRANT,
            WINGS_OF_DRONEANT,
            TUSK_OF_WINDSUS,
            FANGS_OF_WYRM,
            LARAS_1ST_LIST,
            LARAS_2ND_LIST,
            LARAS_3RD_LIST,
            LARAS_4TH_LIST,
            LARAS_5TH_LIST,
            GALATEAS_LETTER,
            BEGINNERS_ARCANA,
            ALMORS_ARCANA,
            CAMONIELL_ARCANA,
            BELTHUS_ARCANA,
            BASILLIA_ARCANA,
            CELESTIEL_ARCANA,
            BRYNTHEA_ARCANA,
            CRYSTAL_OF_STARTING_1ST,
            CRYSTAL_OF_INPROGRESS_1ST,
            CRYSTAL_OF_FOUL_1ST,
            CRYSTAL_OF_DEFEAT_1ST,
            CRYSTAL_OF_VICTORY_1ST,
            CRYSTAL_OF_STARTING_2ND,
            CRYSTAL_OF_INPROGRESS_2ND,
            CRYSTAL_OF_FOUL_2ND,
            CRYSTAL_OF_DEFEAT_2ND,
            CRYSTAL_OF_VICTORY_2ND,
            CRYSTAL_OF_STARTING_3RD,
            CRYSTAL_OF_INPROGRESS_3RD,
            CRYSTAL_OF_FOUL_3RD,
            CRYSTAL_OF_DEFEAT_3RD,
            CRYSTAL_OF_VICTORY_3RD,
            CRYSTAL_OF_STARTING_4TH,
            CRYSTAL_OF_INPROGRESS_4TH,
            CRYSTAL_OF_FOUL_4TH,
            CRYSTAL_OF_DEFEAT_4TH,
            CRYSTAL_OF_VICTORY_4TH,
            CRYSTAL_OF_STARTING_5TH,
            CRYSTAL_OF_INPROGRESS_5TH,
            CRYSTAL_OF_FOUL_5TH,
            CRYSTAL_OF_DEFEAT_5TH,
            CRYSTAL_OF_VICTORY_5TH,
            CRYSTAL_OF_STARTING_6TH,
            CRYSTAL_OF_INPROGRESS_6TH,
            CRYSTAL_OF_FOUL_6TH,
            CRYSTAL_OF_DEFEAT_6TH,
            CRYSTAL_OF_VICTORY_6TH,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [
            PAKO_THE_CAT,
            UNICORN_RACER,
            SHADOW_TUREN,
            MIMI_THE_CAT,
            UNICORN_PHANTASM,
            SILHOUETTE_TILFO,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            NOBLE_ANT,
            NOBLE_ANT_LEADER,
            WYRM,
            TYRANT,
            TYRANT_KINGPIN,
            BREKA_ORC,
            BREKA_ORC_ARCHER,
            BREKA_ORC_SHAMAN,
            BREKA_ORC_OVERLORD,
            BREKA_ORC_WARRIOR,
            FETTERED_SOUL,
            WINDSUS,
            GIANT_FUNGUS,
            MANASHEN_GARGOYLE,
            LETO_LIZARDMAN,
            LETO_LIZARDMAN_ARCHER,
            LETO_LIZARDMAN_SOLDIER,
            LETO_LIZARDMAN_WARRIOR,
            LETO_LIZARDMAN_SHAMAN,
            LETO_LIZARDMAN_OVERLORD,
            KARUL_BUGBEAR,
            ..._.keys( mobIdMap ).map( value => _.parseInt( value ) ),
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00230_TestOfTheSummoner'
    }

    getQuestStartIds(): Array<number> {
        return [ HIGH_SUMMONER_GALATEA ]
    }

    getTalkIds(): Array<number> {
        return [
            HIGH_SUMMONER_GALATEA,
            GROCER_LARA,
            SUMMONER_ALMORS,
            SUMMONER_CAMONIELL,
            SUMMONER_BELTHUS,
            SUMMONER_BASILLA,
            SUMMONER_CELESTIEL,
            SUMMONER_BRYNTHEA,
        ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let conditionValue: number = NpcVariablesManager.get( data.targetId, this.getName() ) as number

        switch ( data.targetNpcId ) {
            case PAKO_THE_CAT:
                switch ( conditionValue ) {
                    case 0:
                        return this.onAttackedFirstTime( data, CRYSTAL_OF_STARTING_1ST, CRYSTAL_OF_INPROGRESS_1ST, NpcStringIds.WHHIISSHH )

                    case 1:
                        return this.onAttackedSecondTime( data, CRYSTAL_OF_STARTING_1ST, CRYSTAL_OF_INPROGRESS_1ST, CRYSTAL_OF_FOUL_1ST )
                }

                return

            case UNICORN_RACER:
                switch ( conditionValue ) {
                    case 0:
                        return this.onAttackedFirstTime( data, CRYSTAL_OF_STARTING_3RD, CRYSTAL_OF_INPROGRESS_3RD, NpcStringIds.START_DUEL )

                    case 1:
                        return this.onAttackedSecondTime( data, CRYSTAL_OF_STARTING_3RD, CRYSTAL_OF_INPROGRESS_3RD, CRYSTAL_OF_FOUL_3RD )
                }

                return

            case SHADOW_TUREN:
                switch ( conditionValue ) {
                    case 0:
                        return this.onAttackedFirstTime( data, CRYSTAL_OF_STARTING_5TH, CRYSTAL_OF_INPROGRESS_5TH, NpcStringIds.SO_SHALL_WE_START )

                    case 1:
                        return this.onAttackedSecondTime( data, CRYSTAL_OF_STARTING_5TH, CRYSTAL_OF_INPROGRESS_5TH, CRYSTAL_OF_FOUL_5TH )
                }

                return

            case MIMI_THE_CAT:
                switch ( conditionValue ) {
                    case 0:
                        return this.onAttackedFirstTime( data, CRYSTAL_OF_STARTING_2ND, CRYSTAL_OF_INPROGRESS_2ND, NpcStringIds.WHISH_FIGHT )

                    case 1:
                        return this.onAttackedSecondTime( data, CRYSTAL_OF_STARTING_2ND, CRYSTAL_OF_INPROGRESS_2ND, CRYSTAL_OF_FOUL_2ND )
                }

                return

            case UNICORN_PHANTASM:
                switch ( conditionValue ) {
                    case 0:
                        return this.onAttackedFirstTime( data, CRYSTAL_OF_STARTING_4TH, CRYSTAL_OF_INPROGRESS_4TH, NpcStringIds.START_DUEL )

                    case 1:
                        return this.onAttackedSecondTime( data, CRYSTAL_OF_STARTING_4TH, CRYSTAL_OF_INPROGRESS_4TH, CRYSTAL_OF_FOUL_4TH )
                }

                return

            case SILHOUETTE_TILFO:
                switch ( conditionValue ) {
                    case 0:
                        return this.onAttackedFirstTime( data, CRYSTAL_OF_STARTING_6TH, CRYSTAL_OF_INPROGRESS_6TH, NpcStringIds.ILL_WALK_ALL_OVER_YOU )

                    case 1:
                        return this.onAttackedSecondTime( data, CRYSTAL_OF_STARTING_6TH, CRYSTAL_OF_INPROGRESS_6TH, CRYSTAL_OF_FOUL_6TH )
                }

                return
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case NOBLE_ANT:
            case NOBLE_ANT_LEADER:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_5TH_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, WINGS_OF_DRONEANT, 2, 30, data.isChampion )
                }

                return

            case WYRM:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_5TH_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, FANGS_OF_WYRM, 3, 30, data.isChampion )
                }

                return

            case TYRANT:
            case TYRANT_KINGPIN:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_4TH_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, TALONS_OF_TYRANT, 3, 30, data.isChampion )
                }

                return

            case BREKA_ORC:
            case BREKA_ORC_ARCHER:
            case BREKA_ORC_WARRIOR:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_3RD_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, BREKAORC_TOTEM, 1, 30, data.isChampion )
                }

                return

            case BREKA_ORC_SHAMAN:
            case BREKA_ORC_OVERLORD:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_3RD_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, BREKAORC_TOTEM, 2, 30, data.isChampion )
                }

                return

            case FETTERED_SOUL:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_3RD_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, CRIMSON_BLOODSTONE, 6, 30, data.isChampion )
                }

                return

            case WINDSUS:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_4TH_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, TUSK_OF_WINDSUS, 3, 30, data.isChampion )
                }

                return

            case GIANT_FUNGUS:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_1ST_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, SAC_OF_REDSPORES, 2, 30, data.isChampion )
                }

                return

            case MANASHEN_GARGOYLE:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_2ND_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, SHARDS_OF_MANASHEN, 2, 30, data.isChampion )
                }

                return

            case LETO_LIZARDMAN:
            case LETO_LIZARDMAN_ARCHER:
            case LETO_LIZARDMAN_SOLDIER:
            case LETO_LIZARDMAN_WARRIOR:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_1ST_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, LETOLIZARDMAN_AMULET, 1, 30, data.isChampion )
                }

                return

            case LETO_LIZARDMAN_SHAMAN:
            case LETO_LIZARDMAN_OVERLORD:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_1ST_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, LETOLIZARDMAN_AMULET, 2, 30, data.isChampion )
                }

                return

            case KARUL_BUGBEAR:
                if ( !QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) && QuestHelper.hasQuestItem( player, LARAS_2ND_LIST ) ) {
                    await QuestHelper.rewardUpToLimit( player, KARULBUGBEAR_TOTEM, 2, 30, data.isChampion )
                }

                return

            case SILHOUETTE_TILFO:
            case UNICORN_PHANTASM:
            case MIMI_THE_CAT:
            case SHADOW_TUREN:
            case UNICORN_RACER:
            case PAKO_THE_CAT:
                let [ inProgressItemId, givenItemId, sayId ] = mobIdMap[ data.npcId ]
                if ( QuestHelper.hasQuestItem( player, inProgressItemId ) ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, sayId )

                    await QuestHelper.takeSingleItem( player, inProgressItemId, 1 )
                    await QuestHelper.giveSingleItem( player, givenItemId, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === eventNames.despawn ) {
            await ( L2World.getObjectById( data.characterId ) as L2Npc ).deleteMe()
            return
        }

        if ( data.eventName === eventNames.killedAttacker ) {
            let objectId = NpcVariablesManager.get( data.characterId, variableNames.attackerId ) as number
            let summon: L2Summon = L2World.getObjectById( objectId ) as L2Summon
            if ( summon && summon.isDead() ) {
                await summon.deleteMe()
                return
            }

            this.startQuestTimer( eventNames.killedAttacker, 5000, data.characterId, 0 )
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()

                    await QuestHelper.giveSingleItem( player, GALATEAS_LETTER, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 122 )

                        return this.getPath( '30634-08a.htm' )
                    }

                    return this.getPath( '30634-08.htm' )
                }

                return

            case '30634-04.htm':
            case '30634-05.htm':
            case '30634-06.htm':
            case '30634-07.htm':
            case '30634-11.html':
            case '30634-11a.html':
            case '30634-11b.html':
            case '30634-11c.html':
            case '30634-11d.html':
                break

            case '30063-02.html':
                switch ( _.random( 4 ) ) {
                    case 0:
                        await QuestHelper.giveSingleItem( player, LARAS_1ST_LIST, 1 )
                        break

                    case 1:
                        await QuestHelper.giveSingleItem( player, LARAS_2ND_LIST, 1 )
                        break

                    case 2:
                        await QuestHelper.giveSingleItem( player, LARAS_3RD_LIST, 1 )
                        break

                    case 3:
                        await QuestHelper.giveSingleItem( player, LARAS_4TH_LIST, 1 )
                        break

                    case 4:
                        await QuestHelper.giveSingleItem( player, LARAS_5TH_LIST, 1 )
                        break

                }

                state.setConditionWithSound( 2, true )
                await QuestHelper.takeSingleItem( player, GALATEAS_LETTER, 1 )
                break

            case '30063-04.html':
                switch ( _.random( 4 ) ) {
                    case 0:
                        await QuestHelper.giveSingleItem( player, LARAS_1ST_LIST, 1 )
                        break

                    case 1:
                        await QuestHelper.giveSingleItem( player, LARAS_2ND_LIST, 1 )
                        break

                    case 2:
                        await QuestHelper.giveSingleItem( player, LARAS_3RD_LIST, 1 )
                        break

                    case 3:
                        await QuestHelper.giveSingleItem( player, LARAS_4TH_LIST, 1 )
                        break

                    case 4:
                        await QuestHelper.giveSingleItem( player, LARAS_5TH_LIST, 1 )
                        break
                }

                break

            case '30635-03.html':
                if ( QuestHelper.hasQuestItem( player, BEGINNERS_ARCANA ) ) {
                    break
                }

                return this.getPath( '30635-02.html' )

            case '30635-04.html':
                QuestHelper.addSkillCastDesire( npc, player, REDUCTION_IN_RECOVERY_TIME, 1, 1000000 )
                await QuestHelper.giveSingleItem( player, CRYSTAL_OF_STARTING_1ST, 1 )
                await QuestHelper.takeMultipleItems( player, 1, BEGINNERS_ARCANA, CRYSTAL_OF_FOUL_1ST, CRYSTAL_OF_DEFEAT_1ST )
                break

            case '30636-03.html':
                if ( QuestHelper.hasQuestItem( player, BEGINNERS_ARCANA ) ) {
                    break
                }

                return this.getPath( '30636-02.html' )

            case '30636-04.html':
                QuestHelper.addSkillCastDesire( npc, player, REDUCTION_IN_RECOVERY_TIME, 1, 1000000 )
                await QuestHelper.giveSingleItem( player, CRYSTAL_OF_STARTING_3RD, 1 )
                await QuestHelper.takeMultipleItems( player, 1, BEGINNERS_ARCANA, CRYSTAL_OF_FOUL_3RD, CRYSTAL_OF_DEFEAT_3RD )
                break

            case '30637-03.html':
                if ( QuestHelper.hasQuestItem( player, BEGINNERS_ARCANA ) ) {
                    break
                }

                return this.getPath( '30637-02.html' )

            case '30637-04.html':
                QuestHelper.addSkillCastDesire( npc, player, REDUCTION_IN_RECOVERY_TIME, 1, 1000000 )
                await QuestHelper.giveSingleItem( player, CRYSTAL_OF_STARTING_5TH, 1 )
                await QuestHelper.takeMultipleItems( player, 1, BEGINNERS_ARCANA, CRYSTAL_OF_FOUL_5TH, CRYSTAL_OF_DEFEAT_5TH )
                break

            case '30638-03.html':
                if ( QuestHelper.hasQuestItem( player, BEGINNERS_ARCANA ) ) {
                    break
                }

                return this.getPath( '30638-02.html' )

            case '30638-04.html':
                QuestHelper.addSkillCastDesire( npc, player, REDUCTION_IN_RECOVERY_TIME, 1, 1000000 )
                await QuestHelper.giveSingleItem( player, CRYSTAL_OF_STARTING_2ND, 1 )
                await QuestHelper.takeMultipleItems( player, 1, BEGINNERS_ARCANA, CRYSTAL_OF_FOUL_2ND, CRYSTAL_OF_DEFEAT_2ND )
                break

            case '30639-03.html':
                if ( QuestHelper.hasQuestItem( player, BEGINNERS_ARCANA ) ) {
                    break
                }

                return this.getPath( '30639-02.html' )

            case '30639-04.html':
                QuestHelper.addSkillCastDesire( npc, player, REDUCTION_IN_RECOVERY_TIME, 1, 1000000 )
                await QuestHelper.giveSingleItem( player, CRYSTAL_OF_STARTING_4TH, 1 )
                await QuestHelper.takeMultipleItems( player, 1, BEGINNERS_ARCANA, CRYSTAL_OF_FOUL_4TH, CRYSTAL_OF_DEFEAT_4TH )
                break

            case '30640-03.html':
                if ( QuestHelper.hasQuestItem( player, BEGINNERS_ARCANA ) ) {
                    break
                }

                return this.getPath( '30640-02.html' )

            case '30640-04.html':
                QuestHelper.addSkillCastDesire( npc, player, REDUCTION_IN_RECOVERY_TIME, 1, 1000000 )
                await QuestHelper.giveSingleItem( player, CRYSTAL_OF_STARTING_6TH, 1 )
                await QuestHelper.takeMultipleItems( player, 1, BEGINNERS_ARCANA, CRYSTAL_OF_FOUL_6TH, CRYSTAL_OF_DEFEAT_6TH )
                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId !== HIGH_SUMMONER_GALATEA ) {
                    break
                }

                if ( ![ ClassIdValues.wizard.id, ClassIdValues.elvenWizard.id, ClassIdValues.darkWizard.id ].includes( player.getClassId() ) ) {
                    return this.getPath( '30634-01.html' )
                }

                if ( player.getLevel() < minimumLevel ) {
                    return this.getPath( '30634-02.html' )
                }

                return this.getPath( '30634-03.htm' )

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case HIGH_SUMMONER_GALATEA:
                        if ( QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) ) {
                            return this.getPath( '30634-09.html' )
                        }

                        let hasGalateaItems = QuestHelper.hasQuestItems( player, ALMORS_ARCANA, BASILLIA_ARCANA, CAMONIELL_ARCANA, CELESTIEL_ARCANA, BELTHUS_ARCANA, BRYNTHEA_ARCANA )
                        if ( !hasGalateaItems ) {
                            return this.getPath( QuestHelper.hasQuestItem( player, BEGINNERS_ARCANA ) ? '30634-11.html' : '30634-10.html' )
                        }

                        await QuestHelper.giveAdena( player, 300960, true )
                        await QuestHelper.giveSingleItem( player, MARK_OF_SUMMONER, 1 )
                        await QuestHelper.addExpAndSp( player, 1664494, 114220 )
                        await state.exitQuest( false, true )

                        player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                        return this.getPath( '30634-12.html' )

                    case GROCER_LARA:
                        if ( QuestHelper.hasQuestItem( player, GALATEAS_LETTER ) ) {
                            return this.getPath( '30063-01.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, LARAS_1ST_LIST, LARAS_2ND_LIST, LARAS_3RD_LIST, LARAS_4TH_LIST, LARAS_5TH_LIST ) ) {
                            return this.getPath( '30063-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LARAS_1ST_LIST ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, LETOLIZARDMAN_AMULET ) >= 30
                                    && QuestHelper.getQuestItemsCount( player, SAC_OF_REDSPORES ) >= 30 ) {
                                await QuestHelper.takeMultipleItems( player, -1, LETOLIZARDMAN_AMULET, SAC_OF_REDSPORES, LARAS_1ST_LIST )
                                await QuestHelper.giveSingleItem( player, BEGINNERS_ARCANA, 2 )

                                state.setConditionWithSound( 3, true )
                                return this.getPath( '30063-06.html' )
                            }

                            return this.getPath( '30063-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LARAS_2ND_LIST ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, KARULBUGBEAR_TOTEM ) >= 30
                                    && QuestHelper.getQuestItemsCount( player, SHARDS_OF_MANASHEN ) >= 30 ) {
                                await QuestHelper.takeMultipleItems( player, -1, KARULBUGBEAR_TOTEM, SHARDS_OF_MANASHEN, LARAS_2ND_LIST )
                                await QuestHelper.giveSingleItem( player, BEGINNERS_ARCANA, 2 )

                                state.setConditionWithSound( 3, true )
                                return this.getPath( '30063-08.html' )
                            }

                            return this.getPath( '30063-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LARAS_3RD_LIST ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, BREKAORC_TOTEM ) >= 30
                                    && QuestHelper.getQuestItemsCount( player, CRIMSON_BLOODSTONE ) >= 30 ) {
                                await QuestHelper.takeMultipleItems( player, -1, BREKAORC_TOTEM, CRIMSON_BLOODSTONE, LARAS_3RD_LIST )
                                await QuestHelper.giveSingleItem( player, BEGINNERS_ARCANA, 2 )

                                state.setConditionWithSound( 3, true )
                                return this.getPath( '30063-10.html' )
                            }

                            return this.getPath( '30063-09.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LARAS_4TH_LIST ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, TALONS_OF_TYRANT ) >= 30
                                    && QuestHelper.getQuestItemsCount( player, TUSK_OF_WINDSUS ) >= 30 ) {
                                await QuestHelper.takeMultipleItems( player, -1, TALONS_OF_TYRANT, TUSK_OF_WINDSUS, LARAS_4TH_LIST )
                                await QuestHelper.giveSingleItem( player, BEGINNERS_ARCANA, 2 )

                                state.setConditionWithSound( 3, true )
                                return this.getPath( '30063-12.html' )
                            }

                            return this.getPath( '30063-11.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LARAS_5TH_LIST ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, WINGS_OF_DRONEANT ) >= 30
                                    && QuestHelper.getQuestItemsCount( player, FANGS_OF_WYRM ) >= 30 ) {
                                await QuestHelper.takeMultipleItems( player, -1, WINGS_OF_DRONEANT, FANGS_OF_WYRM, LARAS_5TH_LIST )
                                await QuestHelper.giveSingleItem( player, BEGINNERS_ARCANA, 2 )

                                state.setConditionWithSound( 3, true )
                                return this.getPath( '30063-14.html' )
                            }

                            return this.getPath( '30063-13.html' )
                        }

                        break

                    case SUMMONER_ALMORS:
                        if ( QuestHelper.hasQuestItem( player, ALMORS_ARCANA ) ) {
                            return this.getPath( '30635-10.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_1ST, CRYSTAL_OF_INPROGRESS_1ST, CRYSTAL_OF_FOUL_1ST, CRYSTAL_OF_DEFEAT_1ST, CRYSTAL_OF_VICTORY_1ST ) ) {
                            return this.getPath( '30635-01.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_1ST, CRYSTAL_OF_INPROGRESS_1ST, CRYSTAL_OF_FOUL_1ST, CRYSTAL_OF_VICTORY_1ST )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_DEFEAT_1ST ) ) {
                            return this.getPath( '30635-05.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_1ST, CRYSTAL_OF_INPROGRESS_1ST, CRYSTAL_OF_DEFEAT_1ST, CRYSTAL_OF_VICTORY_1ST )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_FOUL_1ST ) ) {
                            return this.getPath( '30635-06.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_1ST, CRYSTAL_OF_INPROGRESS_1ST, CRYSTAL_OF_FOUL_1ST, CRYSTAL_OF_DEFEAT_1ST )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_VICTORY_1ST ) ) {
                            await QuestHelper.giveSingleItem( player, ALMORS_ARCANA, 1 )
                            await QuestHelper.takeSingleItem( player, CRYSTAL_OF_VICTORY_1ST, 1 )

                            if ( QuestHelper.hasQuestItems( player, BASILLIA_ARCANA, CAMONIELL_ARCANA, CELESTIEL_ARCANA, BELTHUS_ARCANA, BRYNTHEA_ARCANA ) ) {
                                state.setConditionWithSound( 4, true )
                            }

                            return this.getPath( '30635-07.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_INPROGRESS_1ST, CRYSTAL_OF_FOUL_1ST, CRYSTAL_OF_DEFEAT_1ST, CRYSTAL_OF_VICTORY_1ST )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_STARTING_1ST ) ) {
                            return this.getPath( '30635-08.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_1ST, CRYSTAL_OF_FOUL_1ST, CRYSTAL_OF_DEFEAT_1ST, CRYSTAL_OF_VICTORY_1ST )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_INPROGRESS_1ST ) ) {
                            return this.getPath( '30635-09.html' )
                        }

                        break

                    case SUMMONER_CAMONIELL:
                        if ( QuestHelper.hasQuestItem( player, CAMONIELL_ARCANA ) ) {
                            return this.getPath( '30636-10.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_3RD, CRYSTAL_OF_INPROGRESS_3RD, CRYSTAL_OF_FOUL_3RD, CRYSTAL_OF_DEFEAT_3RD, CRYSTAL_OF_VICTORY_3RD ) ) {
                            return this.getPath( '30636-01.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_3RD, CRYSTAL_OF_INPROGRESS_3RD, CRYSTAL_OF_FOUL_3RD, CRYSTAL_OF_VICTORY_3RD )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_DEFEAT_3RD ) ) {
                            return this.getPath( '30636-05.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_3RD, CRYSTAL_OF_INPROGRESS_3RD, CRYSTAL_OF_DEFEAT_3RD, CRYSTAL_OF_VICTORY_3RD )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_FOUL_3RD ) ) {
                            return this.getPath( '30636-06.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_3RD, CRYSTAL_OF_INPROGRESS_3RD, CRYSTAL_OF_FOUL_3RD, CRYSTAL_OF_DEFEAT_3RD )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_VICTORY_3RD ) ) {
                            await QuestHelper.giveSingleItem( player, CAMONIELL_ARCANA, 1 )
                            await QuestHelper.takeSingleItem( player, CRYSTAL_OF_VICTORY_3RD, 1 )

                            if ( QuestHelper.hasQuestItems( player, ALMORS_ARCANA, BASILLIA_ARCANA, CELESTIEL_ARCANA, BELTHUS_ARCANA, BRYNTHEA_ARCANA ) ) {
                                state.setConditionWithSound( 4, true )
                            }

                            return this.getPath( '30636-07.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_INPROGRESS_3RD, CRYSTAL_OF_FOUL_3RD, CRYSTAL_OF_DEFEAT_3RD, CRYSTAL_OF_VICTORY_3RD )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_STARTING_3RD ) ) {
                            return this.getPath( '30636-08.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_3RD, CRYSTAL_OF_FOUL_3RD, CRYSTAL_OF_DEFEAT_3RD, CRYSTAL_OF_VICTORY_3RD )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_INPROGRESS_3RD ) ) {
                            return this.getPath( '30636-09.html' )
                        }

                        break

                    case SUMMONER_BELTHUS:
                        if ( QuestHelper.hasQuestItem( player, BELTHUS_ARCANA ) ) {
                            return this.getPath( '30637-10.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_5TH, CRYSTAL_OF_INPROGRESS_5TH, CRYSTAL_OF_FOUL_5TH, CRYSTAL_OF_DEFEAT_5TH, CRYSTAL_OF_VICTORY_5TH ) ) {
                            return this.getPath( '30637-01.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_5TH, CRYSTAL_OF_INPROGRESS_5TH, CRYSTAL_OF_FOUL_5TH, CRYSTAL_OF_VICTORY_5TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_DEFEAT_5TH ) ) {
                            return this.getPath( '30637-05.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_5TH, CRYSTAL_OF_INPROGRESS_5TH, CRYSTAL_OF_DEFEAT_5TH, CRYSTAL_OF_VICTORY_5TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_FOUL_5TH ) ) {
                            return this.getPath( '30637-06.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_5TH, CRYSTAL_OF_INPROGRESS_5TH, CRYSTAL_OF_FOUL_5TH, CRYSTAL_OF_DEFEAT_5TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_VICTORY_5TH ) ) {
                            await QuestHelper.giveSingleItem( player, BELTHUS_ARCANA, 1 )
                            await QuestHelper.takeSingleItem( player, CRYSTAL_OF_VICTORY_5TH, 1 )

                            if ( QuestHelper.hasQuestItems( player, ALMORS_ARCANA, BASILLIA_ARCANA, CAMONIELL_ARCANA, CELESTIEL_ARCANA, BRYNTHEA_ARCANA ) ) {
                                state.setConditionWithSound( 4, true )
                            }

                            return this.getPath( '30637-07.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_INPROGRESS_5TH, CRYSTAL_OF_FOUL_5TH, CRYSTAL_OF_DEFEAT_5TH, CRYSTAL_OF_VICTORY_5TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_STARTING_5TH ) ) {
                            return this.getPath( '30637-08.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_5TH, CRYSTAL_OF_FOUL_5TH, CRYSTAL_OF_DEFEAT_5TH, CRYSTAL_OF_VICTORY_5TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_INPROGRESS_5TH ) ) {
                            return this.getPath( '30637-09.html' )
                        }

                        break

                    case SUMMONER_BASILLA:
                        if ( QuestHelper.hasQuestItem( player, BASILLIA_ARCANA ) ) {
                            return this.getPath( '30638-10.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_2ND, CRYSTAL_OF_INPROGRESS_2ND, CRYSTAL_OF_FOUL_2ND, CRYSTAL_OF_DEFEAT_2ND, CRYSTAL_OF_VICTORY_2ND ) ) {
                            return this.getPath( '30638-01.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_2ND, CRYSTAL_OF_INPROGRESS_2ND, CRYSTAL_OF_FOUL_2ND, CRYSTAL_OF_VICTORY_2ND )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_DEFEAT_2ND ) ) {
                            return this.getPath( '30638-05.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_2ND, CRYSTAL_OF_INPROGRESS_2ND, CRYSTAL_OF_DEFEAT_2ND, CRYSTAL_OF_VICTORY_2ND )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_FOUL_2ND ) ) {
                            return this.getPath( '30638-06.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_2ND, CRYSTAL_OF_INPROGRESS_2ND, CRYSTAL_OF_FOUL_2ND, CRYSTAL_OF_DEFEAT_2ND )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_VICTORY_2ND ) ) {
                            await QuestHelper.giveSingleItem( player, BASILLIA_ARCANA, 1 )
                            await QuestHelper.takeSingleItem( player, CRYSTAL_OF_VICTORY_2ND, 1 )

                            if ( QuestHelper.hasQuestItems( player, ALMORS_ARCANA, CAMONIELL_ARCANA, CELESTIEL_ARCANA, BELTHUS_ARCANA, BRYNTHEA_ARCANA ) ) {
                                state.setConditionWithSound( 4, true )
                            }

                            return this.getPath( '30638-07.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_INPROGRESS_2ND, CRYSTAL_OF_FOUL_2ND, CRYSTAL_OF_DEFEAT_2ND, CRYSTAL_OF_VICTORY_2ND )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_STARTING_2ND ) ) {
                            return this.getPath( '30638-08.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_2ND, CRYSTAL_OF_FOUL_2ND, CRYSTAL_OF_DEFEAT_2ND, CRYSTAL_OF_VICTORY_2ND )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_INPROGRESS_2ND ) ) {
                            return this.getPath( '30638-09.html' )
                        }

                        break

                    case SUMMONER_CELESTIEL:
                        if ( QuestHelper.hasQuestItem( player, CELESTIEL_ARCANA ) ) {
                            return this.getPath( '30639-10.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_4TH, CRYSTAL_OF_INPROGRESS_4TH, CRYSTAL_OF_FOUL_4TH, CRYSTAL_OF_DEFEAT_4TH, CRYSTAL_OF_VICTORY_4TH ) ) {
                            return this.getPath( '30639-01.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_4TH, CRYSTAL_OF_INPROGRESS_4TH, CRYSTAL_OF_FOUL_4TH, CRYSTAL_OF_VICTORY_4TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_DEFEAT_4TH ) ) {
                            return this.getPath( '30639-05.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_4TH, CRYSTAL_OF_INPROGRESS_4TH, CRYSTAL_OF_DEFEAT_4TH, CRYSTAL_OF_VICTORY_4TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_FOUL_4TH ) ) {
                            return this.getPath( '30639-06.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_4TH, CRYSTAL_OF_INPROGRESS_4TH, CRYSTAL_OF_FOUL_4TH, CRYSTAL_OF_DEFEAT_4TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_VICTORY_4TH ) ) {
                            await QuestHelper.giveSingleItem( player, CELESTIEL_ARCANA, 1 )
                            await QuestHelper.takeSingleItem( player, CRYSTAL_OF_VICTORY_4TH, 1 )

                            if ( QuestHelper.hasQuestItems( player, ALMORS_ARCANA, BASILLIA_ARCANA, CAMONIELL_ARCANA, BELTHUS_ARCANA, BRYNTHEA_ARCANA ) ) {
                                state.setConditionWithSound( 4, true )
                            }

                            return this.getPath( '30639-07.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_INPROGRESS_4TH, CRYSTAL_OF_FOUL_4TH, CRYSTAL_OF_DEFEAT_4TH, CRYSTAL_OF_VICTORY_4TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_STARTING_4TH ) ) {
                            return this.getPath( '30639-08.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_4TH, CRYSTAL_OF_FOUL_4TH, CRYSTAL_OF_DEFEAT_4TH, CRYSTAL_OF_VICTORY_4TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_INPROGRESS_4TH ) ) {
                            return this.getPath( '30639-09.html' )
                        }

                        break

                    case SUMMONER_BRYNTHEA:
                        if ( QuestHelper.hasQuestItem( player, BRYNTHEA_ARCANA ) ) {
                            return this.getPath( '30640-10.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_6TH, CRYSTAL_OF_INPROGRESS_6TH, CRYSTAL_OF_FOUL_6TH, CRYSTAL_OF_DEFEAT_6TH, CRYSTAL_OF_VICTORY_6TH ) ) {
                            return this.getPath( '30640-01.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_6TH, CRYSTAL_OF_INPROGRESS_6TH, CRYSTAL_OF_FOUL_6TH, CRYSTAL_OF_VICTORY_6TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_DEFEAT_6TH ) ) {
                            return this.getPath( '30640-05.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_6TH, CRYSTAL_OF_INPROGRESS_6TH, CRYSTAL_OF_DEFEAT_6TH, CRYSTAL_OF_VICTORY_6TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_FOUL_6TH ) ) {
                            return this.getPath( '30640-06.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_6TH, CRYSTAL_OF_INPROGRESS_6TH, CRYSTAL_OF_FOUL_6TH, CRYSTAL_OF_DEFEAT_6TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_VICTORY_6TH ) ) {
                            await QuestHelper.giveSingleItem( player, BRYNTHEA_ARCANA, 1 )
                            await QuestHelper.takeSingleItem( player, CRYSTAL_OF_VICTORY_6TH, 1 )

                            if ( QuestHelper.hasQuestItems( player, ALMORS_ARCANA, BASILLIA_ARCANA, CAMONIELL_ARCANA, CELESTIEL_ARCANA, BELTHUS_ARCANA ) ) {
                                state.setConditionWithSound( 4, true )
                            }

                            return this.getPath( '30640-07.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_INPROGRESS_6TH, CRYSTAL_OF_FOUL_6TH, CRYSTAL_OF_DEFEAT_6TH, CRYSTAL_OF_VICTORY_6TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_STARTING_6TH ) ) {
                            return this.getPath( '30640-08.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, CRYSTAL_OF_STARTING_6TH, CRYSTAL_OF_FOUL_6TH, CRYSTAL_OF_DEFEAT_6TH, CRYSTAL_OF_VICTORY_6TH )
                                && QuestHelper.hasQuestItem( player, CRYSTAL_OF_INPROGRESS_6TH ) ) {
                            return this.getPath( '30640-09.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === HIGH_SUMMONER_GALATEA ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    async onAttackedFirstTime( data: AttackableAttackedEvent, neededItemId: number, givenItemId: number, sayId: number ): Promise<void> {
        let isSummon: boolean = data.attackerInstanceType !== InstanceType.L2Summon

        if ( !isSummon ) {
            return
        }

        NpcVariablesManager.set( data.targetId, variableNames.attackerId, data.attackerId )
        NpcVariablesManager.set( data.targetId, this.getName(), 1 )

        this.startQuestTimer( eventNames.despawn, 120000, data.targetId, 0 )
        this.startQuestTimer( eventNames.killedAttacker, 5000, data.targetId, 0 )

        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName() )
        let player = L2World.getPlayer( data.attackerPlayerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( state && state.isStarted() && QuestHelper.hasQuestItem( player, neededItemId ) ) {
            let packet = NpcSay.fromNpcString(
                    npc.getObjectId(),
                    NpcSayType.NpcAll,
                    npc.getTemplate().getDisplayId(),
                    sayId ).getBuffer()

            BroadcastHelper.dataInLocation( npc, packet, npc.getBroadcastRadius() )

            await QuestHelper.takeSingleItem( player, neededItemId, -1 )
            await QuestHelper.giveSingleItem( player, givenItemId, 1 )
            QuestHelper.addAttackDesire( npc, L2World.getObjectById( data.attackerId ) as L2Character, 100000 )
        }

        return
    }

    async onAttackedSecondTime( data: AttackableAttackedEvent, failureItemId: number, neededItemId: number, givenItemId: number ): Promise<void> {
        let isSummon: boolean = data.attackerInstanceType !== InstanceType.L2Summon

        if ( !isSummon || ( NpcVariablesManager.get( data.targetId, variableNames.attackerId ) !== data.attackerId ) ) {
            let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName() )
            let player = L2World.getPlayer( data.attackerPlayerId )
            let npc = L2World.getObjectById( data.targetId ) as L2Npc

            if ( state
                    && state.isStarted()
                    && !QuestHelper.hasQuestItem( player, failureItemId )
                    && QuestHelper.hasQuestItem( player, neededItemId ) ) {

                NpcVariablesManager.set( data.targetId, this.getName(), 2 )

                let packet = NpcSay.fromNpcString(
                        npc.getObjectId(),
                        NpcSayType.NpcAll,
                        npc.getTemplate().getDisplayId(),
                        NpcStringIds.RULE_VIOLATION ).getBuffer()
                BroadcastHelper.dataInLocation( npc, packet, npc.getBroadcastRadius() )


                await QuestHelper.takeSingleItem( player, neededItemId, -1 )
                await QuestHelper.giveSingleItem( player, givenItemId, 1 )
            }

            await npc.deleteMe()
        }
    }
}