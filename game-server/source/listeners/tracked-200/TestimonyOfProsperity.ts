import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { Race } from '../../gameService/enums/Race'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'

const WAREHOUSE_KEEPER_WILFORD = 30005
const WAREHOUSE_KEEPER_PARMAN = 30104
const LILITH = 30368
const GUARD_BRIGHT = 30466
const TRADER_SHARI = 30517
const TRADER_MION = 30519
const IRON_GATES_LOCKIRIN = 30531
const GOLDEN_WHEELS_SPIRON = 30532
const SILVER_SCALES_BALANKI = 30533
const BRONZE_KEYS_KEEF = 30534
const GRAY_PILLAR_MEMBER_FILAUR = 30535
const BLACK_ANVILS_ARIN = 30536
const MARYSE_REDBONNET = 30553
const MINER_BOLTER = 30554
const CARRIER_TOROCCO = 30555
const MASTER_TOMA = 30556
const PIOTUR = 30597
const EMILY = 30620
const MAESTRO_NIKOLA = 30621
const BOX_OF_TITAN = 30622

const ANIMAL_SKIN = 1867
const RECIPE_TITAN_KEY = 3023
const KEY_OF_TITAN = 3030
const RING_OF_TESTIMONY_1ST = 3239
const RING_OF_TESTIMONY_2ND = 3240
const OLD_ACCOUNT_BOOK = 3241
const BLESSED_SEED = 3242
const EMILYS_RECIPE = 3243
const LILITHS_ELVEN_WAFER = 3244
const MAPHR_TABLET_FRAGMENT = 3245
const COLLECTION_LICENSE = 3246
const LOCKIRINS_1ST_NOTICE = 3247
const LOCKIRINS_2ND_NOTICE = 3248
const LOCKIRINS_3RD_NOTICE = 3249
const LOCKIRINS_4TH_NOTICE = 3250
const LOCKIRINS_5TH_NOTICE = 3251
const CONTRIBUTION_OF_SHARI = 3252
const CONTRIBUTION_OF_MION = 3253
const CONTRIBUTION_OF_MARYSE = 3254
const MARYSES_REQUEST = 3255
const CONTRIBUTION_OF_TOMA = 3256
const RECEIPT_OF_BOLTER = 3257
const RECEIPT_OF_CONTRIBUTION_1ST = 3258
const RECEIPT_OF_CONTRIBUTION_2ND = 3259
const RECEIPT_OF_CONTRIBUTION_3RD = 3260
const RECEIPT_OF_CONTRIBUTION_4TH = 3261
const RECEIPT_OF_CONTRIBUTION_5TH = 3262
const PROCURATION_OF_TOROCCO = 3263
const BRIGHTS_LIST = 3264
const MANDRAGORA_PETAL = 3265
const CRIMSON_MOSS = 3266
const MANDRAGORA_BOUGUET = 3267
const PARMANS_INSTRUCTIONS = 3268
const PARMANS_LETTER = 3269
const CLAY_DOUGH = 3270
const PATTERN_OF_KEYHOLE = 3271
const NIKOLAS_LIST = 3272
const STAKATO_SHELL = 3273
const TOAD_LORD_SAC = 3274
const MARSH_SPIDER_THORN = 3275
const CRYSTAL_BROOCH = 3428

const MARK_OF_PROSPERITY = 3238
const DIMENSIONAL_DIAMOND = 7562

const MANDRAGORA_SPROUT1 = 20154
const MANDRAGORA_SAPLING = 20155
const MANDRAGORA_BLOSSOM = 20156
const MARSH_STAKATO = 20157
const MANDRAGORA_SPROUT2 = 20223
const GIANT_CRIMSON_ANT = 20228
const MARSH_STAKATO_WORKER = 20230
const TOAD_LORD = 20231
const MARSH_STAKATO_SOLDIER = 20232
const MARSH_SPIDER = 20233
const MARSH_STAKATO_DRONE = 20234

const minimumLevel = 37

export class TestimonyOfProsperity extends ListenerLogic {
    constructor() {
        super( 'Q00221_TestimonyOfProsperity', 'listeners/tracked-200/TestimonyOfProsperity.ts' )
        this.questId = 221
        this.questItemIds = [
            RECIPE_TITAN_KEY,
            KEY_OF_TITAN,
            RING_OF_TESTIMONY_1ST,
            RING_OF_TESTIMONY_2ND,
            OLD_ACCOUNT_BOOK,
            BLESSED_SEED,
            EMILYS_RECIPE,
            LILITHS_ELVEN_WAFER,
            MAPHR_TABLET_FRAGMENT,
            COLLECTION_LICENSE,
            LOCKIRINS_1ST_NOTICE,
            LOCKIRINS_2ND_NOTICE,
            LOCKIRINS_3RD_NOTICE,
            LOCKIRINS_4TH_NOTICE,
            LOCKIRINS_5TH_NOTICE,
            CONTRIBUTION_OF_SHARI,
            CONTRIBUTION_OF_MION,
            CONTRIBUTION_OF_MARYSE,
            MARYSES_REQUEST,
            CONTRIBUTION_OF_TOMA,
            RECEIPT_OF_BOLTER,
            RECEIPT_OF_CONTRIBUTION_1ST,
            RECEIPT_OF_CONTRIBUTION_2ND,
            RECEIPT_OF_CONTRIBUTION_3RD,
            RECEIPT_OF_CONTRIBUTION_4TH,
            RECEIPT_OF_CONTRIBUTION_5TH,
            PROCURATION_OF_TOROCCO,
            BRIGHTS_LIST,
            MANDRAGORA_PETAL,
            CRIMSON_MOSS,
            MANDRAGORA_BOUGUET,
            PARMANS_INSTRUCTIONS,
            PARMANS_LETTER,
            CLAY_DOUGH,
            PATTERN_OF_KEYHOLE,
            NIKOLAS_LIST,
            STAKATO_SHELL,
            TOAD_LORD_SAC,
            MARSH_SPIDER_THORN,
            CRYSTAL_BROOCH,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            MANDRAGORA_SPROUT1,
            MANDRAGORA_SAPLING,
            MANDRAGORA_BLOSSOM,
            MARSH_STAKATO,
            MANDRAGORA_SPROUT2,
            GIANT_CRIMSON_ANT,
            MARSH_STAKATO_WORKER,
            TOAD_LORD,
            MARSH_STAKATO_SOLDIER,
            MARSH_SPIDER,
            MARSH_STAKATO_DRONE,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00221_TestimonyOfProsperity'
    }

    getQuestStartIds(): Array<number> {
        return [ WAREHOUSE_KEEPER_PARMAN ]
    }

    getTalkIds(): Array<number> {
        return [
            WAREHOUSE_KEEPER_PARMAN,
            WAREHOUSE_KEEPER_WILFORD,
            LILITH,
            GUARD_BRIGHT,
            TRADER_SHARI,
            TRADER_MION,
            IRON_GATES_LOCKIRIN,
            GOLDEN_WHEELS_SPIRON,
            SILVER_SCALES_BALANKI,
            BRONZE_KEYS_KEEF,
            GRAY_PILLAR_MEMBER_FILAUR,
            BLACK_ANVILS_ARIN,
            MARYSE_REDBONNET,
            MINER_BOLTER,
            CARRIER_TOROCCO,
            MASTER_TOMA,
            PIOTUR,
            EMILY,
            MAESTRO_NIKOLA,
            BOX_OF_TITAN,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( npc.getId() ) {
            case MANDRAGORA_SPROUT1:
            case MANDRAGORA_SAPLING:
            case MANDRAGORA_BLOSSOM:
            case MANDRAGORA_SPROUT2:
                if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, BRIGHTS_LIST )
                        && !QuestHelper.hasQuestItem( player, EMILYS_RECIPE )
                        && QuestHelper.getQuestItemsCount( player, MANDRAGORA_PETAL ) < 20 ) {

                    await QuestHelper.rewardSingleQuestItem( player, MANDRAGORA_PETAL, 1, data.isChampion )
                    if ( QuestHelper.getQuestItemsCount( player, MANDRAGORA_PETAL ) >= 20 ) {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case MARSH_STAKATO:
            case MARSH_STAKATO_WORKER:
            case MARSH_STAKATO_SOLDIER:
            case MARSH_STAKATO_DRONE:
                if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_2ND, NIKOLAS_LIST )
                        && !QuestHelper.hasAtLeastOneQuestItem( player, CLAY_DOUGH, PATTERN_OF_KEYHOLE )
                        && QuestHelper.getQuestItemsCount( player, STAKATO_SHELL ) < 20 ) {

                    await QuestHelper.rewardSingleQuestItem( player, STAKATO_SHELL, 1, data.isChampion )
                    this.tryProgressQuest( player, state )
                }

                return

            case GIANT_CRIMSON_ANT:
                if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, BRIGHTS_LIST )
                        && !QuestHelper.hasQuestItem( player, EMILYS_RECIPE )
                        && QuestHelper.getQuestItemsCount( player, CRIMSON_MOSS ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, CRIMSON_MOSS, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, CRIMSON_MOSS ) >= 10 ) {
                        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case TOAD_LORD:
                if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_2ND, NIKOLAS_LIST )
                        && !QuestHelper.hasAtLeastOneQuestItem( player, CLAY_DOUGH, PATTERN_OF_KEYHOLE )
                        && QuestHelper.getQuestItemsCount( player, TOAD_LORD_SAC ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, TOAD_LORD_SAC, 1, data.isChampion )

                    this.tryProgressQuest( player, state )
                }

                return

            case MARSH_SPIDER:
                if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_2ND, NIKOLAS_LIST )
                        && !QuestHelper.hasAtLeastOneQuestItem( player, CLAY_DOUGH, PATTERN_OF_KEYHOLE )
                        && QuestHelper.getQuestItemsCount( player, MARSH_SPIDER_THORN ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, MARSH_SPIDER_THORN, 1, data.isChampion )

                    this.tryProgressQuest( player, state )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()

                    if ( !QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_1ST ) ) {
                        await QuestHelper.giveSingleItem( player, RING_OF_TESTIMONY_1ST, 1 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 50 )

                        return this.getPath( '30104-04e.htm' )
                    }

                    return this.getPath( '30104-04.htm' )
                }

                return

            case '30104-08.html':
                await QuestHelper.takeMultipleItems( player, 1, RING_OF_TESTIMONY_1ST, OLD_ACCOUNT_BOOK, BLESSED_SEED, EMILYS_RECIPE, LILITHS_ELVEN_WAFER )
                await QuestHelper.giveMultipleItems( player, 1, RING_OF_TESTIMONY_2ND, PARMANS_LETTER )

                state.setConditionWithSound( 4, true )
                break

            case '30104-04a.html':
            case '30104-04b.html':
            case '30104-04c.html':
            case '30104-04d.html':
            case '30104-05.html':
            case '30104-08a.html':
            case '30104-08b.html':
            case '30104-08c.html':
            case '30005-02.html':
            case '30005-03.html':
            case '30368-02.html':
            case '30466-02.html':
            case '30531-02.html':
            case '30620-02.html':
            case '30621-02.html':
            case '30621-03.html':
                break

            case '30005-04.html':
                await QuestHelper.giveSingleItem( player, CRYSTAL_BROOCH, 1 )
                break

            case '30368-03.html':
                if ( QuestHelper.hasQuestItem( player, CRYSTAL_BROOCH ) ) {
                    await QuestHelper.giveSingleItem( player, LILITHS_ELVEN_WAFER, 1 )
                    await QuestHelper.takeSingleItem( player, CRYSTAL_BROOCH, 1 )

                    if ( QuestHelper.hasQuestItems( player, OLD_ACCOUNT_BOOK, BLESSED_SEED, EMILYS_RECIPE ) ) {
                        state.setConditionWithSound( 2, true )
                    }

                    break
                }

                return

            case '30466-03.html':
                await QuestHelper.giveSingleItem( player, BRIGHTS_LIST, 1 )
                break

            case '30531-03.html':
                await QuestHelper.giveMultipleItems( player, 1,
                        COLLECTION_LICENSE,
                        LOCKIRINS_1ST_NOTICE,
                        LOCKIRINS_2ND_NOTICE,
                        LOCKIRINS_3RD_NOTICE,
                        LOCKIRINS_4TH_NOTICE,
                        LOCKIRINS_5TH_NOTICE )
                break

            case '30534-03a.html':
                if ( player.getAdena() < 5000 ) {
                    break
                }

                if ( QuestHelper.hasQuestItem( player, PROCURATION_OF_TOROCCO ) ) {
                    await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 5000 )
                    await QuestHelper.takeSingleItem( player, PROCURATION_OF_TOROCCO, 1 )
                    await QuestHelper.giveSingleItem( player, RECEIPT_OF_CONTRIBUTION_3RD, 1 )

                    return this.getPath( '30534-03b.html' )
                }

                return

            case '30555-02.html':
                await QuestHelper.giveSingleItem( player, PROCURATION_OF_TOROCCO, 1 )
                break

            case '30597-02.html':
                await QuestHelper.giveSingleItem( player, BLESSED_SEED, 1 )
                if ( QuestHelper.hasQuestItems( player, OLD_ACCOUNT_BOOK, EMILYS_RECIPE, LILITHS_ELVEN_WAFER ) ) {
                    state.setConditionWithSound( 2, true )
                }

                break

            case '30620-03.html':
                if ( QuestHelper.hasQuestItem( player, MANDRAGORA_BOUGUET ) ) {
                    await QuestHelper.giveSingleItem( player, EMILYS_RECIPE, 1 )
                    await QuestHelper.takeSingleItem( player, MANDRAGORA_BOUGUET, 1 )

                    if ( QuestHelper.hasQuestItems( player, OLD_ACCOUNT_BOOK, BLESSED_SEED, LILITHS_ELVEN_WAFER ) ) {
                        state.setConditionWithSound( 2, true )
                    }

                    break
                }

                return

            case '30621-04.html':
                await QuestHelper.giveSingleItem( player, CLAY_DOUGH, 1 )
                state.setConditionWithSound( 5, true )
                break

            case '30622-02.html':
                if ( QuestHelper.hasQuestItem( player, CLAY_DOUGH ) ) {
                    await QuestHelper.takeSingleItem( player, CLAY_DOUGH, 1 )
                    await QuestHelper.giveSingleItem( player, PATTERN_OF_KEYHOLE, 1 )

                    state.setConditionWithSound( 6, true )
                    break
                }

                return

            case '30622-04.html':
                if ( QuestHelper.hasQuestItem( player, KEY_OF_TITAN ) ) {
                    await QuestHelper.giveSingleItem( player, MAPHR_TABLET_FRAGMENT, 1 )
                    await QuestHelper.takeMultipleItems( player, -1, KEY_OF_TITAN, NIKOLAS_LIST, RECIPE_TITAN_KEY, STAKATO_SHELL, TOAD_LORD_SAC, MARSH_SPIDER_THORN )

                    state.setConditionWithSound( 9, true )
                    break
                }

                return


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === WAREHOUSE_KEEPER_PARMAN ) {
                    if ( player.getRace() !== Race.DWARF ) {
                        return this.getPath( '30104-01.html' )
                    }

                    if ( player.getLevel() < minimumLevel ) {
                        return this.getPath( '30104-02.html' )
                    }

                    if ( player.isInCategory( CategoryType.DWARF_2ND_GROUP ) ) {
                        return this.getPath( '30104-03.htm' )
                    }

                    return this.getPath( '30104-01a.html' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case WAREHOUSE_KEEPER_PARMAN:
                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_1ST ) ) {
                            if ( QuestHelper.hasQuestItems( player, OLD_ACCOUNT_BOOK, BLESSED_SEED, EMILYS_RECIPE, LILITHS_ELVEN_WAFER ) ) {
                                return this.getPath( '30104-06.html' )
                            }

                            return this.getPath( '30104-05.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, PARMANS_INSTRUCTIONS ) ) {
                            await QuestHelper.takeSingleItem( player, PARMANS_INSTRUCTIONS, 1 )
                            await QuestHelper.giveMultipleItems( player, 1, RING_OF_TESTIMONY_2ND, PARMANS_LETTER )

                            state.setConditionWithSound( 4, true )
                            return this.getPath( '30104-10.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_2ND ) ) {
                            if ( QuestHelper.hasQuestItem( player, PARMANS_LETTER ) ) {
                                return this.getPath( '30104-11.html' )
                            }

                            if ( QuestHelper.hasAtLeastOneQuestItem( player, CLAY_DOUGH, PATTERN_OF_KEYHOLE, NIKOLAS_LIST ) ) {
                                return this.getPath( '30104-12.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, MAPHR_TABLET_FRAGMENT ) ) {
                                await QuestHelper.giveAdena( player, 217682, true )
                                await QuestHelper.giveSingleItem( player, MARK_OF_PROSPERITY, 1 )
                                await QuestHelper.addExpAndSp( player, 1199958, 80080 )
                                await state.exitQuest( false, true )

                                player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                                return this.getPath( '30104-13.html' )
                            }
                        }

                        break

                    case WAREHOUSE_KEEPER_WILFORD:
                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_1ST ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, LILITHS_ELVEN_WAFER, CRYSTAL_BROOCH ) ) {
                                return this.getPath( '30005-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CRYSTAL_BROOCH )
                                    && !QuestHelper.hasQuestItem( player, LILITHS_ELVEN_WAFER ) ) {
                                return this.getPath( '30005-05.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, LILITHS_ELVEN_WAFER ) ) {
                                return this.getPath( '30005-06.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_2ND ) ) {
                            return this.getPath( '30005-07.html' )
                        }

                        break

                    case LILITH:
                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_1ST ) ) {
                            if ( QuestHelper.hasQuestItem( player, CRYSTAL_BROOCH )
                                    && !QuestHelper.hasQuestItem( player, LILITHS_ELVEN_WAFER ) ) {
                                return this.getPath( '30368-01.html' )
                            }

                            return this.getPath( '30368-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_2ND ) ) {
                            return this.getPath( '30368-05.html' )
                        }

                        break

                    case GUARD_BRIGHT:
                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_1ST ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, EMILYS_RECIPE, BRIGHTS_LIST, MANDRAGORA_BOUGUET ) ) {
                                return this.getPath( '30466-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, BRIGHTS_LIST ) && !QuestHelper.hasQuestItem( player, EMILYS_RECIPE ) ) {
                                if ( QuestHelper.getQuestItemsCount( player, MANDRAGORA_PETAL ) < 20
                                        || QuestHelper.getQuestItemsCount( player, CRIMSON_MOSS ) < 10 ) {
                                    return this.getPath( '30466-04.html' )
                                }

                                await QuestHelper.takeMultipleItems( player, -1, BRIGHTS_LIST, MANDRAGORA_PETAL, CRIMSON_MOSS )
                                await QuestHelper.giveSingleItem( player, MANDRAGORA_BOUGUET, 1 )

                                return this.getPath( '30466-05.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, MANDRAGORA_BOUGUET )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, EMILYS_RECIPE, BRIGHTS_LIST ) ) {
                                return this.getPath( '30466-06.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, EMILYS_RECIPE ) ) {
                                return this.getPath( '30466-07.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_2ND ) ) {
                            return this.getPath( '30466-08.html' )
                        }

                        break

                    case TRADER_SHARI:
                        if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, COLLECTION_LICENSE ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_1ST, CONTRIBUTION_OF_SHARI, LOCKIRINS_1ST_NOTICE ) ) {
                                await QuestHelper.giveSingleItem( player, CONTRIBUTION_OF_SHARI, 1 )

                                return this.getPath( '30517-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CONTRIBUTION_OF_SHARI )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, LOCKIRINS_1ST_NOTICE, RECEIPT_OF_CONTRIBUTION_1ST ) ) {
                                return this.getPath( '30517-02.html' )
                            }
                        }

                        break

                    case TRADER_MION:
                        if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, COLLECTION_LICENSE ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_2ND, CONTRIBUTION_OF_MION, LOCKIRINS_2ND_NOTICE ) ) {
                                await QuestHelper.giveSingleItem( player, CONTRIBUTION_OF_MION, 1 )

                                return this.getPath( '30519-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CONTRIBUTION_OF_MION )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, LOCKIRINS_2ND_NOTICE, RECEIPT_OF_CONTRIBUTION_2ND ) ) {
                                return this.getPath( '30519-02.html' )
                            }
                        }

                        break

                    case IRON_GATES_LOCKIRIN:
                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_1ST ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, COLLECTION_LICENSE, OLD_ACCOUNT_BOOK ) ) {
                                return this.getPath( '30531-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, COLLECTION_LICENSE ) ) {
                                if ( QuestHelper.hasQuestItems( player, RECEIPT_OF_CONTRIBUTION_1ST, RECEIPT_OF_CONTRIBUTION_2ND, RECEIPT_OF_CONTRIBUTION_3RD, RECEIPT_OF_CONTRIBUTION_4TH, RECEIPT_OF_CONTRIBUTION_5TH ) ) {
                                    await QuestHelper.giveSingleItem( player, OLD_ACCOUNT_BOOK, 1 )
                                    await QuestHelper.takeMultipleItems( player, 1,
                                            COLLECTION_LICENSE,
                                            RECEIPT_OF_CONTRIBUTION_1ST,
                                            RECEIPT_OF_CONTRIBUTION_2ND,
                                            RECEIPT_OF_CONTRIBUTION_3RD,
                                            RECEIPT_OF_CONTRIBUTION_4TH,
                                            RECEIPT_OF_CONTRIBUTION_5TH )

                                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                                    if ( QuestHelper.hasQuestItems( player, BLESSED_SEED, EMILYS_RECIPE, LILITHS_ELVEN_WAFER ) ) {
                                        state.setConditionWithSound( 2, true )
                                    }

                                    return this.getPath( '30531-05.html' )
                                }

                                return this.getPath( '30531-04.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, OLD_ACCOUNT_BOOK ) && !QuestHelper.hasQuestItem( player, COLLECTION_LICENSE ) ) {
                                return this.getPath( '30531-06.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_2ND ) ) {
                            return this.getPath( '30531-07.html' )
                        }

                        break

                    case GOLDEN_WHEELS_SPIRON:
                        if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, COLLECTION_LICENSE ) ) {
                            if ( QuestHelper.hasQuestItem( player, LOCKIRINS_1ST_NOTICE )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, CONTRIBUTION_OF_SHARI, RECEIPT_OF_CONTRIBUTION_1ST ) ) {
                                await QuestHelper.takeSingleItem( player, LOCKIRINS_1ST_NOTICE, 1 )

                                return this.getPath( '30532-01.html' )
                            }

                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_1ST, CONTRIBUTION_OF_SHARI, LOCKIRINS_1ST_NOTICE ) ) {
                                return this.getPath( '30532-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CONTRIBUTION_OF_SHARI )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_1ST, LOCKIRINS_1ST_NOTICE ) ) {
                                await QuestHelper.takeSingleItem( player, CONTRIBUTION_OF_SHARI, 1 )
                                await QuestHelper.giveSingleItem( player, RECEIPT_OF_CONTRIBUTION_1ST, 1 )

                                return this.getPath( '30532-03.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, RECEIPT_OF_CONTRIBUTION_1ST )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, CONTRIBUTION_OF_SHARI, LOCKIRINS_1ST_NOTICE ) ) {
                                return this.getPath( '30532-04.html' )
                            }
                        }

                        break

                    case SILVER_SCALES_BALANKI:
                        if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, COLLECTION_LICENSE ) ) {
                            let hasItemCount: boolean = QuestHelper.getQuestItemsCount( player, CONTRIBUTION_OF_MION ) + QuestHelper.getQuestItemsCount( player, CONTRIBUTION_OF_MARYSE ) < 2
                            if ( QuestHelper.hasQuestItem( player, LOCKIRINS_2ND_NOTICE )
                                    && !QuestHelper.hasQuestItem( player, RECEIPT_OF_CONTRIBUTION_2ND ) && hasItemCount ) {
                                await QuestHelper.takeSingleItem( player, LOCKIRINS_2ND_NOTICE, 1 )
                                return this.getPath( '30533-01.html' )
                            }

                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, LOCKIRINS_2ND_NOTICE, RECEIPT_OF_CONTRIBUTION_2ND )
                                    && hasItemCount ) {
                                return this.getPath( '30533-02.html' )
                            }

                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, LOCKIRINS_2ND_NOTICE, RECEIPT_OF_CONTRIBUTION_2ND )
                                    && QuestHelper.hasQuestItems( player, CONTRIBUTION_OF_MION, CONTRIBUTION_OF_MARYSE ) ) {
                                await QuestHelper.takeSingleItem( player, CONTRIBUTION_OF_MION, 1 )
                                await QuestHelper.takeSingleItem( player, CONTRIBUTION_OF_MARYSE, 1 )
                                await QuestHelper.giveSingleItem( player, RECEIPT_OF_CONTRIBUTION_2ND, 1 )

                                return this.getPath( '30533-03.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, RECEIPT_OF_CONTRIBUTION_2ND )
                                    && !QuestHelper.hasQuestItems( player, LOCKIRINS_2ND_NOTICE, CONTRIBUTION_OF_MION, CONTRIBUTION_OF_MARYSE ) ) {
                                return this.getPath( '30533-04.html' )
                            }
                        }

                        break

                    case BRONZE_KEYS_KEEF:
                        if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, COLLECTION_LICENSE ) ) {
                            if ( QuestHelper.hasQuestItem( player, LOCKIRINS_3RD_NOTICE )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_3RD, PROCURATION_OF_TOROCCO ) ) {
                                await QuestHelper.takeSingleItem( player, LOCKIRINS_3RD_NOTICE, 1 )
                                return this.getPath( '30534-01.html' )
                            }

                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_3RD, PROCURATION_OF_TOROCCO, LOCKIRINS_3RD_NOTICE ) ) {
                                return this.getPath( '30534-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, PROCURATION_OF_TOROCCO )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, LOCKIRINS_3RD_NOTICE, RECEIPT_OF_CONTRIBUTION_3RD ) ) {
                                return this.getPath( '30534-03.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, RECEIPT_OF_CONTRIBUTION_3RD )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, PROCURATION_OF_TOROCCO, LOCKIRINS_3RD_NOTICE ) ) {
                                return this.getPath( '30534-04.html' )
                            }
                        }

                        break

                    case GRAY_PILLAR_MEMBER_FILAUR:
                        if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, COLLECTION_LICENSE ) ) {
                            if ( QuestHelper.hasQuestItem( player, LOCKIRINS_4TH_NOTICE )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_4TH, RECEIPT_OF_BOLTER ) ) {
                                await QuestHelper.takeSingleItem( player, LOCKIRINS_4TH_NOTICE, 1 )
                                return this.getPath( '30535-01.html' )
                            }

                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_4TH, RECEIPT_OF_BOLTER, LOCKIRINS_4TH_NOTICE ) ) {
                                return this.getPath( '30535-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, RECEIPT_OF_BOLTER )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_4TH, LOCKIRINS_4TH_NOTICE ) ) {
                                await QuestHelper.takeSingleItem( player, RECEIPT_OF_BOLTER, 1 )
                                await QuestHelper.giveSingleItem( player, RECEIPT_OF_CONTRIBUTION_4TH, 1 )

                                return this.getPath( '30535-03.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, RECEIPT_OF_CONTRIBUTION_4TH )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_BOLTER, LOCKIRINS_4TH_NOTICE ) ) {
                                return this.getPath( '30535-04.html' )
                            }
                        }

                        break

                    case BLACK_ANVILS_ARIN:
                        if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, COLLECTION_LICENSE ) ) {
                            if ( QuestHelper.hasQuestItem( player, LOCKIRINS_5TH_NOTICE )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_5TH, CONTRIBUTION_OF_TOMA ) ) {
                                await QuestHelper.takeSingleItem( player, LOCKIRINS_5TH_NOTICE, 1 )

                                return this.getPath( '30536-01.html' )
                            }

                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_5TH, CONTRIBUTION_OF_TOMA, LOCKIRINS_5TH_NOTICE ) ) {
                                return this.getPath( '30536-02.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CONTRIBUTION_OF_TOMA )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_5TH, LOCKIRINS_5TH_NOTICE ) ) {
                                await QuestHelper.takeSingleItem( player, CONTRIBUTION_OF_TOMA, 1 )
                                await QuestHelper.giveSingleItem( player, RECEIPT_OF_CONTRIBUTION_5TH, 1 )

                                return this.getPath( '30536-03.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, RECEIPT_OF_CONTRIBUTION_5TH )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, CONTRIBUTION_OF_TOMA, LOCKIRINS_5TH_NOTICE ) ) {
                                return this.getPath( '30536-04.html' )
                            }
                        }

                        break

                    case MARYSE_REDBONNET:
                        if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, COLLECTION_LICENSE ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_2ND, CONTRIBUTION_OF_MARYSE, LOCKIRINS_2ND_NOTICE, MARYSES_REQUEST ) ) {
                                await QuestHelper.giveSingleItem( player, MARYSES_REQUEST, 1 )

                                return this.getPath( '30553-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, MARYSES_REQUEST )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_2ND, CONTRIBUTION_OF_MARYSE, LOCKIRINS_2ND_NOTICE ) ) {
                                if ( QuestHelper.getQuestItemsCount( player, ANIMAL_SKIN ) < 10 ) {
                                    return this.getPath( '30553-02.html' )
                                }

                                await QuestHelper.takeMultipleItems( player, 10, ANIMAL_SKIN, MARYSES_REQUEST )
                                await QuestHelper.giveSingleItem( player, CONTRIBUTION_OF_MARYSE, 1 )

                                return this.getPath( '30553-03.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CONTRIBUTION_OF_MARYSE )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_2ND, LOCKIRINS_2ND_NOTICE, MARYSES_REQUEST ) ) {
                                return this.getPath( '30553-04.html' )
                            }
                        }

                        break

                    case MINER_BOLTER:
                        if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, COLLECTION_LICENSE ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_4TH, RECEIPT_OF_BOLTER, LOCKIRINS_4TH_NOTICE ) ) {
                                await QuestHelper.giveSingleItem( player, RECEIPT_OF_BOLTER, 1 )

                                return this.getPath( '30554-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, RECEIPT_OF_BOLTER )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_4TH, LOCKIRINS_4TH_NOTICE ) ) {
                                return this.getPath( '30554-02.html' )
                            }
                        }

                        break

                    case CARRIER_TOROCCO:
                        if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, COLLECTION_LICENSE ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_3RD, PROCURATION_OF_TOROCCO, LOCKIRINS_3RD_NOTICE ) ) {
                                return this.getPath( '30555-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, PROCURATION_OF_TOROCCO )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_3RD, LOCKIRINS_3RD_NOTICE ) ) {
                                return this.getPath( '30555-03.html' )
                            }
                        }

                        break

                    case MASTER_TOMA:
                        if ( QuestHelper.hasQuestItems( player, RING_OF_TESTIMONY_1ST, COLLECTION_LICENSE ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_5TH, CONTRIBUTION_OF_TOMA, LOCKIRINS_5TH_NOTICE ) ) {
                                await QuestHelper.giveSingleItem( player, CONTRIBUTION_OF_TOMA, 1 )

                                return this.getPath( '30556-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CONTRIBUTION_OF_TOMA )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, RECEIPT_OF_CONTRIBUTION_5TH, LOCKIRINS_5TH_NOTICE ) ) {
                                return this.getPath( '30556-02.html' )
                            }
                        }

                        break

                    case PIOTUR:
                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_1ST ) ) {
                            if ( !QuestHelper.hasQuestItem( player, BLESSED_SEED ) ) {
                                return this.getPath( '30597-01.html' )
                            }

                            return this.getPath( '30597-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_2ND ) ) {
                            return this.getPath( '30597-04.html' )
                        }

                        break

                    case EMILY:
                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_1ST ) ) {
                            if ( QuestHelper.hasQuestItem( player, MANDRAGORA_BOUGUET )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, EMILYS_RECIPE, BRIGHTS_LIST ) ) {
                                return this.getPath( '30620-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, EMILYS_RECIPE ) ) {
                                return this.getPath( '30620-04.html' )
                            }

                            break
                        }

                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_2ND ) ) {
                            return this.getPath( '30620-05.html' )
                        }

                        break

                    case MAESTRO_NIKOLA:
                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_2ND ) ) {
                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, CLAY_DOUGH, PATTERN_OF_KEYHOLE, NIKOLAS_LIST, MAPHR_TABLET_FRAGMENT ) ) {
                                await QuestHelper.takeSingleItem( player, PARMANS_LETTER, 1 )

                                return this.getPath( '30621-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, CLAY_DOUGH )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, PATTERN_OF_KEYHOLE, NIKOLAS_LIST, MAPHR_TABLET_FRAGMENT ) ) {
                                return this.getPath( '30621-05.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, PATTERN_OF_KEYHOLE )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, CLAY_DOUGH, NIKOLAS_LIST, MAPHR_TABLET_FRAGMENT ) ) {
                                await QuestHelper.takeSingleItem( player, PATTERN_OF_KEYHOLE, 1 )
                                await QuestHelper.giveMultipleItems( player, 1, RECIPE_TITAN_KEY, NIKOLAS_LIST )

                                state.setConditionWithSound( 7, true )
                                return this.getPath( '30621-06.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, NIKOLAS_LIST )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, CLAY_DOUGH, PATTERN_OF_KEYHOLE, MAPHR_TABLET_FRAGMENT, KEY_OF_TITAN ) ) {
                                return this.getPath( '30621-07.html' )
                            }

                            if ( QuestHelper.hasQuestItems( player, NIKOLAS_LIST, KEY_OF_TITAN )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, CLAY_DOUGH, PATTERN_OF_KEYHOLE, MAPHR_TABLET_FRAGMENT ) ) {
                                return this.getPath( '30621-08.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, MAPHR_TABLET_FRAGMENT )
                                    && !QuestHelper.hasAtLeastOneQuestItem( player, CLAY_DOUGH, PATTERN_OF_KEYHOLE, NIKOLAS_LIST ) ) {
                                return this.getPath( '30621-09.html' )
                            }
                        }

                        break

                    case BOX_OF_TITAN:
                        if ( QuestHelper.hasQuestItem( player, RING_OF_TESTIMONY_2ND ) ) {
                            if ( QuestHelper.hasQuestItem( player, CLAY_DOUGH )
                                    && !QuestHelper.hasQuestItem( player, PATTERN_OF_KEYHOLE ) ) {
                                return this.getPath( '30622-01.html' )
                            }

                            if ( QuestHelper.hasQuestItem( player, KEY_OF_TITAN )
                                    && !QuestHelper.hasQuestItem( player, MAPHR_TABLET_FRAGMENT ) ) {
                                return this.getPath( '30622-03.html' )
                            }

                            if ( !QuestHelper.hasAtLeastOneQuestItem( player, KEY_OF_TITAN, CLAY_DOUGH ) ) {
                                return this.getPath( '30622-05.html' )
                            }
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === WAREHOUSE_KEEPER_PARMAN ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    tryProgressQuest( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.getQuestItemsCount( player, STAKATO_SHELL ) >= 20
                && QuestHelper.getQuestItemsCount( player, TOAD_LORD_SAC ) >= 10
                && QuestHelper.getQuestItemsCount( player, MARSH_SPIDER_THORN ) >= 10 ) {

            state.setConditionWithSound( 8 )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

            return
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}