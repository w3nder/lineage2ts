import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const LARS = 30063
const BRIGHT = 30466
const EMILLY = 30620
const FRUIT_BASKET = 7136
const AVELLAN_SPICE = 7137
const HONEY_POUCH = 7138
const minimumLevel = 34
const requiredAmount = 100

const monsterRewardChances = {
    20934: 700, // Wasp Worker
    20935: 770, // Wasp Leader
}

type QuestReward = [ number, number, number ] // chance, itemId, amount
const questRewards: Array<QuestReward> = [
    [ 400, ItemTypes.Adena, 2500 ], // Adena
    [ 550, 1865, 50 ], // Varnish
    [ 700, 1870, 50 ], // Coal
    [ 850, 1869, 50 ], // Iron Ore
    [ 1000, 1871, 50 ], // Charcoal
]

export class GatherIngredientsForPie extends ListenerLogic {
    constructor() {
        super( 'Q00299_GatherIngredientsForPie', 'listeners/tracked-200/GatherIngredientsForPie.ts' )
        this.questId = 299
        this.questItemIds = [
            FRUIT_BASKET,
            HONEY_POUCH,
            AVELLAN_SPICE,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00299_GatherIngredientsForPie'
    }

    getQuestStartIds(): Array<number> {
        return [ EMILLY ]
    }

    getTalkIds(): Array<number> {
        return [ LARS, BRIGHT, EMILLY ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), 1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !state ) {
            return
        }

        let player = state.getPlayer()
        if ( _.random( 1000 ) >= QuestHelper.getAdjustedChance( HONEY_POUCH, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            return
        }

        let amount = _.random( 1, 2 )
        await QuestHelper.rewardAndProgressState( player, state, HONEY_POUCH, amount, requiredAmount, data.isChampion, 2 )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30063-02.html':
                if ( state.isCondition( 3 ) ) {
                    await QuestHelper.giveSingleItem( player, AVELLAN_SPICE, 1 )
                    state.setConditionWithSound( 4, true )

                    break
                }

                return

            case '30466-02.html':
                if ( state.isCondition( 5 ) ) {
                    await QuestHelper.giveSingleItem( player, FRUIT_BASKET, 1 )
                    state.setConditionWithSound( 6, true )

                    break
                }

                return

            case '30620-03.htm':
                state.startQuest()
                break

            case '30620-06.html':
                if ( state.isCondition( 2 )
                        && QuestHelper.getQuestItemsCount( player, HONEY_POUCH ) >= 100 ) {
                    await QuestHelper.takeSingleItem( player, HONEY_POUCH, -1 )
                    state.setConditionWithSound( 3, true )

                    break
                }

                return this.getPath( '30620-07.html' )

            case '30620-10.html':
                if ( state.isCondition( 4 )
                        && QuestHelper.hasQuestItem( player, AVELLAN_SPICE ) ) {
                    await QuestHelper.takeSingleItem( player, AVELLAN_SPICE, -1 )
                    state.setConditionWithSound( 5, true )

                    break
                }

                return this.getPath( '30620-11.html' )

            case '30620-14.html':
                if ( state.isCondition( 6 )
                        && QuestHelper.hasQuestItem( player, FRUIT_BASKET ) ) {

                    await QuestHelper.takeSingleItem( player, FRUIT_BASKET, -1 )

                    let chance = _.random( 1000 )
                    let reward: QuestReward = _.find( questRewards, ( item: QuestReward ): boolean => {
                        return item[ 0 ] >= chance
                    } )

                    if ( reward ) {
                        let [ , itemId, amount ] = reward
                        await QuestHelper.rewardSingleItem( player, itemId, amount )
                    }

                    await state.exitQuest( true, true )
                    break
                }

                return this.getPath( '30620-15.html' )

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case LARS:
                switch ( state.getCondition() ) {
                    case 3:
                        return this.getPath( '30063-01.html' )

                    case 4:
                        return this.getPath( '30063-03.html' )
                }

                break

            case BRIGHT:
                switch ( state.getCondition() ) {
                    case 5:
                        return this.getPath( '30466-01.html' )

                    case 6:
                        return this.getPath( '30466-03.html' )
                }

                break

            case EMILLY:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30620-01.htm' : '30620-02.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '30620-05.html' )

                            case 2:
                                if ( QuestHelper.getQuestItemsCount( player, HONEY_POUCH ) >= requiredAmount ) {
                                    return this.getPath( '30620-04.html' )
                                }

                                break

                            case 3:
                                return this.getPath( '30620-08.html' )

                            case 4:
                                if ( QuestHelper.hasQuestItem( player, AVELLAN_SPICE ) ) {
                                    return this.getPath( '30620-09.html' )
                                }

                                break

                            case 5:
                                return this.getPath( '30620-12.html' )

                            case 6:
                                if ( QuestHelper.hasQuestItem( player, FRUIT_BASKET ) ) {
                                    return this.getPath( '30620-13.html' )
                                }

                                break
                        }

                        break
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}