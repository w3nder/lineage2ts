import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const LAKI = 32742
const VIAL_OF_TANTA_BLOOD = 15499

type NamedReward = [ number, number ] // itemId, amount
const icarusRewardItems: Array<NamedReward> = [
    [ 10381, 1 ],
    [ 10405, 1 ],
    [ 10405, 4 ],
    [ 10405, 4 ],
    [ 10405, 6 ],
]

const moiraiRewardItems: Array<NamedReward> = [
    [ 15776, 1 ],
    [ 15779, 1 ],
    [ 15782, 1 ],
    [ 15785, 1 ],
    [ 15788, 1 ],
    [ 15812, 1 ],
    [ 15813, 1 ],
    [ 15814, 1 ],
    [ 15646, 5 ],
    [ 15649, 5 ],
    [ 15652, 5 ],
    [ 15655, 5 ],
    [ 15658, 5 ],
    [ 15772, 1 ],
    [ 15773, 1 ],
    [ 15774, 1 ],
]

const minimumLevel = 82
const monsterRewardChances = {
    22768: 509, // Tanta Lizardman Scout
    22769: 689, // Tanta Lizardman Warrior
    22770: 123, // Tanta Lizardman Soldier
    22771: 159, // Tanta Lizardman Berserker
    22772: 739, // Tanta Lizardman Archer
    22773: 737, // Tanta Lizardman Magician
    22774: 261, // Tanta Lizardman Summoner
}

export class FiguringItOut extends ListenerLogic {
    constructor() {
        super( 'Q00287_FiguringItOut', 'listeners/tracked-200/FiguringItOut.ts' )
        this.questId = 287
        this.questItemIds = [ VIAL_OF_TANTA_BLOOD ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00287_FiguringItOut'
    }

    getQuestStartIds(): Array<number> {
        return [ LAKI ]
    }

    getTalkIds(): Array<number> {
        return [ LAKI ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 1 )
        if ( !player ) {
            return
        }

        if ( _.random( 1000 ) < QuestHelper.getAdjustedChance( VIAL_OF_TANTA_BLOOD, monsterRewardChances[ data.npcId ], data.isChampion ) ) {
            await QuestHelper.rewardSingleQuestItem( player, VIAL_OF_TANTA_BLOOD, 1, data.isChampion )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32742-03.htm':
                state.startQuest()
                break

            case 'Icarus':
                if ( QuestHelper.getQuestItemsCount( player, VIAL_OF_TANTA_BLOOD ) >= 500 ) {
                    let [ itemId, amount ] = _.sample( icarusRewardItems )

                    await QuestHelper.rewardSingleItem( player, itemId, amount )
                    await QuestHelper.takeSingleItem( player, VIAL_OF_TANTA_BLOOD, 500 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_FINISH )
                    return this.getPath( '32742-06.html' )
                }

                return this.getPath( '32742-07.html' )

            case 'Moirai':
                if ( QuestHelper.getQuestItemsCount( player, VIAL_OF_TANTA_BLOOD ) >= 100 ) {
                    let [ itemId, amount ] = _.sample( moiraiRewardItems )

                    await QuestHelper.rewardSingleItem( player, itemId, amount )
                    await QuestHelper.takeSingleItem( player, VIAL_OF_TANTA_BLOOD, 100 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_FINISH )
                    return this.getPath( '32742-08.html' )
                }

                return this.getPath( '32742-09.html' )

            case '32742-11.html':
                if ( !QuestHelper.hasQuestItem( player, VIAL_OF_TANTA_BLOOD ) ) {
                    await state.exitQuest( true, true )

                    return this.getPath( '32742-12.html' )
                }

                break

            case '32742-13.html':
                await state.exitQuest( true, true )
                break

            case '32742-02.htm':
            case '32742-10.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                let canProceed: boolean = player.getLevel() >= minimumLevel && player.hasQuestCompleted( 'Q00250_WatchWhatYouEat' )
                return this.getPath( canProceed ? '32742-01.htm' : '32742-14.htm' )

            case QuestStateValues.STARTED:
                return this.getPath( QuestHelper.getQuestItemsCount( player, VIAL_OF_TANTA_BLOOD ) < 100 ? '32742-04.html' : '32742-05.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}