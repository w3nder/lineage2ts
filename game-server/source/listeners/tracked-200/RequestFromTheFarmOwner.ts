import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

const EDMOND = 30497
const MARIUS = 30405

const SPIDER_SKIN = 1495
const minimumLevel = 15
const SKIN_COUNT = 10
const SKIN_REWARD = 25
const SKIN_BONUS = 250
const eventItemMap = {
    '30405-04.html': [ 1061, 2 ], // Greater Healing Potion
    '30405-05.html': [ 17, 250 ], // Wooden Arrow
    '30405-05a.html': [ 1835, 60 ], // Soulshot: No Grade
    '30405-05c.html': [ 2509, 30 ], // Spiritshot: No Grade
}

export class RequestFromTheFarmOwner extends ListenerLogic {
    constructor() {
        super( 'Q00259_RequestFromTheFarmOwner', 'listeners/tracked-200/RequestFromTheFarmOwner.ts' )
        this.questId = 259
        this.questItemIds = [ SPIDER_SKIN ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            20103, // Giant Spider
            20106, // Talon Spider
            20108, // Blade Spider
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00259_RequestFromTheFarmOwner'
    }

    getQuestStartIds(): Array<number> {
        return [ EDMOND ]
    }

    getTalkIds(): Array<number> {
        return [ EDMOND, MARIUS ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        await QuestHelper.rewardSingleQuestItem( player, SPIDER_SKIN, 1, data.isChampion )
        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '30405-03.html':
            case '30405-05b.html':
            case '30405-05d.html':
            case '30497-07.html':
                break

            case '30405-04.html':
            case '30405-05.html':
            case '30405-05a.html':
            case '30405-05c.html':
                if ( QuestHelper.getQuestItemsCount( player, SPIDER_SKIN ) >= SKIN_COUNT ) {
                    let [ itemId, amount ] = eventItemMap[ data.eventName ]

                    await QuestHelper.rewardSingleItem( player, itemId, amount )
                    await QuestHelper.takeSingleItem( player, SPIDER_SKIN, -1 )
                    break
                }

                return

            case '30405-06.html':
                if ( QuestHelper.getQuestItemsCount( player, SPIDER_SKIN ) >= SKIN_COUNT ) {
                    break
                }

                return this.getPath( '30405-07.html' )

            case '30497-03.html':
                state.startQuest()
                break

            case '30497-06.html':
                await state.exitQuest( true, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case EDMOND:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        return this.getPath( player.getLevel() >= minimumLevel ? '30497-02.htm' : '30497-01.html' )

                    case QuestStateValues.STARTED:
                        let totalCount = QuestHelper.getQuestItemsCount( player, SPIDER_SKIN )
                        if ( totalCount === 0 ) {
                            return this.getPath( '30497-04.html' )
                        }

                        const amount = ( totalCount * SKIN_REWARD ) + ( totalCount >= 10 ? SKIN_BONUS : 0 )
                        await QuestHelper.giveAdena( player, amount, true )
                        await QuestHelper.takeSingleItem( player, SPIDER_SKIN, -1 )

                        return this.getPath( '30497-05.html' )

                }
                break

            case MARIUS:
                return this.getPath( QuestHelper.getQuestItemsCount( player, SPIDER_SKIN ) >= SKIN_COUNT ? '30405-02.html' : '30405-01.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}