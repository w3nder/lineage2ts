import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { Location } from '../../gameService/models/Location'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { QuestStateValues } from '../../gameService/models/quest/State'

const CARADINE = 31740
const LADY_OF_LAKE = 31745
const CARADINE_LETTER_LAST = 7679
const NOBLESS_TIARA = 7694
const CARADINE_LOC = new Location( 143209, 43968, -3038 )
const MIMIRS_ELIXIR = new SkillHolder( 4339 )
const minimumLevel = 75

export class PossessorOfAPreciousSoulPartFour extends ListenerLogic {
    constructor() {
        super( 'Q00247_PossessorOfAPreciousSoulPartFour', 'listeners/tracked-200/PossessorOfAPreciousSoulPartFour.ts' )
        this.questId = 247
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00247_PossessorOfAPreciousSoul4'
    }

    getQuestStartIds(): Array<number> {
        return [ CARADINE ]
    }

    getTalkIds(): Array<number> {
        return [ CARADINE, LADY_OF_LAKE ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( !player.isSubClassActive() ) {
            return this.getPath( 'no_sub.html' )
        }

        switch ( data.eventName ) {
            case '31740-3.html':
                state.startQuest()
                await QuestHelper.takeSingleItem( player, CARADINE_LETTER_LAST, -1 )
                break

            case '31740-5.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                    await player.teleportToLocationWithOffset( CARADINE_LOC, 0 )
                }

                break

            case '31745-5.html':
                if ( state.isCondition( 2 ) ) {
                    await QuestHelper.addExpAndSp( player, 93836, 0 )
                    await QuestHelper.giveSingleItem( player, NOBLESS_TIARA, 1 )
                    await state.exitQuest( false, true )

                    let npc = L2World.getObjectById( data.characterId ) as L2Npc
                    npc.setTarget( player )

                    await npc.doCastWithHolder( MIMIRS_ELIXIR )

                    await player.setNoble( true )
                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                }

                break
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        if ( state.isStarted() && !player.isSubClassActive() ) {
            return this.getPath( 'no_sub.html' )
        }

        switch ( data.characterNpcId ) {
            case CARADINE:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( player.hasQuestCompleted( 'Q00246_PossessorOfAPreciousSoulPartThree' ) ) {
                            return this.getPath( player.getLevel() >= minimumLevel ? '31740-1.htm' : '31740-2.html' )
                        }

                        break

                    case QuestStateValues.STARTED:
                        if ( state.isCondition( 1 ) ) {
                            return this.getPath( '31740-6.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case LADY_OF_LAKE:
                if ( state.isCondition( 2 ) ) {
                    return this.getPath( '31745-1.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}