import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestHelper } from '../helpers/QuestHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

import _ from 'lodash'

const INVENTOR_MARU = 32486
const ENERGY_ORE = 10866
const minimumLevel = 18

const monsterRewardChance = {
    21124: 0.46, // Red Eye Barbed Bat
    21125: 0.48, // Northern Trimden
    21126: 0.5, // Kerope Werewolf
    21127: 0.64, // Northern Goblin
    21128: 0.66, // Spine Golem
    21129: 0.68, // Kerope Werewolf Chief
    21130: 0.76, // Northern Goblin Leader
    21131: 0.78, // Enchanted Spine Golem
}

export class InventionAmbition extends ListenerLogic {
    constructor() {
        super( 'Q00269_InventionAmbition', 'listeners/tracked-200/InventionAmbition.ts' )
        this.questId = 269
        this.questItemIds = [ ENERGY_ORE ]
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChance ).map( value => _.parseInt( value ) )
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00269_InventionAmbition'
    }

    getQuestStartIds(): Array<number> {
        return [ INVENTOR_MARU ]
    }

    getTalkIds(): Array<number> {
        return [ INVENTOR_MARU ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player : L2PcInstance = this.getRandomPartyMemberPerCondition( L2World.getPlayer( data.playerId ), -1, 3, L2World.getObjectById( data.targetId ) as L2Npc )
        if ( !player ) {
            return
        }

        if ( Math.random() > QuestHelper.getAdjustedChance( ENERGY_ORE, monsterRewardChance[ data.npcId ], data.isChampion ) ) {
            return
        }

        await QuestHelper.rewardSingleQuestItem( player, ENERGY_ORE, 1, data.isChampion )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case '32486-03.htm':
                if ( player.getLevel() >= minimumLevel ) {
                    break
                }

                return

            case '32486-04.htm':
                if ( player.getLevel() >= minimumLevel ) {
                    state.startQuest()
                    break
                }

                return

            case '32486-07.html':
                await state.exitQuest( true, true )
                break

            case '32486-08.html':
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                return this.getPath( player.getLevel() >= minimumLevel ? '32486-01.htm' : '32486-02.html' )

            case QuestStateValues.STARTED:
                let count = QuestHelper.getQuestItemsCount( player, ENERGY_ORE )
                if ( count === 0 ) {
                    return this.getPath( '32486-05.html' )
                }

                let amount: number = ( count * 50 ) + ( count >= 10 ? 2044 : 0 )
                await QuestHelper.giveAdena( player, amount, true )
                await QuestHelper.takeSingleItem( player, ENERGY_ORE, -1 )

                return this.getPath( '32486-06.html' )
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}