import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import _ from 'lodash'

const MASTER_TERRY = 30064
const MASTER_DUFNER = 30106
const BLACKSMITH_BRUNON = 30526
const TRADER_VIKTOR = 30684
const MAGISTER_MARINA = 30715

const DUFNERS_LETTER = 2647
const TERRYS_1ST_ORDER = 2648
const TERRYS_2ND_ORDER = 2649
const TERRYS_LETTER = 2650
const VIKTORS_LETTER = 2651
const HAWKEYES_LETTER = 2652
const MYSTERIOUS_SPIRIT_ORE = 2653
const OL_MAHUM_SPIRIT_ORE = 2654
const TUREK_SPIRIT_ORE = 2655
const ANT_SPIRIT_ORE = 2656
const TURAK_BUGBEAR_SPIRIT_ORE = 2657
const TERRY_BOX = 2658
const VIKTORS_REQUEST = 2659
const MEDUSA_SCALES = 2660
const SHILENS_SPIRIT_ORE = 2661
const ANALYSIS_REQUEST = 2662
const MARINAS_LETTER = 2663
const EXPERIMENT_TOOLS = 2664
const ANALYSIS_RESULT = 2665
const TERRYS_3RD_ORDER = 2666
const LIST_OF_HOST = 2667
const ABYSS_SPIRIT_ORE1 = 2668
const ABYSS_SPIRIT_ORE2 = 2669
const ABYSS_SPIRIT_ORE3 = 2670
const ABYSS_SPIRIT_ORE4 = 2671
const TERRYS_REPORT = 2672

const MARK_OF_SEEKER = 2673
const DIMENSIONAL_DIAMOND = 7562

const ANT_CAPTAIN = 20080
const ANT_WARRIOR_CAPTAIN = 20088
const MEDUSA = 20158
const NEER_GHOUL_BERSERKER = 20198
const OL_MAHUM_CAPTAIN = 20211
const MARSH_STAKATO_DRONE = 20234
const TURAK_BUGBEAR_WARRIOR = 20249
const BREKA_ORC_OVERLORD = 20270
const TUREK_ORC_WARLORD = 20495
const LETO_LIZARDMAN_WARRIOR = 20580

const minimumLevel = 35
const maximumLevel = 36

export class TrialOfTheSeeker extends ListenerLogic {
    constructor() {
        super( 'Q00213_TrialOfTheSeeker', 'listeners/tracked-200/TrialOfTheSeeker.ts' )
        this.questId = 213
        this.questItemIds = [
            DUFNERS_LETTER,
            TERRYS_1ST_ORDER,
            TERRYS_2ND_ORDER,
            TERRYS_LETTER,
            VIKTORS_LETTER,
            HAWKEYES_LETTER,
            MYSTERIOUS_SPIRIT_ORE,
            OL_MAHUM_SPIRIT_ORE,
            TUREK_SPIRIT_ORE,
            ANT_SPIRIT_ORE,
            TURAK_BUGBEAR_SPIRIT_ORE,
            TERRY_BOX,
            VIKTORS_REQUEST,
            MEDUSA_SCALES,
            SHILENS_SPIRIT_ORE,
            ANALYSIS_REQUEST,
            MARINAS_LETTER,
            EXPERIMENT_TOOLS,
            ANALYSIS_RESULT,
            TERRYS_3RD_ORDER,
            LIST_OF_HOST,
            ABYSS_SPIRIT_ORE1,
            ABYSS_SPIRIT_ORE2,
            ABYSS_SPIRIT_ORE3,
            ABYSS_SPIRIT_ORE4,
            TERRYS_REPORT,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            ANT_CAPTAIN,
            ANT_WARRIOR_CAPTAIN,
            MEDUSA,
            NEER_GHOUL_BERSERKER,
            OL_MAHUM_CAPTAIN,
            MARSH_STAKATO_DRONE,
            TURAK_BUGBEAR_WARRIOR,
            BREKA_ORC_OVERLORD,
            TUREK_ORC_WARLORD,
            LETO_LIZARDMAN_WARRIOR,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00213_TrialOfTheSeeker'
    }

    getQuestStartIds(): Array<number> {
        return [ MASTER_DUFNER ]
    }

    getTalkIds(): Array<number> {
        return [
            MASTER_DUFNER,
            MASTER_TERRY,
            BLACKSMITH_BRUNON,
            TRADER_VIKTOR,
            MAGISTER_MARINA,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId )

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case ANT_CAPTAIN:
                if ( QuestHelper.hasQuestItem( player, TERRYS_2ND_ORDER ) && !QuestHelper.hasQuestItem( player, ANT_SPIRIT_ORE ) ) {
                    await QuestHelper.giveSingleItem( player, ANT_SPIRIT_ORE, 1 )

                    if ( QuestHelper.hasQuestItems( player, OL_MAHUM_SPIRIT_ORE, TUREK_SPIRIT_ORE, TURAK_BUGBEAR_SPIRIT_ORE ) ) {
                        state.setConditionWithSound( 5 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case ANT_WARRIOR_CAPTAIN:
                if ( QuestHelper.hasQuestItem( player, LIST_OF_HOST ) && !QuestHelper.hasQuestItem( player, ABYSS_SPIRIT_ORE3 ) ) {
                    await QuestHelper.giveSingleItem( player, ABYSS_SPIRIT_ORE3, 1 )

                    if ( QuestHelper.hasQuestItems( player, ABYSS_SPIRIT_ORE1, ABYSS_SPIRIT_ORE2, ABYSS_SPIRIT_ORE4 ) ) {
                        state.setConditionWithSound( 16 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case MEDUSA:
                if ( QuestHelper.hasQuestItem( player, VIKTORS_REQUEST ) && QuestHelper.getQuestItemsCount( player, MEDUSA_SCALES ) < 10 ) {
                    await QuestHelper.rewardSingleQuestItem( player, MEDUSA_SCALES, 1, data.isChampion )

                    if ( QuestHelper.getQuestItemsCount( player, MEDUSA_SCALES ) >= 10 ) {
                        state.setConditionWithSound( 10, true )

                        return
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case NEER_GHOUL_BERSERKER:
                if ( QuestHelper.hasQuestItem( player, TERRYS_1ST_ORDER ) && !QuestHelper.hasQuestItem( player, MYSTERIOUS_SPIRIT_ORE ) ) {
                    if ( _.random( 100 ) < QuestHelper.getAdjustedChance( MYSTERIOUS_SPIRIT_ORE, 50, data.isChampion ) ) {
                        await QuestHelper.giveSingleItem( player, MYSTERIOUS_SPIRIT_ORE, 1 )
                        state.setConditionWithSound( 3, true )
                    }
                }

                return

            case OL_MAHUM_CAPTAIN:
                if ( QuestHelper.hasQuestItem( player, TERRYS_2ND_ORDER ) && !QuestHelper.hasQuestItem( player, OL_MAHUM_SPIRIT_ORE ) ) {
                    await QuestHelper.giveSingleItem( player, OL_MAHUM_SPIRIT_ORE, 1 )

                    if ( QuestHelper.hasQuestItems( player, TUREK_SPIRIT_ORE, ANT_SPIRIT_ORE, TURAK_BUGBEAR_SPIRIT_ORE ) ) {
                        state.setConditionWithSound( 5 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case MARSH_STAKATO_DRONE:
                if ( QuestHelper.hasQuestItem( player, LIST_OF_HOST ) && !QuestHelper.hasQuestItem( player, ABYSS_SPIRIT_ORE1 ) ) {
                    await QuestHelper.giveSingleItem( player, ABYSS_SPIRIT_ORE1, 1 )

                    if ( QuestHelper.hasQuestItems( player, ABYSS_SPIRIT_ORE2, ABYSS_SPIRIT_ORE3, ABYSS_SPIRIT_ORE4 ) ) {
                        state.setConditionWithSound( 16 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case TURAK_BUGBEAR_WARRIOR:
                if ( QuestHelper.hasQuestItem( player, TERRYS_2ND_ORDER ) && !QuestHelper.hasQuestItem( player, TURAK_BUGBEAR_SPIRIT_ORE ) ) {
                    await QuestHelper.giveSingleItem( player, TURAK_BUGBEAR_SPIRIT_ORE, 1 )

                    if ( QuestHelper.hasQuestItems( player, OL_MAHUM_SPIRIT_ORE, TUREK_SPIRIT_ORE, ANT_SPIRIT_ORE ) ) {
                        state.setConditionWithSound( 5 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case BREKA_ORC_OVERLORD:
                if ( QuestHelper.hasQuestItem( player, LIST_OF_HOST ) && !QuestHelper.hasQuestItem( player, ABYSS_SPIRIT_ORE2 ) ) {
                    await QuestHelper.giveSingleItem( player, ABYSS_SPIRIT_ORE2, 1 )

                    if ( QuestHelper.hasQuestItems( player, ABYSS_SPIRIT_ORE1, ABYSS_SPIRIT_ORE3, ABYSS_SPIRIT_ORE4 ) ) {
                        state.setConditionWithSound( 16 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case TUREK_ORC_WARLORD:
                if ( QuestHelper.hasQuestItem( player, TERRYS_2ND_ORDER ) && !QuestHelper.hasQuestItem( player, TUREK_SPIRIT_ORE ) ) {
                    await QuestHelper.giveSingleItem( player, TUREK_SPIRIT_ORE, 1 )

                    if ( QuestHelper.hasQuestItems( player, OL_MAHUM_SPIRIT_ORE, ANT_SPIRIT_ORE, TURAK_BUGBEAR_SPIRIT_ORE ) ) {
                        state.setConditionWithSound( 5 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }

                return

            case LETO_LIZARDMAN_WARRIOR: {
                if ( QuestHelper.hasQuestItem( player, LIST_OF_HOST ) && !QuestHelper.hasQuestItem( player, ABYSS_SPIRIT_ORE4 ) ) {
                    await QuestHelper.giveSingleItem( player, ABYSS_SPIRIT_ORE4, 1 )

                    if ( QuestHelper.hasQuestItems( player, ABYSS_SPIRIT_ORE1, ABYSS_SPIRIT_ORE2, ABYSS_SPIRIT_ORE3 ) ) {
                        state.setConditionWithSound( 16 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                }
                return
            }
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()

                    if ( !QuestHelper.hasQuestItem( player, DUFNERS_LETTER ) ) {
                        await QuestHelper.giveSingleItem( player, DUFNERS_LETTER, 1 )
                    }

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 128 )

                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )
                        return this.getPath( '30106-05a.htm' )
                    }

                    return this.getPath( '30106-05.htm' )
                }

                return

            case '30106-04.htm':
            case '30064-02.html':
            case '30064-07.html':
            case '30064-16.html':
            case '30064-17.html':
            case '30064-19.html':
            case '30684-02.html':
            case '30684-03.html':
            case '30684-04.html':
            case '30684-06.html':
            case '30684-07.html':
            case '30684-08.html':
            case '30684-09.html':
            case '30684-10.html':
                break

            case '30064-03.html':
                if ( QuestHelper.hasQuestItem( player, DUFNERS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, DUFNERS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, TERRYS_1ST_ORDER, 1 )

                    state.setConditionWithSound( 2, true )
                    break
                }

                return

            case '30064-06.html':
                if ( QuestHelper.hasQuestItem( player, TERRYS_1ST_ORDER ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, MYSTERIOUS_SPIRIT_ORE, TERRYS_1ST_ORDER )
                    await QuestHelper.giveSingleItem( player, TERRYS_2ND_ORDER, 1 )

                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '30064-10.html':
                await QuestHelper.giveMultipleItems( player, 1, TERRYS_LETTER, TERRY_BOX )
                await QuestHelper.takeMultipleItems( player, 1, OL_MAHUM_SPIRIT_ORE, TUREK_SPIRIT_ORE, ANT_SPIRIT_ORE, TURAK_BUGBEAR_SPIRIT_ORE, TERRYS_2ND_ORDER )

                state.setConditionWithSound( 6, true )
                break

            case '30064-18.html':
                if ( QuestHelper.hasQuestItem( player, ANALYSIS_RESULT ) ) {
                    await QuestHelper.takeSingleItem( player, ANALYSIS_RESULT, 1 )
                    await QuestHelper.giveSingleItem( player, LIST_OF_HOST, 1 )

                    state.setConditionWithSound( 15, true )
                    break
                }

                return

            case '30684-05.html':
                if ( QuestHelper.hasQuestItem( player, TERRYS_LETTER ) ) {
                    await QuestHelper.takeSingleItem( player, TERRYS_LETTER, 1 )
                    await QuestHelper.giveSingleItem( player, VIKTORS_LETTER, 1 )

                    state.setConditionWithSound( 7, true )
                    break
                }

                return

            case '30684-11.html':
                await QuestHelper.takeMultipleItems( player, 1, TERRYS_LETTER, TERRY_BOX, HAWKEYES_LETTER, VIKTORS_LETTER, VIKTORS_REQUEST )

                state.setConditionWithSound( 9, true )
                break

            case '30684-15.html':
                await QuestHelper.takeMultipleItems( player, -1, VIKTORS_REQUEST, MEDUSA_SCALES )
                await QuestHelper.giveMultipleItems( player, 1, SHILENS_SPIRIT_ORE, ANALYSIS_REQUEST )

                state.setConditionWithSound( 11, true )
                break

            case '30715-02.html':
                await QuestHelper.takeMultipleItems( player, 1, SHILENS_SPIRIT_ORE, ANALYSIS_REQUEST )
                await QuestHelper.giveSingleItem( player, MARINAS_LETTER, 1 )

                state.setConditionWithSound( 12, true )
                break

            case '30715-05.html':
                await QuestHelper.takeSingleItem( player, EXPERIMENT_TOOLS, 1 )
                await QuestHelper.giveSingleItem( player, ANALYSIS_RESULT, 1 )

                state.setConditionWithSound( 14, true )
                break

            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === MASTER_DUFNER ) {
                    if ( [ ClassIdValues.rogue.id, ClassIdValues.elvenScout.id, ClassIdValues.assassin.id ].includes( player.getClassId() ) ) {
                        if ( player.getLevel() < minimumLevel ) {
                            return this.getPath( '30106-02.html' )
                        }
                            return this.getPath( '30106-03.htm' )

                    }
                        return this.getPath( '30106-01.html' )

                }

                break

            case QuestStateValues.STARTED:
                switch ( data.characterNpcId ) {
                    case MASTER_DUFNER:
                        if ( QuestHelper.hasQuestItem( player, DUFNERS_LETTER ) && !QuestHelper.hasQuestItem( player, TERRYS_REPORT ) ) {
                            return this.getPath( '30106-06.html' )
                        }

                        if ( !QuestHelper.hasAtLeastOneQuestItem( player, DUFNERS_LETTER, TERRYS_REPORT ) ) {
                            return this.getPath( '30106-07.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TERRYS_REPORT ) && !QuestHelper.hasQuestItem( player, DUFNERS_LETTER ) ) {
                            await QuestHelper.giveAdena( player, 187606, true )
                            await QuestHelper.giveSingleItem( player, MARK_OF_SEEKER, 1 )
                            await QuestHelper.addExpAndSp( player, 1029478, 66768 )
                            await state.exitQuest( false, true )

                            player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                            return this.getPath( '30106-08.html' )
                        }

                        break

                    case MASTER_TERRY:
                        if ( QuestHelper.hasQuestItem( player, DUFNERS_LETTER ) ) {
                            return this.getPath( '30064-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TERRYS_1ST_ORDER ) ) {
                            if ( !QuestHelper.hasQuestItem( player, MYSTERIOUS_SPIRIT_ORE ) ) {
                                return this.getPath( '30064-04.html' )
                            }
                                return this.getPath( '30064-05.html' )

                        }

                        if ( QuestHelper.hasQuestItem( player, TERRYS_2ND_ORDER ) ) {
                            if ( ( QuestHelper.getQuestItemsCount( player, OL_MAHUM_SPIRIT_ORE ) + QuestHelper.getQuestItemsCount( player, TUREK_SPIRIT_ORE ) + QuestHelper.getQuestItemsCount( player, ANT_SPIRIT_ORE ) + QuestHelper.getQuestItemsCount( player, TURAK_BUGBEAR_SPIRIT_ORE ) ) < 4 ) {
                                return this.getPath( '30064-08.html' )
                            }
                                return this.getPath( '30064-09.html' )

                        }

                        if ( QuestHelper.hasQuestItem( player, TERRYS_LETTER ) ) {
                            return this.getPath( '30064-11.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, VIKTORS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, VIKTORS_LETTER, 1 )
                            await QuestHelper.giveSingleItem( player, HAWKEYES_LETTER, 1 )

                            state.setConditionWithSound( 8, true )
                            return this.getPath( '30064-12.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, HAWKEYES_LETTER ) ) {
                            return this.getPath( '30064-13.html' )
                        }

                        if ( QuestHelper.hasAtLeastOneQuestItem( player, VIKTORS_REQUEST, ANALYSIS_REQUEST, MARINAS_LETTER, EXPERIMENT_TOOLS ) ) {
                            return this.getPath( '30064-14.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ANALYSIS_RESULT ) ) {
                            return this.getPath( '30064-15.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TERRYS_3RD_ORDER ) ) {
                            if ( player.getLevel() < maximumLevel ) {
                                return this.getPath( '30064-20.html' )
                            }

                            await QuestHelper.takeSingleItem( player, TERRYS_3RD_ORDER, 1 )
                            await QuestHelper.giveSingleItem( player, LIST_OF_HOST, 1 )

                            state.setConditionWithSound( 15, true )
                            return this.getPath( '30064-21.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, LIST_OF_HOST ) ) {
                            let totalCount: number = QuestHelper.getQuestItemsCount( player, ABYSS_SPIRIT_ORE1 ) +
                                    QuestHelper.getQuestItemsCount( player, ABYSS_SPIRIT_ORE2 ) +
                                    QuestHelper.getQuestItemsCount( player, ABYSS_SPIRIT_ORE3 ) +
                                    QuestHelper.getQuestItemsCount( player, ABYSS_SPIRIT_ORE4 )
                            if ( totalCount < 4 ) {
                                return this.getPath( '30064-22.html' )
                            }

                            await QuestHelper.takeMultipleItems( player, 1, LIST_OF_HOST, ABYSS_SPIRIT_ORE1, ABYSS_SPIRIT_ORE2, ABYSS_SPIRIT_ORE3, ABYSS_SPIRIT_ORE4 )
                            await QuestHelper.giveSingleItem( player, TERRYS_REPORT, 1 )

                            state.setConditionWithSound( 17, true )
                            return this.getPath( '30064-23.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, TERRYS_REPORT ) ) {
                            return this.getPath( '30064-24.html' )
                        }

                        break

                    case BLACKSMITH_BRUNON:
                        if ( QuestHelper.hasQuestItem( player, MARINAS_LETTER ) ) {
                            await QuestHelper.takeSingleItem( player, MARINAS_LETTER, 1 )
                            await QuestHelper.giveSingleItem( player, EXPERIMENT_TOOLS, 1 )

                            state.setConditionWithSound( 13, true )
                            return this.getPath( '30526-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, EXPERIMENT_TOOLS ) ) {
                            return this.getPath( '30526-02.html' )
                        }

                        break

                    case TRADER_VIKTOR:
                        if ( QuestHelper.hasQuestItem( player, TERRYS_LETTER ) ) {
                            return this.getPath( '30684-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, HAWKEYES_LETTER ) ) {
                            return this.getPath( '30684-12.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, VIKTORS_REQUEST ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, MEDUSA_SCALES ) < 10 ) {
                                return this.getPath( '30684-13.html' )
                            }

                            return this.getPath( '30684-14.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, SHILENS_SPIRIT_ORE, ANALYSIS_REQUEST ) ) {
                            return this.getPath( '30684-16.html' )
                        }

                        if ( QuestHelper.hasQuestItems( player, MARINAS_LETTER, EXPERIMENT_TOOLS, ANALYSIS_REQUEST, TERRYS_REPORT ) ) {
                            return this.getPath( '30684-17.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, VIKTORS_LETTER ) ) {
                            return this.getPath( '30684-05.html' )
                        }

                        break

                    case MAGISTER_MARINA:
                        if ( QuestHelper.hasQuestItems( player, SHILENS_SPIRIT_ORE, ANALYSIS_REQUEST ) ) {
                            return this.getPath( '30715-01.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, MARINAS_LETTER ) ) {
                            return this.getPath( '30715-03.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, EXPERIMENT_TOOLS ) ) {
                            return this.getPath( '30715-04.html' )
                        }

                        if ( QuestHelper.hasQuestItem( player, ANALYSIS_RESULT ) ) {
                            return this.getPath( '30715-06.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === MASTER_DUFNER ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}