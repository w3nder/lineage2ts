import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { AttackableKillEvent, NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateValues } from '../../gameService/models/quest/State'

import _ from 'lodash'

const VIRGIL = 31742
const KASSANDRA = 31743
const OGMAR = 31744
const FALLEN_UNICORN = 31746
const PURE_UNICORN = 31747
const CORNERSTONE = 31748
const MYSTERIOUS_KNIGHT = 31751
const ANGEL_CORPSE = 31752
const KALIS = 30759
const MATILD = 30738
const RESTRAINER_OF_GLORY = 27317
const VIRGILS_LETTER = 7677
const GOLDEN_HAIR = 7590
const ORB_OF_BINDING = 7595
const SORCERY_INGREDIENT = 7596
const CARADINE_LETTER = 7678
const CHANCE_FOR_HAIR = 20
const QUEST_COMMUNE_TO_SLATE = new SkillHolder( 4546 )
const minimumLevel = 60

const variableNames = {
    stones: 's',
}

const eventNames = {
    PURE_UNICORN: 'pu',
    FALLEN_UNICORN: 'fu',
}

export class PossessorOfAPreciousSoulPartTwo extends ListenerLogic {
    constructor() {
        super( 'Q00242_PossessorOfAPreciousSoulPartTwo', 'listeners/tracked-200/PossessorOfAPreciousSoulPartTwo.ts' )
        this.questId = 242
        this.questItemIds = [
            GOLDEN_HAIR,
            ORB_OF_BINDING,
            SORCERY_INGREDIENT,
        ]
    }

    getAttackableKillIds(): Array<number> {
        return [ RESTRAINER_OF_GLORY ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00242_PossessorOfAPreciousSoul2'
    }

    getQuestStartIds(): Array<number> {
        return [ VIRGIL ]
    }

    getTalkIds(): Array<number> {
        return [
            VIRGIL,
            KASSANDRA,
            OGMAR,
            MYSTERIOUS_KNIGHT,
            ANGEL_CORPSE,
            KALIS,
            MATILD,
            FALLEN_UNICORN,
            CORNERSTONE,
            PURE_UNICORN,
        ]
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let player: L2PcInstance = this.getRandomPartyMemberForCondition( L2World.getPlayer( data.playerId ), 9 )
        if ( !player ) {
            return
        }

        if ( QuestHelper.getQuestItemsCount( player, ORB_OF_BINDING ) < 4 ) {
            await QuestHelper.giveSingleItem( player, ORB_OF_BINDING, 1 )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        if ( !player.isSubClassActive() ) {
            return this.getPath( 'no_sub.html' )
        }

        switch ( data.eventName ) {
            case '31742-02.html':
                state.startQuest()
                await QuestHelper.takeSingleItem( player, VIRGILS_LETTER, -1 )
                break

            case '31743-05.html':
                if ( state.isCondition( 1 ) ) {
                    state.setConditionWithSound( 2, true )
                }

                break

            case '31744-02.html':
                if ( state.isCondition( 2 ) ) {
                    state.setConditionWithSound( 3, true )
                }

                break

            case '31751-02.html':
                if ( state.isCondition( 3 ) ) {
                    state.setConditionWithSound( 4, true )
                }

                break

            case '30759-02.html':
                if ( state.isCondition( 6 ) ) {
                    state.setConditionWithSound( 7, true )
                }

                break

            case '30738-02.html':
                if ( state.isCondition( 7 ) ) {
                    state.setConditionWithSound( 8, true )
                    await QuestHelper.giveSingleItem( player, SORCERY_INGREDIENT, 1 )
                }

                break

            case '30759-05.html':
                if ( state.isCondition( 8 ) ) {
                    await QuestHelper.takeMultipleItems( player, -1, GOLDEN_HAIR, SORCERY_INGREDIENT )

                    state.setConditionWithSound( 9, true )
                    state.setVariable( variableNames.stones, 0 )
                }

                break

            case eventNames.PURE_UNICORN:
                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                await npc.deleteMe()

                let unicornPure: L2Npc = QuestHelper.addGenericSpawn( null, PURE_UNICORN, 85884, -76588, -3470, 30000 )
                this.startQuestTimer( eventNames.FALLEN_UNICORN, 30000, unicornPure.getObjectId(), data.playerId )
                return

            case eventNames.FALLEN_UNICORN:
                QuestHelper.addGenericSpawn( null, FALLEN_UNICORN, 85884, -76588, -3470, 0 )
                return
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        if ( state.isStarted() && !player.isSubClassActive() ) {
            return this.getPath( 'no_sub.html' )
        }

        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.characterNpcId ) {
            case VIRGIL:
                switch ( state.getState() ) {
                    case QuestStateValues.CREATED:
                        if ( !player.hasQuestCompleted( 'Q00241_PossessorOfAPreciousSoulPartOne' ) ) {
                            break
                        }

                        return this.getPath( player.getLevel() >= minimumLevel ? '31742-01.htm' : '31742-00.htm' )

                    case QuestStateValues.STARTED:
                        switch ( state.getCondition() ) {
                            case 1:
                                return this.getPath( '31742-03.html' )

                            case 11:
                                await QuestHelper.giveSingleItem( player, CARADINE_LETTER, 1 )
                                await QuestHelper.addExpAndSp( player, 455764, 0 )
                                await state.exitQuest( false, true )
                                return this.getPath( '31742-04.html' )
                        }

                        break

                    case QuestStateValues.COMPLETED:
                        return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break

            case KASSANDRA:
                switch ( state.getCondition() ) {
                    case 1:
                        return this.getPath( '31743-01.html' )

                    case 2:
                        return this.getPath( '31743-06.html' )

                    case 11:
                        return this.getPath( '31743-07.html' )
                }

                break

            case OGMAR:
                switch ( state.getCondition() ) {
                    case 2:
                        return this.getPath( '31744-01.html' )

                    case 3:
                        return this.getPath( '31744-03.html' )
                }

                break

            case MYSTERIOUS_KNIGHT:
                switch ( state.getCondition() ) {
                    case 3:
                        return this.getPath( '31751-01.html' )

                    case 4:
                        return this.getPath( '31751-03.html' )

                    case 5:
                        if ( QuestHelper.hasQuestItem( player, GOLDEN_HAIR ) ) {
                            state.setConditionWithSound( 6, true )

                            return this.getPath( '31751-04.html' )
                        }

                        break

                    case 6:
                        return this.getPath( '31751-05.html' )
                }

                break

            case ANGEL_CORPSE:
                switch ( state.getCondition() ) {
                    case 4:
                        await npc.doDie( npc )

                        if ( CHANCE_FOR_HAIR >= _.random( 100 ) ) {
                            await QuestHelper.giveSingleItem( player, GOLDEN_HAIR, 1 )
                            state.setConditionWithSound( 5, true )
                            return this.getPath( '31752-01.html' )
                        }

                        return this.getPath( '31752-02.html' )

                    case 5:
                        return this.getPath( '31752-02.html' )
                }

                break

            case KALIS:
                switch ( state.getCondition() ) {
                    case 6:
                        return this.getPath( '30759-01.html' )

                    case 7:
                        return this.getPath( '30759-03.html' )

                    case 8:
                        if ( QuestHelper.hasQuestItem( player, SORCERY_INGREDIENT ) ) {
                            return this.getPath( '30759-04.html' )
                        }

                        break

                    case 9:
                        return this.getPath( '30759-06.html' )
                }

                break

            case MATILD:
                switch ( state.getCondition() ) {
                    case 7:
                        return this.getPath( '30738-01.html' )

                    case 8:
                        return this.getPath( '30738-03.html' )
                }

                break

            case CORNERSTONE:
                if ( !state.isCondition( 9 ) ) {
                    break
                }

                if ( !QuestHelper.hasQuestItem( player, ORB_OF_BINDING ) ) {
                    return this.getPath( '31748-01.html' )
                }


                await QuestHelper.takeSingleItem( player, ORB_OF_BINDING, 1 )

                let stoneCount = _.defaultTo( state.getVariable( variableNames.stones ), 0 ) + 1
                state.setVariable( variableNames.stones, stoneCount )

                if ( stoneCount >= 4 ) {
                    state.setConditionWithSound( 10, true )
                }

                npc.setTarget( player )
                await npc.doCastWithHolder( QUEST_COMMUNE_TO_SLATE )
                await npc.doDie( npc )

                return this.getPath( '31748-02.html' )

            case FALLEN_UNICORN:
                switch ( state.getCondition() ) {
                    case 9:
                        return this.getPath( '31746-01.html' )

                    case 10:
                        this.startQuestTimer( eventNames.PURE_UNICORN, 3000, data.characterId, data.playerId )
                        return this.getPath( '31746-02.html' )
                }

                break

            case PURE_UNICORN:
                switch ( state.getCondition() ) {
                    case 10:
                        state.setConditionWithSound( 11, true )
                        return this.getPath( '31747-01.html' )

                    case 11:
                        return this.getPath( '31747-02.html' )
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}