import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcSpawnEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import { QuestVariables } from '../helpers/QuestVariables'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { SocialAction, SocialActionType } from '../../gameService/packets/send/SocialAction'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const PRIESTESS_PUPINA = 30118
const PREACHER_SLA = 30666
const RAMUS = 30667
const KATARI = 30668
const KAKAN = 30669
const NYAKURI = 30670
const OL_MAHUM_PILGRIM = 30732

const BOOK_OF_REFORM = 2822
const LETTER_OF_INTRODUCTION = 2823
const SLAS_LETTER = 2824
const GREETINGS = 2825
const Ol_MAHUM_MONEY = 2826
const KATARIS_LETTER = 2827
const NYAKURIS_LETTER = 2828
const UNDEAD_LIST = 2829
const RAMUSS_LETTER = 2830
const RIPPED_DIARY = 2831
const HUGE_NAIL = 2832
const LETTER_OF_BETRAYER = 2833
const BONE_FRAGMENT4 = 2834
const BONE_FRAGMENT5 = 2835
const BONE_FRAGMENT6 = 2836
const BONE_FRAGMENT7 = 2837
const BONE_FRAGMENT8 = 2838
const KAKANS_LETTER = 3037
const LETTER_GREETINGS1 = 5567
const LETTER_GREETINGS2 = 5568

const MARK_OF_REFORMER = 2821
const DIMENSIONAL_DIAMOND = 7562

const MISERY_SKELETON = 20022
const SKELETON_ARCHER = 20100
const SKELETON_MARKSMAN = 20102
const SKELETON_LORD = 20104
const SILENT_HORROR = 20404

const NAMELESS_REVENANT = 27099
const ARURAUNE = 27128
const OL_MAHUM_INSPECTOR = 27129
const OL_MAHUM_BETRAYER = 27130
const CRIMSON_WEREWOLF = 27131
const KRUDEL_LIZARDMAN = 27132

const DISRUPT_UNDEAD = 1031
const SLEEP = 1069
const VAMPIRIC_TOUCH = 1147
const CURSE_WEAKNESS = 1164
const CURSE_POISON = 1168
const WIND_STRIKE = 1177
const ICE_BOLD = 1184
const DRYAD_ROOT = 1201
const WIND_SHACKLE = 1206
const availableSkills: Array<number> = [
    DISRUPT_UNDEAD,
    SLEEP,
    VAMPIRIC_TOUCH,
    CURSE_WEAKNESS,
    CURSE_POISON,
    WIND_STRIKE,
    ICE_BOLD,
    DRYAD_ROOT,
    WIND_SHACKLE,
]

const movementDestination = new Location( 36787, -3709, 10000 )
const minimumLevel = 39
const eventNames = {
    despawn: 'despawn',
}

export class TestOfTheReformer extends ListenerLogic {
    constructor() {
        super( 'Q00227_TestOfTheReformer', 'listeners/tracked-200/TestOfTheReformer.ts' )
        this.questId = 227
        this.questItemIds = [
            BOOK_OF_REFORM,
            LETTER_OF_INTRODUCTION,
            SLAS_LETTER,
            GREETINGS,
            Ol_MAHUM_MONEY,
            KATARIS_LETTER,
            NYAKURIS_LETTER,
            UNDEAD_LIST,
            RAMUSS_LETTER,
            RAMUSS_LETTER,
            RIPPED_DIARY,
            HUGE_NAIL,
            LETTER_OF_BETRAYER,
            BONE_FRAGMENT4,
            BONE_FRAGMENT5,
            BONE_FRAGMENT6,
            BONE_FRAGMENT7,
            BONE_FRAGMENT8,
            KAKANS_LETTER,
            LETTER_GREETINGS1,
            LETTER_GREETINGS2,
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [ NAMELESS_REVENANT, CRIMSON_WEREWOLF ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            MISERY_SKELETON,
            SKELETON_ARCHER,
            SKELETON_MARKSMAN,
            SKELETON_LORD,
            SILENT_HORROR,
            NAMELESS_REVENANT,
            ARURAUNE,
            OL_MAHUM_INSPECTOR,
            OL_MAHUM_BETRAYER,
            CRIMSON_WEREWOLF,
            KRUDEL_LIZARDMAN,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00227_TestOfTheReformer'
    }

    getQuestStartIds(): Array<number> {
        return [ PRIESTESS_PUPINA ]
    }

    getSpawnIds(): Array<number> {
        return [
            OL_MAHUM_PILGRIM,
            OL_MAHUM_INSPECTOR,
            OL_MAHUM_BETRAYER,
            CRIMSON_WEREWOLF,
            KRUDEL_LIZARDMAN,
        ]
    }

    getTalkIds(): Array<number> {
        return [
            PRIESTESS_PUPINA,
            PREACHER_SLA,
            RAMUS,
            KATARI,
            KAKAN,
            NYAKURI,
            OL_MAHUM_PILGRIM,
        ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        switch ( data.targetNpcId ) {
            case NAMELESS_REVENANT:
                NpcVariablesManager.set( data.targetId, this.getName(), data.skillId === DISRUPT_UNDEAD ? 1 : 2 )
                return

            case CRIMSON_WEREWOLF:
                if ( availableSkills.includes( data.skillId ) ) {
                    let npc = L2World.getObjectById( data.targetId ) as L2Npc
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.COWARDLY_GUY )
                    await npc.deleteMe()
                }

                NpcVariablesManager.set( data.targetId, this.getName(), data.attackerPlayerId )
                return
        }
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state || !state.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        if ( !GeneralHelper.checkIfInRange( 1500, player, npc, true ) ) {
            return
        }

        switch ( data.npcId ) {
            case MISERY_SKELETON:
                if ( state.isMemoState( 16 ) && !QuestHelper.hasQuestItem( player, BONE_FRAGMENT7 ) ) {
                    await QuestHelper.giveSingleItem( player, BONE_FRAGMENT7, 1 )
                    this.tryToProgressQuest( player, state )
                }

                return

            case SKELETON_ARCHER:
                if ( state.isMemoState( 16 ) && !QuestHelper.hasQuestItem( player, BONE_FRAGMENT8 ) ) {
                    await QuestHelper.giveSingleItem( player, BONE_FRAGMENT8, 1 )
                    this.tryToProgressQuest( player, state )
                }

                return

            case SKELETON_MARKSMAN:
                if ( state.isMemoState( 16 ) && !QuestHelper.hasQuestItem( player, BONE_FRAGMENT6 ) ) {
                    await QuestHelper.giveSingleItem( player, BONE_FRAGMENT6, 1 )
                    this.tryToProgressQuest( player, state )
                }

                return

            case SKELETON_LORD:
                if ( state.isMemoState( 16 ) && !QuestHelper.hasQuestItem( player, BONE_FRAGMENT5 ) ) {
                    await QuestHelper.giveSingleItem( player, BONE_FRAGMENT5, 1 )
                    this.tryToProgressQuest( player, state )
                }

                return

            case SILENT_HORROR:
                if ( state.isMemoState( 16 ) && !QuestHelper.hasQuestItem( player, BONE_FRAGMENT4 ) ) {
                    await QuestHelper.giveSingleItem( player, BONE_FRAGMENT4, 1 )
                    this.tryToProgressQuest( player, state )
                }

                return

            case NAMELESS_REVENANT:
                if ( state.isMemoState( 1 )
                        && NpcVariablesManager.get( data.targetId, this.getName() ) === 1
                        && !QuestHelper.hasQuestItem( player, HUGE_NAIL )
                        && QuestHelper.hasQuestItems( player, BOOK_OF_REFORM, RIPPED_DIARY ) ) {

                    if ( QuestHelper.getQuestItemsCount( player, RIPPED_DIARY ) >= 6 ) {
                        QuestHelper.addSpawnAtLocation( ARURAUNE, npc, true, 0 )
                        await QuestHelper.takeSingleItem( player, RIPPED_DIARY, -1 )

                        state.setConditionWithSound( 2 )
                        return
                    }

                    await QuestHelper.rewardSingleQuestItem( player, RIPPED_DIARY, 1, data.isChampion )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case ARURAUNE:
                if ( !QuestHelper.hasQuestItem( player, HUGE_NAIL ) ) {
                    BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, NpcStringIds.THE_CONCEALED_TRUTH_WILL_ALWAYS_BE_REVEALED )
                    await QuestHelper.giveSingleItem( player, HUGE_NAIL, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    state.setMemoState( 3 )
                    state.setConditionWithSound( 3 )
                }

                return

            case OL_MAHUM_INSPECTOR:
                if ( state.isMemoState( 6 ) ) {
                    state.setMemoState( 7 )
                    state.setConditionWithSound( 7, true )
                }

                return

            case OL_MAHUM_BETRAYER:
                if ( state.isMemoState( 8 ) ) {
                    state.setMemoState( 9 )
                    state.setConditionWithSound( 9 )
                    await QuestHelper.giveSingleItem( player, LETTER_OF_BETRAYER, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                }

                return

            case CRIMSON_WEREWOLF:
                if ( NpcVariablesManager.get( data.targetId, this.getName() ) === ( player.getObjectId() )
                        && state.isMemoState( 11 ) ) {
                    state.setMemoState( 12 )
                    state.setConditionWithSound( 13, true )
                }

                return

            case KRUDEL_LIZARDMAN:
                if ( state.isMemoState( 13 ) ) {
                    state.setMemoState( 14 )
                    state.setConditionWithSound( 16, true )
                }

                return
        }
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( eventNames.despawn === data.eventName ) {
            let SPAWNED = _.defaultTo( NpcVariablesManager.get( data.characterId, this.getName() ), 0 ) as number
            if ( SPAWNED < 60 ) {
                NpcVariablesManager.set( data.characterId, this.getName(), SPAWNED + 1 )
                return
            }

            let npc = L2World.getObjectById( data.characterId ) as L2Npc
            await npc.deleteMe()

            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case 'ACCEPT':
                if ( state.isCreated() ) {
                    state.startQuest()
                    state.setMemoState( 1 )

                    await QuestHelper.giveSingleItem( player, BOOK_OF_REFORM, 1 )
                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )

                    if ( !PlayerVariablesManager.get( data.playerId, QuestVariables.secondClassDiamondReward ) ) {
                        PlayerVariablesManager.set( player.getObjectId(), QuestVariables.secondClassDiamondReward, true )

                        await QuestHelper.rewardSingleItem( player, DIMENSIONAL_DIAMOND, 60 )

                        return this.getPath( '30118-04b.htm' )
                    }

                    return this.getPath( '30118-04.htm' )
                }
                return

            case '30118-06.html':
                if ( QuestHelper.hasQuestItem( player, BOOK_OF_REFORM ) ) {
                    await QuestHelper.takeMultipleItems( player, 1, BOOK_OF_REFORM, HUGE_NAIL )
                    await QuestHelper.giveSingleItem( player, LETTER_OF_INTRODUCTION, 1 )

                    state.setMemoState( 4 )
                    state.setConditionWithSound( 4, true )
                    break
                }

                return

            case '30666-02.html':
            case '30666-03.html':
            case '30669-02.html':
            case '30669-05.html':
            case '30670-02.html':
                break

            case '30666-04.html':
                await QuestHelper.takeSingleItem( player, LETTER_OF_INTRODUCTION, 1 )
                await QuestHelper.giveSingleItem( player, SLAS_LETTER, 1 )

                state.setMemoState( 5 )
                state.setConditionWithSound( 5, true )
                break

            case '30669-03.html':
                state.setConditionWithSound( 12, true )
                if ( npc.getSummonedNpcCount() < 1 ) {
                    let pilgrim: L2Npc = QuestHelper.addGenericSpawn( null, OL_MAHUM_PILGRIM, -9282, -89975, -2331, 0, false, 0 )
                    let wolf: L2Npc = QuestHelper.addGenericSpawn( null, CRIMSON_WEREWOLF, -9382, -89852, -2333, 0, false, 0 )

                    AIEffectHelper.notifyAttacked( wolf, pilgrim, 99999 )
                }

                break

            case '30670-03.html':
                state.setConditionWithSound( 15, true )
                if ( npc.getSummonedNpcCount() < 1 ) {
                    let pilgrim: L2Npc = QuestHelper.addGenericSpawn( null, OL_MAHUM_PILGRIM, 125947, -180049, -1778, 0, false, 0 )
                    let lizard: L2Npc = QuestHelper.addGenericSpawn( null, KRUDEL_LIZARDMAN, 126019, -179983, -1781, 0, false, 0 )

                    AIEffectHelper.notifyAttacked( lizard, pilgrim, 99999 )
                }

                break


            default:
                return
        }

        return this.getPath( data.eventName )
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        switch ( data.npcId ) {
            case OL_MAHUM_INSPECTOR:
            case CRIMSON_WEREWOLF:
            case KRUDEL_LIZARDMAN:
            case OL_MAHUM_PILGRIM:
                this.startQuestTimer( eventNames.despawn, 5000, data.characterId, 0, true )
                return

            case OL_MAHUM_BETRAYER:
                this.startQuestTimer( eventNames.despawn, 5000, data.characterId, 0, true )

                let npc = L2World.getObjectById( data.characterId ) as L2Npc
                AIEffectHelper.notifyMove( npc, movementDestination )
                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === PRIESTESS_PUPINA ) {
                    if ( [ ClassIdValues.cleric.id, ClassIdValues.shillienOracle.id ].includes( player.getClassId() ) ) {
                        if ( player.getLevel() >= minimumLevel ) {
                            return this.getPath( '30118-03.htm' )
                        }
                            return this.getPath( '30118-01.html' )

                    }
                        return this.getPath( '30118-02.html' )

                }

                break

            case QuestStateValues.STARTED:
                let memoState = state.getMemoState()
                switch ( data.characterNpcId ) {
                    case PRIESTESS_PUPINA:
                        switch ( memoState ) {
                            case 0:
                                break

                            case 1:
                            case 2:
                                return this.getPath( '30118-04a.html' )

                            case 3:
                                if ( QuestHelper.hasQuestItem( player, HUGE_NAIL ) ) {
                                    return this.getPath( '30118-05.html' )
                                }

                                break

                            default:
                                return this.getPath( '30118-07.html' )
                        }

                        break

                    case PREACHER_SLA:
                        switch ( memoState ) {
                            case 4:
                                if ( QuestHelper.hasQuestItem( player, LETTER_OF_INTRODUCTION ) ) {
                                    return this.getPath( '30666-01.html' )
                                }

                                break

                            case 5:
                                if ( QuestHelper.hasQuestItem( player, SLAS_LETTER ) ) {
                                    return this.getPath( '30666-05.html' )
                                }

                                break

                            case 10:
                                await QuestHelper.giveMultipleItems( player, 1, GREETINGS, LETTER_GREETINGS1, LETTER_GREETINGS2 )

                                state.setMemoState( 11 )
                                state.setConditionWithSound( 11, true )

                                if ( QuestHelper.hasQuestItem( player, Ol_MAHUM_MONEY ) ) {
                                    await QuestHelper.takeSingleItem( player, Ol_MAHUM_MONEY, 1 )
                                    return this.getPath( '30666-06.html' )
                                }

                                return this.getPath( '30666-06a.html' )

                            case 11:
                            case 12:
                            case 13:
                            case 14:
                            case 15:
                            case 16:
                            case 17:
                                return this.getPath( '30666-06b.html' )

                            case 18:
                                if ( QuestHelper.hasQuestItems( player, KATARIS_LETTER, KAKANS_LETTER, NYAKURIS_LETTER, RAMUSS_LETTER ) ) {
                                    await QuestHelper.giveAdena( player, 226528, true )
                                    await QuestHelper.giveSingleItem( player, MARK_OF_REFORMER, 1 )
                                    await QuestHelper.addExpAndSp( player, 1252844, 85972 )
                                    await state.exitQuest( false, true )

                                    player.sendOwnedData( SocialAction( data.playerId, SocialActionType.Congratulate ) )
                                    return this.getPath( '30666-07.html' )
                                }

                                break
                        }

                        break

                    case RAMUS:
                        switch ( memoState ) {
                            case 15:
                                if ( QuestHelper.hasQuestItem( player, LETTER_GREETINGS2 ) && !QuestHelper.hasQuestItem( player, UNDEAD_LIST ) ) {
                                    await QuestHelper.giveSingleItem( player, UNDEAD_LIST, 1 )
                                    await QuestHelper.takeSingleItem( player, LETTER_GREETINGS2, 1 )

                                    state.setMemoState( 16 )
                                    state.setConditionWithSound( 18, true )

                                    return this.getPath( '30667-01.html' )
                                }

                                break

                            case 16:
                                return this.getPath( '30667-02.html' )

                            case 17:
                                if ( QuestHelper.hasQuestItems( player, UNDEAD_LIST, BONE_FRAGMENT4, BONE_FRAGMENT5, BONE_FRAGMENT6, BONE_FRAGMENT7, BONE_FRAGMENT8 ) ) {
                                    await QuestHelper.giveSingleItem( player, RAMUSS_LETTER, 1 )
                                    await QuestHelper.takeMultipleItems( player, 1, UNDEAD_LIST, BONE_FRAGMENT4, BONE_FRAGMENT5, BONE_FRAGMENT6, BONE_FRAGMENT7, BONE_FRAGMENT8 )

                                    state.setMemoState( 18 )
                                    state.setConditionWithSound( 20, true )

                                    return this.getPath( '30667-03.html' )
                                }

                                break
                        }

                        break

                    case KATARI:
                        let npc = L2World.getObjectById( data.characterId ) as L2Npc

                        switch ( memoState ) {
                            case 5:
                            case 6:
                                await QuestHelper.takeSingleItem( player, SLAS_LETTER, 1 )

                                state.setMemoState( 6 )
                                state.setConditionWithSound( 6, true )

                                if ( npc.getSummonedNpcCount() < 1 ) {
                                    let pilgrim: L2Npc = QuestHelper.addGenericSpawn( null, OL_MAHUM_PILGRIM, -4015, 40141, -3664, 0, false, 0 )
                                    let inspector: L2Npc = QuestHelper.addGenericSpawn( null, OL_MAHUM_INSPECTOR, -4034, 40201, -3665, 0, false, 0 )

                                    AIEffectHelper.notifyAttacked( inspector, pilgrim, 99999 )
                                }

                                return this.getPath( '30668-01.html' )

                            case 7:
                            case 8:
                                state.setMemoState( 8 )
                                state.setConditionWithSound( 8, true )

                                if ( npc.getSummonedNpcCount() < 3 ) {
                                    QuestHelper.addGenericSpawn( null, OL_MAHUM_BETRAYER, -4106, 40174, -3660, 0, false, 0 )
                                }

                                return this.getPath( '30668-02.html' )

                            case 9:
                                if ( QuestHelper.hasQuestItem( player, LETTER_OF_BETRAYER ) ) {
                                    await QuestHelper.giveSingleItem( player, KATARIS_LETTER, 1 )
                                    await QuestHelper.takeSingleItem( player, LETTER_OF_BETRAYER, 1 )

                                    state.setMemoState( 10 )
                                    state.setConditionWithSound( 10, true )

                                    return this.getPath( '30668-03.html' )
                                }

                                break
                        }

                        if ( memoState >= 10 ) {
                            return this.getPath( '30668-04.html' )
                        }

                        break

                    case KAKAN:
                        switch ( memoState ) {
                            case 11:
                                if ( QuestHelper.hasQuestItem( player, GREETINGS ) ) {
                                    return this.getPath( '30669-01.html' )
                                }

                                break

                            case 12:
                                if ( QuestHelper.hasQuestItem( player, GREETINGS ) && !QuestHelper.hasQuestItem( player, KAKANS_LETTER ) ) {
                                    await QuestHelper.takeSingleItem( player, GREETINGS, 1 )
                                    await QuestHelper.giveSingleItem( player, KAKANS_LETTER, 1 )

                                    state.setMemoState( 13 )
                                    state.setConditionWithSound( 14, true )

                                    return this.getPath( '30669-04.html' )
                                }

                                break
                        }

                        break

                    case NYAKURI:
                        switch ( memoState ) {
                            case 13:
                                if ( QuestHelper.hasQuestItem( player, LETTER_GREETINGS1 ) ) {
                                    return this.getPath( '30670-01.html' )
                                }

                                break

                            case 14:
                                if ( QuestHelper.hasQuestItem( player, LETTER_GREETINGS1 )
                                        && !QuestHelper.hasQuestItem( player, NYAKURIS_LETTER ) ) {
                                    await QuestHelper.giveSingleItem( player, NYAKURIS_LETTER, 1 )
                                    await QuestHelper.takeSingleItem( player, LETTER_GREETINGS1, 1 )

                                    state.setMemoState( 15 )
                                    state.setConditionWithSound( 17, true )

                                    return this.getPath( '30670-04.html' )
                                }

                                break
                        }

                        break

                    case OL_MAHUM_PILGRIM:
                        if ( memoState === 7 ) {
                            await QuestHelper.giveSingleItem( player, Ol_MAHUM_MONEY, 1 )
                            state.setMemoState( 8 )

                            return this.getPath( '30732-01.html' )
                        }

                        break
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === PRIESTESS_PUPINA ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }

    tryToProgressQuest( player: L2PcInstance, state: QuestState ): void {
        if ( QuestHelper.hasQuestItems( player, BONE_FRAGMENT4, BONE_FRAGMENT5, BONE_FRAGMENT6, BONE_FRAGMENT7, BONE_FRAGMENT8 ) ) {
            state.setMemoState( 17 )
            state.setConditionWithSound( 19 )
        }

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
    }
}