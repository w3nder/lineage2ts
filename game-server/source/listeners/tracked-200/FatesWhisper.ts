import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcSpawnEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { PacketDispatcher } from '../../gameService/PacketDispatcher'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const ZENKIN = 30178
const CLIFF = 30182
const MASTER_KASPAR = 30833
const HEAD_BLACKSMITH_FERRIS = 30847
const MAESTRO_LEORIN = 31002
const COFFER_OF_THE_DEAD = 31027
const CHEST_OF_KERNON = 31028
const CHEST_OF_GOLKONDA = 31029
const CHEST_OF_HALLATE = 31030

const Q_BLOODY_FABRIC_Q0234 = 14361
const Q_WHITE_FABRIC_Q0234 = 14362
const Q_STAR_OF_DESTINY = 5011
const Q_PIPETTE_KNIFE = 4665
const Q_REIRIAS_SOULORB = 4666
const Q_INFERNIUM_SCEPTER_1 = 4667
const Q_INFERNIUM_SCEPTER_2 = 4668
const Q_INFERNIUM_SCEPTER_3 = 4669
const Q_MAESTRO_REORINS_HAMMER = 4670
const Q_MAESTRO_REORINS_MOLD = 4671
const Q_INFERNIUM_VARNISH = 4672
const Q_RED_PIPETTE_KNIFE = 4673

const CRYSTAL_B = 1460

const PLATINUM_TRIBE_GRUNT = 20823
const PLATINUM_TRIBE_ARCHER = 20826
const PLATINUM_TRIBE_WARRIOR = 20827
const PLATINUM_TRIBE_SHAMAN = 20828
const PLATINUM_TRIBE_LORD = 20829
const GUARDIAN_ANGEL = 20830
const SEAL_ANGEL = 20831
const SEAL_ANGEL_R = 20860

const DOMB_DEATH_CABRIO = 25035
const KERNON = 25054
const GOLKONDA_LONGHORN = 25126
const HALLATE_THE_DEATH_LORD = 25220
const BAIUM = 29020

const SWORD_OF_DAMASCUS = 79
const SWORD_OF_DAMASCUS_FOCUS = 4717
const SWORD_OF_DAMASCUS_CRT_DAMAGE = 4718
const SWORD_OF_DAMASCUS_HASTE = 4719
const HAZARD_BOW = 287
const HAZARD_BOW_GUIDENCE = 4828
const HAZARD_BOW_QUICKRECOVERY = 4829
const HAZARD_BOW_CHEAPSHOT = 4830
const LANCIA = 97
const LANCIA_ANGER = 4858
const LANCIA_CRT_STUN = 4859
const LANCIA_LONGBLOW = 4860
const ART_OF_BATTLE_AXE = 175
const ART_OF_BATTLE_AXE_HEALTH = 4753
const ART_OF_BATTLE_AXE_RSK_FOCUS = 4754
const ART_OF_BATTLE_AXE_HASTE = 4755
const STAFF_OF_EVIL_SPRIT = 210
const STAFF_OF_EVIL_SPRIT_MAGICFOCUS = 4900
const STAFF_OF_EVIL_SPRIT_MAGICBLESSTHEBODY = 4901
const STAFF_OF_EVIL_SPRIT_MAGICPOISON = 4902
const DEMONS_SWORD = 234
const DEMONS_SWORD_CRT_BLEED = 4780
const DEMONS_SWORD_CRT_POISON = 4781
const DEMONS_SWORD_MIGHTMOTAL = 4782
const BELLION_CESTUS = 268
const BELLION_CESTUS_CRT_DRAIN = 4804
const BELLION_CESTUS_CRT_POISON = 4805
const BELLION_CESTUS_RSK_HASTE = 4806
const DEADMANS_GLORY = 171
const DEADMANS_GLORY_ANGER = 4750
const DEADMANS_GLORY_HEALTH = 4751
const DEADMANS_GLORY_HASTE = 4752
const SAMURAI_LONGSWORD_SAMURAI_LONGSWORD = 2626
const GUARDIANS_SWORD = 7883
const GUARDIANS_SWORD_CRT_DRAIN = 8105
const GUARDIANS_SWORD_HEALTH = 8106
const GUARDIANS_SWORD_CRT_BLEED = 8107
const TEARS_OF_WIZARD = 7889
const TEARS_OF_WIZARD_ACUMEN = 8117
const TEARS_OF_WIZARD_MAGICPOWER = 8118
const TEARS_OF_WIZARD_UPDOWN = 8119
const STAR_BUSTER = 7901
const STAR_BUSTER_HEALTH = 8132
const STAR_BUSTER_HASTE = 8133
const STAR_BUSTER_RSK_FOCUS = 8134
const BONE_OF_KAIM_VANUL = 7893
const BONE_OF_KAIM_VANUL_MANAUP = 8144
const BONE_OF_KAIM_VANUL_MAGICSILENCE = 8145
const BONE_OF_KAIM_VANUL_UPDOWN = 8146

const TALLUM_BLADE = 80
const CARNIUM_BOW = 288
const HALBARD = 98
const ELEMENTAL_SWORD = 150
const DASPARIONS_STAFF = 212
const BLOODY_ORCHID = 235
const BLOOD_TORNADO = 269
const METEOR_SHOWER = 2504
const KSHANBERK_KSHANBERK = 5233
const INFERNO_MASTER = 7884
const EYE_OF_SOUL = 7894
const HAMMER_OF_DESTROYER = 7899

const eventNames = {
    COFFER_OF_THE_DEAD: COFFER_OF_THE_DEAD.toString(),
    CHEST_OF_KERNON: CHEST_OF_KERNON.toString(),
    CHEST_OF_HALLATE: CHEST_OF_HALLATE.toString(),
    CHEST_OF_GOLKONDA: CHEST_OF_GOLKONDA.toString(),
}

export class FatesWhisper extends ListenerLogic {
    constructor() {
        super( 'Q00234_FatesWhisper', 'listeners/tracked-200/FatesWhisper.ts' )
        this.questId = 234
        this.questItemIds = [
            Q_BLOODY_FABRIC_Q0234,
            Q_WHITE_FABRIC_Q0234,
            Q_PIPETTE_KNIFE,
            Q_REIRIAS_SOULORB,
            Q_INFERNIUM_SCEPTER_1,
            Q_INFERNIUM_SCEPTER_2,
            Q_INFERNIUM_SCEPTER_3,
            Q_MAESTRO_REORINS_HAMMER,
            Q_MAESTRO_REORINS_MOLD,
            Q_INFERNIUM_VARNISH,
            Q_RED_PIPETTE_KNIFE,
        ]
    }

    async applyReward( state: QuestState, player: L2PcInstance, itemId: number, ...requiredItemIds: Array<number> ): Promise<boolean> {
        if ( !QuestHelper.hasAtLeastOneQuestItem( player, ...requiredItemIds ) ) {
            return false
        }

        await QuestHelper.rewardSingleItem( player, itemId, 1 )
        await QuestHelper.rewardSingleItem( player, Q_STAR_OF_DESTINY, 1 )
        await QuestHelper.takeMultipleItems( player, 1, ...requiredItemIds )
        await state.exitQuest( false, true )

        player.broadcastSocialAction( 3 )
        return true
    }

    checkPartyMember( state: QuestState ): boolean {
        return state.isMemoState( 8 ) && QuestHelper.hasQuestItem( state.getPlayer(), Q_WHITE_FABRIC_Q0234 )
    }

    getAttackableAttackIds(): Array<number> {
        return [ BAIUM ]
    }

    getAttackableKillIds(): Array<number> {
        return [
            PLATINUM_TRIBE_GRUNT,
            PLATINUM_TRIBE_ARCHER,
            PLATINUM_TRIBE_WARRIOR,
            PLATINUM_TRIBE_SHAMAN,
            PLATINUM_TRIBE_LORD,
            GUARDIAN_ANGEL,
            SEAL_ANGEL,
            SEAL_ANGEL_R,
            DOMB_DEATH_CABRIO,
            KERNON,
            GOLKONDA_LONGHORN,
            HALLATE_THE_DEATH_LORD,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/quests/Q00234_FatesWhisper'
    }

    getQuestStartIds(): Array<number> {
        return [ MAESTRO_LEORIN ]
    }

    getSpawnIds(): Array<number> {
        return [
            COFFER_OF_THE_DEAD,
            CHEST_OF_KERNON,
            CHEST_OF_HALLATE,
            CHEST_OF_GOLKONDA,
        ]
    }

    getTalkIds(): Array<number> {
        return [
            ZENKIN,
            CLIFF,
            MASTER_KASPAR,
            HEAD_BLACKSMITH_FERRIS,
            MAESTRO_LEORIN,
            COFFER_OF_THE_DEAD,
            CHEST_OF_KERNON,
            CHEST_OF_HALLATE,
            CHEST_OF_GOLKONDA,
        ]
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName() )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.attackerPlayerId )
        if ( !player.getActiveWeaponItem() || player.getActiveWeaponItem().getId() !== Q_PIPETTE_KNIFE ) {
            return
        }

        await QuestHelper.takeSingleItem( player, Q_PIPETTE_KNIFE, 1 )
        await QuestHelper.giveSingleItem( player, Q_RED_PIPETTE_KNIFE, 1 )

        player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
        return BroadcastHelper.broadcastNpcSayStringId( L2World.getObjectById( data.targetId ) as L2Npc, NpcSayType.All, NpcStringIds.WHO_DARES_TO_TRY_AND_STEAL_MY_NOBLE_BLOOD )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        switch ( data.npcId ) {
            case DOMB_DEATH_CABRIO:
                QuestHelper.addSpawnAtLocation( COFFER_OF_THE_DEAD, npc )
                return

            case KERNON:
                QuestHelper.addSpawnAtLocation( CHEST_OF_KERNON, npc )
                return

            case GOLKONDA_LONGHORN:
                QuestHelper.addSpawnAtLocation( CHEST_OF_GOLKONDA, npc )
                return

            case HALLATE_THE_DEATH_LORD:
                QuestHelper.addSpawnAtLocation( CHEST_OF_HALLATE, npc )
                return
        }

        let state: QuestState = this.getRandomPartyMemberState( L2World.getPlayer( data.playerId ), -1, 2, npc )
        if ( !state ) {
            return
        }

        let player: L2PcInstance = state.getPlayer()
        switch ( data.npcId ) {
            case PLATINUM_TRIBE_GRUNT:
            case PLATINUM_TRIBE_ARCHER:
            case PLATINUM_TRIBE_WARRIOR:
            case PLATINUM_TRIBE_SHAMAN:
            case PLATINUM_TRIBE_LORD:
            case GUARDIAN_ANGEL:
            case SEAL_ANGEL:
            case SEAL_ANGEL_R:
                await QuestHelper.rewardSingleQuestItem( player, Q_BLOODY_FABRIC_Q0234, 1, data.isChampion )
                await QuestHelper.takeSingleItem( player, Q_WHITE_FABRIC_Q0234, 1 )

                if ( QuestHelper.getQuestItemsCount( player, Q_BLOODY_FABRIC_Q0234 ) >= 29 ) {
                    state.setConditionWithSound( 9, true )
                    state.showQuestionMark( 234 )
                    return
                }

                player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                return
        }
    }
    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        if ( [
            eventNames.CHEST_OF_GOLKONDA,
            eventNames.CHEST_OF_HALLATE,
            eventNames.CHEST_OF_KERNON,
            eventNames.COFFER_OF_THE_DEAD
].includes( data.eventName ) ) {
            L2World.getObjectById( data.characterId ).decayMe()
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName() )

        if ( !state ) {
            return
        }

        if ( data.eventName.length > 4 ) {
            if ( data.eventName === 'QUEST_ACCEPTED' ) {
                state.setMemoState( 1 )
                state.startQuest()
                state.showQuestionMark( 234 )

                PacketDispatcher.sendCopyData( data.playerId, SoundPacket.ITEMSOUND_QUEST_ACCEPT )
                return this.getPath( '31002-06.html' )
            }

            return this.getPath( data.eventName )
        }

        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ZENKIN:
                if ( data.eventName === '1' ) {
                    state.setMemoState( 7 )
                    state.setConditionWithSound( 6 )
                    state.showQuestionMark( 234 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_MIDDLE )
                    return '30178-02.html'
                }

                return

            case CLIFF:
                switch ( data.eventName ) {
                    case '1':
                        return this.getPath( '30182-02.html' )

                    case '2':
                        return this.getPath( '30182-03.html' )

                    case '3':
                        if ( state.isMemoState( 4 ) && !QuestHelper.hasQuestItems( player, Q_INFERNIUM_VARNISH ) ) {
                            await QuestHelper.giveSingleItem( player, Q_INFERNIUM_VARNISH, 1 )
                            return this.getPath( '30182-04.html' )
                        }

                        break
                }
                return

            case MASTER_KASPAR:
                switch ( data.eventName ) {
                    case '1':
                        if ( state.isMemoState( 7 ) ) {
                            return this.getPath( '30833-02.html' )
                        }

                        return

                    case '2':
                        if ( state.isMemoState( 7 ) ) {
                            await QuestHelper.giveSingleItem( player, Q_PIPETTE_KNIFE, 1 )

                            state.setMemoState( 8 )
                            state.setConditionWithSound( 7, true )
                            state.showQuestionMark( 234 )

                            return this.getPath( '30833-03a.html' )
                        }

                        return

                    case '3':
                        if ( state.isMemoState( 7 ) ) {
                            await QuestHelper.giveSingleItem( player, Q_WHITE_FABRIC_Q0234, 30 )

                            state.setMemoState( 8 )
                            state.setConditionWithSound( 8, true )
                            state.showQuestionMark( 234 )

                            return this.getPath( '30833-03b.html' )
                        }
                        return

                }

                return

            case MAESTRO_LEORIN:
                switch ( data.eventName ) {
                    case '1':
                        return this.getPath( '31002-02.htm' )

                    case '2':
                        return this.getPath( '31002-03.html' )

                    case '3':
                        return this.getPath( '31002-04.html' )

                    case '4':
                        if ( !state.isCompleted() && player.getLevel() >= 75 ) {
                            return this.getPath( '31002-05.html' )
                        }

                        return

                    case '5':
                        if ( state.isMemoState( 1 ) && QuestHelper.hasQuestItem( player, Q_REIRIAS_SOULORB ) ) {
                            await QuestHelper.takeSingleItem( player, Q_REIRIAS_SOULORB, 1 )

                            state.setMemoState( 2 )
                            state.setConditionWithSound( 2, true )
                            state.showQuestionMark( 234 )

                            return this.getPath( '31002-11.html' )
                        }

                        return

                    case '6':
                        if ( state.isMemoState( 2 )
                                && QuestHelper.hasQuestItems( player, Q_INFERNIUM_SCEPTER_1, Q_INFERNIUM_SCEPTER_2, Q_INFERNIUM_SCEPTER_3 ) ) {
                            await QuestHelper.takeMultipleItems( player, -1, Q_INFERNIUM_SCEPTER_1, Q_INFERNIUM_SCEPTER_2, Q_INFERNIUM_SCEPTER_3 )

                            state.setMemoState( 4 )
                            state.setConditionWithSound( 3, true )
                            state.showQuestionMark( 234 )

                            return this.getPath( '31002-14.html' )
                        }

                        return

                    case '7':
                        if ( state.isMemoState( 4 ) && QuestHelper.hasQuestItem( player, Q_INFERNIUM_VARNISH ) ) {
                            await QuestHelper.takeSingleItem( player, Q_INFERNIUM_VARNISH, 1 )

                            state.setMemoState( 5 )
                            state.setConditionWithSound( 4, true )
                            state.showQuestionMark( 234 )

                            return this.getPath( '31002-17.html' )
                        }

                        return

                    case '8':
                        if ( state.isMemoState( 5 )
                                && QuestHelper.hasQuestItem( player, Q_MAESTRO_REORINS_HAMMER ) ) {
                            await QuestHelper.takeSingleItem( player, Q_MAESTRO_REORINS_HAMMER, 1 )

                            state.setMemoState( 6 )
                            state.setConditionWithSound( 5, true )
                            state.showQuestionMark( 234 )

                            return this.getPath( '31002-20.html' )
                        }

                        return

                    case '9':
                        if ( state.isMemoState( 9 ) && QuestHelper.hasQuestItem( player, Q_MAESTRO_REORINS_MOLD ) ) {
                            await QuestHelper.takeSingleItem( player, Q_MAESTRO_REORINS_MOLD, 1 )

                            state.setMemoState( 10 )
                            state.setConditionWithSound( 11, true )
                            state.showQuestionMark( 234 )

                            return this.getPath( '31002-23.html' )
                        }

                        return

                    case '10':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 11 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-26.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '11':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 19 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-26a.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '12':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 12 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-27.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '13':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 13 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-28.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '14': {
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )
                                state.setMemoState( 14 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )
                                return this.getPath( '31002-29.html' )
                            }
                            return this.getPath( '31002-34.html' )
                        }
                        return
                    }
                    case '15':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 15 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-30.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '16':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 16 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-31.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '17':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 17 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-32.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '18':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 18 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-33.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '41':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 41 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-33a.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '42':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 42 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-33b.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '43':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 43 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-33c.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '44':
                        if ( state.isMemoState( 10 ) ) {
                            if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) >= 984 ) {
                                await QuestHelper.takeSingleItem( player, CRYSTAL_B, 984 )

                                state.setMemoState( 44 )
                                state.setConditionWithSound( 12, true )
                                state.showQuestionMark( 234 )

                                return this.getPath( '31002-33d.html' )
                            }

                            return this.getPath( '31002-34.html' )
                        }

                        return

                    case '21':
                        if ( await this.giveReward( state, player, TALLUM_BLADE ) ) {
                            return this.getPath( '31002-44.html' )
                        }

                        return

                    case '22':
                        if ( await this.giveReward( state, player, CARNIUM_BOW ) ) {
                            return this.getPath( '31002-44.html' )
                        }

                        return

                    case '23':
                        if ( await this.giveReward( state, player, HALBARD ) ) {
                            return this.getPath( '31002-44.html' )
                        }

                        return

                    case '24':
                        if ( await this.giveReward( state, player, ELEMENTAL_SWORD ) ) {
                            return this.getPath( '31002-44.html' )
                        }

                        return

                    case '25':
                        if ( await this.giveReward( state, player, DASPARIONS_STAFF ) ) {
                            return this.getPath( '31002-44.html' )
                        }

                        return

                    case '26':
                        if ( await this.giveReward( state, player, BLOODY_ORCHID ) ) {
                            return this.getPath( '31002-44.html' )
                        }
                        return

                    case '27':
                        if ( await this.giveReward( state, player, BLOOD_TORNADO ) ) {
                            return this.getPath( '31002-44.html' )
                        }

                        return

                    case '28':
                        if ( await this.giveReward( state, player, METEOR_SHOWER ) ) {
                            return this.getPath( '31002-44.html' )
                        }

                        return

                    case '29':
                        if ( await this.giveReward( state, player, KSHANBERK_KSHANBERK ) ) {
                            return this.getPath( '31002-44.html' )
                        }

                        return

                    case '30':
                        if ( await this.giveReward( state, player, INFERNO_MASTER ) ) {
                            return this.getPath( '31002-44.html' )
                        }

                        return

                    case '31':
                        if ( await this.giveReward( state, player, EYE_OF_SOUL ) ) {
                            return this.getPath( '31002-44.html' )
                        }

                        return

                    case '32':
                        if ( await this.giveReward( state, player, HAMMER_OF_DESTROYER ) ) {
                            return this.getPath( '31002-44.html' )
                        }

                        return
                }
        }
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        switch ( data.npcId ) {
            case COFFER_OF_THE_DEAD:
                this.startQuestTimer( eventNames.COFFER_OF_THE_DEAD, 1000 * 120, data.characterId, 0 )
                return

            case CHEST_OF_KERNON:
                this.startQuestTimer( eventNames.CHEST_OF_KERNON, 1000 * 120, data.characterId, 0 )
                return

            case CHEST_OF_HALLATE:
                this.startQuestTimer( eventNames.CHEST_OF_HALLATE, 1000 * 120, data.characterId, 0 )
                return

            case CHEST_OF_GOLKONDA:
                this.startQuestTimer( eventNames.CHEST_OF_GOLKONDA, 1000 * 120, data.characterId, 0 )
                return
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, true )
        let player = L2World.getPlayer( data.playerId )

        switch ( data.characterNpcId ) {
            case ZENKIN:
                switch ( state.getMemoState() ) {
                    case 6:
                        return this.getPath( '30178-01.html' )
                    case 7:
                        return this.getPath( '30178-03.html' )
                    case 8:
                        return this.getPath( '30178-04.html' )
                }

                break

            case CLIFF:
                if ( state.isMemoState( 4 ) ) {
                    if ( !QuestHelper.hasQuestItems( player, Q_INFERNIUM_VARNISH ) ) {
                        return this.getPath( '30182-01.html' )
                    }

                    return this.getPath( '30182-05.html' )
                }

                if ( state.getMemoState() >= 5 ) {
                    return this.getPath( '30182-06.html' )
                }

                break

            case MASTER_KASPAR:
                if ( state.isMemoState( 7 ) ) {
                    return this.getPath( '30833-01.html' )
                }

                if ( state.isMemoState( 8 ) ) {
                    let bloodyFabricCount = QuestHelper.getQuestItemsCount( player, Q_BLOODY_FABRIC_Q0234 )
                    let whiteFabricCount = QuestHelper.getQuestItemsCount( player, Q_WHITE_FABRIC_Q0234 )
                    let totalCount: number = bloodyFabricCount + whiteFabricCount
                    let hasKnife: boolean = QuestHelper.hasQuestItems( player, Q_RED_PIPETTE_KNIFE )

                    if ( !hasKnife ) {
                        if ( totalCount >= 30 ) {
                            if ( bloodyFabricCount < 30 ) {
                                return this.getPath( '30833-03c.html' )
                            }

                            await QuestHelper.giveSingleItem( player, Q_MAESTRO_REORINS_MOLD, 1 )
                            await QuestHelper.takeSingleItem( player, Q_BLOODY_FABRIC_Q0234, -1 )

                            state.setMemoState( 9 )
                            state.setConditionWithSound( 10, true )
                            state.showQuestionMark( 234 )

                            return this.getPath( '30833-03d.html' )
                        }

                        if ( totalCount === 0 ) {
                            return this.getPath( '30833-03.html' )
                        }


                        await QuestHelper.giveSingleItem( player, Q_WHITE_FABRIC_Q0234, 30 - whiteFabricCount )
                        await QuestHelper.takeSingleItem( player, Q_BLOODY_FABRIC_Q0234, -1 )

                        return this.getPath( '30833-03e.html' )
                    }

                    if ( totalCount === 0 ) {
                        await QuestHelper.giveSingleItem( player, Q_MAESTRO_REORINS_MOLD, 1 )
                        await QuestHelper.takeSingleItem( player, Q_RED_PIPETTE_KNIFE, 1 )

                        state.setMemoState( 9 )
                        state.setConditionWithSound( 10, true )
                        state.showQuestionMark( 234 )

                        return this.getPath( '30833-04.html' )
                    }

                    break
                }

                if ( state.getMemoState() >= 9 ) {
                    return this.getPath( '30833-05.html' )
                }

                break

            case HEAD_BLACKSMITH_FERRIS:
                if ( state.isMemoState( 5 ) ) {
                    if ( QuestHelper.hasQuestItems( player, Q_MAESTRO_REORINS_HAMMER ) ) {
                        return this.getPath( '30847-02.html' )
                    }

                    await QuestHelper.giveSingleItem( player, Q_MAESTRO_REORINS_HAMMER, 1 )
                    return this.getPath( '30847-01.html' )
                }

                if ( state.getMemoState() >= 6 ) {
                    return this.getPath( '30847-03.html' )
                }

                break

            case MAESTRO_LEORIN:
                if ( state.isCreated() ) {

                    if ( state.isCreated() && ( player.getLevel() < 75 ) ) {
                        return this.getPath( '31002-01a.htm' )
                    }

                    return this.getPath( '31002-01.htm' )
                }

                if ( state.isCompleted() ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                let memoState = state.getMemoState()
                switch ( memoState ) {
                    case 1:
                        if ( QuestHelper.hasQuestItems( player, Q_REIRIAS_SOULORB ) ) {
                            return this.getPath( '31002-10.html' )
                        }

                        return this.getPath( '31002-09.html' )

                    case 2:
                        if ( QuestHelper.hasQuestItems( player, Q_INFERNIUM_SCEPTER_1, Q_INFERNIUM_SCEPTER_2, Q_INFERNIUM_SCEPTER_3 ) ) {
                            return this.getPath( '31002-13.html' )
                        }

                        return this.getPath( '31002-12.html' )

                    case 4:
                        if ( QuestHelper.hasQuestItems( player, Q_INFERNIUM_VARNISH ) ) {
                            return this.getPath( '31002-16.html' )
                        }

                        return this.getPath( '31002-15.html' )

                    case 5:
                        if ( QuestHelper.hasQuestItems( player, Q_MAESTRO_REORINS_HAMMER ) ) {
                            return this.getPath( '31002-19.html' )
                        }

                        return this.getPath( '31002-18.html' )

                    case 6:
                    case 7:
                    case 8:
                        return this.getPath( '31002-21.html' )

                    case 9:
                        if ( QuestHelper.hasQuestItems( player, Q_MAESTRO_REORINS_MOLD ) ) {
                            return this.getPath( '31002-22.html' )
                        }

                        break

                    case 10:
                        if ( QuestHelper.getQuestItemsCount( player, CRYSTAL_B ) < 984 ) {
                            return this.getPath( '31002-24.html' )
                        }

                        return this.getPath( '31002-25.html' )

                    case 11:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, SWORD_OF_DAMASCUS, SWORD_OF_DAMASCUS_FOCUS, SWORD_OF_DAMASCUS_CRT_DAMAGE, SWORD_OF_DAMASCUS_HASTE ) ) {
                            return this.getPath( '31002-35.html' )
                        }

                        return this.getPath( '31002-35a.html' )

                    case 12:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, HAZARD_BOW_GUIDENCE, HAZARD_BOW_QUICKRECOVERY, HAZARD_BOW_CHEAPSHOT, HAZARD_BOW ) ) {
                            return this.getPath( '31002-36.html' )
                        }

                        return this.getPath( '31002-36a.html' )

                    case 13:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, LANCIA_ANGER, LANCIA_CRT_STUN, LANCIA_LONGBLOW, LANCIA ) ) {
                            return this.getPath( '31002-37.html' )
                        }

                        return this.getPath( '31002-37a.html' )

                    case 14:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, ART_OF_BATTLE_AXE_HEALTH, ART_OF_BATTLE_AXE_RSK_FOCUS, ART_OF_BATTLE_AXE_HASTE, ART_OF_BATTLE_AXE ) ) {
                            return this.getPath( '31002-38.html' )
                        }

                        return this.getPath( '31002-38a.html' )

                    case 15:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, STAFF_OF_EVIL_SPRIT_MAGICFOCUS, STAFF_OF_EVIL_SPRIT_MAGICBLESSTHEBODY, STAFF_OF_EVIL_SPRIT_MAGICPOISON, STAFF_OF_EVIL_SPRIT ) ) {
                            return this.getPath( '31002-39.html' )
                        }

                        return this.getPath( '31002-39a.html' )

                    case 16:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, DEMONS_SWORD_CRT_BLEED, DEMONS_SWORD_CRT_POISON, DEMONS_SWORD_MIGHTMOTAL, DEMONS_SWORD ) ) {
                            return this.getPath( '31002-40.html' )
                        }

                        return this.getPath( '31002-40a.html' )

                    case 17:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, BELLION_CESTUS_CRT_DRAIN, BELLION_CESTUS_CRT_POISON, BELLION_CESTUS_RSK_HASTE, BELLION_CESTUS ) ) {
                            return this.getPath( '31002-41.html' )
                        }

                        return this.getPath( '31002-41a.html' )

                    case 18:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, DEADMANS_GLORY_ANGER, DEADMANS_GLORY_HEALTH, DEADMANS_GLORY_HASTE, DEADMANS_GLORY ) ) {
                            return this.getPath( '31002-42.html' )
                        }

                        return this.getPath( '31002-42a.html' )

                    case 19:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, SAMURAI_LONGSWORD_SAMURAI_LONGSWORD ) ) {
                            return this.getPath( '31002-43.html' )
                        }

                        return this.getPath( '31002-43a.html' )

                    case 41:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, GUARDIANS_SWORD, GUARDIANS_SWORD_CRT_DRAIN, GUARDIANS_SWORD_HEALTH, GUARDIANS_SWORD_CRT_BLEED ) ) {
                            return this.getPath( '31002-43b.html' )
                        }

                        return this.getPath( '31002-43c.html' )

                    case 42:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, TEARS_OF_WIZARD, TEARS_OF_WIZARD_ACUMEN, TEARS_OF_WIZARD_MAGICPOWER, TEARS_OF_WIZARD_UPDOWN ) ) {
                            return this.getPath( '31002-43d.html' )
                        }

                        return this.getPath( '31002-43e.html' )

                    case 43:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, STAR_BUSTER, STAR_BUSTER_HEALTH, STAR_BUSTER_HASTE, STAR_BUSTER_RSK_FOCUS ) ) {
                            return this.getPath( '31002-43f.html' )
                        }

                        return this.getPath( '31002-43g.html' )

                    case 44:
                        if ( QuestHelper.hasAtLeastOneQuestItem( player, BONE_OF_KAIM_VANUL, BONE_OF_KAIM_VANUL_MANAUP, BONE_OF_KAIM_VANUL_MAGICSILENCE, BONE_OF_KAIM_VANUL_UPDOWN ) ) {
                            return this.getPath( '31002-43h.html' )
                        }

                        return this.getPath( '31002-43i.html' )
                }

                break

            case COFFER_OF_THE_DEAD:
                let hasSoulOrb = QuestHelper.hasQuestItems( player, Q_REIRIAS_SOULORB )
                if ( state.getMemoState() > 1 || hasSoulOrb ) {
                    return this.getPath( '31027-02.html' )
                }

                if ( state.isMemoState( 1 ) && !hasSoulOrb ) {
                    await QuestHelper.giveSingleItem( player, Q_REIRIAS_SOULORB, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    return this.getPath( '31027-01.html' )
                }

                break

            case CHEST_OF_KERNON:
                let hasScepter = QuestHelper.hasQuestItems( player, Q_INFERNIUM_SCEPTER_1 )
                if ( !state.isMemoState( 2 ) || hasScepter ) {
                    return this.getPath( '31028-02.html' )
                }

                if ( state.isMemoState( 2 ) && !hasScepter ) {
                    await QuestHelper.giveSingleItem( player, Q_INFERNIUM_SCEPTER_1, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    return this.getPath( '31028-01.html' )
                }

                break

            case CHEST_OF_GOLKONDA:
                let hasSecondScepter = QuestHelper.hasQuestItems( player, Q_INFERNIUM_SCEPTER_2 )

                if ( !state.isMemoState( 2 ) || hasSecondScepter ) {
                    return this.getPath( '31029-02.html' )
                }

                if ( state.isMemoState( 2 ) && !hasSecondScepter ) {
                    await QuestHelper.giveSingleItem( player, Q_INFERNIUM_SCEPTER_2, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    return this.getPath( '31029-01.html' )
                }

                break

            case CHEST_OF_HALLATE:
                let hasThirdScepter = QuestHelper.hasQuestItems( player, Q_INFERNIUM_SCEPTER_3 )

                if ( !state.isMemoState( 2 ) || hasThirdScepter ) {
                    return this.getPath( '31030-02.html' )
                }

                if ( state.isMemoState( 2 ) && !hasThirdScepter ) {
                    await QuestHelper.giveSingleItem( player, Q_INFERNIUM_SCEPTER_3, 1 )

                    player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ITEMGET )
                    return this.getPath( '31030-01.html' )
                }

                break
        }
        return QuestHelper.getNoQuestMessagePath()
    }

    async giveReward( state: QuestState, player: L2PcInstance, itemId: number ): Promise<boolean> {
        switch ( state.getMemoState() ) {
            case 11:
                return this.applyReward( state, player, itemId, SWORD_OF_DAMASCUS, SWORD_OF_DAMASCUS_FOCUS, SWORD_OF_DAMASCUS_CRT_DAMAGE, SWORD_OF_DAMASCUS_HASTE )

            case 12:
                return this.applyReward( state, player, itemId, HAZARD_BOW, HAZARD_BOW_GUIDENCE, HAZARD_BOW_QUICKRECOVERY, HAZARD_BOW_CHEAPSHOT )

            case 13:
                return this.applyReward( state, player, itemId, LANCIA, LANCIA_ANGER, LANCIA_CRT_STUN, LANCIA_LONGBLOW )

            case 14:
                return this.applyReward( state, player, itemId, ART_OF_BATTLE_AXE, ART_OF_BATTLE_AXE_HEALTH, ART_OF_BATTLE_AXE_RSK_FOCUS, ART_OF_BATTLE_AXE_HASTE )

            case 15:
                return this.applyReward( state, player, itemId, STAFF_OF_EVIL_SPRIT, STAFF_OF_EVIL_SPRIT_MAGICFOCUS, STAFF_OF_EVIL_SPRIT_MAGICBLESSTHEBODY, STAFF_OF_EVIL_SPRIT_MAGICPOISON )

            case 16:
                return this.applyReward( state, player, itemId, DEMONS_SWORD, DEMONS_SWORD_CRT_BLEED, DEMONS_SWORD_CRT_POISON, DEMONS_SWORD_MIGHTMOTAL )

            case 17:
                return this.applyReward( state, player, itemId, BELLION_CESTUS, BELLION_CESTUS_CRT_DRAIN, BELLION_CESTUS_CRT_POISON, BELLION_CESTUS_RSK_HASTE )

            case 18:
                return this.applyReward( state, player, itemId, DEADMANS_GLORY, DEADMANS_GLORY_ANGER, DEADMANS_GLORY_HEALTH, DEADMANS_GLORY_HASTE )

            case 19:
                return this.applyReward( state, player, itemId, SAMURAI_LONGSWORD_SAMURAI_LONGSWORD )

            case 41:
                return this.applyReward( state, player, itemId, GUARDIANS_SWORD, GUARDIANS_SWORD_CRT_DRAIN, GUARDIANS_SWORD_HEALTH, GUARDIANS_SWORD_CRT_BLEED )

            case 42:
                return this.applyReward( state, player, itemId, TEARS_OF_WIZARD, TEARS_OF_WIZARD_ACUMEN, TEARS_OF_WIZARD_MAGICPOWER, TEARS_OF_WIZARD_UPDOWN )

            case 43:
                return this.applyReward( state, player, itemId, STAR_BUSTER, STAR_BUSTER_HEALTH, STAR_BUSTER_HASTE, STAR_BUSTER_RSK_FOCUS )

            case 44:
                return this.applyReward( state, player, itemId, BONE_OF_KAIM_VANUL, BONE_OF_KAIM_VANUL_MANAUP, BONE_OF_KAIM_VANUL_MAGICSILENCE, BONE_OF_KAIM_VANUL_UPDOWN )

            default:
                return false
        }
    }
}