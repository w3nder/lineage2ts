import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, NpcGeneralEvent, PlayerStartsLogoutEvent, PlayerSummonSpawnEvent } from '../../gameService/models/events/EventType'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Summon } from '../../gameService/models/actor/L2Summon'
import { L2PetInstance } from '../../gameService/models/actor/instance/L2PetInstance'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { AbnormalType } from '../../gameService/models/skills/AbnormalType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'
import { AISkillData } from '../../gameService/models/actor/templates/L2NpcTemplate'
import { SkillCache } from '../../gameService/cache/SkillCache'
import aigle from 'aigle'
import { SkillItem } from '../../gameService/models/skills/SkillItem'

const npcIds: Array<number> = [
    16034, // Improved Baby Buffalo
    16035, // Improved Baby Kookaburra
    16036, // Improved Baby Cougar
]

const BUFF_CONTROL = 5771

const enum EventNames {
    CastBuff = 'cb',
    CastHeal = 'ch'
}

// TODO : convert pets actions to AI Controller trait, to avoid messing with quest timers
// consider that pets may cast heals/buffs only when attacking, per L2 lore
export class ImprovedBabyPets extends ListenerLogic {
    constructor() {
        super( 'ImprovedBabyPets', 'listeners/summons/ImprovedBabyPets.ts' )
    }

    async castBuffSkill( summon: L2Summon, buffStep: number, buffValue: number ): Promise<void> {
        let owner: L2PcInstance = summon.getOwner()
        if ( !owner || owner.isDead() || owner.isInvulnerable() ) {
            return
        }

        let parameters = summon.getTemplate().getSkillParameters()
        let skillData: AISkillData = parameters[ `step${ buffStep }_merged_buff0${ buffValue }` ]
        if ( !skillData ) {
            skillData = parameters[ `step${ buffStep }_buff0${ buffValue }` ]
        }

        if ( !skillData ) {
            return
        }

        let skill = SkillCache.getSkill( skillData.id, skillData.level )
        if ( !skill ) {
            return
        }

        let previousFollowStatus = summon.isFollowingOwner()
        if ( !previousFollowStatus && !summon.isInsideRadius( owner, skill.getCastRange(), true ) ) {
            return
        }

        let canCastSkill = !this.hasAbnormalType( owner, skill.getAbnormalType() ) && summon.canCastSkill( skill )
        if ( !canCastSkill ) {
            return
        }

        let targetType : AISkillData = parameters[ `step${ buffStep }_buff_target0${ buffValue }` ]

        if ( targetType && targetType.id >= 0 && targetType.id <= 2 ) {
            summon.setTarget( ( targetType.id === 1 ) ? summon : owner )
            await summon.doCast( skill )

            let message = new SystemMessageBuilder( SystemMessageIds.PET_USES_S1 )
                    .addSkillName( skill )
                    .getBuffer()
            summon.sendOwnedData( message )

            if ( previousFollowStatus !== summon.isFollowingOwner() ) {
                summon.setAIFollow( previousFollowStatus )
            }

            return
        }
    }

    async castHealSkill( summon: L2Summon, healStep: number, healValue: number ): Promise<void> {
        let player: L2PcInstance = summon.getOwner()
        let parameters = summon.getTemplate().getParameters()
        let skillItem = summon.getTemplate().getSkillParameters()[ `step${ healStep }_heal0${ healValue }` ]
        let skill = SkillCache.getSkill( skillItem.id, skillItem.level )
        if ( !skill ) {
            return
        }

        let targetType: number = _.get( parameters, `step${ healStep }_heal_target0${ healValue }`, 0 ) as number

        let previousFollowStatus = summon.isFollowingOwner()
        if ( !previousFollowStatus && !summon.isInsideRadius( player, skill.getCastRange(), true ) ) {
            return
        }

        let canCastSkill = skill && player && summon.canCastSkill( skill ) && !player.isDead()
        if ( !canCastSkill ) {
            return
        }

        if ( !this.hasAbnormalType( player, skill.getAbnormalType() ) ) {
            if ( ( targetType >= 0 ) && ( targetType <= 2 ) ) {
                // TODO : rewrite using AIController
                summon.setTarget( ( targetType === 1 ) ? summon : player )
                await summon.doCast( skill )

                let message = new SystemMessageBuilder( SystemMessageIds.PET_USES_S1 )
                        .addSkillName( skill )
                        .getBuffer()

                summon.sendOwnedData( message )

                if ( previousFollowStatus !== summon.isFollowingOwner() ) {
                    summon.setAIFollow( previousFollowStatus )
                }
            }
        }
    }

    getCustomListeners(): Array<ListenerDescription> {
        return [
            {
                registerType: ListenerRegisterType.General,
                eventType: EventType.PlayerStartsLogout,
                method: this.finishUp.bind( this ),
                ids: null,
            },
        ]
    }

    async finishUp( data: PlayerStartsLogoutEvent ): Promise<void> {
        this.stopTimers( data.playerId )
    }

    getSummonSpawnIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        if ( player ) {
            let pet: L2PetInstance = player.getSummon() as L2PetInstance
            if ( !pet ) {
                this.stopTimers( data.playerId )
                return
            }

            if ( event === EventNames.CastHeal
                    && player.isInCombat()
                    && !pet.isHungry() ) {

                let hpPer = ( player.getCurrentHp() / player.getMaxHp() ) * 100
                let mpPer = ( player.getCurrentMp() / player.getMaxMp() ) * 100
                let healStep = _.clamp( Math.floor( ( pet.getLevel() / 5 ) - 11 ), 0, 3 )
                let healType = pet.getTemplate().getParameters()[ 'heal_type' ] as number

                switch ( healType ) {
                    case 0:
                        if ( hpPer < 30 ) {
                            await this.castHealSkill( pet, healStep, 2 )
                        } else if ( mpPer < 60 ) {
                            await this.castHealSkill( pet, healStep, 1 )
                        }
                        break

                    case 1: {
                        if ( ( hpPer >= 30 ) && ( hpPer < 70 ) ) {
                            await this.castHealSkill( pet, healStep, 1 )
                        } else if ( hpPer < 30 ) {
                            await this.castHealSkill( pet, healStep, 2 )
                        }
                        break
                    }
                }

                return
            }

            if ( event === EventNames.CastBuff
                    && !pet.isAffectedBySkill( BUFF_CONTROL )
                    && !pet.isHungry() ) {

                let buffStep = _.clamp( Math.floor( ( pet.getLevel() / 5 ) - 11 ), 0, 3 )
                let quest = this
                await aigle.resolve( 2 * ( 1 + buffStep ) ).times( ( index: number ) => {
                    return quest.castBuffSkill( pet, buffStep, index + 1 )
                } )
            }
        }
    }

    async onSummonSpawnEvent( data: PlayerSummonSpawnEvent ): Promise<void> {
        this.startQuestTimer( EventNames.CastBuff, 10000, undefined, data.ownerId, true )
        this.startQuestTimer( EventNames.CastHeal, 3000, undefined, data.ownerId, true )
    }

    hasAbnormalType( player: L2PcInstance, type: AbnormalType ): boolean {
        return !!player.getEffectList().getBuffInfoByAbnormalType( type )
    }

    stopTimers( playerObjectId: number ): void {
        this.stopQuestTimer( EventNames.CastBuff, undefined, playerObjectId )
        this.stopQuestTimer( EventNames.CastHeal, undefined, playerObjectId )
    }
}