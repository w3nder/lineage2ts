import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const GOLEM_TRADER = 13128

export class GolemTrader extends ListenerLogic {
    constructor() {
        super( 'GolemTrader', 'listeners/summons/GolemTrader.ts' )
    }

    getSpawnIds(): Array<number> {
        return [ GOLEM_TRADER ]
    }

    async onSpawnEvent( data : NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        npc.scheduleDespawn( 180000 )
    }
}