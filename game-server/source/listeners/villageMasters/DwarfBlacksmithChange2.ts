import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'

import _ from 'lodash'

const npcIds: Array<number> = [
    30512, // Kusto
    30677, // Flutter
    30687, // Vergara
    30847, // Ferris
    30897, // Roman
    31272, // Noel
    31317, // Lombert
    31961, // Newyear
]

const rewardItemId = 8870
const rewardAmount = 15

const MARK_OF_MAESTRO = 2867
const MARK_OF_GUILDSMAN = 3119
const MARK_OF_PROSPERITY = 3238
const WARSMITH = ClassIdValues.warsmith.id

export class DwarfBlacksmithChange2 extends ListenerLogic {
    constructor() {
        super( 'DwarfBlacksmithChange2', 'quests/villageMasters/DwarfBlacksmithChange2.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/DwarfBlacksmithChange2'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '30512-03.htm':
            case '30512-04.htm':
            case '30512-05.htm':
                return this.getPath( data.eventName )

            case '57':
                return this.getRequestedClassChange( data )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.isInCategory( CategoryType.FOURTH_CLASS_GROUP ) ) {
            return this.getPath( '30512-01.htm' )
        }

        if ( player.isInCategory( CategoryType.WARSMITH_GROUP ) ) {
            if ( [ ClassIdValues.artisan.id, ClassIdValues.warsmith.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30512-02.htm' )
            }

            return this.getPath( '30512-06.htm' )
        }

        return this.getPath( '30512-07.htm' )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( '30512-08.htm' )
        }

        if ( classId === WARSMITH && player.getClassId() === ClassIdValues.artisan.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_GUILDSMAN, MARK_OF_PROSPERITY, MARK_OF_MAESTRO )
            if ( player.getLevel() < 40 ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30512-09.htm' )
                }

                return this.getPath( '30512-10.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_GUILDSMAN, MARK_OF_PROSPERITY, MARK_OF_MAESTRO )

                await player.setClassId( WARSMITH )
                player.setBaseClass( WARSMITH )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30512-11.htm' )
            }

            return this.getPath( '30512-12.htm' )
        }
    }
}