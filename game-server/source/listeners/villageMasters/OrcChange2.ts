import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { QuestHelper } from '../helpers/QuestHelper'

import _ from 'lodash'

const npcIds: Array<number> = [
    30513, // Penatus
    30681, // Karia
    30704, // Garvarentz
    30865, // Ladanza
    30913, // Tushku
    31288, // Aklan
    31326, // Lambac
    31336, // Rahorakti
    31977, // Shaka
]

const MARK_OF_CHALLENGER = 2627
const MARK_OF_PILGRIM = 2721
const MARK_OF_DUELIST = 2762
const MARK_OF_WARSPIRIT = 2879
const MARK_OF_GLORY = 3203
const MARK_OF_CHAMPION = 3276
const MARK_OF_LORD = 3390

const DESTROYER = ClassIdValues.destroyer.id
const TYRANT = ClassIdValues.tyrant.id
const OVERLORD = ClassIdValues.overlord.id
const WARCRYER = ClassIdValues.warcryer.id

const rewardItemId = 8870
const rewardAmount = 15
const minimumLevel = 40

export class OrcChange2 extends ListenerLogic {

    constructor() {
        super( 'OrcChange2', 'quests/villageMasters/OrcChange2.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/OrcChange2'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '46':
            case '48':
            case '51':
            case '52':
                return this.getRequestedClassChange( data )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.isInCategory( CategoryType.FOURTH_CLASS_GROUP )
                && ( player.isInCategory( CategoryType.ORC_MALL_CLASS ) || player.isInCategory( CategoryType.ORC_FALL_CLASS ) ) ) {
            return this.getPath( '30513-01.htm' )
        }

        if ( player.isInCategory( CategoryType.ORC_MALL_CLASS ) || player.isInCategory( CategoryType.ORC_FALL_CLASS ) ) {
            if ( [ ClassIdValues.orcRaider.id, ClassIdValues.destroyer.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30513-02.htm' )
            }

            if ( [ ClassIdValues.orcMonk.id, ClassIdValues.tyrant.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30513-06.htm' )
            }

            if ( [ ClassIdValues.orcShaman.id, ClassIdValues.overlord.id, ClassIdValues.warcryer.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30513-10.htm' )
            }

            return this.getPath( '30513-17.htm' )
        }

        return this.getPath( '30513-18.htm' )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( '30513-19.htm' )
        }

        if ( classId === DESTROYER && player.getClassId() === ClassIdValues.orcRaider.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_CHALLENGER, MARK_OF_GLORY, MARK_OF_CHAMPION )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30513-20.htm' )
                }

                return this.getPath( '30513-21.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_CHALLENGER, MARK_OF_GLORY, MARK_OF_CHAMPION )

                await player.setClassId( DESTROYER )
                player.setBaseClass( DESTROYER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30513-22.htm' )
            }

            return this.getPath( '30513-23.htm' )
        }

        if ( classId === TYRANT && player.getClassId() === ClassIdValues.orcMonk.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_CHALLENGER, MARK_OF_GLORY, MARK_OF_DUELIST )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30513-24.htm' )
                }

                return this.getPath( '30513-25.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_CHALLENGER, MARK_OF_GLORY, MARK_OF_DUELIST )

                await player.setClassId( TYRANT )
                player.setBaseClass( TYRANT )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30513-26.htm' )
            }

            return this.getPath( '30513-27.htm' )
        }

        if ( classId === OVERLORD && player.getClassId() === ClassIdValues.orcShaman.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_PILGRIM, MARK_OF_GLORY, MARK_OF_LORD )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30513-28.htm' )
                }

                return this.getPath( '30513-29.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_PILGRIM, MARK_OF_GLORY, MARK_OF_LORD )

                await player.setClassId( OVERLORD )
                player.setBaseClass( OVERLORD )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30513-30.htm' )
            }

            return this.getPath( '30513-31.htm' )
        }

        if ( classId === WARCRYER && player.getClassId() === ClassIdValues.orcShaman.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_PILGRIM, MARK_OF_GLORY, MARK_OF_WARSPIRIT )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30513-32.htm' )
                }

                return this.getPath( '30513-33.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_PILGRIM, MARK_OF_GLORY, MARK_OF_WARSPIRIT )

                await player.setClassId( WARCRYER )
                player.setBaseClass( WARCRYER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30513-34.htm' )
            }

            return this.getPath( '30513-35.htm' )
        }
    }
}