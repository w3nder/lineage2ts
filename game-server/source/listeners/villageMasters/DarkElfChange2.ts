import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'

import _ from 'lodash'

const npcIds: Array<number> = [
    30195, // Brecson
    30474, // Angus
    30699, // Medown
    30862, // Oltran
    30910, // Xairakin
    31285, // Samael
    31324, // Andromeda
    31334, // Tifaren
    31974, // Drizzit
]

const SHILLIEN_KNIGHT = 33
const BLADEDANCER = 34
const ABYSS_WALKER = 36
const PHANTOM_RANGER = 37
const SPELLHOWLER = 40
const PHANTOM_SUMMONER = 41
const SHILLIEN_ELDER = 43

// Items
const MARK_OF_CHALLENGER = 2627
const MARK_OF_DUTY = 2633
const MARK_OF_SEEKER = 2673
const MARK_OF_SCHOLAR = 2674
const MARK_OF_PILGRIM = 2721
const MARK_OF_DUELIST = 2762
const MARK_OF_SEARCHER = 2809
const MARK_OF_REFORMER = 2821
const MARK_OF_MAGUS = 2840
const MARK_OF_FATE = 3172
const MARK_OF_SAGITTARIUS = 3293
const MARK_OF_WITCHCRAFT = 3307
const MARK_OF_SUMMONER = 3336

// Reward
const SHADOW_ITEM_EXCHANGE_COUPON_C_GRADE = 8870
const rewardAmount = 15

const minimumLevel = 40

export class DarkElfChange2 extends ListenerLogic {
    constructor() {
        super( 'DarkElfChange2', 'quests/villageMasters/DarkElfChange2.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/DarkElfChange2'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '30195-02.htm':
            case '30195-03.htm':
            case '30195-04.htm':
            case '30195-05.htm':
            case '30195-06.htm':
            case '30195-07.htm':
            case '30195-08.htm':
            case '30195-09.htm':
            case '30195-10.htm':
            case '30195-11.htm':
            case '30195-12.htm':
            case '30195-13.htm':
            case '30195-14.htm':
            case '30195-15.htm':
            case '30195-16.htm':
            case '30195-17.htm':
            case '30195-18.htm':
            case '30195-19.htm':
            case '30195-20.htm':
            case '30195-21.htm':
            case '30195-22.htm':
            case '30195-23.htm':
            case '30195-24.htm':
            case '30195-25.htm':
            case '30195-26.htm':
                return this.getPath( data.eventName )

            case '33':
            case '34':
            case '36':
            case '37':
            case '40':
            case '41':
            case '43':
                return this.getRequestedClassChange( data )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.isInCategory( CategoryType.FOURTH_CLASS_GROUP )
                && ( player.isInCategory( CategoryType.DELF_MALL_CLASS ) || player.isInCategory( CategoryType.DELF_FALL_CLASS ) ) ) {
            return this.getPath( '30195-01.htm' )
        }

        if ( player.isInCategory( CategoryType.DELF_MALL_CLASS ) || player.isInCategory( CategoryType.DELF_FALL_CLASS ) ) {
            let classId = player.getClassId()

            if ( [ ClassIdValues.palusKnight.id, ClassIdValues.shillienKnight.id, ClassIdValues.bladedancer.id ].includes( classId ) ) {
                return this.getPath( '30195-02.htm' )
            }

            if ( [ ClassIdValues.assassin.id, ClassIdValues.abyssWalker.id, ClassIdValues.phantomRanger.id ].includes( classId ) ) {
                return this.getPath( '30195-09.htm' )
            }

            if ( [ ClassIdValues.darkWizard.id, ClassIdValues.spellhowler.id, ClassIdValues.phantomSummoner.id ].includes( classId ) ) {
                return this.getPath( '30195-16.htm' )
            }

            if ( [ ClassIdValues.shillienOracle.id, ClassIdValues.shillenElder.id ].includes( classId ) ) {
                return this.getPath( '30195-23.htm' )
            }

            return this.getPath( '30195-27.htm' )
        }

        return this.getPath( '30195-28.htm' )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ) {
        let player = L2World.getPlayer( data.playerId )
        let classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( '30195-29.htm' )
        }

        if ( classId === SHILLIEN_KNIGHT && player.getClassId() === ClassIdValues.palusKnight.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_DUTY, MARK_OF_FATE, MARK_OF_WITCHCRAFT ) ) {
                    return this.getPath( '30195-30.htm' )
                }

                return this.getPath( '30195-31.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_DUTY, MARK_OF_FATE, MARK_OF_WITCHCRAFT ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_DUTY, MARK_OF_FATE, MARK_OF_WITCHCRAFT )

                await player.setClassId( SHILLIEN_KNIGHT )
                player.setBaseClass( SHILLIEN_KNIGHT )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, SHADOW_ITEM_EXCHANGE_COUPON_C_GRADE, rewardAmount )
                return this.getPath( '30195-32.htm' )
            }

            return this.getPath( '30195-33.htm' )
        }

        if ( classId === BLADEDANCER && player.getClassId() === ClassIdValues.palusKnight.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_CHALLENGER, MARK_OF_FATE, MARK_OF_DUELIST ) ) {
                    return this.getPath( '30195-34.htm' )
                }

                return this.getPath( '30195-35.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_CHALLENGER, MARK_OF_FATE, MARK_OF_DUELIST ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_CHALLENGER, MARK_OF_FATE, MARK_OF_DUELIST )

                await player.setClassId( BLADEDANCER )
                player.setBaseClass( BLADEDANCER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, SHADOW_ITEM_EXCHANGE_COUPON_C_GRADE, rewardAmount )
                return this.getPath( '30195-36.htm' )
            }

            return this.getPath( '30195-37.htm' )
        }

        if ( classId === ABYSS_WALKER && player.getClassId() === ClassIdValues.assassin.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_SEEKER, MARK_OF_FATE, MARK_OF_SEARCHER ) ) {
                    return this.getPath( '30195-38.htm' )
                }

                return this.getPath( '30195-39.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_SEEKER, MARK_OF_FATE, MARK_OF_SEARCHER ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SEEKER, MARK_OF_FATE, MARK_OF_SEARCHER )

                await player.setClassId( ABYSS_WALKER )
                player.setBaseClass( ABYSS_WALKER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, SHADOW_ITEM_EXCHANGE_COUPON_C_GRADE, rewardAmount )
                return this.getPath( '30195-40.htm' )
            }

            return this.getPath( '30195-41.htm' )
        }

        if ( classId === PHANTOM_RANGER && player.getClassId() === ClassIdValues.assassin.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_SEEKER, MARK_OF_FATE, MARK_OF_SAGITTARIUS ) ) {
                    return this.getPath( '30195-42.htm' )
                }

                return this.getPath( '30195-43.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_SEEKER, MARK_OF_FATE, MARK_OF_SAGITTARIUS ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SEEKER, MARK_OF_FATE, MARK_OF_SAGITTARIUS )

                await player.setClassId( PHANTOM_RANGER )
                player.setBaseClass( PHANTOM_RANGER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, SHADOW_ITEM_EXCHANGE_COUPON_C_GRADE, rewardAmount )
                return this.getPath( '30195-44.htm' )
            }

            return this.getPath( '30195-45.htm' )
        }

        if ( classId === SPELLHOWLER && player.getClassId() === ClassIdValues.darkWizard.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_SCHOLAR, MARK_OF_FATE, MARK_OF_MAGUS ) ) {
                    return this.getPath( '30195-46.htm' )
                }

                return this.getPath( '30195-47.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_SCHOLAR, MARK_OF_FATE, MARK_OF_MAGUS ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SCHOLAR, MARK_OF_FATE, MARK_OF_MAGUS )

                await player.setClassId( SPELLHOWLER )
                player.setBaseClass( SPELLHOWLER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, SHADOW_ITEM_EXCHANGE_COUPON_C_GRADE, 15 )
                return this.getPath( '30195-48.htm' )
            }

            return this.getPath( '30195-49.htm' )
        }

        if ( classId === PHANTOM_SUMMONER && player.getClassId() === ClassIdValues.darkWizard.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_SCHOLAR, MARK_OF_FATE, MARK_OF_SUMMONER ) ) {
                    return this.getPath( '30195-50.htm' )
                }

                return this.getPath( '30195-51.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_SCHOLAR, MARK_OF_FATE, MARK_OF_SUMMONER ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SCHOLAR, MARK_OF_FATE, MARK_OF_SUMMONER )

                await player.setClassId( PHANTOM_SUMMONER )
                player.setBaseClass( PHANTOM_SUMMONER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, SHADOW_ITEM_EXCHANGE_COUPON_C_GRADE, 15 )
                return this.getPath( '30195-52.htm' )
            }

            return this.getPath( '30195-53.htm' )
        }

        if ( classId === SHILLIEN_ELDER && player.getClassId() === ClassIdValues.shillienOracle.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, MARK_OF_PILGRIM, MARK_OF_FATE, MARK_OF_REFORMER ) ) {
                    return this.getPath( '30195-54.htm' )
                }

                return this.getPath( '30195-55.htm' )
            }

            if ( QuestHelper.hasQuestItems( player, MARK_OF_PILGRIM, MARK_OF_FATE, MARK_OF_REFORMER ) ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_PILGRIM, MARK_OF_FATE, MARK_OF_REFORMER )

                await player.setClassId( SHILLIEN_ELDER )
                player.setBaseClass( SHILLIEN_ELDER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, SHADOW_ITEM_EXCHANGE_COUPON_C_GRADE, 15 )
                return this.getPath( '30195-56.htm' )
            }

            return this.getPath( '30195-57.htm' )
        }
    }
}