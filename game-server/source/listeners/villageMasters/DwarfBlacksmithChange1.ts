import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { QuestHelper } from '../helpers/QuestHelper'
import _ from 'lodash'

const npcIds: Array<number> = [
    30499, // Tapoy
    30504, // Mendio
    30595, // Opix
    32093, // Bolin
]

const FINAL_PASS_CERTIFICATE = 1635
const ARTISAN = ClassIdValues.artisan.id

const minimumLevel = 20
const rewardItemId = 8869
const rewardAmount = 15

export class DwarfBlacksmithChange1 extends ListenerLogic {
    constructor() {
        super( 'DwarfBlacksmithChange1', 'quests/villageMasters/DwarfBlacksmithChange1.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/DwarfBlacksmithChange1'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '30499-01.htm':
            case '30499-02.htm':
            case '30499-03.htm':
            case '30499-04.htm':
            case '30504-01.htm':
            case '30504-02.htm':
            case '30504-03.htm':
            case '30504-04.htm':
            case '30595-01.htm':
            case '30595-02.htm':
            case '30595-03.htm':
            case '30595-04.htm':
            case '32093-01.htm':
            case '32093-02.htm':
            case '32093-03.htm':
            case '32093-04.htm':
                return this.getPath( data.eventName )

            case '56':
                return this.getRequestedClassChange( data )
        }
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc: L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( player.isInCategory( CategoryType.WARSMITH_GROUP ) ) {
            return this.getPath( `${ npc.getId() }-01.htm` )
        }

        return this.getPath( `${ npc.getId() }-05.htm` )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.SECOND_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-06.htm` )
        }

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-07.htm` )
        }

        if ( player.isInCategory( CategoryType.FOURTH_CLASS_GROUP ) ) {
            return this.getPath( '30499-12.htm' )
        }

        if ( classId === ARTISAN && player.getClassId() === ClassIdValues.dwarvenFighter.id ) {
            if ( player.getLevel() < minimumLevel ) {
                if ( QuestHelper.hasQuestItems( player, FINAL_PASS_CERTIFICATE ) ) {
                    return this.getPath( `${ data.characterNpcId }-08.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-09.htm` )
            }

            if ( QuestHelper.hasQuestItems( player, FINAL_PASS_CERTIFICATE ) ) {
                await QuestHelper.takeSingleItem( player, FINAL_PASS_CERTIFICATE, -1 )

                await player.setClassId( ARTISAN )
                player.setBaseClass( ARTISAN )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-10.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-11.htm` )
        }
    }
}