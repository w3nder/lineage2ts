import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { QuestHelper } from '../helpers/QuestHelper'

import _ from 'lodash'

const npcIds: Array<number> = [
    30115, // Jurek
    30174, // Arkenias
    30176, // Valleria
    30694, // Scraide
    30854, // Drikiyan
    31331, // Valdis
    31755, // Halaster
    31996, // Javier
]

const MARK_OF_SCHOLAR = 2674
const MARK_OF_TRUST = 2734
const MARK_OF_MAGUS = 2840
const MARK_OF_WITCHCRAFT = 3307
const MARK_OF_SUMMONER = 3336
const MARK_OF_LIFE = 3140

const SORCERER = ClassIdValues.sorceror.id
const NECROMANCER = ClassIdValues.necromancer.id
const WARLOCK = ClassIdValues.warlock.id
const SPELLSINGER = ClassIdValues.spellsinger.id
const ELEMENTAL_SUMMONER = ClassIdValues.elementalSummoner.id

const rewardItemId = 8870
const rewardAmount = 15
const minimumLevel = 40

export class ElfHumanWizardChange2 extends ListenerLogic {

    constructor() {
        super( 'ElfHumanWizardChange2', 'quests/villageMasters/ElfHumanWizardChange2.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/ElfHumanWizardChange2'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '12':
            case '13':
            case '14':
            case '27':
            case '28':
                return this.getRequestedClassChange( data )
        }

        return this.getPath( data.eventName )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        if ( player.isInCategory( CategoryType.WIZARD_GROUP )
                && player.isInCategory( CategoryType.FOURTH_CLASS_GROUP )
                && ( player.isInCategory( CategoryType.HUMAN_MALL_CLASS ) || player.isInCategory( CategoryType.ELF_MALL_CLASS ) ) ) {
            return this.getPath( '30115-01.htm' )
        }

        if ( player.isInCategory( CategoryType.WIZARD_GROUP )
                && ( player.isInCategory( CategoryType.HUMAN_MALL_CLASS ) || player.isInCategory( CategoryType.ELF_MALL_CLASS ) ) ) {

            if ( [ ClassIdValues.wizard.id, ClassIdValues.sorceror.id, ClassIdValues.necromancer.id, ClassIdValues.warlock.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30115-02.htm' )
            }

            if ( [ ClassIdValues.elvenWizard.id, ClassIdValues.spellsinger.id, ClassIdValues.elementalSummoner.id ].includes( player.getClassId() ) ) {
                return this.getPath( '30115-12.htm' )
            }

            return this.getPath( '30115-19.htm' )
        }

        return this.getPath( '30115-20.htm' )
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( '30115-21.htm' )
        }

        if ( classId === SORCERER && player.getClassId() == ClassIdValues.wizard.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_SCHOLAR, MARK_OF_TRUST, MARK_OF_MAGUS )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30115-22.htm' )
                }

                return this.getPath( '30115-23.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SCHOLAR, MARK_OF_TRUST, MARK_OF_MAGUS )

                await player.setClassId( SORCERER )
                player.setBaseClass( SORCERER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30115-24.htm' )
            }

            return this.getPath( '30115-25.htm' )
        }

        if ( classId === NECROMANCER && player.getClassId() === ClassIdValues.wizard.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_SCHOLAR, MARK_OF_TRUST, MARK_OF_WITCHCRAFT )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30115-26.htm' )
                }

                return this.getPath( '30115-27.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SCHOLAR, MARK_OF_TRUST, MARK_OF_WITCHCRAFT )

                await player.setClassId( NECROMANCER )
                player.setBaseClass( NECROMANCER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30115-28.htm' )
            }

            return this.getPath( '30115-29.htm' )
        }

        if ( classId === WARLOCK && player.getClassId() === ClassIdValues.wizard.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_SCHOLAR, MARK_OF_TRUST, MARK_OF_SUMMONER )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30115-30.htm' )
                }

                return this.getPath( '30115-31.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SCHOLAR, MARK_OF_TRUST, MARK_OF_SUMMONER )

                await player.setClassId( WARLOCK )
                player.setBaseClass( WARLOCK )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30115-32.htm' )
            }

            return this.getPath( '30115-33.htm' )
        }

        if ( classId === SPELLSINGER && player.getClassId() === ClassIdValues.elvenWizard.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_SCHOLAR, MARK_OF_LIFE, MARK_OF_MAGUS )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30115-34.htm' )
                }

                return this.getPath( '30115-35.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SCHOLAR, MARK_OF_LIFE, MARK_OF_MAGUS )

                await player.setClassId( SPELLSINGER )
                player.setBaseClass( SPELLSINGER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30115-36.htm' )
            }

            return this.getPath( '30115-37.htm' )
        }

        if ( classId === ELEMENTAL_SUMMONER && player.getClassId() === ClassIdValues.elvenWizard.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, MARK_OF_SCHOLAR, MARK_OF_LIFE, MARK_OF_SUMMONER )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( '30115-38.htm' )
                }

                return this.getPath( '30115-39.htm' )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeMultipleItems( player, -1, MARK_OF_SCHOLAR, MARK_OF_LIFE, MARK_OF_SUMMONER )

                await player.setClassId( ELEMENTAL_SUMMONER )
                player.setBaseClass( ELEMENTAL_SUMMONER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( '30115-40.htm' )
            }

            return this.getPath( '30115-41.htm' )
        }
    }
}