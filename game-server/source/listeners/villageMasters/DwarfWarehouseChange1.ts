import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ClassIdValues } from '../../gameService/models/base/ClassId'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { CategoryType } from '../../gameService/enums/CategoryType'
import { QuestHelper } from '../helpers/QuestHelper'

import _ from 'lodash'

const npcIds: Array<number> = [
    30498, // Moke
    30503, // Rikadio
    30594, // Ranspo
    32092, // Alder
]

const RING_OF_RAVEN = 1642

const rewardItemId = 8869
const rewardAmount = 15

const SCAVENGER = ClassIdValues.scavenger.id
const minimumLevel = 20

export class DwarfWarehouseChange1 extends ListenerLogic {

    constructor() {
        super( 'DwarfWarehouseChange1', 'quests/villageMasters/DwarfWarehouseChange1.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/village_master/DwarfWarehouseChange1'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        switch ( data.eventName ) {
            case '30498-01.htm':
            case '30498-02.htm':
            case '30498-03.htm':
            case '30498-04.htm':
            case '30503-01.htm':
            case '30503-02.htm':
            case '30503-03.htm':
            case '30503-04.htm':
            case '30594-01.htm':
            case '30594-02.htm':
            case '30594-03.htm':
            case '30594-04.htm':
            case '32092-01.htm':
            case '32092-02.htm':
            case '32092-03.htm':
            case '32092-04.htm':
                return this.getPath( data.eventName )

            case '54':
                return this.getRequestedClassChange( data )
        }
    }

    async getRequestedClassChange( data: NpcGeneralEvent ): Promise<string> {
        const player = L2World.getPlayer( data.playerId )
        const classId: number = _.parseInt( data.eventName )

        if ( player.isInCategory( CategoryType.SECOND_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-06.htm` )
        }

        if ( player.isInCategory( CategoryType.THIRD_CLASS_GROUP ) ) {
            return this.getPath( `${ data.characterNpcId }-07.htm` )
        }

        if ( player.isInCategory( CategoryType.FOURTH_CLASS_GROUP ) ) {
            return this.getPath( '30498-12.htm' )
        }

        if ( classId == SCAVENGER && player.getClassId() === ClassIdValues.dwarvenFighter.id ) {
            let hasRequiredItems: boolean = QuestHelper.hasQuestItems( player, RING_OF_RAVEN )
            if ( player.getLevel() < minimumLevel ) {
                if ( hasRequiredItems ) {
                    return this.getPath( `${ data.characterNpcId }-08.htm` )
                }

                return this.getPath( `${ data.characterNpcId }-09.htm` )
            }

            if ( hasRequiredItems ) {
                await QuestHelper.takeSingleItem( player, RING_OF_RAVEN, -1 )

                await player.setClassId( SCAVENGER )
                player.setBaseClass( SCAVENGER )
                player.broadcastUserInfo()

                await QuestHelper.giveSingleItem( player, rewardItemId, rewardAmount )
                return this.getPath( `${ data.characterNpcId }-10.htm` )
            }

            return this.getPath( `${ data.characterNpcId }-11.htm` )
        }
    }
}