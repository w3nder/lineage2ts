export enum QuestVariables {
    secondClassDiamondReward = '2-class-diamonds',
    newbieRewards = 'newbieRewards',
    guideMission = 'guideMission'
}

export const DefaultEnchantLevel = -1