import { ListenerDescription, ListenerLogic } from '../../gameService/models/ListenerLogic'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import {
    AttackableAttackedEvent,
    AttackableKillEvent, EventType,
    NpcApproachedForTalkEvent,
    NpcGeneralEvent,
    NpcSeeSkillEvent,
    NpcTalkEvent,
} from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { QuestHelper } from './QuestHelper'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { MagicSkillUse, MagicSkillUseWithCharacters } from '../../gameService/packets/send/MagicSkillUse'
import { NpcSay } from '../../gameService/packets/send/builder/NpcSay'
import { L2Object } from '../../gameService/models/L2Object'
import { ILocational } from '../../gameService/models/Location'
import { GeneralHelper } from '../../gameService/helpers/GeneralHelper'
import { PlayerGroupCache } from '../../gameService/cache/PlayerGroupCache'
import { L2Party } from '../../gameService/models/L2Party'
import { ConfigManager } from '../../config/ConfigManager'
import { QuestStateValues } from '../../gameService/models/quest/State'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const classIds: Array<number> = [
    0x7f,
    0x80,
    0x82,
    0x05,
    0x14,
    0x15,
    0x02,
    0x03,
    0x2e,
    0x30,
    0x33,
    0x34,
    0x08,
    0x17,
    0x24,
    0x09,
    0x18,
    0x25,
    0x10,
    0x11,
    0x1e,
    0x0c,
    0x1b,
    0x28,
    0x0e,
    0x1c,
    0x29,
    0x0d,
    0x06,
    0x22,
    0x21,
    0x2b,
    0x37,
    0x39,
]

const eventNames = {
    mobOneTimer: 'mone.t',
    mobOneDespawned: 'mone.d',
    archonDespawned: 'ard',
    mobTwoTimerOne: 'mtwo.tone',
    mobTwoTimerTwo: 'mtwo.ttwo',
    mobTwoTimerThree: 'mtwo.tthree',
    mobTwoDespawned: 'mtwo.d',
    mobThreeTimer: 'mthree.t',
    mobThreeDespawned: 'mthree.d',
}

const variableNames = {
    spawnedStatus: 'ss',
    questStatus: 'qs',
    questValue: 'qv',
    questSelection: 'qsl',
    mobTwoId: 'mti',
    guardianAngelKills: 'gak',
}

export const SagaPlaceholders = {
    playerName: '%playerName%',
}

export const SagaLogicIds = {
    killIds: [
        // Archon Minion npcIds
        21646,
        21647,
        21648,
        21649,
        21650,
        21651,

        // Halisha npcIds
        18212,
        18214,
        18215,
        18216,
        18218,

        // Guardian npcIds
        27214,
        27215,
        27216,
    ],
}

const textLines: Array<string> = [
    `${ SagaPlaceholders.playerName }! Pursued to here! However, I jumped out of the Banshouren boundaries! You look at the giant as the sign of power!`,
    '... Oh ... good! So it was ... let\'s begin!',
    'I do not have the patience ..! I have been a giant force ...! Cough chatter ah ah ah!',
    `Paying homage to those who disrupt the orderly will be ${ SagaPlaceholders.playerName }'s death!`,
    'Now, my soul freed from the shackles of the millennium, Halixia, to the back side I come ...',
    'Why do you interfere others\' battles?',
    'This is a waste of time.. Say goodbye...!',
    '...That is the enemy',
    `...Goodness! ${ SagaPlaceholders.playerName } you are still looking?`,
    `${ SagaPlaceholders.playerName } ... Not just to whom the victory. Only personnel involved in the fighting are eligible to share in the victory.`,
    `Your sword is not an ornament. Don't you think, ${ SagaPlaceholders.playerName }?`,
    'Goodness! I no longer sense a battle there now.',
    'let...',
    'Only engaged in the battle to bar their choice. Perhaps you should regret.',
    'The human nation was foolish to try and fight a giant\'s strength.',
    'Must...Retreat... Too...Strong.',
    `${ SagaPlaceholders.playerName }. Defeat...by...retaining...and...Mo...Hacker`,
    '....! Fight...Defeat...It...Fight...Defeat...It...',
]

export abstract class SagaLogic extends ListenerLogic {
    npcPlayerMapping: { [ objectId: number ]: number } = {}

    constructor( name: string, path: string ) {
        super( name, path )

        this.questItemIds = _.filter( this.getAllItemIds(), ( itemId: number, index: number ): boolean => {
            if ( [ 0, 2 ].includes( index ) ) {
                return false
            }

            return itemId !== 0
        } )
    }

    getQuestStartIds(): Array<number> {
        return [ this.getNpcId( 0 ) ]
    }

    getTalkIds(): Array<number> {
        return this.getAllNpcIds()
    }

    getAttackableKillIds(): Array<number> {
        return [
            ...SagaLogicIds.killIds,
            ...this.getAllMobIds(),
        ]
    }

    getAttackableAttackIds(): Array<number> {
        return [
            this.getMobId( 1 ),
            this.getMobId( 2 ),
        ]
    }

    getCustomListeners(): Array<ListenerDescription> {
        let listeners = []

        // TODO : add skill id association
        if ( this.getMobId( 1 ) ) {
            listeners.push( {
                registerType: ListenerRegisterType.NpcId,
                eventType: EventType.NpcSeeSkill,
                method: this.onNpcSeeSkill.bind( this ),
                ids: [ this.getMobId( 1 ) ],
                targetIds: null
            } )
        }

        return listeners
    }

    getApproachedForTalkIds(): Array<number> {
        return [ this.getNpcId( 4 ) ]
    }

    getState( player: L2PcInstance ): QuestState {
        if ( player.getClassId() === classIds[ this.getId() - 67 ] ) {
            return QuestStateCache.getQuestState( player.getObjectId(), this.getName(), false )
        }

        return null
    }

    getPlayerState( npcObjectId: number ): QuestState {
        let playerId: number = this.npcPlayerMapping[ npcObjectId ]
        if ( !playerId ) {
            return
        }

        return QuestStateCache.getQuestState( playerId, this.getName(), false )
    }

    getMinimumLevel(): number {
        return 76
    }

    getItemId( index: number ): number {
        return this.getAllItemIds()[ index ]
    }

    getMobId( index: number ): number {
        return this.getAllMobIds()[ index ]
    }

    getNpcId( index: number ): number {
        return this.getAllMobIds()[ index ]
    }

    getSpawnLocation( index: number ): ILocational {
        return this.getAllSpawnLocations()[ index ]
    }

    getText( index: number ): string {
        return this.getAllTextLines()[ index ]
    }

    abstract getClassId( player: L2PcInstance ): number

    abstract getPreviousClassId( player: L2PcInstance ): number

    abstract getAllNpcIds(): Array<number>

    abstract getAllMobIds(): Array<number>

    abstract getAllItemIds(): Array<number>

    abstract getAllSpawnLocations(): Array<ILocational>

    abstract getPathPrefix(): string

    getAllTextLines(): Array<string> {
        return textLines
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        switch ( data.eventName ) {
            case '0-011.htm':
            case '0-012.htm':
            case '0-013.htm':
            case '0-014.htm':
            case '0-015.htm':
                break

            case 'accept':
                state.startQuest()
                await QuestHelper.giveSingleItem( player, this.getItemId( 10 ), 1 )

                return this.getPath( '0-03.htm' )

            case '0-1':
                if ( player.getLevel() < this.getMinimumLevel() ) {
                    if ( state.isCreated() ) {
                        await state.exitQuest( true )
                    }

                    return this.getPath( '0-02.htm' )
                }

                return this.getPath( '0-05.htm' )

            case '0-2':
                if ( player.getLevel() < this.getMinimumLevel() ) {
                    await QuestHelper.takeSingleItem( player, this.getItemId( 10 ), -1 )
                    state.setConditionWithSound( 20, true )

                    return this.getPath( '0-08.htm' )
                }

                await QuestHelper.takeSingleItem( player, this.getItemId( 10 ), -1 )
                await QuestHelper.addExpAndSp( player, 2299404, 0 )
                await QuestHelper.giveAdena( player, 5000000, true )
                await QuestHelper.giveSingleItem( player, 6622, 1 )

                let futureClassId: number = this.getClassId( player )
                let previousClassId: number = this.getPreviousClassId( player )

                await player.setClassId( futureClassId )
                if ( !player.isSubClassActive() && player.getBaseClass() === previousClassId ) {
                    player.setBaseClass( futureClassId )
                }

                player.broadcastUserInfo()
                this.showSkillCast( data.characterId, player, 4339 )
                await state.exitQuest( false )

                return this.getPath( '0-07.htm' )

            case '1-3':
                state.setConditionWithSound( 3 )
                return this.getPath( '1-05.htm' )

            case '1-4':
                await QuestHelper.takeSingleItem( player, this.getItemId( 0 ), 1 )
                state.setConditionWithSound( 4 )

                if ( this.getItemId( 11 ) ) {
                    await QuestHelper.takeSingleItem( player, this.getItemId( 11 ), 1 )
                }

                await QuestHelper.giveSingleItem( player, this.getItemId( 1 ), 1 )
                return this.getPath( '1-06.htm' )

            case '2-1':
                state.setConditionWithSound( 2 )
                return this.getPath( '2-05.htm' )

            case '2-2':
                state.setConditionWithSound( 5 )
                await QuestHelper.takeSingleItem( player, this.getItemId( 1 ), 1 )
                await QuestHelper.giveSingleItem( player, this.getItemId( 4 ), 1 )

                return this.getPath( '2-06.htm' )

            case '3-5':
                return this.getPath( '3-07.htm' )

            case '3-6':
                state.setConditionWithSound( 11 )
                return this.getPath( '3-02.htm' )

            case '3-7':
                state.setConditionWithSound( 12 )
                return this.getPath( '3-03.htm' )

            case '3-8':
                state.setConditionWithSound( 13 )
                await QuestHelper.takeSingleItem( player, this.getItemId( 2 ), 1 )
                await QuestHelper.giveSingleItem( player, this.getItemId( 7 ), 1 )

                return this.getPath( '3-08.htm' )

            case '4-1':
                return this.getPath( '4-010.htm' )

            case '4-2':
                await QuestHelper.giveSingleItem( player, this.getItemId( 9 ), 1 )
                state.setConditionWithSound( 18, true )

                return this.getPath( '4-011.htm' )

            case '4-3':
                await QuestHelper.giveSingleItem( player, this.getItemId( 9 ), 1 )
                state.setConditionWithSound( 18, true )
                state.setVariable( variableNames.questStatus, 1 )

                this.showNpcChat( npc, this.getText( 13 ), player )
                await this.removePlayerAssociation( npc )

                this.stopQuestTimer( eventNames.mobTwoDespawned, data.characterId, data.playerId )

                return

            case '5-1':
                state.setConditionWithSound( 6, true )

                await QuestHelper.takeSingleItem( player, this.getItemId( 4 ), 1 )
                this.showSkillCast( data.characterId, player, 4546 )

                return this.getPath( '5-02.htm' )

            case '6-1':
                state.setConditionWithSound( 8, true )
                state.setVariable( variableNames.spawnedStatus, 0 )
                await QuestHelper.takeSingleItem( player, this.getItemId( 5 ), 1 )
                this.showSkillCast( data.characterId, player, 4546 )

                return this.getPath( '6-03.htm' )

            case '7-1':
                switch ( state.getVariable( variableNames.spawnedStatus ) ) {
                    case 0:
                        let mob: L2Npc = QuestHelper.addSpawnAtLocation( this.getMobId( 0 ), this.getSpawnLocation( 0 ), false, 0 )

                        state.setVariable( variableNames.spawnedStatus, 1 )

                        this.startQuestTimer( eventNames.mobOneTimer, 500, mob.getObjectId(), 0 )
                        this.startQuestTimer( eventNames.mobOneDespawned, 300000, mob.getObjectId() )

                        this.addPlayerAssociation( data.playerId, npc )

                        return this.getPath( '7-02.htm' )

                    case 1:
                        return this.getPath( '7-03.htm' )
                }

                return this.getPath( '7-04.htm' )

            case '7-2':
                state.setConditionWithSound( 10, true )

                await QuestHelper.takeSingleItem( player, this.getItemId( 6 ), 1 )
                this.showSkillCast( data.characterId, player, 4546 )

                return this.getPath( '7-06.htm' )

            case '8-1':
                state.setConditionWithSound( 14, true )

                await QuestHelper.takeSingleItem( player, this.getItemId( 7 ), 1 )
                this.showSkillCast( data.characterId, player, 4546 )

                return this.getPath( '8-02.htm' )

            case '9-1':
                state.setConditionWithSound( 17, true )
                state.setVariable( variableNames.questStatus, 0 )

                state.setVariable( variableNames.questSelection, 0 )

                await QuestHelper.takeSingleItem( player, this.getItemId( 8 ), 1 )
                this.showSkillCast( data.characterId, player, 4546 )

                return this.getPath( '9-03.htm' )

            case '10-1':
                switch ( state.getVariable( variableNames.questStatus ) ) {
                    case 0:
                        let npcOne: L2Npc = QuestHelper.addSpawnAtLocation( this.getMobId( 2 ), this.getSpawnLocation( 1 ), false, 0 )
                        let npcTwo: L2Npc = QuestHelper.addSpawnAtLocation( this.getMobId( 4 ), this.getSpawnLocation( 2 ), false, 0 )

                        this.addPlayerAssociation( data.playerId, npcOne, npcTwo )

                        state.setVariable( variableNames.mobTwoId, npcTwo.getObjectId() )
                        state.setVariable( variableNames.questStatus, 1 )
                        state.setVariable( variableNames.questValue, 45 )

                        this.startQuestTimer( eventNames.mobThreeTimer, 500, npcOne.getObjectId(), 0, true )
                        this.startQuestTimer( eventNames.mobThreeDespawned, 59000, npcOne.getObjectId(), 0 )
                        this.startQuestTimer( eventNames.mobTwoTimerOne, 500, npcTwo.getObjectId(), 0 )
                        this.startQuestTimer( eventNames.mobTwoDespawned, 60000, npcTwo.getObjectId(), 0 )

                        return this.getPath( '10-02.htm' )

                    case 45:
                        return this.getPath( '10-03.htm' )
                }

                return this.getPath( '10-04.htm' )

            case '10-2':
                state.setConditionWithSound( 19, true )
                await QuestHelper.takeSingleItem( player, this.getItemId( 9 ), 1 )
                this.showSkillCast( data.characterId, player, 4546 )

                return this.getPath( '10-06.htm' )

            case '11-9':
                state.setConditionWithSound( 15 )
                return this.getPath( '11-03.htm' )

            case eventNames.mobOneTimer:
                this.showNpcChat( npc, this.getText( 0 ), player )
                return

            case eventNames.mobOneDespawned:
                this.showNpcChat( npc, this.getText( 1 ), player )
                await this.removePlayerAssociation( npc )

                state.setVariable( variableNames.spawnedStatus, 0 )
                return

            case eventNames.archonDespawned:
                this.showNpcChat( npc, this.getText( 6 ), player )
                await this.removePlayerAssociation( npc )

                state.setVariable( variableNames.spawnedStatus, 0 )
                return

            case eventNames.mobThreeTimer:
                if ( this.npcPlayerMapping[ state.getVariable( variableNames.mobTwoId ) ] !== data.playerId ) {
                    return
                }

                let mobTwo: L2Npc = L2World.getObjectById( state.getVariable( variableNames.mobTwoId ) ) as L2Npc
                if ( !mobTwo || !GeneralHelper.checkIfInShortRadius( 2000, mobTwo, npc, true ) ) {
                    return
                }

                AIEffectHelper.notifyAttacked( npc, mobTwo, 99999 )
                AIEffectHelper.notifyAttacked( mobTwo, npc, 99999 )

                this.showNpcChat( npc, this.getText( 14 ), player )
                this.stopQuestTimer( eventNames.mobThreeTimer, data.characterId, 0 )

                return

            case eventNames.mobThreeDespawned:
                this.showNpcChat( npc, this.getText( 15 ), player )
                await this.removePlayerAssociation( npc )

                state.setVariable( variableNames.questStatus, 2 )
                return

            case eventNames.mobTwoTimerOne:
                this.showNpcChat( npc, this.getText( 7 ), player )

                this.startQuestTimer( eventNames.mobTwoTimerTwo, 1500, data.characterId, 0 )
                if ( state.getVariable( variableNames.questValue ) === 45 ) {
                    state.setVariable( variableNames.questValue, 0 )
                }

                return

            case eventNames.mobTwoTimerTwo:
                this.showNpcChat( npc, this.getText( 8 ), player )
                this.startQuestTimer( eventNames.mobTwoTimerThree, 10000, data.characterId, 0 )

                return

            case eventNames.mobTwoTimerThree:
                if ( state.getVariable( variableNames.questStatus ) !== 0 ) {
                    return
                }

                this.startQuestTimer( eventNames.mobTwoTimerThree, 13000, data.characterId, 0 )
                this.showNpcChat( npc, this.getText( Math.random() > 0.5 ? 9 : 10 )
                        , player )

                return

            case eventNames.mobTwoDespawned:
                state.incrementVariable( variableNames.questValue, 1 )
                let status: number = state.getVariable( variableNames.questStatus )
                if ( [ 1, 2 ].includes( status ) || state.getVariable( variableNames.questValue ) > 3 ) {
                    state.setVariable( variableNames.questStatus, 0 )

                    this.showNpcChat( npc, this.getText( status === 1 ? 11 : 12 )
                            , player )

                    await this.removePlayerAssociation( npc )
                    return
                }

                this.startQuestTimer( eventNames.mobTwoDespawned, 1000, data.characterId, 0 )
                return
        }

        return this.getPath( data.eventName )
    }

    showSkillCast( whoIsCastingId: number, player: L2PcInstance, skillId: number ): void {
        BroadcastHelper.dataBasedOnVisibility( player, MagicSkillUseWithCharacters( player, player, skillId, 1, 6000, 1 ) )
        BroadcastHelper.dataBasedOnVisibility( player, MagicSkillUse( whoIsCastingId, whoIsCastingId, skillId, 1, 6000, 1 ) )
    }

    showNpcChat( npc: L2Npc, message: string, player: L2PcInstance ): void {
        return BroadcastHelper.dataInLocation( npc,
                NpcSay.fromNpcText(
                npc.getObjectId(),
                NpcSayType.All,
                npc.getId(),
                message.replace( SagaPlaceholders.playerName, player.getName() ) ).getBuffer(), npc.getBroadcastRadius() )
    }

    removePlayerAssociation( npc: L2Npc ): Promise<void> {
        _.unset( this.npcPlayerMapping, npc.getObjectId() )
        return npc.deleteMe()
    }

    addPlayerAssociation( playerId: number, ...objects: Array<L2Object> ): void {
        let listener = this
        _.each( objects, ( npc: L2Object ) => {
            listener.npcPlayerMapping[ npc.getObjectId() ] = playerId
        } )
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        let associatedPlayerState: QuestState = this.getPlayerState( data.targetId )
        if ( !associatedPlayerState ) {
            return
        }

        let state: QuestState = QuestStateCache.getQuestState( data.attackerPlayerId, this.getName(), false )
        switch ( associatedPlayerState.getCondition() ) {
            case 15:
                if ( data.targetNpcId !== this.getMobId( 1 ) ) {
                    return
                }

                if ( !state
                        || state.getPlayerId() !== associatedPlayerState.getPlayerId()
                        || ( state.getPlayerId() === associatedPlayerState.getPlayerId()
                                && PlayerGroupCache.getParty( data.attackerPlayerId ) ) ) {

                    let npc = L2World.getObjectById( data.targetId ) as L2Npc
                    let player = L2World.getPlayer( data.attackerPlayerId )

                    this.showNpcChat( npc, this.getText( 5 ), player )
                    await this.removePlayerAssociation( npc )

                    this.stopQuestTimer( eventNames.archonDespawned, data.targetId, associatedPlayerState.getPlayerId() )
                    associatedPlayerState.setVariable( variableNames.spawnedStatus, 0 )
                }

                return

            case 17:
                if ( data.targetNpcId !== this.getMobId( 2 ) ) {
                    return
                }

                if ( state && state.getPlayerId() === associatedPlayerState.getPlayerId() ) {
                    let statusValue = state.getVariable( variableNames.questStatus ) + 1
                    let npc = L2World.getObjectById( data.targetId ) as L2Npc
                    let player = L2World.getPlayer( data.attackerPlayerId )

                    if ( statusValue === 1 ) {
                        this.showNpcChat( npc, this.getText( 16 ), player )
                    }

                    if ( statusValue > 15 ) {
                        statusValue = 1

                        this.showNpcChat( npc, this.getText( 17 ), player )
                        await this.removePlayerAssociation( npc )

                        this.stopQuestTimer( eventNames.mobThreeDespawned, data.targetId, associatedPlayerState.getPlayerId() )
                        state.setVariable( variableNames.questSelection, 1 )
                    }

                    state.setVariable( variableNames.questStatus, statusValue )
                }

                return
        }
    }

    async onApproachedForTalk( data: NpcApproachedForTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )
        if ( !state ) {
            return
        }

        switch ( state.getCondition() ) {
            case 17:
                let associatedState: QuestState = this.getPlayerState( data.characterId )
                if ( !associatedState ) {
                    break
                }

                let player = L2World.getPlayer( data.playerId )
                player.setLastQuestNpc( data.characterId )

                let selectionValue: number = state.getVariable( variableNames.questSelection )
                let statusValue: number = state.getVariable( variableNames.questStatus )

                if ( state.getPlayerId() === associatedState.getPlayerId() ) {
                    switch ( statusValue ) {
                        case 0:
                            if ( selectionValue === 1 ) {
                                return this.getPath( '4-04.htm' )
                            }

                            return this.getPath( '4-01.htm' )

                        case 1:
                            if ( selectionValue === 1 ) {
                                return this.getPath( '4-06.htm' )
                            }

                            return this.getPath( '4-03.htm' )
                    }

                    break
                }

                switch ( statusValue ) {
                    case 0:
                        if ( selectionValue === 1 ) {
                            return this.getPath( '4-05.htm' )
                        }

                        return this.getPath( '4-02.htm' )

                    case 1:
                        if ( selectionValue === 1 ) {
                            return this.getPath( '4-07.htm' )
                        }

                        break
                }

                break

            case 18:
                return this.getPath( '4-08.htm' )
        }

        ( L2World.getObjectById( data.characterId ) as L2Npc ).showChatWindowDefault( L2World.getPlayer( data.playerId ) )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        if ( data.npcId >= 21646 || data.npcId < 21652 ) {
            await this.processMinionKill( data )
            return
        }

        if ( [ 18212,
              18214,
              18215,
              18216,
              18218 ].includes( data.npcId ) ) {
            await this.processHalishaKill( data )
            return
        }

        if ( data.npcId >= 27214 && data.npcId < 27217 ) {
            await this.processGuardianKill( data )
            return
        }

        if ( data.npcId === this.getMobId( 2 ) ) {
            return
        }

        let associatedState: QuestState = this.getPlayerState( data.targetId )
        if ( !associatedState ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), false )

        if ( data.npcId === this.getMobId( 0 ) ) {
            if ( state
                    && state.isCondition( 8 )
                    && !PlayerGroupCache.getParty( data.playerId )
                    && state.getPlayerId() === associatedState.getPlayerId() ) {
                let player = L2World.getPlayer( data.playerId )

                this.showNpcChat( npc, this.getText( 12 ), player )
                await QuestHelper.giveSingleItem( player, this.getItemId( 6 ), 1 )

                state.setConditionWithSound( 9, true )
            }

            this.stopQuestTimer( eventNames.mobOneDespawned, data.targetId, associatedState.getPlayerId() )
            associatedState.setVariable( variableNames.spawnedStatus, 0 )
            await this.removePlayerAssociation( npc )

            return
        }

        if ( data.npcId === this.getMobId( 1 ) ) {
            if ( state && state.isCondition( 15 ) && !PlayerGroupCache.getParty( data.playerId ) ) {
                let player = L2World.getPlayer( data.playerId )
                let isSameState = state.getPlayerId() === associatedState.getPlayerId()

                this.showNpcChat( npc, this.getText( isSameState ? 4 : 5 ), player )

                if ( isSameState ) {
                    await QuestHelper.takeSingleItem( player, this.getItemId( 3 ), -1 )
                    await QuestHelper.giveSingleItem( player, this.getItemId( 8 ), 1 )

                    state.setConditionWithSound( 16, true )
                }
            }

            this.stopQuestTimer( eventNames.archonDespawned, data.targetId, associatedState.getPlayerId() )
            associatedState.setVariable( variableNames.spawnedStatus, 0 )
            await this.removePlayerAssociation( npc )

            return
        }
    }

    async processMinionKill( data: AttackableKillEvent ): Promise<void> {
        let party: L2Party = PlayerGroupCache.getParty( data.playerId )
        let actionPlayer: L2PcInstance = L2World.getPlayer( data.playerId )
        let listener = this

        let playerStates: Array<QuestState> = _.reduce( party ? party.getMembers() : [ data.playerId ], ( availableStates: Array<QuestState>, playerId: number ): Array<QuestState> => {
            let partyMember = L2World.getPlayer( playerId )
            if ( !partyMember ) {
                return availableStates
            }

            let state: QuestState = listener.getState( partyMember )

            if ( !state || !state.isCondition( 15 ) || state.getVariable( variableNames.spawnedStatus ) !== 0 ) {
                return availableStates
            }

            if ( !GeneralHelper.checkIfInRange( ConfigManager.character.getPartyRange2(), actionPlayer, partyMember, true ) ) {
                return availableStates
            }

            availableStates.push( state )

            return availableStates
        }, [] )

        if ( playerStates.length === 0 ) {
            return
        }

        let state: QuestState = _.sample( playerStates )
        let player: L2PcInstance = state.getPlayer()
        let itemId = this.getItemId( 3 )

        if ( QuestHelper.getQuestItemsCount( player, itemId ) >= 700 ) {
            await QuestHelper.takeSingleItem( player, itemId, 20 )

            let npc: L2Npc = QuestHelper.addSpawnAtLocation( this.getMobId( 1 ), player )
            this.npcPlayerMapping[ npc.getObjectId() ] = player.getObjectId()

            state.setVariable( variableNames.spawnedStatus, 1 )
            this.startQuestTimer( eventNames.archonDespawned, 600000, npc.getObjectId(), 0 )

            this.showNpcChat( npc, this.getText( 13 ), player )
            AIEffectHelper.notifyAttacked( npc, player, 99999 )

            return
        }

        return QuestHelper.rewardSingleQuestItem( player, itemId, _.random( 1, 4 ), data.isChampion )
    }

    async processHalishaKill( data: AttackableKillEvent ): Promise<void> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = this.getState( player )

        if ( !state || !state.isCondition( 15 ) ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc

        this.showNpcChat( npc, this.getText( 4 ), player )
        await QuestHelper.takeSingleItem( player, this.getItemId( 3 ), -1 )
        await QuestHelper.giveSingleItem( player, this.getItemId( 8 ), 1 )

        state.setConditionWithSound( 16, true )
    }

    async processGuardianKill( data: AttackableKillEvent ): Promise<void> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = this.getState( player )

        if ( !state || !state.isCondition( 6 ) ) {
            return
        }

        let kills: number = state.getVariable( variableNames.guardianAngelKills ) || 0
        if ( kills < 9 ) {
            state.incrementVariable( variableNames.guardianAngelKills, 1 )
            return
        }

        await QuestHelper.giveSingleItem( player, this.getItemId( 5 ), 1 )
        state.setConditionWithSound( 7, true )
    }

    async onNpcSeeSkill( data: NpcSeeSkillEvent ): Promise<void> {
        if ( !this.npcPlayerMapping[ data.receiverId ] || this.npcPlayerMapping[ data.receiverId ] !== data.playerId ) {
            return
        }

        let targetIds = new Set<number>( data.targetIds )
        if ( !targetIds.has( this.npcPlayerMapping[ data.receiverId ] ) && !targetIds.has( data.receiverId ) ) {
            return
        }

        let associatedState: QuestState = this.getPlayerState( data.receiverId )
        if ( !associatedState ) {
            return
        }

        let npc = L2World.getObjectById( data.receiverId ) as L2Npc

        this.stopQuestTimer( eventNames.archonDespawned, data.receiverId, associatedState.getPlayerId() )
        associatedState.setVariable( variableNames.spawnedStatus, 0 )

        this.showNpcChat( npc, this.getText( 5 ), L2World.getPlayer( data.playerId ) )
        return this.removePlayerAssociation( npc )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, this.getName(), true )
        let player = L2World.getPlayer( data.playerId )

        switch ( state.getState() ) {
            case QuestStateValues.CREATED:
                if ( data.characterNpcId === this.getNpcId( 0 ) ) {
                    return this.getPath( '0-01.htm' )
                }

                break

            case QuestStateValues.STARTED:
                switch ( state.getCondition() ) {
                    case 1:
                        if ( data.characterNpcId === this.getNpcId( 0 ) ) {
                            return this.getPath( '0-04.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 2 ) ) {
                            return this.getPath( '2-01.htm' )
                        }

                        break

                    case 2:
                        if ( data.characterNpcId === this.getNpcId( 2 ) ) {
                            return this.getPath( '2-02.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 1 ) ) {
                            return this.getPath( '1-01.htm' )
                        }
                        break

                    case 3:
                        if ( data.characterNpcId === this.getNpcId( 1 ) && QuestHelper.hasQuestItem( player, this.getItemId( 0 ) ) ) {
                            if ( this.getItemId( 11 ) === 0 || QuestHelper.hasQuestItem( player, this.getItemId( 11 ) ) ) {
                                return this.getPath( '1-03.htm' )
                            }

                            return this.getPath( '1-02.htm' )
                        }

                        break

                    case 4:
                        if ( data.characterNpcId === this.getNpcId( 1 ) ) {
                            return this.getPath( '1-04.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 2 ) ) {
                            return this.getPath( '2-03.htm' )
                        }

                        break

                    case 5:
                        if ( data.characterNpcId === this.getNpcId( 2 ) ) {
                            return this.getPath( '2-04.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 5 ) ) {
                            return this.getPath( '5-01.htm' )
                        }

                        break

                    case 6:
                        if ( data.characterNpcId === this.getNpcId( 5 ) ) {
                            return this.getPath( '5-03.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 6 ) ) {
                            return this.getPath( '6-01.htm' )
                        }

                        break

                    case 7:
                        if ( data.characterNpcId === this.getNpcId( 6 ) ) {
                            return this.getPath( '6-02.htm' )
                        }

                        break

                    case 8:
                        if ( data.characterNpcId === this.getNpcId( 6 ) ) {
                            return this.getPath( '6-04.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 7 ) ) {
                            return this.getPath( '7-01.htm' )
                        }

                        break

                    case 9:
                        if ( data.characterNpcId === this.getNpcId( 7 ) ) {
                            return this.getPath( '7-05.htm' )
                        }

                        break

                    case 10:
                        if ( data.characterNpcId === this.getNpcId( 7 ) ) {
                            return this.getPath( '7-07.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 3 ) ) {
                            return this.getPath( '3-01.htm' )
                        }

                        break

                    case 11:
                    case 12:
                        if ( data.characterNpcId === this.getNpcId( 3 ) ) {
                            if ( QuestHelper.hasQuestItem( player, this.getItemId( 2 ) ) ) {
                                return this.getPath( '3-05.htm' )
                            }

                            return this.getPath( '3-04.htm' )
                        }

                        break

                    case 13:
                        if ( data.characterNpcId === this.getNpcId( 3 ) ) {
                            return this.getPath( '3-06.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 8 ) ) {
                            return this.getPath( '8-01.htm' )
                        }

                        break

                    case 14:
                        if ( data.characterNpcId === this.getNpcId( 8 ) ) {
                            return this.getPath( '8-03.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 11 ) ) {
                            return this.getPath( '11-01.htm' )
                        }

                        break

                    case 15:
                        if ( data.characterNpcId === this.getNpcId( 11 ) ) {
                            return this.getPath( '11-02.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 9 ) ) {
                            return this.getPath( '9-01.htm' )
                        }

                        break

                    case 16:
                        if ( data.characterNpcId === this.getNpcId( 9 ) ) {
                            return this.getPath( '9-02.htm' )
                        }

                        break

                    case 17:
                        if ( data.characterNpcId === this.getNpcId( 9 ) ) {
                            return this.getPath( '9-04.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 10 ) ) {
                            return this.getPath( '10-01.htm' )
                        }

                        break

                    case 18:
                        if ( data.characterNpcId === this.getNpcId( 10 ) ) {
                            return this.getPath( '10-05.htm' )
                        }

                        break

                    case 19:
                        if ( data.characterNpcId === this.getNpcId( 10 ) ) {
                            return this.getPath( '10-07.htm' )
                        }

                        if ( data.characterNpcId === this.getNpcId( 0 ) ) {
                            return this.getPath( '0-06.htm' )
                        }

                        break

                    case 20:
                        if ( data.characterNpcId !== this.getNpcId( 0 ) ) {
                            break
                        }

                        if ( player.getLevel() >= this.getMinimumLevel() ) {
                            let classId = this.getClassId( player )

                            if ( classId < 131 || classId > 135 ) {
                                await QuestHelper.addExpAndSp( player, 2299404, 0 )
                                await QuestHelper.giveAdena( player, 5000000, true )
                                await QuestHelper.rewardSingleItem( player, 6622, 1 ) // Giant's Codex

                                await player.setClassId( classId )
                                if ( !player.isSubClassActive() && player.getBaseClass() === this.getPreviousClassId( player ) ) {
                                    player.setBaseClass( classId )
                                }

                                this.showSkillCast( data.characterId, player, 4339 )
                                await state.exitQuest( false )

                                player.broadcastUserInfo()
                            }

                            return this.getPath( '0-09.htm' )
                        }

                        return this.getPath( '0-010.htm' )
                }

                break

            case QuestStateValues.COMPLETED:
                if ( data.characterNpcId === this.getNpcId( 0 ) ) {
                    return QuestHelper.getAlreadyCompletedMessagePath()
                }

                break
        }

        return QuestHelper.getNoQuestMessagePath()
    }
}