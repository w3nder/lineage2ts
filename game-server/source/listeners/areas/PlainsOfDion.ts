import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { AttackableAttackedEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcVariablesManager } from '../../gameService/variables/NpcVariablesManager'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { L2Object } from '../../gameService/models/L2Object'
import { L2MonsterInstance } from '../../gameService/models/actor/instance/L2MonsterInstance'
import { QuestHelper } from '../helpers/QuestHelper'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const npcIds: Array<number> = [
    21104, // Delu Lizardman Supplier
    21105, // Delu Lizardman Special Agent
    21107, // Delu Lizardman Commander
]

const sayOnAttack: Array<number> = [
    NpcStringIds.S1_HOW_DARE_YOU_INTERRUPT_OUR_FIGHT_HEY_GUYS_HELP,
    NpcStringIds.S1_HEY_WERE_HAVING_A_DUEL_HERE,
    NpcStringIds.THE_DUEL_IS_OVER_ATTACK,
    NpcStringIds.FOUL_KILL_THE_COWARD,
    NpcStringIds.HOW_DARE_YOU_INTERRUPT_A_SACRED_DUEL_YOU_MUST_BE_TAUGHT_A_LESSON,
]

const sayOnAssist: Array<number> = [
    NpcStringIds.DIE_YOU_COWARD,
    NpcStringIds.KILL_THE_COWARD,
    NpcStringIds.WHAT_ARE_YOU_LOOKING_AT,
]

export class PlainsOfDion extends ListenerLogic {
    constructor() {
        super( 'PlainsOfDion', 'listeners/areas/PlainsOfDion.ts' )
    }

    getAttackableAttackIds(): Array<number> {
        return npcIds
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        if ( !NpcVariablesManager.get( data.targetId, this.getName() ) ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let index = _.random( sayOnAttack.length - 1 )
        let player = L2World.getPlayer( data.attackerPlayerId )

        BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, sayOnAttack[ index ], index < 2 ? player.getName() : undefined )

        L2World.forEachObjectByPredicate( npc, npc.getTemplate().getClanHelpRange(), ( object: L2Object ): boolean => {
            if ( object.isMonster()
                    && npcIds.includes( object.getId() )
                    && !( object as L2MonsterInstance ).isAttackingNow()
                    && !( object as L2MonsterInstance ).isDead() ) {
                QuestHelper.addAttackDesire( ( object as L2MonsterInstance ), player )
                BroadcastHelper.broadcastNpcSayStringId( object as L2Npc, NpcSayType.NpcAll, _.sample( sayOnAssist ) )

                return true
            }

            return false
        } )

        NpcVariablesManager.set( data.targetId, this.getName(), true )
    }
}