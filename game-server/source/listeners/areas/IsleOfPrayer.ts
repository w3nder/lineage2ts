import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AttackableKillEvent } from '../../gameService/models/events/EventType'
import { QuestHelper } from '../helpers/QuestHelper'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import _ from 'lodash'

const YELLOW_SEED_OF_EVIL_SHARD = 9593
const GREEN_SEED_OF_EVIL_SHARD = 9594
const BLUE_SEED_OF_EVIL_SHARD = 9595
const RED_SEED_OF_EVIL_SHARD = 9596

type RewardData = [ number, number ] // itemId, chance
const monsterRewardChances: { [ npcId: number ]: RewardData } = {
    22257: [ YELLOW_SEED_OF_EVIL_SHARD, 2087 ], // Island Guardian
    22258: [ YELLOW_SEED_OF_EVIL_SHARD, 2147 ], // White Sand Mirage
    22259: [ YELLOW_SEED_OF_EVIL_SHARD, 2642 ], // Muddy Coral
    22260: [ YELLOW_SEED_OF_EVIL_SHARD, 2292 ], // Kleopora
    22261: [ GREEN_SEED_OF_EVIL_SHARD, 1171 ], // Seychelles
    22262: [ GREEN_SEED_OF_EVIL_SHARD, 1173 ], // Naiad
    22263: [ GREEN_SEED_OF_EVIL_SHARD, 1403 ], // Sonneratia
    22264: [ GREEN_SEED_OF_EVIL_SHARD, 1207 ], // Castalia
    22265: [ RED_SEED_OF_EVIL_SHARD, 575 ], // Chrysocolla
    22266: [ RED_SEED_OF_EVIL_SHARD, 493 ], // Pythia
    22267: [ RED_SEED_OF_EVIL_SHARD, 770 ], // Dark Water Dragon
    22268: [ BLUE_SEED_OF_EVIL_SHARD, 987 ], // Shade
    22269: [ BLUE_SEED_OF_EVIL_SHARD, 995 ], // Shade
    22270: [ BLUE_SEED_OF_EVIL_SHARD, 1008 ], // Water Dragon Detractor
    22271: [ BLUE_SEED_OF_EVIL_SHARD, 1008 ], // Water Dragon Detractor
}

export class IsleOfPrayer extends ListenerLogic {
    constructor() {
        super( 'IsleOfPrayer', 'listeners/areas/IsleOfPrayer.ts' )
    }

    getAttackableKillIds(): Array<number> {
        return _.keys( monsterRewardChances ).map( value => _.parseInt( value ) )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let [ itemId, chance ] = monsterRewardChances[ data.npcId ]
        if ( _.random( 10000 ) > QuestHelper.getAdjustedChance( itemId, chance, data.isChampion ) ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        npc.dropSingleItem( itemId, QuestHelper.getAdjustedAmount( itemId, 1, data.isChampion ), data.playerId )
    }
}