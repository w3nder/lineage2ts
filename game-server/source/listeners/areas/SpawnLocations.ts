import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const monsterLocations: { [ npcId: number ]: Array<Location> } = {
    // Keltas
    22341: [
        new Location( -27136, 250938, -3523 ),
        new Location( -29658, 252897, -3523 ),
        new Location( -27237, 251943, -3527 ),
        new Location( -28868, 250113, -3479 ),
    ],

    // Keymaster
    22361: [
        new Location( 14091, 250533, -1940 ),
        new Location( 15762, 252440, -2015 ),
        new Location( 19836, 256212, -2090 ),
        new Location( 21940, 254107, -2010 ),
        new Location( 17299, 252943, -2015 ),
    ],

    // Typhoon
    25539: [
        new Location( -20641, 255370, -3235 ),
        new Location( -16157, 250993, -3058 ),
        new Location( -18269, 250721, -3151 ),
        new Location( -16532, 254864, -3223 ),
        new Location( -19055, 253489, -3440 ),
        new Location( -9684, 254256, -3148 ),
        new Location( -6209, 251924, -3189 ),
        new Location( -10547, 251359, -2929 ),
        new Location( -7254, 254997, -3261 ),
        new Location( -4883, 253171, -3322 ),
    ],

    // Mutated Elpy
    25604: [
        new Location( -46080, 246368, -14183 ),
        new Location( -44816, 246368, -14183 ),
        new Location( -44224, 247440, -14184 ),
        new Location( -44896, 248464, -14183 ),
        new Location( -46064, 248544, -14183 ),
        new Location( -46720, 247424, -14183 ),
    ],
}

export class SpawnLocations extends ListenerLogic {
    constructor() {
        super( 'SpawnLocations', 'listeners/areas/SpawnLocations.ts' )
    }

    getSpawnIds(): Array<number> {
        return _.keys( monsterLocations ).map( value => _.parseInt( value ) )
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let location: Location = _.sample( monsterLocations[ data.npcId ] )
        let npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( !npc.isInsideRadius( location, 200 ) ) {
            npc.getSpawnLocation().setXYZCoordinates( location.getX(), location.getY(), location.getZ() )
            npc.setXYZ( location.getX(), location.getY(), location.getZ() )
        }
    }
}