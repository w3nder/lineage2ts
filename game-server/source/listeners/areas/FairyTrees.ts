import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { SkillHolder } from '../../gameService/models/holders/SkillHolder'
import { AttackableKillEvent, NpcSpawnEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2Playable } from '../../gameService/models/actor/L2Playable'
import { QuestHelper } from '../helpers/QuestHelper'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import aigle from 'aigle'

const mobIds: Array<number> = [
    27185, // Fairy Tree of Wind
    27186, // Fairy Tree of Star
    27187, // Fairy Tree of Twilight
    27188, // Fairy Tree of Abyss
]

const SOUL_GUARDIAN = 27189
const VENOMOUS_POISON = new SkillHolder( 4243 )
const requiredDistance = 1500

export class FairyTrees extends ListenerLogic {
    constructor() {
        super( 'FairyTrees', 'listeners/areas/FairyTrees.ts' )
    }

    getAttackableKillIds(): Array<number> {
        return mobIds
    }

    getSpawnIds(): Array<number> {
        return mobIds
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        npc.setIsImmobilized( true )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        let player = L2World.getPlayer( data.playerId )

        if ( !npc || !player || npc.calculateDistance( player, true ) > requiredDistance ) {
            return
        }

        let attacker = L2World.getObjectById( data.attackerId ) as L2Playable
        await aigle.resolve( 20 ).times( async () => {
            let mob = QuestHelper.addSpawnAtLocation( SOUL_GUARDIAN, npc, false, 30000 )
            AIEffectHelper.notifyAttacked( mob, attacker )

            if ( Math.random() > 0.5 ) {
                mob.setTarget( attacker )
                await mob.doCastWithHolder( VENOMOUS_POISON )
            }
        } )
    }
}