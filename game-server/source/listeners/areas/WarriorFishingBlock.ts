import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcGeneralEvent,
    NpcSpawnEvent,
} from '../../gameService/models/events/EventType'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { L2World } from '../../gameService/L2World'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2Character } from '../../gameService/models/actor/L2Character'
import { AIEffectHelper } from '../../gameService/aicontroller/helpers/AIEffectHelper'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const npcIds: Array<number> = [
    18319, // Caught Frog
    18320, // Caught Undine
    18321, // Caught Rakul
    18322, // Caught Sea Giant
    18323, // Caught Sea Horse Soldier
    18324, // Caught Homunculus
    18325, // Caught Flava
    18326, // Caught Gigantic Eye
]

const sayOnSpawn: Array<number> = [
    NpcStringIds.CROAK_CROAK_FOOD_LIKE_S1_IN_THIS_PLACE,
    NpcStringIds.S1_HOW_LUCKY_I_AM,
    NpcStringIds.PRAY_THAT_YOU_CAUGHT_A_WRONG_FISH_S1,
]

const sayOnAttack: Array<number> = [
    NpcStringIds.DO_YOU_KNOW_WHAT_A_FROG_TASTES_LIKE,
    NpcStringIds.I_WILL_SHOW_YOU_THE_POWER_OF_A_FROG,
    NpcStringIds.I_WILL_SWALLOW_AT_A_MOUTHFUL,
]

const sayOnDeath: Array<number> = [
    NpcStringIds.UGH_NO_CHANCE_HOW_COULD_THIS_ELDER_PASS_AWAY_LIKE_THIS,
    NpcStringIds.CROAK_CROAK_A_FROG_IS_DYING,
    NpcStringIds.A_FROG_TASTES_BAD_YUCK,
]

const chanceOfShout: number = 0.33
const despawnTime: number = 50 * 1000

const eventNames = {
    spawn: 'sp',
    despawn: 'dsp',
}

export class WarriorFishingBlock extends ListenerLogic {
    constructor() {
        super( 'WarriorFishingBlock', 'listeners/areas/WarriorFishingBlock.ts' )
    }

    getAttackableAttackIds(): Array<number> {
        return npcIds
    }

    getAttackableKillIds(): Array<number> {
        return npcIds
    }

    getSpawnIds(): Array<number> {
        return npcIds
    }

    async onSpawnEvent( data: NpcSpawnEvent ): Promise<void> {
        this.startQuestTimer( eventNames.spawn, 2000, data.characterId, 0 )
    }

    async onAttackableKillEvent( data: AttackableKillEvent ): Promise<string> {
        BroadcastHelper.broadcastNpcSayStringId(
                L2World.getObjectById( data.targetId ) as L2Npc,
                NpcSayType.NpcAll,
                _.sample( sayOnDeath ) )
        this.stopQuestTimer( eventNames.despawn, data.targetId, 0 )
        return
    }

    async onAttackableAttackedEvent( data: AttackableAttackedEvent ): Promise<void> {
        if ( Math.random() > chanceOfShout ) {
            return
        }

        let npc = L2World.getObjectById( data.targetId ) as L2Npc
        return BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, _.sample( sayOnAttack ) )
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let npc = L2World.getObjectById( data.characterId ) as L2Npc
        if ( !npc || npc.isDead() ) {
            return
        }

        switch ( data.eventName ) {
            case eventNames.spawn:
                let target = npc.getTarget() as L2Character
                if ( !target || !target.isPlayer() ) {
                    await npc.deleteMe()
                    return
                }

                BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.NpcAll, _.sample( sayOnSpawn ) )
                AIEffectHelper.notifyAttacked( npc, target, 2000 )

                this.startQuestTimer( eventNames.despawn, despawnTime, data.characterId, 0 )
                return

            case eventNames.despawn:
                await npc.deleteMe()
                return
        }
    }
}