import { InstanceLogic } from '../helpers/InstanceLogic'
import { Location } from '../../gameService/models/Location'
import { InstanceWorld } from '../../gameService/models/instancezone/InstanceWorld'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { NpcTalkEvent } from '../../gameService/models/events/EventType'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import { L2World } from '../../gameService/L2World'

const GINBY = 32566
const startLocation = new Location( -23530, -8963, -5413 )
const instanceTemplateId = 117

export class SecretAreaInTheKeucereusFortress extends InstanceLogic {
    constructor() {
        super( 'SecretAreaInTheKeucereusFortress', 'listeners/instances/SecretAreaInTheKeucereusFortress.ts' )
    }

    getTalkIds(): Array<number> {
        return [
            GINBY,
        ]
    }

    async onEnterInstance( player: L2PcInstance, world: InstanceWorld, isEnteringFirstTime: boolean ): Promise<void> {
        if ( isEnteringFirstTime ) {
            world.addAllowedId( player.getObjectId() )
        }

        return player.teleportToLocation( startLocation, false, world.getInstanceId() )
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let state: QuestState = QuestStateCache.getQuestState( data.playerId, 'Q10270_BirthOfTheSeed', false )
        if ( !state || state.getMemoState() < 5 || state.getMemoState() >= 20 ) {
            return
        }

        await this.enterInstance( L2World.getPlayer( data.playerId ), 'SecretAreaInTheKeucereusFortress', instanceTemplateId )
        if ( state.isMemoState( 5 ) ) {
            state.setMemoState( 10 )
        }

        return this.getPath( '32566-01.html' )
    }

    getPathPrefix(): string {
        return 'data/datapack/instances/SecretAreaInTheKeucereusFortress1'
    }
}