import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import _ from 'lodash'

const coliseumHelper = 32491
const paddies = 32378
const managers: Array<number> = [
    32377,
    32513,
    32514,
    32515,
    32516,
]

const coliseumLocations: Array<Location> = [
    new Location( -81896, -49589, -10352 ),
    new Location( -82271, -49196, -10352 ),
    new Location( -81886, -48784, -10352 ),
    new Location( -81490, -49167, -10352 ),
]

const returnLocations: Array<Location> = [
    new Location( -59161, -56954, -2036 ),
    new Location( -59155, -56831, -2036 ),
    new Location( -59299, -56955, -2036 ),
    new Location( -59224, -56837, -2036 ),
    new Location( -59134, -56899, -2036 ),
]

const managerLocations: Array<[ Location, Location ]> = [
    [
        new Location( -84451, -45452, -10728 ),
        new Location( -84580, -45587, -10728 ),
    ],
    [
        new Location( -86154, -50429, -10728 ),
        new Location( -86118, -50624, -10728 ),
    ],
    [
        new Location( -82009, -53652, -10728 ),
        new Location( -81802, -53665, -10728 ),
    ],
    [
        new Location( -77603, -50673, -10728 ),
        new Location( -77586, -50503, -10728 ),
    ],
    [
        new Location( -79186, -45644, -10728 ),
        new Location( -79309, -45561, -10728 ),
    ],
]

export class TeleportToUndergroundColiseum extends ListenerLogic {
    constructor() {
        super( 'TeleportToUndergroundColiseum', 'listeners/npcTeleports/TeleportToUndergroundColiseum.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ coliseumHelper ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/TeleportToUndergroundColiseum'
    }

    getQuestStartIds(): Array<number> {
        return [
            ...managers,
             coliseumHelper,
             paddies,
        ]
    }

    getTalkIds(): Array<number> {
        return [
            ...managers,
             coliseumHelper,
             paddies,
        ]
    }

    async onApproachedForTalk(): Promise<string> {
        return this.getPath( '32491.htm' )
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {
        if ( data.eventName.endsWith( '.htm' ) ) {
            return this.getPath( data.eventName )
        }

        let player = L2World.getPlayer( data.playerId )

        if ( data.eventName === 'return' ) {
            await player.teleportToLocation( _.sample( returnLocations ), false )
            return
        }

        let locationIndex = parseInt( data.eventName )
        if ( _.isNumber( locationIndex ) ) {
            await player.teleportToLocation( _.sample( managerLocations[ locationIndex - 1 ] ), false )
            return
        }
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( managers.includes( npc.getId() ) ) {
            await player.teleportToLocation( _.sample( returnLocations ), false )
            return
        }

        await player.teleportToLocation( _.sample( coliseumLocations ), false )
    }
}