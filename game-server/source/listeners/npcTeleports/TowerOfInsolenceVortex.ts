import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ItemTypes } from '../../gameService/values/InventoryValues'

const KEPLON = 30949
const EUCLIE = 30950
const PITHGON = 30951
const DIMENSION_VORTEX_1 = 30952
const DIMENSION_VORTEX_2 = 30953
const DIMENSION_VORTEX_3 = 30954

const GREEN_DIMENSION_STONE = 4401
const BLUE_DIMENSION_STONE = 4402
const RED_DIMENSION_STONE = 4403

const TOI_FLOORS = {
    1: new Location( 114356, 13423, -5096 ),
    2: new Location( 114666, 13380, -3608 ),
    3: new Location( 111982, 16028, -2120 ),
    4: new Location( 114636, 13413, -640 ),
    5: new Location( 114152, 19902, 928 ),
    6: new Location( 117131, 16044, 1944 ),
    7: new Location( 113026, 17687, 2952 ),
    8: new Location( 115571, 13723, 3960 ),
    9: new Location( 114649, 14144, 4976 ),
    10: new Location( 118507, 16605, 5984 ),
}

const TOI_FLOOR_ITEMS = {
    1: GREEN_DIMENSION_STONE,
    2: GREEN_DIMENSION_STONE,
    3: GREEN_DIMENSION_STONE,
    4: BLUE_DIMENSION_STONE,
    5: BLUE_DIMENSION_STONE,
    6: BLUE_DIMENSION_STONE,
    7: RED_DIMENSION_STONE,
    8: RED_DIMENSION_STONE,
    9: RED_DIMENSION_STONE,
    10: RED_DIMENSION_STONE,
}

const DIMENSION_TRADE = {
    'GREEN': GREEN_DIMENSION_STONE,
    'BLUE': BLUE_DIMENSION_STONE,
    'RED': RED_DIMENSION_STONE,
}

export class TowerOfInsolenceVortex extends ListenerLogic {
    constructor() {
        super( 'TowerOfInsolenceVortex', 'listeners/npcTeleports/TowerOfInsolenceVortex.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/ToIVortex'
    }

    getQuestStartIds(): Array<number> {
        return [ KEPLON, EUCLIE, PITHGON, DIMENSION_VORTEX_1, DIMENSION_VORTEX_2, DIMENSION_VORTEX_3 ]
    }

    getTalkIds(): Array<number> {
        return [ KEPLON, EUCLIE, PITHGON, DIMENSION_VORTEX_1, DIMENSION_VORTEX_2, DIMENSION_VORTEX_3 ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        if ( parseInt( event, 10 ) > 0 ) {
            let chosenLocation: Location = TOI_FLOORS[ event ]
            let itemId = TOI_FLOOR_ITEMS[ event ]
            if ( QuestHelper.hasQuestItems( player, itemId ) ) {
                await QuestHelper.takeSingleItem( player, itemId )
                await player.teleportToLocation( chosenLocation, true )
                return
            }

            return this.getPath( 'no-stones.htm' )
        }

        if ( [ 'GREEN', 'BLUE', 'RED' ].includes( event ) ) {
            if ( player.getAdena() >= 10000 ) {
                await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 10000 )
                await QuestHelper.giveSingleItem( player, DIMENSION_TRADE[ event ] )
                return
            }

            return this.getPath( `${ data.characterNpcId }no-adena.htm` )
        }
    }
}