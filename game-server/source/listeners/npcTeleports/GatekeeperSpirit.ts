import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { QuestHelper } from '../helpers/QuestHelper'
import { SevenSignsSeal, SevenSignsSide } from '../../gameService/values/SevenSignsValues'
import { SevenSigns } from '../../gameService/directives/SevenSigns'
import { AttackableKillEvent, NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const GATEKEEPER_SPIRIT_ENTER = 31111
const GATEKEEPER_SPIRIT_EXIT = 31112
const LILITH = 25283
const ANAKIM = 25286

const SPAWN_LILITH_GATEKEEPER = new Location( 184410, -10111, -5488 )
const SPAWN_ANAKIM_GATEKEEPER = new Location( 184410, -13102, -5488 )

const TELEPORT_DUSK = new Location( 184464, -13104, -5504 )
const TELEPORT_DAWN = new Location( 184448, -10112, -5504 )
const EXIT = new Location( 182960, -11904, -4897 )

export class GatekeeperSpirit extends ListenerLogic {
    constructor() {
        super( 'GatekeeperSpirit', 'listeners/npcTeleports/GatekeeperSpirit.ts' )
    }

    getAttackableKillIds(): Array<number> {
        return [ LILITH, ANAKIM ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ GATEKEEPER_SPIRIT_ENTER, GATEKEEPER_SPIRIT_EXIT ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/GatekeeperSpirit'
    }

    getQuestStartIds(): Array<number> {
        return [ GATEKEEPER_SPIRIT_ENTER, GATEKEEPER_SPIRIT_EXIT ]
    }

    getTalkIds(): Array<number> {
        return [ GATEKEEPER_SPIRIT_ENTER, GATEKEEPER_SPIRIT_EXIT ]
    }

    async onAttackableKillEvent( data : AttackableKillEvent ): Promise<string> {
        switch ( data.npcId ) {
            case ANAKIM: {
                this.startQuestTimer( 'ANAKIM', 10000, data.targetId, data.playerId )
                break
            }
            case LILITH: {
                this.startQuestTimer( 'LILITH', 10000, data.targetId, data.playerId )
                break
            }
        }

        return
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        switch ( event ) {
            case 'ANAKIM':
                QuestHelper.addSpawnAtLocation( GATEKEEPER_SPIRIT_EXIT, SPAWN_ANAKIM_GATEKEEPER, false, 900000 )
                return

            case 'LILITH':
                QuestHelper.addSpawnAtLocation( GATEKEEPER_SPIRIT_EXIT, SPAWN_LILITH_GATEKEEPER, false, 900000 )
                return

            case 'TeleportIn':
                if ( !SevenSigns.isSealValidationPeriod() ) {
                    return this.getPath( '31111-no.html' )
                }

                if ( !SevenSigns.hasWinningSide( player.getObjectId() ) || SevenSigns.getPlayerSeal( player.getObjectId() ) !== SevenSignsSeal.Avarice ) {
                    return this.getPath( '31111-no.html' )
                }

                let side = SevenSigns.getWinnerSide()

                if ( side === SevenSignsSide.Dusk ) {
                    await player.teleportToLocation( TELEPORT_DUSK, false )
                    return
                }

                if ( side === SevenSignsSide.Dawn ) {
                    await player.teleportToLocation( TELEPORT_DAWN, false )
                    return
                }

                return

            case 'TeleportOut':
                await player.teleportToLocation( EXIT, true )
                return
        }

        return
    }
}