import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { GrandBossManager } from '../../gameService/cache/GrandBossManager'
import { L2CommandChannel } from '../../gameService/models/L2CommandChannel'
import { ConfigManager } from '../../config/ConfigManager'
import { L2Party } from '../../gameService/models/L2Party'
import { L2World } from '../../gameService/L2World'
import { NpcTalkEvent } from '../../gameService/models/events/EventType'
import { AreaCache } from '../../gameService/cache/AreaCache'

const npcId = 32376
const location = new Location( 16342, 209557, -9352 )
const belethBossId = 29118

export class SteelCitadelTeleport extends ListenerLogic {
    constructor() {
        super( 'SteelCitadelTeleport', 'listeners/npcTeleports/SteelCitadelTeleport.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let belethStatus = GrandBossManager.getBossStatus( belethBossId )

        if ( belethStatus === 3 ) {
            return this.getPath( '32376-02.htm' )
        }

        if ( belethStatus > 0 ) {
            return this.getPath( '32376-03.htm' )
        }

        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc
        let channel : L2CommandChannel = player.getParty() ? player.getParty().getCommandChannel() : null

        if ( !channel
                || ( channel.getLeader().getObjectId() !== player.getObjectId() )
                || ( channel.getMemberCount() < ConfigManager.grandBoss.getBelethMinPlayers() ) ) {
            return this.getPath( '32376-02a.htm' )
        }

        let area = AreaCache.getAreaByName( '20_24_beres_restart_01' )
        await GrandBossManager.setBossStatus( belethStatus, 1 )

        channel.getParties().forEach( ( party: L2Party ) => {
            party.getMembers().forEach( ( memberId : number ) => {
                let member = L2World.getPlayer( memberId )
                if ( member && member.isInsideRadiusCoordinates( npc.getX(), npc.getY(), npc.getZ(), 3000, true ) ) {
                    if ( area ) {
                        // TODO: use grandboss manager to record player entry instead of area
                        //area.allowPlayerEntry( member, 30 )
                    }

                    member.teleportToLocation( location, true )
                }
            } )
        } )

        return
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/SteelCitadelTeleport'
    }
}