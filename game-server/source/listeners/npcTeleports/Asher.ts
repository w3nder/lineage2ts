import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ItemTypes } from '../../gameService/values/InventoryValues'

const ASHER = 32714
const LOCATION = new Location( 43835, -47749, -792 )
const amount = 50000

export class Asher extends ListenerLogic {
    constructor() {
        super( 'Asher', 'listeners/npcTeleports/Asher.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ ASHER ]
    }

    getQuestStartIds(): Array<number> {
        return [ ASHER ]
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        if ( event === 'teleport' ) {
            if ( player.getAdena() >= amount ) {
                await QuestHelper.takeSingleItem( player, ItemTypes.Adena, amount )
                await player.teleportToLocation( LOCATION )
                return
            }

            return this.getPath( '32714-02.html' )
        }

        if ( event === '32714-01.html' ) {
            return this.getPath( event )
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/Asher'
    }
}