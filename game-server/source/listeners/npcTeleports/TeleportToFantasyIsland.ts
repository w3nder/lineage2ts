import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { NpcStringIds } from '../../gameService/packets/NpcStringIds'
import { NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { PlayerVariablesManager } from '../../gameService/variables/PlayerVariablesManager'
import _ from 'lodash'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const PADDIES = 32378
const RUNE_TOWNSHIP = new Location( 43835, -47749, -792 )
const RETURN_LOCATIONS: Array<Location> = [
    new Location( -80826, 149775, -3043 ),
    new Location( -12672, 122776, -3116 ),
    new Location( 15670, 142983, -2705 ),
    new Location( 83400, 147943, -3404 ),
    new Location( 111409, 219364, -3545 ),
    new Location( 82956, 53162, -1495 ),
    new Location( 146331, 25762, -2018 ),
    new Location( 116819, 76994, -2714 ),
    new Location( 43835, -47749, -792 ),
    new Location( 147930, -55281, -2728 ),
    new Location( 87386, -143246, -1293 ),
    new Location( 12882, 181053, -3560 ),
]

const ISLE_LOCATIONS: Array<Location> = [
    new Location( -58752, -56898, -2032 ),
    new Location( -59716, -57868, -2032 ),
    new Location( -60691, -56893, -2032 ),
    new Location( -59720, -55921, -2032 ),
]

const TELEPORTERS = {
    30059: 2, // Trisha
    30080: 3, // Clarissa
    30177: 5, // Valentina
    30233: 7, // Esmeralda
    30256: 1, // Bella
    30320: 0, // Richlin
    30848: 6, // Elisa
    30899: 4, // Flauen
    31320: 8, // Ilyana
    31275: 9, // Tatiana
    31964: 10, // Bilia
}

export class TeleportToFantasyIsland extends ListenerLogic {
    constructor() {
        super( 'TeleportToFantasyIsland', 'listeners/npcTeleports/TeleportToFantasyIsland.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ PADDIES, ..._.keys( TELEPORTERS ).map( value => _.parseInt( value ) ) ]
    }

    getTalkIds(): Array<number> {
        return [ PADDIES, ..._.keys( TELEPORTERS ).map( value => _.parseInt( value ) ) ]
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( data.characterNpcId === PADDIES ) {
            let npc: L2Npc = L2World.getObjectById( data.characterId ) as L2Npc
            let returnId = _.defaultTo( PlayerVariablesManager.get( data.playerId, this.name ) as number, -1 )
            let teleportLocation = _.nth( RETURN_LOCATIONS, returnId )
            if ( teleportLocation ) {
                await player.teleportToLocation( teleportLocation, true )
                PlayerVariablesManager.remove( data.playerId, this.name )
                return
            }

            BroadcastHelper.broadcastNpcSayStringId( npc, NpcSayType.All, NpcStringIds.IF_YOUR_MEANS_OF_ARRIVAL_WAS_A_BIT_UNCONVENTIONAL_THEN_ILL_BE_SENDING_YOU_BACK_TO_RUNE_TOWNSHIP_WHICH_IS_THE_NEAREST_TOWN )
            await player.teleportToLocation( RUNE_TOWNSHIP, true )
            return
        }

        await player.teleportToLocation( _.sample( ISLE_LOCATIONS ), true )
        PlayerVariablesManager.set( data.playerId, this.name, TELEPORTERS[ data.characterNpcId ] )
    }
}