import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { QuestHelper } from '../helpers/QuestHelper'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ItemTypes } from '../../gameService/values/InventoryValues'

const npcId = 32632
const level = 75
const location = new Location( -149406, 255247, -80 )

export class Survivor extends ListenerLogic {
    constructor() {
        super( 'Survivor', 'listeners/npcTeleports/Survivor.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onTalkEvent(): Promise<string> {
        return this.getPath( '32632-1.htm' )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/Survivor'
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        if ( '32632-2.htm' === event ) {
            if ( player.getLevel() < level ) {
                return this.getPath( '32632-3.htm' )
            }

            if ( player.getAdena() < 150000 ) {
                return this.getPath( event )
            }

            await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 150000 )
            await player.teleportToLocation( location )
            return
        }

        return this.getPath( event )
    }
}