import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { NpcApproachedForTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

export class StrongholdsTeleports extends ListenerLogic {
    constructor() {
        super( 'StrongholdsTeleports', 'listeners/npcTeleports/StrongholdsTeleports.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [
            32163,
            32181,
            32184,
            32186,
        ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/StrongholdsTeleports'
    }

    async onApproachedForTalk( data : NpcApproachedForTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )

        if ( player.getLevel() < 20 ) {
            return this.getPath( `${ data.characterNpcId }.htm` )
        }

        return this.getPath( `${ data.characterNpcId }-no.htm` )
    }
}