import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { NpcGeneralEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const npcId = 32734
const location: Location = new Location( -180218, 185923, -10576 )
const minimumLevel = 80

export class Klemis extends ListenerLogic {
    constructor() {
        super( 'Klemis', 'listeners/npcTeleports/Klemis.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getApproachedForTalkIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onNpcEvent( data : NpcGeneralEvent ): Promise<string> {
        if ( data.eventName === 'portInside' ) {
            let player = L2World.getPlayer( data.playerId )

            if ( player.getLevel() >= minimumLevel ) {
                await player.teleportToLocation( location )
                return
            }

            return this.getPath( '32734-01.html' )
        }
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/Klemis'
    }
}