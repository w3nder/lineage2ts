import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const ORAHOCHIN = 32111
const GARIACHIN = 32112

const TELEPORT_ORAHOCIN = new Location( 5171, -1889, -3165 )
const TELEPORT_GARIACHIN = new Location( 7651, -5416, -3155 )

export class ElrokiTeleporters extends ListenerLogic {
    constructor() {
        super( 'ElrokiTeleporters', 'listeners/npcTeleports/ElrokiTeleporters.ts' )
    }

    getApproachedForTalkIds(): Array<number> {
        return [ ORAHOCHIN, GARIACHIN ]
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/ElrokiTeleporters'
    }

    getQuestStartIds(): Array<number> {
        return [ ORAHOCHIN, GARIACHIN ]
    }

    getTalkIds(): Array<number> {
        return [ ORAHOCHIN, GARIACHIN ]
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc

        if ( !player.isInCombat() ) {
            await player.teleportToLocation( ( npc.getId() === ORAHOCHIN ) ? TELEPORT_ORAHOCIN : TELEPORT_GARIACHIN )
            return
        }

        return this.getPath( `${ npc.getId() }-no.html` )
    }
}