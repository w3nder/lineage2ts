import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { QuestState } from '../../gameService/models/quest/QuestState'
import { QuestStateLimits, QuestStateValues } from '../../gameService/models/quest/State'
import { QuestHelper } from '../helpers/QuestHelper'
import { SoundPacket } from '../../gameService/packets/send/SoundPacket'
import { SystemMessageBuilder } from '../../gameService/packets/send/SystemMessage'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { ItemTypes } from '../../gameService/values/InventoryValues'
import { QuestStateCache } from '../../gameService/cache/QuestStateCache'
import _ from 'lodash'
import { teleportCharacterToGeometryCoordinates } from '../../gameService/helpers/TeleportHelper'
import { GeometryId } from '../../gameService/enums/GeometryId'

const TOWN_DAWN = [
    31078, 31079, 31080, 31081, 31083, 31084, 31082, 31692, 31694, 31997, 31168,
]

const TOWN_DUSK = [
    31085, 31086, 31087, 31088, 31090, 31091, 31089, 31693, 31695, 31998, 31169,
]

const TEMPLE_PRIEST = [
    31127, 31128, 31129, 31130, 31131, 31137, 31138, 31139, 31140, 31141,
]

const RIFT_POSTERS = [
    31488, 31489, 31490, 31491, 31492, 31493,
]

const TELEPORTERS = [
    31078, 31079, 31080, 31081, 31082, 31083, 31084, 31692, 31694, 31997,
    31168, 31085, 31086, 31087, 31088, 31089, 31090, 31091, 31693, 31695,
    31998, 31169, 31494, 31495, 31496, 31497, 31498, 31499, 31500, 31501,
    31502, 31503, 31504, 31505, 31506, 31507, 31095, 31096, 31097, 31098,
    31099, 31100, 31101, 31102, 31103, 31104, 31105, 31106, 31107, 31108,
    31109, 31110, 31114, 31115, 31116, 31117, 31118, 31119, 31120, 31121,
    31122, 31123, 31124, 31125,
]

const returnLocations: Array<Location> = [
    new Location( -80555, 150337, -3040 ),
    new Location( -13953, 121404, -2984 ),
    new Location( 16354, 142820, -2696 ),
    new Location( 83369, 149253, -3400 ),
    new Location( 111386, 220858, -3544 ),
    new Location( 83106, 53965, -1488 ),
    new Location( 146983, 26595, -2200 ),
    new Location( 148256, -55454, -2779 ),
    new Location( 45664, -50318, -800 ),
    new Location( 86795, -143078, -1341 ),
    new Location( 115136, 74717, -2608 ),
    new Location( -82368, 151568, -3120 ),
    new Location( -14748, 123995, -3112 ),
    new Location( 18482, 144576, -3056 ),
    new Location( 81623, 148556, -3464 ),
    new Location( 112486, 220123, -3592 ),
    new Location( 82819, 54607, -1520 ),
    new Location( 147570, 28877, -2264 ),
    new Location( 149888, -56574, -2979 ),
    new Location( 44528, -48370, -800 ),
    new Location( 85129, -142103, -1542 ),
    new Location( 116642, 77510, -2688 ),
    new Location( -41572, 209731, -5087 ),
    new Location( -52872, -250283, -7908 ),
    new Location( 45256, 123906, -5411 ),
    new Location( 46192, 170290, -4981 ),
    new Location( 111273, 174015, -5437 ),
    new Location( -20604, -250789, -8165 ),
    new Location( -21726, 77385, -5171 ),
    new Location( 140405, 79679, -5427 ),
    new Location( -52366, 79097, -4741 ),
    new Location( 118311, 132797, -4829 ),
    new Location( 172185, -17602, -4901 ),
    new Location( 83000, 209213, -5439 ),
    new Location( -19500, 13508, -4901 ),
    new Location( 12525, -248496, -9580 ),
    new Location( -41561, 209225, -5087 ),
    new Location( 45242, 124466, -5413 ),
    new Location( 110711, 174010, -5439 ),
    new Location( -22341, 77375, -5173 ),
    new Location( -52889, 79098, -4741 ),
    new Location( 117760, 132794, -4831 ),
    new Location( 171792, -17609, -4901 ),
    new Location( 82564, 209207, -5439 ),
    new Location( -41565, 210048, -5085 ),
    new Location( 45278, 123608, -5411 ),
    new Location( 111510, 174013, -5437 ),
    new Location( -21489, 77372, -5171 ),
    new Location( -52016, 79103, -4739 ),
    new Location( 118557, 132804, -4829 ),
    new Location( 172570, -17605, -4899 ),
    new Location( 83347, 209215, -5437 ),
    new Location( 42495, 143944, -5381 ),
    new Location( 45666, 170300, -4981 ),
    new Location( 77138, 78389, -5125 ),
    new Location( 139903, 79674, -5429 ),
    new Location( -20021, 13499, -4901 ),
    new Location( 113418, 84535, -6541 ),
    new Location( -52940, -250272, -7907 ),
    new Location( 46499, 170301, -4979 ),
    new Location( -20280, -250785, -8163 ),
    new Location( 140673, 79680, -5437 ),
    new Location( -19182, 13503, -4899 ),
    new Location( 12837, -248483, -9579 ),
]

const DIMENSIONAL_FRAGMENT = 7079
const variableNames = {
    indexVariableName: 'npcIndex',
}

export class OracleTeleport extends ListenerLogic {
    constructor() {
        super( 'OracleTeleport', 'listeners/npcTeleports/OracleTeleport.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/OracleTeleport'
    }

    getQuestStartIds(): Array<number> {
        return [
            ...RIFT_POSTERS,
            ...TELEPORTERS,
            ...TEMPLE_PRIEST,
            ...TOWN_DAWN,
            ...TOWN_DUSK,
        ]
    }

    getTalkIds(): Array<number> {
        return [
            ...RIFT_POSTERS,
            ...TELEPORTERS,
            ...TEMPLE_PRIEST,
            ...TOWN_DAWN,
            ...TOWN_DUSK,
        ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {
        let state: QuestState = this.getQuestState( data.playerId, false )

        if ( !state ) {
            return
        }

        let player = L2World.getPlayer( data.playerId )

        let normalizedEvent = _.toLower( data.eventName )
        if ( normalizedEvent === 'return' ) {
            await player.teleportToLocation( returnLocations[ state.getVariable( variableNames.indexVariableName ) ] )

            if ( state.getState() === QuestStateValues.STARTED ) {
                if ( TEMPLE_PRIEST.includes( data.characterNpcId ) ) {
                    player.setIsIn7sDungeon( false )
                    await state.exitQuest( true )
                    return
                }

                if ( RIFT_POSTERS.includes( data.characterNpcId ) && state.getState() === QuestStateValues.STARTED ) {
                    await state.exitQuest( true )
                    return this.getPath( 'rift_back.htm' )
                }
            }

            return
        }

        if ( normalizedEvent === 'festival' ) {

            if ( TOWN_DAWN.includes( data.characterNpcId ) ) {
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -80157, 111344, -4901, 0 )
                player.setIsIn7sDungeon( true )
                return
            }

            if ( TOWN_DUSK.includes( data.characterNpcId ) ) {
                await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -81261, 86531, -5157, 0 )
                player.setIsIn7sDungeon( true )
                return
            }

            return this.getPath( 'oracle1.htm' )
        }

        if ( normalizedEvent === 'dimensional' ) {
            await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -114755, -179466, -6752, 0 )
            return this.getPath( 'oracle.htm' )
        }

        if ( normalizedEvent === '5.htm' ) {
            if ( _.isNumber( state.getVariable( variableNames.indexVariableName ) ) ) {
                return this.getPath( '5a.htm' )
            }

            state.setVariable( variableNames.indexVariableName, TELEPORTERS.indexOf( data.characterNpcId ) )
            state.setState( QuestStateValues.STARTED )

            await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -114755, -179466, -6752, 0 )

            return
        }

        if ( normalizedEvent === '6.htm' ) {
            await state.exitQuest( true )
            return this.getPath( '6.htm' )
        }

        if ( normalizedEvent === 'zigurratdimensional' ) {
            let playerLevel = player.getLevel()
            if ( ( playerLevel >= 20 ) && ( playerLevel < 30 ) ) {
                await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 2000 )
            } else if ( ( playerLevel >= 30 ) && ( playerLevel < 40 ) ) {
                await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 4500 )
            } else if ( ( playerLevel >= 40 ) && ( playerLevel < 50 ) ) {
                await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 8000 )
            } else if ( ( playerLevel >= 50 ) && ( playerLevel < 60 ) ) {
                await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 12500 )
            } else if ( ( playerLevel >= 60 ) && ( playerLevel < 70 ) ) {
                await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 18000 )
            } else if ( playerLevel >= 70 ) {
                await QuestHelper.takeSingleItem( player, ItemTypes.Adena, 24500 )
            }

            state.setVariable( variableNames.indexVariableName, TELEPORTERS.indexOf( data.characterNpcId ) )
            state.setState( QuestStateValues.STARTED )

            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ACCEPT )
            await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -114755, -179466, -6752, 0 )

            return this.getPath( 'ziggurat_rift.htm' )
        }

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let state: QuestState = this.getQuestState( data.playerId, true )

        if ( TOWN_DAWN.includes( data.characterNpcId ) ) {
            state.setState( QuestStateValues.STARTED )

            state.setVariable( variableNames.indexVariableName, TELEPORTERS.indexOf( data.characterNpcId ) )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ACCEPT )

            await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -80157, 111344, -4901, 0 )
            player.setIsIn7sDungeon( true )
            return
        }

        if ( TOWN_DUSK.includes( data.characterNpcId ) ) {
            state.setState( QuestStateValues.STARTED )

            state.setVariable( variableNames.indexVariableName, TELEPORTERS.indexOf( data.characterNpcId ) )
            player.sendCopyData( SoundPacket.ITEMSOUND_QUEST_ACCEPT )

            await teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, player, -81261, 86531, -5157, 0 )
            player.setIsIn7sDungeon( true )
            return
        }

        if ( data.characterNpcId >= 31494 && data.characterNpcId <= 31507 ) {
            if ( player.getLevel() < 20 ) {
                await state.exitQuest( true )
                return this.getPath( '1.htm' )
            }

            if ( QuestStateCache.getAllActiveStatesSize( player.getObjectId() ) > 23 ) {
                await state.exitQuest( true )
                return this.getPath( '1a.htm' )
            }

            if ( !QuestHelper.hasQuestItems( player, DIMENSIONAL_FRAGMENT ) ) {
                return this.getPath( '3.htm' )
            }

            state.setState( QuestStateValues.CREATED )
            return this.getPath( '4.htm' )
        }

        if ( ( data.characterNpcId >= 31095 && data.characterNpcId <= 31111 )
                || ( data.characterNpcId >= 31114 && data.characterNpcId <= 31126 ) ) {
            let playerLevel = player.getLevel()
            if ( playerLevel < 20 ) {
                await state.exitQuest( true )
                return this.getPath( 'ziggurat_lowlevel.htm' )
            }

            if ( QuestStateCache.getAllActiveStatesSize( player.getObjectId() ) > QuestStateLimits.MaximumQuestsAllowed ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TOO_MANY_QUESTS ) )
                await state.exitQuest( true )
                return
            }

            if ( !QuestHelper.hasQuestItems( player, DIMENSIONAL_FRAGMENT ) ) {
                await state.exitQuest( true )
                return this.getPath( 'ziggurat_nofrag.htm' )
            }

            if ( ( playerLevel >= 20 && playerLevel < 30 && player.getAdena() < 2000 )
                    || ( playerLevel >= 30 && playerLevel < 40 && player.getAdena() < 4500 )
                    || ( playerLevel >= 40 && playerLevel < 50 && player.getAdena() < 8000 )
                    || ( playerLevel >= 50 && playerLevel < 60 && player.getAdena() < 12500 )
                    || ( playerLevel >= 60 && playerLevel < 70 && player.getAdena() < 18000 )
                    || ( playerLevel >= 70 && player.getAdena() < 24500 ) ) {
                await state.exitQuest( true )
                return this.getPath( 'ziggurat_noadena.htm' )
            }

            return this.getPath( 'ziggurat.htm' )
        }

        return
    }
}