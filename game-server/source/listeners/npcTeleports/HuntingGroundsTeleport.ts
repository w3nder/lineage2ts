import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { L2Npc } from '../../gameService/models/actor/L2Npc'
import { SevenSigns } from '../../gameService/directives/SevenSigns'
import { SevenSignsSeal, SevenSignsSide, SevenSignsValues } from '../../gameService/values/SevenSignsValues'
import { NpcTalkEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'

const npcIds: Array<number> = [
    31078, 31079, 31080, 31081, 31082, 31083, 31084, 31085, 31086, 31087,
    31088, 31089, 31090, 31091, 31168, 31169, 31692, 31693, 31694, 31695,
    31997, 31998,
]

const dawnNpcIds: Array<number> = [
    31078, 31079, 31080, 31081, 31082, 31083, 31084, 31168, 31692, 31694,
    31997,
]

export class HuntingGroundsTeleport extends ListenerLogic {
    constructor() {
        super( 'HuntingGroundsTeleport', 'listeners/npcTeleports/HuntingGroundsTeleport.ts' )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/HuntingGroundsTeleport'
    }

    getQuestStartIds(): Array<number> {
        return npcIds
    }

    getTalkIds(): Array<number> {
        return npcIds
    }

    async onTalkEvent( data : NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        let npc : L2Npc = L2World.getObjectById( data.characterId ) as L2Npc
        let playerCabal = SevenSigns.getPlayerSide( player.getObjectId() )

        if ( playerCabal === SevenSignsSide.None ) {
            return this.getPath( dawnNpcIds.includes( npc.getId() ) ? 'dawn_tele-no.htm' : 'dusk_tele-no.htm' )
        }

        let check = SevenSigns.isSealValidationPeriod()
                && playerCabal === SevenSigns.getWinningSealSide( SevenSignsSeal.Gnosis )
                && SevenSigns.getPlayerSeal( player.getObjectId() ) === SevenSignsSeal.Gnosis

        switch ( npc.getId() ) {
            case 31078:
            case 31085:
                return this.getPath( check ? 'low_gludin.htm' : 'hg_gludin.htm' )

            case 31079:
            case 31086:
                return this.getPath( check ? 'low_gludio.htm' : 'hg_gludio.htm' )

            case 31080:
            case 31087:
                return this.getPath( check ? 'low_dion.htm' : 'hg_dion.htm' )

            case 31081:
            case 31088:
                return this.getPath( check ? 'low_giran.htm' : 'hg_giran.htm' )

            case 31082:
            case 31089:
                return this.getPath( check ? 'low_heine.htm' : 'hg_heine.htm' )

            case 31083:
            case 31090:
                return this.getPath( check ? 'low_oren.htm' : 'hg_oren.htm' )

            case 31084:
            case 31091:
                return this.getPath( check ? 'low_aden.htm' : 'hg_aden.htm' )

            case 31168:
            case 31169:
                return this.getPath( check ? 'low_hw.htm' : 'hg_hw.htm' )

            case 31692:
            case 31693:
                return this.getPath( check ? 'low_goddard.htm' : 'hg_goddard.htm' )

            case 31694:
            case 31695:
                return this.getPath( check ? 'low_rune.htm' : 'hg_rune.htm' )

            case 31997:
            case 31998:
                return this.getPath( check ? 'low_schuttgart.htm' : 'hg_schuttgart.htm' )
        }

        return
    }
}