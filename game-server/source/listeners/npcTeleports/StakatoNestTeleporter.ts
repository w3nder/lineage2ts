import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Location } from '../../gameService/models/Location'
import { L2World } from '../../gameService/L2World'
import { NpcGeneralEvent, NpcTalkEvent } from '../../gameService/models/events/EventType'
import aigle from 'aigle'
import _ from 'lodash'

const locations: Array<Location> = [
    new Location( 80456, -52322, -5640 ),
    new Location( 88718, -46214, -4640 ),
    new Location( 87464, -54221, -5120 ),
    new Location( 80848, -49426, -5128 ),
    new Location( 87682, -43291, -4128 ),
]

const npcId = 32640

export class StakatoNestTeleporter extends ListenerLogic {
    constructor() {
        super( 'StakatoNestTeleporter', 'listeners/npcTeleports/StakatoNestTeleporter.ts' )
    }

    getQuestStartIds(): Array<number> {
        return [ npcId ]
    }

    getTalkIds(): Array<number> {
        return [ npcId ]
    }

    async onNpcEvent( data: NpcGeneralEvent ): Promise<string> {

        let event = data.eventName
        let player = L2World.getPlayer( data.playerId )

        let index = _.parseInt( event ) - 1
        let location = _.nth( locations, index )

        if ( location ) {
            if ( player.getParty() ) {
                await aigle.resolve( player.getParty().getMembers() ).each( ( playerId: number ) => {
                    let member = L2World.getPlayer( playerId )
                    if ( member && member.isInsideRadius( player, 1000, true ) ) {
                        return member.teleportToLocation( location, true )
                    }
                } )
            }

            await player.teleportToLocation( location, false )
            return
        }

        return
    }

    async onTalkEvent( data: NpcTalkEvent ): Promise<string> {
        let player = L2World.getPlayer( data.playerId )
        if ( player.hasQuestCompleted( 'Q00240_ImTheOnlyOneYouCanTrust' ) ) {
            return this.getPath( '32640.htm' )
        }

        return this.getPath( '32640-no.htm' )
    }

    getPathPrefix(): string {
        return 'data/datapack/ai/npc/Teleports/StakatoNestTeleporter'
    }
}