import { ClanHallManagerBypass } from '../bypass/ClanHallManager'
import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { ClanHallAuctioneerBypass } from '../bypass/ClanHallAuctioneer'
import { VillageMasterBypass } from '../bypass/VillageMaster'
import { PropertyDoormanBypass } from '../bypass/PropertyDoorman'
import { SevenSignsPriestBypass } from '../bypass/SevenSignsPriest'
import { DungeonGatekeeperBypass } from '../bypass/DungeonGatekeeper'
import { FortLogisticsBypass } from '../bypass/FortLogistics'
import { PetManagerBypass } from '../bypass/PetManager'
import { TeleporterBypass } from '../bypass/Teleporter'
import { FortManagerBypass } from '../bypass/FortManager'
import { SepulcherBypass } from '../bypass/Sepulcher'

export const Bypasses: Array<ListenerLogic> = [
    new ClanHallManagerBypass(),
    new ClanHallAuctioneerBypass(),
    new VillageMasterBypass(),
    new PropertyDoormanBypass(),
    new SevenSignsPriestBypass(),
    new DungeonGatekeeperBypass(),
    new FortLogisticsBypass(),
    new PetManagerBypass(),
    new TeleporterBypass(),
    new FortManagerBypass(),
    new SepulcherBypass(),
]