import { InstanceLogic } from '../helpers/InstanceLogic'
import { CastleDungeon } from '../instances/CastleDungeon'
import { SecretAreaInTheKeucereusFortress } from '../instances/SecretAreaInTheKeucereusFortress'

export const Instances: Array<InstanceLogic> = [
    new CastleDungeon(),
    new SecretAreaInTheKeucereusFortress()
]