import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { Alliance } from '../villageMasters/Alliance'
import { Clan } from '../villageMasters/Clan'
import { DarkElfChange1 } from '../villageMasters/DarkElfChange1'
import { DarkElfChange2 } from '../villageMasters/DarkElfChange2'
import { DwarfBlacksmithChange1 } from '../villageMasters/DwarfBlacksmithChange1'
import { DwarfBlacksmithChange2 } from '../villageMasters/DwarfBlacksmithChange2'
import { DwarfWarehouseChange1 } from '../villageMasters/DwarfWarehouseChange1'
import { DwarfWarehouseChange2 } from '../villageMasters/DwarfWarehouseChange2'
import { ElfHumanClericChange2 } from '../villageMasters/ElfHumanClericChange2'
import { ElfHumanFighterChange1 } from '../villageMasters/ElfHumanFighterChange1'
import { ElfHumanFighterChange2 } from '../villageMasters/ElfHumanFighterChange2'
import { ElfHumanWizardChange1 } from '../villageMasters/ElfHumanWizardChange1'
import { ElfHumanWizardChange2 } from '../villageMasters/ElfHumanWizardChange2'
import { KamaelChange1 } from '../villageMasters/KamaelChange1'
import { KamaelChange2 } from '../villageMasters/KamaelChange2'
import { OrcChange1 } from '../villageMasters/OrcChange1'
import { OrcChange2 } from '../villageMasters/OrcChange2'

export const VillageMasterQuests : Array<ListenerLogic> = [
        new Alliance(),
        new Clan(),
        new DarkElfChange1(),
        new DarkElfChange2(),
        new DwarfBlacksmithChange1(),
        new DwarfBlacksmithChange2(),
        new DwarfWarehouseChange1(),
        new DwarfWarehouseChange2(),
        new ElfHumanClericChange2(),
        new ElfHumanFighterChange1(),
        new ElfHumanFighterChange2(),
        new ElfHumanWizardChange1(),
        new ElfHumanWizardChange2(),
        new KamaelChange1(),
        new KamaelChange2(),
        new OrcChange1(),
        new OrcChange2()
]