import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { AltarOfSacrifice } from '../areas/AltarOfSacrifice'
import { BeastFarm } from '../areas/BeastFarm'
import { WarriorFishingBlock } from '../areas/WarriorFishingBlock'
import { PlainsOfDion } from '../areas/PlainsOfDion'
import { DragonValley } from '../areas/DragonValley'
import { FairyTrees } from '../areas/FairyTrees'
import { IsleOfPrayer } from '../areas/IsleOfPrayer'
import { SpawnLocations } from '../areas/SpawnLocations'

export const AreasList: Array<ListenerLogic> = [
    new AltarOfSacrifice(),
    new BeastFarm(),
    new WarriorFishingBlock(),
    new PlainsOfDion(),
    new DragonValley(),
    new FairyTrees(),
    new IsleOfPrayer(),
    new SpawnLocations()
]