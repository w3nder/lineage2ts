import { ListenerLogic } from '../../gameService/models/ListenerLogic'
import { PlayerBirthday } from '../tasks/PlayerBirthday'
import { ClanLeaderChange } from '../tasks/ClanLeaderChange'
import { WondrousCubicReuse } from '../tasks/WondrousCubicReuse'

export const Tasks : Array<ListenerLogic> = [
        new PlayerBirthday(),
        new ClanLeaderChange(),
        new WondrousCubicReuse()
]