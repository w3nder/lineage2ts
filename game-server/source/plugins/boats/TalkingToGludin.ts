import { L2ExecutablePlugin, L2ExecutablePluginStatus } from '../L2ExecutablePlugin'
import { L2BoatInstance } from '../../gameService/models/actor/instance/L2BoatInstance'
import { BoatManager, ShipDock } from '../../gameService/instancemanager/BoatManager'
import { ListenerCache } from '../../gameService/cache/ListenerCache'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, VehiclePathEndedEvent } from '../../gameService/models/events/EventType'
import { VehiclePathPoint } from '../../gameService/models/VehiclePathPoint'
import { L2World } from '../../gameService/L2World'
import { ILocational } from '../../gameService/models/Location'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { ConfigManager } from '../../config/ConfigManager'
import { CreatureSay } from '../../gameService/packets/send/builder/CreatureSay'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { PlaySound } from '../../gameService/packets/send/builder/PlaySound'
import { SoundNames } from '../../gameService/packets/send/SoundPacket'
import { PacketHelper } from '../../gameService/packets/PacketVariables'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const dockGludin: VehiclePathPoint = new VehiclePathPoint( -95686, 150514, -3610, 150, 800 )
const dockTalking: VehiclePathPoint = new VehiclePathPoint( -96622, 261660, -3610, 150, 1800 )
const broadcastLocations: Array<ILocational> = [ dockGludin, dockTalking ]

const pathTalkingToGludin: Array<VehiclePathPoint> = [
    new VehiclePathPoint( -121385, 261660, -3610, 180, 800 ),
    new VehiclePathPoint( -127694, 253312, -3610, 200, 800 ),
    new VehiclePathPoint( -129274, 237060, -3610, 250, 800 ),
    new VehiclePathPoint( -114688, 139040, -3610, 200, 800 ),
    new VehiclePathPoint( -109663, 135704, -3610, 180, 800 ),
    new VehiclePathPoint( -102151, 135704, -3610, 180, 800 ),
    new VehiclePathPoint( -96686, 140595, -3610, 180, 800 ),
    new VehiclePathPoint( -95686, 147718, -3610, 180, 800 ),
    new VehiclePathPoint( -95686, 148718, -3610, 180, 800 ),
    new VehiclePathPoint( -95686, 149718, -3610, 150, 800 ),
]

const pathGludinToTalking: Array<VehiclePathPoint> = [
    new VehiclePathPoint( -95686, 155514, -3610, 180, 800 ),
    new VehiclePathPoint( -95686, 185514, -3610, 250, 800 ),
    new VehiclePathPoint( -60136, 238816, -3610, 200, 800 ),
    new VehiclePathPoint( -60520, 259609, -3610, 180, 1800 ),
    new VehiclePathPoint( -65344, 261460, -3610, 180, 1800 ),
    new VehiclePathPoint( -83344, 261560, -3610, 180, 1800 ),
    new VehiclePathPoint( -88344, 261660, -3610, 180, 1800 ),
    new VehiclePathPoint( -92344, 261660, -3610, 150, 1800 ),
    new VehiclePathPoint( -94242, 261659, -3610, 150, 1800 ),
]

let shoutCount = 0
let currentCycle = 0

const ARRIVED_AT_TALKING: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_ARRIVED_AT_TALKING ).getBuffer() )
const ARRIVED_AT_TALKING_2: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_GLUDIN_AFTER_10_MINUTES ).getBuffer() )
const LEAVE_TALKING5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_GLUDIN_IN_5_MINUTES ).getBuffer() )
const LEAVE_TALKING1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_GLUDIN_IN_1_MINUTE ).getBuffer() )
const LEAVE_TALKING1_2: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.MAKE_HASTE_GET_ON_BOAT ).getBuffer() )
const LEAVE_TALKING0: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_SOON_FOR_GLUDIN ).getBuffer() )
const LEAVING_TALKING: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVING_FOR_GLUDIN ).getBuffer() )
const ARRIVED_AT_GLUDIN: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_ARRIVED_AT_GLUDIN ).getBuffer() )
const ARRIVED_AT_GLUDIN_2: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_TALKING_AFTER_10_MINUTES ).getBuffer() )
const LEAVE_GLUDIN5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_TALKING_IN_5_MINUTES ).getBuffer() )
const LEAVE_GLUDIN1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_TALKING_IN_1_MINUTE ).getBuffer() )
const LEAVE_GLUDIN0: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_SOON_FOR_TALKING ).getBuffer() )
const LEAVING_GLUDIN: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVING_FOR_TALKING ).getBuffer() )
const BUSY_TALKING: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_GLUDIN_TALKING_DELAYED ).getBuffer() )
const BUSY_GLUDIN: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_TALKING_GLUDIN_DELAYED ).getBuffer() )

const ARRIVAL_GLUDIN10: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_TALKING_ARRIVE_AT_GLUDIN_10_MINUTES ).getBuffer() )
const ARRIVAL_GLUDIN5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_TALKING_ARRIVE_AT_GLUDIN_5_MINUTES ).getBuffer() )
const ARRIVAL_GLUDIN1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_TALKING_ARRIVE_AT_GLUDIN_1_MINUTE ).getBuffer() )
const ARRIVAL_TALKING10: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_GLUDIN_ARRIVE_AT_TALKING_10_MINUTES ).getBuffer() )
const ARRIVAL_TALKING5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_GLUDIN_ARRIVE_AT_TALKING_5_MINUTES ).getBuffer() )
const ARRIVAL_TALKING1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_GLUDIN_ARRIVE_AT_TALKING_1_MINUTE ).getBuffer() )

export const TalkingToGludin: L2ExecutablePlugin = {
    getId(): string {
        return 'TalkingToGludin Boat'
    },

    getStatus(): L2ExecutablePluginStatus {
        return undefined
    },

    async onEnd(): Promise<void> {
        shoutCount = 0
        currentCycle = 0
    },

    async onStart(): Promise<void> {
        let boat: L2BoatInstance = BoatManager.registerBoat( 1, -96622, 261660, -3610, 32768 )

        if ( !boat ) {
            return
        }

        ListenerCache.registerListener( ListenerRegisterType.WorldObject, EventType.VehiclePathEnded, null, Helper.runCycle, [ boat.getObjectId() ] )

        let data: VehiclePathEndedEvent = {
            objectId: boat.getObjectId(),
        }

        setTimeout( Helper.runCycle, 180000, data )
        BoatManager.dockShip( ShipDock.TalkingIsland, true )
        return
    },
}

const Helper = {
    async runCycle( data: VehiclePathEndedEvent ): Promise<void> {
        let boat: L2BoatInstance = L2World.getObjectById( data.objectId ) as L2BoatInstance

        if ( !boat ) {
            return
        }

        switch ( currentCycle ) {
            case 0:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_TALKING5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 1:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_TALKING1, LEAVE_TALKING1_2 )
                setTimeout( Helper.runCycle, 40000, data )
                break

            case 2:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_TALKING0 )
                setTimeout( Helper.runCycle, 20000, data )
                break

            case 3:
                BoatManager.dockShip( ShipDock.TalkingIsland, false )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVING_TALKING )

                BroadcastHelper.broadcastDataAtLocations( [ dockGludin ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                await boat.payForRide( 1074, 1, -96777, 258970, -3623 )
                await BoatManager.setBoathPath( data.objectId, pathTalkingToGludin )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 4:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GLUDIN10 )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 5:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GLUDIN5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 6:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GLUDIN1 )
                break

            case 7:
                if ( BoatManager.isDockBusy( ShipDock.GludinHarbor ) ) {
                    if ( shoutCount === 0 ) {
                        BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                                ConfigManager.general.getBoatBroadcastRadius(),
                                BUSY_GLUDIN )
                    }

                    shoutCount = ( shoutCount + 1 ) % 35
                    setTimeout( Helper.runCycle, 5000, data )
                    return
                }

                await BoatManager.setBoathPath( data.objectId, [ dockGludin ] )
                break

            case 8:
                BoatManager.dockShip( ShipDock.GludinHarbor, true )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVED_AT_GLUDIN, ARRIVED_AT_GLUDIN_2 )

                BroadcastHelper.broadcastDataAtLocations( [ dockGludin ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 9:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_GLUDIN5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 10:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_GLUDIN1, LEAVE_TALKING1_2 )
                setTimeout( Helper.runCycle, 40000, data )
                break

            case 11:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_GLUDIN0 )
                setTimeout( Helper.runCycle, 20000, data )
                break

            case 12:
                BoatManager.dockShip( ShipDock.GludinHarbor, false )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVING_GLUDIN )

                BroadcastHelper.broadcastDataAtLocations( [ dockGludin ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )

                await boat.payForRide( 1075, 1, -90015, 150422, -3610 )
                await BoatManager.setBoathPath( data.objectId, pathGludinToTalking )
                setTimeout( Helper.runCycle, 150000, data )
                break

            case 13:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_TALKING10 )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 14:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_TALKING5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 15:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_TALKING1 )
                break

            case 16:
                if ( BoatManager.isDockBusy( ShipDock.TalkingIsland ) ) {
                    if ( shoutCount === 0 ) {
                        BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                                ConfigManager.general.getBoatBroadcastRadius(),
                                BUSY_TALKING )
                    }

                    shoutCount = ( shoutCount + 1 ) % 35

                    setTimeout( Helper.runCycle, 5000, data )
                    return
                }

                await BoatManager.setBoathPath( data.objectId, [ dockTalking ] )
                break

            case 17:
                BoatManager.dockShip( ShipDock.TalkingIsland, true )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVED_AT_TALKING, ARRIVED_AT_TALKING_2 )

                BroadcastHelper.broadcastDataAtLocations( [ dockTalking ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                setTimeout( Helper.runCycle, 300000, data )
                break
        }

        shoutCount = 0
        currentCycle = ( currentCycle + 1 ) % 17
    },
}