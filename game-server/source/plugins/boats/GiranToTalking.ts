import { L2ExecutablePlugin, L2ExecutablePluginStatus } from '../L2ExecutablePlugin'
import { L2BoatInstance } from '../../gameService/models/actor/instance/L2BoatInstance'
import { BoatManager, ShipDock } from '../../gameService/instancemanager/BoatManager'
import { ListenerCache } from '../../gameService/cache/ListenerCache'
import { ListenerRegisterType } from '../../gameService/enums/ListenerRegisterType'
import { EventType, VehiclePathEndedEvent } from '../../gameService/models/events/EventType'
import { L2World } from '../../gameService/L2World'
import { VehiclePathPoint } from '../../gameService/models/VehiclePathPoint'
import { ILocational } from '../../gameService/models/Location'
import { CreatureSay } from '../../gameService/packets/send/builder/CreatureSay'
import { SystemMessageIds } from '../../gameService/packets/SystemMessageIdValues'
import { BroadcastHelper } from '../../gameService/helpers/BroadcastHelper'
import { ConfigManager } from '../../config/ConfigManager'
import { PlaySound } from '../../gameService/packets/send/builder/PlaySound'
import { SoundNames } from '../../gameService/packets/send/SoundPacket'
import { PacketHelper } from '../../gameService/packets/PacketVariables'
import { NpcSayType } from '../../gameService/enums/packets/NpcSayType'

const dockTalking: VehiclePathPoint = new VehiclePathPoint( -96622, 261660, -3610, 150, 800 )
const dockGiran: VehiclePathPoint = new VehiclePathPoint( 48950, 190613, -3610, 150, 800 )
const broadcastLocations: Array<ILocational> = [ dockTalking, dockGiran ]

const pathGiranToTalking: Array<VehiclePathPoint> = [
    new VehiclePathPoint( 51914, 189023, -3610, 150, 800 ),
    new VehiclePathPoint( 60567, 189789, -3610, 150, 800 ),
    new VehiclePathPoint( 63732, 197457, -3610, 200, 800 ),
    new VehiclePathPoint( 63732, 219946, -3610, 250, 800 ),
    new VehiclePathPoint( 62008, 222240, -3610, 250, 1200 ),
    new VehiclePathPoint( 56115, 226791, -3610, 250, 1200 ),
    new VehiclePathPoint( 40384, 226432, -3610, 300, 800 ),
    new VehiclePathPoint( 37760, 226432, -3610, 300, 800 ),
    new VehiclePathPoint( 27153, 226791, -3610, 300, 800 ),
    new VehiclePathPoint( 12672, 227535, -3610, 300, 800 ),
    new VehiclePathPoint( -1808, 228280, -3610, 300, 800 ),
    new VehiclePathPoint( -22165, 230542, -3610, 300, 800 ),
    new VehiclePathPoint( -42523, 235205, -3610, 300, 800 ),
    new VehiclePathPoint( -68451, 259560, -3610, 250, 800 ),
    new VehiclePathPoint( -70848, 261696, -3610, 200, 800 ),
    new VehiclePathPoint( -83344, 261610, -3610, 200, 800 ),
    new VehiclePathPoint( -88344, 261660, -3610, 180, 800 ),
    new VehiclePathPoint( -92344, 261660, -3610, 180, 800 ),
    new VehiclePathPoint( -94242, 261659, -3610, 150, 800 ),
]

const pathTalkingToGiran: Array<VehiclePathPoint> = [
    new VehiclePathPoint( -113925, 261660, -3610, 150, 800 ),
    new VehiclePathPoint( -126107, 249116, -3610, 180, 800 ),
    new VehiclePathPoint( -126107, 234499, -3610, 180, 800 ),
    new VehiclePathPoint( -126107, 219882, -3610, 180, 800 ),
    new VehiclePathPoint( -109414, 204914, -3610, 180, 800 ),
    new VehiclePathPoint( -92807, 204914, -3610, 180, 800 ),
    new VehiclePathPoint( -80425, 216450, -3610, 250, 800 ),
    new VehiclePathPoint( -68043, 227987, -3610, 250, 800 ),
    new VehiclePathPoint( -63744, 231168, -3610, 250, 800 ),
    new VehiclePathPoint( -60844, 231369, -3610, 250, 1800 ),
    new VehiclePathPoint( -44915, 231369, -3610, 200, 800 ),
    new VehiclePathPoint( -28986, 231369, -3610, 200, 800 ),
    new VehiclePathPoint( 8233, 207624, -3610, 200, 800 ),
    new VehiclePathPoint( 21470, 201503, -3610, 180, 800 ),
    new VehiclePathPoint( 40058, 195383, -3610, 180, 800 ),
    new VehiclePathPoint( 43022, 193793, -3610, 150, 800 ),
    new VehiclePathPoint( 45986, 192203, -3610, 150, 800 ),
    dockGiran,
]

const ARRIVED_AT_GIRAN: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_ARRIVED_AT_GIRAN ).getBuffer() )
const ARRIVED_AT_GIRAN_2: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_TALKING_AFTER_10_MINUTES ).getBuffer() )
const LEAVE_GIRAN5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_TALKING_IN_5_MINUTES ).getBuffer() )
const LEAVE_GIRAN1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_TALKING_IN_1_MINUTE ).getBuffer() )
const LEAVE_GIRAN0: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_SOON_FOR_TALKING ).getBuffer() )
const LEAVING_GIRAN: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVING_FOR_TALKING ).getBuffer() )
const ARRIVED_AT_TALKING: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_ARRIVED_AT_TALKING ).getBuffer() )
const ARRIVED_AT_TALKING_2: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_GIRAN_AFTER_10_MINUTES ).getBuffer() )
const LEAVE_TALKING5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_GIRAN_IN_5_MINUTES ).getBuffer() )
const LEAVE_TALKING1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_FOR_GIRAN_IN_1_MINUTE ).getBuffer() )
const LEAVE_TALKING0: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVE_SOON_FOR_GIRAN ).getBuffer() )
const LEAVING_TALKING: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_LEAVING_FOR_GIRAN ).getBuffer() )
const BUSY_TALKING: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_GIRAN_TALKING_DELAYED ).getBuffer() )

const ARRIVAL_TALKING15: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_GIRAN_ARRIVE_AT_TALKING_15_MINUTES ).getBuffer() )
const ARRIVAL_TALKING10: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_GIRAN_ARRIVE_AT_TALKING_10_MINUTES ).getBuffer() )
const ARRIVAL_TALKING5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_GIRAN_ARRIVE_AT_TALKING_5_MINUTES ).getBuffer() )
const ARRIVAL_TALKING1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_GIRAN_ARRIVE_AT_TALKING_1_MINUTE ).getBuffer() )
const ARRIVAL_GIRAN20: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_TALKING_ARRIVE_AT_GIRAN_20_MINUTES ).getBuffer() )
const ARRIVAL_GIRAN15: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_TALKING_ARRIVE_AT_GIRAN_15_MINUTES ).getBuffer() )
const ARRIVAL_GIRAN10: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_TALKING_ARRIVE_AT_GIRAN_10_MINUTES ).getBuffer() )
const ARRIVAL_GIRAN5: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_TALKING_ARRIVE_AT_GIRAN_5_MINUTES ).getBuffer() )
const ARRIVAL_GIRAN1: Buffer = PacketHelper.preservePacket( CreatureSay.fromSystemMessage( 0, NpcSayType.Boat, 801, SystemMessageIds.FERRY_FROM_TALKING_ARRIVE_AT_GIRAN_1_MINUTE ).getBuffer() )

let shoutCount = 0
let currentCycle = 0

export const GiranToTalking: L2ExecutablePlugin = {
    getId(): string {
        return 'GiranToTalking Boat'
    },

    getStatus(): L2ExecutablePluginStatus {
        return undefined
    },

    async onEnd(): Promise<void> {
        shoutCount = 0
        currentCycle = 0
    },

    async onStart(): Promise<void> {
        let boat: L2BoatInstance = BoatManager.registerBoat( 2, 48950, 190613, -3610, 60800 )

        if ( !boat ) {
            return
        }

        ListenerCache.registerListener( ListenerRegisterType.WorldObject, EventType.VehiclePathEnded, null, Helper.runCycle, [ boat.getObjectId() ] )

        let data: VehiclePathEndedEvent = {
            objectId: boat.getObjectId(),
        }

        setTimeout( Helper.runCycle, 180000, data )
    },
}

const Helper = {
    async runCycle( data: VehiclePathEndedEvent ): Promise<void> {
        let boat: L2BoatInstance = L2World.getObjectById( data.objectId ) as L2BoatInstance

        if ( !boat ) {
            return
        }

        switch ( currentCycle ) {
            case 0:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_GIRAN5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 1:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_GIRAN1 )
                setTimeout( Helper.runCycle, 40000, data )
                break

            case 2:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_GIRAN0 )
                setTimeout( Helper.runCycle, 20000, data )
                break

            case 3:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVING_GIRAN, ARRIVAL_TALKING15 )
                BroadcastHelper.broadcastDataAtLocations( [ dockGiran ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )

                await boat.payForRide( 3946, 1, 46763, 187041, -3451 )
                await BoatManager.setBoathPath( data.objectId, pathGiranToTalking )
                setTimeout( Helper.runCycle, 250000, data )
                break

            case 4:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_TALKING10 )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 5:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_TALKING5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 6:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_TALKING1 )
                break

            case 7:
                if ( BoatManager.isDockBusy( ShipDock.TalkingIsland ) ) {
                    if ( shoutCount === 0 ) {
                        BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                                ConfigManager.general.getBoatBroadcastRadius(),
                                BUSY_TALKING )
                    }

                    shoutCount = ( shoutCount + 1 ) % 35

                    setTimeout( Helper.runCycle, 5000, data )
                    return
                }

                await BoatManager.setBoathPath( data.objectId, [ dockTalking ] )
                break

            case 8:
                BoatManager.dockShip( ShipDock.TalkingIsland, true )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVED_AT_TALKING, ARRIVED_AT_TALKING_2 )

                BroadcastHelper.broadcastDataAtLocations( [ dockTalking ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )

                setTimeout( Helper.runCycle, 300000, data )
                break

            case 9:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_TALKING5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 10:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_TALKING1 )
                setTimeout( Helper.runCycle, 40000, data )
                break

            case 11:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVE_TALKING0 )
                setTimeout( Helper.runCycle, 20000, data )
                break

            case 12:
                BoatManager.dockShip( ShipDock.TalkingIsland, false )
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        LEAVING_TALKING )

                BroadcastHelper.broadcastDataAtLocations( [ dockTalking ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )

                await boat.payForRide( 3945, 1, -96777, 258970, -3623 )
                await BoatManager.setBoathPath( data.objectId, pathTalkingToGiran )
                setTimeout( Helper.runCycle, 200000, data )
                break

            case 13:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GIRAN20 )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 14:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GIRAN15 )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 15:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GIRAN10 )
                setTimeout( Helper.runCycle, 300000, data )
                break

            case 16:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GIRAN5 )
                setTimeout( Helper.runCycle, 240000, data )
                break

            case 17:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVAL_GIRAN1 )
                break

            case 18:
                BroadcastHelper.broadcastDataAtLocations( broadcastLocations,
                        ConfigManager.general.getBoatBroadcastRadius(),
                        ARRIVED_AT_GIRAN, ARRIVED_AT_GIRAN_2 )

                BroadcastHelper.broadcastDataAtLocations( [ dockGiran ],
                        ConfigManager.general.getBoatBroadcastRadius(),
                        PlaySound.createSound( SoundNames.ITEMSOUND_SHIP_ARRIVAL_DEPARTURE, boat ) )
                setTimeout( Helper.runCycle, 300000, data )
                break
        }

        shoutCount = 0
        currentCycle = ( currentCycle + 1 ) % 18
    },
}