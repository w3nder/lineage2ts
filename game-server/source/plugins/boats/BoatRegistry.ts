import { L2ExecutablePlugin } from '../L2ExecutablePlugin'
import { RuneToPrimeval } from './RuneToPrimeval'
import { TalkingToGludin } from './TalkingToGludin'
import { InnadrilTour } from './InnadrilTour'
import { GludinToRune } from './GludinToRune'
import { GiranToTalking } from './GiranToTalking'

export const BoatRegistry: Array<L2ExecutablePlugin> = [
    RuneToPrimeval,
    TalkingToGludin,
    InnadrilTour,
    GludinToRune,
    GiranToTalking
]