import { GameClient } from '../../gameService/GameClient'
import { SessionKey } from '../../gameService/interface/SessionKey'

export interface WaitingClient {
    client: GameClient,
    key: SessionKey
}