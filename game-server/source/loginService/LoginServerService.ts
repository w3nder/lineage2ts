import * as net from 'net'
import { Socket } from 'net'
import { GameClient } from '../gameService/GameClient'
import { SessionKey } from '../gameService/interface/SessionKey'
import { WaitingClient } from './interfaces/WaitingClient'
import { getServerListType, ServerAgeStatus, ServerAvailabilityStatus } from './ServerStatus'
import { L2World } from '../gameService/L2World'
import { LoginFail, LoginFailReason } from '../gameService/packets/send/LoginFail'
import { SystemMessageIds } from '../gameService/packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../gameService/packets/send/SystemMessage'
import { DatabaseManager } from '../database/manager'
import { ConfigManager } from '../config/ConfigManager'
import { RpcManager } from '../rpc/manager'
import {
    L2GameLoginServerErrorEvent,
    L2GamePlayerAuthenticationOutcomeEvent,
    L2GamePlayerChangesPasswordOutcomeEvent,
    L2GameRequestPlayerCharactersEvent,
    L2GameRequestPlayerDisconnectEvent,
    L2GameServerEvent,
    L2GameServerOperations,
    L2GameServerRegisteredOutcomeEvent,
} from '../rpc/interface/GameServerOperations'
import {
    L2GameServerParameters,
    L2LoginPlayerAuthenticationEvent,
    L2LoginPlayerChangesPasswordEvent,
    L2LoginPlayerCharactersOnServerEvent,
    L2LoginPlayerLoginEvent,
    L2LoginPlayerLogoutEvent,
    L2LoginRegisterServerEvent,
    L2LoginServerOperations,
    L2LoginServerStatusEvent,
} from '../rpc/interface/LoginServerOperations'
import { L2GameClientRegistry } from '../gameService/L2GameClientRegistry'
import _ from 'lodash'
import * as blake3 from 'blake3'
import { GameClientState } from '../gameService/enums/GameClientState'
import { ServerLog } from '../logger/Logger'

enum RegistrationState {
    Unregistered,
    Registered,
    Declined,
    Denied
}

/*
    Blake3 is fast hash, which means brute-force attacks can unmask password faster than slow algorithms,
    which is not desired, hence double hashing of the hash with sufficient length makes
    brute-force attack much less likely to succeed if at all.
 */
const hashOptions = {
    length: 64,
}

function createPasswordHash( value: string ): string {
    return blake3.hash( blake3.hash( value, hashOptions ).toString( 'base64' ), hashOptions ).toString( 'base64' )
}

class LoginService {
    waitingClients: { [key: string]: WaitingClient } = {}
    accountsInGameServer = {}
    connection: Socket
    serverName: string
    serverId: number = 0
    serverPort: number = 0
    connectionState: RegistrationState = RegistrationState.Unregistered
    isServerEnabled: boolean = false
    requestedCharacters : Record<string, L2LoginPlayerCharactersOnServerEvent> = {}

    constructor() {
        ConfigManager.server.subscribe( this.onConfigurationReloaded.bind( this ) )
        this.serverPort = this.createServerPort()
    }

    connectToLoginServer(): void {
        this.connectionState = RegistrationState.Unregistered

        this.connection = new net.Socket()

        this.connection.on( 'data', this.onConnectionData.bind( this ) )
        this.connection.on( 'error', this.onConnectionEnd.bind( this ) )
        this.connection.on( 'end', this.onConnectionEnd.bind( this ) )

        this.connection.connect( ConfigManager.server.getLoginPort(), ConfigManager.server.getLoginHost() )

        return this.attemptRegistration()
    }

    attemptRegistration(): void {
        let data: L2LoginRegisterServerEvent = this.getServerParameters() as L2LoginRegisterServerEvent

        data.isAcceptAlternativeId = ConfigManager.server.acceptAlternateId()
        data.operationId = L2LoginServerOperations.RegisterServer

        return this.sendPacket( RpcManager.getDataConverter().pack( data ) )
    }

    getServerParameters(): L2GameServerParameters {
        let currentPlayers: number = _.size( L2World.getAllPlayers() )

        let serverStatus = ServerAvailabilityStatus.STATUS_DOWN
        if ( this.isServerEnabled ) {
            serverStatus = ConfigManager.general.serverGMOnly() ? ServerAvailabilityStatus.STATUS_GM_ONLY : ServerAvailabilityStatus.STATUS_AUTO
        }

        return {
            ageLimit: this.getAgeLimit(),
            currentPlayers,
            ip: ConfigManager.server.getExternalServerIp(),
            isPVP: false,
            isServerAcceptingPlayers: ConfigManager.server.getMaxOnlineUsers() > currentPlayers,
            maxPlayers: ConfigManager.server.getMaxOnlineUsers(),
            port: this.serverPort,
            serverId: this.serverId ? this.serverId : ConfigManager.server.getRequestServerId(),
            serverStatus,
            serverType: getServerListType( ConfigManager.server.getServerListType() ),
            useBrackets: ConfigManager.server.getServerListBrackets(),
        }
    }

    createServerPort(): number {
        if ( !_.isNumber( ConfigManager.server.getServerPort() ) || ConfigManager.server.getServerPort() === 0 ) {
            return _.random( 1000, 60000 )
        }

        return ConfigManager.server.getServerPort()
    }

    getAgeLimit(): number {
        if ( ConfigManager.server.getServerListAge() === 0 ) {
            return ServerAgeStatus.All
        }

        if ( ConfigManager.server.getServerListAge() <= 15 ) {
            return ServerAgeStatus.Age15AndUp
        }

        if ( ConfigManager.server.getServerListAge() <= 18 ) {
            return ServerAgeStatus.Age18AndUp
        }

        return ServerAgeStatus.All
    }

    async onConnectionData( incomingData: Buffer ): Promise<void> {
        let result: L2GameServerEvent = RpcManager.getDataConverter().unpack( incomingData )

        switch ( result.operationId ) {
            case L2GameServerOperations.ServerRegisteredOutcome:
                return this.onServerRegistered( result as L2GameServerRegisteredOutcomeEvent )

            case L2GameServerOperations.PlayerAuthenticationOutcome:
                return this.onRequestAuthenticateAccount( result as L2GamePlayerAuthenticationOutcomeEvent )

            case L2GameServerOperations.RequestPlayerDisconnect:
                return this.onRequestPlayerDisconnect( result as L2GameRequestPlayerDisconnectEvent )

            case L2GameServerOperations.RequestPlayerCharacters:
                return this.onRequestPlayerCharacters( result as L2GameRequestPlayerCharactersEvent )

            case L2GameServerOperations.PlayerChangesPasswordOutcome:
                return this.onPlayerChangedPassword( result as L2GamePlayerChangesPasswordOutcomeEvent )

            case L2GameServerOperations.LoginServerError:
                return this.onServerError( result as L2GameLoginServerErrorEvent )
        }

        this.closeConnection()
    }

    onConnectionEnd() {
        this.connection.destroy()
        this.connection = null

        switch ( this.connectionState ) {
            case RegistrationState.Unregistered:
                ServerLog.error( 'Login Server not accepting connection!' )
                break

            case RegistrationState.Declined:
                ServerLog.error( 'Login Server declined registration!' )
                break

            case RegistrationState.Registered:
                ServerLog.error( 'Login Server dropped connection!' )
                break

            case RegistrationState.Denied:
                ServerLog.error( 'Login Server denied registration!' )
                break
        }

        let reconnectionDelay = ConfigManager.server.getLoginServerReconnectInterval()
        ServerLog.info( `Attempting to register at Login Server in ${reconnectionDelay} seconds...` )

        _.delay( this.connectToLoginServer.bind( this ), reconnectionDelay * 1000 )
    }

    sendPacket( packet: Buffer ): void {
        if ( !this.connection || !Buffer.isBuffer( packet ) ) {
            return
        }

        this.connection.write( packet )
    }

    closeConnection(): void {
        if ( !this.connection ) {
            return
        }

        this.connection.end()
        this.connection.destroy()
    }

    onServerError( data: L2GameLoginServerErrorEvent ) {
        this.connectionState = RegistrationState.Declined

        ServerLog.error( `Game Server registration failed due to: ${data.reason}` )
        this.closeConnection()
    }

    onServerRegistered( data: L2GameServerRegisteredOutcomeEvent ) {
        this.serverName = data.serverName
        this.serverId = data.acceptedServerId
        this.connectionState = RegistrationState.Registered

        ServerLog.info( `Game Server '${this.serverName}' registered on Login Server @ ${ConfigManager.server.getLoginHost()}:${ConfigManager.server.getLoginPort()}` )
    }

    async onRequestAuthenticateAccount( data: L2GamePlayerAuthenticationOutcomeEvent ) {
        let { key, client }: WaitingClient = this.waitingClients[ data.accountName ]

        delete this.waitingClients[ data.accountName ]

        if ( !client || client.getAccountName() !== data.accountName ) {
            return
        }

        if ( !data.isSuccess ) {
            ServerLog.trace( `Session key is not correct. Closing connection for account ${data.accountName}` )

            return client.closeConnection( LoginFail( LoginFailReason.IncorrectAccountInfoContactCustomerSupport ) )
        }

        client.setState( GameClientState.Verified )
        client.setSessionId( key )

        await client.updateCharacterSelection()
        client.sendCharacterSelection()

        let event: L2LoginPlayerLoginEvent = {
            accountName: data.accountName,
            operationId: L2LoginServerOperations.PlayerLogin,
        }

        return this.sendPacket( RpcManager.getDataConverter().pack( event ) )
    }

    onRequestPlayerDisconnect( data: L2GameRequestPlayerDisconnectEvent ): Promise<void> {
        let gameClient = this.accountsInGameServer[ data.accountName ]

        if ( gameClient ) {
            return gameClient.closeConnection( SystemMessageBuilder.fromMessageId( SystemMessageIds.ANOTHER_LOGIN_WITH_ACCOUNT ) )
        }
    }

    async onRequestPlayerCharacters( data: L2GameRequestPlayerCharactersEvent ): Promise<void> {
        let characterTimes: Array<number> = await DatabaseManager.getCharacterTable().getCharactersDeleteTime( data.accountName )

        let result: L2LoginPlayerCharactersOnServerEvent = {
            accountName: data.accountName,
            operationId: L2LoginServerOperations.PlayerCharactersOnServer,
            timesToDelete: characterTimes.filter( ( time ) => time !== 0 ),
            totalCharacters: characterTimes.length,
        }

        this.requestedCharacters[ data.accountName ] = result

        return this.sendPacket( RpcManager.getDataConverter().pack( result ) )
    }

    onPlayerChangedPassword( data: L2GamePlayerChangesPasswordOutcomeEvent ) {
        let client: GameClient = L2GameClientRegistry.getClientByAccount( data.accountName )

        if ( client && client.player ) {
            client.player.sendMessage( data.message )
        }
    }

    addWaitingClientAndVerifyLogin( accountName: string, client: GameClient, key: SessionKey ) {
        this.waitingClients[ accountName ] = {
            client,
            key,
        }

        let event: L2LoginPlayerAuthenticationEvent = {
            accountName: accountName,
            operationId: L2LoginServerOperations.PlayerAuthentication,
            key,
        }

        return this.sendPacket( RpcManager.getDataConverter().pack( event ) )
    }

    sendLogout( accountName: string ): void {
        let event: L2LoginPlayerLogoutEvent = {
            accountName,
            operationId: L2LoginServerOperations.PlayerLogout,
        }

        return this.sendPacket( RpcManager.getDataConverter().pack( event ) )
    }

    addGameServerLogin( accountName: string, client: GameClient ): boolean {
        if ( this.accountsInGameServer[ accountName ] ) {
            return false
        }

        this.accountsInGameServer[ accountName ] = client
        return true
    }

    removeGameServerLogin( accountName: string ) : void {
        delete this.accountsInGameServer[ accountName ]
    }

    sendChangePassword( accountName: string, oldPassword: string, newPassword: string ): void {
        let event: L2LoginPlayerChangesPasswordEvent = {
            accountName: accountName,
            currentPassword: createPasswordHash( oldPassword ),
            futurePassword: createPasswordHash( newPassword ),
            operationId: L2LoginServerOperations.PlayerChangesPassword,
        }

        return this.sendPacket( RpcManager.getDataConverter().pack( event ) )
    }

    onConfigurationReloaded() {
        if ( !this.connection || this.connectionState !== RegistrationState.Registered ) {
            return
        }

        let event: L2LoginServerStatusEvent = {
            ...this.getServerParameters(),
            operationId: L2LoginServerOperations.ServerStatus,
        }

        return this.sendPacket( RpcManager.getDataConverter().pack( event ) )
    }

    getServerPort(): number {
        return this.serverPort
    }

    enableGameServer(): void {
        this.isServerEnabled = true

        if ( this.connectionState !== RegistrationState.Registered ) {
            return
        }

        let event: L2LoginServerStatusEvent = {
            ...this.getServerParameters(),
            operationId: L2LoginServerOperations.ServerStatus,
        }

        ServerLog.info( `Server ${this.serverName} is now available to join.` )
        return this.sendPacket( RpcManager.getDataConverter().pack( event ) )
    }

    isConnected(): boolean {
        return this.connectionState === RegistrationState.Registered
    }

    hasRequestedAccount( name: string ) : boolean {
        return !!this.requestedCharacters[ name ]
    }

    resetRequestedAccount( name: string ) : void {
        delete this.requestedCharacters[ name ]
    }
}

export const LoginServerService = new LoginService()