export interface L2BinaryConverter {
    pack( data : any ) : Buffer
    unpack( data : Buffer ) : any
}