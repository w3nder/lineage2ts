export interface L2GameServerEvent {
    operationId : number
}

export enum L2GameServerOperations {
    LoginServerError,
    RequestServerStatus,
    RequestPlayerCharacters,
    RequestPlayerDisconnect,
    PlayerAuthenticationOutcome,
    PlayerChangesPasswordOutcome,
    ServerRegisteredOutcome,
}

export interface L2GameLoginServerErrorEvent extends L2GameServerEvent {
    reason: string
    operationId: L2GameServerOperations.LoginServerError
}

export interface L2GameRequestServerStatusEvent extends L2GameServerEvent {
    operationId: L2GameServerOperations.RequestServerStatus
}

export interface L2GameRequestPlayerDisconnectEvent extends L2GameServerEvent {
    operationId: L2GameServerOperations.RequestPlayerDisconnect
    accountName: string
}

export interface L2GamePlayerAuthenticationOutcomeEvent extends L2GameServerEvent {
    isSuccess: boolean
    operationId: L2GameServerOperations.PlayerAuthenticationOutcome
    accountName: string
}

export interface L2GamePlayerChangesPasswordOutcomeEvent extends L2GameServerEvent {
    isSuccess : boolean
    message : string
    operationId: L2GameServerOperations.PlayerChangesPasswordOutcome
    accountName: string
}

export interface L2GameServerRegisteredOutcomeEvent extends L2GameServerEvent {
    acceptedServerId: number
    serverName: string
    operationId: L2GameServerOperations.ServerRegisteredOutcome
}

export interface L2GameRequestPlayerCharactersEvent extends L2GameServerEvent {
    accountName: string
    operationId: L2GameServerOperations.RequestPlayerCharacters
}