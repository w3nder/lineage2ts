import { L2DataApi } from '../data/interface/l2DataApi'
import { ConfigManager } from '../config/ConfigManager'
import { createSocket, Socket } from 'nanomsg'
import { MessagePackConverter } from './MessagePackConverter'
import { L2World } from '../gameService/L2World'
import { AdminCommandManager } from '../gameService/handler/managers/AdminCommandManager'
import { IAdminCommand } from '../gameService/handler/IAdminCommand'
import { CommandLinkInput, CommandLinkType } from './enums/CommandLinkParameters'
import { ServerLog } from '../logger/Logger'

interface SocketParameters {
    address: string
    types: Set<CommandLinkType>
}

class Manager implements L2DataApi {
    socket: Socket
    socketParameters: SocketParameters = {
        address: undefined,
        types: new Set<CommandLinkType>()
    }

    async load(): Promise<Array<string>> {
        ConfigManager.server.subscribe( this.enableLink.bind( this, true ) )

        this.enableLink()

        if ( !this.socket ) {
            return [
                'CommandLink is disabled'
            ]
        }

        return [
            `CommandLink: enabled on ${ ConfigManager.server.getCommandLinkEndpoint() }`,
            `CommandLink: permitted to act on ${ Array.from( ConfigManager.server.getCommandLinkPermissions() ).join( ',' ) }`,
        ]
    }

    private enableLink( displayMessage: boolean = false ): void {
        if ( !ConfigManager.server.isCommandLinkEnabled() ) {
            this.disableLink()
            return
        }

        if ( this.socketParameters.address !== ConfigManager.server.getCommandLinkEndpoint() ) {
            this.disableLink()
        }

        if ( !this.socket ) {
            this.socket = createSocket( 'pair', {
                tcpnodelay: false
            } )
        }

        this.socket.bind( ConfigManager.server.getCommandLinkEndpoint() )
        this.socket.on( 'data', this.onSocketData.bind( this ) )

        this.regenerateSocketParameters()

        if ( displayMessage ) {
            ServerLog.info( 'CommandLink: enabled on %s', ConfigManager.server.getCommandLinkEndpoint() )
        }
    }

    private disableLink(): void {
        if ( !this.socket ) {
            return
        }

        this.socketParameters.address = undefined
        this.socketParameters.types.clear()

        this.socket.close()
        this.socket = null

        ServerLog.info( 'CommandLink is disabled' )
    }

    private async onSocketData( data: Buffer ) : Promise<void> {
        let event = MessagePackConverter.unpack( data ) as CommandLinkInput

        if ( !this.socketParameters.types.has( event.type ) ) {
            return
        }

        switch ( event.type ) {
            case CommandLinkType.admin:
                if ( !event.replyId ) {
                    return
                }

                let adminPlayer = L2World.getPlayerByName( event.replyId )
                if ( !adminPlayer || !adminPlayer.isGM() ) {
                    return
                }

                if ( !adminPlayer.getAccessLevel().canExecute( event.command ) ) {
                    return
                }

                let commandKey = event.command.split( ' ' )[ 0 ]
                let handler: IAdminCommand = AdminCommandManager.getHandler( commandKey )
                if ( !handler ) {
                    return
                }

                ServerLog.info( 'CommandLink: executing admin command %s', event.command )

                /*
                    Since admin commands are prefixed with admin_ string,
                    we must shave off that part in order for command handler to recognize
                    current command.
                 */
                return handler.onCommand( event.command.substring( 6 ), adminPlayer )

            case CommandLinkType.inquiry:
                return
        }
    }

    private regenerateSocketParameters() : void {
        this.socketParameters.address = ConfigManager.server.getCommandLinkEndpoint()
        this.socketParameters.types.clear()

        ConfigManager.server.getCommandLinkPermissions().forEach( ( value: string ) => {
            let type = CommandLinkType[ value ]
            if ( Number.isNaN( type ) ) {
                return
            }

            this.socketParameters.types.add( type )
        } )
    }
}

export const CommandLink = new Manager()