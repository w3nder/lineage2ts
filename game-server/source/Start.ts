import { loadConfiguration } from './config/ConfigManager'
import perfy from 'perfy'

perfy.start( 'server-load' )
loadConfiguration().then( () => {
    return import( './Server' )
} )