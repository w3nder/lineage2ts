import { ConfigManager } from '../../config/ConfigManager'
import _ from 'lodash'
import pkgDir from 'pkg-dir'
import Database from 'better-sqlite3'
import { ServerLog } from '../../logger/Logger'
import chalk from 'chalk'

const rootDirectory = pkgDir.sync( __dirname )
const database = new Database( `${rootDirectory}/${ConfigManager.database.getDatabaseName()}`, {
    fileMustExist: true
} )

export function getOne ( query: string, ...parameters : Array<string | number> ): unknown {
    try {
        return database.prepare( query ).get( parameters )
    } catch ( error ) {
        ServerLog.error( `Query ${chalk.red( query )} has error ${chalk.red( error.message )}` )
    }

    return null
}

export function getMany( query: string, ...parameters ): Array<any> {
    try {
        return database.prepare( query ).all( ...parameters )
    } catch ( error ) {
        ServerLog.error( `Query ${chalk.red( query )} has error ${chalk.red( error.message )}` )
    }

    return []
}

export function insertOne( query: string, ...parameters ): void {
    try {
        database.prepare( query ).run( ...parameters )
    } catch ( error ) {
        ServerLog.error( `Insert query ${chalk.red( query )} has error ${chalk.red( error.message )}` )
    }
}

export function insertOneWithReturn( query: string, ...parameters ): number | bigint {
    try {
        return database.prepare( query ).run( ...parameters ).lastInsertRowid
    } catch ( error ) {
        ServerLog.error( `Insert query ${chalk.red( query )} has error ${chalk.red( error.message )}` )
    }

    return null
}

/**
 * Makes easy to supply multiple values for lookups
 * Example query: SELECT charId FROM characters WHERE charId in (#values#)
 */
export function getManyForValues( query: string, parameters: Array<unknown> ) {
    let finalQuery = query.replace( '#values#', _.join( _.times( parameters.length, _.constant( '?' ) ), ',' ) )
    return getMany( finalQuery, ...parameters )
}

/**
 * Insert, Update or Delete in bulk
 */
export function insertMany( query: string, parameters: Array<Array<unknown>> ) : void {
    try {
        const statement = database.prepare( query )

        const insertBulk = database.transaction( ( allItems ) => allItems.forEach( ( itemParameters ) => {
            statement.run( ...itemParameters )
        } ) )

        insertBulk( parameters )
    } catch ( error ) {
        ServerLog.error( `Insert many query ${chalk.red( query )} has error ${chalk.red( error.message )}` )
    }
}

/**
 * Makes easy to supply multiple values for deletes
 * Example query: DELETE FROM items WHERE object_id in (#values#)
 */
export function deleteManyForValues( query: string, parameters: Array<unknown> ) : void {
    let finalQuery = query.replace( '#values#', _.join( _.times( parameters.length, _.constant( '?' ) ), ',' ) )
    return insertOne( finalQuery, ...parameters )
}

export function shutdown() : void {
    database.close()
}