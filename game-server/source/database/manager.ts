import { CharacterSubclassesApi } from './interface/CharacterSubclassesApi'
import { CharactersTableApi } from './interface/CharactersTableApi'
import { CharacterSkillsTableApi } from './interface/CharacterSkillsTableApi'
import { ItemElementalsTableApi } from './interface/ItemElementalsTableApi'
import { CharacterFriendsTableApi } from './interface/CharacterFriendsTableApi'
import { PetsTableApi } from './interface/PetsTableApi'
import { CharacterRecommendationBonusTableApi } from './interface/CharacterRecommendationBonusTableApi'
import { QuestStatesTableApi } from './interface/QuestStatesTableApi'
import { CharacterShortcutsTableApi } from './interface/CharacterShortcutsTableApi'
import { MessagesTableApi } from './interface/MessagesTableApi'
import { SQLiteDatabase } from './type/sqliteDatabase'
import { CharacterSkillsSaveTableApi } from './interface/CharacterSkillsSaveTableApi'
import { CharacterItemReuseSaveTableApi } from './interface/CharacterItemReuseSaveTableApi'
import { CharacterRecipeShoplistTableApi } from './interface/CharacterRecipeShoplistTableApi'
import { CharacterUICategoriesTableApi } from './interface/CharacterUICategoriesTableApi'
import { CharacterUIActionsTableApi } from './interface/CharacterUIActionsTableApi'
import { SevenSignsTableApi } from './interface/SevenSignsTableApi'
import { ServitorsTableApi } from './interface/ServitorsTableApi'
import { PetSkillsSaveTableApi } from './interface/PetSkillsSaveTableApi'
import { CharacterSummonSkillsSaveTableApi } from './interface/CharacterSummonSkillsSaveTableApi'
import { CharacterInstanceTimeTableApi } from './interface/CharacterInstanceTimeTableApi'
import { CharacterMacrosTableApi } from './interface/CharacterMacrosTableApi'
import { CursedWeaponsTableApi } from './interface/CursedWeaponsTableApi'
import { CastleTableApi } from './interface/CastleTableApi'
import { ClanDataTableApi } from './interface/ClanDataTableApi'
import { CastleManorProductionTableApi } from './interface/CastleManorProductionTableApi'
import { CastleManorProcureTableApi } from './interface/CastleManorProcureTableApi'
import { CastleSiegeGuardsTableApi } from './interface/CastleSiegeGuardsTableApi'
import { FortTableApi } from './interface/FortTableApi'
import { L2FortFunctionsTableApi } from './interface/FortFunctionsTableApi'
import { PunishmentsTableApi } from './interface/PunishmentsTableApi'
import { ClanhallTableApi } from './interface/ClanhallTableApi'
import { ClanhallFunctionsTableApi } from './interface/ClanhallFunctionsTableApi'
import { AnnouncementsTableApi } from './interface/AnnouncementsTableApi'
import { L2ClanPledgesTableApi } from './interface/ClanSubpledgesTableApi'
import { L2ClanPrivilegesTableApi } from './interface/ClanPrivilegesTableApi'
import { L2ClanSkillsTableApi } from './interface/ClanSkillsTableApi'
import { L2ClanNoticesTableApi } from './interface/ClanNoticesTableApi'
import { L2CharacterHennasTableApi } from './interface/CharacterHennasTableApi'
import { L2CharacterTeleportBookmarksTableApi } from './interface/CharacterTeleportBookmarksTableApi'
import { L2CharacterRecipebookTableApi } from './interface/CharacterRecipebookTableApi'
import { L2DimensionalTransferItemsTableApi } from './interface/DimensionalTransferItemsTableApi'
import { L2CharacterVariablesTableApi } from './interface/CharacterVariablesTableApi'
import { L2AccountVariablesTableApi } from './interface/AccountVariablesTableApi'
import { L2ClanWarsTableApi } from './interface/ClanWarsTableApi'
import { L2CrestsTableApi } from './interface/CrestsTableApi'
import { L2BuylistsTableApi } from './interface/BuylistsTableApi'
import { L2AirshipsTableApi } from './interface/AirshipsTableApi'
import { L2SiegeClansTableApi } from './interface/SiegeClansTableApi'
import { L2PetitionFeedbackTableApi } from './interface/PetitionFeedbackTableApi'
import { L2ItemAuctionBidTableApi } from './interface/ItemAuctionBidTableApi'
import { L2CharacterRaidPointsTableApi } from './interface/CharacterRaidPointsTableApi'
import { L2TerritoryRegistrationsTableApi } from './interface/TerritoryRegistrationsTableApi'
import { L2CharacterContactsTableApi } from './interface/CharacterContactsTableApi'
import { L2ItemsTableApi } from './interface/ItemsTableApi'
import { L2MerchantLeaseTableApi } from './interface/MerchantLeaseTableApi'
import { L2OlympiadNoblesTableApi } from './interface/OlympiadNoblesTableApi'
import { L2HeroesTableApi } from './interface/HeroesTableApi'
import { ConfigManager } from '../config/ConfigManager'
import { L2HeroDiaryTableApi } from './interface/HeroDiaryTableApi'
import { IDatabaseType } from './type/IDatabaseType'
import { L2GrandbossDataTableApi } from './interface/GrandbossDataTableApi'
import { L2GrandbossPlayersTableApi } from './interface/GrandbossPlayersTableApi'
import { L2FortSiegeClansTableApi } from './interface/FortSiegeClansTableApi'
import { L2CastleFunctionsTableApi } from './interface/CastleFunctionsTableApi'
import { L2CastleDoorUpgradeTableApi } from './interface/CastleDoorUpgradeTableApi'
import { L2CastleTrapUpgradeTableApi } from './interface/CastleTrapUpgradeTableApi'
import { L2ClanHallAuctionBidTableApi } from './interface/ClanHallAuctionBidTableApi'
import { L2FortDoorUpgradeTableApi } from './interface/FortDoorUpgradeTableApi'
import { L2ItemAuctionTableApi } from './interface/ItemAuctionTableApi'
import { L2BotReportsTableApi } from './interface/BotReportsTableApi'
import { L2ClanHallAuctionsTableApi } from './interface/ClanHallAuctionsTableApi'
import { L2LotteryTableApi } from './interface/LotteryTableApi'
import { L2OlympiadDataTableApi } from './interface/OlympiadDataTableApi'
import { L2ServerVariablesTableApi } from './interface/ServerVariablesTableApi'
import { L2FishingChampionshipTableApi } from './interface/FishingChampionshipTableApi'
import { L2ItemsOnGroundTableApi } from './interface/ItemsOnGroundTableApi'
import { L2NevitDataTableApi } from './interface/NevitDataTableApi'
import { L2SiegableHallTableApi } from './interface/SiegableHallTableApi'
import { L2NpcRespawnTableApi } from './interface/NpcRespawnTableApi'
import { L2DatabaseOperations } from './operations/DatabaseOperations'

const allEngines: { [ type: string ]: IDatabaseType } = {
    sqlite: SQLiteDatabase,
}

function getEngine(): IDatabaseType {
    return allEngines[ ConfigManager.database.getEngine() ]
}

export const DatabaseManager = {
    loadMe(): void {

    },

    getItems(): L2ItemsTableApi {
        return getEngine().itemsTable
    },

    getCharacterSubclasses(): CharacterSubclassesApi {
        return getEngine().characterSubclassesTable
    },

    getCharacterTable(): CharactersTableApi {
        return getEngine().charactersTable
    },

    getCharacterSkills(): CharacterSkillsTableApi {
        return getEngine().characterSkillsTable
    },

    getItemElementalsTable(): ItemElementalsTableApi {
        return getEngine().itemElementalsTable
    },

    getCharacterFriends(): CharacterFriendsTableApi {
        return getEngine().characterFriendsTable
    },

    getPets(): PetsTableApi {
        return getEngine().petsTable
    },

    getRecommendationBonus: (): CharacterRecommendationBonusTableApi => {
        return getEngine().characterRecommendationBonusTable
    },

    getQuestStatesTable(): QuestStatesTableApi {
        return getEngine().questStatesTable
    },

    getCharacterShortcuts(): CharacterShortcutsTableApi {
        return getEngine().characterShortcutsTable
    },

    getMessages(): MessagesTableApi {
        return getEngine().messagesTable
    },

    getCharacterSkillsSave(): CharacterSkillsSaveTableApi {
        return getEngine().characterSkillsSaveTable
    },

    getCharacterItemReuseSave(): CharacterItemReuseSaveTableApi {
        return getEngine().characterItemReuseSaveTable
    },

    getCharacterRecipeShoplist(): CharacterRecipeShoplistTableApi {
        return getEngine().characterRecipeShoplistTable
    },

    getCharacterUICategories(): CharacterUICategoriesTableApi {
        return getEngine().characterUICategoriesTable
    },

    getCharacterUIActions(): CharacterUIActionsTableApi {
        return getEngine().characterUIActionsTable
    },

    getSevenSigns(): SevenSignsTableApi {
        return getEngine().sevenSignsTable
    },

    getServitors(): ServitorsTableApi {
        return getEngine().servitorsTable
    },

    getPetSkillsSave(): PetSkillsSaveTableApi {
        return getEngine().characterPetSkillsSave
    },

    getCharacterSummonSkillsSave(): CharacterSummonSkillsSaveTableApi {
        return getEngine().characterSummonSkillsSave
    },

    getCharacterInstanceTime(): CharacterInstanceTimeTableApi {
        return getEngine().characterInstanceTime
    },

    getCharacterMacros(): CharacterMacrosTableApi {
        return getEngine().characterMacrosTable
    },

    getCursedWeapons(): CursedWeaponsTableApi {
        return getEngine().cursedWeaponsTable
    },

    getCastleTable(): CastleTableApi {
        return getEngine().castleTable
    },

    getClanDataTable(): ClanDataTableApi {
        return getEngine().clanDataTable
    },

    getCastleManorProduction(): CastleManorProductionTableApi {
        return getEngine().castleManorProduction
    },

    getCastleManorProcure(): CastleManorProcureTableApi {
        return getEngine().castleManorProcure
    },

    getCastleSiegeGuards(): CastleSiegeGuardsTableApi {
        return getEngine().castleSiegeGuards
    },

    getFortTable(): FortTableApi {
        return getEngine().fortTable
    },

    getFortFunctions(): L2FortFunctionsTableApi {
        return getEngine().fortFunctionsTable
    },

    getPunishments(): PunishmentsTableApi {
        return getEngine().punishmentsTable
    },

    getClanhallTable(): ClanhallTableApi {
        return getEngine().clanhallTable
    },

    getClanhallFunctions(): ClanhallFunctionsTableApi {
        return getEngine().clanhallFunctionsTable
    },

    getAnnouncements(): AnnouncementsTableApi {
        return getEngine().announcementsTable
    },

    getClanSubpledges(): L2ClanPledgesTableApi {
        return getEngine().clanSubpledgesTable
    },

    getClanPrivileges(): L2ClanPrivilegesTableApi {
        return getEngine().clanPrivilegesTable
    },

    getClanSkills(): L2ClanSkillsTableApi {
        return getEngine().clanSkillsTable
    },

    getClanNotices(): L2ClanNoticesTableApi {
        return getEngine().clanNoticesTable
    },

    getCharacterHennas(): L2CharacterHennasTableApi {
        return getEngine().characterHennasTable
    },

    getCharacterTeleportBookmarks(): L2CharacterTeleportBookmarksTableApi {
        return getEngine().characterTeleportBookmarks
    },

    getCharacterRecipebook(): L2CharacterRecipebookTableApi {
        return getEngine().characterRecipebook
    },

    getDimensionalTransferItems(): L2DimensionalTransferItemsTableApi {
        return getEngine().dimensionalTransferItems
    },

    getCharacterVariables(): L2CharacterVariablesTableApi {
        return getEngine().characterVariables
    },

    getAccountVariables(): L2AccountVariablesTableApi {
        return getEngine().accountVariables
    },

    getClanWarsTable(): L2ClanWarsTableApi {
        return getEngine().clanWarsTable
    },

    getCrestsTable(): L2CrestsTableApi {
        return getEngine().crestsTable
    },

    getBuylists(): L2BuylistsTableApi {
        return getEngine().buylistsTable
    },

    getAirships(): L2AirshipsTableApi {
        return getEngine().airshipsTable
    },

    getSiegeClans(): L2SiegeClansTableApi {
        return getEngine().siegeClansTable
    },

    getPetitionFeedback(): L2PetitionFeedbackTableApi {
        return getEngine().petitionFeedbackTable
    },

    getItemAuctionBids(): L2ItemAuctionBidTableApi {
        return getEngine().itemAuctionBidTable
    },

    getCharacterRaidPoints(): L2CharacterRaidPointsTableApi {
        return getEngine().characterRaidPointsTable
    },

    getTerritoryRegistrations(): L2TerritoryRegistrationsTableApi {
        return getEngine().territoryRegistrationsTable
    },

    getCharacterContacts(): L2CharacterContactsTableApi {
        return getEngine().characterContactsTable
    },

    getMerchantLease(): L2MerchantLeaseTableApi {
        return getEngine().merchantLeaseTable
    },

    getOlympiadNobles(): L2OlympiadNoblesTableApi {
        return getEngine().olympiadNoblesTable
    },

    getHeroes(): L2HeroesTableApi {
        return getEngine().heroesTable
    },

    getHeroDiary(): L2HeroDiaryTableApi {
        return getEngine().heroDiary
    },

    getGrandbossData(): L2GrandbossDataTableApi {
        return getEngine().grandbossData
    },

    getGrandbossPlayers(): L2GrandbossPlayersTableApi {
        return getEngine().grandbossPlayers
    },

    getFortSiegeClans() : L2FortSiegeClansTableApi {
        return getEngine().fortSiegeClans
    },

    getCastleFunctions() : L2CastleFunctionsTableApi {
        return getEngine().castleFunctions
    },

    getCastleDoorUpgrade() : L2CastleDoorUpgradeTableApi {
        return getEngine().castleDoorUpgrade
    },

    getCastleTrapUpgrade() : L2CastleTrapUpgradeTableApi {
        return getEngine().castleTrapUpgrade
    },

    getAuctionBids() : L2ClanHallAuctionBidTableApi {
        return getEngine().auctionBidTable
    },

    getFortDoorUpgrade() : L2FortDoorUpgradeTableApi {
        return getEngine().fortDoorUpgrade
    },

    getItemAuctions() : L2ItemAuctionTableApi {
        return getEngine().itemAuction
    },

    getBotReports() : L2BotReportsTableApi {
        return getEngine().botReports
    },

    getClanHallAuctions() : L2ClanHallAuctionsTableApi {
        return getEngine().clanHallAuctions
    },

    getLottery() : L2LotteryTableApi {
        return getEngine().lotteryTable
    },


    getOlympiadData() : L2OlympiadDataTableApi {
        return getEngine().olympiadData
    },

    getServerVariables() : L2ServerVariablesTableApi {
        return getEngine().serverVariables
    },

    getFishingChampionship() : L2FishingChampionshipTableApi {
        return getEngine().fishingChampionship
    },

    getItemsOnGround() : L2ItemsOnGroundTableApi {
        return getEngine().itemsOnGround
    },

    getNevitData() : L2NevitDataTableApi {
        return getEngine().nevitData
    },

    getSiegableHalls() : L2SiegableHallTableApi {
        return getEngine().siegableHalls
    },

    getNpcRespawns() : L2NpcRespawnTableApi {
        return getEngine().npcRespawns
    },

    operations() : L2DatabaseOperations {
        return getEngine().operations
    }
}