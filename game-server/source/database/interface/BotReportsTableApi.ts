export interface L2BotReportsTableApi {
    getAll() : Promise<Array<L2BotReportsTableData>>
}

export interface L2BotReportsTableData {
    botObjectId: number
    reporterObjectId: number
    reportTime: number
}