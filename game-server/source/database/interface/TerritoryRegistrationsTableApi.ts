export interface L2TerritoryRegistrationsTableApi {
    remove( castleId: number, objectId: number ) : Promise<void>
    add( castleId: number, objectId: number ) : Promise<void>
}