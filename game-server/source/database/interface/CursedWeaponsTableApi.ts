import { CursedWeapon } from '../../gameService/models/CursedWeapon'

export interface CursedWeaponsTableApi {
    saveWeapon( weapon: CursedWeapon ) : Promise<void>
    deleteWeapon( weapon: CursedWeapon ) : Promise<void>
}