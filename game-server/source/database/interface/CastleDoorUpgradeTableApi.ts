export interface L2CastleDoorUpgradeTableApi {
    getAll( residenceId: number ) : Promise<Array<L2CastleDoorUpgradeTableData>>
    remove( residenceId: number ) : Promise<void>
    update( residenceId: number, doorId: number, ratio: number ): Promise<void>
}

export interface L2CastleDoorUpgradeTableData {
    doorId: number
    ratio: number
}