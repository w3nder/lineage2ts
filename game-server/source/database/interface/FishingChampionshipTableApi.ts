export interface L2FishingChampionshipTableApi {
    getAll() : Promise<Array<L2FishingChampionshipTableItem>>
    removeAll() : Promise<void>
    addAll( values: Array<L2FishingChampionshipTableItem> ) : Promise<void>
    update( item : L2FishingChampionshipTableItem ) : Promise<void>
    add( item : L2FishingChampionshipTableItem ) : Promise<void>
}

export interface L2FishingChampionshipTableItem {
    playerId: number
    fishLength: number
    rewardType: number
    hasClaimedReward: boolean
}