import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'
import { MacroType } from '../../gameService/enums/MacroType'

export interface CharacterMacrosTableApi extends L2DatabaseRemoveOperations {
    getMacros( playerId: number ) : Promise<Array<L2CharacterMacroItem>>
    deleteMany( items: Array<L2CharacterMacroItem> ) : Promise<void>
    upsertMany( items: Array<L2CharacterMacroItem> ) : Promise<void>
}

export interface L2CharacterMacroItem {
    objectId: number
    id: number
    icon: number
    name: string
    description: string
    acronym: string
    commands: Array<L2CharacterMacroCommand>
}

export interface L2CharacterMacroCommand {
    entry: number
    type: MacroType
    dataOne: number // skillId or page for shortcuts
    dataTwo: number // shortcutId
    command: string
}