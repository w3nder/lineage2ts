import { Product } from '../../gameService/models/buylist/Product'

export interface L2BuylistsTableApi {
    getAll() : Promise<Array<L2BuylistsTableItem>>
    saveProducts( products: Array<Product> ) : Promise<void>
    removeProducts( products: Array<Product> ) : Promise<void>
}

export interface L2BuylistsTableItem {
    id: number
    itemId: number
    count: number
    restockTime: number
}