export interface L2NpcRespawnTableApi {
    getAll() : Promise<Array<L2NpcRespawnTableItem>>
    remove( ids: Array<string> ) : Promise<void>
    updateAll( items: Array<L2NpcRespawnTableItem> ) : Promise<void>
}

export interface L2NpcRespawnTableItem {
    id: string
    x: number
    y: number
    z: number
    heading: number
    killTime: number
    hp: number
    mp: number
    lastUpdate: number
}