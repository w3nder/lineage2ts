export interface L2FortSiegeClansTableApi {
    hasRegistration( clanId: number, fortId: number ) : Promise<boolean>
    hasRegistrationForAny( clanId: number, residenceIds: ReadonlyArray<number> ) : Promise<boolean>
    removeAll( residenceId: number ): Promise<void>
    removeClanRegistration( residenceId: number, clanId: number ): Promise<void>
    getRegisteredClanIds( residenceId: number ): Promise<Array<number>>
    addClanRegistration( residenceId: number, clanId: number ): Promise<void>
    removeClan( clanId: number ): Promise<void>
}