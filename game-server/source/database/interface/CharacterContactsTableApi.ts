import { L2DatabaseIdentifierOperations } from '../operations/DatabaseIdentifierOperations'
import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface L2CharacterContactsTableApi extends L2DatabaseIdentifierOperations, L2DatabaseRemoveOperations {
    addContact( objectId: number, contactId: number ) : Promise<void>
    removeContact( objectId: number, contactId: number ) : Promise<void>
    getContacts( objectId: number ) : Promise<Array<number>>
}