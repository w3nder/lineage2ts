import { BuffProperties } from '../../gameService/models/skills/BuffDefinition'

export interface PetSkillsSaveTableApi {
    update( id: number, effects: Array<BuffProperties> ) : Promise<void>
    load( id: number ) : Promise<Array<BuffProperties>>
    delete( id: number ) : Promise<void>
}