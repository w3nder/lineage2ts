export interface L2ClanWarsTableApi {
    setWar( clanIdOne: number, clanIdTwo: number ) : Promise<void>
    removeWar( clanIdOne: number, clanIdTwo: number ) : Promise<void>
    getAttackList( clanId: number ) : Promise<L2ClanWarsMixedData>
    getUnderAttackList( clanId: number ) : Promise<L2ClanWarsMixedData>
    getWarList( clanId: number ) : Promise<L2ClanWarsMixedData>
    deleteClan( clanId: number ) : Promise<void>
    getAll(): Promise<Array<L2ClanWarsTableData>>
}

export interface L2ClanWarsMixedData {
    clanName: string
    allyId: number
    allyName: string
}

export interface L2ClanWarsTableData {
    clanOneId: number
    clanTwoId: number
    wantsPeaceOne: number
    wantsPeaceTwo: number
}