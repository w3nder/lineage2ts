export interface L2ClanSkillsTableApi {
    getAll( clanId: number ) : Promise<Array<ClanSkillsDatabaseItem>>
    updateSkill( clanId: number, skillId: number, skillLevel: number ) : Promise<void>
    addSkill( clanId: number, skillId: number, skillLevel: number, skillName: string, subType: number ) : Promise<void>
    deleteClan( clanId: number ) : Promise<void>
}

export interface ClanSkillsDatabaseItem {
    skillId: number
    skillLevel: number
    pledgeId: number
}