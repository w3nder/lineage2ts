import { SiegableHall } from '../../gameService/models/entity/clanhall/SiegableHall'

export interface L2SiegableHallTableApi {
    getAll(): Promise<Array<L2SiegableHallTableData>>
    updateHalls( halls : Array<SiegableHall> ): Promise<void>
}

export interface L2SiegableHallTableData {
    hallId: number
    name: string
    ownerId: number
    description: string
    location: string
    nextSiege: number
    duration: number
    siegeStartCron: string
}