import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'
import { AbstractVariablesMap } from '../../gameService/variables/AbstractVariablesManager'

export interface L2CharacterVariablesTableApi extends L2DatabaseRemoveOperations {
    getVariables( objectId: number ): Promise<AbstractVariablesMap>
    setManyVariables( variableMap: Record<string, AbstractVariablesMap>, propertyNames: Set<string | number> ) : Promise<void>
}