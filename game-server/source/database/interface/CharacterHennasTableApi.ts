import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface L2CharacterHennasTableApi extends L2DatabaseRemoveOperations {
    getAll( objectId: number, classIndex: number ) : Promise<L2CharacterHennas>
    add( items: Array<L2CharacterHennaItem> ) : Promise<void>
    delete( items: Array<L2CharacterHennaItem> ) : Promise<void>
    deleteByClassIndex( objectId: number, classIndex: number ) : Promise<void>
}

/*
    Note that items must be sorted in ascending order by slot.
 */
export type L2CharacterHennas = Array<L2CharacterHennaItem>

export interface L2CharacterHennaItem {
    slot: number
    symbolId: number
    objectId: number
    classIndex: number
}