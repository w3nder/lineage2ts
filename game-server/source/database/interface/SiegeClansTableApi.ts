export interface L2SiegeClansTableApi {
    isRegistered( clanId: number, residenceId: number ) : Promise<boolean>
    addClan( clanId: number, residenceId: number, type: number ) : Promise<void>
    updateClan( clanId: number, residenceId: number, type: number ) : Promise<void>
    deleteClan( clanId: number, residenceId: number ) : Promise<void>
    clanInfoForCastle( residenceId: number ): Promise<Array<L2SiegeClansTableInfo>>
    hasRegistrationForAny( clanId: number, residenceIds: Array<number> ) : Promise<boolean>
}

export interface L2SiegeClansTableInfo {
    type: number
    clanId: number
}