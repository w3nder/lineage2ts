export interface L2OlympiadDataTableApi {
    addStatus( currentCycle: number, period: number, olympiadEnd: number, validationEnd: number, nextWeeklyChange: number ) : Promise<void>
}