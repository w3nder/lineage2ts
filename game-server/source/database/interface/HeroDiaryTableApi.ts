export interface L2HeroDiaryTableApi {
    addEntry( objectId: number, timestamp: number, actionId: number, value: number ) : Promise<void>
    getItems( objectId: number ): Promise<Array<L2HeroDiaryTableItem>>
    getItemsByIds( objectIds: Array<number> ): Promise<Array<L2HeroDiaryTableItem>>
}

export interface L2HeroDiaryTableItem {
    objectId: number
    time: number
    action: number
    value: number
}