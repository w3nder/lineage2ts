import { PunishmentRecord } from '../../gameService/models/punishment/PunishmentRecord'

export interface PunishmentsTableApi {
    getAll() : Promise<Array<PunishmentRecord>>
    addRecord( record: PunishmentRecord ) : Promise<void>
    removeRecords( records: Array<PunishmentRecord> ) : Promise<void>
    updateRecord( record: PunishmentRecord ): Promise<void>
}