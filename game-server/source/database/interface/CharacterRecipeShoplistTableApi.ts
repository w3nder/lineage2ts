import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

export interface CharacterRecipeShoplistTableApi {
    deletePlayer( player: L2PcInstance ) : Promise<void>
    createPlayer( player: L2PcInstance ) : Promise<void>
    getItem( objectId: number ) : Promise<Array<L2PlayerRecipeShopItem>>
}

export interface L2PlayerRecipeShopItem {
    recipeId: number
    price: number
}