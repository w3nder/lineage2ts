export interface L2AirshipsTableApi {
    getAll() : Promise<{ [ key: number ]: { [ key: string ] : any } }>
    addShip( ownerId: number, fuel: number ) : Promise<void>
    updateShip( ownerId: number, fuel: number ) : Promise<void>
}