import { L2ServitorInstance } from '../../gameService/models/actor/instance/L2ServitorInstance'

export interface ServitorsTableApi {
    removeServitor( playerId: number ) : Promise<void>
    saveServitor( servitor: L2ServitorInstance ) : Promise<void>
    getRestoredPlayerIds() : Promise<Array<number>>
    getServitor( playerId: number ) : Promise<L2ServitorTableData>
}

export interface L2ServitorTableData {
    ownerId: number
    summonSkillId: number
    hp: number
    mp: number
    remainingTime: number
}