import { AuctionableHall } from '../../gameService/models/entity/clanhall/AuctionableHall'

export interface ClanhallTableApi {
    getAll() : Promise<Array<ClanHallTableData>>
    updateHalls( items : Array<AuctionableHall> ) : Promise<void>
}

export interface ClanHallTableData {
    hallId: number
    name: string
    ownerId: number
    description: string
    location: string
    paidUntil: number
    grade: number
    isPaidOff: boolean
    leaseAmount: number
}