import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'
import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface CharacterRecommendationBonusTableApi extends L2DatabaseRemoveOperations {
    loadPlayer( player: L2PcInstance ) : Promise<number>
    storePlayer( player: L2PcInstance, endTime: number ) : Promise<void>
}