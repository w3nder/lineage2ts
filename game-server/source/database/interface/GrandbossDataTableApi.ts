export interface L2GrandbossDataTableApi {
    getAll() : Promise<Array<L2GrandbossDataItem>>
    setStatus( bossId: number, status: number ) : Promise<void>
    update( item: L2GrandbossDataItem ) : Promise<void>
}

export interface L2GrandbossDataItem {
    bossId: number
    x: number
    y: number
    z: number
    heading: number
    respawnTime: number
    currentHp: number
    currentMp: number
    status: number
}