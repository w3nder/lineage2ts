import { ClanHallFunction } from '../../gameService/models/clan/ClanHallFunction'

export interface ClanhallFunctionsTableApi {
    removeAll( hallId: number, types: Array<number> ) : Promise<void>
    removeFunction( data: ClanHallFunction ) : Promise<void>
    getById( id: number ): Promise<Array<ClanHallFunction>>
    updateFunctions( items: Array<ClanHallFunction> ): Promise<void>
}