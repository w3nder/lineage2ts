export interface L2ClanHallAuctionBidTableApi {
    removeAuction( id: number, bidderId: number ) : Promise<void>
    removeByAuctionId( auctionId: number ): Promise<void>
    getBidsByAuctionId( id: number ): Promise<Array<L2lanHallAuctionBidTableData>>
    addBid( data: L2lanHallAuctionBidTableData ): Promise<void>
    updateBid( data: L2lanHallAuctionBidTableData ): Promise<void>
}

export interface L2lanHallAuctionBidTableData {
    auctionId : number
    bidderId: number
    name: string
    maxBidAmount: number
    clanName: string
    bidTime: number
}