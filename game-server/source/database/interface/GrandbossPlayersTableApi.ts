export interface L2GrandbossPlayersTableApi {
    getAll() : Promise<Array<L2GrandbossPlayerData>>
    removeAll() : Promise<void>
    addPlayers( items: Array<L2GrandbossPlayerData> ) : Promise<void>
}

export interface L2GrandbossPlayerData {
    objectId: number
    zoneId: number
}