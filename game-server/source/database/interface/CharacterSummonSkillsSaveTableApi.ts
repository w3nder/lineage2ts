import { BuffProperties } from '../../gameService/models/skills/BuffDefinition'

export interface CharacterSummonSkillsSaveTableApi {
    update( ownerId: number, classIndex: number, summonSkillId: number, effects: Array<BuffProperties> ) : Promise<void>
    load( ownerId: number, classIndex: number, summonSkillId: number ) : Promise<Array<CharacterSummonSkillsItem>>
    delete( ownerId: number, classIndex: number, summonSkillId: number ) : Promise<void>
}

export interface CharacterSummonSkillsItem {
    skillId: number
    skillLevel: number
    remainingTime: number
}