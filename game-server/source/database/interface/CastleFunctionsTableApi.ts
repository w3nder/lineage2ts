export interface L2CastleFunctionsTableApi {
    remove( residenceId: number, type: number ) : Promise<void>
    update( residenceId: number, type: number, level: number, fee: number, rate: number, endTime: number ) : Promise<void>
    getAll( residenceId: number ) : Promise<Array<L2CastleFunctionsTableData>>
}

export interface L2CastleFunctionsTableData {
    residenceId: number
    type: number
    level: number
    fee: number
    rate: number
    endTime: number
}