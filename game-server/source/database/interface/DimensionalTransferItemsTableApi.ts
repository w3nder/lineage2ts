import { L2DimensionalTransferItem } from '../../gameService/models/items/L2DimensionalTransferItem'
import { L2DatabaseRemoveOperations } from '../operations/RemoveOperations'

export interface L2DimensionalTransferItemsTableApi extends L2DatabaseRemoveOperations {
    getItems( playerId: number ) : Promise<Array<L2DimensionalTransferItem>>
    updateItems( playerId: number, items : Array<L2DimensionalTransferItem> ) : Promise<void>
}