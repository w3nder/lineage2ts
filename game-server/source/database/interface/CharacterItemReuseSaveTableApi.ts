import { L2PcInstance } from '../../gameService/models/actor/instance/L2PcInstance'

export interface CharacterItemReuseSaveTableApi {
    deleteTimes( objectId: number ) : Promise<void>
    createTimes( player: L2PcInstance ) : Promise<void>
    getTimes( objectId: number ) : Promise<Array<L2CharacterItemReuse>>
}

export interface L2CharacterItemReuse {
    itemId: number
    reuseDelay: number
    reuseExpiration: number
}