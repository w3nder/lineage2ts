import { L2SiegeClansTableApi, L2SiegeClansTableInfo } from '../../interface/SiegeClansTableApi'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteSiegeClansTable : L2SiegeClansTableApi = {
    async hasRegistrationForAny( clanId: number, residenceIds: Array<number> ): Promise<boolean> {
        const values = _.join( _.times( residenceIds.length, _.constant( '?' ) ), ',' )
        const query = `SELECT EXISTS(SELECT 1 FROM siege_clans where clan_id=? and castle_id in (${values}))`

        let result = databaseEngine.getOne( query, clanId, ...residenceIds )
        return _.values( result )[ 0 ] === 1
    },

    async clanInfoForCastle( residenceId: number ): Promise<Array<L2SiegeClansTableInfo>> {
        const query = 'SELECT clan_id,type FROM siege_clans where castle_id=?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, residenceId )
        return databaseItems.map( ( databaseItem: any ) => {
            return {
                type: databaseItem[ 'type' ],
                clanId: databaseItem[ 'clan_id' ]
            }
        } )
    },

    async deleteClan( clanId: number, residenceId: number ): Promise<void> {
        const query = 'DELETE FROM siege_clans WHERE castle_id=? and clan_id=?'
        return databaseEngine.insertOne( query, residenceId, clanId )
    },

    async addClan( clanId: number, residenceId: number, type: number ): Promise<void> {
        const query = 'INSERT INTO siege_clans (clan_id,castle_id,type,castle_owner) values (?,?,?,0)'
        return databaseEngine.insertOne( query, clanId, residenceId, type )
    },

    async updateClan( clanId: number, residenceId: number, type: number ): Promise<void> {
        const query = 'UPDATE siege_clans SET type = ? WHERE castle_id = ? AND clan_id = ?'
        return databaseEngine.insertOne( query, type, residenceId, clanId )
    },

    async isRegistered( clanId: number, residenceId: number ): Promise<boolean> {
        const query = 'SELECT EXISTS(SELECT 1 FROM siege_clans where clan_id=? and castle_id=?)'
        let result = databaseEngine.getOne( query, clanId, residenceId )

        return _.values( result )[ 0 ] === 1
    }
}