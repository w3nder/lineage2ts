import { L2DatabaseOperations } from '../../operations/DatabaseOperations'
import { shutdown } from '../../engines/sqlite'

export const SQLiteDatabaseOperations: L2DatabaseOperations = {
    async shutdown(): Promise<void> {
        return shutdown()
    }
}