import { CharacterUIActionsTableApi } from '../../interface/CharacterUIActionsTableApi'
import { ActionKey } from '../../../gameService/models/ActionKey'
import { UIKeys } from '../../../data/interface/UIDataApi'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterUIActionsTable : CharacterUIActionsTableApi = {
    async getKeys( objectId: number ): Promise<UIKeys> {
        let query = 'SELECT * FROM character_ui_actions WHERE `objectId` = ? ORDER BY `categoryId`, `orderIndex`'
        let results : Array<unknown> = databaseEngine.getMany( query, objectId )

        let values : UIKeys = {}
        results.forEach( ( databaseItem: any ) => {
            let categoryId : number = databaseItem[ 'categoryId' ]
            if ( !_.has( values, categoryId ) ) {
                values[ categoryId ] = []
            }

            values[ categoryId ].push( {
                categoryId,
                commandId: databaseItem[ 'commandId' ],
                key: databaseItem[ 'key' ],
                toggleKeyOne: databaseItem[ 'toggleOne' ],
                toggleKeyTwo: databaseItem[ 'toggleTwo' ],
                visibleStatus: databaseItem[ 'visible' ]
            } )
        } )

        return values
    },

    async setKeys( objectId: number, data: UIKeys ): Promise<void> {
        let query = 'INSERT OR REPLACE INTO character_ui_actions (`objectId`, `categoryId`, `orderIndex`, `commandId`, `key`, `toggleOne`, `toggleTwo`, `visible`) VALUES (?,?,?,?,?,?,?,?)'
        let itemsToSave = []
        let index = 0

        _.each( data, ( list: Array<ActionKey> ) => {
            _.each( list, ( key: ActionKey ) => {
                let item = [
                    objectId,
                    key.categoryId,
                    index++,
                    key.commandId,
                    key.toggleKeyOne,
                    key.toggleKeyTwo,
                    key.visibleStatus
                ]

                itemsToSave.push( item )
            } )
        } )

        if ( itemsToSave.length > 0 ) {
            return databaseEngine.insertMany( query, itemsToSave )
        }
    }
}