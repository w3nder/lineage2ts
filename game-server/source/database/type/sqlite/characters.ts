import {
    CharactersTableApi,
    L2CharactersTableClanInfo,
    L2CharactersTableFriendInfo,
    L2CharactersTableFullInfo,
    L2CharactersTableNamePair,
} from '../../interface/CharactersTableApi'
import { L2PcInstance } from '../../../gameService/models/actor/instance/L2PcInstance'
import { PcAppearance } from '../../../gameService/models/actor/appearance/PcAppearance'
import { Sex } from '../../../gameService/enums/Sex'
import { L2ClanMember } from '../../../gameService/models/L2ClanMember'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

// TODO : add data types for player and clan, move initialization logic out of database layer
export const SQLiteCharactersTable: CharactersTableApi = {
    async getDeleteCharacterCount(): Promise<number> {
        const query = 'SELECT count(charId) as total FROM characters WHERE deletetime != 0 and deletetime < strftime(\'%s\',\'now\')'
        let databaseItem: any = databaseEngine.getOne( query )

        return _.get( databaseItem, 'total', 0 )
    },

    async getDeleteCharacterIds( amount : number ): Promise<Array<number>> {
        const query = 'SELECT charId FROM characters WHERE deletetime != 0 and deletetime < strftime(\'%s\',\'now\') limit ?'
        let databaseItems : Array<any> = databaseEngine.getMany( query, amount )

        return _.map( databaseItems, 'charId' )
    },

    async getHighestId(): Promise<number> {
        const query = 'SELECT MAX(charId) as objectId FROM characters'
        let databaseItem: any = databaseEngine.getOne( query )

        return _.get( databaseItem, 'objectId', 0 )
    },

    async updateLocation( objectId: number, x: number, y: number, z: number ): Promise<void> {
        const query = 'UPDATE characters SET x=?, y=?, z=? WHERE charId=?'
        return databaseEngine.insertOne( query, x, y, z, objectId )
    },

    async setAccessLevel( objectId: number, level: number ): Promise<void> {
        const query = 'UPDATE characters SET accesslevel=? WHERE charId=?'
        return databaseEngine.insertOne( query, level, objectId )
    },

    async getClanInfoByIds( ids: Array<number> ): Promise<Array<L2CharactersTableClanInfo>> {
        let query = 'SELECT char_name,charId,clanid FROM characters WHERE charId in (#values#)'
        let databaseItems : Array<unknown> = databaseEngine.getManyForValues( query, ids )

        return databaseItems.map( ( databaseItem: any ): L2CharactersTableClanInfo => {
            return {
                clanId: databaseItem[ 'clanid' ],
                name: databaseItem[ 'char_name' ],
                objectId: databaseItem[ 'charId' ],
            }
        } )
    },

    async setClanPrivileges( objectId: number, privilegesMask: number ): Promise<void> {
        const query = 'UPDATE characters SET clan_privs = ? WHERE charId = ?'
        return databaseEngine.insertOne( query, privilegesMask, objectId )
    },

    async getUsedIds( ids: Array<number> ): Promise<Array<number>> {
        const query = 'SELECT charId FROM characters WHERE charId in (#values#)'
        let databaseItems: Array<unknown> = databaseEngine.getManyForValues( query, ids )

        return databaseItems.map( ( item: any ) => item.charId )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM characters WHERE charId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async getFriendListItems( objectIds: Array<number> ): Promise<Array<L2CharactersTableFriendInfo>> {
        const query = 'SELECT charId, char_name, online, classid, level FROM characters WHERE charId in (#values#)'
        let databaseItems : Array<unknown> = databaseEngine.getManyForValues( query, objectIds )

        return databaseItems.map( ( databaseItem: any ): L2CharactersTableFriendInfo => {
            return {
                objectId: databaseItem[ 'charId' ],
                name: databaseItem[ 'char_name' ],
                classId: databaseItem[ 'classid' ],
                level: databaseItem[ 'level' ],
            }
        } )
    },

    async updatePledge( objectId: number, pledgeType: number ): Promise<void> {
        const query = 'UPDATE characters SET subpledge=? WHERE charId=?'
        return databaseEngine.insertOne( query, pledgeType, objectId )
    },

    async updatePowerGrade( objectId: number, grade: number ): Promise<void> {
        const query = 'UPDATE characters SET power_grade=? WHERE charId=?'
        return databaseEngine.insertOne( query, grade, objectId )
    },

    async getAccessLevelById( objectId: number ): Promise<number> {
        const query = 'SELECT accesslevel FROM characters WHERE charId=?'
        let databaseItem = databaseEngine.getOne( query, objectId )
        return _.get( databaseItem, 'accesslevel' )
    },

    async getIdByName( name: string ): Promise<number> {
        const query = 'SELECT charId FROM characters WHERE char_name=?'
        let databaseItem : unknown = databaseEngine.getOne( query, name )
        return _.get( databaseItem, 'charId', 0 )
    },

    async getOtherCharacterNames( objectId: number, accountName: string ): Promise<Map<number, string>> {
        let query = 'SELECT charId, char_name FROM characters WHERE account_name=? AND charId<>?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, accountName, objectId )

        return _.reduce( databaseItems, ( finalMap: Map<number, string>, databaseItem: any ) => {
            finalMap.set( databaseItem[ 'charId' ], databaseItem[ 'char_name' ] )
            return finalMap
        }, new Map<number, string>() )
    },

    async setApprenticeAndSponsor( objectId: number, apprenticeId: number, sponsorId: number ): Promise<void> {
        let query = 'UPDATE characters SET apprentice=?,sponsor=? WHERE charId=?'
        return databaseEngine.insertOne( query, apprenticeId, sponsorId, objectId )
    },

    async resetApprentice( objectId: number ): Promise<void> {
        let query = 'UPDATE characters SET apprentice=0 WHERE apprentice=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async resetSponsor( objectId: number ): Promise<void> {
        let query = 'UPDATE characters SET sponsor=0 WHERE sponsor=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async updateClanStatus( objectId: number, title: string, joinExpiryTime: number, createExpiryTime: number ): Promise<void> {
        let query = 'UPDATE characters SET clanid=0, title=?, clan_join_expiry_time=?, clan_create_expiry_time=?, clan_privs=0, wantspeace=0, subpledge=0, lvl_joined_academy=0, apprentice=0, sponsor=0 WHERE charId=?'
        return databaseEngine.insertOne( query, title, joinExpiryTime, createExpiryTime, objectId )
    },

    async getNameById( id: number ): Promise<string> {
        let query = 'SELECT char_name FROM characters WHERE charId=?'
        let result = databaseEngine.getOne( query, id )

        return _.get( result, 'char_name' )
    },

    async getNameByIds( ids: Array<number> ): Promise<Array<L2CharactersTableNamePair>> {
        let query = 'SELECT char_name,charId FROM characters WHERE charId in (#values#)'
        let databaseItems : Array<unknown> = databaseEngine.getManyForValues( query, ids )

        return databaseItems.map( ( databaseItem: any ): L2CharactersTableNamePair => {
            return {
                name: databaseItem[ 'char_name' ],
                objectId: databaseItem[ 'charId' ],
            }
        } )
    },

    async addPlayerCharacter( player: L2PcInstance ): Promise<void> {
        let query = 'INSERT INTO characters (account_name,charId,char_name,level,maxHp,curHp,maxCp,curCp,maxMp,curMp,face,hairStyle,hairColor,sex,exp,sp,karma,fame,pvpkills,pkkills,clanid,race,classid,deletetime,cancraft,title,title_color,accesslevel,online,isin7sdungeon,clan_privs,wantspeace,base_class,nobless,power_grade,createDate) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)'

        return databaseEngine.insertOne( query,
                player.getAccountName(),
                player.getObjectId(),
                player.getName(),
                player.getBaseLevel(),

                player.getMaxHp(),
                player.getCurrentHp(),
                player.getMaxCp(),
                player.getCurrentCp(),
                player.getMaxMp(),
                player.getCurrentMp(),

                player.getAppearance().getFace(),
                player.getAppearance().getHairStyle(),
                player.getAppearance().getHairColor(),
                player.getAppearance().getSex() ? 1 : 0,

                player.getBaseExp(),
                player.getBaseSp(),
                player.getKarma(),
                player.getFame(),

                player.getPvpKills(),
                player.getPkKills(),
                player.getClanId(),
                player.getRace(),

                player.getClassId(),
                player.getDeleteTimer(),
                player.hasDwarvenCraft() ? 1 : 0,
                player.getTitle(),

                player.getAppearance().getTitleColor(),
                player.getAccessLevel().level,
                player.isOnlineValue(),
                player.isIn7sDungeon() ? 1 : 0,

                player.getClanPriviledgesBitmask(),
                player.getWantsPeace(),
                player.getBaseClass(),

                player.isNoble() ? 1 : 0,
                0,
                player.getCreateDate(),
        )
    },

    async getAccountCharacterCount( accountName: string ): Promise<number> {
        let query = 'SELECT COUNT(char_name) FROM characters WHERE account_name=?'
        let result = databaseEngine.getOne( query, accountName )

        return _.values( result )[ 0 ]
    },

    async isExistingCharacterName( accountName: string ): Promise<boolean> {
        const query = 'SELECT EXISTS(SELECT 1 FROM characters WHERE char_name = ?)'
        let result = databaseEngine.getOne( query, accountName )

        return _.values( result )[ 0 ] === 1
    },

    async getClanIdForCharacter( characterId: number ): Promise<number> {
        let query = 'SELECT clanId FROM characters WHERE charId=?'
        let result = databaseEngine.getOne( query, characterId )

        return _.get( result[ 0 ], 'clanId' )
    },

    async updateDeleteTime( characterId: number, days: number, useDirectValue: boolean = false ): Promise<void> {
        let query = 'UPDATE characters SET deletetime=? WHERE charId=?'
        let deletetime = useDirectValue ? days : Date.now() + 86400000 * days
        return databaseEngine.insertOne( query, deletetime, characterId )
    },

    async updatePlayerCharacter( player: L2PcInstance ): Promise<void> {
        let query = 'UPDATE characters SET level=?,maxHp=?,curHp=?,maxCp=?,curCp=?,maxMp=?,curMp=?,face=?,hairStyle=?,hairColor=?,sex=?,heading=?,x=?,y=?,z=?,exp=?,expBeforeDeath=?,sp=?,karma=?,fame=?,pvpkills=?,pkkills=?,clanid=?,race=?,classid=?,deletetime=?,title=?,title_color=?,online=?,isin7sdungeon=?,clan_privs=?,wantspeace=?,base_class=?,onlinetime=?,nobless=?,power_grade=?,subpledge=?,lvl_joined_academy=?,apprentice=?,sponsor=?,clan_join_expiry_time=?,clan_create_expiry_time=?,char_name=?,death_penalty_level=?,bookmarkslot=?,vitality_points=? WHERE charId=?'
        let totalOnlineTime = player.getOnlineTime()
        if ( player.getOnlineBeginTime() > 0 ) {
            totalOnlineTime += Math.floor( ( Date.now() - player.getOnlineBeginTime() ) / 1000 )
        }

        return databaseEngine.insertOne(
                query,
                player.getBaseLevel(),
                player.getMaxHp(),
                player.getCurrentHp(),
                player.getMaxCp(),
                player.getCurrentCp(),
                player.getMaxMp(),
                player.getCurrentMp(),
                player.getAppearance().getFace(),
                player.getAppearance().getHairStyle(),
                player.getAppearance().getHairColor(),
                player.getAppearance().getSex() ? 1 : 0,
                player.getHeading(),
                player.inObserverMode() ? player.getLastLocation().getX() : player.getX(),
                player.inObserverMode() ? player.getLastLocation().getY() : player.getY(),
                player.inObserverMode() ? player.getLastLocation().getZ() : player.getZ(),
                player.getBaseExp(),
                player.getExpBeforeDeath(),
                player.getBaseSp(),
                player.getKarma(),
                player.getFame(),
                player.getPvpKills(),
                player.getPkKills(),
                player.getClanId(),
                player.getRace(),
                player.getClassId(),
                player.getDeleteTimer(),
                player.getTitle(),
                player.getAppearance().getTitleColor(),
                player.isOnlineValue(),
                player.isIn7sDungeon() ? 1 : 0,
                player.getClanPriviledgesBitmask(),
                player.getWantsPeace(),
                player.getBaseClass(),
                totalOnlineTime,
                player.isNoble() ? 1 : 0,
                player.getPowerGrade(),
                player.getPledgeType(),
                player.getLevelJoinedAcademy(),
                player.getApprentice(),
                player.getSponsor(),
                player.getClanJoinExpiryTime(),
                player.getClanCreateExpiryTime(),
                player.getName(),
                player.getDeathPenaltyBuffLevel(),
                player.getAvailableBookmarkSlots(),
                player.getVitalityPoints(),
                player.getObjectId(),
        )
    },

    async updateOnlineStatus( player: L2PcInstance ): Promise<void> {
        let query = 'UPDATE characters SET online=?, lastAccess=? WHERE charId=?'
        return databaseEngine.insertOne( query, player.isOnlineValue(), Date.now(), player.getObjectId() )
    },

    async updateKarmaStatus( karma: number, pkKills: number, playerId: number ): Promise<void> {
        let query = 'UPDATE characters SET karma=?, pkkills=? WHERE charId=?'
        return databaseEngine.insertOne( query, karma, pkKills, playerId )
    },

    async getCharactersDeleteTime( accountName ): Promise<Array<number>> {
        let query = 'SELECT deletetime FROM characters WHERE account_name=?'
        return databaseEngine.getMany( query, accountName )
    },

    async getActiveCharacters( accountName: string ): Promise<Array<L2CharactersTableFullInfo>> {
        let query = 'SELECT * FROM characters WHERE account_name=? and ( deletetime = 0 or deletetime > strftime(\'%s\',\'now\') ) ORDER BY createDate'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, accountName )

        return databaseItems.map( ( databaseItem: any ): L2CharactersTableFullInfo => {
            return {
                accessLevel: databaseItem[ 'accesslevel' ],
                baseClassId: databaseItem[ 'base_class' ],
                clanId: databaseItem[ 'clanid' ],
                classId: databaseItem[ 'classid' ],
                currentCp: databaseItem[ 'curCp' ],
                currentHp: databaseItem[ 'curHp' ],
                currentMp: databaseItem[ 'curMp' ],
                deleteTime: databaseItem[ 'deletetime' ],
                exp: databaseItem[ 'exp' ],
                face: databaseItem[ 'face' ],
                hairColor: databaseItem[ 'hairColor' ],
                hairStyle: databaseItem[ 'hairStyle' ],
                karma: databaseItem[ 'karma' ],
                lastAccessTime: databaseItem[ 'lastAccess' ],
                level: databaseItem[ 'level' ],
                maxCp: databaseItem[ 'maxCp' ],
                maxHp: databaseItem[ 'maxHp' ],
                maxMp: databaseItem[ 'maxMp' ],
                name: databaseItem[ 'char_name' ],
                objectId: databaseItem[ 'charId' ],
                pkKills: databaseItem[ 'pkkills' ],
                pvpKills: databaseItem[ 'pvpkills' ],
                race: databaseItem[ 'race' ],
                sex: databaseItem[ 'sex' ],
                sp: databaseItem[ 'sp' ],
                vitalityPoints: databaseItem[ 'vitality_points' ],
                x: databaseItem[ 'x' ],
                y: databaseItem[ 'y' ],
                z: databaseItem[ 'z' ],
            }
        } )
    },

    async loadPlayer( objectId: number ): Promise<L2PcInstance> {
        let query = 'SELECT * FROM characters WHERE charId=?'
        let playerData : Object = databaseEngine.getOne( query, objectId )

        if ( _.isEmpty( playerData ) ) {
            return null
        }


        let appearance = new PcAppearance(
                playerData[ 'face' ],
                playerData[ 'hairColor' ],
                playerData[ 'hairStyle' ],
                playerData[ 'sex' ] !== Sex.MALE,
        )

        let classId = playerData[ 'classid' ]
        let player: L2PcInstance = new L2PcInstance( objectId, classId, playerData[ 'account_name' ], appearance )

        player.setAccessLevel( playerData[ 'accesslevel' ] )
        player.setLastAccess( playerData[ 'lastAccess' ] )
        player.setExp( playerData[ 'exp' ] )
        player.setExpBeforeDeath( playerData[ 'expBeforeDeath' ] )

        player.setLevel( playerData[ 'level' ] )
        player.setSp( playerData[ 'sp' ] )
        player.setWantsPeace( playerData[ 'wantspeace' ] )
        player.setHeading( playerData[ 'heading' ] )

        player.karma = playerData[ 'karma' ]
        player.fame = playerData[ 'fame' ]
        player.pvpKills = playerData[ 'pvpkills' ]
        player.pkKills = playerData[ 'pkkills' ]

        player.setOnlineTime( playerData[ 'onlinetime' ] )
        await player.setNoble( playerData[ 'nobless' ] === 1 )
        player.setClanJoinExpiryTime( playerData[ 'clan_join_expiry_time' ] )

        player.setClanCreateExpiryTime( playerData[ 'clan_create_expiry_time' ] )
        player.setPowerGrade( playerData[ 'power_grade' ] )
        player.setPledgeType( playerData[ 'subpledge' ] )
        player.setDeleteTime( playerData[ 'deletetime' ] )

        player.setName( playerData[ 'char_name' ] )
        player.setTitle( playerData[ 'title' ] )
        player.getAppearance().setTitleColor( playerData[ 'title_color' ] )
        player.setUptime( Date.now() )

        player.setCurrentCp( playerData[ 'curCp' ] )
        player.setCurrentHp( playerData[ 'curHp' ] )
        player.setCurrentMp( playerData[ 'curMp' ] )
        player.setClassIndex( 0 )

        player.setBaseClass( playerData[ 'base_class' ] )
        player.setApprentice( playerData[ 'apprentice' ] )
        player.setSponsor( playerData[ 'sponsor' ] )
        player.setLevelJoinedAcademy( playerData[ 'lvl_joined_academy' ] )

        player.setIsIn7sDungeon( playerData[ 'isin7sdungeon' ] === 1 )
        player.setDeathPenaltyBuffLevel( playerData[ 'death_penalty_level' ] )
        player.setVitalityPoints( playerData[ 'vitality_points' ] )

        player.setXYZInvisible( playerData[ 'x' ], playerData[ 'y' ], playerData[ 'z' ] )
        player.setAvailableBookmarkSlots( playerData[ 'bookmarkslot' ] )
        player.setCreateDate( playerData[ 'createDate' ] )
        player.setClanId( playerData[ 'clanid' ] )

        return player
    },

    async getClanMembers( clanId: number ): Promise<Array<L2ClanMember>> {
        let query = 'SELECT char_name,level,classid,charId,title,power_grade,subpledge,apprentice,sponsor,sex,race FROM characters WHERE clanid=?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, clanId )

        return databaseItems.map( ( databaseItem: any ) => {
            let member = new L2ClanMember( clanId )

            member.name = databaseItem[ 'char_name' ]
            member.level = databaseItem[ 'level' ]
            member.classId = databaseItem[ 'classid' ]
            member.objectId = databaseItem[ 'charId' ]

            member.pledgeType = databaseItem[ 'subpledge' ]
            member.title = databaseItem[ 'title' ]
            member.powerGrade = databaseItem[ 'power_grade' ]
            member.apprentice = databaseItem[ 'apprentice' ]

            member.sponsor = databaseItem[ 'sponsor' ]
            member.sex = databaseItem[ 'sex' ] !== 0
            member.race = databaseItem[ 'race' ]

            return member
        } )
    }
}