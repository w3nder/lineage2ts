import { L2CrestsTableApi } from '../../interface/CrestsTableApi'
import { CrestType, L2Crest } from '../../../gameService/models/L2Crest'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCrestsTable : L2CrestsTableApi = {
    async addCrest( id: number, image: Buffer, type: CrestType ): Promise<void> {
        const query = 'INSERT INTO `crests`(`crest_id`, `data`, `type`) VALUES(?, ?, ?)'
        return databaseEngine.insertOne( query, id, image, type )
    },

    async getAll(): Promise<Array<L2Crest>> {
        const query = 'SELECT `crest_id`, `data`, `type` FROM `crests`'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )
        return databaseItems.map( ( databaseItem: any ) => {

            const crest = new L2Crest()

            crest.id = databaseItem[ 'crest_id' ]
            crest.imageData = databaseItem[ 'data' ]
            crest.type = databaseItem[ 'type' ]

            return crest
        } )
    },

    async removeCrest( id: number ): Promise<void> {
        const query = 'DELETE FROM `crests` WHERE `crest_id` = ?'
        return databaseEngine.insertOne( query, id )
    }
}