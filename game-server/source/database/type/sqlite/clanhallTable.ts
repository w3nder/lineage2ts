import { ClanhallTableApi, ClanHallTableData } from '../../interface/ClanhallTableApi'
import { AuctionableHall } from '../../../gameService/models/entity/clanhall/AuctionableHall'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteClanhallTable: ClanhallTableApi = {
    async updateHalls( items : Array<AuctionableHall> ): Promise<void> {
        const query = 'UPDATE clanhall SET ownerId=?, paidUntil=?, paid=? WHERE id=?'
        let parameters = items.map( ( clanHall : AuctionableHall ) : Array<number> => {
            return [
                clanHall.getOwnerId(),
                clanHall.paidUntil,
                clanHall.isPaidOff ? 1 : 0,
                clanHall.getId()
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async getAll(): Promise<Array<ClanHallTableData>> {
        const query = 'SELECT * FROM clanhall'

        let results : Array<unknown> = databaseEngine.getMany( query )
        return results.map( ( item: any ): ClanHallTableData => {
            return {
                description: item.desc,
                grade: item.Grade,
                hallId: item.id,
                isPaidOff: item.paid !== 0,
                leaseAmount: item.lease,
                location: item.location,
                name: item.name,
                ownerId: item.ownerId,
                paidUntil: item.paidUntil,
            }
        } )
    },
}