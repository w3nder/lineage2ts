import { L2CastleDoorUpgradeTableApi, L2CastleDoorUpgradeTableData } from '../../interface/CastleDoorUpgradeTableApi'
import { getMany, insertOne } from '../../engines/sqlite'

export const SQLiteCastleDoorUpgradeTable: L2CastleDoorUpgradeTableApi = {
    async update( residenceId: number, doorId: number, ratio: number ): Promise<void> {
        const query = 'REPLACE INTO castle_doorupgrade (doorId, ratio, castleId) values (?,?,?)'
        return insertOne( query, doorId, ratio, residenceId )
    },

    async getAll( residenceId: number ): Promise<Array<L2CastleDoorUpgradeTableData>> {
        const query = 'SELECT * FROM castle_doorupgrade WHERE castleId=?'
        let databaseItems : Array<unknown> = getMany( query, residenceId )
        return databaseItems.map( ( databaseItem: any ): L2CastleDoorUpgradeTableData => {
            return {
                doorId: databaseItem[ 'doorId' ],
                ratio: databaseItem[ 'ratio' ],
            }
        } )
    },

    async remove( residenceId: number ): Promise<void> {
        const query = 'DELETE FROM castle_doorupgrade WHERE castleId=?'
        return insertOne( query, residenceId )
    }
}