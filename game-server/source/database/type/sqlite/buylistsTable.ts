import { L2BuylistsTableApi, L2BuylistsTableItem } from '../../interface/BuylistsTableApi'
import { Product } from '../../../gameService/models/buylist/Product'
import { getMany, insertMany } from '../../engines/sqlite'

export const SQLiteBuylistsTable: L2BuylistsTableApi = {
    async getAll(): Promise<Array<L2BuylistsTableItem>> {
        const query = 'SELECT * FROM buylists'
        let databaseItems: Array<unknown> = getMany( query )

        return databaseItems.map( ( databaseItem: Object ) => {
            return {
                id: databaseItem[ 'buylist_id' ],
                itemId: databaseItem[ 'item_id' ],
                count: databaseItem[ 'count' ],
                restockTime: databaseItem[ 'next_restock_time' ]
            }
        } )
    },

    async saveProducts( products: Array<Product> ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO buylists (buylist_id, item_id, count, next_restock_time) VALUES(?, ?, ?, ?)'
        return insertMany( query, products.map( ( product: Product ): Array<number> => {
            return [
                product.listId,
                product.getItemId(),
                product.count,
                product.futureRestockTime
            ]
        } ) )
    },

    async removeProducts( products: Array<Product> ): Promise<void> {
        const query = 'DELETE FROM buylists WHERE buylist_id=? and item_id=?'
        return insertMany( query, products.map( ( product: Product ): Array<number> => {
            return [
                product.listId,
                product.getItemId(),
            ]
        } ) )
    }
}