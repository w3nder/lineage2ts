import { ClanSkillsDatabaseItem, L2ClanSkillsTableApi } from '../../interface/ClanSkillsTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteClanSkillsTable: L2ClanSkillsTableApi = {
    async deleteClan( clanId: number ): Promise<void> {
        const query = 'DELETE FROM clan_skills WHERE clan_id=?'
        return databaseEngine.insertOne( query, clanId )
    },

    async addSkill( clanId: number, skillId: number, skillLevel: number, skillName: string, subType: number ): Promise<void> {
        const query = 'INSERT INTO clan_skills (clan_id,skill_id,skill_level,skill_name,sub_pledge_id) VALUES (?,?,?,?,?)'
        return databaseEngine.insertOne( query, clanId, skillId, skillLevel, skillName, subType )
    },

    async updateSkill( clanId: number, skillId: number, skillLevel: number ): Promise<void> {
        const query = 'UPDATE clan_skills SET skill_level=? WHERE skill_id=? AND clan_id=?'
        return databaseEngine.insertOne( query, skillLevel, skillId, clanId )
    },

    async getAll( clanId: number ): Promise<Array<ClanSkillsDatabaseItem>> {
        let query = 'SELECT skill_id,skill_level,sub_pledge_id FROM clan_skills WHERE clan_id=?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, clanId )
        return databaseItems.map( ( databaseItem: any ) => {
            let item: ClanSkillsDatabaseItem = {
                pledgeId: databaseItem[ 'sub_pledge_id' ],
                skillId: databaseItem[ 'skill_id' ],
                skillLevel: databaseItem[ 'skill_level' ]
            }

            return item
        } )
    }

}