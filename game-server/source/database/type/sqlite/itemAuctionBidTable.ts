import { L2ItemAuctionBidTableApi, L2ItemAuctionBidTableData } from '../../interface/ItemAuctionBidTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteItemAuctionBidTable : L2ItemAuctionBidTableApi = {
    async getAuctionBids( auctionIds: Array<number> ): Promise<Array<L2ItemAuctionBidTableData>> {
        const query = 'SELECT * FROM item_auction_bid WHERE id in (#values#)'
        let databaseItems : Array<unknown> = databaseEngine.getManyForValues( query, auctionIds )
        return databaseItems.map( ( databaseItem: any ) : L2ItemAuctionBidTableData => {
            return {
                amount: databaseItem[ 'amount' ],
                playerId: databaseItem[ 'playerId' ],
                id: databaseItem[ 'id' ]
            }
        } )
    },

    async deleteAuctions( ids: Array<number> ): Promise<void> {
        const query = 'DELETE FROM item_auction_bid WHERE id in (#values#)'
        return databaseEngine.deleteManyForValues( query, ids )
    },

    async addBid( id: number, playerId: number, amount: number ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO item_auction_bid (id, playerId, amount) VALUES (?, ?, ?)'
        return databaseEngine.insertOne( query, id, playerId, amount )
    },

    async deleteBid( id: number, playerId: number ): Promise<void> {
        const query = 'DELETE FROM item_auction_bid WHERE id = ? AND playerId = ?'
        return databaseEngine.insertOne( query, id, playerId )
    }
}