import { CharacterRecommendationBonusTableApi } from '../../interface/CharacterRecommendationBonusTableApi'
import { L2PcInstance } from '../../../gameService/models/actor/instance/L2PcInstance'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterRecommendationBonusTable : CharacterRecommendationBonusTableApi = {
    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM character_reco_bonus WHERE charId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async storePlayer( player: L2PcInstance, endTime: number ): Promise<void> {
        let query = 'INSERT INTO character_reco_bonus (charId,rec_have,rec_left,time_left) VALUES (?,?,?,?) ON CONFLICT (charId) DO UPDATE SET rec_have=?, rec_left=?, time_left=?'
        return databaseEngine.insertOne(
            query,
            player.getObjectId(),
            player.getRecommendationsHave(),
            player.getRecommendationsLeft(),
            endTime,
            player.getRecommendationsHave(),
            player.getRecommendationsLeft(),
            endTime )
    },

    async loadPlayer( player: L2PcInstance ): Promise<number> {
        let query = 'SELECT rec_have,rec_left,time_left FROM character_reco_bonus WHERE charId=? LIMIT 1'
        let result = databaseEngine.getOne( query, player.getObjectId() )

        if ( !_.isEmpty( result ) ) {
            player.setRecommendationsHave( result[ 'rec_have' ] )
            player.setRecommendationsLeft( result[ 'rec_left' ] )

            return result[ 'time_left' ]
        }

        return 0
    }

}