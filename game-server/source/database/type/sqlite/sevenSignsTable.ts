import { L2SevenSignsCharacterData, SevenSignsTableApi } from '../../interface/SevenSignsTableApi'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'
import { SevenSignsSide } from '../../../gameService/values/SevenSignsValues'
import { EnumToSeal, EnumToSide, SealToEnum, SideToEnum } from '../../../gameService/enums/SevenSigns'

export const SQLiteSevenSignsTable: SevenSignsTableApi = {
    getPlayer( objectId: number ): Promise<L2SevenSignsCharacterData> {
        const query = 'SELECT * FROM seven_signs WHERE objectId=?'
        let databaseItem : unknown = databaseEngine.getOne( query, objectId )

        if ( !databaseItem ) {
            return
        }

        return Promise.resolve( {
            ancientAdenaCount: databaseItem[ 'ancientAdenaAmount' ],
            blueStoneCount: databaseItem[ 'blueStones' ],
            side: SideToEnum[ databaseItem[ 'side' ] as number ],
            contributionScore: databaseItem[ 'contributionScore' ],
            greenStoneCount: databaseItem[ 'greenStones' ],
            redStoneCount: databaseItem[ 'redStones' ],
            seal: SealToEnum[ databaseItem[ 'seal' ] as number ],
            objectId: databaseItem[ 'objectId' ],
            lastContributionTime: databaseItem[ 'contributionTime' ],
            joinTime: databaseItem[ 'joinTime' ]
        } )
    },

    savePlayers( playerData: Array<L2SevenSignsCharacterData> ): Promise<void> {
        const query = 'INSERT OR REPLACE into seven_signs (objectId, side, seal, redStones, greenStones, blueStones, ancientAdenaAmount, contributionScore, contributionTime, joinTime) values (?,?,?,?,?,?,?,?,?,?)'

        let items = _.map( playerData, ( data: L2SevenSignsCharacterData ) => {
            return [
                data.objectId,
                EnumToSide[ data.side ],
                EnumToSeal[ data.seal ],
                data.redStoneCount,
                data.greenStoneCount,
                data.blueStoneCount,
                data.ancientAdenaCount,
                data.contributionScore,
                data.lastContributionTime,
                data.joinTime,
            ]
        } )

        databaseEngine.insertMany( query, items )

        return Promise.resolve()
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM seven_signs WHERE objectId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async removeAll() : Promise<void> {
        const query = 'DELETE FROM seven_signs'
        return databaseEngine.insertOne( query )
    },

    async getSideTotals() : Promise<Record<Partial<SevenSignsSide>, number>> {
        const query = 'select side, count(*) as amount from seven_signs GROUP BY side'
        let databaseItems = databaseEngine.getMany( query )

        return databaseItems.reduce( ( totals: Record<Partial<SevenSignsSide>, number>, databaseItem: unknown ) : Record<Partial<SevenSignsSide>, number> => {
            let side = SideToEnum[ databaseItem[ 'side' ] as number ]
            totals[ side ] = databaseItem[ 'amount' ]
            return totals
        }, {
            [ SevenSignsSide.Dawn ]: 0,
            [ SevenSignsSide.Dusk ]: 0,
        } )
    }
}