import { L2OlympiadNoblesTableApi } from '../../interface/OlympiadNoblesTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteOlympiadNoblesTable : L2OlympiadNoblesTableApi = {
    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM olympiad_nobles WHERE charId=?'
        return databaseEngine.insertOne( query, objectId )
    }
}