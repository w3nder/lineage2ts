import { CastleManorProcureTableApi, L2CastleManorProcureTableData } from '../../interface/CastleManorProcureTableApi'
import { CropProcure } from '../../../gameService/models/manor/CropProcure'
import _ from 'lodash'
import { getMany, insertMany, insertOne } from '../../engines/sqlite'

export const SQLiteCastleManorProcureTable: CastleManorProcureTableApi = {
    async addAll( items: Array<L2CastleManorProcureTableData> ): Promise<void> {
        const query = 'INSERT INTO castle_manor_procure VALUES (?, ?, ?, ?, ?, ?, ?)'
        let values = _.map( items, ( item: L2CastleManorProcureTableData ) => {
            return [
                item.castleId,
                item.cropId,
                item.amount,
                item.startAmount,
                item.price,
                item.rewardType,
                item.nextPeriod,
            ]
        } )

        return insertMany( query, values )
    },

    async deleteAll(): Promise<void> {
        const query = 'DELETE FROM castle_manor_procure'
        return insertOne( query )
    },

    async getAll(): Promise<Array<L2CastleManorProcureTableData>> {
        const query = 'SELECT * FROM castle_manor_procure'
        let databaseItems : Array<unknown> = getMany( query )

        return databaseItems.map( ( databaseItem: any ): L2CastleManorProcureTableData => {
            return {
                amount: databaseItem[ 'amount' ],
                castleId: databaseItem[ 'castle_id' ],
                cropId: databaseItem[ 'crop_id' ],
                nextPeriod: databaseItem[ 'next_period' ],
                price: databaseItem[ 'price' ],
                rewardType: databaseItem[ 'reward_type' ],
                startAmount: databaseItem[ 'start_amount' ],
            }
        } )
    },

    async removeCastleForNextPeriod( castleId: number ): Promise<void> {
        const query = 'DELETE FROM castle_manor_procure WHERE castle_id = ? AND next_period = 1'
        return insertOne( query, castleId )
    },

    async addCastleProducts( castleId: number, items: Array<CropProcure> ): Promise<void> {
        const query = 'INSERT INTO castle_manor_procure VALUES (?, ?, ?, ?, ?, ?, ?)'
        let data = _.map( items, ( item: CropProcure ) => {
            return [
                castleId,
                item.getId(),
                item.getAmount(),
                item.getStartAmount(),
                item.getPrice(),
                item.getReward(),
                'true',
            ]
        } )
        return insertMany( query, data )
    },

    async updateItems( castleId: number, items: Array<CropProcure> ): Promise<void> {
        const query = 'UPDATE castle_manor_procure SET amount = ? WHERE castle_id = ? AND crop_id = ? AND next_period = 0'
        let data = _.map( items, ( item: CropProcure ) => {
            return [
                item.getAmount(),
                castleId,
                item.getId(),
            ]
        } )

        return insertMany( query, data )
    },

    async removeCastle( castleId: number ): Promise<void> {
        let query = 'DELETE FROM castle_manor_procure WHERE castle_id = ?'
        return insertOne( query, castleId )
    },
}