import { L2ClanWarsMixedData, L2ClanWarsTableApi, L2ClanWarsTableData } from '../../interface/ClanWarsTableApi'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteClanWarsTable : L2ClanWarsTableApi = {
    async getAll(): Promise<Array<L2ClanWarsTableData>> {
        const query = 'select * from clan_wars'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ) : L2ClanWarsTableData => {
            return {
                clanOneId: _.parseInt( databaseItem[ 'clan1' ] ),
                clanTwoId: _.parseInt( databaseItem[ 'clan2' ] ),
                wantsPeaceOne: databaseItem[ 'wantspeace1' ],
                wantsPeaceTwo: databaseItem[ 'wantspeace2' ]
            }
        } )
    },

    async deleteClan( clanId: number ): Promise<void> {
        const query = 'DELETE FROM clan_wars WHERE clan1=? OR clan2=?'
        return databaseEngine.insertOne( query, clanId )
    },

    async getAttackList( clanId: number ): Promise<L2ClanWarsMixedData> {
        const query = 'SELECT clan_name,clan_id,ally_id,ally_name FROM clan_data,clan_wars WHERE clan1=? AND clan_id=clan2 AND clan2 NOT IN (SELECT clan1 FROM clan_wars WHERE clan2=?)'
        let databaseItem = databaseEngine.getOne( query, clanId, clanId )

        return {
            allyId: databaseItem[ 'ally_id' ],
            allyName: databaseItem[ 'ally_name' ],
            clanName: databaseItem[ 'clan_name' ]
        }
    },

    async getUnderAttackList( clanId: number ): Promise<L2ClanWarsMixedData> {
        const query = 'SELECT clan_name,clan_id,ally_id,ally_name FROM clan_data,clan_wars WHERE clan2=? AND clan_id=clan1 AND clan1 NOT IN (SELECT clan2 FROM clan_wars WHERE clan1=?)'
        let databaseItem = databaseEngine.getOne( query, clanId, clanId )
        return {
            allyId: databaseItem[ 'ally_id' ],
            allyName: databaseItem[ 'ally_name' ],
            clanName: databaseItem[ 'clan_name' ]
        }
    },

    async getWarList( clanId: number ): Promise<L2ClanWarsMixedData> {
        const query = 'SELECT clan_name,clan_id,ally_id,ally_name FROM clan_data,clan_wars WHERE clan1=? AND clan_id=clan2 AND clan2 IN (SELECT clan1 FROM clan_wars WHERE clan2=?)'
        let databaseItem = databaseEngine.getOne( query, clanId, clanId )

        return {
            allyId: databaseItem[ 'ally_id' ],
            allyName: databaseItem[ 'ally_name' ],
            clanName: databaseItem[ 'clan_name' ]
        }
    },

    async removeWar( clanIdOne: number, clanIdTwo: number ): Promise<void> {
        const query = 'DELETE FROM clan_wars WHERE clan1=? AND clan2=?'
        return databaseEngine.insertOne( query, clanIdOne, clanIdTwo )
    },

    async setWar( clanIdOne: number, clanIdTwo: number ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO clan_wars (clan1, clan2, wantspeace1, wantspeace2) VALUES(?,?,?,?)'
        return databaseEngine.insertOne( query, clanIdOne, clanIdTwo, 0, 0 )
    }
}