import { L2DimensionalTransferItemsTableApi } from '../../interface/DimensionalTransferItemsTableApi'
import { L2DimensionalTransferItem } from '../../../gameService/models/items/L2DimensionalTransferItem'
import * as databaseEngine from '../../engines/sqlite'

function removeCharacterData( playerId: number ) : void {
    const query = 'DELETE FROM dimensional_transfer_items WHERE playerId=? '
    return databaseEngine.insertOne( query, playerId )
}

export const SQLiteDimensionalTransferItems: L2DimensionalTransferItemsTableApi = {
    async removeCharacter( playerId: number ): Promise<void> {
        return removeCharacterData( playerId )
    },

    async getItems( objectId: number ): Promise<Array<L2DimensionalTransferItem>> {
        const query = 'SELECT * FROM dimensional_transfer_items WHERE playerId=?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, objectId )

        return databaseItems.map( ( data: any ) => {
            return {
                itemId: data.itemId,
                amount: data.amount,
                sender: data.sender,
                createDate: data.createDate,
                updateDate: data.updateDate
            }
        } )
    },

    async updateItems( playerId: number, items : Array<L2DimensionalTransferItem> ) {
        removeCharacterData( playerId )

        const query = 'INSERT INTO dimensional_transfer_items (playerId, itemId, amount, sender, createDate, updateDate) VALUES (?, ?, ?, ?, ?, ?)'
        let dataItems = items.map( ( item: L2DimensionalTransferItem ) => {
            return [
                playerId,
                item.itemId,
                item.amount,
                item.sender,
                item.createDate,
                item.updateDate
            ]
        } )

        return databaseEngine.insertMany( query, dataItems )
    }
}