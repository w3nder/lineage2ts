import { L2ClanNoticesTableApi, L2ClanNoticesTableItem, L2ClanNoticeStatus } from '../../interface/ClanNoticesTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteClanNoticesTable : L2ClanNoticesTableApi = {
    async createNotice( clanId: number, text: string, status: L2ClanNoticeStatus ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO clan_notices (clanId,text,status) VALUES (?,?,?)'
        return databaseEngine.insertOne( query, clanId, text, status )
    },

    async deleteClan( clanId: number ): Promise<void> {
        const query = 'DELETE FROM clan_notices WHERE clanId=?'
        return databaseEngine.insertOne( query, clanId )
    },

    async getNotice( clanId: number ): Promise<L2ClanNoticesTableItem> {
        let query = 'SELECT status,text FROM clan_notices WHERE clanId=?'
        let databaseItem : any = databaseEngine.getOne( query, clanId )

        if ( databaseItem ) {
            return {
                status: databaseItem.status,
                text: databaseItem.text,
                clanId
            }
        }

        return null
    }
}