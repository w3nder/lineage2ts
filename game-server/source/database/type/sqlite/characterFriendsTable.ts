import { CharacterFriendsTableApi, L2CharacterFriendUpdate } from '../../interface/CharacterFriendsTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterFriendsTable : CharacterFriendsTableApi = {
    async addBlocklists( items: Array<L2CharacterFriendUpdate> ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO character_friends (objectId, friendId, relationType) VALUES (?, ?, 1)'
        const parameters = items.map( item => {
            return [
                item.fromId,
                item.toId
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async removeBlocklists( items: Array<L2CharacterFriendUpdate> ): Promise<void> {
        const query = 'DELETE FROM character_friends WHERE objectId=? AND friendId=? AND relationType=1'
        const parameters = items.map( item => {
            return [
                item.fromId,
                item.toId
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async addFriends( items: Array<L2CharacterFriendUpdate> ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO character_friends (objectId, friendId) VALUES (?, ?), (?, ?)'
        const parameters = items.map( item => {
            return [
                item.fromId,
                item.toId,
                item.toId,
                item.fromId
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async removeFriends( items: Array<L2CharacterFriendUpdate> ): Promise<void> {
        const query = 'DELETE FROM character_friends WHERE (objectId=? AND friendId=?) OR (objectId=? AND friendId=?)'
        const parameters = items.map( item => {
            return [
                item.fromId,
                item.toId,
                item.toId,
                item.fromId
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM character_friends WHERE objectId=? OR friendId=?'
        return databaseEngine.insertOne( query, objectId, objectId )
    },

    async getFriendIds( objectId: number ): Promise<Array<number>> {
        const query = 'SELECT friendId FROM character_friends WHERE objectId=? AND relationType=0 AND friendId<>objectId'
        let databaseItems = databaseEngine.getMany( query, objectId )

        return databaseItems.map( item => item[ 'friendId' ] )
    },

    async getBlocklistIds( objectId: number ): Promise<Array<number>> {
        let query = 'SELECT friendId FROM character_friends WHERE objectId=? AND relationType=1'
        let databaseItems = databaseEngine.getMany( query, objectId )

        return databaseItems.map( item => item[ 'friendId' ] )
    },
}