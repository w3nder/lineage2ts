import {
    L2CharacterHennaItem,
    L2CharacterHennas,
    L2CharacterHennasTableApi
} from '../../interface/CharacterHennasTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterHennasTable : L2CharacterHennasTableApi = {
    async deleteByClassIndex( objectId: number, classIndex: number ): Promise<void> {
        const query = 'DELETE FROM character_hennas WHERE objectId=? AND classIndex=?'
        return databaseEngine.insertOne( query, objectId, classIndex )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM character_hennas WHERE objectId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async delete( items: Array<L2CharacterHennaItem> ): Promise<void> {
        const query = 'DELETE FROM character_hennas WHERE objectId=? AND slot=? AND classIndex=?'
        const parameters = items.map( item => {
            return [
                item.objectId,
                item.symbolId,
                item.slot,
                item.classIndex
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async add( items: Array<L2CharacterHennaItem> ) {
        const query = 'INSERT OR REPLACE INTO character_hennas (objectId,symbolId,slot,classIndex) VALUES (?,?,?,?)'
        const parameters = items.map( item => {
            return [
                item.objectId,
                item.symbolId,
                item.slot,
                item.classIndex
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async getAll( objectId: number, classIndex: number ): Promise<L2CharacterHennas> {
        const query = 'SELECT slot,symbolId FROM character_hennas WHERE objectId=? AND classIndex=? ORDER BY slot ASC'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, objectId, classIndex )

        return databaseItems.map( ( item: Object ) : L2CharacterHennaItem => {
            return {
                classIndex,
                objectId,
                slot: item[ 'slot' ],
                symbolId: item[ 'symbolId' ]
            }
        } )
    }
}