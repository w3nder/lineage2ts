import { L2AirshipsTableApi } from '../../interface/AirshipsTableApi'
import _ from 'lodash'
import { getMany, insertOne } from '../../engines/sqlite'

export const SQLiteAirshipsTable : L2AirshipsTableApi = {
    async addShip( ownerId: number, fuel: number ): Promise<void> {
        const query = 'INSERT INTO airships (owner_id,fuel) VALUES (?,?)'
        return insertOne( query, ownerId, fuel )
    },

    async getAll(): Promise<{ [ key: number]: { [ key: string ] : any } }> {
        const query = 'SELECT * FROM airships'
        let databaseItems = getMany( query )

        return _.reduce( databaseItems, ( finalMap: { [ key: number]: { [ key: string ] : any } }, databaseItem: any ) => {
            finalMap[ databaseItem[ 'owner_id' ] ] = databaseItem
            return finalMap
        }, {} )
    },

    async updateShip( ownerId: number, fuel: number ): Promise<void> {
        const query = 'UPDATE airships SET fuel=? WHERE owner_id=?'
        return insertOne( query, fuel, ownerId )
    }
}