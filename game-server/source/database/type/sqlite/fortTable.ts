import { FortTableApi, L2FortTableData } from '../../interface/FortTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteFortTable : FortTableApi = {
    async getAll(): Promise<Array<L2FortTableData>> {
        const query = 'SELECT * FROM fort'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )
        return databaseItems.map( ( databaseItem: any ) : L2FortTableData => {
            return {
                id: databaseItem[ 'id' ],
                name: databaseItem[ 'name' ],
                siegeDate: databaseItem[ 'siegeDate' ],
                lastOwnedTime: databaseItem[ 'lastOwnedTime' ],
                ownerId: databaseItem[ 'owner' ],
                fortType: databaseItem[ 'fortType' ],
                state: databaseItem[ 'state' ],
                castleId: databaseItem[ 'castleId' ],
                supplyLevel: databaseItem[ 'supplyLvL' ]
            }
        } )
    },

    async updateAll( items: Array<L2FortTableData> ): Promise<void> {
        const query = 'UPDATE fort SET supplyLvL=?,state=?,castleId=?,siegeDate=?,owner=? WHERE id = ?'
        const dataItems = items.map( ( data: L2FortTableData ) => {
            return [
                data.supplyLevel,
                data.state,
                data.castleId,
                data.siegeDate,
                data.ownerId,
                data.id
            ]
        } )

        return databaseEngine.insertMany( query, dataItems )
    }
}