import { L2ItemsOnGroundTableApi, L2ItemsOnGroundTableData } from '../../interface/ItemsOnGroundTableApi'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteItemsOnGroundTable : L2ItemsOnGroundTableApi = {
    async getHighestId(): Promise<number> {
        const query = 'SELECT MAX(objectId) as object_id FROM items_on_ground'
        let databaseItem : unknown = databaseEngine.getOne( query )

        if ( !databaseItem ) {
            return 0
        }

        return _.defaultTo( databaseItem[ 'objectId' ], 0 )
    },

    async getUsedIds( ids: Array<number> ): Promise<Array<number>> {
        const query = 'SELECT objectId FROM items_on_ground WHERE objectId in (#values#)'
        let databaseItems : Array<unknown> = databaseEngine.getManyForValues( query, ids )

        return _.map( databaseItems, 'objectId' )
    },

    async addAll( data : Array<L2ItemsOnGroundTableData> ) {
        const query = 'INSERT INTO items_on_ground(objectId,itemId,count,enchantLevel,x,y,z,dropTime,type) VALUES(?,?,?,?,?,?,?,?,?)'
        let parameters = _.map( data, ( item: L2ItemsOnGroundTableData ) => {
            return [
                    item.objectId,
                    item.itemId,
                    item.count,
                    item.enchantLevel,
                    item.x,
                    item.y,
                    item.z,
                    item.dropTime,
                    item.type
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async getAll(): Promise<Array<L2ItemsOnGroundTableData>> {
        const query = 'SELECT * FROM items_on_ground'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ) : L2ItemsOnGroundTableData => {
            return {
                count: databaseItem[ 'count' ],
                dropTime: databaseItem[ 'dropTime' ],
                enchantLevel: databaseItem[ 'enchantLevel' ],
                itemId: databaseItem[ 'itemId' ],
                objectId: databaseItem[ 'objectId' ],
                type: databaseItem[ 'type' ],
                x: databaseItem[ 'x' ],
                y: databaseItem[ 'y' ],
                z: databaseItem[ 'z' ]
            }
        } )
    },

    async removeAll(): Promise<void> {
        const query = 'DELETE FROM items_on_ground'
        return databaseEngine.insertOne( query )
    },

    async removeByDropTime( time: number ): Promise<void> {
        const query = 'DELETE FROM items_on_ground WHERE dropTime > 0 AND dropTime < ?'
        return databaseEngine.insertOne( query, time )
    }
}