import { CharacterUICategoriesTableApi } from '../../interface/CharacterUICategoriesTableApi'
import { UICategories } from '../../../data/interface/UIDataApi'

import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterUICategoriesTable : CharacterUICategoriesTableApi = {
    async getCategories( objectId: number ): Promise<UICategories> {
        let query = 'SELECT * FROM character_ui_categories WHERE `objectId` = ? ORDER BY `categoryId`, `orderIndex`'
        let results : Array<unknown> = databaseEngine.getMany( query, objectId )

        let values : UICategories = {}

        results.forEach( ( databaseItem: any ) => {
            let categoryId : string = databaseItem[ 'categoryId' ]
            let commandId : number = databaseItem[ 'commandId' ]

            if ( !_.has( values, categoryId ) ) {
                values[ categoryId ] = []
            }

            values[ categoryId ].push( commandId )
        } )

        return values
    },

    async setCategories( objectId: number, data : UICategories ): Promise<void> {
        let query = 'INSERT OR REPLACE INTO character_ui_categories (`objectId`, `categoryId`, `orderIndex`, `commandId`) VALUES (?,?,?,?)'
        let itemsToSave = []
        let index = 0

        _.each( data, ( keys: Array<number>, categoryId: string ) => {
            _.each( keys, ( currentKey: number ) => {
                let item = [
                    objectId,
                    categoryId,
                    index++,
                    currentKey
                ]

                itemsToSave.push( item )
            } )
        } )

        if ( itemsToSave.length > 0 ) {
            return databaseEngine.insertMany( query, itemsToSave )
        }
    }
}