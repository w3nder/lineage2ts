import { CharacterRecipeShoplistTableApi, L2PlayerRecipeShopItem } from '../../interface/CharacterRecipeShoplistTableApi'
import { L2PcInstance } from '../../../gameService/models/actor/instance/L2PcInstance'
import { L2ManufactureItem } from '../../../gameService/models/L2ManufactureItem'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterRecipeShoplist : CharacterRecipeShoplistTableApi = {
    async getItem( objectId: number ): Promise<Array<L2PlayerRecipeShopItem>> {
        let query = 'SELECT * FROM character_recipeshoplist WHERE charId=?'
        let results : Array<unknown> = databaseEngine.getMany( query, objectId )

        return results.map( ( item : any ) : L2PlayerRecipeShopItem => {
            return {
                price: item.price,
                recipeId: item.recipeId
            }
        } )
    },

    async createPlayer( player: L2PcInstance ): Promise<void> {
        let query = 'INSERT OR REPLACE INTO character_recipeshoplist (`charId`, `recipeId`, `price`, `index`) VALUES (?, ?, ?, ?)'
        let index = 1
        let itemsToSave = player.getManufactureItems().map( ( recipeItem: L2ManufactureItem ) => {
            return [
                player.getObjectId(),
                recipeItem.recipeId,
                recipeItem.adenaCost,
                index++
            ]
        } )

        if ( itemsToSave.length > 0 ) {
            return databaseEngine.insertMany( query, itemsToSave )
        }
    },

    async deletePlayer( player: L2PcInstance ): Promise<void> {
        let query = 'DELETE FROM character_recipeshoplist WHERE charId=?'
        return databaseEngine.insertOne( query, player.getObjectId() )
    }
}