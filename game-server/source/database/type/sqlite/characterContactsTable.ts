import { L2CharacterContactsTableApi } from '../../interface/CharacterContactsTableApi'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteCharacterContactsTable: L2CharacterContactsTableApi = {
    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM character_contacts WHERE charId = ? or contactId = ?'
        return databaseEngine.insertOne( query, objectId, objectId )
    },

    async getHighestId(): Promise<number> {
        const query = 'SELECT MAX(charId) as charId, MAX(contactId) as contactId FROM character_contacts'
        let databaseItem : any = databaseEngine.getOne( query )

        if ( !databaseItem ) {
            return 0
        }

        return Math.max(
                _.defaultTo( databaseItem[ 'charId' ], 0 ),
                _.defaultTo( databaseItem[ 'contactId' ], 0 )
        )
    },

    async getUsedIds( ids: Array<number> ): Promise<Array<number>> {
        const query = 'SELECT contactId FROM character_contacts WHERE contactId in (#values#)'
        let databaseItems : Array<any> = databaseEngine.getManyForValues( query, ids )

        return _.map( databaseItems, 'contactId' )
    },

    async addContact( objectId: number, contactId: number ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO character_contacts (charId, contactId) VALUES (?, ?)'
        return databaseEngine.insertOne( query, objectId, contactId )
    },

    async getContacts( objectId: number ): Promise<Array<number>> {
        const query = 'SELECT contactId FROM character_contacts WHERE charId = ?'
        let databaseItems = databaseEngine.getMany( query, objectId )

        return _.map( databaseItems, ( databaseItem: any ) => {
            return databaseItem[ 'contactId' ]
        } )
    },

    async removeContact( objectId: number, contactId: number ): Promise<void> {
        const query = 'DELETE FROM character_contacts WHERE charId = ? and contactId = ?'
        return databaseEngine.insertOne( query, objectId, contactId )
    }
}