import { L2GrandbossPlayerData, L2GrandbossPlayersTableApi } from '../../interface/GrandbossPlayersTableApi'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteGrandbossPlayersTable: L2GrandbossPlayersTableApi = {
    async addPlayers( items: Array<L2GrandbossPlayerData> ): Promise<void> {
        const query = 'INSERT INTO grandboss_list (player_id,zone) VALUES (?,?)'
        const data = _.map( items, ( currentItem: L2GrandbossPlayerData ) => {
            return [
                currentItem.objectId,
                currentItem.zoneId,
            ]
        } )

        return databaseEngine.insertMany( query, data )
    },

    async getAll(): Promise<Array<L2GrandbossPlayerData>> {
        const query = 'SELECT * from grandboss_list'
        const databaseItems : Array<unknown> = databaseEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ): L2GrandbossPlayerData => {
            return {
                objectId: databaseItem[ 'player_id' ],
                zoneId: databaseItem[ 'zone' ],
            }
        } )
    },

    async removeAll(): Promise<void> {
        const query = 'DELETE FROM grandboss_list'
        return databaseEngine.insertOne( query )
    },
}