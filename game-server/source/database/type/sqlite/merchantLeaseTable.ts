import { L2MerchantLeaseTableApi } from '../../interface/MerchantLeaseTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteMerchantLeaseTable: L2MerchantLeaseTableApi = {
    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM merchant_lease WHERE player_id=?'
        return databaseEngine.insertOne( query, objectId )
    }
}