import { PunishmentsTableApi } from '../../interface/PunishmentsTableApi'
import { PunishmentRecord } from '../../../gameService/models/punishment/PunishmentRecord'
import { PunishmentEffect } from '../../../gameService/models/punishment/PunishmentEffect'
import { PunishmentType } from '../../../gameService/models/punishment/PunishmentType'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'

export const SQLitePunishmentsTable : PunishmentsTableApi = {
    async updateRecord( record: PunishmentRecord ): Promise<void> {
        const query = 'UPDATE punishments SET expiration = ? WHERE id = ?'
        return databaseEngine.insertOne( query, record.expirationTime, record.id )
    },

    async getAll(): Promise<Array<PunishmentRecord>> {
        let query = 'SELECT * FROM punishments'
        let results : Array<unknown> = databaseEngine.getMany( query )

        return results.map( ( databaseItem: any ) => {
            let record = new PunishmentRecord(
                    databaseItem[ 'key' ],
                    PunishmentEffect[ databaseItem[ 'affect' ] as string ],
                    PunishmentType[ databaseItem[ 'type' ] as string ],
                    databaseItem[ 'expiration' ],
                    databaseItem[ 'reason' ],
                    databaseItem[ 'punishedBy' ],
                    true
            )

            record.id = databaseItem[ 'id' ]
            return record
        } )
    },

    async addRecord( record: PunishmentRecord ): Promise<void> {
        let query = 'INSERT INTO punishments (`id`, `key`, `affect`, `type`, `expiration`, `reason`) VALUES (?, ?, ?, ?, ?, ?)'
        return databaseEngine.insertOne( query, record.id, record.targetId, record.effect, record.type, record.expirationTime, record.reason )
    },

    async removeRecords( records: Array<PunishmentRecord> ) : Promise<void> {
        let query = 'DELETE FROM punishments WHERE id in (#values#)'
        return databaseEngine.deleteManyForValues( query, records.map( record => record.id ) )
    }
}