import { ItemElementalsTableApi, L2ItemElementalTableData } from '../../interface/ItemElementalsTableApi'
import { L2ItemInstance } from '../../../gameService/models/items/instance/L2ItemInstance'
import { Elementals } from '../../../gameService/models/Elementals'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteItemElementalsTable : ItemElementalsTableApi = {
    async deleteByElementType( objectId: number, element: number ): Promise<void> {
        const query = 'DELETE FROM item_elementals WHERE objectId = ? AND type = ?'
        return databaseEngine.insertOne( query, objectId, element )
    },

    async updateItems( items: Array<L2ItemInstance> ): Promise<void> {
        let objectIds = items.map( ( currentItem: L2ItemInstance ) => currentItem.getObjectId() )

        await SQLiteItemElementalsTable.deleteByObjectIds( objectIds )

        let query = 'INSERT INTO item_elementals VALUES(?,?,?)'
        let dataSet = []

        items.forEach( ( item: L2ItemInstance ) => {
            let objectId = item.getObjectId()

            item.elementals.forEach( ( value: Elementals ) => {
                dataSet.push( [ objectId, value.getElement(), value.getValue() ] )
            } )
        } )

        if ( dataSet.length > 0 ) {
            return databaseEngine.insertMany( query, dataSet )
        }

        return
    },

    async deleteByObjectIds( ids: Array<number> ): Promise<void> {
        let query = 'DELETE FROM item_elementals WHERE objectId in (#values#)'
        return databaseEngine.deleteManyForValues( query, ids )
    },

    async getAttributes( objectId: number ): Promise<Array<L2ItemElementalTableData>> {
        let query = 'SELECT type,value FROM item_elementals WHERE objectId=?'
        let items : Array<unknown> = databaseEngine.getMany( query, objectId )

        return items.map( ( item : Object ) : L2ItemElementalTableData => {
            return {
                objectId,
                type: item[ 'type' ],
                value: item[ 'value' ]
            }
        } )
    },

    async insertItemElementals( item: L2ItemInstance ): Promise<void> {
        let query = 'INSERT INTO item_elementals (objectId, type, value) VALUES(?,?,?)'
        let objectId = item.getObjectId()
        let parameters = item.elementals.map( ( value: Elementals ) => {
            return [ objectId, value.getElement(), value.getValue() ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async deleteByObjectId( id: number ): Promise<void> {
        let query = 'DELETE FROM item_elementals WHERE objectId = ?'
        return databaseEngine.insertOne( query, id )
    }
}