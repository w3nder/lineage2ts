import { CharacterSummonSkillsItem, CharacterSummonSkillsSaveTableApi } from '../../interface/CharacterSummonSkillsSaveTableApi'
import * as databaseEngine from '../../engines/sqlite'
import { BuffProperties } from '../../../gameService/models/skills/BuffDefinition'

const deleteQuery = 'DELETE FROM character_summon_skills_save WHERE ownerId=? AND ownerClassIndex=? AND summonSkillId=?'

export const SQLiteCharacterSummonSkillSaveTable : CharacterSummonSkillsSaveTableApi = {
    async delete( ownerId: number, classIndex: number, summonSkillId: number ): Promise<void> {
        return databaseEngine.insertOne( deleteQuery, ownerId, classIndex, summonSkillId )
    },

    async load( ownerId: number, classIndex: number, summonSkillId: number ): Promise<Array<CharacterSummonSkillsItem>> {

        let query = 'SELECT skill_id,skill_level,remaining_time,buff_index FROM character_summon_skills_save WHERE ownerId=? AND ownerClassIndex=? AND summonSkillId=? ORDER BY buff_index'
        let results : Array<unknown> = databaseEngine.getMany( query, ownerId, classIndex, summonSkillId )

        return results.map( ( databaseItem: any ) : CharacterSummonSkillsItem => {
            return {
                remainingTime: databaseItem[ 'remaining_time' ],
                skillId: databaseItem[ 'skill_id' ],
                skillLevel: databaseItem[ 'skill_level' ]
            }
        } )
    },

    async update( ownerId: number, classIndex: number, summonSkillId: number, effects: Array<BuffProperties> ): Promise<void> {
        let query = 'INSERT INTO character_summon_skills_save (ownerId,ownerClassIndex,summonSkillId,skill_id,skill_level,remaining_time,buff_index) VALUES (?,?,?,?,?,?,?)'
        let itemsToSave = effects.map( ( effect: BuffProperties, index: number ) => {
            return [
                ownerId,
                classIndex,
                summonSkillId,
                effect.skill.getId(),
                effect.skill.getLevel(),
                effect.durationMs,
                ++index
            ]
        } )

        if ( itemsToSave.length > 0 ) {
            databaseEngine.insertMany( query, itemsToSave )
        }
    }
}