import { L2ClanHallAuctionBidTableApi, L2lanHallAuctionBidTableData } from '../../interface/ClanHallAuctionBidTableApi'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteClanHallAuctionBidTable : L2ClanHallAuctionBidTableApi = {
    async addBid( data: L2lanHallAuctionBidTableData ): Promise<void> {
        const query = 'INSERT INTO clanhall_auction_bid (id, auctionId, bidderId, bidderName, maxBid, clan_name, time_bid) VALUES (?, ?, ?, ?, ?, ?, ?)'
        return databaseEngine.insertOne( query,
                data.bidderId,
                data.auctionId,
                data.bidderId,
                data.name,
                data.maxBidAmount,
                data.clanName,
                data.bidTime )
    },

    async updateBid( data: L2lanHallAuctionBidTableData ): Promise<void> {
        const query = 'UPDATE clanhall_auction_bid SET bidderName=?, maxBid=?, time_bid=? WHERE auctionId=? AND bidderId=?'
        return databaseEngine.insertOne( query,
                data.name,
                data.maxBidAmount,
                data.bidTime,
                data.auctionId,
                data.bidderId )
    },


    async removeByAuctionId( auctionId: number ): Promise<void> {
        const query = 'DELETE FROM clanhall_auction_bid WHERE auctionId=?'
        return databaseEngine.insertOne( query, auctionId )
    },

    async getBidsByAuctionId( id: number ): Promise<Array<L2lanHallAuctionBidTableData>> {
        const query = 'SELECT bidderId, bidderName, maxBid, clan_name, time_bid FROM clanhall_auction_bid WHERE auctionId = ?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, id )

        return databaseItems.map( ( databaseItem: any ) : L2lanHallAuctionBidTableData => {
            return {
                auctionId: id,
                bidTime: databaseItem[ 'time_bid' ],
                bidderId: databaseItem[ 'bidderId' ],
                clanName: databaseItem[ 'clan_name' ],
                maxBidAmount: databaseItem[ 'maxBid' ],
                name: databaseItem[ 'bidderName' ]
            }
        } )
    },

    async removeAuction( id: number, bidderId: number ): Promise<void> {
        const query = 'DELETE FROM clanhall_auction_bid WHERE auctionId=? AND bidderId=?'
        return databaseEngine.insertOne( query, id, bidderId )
    }
}