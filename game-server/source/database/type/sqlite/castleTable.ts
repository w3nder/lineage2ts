import { CastleTableApi, L2CastleTableData } from '../../interface/CastleTableApi'
import * as databaseEngine from '../../engines/sqlite'
import _ from 'lodash'
import { Castle } from '../../../gameService/models/entity/Castle'

export const SQLiteCastleTable : CastleTableApi = {
    async resetClanTax( clanId: number ): Promise<void> {
        const query = 'UPDATE castle SET taxPercent = 0 WHERE id = ?'
        return databaseEngine.insertOne( query, clanId )
    },

    async getAll(): Promise<Array<L2CastleTableData>> {
        const query = 'select * from castle order by id'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )
        return databaseItems.map( ( databaseItem: any ) : L2CastleTableData => {
            return {
                id: databaseItem[ 'id' ],
                name: databaseItem[ 'name' ],
                taxPercent: databaseItem[ 'taxPercent' ],
                treasury: databaseItem[ 'treasury' ],
                siegeDate: databaseItem[ 'siegeDate' ],
                isRegistrationOver: databaseItem[ 'regTimeOver' ] === 'true',
                registrationTimeEnd: databaseItem[ 'regTimeEnd' ],
                showNpcCrest: databaseItem[ 'showNpcCrest' ] === 'true',
                ticketBuyCount: databaseItem[ 'ticketBuyCount' ]
            }
        } )
    },

    async updateTicketBuyCount( castles: Array<Castle> ): Promise<void> {
        const query = 'UPDATE castle SET ticketBuyCount = ? WHERE id = ?'
        const values = castles.map( ( castle: Castle ) : [ number, number ] => {
            return [ castle.getTicketBuyCount(), castle.getResidenceId() ]
        } )

        return databaseEngine.insertMany( query, values )
    },

    async updateTaxValue( castles: Array<Castle> ): Promise<void> {
        const query = 'UPDATE castle SET taxPercent = ? WHERE id = ?'
        const values = castles.map( ( castle: Castle ) : [ number, number ] => {
            return [ castle.getTaxPercent(), castle.getResidenceId() ]
        } )

        return databaseEngine.insertMany( query, values )
    },

    async updateSiegeInfo( residenceId: number, siegeDate: number, timeRegistrationOverDate: number, isRegistrationOver: boolean ): Promise<void> {
        const query = 'UPDATE castle SET siegeDate = ?, regTimeEnd = ?, regTimeOver = ?  WHERE id = ?'
        return databaseEngine.insertOne( query, siegeDate, timeRegistrationOverDate, _.toString( isRegistrationOver ), residenceId )
    },

    async updateCrestStatus( castles: Array<Castle> ): Promise<void> {
        const query = 'UPDATE castle SET showNpcCrest = ? WHERE id = ?'
        const values = castles.map( ( castle: Castle ) : [ string, number ] => {
            return [ castle.getShowNpcCrest() ? 'true' : 'false', castle.getResidenceId() ]
        } )

        return databaseEngine.insertMany( query, values )
    },

    async updateTreasury( castles: Array<Castle> ) : Promise<void> {
        const query = 'UPDATE castle SET treasury = ? WHERE id = ?'
        const values = castles.map( ( castle: Castle ) : [ number, number ] => {
            return [ castle.getTreasury(), castle.getResidenceId() ]
        } )

        return databaseEngine.insertMany( query, values )
    }
}