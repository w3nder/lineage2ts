import { ClanhallFunctionsTableApi } from '../../interface/ClanhallFunctionsTableApi'
import _ from 'lodash'
import * as databaseEngine from '../../engines/sqlite'
import { ClanHallFunction } from '../../../gameService/models/clan/ClanHallFunction'

export const SQLiteClanhallFunctionsTable: ClanhallFunctionsTableApi = {
    async getById( id: number ): Promise<Array<ClanHallFunction>> {
        let query = 'SELECT * FROM clanhall_functions WHERE hallId=?'
        let results = databaseEngine.getMany( query, id )

        return _.map( results, ( databaseItem: any ): ClanHallFunction => {
            return {
                endDate: databaseItem.endTime,
                fee: databaseItem.chargeFee,
                hallId: databaseItem.hallId,
                level: databaseItem.level,
                chargeTimeInterval: databaseItem.chargeInterval,
                type: databaseItem.type,
            }
        } )
    },

    async removeFunction( data: ClanHallFunction ): Promise<void> {
        const query = 'DELETE FROM clanhall_functions WHERE hallId=? AND type=?'
        return databaseEngine.insertOne( query, data.hallId, data.type )
    },

    async updateFunctions( items: Array<ClanHallFunction> ): Promise<void> {
        const query = 'REPLACE INTO clanhall_functions (hallId, type, level, chargeFee, chargeInterval, endTime) VALUES (?,?,?,?,?,?)'
        let parameters = items.map( ( data : ClanHallFunction ) : Array<number> => {
            return [
                data.hallId,
                data.type,
                data.level,
                data.fee,
                data.chargeTimeInterval,
                data.endDate
            ]
        } )
        return databaseEngine.insertMany( query, parameters )
    },

    async removeAll( hallId: number, types: Array<number> ): Promise<void> {
        let query = 'DELETE FROM clanhall_functions WHERE hallId=? AND type=?'

        let items = _.map( types, ( type: number ) => {
            return [ hallId, type ]
        } )

        return databaseEngine.insertMany( query, items )
    }
}