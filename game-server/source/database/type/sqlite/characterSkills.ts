import { CharacterSkillsTableApi } from '../../interface/CharacterSkillsTableApi'
import { L2PcInstance } from '../../../gameService/models/actor/instance/L2PcInstance'
import { Skill } from '../../../gameService/models/Skill'
import { SkillCache } from '../../../gameService/cache/SkillCache'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteCharacterSkillsTable: CharacterSkillsTableApi = {
    async deleteByClassIndex( objectId: number, classIndex: number ): Promise<void> {
        const query = 'DELETE FROM character_skills WHERE charId=? AND class_index=?'
        return databaseEngine.insertOne( query, objectId, classIndex )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM character_skills WHERE charId=?'
        return databaseEngine.insertOne( query, objectId )
    },

    async deleteSkills( player: L2PcInstance, skills: Array<Skill> ): Promise<void> {
        const query = 'DELETE FROM character_skills WHERE skill_id=? AND charId=? AND class_index=?'

        let objectId = player.getObjectId()
        let classIndex = player.getClassIndex()

        let parameters = skills.map( ( skill: Skill ) => {
            return [
                skill.getId(),
                objectId,
                classIndex,
            ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async getSkills( player: L2PcInstance ): Promise<Array<Skill>> {
        const query = 'SELECT skill_id,skill_level FROM character_skills WHERE charId=? AND class_index=?'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query, player.getObjectId(), player.getClassIndex() )

        return databaseItems.map( ( databaseItem: any ) => {
            let id = databaseItem[ 'skill_id' ]
            let level = databaseItem[ 'skill_level' ]

            return SkillCache.getSkill( id, level )
        } )
    },

    async upsertSkills( objectId: number, classIndex: number, skills: Array<Skill> ): Promise<void> {
        const query = 'INSERT OR REPLACE INTO character_skills (charId,skill_id,skill_level,class_index) VALUES (?,?,?,?)'
        let parameters = skills.map( ( currentSkill: Skill ) => {
            return [ objectId, currentSkill.getId(), currentSkill.getLevel(), classIndex ]
        } )

        return databaseEngine.insertMany( query, parameters )
    },

    async deleteSkill( player: L2PcInstance, skill: Skill ): Promise<void> {
        const query = 'DELETE FROM character_skills WHERE skill_id=? AND charId=? AND class_index=?'
        return databaseEngine.insertOne( query, skill.getId(), player.getObjectId(), player.getClassIndex() )
    },
}