import { L2GrandbossDataItem, L2GrandbossDataTableApi } from '../../interface/GrandbossDataTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteGrandbossDataTable: L2GrandbossDataTableApi = {
    async getAll(): Promise<Array<L2GrandbossDataItem>> {
        const query = 'SELECT * from grandboss_data'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ): L2GrandbossDataItem => {
            return {
                bossId: databaseItem[ 'boss_id' ],
                currentHp: databaseItem[ 'currentHP' ],
                currentMp: databaseItem[ 'currentMP' ],
                heading: databaseItem[ 'heading' ],
                respawnTime: databaseItem[ 'respawn_time' ],
                status: databaseItem[ 'status' ],
                x: databaseItem[ 'loc_x' ],
                y: databaseItem[ 'loc_y' ],
                z: databaseItem[ 'loc_z' ],
            }
        } )
    },

    async setStatus( bossId: number, status: number ): Promise<void> {
        const query = 'UPDATE grandboss_data set status = ? where boss_id = ?'
        return databaseEngine.insertOne( query, status, bossId )
    },

    async update( item: L2GrandbossDataItem ): Promise<void> {
        const query = 'UPDATE grandboss_data set loc_x = ?, loc_y = ?, loc_z = ?, heading = ?, respawn_time = ?, currentHP = ?, currentMP = ?, status = ? where boss_id = ?'
        return databaseEngine.insertOne( query,
                item.x,
                item.y,
                item.z,
                item.heading,
                item.respawnTime,
                item.currentHp,
                item.currentMp,
                item.status,
                item.bossId )
    },
}