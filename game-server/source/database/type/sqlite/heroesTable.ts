import { L2HeroesTableApi, L2HeroesTableItem } from '../../interface/HeroesTableApi'
import * as databaseEngine from '../../engines/sqlite'

export const SQLiteHeroesTable : L2HeroesTableApi = {
    async getAll(): Promise<Array<L2HeroesTableItem>> {
        const query = 'select * from heroes'
        let databaseItems : Array<unknown> = databaseEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ) : L2HeroesTableItem => {
            return {
                classId: databaseItem[ 'class_id' ],
                count: databaseItem[ 'count' ],
                isClaimed: databaseItem[ 'claimed' ] === 'true',
                message: databaseItem[ 'message' ],
                objectId: databaseItem[ 'charId' ],
                played: databaseItem[ 'played' ]
            }
        } )
    },

    async addCharacter( objectId: number, classId: number, count: number, played: number, isClaimed: boolean ): Promise<void> {
        const query = 'INSERT INTO heroes (charId, class_id, count, played, claimed) VALUES (?,?,?,?,?)'
        return databaseEngine.insertOne( query, objectId, classId, count, played, isClaimed ? 'true' : 'false' )
    },

    async resetAllPlayed(): Promise<void> {
        const query = 'UPDATE heroes SET played = 0'
        return databaseEngine.insertOne( query )
    },

    async removeCharacter( objectId: number ): Promise<void> {
        const query = 'DELETE FROM heroes WHERE charId=?'
        return databaseEngine.insertOne( query, objectId )
    }
}