import { IDatabaseType } from './IDatabaseType'
import { SQLiteCharacterSubclassesTable } from './sqlite/characterSubclasses'
import { SQLiteAnnouncementsTable } from './sqlite/announcementsTable'
import { SQLiteCharacterFriendsTable } from './sqlite/characterFriendsTable'
import { SQLiteQuestStatesTable } from './sqlite/questStatesTable'
import { SQLiteCharacterRecommendationBonusTable } from './sqlite/characterRecommendationBonus'
import { SQLiteCharacterShortcutsTable } from './sqlite/characterShortcutsTable'
import { SQLiteCharacterSkillsTable } from './sqlite/characterSkills'
import { SQLiteCharactersTable } from './sqlite/characters'
import { SQLiteItemElementalsTable } from './sqlite/itemElementalsTable'
import { SQLiteItemsTable } from './sqlite/itemsTable'
import { SQLiteMessagesTable } from './sqlite/messagesTable'
import { SQLiteCharacterSkillsSaveTable } from './sqlite/characterSkillsSaveTable'
import { SQLiteCharacterItemReuseSaveTable } from './sqlite/characterItemReuseSaveTable'
import { SQLiteCharacterRecipeShoplist } from './sqlite/characterRecipeShoplistTable'
import { SQLiteCharacterUICategoriesTable } from './sqlite/characterUICategoriesTable'
import { SQLiteCharacterUIActionsTable } from './sqlite/characterUIActionsTable'
import { SQLiteSevenSignsTable } from './sqlite/sevenSignsTable'
import { SQLiteCharacterSummonsTable } from './sqlite/servitorTable'
import { SQLiteCharacterPetSkillsSaveTable } from './sqlite/characterPetSkillsSaveTable'
import { SQLiteCharacterSummonSkillSaveTable } from './sqlite/characterSummonSkillSaveTable'
import { SQLiteCharacterInstanceTimeTable } from './sqlite/characterInstanceTimeTable'
import { SQLiteCharacterMacrosTable } from './sqlite/characterMacrosTable'
import { SQLiteCursedWeaponsTable } from './sqlite/cursedWeaponsTable'
import { SQLitePetsTable } from './sqlite/petsTable'
import { SQLiteCastleTable } from './sqlite/castleTable'
import { SQLiteCastleManorProcureTable } from './sqlite/castleManorProcureTable'
import { SQLiteCastleManorProductionTable } from './sqlite/castleManorProductionTable'
import { SQLiteCastleSiegeGuards } from './sqlite/castleSiegeGuardsTable'
import { SQLiteFortTable } from './sqlite/fortTable'
import { SQLiteFortFunctionsTable } from './sqlite/fortFunctionsTable'
import { SQLitePunishmentsTable } from './sqlite/punishmentsTable'
import { SQLiteClanhallTable } from './sqlite/clanhallTable'
import { SQLiteClanhallFunctionsTable } from './sqlite/clanhallFunctionsTable'
import { SQLiteClanSubpledgesTable } from './sqlite/clanSubpledges'
import { SQLiteClanDataTable } from './sqlite/clanDataTable'
import { SQLiteClanPrivileges } from './sqlite/clanPrivilegesTable'
import { SQLiteClanSkillsTable } from './sqlite/clanSkillsTable'
import { SQLiteClanNoticesTable } from './sqlite/clanNoticesTable'
import { SQLiteCharacterHennasTable } from './sqlite/characterHennasTable'
import { SQLiteCharacterTeleportBookmarks } from './sqlite/characterTeleportBookmarks'
import { SQLiteCharacterRecipebook } from './sqlite/characterRecipebook'
import { SQLiteDimensionalTransferItems } from './sqlite/dimensionalTransferItems'
import { SQLiteCharacterVariables } from './sqlite/characterVariables'
import { SQLiteAccountVariables } from './sqlite/accountVariables'
import { SQLiteClanWarsTable } from './sqlite/clanWarsTable'
import { SQLiteCrestsTable } from './sqlite/crestsTable'
import { SQLiteBuylistsTable } from './sqlite/buylistsTable'
import { SQLiteAirshipsTable } from './sqlite/airshipsTable'
import { SQLiteSiegeClansTable } from './sqlite/siegeClansTable'
import { SQLitePetitionFeedbackTable } from './sqlite/petitionFeedbackTable'
import { SQLiteItemAuctionBidTable } from './sqlite/itemAuctionBidTable'
import { SQLiteCharacterRaidPointsTable } from './sqlite/characterRaidPointsTable'
import { SQLiteTerritoryRegistrationsTable } from './sqlite/territoryRegistrationsTable'
import { SQLiteCharacterContactsTable } from './sqlite/characterContactsTable'
import { SQLiteMerchantLeaseTable } from './sqlite/merchantLeaseTable'
import { SQLiteOlympiadNoblesTable } from './sqlite/olympiadNoblesTable'
import { SQLiteHeroesTable } from './sqlite/heroesTable'
import { SQLiteHeroDiaryTable } from './sqlite/heroDiaryTable'
import { SQLiteGrandbossDataTable } from './sqlite/grandbossDataTable'
import { SQLiteGrandbossPlayersTable } from './sqlite/grandbossPlayersTable'
import { SQLiteFortSiegeClansTable } from './sqlite/fortSiegeClansTable'
import { SQLiteCastleFunctionsTable } from './sqlite/castleFunctionsTable'
import { SQLiteCastleDoorUpgradeTable } from './sqlite/castleDoorUpgradeTable'
import { SQLiteCastleTrapUpgradeTable } from './sqlite/castleTrapUpgradeTable'
import { SQLiteClanHallAuctionBidTable } from './sqlite/clanHallAuctionBidTable'
import { SQLiteFortDoorUpgradeTable } from './sqlite/fortDoorUpgradeTable'
import { SQLiteItemAuctionTable } from './sqlite/itemAuctionTable'
import { SQLiteBotReportsTable } from './sqlite/botReportsTable'
import { SQLiteClanHallAuctionsTable } from './sqlite/clanHallAuctionsTable'
import { SQLiteLotteryTable } from './sqlite/lotteryTable'
import { SQLiteOlympiadDataTable } from './sqlite/olympiadData'
import { SQLiteFishingChampionshipTable } from './sqlite/fishingChampionshipTable'
import { SQLiteServerVariablesTable } from './sqlite/serverVariablesTable'
import { SQLiteItemsOnGroundTable } from './sqlite/itemsOnGroundTable'
import { SQLiteNevitDataTable } from './sqlite/nevitDataTable'
import { SQLiteSiegableHallTable } from './sqlite/siegableHallTable'
import { SQLiteNpcRespawnTable } from './sqlite/npcRespawnTable'
import { SQLiteDatabaseOperations } from './sqlite/Operations'

export const SQLiteDatabase: IDatabaseType = {
    characterSubclassesTable: SQLiteCharacterSubclassesTable,
    announcementsTable: SQLiteAnnouncementsTable,
    characterFriendsTable: SQLiteCharacterFriendsTable,
    questStatesTable: SQLiteQuestStatesTable,
    characterRecommendationBonusTable: SQLiteCharacterRecommendationBonusTable,
    characterShortcutsTable: SQLiteCharacterShortcutsTable,
    characterSkillsTable: SQLiteCharacterSkillsTable,
    charactersTable: SQLiteCharactersTable,
    itemElementalsTable: SQLiteItemElementalsTable,
    itemsTable: SQLiteItemsTable,
    messagesTable: SQLiteMessagesTable,
    petsTable: SQLitePetsTable,
    characterSkillsSaveTable: SQLiteCharacterSkillsSaveTable,
    characterItemReuseSaveTable: SQLiteCharacterItemReuseSaveTable,
    characterRecipeShoplistTable: SQLiteCharacterRecipeShoplist,
    characterUIActionsTable: SQLiteCharacterUIActionsTable,
    characterUICategoriesTable: SQLiteCharacterUICategoriesTable,
    sevenSignsTable: SQLiteSevenSignsTable,
    servitorsTable: SQLiteCharacterSummonsTable,
    characterPetSkillsSave: SQLiteCharacterPetSkillsSaveTable,
    characterSummonSkillsSave: SQLiteCharacterSummonSkillSaveTable,
    characterInstanceTime: SQLiteCharacterInstanceTimeTable,
    characterMacrosTable: SQLiteCharacterMacrosTable,
    cursedWeaponsTable: SQLiteCursedWeaponsTable,
    castleTable: SQLiteCastleTable,
    clanDataTable: SQLiteClanDataTable,
    castleManorProcure: SQLiteCastleManorProcureTable,
    castleManorProduction: SQLiteCastleManorProductionTable,
    castleSiegeGuards: SQLiteCastleSiegeGuards,
    fortTable: SQLiteFortTable,
    fortFunctionsTable: SQLiteFortFunctionsTable,
    punishmentsTable: SQLitePunishmentsTable,
    clanhallTable: SQLiteClanhallTable,
    clanhallFunctionsTable: SQLiteClanhallFunctionsTable,
    clanSubpledgesTable: SQLiteClanSubpledgesTable,
    clanPrivilegesTable: SQLiteClanPrivileges,
    clanSkillsTable: SQLiteClanSkillsTable,
    clanNoticesTable: SQLiteClanNoticesTable,
    characterHennasTable: SQLiteCharacterHennasTable,
    characterTeleportBookmarks: SQLiteCharacterTeleportBookmarks,
    characterRecipebook: SQLiteCharacterRecipebook,
    dimensionalTransferItems: SQLiteDimensionalTransferItems,
    characterVariables: SQLiteCharacterVariables,
    accountVariables: SQLiteAccountVariables,
    clanWarsTable: SQLiteClanWarsTable,
    crestsTable: SQLiteCrestsTable,
    buylistsTable: SQLiteBuylistsTable,
    airshipsTable: SQLiteAirshipsTable,
    siegeClansTable: SQLiteSiegeClansTable,
    petitionFeedbackTable: SQLitePetitionFeedbackTable,
    itemAuctionBidTable: SQLiteItemAuctionBidTable,
    characterRaidPointsTable: SQLiteCharacterRaidPointsTable,
    territoryRegistrationsTable: SQLiteTerritoryRegistrationsTable,
    characterContactsTable: SQLiteCharacterContactsTable,
    merchantLeaseTable: SQLiteMerchantLeaseTable,
    olympiadNoblesTable: SQLiteOlympiadNoblesTable,
    heroesTable: SQLiteHeroesTable,
    heroDiary: SQLiteHeroDiaryTable,
    grandbossData: SQLiteGrandbossDataTable,
    grandbossPlayers: SQLiteGrandbossPlayersTable,
    fortSiegeClans: SQLiteFortSiegeClansTable,
    castleFunctions: SQLiteCastleFunctionsTable,
    castleDoorUpgrade: SQLiteCastleDoorUpgradeTable,
    castleTrapUpgrade: SQLiteCastleTrapUpgradeTable,
    auctionBidTable: SQLiteClanHallAuctionBidTable,
    fortDoorUpgrade: SQLiteFortDoorUpgradeTable,
    itemAuction: SQLiteItemAuctionTable,
    botReports: SQLiteBotReportsTable,
    clanHallAuctions: SQLiteClanHallAuctionsTable,
    lotteryTable: SQLiteLotteryTable,
    olympiadData: SQLiteOlympiadDataTable,
    fishingChampionship: SQLiteFishingChampionshipTable,
    serverVariables: SQLiteServerVariablesTable,
    itemsOnGround: SQLiteItemsOnGroundTable,
    nevitData: SQLiteNevitDataTable,
    siegableHalls: SQLiteSiegableHallTable,
    npcRespawns: SQLiteNpcRespawnTable,
    operations: SQLiteDatabaseOperations
}