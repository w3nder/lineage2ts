import { GeoPolygonType } from './enums/GeoPolygon'
import logSymbols from 'log-symbols'
import { PolygonSize } from './enums/PolygonSize'
import { PolygonLength } from './PolygonLength'
import { getPolygonHeight } from './TypeOperations'

function convertToPolygons( data: Buffer ) : Array<Buffer> {
    let index = 0
    let polygons : Array<Buffer> = new Array<Buffer>( PolygonSize.PolygonsInRegion )
    let polygonIndex = 0
    while ( index < data.length ) {
        let chunk : Buffer = extractPolygon( data, index )
        polygons[ polygonIndex ] = chunk
        index += chunk.length
        polygonIndex++
    }

    return polygons
}

function extractPolygon( data: Buffer, index : number ) : Buffer {
    let type = data.readUInt8( index )
    if ( type === GeoPolygonType.MultiLayer ) {
        return extractMultiLayerPolygon( data, index )
    }

    if ( type === GeoPolygonType.MultiLayerL2J ) {
        let size = data.readUInt16LE( index + 1 )
        return data.subarray( index, size + index + 3 )
    }

    return data.subarray( index, PolygonLength[ type ] + index )
}

function extractMultiLayerPolygon( data: Buffer, startIndex: number ) : Buffer {
    let layerSize = data.readUInt8( startIndex + 1 )
    let index = startIndex + 2

    /*
        See data layout of multi-layer polygon.
        We must account for metadata and compute actual length of each layer:
        - layer type
        - layer size
        - each metadata chunk
     */
    let polygonSize = 2 + layerSize * PolygonSize.MultiLayerMetadataSize
    while ( layerSize > 0 ) {
        let type = data.readUInt8( index )
        polygonSize += PolygonLength[ type ]
        index += PolygonSize.MultiLayerMetadataSize
        layerSize--
    }

    return data.subarray( startIndex, polygonSize + startIndex )
}

/*
    TODO:
    - add check for any blocking of destination
 */
export class GeoRegion {
    polygons: Array<Buffer>
    constructor( data: Buffer ) {
        this.polygons = convertToPolygons( data )
    }

    /*
        We should be given relative coordinates to current world range
        Each polygon index is computed via formula of (value / 128) % 256
     */
    getHeight( xRelative: number, yRelative: number, expectedZ: number ) : number {
        let xPolygonRelative = ( xRelative >> PolygonSize.WorldPolygonShift ) % PolygonSize.PolygonsInSection
        let yPolygonRelative = ( yRelative >> PolygonSize.WorldPolygonShift ) % PolygonSize.PolygonsInSection
        let index = ( xPolygonRelative * PolygonSize.PolygonsInSection ) + yPolygonRelative
        let polygon = this.polygons[ index ]

        if ( !polygon ) {
            console.log( logSymbols.error, 'Unable to find geo polygon for (x,y)', xRelative, yRelative, 'using computed index = ', index )
            return expectedZ
        }

        return getPolygonHeight( polygon, xRelative, yRelative, expectedZ )
    }
}