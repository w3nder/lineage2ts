export const enum BlockDirection {
    East = 1,
    West = 2,
    South = 4,
    North = 8,
    NE = 9,
    NW = 10,
    SE = 5,
    SW = 6,
    All = 15,
}