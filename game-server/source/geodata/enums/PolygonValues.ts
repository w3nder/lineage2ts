export const enum PolygonValues {
    Block8BitNoValue = 255,
    LowestHeight = -32768,
    HighestHeight = 32767,
    DefaultHeightAdjustment = 40
}