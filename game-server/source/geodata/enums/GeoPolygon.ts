export const enum GeoPolygonType {
    /*
        Flat polygon that consists of information about polygon type and 16bit height. No blocking is possible.
        Used for general flat space, such as certain spaces in necropolis and catacombs pathways.
        Format:
        - height as Int16
     */
    Flat,

    /*
        Flat polygon that consists of information about polygon type and 16bit height.
        Each cell contains blocking Int8 data.
        Used for necropolis and catacombs type of dungeons, where flat labyrinth of pathways are possible.
        Format:
        - height as Int16
        - 64 bytes as UInt8 values, each byte represents blocking cell data (movement direction block, bitmask value)
     */
    FlatBlocking,

    /*
        Polygon with different height of cells, has base height. Relative heights for each cell consume UInt8.
        No blocking is possible.
        Used for above-ground spaces where free range of movement is possible with slight variation of terrain.
        Format:
        - base height as Int16 (lowest height of the block)
        - 64 bytes as UInt8 values, each byte represents relative height (total height = base + relative)
     */
    MultiHeight8bit,

    /*
        Polygon with different height of cells, has base height. Relative heights for each cell consume UInt16.
        No blocking is possible.
        Used for above-ground spaces where free range of movement is possible with big variation of terrain.
        TODO: due to big variation of values, blocking movement must be required, hence remove such type and correct geodata automatically.
        Format:
        - 128 bytes as UInt16 values, each 16-bit integer represents total height
     */
    MultiHeight16bit,

    /*
        Polygon with different height of cells. Combines height and blocking status in Int16 (2 bytes) data (similar to L2J complex block).
        Additional data for blocking cells.
        Used for any and all situations where polygon would contain blocking data that requires big variation of heights. These polygons are rare.
        Format:
        - 128 bytes as UInt16 values, each 16-bit integer represents total height and blocking data
          (first 12 bits for height or Int16 & 0xFFF0, last 4 bits for blocking data or Int16 & 0x000F)
     */
    MultiHeightBlocking16bit,

    /*
        Special type that contains multiple polygon types, a wrapper for multiple other GeoPolygonType items.
        Format:
        - amount of layers (UInt8)
        - for each layer (3 bytes): type (UInt8), min value (Int16) (to recognize which layer to pick, type allows us to jump to correct binary index)
        - binary data for each layer (using same order as layer info data in previous step)
     */
    MultiLayer,

    /*
        L2J format of geodata for multi-layer block (polygon).
        Used only in circumstances when multi-layer data cannot be reliably split into distinct layers, hence left intact
        for future use.
        Format:
        - size (UInt16)
        - binary data in L2J multi-layer format
     */
    MultiLayerL2J = 255
}