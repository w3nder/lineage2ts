import { GeoPolygonType } from './enums/GeoPolygon'

export const PolygonLength : Record<number, number> = {
    [ GeoPolygonType.Flat ]: 3,
    [ GeoPolygonType.FlatBlocking ]: 67,
    [ GeoPolygonType.MultiHeight8bit ]: 67,
    [ GeoPolygonType.MultiHeight16bit ]: 129,
    [ GeoPolygonType.MultiHeightBlocking16bit ]: 129,
}