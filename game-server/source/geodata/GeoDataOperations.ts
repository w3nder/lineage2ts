import { getRegionCode, L2MapTile, L2WorldLimits } from '../gameService/enums/L2MapTile'
import { GeoRegion } from './GeoRegion'

export const enum GeoDataLimits {
    LowestHeight = -16384
}

export class GeoDataOperations {
    regions: Record<number, GeoRegion> = {}
    hasLoadedRegions: boolean = false

    getZ( x: number, y: number, currentZ: number ) : number {
        if ( !this.hasLoadedRegions ) {
            return currentZ
        }

        let xDifference = x - L2WorldLimits.MinX
        let xTile = ( xDifference >> L2MapTile.SizeShift ) + L2MapTile.XMin
        let yDifference = y - L2WorldLimits.MinY
        let yTile = ( yDifference >> L2MapTile.SizeShift ) + L2MapTile.YMin

        let regionKey = getRegionCode( xTile, yTile )
        let region = this.regions[ regionKey ]

        if ( !region ) {
            return currentZ
        }

        return region.getHeight( xDifference, yDifference, currentZ )
    }

    isInvalidZ( value: number ) : boolean {
        return value <= GeoDataLimits.LowestHeight
    }
}