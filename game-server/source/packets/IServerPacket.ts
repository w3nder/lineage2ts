export interface IServerPacket {
    writeC( value : number ) : IServerPacket
    writeH( value : number ) : IServerPacket
    writeD( value : number ) : IServerPacket
    writeF( value : number ) : IServerPacket
    writeS( value : string ) : IServerPacket
    writeB( value : Buffer ) : IServerPacket
    writeQ( value : number ) : IServerPacket
    getBuffer() : Buffer
}