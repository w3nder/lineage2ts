import { L2DataLoader } from './gameService/loader/DataLoader'
import { L2GameClientRegistry } from './gameService/L2GameClientRegistry'
import { GameClient } from './gameService/GameClient'
import { LoginServerService } from './loginService/LoginServerService'
import { ConfigManager } from './config/ConfigManager'
import * as net from 'net'
import { Socket } from 'net'
import { L2World } from './gameService/L2World'
import { EventPoolCache } from './gameService/cache/EventPoolCache'
import perfy from 'perfy'
import { DatabaseManager } from './database/manager'
import { DataManager } from './data/manager'
import aigle from 'aigle'
import { ServerLog } from './logger/Logger'

let server : net.Server

function onGameClientJoining( connection: Socket ) : void {
    return L2GameClientRegistry.addClient( new GameClient( connection ) )
}

function onServerStarted() : void {
    ServerLog.info( `Game Server accepting L2 H5 clients on ${ ConfigManager.server.getExternalServerIp().join( '.' ) }:${ LoginServerService.getServerPort() }` )
}

async function shutdownSequence() : Promise<void> {
    let players = Object.values( L2GameClientRegistry.playerToClientMap )
    if ( players.length > 0 ) {
        await aigle.resolve( players ).eachLimit( 5, async ( client : GameClient ) => {
            await client.runCleanUpTask()
            client.logout()
        } )

        ServerLog.info( `Game Shutdown : Removed ${players.length} players` )
    }

    server.close()
    LoginServerService.closeConnection()

    ServerLog.info( 'Game Shutdown : Server terminated all network connections' )

    DataManager.operations().shutdown()
    ServerLog.info( 'Game Shutdown : Datapack performed shutdown' )

    DataManager.getGeoRegionData().shutdown()
    ServerLog.info( 'Game Shutdown : Geopack performed shutdown' )

    await DatabaseManager.operations().shutdown()
    ServerLog.info( 'Game Shutdown : Database performed shutdown' )

    ServerLog.flush( () => process.exit() )
}

function onMakeServerAvailable() {
    server = net.createServer( onGameClientJoining ).listen( LoginServerService.getServerPort(), onServerStarted )

    ServerLog.info( `Game Server loaded in ${ metrics.time } seconds.` )
    ServerLog.info( `Game Server initialized in ${ perfy.end( 'server-start' ).time } seconds.` )
    ServerLog.info( `Game Server world contains ${ L2World.getSize() } objects.` )

    LoginServerService.enableGameServer()

    if ( global.gc ) {
        global.gc()
    }

    EventPoolCache.setLogging( true )

    /*
        We must be able to load all data and initialize server before we are able to shut down
        all parts properly.
     */
    /*
        Used by various restart utilities that manage application process.
     */
    process.once( 'SIGUSR1', shutdownSequence )
    process.once( 'SIGUSR2', shutdownSequence )

    /*
        Used by terminal applications to interrupt process, aka Ctrl + C.
     */
    process.on( 'SIGINT', shutdownSequence )

    /*
        Termination signal used by OS to signify process must be killed (similar to SIGKILL).
     */
    process.on( 'SIGTERM', shutdownSequence )
}

let metrics = perfy.end( 'server-load' )
perfy.start( 'server-start' )

LoginServerService.connectToLoginServer()
L2DataLoader.start().then( onMakeServerAvailable )