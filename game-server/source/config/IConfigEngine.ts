import { L2CharacterConfigurationApi } from './interface/CharacterConfigurationApi'
import { L2DatabaseConfigurationApi } from './interface/DatabaseConfigurationApi'
import { L2GeneralConfigurationApi } from './interface/GeneralConfigurationApi'
import { L2ServerConfigurationApi } from './interface/ServerConfigurationApi'
import { L2SevenSignsConfigurationApi } from './interface/SevenSignsConfigurationApi'
import { L2SiegeConfigurationApi } from './interface/SiegeConfigurationApi'
import { L2TerritoryWarConfigurationApi } from './interface/TerritoryWarConfigurationApi'
import { L2CustomsConfigurationApi } from './interface/CustomsConfigurationApi'
import { L2NPCConfigurationApi } from './interface/NPCConfigurationApi'
import { L2OlympiadConfigurationApi } from './interface/OlympiadConfigurationApi'
import { L2RatesConfigurationApi } from './interface/RatesConfigurationApi'
import { L2VitalityConfigurationApi } from './interface/VitalityConfigurationApi'
import { L2TvTConfigurationApi } from './interface/TvTConfigurationApi'
import { L2FortSiegeConfigurationApi } from './interface/FortSiegeConfigurationApi'
import { L2PvPConfigurationApi } from './interface/PvPConfigurationApi'
import { L2FortressConfiguration } from './interface/FortressConfiguration'
import { L2ClanConfigurationApi } from './interface/ClanConfigurationApi'
import { L2TuningConfigurationApi } from './interface/TuningConfigurationApi'
import { L2ClanHallConfigurationApi } from './interface/ClanHallConfigurationApi'
import { L2CastleConfigurationApi } from './interface/CastleConfigurationApi'
import { L2GraciaSeedsConfigurationApi } from './interface/GraciaSeedsConfigurationApi'
import { L2GrandBossConfigurationApi } from './interface/GrandBossConfigurationApi'
import { L2DataConfigurationApi } from './interface/DataConfigurationApi'
import { L2TeleporterConfigurationApi } from './interface/TeleporterConfigurationApi'
import { L2DiagnosticConfigurationApi } from './interface/DiagnosticConfigurationApi'
import { L2RangeConfigurationApi } from './interface/RangeConfigurationApi'

export interface L2IConfigEngine {
    character: L2CharacterConfigurationApi
    data: L2DataConfigurationApi
    general: L2GeneralConfigurationApi
    server: L2ServerConfigurationApi
    sevenSigns: L2SevenSignsConfigurationApi
    siege: L2SiegeConfigurationApi
    territoryWar: L2TerritoryWarConfigurationApi
    customs: L2CustomsConfigurationApi
    npc: L2NPCConfigurationApi
    olympiad: L2OlympiadConfigurationApi
    rates: L2RatesConfigurationApi
    vitality: L2VitalityConfigurationApi
    tvt: L2TvTConfigurationApi
    fortSiege: L2FortSiegeConfigurationApi
    pvp: L2PvPConfigurationApi
    fortress: L2FortressConfiguration
    clan: L2ClanConfigurationApi
    tuning: L2TuningConfigurationApi
    clanhall: L2ClanHallConfigurationApi
    castle: L2CastleConfigurationApi
    graciaSeeds: L2GraciaSeedsConfigurationApi
    grandBoss: L2GrandBossConfigurationApi
    database: L2DatabaseConfigurationApi
    teleporter: L2TeleporterConfigurationApi
    diagnostic: L2DiagnosticConfigurationApi
    range: L2RangeConfigurationApi
}