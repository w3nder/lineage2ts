import { L2ConfigurationApi } from '../IConfiguration'

export interface L2CharacterConfigurationApi extends L2ConfigurationApi {

    allowAugmentPvPItems(): boolean

    allowEntireTree(): boolean

    alternativeCrafting(): boolean

    autoLearnDivineInspiration(): boolean

    autoLearnForgottenScrollSkills(): boolean

    autoLearnSkills(): boolean

    autoLoot(): boolean

    autoLootHerbs(): boolean

    autoLootRaids(): boolean

    blacksmithUseRecipes(): boolean

    cancelBow(): boolean

    cancelByHit(): string

    cancelCast(): boolean

    clanLeaderInstantActivation(): boolean

    craftMasterwork(): boolean

    crafting(): boolean

    danceCancelBuff(): boolean

    danceConsumeAdditionalMP(): boolean

    decreaseSkillOnDelevel(): boolean

    delevel(): boolean

    divineInspirationSpBookNeeded(): boolean

    enchantSkillSpBookNeeded(): boolean

    expertisePenalty(): boolean

    fameForDeadPlayers(): boolean

    freeTeleporting(): boolean

    getAugmentationAccSkillChance(): number

    getAugmentationBaseStatChance(): number

    getAugmentationBlacklist(): Array<number>

    getAugmentationHighGlowChance(): number

    getAugmentationHighSkillChance(): number

    getAugmentationMidGlowChance(): number

    getAugmentationMidSkillChance(): number

    getAugmentationNGGlowChance(): number

    getAugmentationNGSkillChance(): number

    getAugmentationTopGlowChance(): number

    getAugmentationTopSkillChance(): number

    getBaseSubclassLevel(): number

    getBlockListLimit(): number

    getCastleFameIntervalPoints(): number

    getCastleFameInterval(): number

    getCharMaxNumber(): number

    getClanLeaderWeekdayChange(): number

    getClanLeaderHourChange(): number

    getClanMembersForWar(): number

    getCommonRecipeLimit(): number

    getCpRegenMultiplier(): number

    getCraftingRareSpRate(): number

    getCraftingRareXpRate(): number

    getCraftingSpRate(): number

    getCraftingSpeed(): number

    getCraftingXpRate(): number

    getDaysBeforeAcceptNewClanWhenDismissed(): number

    getDaysBeforeCreateAClan(): number

    getDaysBeforeCreateNewAllyWhenDissolved(): number

    getDaysBeforeJoinAClan(): number

    getDaysBeforeJoinAllyWhenDismissed(): number

    getDaysBeforeJoiningAllianceAfterLeaving(): number

    getDaysToPassToDissolveAClan(): number

    getDeathPenaltyChance(): number

    getDaysToDeleteCharacter(): number

    getDwarfRecipeLimit(): number

    getEffectTickRatio(): number

    getEnchantBlacklist(): Array<number>

    getEnchantChanceElementCrystal(): number

    getEnchantChanceElementEnergy(): number

    getEnchantChanceElementJewel(): number

    getEnchantChanceElementStone(): number

    getExponentSp(): number

    getExponentXp(): number

    getFeeDeleteSubClassSkills(): number

    getFeeDeleteTransferSkills(): number

    getForbiddenNames(): Set<string>

    getFortressFameIntervalPoints(): number

    getFortressFameInterval(): number

    getFreightPrice(): number

    getFriendListLimit(): number

    getHpRegenerationMultiplier(): number

    getMaxAbnormalStateSuccessRate(): number

    getMaxBuffAmount(): number

    getMaxDanceAmount(): number

    getMaxEvasion(): number

    getMaxExpBonus(): number

    getMaxMagicAttackSpeed(): number

    getMaxMagicCritRate(): number

    getMaxNumberOfClansInAlly(): number

    getMaxPowerAttackSpeed(): number

    getMaxPowerCritRate(): number

    getMaxPersonalFamePoints(): number

    getMaxPetLevel(): number

    getMaxPetitionsPending(): number

    getMaxPetitionsPerPlayer(): number

    getMaxPlayerLevel(): number

    getMaxPvtStoreBuySlotsDwarf(): number

    getMaxPvtStoreBuySlotsOther(): number

    getMaxPvtStoreSellSlotsDwarf(): number

    getMaxPvtStoreSellSlotsOther(): number

    getMaxRunSpeed(): number

    getMaxSpBonus(): number

    getMaxSubclass(): number

    getMaxSubclassLevel(): number

    getMaxTriggeredBuffAmount(): number

    getMaximumFreightSlots(): number

    getMaximumSlotsForDwarf(): number

    getMaximumSlotsForGMPlayer(): number

    getMaximumSlotsForNoDwarf(): number

    getMaximumSlotsForQuestItems(): number

    getMaximumWarehouseSlotsForClan(): number

    getMaximumWarehouseSlotsForDwarf(): number

    getMaximumWarehouseSlotsForNoDwarf(): number

    getMinAbnormalStateSuccessRate(): number

    getMpRegenerationMultiplier(): number

    getNpcTalkBlockingTime(): number

    getPartyRange(): number

    getPartyRange2(): number

    getPartyXpCutoffGaps(): Array<[number, number, number]>

    getPartyXpCutoffLevel(): number

    getPartyXpCutoffMethod(): string

    getPartyXpCutoffPercent(): number

    getPerfectShieldBlockRate(): number

    getPetNameTemplate(): RegExp

    getPlayerFakeDeathUpProtection(): number

    getPlayerNameTemplate(): RegExp

    getPlayerSpawnProtection(): number

    getPlayerSpawnProtectionAllowedItems(): Array<number>

    getPlayerTeleportProtection(): number

    getRaidLootRightsCCSize(): number

    getRaidLootRightsInterval(): number

    getRespawnRestoreCP(): number

    getRespawnRestoreHP(): number

    getRespawnRestoreMP(): number

    getRetailLikeAugmentationHighGradeChance(): Array<number>

    getRetailLikeAugmentationMidGradeChance(): Array<number>

    getRetailLikeAugmentationNoGradeChance(): Array<number>

    getRetailLikeAugmentationTopGradeChance(): Array<number>

    getRunSpeedBoost(): number

    getSkillDuration(): { [skillId: number]: number }

    getSkillReuse(): { [skillId: number]: number }

// Party

    getStartingAdena(): number

    getStartingLevel(): number

    getStartingSP(): number

    getTeleportWatchdogTimeout(): number

    getUnstuckInterval(): number

    getWeightLimit(): number

    initialEquipmentEvent(): boolean

    karmaPlayerCanBeKilledInPeaceZone(): boolean

    karmaPlayerCanShop(): boolean

    karmaPlayerCanTeleport(): boolean

    karmaPlayerCanTrade(): boolean

    karmaPlayerCanUseGK(): boolean

    karmaPlayerCanUseWareHouse(): boolean

    leavePartyLeader(): boolean

    lifeCrystalNeeded(): boolean

    magicFailures(): boolean

    membersCanWithdrawFromClanWH(): boolean

    modifySkillDuration(): boolean

    modifySkillReuse(): boolean

    offsetOnTeleport(): boolean

    petitioningAllowed(): boolean

    randomRespawnInTown(): boolean

    removeCastleCirclets(): boolean

    restorePetOnReconnect(): boolean

    restoreServitorOnReconnect(): boolean

    retailLikeAugmentation(): boolean

    retailLikeAugmentationAccessory(): boolean

    shieldBlocks(): boolean

    silenceModeExclude(): boolean

    skillLearn(): boolean

    storeDances(): boolean

    storeRecipeShopList(): boolean

    storeSkillCooltime(): boolean

    storeUISettings(): boolean

    subclassEverywhere(): boolean

    subclassStoreSkillCooltime(): boolean

    subclassWithoutQuests(): boolean

    summonStoreSkillCooltime(): boolean

    transformationWithoutQuest(): boolean

    tutorial(): boolean

    validateTriggerSkills(): boolean

    getMaximumRefundCapacity() : number

    isNevitEnabled() : boolean
    getNevitHuntingPointLimit() : number
    getNevitEffectTime() : number
    getNevitAdventTime() : number
    getNevitIgnoreAdventTime() : boolean
    getNevitLevelupPoints() : number
    getNevitBlessedLevelupPoints() : number
    getNevitBlessedSpBonus() : number
    getNevitBlessedExpBonus() : number
}