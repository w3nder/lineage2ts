import { L2ConfigurationApi } from '../IConfiguration'

export interface L2DataConfigurationApi extends L2ConfigurationApi {
    getDataEngine() : string
    getDataOptions() : string
    getGeoEngine() : string
    getGeoOptions() : string
}