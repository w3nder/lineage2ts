import { L2ConfigurationApi } from '../IConfiguration'

export interface L2TeleporterConfigurationApi extends L2ConfigurationApi {
    isEnabled() : boolean
    isRestrictedInEvents() : boolean
    isRestrictedInDuel() : boolean
    isRestrictedInFight() : boolean
    isRestrictedInPVP() : boolean
    isRestrictedForChaoticPlayers() : boolean
    isVoicedCommandEnabled() : boolean
    getVoicedCommand() : string
    getVoicedName() : string
    getVoicedRequiredItems() : Array<number>
}