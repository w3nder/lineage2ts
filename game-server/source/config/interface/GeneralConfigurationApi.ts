import { L2ConfigurationApi } from '../IConfiguration'
import { ItemInstanceType } from '../../gameService/models/items/ItemInstanceType'

export interface L2GeneralConfigurationApi extends L2ConfigurationApi {

    allowAttachments(): boolean

    allowBoat(): boolean

    allowCursedWeapons(): boolean

    allowDiscardItem(): boolean

    allowFishing(): boolean

    allowFishingChampionship() : boolean

    allowLottery(): boolean

    allowMail(): boolean

    allowManor(): boolean

    allowPetWalkers(): boolean

    allowRace(): boolean

    allowRefund(): boolean

    allowRentPet(): boolean

    allowReportsFromSameClanMembers(): boolean

    allowSummonInInstance(): boolean

    allowWater(): boolean

    allowWear(): boolean

    autoDeleteInvalidQuestData(): boolean

    customBuyListLoad(): boolean

    customItemsLoad(): boolean

    customMultisellLoad(): boolean

    customNpcBufferTables(): boolean

    customNpcData(): boolean

    customSkillsLoad(): boolean

    customSpawnlistTable(): boolean

    databaseCleanUp(): boolean

    isSaveDroppedResetAfterLoad(): boolean

    enableBlockCheckerEvent(): boolean

    enableBotReportButton(): boolean

    enableCommunityBoard(): boolean

    enableFallingDamage(): boolean

    everybodyHasAdminRights(): boolean

    forceInventoryUpdate(): boolean

    gameGuardEnforce(): boolean

    gameGuardProhibitAction(): boolean

    getAutoJumpsDelayMax(): number

    getAutoJumpsDelayMin(): number

    getBBSDefault(): string

    getBirthdayGiftDelivery() : string

    getBanChatChannels(): Set<number>

    getBirthdayGifts(): Array<number>

    getBirthdayMailSubject(): string

    getBirthdayMailText(): string

    getBlockCheckerMinTeamMembers(): number

    getBoatBroadcastRadius(): number

    getBossRoomTimeMultiply(): number

    getBotReportDelay(): number

    getBotReportPointsResetHour(): string

    getCaptainCost(): number

    getChatFilters(): Array<RegExp>

    getChatFilterCharacters(): string

    getCommanderCost(): number

    getDefaultPunishment(): number

    getDefaultPunishmentInterval(): number

    getEjectDeadPlayerTime(): number

    getGlobalChat(): string

    getHeroCost(): number

    getInstanceFinishTime(): number

    getItemAuctionTimeExtendsOnBid(): number

    getItemAuctionDeliveryMethod() : string

    getItemAuctionDelaySecondsBeforeStart() : number

    getItemAuctionStartCron() : string

    getPlayerObjectScanInterval(): number

    getLottery2and1NumberPrize(): number

    getLottery3NumberRate(): number

    getLottery4NumberRate(): number

    getLottery5NumberRate(): number

    getLotteryPrize(): number

    getLotteryTicketPrice(): number

    getManorApproveMin(): number

    getManorApproveTime(): number

    getManorMaintenanceMin(): number

    getManorRefreshMin(): number

    getManorRefreshTime(): number

    getManorSavePeriodRate(): number

    getMaxMonsterAnimation(): number

    getMaxNPCAnimation(): number

    getMaxRiftJumps(): number

    getMinMonsterAnimation(): number

    getMinNPCAnimation(): number

    getNormalEnchantCostMultipiler(): number

    getNumberOfNecessaryPartyMembers(): number

    getOfficerCost(): number

    getPeaceZoneMode(): number

    getProtectedItems(): Set<number>

    getRecruitCost(): number

    getRiftMinPartySize(): number

    getRiftSpawnDelay(): number

    getSafeEnchantCostMultipiler(): number

    saveDroppedItem(): boolean

    getSaveDroppedItemInterval(): number

    getSaveDroppedProtectedTypes() : Set<ItemInstanceType>

    getSaveDroppedExpiration() : number

    getSoldierCost(): number

    getTimeOfAttack(): number

    getTimeOfCoolDown(): number

    getTimeOfEntry(): number

    getTimeOfWarmUp(): number

    getTradeChat(): string

    getWearDelay(): number

    getWearPrice(): number

    gmAudit(): boolean

    gmGiveSpecialAuraSkills(): boolean

    gmGiveSpecialSkills(): boolean

    gmHeroAura(): boolean

    gmItemRestriction(): boolean

    gmRestartFighting(): boolean

    gmShowAnnouncerName(): boolean

    gmShowCritAnnouncerName(): boolean

    gmSkillRestriction(): boolean

    gmStartupAutoList(): boolean

    gmStartupDietMode(): boolean

    gmStartupInvisible(): boolean

    gmStartupInvulnerable(): boolean

    gmStartupSilence(): boolean

    gmTradeRestrictedItems(): boolean

    hellboundWithoutQuest(): boolean

    instanceDebug(): boolean

    isHBCEFairPlay(): boolean

    itemAuctionEnabled(): boolean

    jailDisableChat(): boolean

    jailDisableTransaction(): boolean

    jailIsPvp(): boolean

    logAutoAnnouncements(): boolean

    logChat(): boolean

    logItemEnchants(): boolean

    logItems(): boolean

    logItemsSmallLog(): boolean

    logSkillEnchants(): boolean

    manorSaveAllActions(): boolean

    multipleItemDrop(): boolean

    onlyGMItemsFree(): boolean

    preciseDropCalculation(): boolean

    restorePlayerInstance(): boolean

    saveGmSpawnOnCustom(): boolean

    serverGMOnly(): boolean

    isServerNewsEnabled(): boolean

    getServerNewsPath() : string

    skillCheckEnable(): boolean

    skillCheckGM(): boolean

    skillCheckRemove(): boolean

    useChatFilter(): boolean

    getLotteryPeriodDays(): number

    getChatTextLimit(): number

    getChatLinkedItemLimit(): number

    getFishingChampionshipRewardItemId() : number

    getFishingChampionshipReward1() : number
    getFishingChampionshipReward2() : number
    getFishingChampionshipReward3() : number
    getFishingChampionshipReward4() : number
    getFishingChampionshipReward5() : number
    getFishingChampionshipRewardOthers() : number

    getFishingChampionshipIntervalCron() : string
    getFishingChampionshipParticipantAmount() : number
    isAllowFishingExperience() : boolean
    getFishingExperienceMultiplier() : number
    getFishingDurationMultiplier()
    isClanNoticeTemplateEnabled() : boolean
    getClanNoticeTemplatePath() : string
    getMailSystemName() : string
    getMailCodExpirationTime() : number
    getMailDefaultExpirationTime() : number
}