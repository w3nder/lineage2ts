import { L2ConfigurationApi } from '../IConfiguration'

export interface L2FortressConfiguration extends L2ConfigurationApi {

    getBloodOathCount(): number

    getExpRegenerationFeeLvl1(): number

    getExpRegenerationFeeLvl2(): number

    getExpRegenerationFunctionFeeRatio(): number

    getFeeForCastle(): number

    getHpRegenerationFeeLvl1(): number

    getHpRegenerationFeeLvl2(): number

    getHpRegenerationFunctionFeeRatio(): number

    getMaxKeepTime(): number

    getMaxSupplyLevel(): number

    getMpRegenerationFeeLvl1(): number

    getMpRegenerationFeeLvl2(): number

    getMpRegenerationFunctionFeeRatio(): number

    getPeriodicUpdateFrequency(): number

    getSupportFeeLvl1(): number

    getSupportFeeLvl2(): number

    getSupportFunctionFeeRatio(): number

    getTeleportFunctionFeeLvl1(): number

    getTeleportFunctionFeeLvl2(): number

    getTeleportFunctionFeeRatio(): number
}