import { L2ConfigurationApi } from '../IConfiguration'

export interface L2VitalityConfigurationApi extends L2ConfigurationApi {

    enabled(): boolean

    getRateRecoveryOnReconnect(): number

    getIntervalPoints(): number

    getRateVitalityGain(): number

    getRateVitalityLevel1(): number

    getRateVitalityLevel2(): number

    getRateVitalityLevel3(): number

    getRateVitalityLevel4(): number

    getRateVitalityLost(): number

    getStartingVitalityPoints(): number

    recoverVitalityOnReconnect(): boolean

    getRecoveryInterval() : number

    requirePeaceArea() : boolean
}