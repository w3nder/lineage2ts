import { L2ConfigurationApi } from '../IConfiguration'

export interface L2CastleConfigurationApi extends L2ConfigurationApi {

    allowRideWyvernAlways(): boolean

    allowRideWyvernDuringSiege(): boolean

    getExpRegenerationFeeLvl1(): number

    getExpRegenerationFeeLvl2(): number

    getExpRegenerationFunctionFeeRatio(): number

    getHpRegenerationFeeLvl1(): number

    getHpRegenerationFeeLvl2(): number

    getHpRegenerationFunctionFeeRatio(): number

    getInnerDoorUpgradePriceLvl2(): number

    getInnerDoorUpgradePriceLvl3(): number

    getInnerDoorUpgradePriceLvl5(): number

    getMpRegenerationFeeLvl1(): number

    getMpRegenerationFeeLvl2(): number

    getMpRegenerationFunctionFeeRatio(): number

    getOuterDoorUpgradePriceLvl2(): number

    getOuterDoorUpgradePriceLvl3(): number

    getOuterDoorUpgradePriceLvl5(): number

    getSiegeHourList(): Array<number>

    getSupportFeeLvl1(): number

    getSupportFeeLvl2(): number

    getSupportFunctionFeeRatio(): number

    getTeleportFunctionFeeLvl1(): number

    getTeleportFunctionFeeLvl2(): number

    getTeleportFunctionFeeRatio(): number

    getTrapUpgradePriceLvl1(): number

    getTrapUpgradePriceLvl2(): number

    getTrapUpgradePriceLvl3(): number

    getTrapUpgradePriceLvl4(): number

    getWallUpgradePriceLvl2(): number

    getWallUpgradePriceLvl3(): number

    getWallUpgradePriceLvl5(): number
}