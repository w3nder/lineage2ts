import { L2ConfigurationApi } from '../IConfiguration'

export interface L2DatabaseConfigurationApi extends L2ConfigurationApi {

    getEngine(): string

    getConnectionParameters(): { [name: string]: string }

    getDatabaseUsername(): string
    getDatabasePassword(): string

    getDatabaseName() : string
    getDatabaseHost() : string
    getDatabasePort() : number
}