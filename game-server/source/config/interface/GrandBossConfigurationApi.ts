import { L2ConfigurationApi } from '../IConfiguration'

export interface L2GrandBossConfigurationApi extends L2ConfigurationApi {

    getAntharasWaitTime(): number

    getBelethMinPlayers(): number

    getIntervalOfAntharasSpawn(): number

    getIntervalOfBaiumSpawn(): number

    getIntervalOfBelethSpawn(): number

    getIntervalOfCoreSpawn(): number

    getIntervalOfOrfenSpawn(): number

    getIntervalOfQueenAntSpawn(): number

    getIntervalOfValakasSpawn(): number

    getRandomOfAntharasSpawn(): number

    getRandomOfBaiumSpawn(): number

    getRandomOfBelethSpawn(): number

    getRandomOfCoreSpawn(): number

    getRandomOfOrfenSpawn(): number

    getRandomOfQueenAntSpawn(): number

    getRandomOfValakasSpawn(): number

    getValakasWaitTime(): number
}