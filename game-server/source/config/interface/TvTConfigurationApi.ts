import { L2ConfigurationApi } from '../IConfiguration'

export interface L2TvTConfigurationApi extends L2ConfigurationApi {

    allowPotion(): boolean

    allowScroll(): boolean

    allowSummonByItem(): boolean

    allowTargetTeamMember(): boolean

    allowVoicedInfoCommand(): boolean

    enabled(): boolean

    getDoorsToClose(): Array<number>

    getDoorsToOpen(): Array<number>

    getEffectsRemoval(): number

    getFighterBuffs(): Array<L2TvTConfigurationSkill>

    getInstanceFile(): string

    getInterval(): Array<string>

    getMageBuffs(): Array<L2TvTConfigurationSkill>

    getMaxParticipantsPerIP(): number

    getMaxPlayerLevel(): number

    getMaxPlayersInTeams(): number

    getMinPlayerLevel(): number

    getMinPlayersInTeams(): number

    getParticipationFee(): L2TvTConfigurationItem

    getParticipationNpcId(): number

    getParticipationNpcLocation(): L2TvTConfigurationLocation

    getParticipationTime(): number

    getRespawnTeleportDelay(): number

    getReward(): Array<L2TvTConfigurationItem>

    getRunningTime(): number

    getStartLeaveTeleportDelay(): number

    getTeam1Loc(): L2TvTConfigurationLocation

    getTeam1Name(): string

    getTeam2Loc(): L2TvTConfigurationLocation

    getTeam2Name(): string

    instanced(): boolean

    rewardTeamTie(): boolean
}

export interface L2TvTConfigurationItem {
    id: number
    count: number
}

export interface L2TvTConfigurationLocation {
    x: number
    y: number
    z: number
    heading: number
    instanceId: number
}

export interface L2TvTConfigurationSkill {
    id: number
    level: number
}