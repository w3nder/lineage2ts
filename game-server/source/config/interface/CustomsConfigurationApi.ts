import { L2ConfigurationApi } from '../IConfiguration'

export interface L2CustomsConfigurationApi extends L2ConfigurationApi {
    announcePkPvP(): boolean

    announcePkPvPNormalMessage(): boolean

    antiFeedDisconnectedAsDualbox(): boolean

    antiFeedDualbox(): boolean

    antiFeedEnable(): boolean

    autoLootHerbsVoiceRestore(): boolean

    autoLootItemsVoiceRestore(): boolean

    autoLootVoiceCommand(): boolean

    autoLootVoiceRestore(): boolean

    bankingEnabled(): boolean

    championEnable(): boolean

    championEnableInInstances(): boolean

    championEnableVitality(): boolean

    championPassive(): boolean

    chatAdmin(): boolean

    displayServerTime(): boolean

    enableManaPotionSupport(): boolean

    enableWarehouseSortingClan(): boolean

    enableWarehouseSortingPrivate(): boolean

    getAnnouncePkMsg(): string

    getAnnouncePvpMsg(): string

    getPlayerKillInterval(): number

    getAutoLootHerbsList(): Array<number>

    getAutoLootItemsList(): Array<number>

    getBankingAdenaCount(): number

    getBankingGoldbarCount(): number

    getChampionAdenasRewardsAmount(): number

    getChampionAdenasRewardsChance(): number

    getChampionAttack(): number

    getChampionFrequency(): number

    getChampionHp(): number

    getChampionHpRegen(): number

    getChampionMaxLevel(): number

    getChampionMinLevel(): number

    getChampionRewardHigherLevelItemChance(): number

    getChampionRewardItemID(): number

    getChampionRewardItemQuantity(): number

    getChampionRewardLowerLevelItemChance(): number

    getChampionRewardsAmount(): number
    getChampionQuestRewardsAmount(): number

    getChampionRewardsChance(): number
    getChampionQuestRewardsChance(): number

    getChampionRewardsExpSp(): number

    getChampionSpeedAttack(): number

    getChampionTitle(): string

    getDualboxCheckMaxL2EventParticipantsPerIP(): number

    getDualboxCheckMaxOlympiadParticipantsPerIP(): number

    getDualboxCheckMaxPlayersPerIP(): number

    getDualboxCheckWhitelist(): { [ key: number ]: number }

    getOfflineMaxDays(): number

    getOfflineNameColor(): number

    getScreenWelcomeMessageText(): string

    getScreenWelcomeMessageTime(): number

    hellboundStatus(): boolean

    l2WalkerProtection(): boolean

    offlineCraftEnable(): boolean

    offlineDisconnectFinished(): boolean

    offlineFame(): boolean

    offlineModeInPeaceZone(): boolean

    offlineModeNoDamage(): boolean

    offlineSetNameColor(): boolean

    offlineTradeEnable(): boolean

    restoreOffliners(): boolean

    screenWelcomeMessageEnable(): boolean

    viewNpcVoiced() : boolean
    viewNpcEnabled() : boolean
    viewNpcItems() : boolean

    changePasswordEnabled() : boolean
    getTreasureChestVisibility(): number
    isAccountPrivilegesEnabled() : boolean
    getAccountPrivilegesAmount() : number
    getAccountPrivilegesLevel() : number
}