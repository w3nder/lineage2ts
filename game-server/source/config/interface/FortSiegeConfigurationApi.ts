import { L2ConfigurationApi } from '../IConfiguration'

export interface L2FortSiegeConfigurationApi extends L2ConfigurationApi {

    getAaruCommander1(): string

    getAaruCommander2(): string

    getAaruCommander3(): string

    getAaruFlag1(): string

    getAaruFlag2(): string

    getAaruFlag3(): string

    getAntharasCommander1(): string

    getAntharasCommander2(): string

    getAntharasCommander3(): string

    getAntharasCommander4(): string

    getAntharasFlag1(): string

    getAntharasFlag2(): string

    getAntharasFlag3(): string

    getArchaicCommander1(): string

    getArchaicCommander2(): string

    getArchaicCommander3(): string

    getArchaicFlag1(): string

    getArchaicFlag2(): string

    getArchaicFlag3(): string

    getAttackerMaxClans(): number

    getBayouCommander1(): string

    getBayouCommander2(): string

    getBayouCommander3(): string

    getBayouCommander4(): string

    getBayouFlag1(): string

    getBayouFlag2(): string

    getBayouFlag3(): string

    getBorderlandCommander1(): string

    getBorderlandCommander2(): string

    getBorderlandCommander3(): string

    getBorderlandCommander4(): string

    getBorderlandFlag1(): string

    getBorderlandFlag2(): string

    getBorderlandFlag3(): string

    getCloudMountainCommander1(): string

    getCloudMountainCommander2(): string

    getCloudMountainCommander3(): string

    getCloudMountainCommander4(): string

    getCloudMountainFlag1(): string

    getCloudMountainFlag2(): string

    getCloudMountainFlag3(): string

    getCountDownLength(): number

    getDemonCommander1(): string

    getDemonCommander2(): string

    getDemonCommander3(): string

    getDemonFlag1(): string

    getDemonFlag2(): string

    getDemonFlag3(): string

    getDragonspineCommander1(): string

    getDragonspineCommander2(): string

    getDragonspineCommander3(): string

    getDragonspineFlag1(): string

    getDragonspineFlag2(): string

    getDragonspineFlag3(): string

    getFloranCommander1(): string

    getFloranCommander2(): string

    getFloranCommander3(): string

    getFloranCommander4(): string

    getFloranFlag1(): string

    getFloranFlag2(): string

    getFloranFlag3(): string

    getHiveCommander1(): string

    getHiveCommander2(): string

    getHiveCommander3(): string

    getHiveFlag1(): string

    getHiveFlag2(): string

    getHiveFlag3(): string

    getHuntersCommander1(): string

    getHuntersCommander2(): string

    getHuntersCommander3(): string

    getHuntersCommander4(): string

    getHuntersFlag1(): string

    getHuntersFlag2(): string

    getHuntersFlag3(): string

    getIvoryCommander1(): string

    getIvoryCommander2(): string

    getIvoryCommander3(): string

    getIvoryFlag1(): string

    getIvoryFlag2(): string

    getIvoryFlag3(): string

    getMaxFlags(): number

    getMonasticCommander1(): string

    getMonasticCommander2(): string

    getMonasticCommander3(): string

    getMonasticFlag1(): string

    getMonasticFlag2(): string

    getMonasticFlag3(): string

    getNarsellCommander1(): string

    getNarsellCommander2(): string

    getNarsellCommander3(): string

    getNarsellFlag1(): string

    getNarsellFlag2(): string

    getNarsellFlag3(): string

    getShantyCommander1(): string

    getShantyCommander2(): string

    getShantyCommander3(): string

    getShantyFlag1(): string

    getShantyFlag2(): string

    getShantyFlag3(): string

    getSiegeClanMinLevel(): number

    getSiegeLength(): number

    getSouthernCommander1(): string

    getSouthernCommander2(): string

    getSouthernCommander3(): string

    getSouthernCommander4(): string

    getSouthernFlag1(): string

    getSouthernFlag2(): string

    getSouthernFlag3(): string

    getSuspiciousMerchantRespawnDelay(): number

    getSwampCommander1(): string

    getSwampCommander2(): string

    getSwampCommander3(): string

    getSwampCommander4(): string

    getSwampFlag1(): string

    getSwampFlag2(): string

    getSwampFlag3(): string

    getTanorCommander1(): string

    getTanorCommander2(): string

    getTanorCommander3(): string

    getTanorFlag1(): string

    getTanorFlag2(): string

    getTanorFlag3(): string

    getValleyCommander1(): string

    getValleyCommander2(): string

    getValleyCommander3(): string

    getValleyCommander4(): string

    getValleyFlag1(): string

    getValleyFlag2(): string

    getValleyFlag3(): string

    getWesternCommander1(): string

    getWesternCommander2(): string

    getWesternCommander3(): string

    getWesternCommander4(): string

    getWesternFlag1(): string

    getWesternFlag2(): string

    getWesternFlag3(): string

    getWhiteSandsCommander1(): string

    getWhiteSandsCommander2(): string

    getWhiteSandsCommander3(): string

    getWhiteSandsFlag1(): string

    getWhiteSandsFlag2(): string

    getWhiteSandsFlag3(): string

    justToTerritory(): boolean
}