import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2SiegeConfigurationApi, L2SiegeConfigurationTowerSpawn } from '../../interface/SiegeConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'
import _ from 'lodash'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

function getFlameTowerSpawn( value: string ): L2SiegeConfigurationTowerSpawn {
    let [ x, y, z, npcId, ...zoneIds ] = _.map( _.split( value, ',' ), value => _.parseInt( value ) )

    return {
        x,
        y,
        z,
        npcId,
        zoneIds,
    }
}

function getControlTowerSpawn( value: string ): L2SiegeConfigurationTowerSpawn {
    let [ x, y, z, npcId ] = _.map( _.split( value, ',' ), value => _.parseInt( value ) )

    return {
        x,
        y,
        z,
        npcId,
        zoneIds: [],
    }
}

class Configuration extends L2ConfigurationSubscription implements L2SiegeConfigurationApi {
    getAdenFlameTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'AdenFlameTower2', getFlameTowerSpawn )
    }

    getAdenControlTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'AdenControlTower1', getControlTowerSpawn )
    }

    getAdenControlTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'AdenControlTower2', getControlTowerSpawn )
    }

    getAdenControlTower3(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'AdenControlTower3', getControlTowerSpawn )
    }

    getAdenFlameTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'AdenFlameTower1', getFlameTowerSpawn )
    }

    getAdenMaxMercenaries(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AdenMaxMercenaries' )
    }

    getAttackerMaxClans(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AttackerMaxClans' )
    }

    getAttackerRespawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AttackerRespawn' )
    }

    getBloodAllianceReward(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BloodAllianceReward' )
    }

    getClanMinLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanMinLevel' )
    }

    getDefenderMaxClans(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DefenderMaxClans' )
    }

    getDionControlTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'DionControlTower1', getControlTowerSpawn )
    }

    getDionControlTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'DionControlTower2', getControlTowerSpawn )
    }

    getDionControlTower3(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'DionControlTower3', getControlTowerSpawn )
    }

    getDionFlameTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'DionFlameTower1', getFlameTowerSpawn )
    }

    getDionFlameTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'getDionFlameTower2', getFlameTowerSpawn )
    }

    getDionMaxMercenaries(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DionMaxMercenaries' )
    }

    getGiranControlTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GiranControlTower1', getControlTowerSpawn )
    }

    getGiranControlTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GiranControlTower2', getControlTowerSpawn )
    }

    getGiranControlTower3(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GiranControlTower3', getControlTowerSpawn )
    }

    getGiranFlameTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GiranFlameTower1', getFlameTowerSpawn )
    }

    getGiranFlameTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GiranFlameTower2', getFlameTowerSpawn )
    }

    getGiranMaxMercenaries(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'GiranMaxMercenaries' )
    }

    getGludioControlTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GludioControlTower1', getControlTowerSpawn )
    }

    getGludioControlTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GludioControlTower2', getControlTowerSpawn )
    }

    getGludioControlTower3(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GludioControlTower3', getControlTowerSpawn )
    }

    getGludioFlameTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GludioFlameTower1', getFlameTowerSpawn )
    }

    getGludioFlameTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GludioFlameTower2', getFlameTowerSpawn )
    }

    getGludioMaxMercenaries(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'GludioMaxMercenaries' )
    }

    getGoddardControlTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GoddardControlTower1', getControlTowerSpawn )
    }

    getGoddardControlTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GoddardControlTower2', getControlTowerSpawn )
    }

    getGoddardControlTower3(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GoddardControlTower3', getControlTowerSpawn )
    }

    getGoddardFlameTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GoddardFlameTower1', getFlameTowerSpawn )
    }

    getGoddardFlameTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'GoddardFlameTower2', getFlameTowerSpawn )
    }

    getGoddardMaxMercenaries(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'GoddardMaxMercenaries' )
    }

    getInnadrilControlTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'InnadrilControlTower1', getControlTowerSpawn )
    }

    getInnadrilControlTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'InnadrilControlTower2', getControlTowerSpawn )
    }

    getInnadrilControlTower3(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'InnadrilControlTower3', getControlTowerSpawn )
    }

    getInnadrilFlameTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'InnadrilFlameTower1', getFlameTowerSpawn )
    }

    getInnadrilFlameTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'InnadrilFlameTower2', getFlameTowerSpawn )
    }

    getInnadrilMaxMercenaries(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'InnadrilMaxMercenaries' )
    }

    getMaxFlags(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxFlags' )
    }

    getOrenControlTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'OrenControlTower1', getControlTowerSpawn )
    }

    getOrenControlTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'OrenControlTower2', getControlTowerSpawn )
    }

    getOrenControlTower3(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'OrenControlTower3', getControlTowerSpawn )
    }

    getOrenFlameTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'OrenFlameTower1', getFlameTowerSpawn )
    }

    getOrenFlameTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'OrenFlameTower2', getFlameTowerSpawn )
    }

    getOrenMaxMercenaries(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'OrenMaxMercenaries' )
    }

    getRuneControlTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'RuneControlTower1', getControlTowerSpawn )
    }

    getRuneControlTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'RuneControlTower2', getControlTowerSpawn )
    }

    getRuneControlTower3(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'RuneControlTower3', getControlTowerSpawn )
    }

    getRuneFlameTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'RuneFlameTower1', getFlameTowerSpawn )
    }

    getRuneFlameTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'RuneFlameTower2', getFlameTowerSpawn )
    }

    getRuneMaxMercenaries(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RuneMaxMercenaries' )
    }

    getSchuttgartControlTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'SchuttgartControlTower1', getControlTowerSpawn )
    }

    getSchuttgartControlTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'SchuttgartControlTower2', getControlTowerSpawn )
    }

    getSchuttgartControlTower3(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'SchuttgartControlTower3', getControlTowerSpawn )
    }

    getSchuttgartFlameTower1(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'SchuttgartFlameTower1', getFlameTowerSpawn )
    }

    getSchuttgartFlameTower2(): L2SiegeConfigurationTowerSpawn {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'SchuttgartFlameTower2', getFlameTowerSpawn )
    }

    getSchuttgartMaxMercenaries(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SchuttgartMaxMercenaries' )
    }

    getSiegeLength(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SiegeLength' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'siege.properties' )
        cachedValues = {}
        return
    }
}

export const DotEnvSiegeConfiguration: L2SiegeConfigurationApi = new Configuration()