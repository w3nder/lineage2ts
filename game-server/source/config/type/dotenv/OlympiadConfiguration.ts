import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2OlympiadConfigurationApi } from '../../interface/OlympiadConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2OlympiadConfigurationApi {
    announceGames(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AnnounceGames' )
    }

    getBattlePeriod(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BattlePeriod' )
    }

    getClassedParticipants(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClassedParticipants' )
    }

    getClassedReward(): { [ key: number ]: number } {
        return DotEnvConfigurationEngineHelper.getNumberMap( configuration, cachedValues, 'ClassedReward' )
    }

    getCompetitionPeriod(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CompetitionPeriod' )
    }

    getCompetitionRewardItem(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CompetitionRewardItem' )
    }

    getCurrentCycle(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CurrentCycle' )
    }

    getDividerClassed(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DividerClassed' )
    }

    getDividerNonClassed(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DividerNonClassed' )
    }

    getEnchantLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'EnchantLimit' )
    }

    getGPPerPoint(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'GPPerPoint' )
    }

    getHeroPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HeroPoints' )
    }

    getMaxBuffs(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxBuffs' )
    }

    getMaxPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPoints' )
    }

    getMaxWeeklyMatches(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxWeeklyMatches' )
    }

    getMaxWeeklyMatchesClassed(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxWeeklyMatchesClassed' )
    }

    getMaxWeeklyMatchesNonClassed(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxWeeklyMatchesNonClassed' )
    }

    getMaxWeeklyMatchesTeam(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxWeeklyMatchesTeam' )
    }

    getMinMatchesForPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinMatchesForPoints' )
    }

    getNextWeeklyChange(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'NextWeeklyChange' )
    }

    getNonClassedParticipants(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'NonClassedParticipants' )
    }

    getNonClassedReward(): { [ key: number ]: number } {
        return DotEnvConfigurationEngineHelper.getNumberMap( configuration, cachedValues, 'NonClassedReward' )
    }

    getOlympiadEnd(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'OlympiadEnd' )
    }

    getPeriod(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'Period' )
    }

    getRank1Points(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'Rank1Points' )
    }

    getRank2Points(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'Rank2Points' )
    }

    getRank3Points(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'Rank3Points' )
    }

    getRank4Points(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'Rank4Points' )
    }

    getRank5Points(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'Rank5Points' )
    }

    getRegistrationDisplayNumber(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RegistrationDisplayNumber' )
    }

    getRestrictedItems(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'RestrictedItems' )
    }

    getStartHour(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'StartHour' )
    }

    getStartMinute(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'StartMinute' )
    }

    getStartPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'StartPoints' )
    }

    getTeamReward(): { [ key: number ]: number } {
        return DotEnvConfigurationEngineHelper.getNumberMap( configuration, cachedValues, 'TeamReward' )
    }

    getTeamsParticipants(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TeamsParticipants' )
    }

    getValidationEnd(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ValidationEnd' )
    }

    getValidationPeriod(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ValidationPeriod' )
    }

    getWaitTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'WaitTime' )
    }

    getWeeklyPeriod(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'WeeklyPeriod' )
    }

    getWeeklyPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'WeeklyPoints' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'olympiad.properties' )
        cachedValues = {}
        return
    }

    logFights(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'LogFights' )
    }

    showMonthlyWinners(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ShowMonthlyWinners' )
    }
}

export const DotEnvOlympiadConfiguration: L2OlympiadConfigurationApi = new Configuration()