import { L2NPCConfigurationApi } from '../../interface/NPCConfigurationApi'
import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

function getCorpseTime( value: string ) : { [ key: string ] : number } {
    let pairs : Array<string> = value.split( ';' )
    return pairs.reduce( ( finalMap: { [ key: string ] : number }, currentPair : string ) => {
        let [ key, value ] = currentPair.split( ':' )

        finalMap[ key ] = parseInt( value )
        return finalMap
    }, {} )
}

class Configuration extends L2ConfigurationSubscription implements L2NPCConfigurationApi {
    getDisabledMerchantIds(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'DisabledMerchantIds' )
    }

    randomWalkChance(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RandomWalkChance' )
    }

    allowWyvernUpgrader(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AllowWyvernUpgrader' )
    }

    announceMammonSpawn(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AnnounceMammonSpawn' )
    }

    attackableNpcs(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AttackableNpcs' )
    }

    getCorpseConsumeSkillAllowedTimeBeforeDecay(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CorpseConsumeSkillAllowedTimeBeforeDecay' )
    }

    getCritDamagePenaltyForLevelDifferences(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'CritDmgPenaltyForLvLDifferences' )
    }

    getCustomMinionsRespawnTime(): { [ key: number ]: number } {
        return DotEnvConfigurationEngineHelper.getNumberMap( configuration, cachedValues, 'CustomMinionsRespawnTime' )
    }

    getDamagePenaltyForLevelDifferences(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'DmgPenaltyForLvLDifferences' )
    }

    getCorpseTimePerEntityType(): { [ key: string ]: number } {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'CorpseTimePerEntityType', getCorpseTime )
    }

    getDefaultCorpseTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DefaultCorpseTime' )
    }

    getDropAdenaMaxLevelDifference(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DropAdenaMaxLevelDifference' )
    }

    getDropAdenaMinLevelDifference(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DropAdenaMinLevelDifference' )
    }

    getDropAdenaMinLevelGapChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DropAdenaMinLevelGapChance' )
    }

    getDropItemMaxLevelDifference(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DropItemMaxLevelDifference' )
    }

    getDropItemMinLevelDifference(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DropItemMinLevelDifference' )
    }

    getDropItemMinLevelGapChance(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DropItemMinLevelGapChance' )
    }

    getGrandChaosTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'GrandChaosTime' )
    }

    getMaxAggroRange(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxAggroRange' )
    }

    getMaxDriftRange(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxDriftRange' )
    }

    getMaximumSlotsForPet(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaximumSlotsForPet' )
    }

    getMinNPCLevelForDamagePenalty(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinNPCLevelForDmgPenalty' )
    }

    getMinNPCLevelForMagicPenalty(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinNPCLevelForMagicPenalty' )
    }

    getMinionChaosTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinionChaosTime' )
    }

    getPetHpRegenerationMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'PetHpRegenMultiplier' )
    }

    getPetMpRegenerationMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'PetMpRegenMultiplier' )
    }

    getPetRentNPCs(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'PetRentNPCs' )
    }

    getRaidChaosTime(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RaidChaosTime' )
    }

    getRaidHpRegenerationMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RaidHpRegenMultiplier' )
    }

    getRaidMagicAttackMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RaidMAttackMultiplier' )
    }

    getRaidMagicDefenceMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RaidMDefenceMultiplier' )
    }

    getRaidMaxRespawnMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RaidMaxRespawnMultiplier' )
    }

    getRaidMinRespawnMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RaidMinRespawnMultiplier' )
    }

    getRaidMinionRespawnTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RaidMinionRespawnTime' )
    }

    getRaidMpRegenerationMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RaidMpRegenMultiplier' )
    }

    getRaidPowerAttackMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RaidPAttackMultiplier' )
    }

    getRaidPowerDefenceMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RaidPDefenceMultiplier' )
    }

    getSkillChancePenaltyForLevelDifferences(): Array<number> {
        return DotEnvConfigurationEngineHelper.getFloatArray( configuration, cachedValues, 'SkillChancePenaltyForLvLDifferences' )
    }

    getSkillDamagePenaltyForLevelDifferences(): Array<number> {
        return DotEnvConfigurationEngineHelper.getFloatArray( configuration, cachedValues, 'SkillDmgPenaltyForLvLDifferences' )
    }

    getSpoiledCorpseExtendTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SpoiledCorpseExtendTime' )
    }

    guardAttackAggroMob(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GuardAttackAggroMob' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'npc.properties' )
        cachedValues = {}
        return
    }

    mobAggroInPeaceZone(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'MobAggroInPeaceZone' )
    }

    raidCurse(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RaidCurse' )
    }

    randomEnchantEffect(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RandomEnchantEffect' )
    }

    showCrestWithoutQuest(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ShowCrestWithoutQuest' )
    }

    showNpcCustomTitle(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ShowNpcCustomTitle' )
    }

    showNpcCustomTitleTemplate(): string {
        return configuration[ 'ShowNpcCustomTitleTemplate' ]
    }

    showNpcCustomTitleAggressiveMark(): string {
        return configuration[ 'ShowNpcCustomTitleAggressiveMark' ]
    }

    useDeepBlueDropRules(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'UseDeepBlueDropRules' )
    }

    useDeepBlueDropRulesRaid(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'UseDeepBlueDropRulesRaid' )
    }

    getNpcHpMultiplierRatio(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'NpcHpMultiplierRatio' )
    }

    isNpcHpMultiplierEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'NpcHpMultiplierEnabled' )
    }
}

export const DotEnvNpcConfiguration: L2NPCConfigurationApi = new Configuration()