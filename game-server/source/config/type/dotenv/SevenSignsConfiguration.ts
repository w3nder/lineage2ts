import { L2SevenSignsConfigurationApi } from '../../interface/SevenSignsConfigurationApi'
import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2SevenSignsConfigurationApi {
    castleForDawn(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'CastleForDawn' )
    }

    castleForDusk(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'CastleForDusk' )
    }

    getDawnGatesMagicDefenceMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'DawnGatesMdefMult' )
    }

    getDawnGatesPowerDefenceMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'DawnGatesPdefMult' )
    }

    getDuskGatesMagicDefenceMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'DuskGatesMdefMult' )
    }

    getDuskGatesPowerDefenceMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'DuskGatesPdefMult' )
    }

    getFestivalChestSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FestivalChestSpawn' )
    }

    getFestivalCycleLength(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FestivalCycleLength' )
    }

    getFestivalFirstSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FestivalFirstSpawn' )
    }

    getFestivalFirstSwarm(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FestivalFirstSwarm' )
    }

    getFestivalLength(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FestivalLength' )
    }

    getFestivalManagerStart(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FestivalManagerStart' )
    }

    getFestivalMinPlayer(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FestivalMinPlayer' )
    }

    getFestivalSecondSpawn(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FestivalSecondSpawn' )
    }

    getFestivalSecondSwarm(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FestivalSecondSwarm' )
    }

    getMaxPlayerContribution(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxPlayerContrib' )
    }

    getSevenSignsDawnTicketBundle(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SevenSignsDawnTicketBundle' )
    }

    getSevenSignsDawnTicketPrice(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SevenSignsDawnTicketPrice' )
    }

    getSevenSignsDawnTicketQuantity(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SevenSignsDawnTicketQuantity' )
    }

    getSevenSignsJoinDawnFee(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SevenSignsJoinDawnFee' )
    }

    getSevenSignsManorsAgreementId(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SevenSignsManorsAgreementId' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'sevensigns.properties' )
        cachedValues = {}
        return
    }

    requireClanCastle(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RequireClanCastle' )
    }

    strictSevenSigns(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'StrictSevenSigns' )
    }

    getBlueStoneAAReward(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BlueStoneAAReward' )
    }

    getBlueStoneContributionPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BlueStoneContributionPoints' )
    }

    getGreenStoneAAReward(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'GreenStoneAAReward' )
    }

    getGreenStoneContributionPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'GreenStoneContributionPoints' )
    }

    getRedStoneAAReward(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RedStoneAAReward' )
    }

    getRedStoneContributionPoints(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RedStoneContributionPoints' )
    }

    getCompetitionPeriodDuration(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CompetitionPeriodDuration' )
    }

    getInitialPeriodDuration(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'InitialPeriodDuration' )
    }

    getPeriodStartCron(): string {
        return configuration[ 'PeriodStartCron' ]
    }
}

export const DotEnvSevenSignsConfiguration: L2SevenSignsConfigurationApi = new Configuration()