import { L2PvPConfigurationApi } from '../../interface/PvPConfigurationApi'
import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2PvPConfigurationApi {
    awardPKKillPVPPoint(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AwardPKKillPVPPoint' )
    }

    canGMDropEquipment(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'CanGMDropEquipment' )
    }

    getMinimumPKRequiredToDrop(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinimumPKRequiredToDrop' )
    }

    getNonDroppableItems(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'NonDroppableItems' )
    }

    getPetItems(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'PetItems' )
    }

    getPvPVsNormalTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PvPVsNormalTime' )
    }

    getPvPVsPvPTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PvPVsPvPTime' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'pvp.properties' )
        cachedValues = {}
        return
    }
}

export const DotEnvPvPConfiguration: L2PvPConfigurationApi = new Configuration()