import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2ServerConfigurationApi } from '../../interface/ServerConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

function createIpAddress( value: string ): Array<number> {
    return value.split( '.' ).map( ( value: string ) => parseInt( value, 10 ) )
}

class Configuration extends L2ConfigurationSubscription implements L2ServerConfigurationApi {
    getLoginServerReconnectInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'LoginServerReconnectInterval' )
    }

    getPacketDebounceInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PacketDebounceInterval' )
    }

    getExternalServerIp(): Array<number> {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'ExternalServerIp', createIpAddress )
    }

    acceptAlternateId(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AcceptAlternateId' )
    }

    getAllowedProtocolRevisions(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'AllowedProtocolRevisions' )
    }

    getLoginHost(): string {
        return configuration[ 'LoginHost' ]
    }

    getLoginPort(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'LoginPort' )
    }

    getMaxOnlineUsers(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxOnlineUsers' )
    }

    getRequestServerId(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RequestServerId' )
    }

    getServerPort(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ServerPort' )
    }

    getServerListAge(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ServerListAge' )
    }

    getServerListBrackets(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ServerListBrackets' )
    }

    getServerListType(): string {
        return configuration[ 'ServerListType' ]
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'server.properties' )
        cachedValues = {}
        return
    }

    randomizeCharacterSelection(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RandomizeCharacterSelection' )
    }

    getCommandLinkEndpoint(): string {
        return configuration[ 'CommandLinkEndpoint' ]
    }

    getCommandLinkPermissions(): Set<string> {
        return DotEnvConfigurationEngineHelper.getStringSet( configuration, cachedValues, 'CommandLinkPermissions' )
    }

    isCommandLinkEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'CommandLinkEnabled' )
    }
}

export const DotEnvServerConfiguration: L2ServerConfigurationApi = new Configuration()