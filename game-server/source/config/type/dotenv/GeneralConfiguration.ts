import { L2GeneralConfigurationApi } from '../../interface/GeneralConfigurationApi'
import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2ConfigurationSubscription } from '../../IConfiguration'
import _ from 'lodash'
import { ItemInstanceType } from '../../../gameService/models/items/ItemInstanceType'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

function createFilterPatterns( value: string ): Array<RegExp> {
    return _.split( value, ',' ).map( ( word: string ) => {
        return new RegExp( word, 'i' )
    } )
}

function convertProtectedTypes( value: string ) : Set<ItemInstanceType> {
    let types = new Set<ItemInstanceType>()

    _.split( value, ',' ).forEach( ( chunk: string ) : void => {
        switch ( chunk ) {
            case 'armor':
                types.add( ItemInstanceType.L2Armor )
                break

            case 'etc':
                types.add( ItemInstanceType.L2EtcItem )
                break

            case 'weapon':
                types.add( ItemInstanceType.L2Weapon )
                break
        }
    } )

    return types
}

class Configuration extends L2ConfigurationSubscription implements L2GeneralConfigurationApi {
    getMailCodExpirationTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MailCodExpirationTime' )
    }

    getMailDefaultExpirationTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MailDefaultExpirationTime' )
    }

    getMailSystemName(): string {
        return configuration[ 'MailSystemName' ]
    }

    isClanNoticeTemplateEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ClanNoticeTemplateEnabled' )
    }
    getClanNoticeTemplatePath(): string {
        return configuration[ 'ClanNoticeTemplatePath' ]
    }
    getFishingDurationMultiplier() {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'FishingDurationMultiplier' )
    }

    getFishingExperienceMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'FishingExperienceMultiplier' )
    }

    isAllowFishingExperience(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowFishingExperience' )
    }

    getFishingChampionshipParticipantAmount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FishingChampionshipParticipantAmount' )
    }

    getFishingChampionshipRewardOthers(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FishingChampionshipRewardOthers' )
    }

    getFishingChampionshipIntervalCron(): string {
        return configuration[ 'FishingChampionshipIntervalCron' ]
    }

    getFishingChampionshipReward1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FishingChampionshipReward1' )
    }

    getFishingChampionshipReward2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FishingChampionshipReward2' )
    }

    getFishingChampionshipReward3(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FishingChampionshipReward3' )
    }

    getFishingChampionshipReward4(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FishingChampionshipReward4' )
    }

    getFishingChampionshipReward5(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FishingChampionshipReward5' )
    }

    getFishingChampionshipRewardItemId(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FishingChampionshipRewardItemId' )
    }

    allowFishingChampionship(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowFishingChampionship' )
    }

    getChatLinkedItemLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChatLinkedItemLimit' )
    }

    getChatTextLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ChatTextLimit' )
    }

    getLotteryPeriodDays(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'LotteryPeriod' )
    }

    allowAttachments(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowAttachments' )
    }

    allowBoat(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowBoat' )
    }

    allowCursedWeapons(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowCursedWeapons' )
    }

    allowDiscardItem(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowDiscardItem' )
    }

    allowFishing(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowFishing' )
    }

    allowLottery(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowLottery' )
    }

    allowMail(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowMail' )
    }

    allowManor(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowManor' )
    }

    allowPetWalkers(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowPetWalkers' )
    }

    allowRace(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowRace' )
    }

    allowRefund(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowRefund' )
    }

    allowRentPet(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowRentPet' )
    }

    allowReportsFromSameClanMembers(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowReportsFromSameClanMembers' )
    }

    allowSummonInInstance(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowSummonInInstance' )
    }

    allowWater(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowWater' )
    }

    allowWear(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AllowWear' )
    }

    autoDeleteInvalidQuestData(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'AutoDeleteInvalidQuestData' )
    }

    customBuyListLoad(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'CustomBuyListLoad' )
    }

    customItemsLoad(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'CustomItemsLoad' )
    }

    customMultisellLoad(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'CustomMultisellLoad' )
    }

    customNpcBufferTables(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'CustomNpcBufferTables' )
    }

    customNpcData(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'CustomNpcData' )
    }

    customSkillsLoad(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'CustomSkillsLoad' )
    }

    customSpawnlistTable(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'CustomSpawnlistTable' )
    }

    databaseCleanUp(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'DatabaseCleanUp' )
    }

    isSaveDroppedResetAfterLoad(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SaveDroppedResetAfterLoad' )
    }

    enableBlockCheckerEvent(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'EnableBlockCheckerEvent' )
    }

    enableBotReportButton(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'EnableBotReportButton' )
    }

    enableCommunityBoard(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'EnableCommunityBoard' )
    }

    enableFallingDamage(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'EnableFallingDamage' )
    }

    everybodyHasAdminRights(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'EverybodyHasAdminRights' )
    }

    forceInventoryUpdate(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ForceInventoryUpdate' )
    }

    gameGuardEnforce(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GameGuardEnforce' )
    }

    gameGuardProhibitAction(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GameGuardProhibitAction' )
    }

    getAutoJumpsDelayMax(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AutoJumpsDelayMax', 1000 )
    }

    getAutoJumpsDelayMin(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AutoJumpsDelayMin', 1000 )
    }

    getBBSDefault(): string {
        return configuration[ 'BBSDefault' ]
    }

    getBanChatChannels(): Set<number> {
        return DotEnvConfigurationEngineHelper.getNumberSet( configuration, cachedValues, 'getBanChatChannels' )
    }

    getBirthdayGiftDelivery(): string {
        return configuration[ 'BirthdayGiftDelivery' ]
    }

    getBirthdayGifts(): Array<number> {
        return DotEnvConfigurationEngineHelper.getNumberArray( configuration, cachedValues, 'BirthdayGifts' )
    }

    getBirthdayMailSubject(): string {
        return configuration[ 'BirthdayMailSubject' ]
    }

    getBirthdayMailText(): string {
        return configuration[ 'BirthdayMailText' ]
    }

    getBlockCheckerMinTeamMembers(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BlockCheckerMinTeamMembers' )
    }

    getBoatBroadcastRadius(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BoatBroadcastRadius' )
    }

    getBossRoomTimeMultiply(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BossRoomTimeMultiply' )
    }

    getBotReportDelay(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'BotReportDelay', 60000 )
    }

    getBotReportPointsResetHour(): string {
        return configuration[ 'BotReportPointsResetHour' ]
    }

    getCaptainCost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CaptainCost' )
    }

    getChatFilters(): Array<RegExp> {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'ChatFilter', createFilterPatterns )
    }

    getChatFilterCharacters(): string {
        return configuration[ 'ChatFilterChars' ]
    }

    getCommanderCost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CommanderCost' )
    }

    getDefaultPunishment(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DefaultPunish' )
    }

    getDefaultPunishmentInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DefaultPunishParam', 1000 )
    }

    getEjectDeadPlayerTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'EjectDeadPlayerTime', 1000 )
    }

    getGlobalChat(): string {
        return configuration[ 'GlobalChat' ]
    }

    getHeroCost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HeroCost' )
    }

    getInstanceFinishTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'InstanceFinishTime' )
    }

    getItemAuctionTimeExtendsOnBid(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ItemAuctionTimeExtendsOnBid', 1000 )
    }

    getPlayerObjectScanInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PlayerObjectScanInterval' )
    }

    getLottery2and1NumberPrize(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'Lottery2and1NumberPrize' )
    }

    getLottery3NumberRate(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'Lottery3NumberRate' )
    }

    getLottery4NumberRate(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'Lottery4NumberRate' )
    }

    getLottery5NumberRate(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'Lottery5NumberRate' )
    }

    getLotteryPrize(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'LotteryPrize' )
    }

    getLotteryTicketPrice(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'LotteryTicketPrice' )
    }

    getManorApproveMin(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ManorApproveMin' )
    }

    getManorApproveTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ManorApproveTime' )
    }

    getManorMaintenanceMin(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ManorMaintenanceMin' )
    }

    getManorRefreshMin(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ManorRefreshMin' )
    }

    getManorRefreshTime(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ManorRefreshTime' )
    }

    getManorSavePeriodRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ManorSavePeriodRate' )
    }

    getMaxMonsterAnimation(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxMonsterAnimation' )
    }

    getMaxNPCAnimation(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxNPCAnimation' )
    }

    getMaxRiftJumps(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxRiftJumps' )
    }

    getMinMonsterAnimation(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinMonsterAnimation' )
    }

    getMinNPCAnimation(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinNPCAnimation' )
    }

    getNormalEnchantCostMultipiler(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'NormalEnchantCostMultipiler' )
    }

    getNumberOfNecessaryPartyMembers(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'NumberOfNecessaryPartyMembers' )
    }

    getOfficerCost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'OfficerCost' )
    }

    getPeaceZoneMode(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PeaceZoneMode' )
    }

    getProtectedItems(): Set<number> {
        return DotEnvConfigurationEngineHelper.getNumberSet( configuration, cachedValues, 'ProtectedItems' )
    }

    getRecruitCost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RecruitCost' )
    }

    getRiftMinPartySize(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RiftMinPartySize' )
    }

    getRiftSpawnDelay(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RiftSpawnDelay' )
    }

    getSafeEnchantCostMultipiler(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SafeEnchantCostMultipiler' )
    }

    getSaveDroppedItemInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SaveDroppedItemInterval', 60000 )
    }

    getSoldierCost(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SoldierCost' )
    }

    getTimeOfAttack(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TimeOfAttack', 60000 )
    }

    getTimeOfCoolDown(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TimeOfCoolDown', 60000 )
    }

    getTimeOfEntry(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TimeOfEntry' )
    }

    getTimeOfWarmUp(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TimeOfWarmUp' )
    }

    getTradeChat(): string {
        return configuration[ 'TradeChat' ]
    }

    getWearDelay(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'WearDelay', 1000 )
    }

    getWearPrice(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'WearPrice' )
    }

    gmAudit(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMAudit' )
    }

    gmGiveSpecialAuraSkills(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMGiveSpecialAuraSkills' )
    }

    gmGiveSpecialSkills(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMGiveSpecialSkills' )
    }

    gmHeroAura(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMHeroAura' )
    }

    gmItemRestriction(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMItemRestriction' )
    }

    gmRestartFighting(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMRestartFighting' )
    }

    gmShowAnnouncerName(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMShowAnnouncerName' )
    }

    gmShowCritAnnouncerName(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMShowCritAnnouncerName' )
    }

    gmSkillRestriction(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMSkillRestriction' )
    }

    gmStartupAutoList(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMStartupAutoList' )
    }

    gmStartupDietMode(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMStartupDietMode' )
    }

    gmStartupInvisible(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMStartupInvisible' )
    }

    gmStartupInvulnerable(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMStartupInvulnerable' )
    }

    gmStartupSilence(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMStartupSilence' )
    }

    gmTradeRestrictedItems(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'GMTradeRestrictedItems' )
    }

    hellboundWithoutQuest(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'HellboundWithoutQuest' )
    }

    instanceDebug(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'InstanceDebug' )
    }

    isHBCEFairPlay(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'HBCEFairPlay' )
    }

    itemAuctionEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ItemAuctionEnabled' )
    }

    jailDisableChat(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'JailDisableChat' )
    }

    jailDisableTransaction(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'JailDisableTransaction' )
    }

    jailIsPvp(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'JailIsPvp' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'general.properties' )
        cachedValues = {}
        return
    }

    logAutoAnnouncements(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'LogAutoAnnouncements' )
    }

    logChat(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'LogChat' )
    }

    logItemEnchants(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'LogItemEnchants' )
    }

    logItems(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'LogItems' )
    }

    logItemsSmallLog(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'LogItemsSmallLog' )
    }

    logSkillEnchants(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'LogSkillEnchants' )
    }

    manorSaveAllActions(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ManorSaveAllActions' )
    }

    multipleItemDrop(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'MultipleItemDrop' )
    }

    onlyGMItemsFree(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'OnlyGMItemsFree' )
    }

    preciseDropCalculation(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'PreciseDropCalculation' )
    }

    restorePlayerInstance(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'RestorePlayerInstance' )
    }

    saveDroppedItem(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SaveDroppedItem' )
    }

    getSaveDroppedProtectedTypes(): Set<ItemInstanceType> {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'SaveDroppedProtectedTypes', convertProtectedTypes )
    }

    getSaveDroppedExpiration(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SaveDroppedExpiration', 60000 )
    }

    saveGmSpawnOnCustom(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SaveGmSpawnOnCustom' )
    }

    serverGMOnly(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ServerGMOnly' )
    }

    isServerNewsEnabled(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ShowServerNews' )
    }

    getServerNewsPath() : string {
        return configuration[ 'ServerNewsPath' ]
    }

    skillCheckEnable(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SkillCheckEnable' )
    }

    skillCheckGM(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SkillCheckGM' )
    }

    skillCheckRemove(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SkillCheckRemove' )
    }

    useChatFilter(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'UseChatFilter' )
    }

    getItemAuctionDeliveryMethod(): string {
        return configuration[ 'ItemAuctionDeliveryMethod' ]
    }

    getItemAuctionDelaySecondsBeforeStart(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ItemAuctionDelaySecondsBeforeStart' )
    }

    getItemAuctionStartCron(): string {
        return configuration[ 'ItemAuctionStartCron' ]
    }
}

export const DotEnvGeneralConfiguration: L2GeneralConfigurationApi = new Configuration()