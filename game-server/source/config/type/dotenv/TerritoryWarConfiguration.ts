import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2TerritoryWarConfigurationApi } from '../../interface/TerritoryWarConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2TerritoryWarConfigurationApi {
    getClanMinLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ClanMinLevel' )
    }

    getDefenderMaxClans(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DefenderMaxClans' )
    }

    getDefenderMaxPlayers(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DefenderMaxPlayers' )
    }

    getMinTerritoryBadgeForBigStrider(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinTerritoryBadgeForBigStrider' )
    }

    getMinTerritoryBadgeForNobless(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinTerritoryBadgeForNobless' )
    }

    getMinTerritoryBadgeForStriders(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinTerritoryBadgeForStriders' )
    }

    getPlayerMinLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PlayerMinLevel' )
    }

    getWarLength(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'WarLength' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'territorywar.properties' )
        cachedValues = {}
        return
    }

    playerWithWardCanBeKilledInPeaceZone(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'PlayerWithWardCanBeKilledInPeaceZone' )
    }

    returnWardsWhenTWStarts(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ReturnWardsWhenTWStarts' )
    }

    spawnWardsWhenTWIsNotInProgress(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SpawnWardsWhenTWIsNotInProgress' )
    }
}

export const DotEnvTerritoryWarConfiguration: L2TerritoryWarConfigurationApi = new Configuration()