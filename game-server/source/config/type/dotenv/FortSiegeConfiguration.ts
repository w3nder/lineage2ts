import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2FortSiegeConfigurationApi } from '../../interface/FortSiegeConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2FortSiegeConfigurationApi {
    getAaruCommander1(): string {
        return configuration[ 'AaruCommander1' ]
    }

    getAaruCommander2(): string {
        return configuration[ 'AaruCommander2' ]
    }

    getAaruCommander3(): string {
        return configuration[ 'AaruCommander3' ]
    }

    getAaruFlag1(): string {
        return configuration[ 'AaruFlag1' ]
    }

    getAaruFlag2(): string {
        return configuration[ 'AaruFlag2' ]
    }

    getAaruFlag3(): string {
        return configuration[ 'AaruFlag3' ]
    }

    getAntharasCommander1(): string {
        return configuration[ 'AntharasCommander1' ]
    }

    getAntharasCommander2(): string {
        return configuration[ 'AntharasCommander2' ]
    }

    getAntharasCommander3(): string {
        return configuration[ 'AntharasCommander3' ]
    }

    getAntharasCommander4(): string {
        return configuration[ 'AntharasCommander4' ]
    }

    getAntharasFlag1(): string {
        return configuration[ 'AntharasFlag1' ]
    }

    getAntharasFlag2(): string {
        return configuration[ 'AntharasFlag2' ]
    }

    getAntharasFlag3(): string {
        return configuration[ 'AntharasFlag3' ]
    }

    getArchaicCommander1(): string {
        return configuration[ 'ArchaicCommander1' ]
    }

    getArchaicCommander2(): string {
        return configuration[ 'ArchaicCommander2' ]
    }

    getArchaicCommander3(): string {
        return configuration[ 'ArchaicCommander3' ]
    }

    getArchaicFlag1(): string {
        return configuration[ 'ArchaicFlag1' ]
    }

    getArchaicFlag2(): string {
        return configuration[ 'ArchaicFlag2' ]
    }

    getArchaicFlag3(): string {
        return configuration[ 'ArchaicFlag3' ]
    }

    getAttackerMaxClans(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AttackerMaxClans' )
    }

    getBayouCommander1(): string {
        return configuration[ 'BayouCommander1' ]
    }

    getBayouCommander2(): string {
        return configuration[ 'BayouCommander2' ]
    }

    getBayouCommander3(): string {
        return configuration[ 'BayouCommander3' ]
    }

    getBayouCommander4(): string {
        return configuration[ 'BayouCommander4' ]
    }

    getBayouFlag1(): string {
        return configuration[ 'BayouFlag1' ]
    }

    getBayouFlag2(): string {
        return configuration[ 'BayouFlag2' ]
    }

    getBayouFlag3(): string {
        return configuration[ 'BayouFlag3' ]
    }

    getBorderlandCommander1(): string {
        return configuration[ 'BorderlandCommander1' ]
    }

    getBorderlandCommander2(): string {
        return configuration[ 'BorderlandCommander2' ]
    }

    getBorderlandCommander3(): string {
        return configuration[ 'BorderlandCommander3' ]
    }

    getBorderlandCommander4(): string {
        return configuration[ 'BorderlandCommander4' ]
    }

    getBorderlandFlag1(): string {
        return configuration[ 'BorderlandFlag1' ]
    }

    getBorderlandFlag2(): string {
        return configuration[ 'BorderlandFlag2' ]
    }

    getBorderlandFlag3(): string {
        return configuration[ 'BorderlandFlag3' ]
    }

    getCloudMountainCommander1(): string {
        return configuration[ 'CloudMountainCommander1' ]
    }

    getCloudMountainCommander2(): string {
        return configuration[ 'CloudMountainCommander2' ]
    }

    getCloudMountainCommander3(): string {
        return configuration[ 'CloudMountainCommander3' ]
    }

    getCloudMountainCommander4(): string {
        return configuration[ 'CloudMountainCommander4' ]
    }

    getCloudMountainFlag1(): string {
        return configuration[ 'CloudMountainFlag1' ]
    }

    getCloudMountainFlag2(): string {
        return configuration[ 'CloudMountainFlag2' ]
    }

    getCloudMountainFlag3(): string {
        return configuration[ 'CloudMountainFlag3' ]
    }

    getCountDownLength(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CountDownLength' )
    }

    getDemonCommander1(): string {
        return configuration[ 'DemonCommander1' ]
    }

    getDemonCommander2(): string {
        return configuration[ 'DemonCommander2' ]
    }

    getDemonCommander3(): string {
        return configuration[ 'DemonCommander3' ]
    }

    getDemonFlag1(): string {
        return configuration[ 'DemonFlag1' ]
    }

    getDemonFlag2(): string {
        return configuration[ 'DemonFlag2' ]
    }

    getDemonFlag3(): string {
        return configuration[ 'DemonFlag3' ]
    }

    getDragonspineCommander1(): string {
        return configuration[ 'DragonspineCommander1' ]
    }

    getDragonspineCommander2(): string {
        return configuration[ 'DragonspineCommander2' ]
    }

    getDragonspineCommander3(): string {
        return configuration[ 'DragonspineCommander3' ]
    }

    getDragonspineFlag1(): string {
        return configuration[ 'DragonspineFlag1' ]
    }

    getDragonspineFlag2(): string {
        return configuration[ 'DragonspineFlag2' ]
    }

    getDragonspineFlag3(): string {
        return configuration[ 'DragonspineFlag3' ]
    }

    getFloranCommander1(): string {
        return configuration[ 'FloranCommander1' ]
    }

    getFloranCommander2(): string {
        return configuration[ 'FloranCommander2' ]
    }

    getFloranCommander3(): string {
        return configuration[ 'FloranCommander3' ]
    }

    getFloranCommander4(): string {
        return configuration[ 'FloranCommander4' ]
    }

    getFloranFlag1(): string {
        return configuration[ 'FloranFlag1' ]
    }

    getFloranFlag2(): string {
        return configuration[ 'FloranFlag2' ]
    }

    getFloranFlag3(): string {
        return configuration[ 'FloranFlag3' ]
    }

    getHiveCommander1(): string {
        return configuration[ 'HiveCommander1' ]
    }

    getHiveCommander2(): string {
        return configuration[ 'HiveCommander2' ]
    }

    getHiveCommander3(): string {
        return configuration[ 'HiveCommander3' ]
    }

    getHiveFlag1(): string {
        return configuration[ 'HiveFlag1' ]
    }

    getHiveFlag2(): string {
        return configuration[ 'HiveFlag2' ]
    }

    getHiveFlag3(): string {
        return configuration[ 'HiveFlag3' ]
    }

    getHuntersCommander1(): string {
        return configuration[ 'HuntersCommander1' ]
    }

    getHuntersCommander2(): string {
        return configuration[ 'HuntersCommander2' ]
    }

    getHuntersCommander3(): string {
        return configuration[ 'HuntersCommander3' ]
    }

    getHuntersCommander4(): string {
        return configuration[ 'HuntersCommander4' ]
    }

    getHuntersFlag1(): string {
        return configuration[ 'HuntersFlag1' ]
    }

    getHuntersFlag2(): string {
        return configuration[ 'HuntersFlag2' ]
    }

    getHuntersFlag3(): string {
        return configuration[ 'HuntersFlag3' ]
    }

    getIvoryCommander1(): string {
        return configuration[ 'IvoryCommander1' ]
    }

    getIvoryCommander2(): string {
        return configuration[ 'IvoryCommander2' ]
    }

    getIvoryCommander3(): string {
        return configuration[ 'IvoryCommander3' ]
    }

    getIvoryFlag1(): string {
        return configuration[ 'IvoryFlag1' ]
    }

    getIvoryFlag2(): string {
        return configuration[ 'IvoryFlag2' ]
    }

    getIvoryFlag3(): string {
        return configuration[ 'IvoryFlag3' ]
    }

    getMaxFlags(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxFlags' )
    }

    getMonasticCommander1(): string {
        return configuration[ 'MonasticCommander1' ]
    }

    getMonasticCommander2(): string {
        return configuration[ 'MonasticCommander2' ]
    }

    getMonasticCommander3(): string {
        return configuration[ 'MonasticCommander3' ]
    }

    getMonasticFlag1(): string {
        return configuration[ 'MonasticFlag1' ]
    }

    getMonasticFlag2(): string {
        return configuration[ 'MonasticFlag2' ]
    }

    getMonasticFlag3(): string {
        return configuration[ 'MonasticFlag3' ]
    }

    getNarsellCommander1(): string {
        return configuration[ 'NarsellCommander1' ]
    }

    getNarsellCommander2(): string {
        return configuration[ 'NarsellCommander2' ]
    }

    getNarsellCommander3(): string {
        return configuration[ 'NarsellCommander3' ]
    }

    getNarsellFlag1(): string {
        return configuration[ 'NarsellFlag1' ]
    }

    getNarsellFlag2(): string {
        return configuration[ 'NarsellFlag2' ]
    }

    getNarsellFlag3(): string {
        return configuration[ 'NarsellFlag3' ]
    }

    getShantyCommander1(): string {
        return configuration[ 'ShantyCommander1' ]
    }

    getShantyCommander2(): string {
        return configuration[ 'ShantyCommander2' ]
    }

    getShantyCommander3(): string {
        return configuration[ 'ShantyCommander3' ]
    }

    getShantyFlag1(): string {
        return configuration[ 'ShantyFlag1' ]
    }

    getShantyFlag2(): string {
        return configuration[ 'ShantyFlag2' ]
    }

    getShantyFlag3(): string {
        return configuration[ 'ShantyFlag3' ]
    }

    getSiegeClanMinLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SiegeClanMinLevel' )
    }

    getSiegeLength(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SiegeLength' )
    }

    getSouthernCommander1(): string {
        return configuration[ 'SouthernCommander1' ]
    }

    getSouthernCommander2(): string {
        return configuration[ 'SouthernCommander2' ]
    }

    getSouthernCommander3(): string {
        return configuration[ 'SouthernCommander3' ]
    }

    getSouthernCommander4(): string {
        return configuration[ 'SouthernCommander4' ]
    }

    getSouthernFlag1(): string {
        return configuration[ 'SouthernFlag1' ]
    }

    getSouthernFlag2(): string {
        return configuration[ 'SouthernFlag2' ]
    }

    getSouthernFlag3(): string {
        return configuration[ 'SouthernFlag3' ]
    }

    getSuspiciousMerchantRespawnDelay(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SuspiciousMerchantRespawnDelay' )
    }

    getSwampCommander1(): string {
        return configuration[ 'SwampCommander1' ]
    }

    getSwampCommander2(): string {
        return configuration[ 'SwampCommander2' ]
    }

    getSwampCommander3(): string {
        return configuration[ 'SwampCommander3' ]
    }

    getSwampCommander4(): string {
        return configuration[ 'SwampCommander4' ]
    }

    getSwampFlag1(): string {
        return configuration[ 'SwampFlag1' ]
    }

    getSwampFlag2(): string {
        return configuration[ 'SwampFlag2' ]
    }

    getSwampFlag3(): string {
        return configuration[ 'SwampFlag3' ]
    }

    getTanorCommander1(): string {
        return configuration[ 'TanorCommander1' ]
    }

    getTanorCommander2(): string {
        return configuration[ 'TanorCommander2' ]
    }

    getTanorCommander3(): string {
        return configuration[ 'TanorCommander3' ]
    }

    getTanorFlag1(): string {
        return configuration[ 'TanorFlag1' ]
    }

    getTanorFlag2(): string {
        return configuration[ 'TanorFlag2' ]
    }

    getTanorFlag3(): string {
        return configuration[ 'TanorFlag3' ]
    }

    getValleyCommander1(): string {
        return configuration[ 'ValleyCommander1' ]
    }

    getValleyCommander2(): string {
        return configuration[ 'ValleyCommander2' ]
    }

    getValleyCommander3(): string {
        return configuration[ 'ValleyCommander3' ]
    }

    getValleyCommander4(): string {
        return configuration[ 'ValleyCommander4' ]
    }

    getValleyFlag1(): string {
        return configuration[ 'ValleyFlag1' ]
    }

    getValleyFlag2(): string {
        return configuration[ 'ValleyFlag2' ]
    }

    getValleyFlag3(): string {
        return configuration[ 'ValleyFlag3' ]
    }

    getWesternCommander1(): string {
        return configuration[ 'WesternCommander1' ]
    }

    getWesternCommander2(): string {
        return configuration[ 'WesternCommander2' ]
    }

    getWesternCommander3(): string {
        return configuration[ 'WesternCommander3' ]
    }

    getWesternCommander4(): string {
        return configuration[ 'WesternCommander4' ]
    }

    getWesternFlag1(): string {
        return configuration[ 'WesternFlag1' ]
    }

    getWesternFlag2(): string {
        return configuration[ 'WesternFlag2' ]
    }

    getWesternFlag3(): string {
        return configuration[ 'WesternFlag3' ]
    }

    getWhiteSandsCommander1(): string {
        return configuration[ 'WhiteSandsCommander1' ]
    }

    getWhiteSandsCommander2(): string {
        return configuration[ 'WhiteSandsCommander2' ]
    }

    getWhiteSandsCommander3(): string {
        return configuration[ 'WhiteSandsCommander3' ]
    }

    getWhiteSandsFlag1(): string {
        return configuration[ 'WhiteSandsFlag1' ]
    }

    getWhiteSandsFlag2(): string {
        return configuration[ 'WhiteSandsFlag2' ]
    }

    getWhiteSandsFlag3(): string {
        return configuration[ 'WhiteSandsFlag3' ]
    }

    justToTerritory(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'JustToTerritory' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'fortsiege.properties' )
        cachedValues = {}
        return
    }
}

export const DotEnvFortSiegeConfiguration: L2FortSiegeConfigurationApi = new Configuration()