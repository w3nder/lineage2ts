import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2ClanHallConfigurationApi } from '../../interface/ClanHallConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

class Configuration extends L2ConfigurationSubscription implements L2ClanHallConfigurationApi {
    enableFame(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'EnableFame' )
    }

    getCurtainFunctionFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CurtainFunctionFeeLvl1' )
    }

    getCurtainFunctionFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CurtainFunctionFeeLvl2' )
    }

    getCurtainFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CurtainFunctionFeeRatio' )
    }

    getExpRegenerationFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFeeLvl1' )
    }

    getExpRegenerationFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFeeLvl2' )
    }

    getExpRegenerationFeeLvl3(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFeeLvl3' )
    }

    getExpRegenerationFeeLvl4(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFeeLvl4' )
    }

    getExpRegenerationFeeLvl5(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFeeLvl5' )
    }

    getExpRegenerationFeeLvl6(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFeeLvl6' )
    }

    getExpRegenerationFeeLvl7(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFeeLvl7' )
    }

    getExpRegenerationFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ExpRegenerationFunctionFeeRatio' )
    }

    getFameAmount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FameAmount' )
    }

    getFameFrequency(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FameFrequency' )
    }

    getFrontPlatformFunctionFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FrontPlatformFunctionFeeLvl1' )
    }

    getFrontPlatformFunctionFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FrontPlatformFunctionFeeLvl2' )
    }

    getFrontPlatformFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'FrontPlatformFunctionFeeRatio' )
    }

    getHpRegenerationFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl1' )
    }

    getHpRegenerationFeeLvl10(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl10' )
    }

    getHpRegenerationFeeLvl11(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl11' )
    }

    getHpRegenerationFeeLvl12(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl12' )
    }

    getHpRegenerationFeeLvl13(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl13' )
    }

    getHpRegenerationFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl2' )
    }

    getHpRegenerationFeeLvl3(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl3' )
    }

    getHpRegenerationFeeLvl4(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl4' )
    }

    getHpRegenerationFeeLvl5(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl5' )
    }

    getHpRegenerationFeeLvl6(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl6' )
    }

    getHpRegenerationFeeLvl7(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl7' )
    }

    getHpRegenerationFeeLvl8(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl8' )
    }

    getHpRegenerationFeeLvl9(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFeeLvl9' )
    }

    getHpRegenerationFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'HpRegenerationFunctionFeeRatio' )
    }

    getItemCreationFunctionFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ItemCreationFunctionFeeLvl1' )
    }

    getItemCreationFunctionFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ItemCreationFunctionFeeLvl2' )
    }

    getItemCreationFunctionFeeLvl3(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ItemCreationFunctionFeeLvl3' )
    }

    getItemCreationFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ItemCreationFunctionFeeRatio' )
    }

    getMaxAttackers(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxAttackers' )
    }

    getMaxFlagsPerClan(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MaxFlagsPerClan' )
    }

    getMinClanLevel(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MinClanLevel' )
    }

    getMpRegenerationFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFeeLvl1' )
    }

    getMpRegenerationFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFeeLvl2' )
    }

    getMpRegenerationFeeLvl3(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFeeLvl3' )
    }

    getMpRegenerationFeeLvl4(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFeeLvl4' )
    }

    getMpRegenerationFeeLvl5(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFeeLvl5' )
    }

    getMpRegenerationFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MpRegenerationFunctionFeeRatio' )
    }

    getSupportFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl1' )
    }

    getSupportFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl2' )
    }

    getSupportFeeLvl3(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl3' )
    }

    getSupportFeeLvl4(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl4' )
    }

    getSupportFeeLvl5(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl5' )
    }

    getSupportFeeLvl6(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl6' )
    }

    getSupportFeeLvl7(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl7' )
    }

    getSupportFeeLvl8(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFeeLvl8' )
    }

    getSupportFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SupportFunctionFeeRatio' )
    }

    getTeleportFunctionFeeLvl1(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TeleportFunctionFeeLvl1' )
    }

    getTeleportFunctionFeeLvl2(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TeleportFunctionFeeLvl2' )
    }

    getTeleportFunctionFeeRatio(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'TeleportFunctionFeeRatio' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'clanhall.properties' )
        cachedValues = {}
        return
    }

    mpBuffFree(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'MpBuffFree' )
    }

    getAuctionTransactionTax(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'AuctionTransactionTax' )
    }
}

export const DotEnvClanHallConfiguration: L2ClanHallConfigurationApi = new Configuration()