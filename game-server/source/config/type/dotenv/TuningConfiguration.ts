import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2TuningConfigurationApi } from '../../interface/TuningConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'
import { parseInt } from 'lodash'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

function extractItemInventoryLimits( value: string ) : Record<number, number> {
    return value.split( ';' ).reduce( ( totalLimits: Record<number, number>, valuePair: string ) : Record<number, number> => {
        if ( valuePair ) {
            let parsedValues = valuePair.split( '-' ).map( value => parseInt( value, 10 ) )
            if ( Number.isInteger( parsedValues[ 0 ] ) && Number.isInteger( parsedValues[ 1 ] ) ) {
                totalLimits[ parsedValues[ 0 ] ] = parsedValues[ 1 ]
            }
        }

        return totalLimits
    }, {} )
}

class Configuration extends L2ConfigurationSubscription implements L2TuningConfigurationApi {
    getShadowItemManaConsumeAmount(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ShadowItemManaConsumeAmount' )
    }

    getShadowItemManaConsumeInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ShadowItemManaConsumeInterval' )
    }

    isPetShowingEarnedExp(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'PetShowingEarnedExp' )
    }

    getPetControlLevelDifference(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PetControlLevelDifference' )
    }

    getAutoDestroyInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AutoDestroyInterval' )
    }

    getAutoDestroyHerbMillis(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AutoDestroyHerbMillis' )
    }

    getAutoDestroyAdenaMillis(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AutoDestroyAdenaMillis' )
    }

    getAutoDestroyArmorMillis(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AutoDestroyArmorMillis' )
    }

    getAutoDestroyWeaponMillis(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AutoDestroyWeaponMillis' )
    }

    getAutoDestroyRecipeMillis(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AutoDestroyRecipeMillis' )
    }

    getAutoDestroyOtherMillis(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AutoDestroyOtherMillis' )
    }

    getMailInboxSize(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MailInboxSize' )
    }

    getMailMessageAttachmentsLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MailMessageAttachmentsAmount' )
    }

    getMailOutboxSize(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MailOutboxSize' )
    }

    getMailMessageAttachmentFee(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MailMessageAttachmentFee' )
    }

    getMailMessageFee(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'MailMessageFee' )
    }

    getLevelupRestoreCP(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'LevelupRestoreCP' )
    }

    getLevelupRestoreHP(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'LevelupRestoreHP' )
    }

    getLevelupRestoreMP(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'LevelupRestoreMP' )
    }

    getWondrousCubicResetCron(): string {
        return configuration[ 'WondrousCubicResetCron' ]
    }

    getFullArmorHPBonusMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'FullArmorHPBonusMultiplier' )
    }

    getReducedCostTeleportLevelLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'ReducedCostTeleportLevelLimit' )
    }

    getNewbieTeleportLevelLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'NewbieTeleportLevelLimit' )
    }

    getDropProtectionDuration(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'DropProtectionDuration' )
    }

    getAirshipFuelConsumptionRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'AirshipFuelConsumptionRate' )
    }

    getCrystallizeCountMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'CrystallizeCountMultiplier' )
    }

    getWarehouseItemDepositFee(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'WarehouseItemDepositFee' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'tuning.properties' )
        cachedValues = {}
        return
    }

    isMailToSelfPermitted(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'MailToSelfPermitted' )
    }

    getSowingLevelsIncrement(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'SowingLevelsIncrement' )
    }

    isSowingUsingLeveledSeed(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'SowingUsingLeveledSeed' )
    }

    isManufactureRemoveRecipeIngredients(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'ManufactureRemoveRecipeIngredients' )
    }

    getWarehouseCacheIntervalMinutes(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'WarehouseCacheIntervalMinutes' )
    }

    getPetFeedInterval(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PetFeedInterval' )
    }

    getItemInventoryLimits(): Record<number, number> {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'ItemInventoryLimits', extractItemInventoryLimits )
    }

    getBuylistProductRestockMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'BuylistProductRestockMultiplier' )
    }
}

export const DotEnvTuningConfiguration: L2TuningConfigurationApi = new Configuration()