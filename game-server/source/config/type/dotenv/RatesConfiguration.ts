import { DotEnvConfigurationEngineHelper } from '../../engine/dotenv'
import { L2RatesConfigurationApi } from '../../interface/RatesConfigurationApi'
import { L2ConfigurationSubscription } from '../../IConfiguration'

let configuration: { [ name: string ]: string }
let cachedValues: { [ name: string ]: any }

function getPositiveFloatValue( value: string ) : number {
    return Math.abs( parseFloat( value ) )
}

class Configuration extends L2ConfigurationSubscription implements L2RatesConfigurationApi {
    getExtractableScrollMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'ExtractableScrollMultiplier', getPositiveFloatValue )
    }

    getExtractablePotionMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'ExtractablePotionMultiplier', getPositiveFloatValue )
    }

    getExtractableRecipeMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'ExtractableRecipeMultiplier', getPositiveFloatValue )
    }

    getExtractableArmorMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'ExtractableArmorMultiplier', getPositiveFloatValue )
    }

    getExtractableWeaponMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getCustomValue( configuration, cachedValues, 'ExtractableWeaponMultiplier', getPositiveFloatValue )
    }

    getPartyPlayerSpBonuses(): Array<number> {
        return DotEnvConfigurationEngineHelper.getFloatArray( configuration, cachedValues, 'PartyPlayerSpBonuses' )
    }

    getPartyPlayerExpBonuses(): Array<number> {
        return DotEnvConfigurationEngineHelper.getFloatArray( configuration, cachedValues, 'PartyPlayerExpBonuses' )
    }

    isQuestExpUsePlayerBonusModifier(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'QuestExpUsePlayerBonusModifier' )
    }

    isQuestSpUsePlayerBonusModifier(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'QuestSpUsePlayerBonusModifier' )
    }

    getRateQuestDropChance(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateQuestDropChance' )
    }

    getRateQuestDropMaximum(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateQuestDropMaximum' )
    }

    getRateQuestDropMinimum(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateQuestDropMinimum' )
    }

    getDropEtcChanceMaxMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'DropEtcChanceMaxMultiplier' )
    }

    getDropEtcChanceMinMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'DropEtcChanceMinMultiplier' )
    }

    getEtcDropAmountMaxMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'EtcDropAmountMax' )
    }

    getEtcDropAmountMinMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'EtcDropAmountMin' )
    }

    getCorpseDropAmountMinMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'CorpseDropAmountMin' )
    }

    getCorpseDropAmountMaxMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'CorpseDropAmountMax' )
    }

    getCorpseDropChanceMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'CorpseDropChanceMultiplier' )
    }

    getDeathDropAmountMinMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'DeathDropAmountMin' )
    }

    getDeathDropAmountMaxMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'DeathDropAmountMax' )
    }

    getDeathDropChanceMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'DeathDropChanceMultiplier' )
    }

    getDropAmountMultiplierByItemId(): { [ key: number ]: number } {
        return DotEnvConfigurationEngineHelper.getFloatMap( configuration, cachedValues, 'DropAmountMultiplierByItemId' )
    }

    getDropChanceMultiplierByItemId(): { [ key: number ]: number } {
        return DotEnvConfigurationEngineHelper.getFloatMap( configuration, cachedValues, 'DropChanceMultiplierByItemId' )
    }

    getHerbDropAmountMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'HerbDropAmountMultiplier' )
    }

    getHerbDropChanceMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'HerbDropChanceMultiplier' )
    }

    getKarmaDropLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'KarmaDropLimit' )
    }

    getKarmaRateDrop(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'KarmaRateDrop' )
    }

    getKarmaRateDropEquipment(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'KarmaRateDropEquip' )
    }

    getKarmaRateDropEquippedWeapon(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'KarmaRateDropEquipWeapon' )
    }

    getKarmaRateDropItem(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'KarmaRateDropItem' )
    }

    getPetFoodRate(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PetFoodRate' )
    }

    getPetXpRate(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'PetXpRate' )
    }

    getPlayerDropLimit(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PlayerDropLimit' )
    }

    getPlayerRateDrop(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PlayerRateDrop' )
    }

    getPlayerRateDropEquipment(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PlayerRateDropEquip' )
    }

    getPlayerRateDropEquippedWeapon(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PlayerRateDropEquipWeapon' )
    }

    getPlayerRateDropItem(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'PlayerRateDropItem' )
    }

    getRaidDropAmountMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RaidDropAmountMultiplier' )
    }

    getRaidDropChanceMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RaidDropChanceMultiplier' )
    }

    getRateDropManor(): number {
        return DotEnvConfigurationEngineHelper.getNumber( configuration, cachedValues, 'RateDropManor' )
    }

    getRateExtractable(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateExtractable' )
    }

    getHellboundPointsDecreaseMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'HellboundPointsDecreaseMultiplier' )
    }

    getHellboundPointsIncreaseMultiplier(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'HellboundPointsIncreaseMultiplier' )
    }

    getRateKarmaExpLost(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateKarmaExpLost' )
    }

    getRateKarmaLost(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateKarmaLost' )
    }

    getRatePartySp(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RatePartySp' )
    }

    getRatePartyXp(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RatePartyXp' )
    }

    getRateQuestReward(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateQuestReward' )
    }

    getRateQuestRewardAdena(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateQuestRewardAdena' )
    }

    getRateQuestRewardMaterial(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateQuestRewardMaterial' )
    }

    getRateQuestRewardPotion(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateQuestRewardPotion' )
    }

    getRateQuestRewardRecipe(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateQuestRewardRecipe' )
    }

    getRateQuestRewardSP(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateQuestRewardSP' )
    }

    getRateQuestRewardScroll(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateQuestRewardScroll' )
    }

    getRateQuestRewardXP(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateQuestRewardXP' )
    }

    getRateSiegeGuardsPrice(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateSiegeGuardsPrice' )
    }

    getRateSp(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateSp' )
    }

    getRateXp(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'RateXp' )
    }

    getSinEaterXpRate(): number {
        return DotEnvConfigurationEngineHelper.getFloat( configuration, cachedValues, 'SinEaterXpRate' )
    }

    async load(): Promise<void> {
        configuration = await DotEnvConfigurationEngineHelper.getConfig( 'rates.properties' )
        cachedValues = {}
        return
    }

    useQuestRewardMultipliers(): boolean {
        return DotEnvConfigurationEngineHelper.getBoolean( configuration, cachedValues, 'UseQuestRewardMultipliers' )
    }
}

export const DotEnvRatesConfiguration: L2RatesConfigurationApi = new Configuration()