class Properties {
    startTime: number = Date.now()

    getStartTime() {
        return this.startTime
    }
}

export const GameServerProperties = new Properties()