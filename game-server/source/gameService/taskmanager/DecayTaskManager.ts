import { L2Character } from '../models/actor/L2Character'
import { L2World } from '../L2World'
import { L2Npc } from '../models/actor/L2Npc'
import { ConfigManager } from '../../config/ConfigManager'
import { L2Attackable } from '../models/actor/L2Attackable'
import Timeout = NodeJS.Timeout
import _ from 'lodash'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { NpcTemplateType } from '../enums/NpcTemplateType'

interface DecayData {
    task: Timeout
    endTime: number
}

const objectPool = new ObjectPool( 'DecayTaskManager', () : DecayData => {
    return {
        endTime: 0,
        task: undefined
    }
} )

class Manager {
    tasks: { [ objectId: number ]: DecayData } = {}

    add( character: L2Character ) : void {
        if ( !character || this.tasks[ character.getObjectId() ] ) {
            return
        }

        let duration = this.getDelay( character ) * 1000
        let data = objectPool.getValue()

        data.task = setTimeout( this.runDecayTask.bind( this ), duration, character.getObjectId() )
        data.endTime = Date.now() + duration

        this.tasks[ character.getObjectId() ] = data
    }

    cancel( character: L2Character ): void {
        this.stopTask( character.getObjectId() )
    }

    runDecayTask( objectId: number ) {
        let character = L2World.getObjectById( objectId ) as L2Character
        if ( character ) {
            character.onDecay()
        }

        this.stopTask( objectId )
    }

    stopTask( objectId: number ) {
        let data = this.tasks[ objectId ]

        if ( data ) {
            clearTimeout( data.task )
            objectPool.recycleValue( data )

            delete this.tasks[ objectId ]
        }
    }

    getDelay( character: L2Character ): number {
        let delay: number

        if ( character.isNpc() ) {
            delay = _.get( ConfigManager.npc.getCorpseTimePerEntityType(), NpcTemplateType[ ( character as L2Npc ).getTemplate().getType() ], ( character as L2Npc ).getTemplate().getCorpseTime() )
        } else {
            delay = ConfigManager.npc.getDefaultCorpseTime()
        }

        if ( character.isAttackable() && ( ( character as L2Attackable ).isSpoiled() || ( character as L2Attackable ).isSeeded() ) ) {
            delay += ConfigManager.npc.getSpoiledCorpseExtendTime()
        }

        return delay
    }

    getRemainingTime( character: L2Character ) {
        if ( !character || !this.tasks[ character.getObjectId() ] ) {
            return Number.MAX_SAFE_INTEGER
        }

        return this.tasks[ character.getObjectId() ].endTime - Date.now()
    }
}

export const DecayTaskManager = new Manager()