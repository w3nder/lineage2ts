import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { RecipeItemMaker } from '../models/recipes/RecipeItemMaker'
import { RecipeBookItemList } from '../packets/send/RecipeBookItemList'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { L2RecipeDefinition } from '../models/l2RecipeDefinition'
import { ConfigManager } from '../../config/ConfigManager'
import { DataManager } from '../../data/manager'
import { recordDataViolation } from '../helpers/PlayerViolations'
import { DataIntegrityViolationType } from '../models/events/EventType'
import { PlayerRecipeCache } from '../cache/PlayerRecipeCache'

class Controller {
    makers: { [ key: number ]: RecipeItemMaker } = {}

    requestBookOpen( player: L2PcInstance, isDwarvenCraft: boolean ) {
        if ( !this.makers[ player.getObjectId() ] ) {
            player.sendOwnedData( RecipeBookItemList( player, isDwarvenCraft ) )
            return
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_ALTER_RECIPEBOOK_WHILE_CRAFTING ) )
    }

    requestMakeItem( player: L2PcInstance, recipeId: number ): void {
        if ( player.isInCombat() || player.isInDuel() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_OPERATE_PRIVATE_STORE_DURING_COMBAT ) )
            return
        }

        let recipeList: L2RecipeDefinition = this.getValidRecipeList( player, recipeId )
        if ( !recipeList ) {
            return
        }

        let books = PlayerRecipeCache.getBooks( player.getObjectId() )
        if ( !books.dwarven.has( recipeList.id ) && !books.common.has( recipeList.id ) ) {
            recordDataViolation( player.getObjectId(), '91ae3b1f-c289-41bc-9458-485e87c7fb34', DataIntegrityViolationType.Craft, 'Player attempts crafting for missing recipe id', [ recipeList.id ] )
            player.setIsInCraftMode( false )
            return
        }

        // Check if player is busy (possible if alt game creation is enabled)
        if ( ConfigManager.character.alternativeCrafting() && this.makers[ player.getObjectId() ] ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S2_S1 )
                    .addItemNameWithId( recipeList.productionItemId )
                    .addString( 'You are busy creating item' )
                    .getBuffer()
            player.sendOwnedData( packet )
            return
        }

        let maker: RecipeItemMaker = new RecipeItemMaker( player, recipeList, player )
        this.makers[ player.getObjectId() ] = maker
        setTimeout( maker.onStart.bind( maker ), 100 )
    }

    getValidRecipeList( player: L2PcInstance, recipeId: number ): L2RecipeDefinition {
        let recipeList: L2RecipeDefinition = DataManager.getRecipeData().getRecipeList( recipeId )
        if ( !recipeList || recipeList.items.length === 0 ) {
            if ( player.isGM() ) {
                player.sendMessage( `Cannot find L2RecipeList for id = ${ recipeId }` )
            } else {
                recordDataViolation( player.getObjectId(), 'f163bd40-e698-44e8-be0a-28489fa04d72', DataIntegrityViolationType.Craft, 'Recipe does not exist with provided id', [ recipeId ] )
            }

            player.setIsInCraftMode( false )
            return null
        }

        return recipeList
    }

    removeMaker( objectId: number ): void {
        delete this.makers[ objectId ]
    }

    requestManufactureItem( player: L2PcInstance, recipeId: number, customer: L2PcInstance ): void {
        let recipeList: L2RecipeDefinition = this.getValidRecipeList( player, recipeId )
        if ( !recipeList ) {
            return
        }

        let books = PlayerRecipeCache.getBooks( player.getObjectId() )
        if ( !books.dwarven.has( recipeList.id ) && !books.common.has( recipeList.id ) ) {
            recordDataViolation( player.getObjectId(), '9f1f841a-b381-41c0-9d53-8a868558dc0a', DataIntegrityViolationType.Craft, 'Player attempts crafting for missing recipe id', [ recipeList.id ] )
            player.setIsInCraftMode( false )
            return
        }

        if ( ConfigManager.character.alternativeCrafting() && this.makers[ player.getObjectId() ] ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLOSE_STORE_WINDOW_AND_TRY_AGAIN ) )
            return
        }

        let maker: RecipeItemMaker = new RecipeItemMaker( player, recipeList, customer )
        this.makers[ player.getObjectId() ] = maker
        setTimeout( maker.onStart.bind( maker ), 100 )
    }
}

export const RecipeController = new Controller()