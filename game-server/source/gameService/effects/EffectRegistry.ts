import { AbstractEffect } from '../models/effects/AbstractEffect'
import { ConsumeAgathionEnergy } from './consume/ConsumeAgathionEnergy'
import { ConsumeChameleonRest } from './consume/ConsumeChameleonRest'
import { ConsumeFakeDeath } from './consume/ConsumeFakeDeath'
import { ConsumeHp } from './consume/ConsumeHp'
import { ConsumeMp } from './consume/ConsumeMp'
import { ConsumeMpByLevel } from './consume/ConsumeMpByLevel'
import { ConsumeRest } from './consume/ConsumeRest'
import { BlockAction } from './other/BlockAction'
import { Buff } from './other/Buff'
import { Debuff } from './other/Debuff'
import { Detection } from './other/Detection'
import { Flag } from './other/Flag'
import { Grow } from './other/Grow'
import { ImmobileBuff } from './other/ImmobileBuff'
import { ImmobilePetBuff } from './other/ImmobilePetBuff'
import { Mute } from './other/Mute'
import { OpenChest } from './other/OpenChest'
import { OpenDoor } from './other/OpenDoor'
import { Recovery } from './other/Recovery'
import { Root } from './other/Root'
import { SilentMove } from './other/SilentMove'
import { Sleep } from './other/Sleep'
import { Stun } from './other/Stun'
import { ThrowUp } from './other/ThrowUp'
import { Backstab } from './instant/Backstab'
import { Blink } from './instant/Blink'
import { Bluff } from './instant/Bluff'
import { CallParty } from './instant/CallParty'
import { CallPc } from './instant/CallPc'
import { CallSkill } from './instant/CallSkill'
import { ChangeFace } from './instant/ChangeFace'
import { ChangeHairColor } from './instant/ChangeHairColor'
import { ChangeHairStyle } from './instant/ChangeHairStyle'
import { ClanGate } from './instant/ClanGate'
import { Confuse } from './instant/Confuse'
import { ConsumeBody } from './instant/ConsumeBody'
import { ConvertItem } from './instant/ConvertItem'
import { Cp } from './instant/Cp'
import { DeathLink } from './instant/DeathLink'
import { DeleteHate } from './instant/DeleteHate'
import { DeleteHateOfMe } from './instant/DeleteHateOfMe'
import { DetectHiddenObjects } from './instant/DetectHiddenObjects'
import { DispelAll } from './instant/DispelAll'
import { DispelByCategory } from './instant/DispelByCategory'
import { DispelBySlot } from './instant/DispelBySlot'
import { DispelBySlotProbability } from './instant/DispelBySlotProbability'
import { EnergyAttack } from './instant/EnergyAttack'
import { Escape } from './instant/Escape'
import { FatalBlow } from './instant/FatalBlow'
import { Fishing } from './instant/Fishing'
import { FlySelf } from './instant/FlySelf'
import { FocusEnergy } from './instant/FocusEnergy'
import { FocusMaxEnergy } from './instant/FocusMaxEnergy'
import { FocusSouls } from './instant/FocusSouls'
import { FoodForPet } from './instant/FoodForPet'
import { GetAgro } from './instant/GetAgro'
import { GiveRecommendation } from './instant/GiveRecommendation'
import { GiveSp } from './instant/GiveSp'
import { Harvesting } from './instant/Harvesting'
import { HeadquarterCreate } from './instant/HeadquarterCreate'
import { Heal } from './instant/Heal'
import { Hp } from './instant/Hp'
import { HpByLevel } from './instant/HpByLevel'
import { HpDrain } from './instant/HpDrain'
import { HpPerMax } from './instant/HpPerMax'
import { AddHate } from './instant/AddHate'
import { InstantAgathionEnergy } from './instant/InstantAgathionEnergy'
import { Lethal } from './instant/Lethal'
import { MagicalAttack } from './instant/MagicalAttack'
import { MagicalAttackByAbnormal } from './instant/MagicalAttackByAbnormal'
import { MagicalAttackMp } from './instant/MagicalAttackMp'
import { MagicalSoulAttack } from './instant/MagicalSoulAttack'
import { ManaHealByLevel } from './instant/ManaHealByLevel'
import { Mp } from './instant/Mp'
import { MpPerMax } from './instant/MpPerMax'
import { OpenCommonRecipeBook } from './instant/OpenCommonRecipeBook'
import { OpenDwarfRecipeBook } from './instant/OpenDwarfRecipeBook'
import { OutpostCreate } from './instant/OutpostCreate'
import { OutpostDestroy } from './instant/OutpostDestroy'
import { PhysicalAttack } from './instant/PhysicalAttack'
import { PhysicalAttackHpLink } from './instant/PhysicalAttackHpLink'
import { PhysicalSoulAttack } from './instant/PhysicalSoulAttack'
import { Pumping } from './instant/Pumping'
import { RandomizeHate } from './instant/RandomizeHate'
import { RebalanceHP } from './instant/RebalanceHP'
import { RebalanceMP } from './instant/RebalanceMP'
import { Reeling } from './instant/Reeling'
import { RefuelAirship } from './instant/RefuelAirship'
import { Restoration } from './instant/Restoration'
import { RestorationRandom } from './instant/RestorationRandom'
import { Resurrection } from './instant/Resurrection'
import { RunAway } from './instant/RunAway'
import { SetSkill } from './instant/SetSkill'
import { SkillTurning } from './instant/SkillTurning'
import { SoulBlow } from './instant/SoulBlow'
import { Sow } from './instant/Sow'
import { Spoil } from './instant/Spoil'
import { StaticDamage } from './instant/StaticDamage'
import { StealAbnormal } from './instant/StealAbnormal'
import { Summon } from './instant/Summon'
import { SummonAgathion } from './instant/SummonAgathion'
import { SummonCubic } from './instant/SummonCubic'
import { SummonNpc } from './instant/SummonNpc'
import { SummonPet } from './instant/SummonPet'
import { SummonTrap } from './instant/SummonTrap'
import { Sweeper } from './instant/Sweeper'
import { TakeCastle } from './instant/TakeCastle'
import { TakeFort } from './instant/TakeFort'
import { TakeFortStart } from './instant/TakeFortStart'
import { TakeTerritoryFlag } from './instant/TakeTerritoryFlag'
import { TargetCancel } from './instant/TargetCancel'
import { TargetMeProbability } from './instant/TargetMeProbability'
import { Teleport } from './instant/Teleport'
import { TeleportToTarget } from './instant/TeleportToTarget'
import { TransferHate } from './instant/TransferHate'
import { TrapDetect } from './instant/TrapDetect'
import { TrapRemove } from './instant/TrapRemove'
import { Unsummon } from './instant/Unsummon'
import { UnsummonAgathion } from './instant/UnsummonAgathion'
import { VitalityPointUp } from './instant/VitalityPointUp'
import { AttackTrait } from './toggle/AttackTrait'
import { Betray } from './toggle/Betray'
import { BlockBuff } from './toggle/BlockBuff'
import { BlockBuffSlot } from './toggle/BlockBuffSlot'
import { BlockChat } from './toggle/BlockChat'
import { BlockDamage } from './toggle/BlockDamage'
import { BlockDebuff } from './toggle/BlockDebuff'
import { BlockParty } from './toggle/BlockParty'
import { BlockResurrection } from './toggle/BlockResurrection'
import { ChangeFishingMastery } from './toggle/ChangeFishingMastery'
import { CrystalGradeModify } from './toggle/CrystalGradeModify'
import { CubicMastery } from './toggle/CubicMastery'
import { DefenceTrait } from './toggle/DefenceTrait'
import { Disarm } from './toggle/Disarm'
import { EnableCloak } from './toggle/EnableCloak'
import { Fear } from './toggle/Fear'
import { Hide } from './toggle/Hide'
import { Lucky } from './toggle/Lucky'
import { MaxCp } from './toggle/MaxCp'
import { MaxHp } from './toggle/MaxHp'
import { MaxMp } from './toggle/MaxMp'
import { NoblesseBless } from './toggle/NoblesseBless'
import { Passive } from './toggle/Passive'
import { PhysicalAttackMute } from './toggle/PhysicalAttackMute'
import { PhysicalMute } from './toggle/PhysicalMute'
import { ProtectionBlessing } from './toggle/ProtectionBlessing'
import { ResistSkill } from './toggle/ResistSkill'
import { ResurrectionSpecial } from './toggle/ResurrectionSpecial'
import { ServitorShare } from './toggle/ServitorShare'
import { SingleTarget } from './toggle/SingleTarget'
import { SoulEating } from './toggle/SoulEating'
import { TalismanSlot } from './toggle/TalismanSlot'
import { TargetMe } from './toggle/TargetMe'
import { TransferDamage } from './toggle/TransferDamage'
import { Transformation } from './toggle/Transformation'
import { TriggerSkillByAttack } from './toggle/TriggerSkillByAttack'
import { TriggerSkillByAvoid } from './toggle/TriggerSkillByAvoid'
import { TriggerSkillByDamage } from './toggle/TriggerSkillByDamage'
import { TriggerSkillBySkill } from './toggle/TriggerSkillBySkill'
import { TickHp } from './dot/TickHp'
import { TickHpFatal } from './dot/TickHpFatal'
import { TickMp } from './dot/TickMp'
import { MagicalAttackRange } from './instant/MagicalAttackRange'
import { InstantDispelByName } from './instant/InstantDispelByName'

type EffectProducer = new ( parameters? : any ) => AbstractEffect
const allEffects: { [key: string]: EffectProducer } = {
    'ConsumeAgathionEnergy': ConsumeAgathionEnergy,
    'ConsumeChameleonRest': ConsumeChameleonRest,
    'ConsumeFakeDeath': ConsumeFakeDeath,
    'ConsumeHp': ConsumeHp,
    'ConsumeMp': ConsumeMp,
    'ConsumeMpByLevel': ConsumeMpByLevel,
    'ConsumeRest': ConsumeRest,
    'BlockAction': BlockAction,
    'Buff': Buff,
    'Debuff': Debuff,
    'Detection': Detection,
    'Flag': Flag,
    'Grow': Grow,
    'ImmobileBuff': ImmobileBuff,
    'ImmobilePetBuff': ImmobilePetBuff,
    'Mute': Mute,
    'OpenChest': OpenChest,
    'OpenDoor': OpenDoor,
    'Recovery': Recovery,
    'Root': Root,
    'SilentMove': SilentMove,
    'Sleep': Sleep,
    'Stun': Stun,
    'ThrowUp': ThrowUp,
    'Backstab': Backstab,
    'Blink': Blink,
    'Bluff': Bluff,
    'CallParty': CallParty,
    'CallPc': CallPc,
    'CallSkill': CallSkill,
    'ChangeFace': ChangeFace,
    'ChangeHairColor': ChangeHairColor,
    'ChangeHairStyle': ChangeHairStyle,
    'ClanGate': ClanGate,
    'Confuse': Confuse,
    'ConsumeBody': ConsumeBody,
    'ConvertItem': ConvertItem,
    'Cp': Cp,
    'DeathLink': DeathLink,
    'DeleteHate': DeleteHate,
    'DeleteHateOfMe': DeleteHateOfMe,
    'DetectHiddenObjects': DetectHiddenObjects,
    'DispelAll': DispelAll,
    'DispelByCategory': DispelByCategory,
    'DispelBySlot': DispelBySlot,
    'DispelBySlotProbability': DispelBySlotProbability,
    'EnergyAttack': EnergyAttack,
    'Escape': Escape,
    'FatalBlow': FatalBlow,
    'Fishing': Fishing,
    'FlySelf': FlySelf,
    'FocusEnergy': FocusEnergy,
    'FocusMaxEnergy': FocusMaxEnergy,
    'FocusSouls': FocusSouls,
    'FoodForPet': FoodForPet,
    'GetAgro': GetAgro,
    'GiveRecommendation': GiveRecommendation,
    'GiveSp': GiveSp,
    'Harvesting': Harvesting,
    'HeadquarterCreate': HeadquarterCreate,
    'Heal': Heal,
    'Hp': Hp,
    'HpByLevel': HpByLevel,
    'HpDrain': HpDrain,
    'HpPerMax': HpPerMax,
    'AddHate': AddHate,
    'InstantAgathionEnergy': InstantAgathionEnergy,
    'Lethal': Lethal,
    'MagicalAttack': MagicalAttack,
    'MagicalAttackByAbnormal': MagicalAttackByAbnormal,
    'MagicalAttackMp': MagicalAttackMp,
    'MagicalSoulAttack': MagicalSoulAttack,
    'ManaHealByLevel': ManaHealByLevel,
    'Mp': Mp,
    'MpPerMax': MpPerMax,
    'OpenCommonRecipeBook': OpenCommonRecipeBook,
    'OpenDwarfRecipeBook': OpenDwarfRecipeBook,
    'OutpostCreate': OutpostCreate,
    'OutpostDestroy': OutpostDestroy,
    'PhysicalAttack': PhysicalAttack,
    'PhysicalAttackHpLink': PhysicalAttackHpLink,
    'PhysicalSoulAttack': PhysicalSoulAttack,
    'Pumping': Pumping,
    'RandomizeHate': RandomizeHate,
    'RebalanceHP': RebalanceHP,
    'RebalanceMP': RebalanceMP,
    'Reeling': Reeling,
    'RefuelAirship': RefuelAirship,
    'Restoration': Restoration,
    'RestorationRandom': RestorationRandom,
    'Resurrection': Resurrection,
    'RunAway': RunAway,
    'SetSkill': SetSkill,
    'SkillTurning': SkillTurning,
    'SoulBlow': SoulBlow,
    'Sow': Sow,
    'Spoil': Spoil,
    'StaticDamage': StaticDamage,
    'StealAbnormal': StealAbnormal,
    'Summon': Summon,
    'SummonAgathion': SummonAgathion,
    'SummonCubic': SummonCubic,
    'SummonNpc': SummonNpc,
    'SummonPet': SummonPet,
    'SummonTrap': SummonTrap,
    'Sweeper': Sweeper,
    'TakeCastle': TakeCastle,
    'TakeFort': TakeFort,
    'TakeFortStart': TakeFortStart,
    'TakeTerritoryFlag': TakeTerritoryFlag,
    'TargetCancel': TargetCancel,
    'TargetMeProbability': TargetMeProbability,
    'Teleport': Teleport,
    'TeleportToTarget': TeleportToTarget,
    'TransferHate': TransferHate,
    'TrapDetect': TrapDetect,
    'TrapRemove': TrapRemove,
    'Unsummon': Unsummon,
    'UnsummonAgathion': UnsummonAgathion,
    'VitalityPointUp': VitalityPointUp,
    'AttackTrait': AttackTrait,
    'Betray': Betray,
    'BlockBuff': BlockBuff,
    'BlockBuffSlot': BlockBuffSlot,
    'BlockChat': BlockChat,
    'BlockDamage': BlockDamage,
    'BlockDebuff': BlockDebuff,
    'BlockParty': BlockParty,
    'BlockResurrection': BlockResurrection,
    'ChangeFishingMastery': ChangeFishingMastery,
    'CrystalGradeModify': CrystalGradeModify,
    'CubicMastery': CubicMastery,
    'DefenceTrait': DefenceTrait,
    'Disarm': Disarm,
    'EnableCloak': EnableCloak,
    'Fear': Fear,
    'Hide': Hide,
    'Lucky': Lucky,
    'MaxCp': MaxCp,
    'MaxHp': MaxHp,
    'MaxMp': MaxMp,
    'NoblesseBless': NoblesseBless,
    'Passive': Passive,
    'PhysicalAttackMute': PhysicalAttackMute,
    'PhysicalMute': PhysicalMute,
    'ProtectionBlessing': ProtectionBlessing,
    'ResistSkill': ResistSkill,
    'ResurrectionSpecial': ResurrectionSpecial,
    'ServitorShare': ServitorShare,
    'SingleTarget': SingleTarget,
    'SoulEating': SoulEating,
    'TalismanSlot': TalismanSlot,
    'TargetMe': TargetMe,
    'TransferDamage': TransferDamage,
    'Transformation': Transformation,
    'TriggerSkillByAttack': TriggerSkillByAttack,
    'TriggerSkillByAvoid': TriggerSkillByAvoid,
    'TriggerSkillByDamage': TriggerSkillByDamage,
    'TriggerSkillBySkill': TriggerSkillBySkill,
    'TickHp': TickHp,
    'TickHpFatal': TickHpFatal,
    'TickMp': TickMp,
    'MagicalAttackRange': MagicalAttackRange,
    'InstantDispelByName': InstantDispelByName
}

export const EffectRegistry = {
    createEffect( name, parameters: any ): AbstractEffect {
        let constructor: EffectProducer = allEffects[ name ]

        if ( constructor ) {
            return new constructor( parameters )
        }

        return null
    }
}