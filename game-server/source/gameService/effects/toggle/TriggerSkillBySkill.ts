import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { SkillHolder } from '../../models/holders/SkillHolder'
import { L2TargetType, L2TargetTypeVerifiers, ChooseCaster, TargetVerifierMethodType } from '../../models/skills/targets/L2TargetType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { CreatureUsesSkillEvent, EventType } from '../../models/events/EventType'
import { EventListenerData, EventListenerMethod } from '../../models/events/listeners/EventListenerData'
import { Skill } from '../../models/Skill'
import { L2Object } from '../../models/L2Object'
import { L2Character } from '../../models/actor/L2Character'
import Aigle from 'aigle'
import { ListenerCache, ListenerPredicate } from '../../cache/ListenerCache'
import { L2World } from '../../L2World'
import _ from 'lodash'

export class TriggerSkillBySkill extends AbstractEffect {
    skillId: number
    chance: number
    skill: SkillHolder
    targetType: L2TargetType
    removeListenerMethod: ListenerPredicate
    onSkillUseMethod: EventListenerMethod

    constructor( parameters: Object ) {
        super()
        this.skillId = _.parseInt( _.get( parameters, 'castSkillId', '0' ) )
        this.chance = parseFloat( _.get( parameters, 'chance', '100' ) )

        let skillId = _.parseInt( _.get( parameters, 'skillId', '0' ) )
        let skillLevel = _.parseInt( _.get( parameters, 'skillLevel', '0' ) )
        this.skill = new SkillHolder( skillId, skillLevel )

        this.targetType = L2TargetType[ _.get( parameters, 'targetType', 'SELF' ) as string ]
        this.removeListenerMethod = this.removeListenerPredicate.bind( this )
        this.onSkillUseMethod = this.onSkillUse.bind( this )
    }

    async onExit( info: BuffInfo ) {
        ListenerCache.removeObjectListener( info.getAffectedId(), EventType.CharacterUsesSkill, this.removeListenerMethod )
    }

    async onStart( info: BuffInfo ) {
        ListenerCache.addObjectListener( info.getAffectedId(), EventType.CharacterUsesSkill, this, this.onSkillUseMethod )
    }

    removeListenerPredicate( data: EventListenerData ): boolean {
        return data.owner === this
    }

    canStart(): boolean {
        if ( this.chance === 0
                || this.skill.getId() === 0
                || this.skill.getLevel() === 0
                || this.skillId === 0 ) {
            return false
        }

        return _.random( 100 ) <= this.chance
    }

    async onSkillUse( eventData: CreatureUsesSkillEvent ) : Promise<void> {
        if ( this.skillId !== eventData.skillId ) {
            return
        }

        let triggerSkill: Skill = this.skill.getSkill()
        let attacker: L2Character = L2World.getObjectById( eventData.attackerId ) as L2Character
        let target: L2Character = L2World.getObjectById( eventData.mainTargetId ) as L2Character

        let triggerCasts: Array<Promise<void>> = triggerSkill
                .getTargetWithSelectedTarget( attacker, target )
                .reduce( ( promises: Array<Promise<void>>, target: L2Object ) => {
                    if ( target && target.isCharacter() && !( target as L2Character ).isInvulnerable() ) {
                        promises.push( attacker.makeTriggerCast( triggerSkill, ( target as L2Character ) ) )
                    }

                    return promises
                }, [] )

        if ( triggerCasts.length > 0 ) {
            await Promise.all( triggerCasts )
        }
    }
}