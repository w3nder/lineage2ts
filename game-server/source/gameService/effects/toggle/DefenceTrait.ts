import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { TraitType, TraitTypeValues } from '../../models/stats/TraitType'

export class DefenceTrait extends AbstractEffect {
    traits: { [ key: string] : number } = {}

    constructor( parameters: Object ) {
        super()

        let effect = this
        _.each( parameters, ( value: any, key: string ) => {
            effect.traits[ key ] = ( _.parseInt( value ) + 100 ) / 100
        } )
    }

    async onStart( info: BuffInfo ) {
        let stats = info.getAffected().getStat()
        _.each( this.traits, ( value: number, key: string ) => {
            let trait : TraitType = TraitTypeValues[ key ]
            if ( value < 2 ) {
                stats.getDefenceTraits()[ trait.id ] *= value
                stats.getDefenceTraitsCount()[ trait.id ]++
            } else {
                stats.getTraitsInvulnerable()[ trait.id ]++
            }
        } )
    }

    async onExit( buff: BuffInfo ) {
        let stats = buff.getAffected().getStat()
        _.each( this.traits, ( value: number, key: string ) => {
            let trait : TraitType = TraitTypeValues[ key ]
            if ( value < 2 ) {
                stats.getDefenceTraits()[ trait.id ] /= value
                stats.getDefenceTraitsCount()[ trait.id ]--
            } else {
                stats.getTraitsInvulnerable()[ trait.id ]--
            }
        } )
    }
}