import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { AbnormalType } from '../../models/skills/AbnormalType'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class BlockBuffSlot extends AbstractEffect {
    slots: Array<AbnormalType> = []

    constructor( parameters: Object ) {
        super()

        this.slots = _.map( _.split( _.get( parameters, 'slot' ), ';' ), ( value: string ) => {
            return AbnormalType[ value ]
        } )
    }

    async onStart( info: BuffInfo ) {
        info.getAffected().getEffectList().addBlockedBuffSlots( this.slots )
    }

    async onExit( info: BuffInfo ) {
        info.getAffected().getEffectList().removeBlockedBuffSlots( this.slots )
    }
}