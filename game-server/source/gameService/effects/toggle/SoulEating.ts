import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { EventType, PlayableExpChangedEvent } from '../../models/events/EventType'
import { EventListenerData, EventListenerMethod } from '../../models/events/listeners/EventListenerData'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { Stats } from '../../models/stats/Stats'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ExSpawnEmitter } from '../../packets/send/ExSpawnEmitter'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ListenerCache, ListenerPredicate } from '../../cache/ListenerCache'
import { L2World } from '../../L2World'
import _ from 'lodash'

export class SoulEating extends AbstractEffect {
    amountNeeded: number
    removeListenerMethod: ListenerPredicate
    onExperienceReceivedMethod: EventListenerMethod

    constructor( parameters: Object ) {
        super()
        this.amountNeeded = _.parseInt( _.get( parameters, 'expNeeded', '0' ) )
        this.removeListenerMethod = this.removeListenerPredicate.bind( this )
        this.onExperienceReceivedMethod = this.onExperienceReceived.bind( this )
    }

    async onExit( info: BuffInfo ) {
        let target = info.getAffected()
        if ( target ) {
            ListenerCache.removeObjectListener( target.getObjectId(), EventType.PlayableExpChanged, this.removeListenerMethod )
        }
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        if ( target.isPlayer() ) {
            ListenerCache.addObjectListener( target.getObjectId(), EventType.PlayableExpChanged, this, this.onExperienceReceivedMethod )
        }
    }

    async onExperienceReceived( data: PlayableExpChangedEvent ): Promise<void> {
        let character = L2World.getObjectById( data.characterId )

        if ( character && character.isPlayer() && data.newValue >= this.amountNeeded ) {
            let player: L2PcInstance = character.getActingPlayer()
            let maxSouls = player.calculateStat( Stats.MAX_SOULS, 0, null, null )
            if ( player.getChargedSouls() >= maxSouls ) {
                ( character as L2PcInstance ).sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SOUL_CANNOT_BE_ABSORBED_ANYMORE ) )
                return
            }

            player.increaseSouls( 1 )

            let target = player.getTarget()
            if ( target && target.isNpc() ) {
                BroadcastHelper.dataToSelfInRange( player, ExSpawnEmitter( player.getObjectId(), target.objectId ), 500 )
            }

            return
        }

        return
    }

    removeListenerPredicate( data: EventListenerData ): boolean {
        return data.owner === this
    }
}