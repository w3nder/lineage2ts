import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { getPossibleSkill } from '../../models/holders/SkillHolder'
import { L2TargetType } from '../../models/skills/targets/L2TargetType'
import { InstanceType } from '../../enums/InstanceType'
import { WeaponType, WeaponTypeMask } from '../../models/items/type/WeaponType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { CharacterReceivedDamageEvent, EventType } from '../../models/events/EventType'
import { L2Object } from '../../models/L2Object'
import { L2Character } from '../../models/actor/L2Character'
import { EventListenerData, EventListenerMethod } from '../../models/events/listeners/EventListenerData'
import { ListenerCache, ListenerPredicate } from '../../cache/ListenerCache'
import { L2World } from '../../L2World'
import _ from 'lodash'
import { SkillItem } from '../../models/skills/SkillItem'

export class TriggerSkillByAttack extends AbstractEffect {
    minLevel: number
    maxLevel: number
    minDamage: number
    chance: number
    skill: SkillItem
    targetType: L2TargetType
    attackerType: InstanceType
    allowWeapons: number = 0
    isCritical: boolean

    removeListenerMethod: ListenerPredicate
    onAttackEventMethod: EventListenerMethod

    constructor( parameters: Object ) {
        super()
        this.minLevel = _.parseInt( _.get( parameters, 'minAttackerLevel', '1' ) )
        this.maxLevel = _.parseInt( _.get( parameters, 'maxAttackerLevel', '100' ) )
        this.minDamage = _.parseInt( _.get( parameters, 'minDamage', '1' ) )
        this.chance = parseFloat( _.get( parameters, 'chance', '100' ) )

        let id = _.parseInt( _.get( parameters, 'skillId', '1' ) )
        let level = _.parseInt( _.get( parameters, 'skillLevel', '1' ) )
        this.skill = {
            id,
            level
        }

        this.targetType = L2TargetType[ _.get( parameters, 'targetType', 'SELF' ) as string ]
        this.attackerType = InstanceType[ _.get( parameters, 'attackerType', 'L2Character' ) as string ]

        this.isCritical = _.get( parameters, 'isCritical', false )
        let allowWeapons = _.get( parameters, 'allowWeapons' )

        if ( allowWeapons === 'ALL' ) {
            return
        }

        this.allowWeapons = _.reduce( _.split( allowWeapons, ',' ), ( allowWeapons: number, name: string ) => {
            allowWeapons |= WeaponTypeMask( WeaponType[ name ] )

            return allowWeapons
        }, 0 )

        this.removeListenerMethod = this.removeListenerPredicate.bind( this )
        this.onAttackEventMethod = this.onAttackEvent.bind( this )
    }

    canStart(): boolean {
        if ( this.chance === 0 || !this.skill.id || !this.skill.level ) {
            return false
        }

        return _.random( 100 ) <= this.chance
    }

    async onExit( info: BuffInfo ) {
        ListenerCache.removeObjectListener( info.getAffectedId(), EventType.CharacterReceivedDamage, this.removeListenerMethod )
    }

    async onStart( info: BuffInfo ) {
        ListenerCache.addObjectListener( info.getAffectedId(), EventType.CharacterReceivedDamage, this, this.onAttackEventMethod )
    }

    async onAttackEvent( eventData: CharacterReceivedDamageEvent ) : Promise<void> {
        if ( eventData.attackerId === eventData.targetId
                || eventData.isDOT
                || eventData.isReflected
                || this.isCritical !== eventData.isCritical ) {
            return
        }

        let skill = getPossibleSkill( this.skill.id, this.skill.level )
        if ( !skill ) {
            return
        }

        let attacker: L2Character = L2World.getObjectById( eventData.attackerId ) as L2Character
        let target: L2Character = L2World.getObjectById( eventData.targetId ) as L2Character
        if ( this.targetType === L2TargetType.SELF
                && skill.getCastRange() > 0
                && attacker.calculateDistance( target, true ) > skill.getCastRange() ) {
            return
        }

        if ( attacker.getLevel() < this.minLevel || attacker.getLevel() > this.maxLevel ) {
            return
        }

        if ( eventData.damage < this.minDamage || !attacker.isInstanceType( this.attackerType ) ) {
            return
        }

        if ( this.allowWeapons > 0 && ( !attacker.getActiveWeaponItem() || ( WeaponTypeMask( attacker.getActiveWeaponItem().getItemType() ) & this.allowWeapons ) === 0 ) ) {
            return
        }

        let triggerCasts: Array<Promise<void>> = skill
                .getTargetWithSelectedTarget( attacker, target )
                .reduce( ( promises: Array<Promise<void>>, target: L2Object ) => {
            if ( target && target.isCharacter() && !( target as L2Character ).isInvulnerable() ) {
                promises.push( attacker.makeTriggerCast( skill, ( target as L2Character ) ) )
            }

            return promises
        }, [] )

        if ( triggerCasts.length > 0 ) {
            await Promise.all( triggerCasts )
        }
    }

    removeListenerPredicate( data: EventListenerData ): boolean {
        return data.owner === this
    }
}