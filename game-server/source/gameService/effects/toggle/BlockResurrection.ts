import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'

export class BlockResurrection extends AbstractEffect {
    effectFlags = EffectFlag.BLOCK_RESURRECTION
}