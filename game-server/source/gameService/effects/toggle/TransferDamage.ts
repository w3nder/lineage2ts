import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Playable } from '../../models/actor/L2Playable'

export class TransferDamage extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayable() && info.getEffector().isPlayer()
    }

    async onExit( info: BuffInfo ) {
        ( info.getAffected() as L2Playable ).setTransferDamageTo( null )
    }

    async onStart( info: BuffInfo ) {
        ( info.getAffected() as L2Playable ).setTransferDamageTo( info.getEffector().getActingPlayer() )
    }
}