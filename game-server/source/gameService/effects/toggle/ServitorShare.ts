import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { EffectFlag } from '../../enums/EffectFlag'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class ServitorShare extends AbstractEffect {
    stats: Record<string, number> = {}
    effectFlags = EffectFlag.SERVITOR_SHARE
    effectType = L2EffectType.Buff

    constructor( parameters: Object ) {
        super()

        let effect = this
        _.each( parameters, ( value: any, key: string ) => {
            effect.stats[ key ] = parseFloat( value )
        } )
    }

    async onStart( info: BuffInfo ) {
        let player = info.getAffected().getActingPlayer()
        player.setServitorShare( this.stats )

        if ( player.getSummon() ) {
            player.getSummon().broadcastInfo()
            player.getSummon().getStatus().startHpMpRegeneration()
        }
    }

    async onExit( info: BuffInfo ) {
        let target = info.getAffected()
        target.getActingPlayer().setServitorShare( null )

        let summon = target.getSummon()
        if ( summon ) {
            if ( summon.getCurrentHp() > summon.getMaxHp() ) {
                summon.setCurrentHp( summon.getMaxHp() )
            }

            if ( summon.getCurrentMp() > summon.getMaxMp() ) {
                summon.setCurrentMp( summon.getMaxMp() )
            }

            summon.broadcastInfo()
        }
    }
}