import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { TransformHelper } from '../../helpers/TransformHelper'

export class Transformation extends AbstractEffect {
    id: number

    constructor( parameters: Object ) {
        super()
        this.id = _.parseInt( _.get( parameters, 'id', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayer()
    }

    async onExit( info: BuffInfo ) {
        return info.getAffected().stopTransformation( false )
    }

    async onStart( info: BuffInfo ) {
        return TransformHelper.transformPlayer( this.id, info.getAffected().getActingPlayer() )
    }
}