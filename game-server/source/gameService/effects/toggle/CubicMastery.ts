import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class CubicMastery extends AbstractEffect {
    count: number

    constructor( parameters: Object ) {
        super()
        this.count = _.parseInt( _.get( parameters, 'cubicCount', '1' ) )
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        return info.getSkill().isPassive()
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return info.getEffector() && target && target.isPlayer()
    }

    async onStart( info: BuffInfo ) {
        info.getAffected().getActingPlayer().getStat().setMaxCubicCount( this.count )
    }

    async onExit( info: BuffInfo ) {
        info.getAffected().getActingPlayer().getStat().setMaxCubicCount( 1 )
    }
}