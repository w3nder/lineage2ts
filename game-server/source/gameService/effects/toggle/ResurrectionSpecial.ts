import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { EffectFlag } from '../../enums/EffectFlag'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'

export class ResurrectionSpecial extends AbstractEffect {
    power: number
    recovery: number
    effectFlags = EffectFlag.RESURRECTION_SPECIAL
    effectType = L2EffectType.ResurrectionRecovery

    constructor( parameters: Object ) {
        super()
        this.power = _.parseInt( _.get( parameters, 'resPower', '0' ) )
        this.recovery = _.parseInt( _.get( parameters, 'resRecovery', '0' ) )
    }

    async onExit( info: BuffInfo ) {
        let caster: L2PcInstance = info.getEffector().getActingPlayer()

        let target = info.getAffected()
        if ( target.isPlayer() ) {
            target.getActingPlayer().reviveRequest( caster, false, this.power, this.recovery )
            return
        }

        if ( target.isPet() ) {
            let pet: L2PetInstance = target as L2PetInstance
            let player = pet.getActingPlayer()
            player.reviveRequest( player, true, this.power, this.recovery )
        }
    }
}