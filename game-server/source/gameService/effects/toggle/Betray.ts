import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { EffectFlag } from '../../enums/EffectFlag'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'

export class Betray extends AbstractEffect {
    effectFlags = EffectFlag.BETRAYED
    effectType = L2EffectType.Debuff

    canStart( info: BuffInfo ): boolean {
        return info.getEffector().isPlayer() && info.getAffected().isSummon()
    }

    async onStart( info: BuffInfo ) {
        return AIEffectHelper.notifyAttackedWithTargetId( info.getAffected(), info.getEffectorId(), 1, 1 )
    }
}