import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { SkillItem } from '../../models/skills/SkillItem'

export class ResistSkill extends AbstractEffect {
    skills: Array<SkillItem> = []
    effectType = L2EffectType.Buff

    constructor( parameters: Object ) {
        super()

        let counter = 1
        while ( true ) {
            let id = _.get( parameters, `skillId${counter}`, 0 )
            let level = _.get( parameters, `skillLvl${counter}`, 0 )

            if ( id === 0 ) {
                break
            }

            this.skills.push( {
                id,
                level
            } )

            counter++
        }
    }

    async onStart( info: BuffInfo ) {
        let target : L2Character = info.getAffected()

        this.skills.forEach( ( item: SkillItem ) => {
            target.addInvulnerabilityAgainstSkill( item.id, item.level )
        } )
    }

    async onExit( info: BuffInfo ) {
        let target : L2Character = info.getAffected()

        this.skills.forEach( ( item: SkillItem ) => {
            target.removeInvulnerabilityAgainstSkill( item.id )
        } )
    }
}