import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'

export class PhysicalMute extends AbstractEffect {
    effectFlags = EffectFlag.PSYCHICAL_MUTED

    async onStart( info: BuffInfo ) {
        return AIEffectHelper.notifyAttackedWithTargetId( info.getAffected(), info.getEffectorId(), 1, 1 )
    }
}