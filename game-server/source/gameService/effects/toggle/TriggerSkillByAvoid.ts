import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2TargetType } from '../../models/skills/targets/L2TargetType'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { CreatureAvoidAttackEvent, EventType } from '../../models/events/EventType'
import { EventListenerData, EventListenerMethod } from '../../models/events/listeners/EventListenerData'
import { L2Object } from '../../models/L2Object'
import { L2Character } from '../../models/actor/L2Character'
import { ListenerCache, ListenerPredicate } from '../../cache/ListenerCache'
import { L2World } from '../../L2World'
import { SkillItem } from '../../models/skills/SkillItem'
import { getPossibleSkill } from '../../models/holders/SkillHolder'

export class TriggerSkillByAvoid extends AbstractEffect {
    chance: number
    skill: SkillItem
    targetType: L2TargetType
    removeListenerMethod: ListenerPredicate
    onAvoidEventMethod: EventListenerMethod

    constructor( parameters: Object ) {
        super()
        this.chance = parseFloat( _.get( parameters, 'chance', '100' ) )

        let id = _.parseInt( _.get( parameters, 'skillId', '0' ) )
        let level = _.parseInt( _.get( parameters, 'skillLevel', '0' ) )

        this.skill = {
            id,
            level
        }
        this.targetType = L2TargetType[ _.get( parameters, 'targetType', 'SELF' ) as string ]
        this.removeListenerMethod = this.removeListenerPredicate.bind( this )
        this.onAvoidEventMethod = this.onAvoidEvent.bind( this )
    }

    canStart(): boolean {
        if ( this.chance === 0 || !this.skill.id || !this.skill.level ) {
            return false
        }

        return _.random( 100 ) <= this.chance
    }

    async onAvoidEvent( eventData: CreatureAvoidAttackEvent ) : Promise<void> {
        if ( eventData.isDOT ) {
            return
        }

        let skill = getPossibleSkill( this.skill.id, this.skill.level )
        if ( !skill ) {
            return
        }

        let attacker: L2Character = L2World.getObjectById( eventData.attackerId ) as L2Character
        let target: L2Character = L2World.getObjectById( eventData.targetId ) as L2Character
        if ( ( this.targetType === L2TargetType.SELF && skill.getCastRange() > 0 )
                && ( attacker.calculateDistance( target, true ) > skill.getCastRange() ) ) {
            return
        }

        let triggerCasts: Array<Promise<void>> = skill
                .getTargetWithSelectedTarget( attacker, target )
                .reduce( ( promises: Array<Promise<void>>, target: L2Object ) => {
                    if ( target && target.isCharacter() && !( target as L2Character ).isInvulnerable() ) {
                        promises.push( attacker.makeTriggerCast( skill, ( target as L2Character ) ) )
                    }

                    return promises
                }, [] )

        if ( triggerCasts.length > 0 ) {
            await Promise.all( triggerCasts )
        }
    }

    async onExit( info: BuffInfo ) {
        ListenerCache.removeObjectListener( info.getAffectedId(), EventType.CreatureAvoidAttack, this.removeListenerMethod )
    }

    async onStart( info: BuffInfo ) {
        ListenerCache.addObjectListener( info.getAffectedId(), EventType.CreatureAvoidAttack, this, this.onAvoidEventMethod )
    }

    removeListenerPredicate( data: EventListenerData ): boolean {
        return data.owner === this
    }
}