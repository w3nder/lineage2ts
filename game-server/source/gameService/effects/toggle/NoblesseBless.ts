import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { EffectFlag } from '../../enums/EffectFlag'

export class NoblesseBless extends AbstractEffect {
    effectFlags = EffectFlag.NOBLESS_BLESSING
    effectType = L2EffectType.NoblesseBlessing

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return info.getEffector() && target && target.isPlayable()
    }
}