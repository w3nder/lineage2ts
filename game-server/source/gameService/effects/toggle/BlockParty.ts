import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { PunishmentManager } from '../../instancemanager/PunishmentManager'
import { PunishmentEffect } from '../../models/punishment/PunishmentEffect'
import { PunishmentType } from '../../models/punishment/PunishmentType'

export class BlockParty extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return target && target.isPlayer()
    }

    async onStart( info: BuffInfo ) {
        return PunishmentManager.startPunishment( info.getAffectedId().toString(), PunishmentEffect.CHARACTER, PunishmentType.PARTY_BAN, 0, 'Party banned by bot report', 'BlockParty', true )
    }

    async onExit( info: BuffInfo ) {
        return PunishmentManager.startPunishment( info.getAffectedId().toString(), PunishmentEffect.CHARACTER, PunishmentType.PARTY_BAN, 0, 'Party banned by bot report', 'BlockParty', true )
    }
}