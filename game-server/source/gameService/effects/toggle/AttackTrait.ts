import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { TraitType, TraitTypeValues } from '../../models/stats/TraitType'

export class AttackTrait extends AbstractEffect {
    traits: { [ key: string ]: number } = {}

    constructor( parameters: Object ) {
        super()

        let effect = this
        _.each( parameters, ( value: any, key: string ) => {
            effect.traits[ key ] = _.parseInt( value )
        } )
    }

    async onStart( info: BuffInfo ) {
        let stats = info.getAffected().getStat()

        _.each( this.traits, ( value: number, key: string ) => {
            let trait : TraitType = TraitTypeValues[ key ]
            stats.getAttackTraits()[ trait.id ] *= value
            stats.getAttackTraitsCount()[ trait.id ]++
        } )
    }

    async onExit( info: BuffInfo ) {
        let stats = info.getAffected().getStat()

        _.each( this.traits, ( value: number, key: string ) => {
            let trait : TraitType = TraitTypeValues[ key ]
            stats.getAttackTraits()[ trait.id ] /= value
            stats.getAttackTraitsCount()[ trait.id ]--
        } )
    }
}