import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { DataManager } from '../../../data/manager'
import { ExBRAgathionEnergyItem } from '../../packets/send/ExBRAgathionEnergyInfo'
import { InventorySlot } from '../../values/InventoryValues'
import _ from 'lodash'

export class ConsumeAgathionEnergy extends AbstractEffect {
    energy: number

    constructor( parameters: Object ) {
        super()
        this.energy = _.parseInt( _.get( parameters, 'energy', '0' ) )
        this.ticks = _.parseInt( _.get( parameters, 'ticks', '0' ) )
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        if ( info.getAffected().isDead() || !info.getAffected().isPlayer() ) {
            return false
        }

        let target = info.getAffected().getActingPlayer()
        let agathionInfo = DataManager.getAgathionData().getByNpcId( target.getAgathionId() )
        if ( !agathionInfo || !agathionInfo.energy ) {
            return false
        }

        let agathionItem = target.getInventory().getPaperdollItem( InventorySlot.LeftBracelet )
        if ( !agathionItem || ( agathionInfo.itemId !== agathionItem.getId() ) ) {
            return false
        }

        let amount = Math.round( this.energy * this.getTicksMultiplier() )
        if ( ( amount < 0 ) && ( ( agathionItem.getAgathionRemainingEnergy() + amount ) <= 0 ) ) {
            return false
        }

        agathionItem.setAgathionRemainingEnergy( agathionItem.getAgathionRemainingEnergy() + amount )
        target.sendOwnedData( ExBRAgathionEnergyItem( agathionItem ) )

        return true
    }
}