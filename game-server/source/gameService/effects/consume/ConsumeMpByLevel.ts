import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import _ from 'lodash'

export class ConsumeMpByLevel extends AbstractEffect {
    power: number

    constructor( parameters: Object ) {
        super()
        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.ticks = _.parseInt( _.get( parameters, 'ticks', '0' ) )
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        let target = info.getAffected()
        if ( target.isDead() ) {
            return false
        }

        let curentMp = target.getCurrentMp()
        let amount = this.power * this.getTicksMultiplier() * ( ( target.getLevel() - 1 ) / 7.5 )
        if ( ( amount < 0 ) && ( ( curentMp + amount ) <= 0 ) ) {
            if ( target.isPlayable() ) {
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SKILL_REMOVED_DUE_LACK_MP ) )
            }

            return false
        }

        target.setCurrentMp( Math.min( curentMp + amount, target.getMaxRecoverableMp() ) )
        return true
    }
}