import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ChangeWaitType, ChangeWaitTypeValue } from '../../packets/send/ChangeWaitType'
import { Revive } from '../../packets/send/Revive'
import _ from 'lodash'

export class ConsumeFakeDeath extends AbstractEffect {
    power: number
    effectType = L2EffectType.FakeDeath

    constructor( parameters: Object ) {
        super()
        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.ticks = _.parseInt( _.get( parameters, 'ticks', '0' ) )
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        let target = info.getAffected()
        if ( target.isDead() ) {
            return false
        }

        let damage = this.power * this.getTicksMultiplier()
        if ( ( damage < 0 ) && ( ( target.getCurrentMp() + damage ) <= 0 ) && target.isPlayable() ) {
            target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SKILL_REMOVED_DUE_LACK_MP ) )
            return false
        }

        target.setCurrentMp( Math.min( target.getCurrentMp() + damage, target.getMaxRecoverableMp() ) )
        return true
    }

    async onExit( info: BuffInfo ) {
        let target = info.getAffected()

        if ( target.isPlayer() ) {
            let player = target.getActingPlayer()
            player.setIsFakeDeath( false )
            player.setRecentFakeDeath( true )

            player.sendOwnedData( ChangeWaitType( target, ChangeWaitTypeValue.StoppingFakeDeath ) )
            player.sendOwnedData( Revive( target.getObjectId() ) )
        }
    }
}