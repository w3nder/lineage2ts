import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import _ from 'lodash'

export class ConsumeHp extends AbstractEffect {
    power: number

    constructor( parameters: Object ) {
        super()
        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.ticks = _.parseInt( _.get( parameters, 'ticks', '0' ) )
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        let target = info.getAffected()
        if ( target.isDead() ) {
            return false
        }

        let amount = this.power * this.getTicksMultiplier()
        let currentHp = target.getCurrentHp()
        if ( ( amount < 0 ) && ( ( currentHp + amount ) <= 0 ) ) {
            target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SKILL_REMOVED_DUE_LACK_HP ) )
            return false
        }

        target.setCurrentHp( Math.min( currentHp + amount, target.getMaxRecoverableHp() ) )
        return true
    }
}