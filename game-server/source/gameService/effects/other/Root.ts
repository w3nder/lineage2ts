import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectFlag } from '../../enums/EffectFlag'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'

export class Root extends AbstractEffect {
    effectFlags = EffectFlag.ROOTED
    effectType = L2EffectType.Root

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        target.abortMoving()
        return AIEffectHelper.notifyAttackedWithTargetId( target, info.getEffectorId(), 1, 1 )
    }
}