import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { PlayerPvpFlag } from '../../enums/PlayerPvpFlag'

export class Flag extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        return info.getAffected() && info.getAffected().isPlayer()
    }

    async onExit( buff: BuffInfo ) {
        buff.getAffected().getActingPlayer().updatePvPFlag( PlayerPvpFlag.None )
    }

    async onStart( info: BuffInfo ) {
        info.getAffected().getActingPlayer().updatePvPFlag( PlayerPvpFlag.Flagged )
    }
}