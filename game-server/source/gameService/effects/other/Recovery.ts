import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class Recovery extends AbstractEffect {
    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayer()
    }

    async onStart( info: BuffInfo ) {
        return info.getAffected().getActingPlayer().reduceDeathPenaltyBuffLevel()
    }
}