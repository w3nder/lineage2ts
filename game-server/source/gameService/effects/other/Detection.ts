import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { AbnormalType } from '../../models/skills/AbnormalType'

export class Detection extends AbstractEffect {

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let effector = info.getEffector()
        let affected = info.getAffected()

        if ( !effector.isPlayer() || !affected.isPlayer() ) {
            return
        }

        let player = effector.getActingPlayer()
        let target = affected.getActingPlayer()

        if ( target.isInvisible() ) {
            if ( player.isInPartyWith( target ) ) {
                return
            }
            if ( player.isInClanWith( target ) ) {
                return
            }
            if ( player.isInAllyWith( target ) ) {
                return
            }

            return target.getEffectList().stopSkillEffects( true, AbnormalType.HIDE )
        }
    }
}