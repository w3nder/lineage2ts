import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class Buff extends AbstractEffect {
    effectType = L2EffectType.Buff

    async onTick( info: BuffInfo ) : Promise<boolean> {
        let skill = info.getSkill()
        return skill.isPassive() || skill.isToggle()
    }
}