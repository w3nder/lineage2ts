import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ChestInstance } from '../../models/actor/instance/L2ChestInstance'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'

export class OpenChest extends AbstractEffect {

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        let player = info.getEffector()
        return target.isChest()
                && !target.isDead()
                && player.getInstanceId() === target.getInstanceId()
    }

    async onStart( info: BuffInfo ) : Promise<void> {
        let target = info.getAffected()
        let player: L2PcInstance = info.getEffector().getActingPlayer()
        let chest: L2ChestInstance = target as L2ChestInstance
        if ( player.getInstanceId() !== chest.getInstanceId() ) {
            return
        }

        if ( ( player.getLevel() <= 77 && Math.abs( chest.getLevel() - player.getLevel() ) <= 6 )
                || ( player.getLevel() >= 78 && Math.abs( chest.getLevel() - player.getLevel() ) <= 5 ) ) {

            player.broadcastSocialAction( 3 )
            chest.setSpecialDrop()
            chest.setMustRewardExpSp( false )

            return chest.reduceCurrentHp( chest.getMaxHp(), player, info.getSkill() )
        }

        player.broadcastSocialAction( 13 )

        AIEffectHelper.notifyAttacked( chest, player )
    }
}