import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectCalculationType } from '../../enums/EffectCalculationType'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'

export class TickMp extends AbstractEffect {
    power: number
    mode: EffectCalculationType

    constructor( parameters: Object ) {
        super()
        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.mode = EffectCalculationType[ _.get( parameters, 'mode', 'DIFF' ) as string ]
        this.ticks = _.get( parameters, 'ticks', 1 )
    }

    async onTick( info: BuffInfo ) : Promise<boolean> {
        let target: L2Character = info.getAffected()
        if ( target.isDead() ) {
            return false
        }

        let power = 0
        let mp = target.getCurrentMp()
        switch ( this.mode ) {
            case EffectCalculationType.DIFF: {
                power = this.power * this.getTicksMultiplier()
                break
            }
            case EffectCalculationType.PER: {
                power = mp * this.power * this.getTicksMultiplier()
                break
            }
        }

        if ( power < 0 ) {
            target.reduceCurrentMp( Math.abs( power ) )
        } else {
            let maxMp = target.getMaxRecoverableMp()

            // Not needed to set the MP and send update packet if player is already at max MP
            if ( mp >= maxMp ) {
                return true
            }

            target.setCurrentMp( Math.min( mp + power, maxMp ) )
        }

        return false
    }
}