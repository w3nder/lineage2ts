import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class StaticDamage extends AbstractEffect {
    power: number

    constructor( parameters: Object ) {
        super()

        this.power = _.parseInt( _.get( parameters, 'power', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        return !info.getEffector().isAlikeDead()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        let attacker = info.getEffector()
        await target.reduceCurrentHp( this.power, attacker, info.getSkill() )
        target.notifyDamageReceived( this.power, attacker, info.getSkill(), false, false, false )

        if ( attacker.isPlayer() ) {
            attacker.sendDamageMessage( target, this.power, false, false, false )
        }
    }
}