import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ConfirmDialog } from '../../packets/send/SystemMessage'

import _ from 'lodash'
import { PcRequestVariable, VariableNames } from '../../variables/VariableTypes'
import { PlayerVariablesManager } from '../../variables/PlayerVariablesManager'

export class CallPc extends AbstractEffect {
    itemId: number
    itemCount: number

    constructor( parameters: Object ) {
        super()
        this.itemId = _.parseInt( _.get( parameters, 'itemId', '0' ) )
        this.itemCount = _.parseInt( _.get( parameters, 'itemCount', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target : L2PcInstance = info.getAffected().getActingPlayer()
        let summoner : L2PcInstance = info.getEffector().getActingPlayer()

        if ( summoner && target && summoner.canSummonTarget( target ) ) {
            /*
                TODO : decide to add object pool
                - consider not using player variables since these can be automatically saved
             */
            let data : PcRequestVariable = {
                amount: this.itemCount,
                itemId: this.itemId,
                playerId: summoner.getObjectId(),
                expireTime: Date.now() + 30000
            }

            PlayerVariablesManager.set( target.getObjectId(), VariableNames.pcRequest, data )

            let packet = new ConfirmDialog( SystemMessageIds.C1_WISHES_TO_SUMMON_YOU_FROM_S2_DO_YOU_ACCEPT )
                    .addCharacterName( summoner )
                    .regionFromCoordinates( summoner.getX(), summoner.getY(), summoner.getZ() )
                    .addTime( 30000 )
                    .addRequesterId( summoner.getObjectId() )
                    .getBuffer()

            target.sendOwnedData( packet )
        }
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffectedId() !== info.getEffectorId()
    }
}