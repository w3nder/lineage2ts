import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Clan } from '../../models/L2Clan'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

export class ClanGate extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayer() && !!info.getAffected().getActingPlayer().getClan()
    }

    async onStart( info: BuffInfo ) {
        let player = info.getAffected().getActingPlayer()
        let clan: L2Clan = player.getClan()
        if ( clan ) {
            clan.broadcastDataToOtherOnlineMembers( SystemMessageBuilder.fromMessageId( SystemMessageIds.COURT_MAGICIAN_CREATED_PORTAL ), player )
        }
    }
}