import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { InventoryAction } from '../../enums/InventoryAction'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export class Restoration extends AbstractEffect {
    itemId: number
    amount: number

    constructor( parameters: Object ) {
        super()

        this.itemId = _.parseInt( _.get( parameters, 'itemId', '0' ) )
        this.amount = _.parseInt( _.get( parameters, 'itemCount', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target : L2Character = info.getAffected()
        return target && target.isPlayable()
    }

    async onStart( info: BuffInfo ) {
        let target : L2Character = info.getAffected()
        if ( this.itemId <= 0 || this.amount <= 0 ) {
            target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOTHING_INSIDE_THAT ) )
            return
        }

        if ( target.isPlayer() ) {
            await ( target as L2PcInstance ).addItem( this.itemId, this.amount, -1, info.getEffectorId(), InventoryAction.SkillProduced )
            return
        }

        if ( target.isPet() ) {
            await target.getInventory().addItem( this.itemId, this.amount, info.getEffectorId(), 'Effect:Restoration' )
        }
    }
}