import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { Stats } from '../../models/stats/Stats'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

import _ from 'lodash'

export class FocusSouls extends AbstractEffect {
    charge: number

    constructor( parameters: Object ) {
        super()

        this.charge = _.parseInt( _.get( parameters, 'charge', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return target.isPlayer() && !target.isAlikeDead()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target: L2PcInstance = info.getAffected().getActingPlayer()
        let maxSouls = target.calculateStat( Stats.MAX_SOULS, 0, null, null )

        if ( maxSouls > 0 ) {
            let amount = this.charge
            if ( ( target.getChargedSouls() < maxSouls ) ) {
                let count = ( ( target.getChargedSouls() + amount ) <= maxSouls ) ? amount : ( maxSouls - target.getChargedSouls() )
                target.increaseSouls( count )
                return
            }

            target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SOUL_CANNOT_BE_INCREASED_ANYMORE ) )
        }
    }
}