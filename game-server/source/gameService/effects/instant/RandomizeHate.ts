import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { L2Character } from '../../models/actor/L2Character'
import { AggroCache, AggroData } from '../../cache/AggroCache'
import _ from 'lodash'

export class RandomizeHate extends AbstractEffect {
    chance: number

    constructor( parameters: Object ) {
        super()

        this.chance = _.parseInt( _.get( parameters, 'power', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    calculateSuccess( info: BuffInfo ): boolean {
        return Formulas.calculateProbability( this.chance, info.getEffector(), info.getAffected(), info.getSkill() )
    }

    canStart( info: BuffInfo ): boolean {
        let target: L2Character = info.getAffected()
        return !( !target || info.getAffectedId() === info.getEffectorId() || !target.isAttackable() )
    }

    async onStart( info: BuffInfo ) {
        let amount: number = AggroCache.getAggroAmount( info.getAffectedId(), info.getEffectorId() )
        if ( amount <= 0 ) {
            return
        }

        let aggroItems: Array<AggroData> = _.filter( AggroCache.getOwnedData( info.getAffectedId() ), ( data: AggroData ) => {
            return data.attackerId !== info.getEffectorId() && data.hateAmount > 0
        } )

        if ( aggroItems.length === 0 ) {
            return
        }

        AggroCache.resetAggro( info.getAffectedId(), info.getEffectorId(), 0 )

        let data: AggroData = _.sample( aggroItems )
        data.hateAmount = data.hateAmount + amount
    }
}