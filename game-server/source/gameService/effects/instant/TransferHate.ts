import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { L2World } from '../../L2World'
import { AggroCache, AggroData, CharacterAggroData } from '../../cache/AggroCache'
import _ from 'lodash'

export class TransferHate extends AbstractEffect {
    chance: number

    constructor( parameters: Object ) {
        super()

        this.chance = _.parseInt( _.get( parameters, 'chance', '100' ) )
    }

    calculateSuccess( info: BuffInfo ): boolean {
        return Formulas.calculateProbability( this.chance, info.getEffector(), info.getAffected(), info.getSkill() )
    }

    canStart( info: BuffInfo ): boolean {
        return GeneralHelper.checkIfInRange( info.getSkill().getEffectRange(), info.getEffector(), info.getAffected(), true )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let attacker = info.getEffector()

        L2World.getVisibleObjectIds( attacker, info.getSkill().getAffectRange() ).forEach( ( objectId: number ) => {
            let characterData: CharacterAggroData = AggroCache.getOwnedData( objectId )
            if ( !characterData ) {
                return
            }

            let data: AggroData = characterData[ info.getEffectorId() ]
            if ( !data || data.hateAmount === 0 ) {
                return
            }

            AggroCache.addAggro( objectId, info.getAffectedId(), 0, data.hateAmount )
            data.hateAmount = 0
        } )
    }
}