import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { ShotType } from '../../enums/ShotType'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

export class PhysicalSoulAttack extends AbstractEffect {
    power: number
    criticalChance: number
    ignoreShieldDefence: boolean
    effectType = L2EffectType.PhysicalAttack

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.criticalChance = _.parseInt( _.get( parameters, 'criticalChance', '0' ) )
        this.ignoreShieldDefence = _.get( parameters, 'ignoreShieldDefence', false )
    }

    calculateSuccess( info: BuffInfo ): boolean {
        return !Formulas.calculatePhysicalSkillEvasion( info.getEffector(), info.getAffected(), info.getSkill() )
    }

    canStart( info: BuffInfo ): boolean {
        return !info.getEffector().isAlikeDead()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target: L2Character = info.getAffected()
        let attacker: L2Character = info.getEffector()
        let skill: Skill = info.getSkill()

        if ( target.isPlayer() && target.getActingPlayer().isFakeDeath() ) {
            await target.stopFakeDeath( true )
        }

        let ss = skill.isPhysical() && attacker.isChargedShot( ShotType.Soulshot )
        let shieldUse = 0

        if ( !this.ignoreShieldDefence ) {
            shieldUse = Formulas.calculateShieldUse( attacker, target, skill, true )
        }

        let crit = false
        if ( this.criticalChance > 0 ) {
            crit = Formulas.calculateSkillCrit( attacker, target, this.criticalChance )
        }

        let damage = Formulas.calculateSkillPhysicalDamage( attacker, target, skill, shieldUse, false, ss, this.power )

        if ( ( skill.getMaxSoulConsumeCount() > 0 ) && attacker.isPlayer() ) {
            damage *= 1 + ( info.getCharges() * 0.04 )
        }

        if ( crit ) {
            damage *= 2
        }

        if ( damage > 0 ) {
            attacker.sendDamageMessage( target, damage, false, crit, false )
            await target.reduceCurrentHp( damage, attacker, skill )
            target.notifyDamageReceived( damage, attacker, skill, crit, false, false )

            return Formulas.calculateDamageReflected( attacker, target, skill, crit )
        }

        if ( !attacker.isPlayable() ) {
            return
        }

        attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACK_FAILED ) )
    }
}