import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2TrapInstance } from '../../models/actor/instance/L2TrapInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

export class TrapRemove extends AbstractEffect {
    power: number

    constructor( parameters: Object ) {
        super()

        this.power = _.parseInt( _.get( parameters, 'power', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return target.isTrap() && !target.isAlikeDead()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let trap: L2TrapInstance = info.getAffected() as L2TrapInstance
        if ( !trap.canBeSeenBy( info.getEffector() ) ) {
            if ( info.getEffector().isPlayer() ) {
                info.getEffector().sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
            }
            return
        }

        if ( trap.getLevel() > this.power ) {
            return
        }

        trap.unSummon()

        if ( info.getEffector().isPlayer() ) {
            info.getEffector().sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.A_TRAP_DEVICE_HAS_BEEN_STOPPED ) )
        }
    }
}