import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2TrapInstance } from '../../models/actor/instance/L2TrapInstance'

export class TrapDetect extends AbstractEffect {
    power: number

    constructor( parameters: Object ) {
        super()

        this.power = _.parseInt( _.get( parameters, 'power', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return target.isTrap() && !target.isAlikeDead()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let trap: L2TrapInstance = info.getAffected() as L2TrapInstance
        if ( trap.getLevel() <= this.power ) {
            trap.setDetected( info.getEffector() )
        }
    }
}