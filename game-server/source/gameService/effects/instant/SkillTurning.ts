import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'

export class SkillTurning extends AbstractEffect {
    chance: number

    constructor( parameters: Object ) {
        super()

        this.chance = _.parseInt( _.get( parameters, 'chance', '100' ) )
    }

    isInstant(): boolean {
        return true
    }

    calculateSuccess( info: BuffInfo ): boolean {
        return Formulas.calculateProbability( this.chance, info.getEffector(), info.getAffected(), info.getSkill() )
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        if ( !target
                || info.getAffectedId() === info.getEffectorId()
                || target.isRaid() ) {
            return
        }

        target.attemptCastStop()
    }
}