import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { Formulas } from '../../models/stats/Formulas'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { ShotType } from '../../enums/ShotType'

import _ from 'lodash'

export class MagicalAttackMp extends AbstractEffect {
    power: number
    effectType = L2EffectType.MagicalAttack

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
    }

    canStart( info: BuffInfo ): boolean {
        return !info.getEffector().isAlikeDead()
    }

    isInstant(): boolean {
        return true
    }

    calculateSuccess( info: BuffInfo ): boolean {
        let target = info.getAffected()
        if ( target.isInvulnerable() || target.isMpBlocked() ) {
            return false
        }
        let attacker = info.getEffector()
        if ( !Formulas.calculateMagicAffected( attacker, target, info.getSkill() ) ) {
            if ( attacker.isPlayer() ) {
                attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACK_FAILED ) )
            }

            if ( target.isPlayer() ) {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_RESISTED_C2_DRAIN2 )
                        .addCharacterName( target )
                        .addCharacterName( attacker )
                        .getBuffer()
                target.sendOwnedData( packet )
            }

            return false
        }

        return true
    }

    async onStart( info: BuffInfo ) {
        let target: L2Character = info.getAffected()
        let attacker = info.getEffector()
        let skill: Skill = info.getSkill()

        let sps = skill.useSpiritShot() && attacker.isChargedShot( ShotType.Spiritshot )
        let bss = skill.useSpiritShot() && attacker.isChargedShot( ShotType.BlessedSpiritshot )
        let shieldUse = Formulas.calculateShieldUse( attacker, target, skill )
        let magicCrit = Formulas.calculateMagicCrit( attacker, target, skill )
        let damage = Formulas.calculateManaDamage( attacker, target, skill, shieldUse, sps, bss, magicCrit, this.power )
        let mp = ( damage > target.getCurrentMp() ? target.getCurrentMp() : damage )

        if ( damage > 0 ) {
            await target.stopEffectsOnDamage( true )
            target.setCurrentMp( target.getCurrentMp() - mp )
        }
    }
}