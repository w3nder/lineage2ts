import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { L2Character } from '../../models/actor/L2Character'
import { ShotType } from '../../enums/ShotType'

export class SoulBlow extends AbstractEffect {
    power: number
    chance: number
    effectType = L2EffectType.PhysicalAttack

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.chance = _.parseInt( _.get( parameters, 'chance', '0' ) )
    }

    calculateSuccess( info: BuffInfo ): boolean {
        let attacker = info.getEffector()
        let target = info.getAffected()
        return !Formulas.calculatePhysicalSkillEvasion( attacker, target, info.getSkill() )
                && Formulas.calculateBlowSuccess( attacker, target, info.getSkill(), this.chance )
    }

    canStart( info: BuffInfo ): boolean {
        return !info.getEffector().isAlikeDead()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target: L2Character = info.getAffected()
        let attacker: L2Character = info.getEffector()

        let useSoulShots = info.getSkill().useSoulShot() && attacker.isChargedShot( ShotType.Soulshot )
        let shieldUse = Formulas.calculateShieldUse( attacker, target, info.getSkill() )
        let damage = Formulas.calculateBlowDamage( attacker, target, info.getSkill(), shieldUse, useSoulShots, this.power )
        if ( ( info.getSkill().getMaxSoulConsumeCount() > 0 ) && attacker.isPlayer() ) {
            // Souls Formula (each soul increase +4%)
            damage *= 1 + ( info.getCharges() * 0.04 )
        }

        await target.reduceCurrentHp( damage, attacker, info.getSkill() )
        target.notifyDamageReceived( damage, attacker, info.getSkill(), false, false, false )

        if ( !target.isRaid() && Formulas.calculateAttackBreak( target, damage ) ) {
            target.notifyAttackInterrupted()
            target.attemptCastStop()
        }

        if ( attacker.isPlayer() ) {
            attacker.sendDamageMessage( target, damage, false, true, false )
        }

        return Formulas.calculateDamageReflected( attacker, target, info.getSkill(), true )
    }
}