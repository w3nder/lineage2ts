import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectCalculationType } from '../../enums/EffectCalculationType'
import { L2Character } from '../../models/actor/L2Character'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { DataManager } from '../../../data/manager'
import { ExBRAgathionEnergyItem } from '../../packets/send/ExBRAgathionEnergyInfo'

import _ from 'lodash'
import { InventorySlot } from '../../values/InventoryValues'

export class InstantAgathionEnergy extends AbstractEffect {
    energy: number
    mode: EffectCalculationType

    constructor( parameters: Object ) {
        super()

        this.energy = parseFloat( _.get( parameters, 'energy', '0' ) )
        this.mode = EffectCalculationType[ _.get( parameters, 'mode', 'DIFF' ) as string ]
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target: L2Character = info.getAffected()
        return !target.isDead() && target.isPlayer()
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected().getActingPlayer()
        let agathionInfo = DataManager.getAgathionData().getByNpcId( target.getAgathionId() )
        if ( !agathionInfo || !agathionInfo.energy ) {
            return
        }

        let agathionItem = target.getInventory().getPaperdollItem( InventorySlot.LeftBracelet )
        if ( !agathionItem || ( agathionInfo.itemId !== agathionItem.getId() ) ) {
            return
        }

        let agathionEnergy = 0
        switch ( this.mode ) {
            case EffectCalculationType.DIFF:
                agathionEnergy = Math.round( Math.max( 0, agathionItem.getAgathionRemainingEnergy() + this.energy ) )
                break
            case EffectCalculationType.PER:
                agathionEnergy = Math.round( ( agathionItem.getAgathionRemainingEnergy() * this.energy ) / 100.0 )
                break
        }

        agathionItem.setAgathionRemainingEnergy( agathionEnergy )
        target.sendOwnedData( ExBRAgathionEnergyItem( agathionItem ) )
    }
}