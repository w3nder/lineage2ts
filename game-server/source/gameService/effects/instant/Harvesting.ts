import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2MonsterInstance } from '../../models/actor/instance/L2MonsterInstance'
import { PacketDispatcher } from '../../PacketDispatcher'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

import _ from 'lodash'
import { ItemData } from '../../interface/ItemData'

export class Harvesting extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        let attacker = info.getEffector()
        let target = info.getAffected()
        return attacker
                && target
                && attacker.isPlayer()
                && target.isMonster()
                && target.isDead()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let monster: L2MonsterInstance = info.getAffected() as L2MonsterInstance
        let playerId = info.getEffectorId()

        if ( playerId !== monster.getSeederId() ) {
            PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_HARVEST ) )
            return
        }

        if ( monster.isSeeded() ) {
            let player: L2PcInstance = info.getEffector().getActingPlayer()
            if ( this.shouldHarvest( player, monster ) ) {
                let item: ItemData = monster.getHarvestItem()
                if ( item ) {
                    await player.getInventory().addItem( item.itemId, item.amount, monster.objectId, 'Effect:Harvesting' )

                    let message: Buffer
                    if ( item.amount === 1 ) {
                        message = new SystemMessageBuilder( SystemMessageIds.YOU_PICKED_UP_S1 )
                                .addItemNameWithId( item.itemId )
                                .getBuffer()
                    } else {
                        message = new SystemMessageBuilder( SystemMessageIds.YOU_PICKED_UP_S1 )
                                .addItemNameWithId( item.itemId )
                                .addNumber( item.amount )
                                .getBuffer()
                    }

                    PacketDispatcher.sendOwnedData( playerId, message )

                    if ( player.isInParty() ) {
                        let partyPacket: Buffer
                        if ( item.amount === 1 ) {
                            partyPacket = new SystemMessageBuilder( SystemMessageIds.C1_HARVESTED_S2S )
                                    .addString( player.getName() )
                                    .addItemNameWithId( item.itemId )
                                    .getBuffer()
                        } else {
                            partyPacket = new SystemMessageBuilder( SystemMessageIds.C1_HARVESTED_S2S )
                                    .addString( player.getName() )
                                    .addNumber( item.amount )
                                    .addItemNameWithId( item.itemId )
                                    .getBuffer()
                        }

                        player.getParty().broadcastDataToPartyMembers( player.objectId, partyPacket )
                    }

                    monster.emptyHarvestItem()
                }
            }
            return PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_HARVEST_HAS_FAILED ) )
        }

        return PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_HARVEST_FAILED_BECAUSE_THE_SEED_WAS_NOT_SOWN ) )
    }

    shouldHarvest( player: L2PcInstance, monster: L2MonsterInstance ) {
        let levelPlayer = player.getLevel()
        let levelTarget = monster.getLevel()

        let levelDifference = ( levelPlayer - levelTarget )
        if ( levelDifference < 0 ) {
            levelDifference = -levelDifference
        }

        // apply penalty, monster <=> player levels
        // 5% penalty for each level
        let rate = 100
        if ( levelDifference > 5 ) {
            rate -= ( levelDifference - 5 ) * 5
        }

        // success rate can't be less than 1%
        if ( rate < 1 ) {
            rate = 1
        }

        return _.random( 99 ) < rate
    }
}