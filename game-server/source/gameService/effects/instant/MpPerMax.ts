import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { L2Character } from '../../models/actor/L2Character'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import _ from 'lodash'

export class MpPerMax extends AbstractEffect {
    power : number
    effectType = L2EffectType.MpChangeByPercent

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target : L2Character = info.getAffected()
        return target && !target.isDead() && !target.isDoor()
    }

    async onStart( info: BuffInfo ) {
        let target : L2Character = info.getAffected()
        let full = this.power === 100

        let amount = full ? target.getMaxMp() : ( target.getMaxMp() * this.power ) / 100
        amount = Math.max( Math.min( amount, target.getMaxRecoverableMp() - target.getCurrentMp() ), 0 )

        if ( amount !== 0 ) {
            target.setCurrentMp( amount + target.getCurrentMp() )
        }

        if ( !target.isPlayable() ) {
            return
        }

        if ( info.getEffectorId() !== target.getObjectId() ) {
            let packet : Buffer = new SystemMessageBuilder( SystemMessageIds.S2_MP_HAS_BEEN_RESTORED_BY_C1 )
                    .addCharacterName( info.getEffector() )
                    .addNumber( amount )
                    .getBuffer()
            return target.sendOwnedData( packet )
        }

        let packet : Buffer = new SystemMessageBuilder( SystemMessageIds.S1_MP_HAS_BEEN_RESTORED )
                .addNumber( amount )
                .getBuffer()
        return target.sendOwnedData( packet )
    }
}