import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { ShotType } from '../../enums/ShotType'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { CrystalType } from '../../models/items/type/CrystalType'
import { Stats } from '../../models/stats/Stats'
import { Formulas } from '../../models/stats/Formulas'
import { PacketDispatcher } from '../../PacketDispatcher'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

import _ from 'lodash'

export class Heal extends AbstractEffect {
    power: number
    effectType = L2EffectType.Hp

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target : L2Character = info.getAffected()
        if ( !target || target.isDead() || target.isDoor() || target.isInvulnerable() ) {
            return false
        }

        return true
    }

    async onStart( info: BuffInfo ) {
        let target : L2Character = info.getAffected()
        let attacker : L2Character = info.getEffector()
        let skill : Skill = info.getSkill()

        let amount = this.power
        let staticShotBonus = 0
        let magicAttackMultiplier = 1
        let sps = skill.isMagic() && attacker.isChargedShot( ShotType.Spiritshot )
        let bss = skill.isMagic() && attacker.isChargedShot( ShotType.BlessedSpiritshot )

        if ( ( ( sps || bss ) && ( attacker.isPlayer() && attacker.getActingPlayer().isMageClass() ) ) || attacker.isSummon() ) {
            staticShotBonus = skill.getFinishCostMp()
            magicAttackMultiplier = bss ? 4 : 2
            staticShotBonus *= bss ? 2.4 : 1.0
        } else if ( ( sps || bss ) && attacker.isNpc() ) {
            staticShotBonus = 2.4 * skill.getFinishCostMp()
            magicAttackMultiplier = 4
        } else {
            let weaponInst : L2ItemInstance = attacker.getActiveWeaponInstance()
            if ( weaponInst ) {
                magicAttackMultiplier = weaponInst.getItem().getItemGrade() === CrystalType.S84 ? 4 : weaponInst.getItem().getItemGrade() === CrystalType.S80 ? 2 : 1
            }

            magicAttackMultiplier = bss ? magicAttackMultiplier * 4 : magicAttackMultiplier + 1
        }

        if ( !skill.isStatic() ) {
            amount += staticShotBonus + Math.sqrt( magicAttackMultiplier * attacker.getMagicAttack( attacker, null ) )
            amount = target.calculateStat( Stats.HEAL_EFFECT, amount, null, null )

            if ( skill.isMagic() && Formulas.calculateMagicCrit( attacker, target, skill ) ) {
                amount *= 3
            }
        }


        amount = Math.max( Math.min( amount, target.getMaxRecoverableHp() - target.getCurrentHp() ), 0 )
        if ( amount !== 0 ) {
            target.setCurrentHp( amount + target.getCurrentHp() )
        }

        if ( target.isPlayer() ) {
            if ( skill.getId() === 4051 ) {
                return target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.REJUVENATING_HP ) )
            }

            if ( attacker.isPlayer() && ( attacker.objectId !== target.objectId ) ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S2_HP_HAS_BEEN_RESTORED_BY_C1 )
                        .addString( attacker.getName() )
                        .addNumber( amount )
                        .getBuffer()
                return PacketDispatcher.sendOwnedData( target.objectId, packet )
            }

            let packet = new SystemMessageBuilder( SystemMessageIds.S1_HP_HAS_BEEN_RESTORED )
                    .addNumber( amount )
                    .getBuffer()
            PacketDispatcher.sendOwnedData( target.objectId, packet )
        }
    }
}