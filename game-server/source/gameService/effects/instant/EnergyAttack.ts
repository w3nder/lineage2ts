import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas, FormulasValues } from '../../models/stats/Formulas'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { ShotType } from '../../enums/ShotType'
import { L2Weapon } from '../../models/items/L2Weapon'
import { WeaponType } from '../../models/items/type/WeaponType'
import { Stats } from '../../models/stats/Stats'

import _ from 'lodash'

export class EnergyAttack extends AbstractEffect {
    power: number
    criticalChance: number
    ignoreShieldDefence: boolean
    effectType = L2EffectType.PhysicalAttack

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.criticalChance = _.parseInt( _.get( parameters, 'criticalChance', '0' ) )
        this.ignoreShieldDefence = _.get( parameters, 'ignoreShieldDefence', false )
    }

    calculateSuccess( info: BuffInfo ): boolean {
        return !Formulas.calculatePhysicalSkillEvasion( info.getEffector(), info.getAffected(), info.getSkill() )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return info.getEffector().isPlayer()
    }

    async onStart( info: BuffInfo ) {
        let attacker : L2PcInstance = info.getEffector().getActingPlayer()

        let target : L2Character = info.getAffected()
        let skill : Skill = info.getSkill()

        let attack = attacker.getPowerAttack( target )
        let defence = target.getPowerDefence( attacker )

        if ( !this.ignoreShieldDefence ) {
            let shield = Formulas.calculateShieldUse( attacker, target, skill, true )
            switch ( shield ) {
                case FormulasValues.SHIELD_DEFENSE_FAILED: {
                    break
                }
                case FormulasValues.SHIELD_DEFENSE_SUCCEED: {
                    defence += target.getShieldDefence()
                    break
                }
                case FormulasValues.SHIELD_DEFENSE_PERFECT_BLOCK: {
                    defence = -1
                    break
                }
            }
        }

        let damage = 1
        let critical = false

        if ( defence !== -1 ) {
            let damageMultiplier = Formulas.calculateWeaponTraitBonus( attacker, target ) * Formulas.calculateAttributeBonus( attacker, target, skill ) * Formulas.calculateGeneralTraitBonus( attacker, target, skill.getTraitType(), true )

            let ss = info.getSkill().useSoulShot() && attacker.isChargedShot( ShotType.Soulshot )
            let ssBoost = ss ? 2 : 1.0

            let weaponTypeBoost
            let weapon : L2Weapon = attacker.getActiveWeaponItem()
            if ( weapon && ( ( weapon.getItemType() === WeaponType.BOW ) || ( weapon.getItemType() === WeaponType.CROSSBOW ) ) ) {
                weaponTypeBoost = 70
            } else {
                weaponTypeBoost = 77
            }

            let energyChargesBoost = ( ( ( attacker.getCharges() + skill.getChargeConsume() ) - 1 ) * 0.2 ) + 1

            attack += this.power
            attack *= ssBoost
            attack *= energyChargesBoost
            attack *= weaponTypeBoost

            damage = attack / defence
            damage *= damageMultiplier

            if ( target.isPlayer() ) {
                damage *= attacker.getStat().calculateStat( Stats.PVP_PHYS_SKILL_DMG, 1.0 )
                damage *= target.getStat().calculateStat( Stats.PVP_PHYS_SKILL_DEF, 1.0 )
                damage = attacker.getStat().calculateStat( Stats.PHYSICAL_SKILL_POWER, damage )
            }

            if ( this.criticalChance > 0 ) {
                critical = Formulas.calculateSkillCrit( attacker, target, this.criticalChance )
            }

            if ( critical ) {
                damage *= 2
            }
        }

        if ( damage > 0 ) {
            attacker.sendDamageMessage( target, damage, false, critical, false )
            await target.reduceCurrentHp( damage, attacker, skill )
            target.notifyDamageReceived( damage, attacker, skill, critical, false, false )

            return Formulas.calculateDamageReflected( attacker, target, skill, critical )
        }
    }
}