import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2World } from '../../L2World'

export class CallParty extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        return !info.getEffector().isInParty()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let attacker = info.getEffector()
        let attackerPlayer = attacker.getActingPlayer()

        attacker.getParty().getMembers().forEach( ( memberId: number ) => {
            let player = L2World.getPlayer( memberId )
            if ( attackerPlayer.canSummonTarget( player ) ) {
                player.teleportToLocation( attacker, true )
            }
        } )
    }
}