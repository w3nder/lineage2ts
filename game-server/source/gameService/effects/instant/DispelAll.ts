import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class DispelAll extends AbstractEffect {
    effectType = L2EffectType.Dispell

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        return info.getAffected().stopAllEffects()
    }
}