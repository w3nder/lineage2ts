import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2Character } from '../../models/actor/L2Character'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2ExtractableProductItem, L2ExtractableSkill } from '../../models/L2ExtractableSkill'
import { PacketDispatcher } from '../../PacketDispatcher'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { ItemData } from '../../interface/ItemData'
import { DataManager } from '../../../data/manager'
import { L2Item } from '../../models/items/L2Item'
import { PlayerVariablesManager } from '../../variables/PlayerVariablesManager'
import { MultipliersVariableNames, PremiumMultipliersPropertyName } from '../../values/L2PcValues'
import { InventoryAction } from '../../enums/InventoryAction'
import aigle from 'aigle'
import _ from 'lodash'

/*
    Restoration meaning it extracts (restores to original state) items that were packed in a skill.
    Such skill usually applies to items that are double-clicked in inventory to produce yet another item.
 */
export class RestorationRandom extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        let target: L2Character = info.getAffected()
        let attacker: L2Character = info.getEffector()
        if ( !attacker || !target || !attacker.isPlayer() || !target.isPlayer() ) {
            return false
        }

        let data: L2ExtractableSkill = info.getSkill().getExtractableSkill()
        return data && data.products.length > 0
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        /*
            Extractable chance contains fine-grained float based numbers
         */
        let masterChance: number = 100 * Math.random()
        let modifier: number = 0

        let skill: L2ExtractableSkill = info.getSkill().getExtractableSkill()
        let product: L2ExtractableProductItem = skill.products.find( ( item: L2ExtractableProductItem ): boolean => {
            let currentChance = item.chance
            if ( masterChance >= modifier && masterChance <= ( currentChance + modifier ) ) {
                return true
            }

            modifier += currentChance
            return false
        } )

        if ( !product ) {
            PacketDispatcher.sendOwnedData( info.getAffectedId(), SystemMessageBuilder.fromMessageId( SystemMessageIds.NOTHING_INSIDE_THAT ) )
            return
        }

        let player: L2PcInstance = info.getAffected().getActingPlayer()
        await aigle.resolve( product.items ).eachSeries( ( data: ItemData ) => {
            let amount = Math.floor( data.amount * Helper.getItemRate( data.itemId ) * Helper.getPlayerAmountBonus( player.getObjectId() ) )
            if ( amount === 0 ) {
                return
            }

            return player.addItem( data.itemId, amount, -1, info.getEffectorId(), InventoryAction.SkillProduced )
        } )
    }
}

const Helper = {
    getItemRate( itemId: number ): number {
        let item: L2Item = DataManager.getItems().getTemplate( itemId )

        if ( item.isScroll() ) {
            return ConfigManager.rates.getExtractableScrollMultiplier()
        }

        if ( item.isPotion() ) {
            return ConfigManager.rates.getExtractablePotionMultiplier()
        }

        if ( item.isRecipe() ) {
            return ConfigManager.rates.getExtractableRecipeMultiplier()
        }

        if ( item.isArmorItem() ) {
            return ConfigManager.rates.getExtractableArmorMultiplier()
        }

        if ( item.isWeaponItem() ) {
            return ConfigManager.rates.getExtractableWeaponMultiplier()
        }

        return ConfigManager.rates.getRateExtractable()
    },

    getPlayerAmountBonus( playerId: number ): number {
        return _.get( PlayerVariablesManager.get( playerId, PremiumMultipliersPropertyName ), MultipliersVariableNames.extractableItems, 1 ) as number
    },
}