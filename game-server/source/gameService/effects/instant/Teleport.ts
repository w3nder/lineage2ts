import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { teleportCharacterToGeometryCoordinates, } from '../../helpers/TeleportHelper'
import { GeometryId } from '../../enums/GeometryId'

export class Teleport extends AbstractEffect {
    x: number
    y: number
    z: number
    effectType = L2EffectType.Teleport

    constructor( parameters: Object ) {
        super()

        this.x = _.parseInt( _.get( parameters, 'x', '0' ) )
        this.y = _.parseInt( _.get( parameters, 'y', '0' ) )
        this.z = _.parseInt( _.get( parameters, 'z', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    onStart( info: BuffInfo ) : Promise<void> {
        return teleportCharacterToGeometryCoordinates( GeometryId.SpellTeleport, info.getAffected(), this.x, this.y, this.z )
    }
}