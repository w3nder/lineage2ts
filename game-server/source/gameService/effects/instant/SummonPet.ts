import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2PetData } from '../../models/L2PetData'
import { DataManager } from '../../../data/manager'
import { PlayerVariablesManager } from '../../variables/PlayerVariablesManager'
import { PetRequestVariable, VariableNames } from '../../variables/VariableTypes'
import { CharacterSummonCache } from '../../cache/CharacterSummonCache'

export class SummonPet extends AbstractEffect {

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        let attacker = info.getEffector()
        return !( !attacker || !target || !attacker.isPlayer() || !target.isPlayer() || target.isAlikeDead() )
    }

    async onStart( info: BuffInfo ) {
        let player: L2PcInstance = info.getEffector().getActingPlayer()

        if ( player.hasSummon() || player.isMounted() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ALREADY_HAVE_A_PET ) )
            return
        }

        let data = PlayerVariablesManager.get( player.getObjectId(), VariableNames.petRequest ) as PetRequestVariable
        if ( !data ) {
            return
        }

        PlayerVariablesManager.remove( player.getObjectId(), VariableNames.petRequest )

        let petData: L2PetData = DataManager.getPetData().getPetDataByItemId( data.itemId )
        if ( !petData ) {
            return
        }

        let item : L2ItemInstance = player.getInventory().getItemByObjectId( data.objectId )
        if ( !item ) {
            return
        }

        await CharacterSummonCache.restorePet( petData, player, item )
    }
}