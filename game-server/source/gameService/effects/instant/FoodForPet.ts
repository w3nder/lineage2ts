import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { MountType } from '../../enums/MountType'
import _ from 'lodash'

export class FoodForPet extends AbstractEffect {
    normal: number
    ride: number
    wyvern: number

    constructor( parameters: Object ) {
        super()

        this.normal = _.parseInt( _.get( parameters, 'normal', '0' ) )
        this.ride = _.parseInt( _.get( parameters, 'ride', '0' ) )
        this.wyvern = _.parseInt( _.get( parameters, 'wyvern', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let character: L2Character = info.getEffector()

        if ( character.isPet() ) {
            let pet = character as L2PetInstance
            pet.setCurrentFeed( pet.getCurrentFeed() + ( this.normal * ConfigManager.rates.getPetFoodRate() ) )
            return
        }

        if ( !character.isPlayer() ) {
            return
        }

        let player: L2PcInstance = character.getActingPlayer()
        let mountType = player.getMountType()
        if ( mountType === MountType.NONE ) {
            return
        }

        if ( mountType === MountType.WYVERN && this.wyvern !== 0 ) {
            player.setCurrentFeed( player.getCurrentFeed() + this.wyvern )
            return
        }

        if ( this.ride !== 0 ) {
            player.setCurrentFeed( player.getCurrentFeed() + this.ride )
        }
    }
}