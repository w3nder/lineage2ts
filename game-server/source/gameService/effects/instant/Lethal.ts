import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { Formulas } from '../../models/stats/Formulas'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import _ from 'lodash'
import { PlayerPermission } from '../../enums/PlayerPermission'

export class Lethal extends AbstractEffect {
    fullValue: number
    halfValue: number

    constructor( parameters: Object ) {
        super()

        this.fullValue = parseFloat( _.get( parameters, 'fullLethal', '0' ) )
        this.halfValue = parseFloat( _.get( parameters, 'halfLethal', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let attacker: L2Character = info.getEffector()
        if ( attacker.isPlayer() && !attacker.getAccessLevel().hasPermission( PlayerPermission.GiveDamage ) ) {
            return
        }

        let target: L2Character = info.getAffected()
        let skill: Skill = info.getSkill()
        if ( skill.getMagicLevel() < ( target.getLevel() - 6 ) ) {
            return
        }

        if ( !target.isLethalable() || target.isInvulnerable() ) {
            return
        }

        let chanceMultiplier = Formulas.calculateAttributeBonus( attacker, target, skill ) * Formulas.calculateGeneralTraitBonus( attacker, target, skill.getTraitType(), false )
        // Lethal Strike
        if ( _.random( 100 ) < ( this.fullValue * chanceMultiplier ) ) {
            // for Players CP and HP is set to 1.
            if ( target.isPlayer() ) {
                target.notifyDamageReceived( target.getCurrentHp() - 1, attacker, skill, true, false, false )
                target.setCurrentCp( 1 )
                target.setCurrentHp( 1 )
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LETHAL_STRIKE ) )
            } else if ( target.isMonster() || target.isSummon() ) {
                target.notifyDamageReceived( target.getCurrentHp() - 1, attacker, skill, true, false, false )
                target.setCurrentHp( 1 )
            }

            attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LETHAL_STRIKE_SUCCESSFUL ) )

            return
        }

        if ( _.random( 100 ) < ( this.halfValue * chanceMultiplier ) ) {
            if ( target.isPlayer() ) {
                target.setCurrentCp( 1 )
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.HALF_KILL ) )
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CP_DISAPPEARS_WHEN_HIT_WITH_A_HALF_KILL_SKILL ) )
            } else if ( target.isMonster() || target.isSummon() ) {
                target.notifyDamageReceived( target.getCurrentHp() * 0.5, attacker, skill, true, false, false )
                target.setCurrentHp( target.getCurrentHp() * 0.5 )
            }

            attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.HALF_KILL ) )
        }
    }
}