import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'

export class RunAway extends AbstractEffect {
    power: number
    time: number

    constructor( parameters: Object ) {
        super()

        this.power = _.parseInt( _.get( parameters, 'power', '0' ) )
        this.time = _.parseInt( _.get( parameters, 'time', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isAttackable() && _.random( 100 ) > this.power
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        if ( target.canAbortCast() ) {
            target.abortCast()
        }

        return AIEffectHelper.notifyFlee( target, this.time )
    }
}