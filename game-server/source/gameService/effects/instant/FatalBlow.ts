import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { ShotType } from '../../enums/ShotType'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

import _ from 'lodash'

export class FatalBlow extends AbstractEffect {
    power: number
    blowChance: number
    criticalChance: number
    effectType = L2EffectType.PhysicalAttack

    constructor( parameters: Object ) {
        super()

        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.criticalChance = _.parseInt( _.get( parameters, 'criticalChance', '0' ) )
        this.blowChance = _.parseInt( _.get( parameters, 'blowChance', '0' ) )
    }

    calculateSuccess( info: BuffInfo ): boolean {
        let attacker = info.getEffector()
        let target = info.getAffected()
        return !Formulas.calculatePhysicalSkillEvasion( attacker, target, info.getSkill() )
                && Formulas.calculateBlowSuccess( attacker, target, info.getSkill(), this.blowChance )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return !info.getEffector().isAlikeDead()
    }

    async onStart( info: BuffInfo ) {
        let target : L2Character = info.getAffected()
        let attacker : L2Character = info.getEffector()
        let skill : Skill = info.getSkill()

        if ( attacker.isAlikeDead() ) {
            return
        }

        let usingSoulsShots : boolean = skill.useSoulShot() && attacker.isChargedShot( ShotType.Soulshot )
        let shieldUse = Formulas.calculateShieldUse( attacker, target, skill )
        let damage = Formulas.calculateBlowDamage( attacker, target, skill, shieldUse, usingSoulsShots, this.power )

        let isCritical : boolean = false
        if ( this.criticalChance > 0 ) {
            isCritical = Formulas.calculateSkillCrit( attacker, target, this.criticalChance )
        }

        if ( isCritical ) {
            damage *= 2
        }

        await target.reduceCurrentHp( damage, attacker, skill )
        target.notifyDamageReceived( damage, attacker, skill, isCritical, false, false )

        if ( !target.isRaid() && Formulas.calculateAttackBreak( target, damage ) ) {
            target.notifyAttackInterrupted()
            target.attemptCastStop()
        }

        if ( attacker.isPlayer() ) {
            let player : L2PcInstance = attacker.getActingPlayer()
            player.sendDamageMessage( target, damage, false, isCritical, false )
        }

        return Formulas.calculateDamageReflected( attacker, target, skill, isCritical )
    }
}