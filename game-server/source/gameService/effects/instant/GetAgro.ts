import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Attackable } from '../../models/actor/L2Attackable'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'

export class GetAgro extends AbstractEffect {
    effectType = L2EffectType.Aggression

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) : Promise<void> {
        let target = info.getAffected()
        if ( ( target.isAttackable() )
                && target.hasAIController()
                && ( ( target as L2Attackable ).getMostHated().getObjectId() !== info.getEffectorId() ) ) {

            return AIEffectHelper.notifyAttackedWithTargetId( target as L2Attackable, info.getEffectorId(), 0, 1 )
        }
    }
}