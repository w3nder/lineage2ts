import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { L2Character } from '../../models/actor/L2Character'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { PacketDispatcher } from '../../PacketDispatcher'

import _ from 'lodash'

export class HpPerMax extends AbstractEffect {
    power: number
    effectType = L2EffectType.Hp

    constructor( parameters: Object ) {
        super()

        this.power = _.parseInt( _.get( parameters, 'power', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target: L2Character = info.getAffected()
        return target && !target.isDead() && !target.isDoor()
    }

    async onStart( info: BuffInfo ) {
        let target: L2Character = info.getAffected()
        let full = this.power === 100

        let amount = full ? target.getMaxHp() : ( target.getMaxHp() * this.power ) / 100.0
        amount = Math.max( Math.floor( Math.min( amount, target.getMaxRecoverableHp() - target.getCurrentHp() ) ), 0 )

        if ( amount !== 0 ) {
            target.setCurrentHp( amount + target.getCurrentHp() )
        }

        if ( info.getEffectorId() !== target.getObjectId() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S2_HP_HAS_BEEN_RESTORED_BY_C1 )
                    .addCharacterName( info.getEffector() )
                    .addNumber( amount )
                    .getBuffer()
            return PacketDispatcher.sendOwnedData( target.getObjectId(), packet )
        }

        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_HP_HAS_BEEN_RESTORED )
                .addNumber( amount )
                .getBuffer()
        PacketDispatcher.sendOwnedData( target.getObjectId(), packet )
    }
}