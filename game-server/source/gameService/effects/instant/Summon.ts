import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CharacterSummonCache } from '../../cache/CharacterSummonCache'
import _ from 'lodash'
import { ItemDefinition } from '../../interface/ItemDefinition'

export class Summon extends AbstractEffect {
    npcId: number
    expMultiplier: number
    consumeItem: ItemDefinition
    lifeTime: number
    consumeInterval: number

    constructor( parameters: Object ) {
        super()

        this.npcId = _.parseInt( _.get( parameters, 'npcId', '0' ) )
        this.expMultiplier = parseFloat( _.get( parameters, 'expMultiplier', '1' ) )

        let id = _.parseInt( _.get( parameters, 'consumeItemId', '0' ) )
        let count = _.parseInt( _.get( parameters, 'consumeItemCount', '1' ) )
        this.consumeItem = {
            id,
            count
        }

        this.consumeInterval = _.parseInt( _.get( parameters, 'consumeItemInterval', '0' ) )
        this.lifeTime = _.parseInt( _.get( parameters, 'lifeTime', '3600' ) ) * 1000
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return target.isPlayer() && !target.hasSummon()
    }

    async onStart( info: BuffInfo ) {
        let player : L2PcInstance = info.getAffected().getActingPlayer()
        return CharacterSummonCache.createServitor( this.npcId, this.expMultiplier, this.lifeTime, this.consumeItem, this.consumeInterval, info.getSkill().getId(), player )
    }
}