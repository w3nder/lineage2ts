import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PacketDispatcher } from '../../PacketDispatcher'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { UserInfo } from '../../packets/send/UserInfo'
import { ExVoteSystemInfoWithPlayer } from '../../packets/send/ExVoteSystemInfo'
import _ from 'lodash'

export class GiveRecommendation extends AbstractEffect {
    amount: number

    constructor( parameters: Object ) {
        super()

        this.amount = _.parseInt( _.get( parameters, 'amount', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayer()
    }

    async onStart( info: BuffInfo ) {
        let target : L2PcInstance = info.getAffected().getActingPlayer()
        let recommendationsGiven = this.amount

        if ( ( target.getRecommendationsHave() + this.amount ) >= 255 ) {
            recommendationsGiven = 255 - target.getRecommendationsHave()
        }

        if ( recommendationsGiven > 0 ) {
            target.setRecommendationsHave( target.getRecommendationsHave() + recommendationsGiven )

            let packet : Buffer = new SystemMessageBuilder( SystemMessageIds.YOU_OBTAINED_S1_RECOMMENDATIONS )
                    .addNumber( recommendationsGiven )
                    .getBuffer()

            target.sendOwnedData( packet )
            target.sendDebouncedPacket( UserInfo )
            target.sendOwnedData( ExVoteSystemInfoWithPlayer( target ) )
            return
        }

        PacketDispatcher.sendOwnedData( info.getEffectorId(), SystemMessageBuilder.fromMessageId( SystemMessageIds.NOTHING_HAPPENED ) )
    }
}