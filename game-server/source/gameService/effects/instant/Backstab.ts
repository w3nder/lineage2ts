import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Formulas } from '../../models/stats/Formulas'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { ShotType } from '../../enums/ShotType'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import _ from 'lodash'

export class Backstab extends AbstractEffect {
    power: number
    blowChance: number
    criticalChance: number
    effectType = L2EffectType.PhysicalAttack

    constructor( parameters: Object ) {
        super()
        this.power = parseFloat( _.get( parameters, 'power', '0' ) )
        this.blowChance = _.parseInt( _.get( parameters, 'blowChance', '0' ) )
        this.criticalChance = _.parseInt( _.get( parameters, 'criticalChance', '0' ) )
    }

    calculateSuccess( info: BuffInfo ): boolean {
        let target = info.getAffected()
        let attacker = info.getEffector()

        return !attacker.isInFrontOf( target )
                && !Formulas.calculatePhysicalSkillEvasion( attacker, target, info.getSkill() )
                && Formulas.calculateBlowSuccess( attacker, target, info.getSkill(), this.blowChance )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        if ( info.getEffector().isAlikeDead() ) {
            return
        }

        let target: L2Character = info.getAffected()
        let attacker: L2Character = info.getEffector()
        let skill: Skill = info.getSkill()
        let isCharged: boolean = skill.useSoulShot() && attacker.isChargedShot( ShotType.Soulshot )
        let shieldUse = Formulas.calculateShieldUse( attacker, target, skill )
        let damage = Formulas.calculateBackstabDamage( attacker, target, skill, shieldUse, isCharged, this.power )

        if ( Formulas.calculateSkillCrit( attacker, target, this.criticalChance ) ) {
            damage *= 2
        }

        await target.reduceCurrentHp( damage, attacker, skill )
        target.notifyDamageReceived( damage, attacker, skill, true, false, false )

        if ( !target.isRaid() && Formulas.calculateAttackBreak( target, damage ) ) {
            target.notifyAttackInterrupted()
            target.attemptCastStop()
        }

        if ( attacker.isPlayer() ) {
            let activePlayer: L2PcInstance = attacker.getActingPlayer()
            activePlayer.sendDamageMessage( target, damage, false, true, false )
        }

        return Formulas.calculateDamageReflected( attacker, target, skill, true )
    }
}