import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { DegreeToRadian, GeneralHelper } from '../../helpers/GeneralHelper'
import { Location } from '../../models/Location'
import { FlyToLocation, FlyType } from '../../packets/send/FlyToLocation'
import { ValidateLocation } from '../../packets/send/ValidateLocation'
import _ from 'lodash'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { PathFinding } from '../../../geodata/PathFinding'

export class Blink extends AbstractEffect {
    flyCourse: number
    flyRadius: number

    constructor( parameters: Object ) {
        super()
        this.flyCourse = _.parseInt( _.get( parameters, 'flyCourse', '0' ) ) * ( Math.PI / 180 )
        this.flyRadius = _.parseInt( _.get( parameters, 'flyRadius', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        let angle = GeneralHelper.convertHeadingToDegree( target.getHeading() )
        let radian = angle * DegreeToRadian

        let differenceX = ( Math.cos( Math.PI + radian + this.flyCourse ) * this.flyRadius )
        let differenceY = ( Math.sin( Math.PI + radian + this.flyCourse ) * this.flyRadius )

        let x = target.getX() + differenceX
        let y = target.getY() + differenceY
        let z = target.getZ()

        let destination: Location = PathFinding.getApproximateDestination( target, x, y, z )

        await AIEffectHelper.setNextIntent( target, AIIntent.WAITING )
        BroadcastHelper.dataToSelfBasedOnVisibility( target, FlyToLocation( target, destination, FlyType.Dummy ) )

        target.setXYZLocation( destination )
        BroadcastHelper.dataToSelfBasedOnVisibility( target, ValidateLocation( target ) )
    }
}