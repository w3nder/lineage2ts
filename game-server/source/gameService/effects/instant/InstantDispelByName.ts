import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import _ from 'lodash'

export class InstantDispelByName extends AbstractEffect {
    skillId: number
    effectType = L2EffectType.Dispell

    constructor( parameters: Object ) {
        super()
        this.skillId = _.parseInt( _.get( parameters, 'id', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let affected: L2Character = info.getAffected()
        if ( affected ) {
            return affected.getEffectList().stopSkillEffects( true, this.skillId )
        }
    }
}