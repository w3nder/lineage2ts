import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { EffectCalculationType } from '../../enums/EffectCalculationType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2Character } from '../../models/actor/L2Character'
import { Skill } from '../../models/Skill'
import { Stats } from '../../models/stats/Stats'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

import _ from 'lodash'

export class Mp extends AbstractEffect {
    amount: number
    mode: EffectCalculationType

    constructor( parameters: Object ) {
        super()

        this.amount = parseFloat( _.get( parameters, 'amount', '0' ) )
        this.mode = EffectCalculationType[ _.get( parameters, 'mode', 'DIFF' ) as string ]
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target: L2Character = info.getAffected()
        return !( !target || target.isDead() || target.isDoor() || target.isInvulnerable() || target.isMpBlocked() )
    }

    async onStart( info: BuffInfo ) {
        let target: L2Character = info.getAffected()
        let character: L2Character = info.getEffector()
        let skill: Skill = info.getSkill()

        let amount = 0
        switch ( this.mode ) {
            case EffectCalculationType.DIFF:
                if ( this.amount < 0 ) {
                    amount = this.amount
                } else {
                    if ( !skill.isStatic() ) {
                        amount = target.calculateStat( Stats.MANA_CHARGE, this.amount, null, null )
                    }

                    amount = Math.min( skill.isStatic() ? this.amount : amount, target.getMaxRecoverableMp() - target.getCurrentMp() )
                }
                break
            case EffectCalculationType.PER:
                if ( this.amount < 0 ) {
                    amount = ( target.getCurrentMp() * this.amount ) / 100
                } else {
                    amount = Math.min( ( target.getMaxMp() * this.amount ) / 100.0, target.getMaxRecoverableMp() - target.getCurrentMp() )
                }
                break
        }

        if ( amount >= 0 ) {
            if ( amount > 0 ) {
                target.setCurrentMp( amount + target.getCurrentMp() )
            }

            if ( !target.isPlayable() ) {
                return
            }

            if ( character && character.objectId !== target.objectId ) {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S2_MP_HAS_BEEN_RESTORED_BY_C1 )
                        .addCharacterName( info.getEffector() )
                        .addNumber( amount )
                        .getBuffer()
                return target.sendOwnedData( packet )
            }

            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_MP_HAS_BEEN_RESTORED )
                    .addNumber( amount )
                    .getBuffer()
            return target.sendOwnedData( packet )
        }

        target.reduceCurrentMp( Math.abs( amount ) )
    }
}