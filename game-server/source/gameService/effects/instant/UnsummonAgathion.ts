import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export class UnsummonAgathion extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        return info.getEffector().isPlayer()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let player: L2PcInstance = info.getEffector().getActingPlayer()

        player.setAgathionId( 0 )
        player.broadcastUserInfo()
    }
}