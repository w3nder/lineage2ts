import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2Weapon } from '../../models/items/L2Weapon'
import { WeaponType } from '../../models/items/type/WeaponType'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { EtcItemType } from '../../enums/items/EtcItemType'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { InventorySlot } from '../../values/InventoryValues'
import _ from 'lodash'
import { GeoPolygonCache } from '../../cache/GeoPolygonCache'
import { PathFinding } from '../../../geodata/PathFinding'
import { WaterArea } from '../../models/areas/type/Water'
import { L2World } from '../../L2World'
import { L2WorldArea } from '../../models/areas/WorldArea'
import { AreaType } from '../../models/areas/AreaType'

const minimumDistance = 90
const maximumDistance = 250

export class Fishing extends AbstractEffect {
    effectType = L2EffectType.FishingStart

    canStart( info: BuffInfo ): boolean {
        let character = info.getEffector()
        if ( character.isPlayer() ) {
            let player: L2PcInstance = character.getActingPlayer()
            if ( ConfigManager.general.allowFishing() || player.hasActionOverride( PlayerActionOverride.SkillAction ) ) {
                return true
            }

            player.sendMessage( 'Fishing is disabled!' )
        }

        return false
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) : Promise<void> {
        let player: L2PcInstance = info.getEffector().getActingPlayer()
        if ( player.isFishing() ) {
            if ( player.getFishCombat() ) {
                await player.getFishCombat().endFishing( false )
            } else {
                player.endFishing( false )
            }

            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FISHING_ATTEMPT_CANCELLED ) )
            return
        }

        let equippedWeapon: L2Weapon = player.getActiveWeaponItem()
        if ( !equippedWeapon || equippedWeapon.getItemType() !== WeaponType.FISHINGROD ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FISHING_POLE_NOT_EQUIPPED ) )
            return
        }

        let equipedLeftHand: L2ItemInstance = player.getInventory().getPaperdollItem( InventorySlot.LeftHand )
        if ( !equipedLeftHand || ( equipedLeftHand.getItemType() !== EtcItemType.LURE ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.BAIT_ON_HOOK_BEFORE_FISHING ) )
            return
        }

        if ( !player.isGM() ) {
            if ( player.isInBoat() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_FISH_ON_BOAT ) )
                return
            }

            if ( player.isInCraftMode() || player.isInStoreMode() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_FISH_WHILE_USING_RECIPE_BOOK ) )
                return
            }

            if ( player.isInArea( AreaType.Water ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_FISH_UNDER_WATER ) )
                return
            }
        }

        let distance = _.random( minimumDistance, maximumDistance )
        let angle = GeneralHelper.convertHeadingToDegree( player.getHeading() )
        let radian = angle * ( Math.PI / 180 )
        let sin = Math.sin( radian )
        let cos = Math.cos( radian )
        let baitX = Math.round( player.getX() + ( cos * distance ) )
        let baitY = Math.round( player.getY() + ( sin * distance ) )

        let area: WaterArea = L2World.getAreasWithBoundingBox(
            Math.round( player.getX() + ( cos * minimumDistance ) ),
            Math.round( player.getY() + ( sin * minimumDistance ) ),
            Math.round( player.getX() + ( cos * maximumDistance ) ),
            Math.round( player.getY() + ( sin * maximumDistance ) ) ).find( ( area: L2WorldArea ) : boolean => area.type === AreaType.Water ) as WaterArea

        let baitZ : number = this.computeBaitZ( player, baitX, baitY, area )

        if ( baitZ === Number.MIN_VALUE ) {
            if ( !player.isGM() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_FISH_HERE ) )
                return
            }

            baitZ = player.getZ()
        }

        if ( !( await player.destroyItemWithCount( equipedLeftHand, 1, false, 'Fishing.onStart' ) ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_BAIT ) )
            return
        }

        player.setLure( equipedLeftHand )
        return player.startFishing( baitX, baitY, baitZ )
    }

    computeBaitZ( player: L2PcInstance, baitX: number, baitY: number, waterZone: WaterArea ) : number {
        if ( !waterZone ) {
            return Number.MIN_VALUE
        }

        let baitZ : number = waterZone.form.maximumZ

        if ( !PathFinding.canSeeTarget( player, baitX, baitY, baitZ ) ) {
            return Number.MIN_VALUE
        }

        if ( GeoPolygonCache.getZ( baitX, baitY, baitZ ) > baitZ ) {
            return Number.MIN_VALUE
        }

        if ( GeoPolygonCache.getZ( baitX, baitY, player.getZ() ) > baitZ ) {
            return Number.MIN_VALUE
        }

        return baitZ
    }
}