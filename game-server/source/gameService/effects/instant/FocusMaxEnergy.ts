import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { Skill } from '../../models/Skill'

export class FocusMaxEnergy extends AbstractEffect {
    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        return info.getAffected().isPlayer()
    }

    async onStart( info: BuffInfo ) {
        let target = info.getAffected()
        let sonicMastery : Skill = target.getSkills()[ 992 ]
        let focusMastery : Skill = target.getSkills()[ 993 ]
        let maxCharge = sonicMastery ? sonicMastery.getLevel() : focusMastery ? focusMastery.getLevel() : 0

        if ( maxCharge !== 0 ) {
            let player = target.getActingPlayer()
            let count = maxCharge - player.getCharges()
            player.increaseCharges( count, maxCharge )
        }
    }
}