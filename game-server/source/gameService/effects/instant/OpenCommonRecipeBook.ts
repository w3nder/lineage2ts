import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { RecipeController } from '../../taskmanager/RecipeController'

export class OpenCommonRecipeBook extends AbstractEffect {

    canStart( info: BuffInfo ): boolean {
        return info.getEffector().isPlayer()
    }

    isInstant(): boolean {
        return true
    }

    async onStart( info: BuffInfo ) {
        let player: L2PcInstance = info.getEffector().getActingPlayer()
        if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_CREATED_WHILE_ENGAGED_IN_TRADING ) )
            return
        }

        RecipeController.requestBookOpen( player, false )
    }
}