import { AbstractEffect } from '../../models/effects/AbstractEffect'
import _ from 'lodash'
import { BuffInfo } from '../../models/skills/BuffInfo'

export class VitalityPointUp extends AbstractEffect {
    value: number

    constructor( parameters: Object ) {
        super()

        this.value = _.parseInt( _.get( parameters, 'value', '0' ) )
    }

    isInstant(): boolean {
        return true
    }

    canStart( info: BuffInfo ): boolean {
        let target = info.getAffected()
        return target && target.isPlayer()
    }

    async onStart( info: BuffInfo ) {
        let player = info.getAffected().getActingPlayer()

        player.updateVitalityPoints( this.value, false, true )
    }
}