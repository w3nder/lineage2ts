export enum AIIntent {
    WAITING,
    ATTACK,
    FOLLOW,
    CAST,
    INTERACT,
    FLEE,
    RETURN,
    REST
}