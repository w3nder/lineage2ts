import { IAITrait } from '../../interface/IAITrait'
import { AIIntent } from '../../enums/AIIntent'
import { AIEffect, AIEventData, AIFollowObjectEvent } from '../../enums/AIEffect'
import { L2World } from '../../../L2World'
import { L2Summon } from '../../../models/actor/L2Summon'
import { EffectsCache } from '../../EffectCache'
import { L2Character } from '../../../models/actor/L2Character'
import { PlayableFollowAction } from '../actions/PlayableFollowAction'
import { L2Playable } from '../../../models/actor/L2Playable'
import { L2Object } from '../../../models/L2Object'

class SummonFollowBaseTrait extends PlayableFollowAction implements IAITrait {
    switchMap: Record<number, AIIntent> = {
        [ AIEffect.MoveToLocation ]: AIIntent.WAITING,
        [ AIEffect.ForceAttackTarget ]: AIIntent.ATTACK,
        [ AIEffect.ForceSpellCast ]: AIIntent.CAST,
        [ AIEffect.InteractTarget ]: AIIntent.INTERACT,
        [ AIEffect.Attacked ]: AIIntent.ATTACK
    }

    describeState( objectId: number, playerId: number ): void {
        return this.describeMovementAction( L2World.getObjectById( objectId ) as L2Summon, playerId )
    }

    getName(): string {
        return 'SummonFollowTrait'
    }

    async onEnd( objectId: number ): Promise<void> {
        return this.stopFollowing( L2World.getObjectById( objectId ) as L2Summon )
    }

    onStart( objectId: number ): Promise<void> {
        this.startFollowing( L2World.getObjectById( objectId ) as L2Summon, EffectsCache.getParameters( objectId, AIEffect.FollowObject ) as AIFollowObjectEvent )
        return
    }

    processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        switch ( effect ) {
            case AIEffect.FollowObject:
                this.startFollowing( L2World.getObjectById( objectId ) as L2Summon, parameters as AIFollowObjectEvent )
                return
        }
    }

    getFollowTarget( playable: L2Playable, data: AIFollowObjectEvent ): L2Object {
        if ( data?.targetId ) {
            return L2World.getObjectById( data.targetId ) as L2Character
        }

        return ( playable as L2Summon ).getOwner()
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return effect === AIEffect.FollowObject
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return this.switchMap[ effect ] !== undefined
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return this.switchMap[ effect ]
    }
}

export const SummonFollowTrait = new SummonFollowBaseTrait()