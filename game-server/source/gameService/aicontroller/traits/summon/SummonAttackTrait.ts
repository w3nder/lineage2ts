import { IAITrait } from '../../interface/IAITrait'
import { AIIntent } from '../../enums/AIIntent'
import { AIEffect, AIEventData, AIForceAttackTargetEvent } from '../../enums/AIEffect'
import { PlayableAttackAction } from '../actions/PlayableAttackAction'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'
import { L2World } from '../../../L2World'
import { L2Playable } from '../../../models/actor/L2Playable'

const processedEffects : Set<AIEffect> = new Set<AIEffect>( [
    AIEffect.MoveFinished,
    AIEffect.ForceAttackTarget,
    AIEffect.ReadyToAttack,
] )

class SummonAttackBaseTrait extends PlayableAttackAction implements IAITrait {

    switchMap: Record<number, AIIntent> = {
        [ AIEffect.FollowObject ]: AIIntent.WAITING,
        [ AIEffect.MoveToLocation ]: AIIntent.WAITING,
        [ AIEffect.ForceSpellCast ]: AIIntent.CAST,
        [ AIEffect.InteractTarget ]: AIIntent.INTERACT
    }

    getName(): string {
        return 'SummonAttackTrait'
    }

    onEnd( objectId: number ): void {
        return this.stopAttackAction( objectId )
    }

    onStart( objectId: number ): Promise<void> {
        return this.startAttackAction( objectId )
    }

    processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        switch ( effect ) {
            case AIEffect.MoveFinished:
                this.stopFollowTask( objectId )
                return this.startAttackAction( objectId )

            case AIEffect.ForceAttackTarget:
                this.setNextTarget( objectId, ( parameters as AIForceAttackTargetEvent ).targetId )
                return

            case AIEffect.TargetDead:
                AIEffectHelper.scheduleNextIntent( L2World.getObjectById( objectId ) as L2Playable , AIIntent.WAITING )
                return

            case AIEffect.ReadyToAttack:
                return this.startAttackAction( objectId )
        }
    }

    describeState( objectId: number, playerId: number ): void {
        return this.describeAttackAction( objectId, playerId )
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return this.switchMap[ effect ] !== undefined
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return this.switchMap[ effect ]
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return processedEffects.has( effect )
    }
}

export const SummonAttackTrait = new SummonAttackBaseTrait()