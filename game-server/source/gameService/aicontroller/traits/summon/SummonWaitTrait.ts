import { IAITrait } from '../../interface/IAITrait'
import { AIEffect } from '../../enums/AIEffect'
import { AIIntent } from '../../enums/AIIntent'
import { L2World } from '../../../L2World'
import { L2Summon } from '../../../models/actor/L2Summon'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'

class SummonWaitBaseTrait implements IAITrait {
    switchMap: Record<number, AIIntent> = {
        [ AIEffect.Attacked ]: AIIntent.ATTACK,
        [ AIEffect.FollowObject ]: AIIntent.FOLLOW,
        [ AIEffect.ForceAttackTarget ]: AIIntent.ATTACK,
        [ AIEffect.ForceSpellCast ]: AIIntent.CAST,
        [ AIEffect.InteractTarget ]: AIIntent.INTERACT
    }

    describeState(): void {}

    getName(): string {
        return 'SummonWaitTrait'
    }

    onEnd( objectId: number ): void {}

    onStart( objectId: number ): Promise<void> {
        let summon = L2World.getObjectById( objectId ) as L2Summon
        if ( summon.isFollowingOwner() ) {
            AIEffectHelper.scheduleNextIntent( summon, AIIntent.FOLLOW )
        }

        return
    }

    processEffect(): Promise<void> {
        return Promise.resolve()
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return false
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return this.switchMap[ effect ] !== undefined
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return this.switchMap[ effect ]
    }
}

export const SummonWaitTrait = new SummonWaitBaseTrait()