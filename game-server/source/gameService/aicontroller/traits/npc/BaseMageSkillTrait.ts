import { L2Attackable } from '../../../models/actor/L2Attackable'
import { L2Character } from '../../../models/actor/L2Character'
import { AISkillUsage, AISkillUsageType } from '../../../enums/AISkillUsage'
import { Skill } from '../../../models/Skill'
import _ from 'lodash'
import { BaseSkillUsingTrait } from './BaseSkillUsingTrait'

export abstract class BaseMageSkillTrait extends BaseSkillUsingTrait {
    shouldCastSkill( npc: L2Attackable, target: L2Character, isInMeleeRange: boolean ): boolean {
        let type = this.getNextUsageType( npc, isInMeleeRange )
        if ( type === null ) {
            return false
        }

        let skill = this.getUsageSkill( npc.getTemplate().aiSkillUsage, type, isInMeleeRange ? 0 : 0.10 )
        return this.isAINotifiedToCastSkill( npc, target, skill )
    }

    getUsageSkill( usage: AISkillUsage, type: AISkillUsageType, additionalProbability: number ) : Skill {
        if ( usage.available[ type ].length > 0 && ( usage.probability[ type ] > ( Math.random() + additionalProbability ) ) ) {
            return _.sample( usage.available[ type ] )
        }

        return null
    }

    abstract getNextUsageType( npc: L2Attackable, isInMeleeRange: boolean ) : AISkillUsageType
}