import { L2Attackable } from '../../../models/actor/L2Attackable'
import { L2Character } from '../../../models/actor/L2Character'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'
import { AISkillUsage, AISkillUsageType } from '../../../enums/AISkillUsage'
import { Skill } from '../../../models/Skill'
import _ from 'lodash'
import { BaseWarriorSkillTrait } from './BaseWarriorSkillTrait'

const defaultUsageOrder : Readonly<Array<AISkillUsageType>> = [
    AISkillUsageType.Buff,
    AISkillUsageType.Attack,
    AISkillUsageType.Attack,
    AISkillUsageType.Attack,
    AISkillUsageType.Special
]

class FullSkillsWarriorTrait extends BaseWarriorSkillTrait {
    usageTypeOrder: Readonly<Array<AISkillUsageType>>
    name: string = 'AllSkillsWarriorTrait'

    constructor( order : Readonly<Array<AISkillUsageType>> = defaultUsageOrder ) {
        super()
        this.usageTypeOrder = order
    }

    shouldCastSkill( npc: L2Attackable, target: L2Character, isInMeleeRange: boolean ): boolean {
        if ( isInMeleeRange && target.getCurrentHp() < ( target.getMaxHp() * 0.2 ) ) {
            return false
        }

        let parameters = npc.getTemplate().aiSkillUsage
        let specialSkill = this.getSpecialSkill( npc, parameters )
        if ( specialSkill && npc.calculateDistance( target, true ) < specialSkill.getCastRange() ) {
            AIEffectHelper.notifyMustCastSpell( npc, npc, specialSkill.getId(), specialSkill.getLevel(), false, false )
            return true
        }

        return this.performCastCheck( npc, target, parameters, this.getNextUsageType( npc ) )
    }

    getSpecialSkill( npc: L2Attackable, usage: AISkillUsage ) : Skill {
        if ( usage.available[ AISkillUsageType.Special ].length > 0 && npc.getCurrentHp() < ( npc.getMaxHp() * 0.20 ) ) {
            return _.sample( usage.available[ AISkillUsageType.Special ] )
        }

        return null
    }

    getNextUsageType( npc: L2Attackable ) : AISkillUsageType {
        let usageState = npc.getSkillUsage()
        let type = this.usageTypeOrder[ usageState.index ]

        usageState.index++

        if ( usageState.index >= this.usageTypeOrder.length ) {
            usageState.index = 0
        }

        return type
    }
}

export const AllSkillsWarriorTrait = new FullSkillsWarriorTrait()