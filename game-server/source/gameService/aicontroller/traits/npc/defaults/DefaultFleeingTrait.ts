import Timeout = NodeJS.Timeout
import { ObjectPool } from '../../../../helpers/ObjectPoolHelper'
import { AIEffect } from '../../../enums/AIEffect'
import { L2Attackable } from '../../../../models/actor/L2Attackable'
import { L2World } from '../../../../L2World'
import { AIEffectHelper } from '../../../helpers/AIEffectHelper'
import { AIIntent } from '../../../enums/AIIntent'
import { GeneralHelper } from '../../../../helpers/GeneralHelper'
import { ILocational, Location } from '../../../../models/Location'
import { PathFinding } from '../../../../../geodata/PathFinding'
import { BroadcastHelper } from '../../../../helpers/BroadcastHelper'
import { MoveToLocationWithCharacter } from '../../../../packets/send/MoveToLocation'
import { BaseMoveAction } from '../../actions/BaseMoveAction'
import { IAITrait } from '../../../interface/IAITrait'

interface WalkingTraitParameters {
    abortTask: Timeout
    endTime: number
}

const traitParametersCache = new ObjectPool( 'DefaultFeelingTraitAction', (): WalkingTraitParameters => {
    return {
        abortTask: undefined,
        endTime: 0,
    }
} )

const timerRegistry: { [ objectId: number ]: WalkingTraitParameters } = {}
const processedEffects : Set<AIEffect> = new Set<AIEffect>( [
    AIEffect.Attacked,
    AIEffect.MoveFinished
] )

export class DefaultFleeingTraitAction extends BaseMoveAction implements IAITrait {
    fleeDistance: number
    fleeDuration: number

    getName(): string {
        return 'DefaultFleeingTraitAction'
    }

    cleanUp( objectId: number ): void {
        let data: WalkingTraitParameters = timerRegistry[ objectId ]
        if ( data ) {
            clearTimeout( data.abortTask )
            delete timerRegistry[ objectId ]

            traitParametersCache.recycleValue( data )
        }
    }

    createDefaults( objectId: number ): WalkingTraitParameters {
        if ( !timerRegistry[ objectId ] ) {
            timerRegistry[ objectId ] = traitParametersCache.getValue()
        }

        return timerRegistry[ objectId ]
    }

    constructor( fleeDistance: number = 500, fleeDuration: number = 5000 ) {
        super()

        this.fleeDistance = fleeDistance
        this.fleeDuration = fleeDuration
    }

    onEnd( objectId: number ): void {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable

        npc.abortMoving()

        this.cleanUp( objectId )
    }

    async onStart( objectId: number ): Promise<void> {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable

        if ( !this.canFlee( npc ) ) {
            npc.abortMoving()
            npc.setIsRunning( false, true )
            return
        }

        if ( !npc.isRunning() ) {
            npc.setIsRunning( false, true )
        }

        this.prepareMoveParameters( npc )
        this.fleeFromLocation( npc, npc.getMostHated() )

        return
    }

    prepareMoveParameters( npc ) {
        let data: WalkingTraitParameters = this.createDefaults( npc.getObjectId() )

        data.abortTask = setTimeout( this.onEndFleeing, this.getFleeInterval(), npc.getObjectId() )
        data.endTime = Date.now() + this.getFleeInterval()
    }

    getFleeInterval(): number {
        return this.fleeDuration
    }

    processEffect( objectId: number, effect: AIEffect ): Promise<void> {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable

        switch ( effect ) {
            case AIEffect.Attacked:
            case AIEffect.MoveFinished:
                if ( timerRegistry[ objectId ].endTime < Date.now() ) {
                    if ( !npc.isMoving() ) {
                        return this.onStart( objectId )
                    }

                    return
                }

                AIEffectHelper.scheduleNextIntent( npc, AIIntent.ATTACK )
                return
        }
    }

    canFlee( npc: L2Attackable ): boolean {
        return !npc.isDead() && !npc.isMovementDisabled()
    }

    fleeFromLocation( npc: L2Attackable, runFromLocation: ILocational ): void {
        let [ x, y, z ] = GeneralHelper.getAngleLocation( npc, GeneralHelper.calculateAngleFromLocations( npc, runFromLocation ), this.fleeDistance )
        let moveLocation: Location = PathFinding.getApproximateDestination( npc, x, y, z )

        if ( !npc.moveToLocation( moveLocation.getX(), moveLocation.getY(), moveLocation.getZ(), 0 ) ) {
            return
        }

        BroadcastHelper.dataInLocation( npc, MoveToLocationWithCharacter( npc ), npc.getBroadcastRadius() )
    }

    onEndFleeing( objectId: number ) {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable
        AIEffectHelper.scheduleNextIntent( npc, AIIntent.ATTACK )
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return processedEffects.has( effect )
    }

    describeState( objectId: number, playerId: number ): void {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable

        return this.describeMovementAction( npc, playerId )
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return false
    }

    switchIntent(): AIIntent {
        return AIIntent.ATTACK
    }
}

export const DefaultFleeingTrait = new DefaultFleeingTraitAction()