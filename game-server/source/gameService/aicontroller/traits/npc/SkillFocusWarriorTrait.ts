import { AISkillUsageType } from '../../../enums/AISkillUsage'
import { L2Attackable } from '../../../models/actor/L2Attackable'
import { BaseWarriorSkillTrait } from './BaseWarriorSkillTrait'

class SkillFocusWarriorTrait extends BaseWarriorSkillTrait {

    defaultUsageType: AISkillUsageType
    maxIndex: number

    constructor( name: string, usage: AISkillUsageType, index: number ) {
        super()
        this.defaultUsageType = usage
        this.maxIndex = index
        this.name = name
    }

    getNextUsageType( npc: L2Attackable ) : AISkillUsageType {
        let usageState = npc.getSkillUsage()

        if ( usageState.index === 0 ) {
            usageState.index++
            return this.defaultUsageType
        }

        usageState.index++

        if ( usageState.index >= this.maxIndex ) {
            usageState.index = 0
        }

        return null
    }
}

export const BuffSkillWarriorTrait = new SkillFocusWarriorTrait( 'BuffSkillWarriorTrait', AISkillUsageType.Buff, 10 )
export const AttackSkillWarriorTrait = new SkillFocusWarriorTrait( 'AttackSkillWarriorTrait', AISkillUsageType.Attack, 4 )
export const SpecialSkillWarriorTrait = new SkillFocusWarriorTrait( 'SpecialSkillWarriorTrait', AISkillUsageType.Special, 8 )