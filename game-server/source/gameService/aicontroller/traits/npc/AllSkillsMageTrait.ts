import { AISkillUsageType } from '../../../enums/AISkillUsage'
import { L2Attackable } from '../../../models/actor/L2Attackable'
import { BaseMageSkillTrait } from './BaseMageSkillTrait'

const defaultUsageOrder : Readonly<Array<AISkillUsageType>> = [
    AISkillUsageType.Attack,
    AISkillUsageType.Attack,
    AISkillUsageType.Buff,
    AISkillUsageType.Attack,
    AISkillUsageType.Special
]

class FullSkillsMageTrait extends BaseMageSkillTrait {
    usageTypeOrder: Readonly<Array<AISkillUsageType>>
    name: string = 'AllSkillsMageTrait'

    constructor( order : Readonly<Array<AISkillUsageType>> = defaultUsageOrder ) {
        super()
        this.usageTypeOrder = order
    }

    getNextUsageType( npc: L2Attackable, isInMeleeRange: boolean ) : AISkillUsageType {
        let usageState = npc.getSkillUsage()
        let type = this.usageTypeOrder[ usageState.index ]

        usageState.index++

        if ( !isInMeleeRange ) {
            return type
        }

        if ( usageState.index >= this.usageTypeOrder.length ) {
            usageState.index = 0
            return null
        }

        return type
    }
}

export const AllSkillsMageTrait = new FullSkillsMageTrait()