import { IAITrait } from '../../interface/IAITrait'
import { L2Attackable } from '../../../models/actor/L2Attackable'
import { L2World } from '../../../L2World'
import { EffectsCache } from '../../EffectCache'
import { AIEffect, AIForceSpellCastEvent } from '../../enums/AIEffect'
import { L2Character } from '../../../models/actor/L2Character'
import { SkillCache } from '../../../cache/SkillCache'
import { Skill } from '../../../models/Skill'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'
import { AIIntent } from '../../enums/AIIntent'

const processedEffects : Set<number> = new Set<number>( [
    AIEffect.SkillCastCanceled,
    AIEffect.SkillCastFinished,
    AIEffect.ForceSpellCast
] )

export class NonMovingCastTrait implements IAITrait {
    name: string
    skipRangeCheck: boolean = false

    constructor( name: string, skipRange: boolean ) {
        this.name = name
        this.skipRangeCheck = skipRange
    }

    async onStart( objectId: number ): Promise<void> {
        let npc: L2Attackable = L2World.getObjectById( objectId ) as L2Attackable
        let parameters = EffectsCache.getParameters( npc.getObjectId(), AIEffect.ForceSpellCast ) as AIForceSpellCastEvent

        if ( !parameters ) {
            return this.abortCast( npc )
        }

        let target: L2Character = L2World.getObjectById( parameters.targetId ) as L2Character
        if ( !target ) {
            return this.abortCast( npc )
        }

        let skill = SkillCache.getSkill( parameters.skillId, parameters.skillLevel )
        if ( !skill || !this.canCastSkill( npc, skill ) ) {
            return this.abortCast( npc )
        }

        if ( target === npc || this.skipRangeCheck ) {
            return this.performCast( npc, target, skill )
        }

        let range = skill.getCastRange()
        let distanceToTarget = npc.calculateDistance( target, true )

        if ( distanceToTarget > range ) {
            return this.abortCast( npc )
        }

        return this.performCast( npc, target, skill )
    }

    canCastSkill( caster: L2Attackable, skill: Skill ): boolean {
        if ( caster.isCastingNow() && !skill.isSimultaneousCast() ) {
            return false
        }

        if ( caster.isSkillDisabled( skill ) ) {
            return false
        }

        if ( skill.getHpConsume() > caster.getCurrentHp() ) {
            return false
        }

        if ( ( skill.isMagic() && caster.isMuted() ) ) {
            return false
        }

        if ( skill.isPhysical() && caster.isPhysicalMuted() ) {
            return false
        }

        return true
    }

    performCast( npc: L2Attackable, target: L2Character, skill: Skill ): Promise<void> {
        npc.abortMoving()
        npc.setTarget( target )

        return npc.doCast( skill )
    }

    abortCast( npc: L2Character ) : void {
        return AIEffectHelper.scheduleNextIntent( npc, AIIntent.WAITING )
    }

    onEnd( objectId: number ): void {}

    canProcessEffect( effect: AIEffect ): boolean {
        return processedEffects.has( effect )
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return false
    }

    describeState( objectId: number, playerId: number ): void {}

    getName(): string {
        return this.name
    }

    processEffect( objectId: number, effect: AIEffect ): Promise<void> {
        if ( processedEffects.has( effect ) ) {
            return this.onStart( objectId )
        }
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return undefined
    }
}

export const NonMovingCastAnyRange = new NonMovingCastTrait( 'NonMovingCastAnyRange', true )
export const NonMovingCastRestrictedRange = new NonMovingCastTrait( 'NonMovingCastRestrictedRange', true )