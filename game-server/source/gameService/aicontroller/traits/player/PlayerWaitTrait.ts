import { IAITrait } from '../../interface/IAITrait'
import { L2World } from '../../../L2World'
import { AIIntent } from '../../enums/AIIntent'
import { AIEffect, AIEventData, AIMoveToLocationEvent } from '../../enums/AIEffect'
import { L2PcInstance } from '../../../models/actor/instance/L2PcInstance'
import { EffectsCache } from '../../EffectCache'
import { PlayerBaseMoveAction } from '../actions/PlayerBaseMoveAction'

class PlayerWaitBaseTrait extends PlayerBaseMoveAction implements IAITrait {

    /*
        Please note that any attack for player must be forced. Otherwise, if reacting to attack,
        it would mean that player actions can be interrupted (player moves, and if attacked, chases target or attacks back).
        Thus, caution must be taken to always interpret player actions as forced actions, since from standpoint of AI
        player does not possess automatic behavior akin to npcs.
     */
    switchMap: Record<number, AIIntent> = {
        [ AIEffect.FollowObject ]: AIIntent.FOLLOW,
        [ AIEffect.ForceAttackTarget ]: AIIntent.ATTACK,
        [ AIEffect.ForceSpellCast ]: AIIntent.CAST,
        [ AIEffect.InteractTarget ]: AIIntent.INTERACT
    }

    getName(): string {
        return 'PlayerWaitTrait'
    }

    describeState( objectId: number, playerId: number ): void {
        let player = L2World.getPlayer( objectId ) as L2PcInstance
        return this.describeMovementAction( player, playerId )
    }

    onEnd( objectId: number ): void {
        let player = L2World.getPlayer( objectId ) as L2PcInstance
        this.removeMovementState( objectId )
        if ( !player.isMoving() ) {
            return
        }

        return player.abortMoving()
    }

    onStart( playerId: number ): Promise<void> {
        let player = L2World.getPlayer( playerId )

        let moveParameters = EffectsCache.getParameters( playerId, AIEffect.MoveToLocation ) as AIMoveToLocationEvent
        if ( moveParameters ) {
            this.moveToCoordinates( player, moveParameters.x, moveParameters.y, moveParameters.z, moveParameters.heading, moveParameters.instanceId )
            return
        }
    }

    processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        switch ( effect ) {
            case AIEffect.MoveToLocation:
                let data = parameters as AIMoveToLocationEvent
                this.moveToCoordinates( L2World.getPlayer( objectId ), data.x, data.y, data.z, data.heading, data.instanceId )
                return
        }
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return effect === AIEffect.MoveToLocation
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return this.switchMap[ effect ] !== undefined
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return this.switchMap[ effect ]
    }
}

export const PlayerWaitTrait = new PlayerWaitBaseTrait()