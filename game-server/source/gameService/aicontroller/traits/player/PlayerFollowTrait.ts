import { IAITrait } from '../../interface/IAITrait'
import { AIIntent } from '../../enums/AIIntent'
import { AIEffect, AIEventData, AIFollowObjectEvent } from '../../enums/AIEffect'
import { L2World } from '../../../L2World'
import { EffectsCache } from '../../EffectCache'
import { L2PcInstance } from '../../../models/actor/instance/L2PcInstance'
import { PlayableFollowAction } from '../actions/PlayableFollowAction'

class PlayerFollowBaseTrait extends PlayableFollowAction implements IAITrait {

    switchMap: Record<number, AIIntent> = {
        [ AIEffect.MoveToLocation ]: AIIntent.WAITING,
        [ AIEffect.ForceAttackTarget ]: AIIntent.ATTACK,
        [ AIEffect.ForceSpellCast ]: AIIntent.CAST,
        [ AIEffect.InteractTarget ]: AIIntent.INTERACT
    }

    describeState( objectId: number, playerId: number ): void {
        return this.describeMovementAction( L2World.getPlayer( objectId ), playerId )
    }

    getName(): string {
        return 'PlayerFollowTrait'
    }

    onEnd( objectId: number ): void {
        return this.stopFollowing( L2World.getPlayer( objectId ) )
    }

    onStart( objectId: number ): Promise<void> {
        this.startFollowing( L2World.getPlayer( objectId ) as L2PcInstance, EffectsCache.getParameters( objectId, AIEffect.FollowObject ) as AIFollowObjectEvent )
        return
    }

    processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        switch ( effect ) {
            case AIEffect.FollowObject:
                this.startFollowing( L2World.getPlayer( objectId ) as L2PcInstance, parameters as AIFollowObjectEvent )
                return
        }
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return effect === AIEffect.FollowObject
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return this.switchMap[ effect ] !== undefined
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return this.switchMap[ effect ]
    }
}

export const PlayerFollowTrait = new PlayerFollowBaseTrait()