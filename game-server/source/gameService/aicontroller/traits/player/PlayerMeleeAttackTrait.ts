import { IAITrait } from '../../interface/IAITrait'
import { AIIntent } from '../../enums/AIIntent'
import { L2PcInstance } from '../../../models/actor/instance/L2PcInstance'
import { AIEffect, AIEventData, AIForceAttackTargetEvent } from '../../enums/AIEffect'
import { PlayableAttackAction } from '../actions/PlayableAttackAction'
import { DuelState } from '../../../enums/DuelState'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { AIEffectHelper } from '../../helpers/AIEffectHelper'
import { L2World } from '../../../L2World'
import { L2Playable } from '../../../models/actor/L2Playable'

const processedEffects : Set<AIEffect> = new Set<AIEffect>( [
    AIEffect.MoveFinished,
    AIEffect.ForceAttackTarget,
    AIEffect.TargetDead,
    AIEffect.ReadyToAttack,
] )

class PlayerMeleeAttackBaseTrait extends PlayableAttackAction implements IAITrait {
    switchMap: Record<number, AIIntent> = {
        [ AIEffect.FollowObject ]: AIIntent.WAITING,
        [ AIEffect.MoveToLocation ]: AIIntent.WAITING,
        [ AIEffect.ForceSpellCast ]: AIIntent.CAST,
        [ AIEffect.InteractTarget ]: AIIntent.INTERACT
    }

    getName(): string {
        return 'PlayerMeleeAttackTrait'
    }

    describeState( objectId: number, otherPlayerId: number ): void {
        return this.describeAttackAction( objectId, otherPlayerId )
    }

    /*
        Mode of operation:
        - attack target
        - follow target if out of range of weapon
        - disengage attack if target is dead

        Movement:
        - move close to target if out of weapon range (melee weapon)
        - stop if target is in range

        Ensure change of target for ForceAttackTarget event.
        Cast and interactions are handled by other traits.
        No changing target based on accumulated aggro inside trait (target change is handled by spell effect)
     */
    processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ): Promise<void> {
        switch ( effect ) {
            case AIEffect.MoveFinished:
                this.stopFollowTask( objectId )
                return this.startAttackAction( objectId )

            case AIEffect.ForceAttackTarget:
                this.setNextTarget( objectId, ( parameters as AIForceAttackTargetEvent ).targetId )
                return

            case AIEffect.TargetDead:
                AIEffectHelper.scheduleNextIntent( L2World.getObjectById( objectId ) as L2Playable , AIIntent.WAITING )
                return

            case AIEffect.ReadyToAttack:
                return this.startAttackAction( objectId )
        }
    }

    canProcessEffect( effect: AIEffect ): boolean {
        return processedEffects.has( effect )
    }

    onStart( objectId: number ): Promise<void> {
        return this.startAttackAction( objectId )
    }

    onEnd( objectId: number ): Promise<void> {
        this.stopAttackAction( objectId )
        return
    }

    canMoveToCoordinates( player: L2PcInstance ): boolean {
        if ( player.getDuelState() !== DuelState.Dead ) {
            return true
        }

        player.sendOwnedData( ActionFailed() )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_MOVE_FROZEN ) )
        return false
    }

    canSwitchIntent( effect: AIEffect ): boolean {
        return this.switchMap[ effect ] !== undefined
    }

    switchIntent( effect: AIEffect ): AIIntent {
        return this.switchMap[ effect ]
    }
}

export const PlayerMeleeAttackTrait = new PlayerMeleeAttackBaseTrait()