import { IAITrait } from '../interface/IAITrait'
import { AIIntent } from '../enums/AIIntent'

export const EmptyTrait: IAITrait = {
    canSwitchIntent(): boolean {
        return true
    },

    switchIntent(): AIIntent {
        return AIIntent.WAITING
    },

    getName(): string {
        return 'EmptyTrait'
    },

    describeState(): void {},

    processEffect(): Promise<void> {
        return Promise.resolve()
    },

    onStart(): Promise<void> {
        return Promise.resolve()
    },

    onEnd(): void {},

    canProcessEffect(): boolean {
        return false
    }
}