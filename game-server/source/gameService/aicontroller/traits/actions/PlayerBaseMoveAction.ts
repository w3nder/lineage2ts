import { BaseMoveAction } from './BaseMoveAction'
import { DuelState } from '../../../enums/DuelState'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../../models/actor/instance/L2PcInstance'

export class PlayerBaseMoveAction extends BaseMoveAction {
    canMoveToCoordinates( player: L2PcInstance ): boolean {
        if ( player.getDuelState() !== DuelState.Dead ) {
            return true
        }

        player.sendOwnedData( ActionFailed() )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_MOVE_FROZEN ) )
        return false
    }
}