import { AIEffect, AIEventData } from '../enums/AIEffect'
import { AIIntent } from '../enums/AIIntent'

export interface IAITrait {
    getName(): string
    onStart( objectId: number ) : Promise<void>
    onEnd( objectId : number ) : void
    processEffect( objectId: number, effect: AIEffect, parameters: AIEventData ) : Promise<void>
    describeState( objectId: number, playerId: number ): void
    canProcessEffect( effect: AIEffect ) : boolean
    canSwitchIntent( effect: AIEffect ) : boolean
    switchIntent( effect: AIEffect ) : AIIntent
}

export type TraitSettings = Record<AIIntent,IAITrait>