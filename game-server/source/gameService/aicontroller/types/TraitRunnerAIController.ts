import { AIIntent } from '../enums/AIIntent'
import { AIEffect, AIEventData } from '../enums/AIEffect'
import { EmptyTrait } from '../traits/EmptyTrait'
import { EffectsCache } from '../EffectCache'
import { AggroCache } from '../../cache/AggroCache'
import { IAIController } from '../interface/IAIController'
import { CharacterAIController } from './CharacterAIController'

const storedEffectTypes = new Set<number>( [
        AIEffect.Attacked,
        AIEffect.ForceSpellCast,
        AIEffect.ForceAttackTarget,
        AIEffect.MoveToLocation,
        AIEffect.InteractTarget
] )

export class TraitRunnerAIController extends CharacterAIController implements IAIController {

    async notifyEffect( effect: AIEffect, parameters: AIEventData ): Promise<void> {
        if ( this.currentTrait.canProcessEffect( effect ) ) {
            await this.currentTrait.processEffect( this.ownerId, effect, parameters )
        }

        let intent: AIIntent = this.getNextIntent( effect, parameters )
        if ( intent === this.currentIntent ) {
            return
        }

        if ( storedEffectTypes.has( effect ) ) {
            EffectsCache.setParameters( this.ownerId, effect, EffectsCache.cloneParameters( parameters ) )
        }

        if ( intent === AIIntent.WAITING ) {
            AggroCache.clearData( this.ownerId )
        }

        return this.switchIntent( intent )
    }

    private runCurrentTrait(): Promise<void> {
        this.stopCurrentTrait()

        this.currentTrait = this.traitMap[ this.currentIntent ]

        if ( !this.currentTrait ) {
            console.log( 'TraitRunnerAIController.runTrait: no traits to run for intent', AIIntent[ this.currentIntent ] )
            this.currentTrait = this.traitMap[ this.defaultIntent ]
        }

        if ( !this.currentTrait ) {
            this.currentTrait = EmptyTrait
            return
        }

        return this.currentTrait.onStart( this.ownerId )
    }

    switchIntent( intent: AIIntent ): Promise<void> {
        this.currentIntent = intent ?? this.defaultIntent

        return this.runCurrentTrait()
    }

    describeState( playerId: number ): void {
        return this.currentTrait.describeState( this.ownerId, playerId )
    }

    deactivate(): void {
        return this.resetParameters()
    }

    async activate(): Promise<void> {
        this.canAct = true
        return this.runCurrentTrait()
    }
}