import { AIIntent } from '../enums/AIIntent'
import { IAITrait, TraitSettings } from '../interface/IAITrait'
import { EmptyTrait } from '../traits/EmptyTrait'
import { L2Character } from '../../models/actor/L2Character'
import { AIEffect, AIEventData, AIForceIntentChangeEvent } from '../enums/AIEffect'
import { EffectsCache } from '../EffectCache'
import { AggroCache } from '../../cache/AggroCache'
import { FightStanceCache } from '../../cache/FightStanceCache'

export class CharacterAIController {
    ownerId: number
    defaultIntent: AIIntent
    currentIntent: AIIntent
    traitMap: TraitSettings
    currentTrait: IAITrait = EmptyTrait
    protected canAct: boolean = false

    constructor( character: L2Character, traits: TraitSettings, defaultIntent: AIIntent = AIIntent.WAITING ) {
        this.ownerId = character.getObjectId()
        this.defaultIntent = defaultIntent
        this.currentIntent = defaultIntent
        this.traitMap = traits

        if ( !traits ) {
            throw new Error( 'Unable to locate default traits' )
        }
    }

    protected getNextIntent( effect: AIEffect, parameters: AIEventData ) : AIIntent {
        if ( effect === AIEffect.ForceIntentChange ) {
            return ( parameters as AIForceIntentChangeEvent ).nextIntent
        }

        if ( this.currentTrait.canSwitchIntent( effect ) ) {
            return this.currentTrait.switchIntent( effect )
        }

        return this.currentIntent
    }

    protected stopCurrentTrait(): void {
        return this.currentTrait.onEnd( this.ownerId )
    }

    protected resetParameters() : void {
        this.canAct = false
        this.currentIntent = this.defaultIntent

        this.stopCurrentTrait()
        this.currentTrait = EmptyTrait

        EffectsCache.removeParameters( this.ownerId )
        AggroCache.clearData( this.ownerId )
        FightStanceCache.clearStances( this.ownerId )
    }

    isActive() : boolean {
        return this.canAct
    }

    isIntent( intent: AIIntent ) : boolean {
        return this.currentIntent === intent
    }
}