import { IAITrait, TraitSettings } from './interface/IAITrait'
import { AIIntent } from './enums/AIIntent'
import { BoatMoveTrait } from './traits/BoatMoveTrait'
import { L2Vehicle } from '../models/actor/L2Vehicle'
import { InstanceType } from '../enums/InstanceType'
import { EmptyTrait } from './traits/EmptyTrait'
import { L2Playable } from '../models/actor/L2Playable'
import { PlayerWaitTrait } from './traits/player/PlayerWaitTrait'
import { PlayerMeleeAttackTrait } from './traits/player/PlayerMeleeAttackTrait'
import { PlayerInteractTrait } from './traits/player/PlayerInteractTrait'
import { PlayerCastTrait } from './traits/player/PlayerCastTrait'
import { SummonWaitTrait } from './traits/summon/SummonWaitTrait'
import { SummonFollowTrait } from './traits/summon/SummonFollowTrait'
import { SummonInteractTrait } from './traits/summon/SummonInteractTrait'
import { SummonAttackTrait } from './traits/summon/SummonAttackTrait'
import { PlayerFollowTrait } from './traits/player/PlayerFollowTrait'

// TODO : implement and assign all other traits
// TODO : consider using one global default traits objects (if known that there will not be assignments per trait)
// TODO : consider using object pool for default traits

// TODO : create variation of trait that handles shout messages per AI parameter of ShoutMsg1 (and others, ShoutMsg2...)
// TODO : add npc Id mapping for traits, so that listeners can set/override AI traits without additional spawn logic
export const TraitRegistry = {
    getVehicleTraits( vehicle: L2Vehicle ): IAITrait {
        switch ( vehicle.instanceType ) {
            case InstanceType.L2BoatInstance:
                return BoatMoveTrait
        }

        return EmptyTrait
    },

    getPlayableTraits( playable: L2Playable ): TraitSettings {
        switch ( playable.getInstanceType() ) {
            case InstanceType.L2PcInstance:
                return {
                    [ AIIntent.WAITING ]: PlayerWaitTrait,
                    [ AIIntent.ATTACK ]: PlayerMeleeAttackTrait,
                    [ AIIntent.FLEE ]: EmptyTrait,
                    [ AIIntent.INTERACT ]: PlayerInteractTrait,
                    [ AIIntent.CAST ]: PlayerCastTrait,
                    [ AIIntent.FOLLOW ]: PlayerFollowTrait,
                    [ AIIntent.REST ]: PlayerWaitTrait,
                    [ AIIntent.RETURN ]: EmptyTrait
                }
        }

        return {
            [ AIIntent.WAITING ]: SummonWaitTrait,
            [ AIIntent.ATTACK ]: SummonAttackTrait,
            [ AIIntent.FLEE ]: EmptyTrait,
            [ AIIntent.INTERACT ]: SummonInteractTrait,
            [ AIIntent.CAST ]: EmptyTrait,
            [ AIIntent.RETURN ]: EmptyTrait,
            [ AIIntent.FOLLOW ]: SummonFollowTrait,
            [ AIIntent.REST ]: EmptyTrait
        }
    },
}
