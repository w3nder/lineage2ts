import { L2Character } from '../../models/actor/L2Character'
import { AIEffectPoolCache } from '../../cache/AIEffectPoolCache'
import { ILocational } from '../../models/Location'
import { AIIntent } from '../enums/AIIntent'
import {
    AIAttackedEvent,
    AIEffect,
    AIFleeAwayEvent,
    AIFollowObjectEvent,
    AIForceAttackTargetEvent,
    AIForceIntentChangeEvent,
    AIForceSpellCastEvent,
    AIInteractTargetEvent,
    AIMoveToLocationEvent,
    AIOwnerHpReducedEvent,
    AIOwnerMpReducedEvent,
    AISkillCastCanceledEvent,
    AISkillCastFinishedEvent,
    AITargetDeadEvent,
} from '../enums/AIEffect'
import { AIControllerCapable } from '../../models/actor/AIControllerCapable'
import { L2Object } from '../../models/L2Object'
import { AggroCache } from '../../cache/AggroCache'
import { FightStanceCache } from '../../cache/FightStanceCache'

export const AIEffectHelper = {
    notifyAttacked( object: AIControllerCapable, attacker: L2Object, aggroAmount: number = 1 ): void {
        return AIEffectHelper.notifyAttackedWithTargetId( object, attacker.getObjectId(), 0, aggroAmount )
    },

    notifyAttackedWithTargetId( object: AIControllerCapable, attackerId: number, damage: number, aggro: number ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        if ( FightStanceCache.hasPermanentStance( object.getObjectId() ) || FightStanceCache.hasPlayableStance( object.getObjectId() ) ) {
            AggroCache.addAggro( object.getObjectId(), attackerId, damage, aggro )
            return
        }

        AggroCache.addAggro( object.getObjectId(), attackerId, damage, aggro )

        let data = AIEffectPoolCache.getData( AIEffect.Attacked ) as AIAttackedEvent

        data.attackerId = attackerId
        data.damage = damage
        data.aggro = aggro

        object.getAIController().notifyEffect( AIEffect.Attacked, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.Attacked, data ) )
    },

    notifyForceAttack( object: AIControllerCapable, attacker: L2Object ) : void {
        return AIEffectHelper.notifyForceAttackWithTargetId( object, attacker.getObjectId() )
    },

    notifyForceAttackWithTargetId( object: AIControllerCapable, targetId: number, damage: number = 0, aggro: number = 1 ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        AggroCache.addAggro( object.getObjectId(), targetId, damage, aggro )

        let data = AIEffectPoolCache.getData( AIEffect.ForceAttackTarget ) as AIForceAttackTargetEvent

        data.targetId = targetId

        object.getAIController().notifyEffect( AIEffect.ForceAttackTarget, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.ForceAttackTarget, data ) )
    },

    notifyMustCastSpell( object: AIControllerCapable, target: L2Object, skillId: number, skillLevel: number, isControlPressed : boolean, isShiftPressed : boolean ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.ForceSpellCast ) as AIForceSpellCastEvent

        data.targetId = target ? target.getObjectId() : 0
        data.skillId = skillId
        data.skillLevel = skillLevel
        data.isControlPressed = isControlPressed
        data.isShiftPressed = isShiftPressed

        object.getAIController().notifyEffect( AIEffect.ForceSpellCast, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.ForceSpellCast, data ) )
    },

    notifyMove( object: AIControllerCapable, location: ILocational ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.MoveToLocation ) as AIMoveToLocationEvent

        data.x = location.getX()
        data.y = location.getY()
        data.z = location.getZ()
        data.instanceId = location.getInstanceId()
        data.heading = location.getHeading()

        object.getAIController().notifyEffect( AIEffect.MoveToLocation, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.MoveToLocation, data ) )
    },

    notifyMoveWithCoordinates( object: AIControllerCapable, x: number, y: number, z: number, heading: number = 0, instanceId: number = 0 ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.MoveToLocation ) as AIMoveToLocationEvent

        data.x = x
        data.y = y
        data.z = z
        data.heading = heading
        data.instanceId = instanceId

        object.getAIController().notifyEffect( AIEffect.MoveToLocation, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.MoveToLocation, data ) )
    },

    setNextIntent( object: AIControllerCapable, nextIntent: AIIntent ): Promise<void> {
        if ( !object.getAIController().isActive() ) {
            return
        }

        return object.getAIController().switchIntent( nextIntent )
    },

    scheduleNextIntent( object: AIControllerCapable, nextIntent: AIIntent ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.ForceIntentChange ) as AIForceIntentChangeEvent
        data.nextIntent = nextIntent

        object.getAIController().notifyEffect( AIEffect.ForceIntentChange, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.ForceIntentChange, data ) )
    },

    notifyFollow( object: AIControllerCapable, targetId: number, distance: number = 0 ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.FollowObject ) as AIFollowObjectEvent

        data.targetId = targetId
        data.distance = distance

        object.getAIController().notifyEffect( AIEffect.FollowObject, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.FollowObject, data ) )
    },

    notifyTargetDead( object: AIControllerCapable, deadTarget: L2Character ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.TargetDead ) as AITargetDeadEvent

        data.targetId = deadTarget ? deadTarget.getObjectId() : 0

        object.getAIController().notifyEffect( AIEffect.TargetDead, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.TargetDead, data ) )
    },

    notifyFlee( object: AIControllerCapable, time: number ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.FleeAway ) as AIFleeAwayEvent

        data.time = time

        object.getAIController().notifyEffect( AIEffect.FleeAway, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.FleeAway, data ) )
    },

    notifyInteractWithTarget( object: AIControllerCapable, targetId: number, shouldInteract: boolean = true ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.InteractTarget ) as AIInteractTargetEvent

        data.targetId = targetId
        data.shouldInteract = shouldInteract

        object.getAIController().notifyEffect( AIEffect.InteractTarget, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.InteractTarget, data ) )
    },

    notifyCastingFinished( object: AIControllerCapable, skillId: number, isSimultaneous: boolean ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.SkillCastFinished ) as AISkillCastFinishedEvent

        data.skillId = skillId
        data.isSimultaneous = isSimultaneous

        object.getAIController().notifyEffect( AIEffect.SkillCastFinished, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.SkillCastFinished, data ) )
    },

    notifyCastingCancelled( object: AIControllerCapable, skillId: number, isSimultaneous: boolean ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.SkillCastCanceled ) as AISkillCastCanceledEvent

        data.skillId = skillId
        data.isSimultaneous = isSimultaneous

        object.getAIController().notifyEffect( AIEffect.SkillCastCanceled, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.SkillCastCanceled, data ) )
    },

    notifyMoveFinished( object: AIControllerCapable ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        object.getAIController().notifyEffect( AIEffect.MoveFinished, null )
    },

    notifyReadyForAttack( object: AIControllerCapable ): void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        object.getAIController().notifyEffect( AIEffect.ReadyToAttack, null )
    },

    notifyOwnerHpReduced( object: AIControllerCapable, damage: number, attackerId: number ) : void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.OwnerHPReduced ) as AIOwnerHpReducedEvent

        data.damage = damage
        data.attackerId = attackerId

        object.getAIController().notifyEffect( AIEffect.OwnerHPReduced, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.OwnerHPReduced, data ) )
    },

    notifyOwnerMpReduced( object: AIControllerCapable, damage: number ) : void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        let data = AIEffectPoolCache.getData( AIEffect.OwnerMPReduced ) as AIOwnerMpReducedEvent

        data.damage = damage

        object.getAIController().notifyEffect( AIEffect.OwnerMPReduced, data ).finally( () => AIEffectPoolCache.recyleData( AIEffect.OwnerMPReduced, data ) )
    },

    notifyMoveFailed( object: AIControllerCapable ) : void {
        if ( !object.getAIController().isActive() ) {
            return
        }

        object.getAIController().notifyEffect( AIEffect.MoveFailed, null )
    }
}