import { L2ClanMember } from './L2ClanMember'
import { ItemContainer } from './itemcontainer/ItemContainer'
import { Skill } from './Skill'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { PacketDispatcher } from '../PacketDispatcher'
import { PledgeShowMemberListUpdate } from '../packets/send/PledgeShowMemberListUpdate'
import { PledgeSkillList } from '../packets/send/PledgeSkillList'
import { ConfigManager } from '../../config/ConfigManager'
import { CastleManager } from '../instancemanager/CastleManager'
import { SiegeManager } from '../instancemanager/SiegeManager'
import { FortManager } from '../instancemanager/FortManager'
import { PledgeShowMemberListDeleteAll } from '../packets/send/PledgeShowMemberListDeleteAll'
import { DatabaseManager } from '../../database/manager'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { PledgeShowInfoUpdate } from '../packets/send/PledgeShowInfoUpdate'
import { ClanSkillsDatabaseItem } from '../../database/interface/ClanSkillsTableApi'
import { SkillCache } from '../cache/SkillCache'
import { L2World } from '../L2World'
import { PledgeShowMemberListAll } from '../packets/send/PledgeShowMemberListAll'
import { CrestCache } from '../cache/CrestCache'
import { ClanPriviledgeHelper } from './ClanPrivilege'
import { ClassId } from './base/ClassId'
import { PledgeSkillListAdd } from '../packets/send/PledgeSkillListAdd'
import { ExSubPledgeSkillAdd } from '../packets/send/ExSubPledgeSkillAdd'
import { ClanCache } from '../cache/ClanCache'
import { PacketVariables } from '../packets/PacketVariables'
import { UserInfo } from '../packets/send/UserInfo'
import { ExBrExtraUserInfo } from '../packets/send/ExBrExtraUserInfo'
import { ListenerCache } from '../cache/ListenerCache'
import {
    ClanChangeLevelEvent,
    EventType,
    PlayerClanJoinEvent,
    PlayerClanLeaderChangeEvent,
    PlayerClanLeaderScheduledEvent,
    PlayerClanLevelUpEvent,
    PlayerLeftClanEvent,
} from './events/EventType'
import { TerritoryWarManager } from '../instancemanager/TerritoryWarManager'
import { Territory } from '../instancemanager/territory/Territory'
import { StatusUpdate, StatusUpdateProperty } from '../packets/send/builder/StatusUpdate'
import { GeneralHelper } from '../helpers/GeneralHelper'
import { L2ClanValues } from '../values/L2ClanValues'
import { SubPledgeInfo } from '../packets/send/SubPledgeInfo'
import { ClanWarehouse } from './itemcontainer/ClanWarehouse'
import { L2ClanNoticesTableItem, L2ClanNoticeStatus } from '../../database/interface/ClanNoticesTableApi'
import { BlocklistCache } from '../cache/BlocklistCache'
import { EventPoolCache } from '../cache/EventPoolCache'
import aigle from 'aigle'
import _ from 'lodash'
import { ClanHall } from './entity/ClanHall'
import { ClanHallManager } from '../instancemanager/ClanHallManager'
import { ClanHallSiegeManager } from '../instancemanager/ClanHallSiegeManager'
import { PlayerRelationStatus } from '../cache/PlayerRelationStatusCache'
import { AreaType } from './areas/AreaType'
import { ClanPrivilege } from '../enums/ClanPriviledge'
import { RankPrivileges } from './RankPrivileges'
import { ClanPledge } from './ClanPledge'
import { L2ClanPledgeTableItem } from '../../database/interface/ClanSubpledgesTableApi'
import { PledgeShowMemberListAdd } from '../packets/send/PledgeShowMemberListAdd'

const millisBeforeCreatingClan = ConfigManager.character.getDaysBeforeCreateAClan() * 24 * 3600 * 1000
const millisBeforeJoiningClan = ConfigManager.character.getDaysBeforeJoinAClan() * 24 * 3600 * 1000

export class L2Clan {
    name: string
    clanId: number = 0
    leader: L2ClanMember
    members: { [ key: number ]: L2ClanMember } = {}
    leaderId: number = 0 // used during loading of clan from data source

    allyName: string = ''
    allyId: number = 0
    level: number = 0
    castleId: number = 0
    fortId: number = 0
    hideoutId: number = 0
    crestId: number = 0
    crestLargeId: number = 0
    allyCrestId: number = 0
    auctionIdWithBid: number = 0
    allyPenaltyExpiryTime: number = 0
    allyPenaltyType: number = 0
    characterPenaltyExpiryTime: number = 0
    dissolvingExpiryTime: number = 0
    bloodAllianceCount: number = 0
    bloodOathCount: number = 0

    warehouse: ItemContainer
    atWarWith: Set<number> = new Set<number>()
    atWarAttackers: Set<number> = new Set<number>()

    skills: { [ key: number ]: Skill } = {}
    privileges: { [ key: number ]: RankPrivileges } = {}
    subPledges: Record<number, ClanPledge> = {}
    subPledgeSkills: { [ key: number ]: Skill } = {}

    reputationScore: number = 0
    rank: number = 0

    noticeText: string = ''
    noticeEnabled: boolean = false
    newLeaderId: number = 0

    siegeKills: number = 0
    siegeDeaths: number = 0

    constructor( clanId: number ) {
        this.clanId = clanId
        this.initializePrivileges()
    }

    async addClanMember( player: L2PcInstance ): Promise<void> {
        let member: L2ClanMember = L2ClanMember.fromPlayer( this.getId(), player )
        member.setPlayerInstance( player )
        this.addClanMemberInstance( member )

        player.setClan( this )
        player.setPledgeClass( L2ClanMember.calculatePledgeClass( player ) )
        player.sendOwnedData( PledgeShowMemberListUpdate( player ) )
        player.sendOwnedData( PledgeSkillList( this ) )

        await this.addSkillEffects( player )
        PlayerRelationStatus.computeRelationStatus( player )

        if ( ListenerCache.hasGeneralListener( EventType.PlayerJoinsClan ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerJoinsClan ) as PlayerClanJoinEvent

            eventData.playerId = player.getObjectId()
            eventData.clanId = this.getId()
            eventData.clanLevel = this.getLevel()

            await ListenerCache.sendGeneralEvent( EventType.PlayerJoinsClan, eventData )
        }
    }

    addClanMemberInstance( member: L2ClanMember ) {
        this.members[ member.getObjectId() ] = member
    }

    async addNewSkill( skill: Skill, subType: number = -2 ) {
        let oldSkill: Skill
        if ( skill != null ) {

            if ( subType === -2 ) {
                oldSkill = this.skills[ skill.getId() ]
                this.skills[ skill.getId() ] = skill
            }

            if ( subType === 0 ) {
                oldSkill = this.subPledgeSkills[ skill.getId() ]
                this.subPledgeSkills[ skill.getId() ] = skill
            } else {
                let subunit: ClanPledge = this.getPledge( subType )
                if ( subunit ) {
                    oldSkill = this.addSkillToPledge( subunit, skill )
                } else {
                    return oldSkill
                }
            }

            if ( oldSkill ) {
                await DatabaseManager.getClanSkills().updateSkill( this.getId(), oldSkill.getId(), skill.getLevel() )
            } else {
                await DatabaseManager.getClanSkills().addSkill( this.getId(), skill.getId(), skill.getLevel(), skill.getName(), subType )
            }

            let packet = new SystemMessageBuilder( SystemMessageIds.CLAN_SKILL_S1_ADDED )
                    .addSkillNameById( skill.getId() )

            await aigle.resolve( this.members ).each( async ( member: L2ClanMember ) => {
                let player = member.getPlayerInstance()
                if ( !player || !member.isOnline() ) {
                    return
                }

                if ( subType === -2 ) {
                    if ( skill.getMinPledgeClass() <= player.getPledgeClass() ) {
                        await player.addSkill( skill, false )
                        player.sendOwnedData( PledgeSkillListAdd( skill.getId(), skill.getLevel() ) )
                        player.sendOwnedData( packet.getBuffer() )
                        player.sendSkillList()
                    }
                } else {
                    if ( member.getPledgeType() === subType ) {
                        await player.addSkill( skill, false )
                        player.sendOwnedData( ExSubPledgeSkillAdd( subType, skill.getId(), skill.getLevel() ) )
                        player.sendOwnedData( packet.getBuffer() )
                        player.sendSkillList()
                    }
                }
            } )
        }

        return oldSkill
    }

    addReputationScore( value: number, shouldSave: boolean ): Promise<void> {
        return this.setReputationScore( this.getReputationScore() + value, shouldSave )
    }

    addSiegeDeath() {
        this.siegeDeaths++
    }

    addSiegeKill() {
        this.siegeKills++
    }

    async addSkillEffects( player: L2PcInstance ): Promise<void> {
        if ( !player ) {
            return
        }

        await aigle.resolve( this.skills ).each( ( skill: Skill ) => {
            if ( skill.getMinPledgeClass() <= player.getPledgeClass() ) {
                return player.addSkill( skill, false )
            }
        } )

        if ( player.getPledgeType() === 0 ) {
            await aigle.resolve( this.subPledgeSkills ).each( ( skill: Skill ) => {
                return player.addSkill( skill, false )
            } )
        } else {
            let pledge: ClanPledge = this.getPledge( player.getPledgeType() )
            if ( !pledge ) {
                return
            }

            await aigle.resolve( pledge.skills ).each( ( skill: Skill ) => {
                return player.addSkill( skill, false )
            } )
        }

        if ( this.reputationScore < 0 ) {
            this.skillsStatus( player, true )
        }
    }

    broadcastClanStatus() : void {
        let playerIds = this.getOnlineMembers()

        if ( playerIds.length === 0 ) {
            return
        }

        let allPacket = PledgeShowMemberListAll( this )
        let deletePacket = PledgeShowMemberListDeleteAll()

        playerIds.forEach( ( memberId: number ) => {
            PacketDispatcher.sendCopyData( memberId, deletePacket )
            PacketDispatcher.sendOwnedData( memberId, allPacket )
        } )

        return this.onPledgeShowMemberListAll( playerIds )
    }

    broadcastDataToOnlineAllyMembers( packet: Buffer ) {
        _.each( ClanCache.getClanAllies( this.getAllyId() ), ( clan: L2Clan ) => {
            clan.broadcastDataToOnlineMembers( packet )
        } )
    }

    broadcastDataToOnlineMembers( packet: Buffer ) {
        _.each( this.members, ( member: L2ClanMember ) => {
            if ( member && member.isOnline() ) {
                PacketDispatcher.sendCopyData( member.playerId, packet )
            }
        } )
    }

    broadcastDataToOtherOnlineMembers( packet: Buffer, player: L2PcInstance ) {
        _.each( this.members, ( member: L2ClanMember ) => {
            if ( member && member.isOnline() && member.playerId !== player.getObjectId() ) {
                PacketDispatcher.sendCopyData( member.playerId, packet )
            }
        } )
    }

    broadcastBlockedDataToOtherOnlineMembers( packet: Buffer, player: L2PcInstance ) {
        _.each( this.members, ( member: L2ClanMember ) => {
            let memberPlayer = member.getPlayerInstance()
            if ( memberPlayer.isOnline()
                    && !BlocklistCache.isBlocked( memberPlayer.getObjectId(), player.getObjectId() ) ) {
                memberPlayer.sendCopyData( packet )
            }
        } )
    }

    async changeAllyCrest( crestId: number, useCurrentClan: boolean ) {
        if ( !useCurrentClan ) {
            if ( this.getAllyCrestId() !== 0 ) {
                await CrestCache.removeCrest( this.getAllyCrestId() )
            }
            await DatabaseManager.getClanDataTable().updateCrestForAlly( this.getAllyId(), crestId )
        } else {
            await DatabaseManager.getClanDataTable().updateCrestForClan( this.getId(), crestId )
        }

        let broadcastToMember = ( memberId: number ) => {
            let player = L2World.getPlayer( memberId )
            if ( player ) {
                player.broadcastUserInfo()
            }
        }

        if ( useCurrentClan ) {
            this.setAllyCrestId( crestId )

            _.each( this.getOnlineMembers(), broadcastToMember )
            return
        }

        _.each( ClanCache.getClanAllies( this.getAllyId() ), ( clan: L2Clan ) => {
            clan.setAllyCrestId( crestId )
            _.each( clan.getOnlineMembers(), broadcastToMember )
        } )
    }

    async changeClanCrest( crestId: number ) {
        if ( this.getCrestId() !== 0 ) {
            await CrestCache.removeCrest( this.getCrestId() )
        }

        this.setCrestId( crestId )

        return DatabaseManager.getClanDataTable().updateCrest( this.getId(), crestId )
    }

    async changeLargeCrest( crestId: number ) {
        if ( this.getCrestLargeId() !== 0 ) {
            await CrestCache.removeCrest( this.getCrestLargeId() )
        }

        this.setCrestLargeId( crestId )

        await DatabaseManager.getClanDataTable().updateLargeCrestForClan( this.getId(), crestId )

        _.each( this.getOnlineMembers(), ( memberId: number ) => {
            let player = L2World.getPlayer( memberId )
            if ( player ) {
                player.broadcastUserInfo()
            }
        } )
    }

    async changeLevel( level: number ): Promise<void> {
        await DatabaseManager.getClanDataTable().updateClanLevel( this.getId(), level )

        let previousLevel = this.getLevel()
        this.setLevel( level )

        if ( ListenerCache.hasGeneralListener( EventType.ClanChangeLevel ) ) {
            let eventData = EventPoolCache.getData( EventType.ClanChangeLevel ) as ClanChangeLevelEvent

            eventData.clanId = this.getId()
            eventData.previousLevel = previousLevel
            eventData.nextLevel = level

            await ListenerCache.sendGeneralEvent( EventType.ClanChangeLevel, eventData )
        }

        let leader: L2PcInstance = this.getLeader().getPlayerInstance()
        if ( leader ) {
            if ( level > 4 ) {
                await SiegeManager.addSiegeSkills( leader )
                leader.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_CAN_ACCUMULATE_CLAN_REPUTATION_POINTS ) )
            } else {
                await SiegeManager.removeSiegeSkills( leader )
            }
        }

        if ( previousLevel < level ) {
            this.broadcastDataToOnlineMembers( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_LEVEL_INCREASED ) )
        }

        this.broadcastDataToOnlineMembers( PledgeShowInfoUpdate( this ) )
    }

    checkAllyJoinCondition( player: L2PcInstance, target: L2PcInstance ) {
        if ( !player ) {
            return false
        }

        if ( ( player.getAllyId() === 0 ) || !player.isClanLeader() || ( player.getClanId() !== player.getAllyId() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FEATURE_ONLY_FOR_ALLIANCE_LEADER ) )
            return false
        }

        let leaderClan: L2Clan = player.getClan()
        if ( leaderClan.getAllyPenaltyExpiryTime() > Date.now() ) {
            if ( leaderClan.getAllyPenaltyType() === L2ClanValues.PENALTY_TYPE_DISMISS_CLAN ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_INVITE_CLAN_WITHIN_1_DAY ) )
                return false
            }
        }

        if ( !target ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_INVITED_THE_WRONG_TARGET ) )
            return false
        }

        if ( player.getObjectId() === target.getObjectId() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_INVITE_YOURSELF ) )
            return false
        }

        if ( !target.getClan() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_MUST_BE_IN_CLAN ) )
            return false
        }

        if ( !target.isClanLeader() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_IS_NOT_A_CLAN_LEADER )
                    .addString( target.getName() )
                    .getBuffer()
            player.sendOwnedData( packet )
            return false
        }

        let targetClan: L2Clan = target.getClan()
        if ( target.getAllyId() !== 0 ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_CLAN_ALREADY_MEMBER_OF_S2_ALLIANCE )
                    .addString( targetClan.getName() )
                    .addString( targetClan.getAllyName() )
                    .getBuffer()
            player.sendOwnedData( packet )
            return false
        }

        if ( targetClan.getAllyPenaltyExpiryTime() > Date.now() ) {
            if ( targetClan.getAllyPenaltyType() === L2ClanValues.PENALTY_TYPE_CLAN_LEFT ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANT_ENTER_ALLIANCE_WITHIN_1_DAY )
                        .addString( target.getClan().getName() )
                        .addString( target.getClan().getAllyName() )
                        .getBuffer()
                player.sendOwnedData( packet )
                return false
            }

            if ( targetClan.getAllyPenaltyType() === L2ClanValues.PENALTY_TYPE_CLAN_DISMISSED ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_ENTER_ALLIANCE_WITHIN_1_DAY ) )
                return false
            }
        }
        if ( ( player.isInArea( AreaType.CastleSiege ) && target.isInArea( AreaType.CastleSiege ) )
            || ( player.isInArea( AreaType.FortSiege ) && target.isInArea( AreaType.FortSiege ) ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.OPPOSING_CLAN_IS_PARTICIPATING_IN_SIEGE ) )
            return false
        }
        if ( leaderClan.isAtWarWith( targetClan.getId() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MAY_NOT_ALLY_CLAN_BATTLE ) )
            return false
        }

        if ( ClanCache.getClanAllies( player.getAllyId() ).length >= ConfigManager.character.getMaxNumberOfClansInAlly() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_EXCEEDED_THE_LIMIT ) )
            return false
        }

        return true
    }

    checkClanJoinCondition( player: L2PcInstance, target: L2PcInstance, pledgeType: number ) {
        if ( !player ) {
            return false
        }

        if ( !player.hasClanPrivilege( ClanPrivilege.InviteToClan ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
            return false
        }

        if ( !target ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_INVITED_THE_WRONG_TARGET ) )
            return false
        }

        if ( player.getObjectId() === target.getObjectId() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_INVITE_YOURSELF ) )
            return false
        }

        if ( this.getCharacterPenaltyExpiryTime() > Date.now() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_MUST_WAIT_BEFORE_ACCEPTING_A_NEW_MEMBER ) )
            return false
        }
        if ( target.getClanId() !== 0 ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_WORKING_WITH_ANOTHER_CLAN )
                    .addString( target.getName() )
                    .getBuffer()
            player.sendOwnedData( packet )
            return false
        }

        if ( target.getClanJoinExpiryTime() > Date.now() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.C1_MUST_WAIT_BEFORE_JOINING_ANOTHER_CLAN )
                    .addString( target.getName() )
                    .getBuffer()
            player.sendOwnedData( packet )
            return false
        }

        if ( ( target.getLevel() > 40 || ClassId.getClassIdByIdentifier( target.getClassId() ).level >= 2 )
                && pledgeType === -1 ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_DOESNOT_MEET_REQUIREMENTS_TO_JOIN_ACADEMY )
                    .addString( target.getName() )
                    .getBuffer()
            player.sendOwnedData( packet )

            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ACADEMY_REQUIREMENTS ) )
            return false
        }

        if ( this.getSubPledgeMembersCount( pledgeType ) >= this.getMaxNumberOfMembers( pledgeType ) ) {
            if ( pledgeType === 0 ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_CLAN_IS_FULL )
                        .addString( this.getName() )
                        .getBuffer()
                player.sendOwnedData( packet )
                return false
            }

            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SUBCLAN_IS_FULL ) )
            return false
        }

        return true
    }

    deleteAttackerClan( clan: L2Clan ) {
        this.atWarAttackers.delete( clan.getId() )
    }

    deleteEnemyClan( clan: L2Clan ) {
        this.atWarWith.delete( clan.getId() )
    }

    async dissolveAlly( player: L2PcInstance ): Promise<void> {
        if ( this.getAllyId() === 0 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_CURRENT_ALLIANCES ) )
            return
        }

        if ( !player.isClanLeader() || ( this.getId() !== this.getAllyId() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FEATURE_ONLY_FOR_ALLIANCE_LEADER ) )
            return
        }

        if ( player.isInSiegeArea() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISSOLVE_ALLY_WHILE_IN_SIEGE ) )
            return
        }

        this.broadcastDataToOnlineAllyMembers( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALLIANCE_DISOLVED ) )

        let currentClanId = this.getId()
        await aigle.resolve( ClanCache.getClanAllies( this.getAllyId() ) ).eachSeries( async ( clan: L2Clan ) => {
            if ( clan.getId() !== currentClanId ) {
                clan.setAllyId( 0 )
                clan.setAllyName( null )
                clan.setAllyPenaltyExpiryTime( 0, 0 )
                clan.scheduleUpdate()

                clan.updateRelationStatusForMembers()
            }
        } )

        this.setAllyId( 0 )
        this.setAllyName( null )
        await this.changeAllyCrest( 0, false )

        this.setAllyPenaltyExpiryTime( Date.now() + PacketVariables.millisInDay * ConfigManager.character.getDaysBeforeCreateNewAllyWhenDissolved(), L2ClanValues.PENALTY_TYPE_DISSOLVE_ALLY )
        this.scheduleUpdate()
    }

    getAllRankPrivileges() {
        return _.values( this.privileges )
    }

    getAllSubPledges(): Array<ClanPledge> {
        return Object.values( this.subPledges )
    }

    getAllyCrestId() {
        return this.allyCrestId
    }

    getAllyId() {
        return this.allyId
    }

    getAllyName() {
        return this.allyName
    }

    getAllyPenaltyExpiryTime() {
        return this.allyPenaltyExpiryTime
    }

    getAllyPenaltyType() {
        return this.allyPenaltyType
    }

    getAttackClanIds() {
        return this.atWarAttackers
    }

    getAuctionIdWithBid(): number {
        return this.auctionIdWithBid
    }

    getBloodAllianceCount() {
        return this.bloodAllianceCount
    }

    getBloodOathCount() {
        return this.bloodOathCount
    }

    getCastleId() {
        return this.castleId
    }

    getCharacterPenaltyExpiryTime() {
        return this.characterPenaltyExpiryTime
    }

    getClanMember( objectId: number ): L2ClanMember {
        return this.members[ objectId ]
    }

    getClanMemberByName( name: string ): L2ClanMember {
        return _.find( this.members, ( member: L2ClanMember ) => {
            return member.getName() === name
        } )
    }

    getCrestId(): number {
        return this.crestId
    }

    getCrestLargeId() {
        return this.crestLargeId
    }

    getDissolvingExpiryTime() {
        return this.dissolvingExpiryTime
    }

    getFortId() {
        return this.fortId
    }

    getHideoutId() {
        return this.hideoutId
    }

    getId(): number {
        return this.clanId
    }

    getLeader() {
        return this.leader
    }

    getLeaderId() {
        return this.leader ? this.leader.getObjectId() : 0
    }

    getLeaderName() {
        if ( this.leader ) {
            return this.leader.getName()
        }

        return ''
    }

    getLeaderSubPledge( leaderId: number ): number {
        let outcome = 0

        _.each( this.subPledges, ( pledge: ClanPledge ) => {
            if ( pledge.leaderId && pledge.leaderId === leaderId ) {
                outcome = pledge.type
            }
        } )

        return outcome
    }

    getLevel() {
        return this.level
    }

    getMaxNumberOfMembers( pledgeType: number ) {
        switch ( pledgeType ) {
            case 0:
                switch ( this.getLevel() ) {
                    case 3:
                        return 30
                    case 2:
                        return 20
                    case 1:
                        return 15
                    case 0:
                        return 10
                    default:
                        return 40
                }
            case -1:
                return 20
            case 100:
            case 200:
                return this.getLevel() === 11 ? 30 : 20
            case 1001:
            case 1002:
            case 2001:
            case 2002:
                switch ( this.getLevel() ) {
                    case 9:
                    case 10:
                    case 11:
                        return 25
                    default:
                        return 10
                }
        }

        return 0
    }

    getMembers(): Array<L2ClanMember> {
        return _.values( this.members )
    }

    getMembersCount(): number {
        return _.size( this.members )
    }

    getName() {
        return this.name
    }

    getNewLeaderId() {
        return this.newLeaderId
    }

    getNotice() {
        return this.noticeText
    }

    getOnlineMembers( idToExclude: number = 0 ): Array<number> {
        return _.reduce( this.members, ( applicableMembers: Array<number>, member: L2ClanMember ) => {
            if ( member && member.isOnline() && member.getObjectId() !== idToExclude ) {
                applicableMembers.push( member.getObjectId() )
            }

            return applicableMembers
        }, [] )
    }

    getOnlineMembersCount() {
        return _.reduce( this.members, ( total: number, member: L2ClanMember ) => {
            if ( member && member.isOnline() ) {
                total++
            }

            return total
        }, 0 )
    }

    getRank() {
        return this.rank
    }

    getRankPrivileges( rank: number ): number {
        let rankPrivilege = this.privileges[ rank ]
        if ( rankPrivilege ) {
            return rankPrivilege.privileges
        }

        return 0
    }

    getReputationScore() {
        return this.reputationScore
    }

    getSiegeDeaths() {
        return this.siegeDeaths
    }

    getSiegeKills() {
        return this.siegeKills
    }

    getSkills() {
        return this.skills
    }

    getPledge( pledgeType: number ) {
        return this.subPledges[ pledgeType ]
    }

    getSubPledgeMembersCount( pledgeType: number ) {
        return _.reduce( this.members, ( count: number, member: L2ClanMember ) => {
            if ( member.getPledgeType() === pledgeType ) {
                count++
            }

            return count
        }, 0 )
    }

    getWarClanIds() {
        return this.atWarWith
    }

    getWarehouse(): ItemContainer {
        if ( !this.warehouse ) {
            this.warehouse = new ClanWarehouse( this )
        }
        return this.warehouse
    }

    async increaseBloodOathCount() {
        this.bloodOathCount += ConfigManager.fortress.getBloodOathCount()
        return DatabaseManager.getClanDataTable().updateBloodOathCount( this.getId(), this.getBloodOathCount() )
    }

    initializePrivileges() {
        for ( let rank = 1; rank < 11; rank++ ) {
            this.privileges[ rank ] = this.createRankPrivileges( rank, 0 )
        }
    }

    isAtWar() {
        return this.atWarWith.size === 0
    }

    isAtWarWith( clanId: number ) : boolean {
        return this.atWarWith.has( clanId )
    }

    isLearnableSubPledgeSkill( skill: Skill, subType: number ): boolean {
        if ( subType === -1 ) {
            return false
        }

        let id = skill.getId()
        let currentSkill: Skill
        if ( subType === 0 ) {
            currentSkill = this.subPledgeSkills[ id ]
        } else {
            let pledge = this.subPledges[ subType ]
            currentSkill = pledge ? this.getPledgeSkill( pledge, id ) : null
        }

        if ( currentSkill && ( ( currentSkill.getLevel() + 1 ) === skill.getLevel() ) ) {
            return true
        }

        return !currentSkill && skill.getLevel() === 1
    }

    isLearnableSubSkill( skillId: number, skillLevel: number ) {
        let currentSkill: Skill = this.subPledgeSkills[ skillId ]
        if ( currentSkill && ( ( currentSkill.getLevel() + 1 ) === skillLevel ) ) {
            return true
        }

        if ( !currentSkill && ( skillLevel === 1 ) ) {
            return true
        }

        return _.some( this.subPledges, ( item: ClanPledge ) => {
            if ( item.type === -1 ) {
                return false
            }
            let skill = this.getPledgeSkill( item, skillId )
            if ( skill && ( ( skill.getLevel() + 1 ) === skillLevel ) ) {
                return true
            }

            return !skill && skillLevel === 1
        } )
    }

    isMember( objectId: number ): boolean {
        return !!this.members[ objectId ]
    }

    isNoticeEnabled() {
        return this.noticeEnabled
    }

    private getPledgeSkill( pledge: ClanPledge, skillId: number ) : Skill {
        return pledge.skills.find( skill => skill.getId() === skillId )
    }

    async attemptLevelup( player: L2PcInstance ): Promise<boolean> {
        if ( !player.isClanLeader() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
            return false
        }

        if ( Date.now() < this.getDissolvingExpiryTime() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_RISE_LEVEL_WHILE_DISSOLUTION_IN_PROGRESS ) )
            return false
        }

        let increaseClanLevel = false
        let oldLevel = this.getLevel()

        switch ( oldLevel ) {
            case 0:
                if ( player.getSp() >= 20000 && ( player.getAdena() >= 650000 ) ) {
                    if ( await player.reduceAdena( 650000, true, 'L2Clan.levelUpClan' ) ) {
                        player.setSp( player.getSp() - 20000 )

                        let message = new SystemMessageBuilder( SystemMessageIds.SP_DECREASED_S1 )
                                .addNumber( 20000 )
                                .getBuffer()
                        player.sendOwnedData( message )
                        increaseClanLevel = true
                    }
                }
                break

            case 1:
                if ( player.getSp() >= 100000 && ( player.getAdena() >= 2500000 ) ) {
                    if ( await player.reduceAdena( 2500000, true, 'L2Clan.levelUpClan' ) ) {
                        player.setSp( player.getSp() - 100000 )

                        let message = new SystemMessageBuilder( SystemMessageIds.SP_DECREASED_S1 )
                                .addNumber( 100000 )
                                .getBuffer()
                        player.sendOwnedData( message )
                        increaseClanLevel = true
                    }
                }
                break

            case 2:
                if ( player.getSp() >= 350000 && player.getInventory().getItemByItemId( 1419 ) ) {
                    // itemId 1419 == Blood Mark
                    if ( await player.destroyItemByItemId( 1419, 1, false, 'L2Clan.levelUpClan' ) ) {
                        player.setSp( player.getSp() - 350000 )

                        let firstMessage = new SystemMessageBuilder( SystemMessageIds.SP_DECREASED_S1 )
                                .addNumber( 350000 )
                                .getBuffer()
                        player.sendOwnedData( firstMessage )

                        let secondMessage = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                                .addItemNameWithId( 1419 )
                                .getBuffer()
                        player.sendOwnedData( secondMessage )
                        increaseClanLevel = true
                    }
                }
                break

            case 3:
                if ( player.getSp() >= 1000000 && player.getInventory().getItemByItemId( 3874 ) ) {
                    // itemId 3874 == Alliance Manifesto
                    if ( await player.destroyItemByItemId( 3874, 1, false, 'L2Clan.levelUpClan' ) ) {
                        player.setSp( player.getSp() - 1000000 )

                        let firstMessage = new SystemMessageBuilder( SystemMessageIds.SP_DECREASED_S1 )
                                .addNumber( 1000000 )
                                .getBuffer()
                        player.sendOwnedData( firstMessage )

                        let secondMessage = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                                .addItemNameWithId( 3874 )
                                .getBuffer()
                        player.sendOwnedData( secondMessage )
                        increaseClanLevel = true
                    }
                }
                break

            case 4:
                if ( player.getSp() >= 2500000 && player.getInventory().getItemByItemId( 3870 ) ) {
                    // itemId 3870 == Seal of Aspiration
                    if ( await player.destroyItemByItemId( 3870, 1, false, 'L2Clan.levelUpClan' ) ) {
                        player.setSp( player.getSp() - 2500000 )

                        let firstMessage = new SystemMessageBuilder( SystemMessageIds.SP_DECREASED_S1 )
                                .addNumber( 2500000 )
                                .getBuffer()
                        player.sendOwnedData( firstMessage )

                        let secondMessage = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                                .addItemNameWithId( 3870 )
                                .getBuffer()
                        player.sendOwnedData( secondMessage )
                        increaseClanLevel = true
                    }
                }
                break

            case 5:
                if ( this.getReputationScore() >= ConfigManager.clan.getClanLevel6Cost()
                        && this.getMembersCount() >= ConfigManager.clan.getClanLevel6Requirement() ) {
                    await this.setReputationScore( this.getReputationScore() - ConfigManager.clan.getClanLevel6Cost(), true )

                    let message = new SystemMessageBuilder( SystemMessageIds.S1_DEDUCTED_FROM_CLAN_REP )
                            .addNumber( ConfigManager.clan.getClanLevel6Cost() )
                            .getBuffer()
                    player.sendOwnedData( message )
                    increaseClanLevel = true
                }
                break

            case 6:
                if ( this.getReputationScore() >= ConfigManager.clan.getClanLevel7Cost()
                        && this.getMembersCount() >= ConfigManager.clan.getClanLevel7Requirement() ) {
                    await this.setReputationScore( this.getReputationScore() - ConfigManager.clan.getClanLevel7Cost(), true )

                    let message = new SystemMessageBuilder( SystemMessageIds.S1_DEDUCTED_FROM_CLAN_REP )
                            .addNumber( ConfigManager.clan.getClanLevel7Cost() )
                            .getBuffer()
                    player.sendOwnedData( message )
                    increaseClanLevel = true
                }
                break

            case 7:
                if ( this.getReputationScore() >= ConfigManager.clan.getClanLevel8Cost()
                        && this.getMembersCount() >= ConfigManager.clan.getClanLevel8Requirement() ) {
                    await this.setReputationScore( this.getReputationScore() - ConfigManager.clan.getClanLevel8Cost(), true )

                    let message = new SystemMessageBuilder( SystemMessageIds.S1_DEDUCTED_FROM_CLAN_REP )
                            .addNumber( ConfigManager.clan.getClanLevel8Cost() )
                            .getBuffer()
                    player.sendOwnedData( message )
                    increaseClanLevel = true
                }
                break

            case 8:
                if ( this.getReputationScore() >= ConfigManager.clan.getClanLevel9Cost()
                        && player.getInventory().getItemByItemId( 9910 )
                        && this.getMembersCount() >= ConfigManager.clan.getClanLevel9Requirement() ) {
                    // itemId 9910 == Blood Oath
                    if ( await player.destroyItemByItemId( 9910, 150, false, 'L2Clan.levelUpClan' ) ) {
                        await this.setReputationScore( this.getReputationScore() - ConfigManager.clan.getClanLevel9Cost(), true )

                        let firstMessage = new SystemMessageBuilder( SystemMessageIds.S1_DEDUCTED_FROM_CLAN_REP )
                                .addNumber( ConfigManager.clan.getClanLevel9Cost() )
                                .getBuffer()
                        player.sendOwnedData( firstMessage )

                        let secondMessage = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                                .addItemNameWithId( 9910 )
                                .addNumber( 150 )
                                .getBuffer()
                        player.sendOwnedData( secondMessage )
                        increaseClanLevel = true
                    }
                }
                break

            case 9:
                if ( this.getReputationScore() >= ConfigManager.clan.getClanLevel10Cost()
                        && player.getInventory().getItemByItemId( 9911 )
                        && this.getMembersCount() >= ConfigManager.clan.getClanLevel10Requirement() ) {
                    // itemId 9911 == Blood Alliance
                    if ( await player.destroyItemByItemId( 9911, 5, false, 'L2Clan.levelUpClan' ) ) {
                        await this.setReputationScore( this.getReputationScore() - ConfigManager.clan.getClanLevel10Cost(), true )

                        let firstMessage = new SystemMessageBuilder( SystemMessageIds.S1_DEDUCTED_FROM_CLAN_REP )
                                .addNumber( ConfigManager.clan.getClanLevel10Cost() )
                                .getBuffer()
                        player.sendOwnedData( firstMessage )

                        let secondMessage = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                                .addItemNameWithId( 9911 )
                                .addNumber( 5 )
                                .getBuffer()
                        player.sendOwnedData( secondMessage )
                        increaseClanLevel = true
                    }
                }
                break

            case 10:
                let clanId = this.getId()
                let hasTerritory = _.some( TerritoryWarManager.getAllTerritories(), ( territory: Territory ) => {
                    return territory.getOwnerClan() && territory.getOwnerClan().getId() === clanId
                } )

                if ( hasTerritory
                        && this.getReputationScore() >= ConfigManager.clan.getClanLevel11Cost()
                        && this.getMembersCount() >= ConfigManager.clan.getClanLevel11Requirement() ) {
                    await this.setReputationScore( this.getReputationScore() - ConfigManager.clan.getClanLevel11Cost(), true )

                    let message = new SystemMessageBuilder( SystemMessageIds.S1_DEDUCTED_FROM_CLAN_REP )
                            .addNumber( ConfigManager.clan.getClanLevel11Cost() )
                            .getBuffer()
                    player.sendOwnedData( message )
                    increaseClanLevel = true
                }
                break

            default:
                return false
        }

        if ( !increaseClanLevel ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_TO_INCREASE_CLAN_LEVEL ) )
            return false
        }

        player.sendOwnedData( StatusUpdate.forValue( player.getObjectId(), StatusUpdateProperty.SP, player.getSp() ) )

        await this.changeLevel( this.getLevel() + 1 )

        if ( ListenerCache.hasGeneralListener( EventType.PlayerClanLevelup ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerClanLevelup ) as PlayerClanLevelUpEvent

            eventData.clanId = this.getId()
            eventData.oldLevel = oldLevel
            eventData.newLevel = this.getLevel()

            await ListenerCache.sendGeneralEvent( EventType.PlayerClanLevelup, eventData )
        }

        return true
    }

    async removeClanMember( objectId: number, clanJoinExpiryTime: number ): Promise<void> {
        let exMember: L2ClanMember = this.members[ objectId ]

        delete this.members[ objectId ]

        if ( !exMember ) {
            return
        }

        let subPledgeLeader: number = this.getLeaderSubPledge( objectId )
        if ( subPledgeLeader !== 0 ) {
            let pledge = this.getPledge( subPledgeLeader )
            pledge.leaderId = 0

            await DatabaseManager.getClanSubpledges().updatePledge( 0, pledge.name, this.getId(), subPledgeLeader )
        }

        if ( exMember.getApprentice() !== 0 ) {
            let apprentice: L2ClanMember = this.getClanMember( exMember.getApprentice() )
            if ( apprentice ) {
                if ( apprentice.getPlayerInstance() ) {
                    apprentice.getPlayerInstance().setSponsor( 0 )
                } else {
                    apprentice.setApprenticeAndSponsor( 0, 0 )
                }

                await apprentice.saveApprenticeAndSponsor( 0, 0 )
            }
        }
        if ( exMember.getSponsor() !== 0 ) {
            let sponsor: L2ClanMember = this.getClanMember( exMember.getSponsor() )
            if ( sponsor ) {
                if ( sponsor.getPlayerInstance() ) {
                    sponsor.getPlayerInstance().setApprentice( 0 )
                } else {
                    sponsor.setApprenticeAndSponsor( 0, 0 )
                }

                await sponsor.saveApprenticeAndSponsor( 0, 0 )
            }
        }

        await exMember.saveApprenticeAndSponsor( 0, 0 )

        if ( ConfigManager.character.removeCastleCirclets() ) {
            await CastleManager.removeMemberCirclet( exMember, this.getCastleId() )
        }

        let player: L2PcInstance = exMember.getPlayerInstance()
        if ( player ) {
            if ( !player.isNoble() ) {
                player.setTitle( '' )
            }

            player.setApprentice( 0 )
            player.setSponsor( 0 )

            if ( player.isClanLeader() ) {
                await SiegeManager.removeSiegeSkills( player )
                player.setClanCreateExpiryTime( Date.now() + millisBeforeCreatingClan )
            }

            await this.removeSkillEffects( player )

            if ( player.getClan().getCastleId() > 0 ) {
                await CastleManager.getCastleByOwner( player.getClan() ).removeResidentialSkills( player )
            }
            if ( player.getClan().getFortId() > 0 ) {
                await FortManager.getFortByOwner( player.getClan() ).removeResidentialSkills( player )
            }

            player.sendSkillList()
            player.setClan( null )

            if ( exMember.getPledgeType() !== -1 ) {
                player.setClanJoinExpiryTime( clanJoinExpiryTime )
            }

            player.setPledgeClass( L2ClanMember.calculatePledgeClass( player ) )
            player.broadcastUserInfo()

            player.sendOwnedData( PledgeShowMemberListDeleteAll() )
            PlayerRelationStatus.computeRelationStatus( player )
        } else {
            let createExpiryTime = this.getLeaderId() === objectId ? Date.now() + millisBeforeCreatingClan : 0
            await DatabaseManager.getCharacterTable().updateClanStatus( objectId, '', clanJoinExpiryTime, createExpiryTime )
            await DatabaseManager.getCharacterTable().resetApprentice( objectId )
            await DatabaseManager.getCharacterTable().resetSponsor( objectId )
        }

        if ( ListenerCache.hasGeneralListener( EventType.PlayerLeavesClan ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerLeavesClan ) as PlayerLeftClanEvent

            eventData.playerId = objectId
            eventData.clanId = this.getId()
            eventData.clanLevel = this.getLevel()

            return ListenerCache.sendGeneralEvent( EventType.PlayerLeavesClan, eventData )
        }
    }

    async removeSkillEffects( player: L2PcInstance ): Promise<void> {
        await aigle.resolve( this.skills ).eachSeries( ( skill: Skill ) => {
            return player.removeSkill( skill, false )
        } )

        if ( player.getPledgeType() === 0 ) {
            await aigle.resolve( this.subPledgeSkills ).eachSeries( ( skill: Skill ) => {
                return player.removeSkill( skill, false )
            } )

            return
        }

        let pledge: ClanPledge = this.getPledge( player.getPledgeType() )
        if ( !pledge ) {
            return
        }

        await aigle.resolve( pledge.skills ).eachSeries( ( skill: Skill ) => {
            return player.removeSkill( skill, false )
        } )
    }

    async resetBloodAllianceCount(): Promise<void> {
        this.bloodAllianceCount = 0
        await this.updateBloodAllianceCountInDatabase()
    }

    async restoreMembers() {
        let members = await DatabaseManager.getCharacterTable().getClanMembers( this.getId() )

        members.forEach( ( member: L2ClanMember ) => {
            this.members[ member.objectId ] = member

            if ( member.objectId === this.leaderId ) {
                this.leader = member
            }
        } )
    }

    async restoreNotice(): Promise<void> {
        let data: L2ClanNoticesTableItem = await DatabaseManager.getClanNotices().getNotice( this.getId() )

        if ( data ) {
            this.noticeEnabled = data.status === L2ClanNoticeStatus.Enabled
            this.noticeText = data.text
        }
    }

    async restoreRankPrivileges() {
        let privilegeMap = await DatabaseManager.getClanPrivileges().getAll( this.getId() )

        const clan = this
        _.each( privilegeMap, ( mask: number, rank: string ) => {
            clan.privileges[ rank ].setPriviledges( mask )
        } )
    }

    async restoreRest(): Promise<void> {

        if ( this.getAllyPenaltyExpiryTime() < Date.now() ) {
            this.setAllyPenaltyExpiryTime( 0, 0 )
        }

        if ( ( this.getCharacterPenaltyExpiryTime() + millisBeforeJoiningClan ) < Date.now() ) {
            this.setCharacterPenaltyExpiryTime( 0 )
        }

        await this.restoreMembers()
        await this.restoreSubPledges()
        await this.restoreRankPrivileges()
        await this.restoreSkills()
        return this.restoreNotice()
    }

    async restoreSkills() {
        let skills: Array<ClanSkillsDatabaseItem> = await DatabaseManager.getClanSkills().getAll( this.getId() )

        skills.forEach( ( item: ClanSkillsDatabaseItem ) => {

            let skill = SkillCache.getSkill( item.skillId, item.skillLevel )
            if ( item.pledgeId === -2 ) {
                this.skills[ item.skillId ] = skill
                return
            }

            if ( item.pledgeId === 0 ) {
                this.subPledgeSkills[ item.skillId ] = skill
                return
            }

            let pledge: ClanPledge = this.subPledges[ item.pledgeId ]
            if ( pledge ) {
                this.addSkillToPledge( pledge, skill )
            }
        } )
    }

    private addSkillToPledge( pledge: ClanPledge, skill: Skill ) : Skill {
        let oldSkill = pledge.skills.find( existingSkill => existingSkill.getId() === skill.getId() )
        _.pull( pledge.skills, oldSkill )

        pledge.skills.push( skill )
        return oldSkill
    }

    async restoreSubPledges() {
        let items : Array<L2ClanPledgeTableItem> = await DatabaseManager.getClanSubpledges().getPledges( this.getId() )
        this.subPledges = items.reduce( ( allPledges: Record<number, ClanPledge>, item: L2ClanPledgeTableItem ) : Record<number, ClanPledge> => {

            allPledges[ item.type ] = {
                leaderId: item.leaderId,
                name: item.name,
                skills: [],
                type: item.type
            }

            return allPledges
        }, {} )
    }

    setAllyCrestId( value: number ) {
        this.allyCrestId = value
    }

    setAllyId( id: number ) {
        this.allyId = id
    }

    setAllyName( name: string ) {
        this.allyName = name
    }

    setAllyPenaltyExpiryTime( time: number, type: number ) {
        this.allyPenaltyExpiryTime = time
        this.allyPenaltyType = type
    }

    setAttackerClan( clan: L2Clan ) {
        this.atWarAttackers.add( clan.getId() )
    }

    async setAuctionBidAt( id: number, storeInDatabase: boolean ): Promise<void> {
        this.auctionIdWithBid = id
        if ( storeInDatabase ) {
            await DatabaseManager.getClanDataTable().updateAuction( this.getId(), id )
        }
    }

    setCastleId( value: number ) {
        this.castleId = value
    }

    setCharacterPenaltyExpiryTime( value: number ) {
        this.characterPenaltyExpiryTime = value
    }

    setCrestId( value: number ) {
        this.crestId = value
    }

    setCrestLargeId( value: number ) {
        this.crestLargeId = value
    }

    setDissolvingExpiryTime( value: number ) {
        this.dissolvingExpiryTime = value
    }

    setEnemyClan( clan: L2Clan ) {
        this.atWarWith.add( clan.getId() )
    }

    setFortId( value: number ) {
        this.fortId = value
    }

    setHideoutId( id: number ) {
        this.hideoutId = id
    }

    setLeader( leader: L2ClanMember ) {
        this.leader = leader
        this.members[ leader.getObjectId() ] = leader
    }

    setLeaderId( id: number ) {
        this.leaderId = id
    }

    setLevel( value: number ) {
        this.level = value
    }

    setName( name: string ) {
        this.name = name
    }

    async setNewLeader( member: L2ClanMember ): Promise<void> {
        let newLeader: L2PcInstance = member.getPlayerInstance()
        let exMember: L2ClanMember = this.getLeader()
        let exLeader: L2PcInstance = exMember.getPlayerInstance()

        if ( ListenerCache.hasGeneralListener( EventType.PlayerClanLeaderChange ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerClanLeaderChange ) as PlayerClanLeaderChangeEvent

            eventData.clanId = this.getId()
            eventData.newLeaderId = newLeader.getObjectId()
            eventData.oldLeaderId = exLeader ? exLeader.getObjectId() : 0

            await ListenerCache.sendGeneralEvent( EventType.PlayerClanLeaderChange, eventData )
        }

        if ( exLeader ) {
            if ( exLeader.isFlying() ) {
                await exLeader.dismount()
            }

            if ( this.getLevel() >= SiegeManager.getSiegeClanMinLevel() ) {
                await SiegeManager.removeSiegeSkills( exLeader )
            }
            exLeader.setClanPrivileges( ClanPriviledgeHelper.getDefaultPrivileges() )
            exLeader.broadcastUserInfo()

        }

        await DatabaseManager.getCharacterTable().setClanPrivileges( this.getLeaderId(), ClanPriviledgeHelper.getDefaultPrivileges() )

        this.setLeader( member )
        if ( this.getNewLeaderId() !== 0 ) {
            this.setNewLeaderId( 0, true )
        } else {
            this.scheduleUpdate()
        }

        if ( exLeader ) {
            exLeader.setPledgeClass( L2ClanMember.calculatePledgeClass( exLeader ) )
            exLeader.broadcastUserInfo()
            await exLeader.checkItemRestriction()
        }

        if ( newLeader ) {
            newLeader.setPledgeClass( L2ClanMember.calculatePledgeClass( newLeader ) )
            newLeader.setClanPrivileges( ClanPriviledgeHelper.getFullPrivileges() )

            if ( this.getLevel() >= SiegeManager.getSiegeClanMinLevel() ) {
                await SiegeManager.addSiegeSkills( newLeader )
            }
            newLeader.broadcastUserInfo()
            PlayerRelationStatus.computeRelationStatus( newLeader )
        }

        await DatabaseManager.getCharacterTable().setClanPrivileges( this.getLeaderId(), ClanPriviledgeHelper.getFullPrivileges() )
        this.broadcastClanStatus()

        let message: Buffer = new SystemMessageBuilder( SystemMessageIds.CLAN_LEADER_PRIVILEGES_HAVE_BEEN_TRANSFERRED_TO_C1 )
                .addString( member.getName() )
                .getBuffer()
        this.broadcastDataToOnlineMembers( message )
    }

    setNewLeaderId( id: number, storeInDatabase: boolean ): void {
        if ( this.newLeaderId === id ) {
            return
        }


        this.newLeaderId = id

        if ( ListenerCache.hasGeneralListener( EventType.PlayerClanLeaderChangeScheduled ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerClanLeaderChangeScheduled ) as PlayerClanLeaderScheduledEvent

            eventData.clanId = this.getId()
            eventData.newLeaderId = id
            eventData.oldLeaderId = this.getLeaderId()

            ListenerCache.sendGeneralEvent( EventType.PlayerClanLeaderChangeScheduled, eventData )
        }

        if ( storeInDatabase ) {
            return this.scheduleUpdate()
        }
    }

    resetNewLeaderId(): void {
        this.newLeaderId = 0
    }

    async setRankPrivileges( rank: number, privileges: number ): Promise<void> {
        let rankPrivileges: RankPrivileges = this.privileges[ rank ]

        await DatabaseManager.getClanPrivileges().storePrivilege( this.getId(), rank, privileges )

        if ( rankPrivileges ) {
            rankPrivileges.privileges = privileges

            this.getMembers().forEach( ( member: L2ClanMember ) => {
                let player = member.getPlayerInstance()
                if ( player && player.isOnline() && member.getPowerGrade() === rank ) {
                    player.setClanPrivileges( privileges )
                    player.sendDebouncedPacket( UserInfo )
                    player.sendDebouncedPacket( ExBrExtraUserInfo )
                }
            } )

            this.broadcastClanStatus()
            return
        }

        this.privileges[ rank ] = this.createRankPrivileges( rank, privileges )
    }

    private createRankPrivileges( rank: number, privileges: number ) : RankPrivileges {
        return {
            privileges,
            rank
        }
    }

    async setReputationScore( value: number, shouldSave: boolean ): Promise<void> {
        const clan = this
        const applySkillStatus = ( member: L2ClanMember ) => {
            let player = member.getPlayerInstance()
            if ( member.isOnline() && player ) {
                clan.skillsStatus( player, true )
            }
        }

        if ( ( this.reputationScore >= 0 ) && ( value < 0 ) ) {
            this.broadcastDataToOnlineMembers( SystemMessageBuilder.fromMessageId( SystemMessageIds.REPUTATION_POINTS_0_OR_LOWER_CLAN_SKILLS_DEACTIVATED ) )

            _.each( this.members, applySkillStatus )
        } else if ( ( this.reputationScore < 0 ) && ( value >= 0 ) ) {
            this.broadcastDataToOnlineMembers( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_SKILLS_WILL_BE_ACTIVATED_SINCE_REPUTATION_IS_0_OR_HIGHER ) )

            _.each( this.members, applySkillStatus )
        }

        this.setReputationScoreDirect( value )
        this.broadcastDataToOnlineMembers( PledgeShowInfoUpdate( this ) )

        if ( shouldSave ) {
            await this.saveReputationScore()
        }
    }

    setReputationScoreDirect( value: number ) : void {
        this.reputationScore = _.clamp( value, -100000000, 100000000 )
    }

    saveReputationScore(): Promise<void> {
        // TODO : add debounced way of saving various clan data pieces
        return DatabaseManager.getClanDataTable().setReputationScore( this.getId(), this.getReputationScore() )
    }

    skillsStatus( player: L2PcInstance, disable: boolean ) {
        if ( !player ) {
            return
        }

        let decideOutcome = ( skill: Skill ) => {
            if ( disable ) {
                player.disableSkill( skill, -1 )
            } else {
                player.enableSkill( skill )
            }
        }

        _.each( this.skills, decideOutcome )

        if ( player.getPledgeType() === 0 ) {
            _.each( this.subPledgeSkills, decideOutcome )
        } else {
            let pledge: ClanPledge = this.getPledge( player.getPledgeType() )
            if ( pledge ) {
                pledge.skills.forEach( decideOutcome )
            }
        }
    }

    takeReputationScore( value: number, shouldSave: boolean ): Promise<void> {
        return this.setReputationScore( this.getReputationScore() - value, shouldSave )
    }

    updateBloodAllianceCountInDatabase(): Promise<void> {
        return DatabaseManager.getClanDataTable().updateBloodAllianceCount( this.getId(), this.getBloodAllianceCount() )
    }

    scheduleUpdate() : void {
        return ClanCache.scheduleUpdate( this.getId() )
    }

    updateClanMember( player: L2PcInstance ): void {
        let member: L2ClanMember = L2ClanMember.fromPlayer( player.getClanId(), player )
        if ( player.isClanLeader() ) {
            return this.setLeader( member )
        }

        this.addClanMemberInstance( member )
    }

    async resetBloodOathCount(): Promise<void> {
        this.bloodOathCount = 0
        await this.updateBloodOathCountInDatabase()
    }

    async updateBloodOathCountInDatabase() {
        return DatabaseManager.getClanDataTable().updateBloodOathCount( this.getId(), this.getBloodOathCount() )
    }

    async createAlly( player: L2PcInstance, allyName: string ): Promise<void> {

        if ( !L2Clan.canPlayerCreateAlliance( player ) ) {
            return
        }

        if ( !GeneralHelper.isAlphanumericString( allyName ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_ALLIANCE_NAME ) )
            return
        }
        if ( allyName.length > 16 || allyName.length < 2 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_ALLIANCE_NAME_LENGTH ) )
            return
        }

        let normalizedName = allyName.trim()
        if ( ClanCache.isAllyExists( normalizedName ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALLIANCE_ALREADY_EXISTS ) )
            return
        }

        this.setAllyId( this.getId() )
        this.setAllyName( normalizedName )
        this.setAllyPenaltyExpiryTime( 0, 0 )
        this.scheduleUpdate()

        player.sendDebouncedPacket( UserInfo )
        player.sendDebouncedPacket( ExBrExtraUserInfo )
        player.sendMessage( `Alliance ${ normalizedName } has been created.` )

        this.updateRelationStatusForMembers()
    }

    static canPlayerCreateAlliance( player: L2PcInstance ): boolean {
        if ( !player ) {
            return false
        }

        if ( !player.isClanLeader() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_CLAN_LEADER_CREATE_ALLIANCE ) )
            return false
        }

        let clan: L2Clan = player.getClan()

        if ( clan.getAllyId() !== 0 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALREADY_JOINED_ALLIANCE ) )
            return false
        }

        if ( clan.getLevel() < 5 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TO_CREATE_AN_ALLY_YOU_CLAN_MUST_BE_LEVEL_5_OR_HIGHER ) )
            return false
        }

        let currentTime = Date.now()
        if ( clan.getAllyPenaltyExpiryTime() > currentTime && clan.getAllyPenaltyType() === L2ClanValues.PENALTY_TYPE_DISSOLVE_ALLY ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_CREATE_ALLIANCE_10_DAYS_DISOLUTION ) )
            return false
        }

        if ( clan.getDissolvingExpiryTime() > currentTime ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_MAY_NOT_CREATE_ALLY_WHILE_DISSOLVING ) )
            return false
        }

        return true
    }

    // TODO : convert to use debounced way of saving data
    updateSubPledgeInDatabase( pledgeType: number ): Promise<void> {
        let subPledge = this.getPledge( pledgeType )
        return DatabaseManager.getClanSubpledges().updatePledge( subPledge.leaderId, subPledge.name, this.getId(), pledgeType )
    }

    getSubPledgeByName( name: string ): ClanPledge {
        let normalizedName = name.toLowerCase()

        return _.find( this.subPledges, ( pledge: ClanPledge ): boolean => {
            return pledge.name.toLowerCase() === normalizedName
        } )
    }

    async createSubPledge( player: L2PcInstance, startPledgeType: number, leaderId: number, pledgeName: string ): Promise<ClanPledge> {
        let pledgeType = this.getAvailablePledgeTypes( startPledgeType )
        if ( pledgeType === L2ClanValues.SUBUNIT_NONE ) {
            if ( startPledgeType === L2ClanValues.SUBUNIT_ACADEMY ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_HAS_ALREADY_ESTABLISHED_A_CLAN_ACADEMY ) )
                return
            }

            player.sendMessage( 'You can\'t create any more sub-units of this type' )
            return null
        }

        if ( this.leader.getObjectId() === leaderId ) {
            player.sendMessage( 'Leader is not correct' )
            return null
        }

        if ( pledgeType !== L2ClanValues.SUBUNIT_ACADEMY
                && ( ( ( this.getReputationScore() < ConfigManager.clan.getCreateRoyalGuardCost() ) && ( pledgeType < L2ClanValues.SUBUNIT_KNIGHT1 ) )
                        || ( ( this.getReputationScore() < ConfigManager.clan.getCreateKnightUnitCost() ) && ( pledgeType > L2ClanValues.SUBUNIT_ROYAL2 ) ) ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_CLAN_REPUTATION_SCORE_IS_TOO_LOW ) )
            return null
        }

        await DatabaseManager.getClanSubpledges().addPledge( this.getId(), pledgeType, pledgeName, pledgeType !== -1 ? leaderId : 0 )

        let subPledge : ClanPledge = {
            type: pledgeType,
            leaderId,
            name: pledgeName,
            skills: []
        }
        this.subPledges[ pledgeType ] = subPledge

        if ( pledgeType !== L2ClanValues.SUBUNIT_ACADEMY ) {
            let cost = pledgeType < L2ClanValues.SUBUNIT_KNIGHT1 ? ConfigManager.clan.getCreateRoyalGuardCost() : ConfigManager.clan.getCreateKnightUnitCost()
            await this.setReputationScore( this.getReputationScore() - cost, true )
        }

        this.broadcastDataToOnlineMembers( PledgeShowInfoUpdate( this ) )
        this.broadcastDataToOnlineMembers( SubPledgeInfo( subPledge, this ) )

        return subPledge
    }

    getAvailablePledgeTypes( pledgeType: L2ClanValues ): L2ClanValues {
        if ( this.subPledges[ pledgeType ] ) {
            switch ( pledgeType ) {
                case L2ClanValues.SUBUNIT_ROYAL2:
                case L2ClanValues.SUBUNIT_ACADEMY:
                case L2ClanValues.SUBUNIT_KNIGHT4:
                    return L2ClanValues.SUBUNIT_NONE

                case L2ClanValues.SUBUNIT_ROYAL1:
                    return this.getAvailablePledgeTypes( L2ClanValues.SUBUNIT_ROYAL2 )

                case L2ClanValues.SUBUNIT_KNIGHT1:
                    return this.getAvailablePledgeTypes( L2ClanValues.SUBUNIT_KNIGHT2 )

                case L2ClanValues.SUBUNIT_KNIGHT2:
                    return this.getAvailablePledgeTypes( L2ClanValues.SUBUNIT_KNIGHT3 )

                case L2ClanValues.SUBUNIT_KNIGHT3:
                    return this.getAvailablePledgeTypes( L2ClanValues.SUBUNIT_KNIGHT4 )
            }
        }

        return pledgeType
    }

    getClanHall(): ClanHall {
        if ( this.hideoutId === 0 ) {
            return
        }

        return ClanHallManager.getClanHallById( this.hideoutId ) || ClanHallSiegeManager.getSiegableHall( this.hideoutId )
    }

    isLeader( objectId: number ) : boolean {
        return this.getLeaderId() === objectId
    }

    updateRelationStatusForMembers() : void {
        this.getMembers().forEach( ( member ) => {
            let otherPlayer = member.getPlayerInstance()
            if ( !otherPlayer ) {
                return
            }

            PlayerRelationStatus.computeRelationStatus( otherPlayer )
        } )
    }

    onPledgeShowMemberListAll( playerIds: Array<number> ) : void {
        let pledgePackets : Array<Buffer> = this.getAllSubPledges().map( ( pledge: ClanPledge ) : Buffer => {
            return SubPledgeInfo( pledge, this )
        } )

        this.getMembers().forEach( ( member: L2ClanMember ) => {
            if ( member.getPledgeType() === 0 ) {
                return
            }

            pledgePackets.push( PledgeShowMemberListAdd.fromClanMember( member ) )
        } )

        for ( const playerId of playerIds ) {
            for ( const currentPacket of pledgePackets ) {
                PacketDispatcher.sendCopyData( playerId, currentPacket )
            }

            PacketDispatcher.sendDebouncedPacket( playerId, UserInfo )
            PacketDispatcher.sendDebouncedPacket( playerId, ExBrExtraUserInfo )
        }
    }
}