import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { ElementalType } from '../../enums/ElementalType'

export class ItemInfo {
    enchantLevel: number
    augmentId: number
    elementId: number
    elementPower: number
    defenceAttributes: ReadonlyArray<number> = []

    static fromItemInstance( item: L2ItemInstance ) : ItemInfo {
        let itemInfo = new ItemInfo()

        itemInfo.enchantLevel = item.getEnchantLevel()
        itemInfo.augmentId = item.getAugmentation() ? item.getAugmentation().getAugmentationId() : 0
        itemInfo.elementId = item.getAttackElementType()
        itemInfo.elementPower = item.getAttackElementPower()

        itemInfo.defenceAttributes = structuredClone( item.elementalDefenceAttributes )

        return itemInfo
    }

    static fromEnchantmentLevel( level: number ) : ItemInfo {
        let itemInfo = new ItemInfo()

        itemInfo.enchantLevel = level
        itemInfo.augmentId = 0
        itemInfo.elementId = ElementalType.NONE
        itemInfo.elementPower = 0

        itemInfo.defenceAttributes = [ 0, 0, 0, 0, 0, 0 ]

        return itemInfo
    }

    getEnchantLevel() : number {
        return this.enchantLevel
    }

    getAugmentId() : number {
        return this.augmentId
    }

    getElementId() : number {
        return this.elementId
    }

    getElementPower() : number {
        return this.elementPower
    }

    getDefenseAttributes() : ReadonlyArray<number> {
        return this.defenceAttributes
    }
}