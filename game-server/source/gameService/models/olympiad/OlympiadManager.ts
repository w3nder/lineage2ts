import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { OlympiadGameManager } from './OlympiadGameManager'
import { OlympiadGameTask } from './OlympiadGameTask'
import { AbstractOlympiadGame } from './AbstractOlympiadGame'
import { CompetitionType } from './CompetitionType'
import _ from 'lodash'

class Manager {
    nonClassBasedRegisters: Array<number> = []
    classBasedRegisters: { [key: number]: Array<number> } = {}
    teamsBasedRegisters: Array<Array<number>> = []
    isCompetitionPeriodValue: boolean = false

    isCompetitionPeriod(): boolean {
        return this.isCompetitionPeriodValue
    }

    isInCompetition( player: L2PcInstance, observer: L2PcInstance, showMessage: boolean ) : boolean {
        if ( !this.isCompetitionPeriod() ) {
            return false
        }

        let outcome = false
        _.each( _.range( OlympiadGameManager.getNumberOfStadiums() ), ( index: number ) => {
            let game: AbstractOlympiadGame = OlympiadGameManager.getOlympiadTask( index ).getGame()
            if ( !game ) {
                return
            }

            if ( game.containsParticipant( player.getObjectId() ) ) {
                if ( !showMessage ) {
                    outcome = true
                    return false
                }

                switch ( game.getType() ) {
                    case CompetitionType.CLASSED:
                        let classedPacket: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_IS_ALREADY_REGISTERED_ON_THE_CLASS_MATCH_WAITING_LIST )
                                .addPlayerCharacterName( player )
                                .getBuffer()
                        observer.sendOwnedData( classedPacket )
                        break
                    case CompetitionType.NON_CLASSED:
                        let nonClassedPacket: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_IS_ALREADY_REGISTERED_ON_THE_NON_CLASS_LIMITED_MATCH_WAITING_LIST )
                                .addPlayerCharacterName( player )
                                .getBuffer()
                        observer.sendOwnedData( nonClassedPacket )
                        break
                    case CompetitionType.TEAMS:
                        let teamsPacket: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_IS_ALREADY_REGISTERED_NON_CLASS_LIMITED_EVENT_TEAMS )
                                .addPlayerCharacterName( player )
                                .getBuffer()
                        observer.sendOwnedData( teamsPacket )
                        break
                }

                outcome = true
                return false
            }
        } )

        return outcome
    }

    isRegistered( player: L2PcInstance, observer: L2PcInstance, showMessage: boolean ): boolean {
        let objectId = player.getObjectId()
        let outcome = false

        _.each( this.teamsBasedRegisters, ( team: Array<number> ) => {
            if ( _.includes( team, objectId ) ) {
                if ( showMessage ) {
                    let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_ALREADY_REGISTERED_NON_CLASS_LIMITED_EVENT_TEAMS )
                            .addPlayerCharacterName( player )
                            .getBuffer()

                    observer.sendOwnedData( packet )
                }
                outcome = true
                return
            }
        } )

        if ( outcome ) {
            return outcome
        }

        if ( this.nonClassBasedRegisters.includes( objectId ) ) {
            if ( showMessage ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_ALREADY_REGISTERED_ON_THE_NON_CLASS_LIMITED_MATCH_WAITING_LIST )
                        .addPlayerCharacterName( player )
                        .getBuffer()
                observer.sendOwnedData( packet )
            }
            return true
        }

        let classes: Array<number> = this.classBasedRegisters[ player.getBaseClass() ]
        if ( _.includes( classes, objectId ) ) {
            if ( showMessage ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_ALREADY_REGISTERED_ON_THE_CLASS_MATCH_WAITING_LIST )
                        .addPlayerCharacterName( player )
                        .getBuffer()
                observer.sendOwnedData( packet )
            }
            return true
        }

        return false
    }

    isRegisteredInCompetition( player: L2PcInstance ): boolean {
        return this.isRegistered( player, player, false ) || this.isInCompetition( player, player, false )
    }

    isRegisteredNoble( player: L2PcInstance ): boolean {
        return this.isRegistered( player, player, false )
    }

    removeDisconnectedCompetitor( player: L2PcInstance ): void {
        let task: OlympiadGameTask = OlympiadGameManager.getOlympiadTask( player.getOlympiadGameId() )
        if ( task && task.isGameStarted() ) {
            task.getGame().handleDisconnect( player )
        }

        let objectId = player.getObjectId()
        if ( this.nonClassBasedRegisters.includes( objectId ) ) {
            _.pull( this.nonClassBasedRegisters, objectId )
            return
        }

        let classedRegister = this.classBasedRegisters[ player.getBaseClass() ]
        if ( classedRegister && classedRegister.includes( objectId ) ) {
            _.pull( this.classBasedRegisters[ player.getBaseClass() ], objectId )
            return
        }

        _.each( this.teamsBasedRegisters, ( teamRegister: Array<number> ) => {
            if ( teamRegister && teamRegister.includes( objectId ) ) {
                _.pull( teamRegister, objectId )
                // TODO : ThreadPoolManager.getInstance().executeGeneral(new AnnounceUnregToTeam(team));
            }
        } )
    }

    unRegisterNoble( player: L2PcInstance ) : boolean {
        // TODO : implement me
        return false
    }

    registerNoble( player: L2PcInstance, type: CompetitionType ) : boolean {
        // TODO : implement me
        return false
    }

    getCountOpponents() : number {
        return this.nonClassBasedRegisters.length + _.size( this.classBasedRegisters ) + this.teamsBasedRegisters.length
    }
}

export const OlympiadManager = new Manager()