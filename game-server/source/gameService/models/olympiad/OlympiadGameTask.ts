import { ConfigManager } from '../../../config/ConfigManager'
import { AbstractOlympiadGame } from './AbstractOlympiadGame'
import { OlympiadStadiumArea } from '../areas/type/OlympiadStadium'
import { ILocational } from '../Location'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { ExOlympiadUserInfo } from '../../packets/send/ExOlympiadUserInfo'
import { PacketDispatcher } from '../../PacketDispatcher'
import { L2World } from '../../L2World'
import Buffer from 'buffer'

enum GameState {
    BEGIN,
    TELEPORT_TO_ARENA,
    GAME_STARTED,
    BATTLE_COUNTDOWN_FIRST,
    BATTLE_COUNTDOWN_SECOND,
    BATTLE_STARTED,
    BATTLE_IN_PROGRESS,
    GAME_CANCELLED,
    GAME_STOPPED,
    TELEPORT_TO_TOWN,
    CLEANUP,
    IDLE
}

// TODO : implement more of olympiad game logic
export const OlympiadGameTaskValues = {
    BATTLE_PERIOD: ConfigManager.olympiad.getBattlePeriod(),
    TELEPORT_TO_ARENA_TIMES: [
        120,
        60,
        30,
        15,
        10,
        5,
        4,
        3,
        2,
        1,
        0
    ],

    BATTLE_START_TIME_FIRST: [
        60,
        50,
        40,
        30,
        20,
        10,
        0
    ],

    BATTLE_START_TIME_SECOND: [
        10,
        5,
        4,
        3,
        2,
        1,
        0
    ],

    TELEPORT_TO_TOWN_TIMES: [
        40,
        30,
        20,
        10,
        5,
        4,
        3,
        2,
        1,
        0
    ]
}

export class OlympiadGameTask {
    area: OlympiadStadiumArea
    game: AbstractOlympiadGame
    state: GameState = GameState.IDLE
    needAnnouncement: boolean = false
    countDown: number = 0
    instanceId: number = 0

    isBattleStarted() : boolean {
        return this.state === GameState.BATTLE_IN_PROGRESS
    }

    isGameStarted() {
        return ( this.state >= GameState.GAME_STARTED ) && ( this.state <= GameState.CLEANUP )
    }

    getGame() {
        return this.game
    }

    isBattleFinished() {
        return this.state === GameState.TELEPORT_TO_TOWN
    }

    isRunning() {
        return this.state !== GameState.IDLE
    }

    getSpectatorLocation() : ILocational {
        return this.area.properties.spectator
    }

    broadcastStatusUpdate( player: L2PcInstance ) {
        let packet = ExOlympiadUserInfo( player )
        L2World.forPlayersByBox( this.area.getSpatialIndex(), ( target: L2PcInstance ) => {
            if ( target && ( target.inObserverMode() || ( target.getOlympiadSide() !== player.getOlympiadSide() ) ) ) {
                PacketDispatcher.sendCopyData( target.getObjectId(), packet )
            }
        } )
    }

    broadcastPacketToObservers( packet: Buffer ): void {
        L2World.forPlayersByBox( this.area.getSpatialIndex(), ( target: L2PcInstance ) => {
            if ( target && target.inObserverMode() ) {
                PacketDispatcher.sendCopyData( target.getObjectId(), packet )
            }
        } )
    }

    getInstanceId() : number {
        return this.instanceId
    }
}