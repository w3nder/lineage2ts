export enum CompetitionType {
    CLASSED,
    NON_CLASSED,
    TEAMS,
    OTHER
}

export const CompetitionTypeMap = {
    [ CompetitionType.CLASSED ]: 'classed',
    [ CompetitionType.NON_CLASSED ]: 'non-classed',
    [ CompetitionType.TEAMS ]: 'teams',
    [ CompetitionType.OTHER ]: 'other'
}