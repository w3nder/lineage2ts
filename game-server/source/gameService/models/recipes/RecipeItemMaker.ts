import { L2RecipeDefinition } from '../l2RecipeDefinition'
import { Skill } from '../Skill'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { L2ManufactureItem } from '../L2ManufactureItem'
import { SystemMessageBuilder, SystemMessageHelper } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { PacketDispatcher } from '../../PacketDispatcher'
import { RecipeController } from '../../taskmanager/RecipeController'
import { RecipeItemMakeInfo } from '../../packets/send/RecipeItemMakeInfo'
import { RecipeShopItemInfo } from '../../packets/send/RecipeShopItemInfo'
import { Inventory } from '../itemcontainer/Inventory'
import { L2RecipeItem } from '../l2RecipeItem'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { StatType } from '../../enums/StatType'
import aigle from 'aigle'
import _ from 'lodash'
import { ObjectPool } from '../../helpers/ObjectPoolHelper'
import { ClanHallFunctionType } from '../../enums/clanhall/ClanHallFunctionType'
import { CommonSkillIds } from '../../enums/skills/CommonSkillIds'

interface ConsumedItem {
    itemId: number
    quantity: number
    name: string
    price: number
}

const ConsumedItemCache = new ObjectPool( 'RecipeItemMaker', (): ConsumedItem => {
    return {
        itemId: 0,
        name: '',
        price: 0,
        quantity: 0,
    }
} )

export class RecipeItemMaker {
    items: Array<ConsumedItem> = []
    recipe: L2RecipeDefinition
    makerId: number
    customerId: number
    skill: Skill
    skillId: number
    skillLevel: number
    creationPasses: number = 1
    exp: number = -1
    sp: number = -1
    price: number
    totalItems: number
    delay: number

    constructor( player: L2PcInstance, recipe: L2RecipeDefinition, target: L2PcInstance ) {
        this.makerId = player.getObjectId()
        this.recipe = recipe
        this.customerId = target.getObjectId()

        this.skillId = recipe.isDwarvenRecipe ? CommonSkillIds.CreateDwarvenItems : CommonSkillIds.CreateCommonItems
        this.skillLevel = player.getSkillLevel( this.skillId )
        this.skill = player.getKnownSkill( this.skillId )

        player.setIsInCraftMode( true )
    }

    async onStart() {
        if ( !this.isValidTransaction() ) {
            return this.onStop()
        }

        if ( !await this.calculatePrerequisites() ) {
            return this.onStop()
        }

        return this.processNormalCrafting()
    }

    isValidTransaction(): boolean {
        if ( !ConfigManager.character.crafting() ) {
            PacketDispatcher.sendOwnedData( this.customerId, SystemMessageHelper.sendString( 'Item creation is currently disabled.' ) )
            return false
        }

        let player = L2World.getPlayer( this.makerId )
        let target = L2World.getPlayer( this.customerId )

        if ( !player || !player.isOnline() || !target || !target.isOnline() ) {
            return false
        }

        if ( player.isAlikeDead() || player.isProcessingTransaction() ) {
            player.sendOwnedData( ActionFailed() )
            return false
        }

        if ( target.isAlikeDead() || target.isProcessingTransaction() ) {
            player.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.recipe.items.length === 0 ) {
            player.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.recipe.level > this.skillLevel ) {
            player.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.makerId !== this.customerId ) {
            let recipeId = this.recipe.id
            let item: L2ManufactureItem = player.getManufactureItems().find( ( item: L2ManufactureItem ) => item.recipeId === recipeId )
            if ( item ) {
                this.price = item.adenaCost
                if ( target.getAdena() < this.price ) {
                    target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                    return false
                }
            }
        }

        if ( this.customerId !== this.makerId && this.price > 0 && target.getInventory().getAdenaAmount() < this.price ) {
            target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
            return false
        }

        return true
    }

    onStop() {
        this.updateMakeInfo( false )

        let player = L2World.getPlayer( this.makerId )
        if ( player ) {
            player.setIsInCraftMode( false )
        }

        RecipeController.removeMaker( this.makerId )
    }

    updateMakeInfo( isSuccess: boolean ) {
        if ( this.customerId === this.makerId ) {
            PacketDispatcher.sendOwnedData( this.customerId, RecipeItemMakeInfo( this.recipe.id, this.customerId, isSuccess ) )
            return
        }

        let makerPlayer = L2World.getPlayer( this.makerId )
        if ( !makerPlayer ) {
            return
        }

        PacketDispatcher.sendOwnedData( this.customerId, RecipeShopItemInfo( makerPlayer, this.recipe.id ) )
    }

    async calculatePrerequisites(): Promise<boolean> {
        this.items = await this.getConsumedItems()
        if ( this.items.length === 0 ) {
            return false
        }

        this.totalItems = this.items.reduce( ( totalAmount: number, item: ConsumedItem ) => {
            return totalAmount + item.quantity
        }, 0 )

        if ( this.totalItems === 0 ) {
            return false
        }

        if ( await this.processStatUsage( false ) ) {
            return false
        }

        this.updateMakeInfo( true )

        let player = L2World.getPlayer( this.makerId )
        player.setIsInCraftMode( false )

        return true
    }

    async getConsumedItems(): Promise<Array<ConsumedItem>> {
        let target = L2World.getPlayer( this.customerId )
        let customerInventory: Inventory = target.getInventory()
        let materials: Array<ConsumedItem> = []

        let shouldExit = this.recipe.items.some( ( requiredItem: L2RecipeItem ) => {
            if ( requiredItem.quantity > 0 ) {
                let item: L2ItemInstance = customerInventory.getItemByItemId( requiredItem.itemId )
                let itemQuantityAmount = !item ? 0 : item.getCount()

                if ( itemQuantityAmount < requiredItem.quantity ) {
                    let packet = new SystemMessageBuilder( SystemMessageIds.MISSING_S2_S1_TO_CREATE )
                            .addItemNameWithId( requiredItem.itemId )
                            .addNumber( requiredItem.quantity - itemQuantityAmount )
                            .getBuffer()
                    target.sendOwnedData( packet )
                    return true
                }

                let data = ConsumedItemCache.getValue()

                data.itemId = item.getId()
                data.quantity = requiredItem.quantity
                data.name = item.getItem().getName()
                data.price = item.getReferencePrice()

                materials.push( data )
            }

            return false
        } )

        if ( shouldExit ) {
            ConsumedItemCache.recycleValues( materials )
            return []
        }

        return materials
    }

    async processStatUsage( shouldApply: boolean ): Promise<boolean> {
        let player = L2World.getPlayer( this.makerId )
        let target = L2World.getPlayer( this.customerId )

        let mpMultilplier = getPlayerMpMultiplier( player )
        let stat = this.recipe.statConsume
        let modifiedValue = ( stat.value / this.creationPasses ) * mpMultilplier

        if ( stat.type === StatType.HP ) {
            if ( player.getCurrentHp() <= modifiedValue ) {
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_HP ) )
                return false
            }

            if ( shouldApply ) {
                await player.reduceCurrentHp( modifiedValue, player, this.skill )
            }

            return true
        }

        if ( stat.type === StatType.MP ) {
            if ( player.getCurrentMp() < modifiedValue ) {
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_MP ) )
                return false
            }

            if ( shouldApply ) {
                player.reduceCurrentMp( modifiedValue )
            }

            return true
        }

        target.sendMessage( 'Recipe error!!!, please tell this to your GM.' )
        return false
    }

    async processNormalCrafting(): Promise<void> {
        await this.processStatUsage( true )

        let player = L2World.getPlayer( this.makerId )
        let target = L2World.getPlayer( this.customerId )

        if ( this.customerId !== this.makerId && this.price > 0 ) {
            let customerAdena = target.getInventory().getAdenaItem()
            if ( !customerAdena ) {
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                return
            }

            let adenaItem: L2ItemInstance = await target.transferItem( customerAdena.getObjectId(), this.price, player.getInventory() )
            if ( !adenaItem ) {
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                return
            }
        }

        await aigle.resolve( this.items ).eachSeries( async ( item: ConsumedItem ) => {
            await target.getInventory().destroyItemByItemId( item.itemId, item.quantity, 0, 'RecipeItemMaker.getConsumedItems' )

            if ( item.quantity > 1 ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                        .addItemNameWithId( item.itemId )
                        .addNumber( item.quantity )
                        .getBuffer()
                target.sendOwnedData( packet )
            } else {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                        .addItemNameWithId( item.itemId )
                        .getBuffer()
                target.sendOwnedData( packet )
            }
        } )

        if ( _.random( 100 ) < this.recipe.successRate ) {
            await this.rewardPlayer()
            this.updateMakeInfo( true )
        } else {
            if ( this.customerId !== this.makerId ) {
                let playerStatus = new SystemMessageBuilder( SystemMessageIds.CREATION_OF_S2_FOR_C1_AT_S3_ADENA_FAILED )
                        .addString( target.getName() )
                        .addItemNameWithId( this.recipe.productionItemId )
                        .getBuffer()
                player.sendOwnedData( playerStatus )

                let targetStatus = new SystemMessageBuilder( SystemMessageIds.C1_FAILED_TO_CREATE_S2_FOR_S3_ADENA )
                        .addString( player.getName() )
                        .addItemNameWithId( this.recipe.productionItemId )
                        .addNumber( this.price )
                        .getBuffer()
                target.sendOwnedData( targetStatus )
            } else {
                target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_MIXING_FAILED ) )
            }

            this.updateMakeInfo( false )
        }

        player.setIsInCraftMode( false )

        return this.unRegister()
    }

    async rewardPlayer(): Promise<void> {
        let rareProductionId = this.recipe.rareItemId
        let itemId = this.recipe.productionItemId
        let itemCount = this.recipe.productionAmount
        let player = L2World.getPlayer( this.makerId )
        let target = L2World.getPlayer( this.customerId )

        if ( ( rareProductionId !== -1 )
                && ( ( rareProductionId === itemId ) || ConfigManager.character.craftMasterwork() )
                && _.random( 100 ) < this.recipe.rarity ) {
            itemId = rareProductionId
            itemCount = this.recipe.rareCount
        }

        await target.getInventory().addItem( itemId, itemCount, player.getObjectId(), 'RecipeItemMaker.rewardPlayer' )

        if ( this.customerId !== this.makerId ) {
            if ( itemCount === 1 ) {
                let playerMessage = new SystemMessageBuilder( SystemMessageIds.S2_CREATED_FOR_C1_FOR_S3_ADENA )
                        .addString( target.getName() )
                        .addItemNameWithId( itemId )
                        .addNumber( this.price )
                        .getBuffer()
                player.sendOwnedData( playerMessage )

                let targetMessage = new SystemMessageBuilder( SystemMessageIds.C1_CREATED_S2_FOR_S3_ADENA )
                        .addString( player.getName() )
                        .addItemNameWithId( itemId )
                        .addNumber( this.price )
                        .getBuffer()
                target.sendOwnedData( targetMessage )
            } else {
                let playerMessage = new SystemMessageBuilder( SystemMessageIds.S2_S3_S_CREATED_FOR_C1_FOR_S4_ADENA )
                        .addString( target.getName() )
                        .addNumber( itemCount )
                        .addItemNameWithId( itemId )
                        .addNumber( this.price )
                        .getBuffer()
                player.sendOwnedData( playerMessage )

                let targetMessage = new SystemMessageBuilder( SystemMessageIds.C1_CREATED_S2_S3_S_FOR_S4_ADENA )
                        .addString( player.getName() )
                        .addNumber( itemCount )
                        .addItemNameWithId( itemId )
                        .addNumber( this.price )
                        .getBuffer()
                target.sendOwnedData( targetMessage )
            }
        }

        if ( itemCount > 1 ) {
            let message = new SystemMessageBuilder( SystemMessageIds.EARNED_S2_S1_S )
                    .addItemNameWithId( itemId )
                    .addNumber( itemCount )
                    .getBuffer()
            target.sendOwnedData( message )
        } else {
            let message = new SystemMessageBuilder( SystemMessageIds.EARNED_ITEM_S1 )
                    .addItemNameWithId( itemId )
                    .getBuffer()
            target.sendOwnedData( message )
        }
    }

    unRegister(): void {
        RecipeController.removeMaker( this.makerId )
        ConsumedItemCache.recycleValues( this.items )

        this.items.length = 0
    }
}

export function getPlayerMpMultiplier( player: L2PcInstance ): number {
    let mpMultilplier = player.getClanHallFunctionLevel( ClanHallFunctionType.CraftingMpDecrease )
    if ( mpMultilplier === 0 ) {
        return 1
    }

    return 1 + ( Math.min( 99, mpMultilplier ) / 100 )
}