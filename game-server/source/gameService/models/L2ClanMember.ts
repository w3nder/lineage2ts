import { L2PcInstance } from './actor/instance/L2PcInstance'
import { L2Clan } from './L2Clan'
import { L2World } from '../L2World'
import { SiegeManager } from '../instancemanager/SiegeManager'
import { DatabaseManager } from '../../database/manager'
import { ClanCache } from '../cache/ClanCache'

export class L2ClanMember {
    clanId: number
    objectId: number = 0
    name: string
    title: string
    powerGrade: number = 0
    level: number = 0
    classId: number = 0
    sex: boolean
    race: number = 0
    playerId: number = 0// object id of L2Object
    pledgeType: number = 0
    apprentice: number = 0
    sponsor: number = 0

    constructor( clanId: number ) {
        this.clanId = clanId
    }

    static fromPlayer( clanId: number, player: L2PcInstance ) : L2ClanMember {
        let member = new L2ClanMember( clanId )

        member.playerId = player.getObjectId()
        member.objectId = player.getObjectId()
        member.name = player.getName()
        member.level = player.getLevel()
        member.classId = player.getClassId()
        member.pledgeType = player.getPledgeType()
        member.powerGrade = player.getPowerGrade()
        member.title = player.getTitle()
        member.sponsor = 0
        member.apprentice = 0
        member.sex = player.getAppearance().getSex()
        member.race = player.getRace()

        return member
    }

    getClan() : L2Clan {
        return ClanCache.getClan( this.clanId )
    }

    setPlayerInstance( player: L2PcInstance ) {
        let previousPlayer = L2World.getPlayer( this.playerId )
        if ( !player && previousPlayer ) {
            this.name = previousPlayer.getName()
            this.level = previousPlayer.getLevel()
            this.classId = previousPlayer.getClassId()
            this.objectId = previousPlayer.getObjectId()
            this.pledgeType = previousPlayer.getPledgeType()
            this.powerGrade = previousPlayer.getPowerGrade()
            this.title = previousPlayer.getTitle()
            this.sponsor = previousPlayer.getSponsor()
            this.apprentice = previousPlayer.getApprentice()
            this.sex = previousPlayer.getAppearance().getSex()
            this.race = previousPlayer.getRace()
        }

        if ( player ) {
            let currentClan = this.getClan()

            currentClan.addSkillEffects( player )
            if ( ( currentClan.getLevel() > 3 ) && player.isClanLeader() ) {
                SiegeManager.addSiegeSkills( player )
            }
            if ( player.isClanLeader() ) {
                currentClan.setLeader( this )
            }
        }

        this.playerId = player ? player.getObjectId() : 0
    }

    static calculatePledgeClass( player: L2PcInstance ) : number {
        let pledgeClass : number = 0
        if ( !player ) {
            return pledgeClass
        }

        let clan : L2Clan = player.getClan()
        if ( clan ) {
            switch ( clan.getLevel() ) {
                case 4:
                    if ( player.isClanLeader() ) {
                        pledgeClass = 3
                    }
                    break
                case 5:
                    if ( player.isClanLeader() ) {
                        pledgeClass = 4
                    } else {
                        pledgeClass = 2
                    }
                    break
                case 6:
                    switch ( player.getPledgeType() ) {
                        case -1:
                            pledgeClass = 1
                            break
                        case 100:
                        case 200:
                            pledgeClass = 2
                            break
                        case 0:
                            if ( player.isClanLeader() ) {
                                pledgeClass = 5
                            } else {
                                pledgeClass = function () : number {
                                    switch ( clan.getLeaderSubPledge( player.getObjectId() ) ) {
                                        case 100:
                                        case 200:
                                            return 4
                                        default:
                                            return 3
                                    }
                                }()
                            }
                            break
                    }
                    break
                case 7:
                    switch ( player.getPledgeType() ) {
                        case -1:
                            pledgeClass = 1
                            break
                        case 100:
                        case 200:
                            pledgeClass = 3
                            break
                        case 1001:
                        case 1002:
                        case 2001:
                        case 2002:
                            pledgeClass = 2
                            break
                        case 0:
                            if ( player.isClanLeader() ) {
                                pledgeClass = 7
                            } else {
                                switch ( clan.getLeaderSubPledge( player.getObjectId() ) ) {
                                    case 100:
                                    case 200:
                                        pledgeClass = 6
                                        break

                                    case 1001:
                                    case 1002:
                                    case 2001:
                                    case 2002:
                                        pledgeClass = 5
                                        break

                                    default:
                                        pledgeClass = 4
                                        break
                                }
                            }
                            break
                    }
                    break
                case 8:
                    switch ( player.getPledgeType() ) {
                        case -1:
                            pledgeClass = 1
                            break
                        case 100:
                        case 200:
                            pledgeClass = 4
                            break
                        case 1001:
                        case 1002:
                        case 2001:
                        case 2002:
                            pledgeClass = 3
                            break
                        case 0:
                            if ( player.isClanLeader() ) {
                                pledgeClass = 8
                            } else {
                                pledgeClass = function () : number {
                                    switch ( clan.getLeaderSubPledge( player.getObjectId() ) ) {
                                        case 100:
                                        case 200:
                                            return 7
                                        case 1001:
                                        case 1002:
                                        case 2001:
                                        case 2002:
                                            return 6
                                        default:
                                            return 5
                                    }
                                }()
                            }
                            break
                    }
                    break
                case 9:
                    switch ( player.getPledgeType() ) {
                        case -1:
                            pledgeClass = 1
                            break
                        case 100:
                        case 200:
                            pledgeClass = 5
                            break
                        case 1001:
                        case 1002:
                        case 2001:
                        case 2002:
                            pledgeClass = 4
                            break
                        case 0:
                            if ( player.isClanLeader() ) {
                                pledgeClass = 9
                            } else {
                                pledgeClass = function () : number {
                                    switch ( clan.getLeaderSubPledge( player.getObjectId() ) ) {
                                        case 100:
                                        case 200:
                                            return 8
                                        case 1001:
                                        case 1002:
                                        case 2001:
                                        case 2002:
                                            return 7
                                        default:
                                            return 6
                                    }
                                }()
                            }
                            break
                    }
                    break
                case 10:
                    switch ( player.getPledgeType() ) {
                        case -1:
                            pledgeClass = 1
                            break
                        case 100:
                        case 200:
                            pledgeClass = 6
                            break
                        case 1001:
                        case 1002:
                        case 2001:
                        case 2002:
                            pledgeClass = 5
                            break
                        case 0:
                            if ( player.isClanLeader() ) {
                                pledgeClass = 10
                            } else {
                                pledgeClass = function () : number {
                                    switch ( clan.getLeaderSubPledge( player.getObjectId() ) ) {
                                        case 100:
                                        case 200:
                                            return 9
                                        case 1001:
                                        case 1002:
                                        case 2001:
                                        case 2002:
                                            return 8
                                        default:
                                            return 7
                                    }
                                }()
                            }
                            break
                    }
                    break
                case 11:
                    switch ( player.getPledgeType() ) {
                        case -1:
                            pledgeClass = 1
                            break
                        case 100:
                        case 200:
                            pledgeClass = 7
                            break
                        case 1001:
                        case 1002:
                        case 2001:
                        case 2002:
                            pledgeClass = 6
                            break
                        case 0:
                            if ( player.isClanLeader() ) {
                                pledgeClass = 11
                            } else {
                                pledgeClass = function () {
                                    switch ( clan.getLeaderSubPledge( player.getObjectId() ) ) {
                                        case 100:
                                        case 200:
                                            return 10
                                        case 1001:
                                        case 1002:
                                        case 2001:
                                        case 2002:
                                            return 9
                                        default:
                                            return 8
                                    }
                                }()
                            }
                            break
                    }
                    break
                default:
                    pledgeClass = 1
                    break
            }
        }

        if ( player.isNoble() && ( pledgeClass < 5 ) ) {
            pledgeClass = 5
        }

        if ( player.isHero() && ( pledgeClass < 8 ) ) {
            pledgeClass = 8
        }
        return pledgeClass
    }

    isOnline() {
        let player = L2World.getPlayer( this.playerId )
        if ( !player || !player.isOnline() ) {
            return false
        }

        return !player.isInOfflineMode()
    }

    getPlayerInstance() : L2PcInstance {
        return L2World.getPlayer( this.playerId )
    }

    getName() {
        let player = this.getPlayerInstance()
        if ( player ) {
            return player.getName()
        }

        return this.name
    }

    getLevel() {
        let player = this.getPlayerInstance()
        if ( player ) {
            return player.getLevel()
        }

        return this.level
    }

    getClassId() {
        let player = this.getPlayerInstance()
        if ( player ) {
            return player.getClassId()
        }

        return this.classId
    }

    getPledgeType() {
        let player = this.getPlayerInstance()
        if ( player ) {
            return player.getPledgeType()
        }

        return this.pledgeType
    }

    getObjectId() {
        let player = this.getPlayerInstance()
        if ( player ) {
            return player.getObjectId()
        }

        return this.objectId
    }

    getSponsor() {
        let player = this.getPlayerInstance()
        if ( player ) {
            return player.getSponsor()
        }

        return this.sponsor
    }

    getApprentice() {
        let player = this.getPlayerInstance()
        if ( player ) {
            return player.getApprentice()
        }

        return this.apprentice
    }

    setApprenticeAndSponsor( apprenticeId: number, sponsorId: number ) {
        this.apprentice = apprenticeId
        this.sponsor = sponsorId
    }

    async saveApprenticeAndSponsor( apprenticeId: number, sponsorId: number ) {
        let objectId = this.getObjectId()
        return DatabaseManager.getCharacterTable().setApprenticeAndSponsor( objectId, apprenticeId, sponsorId )
    }

    getSex() : boolean {
        let player = this.getPlayerInstance()
        if ( player ) {
            return player.getAppearance().getSex()
        }

        return this.sex
    }

    getRace() : number {
        let player = this.getPlayerInstance()
        if ( player ) {
            return player.getRace()
        }

        return this.race
    }

    getPowerGrade() {
        let player = this.getPlayerInstance()
        if ( player ) {
            return player.getPowerGrade()
        }

        return this.powerGrade
    }

    async setPowerGrade( grade: number ) {
        this.powerGrade = grade
        let player = this.getPlayerInstance()
        if ( player ) {
            player.setPowerGrade( grade )
            return
        }

        await DatabaseManager.getCharacterTable().updatePowerGrade( this.playerId, grade )
    }

    getApprenticeOrSponsorName() {
        let player = this.getPlayerInstance()
        if ( player ) {
            this.apprentice = player.getApprentice()
            this.sponsor = player.getSponsor()
        }

        if ( this.apprentice !== 0 ) {
            let apprentice : L2ClanMember = this.getClan().getClanMember( this.apprentice )
            if ( apprentice ) {
                return apprentice.getName()
            }

            return 'Error'
        }

        if ( this.sponsor !== 0 ) {
            let sponsor : L2ClanMember = this.getClan().getClanMember( this.sponsor )
            if ( sponsor ) {
                return sponsor.getName()
            }

            return 'Error'
        }

        return ''
    }

    getTitle() : string {
        let player = this.getPlayerInstance()
        if ( player ) {
            return player.getTitle()
        }

        return this.title
    }

    setPledgeType( pledgeType: number ) : Promise<void> {
        this.pledgeType = pledgeType

        let player = this.getPlayerInstance()
        if ( player ) {
            player.setPledgeType( pledgeType )
            return
        }

        return DatabaseManager.getCharacterTable().updatePledge( this.getObjectId(), pledgeType )
    }
}