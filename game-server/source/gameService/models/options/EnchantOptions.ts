import { DataManager } from '../../../data/manager'

type EnchantOptionValues = [ number, number, number ]
export type EnchantOptionsParameters = Readonly<EnchantOptionValues>
export const DefaultEnchantOptions : EnchantOptionsParameters = [ 0,0,0 ]
export interface EnchantOptions {
    level: number
    options: EnchantOptionsParameters
}

export function getEnchantOptions( itemId: number, enchantLevel: number ): EnchantOptionsParameters {
    let option: EnchantOptions = DataManager.getEnchantItemOptionsData().getOptions( itemId, enchantLevel )
    if ( option ) {
        return option.options
    }

    return DefaultEnchantOptions
}