import { OptionsSkillType } from '../../enums/OptionsSkillType'
import { SkillHolder } from '../holders/SkillHolder'

export class OptionsSkillHolder extends SkillHolder {
    type: OptionsSkillType
    chance: number

    constructor( id: number, level: number, chance: number, type: OptionsSkillType ) {
        super( id, level )

        this.chance = chance
        this.type = type
    }

    getChance() {
        return this.chance
    }

    getSkillType() {
        return this.type
    }
}