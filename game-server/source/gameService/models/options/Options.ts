import { FunctionTemplate } from '../stats/functions/FunctionTemplate'
import { OptionsSkillHolder } from './OptionsSkillHolder'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { Skill } from '../Skill'
import { AbstractFunction } from '../stats/functions/AbstractFunction'
import { SkillCoolTime } from '../../packets/send/SkillCoolTime'

export class Options {
    id: number
    functions: Array<FunctionTemplate> = []

    activeSkill: Skill
    passiveSkill: Skill

    activationSkill: OptionsSkillHolder

    constructor( id: number ) {
        this.id = id
    }

    async remove( player: L2PcInstance ) : Promise<void> {
        if ( this.functions && this.functions.length > 0 ) {
            player.removeStatsOwner( this )
        }

        if ( this.hasActiveSkill() ) {
            await player.removeSkill( this.getActiveSkill(), false, false )
        }

        if ( this.hasPassiveSkill() ) {
            await player.removeSkill( this.getPassiveSkill(), false, true )
        }

        if ( this.hasActivationSkill() ) {
            player.removeTriggerSkill( this.activationSkill )
        }

        player.sendSkillList()
    }

    async addSkill( player: L2PcInstance, skill: Skill ) : Promise<void> {
        await player.addSkill( skill )

        if ( skill.isActive() ) {
            let remainingTime = player.getSkillReuseRemainingMs( skill )
            if ( remainingTime > 0 ) {
                player.addSkillReuse( skill, remainingTime )
                player.disableSkill( skill, remainingTime )
            }

            player.sendDebouncedPacket( SkillCoolTime )
        }
    }

    async apply( player: L2PcInstance ) : Promise<void> {
        let playerFunctions = this.getStatFunctions( player )
        if ( playerFunctions.length > 0 ) {
            player.addStatFunctions( playerFunctions, true )
        }

        if ( this.hasActiveSkill() ) {
            await this.addSkill( player, this.getActiveSkill() )
        }

        if ( this.hasPassiveSkill() ) {
            await this.addSkill( player, this.getPassiveSkill() )
        }

        if ( this.hasActivationSkill() ) {
            player.addTriggerSkill( this.activationSkill )
        }

        player.sendSkillList()
    }

    hasActivationSkill() : boolean {
        return !!this.activationSkill
    }

    hasActiveSkill() : boolean {
        return !!this.activeSkill
    }

    hasPassiveSkill() {
        return !!this.passiveSkill
    }

    private getActiveSkill() : Skill {
        return this.activeSkill
    }

    private getPassiveSkill() : Skill {
        return this.passiveSkill
    }

    getStatFunctions( player: L2PcInstance ) : Array<AbstractFunction> {
        if ( this.functions.length === 0 ) {
            return []
        }

        let options = this
        return this.functions.reduce( ( allFunctions: Array<AbstractFunction>, template: FunctionTemplate ) => {
            let currentFunction = template.createFunction( player, player, null, options )
            if ( currentFunction ) {
                allFunctions.push( currentFunction )
            }

            return allFunctions
        }, [] )
    }
}