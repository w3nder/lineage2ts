import { Race } from '../../enums/Race'
import { ClassLevel } from './ClassLevel'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import _ from 'lodash'
import { PlayerClassDefinition, PlayerClassDefinitions } from './PlayerClassDefinitions'

export interface IPlayerClass extends PlayerClassDefinition{
    name: string
}

const PlayerClassIdValues : Record<number, IPlayerClass> = {}
const PlayerClassNameValues : Record<string, IPlayerClass> = {}

_.each( PlayerClassDefinitions, ( value: PlayerClassDefinition, name: string ) => {
    const playerClass : IPlayerClass = {
        name,
        ...value
    }

    PlayerClassIdValues[ playerClass.classId ] = playerClass
    PlayerClassNameValues[ playerClass.name ] = playerClass
} )

export function getPlayerClassById( classId: number ): IPlayerClass {
    return PlayerClassIdValues[ classId ]
}

function getSet( requiredRace: Race | null, requiredLevel: ClassLevel | null ): Array<IPlayerClass> {
    return _.reduce( PlayerClassIdValues, ( allItems : Array<IPlayerClass>, value : IPlayerClass ) => {
        if ( ( requiredRace === null || requiredRace === value.race ) && ( requiredLevel === null || requiredLevel === value.level ) ) {
            allItems.push( value )
        }

        return allItems
    }, [] )
}

const neverSubclassedValues : Array<IPlayerClass> = _.at( PlayerClassNameValues, [ 'Overlord', 'Warsmith' ] )
const mainSubclassValues : Array<IPlayerClass> = _.pullAll( getSet( null, ClassLevel.Third ), neverSubclassedValues )

const nonKamaelSubclassValues : Array<IPlayerClass> = _.pullAll( [ ...mainSubclassValues ], getSet( Race.KAMAEL, ClassLevel.Third ) )
const kamaelSubclassValues : Array<IPlayerClass> = getSet( Race.KAMAEL, ClassLevel.Third )

const classSetOne = _.at( PlayerClassNameValues, [ 'DarkAvenger', 'Paladin', 'TempleKnight', 'ShillienKnight' ] )
const classSetTwo = _.at( PlayerClassNameValues, [ 'TreasureHunter', 'AbyssWalker', 'Plainswalker' ] )
const classSetThree = _.at( PlayerClassNameValues, [ 'Hawkeye', 'SilverRanger', 'PhantomRanger' ] )
const classSetFour = _.at( PlayerClassNameValues, [ 'Warlock', 'ElementalSummoner', 'PhantomSummoner' ] )
const classSetFive = _.at( PlayerClassNameValues, [ 'Sorceror', 'Spellsinger', 'Spellhowler' ] )

const subclassSets : Record<string, Array<IPlayerClass>> = {
    DarkAvenger: classSetOne,
    Paladin: classSetOne,
    TempleKnight: classSetOne,
    ShillienKnight: classSetOne,

    TreasureHunter: classSetTwo,
    AbyssWalker: classSetTwo,
    Plainswalker: classSetTwo,

    Hawkeye: classSetThree,
    SilverRanger: classSetThree,
    PhantomRanger: classSetThree,

    Warlock: classSetFour,
    ElementalSummoner: classSetFour,
    PhantomSummoner: classSetFour,

    Sorceror: classSetFive,
    Spellsinger: classSetFive,
    Spellhowler: classSetFive,
}

export function getAvailableSubclasses( playerClass : IPlayerClass, player: L2PcInstance ) : Array<IPlayerClass> {
    if ( playerClass.level !== ClassLevel.Third ) {
        return null
    }

    if ( player.getRace() !== Race.KAMAEL ) {
        let subclasses : Array<IPlayerClass> = [ ... nonKamaelSubclassValues ]

        _.pull( subclasses, playerClass )

        switch ( player.getRace() ) {
            case Race.ELF:
                _.pullAll( subclasses, getSet( Race.DARK_ELF, ClassLevel.Third ) )
                break

            case Race.DARK_ELF:
                _.pullAll( subclasses, getSet( Race.ELF, ClassLevel.Third ) )
                break
        }

        let unavailableClasses : Array<IPlayerClass> = subclassSets[ playerClass.name ]

        if ( unavailableClasses ) {
            _.pullAll( subclasses, unavailableClasses )
        }

        return subclasses
    }

    let subclasses : Array<IPlayerClass> = [ ... kamaelSubclassValues ]
    _.pull( subclasses, playerClass )

    // Check sex, male subclasses female and vice versa
    // If server owner set MaxSubclass > 3 some kamael's cannot take 4 sub
    // So, in that situation we must skip sex check
    if ( ConfigManager.character.getMaxSubclass() <= 3 ) {
        if ( player.getAppearance().getSex() ) {
            _.pull( subclasses, PlayerClassNameValues[ 'femaleSoulbreaker' ] )
        } else {
            _.pull( subclasses, PlayerClassNameValues[ 'maleSoulbreaker' ] )
        }
    }

    if ( !player.getSubClasses()[ 2 ] || player.getSubClasses()[ 2 ].getLevel() < 75 ) {
        _.pull( subclasses, PlayerClassNameValues[ 'inspector' ] )
    }

    return subclasses
}