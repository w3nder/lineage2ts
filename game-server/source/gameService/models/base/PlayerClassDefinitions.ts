import { ClassType } from './ClassType'
import { ClassLevel } from './ClassLevel'
import { Race } from '../../enums/Race'

export interface PlayerClassDefinition {
    race: Race,
    level: ClassLevel,
    type: ClassType
    classId: number
}

// TODO : unify with class id since it looks like data is duplicated
export const PlayerClassDefinitions = {
    Warrior: {
        race: Race.HUMAN,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 1
    },
    Gladiator: {
        race: Race.HUMAN,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 2
    },
    Warlord: {
        race: Race.HUMAN,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 3
    },
    HumanKnight: {
        race: Race.HUMAN,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 4
    },
    Paladin: {
        race: Race.HUMAN,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 5
    },
    DarkAvenger: {
        race: Race.HUMAN,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 6
    },
    Rogue: {
        race: Race.HUMAN,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 7
    },
    TreasureHunter: {
        race: Race.HUMAN,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 8
    },
    Hawkeye: {
        race: Race.HUMAN,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 9
    },
    HumanMystic: {
        race: Race.HUMAN,
        level: ClassLevel.First,
        type: ClassType.Mystic,
        classId: 10
    },
    HumanWizard: {
        race: Race.HUMAN,
        level: ClassLevel.Second,
        type: ClassType.Mystic,
        classId: 11
    },
    Sorceror: {
        race: Race.HUMAN,
        level: ClassLevel.Third,
        type: ClassType.Mystic,
        classId: 12
    },
    Necromancer: {
        race: Race.HUMAN,
        level: ClassLevel.Third,
        type: ClassType.Mystic,
        classId: 13
    },
    Warlock: {
        race: Race.HUMAN,
        level: ClassLevel.Third,
        type: ClassType.Mystic,
        classId: 14
    },
    Cleric: {
        race: Race.HUMAN,
        level: ClassLevel.Second,
        type: ClassType.Priest,
        classId: 15
    },
    Bishop: {
        race: Race.HUMAN,
        level: ClassLevel.Third,
        type: ClassType.Priest,
        classId: 16
    },
    Prophet: {
        race: Race.HUMAN,
        level: ClassLevel.Third,
        type: ClassType.Priest,
        classId: 17
    },
    ElvenFighter: {
        race: Race.ELF,
        level: ClassLevel.First,
        type: ClassType.Fighter,
        classId: 18
    },
    ElvenKnight: {
        race: Race.ELF,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 19
    },
    TempleKnight: {
        race: Race.ELF,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 20
    },
    Swordsinger: {
        race: Race.ELF,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 21
    },
    ElvenScout: {
        race: Race.ELF,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 22
    },
    Plainswalker: {
        race: Race.ELF,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 23
    },
    SilverRanger: {
        race: Race.ELF,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 24
    },
    ElvenMystic: {
        race: Race.ELF,
        level: ClassLevel.First,
        type: ClassType.Mystic,
        classId: 25
    },
    ElvenWizard: {
        race: Race.ELF,
        level: ClassLevel.Second,
        type: ClassType.Mystic,
        classId: 26
    },
    Spellsinger: {
        race: Race.ELF,
        level: ClassLevel.Third,
        type: ClassType.Mystic,
        classId: 27
    },
    ElementalSummoner: {
        race: Race.ELF,
        level: ClassLevel.Third,
        type: ClassType.Mystic,
        classId: 28
    },
    ElvenOracle: {
        race: Race.ELF,
        level: ClassLevel.Second,
        type: ClassType.Priest,
        classId: 29
    },
    ElvenElder: {
        race: Race.ELF,
        level: ClassLevel.Third,
        type: ClassType.Priest,
        classId: 30
    },
    DarkElvenFighter: {
        race: Race.DARK_ELF,
        level: ClassLevel.First,
        type: ClassType.Fighter,
        classId: 31
    },
    PalusKnight: {
        race: Race.DARK_ELF,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 32
    },
    ShillienKnight: {
        race: Race.DARK_ELF,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 33
    },
    Bladedancer: {
        race: Race.DARK_ELF,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 34
    },
    Assassin: {
        race: Race.DARK_ELF,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 35
    },
    AbyssWalker: {
        race: Race.DARK_ELF,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 36
    },
    PhantomRanger: {
        race: Race.DARK_ELF,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 37
    },
    DarkElvenMystic: {
        race: Race.DARK_ELF,
        level: ClassLevel.First,
        type: ClassType.Mystic,
        classId: 38
    },
    DarkElvenWizard: {
        race: Race.DARK_ELF,
        level: ClassLevel.Second,
        type: ClassType.Mystic,
        classId: 39
    },
    Spellhowler: {
        race: Race.DARK_ELF,
        level: ClassLevel.Third,
        type: ClassType.Mystic,
        classId: 40
    },
    PhantomSummoner: {
        race: Race.DARK_ELF,
        level: ClassLevel.Third,
        type: ClassType.Mystic,
        classId: 41
    },
    ShillienOracle: {
        race: Race.DARK_ELF,
        level: ClassLevel.Second,
        type: ClassType.Priest,
        classId: 42
    },
    ShillienElder: {
        race: Race.DARK_ELF,
        level: ClassLevel.Third,
        type: ClassType.Priest,
        classId: 43
    },
    OrcFighter: {
        race: Race.ORC,
        level: ClassLevel.First,
        type: ClassType.Fighter,
        classId: 44
    },
    OrcRaider: {
        race: Race.ORC,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 45
    },
    Destroyer: {
        race: Race.ORC,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 46
    },
    OrcMonk: {
        race: Race.ORC,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 47
    },
    Tyrant: {
        race: Race.ORC,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 48
    },
    OrcMystic: {
        race: Race.ORC,
        level: ClassLevel.First,
        type: ClassType.Mystic,
        classId: 49
    },
    OrcShaman: {
        race: Race.ORC,
        level: ClassLevel.Second,
        type: ClassType.Mystic,
        classId: 50
    },
    Overlord: {
        race: Race.ORC,
        level: ClassLevel.Third,
        type: ClassType.Mystic,
        classId: 51
    },
    Warcryer: {
        race: Race.ORC,
        level: ClassLevel.Third,
        type: ClassType.Mystic,
        classId: 52
    },
    DwarvenFighter: {
        race: Race.DWARF,
        level: ClassLevel.First,
        type: ClassType.Fighter,
        classId: 53
    },
    DwarvenScavenger: {
        race: Race.DWARF,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 54
    },
    BountyHunter: {
        race: Race.DWARF,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 55
    },
    DwarvenArtisan: {
        race: Race.DWARF,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 56
    },
    Warsmith: {
        race: Race.DWARF,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 57
    },
    dummyEntry1: {
        race: null,
        level: null,
        type: null,
        classId: 58
    },
    dummyEntry2: {
        race: null,
        level: null,
        type: null,
        classId: 59
    },
    dummyEntry3: {
        race: null,
        level: null,
        type: null,
        classId: 60
    },
    dummyEntry4: {
        race: null,
        level: null,
        type: null,
        classId: 61
    },
    dummyEntry5: {
        race: null,
        level: null,
        type: null,
        classId: 62
    },
    dummyEntry6: {
        race: null,
        level: null,
        type: null,
        classId: 63
    },
    dummyEntry7: {
        race: null,
        level: null,
        type: null,
        classId: 64
    },
    dummyEntry8: {
        race: null,
        level: null,
        type: null,
        classId: 65
    },
    dummyEntry9: {
        race: null,
        level: null,
        type: null,
        classId: 66
    },
    dummyEntry10: {
        race: null,
        level: null,
        type: null,
        classId: 67
    },
    dummyEntry11: {
        race: null,
        level: null,
        type: null,
        classId: 68
    },
    dummyEntry12: {
        race: null,
        level: null,
        type: null,
        classId: 69
    },
    dummyEntry13: {
        race: null,
        level: null,
        type: null,
        classId: 70
    },
    dummyEntry14: {
        race: null,
        level: null,
        type: null,
        classId: 71
    },
    dummyEntry15: {
        race: null,
        level: null,
        type: null,
        classId: 72
    },
    dummyEntry16: {
        race: null,
        level: null,
        type: null,
        classId: 73
    },
    dummyEntry17: {
        race: null,
        level: null,
        type: null,
        classId: 74
    },
    dummyEntry18: {
        race: null,
        level: null,
        type: null,
        classId: 75
    },
    dummyEntry19: {
        race: null,
        level: null,
        type: null,
        classId: 76
    },
    dummyEntry20: {
        race: null,
        level: null,
        type: null,
        classId: 77
    },
    dummyEntry21: {
        race: null,
        level: null,
        type: null,
        classId: 78
    },
    dummyEntry22: {
        race: null,
        level: null,
        type: null,
        classId: 79
    },
    dummyEntry23: {
        race: null,
        level: null,
        type: null,
        classId: 80
    },
    dummyEntry24: {
        race: null,
        level: null,
        type: null,
        classId: 81
    },
    dummyEntry25: {
        race: null,
        level: null,
        type: null,
        classId: 82
    },
    dummyEntry26: {
        race: null,
        level: null,
        type: null,
        classId: 83
    },
    dummyEntry27: {
        race: null,
        level: null,
        type: null,
        classId: 84
    },
    dummyEntry28: {
        race: null,
        level: null,
        type: null,
        classId: 85
    },
    dummyEntry29: {
        race: null,
        level: null,
        type: null,
        classId: 86
    },
    dummyEntry30: {
        race: null,
        level: null,
        type: null,
        classId: 87
    },
    duelist: {
        race: Race.HUMAN,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 88
    },
    dreadnought: {
        race: Race.HUMAN,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 89
    },
    phoenixKnight: {
        race: Race.HUMAN,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 90
    },
    hellKnight: {
        race: Race.HUMAN,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 91
    },
    sagittarius: {
        race: Race.HUMAN,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 92
    },
    adventurer: {
        race: Race.HUMAN,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 93
    },
    archmage: {
        race: Race.HUMAN,
        level: ClassLevel.Fourth,
        type: ClassType.Mystic,
        classId: 94
    },
    soultaker: {
        race: Race.HUMAN,
        level: ClassLevel.Fourth,
        type: ClassType.Mystic,
        classId: 95
    },
    arcanaLord: {
        race: Race.HUMAN,
        level: ClassLevel.Fourth,
        type: ClassType.Mystic,
        classId: 96
    },
    cardinal: {
        race: Race.HUMAN,
        level: ClassLevel.Fourth,
        type: ClassType.Priest,
        classId: 97
    },
    hierophant: {
        race: Race.HUMAN,
        level: ClassLevel.Fourth,
        type: ClassType.Priest,
        classId: 98
    },
    evaTemplar: {
        race: Race.ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 99
    },
    swordMuse: {
        race: Race.ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 100
    },
    windRider: {
        race: Race.ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 101
    },
    moonlightSentinel: {
        race: Race.ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 102
    },
    mysticMuse: {
        race: Race.ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Mystic,
        classId: 103
    },
    elementalMaster: {
        race: Race.ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Mystic,
        classId: 104
    },
    evaSaint: {
        race: Race.ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Priest,
        classId: 105
    },
    shillienTemplar: {
        race: Race.DARK_ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 106
    },
    spectralDancer: {
        race: Race.DARK_ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 107
    },
    ghostHunter: {
        race: Race.DARK_ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 108
    },
    ghostSentinel: {
        race: Race.DARK_ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 109
    },
    stormScreamer: {
        race: Race.DARK_ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Mystic,
        classId: 110
    },
    spectralMaster: {
        race: Race.DARK_ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Mystic,
        classId: 111
    },
    shillienSaint: {
        race: Race.DARK_ELF,
        level: ClassLevel.Fourth,
        type: ClassType.Priest,
        classId: 112
    },
    titan: {
        race: Race.ORC,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 113
    },
    grandKhavatari: {
        race: Race.ORC,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 114
    },
    dominator: {
        race: Race.ORC,
        level: ClassLevel.Fourth,
        type: ClassType.Mystic,
        classId: 115
    },
    doomcryer: {
        race: Race.ORC,
        level: ClassLevel.Fourth,
        type: ClassType.Mystic,
        classId: 116
    },
    fortuneSeeker: {
        race: Race.DWARF,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 117
    },
    maestro: {
        race: Race.DWARF,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 118
    },
    dummyEntry31: {
        race: null,
        level: null,
        type: null,
        classId: 119
    },
    dummyEntry32: {
        race: null,
        level: null,
        type: null,
        classId: 120
    },
    dummyEntry33: {
        race: null,
        level: null,
        type: null,
        classId: 121
    },
    dummyEntry34: {
        race: null,
        level: null,
        type: null,
        classId: 122
    },
    maleSoldier: {
        race: Race.KAMAEL,
        level: ClassLevel.First,
        type: ClassType.Fighter,
        classId: 123
    },
    femaleSoldier: {
        race: Race.KAMAEL,
        level: ClassLevel.First,
        type: ClassType.Fighter,
        classId: 124
    },
    trooper: {
        race: Race.KAMAEL,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 125
    },
    warder: {
        race: Race.KAMAEL,
        level: ClassLevel.Second,
        type: ClassType.Fighter,
        classId: 126
    },
    berserker: {
        race: Race.KAMAEL,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 127
    },
    maleSoulbreaker: {
        race: Race.KAMAEL,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 128
    },
    femaleSoulbreaker: {
        race: Race.KAMAEL,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 129
    },
    arbalester: {
        race: Race.KAMAEL,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 130
    },
    doombringer: {
        race: Race.KAMAEL,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 131
    },
    maleSoulhound: {
        race: Race.KAMAEL,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 132
    },
    femaleSoulhound: {
        race: Race.KAMAEL,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 133
    },
    trickster: {
        race: Race.KAMAEL,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 134
    },
    inspector: {
        race: Race.KAMAEL,
        level: ClassLevel.Third,
        type: ClassType.Fighter,
        classId: 135
    },
    judicator: {
        race: Race.KAMAEL,
        level: ClassLevel.Fourth,
        type: ClassType.Fighter,
        classId: 136
    }
}