import { getPlayerClassById, IPlayerClass } from './PlayerClass'
import { ConfigManager } from '../../../config/ConfigManager'
import { PcStats } from '../actor/stat/PcStats'
import { L2PcInstance } from '../actor/instance/L2PcInstance'

export class SubClass {
    playerClass: IPlayerClass
    classIndex: number
    stats: PcStats

    constructor( player: L2PcInstance, amount: number ) {
        this.stats = new PcStats( player )

        this.stats.setExp( amount )
        this.stats.setLevel( ConfigManager.character.getBaseSubclassLevel() )
    }

    getStat(): PcStats {
        return this.stats
    }

    getClassIndex(): number {
        return this.classIndex
    }

    getExp() : number {
        return this.stats.getExp()
    }

    getLevel() : number {
        return this.stats.getLevel()
    }

    getSp() : number {
        return this.stats.getSp()
    }

    getClassId() {
        return this.playerClass.classId
    }

    setClassId( value: number ) {
        this.playerClass = getPlayerClassById( value )
    }

    setExp( value: number ) {
        this.stats.setExp( value )
    }

    setLevel( value: number ) {
        this.stats.setLevel( value )
    }

    setSp( value: number ) {
        this.stats.setSp( value )
    }

    setClassIndex( value: number ) {
        this.classIndex = value
    }
}