import { Stats } from './Stats'
import { L2Character } from '../actor/L2Character'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { Skill } from '../Skill'
import { TraitType, TraitTypeValues } from './TraitType'
import { AttributeType } from '../skills/AttributeType'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { PacketDispatcher } from '../../PacketDispatcher'
import { BaseStats } from './BaseStats'
import { ShotType } from '../../enums/ShotType'
import { DataManager } from '../../../data/manager'
import { ConfigManager } from '../../../config/ConfigManager'
import { InstanceType } from '../../enums/InstanceType'
import { L2Item } from '../items/L2Item'
import { ItemInstanceType } from '../items/ItemInstanceType'
import { L2Armor } from '../items/L2Armor'
import { ArmorType } from '../../enums/items/ArmorType'
import { L2Weapon } from '../items/L2Weapon'
import { WeaponType, WeaponTypeMap } from '../items/type/WeaponType'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { BuffInfo } from '../skills/BuffInfo'
import { DispelCategory } from '../../enums/DispelCategory'
import { L2CubicInstance } from '../actor/instance/L2CubicInstance'
import { FortFunctionType } from '../entity/Fort'
import { L2PetInstance } from '../actor/instance/L2PetInstance'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { Siege } from '../entity/Siege'
import { SiegeManager } from '../../instancemanager/SiegeManager'
import { L2SiegeClan } from '../L2SiegeClan'
import { L2World } from '../../L2World'
import { L2Npc } from '../actor/L2Npc'
import { HitConditionBonusManager } from '../../cache/HitConditionBonusManager'
import _ from 'lodash'
import { BuffProperties } from '../skills/BuffDefinition'
import { CastleFunctions } from '../../enums/CastleFunctions'
import { MotherTreeArea } from '../areas/type/MotherTree'
import { AreaType } from '../areas/AreaType'
import { AreaDiscoveryManager } from '../../cache/AreaDiscoveryManager'
import { ClanHallFunctionType } from '../../enums/clanhall/ClanHallFunctionType'

const SHIELD_DEFENSE_FAILED = 0 // no shield defense
const SHIELD_DEFENSE_SUCCEED = 1 // normal shield defense
const SHIELD_DEFENSE_PERFECT_BLOCK = 2 // perfect block
const MELEE_ATTACK_RANGE = 40

export const FormulasValues = {
    SHIELD_DEFENSE_FAILED,
    SHIELD_DEFENSE_SUCCEED,
    SHIELD_DEFENSE_PERFECT_BLOCK,
}

export const Formulas = {
    getRegenerationPeriod( character: L2Character ): number {
        return character.isDoor() ? 300000 : 3000
    },

    calculateSkillEffectDurationSeconds( caster: L2Character, target: L2Character, skill: Skill ): number {
        let time = -1

        if ( !skill.isPassive() && !skill.isToggle() ) {
            time = skill.getAbnormalTime()
        }

        /*
            An herb buff will affect both master and servitor, but the buff duration will be half of the normal duration.
            If a servitor is not summoned, the master will receive the full buff duration.
         */
        if ( target && target.isServitor() && skill.isAbnormalInstant ) {
            time = time * 2
        }

        if ( Formulas.calculateSkillMastery( caster, skill ) ) {
            time *= 2
        }

        if ( caster && target && skill.isDebuff() ) {
            let statModifier = skill.getBasicProperty()( target )
            let traitModifier = Formulas.calculateGeneralTraitBonus( caster, target, skill.getTraitType(), false )
            let levelBonus = Formulas.calculateLevelBonusModifier( caster, target, skill )
            let attributeBonus = Formulas.calculateAttributeBonus( caster, target, skill )
            time = Math.ceil( _.clamp( ( ( time * traitModifier * levelBonus * attributeBonus ) / statModifier ), ( time * 0.5 ), time ) )
        }

        return time
    },

    calculateGeneralTraitBonus( attacker: L2Character, target: L2Character, trait: TraitType, ignoreResistance: boolean ): number {
        if ( trait === TraitTypeValues.NONE ) {
            return 1
        }

        if ( target.getStat().isTraitInvulnerable( trait ) ) {
            return 0
        }

        switch ( trait.type ) {
            case 2:
                if ( !attacker.getStat().hasAttackTrait( trait ) || !target.getStat().hasDefenceTrait( trait ) ) {
                    return 1
                }
                break
            case 3:
                if ( ignoreResistance ) {
                    return 1
                }
                break
            default:
                return 1
        }

        let result = attacker.getStat().getAttackTrait( trait ) - target.getStat().getDefenceTrait( trait ) + 1
        return _.clamp( result, 0.05, 2 )
    },

    calculateLevelBonusModifier( attacker: L2Character, target: L2Character, skill: Skill ): number {
        let attackerLevel = skill.getMagicLevel() > 0 ? skill.getMagicLevel() : attacker.getLevel()
        let skillLevelBonus = 1 + skill.getLevelBonusRate() / 100
        let levelModifier = 1 + ( attackerLevel - target.getLevel() ) / 100
        return skillLevelBonus * levelModifier
    },

    calculateAttributeBonus( attacker: L2Character, target: L2Character, skill: Skill ): number {

        let attackOutcome: number = 0

        if ( skill ) {
            if ( ( skill.getAttributeType() === AttributeType.NONE ) || ( attacker.getAttackElement() !== skill.getAttributeType() ) ) {
                return 1
            }
            attackOutcome = attacker.getAttackElementValue( attacker.getAttackElement() ) + skill.getAttributePower()
        } else {
            attackOutcome = attacker.getAttackElementValue( attacker.getAttackElement() )
            if ( attackOutcome === 0 ) {
                return 1
            }
        }

        let defenceOutcome = target.getDefenceElementValue( attacker.getAttackElement() )

        if ( attackOutcome <= defenceOutcome ) {
            return 1
        }

        let attackModifier = 0
        let defenceModifier = 0

        if ( attackOutcome >= 450 ) {
            if ( defenceOutcome >= 450 ) {
                attackModifier = 0.06909
                defenceModifier = 0.078
            } else if ( defenceOutcome >= 350 ) {
                attackModifier = 0.0887
                defenceModifier = 0.1007
            } else {
                attackModifier = 0.129
                defenceModifier = 0.1473
            }
        } else if ( attackOutcome >= 300 ) {
            if ( defenceOutcome >= 300 ) {
                attackModifier = 0.0887
                defenceModifier = 0.1007
            } else if ( defenceOutcome >= 150 ) {
                attackModifier = 0.129
                defenceModifier = 0.1473
            } else {
                attackModifier = 0.25
                defenceModifier = 0.2894
            }
        } else if ( attackOutcome >= 150 ) {
            if ( defenceOutcome >= 150 ) {
                attackModifier = 0.129
                defenceModifier = 0.1473
            } else if ( defenceOutcome >= 0 ) {
                attackModifier = 0.25
                defenceModifier = 0.2894
            } else {
                attackModifier = 0.4
                defenceModifier = 0.55
            }
        } else if ( attackOutcome >= -99 ) {
            if ( defenceOutcome >= 0 ) {
                attackModifier = 0.25
                defenceModifier = 0.2894
            } else {
                attackModifier = 0.4
                defenceModifier = 0.55
            }
        } else {
            if ( defenceOutcome >= 450 ) {
                attackModifier = 0.06909
                defenceModifier = 0.078
            } else if ( defenceOutcome >= 350 ) {
                attackModifier = 0.0887
                defenceModifier = 0.1007
            } else {
                attackModifier = 0.129
                defenceModifier = 0.1473
            }
        }

        let differenceOutcome = attackOutcome - defenceOutcome
        let minValue = 0
        let maxValue = 0

        if ( differenceOutcome >= 300 ) {
            maxValue = 100.0
            minValue = -50
        } else if ( differenceOutcome >= 150 ) {
            maxValue = 70.0
            minValue = -50
        } else if ( differenceOutcome >= -150 ) {
            maxValue = 40.0
            minValue = -50
        } else if ( differenceOutcome >= -300 ) {
            maxValue = 40.0
            minValue = -60
        } else {
            maxValue = 40.0
            minValue = -80
        }

        attackOutcome += 100
        attackOutcome *= attackOutcome

        attackModifier = ( attackOutcome / 144.0 ) * attackModifier

        defenceOutcome += 100
        defenceOutcome *= defenceOutcome

        defenceModifier = ( defenceOutcome / 169.0 ) * defenceModifier

        let differenceModifier = attackModifier - defenceModifier

        differenceModifier = _.clamp( differenceModifier, minValue, maxValue )

        let result = ( differenceModifier / 100.0 ) + 1

        if ( attacker.isPlayer() && target.isPlayer() && ( result < 1.0 ) ) {
            result = 1.0
        }

        return result
    },

    calculateEffectSuccess( attacker: L2Character, target: L2Character, skill: Skill ) {
        if ( target.isDoor() || target.isInstanceType( InstanceType.L2SiegeFlagInstance ) || target.isStaticObject() ) {
            return false
        }

        if ( skill.isDebuff() && target.isDebuffBlocked() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.C1_RESISTED_YOUR_S2 )
                    .addCharacterName( target )
                    .addSkillName( skill )
                    .getBuffer()
            attacker.sendOwnedData( packet )
            return false
        }

        let activateRate = skill.getActivateRate()
        if ( activateRate === -1 || skill.getBasicProperty() === BaseStats.NONE ) {
            return true
        }

        let magicLevel = skill.getMagicLevel()
        if ( magicLevel <= -1 ) {
            magicLevel = target.getLevel() + 3
        }

        let targetBaseStat = function () {
            switch ( skill.getBasicProperty() ) {
                case BaseStats.STR:
                    return target.getSTR()
                case BaseStats.DEX:
                    return target.getDEX()
                case BaseStats.CON:
                    return target.getCON()
                case BaseStats.INT:
                    return target.getINT()
                case BaseStats.MEN:
                    return target.getMEN()
                case BaseStats.WIT:
                    return target.getWIT()
                default:
                    return 0
            }
        }()

        let baseModifier = ( ( ( magicLevel - target.getLevel() ) + 3 ) * skill.getLevelBonusRate() ) + activateRate + 30.0 - targetBaseStat
        let elementModifier = this.calculateAttributeBonus( attacker, target, skill )
        let traitModifier = this.calculateGeneralTraitBonus( attacker, target, skill.getTraitType(), false )
        let buffDebuffModifier = 1 + ( target.calculateStat( skill.isDebuff() ? Stats.DEBUFF_VULN : Stats.BUFF_VULN, 1, null, null ) / 100 )
        let magicAttackModifier = 1

        if ( skill.isMagic() ) {
            let magicAttack = attacker.getMagicAttack( null, null )
            let outcome = 0
            if ( attacker.isChargedShot( ShotType.BlessedSpiritshot ) ) {
                outcome = magicAttack * 3.0
            }
            outcome += magicAttack
            outcome = ( Math.sqrt( outcome ) / target.getMagicDefence( null, null ) ) * 11.0
            magicAttackModifier = outcome
        }

        let rate = baseModifier * elementModifier * traitModifier * magicAttackModifier * buffDebuffModifier
        let finalRate = traitModifier > 0 ? _.clamp( rate, skill.getMinChance(), skill.getMaxChance() ) : 0

        if ( finalRate <= _.random( 100 ) ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.C1_RESISTED_YOUR_S2 )
                    .addCharacterName( target )
                    .addSkillName( skill )
                    .getBuffer()
            attacker.sendOwnedData( packet )
            return false
        }

        return true
    },

    calculateSkillMastery( actor: L2Character, skill: Skill ): boolean {
        if ( skill.isStatic() ) {
            return false
        }

        let value = actor.getStat().calculateStat( Stats.SKILL_CRITICAL, 0, null, null )
        if ( value === 0 ) {
            return false
        }

        if ( actor.isPlayer() ) {
            let outcome
            switch ( value ) {
                case 1:
                    outcome = BaseStats.STR( actor )
                    break
                case 4:
                    outcome = BaseStats.INT( actor )
                    break
                default:
                    return false
            }

            outcome = outcome * actor.getStat().calculateStat( Stats.SKILL_CRITICAL_PROBABILITY, 1, null, null )
            return outcome > _.random( 100 )
        }

        return false
    },

    calculateBuffDebuffReflection( target: L2Character, skill: Skill ): boolean {
        return target.calculateStat( skill.isMagic() ? Stats.REFLECT_SKILL_MAGIC : Stats.REFLECT_SKILL_PHYSIC, 0, null, skill ) > _.random( 100 )
    },

    calculateKarmaLost( player: L2PcInstance, exp: number ): number {
        let multiplier = DataManager.getKarmaData().getMultiplier( player.getLevel() )
        if ( exp > 0 ) {
            exp /= ConfigManager.rates.getRateKarmaLost()
        }
        return Math.floor( ( Math.abs( exp ) / multiplier ) / 30 )
    },

    calculatePhysicalSkillEvasion( attacker: L2Character, target: L2Character, skill: Skill ) {
        if ( skill.isMagic() || skill.isDebuff() ) {
            return false
        }

        if ( _.random( 100 ) < target.calculateStat( Stats.P_SKILL_EVASION, 0, null, skill ) ) {
            if ( attacker.isPlayer() ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.C1_DODGES_ATTACK )
                        .addString( target.getName() )
                        .getBuffer()

                attacker.sendOwnedData( packet )
            }

            if ( target.isPlayer() ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.AVOIDED_C1_ATTACK2 )
                        .addString( attacker.getName() )
                        .getBuffer()

                attacker.sendOwnedData( packet )
            }

            return true
        }

        return false
    },

    calculateBlowSuccess( attacker: L2Character, target: L2Character, skill: Skill, blowChance: number ) {
        let dexMod = BaseStats.DEX( attacker )
        let sideMod = ( attacker.isInFrontOfTarget() ) ? 1 : ( attacker.isBehindTarget() ) ? 2 : 1.5
        let baseRate = blowChance * dexMod * sideMod
        let rate = attacker.calculateStat( Stats.BLOW_RATE, baseRate, target, null )
        return _.random( 100 ) < rate
    },

    calculateShieldUse( attacker: L2Character, target: L2Character, skill: Skill, sendSystemMessage: boolean = true ) {
        let item: L2Item = target.getSecondaryWeaponItem()
        if ( !item
                || !item.isInstanceType( ItemInstanceType.L2Armor )
                || ( ( item as L2Armor ).getItemType() === ArmorType.SIGIL ) ) {
            return 0
        }

        let shieldBlockRate = target.calculateStat( Stats.SHIELD_RATE, 0, attacker, null ) * BaseStats.DEX( target )
        if ( shieldBlockRate <= 1e-6 ) {
            return 0
        }

        let degrees = target.calculateStat( Stats.SHIELD_DEFENCE_ANGLE, 0, null, null ) + 120
        if ( ( degrees < 360 ) && ( !target.isFacing( attacker, degrees ) ) ) {
            return 0
        }

        let blockStatus = SHIELD_DEFENSE_FAILED

        let weapon: L2Weapon = attacker.getActiveWeaponItem()
        if ( weapon && weapon.getItemType() === WeaponType.BOW ) {
            shieldBlockRate *= 1.3
        }

        if ( ( shieldBlockRate > 0 ) && ( ( 100 - ConfigManager.character.getPerfectShieldBlockRate() ) < _.random( 100 ) ) ) {
            blockStatus = SHIELD_DEFENSE_PERFECT_BLOCK
        } else if ( shieldBlockRate > _.random( 100 ) ) {
            blockStatus = SHIELD_DEFENSE_SUCCEED
        }

        if ( sendSystemMessage && target.isPlayer() ) {
            let player: L2PcInstance = target.getActingPlayer()

            switch ( blockStatus ) {
                case SHIELD_DEFENSE_SUCCEED:
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SHIELD_DEFENCE_SUCCESSFULL ) )
                    break
                case SHIELD_DEFENSE_PERFECT_BLOCK:
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_EXCELLENT_SHIELD_DEFENSE_WAS_A_SUCCESS ) )
                    break
            }
        }

        return blockStatus
    },

    calculateBackstabDamage( attacker: L2Character, target: L2Character, skill: Skill, shieldUse: number, isCharged: boolean, power: number ) {
        let defence = target.getPowerDefence( attacker )

        switch ( shieldUse ) {
            case FormulasValues.SHIELD_DEFENSE_SUCCEED:
                defence += target.getShieldDefence()
                break
            case FormulasValues.SHIELD_DEFENSE_PERFECT_BLOCK:
                return 1
        }

        let isPvP = attacker.isPlayable() && target.isPlayer()
        let damage
        let proximityBonus = attacker.isBehindTarget() ? 1.2 : attacker.isInFrontOfTarget() ? 1 : 1.1 // Behind: +20% - Side: +10%
        let soulsShotBoost = isCharged ? 1.458 : 1
        let pvpBonus = 1

        if ( isPvP ) {
            pvpBonus = attacker.calculateStat( Stats.PVP_PHYS_SKILL_DMG, 1, null, null )
            defence *= target.calculateStat( Stats.PVP_PHYS_SKILL_DEF, 1, null, null )
        }

        let baseMod = ( ( 77 * ( power + attacker.getPowerAttack( target ) ) ) / defence ) * soulsShotBoost
        let criticalMod = ( attacker.calculateStat( Stats.CRITICAL_DAMAGE, 1, target, skill ) )
        let criticalModPos = ( ( ( attacker.calculateStat( Stats.CRITICAL_DAMAGE_POS, 1, target, skill ) - 1 ) / 2 ) + 1 )
        let criticalVulnMod = ( target.calculateStat( Stats.DEFENCE_CRITICAL_DAMAGE, 1, target, skill ) )
        let criticalAddMod = ( ( attacker.calculateStat( Stats.CRITICAL_DAMAGE_ADD, 0, target, skill ) * 6.1 * 77 ) / defence )
        let criticalAddVuln = target.calculateStat( Stats.DEFENCE_CRITICAL_DAMAGE_ADD, 0, target, skill )
        let generalTraitMod = Formulas.calculateGeneralTraitBonus( attacker, target, skill.getTraitType(), false )
        let attributeMod = Formulas.calculateAttributeBonus( attacker, target, skill )
        let weaponMod = attacker.getRandomDamageMultiplier()

        let penaltyMod = 1
        if ( target.isAttackable()
                && !target.isRaid()
                && !target.isRaidMinion()
                && ( target.getLevel() >= ConfigManager.npc.getMinNPCLevelForDamagePenalty() )
                && ( attacker.getActingPlayer() )
                && ( ( target.getLevel() - attacker.getActingPlayer().getLevel() ) >= 2 ) ) {
            let levelDifference = target.getLevel() - attacker.getActingPlayer().getLevel() - 1
            if ( levelDifference >= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences().length ) {
                penaltyMod *= _.last( ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences() )
            } else {
                penaltyMod *= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences()[ levelDifference ]
            }

        }

        damage = ( baseMod * criticalMod * criticalModPos * criticalVulnMod * proximityBonus * pvpBonus ) + criticalAddMod + criticalAddVuln
        damage *= generalTraitMod
        damage *= attributeMod
        damage *= weaponMod
        damage *= penaltyMod

        return Math.max( damage, 1 )
    },

    calculateSkillCrit( attacker: L2Character, target: L2Character, criticalChance: number ) {
        return ( BaseStats.STR( attacker ) * criticalChance ) > _.random( 100 )
    },

    calculateAttackBreak( target: L2Character, damage: number ) {
        if ( target.isChanneling() ) {
            return false
        }

        let value = 0
        if ( ConfigManager.character.cancelCast() && target.isCastingNow() ) {
            value = 15
        }

        if ( ConfigManager.character.cancelBow() && target.isAttackingNow() ) {
            let weapon: L2Weapon = target.getActiveWeaponItem()
            if ( weapon && ( weapon.getItemType() === WeaponType.BOW ) ) {
                value = 15
            }
        }

        if ( target.isRaid() || target.isInvulnerable() || ( value <= 0 ) ) {
            return false
        }

        value += Math.sqrt( 13 * damage )
        value -= ( ( BaseStats.MEN( target ) * 100 ) - 100 )

        let rate = target.calculateStat( Stats.ATTACK_CANCEL, value, null, null )
        rate = Math.max( Math.min( rate, 99 ), 1 )

        return _.random( 100 ) < rate
    },

    async calculateDamageReflected( attacker: L2Character, target: L2Character, skill: Skill, isCritical: boolean ): Promise<void> {
        if ( skill.isMagic() || ( skill.getCastRange() > MELEE_ATTACK_RANGE ) ) {
            return
        }

        let chance = target.calculateStat( Stats.VENGEANCE_SKILL_PHYSICAL_DAMAGE, 0, target, skill )
        if ( _.random( 100 ) < chance ) {
            if ( target.isPlayer() ) {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.COUNTERED_C1_ATTACK )
                        .addCharacterName( attacker )
                        .getBuffer()
                target.sendOwnedData( packet )
            }

            if ( attacker.isPlayer() ) {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_PERFORMING_COUNTERATTACK )
                        .addCharacterName( target )
                        .getBuffer()
                attacker.sendOwnedData( packet )
            }

            let counterdmg = ( ( ( target.getPowerAttack( attacker ) * 10.0 ) * 70.0 ) / attacker.getPowerDefence( target ) )
            counterdmg *= Formulas.calculateWeaponTraitBonus( attacker, target )
            counterdmg *= Formulas.calculateGeneralTraitBonus( attacker, target, skill.getTraitType(), false )
            counterdmg *= Formulas.calculateAttributeBonus( attacker, target, skill )

            await attacker.reduceCurrentHp( counterdmg, target, skill )
            if ( isCritical ) {
                await attacker.reduceCurrentHp( counterdmg, target, skill )
            }

            target.notifyDamageReceived( counterdmg, attacker, skill, isCritical, false, true )
        }
    },

    calculateWeaponTraitBonus( attacker: L2Character, target: L2Character ) {
        let type: TraitType = WeaponTypeMap[ attacker.getAttackType() ]
        let result = target.getStat().getDefenceTraits()[ type.id ] - 1.0
        return 1.0 - result
    },

    calculateProbability( chance: number, attacker: L2Character, target: L2Character, skill: Skill ) {
        let attributeBonus = Formulas.calculateAttributeBonus( attacker, target, skill )
        let traitBonus = Formulas.calculateGeneralTraitBonus( attacker, target, skill.getTraitType(), false )
        return _.random( 100 ) < ( ( ( ( ( ( skill.getMagicLevel() + chance ) - target.getLevel() ) + 30 ) - target.getINT() ) * attributeBonus ) * traitBonus )
    },

    calculateMagicCrit( attacker: L2Character, target: L2Character, skill: Skill ) : boolean {
        return attacker.getMCriticalHit( target, skill ) > _.random( 100 )
    },

    calculateMagicDamage( attacker: L2Character, target: L2Character, skill: Skill, shieldUse: number, isSpiritshot: boolean, isBlessedSpiritshot: boolean, magicCrit: boolean, power: number ): number {
        return Formulas.calculateMagicDamageWithShield( attacker, target, skill, shieldUse, 0, isSpiritshot, isBlessedSpiritshot, magicCrit, power )
    },

    calculateMagicDamageWithShield( attacker: L2Character, target: L2Character, skill: Skill, shieldUse: number, shieldDefence: number, isSpiritshot: boolean, isBlessedSpiritshot: boolean, magicCrit: boolean, power: number ): number {
        let magicDamageBase = target.getMagicDefence( attacker, skill )
        switch ( shieldUse ) {
            case FormulasValues.SHIELD_DEFENSE_SUCCEED:
                magicDamageBase += target.getShieldDefence()
                break
            case FormulasValues.SHIELD_DEFENSE_PERFECT_BLOCK:
                return 1
        }

        let magicAttackBase = attacker.getMagicAttack( target, skill )
        let isPvP = attacker.isPlayable() && target.isPlayable()

        if ( isPvP ) {
            if ( skill.isMagic() ) {
                magicDamageBase *= target.calculateStat( Stats.PVP_MAGICAL_DEF, 1, null, null )
            } else {
                magicDamageBase *= target.calculateStat( Stats.PVP_PHYS_SKILL_DEF, 1, null, null )
            }
        }

        magicAttackBase *= isBlessedSpiritshot ? 4 : isSpiritshot ? 2 : 1
        let damage = ( ( 91 * Math.sqrt( magicAttackBase ) ) / magicDamageBase ) * power

        let calculateMagicSuccess = Formulas.calculateMagicSuccess( attacker, target, skill )
        if ( ConfigManager.character.magicFailures() && !calculateMagicSuccess ) {
            if ( attacker.isPlayer() ) {
                if ( calculateMagicSuccess && ( ( target.getLevel() - attacker.getLevel() ) <= 9 ) ) {
                    if ( skill.hasEffectType( L2EffectType.HpDrain ) ) {
                        attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DRAIN_HALF_SUCCESFUL ) )
                    } else {
                        attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACK_FAILED ) )
                    }

                    damage /= 2
                } else {
                    let packet = new SystemMessageBuilder( SystemMessageIds.C1_RESISTED_YOUR_S2 )
                            .addCharacterName( target )
                            .addSkillName( skill )
                            .getBuffer()

                    attacker.sendOwnedData( packet )
                    damage = 1
                }
            }

            if ( target.isPlayer() ) {
                let messageId = ( skill.hasEffectType( L2EffectType.HpDrain ) ) ? SystemMessageIds.RESISTED_C1_DRAIN : SystemMessageIds.RESISTED_C1_MAGIC
                let packet: Buffer = new SystemMessageBuilder( messageId )
                        .addCharacterName( attacker )
                        .getBuffer()

                target.sendOwnedData( packet )
            }
        } else if ( magicCrit ) {
            damage *= attacker.isPlayer() && target.isPlayer() ? 2.5 : 3
            damage *= attacker.calculateStat( Stats.MAGIC_CRIT_DMG, 1, null, null )
        }

        damage *= attacker.getRandomDamageMultiplier()

        if ( isPvP ) {
            let stat = skill.isMagic() ? Stats.PVP_MAGICAL_DMG : Stats.PVP_PHYS_SKILL_DMG
            damage *= attacker.calculateStat( stat, 1, null, null )
        }

        damage *= Formulas.calculateAttributeBonus( attacker, target, skill )

        if ( target.isAttackable() ) {
            if ( !target.isRaid()
                    && !target.isRaidMinion()
                    && ( target.getLevel() >= ConfigManager.npc.getMinNPCLevelForDamagePenalty() )
                    && attacker.getActingPlayer()
                    && ( ( target.getLevel() - attacker.getActingPlayer().getLevel() ) >= 2 ) ) {
                let levelDifference = target.getLevel() - attacker.getActingPlayer().getLevel() - 1
                if ( levelDifference >= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences().length ) {
                    damage *= _.last( ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences() )
                } else {
                    damage *= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences()[ levelDifference ]
                }
            }
        }
        return damage
    },

    calculateMagicSuccess( attacker: L2Character, target: L2Character, skill: Skill ) {

        let lvlDifference = ( target.getLevel() - ( skill.getMagicLevel() > 0 ? skill.getMagicLevel() : attacker.getLevel() ) )
        let lvlModifier = Math.pow( 1.3, lvlDifference )
        let targetModifier = 1.00
        if ( target.isAttackable()
                && !target.isRaid()
                && !target.isRaidMinion()
                && ( target.getLevel() >= ConfigManager.npc.getMinNPCLevelForDamagePenalty() )
                && attacker.getActingPlayer()
                && ( ( target.getLevel() - attacker.getActingPlayer().getLevel() ) >= 3 ) ) {
            let lvlDiff = target.getLevel() - attacker.getActingPlayer().getLevel() - 2
            if ( lvlDiff >= ConfigManager.npc.getSkillChancePenaltyForLevelDifferences().length ) {
                targetModifier = _.last( ConfigManager.npc.getSkillChancePenaltyForLevelDifferences() )
            } else {
                targetModifier = ConfigManager.npc.getSkillChancePenaltyForLevelDifferences()[ lvlDiff ]
            }
        }

        let resModifier = target.calculateStat( Stats.MAGIC_SUCCESS_RES, 1, null, skill )
        let rawRate = lvlModifier * targetModifier * resModifier
        if ( rawRate >= 100 ) {
            return false
        }

        return _.random( 100, true ) < ( 100 - Math.floor( rawRate ) )
    },

    calculateCancelEffects( attacker: L2Character, target: L2Character, skill: Skill, slot: DispelCategory, rate: number, maxAmount: number ): Array<Skill> {
        let allBuffs: Array<Skill> = []
        switch ( slot ) {
            case DispelCategory.BUFF:
                let cancelMagicLvl = skill.getMagicLevel()
                let vulnerability = target.calculateStat( Stats.CANCEL_VULN, 0, target, null )
                let prof = attacker.calculateStat( Stats.CANCEL_PROF, 0, target, null )

                let resistanceModifier = 1 + ( ( ( vulnerability + prof ) * -1 ) / 100 )
                let finalRate = rate / resistanceModifier

                let buffs: Array<BuffInfo> = [ ...target.getEffectList().getBuffs(), ...target.getEffectList().getDances(), ...target.getEffectList().getTriggered() ].reverse()

                _.each( buffs, ( currentBuff: BuffInfo ) => {
                    if ( !currentBuff.getSkill().canBeStolen() || ( !Formulas.calculateCancelSuccess( currentBuff, cancelMagicLvl, finalRate, skill ) ) ) {
                        return
                    }

                    allBuffs.push( currentBuff.getSkill() )
                    if ( allBuffs.length >= maxAmount ) {
                        return false
                    }
                } )
                break

            case DispelCategory.DEBUFF:
                _.each( target.getEffectList().getDebuffs(), ( currentBuff: BuffInfo ) => {
                    if ( currentBuff.getSkill().isDebuff() && !currentBuff.getSkill().isIrreplaceableBuff() && _.random( 100 ) <= rate ) {
                        allBuffs.push( currentBuff.getSkill() )
                        if ( allBuffs.length >= maxAmount ) {
                            return false
                        }
                    }
                } )
                break
        }

        return allBuffs
    },

    calculateCancelSuccess( info: BuffInfo, cancelMagicLvl: number, rate: number, skill: Skill ): boolean {
        let calculatedRate = rate * ( info.getSkill().getMagicLevel() > 0 ? 1 + ( ( cancelMagicLvl - info.getSkill().getMagicLevel() ) / 100 ) : 1 )
        return _.random( 100 ) < _.clamp( calculatedRate, skill.getMinChance(), skill.getMaxChance() )
    },

    calculateBlowDamage( attacker: L2Character, target: L2Character, skill: Skill, shieldUse: number, usingSoulsShots: boolean, power: number ): number {
        let defence = target.getPowerDefence( attacker )

        switch ( shieldUse ) {
            case FormulasValues.SHIELD_DEFENSE_SUCCEED:
                defence += target.getShieldDefence()
                break
            case FormulasValues.SHIELD_DEFENSE_PERFECT_BLOCK:
                return 1
        }

        let isPvP: boolean = attacker.isPlayable() && target.isPlayable()
        let damage
        let proximityBonus = attacker.isBehindTarget() ? 1.2 : attacker.isInFrontOfTarget() ? 1 : 1.1 // Behind: +20% - Side: +10%
        let ssboost = usingSoulsShots ? 1.458 : 1
        let pvpBonus = 1

        if ( isPvP ) {
            pvpBonus = attacker.calculateStat( Stats.PVP_PHYS_SKILL_DMG, 1, null, null )
            defence *= target.calculateStat( Stats.PVP_PHYS_SKILL_DEF, 1, null, null )
        }

        let baseMod = ( ( 77 * ( power + ( attacker.getPowerAttack( target ) * ssboost ) ) ) / defence )
        let criticalMod = attacker.calculateStat( Stats.CRITICAL_DAMAGE, 1, target, skill )
        let criticalModPos = ( ( attacker.calculateStat( Stats.CRITICAL_DAMAGE_POS, 1, target, skill ) - 1 ) / 2 ) + 1
        let criticalVulnMod = target.calculateStat( Stats.DEFENCE_CRITICAL_DAMAGE, 1, target, skill )
        let criticalAddMod = ( attacker.calculateStat( Stats.CRITICAL_DAMAGE_ADD, 0 ) * 6.1 * 77 ) / defence
        let criticalAddVuln = target.calculateStat( Stats.DEFENCE_CRITICAL_DAMAGE_ADD, 0, target, skill )
        let weaponTraitMod = Formulas.calculateWeaponTraitBonus( attacker, target )
        let generalTraitMod = Formulas.calculateGeneralTraitBonus( attacker, target, skill.getTraitType(), false )
        let attributeMod = Formulas.calculateAttributeBonus( attacker, target, skill )
        let weaponMod = attacker.getRandomDamageMultiplier()

        let penaltyMod = 1
        if ( target.isAttackable()
                && !target.isRaid()
                && !target.isRaidMinion()
                && ( target.getLevel() >= ConfigManager.npc.getMinNPCLevelForDamagePenalty() )
                && attacker.getActingPlayer()
                && ( ( target.getLevel() - attacker.getActingPlayer().getLevel() ) >= 2 ) ) {

            let levelDifference = target.getLevel() - attacker.getActingPlayer().getLevel() - 1
            if ( levelDifference >= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences().length ) {
                penaltyMod *= _.last( ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences() )
            } else {
                penaltyMod *= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences()[ levelDifference ]
            }
        }
        damage = ( baseMod * criticalMod * criticalModPos * criticalVulnMod * proximityBonus * pvpBonus ) + criticalAddMod + criticalAddVuln
        damage *= weaponTraitMod
        damage *= generalTraitMod
        damage *= attributeMod
        damage *= weaponMod
        damage *= penaltyMod

        return Math.max( damage, 1 )
    },

    calculateMagicAffected( attacker: L2Character, target: L2Character, skill: Skill ) {
        if ( skill.isDebuff() && target.isDebuffBlocked() ) {
            return false
        }

        let defenceModifier = 0
        if ( skill.isActive() && skill.isBad() ) {
            defenceModifier = target.getMagicDefence( attacker, skill )
        }

        let attackModifier = 2 * attacker.getMagicAttack( target, skill ) * Formulas.calculateGeneralTraitBonus( attacker, target, skill.getTraitType(), false )
        let deference = ( attackModifier - defenceModifier ) / ( attackModifier + defenceModifier )

        deference += 0.5 * Math.random()
        return deference > 0
    },

    calculateManaDamage( attacker: L2Character, target: L2Character, skill: Skill, shieldUse: number, useSpiritshot: boolean, useBlessed: boolean, magicCrit: boolean, power: number ): number {
        /*
            Main formula : (SQR(M.Atk)*Power*(Target Max MP/97))/M.Def
         */
        let magicAttack = attacker.getMagicAttack( target, skill )
        let magicDefence = target.getMagicDefence( attacker, skill )
        let maxMp = target.getMaxMp()

        switch ( shieldUse ) {
            case SHIELD_DEFENSE_SUCCEED:
                magicDefence += target.getShieldDefence()
                break
            case SHIELD_DEFENSE_PERFECT_BLOCK:
                return 1
        }

        magicAttack *= useBlessed ? 4 : useSpiritshot ? 2 : 1

        let damage = ( Math.sqrt( magicAttack ) * power * ( maxMp / 97 ) ) / magicDefence
        damage *= Formulas.calculateGeneralTraitBonus( attacker, target, skill.getTraitType(), false )

        if ( target.isAttackable() ) {
            if ( !target.isRaid()
                    && !target.isRaidMinion()
                    && ( target.getLevel() >= ConfigManager.npc.getMinNPCLevelForDamagePenalty() )
                    && attacker.getActingPlayer()
                    && ( ( target.getLevel() - attacker.getActingPlayer().getLevel() ) >= 2 ) ) {

                let lvlDiff = target.getLevel() - attacker.getActingPlayer().getLevel() - 1
                if ( lvlDiff >= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences().length ) {
                    damage *= _.last( ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences() )
                } else {
                    damage *= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences()[ lvlDiff ]
                }
            }
        }

        if ( ConfigManager.character.magicFailures() && !Formulas.calculateMagicSuccess( attacker, target, skill ) ) {
            if ( attacker.isPlayer() ) {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.DAMAGE_DECREASED_BECAUSE_C1_RESISTED_C2_MAGIC )
                        .addCharacterName( target )
                        .addCharacterName( attacker )
                        .getBuffer()
                attacker.sendOwnedData( packet )
                damage /= 2
            }

            if ( target.isPlayer() ) {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_WEAKLY_RESISTED_C2_MAGIC )
                        .addCharacterName( target )
                        .addCharacterName( attacker )
                        .getBuffer()
                target.sendOwnedData( packet )
            }
        }

        if ( magicCrit ) {
            damage *= 3
            attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CRITICAL_HIT_MAGIC ) )
        }

        return damage
    },

    calculateSkillPhysicalDamage( attacker: L2Character, target: L2Character, skill: Skill, shieldUse: number, crit: boolean, ss: boolean, power: number ): number {
        let defence = target.getPowerDefence( attacker )

        switch ( shieldUse ) {
            case SHIELD_DEFENSE_SUCCEED:
                if ( !ConfigManager.character.shieldBlocks() ) {
                    defence += target.getShieldDefence()
                }
                break
            case SHIELD_DEFENSE_PERFECT_BLOCK:
                return 1.0
        }

        let isPvP = attacker.isPlayable() && target.isPlayable()
        let proximityBonus = attacker.isBehindTarget() ? 1.2 : attacker.isInFrontOfTarget() ? 1 : 1.1 // Behind: +20% - Side: +10% (TODO: values are unconfirmed, possibly custom, remove or update when confirmed)
        let damage
        let ssBoost = ss ? 2 : 1
        let pvpBonus = 1

        if ( isPvP ) {
            pvpBonus = attacker.getStat().calculateStat( Stats.PVP_PHYS_SKILL_DMG, 1.0 )
            defence *= target.getStat().calculateStat( Stats.PVP_PHYS_SKILL_DEF, 1.0 )
        }

        let baseMod = ( ( 77 * ( power + ( attacker.getPowerAttack( target ) * ssBoost ) ) ) / defence )

        let penaltyMod = 1
        if ( ( target.isAttackable() )
                && !target.isRaid()
                && !target.isRaidMinion()
                && ( target.getLevel() >= ConfigManager.npc.getMinNPCLevelForDamagePenalty() )
                && attacker.getActingPlayer()
                && ( ( target.getLevel() - attacker.getActingPlayer().getLevel() ) >= 2 ) ) {

            let levelDifference = target.getLevel() - attacker.getActingPlayer().getLevel() - 1
            if ( levelDifference >= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences().length ) {
                penaltyMod *= _.last( ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences() )
            } else {
                penaltyMod *= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences()[ levelDifference ]
            }

            if ( crit ) {
                if ( levelDifference >= ConfigManager.npc.getCritDamagePenaltyForLevelDifferences().length ) {
                    penaltyMod *= _.last( ConfigManager.npc.getCritDamagePenaltyForLevelDifferences() )
                } else {
                    penaltyMod *= ConfigManager.npc.getCritDamagePenaltyForLevelDifferences()[ levelDifference ]
                }
            } else {
                if ( levelDifference >= ConfigManager.npc.getDamagePenaltyForLevelDifferences().length ) {
                    penaltyMod *= _.last( ConfigManager.npc.getDamagePenaltyForLevelDifferences() )
                } else {
                    penaltyMod *= ConfigManager.npc.getDamagePenaltyForLevelDifferences()[ levelDifference ]
                }
            }
        }

        damage = baseMod * proximityBonus * pvpBonus
        damage *= Formulas.calculateAttackTraitBonus( attacker, target )
        damage *= Formulas.calculateAttributeBonus( attacker, target, skill )
        damage *= attacker.getRandomDamageMultiplier()
        damage *= penaltyMod
        damage = attacker.calculateStat( Stats.PHYSICAL_SKILL_POWER, damage )

        return Math.max( damage, 1 )
    },

    calculateAttackTraitBonus( attacker: L2Character, target: L2Character ) {
        let weaponTraitBonus = Formulas.calculateWeaponTraitBonus( attacker, target )
        if ( weaponTraitBonus === 0 ) {
            return 0
        }

        let weaknessBonus = 1.0
        _.each( TraitTypeValues, ( type: TraitType ) => {
            if ( type.type === 2 ) {
                weaknessBonus *= Formulas.calculateGeneralTraitBonus( attacker, target, type, true )
                if ( weaknessBonus === 0 ) {
                    return false
                }
            }
        } )

        if ( weaknessBonus === 0 ) {
            return 0
        }

        return _.clamp( ( weaponTraitBonus * weaknessBonus ), 0.05, 2.0 )
    },

    calculateSkillResurrectRestorePercent( basePower: number, caster: L2Character ) {
        if ( ( basePower === 0 ) || ( basePower === 100 ) ) {
            return basePower
        }

        let restorePercent = basePower * BaseStats.WIT( caster )
        if ( ( restorePercent - basePower ) > 20.0 ) {
            restorePercent += 20.0
        }

        restorePercent = Math.max( restorePercent, basePower )
        restorePercent = Math.min( restorePercent, 90.0 )

        return restorePercent
    },

    calculateStealEffects( attacker: L2Character, target: L2Character, skill: Skill, slot: DispelCategory, rate: number, max: number ): Array<BuffProperties> {
        if ( slot === DispelCategory.BUFF ) {
            let buffs: Array<BuffProperties> = []
            let cancelMagicLvl = skill.getMagicLevel()

            let addBuff = ( info: BuffInfo ) => {
                if ( buffs.length >= max ) {
                    return false
                }

                if ( !info.getSkill().canBeStolen() || info.getRemainingMs() < 1000 ) {
                    return
                }

                if ( Formulas.calculateCancelSuccess( info, cancelMagicLvl, rate, skill ) ) {
                    buffs.push( info.getProperties() )
                }
            }

            _.each( target.getEffectList().getBuffs(), addBuff )
            _.each( target.getEffectList().getDances(), addBuff )
            _.each( target.getEffectList().getTriggered(), addBuff )

            return buffs
        }

        if ( slot === DispelCategory.DEBUFF ) {
            let debuffs: Array<BuffProperties> = []

            _.each( target.getEffectList().getDebuffs(), ( info: BuffInfo ) => {
                if ( info.getSkill().isDebuff() && !info.getSkill().isIrreplaceableBuff() && _.random( 100 ) <= rate ) {
                    debuffs.push( info.getProperties() )
                }

                if ( debuffs.length >= max ) {
                    return false
                }
            } )

            return debuffs
        }

        return []
    },

    calculateCubicSkillSuccess( attacker: L2CubicInstance, target: L2Character, skill: Skill, shieldUse: number ): boolean {
        if ( skill.isDebuff() && target.isDebuffBlocked() ) {
            return false
        }

        if ( shieldUse === SHIELD_DEFENSE_PERFECT_BLOCK ) {
            return false
        }

        if ( skill.hasDebuffReflection() && Formulas.calculateBuffDebuffReflection( target, skill ) ) {
            return false
        }

        let baseRate = skill.getActivateRate()
        let statModifier = skill.getBasicProperty()( target )
        let rate = ( baseRate / statModifier )

        let resistanceModifier = Formulas.calculateGeneralTraitBonus( attacker.getOwner(), target, skill.getTraitType(), false )
        rate *= resistanceModifier

        let levelModifier = Formulas.calculateLevelBonusModifier( attacker.getOwner(), target, skill )
        rate *= levelModifier

        let elementModifier = Formulas.calculateAttributeBonus( attacker.getOwner(), target, skill )
        rate *= elementModifier

        // TODO : Find out Matk/Mdef Bonus

        let finalRate = _.clamp( rate, skill.getMinChance(), skill.getMaxChance() )
        return _.random( 100 ) < finalRate
    },

    calculateCubicMagicDamage( attacker: L2CubicInstance, target: L2Character, skill: Skill, magicCrit: boolean, shieldUse: number ): number {
        let owner: L2PcInstance = attacker.getOwner()
        let mDef = target.getMagicDefence( owner, skill )
        switch ( shieldUse ) {
            case SHIELD_DEFENSE_SUCCEED:
                mDef += target.getShieldDefence()
                break
            case SHIELD_DEFENSE_PERFECT_BLOCK:
                return 1
        }

        let damage = ( 91 * attacker.getCubicPower() ) / mDef

        if ( ConfigManager.character.magicFailures() && !Formulas.calculateMagicSuccess( owner, target, skill ) ) {

            let ownerId = attacker.getOwnerId()
            if ( ( ( target.getLevel() - skill.getMagicLevel() ) <= 9 ) ) {
                if ( skill.hasEffectType( L2EffectType.HpDrain ) ) {
                    PacketDispatcher.sendOwnedData( ownerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.DRAIN_HALF_SUCCESFUL ) )
                } else {
                    PacketDispatcher.sendOwnedData( ownerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACK_FAILED ) )
                }
                damage /= 2
            } else {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_RESISTED_YOUR_S2 )
                        .addCharacterName( target )
                        .addSkillName( skill )
                        .getBuffer()

                PacketDispatcher.sendOwnedData( ownerId, packet )
                damage = 1
            }

            if ( target.isPlayer() ) {
                if ( skill.hasEffectType( L2EffectType.HpDrain ) ) {
                    let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.RESISTED_C1_DRAIN )
                            .addCharacterName( owner )
                            .getBuffer()

                    target.sendOwnedData( packet )
                } else {
                    let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.RESISTED_C1_MAGIC )
                            .addCharacterName( owner )
                            .getBuffer()

                    target.sendOwnedData( packet )
                }
            }
        } else if ( magicCrit ) {
            damage *= 3
        }

        damage *= Formulas.calculateAttributeBonus( owner, target, skill )

        if ( target.isAttackable() ) {
            if ( !target.isRaid()
                    && !target.isRaidMinion()
                    && ( target.getLevel() >= ConfigManager.npc.getMinNPCLevelForDamagePenalty() )
                    && ( ( target.getLevel() - attacker.getOwner().getLevel() ) >= 2 ) ) {
                let lvlDiff = target.getLevel() - attacker.getOwner().getLevel() - 1
                if ( lvlDiff >= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences().length ) {
                    damage *= _.last( ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences() )
                } else {
                    damage *= ConfigManager.npc.getSkillDamagePenaltyForLevelDifferences()[ lvlDiff ]
                }
            }
        }

        return damage
    },

    calculateCastTime( character: L2Character, skill: Skill ) {
        let skillAnimTime = skill.getHitTime()
        if ( !skill.isChanneling() || skill.getChannelingSkillId() === 0 ) {

            if ( !skill.isStatic() ) {
                let speed = skill.isMagic() ? character.getMagicAttackSpeed() : character.getPowerAttackSpeed()
                skillAnimTime = ( skillAnimTime / speed ) * 333
            }

            if ( skill.isMagic() && ( character.isChargedShot( ShotType.Spiritshot ) || character.isChargedShot( ShotType.BlessedSpiritshot ) ) ) {
                skillAnimTime = skillAnimTime * 0.6
            }

            if ( ( skillAnimTime < 500 ) && ( skill.getHitTime() > 500 ) ) {
                skillAnimTime = 500
            }
        }

        return skillAnimTime
    },

    calculateFallDamage( character: L2Character, fallHeight: number ): number {
        if ( !ConfigManager.general.enableFallingDamage() || ( fallHeight < 0 ) ) {
            return 0
        }

        return character.calculateStat( Stats.FALL, ( fallHeight * character.getMaxHp() ) / 1000.0, null, null )
    },

    calculateCpRegeneration( player: L2PcInstance ): number {
        let init = player.getTemplate().getBaseCpRegeneration( player.getLevel() ) * player.getLevelModifier() * BaseStats.CON( player )
        let multiplier = ConfigManager.character.getCpRegenMultiplier() * player.getClanFunctionMultiplier( ClanHallFunctionType.RestoreCP )

        if ( player.isSitting() ) {
            multiplier *= 1.5

        } else if ( !player.isMoving() ) {
            multiplier *= 1.1

        } else if ( player.isRunning() ) {
            multiplier *= 0.7
        }

        return player.calculateStat( Stats.REGENERATE_CP_RATE, Math.max( 1, init ), null, null ) * multiplier
    },

    calculateHpRegeneration( character: L2Character ): number {
        let startValue = character.isPlayer() ? character.getActingPlayer().getTemplate().getBaseHpRegeneration( character.getLevel() ) : character.getTemplate().getHpRegeneration()
        let multiplier = character.isRaid() ? ConfigManager.npc.getRaidHpRegenerationMultiplier() : ConfigManager.character.getHpRegenerationMultiplier()
        let bonus = 0

        if ( ConfigManager.customs.championEnable() && character.isChampion() ) {
            multiplier *= ConfigManager.customs.getChampionHpRegen()
        }

        if ( character.isPlayer() ) {
            let player: L2PcInstance = character.getActingPlayer()

            let siegeModifier = Formulas.calculateSiegeRegenerationModifier( player )
            if ( siegeModifier > 0 ) {
                multiplier *= siegeModifier
            }

            multiplier = multiplier
                    * player.getClanFunctionMultiplier( ClanHallFunctionType.RestoreHp )
                    * player.getCastleFunctionMultiplier( CastleFunctions.RegenerateHp )
                    * player.getFortFunctionMultiplier( FortFunctionType.RestoreHp )

            if ( player.isInArea( AreaType.MotherTree ) ) {
                let area = AreaDiscoveryManager.getEnteredAreaByType( player.getObjectId(), AreaType.MotherTree ) as MotherTreeArea
                let regenerationBonus = !area ? 0 : area.properties.hpBonus
                bonus += regenerationBonus
            }

            if ( player.isSitting() ) {
                multiplier *= 1.5

            } else if ( !player.isMoving() ) {
                multiplier *= 1.1

            } else if ( player.isRunning() ) {
                multiplier *= 0.7
            }

            startValue *= character.getLevelModifier() * BaseStats.CON( character )
        } else if ( character.isPet() ) {
            startValue = ( character as L2PetInstance ).getPetLevelData().getPetHPRegeneration() * ConfigManager.npc.getPetHpRegenerationMultiplier()
        }

        return character.calculateStat( Stats.REGENERATE_HP_RATE, Math.max( 1, startValue ), null, null ) * multiplier + bonus
    },

    calculateSiegeRegenerationModifier( player: L2PcInstance ): number {
        if ( !player || !player.getClan() ) {
            return 0
        }

        let siege: Siege = SiegeManager.getSiege( player.getX(), player.getY(), player.getZ() )
        if ( !siege || !siege.isInProgress() ) {
            return 0
        }

        let siegeClan: L2SiegeClan = siege.getAttackerClanById( player.getClan().getId() )
        if ( !siegeClan || _.isEmpty( siegeClan.getFlag() ) || !GeneralHelper.checkIfInRange( 200, player, L2World.getObjectById( siegeClan.getFlag()[ 0 ] ) as L2Npc, true ) ) {
            return 0
        }

        return 1.5
    },

    calculateMpRegeneration( character: L2Character ): number {
        let startValue = character.isPlayer() ? character.getActingPlayer().getTemplate().getBaseMpRegeneration( character.getLevel() ) : character.getTemplate().getMpRegeneration()
        let multiplier = character.isRaid() ? ConfigManager.npc.getRaidMpRegenerationMultiplier() : ConfigManager.character.getMpRegenerationMultiplier()
        let bonus = 0

        if ( character.isPlayer() ) {
            let player: L2PcInstance = character as L2PcInstance

            multiplier = multiplier
                    * player.getClanFunctionMultiplier( ClanHallFunctionType.RestoreMp )
                    * player.getCastleFunctionMultiplier( CastleFunctions.RegenerateMp )
                    * player.getFortFunctionMultiplier( FortFunctionType.RestoreMp )

            if ( player.isInArea( AreaType.MotherTree ) ) {
                let area = AreaDiscoveryManager.getEnteredAreaByType( player.getObjectId(), AreaType.MotherTree ) as MotherTreeArea
                let regenerationBonus = !area ? 0 : area.properties.mpBonus
                bonus += regenerationBonus
            }

            if ( player.isSitting() ) {
                multiplier *= 1.5
            } else if ( !player.isMoving() ) {
                multiplier *= 1.1
            } else if ( player.isRunning() ) {
                multiplier *= 0.7
            }

            startValue *= character.getLevelModifier() * BaseStats.MEN( character )
        } else if ( character.isPet() ) {
            startValue = ( character as L2PetInstance ).getPetLevelData().getPetMPRegeneration() * ConfigManager.npc.getPetMpRegenerationMultiplier()
        }

        return character.calculateStat( Stats.REGENERATE_MP_RATE, Math.max( 1, startValue ), null, null ) * multiplier + bonus
    },

    calculateHitMiss( attacker: L2Character, target: L2Character ): boolean {
        let chance = ( 80 + ( 2 * ( attacker.getAccuracy() - target.getEvasionRate( attacker ) ) ) ) * 10

        chance *= HitConditionBonusManager.getBonus( attacker, target )

        chance = Math.max( chance, 200 )
        chance = Math.min( chance, 980 )

        return chance < _.random( 1000 )
    },

    calculateCrit( attacker: L2Character, target: L2Character ): boolean {
        let rate = attacker.getStat().calculateStat( Stats.CRITICAL_RATE_POS, attacker.getStat().getCriticalHit( target, null ) )
        return ( target.getStat().calculateStat( Stats.DEFENCE_CRITICAL_RATE, rate, null, null ) + target.getStat().calculateStat( Stats.DEFENCE_CRITICAL_RATE_ADD, 0, null, null ) ) > _.random( 1000 )
    },

    calculatePhysicalDamage( attacker: L2Character, target: L2Character, shieldUse: number, isCrit: boolean, useShot: boolean ): number {
        let defence = target.getPowerDefence( attacker )

        switch ( shieldUse ) {
            case SHIELD_DEFENSE_SUCCEED:
                if ( !ConfigManager.character.shieldBlocks() ) {
                    defence += target.getShieldDefence()
                }
                break
            case SHIELD_DEFENSE_PERFECT_BLOCK:
                return 1
        }

        let isPvP = attacker.isPlayable() && target.isPlayable()
        let proximityBonus = attacker.isBehindTarget() ? 1.2 : attacker.isInFrontOfTarget() ? 1 : 1.1 // Behind: +20% - Side: +10%
        let damage = attacker.getPowerAttack( target )
        let ssboost = useShot ? 2 : 1

        if ( isPvP ) {
            defence *= target.calculateStat( Stats.PVP_PHYSICAL_DEF, 1, null, null )
        }

        damage *= ssboost

        if ( isCrit ) {
            damage = 2 * attacker.calculateStat( Stats.CRITICAL_DAMAGE, 1, target, null )
                    * attacker.calculateStat( Stats.CRITICAL_DAMAGE_POS, 1, target, null )
                    * target.calculateStat( Stats.DEFENCE_CRITICAL_DAMAGE, 1, target, null )
                    * ( ( 76 * damage * proximityBonus ) / defence )
            damage += ( ( attacker.calculateStat( Stats.CRITICAL_DAMAGE_ADD, 0, target, null ) * 77 ) / defence )
            damage += target.calculateStat( Stats.DEFENCE_CRITICAL_DAMAGE_ADD, 0, target, null )
        } else {
            damage = ( 76 * damage * proximityBonus ) / defence
        }

        damage *= Formulas.calculateAttackTraitBonus( attacker, target )
        damage *= attacker.getRandomDamageMultiplier()

        if ( isPvP ) {
            damage *= attacker.calculateStat( Stats.PVP_PHYSICAL_DMG, 1, null, null )
        }

        damage *= Formulas.calculateAttributeBonus( attacker, target, null )

        if ( target.isAttackable() ) {
            if ( !target.isRaid()
                    && !target.isRaidMinion()
                    && ( target.getLevel() >= ConfigManager.npc.getMinNPCLevelForDamagePenalty() )
                    && ( attacker.getActingPlayer() )
                    && ( ( target.getLevel() - attacker.getActingPlayer().getLevel() ) >= 2 ) ) {
                let difference = target.getLevel() - attacker.getActingPlayer().getLevel() - 1

                if ( isCrit ) {
                    if ( difference >= ConfigManager.npc.getCritDamagePenaltyForLevelDifferences().length ) {
                        damage *= _.last( ConfigManager.npc.getCritDamagePenaltyForLevelDifferences() )
                    } else {
                        damage *= ConfigManager.npc.getCritDamagePenaltyForLevelDifferences()[ difference ]
                    }
                } else {
                    if ( difference >= ConfigManager.npc.getDamagePenaltyForLevelDifferences().length ) {
                        damage *= _.last( ConfigManager.npc.getDamagePenaltyForLevelDifferences() )
                    } else {
                        damage *= ConfigManager.npc.getDamagePenaltyForLevelDifferences()[ difference ]
                    }
                }
            }
        }

        return Math.max( damage, 1 )
    },

    calculateKarmaGain( count: number, isSummon: boolean ): number {
        let result = 43200

        if ( isSummon ) {
            result = Math.floor( ( ( ( count * 0.375 ) + 1 ) * 60 ) * 4 ) - 150

            if ( result > 10800 ) {
                return 10800
            }
        }

        if ( count < 99 ) {
            return Math.floor( ( ( ( count * 0.5 ) + 1 ) * 60 ) * 12 )
        }

        if ( count < 180 ) {
            return Math.floor( ( ( ( count * 0.125 ) + 37.75 ) * 60 ) * 12 )
        }

        return result
    },
}