import { StatFunction } from '../../../enums/StatFunction'
import { Condition } from '../../conditions/Condition'
import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'
import { AbstractFunction } from './AbstractFunction'

export class FunctionTemplate {
    values: FunctionTemplateValues
    method: ( FunctionTemplateValues, any ) => AbstractFunction

    constructor( name: string, stat: string, value: number ) {
        let [ statMethod, order ] = StatFunction[ name.toUpperCase() ]

        this.values = {
            condition: null,
            order,
            stat,
            value,
        }

        this.method = statMethod
    }

    createFunction( caster: L2Character, target: L2Character, skill: Skill, owner: any ): AbstractFunction {
        if ( this.testCondition( caster, target, skill ) ) {
            return null
        }

        return this.generateFunction( owner )
    }

    testCondition( caster: L2Character, target: L2Character, skill: Skill ) : boolean {
        if ( this.values.condition && !this.values.condition.test( caster, target, skill ) ) {
            return false
        }

        return true
    }

    generateFunction( owner: any ) : AbstractFunction {
        return this.method( this.values, owner )
    }
}

// TODO : remove condition since it does not seem to be used and refactor FunctionTemplate.generateFunction
export interface FunctionTemplateValues {
    condition: Condition
    stat: string
    order: number
    value: number
}

export const NullTemplateValues: FunctionTemplateValues = {
    condition: null,
    order: 1,
    stat: null,
    value: 0,
}