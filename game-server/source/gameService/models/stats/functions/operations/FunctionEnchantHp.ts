import { AbstractFunction } from '../AbstractFunction'
import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2ItemInstance } from '../../../items/instance/L2ItemInstance'
import { EnchantItemBonusManager } from '../../../../cache/EnchantItemBonusManager'
import { FunctionTemplateValues } from '../FunctionTemplate'

export class FunctionEnchantHp extends AbstractFunction {
    static fromParameters( values: FunctionTemplateValues, owner: any ): FunctionEnchantHp {
        return new FunctionEnchantHp( values, owner )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        if ( this.getApplyCondition() && !this.getApplyCondition().test( effector, affected, skill ) ) {
            return initialValue
        }

        let item: L2ItemInstance = this.getFunctionOwner() as L2ItemInstance
        if ( item.getEnchantLevel() > 0 ) {
            return initialValue + EnchantItemBonusManager.getHPBonus( item )
        }

        return initialValue
    }
}