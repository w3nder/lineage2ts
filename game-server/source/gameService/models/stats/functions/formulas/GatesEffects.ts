import { Stats } from '../../Stats'
import { BaseStats } from '../../BaseStats'
import { SevenSigns } from '../../../../directives/SevenSigns'
import { AbstractFunction } from '../AbstractFunction'
import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { ConfigManager } from '../../../../../config/ConfigManager'
import { SevenSignsSeal, SevenSignsSide, SevenSignsValues } from '../../../../values/SevenSignsValues'
import { NullTemplateValues } from '../FunctionTemplate'

export class FunctionGatesPowerDefenceModifier extends AbstractFunction {
    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        if ( SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) === SevenSignsSide.Dawn ) {
            return initialValue * ConfigManager.sevenSigns.getDawnGatesPowerDefenceMultiplier()
        }

        if ( SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) === SevenSignsSide.Dusk ) {
            return initialValue * ConfigManager.sevenSigns.getDuskGatesPowerDefenceMultiplier()
        }

        return initialValue * BaseStats.CON( effector )
    }

    getStat(): string {
        return Stats.POWER_DEFENCE
    }
}

export class FunctionGatesMagicDefenceModifier extends AbstractFunction {
    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        if ( SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) === SevenSignsSide.Dawn ) {
            return initialValue * ConfigManager.sevenSigns.getDawnGatesMagicDefenceMultiplier()
        }

        if ( SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) === SevenSignsSide.Dusk ) {
            return initialValue * ConfigManager.sevenSigns.getDuskGatesMagicDefenceMultiplier()
        }

        return initialValue * BaseStats.CON( effector )
    }

    getStat(): string {
        return Stats.MAGIC_DEFENCE
    }
}

export const GatesPowerDefenceModifier: AbstractFunction = new FunctionGatesPowerDefenceModifier()
export const GatesMagicDefenceModifier: AbstractFunction = new FunctionGatesMagicDefenceModifier()