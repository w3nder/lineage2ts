import { Stats } from '../../Stats'
import { BaseStats } from '../../BaseStats'
import { AbstractFunction } from '../AbstractFunction'
import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { NullTemplateValues } from '../FunctionTemplate'

export class FunctionMaxHpMultiplier extends AbstractFunction {

    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( attacker: L2Character, target: L2Character, skill: Skill, initialValue: number ) {
        return initialValue * BaseStats.CON( attacker )
    }

    getStat(): string {
        return Stats.MAX_HP
    }
}

export class FunctionMaxMpMultiplier extends AbstractFunction {
    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        return initialValue * BaseStats.MEN( effector )
    }

    getStat(): string {
        return Stats.MAX_MP
    }
}

export class FunctionMaxCpMultiplier extends AbstractFunction {

    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        return initialValue * BaseStats.CON( effector )
    }

    getStat(): string {
        return Stats.MAX_CP
    }
}

export const MaxHpMultiplier: AbstractFunction = new FunctionMaxHpMultiplier()
export const MaxMpMultiplier: AbstractFunction = new FunctionMaxMpMultiplier()
export const MaxCpMultiplier: AbstractFunction = new FunctionMaxCpMultiplier()