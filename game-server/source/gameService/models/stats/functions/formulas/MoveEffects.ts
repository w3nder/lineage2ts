import { Stats } from '../../Stats'
import { BaseStats } from '../../BaseStats'
import { AbstractFunction } from '../AbstractFunction'
import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { NullTemplateValues } from '../FunctionTemplate'

export class FunctionMoveSpeed extends AbstractFunction {
    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        return initialValue * BaseStats.DEX( effector )
    }

    getStat(): string {
        return Stats.MOVE_SPEED
    }
}

export const MoveSpeed: AbstractFunction = new FunctionMoveSpeed()