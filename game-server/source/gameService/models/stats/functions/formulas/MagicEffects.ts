import { Stats } from '../../Stats'
import { BaseStats } from '../../BaseStats'
import { L2PcInstance } from '../../../actor/instance/L2PcInstance'
import { AbstractFunction } from '../AbstractFunction'
import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { InventorySlot } from '../../../../values/InventoryValues'
import { NullTemplateValues } from '../FunctionTemplate'

const inventorySlots : Array<InventorySlot> = [
    InventorySlot.LeftFinger,
    InventorySlot.RightFinger,
    InventorySlot.LeftEar,
    InventorySlot.RightEar,
    InventorySlot.Neck,
]

const getPlayerValue = ( initialValue: number, player: L2PcInstance ): number => {
    let outcome = initialValue
    let isPlayerTransformed = player.isTransformed()

    inventorySlots.some( ( slot: number ) => {
        if ( !player.getInventory().isPaperdollSlotEmpty( slot ) ) {
            let bestSlot
            if ( isPlayerTransformed ) {
                bestSlot = player.getTransformation().getBaseDefenceBySlot( player, slot )
            } else {
                bestSlot = slot
            }

            outcome -= player.getTemplate().getBaseDefBySlot( bestSlot )

            if ( outcome <= 0 ) {
                outcome = 0
                return true
            }
        }

        return false
    } )

    return outcome
}

export class FunctionMagicDefenceModifier extends AbstractFunction {

    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        let startValue = initialValue
        if ( effector.isPlayer() ) {
            let player = effector.getActingPlayer()
            startValue = getPlayerValue( startValue, player )
        } else if ( effector.isPet() && effector.getInventory().getPaperdollObjectId( InventorySlot.Neck ) ) {
            startValue = startValue - 13
        }

        return startValue * BaseStats.MEN( effector ) * effector.getLevelModifier()
    }

    getStat(): string {
        return Stats.MAGIC_DEFENCE
    }
}

export class FunctionMagicAttackSpeed extends AbstractFunction {
    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        return initialValue * BaseStats.WIT( effector )
    }

    getStat(): string {
        return Stats.MAGIC_ATTACK_SPEED
    }
}

export class FunctionMagicAttackModifier extends AbstractFunction {

    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        let levelModifier = BaseStats.INT( effector.isPlayer() ? effector.getActingPlayer() : effector )
        let initialModifier = effector.isPlayer() ? effector.getActingPlayer().getLevelModifier() : effector.getLevelModifier()
        return initialValue * Math.pow( levelModifier, 2 ) * Math.pow( initialModifier, 2 )
    }

    getStat(): string {
        return Stats.MAGIC_ATTACK
    }
}

export class FunctionMagicAttackCritical extends AbstractFunction {

    constructor() {
        super( NullTemplateValues, null )
    }

    calculate( effector: L2Character, affected: L2Character, skill: Skill, initialValue: number ) {
        if ( !effector.isPlayer() || effector.getActiveWeaponInstance() ) {
            return initialValue * BaseStats.WIT( effector ) * 10
        }

        return initialValue
    }

    getStat(): string {
        return Stats.MCRITICAL_RATE
    }
}

export const MagicDefenceModifier: AbstractFunction = new FunctionMagicDefenceModifier()
export const MagicAttackSpeed: AbstractFunction = new FunctionMagicAttackSpeed()
export const MagicAttackModifier: AbstractFunction = new FunctionMagicAttackModifier()
export const MagicAttackCritical: AbstractFunction = new FunctionMagicAttackCritical()