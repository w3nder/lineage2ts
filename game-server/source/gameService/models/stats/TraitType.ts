export function getTraitType( id: number, type: number ): TraitType {
    return {
        id,
        type,
    }
}

export const TraitTypeValues = {
    NONE: getTraitType( 0, 0 ),
    SWORD: getTraitType( 1, 1 ),
    BLUNT: getTraitType( 2, 1 ),
    DAGGER: getTraitType( 3, 1 ),
    POLE: getTraitType( 4, 1 ),
    FIST: getTraitType( 5, 1 ),
    BOW: getTraitType( 6, 1 ),
    ETC: getTraitType( 7, 1 ),
    UNK_8: getTraitType( 8, 0 ),
    POISON: getTraitType( 9, 3 ),
    HOLD: getTraitType( 10, 3 ),
    BLEED: getTraitType( 11, 3 ),
    SLEEP: getTraitType( 12, 3 ),
    SHOCK: getTraitType( 13, 3 ),
    DERANGEMENT: getTraitType( 14, 3 ),
    BUG_WEAKNESS: getTraitType( 15, 2 ),
    ANIMAL_WEAKNESS: getTraitType( 16, 2 ),
    PLANT_WEAKNESS: getTraitType( 17, 2 ),
    BEAST_WEAKNESS: getTraitType( 18, 2 ),
    DRAGON_WEAKNESS: getTraitType( 19, 2 ),
    PARALYZE: getTraitType( 20, 3 ),
    DUAL: getTraitType( 21, 1 ),
    DUALFIST: getTraitType( 22, 1 ),
    BOSS: getTraitType( 23, 3 ),
    GIANT_WEAKNESS: getTraitType( 24, 2 ),
    CONSTRUCT_WEAKNESS: getTraitType( 25, 2 ),
    DEATH: getTraitType( 26, 3 ),
    VALAKAS: getTraitType( 27, 2 ),
    ANESTHESIA: getTraitType( 28, 2 ),
    CRITICAL_POISON: getTraitType( 29, 3 ),
    ROOT_PHYSICALLY: getTraitType( 30, 3 ),
    ROOT_MAGICALLY: getTraitType( 31, 3 ),
    RAPIER: getTraitType( 32, 1 ),
    CROSSBOW: getTraitType( 33, 1 ),
    ANCIENTSWORD: getTraitType( 34, 1 ),
    TURN_STONE: getTraitType( 35, 3 ),
    GUST: getTraitType( 36, 3 ),
    PHYSICAL_BLOCKADE: getTraitType( 37, 3 ),
    TARGET: getTraitType( 38, 3 ),
    PHYSICAL_WEAKNESS: getTraitType( 39, 3 ),
    MAGICAL_WEAKNESS: getTraitType( 40, 3 ),
    DUALDAGGER: getTraitType( 41, 1 ),
    DEMONIC_WEAKNESS: getTraitType( 42, 2 ), // CT26_P4
    DIVINE_WEAKNESS: getTraitType( 43, 2 ),
    ELEMENTAL_WEAKNESS: getTraitType( 44, 2 ),
    FAIRY_WEAKNESS: getTraitType( 45, 2 ),
    HUMAN_WEAKNESS: getTraitType( 46, 2 ),
    HUMANOID_WEAKNESS: getTraitType( 47, 2 ),
    UNDEAD_WEAKNESS: getTraitType( 48, 2 ),
}

export interface TraitType {
    id: number,
    type: number
}