import { EventTerminationResult, EventTypeData } from '../EventType'
import { ListenerLogic } from '../../ListenerLogic'

export type EventListenerTerminatedMethod = ( data: EventTypeData ) => Promise<EventTerminationResult>
export type EventListenerMethod = ( data: EventTypeData ) => Promise<EventTerminationResult | void | string>
export interface EventListenerData {
    owner: Object | ListenerLogic
    method: EventListenerMethod
    targetIds: Set<number>
}