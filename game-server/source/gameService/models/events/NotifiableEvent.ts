import { EventTerminationResult, EventTypeData } from './EventType'

export interface NotifiableEvent {
    notifyEventPromise( playerId: number, originatorId: number, outcome: Promise< string | void > ): Promise<void>
    notifyEventWithTermination( playerId: number, originatorId: number, method: Function, parameters: EventTypeData ): Promise<EventTerminationResult>
}