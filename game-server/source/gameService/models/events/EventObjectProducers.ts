import {
    AttackableAttackedEvent,
    AttackableKillEvent,
    NpcAttemptsDyingEvent,
    CharacterEnterZoneEvent,
    CharacterKilledEvent,
    CharacterReceivedDamageEvent,
    CharacterStoppedMovingEvent,
    CharacterTeleportedEvent,
    CreatureAvoidAttackEvent,
    CreatureUsesSkillEvent,
    EventType,
    EventTypeData,
    ItemAuctionBidEvent,
    ItemBypassEvent,
    ItemCreateEvent,
    ItemEndOfLifeEvent,
    ItemTalkingEvent,
    NpcApproachedForTalkEvent,
    NpcBypassEvent,
    NpcGeneralEvent,
    NpcManorBypassEvent,
    NpcRouteNodeArrivedEvent,
    NpcRouteFinishedEvent,
    NpcReceivingEvent,
    NpcSeePlayerEvent,
    NpcSeeSkillEvent,
    PacketDataViolationEvent,
    PlayableExpChangedEvent,
    PlayerAddHennaEvent,
    PlayerAddItemEvent,
    PlayerAddItemToClanWarehouseEvent,
    PlayerAugmentItemEvent,
    PlayerBypassEvent,
    PlayerCanDestroyItemEvent,
    PlayerCanDropItemEvent,
    PlayerChangedLevelEvent,
    PlayerChatEvent,
    PlayerClanJoinEvent,
    PlayerClanLeaderChangeEvent,
    PlayerClanLeaderScheduledEvent,
    PlayerClanLevelUpEvent,
    PlayerCreatedClanEvent,
    PlayerDestroyItemEvent,
    PlayerDestroyItemInClanWarehouseEvent,
    PlayerDestroysClanEvent,
    PlayerDieEvent,
    PlayerDropItemEvent,
    PlayerEquipItemEvent,
    PlayerKarmaChangedEvent,
    PlayerKilledEvent,
    PlayerLearnSkillEvent,
    PlayerLeftClanEvent,
    PlayerLoggedOutEvent,
    PlayerLoginEvent,
    PlayerPartyRequestEvent,
    PlayerPickupItemEvent,
    PlayerPKChangedEvent,
    PlayerPrivateChatEvent,
    PlayerReceivesTradeItemEvent,
    PlayerRemoveHennaEvent,
    PlayerSelectCharacterEvent,
    PlayerSitEvent,
    PlayerStandEvent,
    PlayerStartCreateClanEvent,
    PlayerStartDestroyClanEvent,
    PlayerStartsLogoutEvent,
    PlayerStoppedMovingEvent,
    PlayerSummonTalkEvent,
    PlayerTransferItemEvent,
    PlayerTransferItemFromClanWarehouseEvent,
    PlayerTutorialClientEvent,
    PlayerTutorialCommandEvent,
    PlayerTutorialLinkEvent,
    PlayerTutorialQuestionMarkEvent,
    VoiceCommandEvent,
    PlayableKilledEvent,
    ClanChangeLevelEvent,
    TWPlayerLoginEvent,
    TWPlayerDeathEvent,
    AttackableInitialAttackEvent,
    PlayerCreatedEvent, NpcSpawnEvent, NpcTeleportEvent,
} from './EventType'

export type EventObjectFactory = () => EventTypeData
export const SupportedEventTypes: Record<number, EventObjectFactory> = {
    [ EventType.AttackableAttacked ]: (): AttackableAttackedEvent => {
        return {
            attackerId: 0,
            attackerInstanceType: 0,
            targetNpcId: 0,
            attackerPlayerId: 0,
            targetId: 0,
            damage: 0,
            skillId: 0,
            skillLevel: 0,
            isChampion: false,
        }
    },

    [ EventType.AttackableKilled ]: (): AttackableKillEvent => {
        return {
            isChampion: false,
            playerId: 0,
            attackerId: 0,
            targetId: 0,
            npcId: 0,
            instanceId: 0,
        }
    },

    [ EventType.CharacterReceivedDamage ]: (): CharacterReceivedDamageEvent => {
        return {
            attackerId: 0,
            targetId: 0,
            skillId: 0,
            skillLevel: 0,
            isCritical: false,
            isDOT: false,
            isReflected: false,
            damage: 0,
        }
    },

    [ EventType.CharacterKilled ]: (): CharacterKilledEvent => {
        return {
            attackerId: 0,
            targetId: 0,
        }
    },

    [ EventType.CharacterEnterArea ]: (): CharacterEnterZoneEvent => {
        return {
            characterId: 0,
            areaId: 0,
            areaType: 0,
            characterInstanceType: 0,
        }
    },

    [ EventType.NpcSeePlayer ]: (): NpcSeePlayerEvent => {
        return {
            npcId: 0,
            npcObjectId: 0,
            playerId: 0,
        }
    },

    [ EventType.NpcApproachedForTalk ]: (): NpcApproachedForTalkEvent => {
        return {
            characterId: 0,
            playerId: 0,
            characterNpcId: 0,
        }
    },

    [ EventType.NpcRouteNodeArrived ]: (): NpcRouteNodeArrivedEvent => {
        return {
            routeName: undefined,
            nodeIndex: 0,
            characterId: 0
        }
    },

    [ EventType.NpcRouteFinished ]: (): NpcRouteFinishedEvent => {
        return {
            routeName: undefined,
            characterId: 0,
        }
    },

    [ EventType.NpcSeeSkill ]: (): NpcSeeSkillEvent => {
        return {
            originatorId: 0,
            targetIds: null,
            playerId: 0,
            receiverId: 0,
            receiverNpcId: 0,
            receiverIsChampion: false,
            skillId: 0,
            skillLevel: 0,
        }
    },

    [ EventType.NpcEvent ]: (): NpcGeneralEvent => {
        return {
            eventName: null,
            characterId: 0,
            playerId: 0,
            characterNpcId: 0,
            isTimer: false
        }
    },

    [ EventType.PlayerAddItem ]: (): PlayerAddItemEvent => {
        return {
            amount: 0,
            itemTemplateId: 0,
            playerId: 0,
            itemObjectId: 0,
            reason: undefined,
            previousOwnerId: 0
        }
    },

    [ EventType.PlayerPickupItem ]: (): PlayerPickupItemEvent => {
        return {
            playerId: 0,
            itemObjectId: 0,
            amount: 0,
            itemId: 0,
        }
    },

    [ EventType.PlayerKilled ]: (): PlayerKilledEvent => {
        return {
            targetClassId: 0,
            attackerLevel: 0,
            targetLevel: 0,
            attackerId: 0,
            targetId: 0,
            attackerInstanceType: 0,
            instanceId: 0
        }
    },

    [ EventType.PlayerDestroyItem ]: (): PlayerDestroyItemEvent => {
        return {
            amount: 0,
            itemObjectId: 0,
            itemTemplateId: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerCanDestroyItem ]: (): PlayerCanDestroyItemEvent => {
        return {
            amount: 0,
            itemObjectId: 0,
            itemTemplateId: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerDropItem ]: (): PlayerDropItemEvent => {
        return {
            amount: 0,
            itemObjectId: 0,
            itemTemplateId: 0,
            locationX: 0,
            locationY: 0,
            locationZ: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerCanDropItem ]: (): PlayerCanDropItemEvent => {
        return {
            amount: 0,
            itemObjectId: 0,
            itemTemplateId: 0,
            locationX: 0,
            locationY: 0,
            locationZ: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerTransferItem ]: (): PlayerTransferItemEvent => {
        return {
            amount: 0,
            itemObjectId: 0,
            itemTemplateId: 0,
            playerId: 0,
            destinationType: undefined,
            destinationPlayerId: 0
        }
    },

    [ EventType.ItemAuctionBid ]: (): ItemAuctionBidEvent => {
        return {
            auctionId: 0,
            bidAmount: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerReceivesTradeItem ]: (): PlayerReceivesTradeItemEvent => {
        return {
            itemAmount: 0,
            itemId: 0,
            type: '',
            receiverId: 0,
            senderId: 0,
        }
    },

    [ EventType.CharacterUsesSkill ]: (): CreatureUsesSkillEvent => {
        return {
            attackerId: 0,
            isSimultaneous: false,
            mainTargetId: 0,
            skillId: 0,
            skillLevel: 0,
            targetIds: undefined,
        }
    },

    [ EventType.PlayerChat ]: (): PlayerChatEvent => {
        return {
            playerId: 0,
            targetId: 0,
            text: '',
            type: 0,
        }
    },

    [ EventType.PlayerBypass ]: (): PlayerBypassEvent => {
        return {
            bypassOriginId: 0,
            command: '',
            playerId: 0,
        }
    },

    [ EventType.PlayableExpChanged ]: (): PlayableExpChangedEvent => {
        return {
            characterId: 0,
            currentLevel: 0,
            newValue: 0,
            oldValue: 0,
        }
    },

    [ EventType.PlayerChangedLevel ]: (): PlayerChangedLevelEvent => {
        return {
            newLevelValue: 0,
            oldLevelValue: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerKarmaChanged ]: (): PlayerKarmaChangedEvent => {
        return {
            newKarma: 0,
            oldKarma: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerEquipItem ]: (): PlayerEquipItemEvent => {
        return {
            objectId: 0,
            playerId: 0,
            itemId: 0,
        }
    },

    [ EventType.CharacterTeleported ]: (): CharacterTeleportedEvent => {
        return {
            characterId: 0,
            characterNpcId: 0,
        }
    },

    [ EventType.NpcReceivingEvent ]: (): NpcReceivingEvent => {
        return {
            eventName: '',
            receiverId: 0,
            referenceId: 0,
            senderId: 0,
        }
    },

    [ EventType.PlayerAugmentItem ]: (): PlayerAugmentItemEvent => {
        return {
            isAugmented: false,
            objectId: 0,
            playerId: 0,
            itemId: 0,
        }
    },

    [ EventType.ItemBypass ]: (): ItemBypassEvent => {
        return {
            eventName: '',
            itemId: 0,
            objectId: 0,
            playerId: 0,
            isArmor: false,
            isEtc: false,
            isWeapon: false,
        }
    },

    [ EventType.ItemTalking ]: (): ItemTalkingEvent => {
        return {
            isArmor: false,
            isEtc: false,
            isWeapon: false,
            itemId: 0,
            objectId: 0,
            playerId: 0,
        }
    },

    [ EventType.ItemEndOfLife ]: (): ItemEndOfLifeEvent => {
        return {
            itemId: 0,
            objectId: 0,
            playerId: 0,
        }
    },

    [ EventType.ItemCreate ]: (): ItemCreateEvent => {
        return {
            amount: 0,
            itemObjectId: 0,
            playerId: 0,
            reason: '',
        }
    },

    [ EventType.PlayerTutorialClient ]: (): PlayerTutorialClientEvent => {
        return {
            eventId: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerPrivateChat ]: (): PlayerPrivateChatEvent => {
        return {
            message: '',
            receiverId: 0,
            receiverName: '',
            senderId: 0,
        }
    },

    [ EventType.PlayerTutorialLink ]: (): PlayerTutorialLinkEvent => {
        return {
            command: '',
            playerId: 0,
        }
    },

    [ EventType.PlayerTutorialCommand ]: (): PlayerTutorialCommandEvent => {
        return {
            command: '',
            playerId: 0,
        }
    },

    [ EventType.PlayerTutorialQuestionMark ]: (): PlayerTutorialQuestionMarkEvent => {
        return {
            playerId: 0,
            responseId: 0,
        }
    },

    [ EventType.PlayerStoppedMoving ]: (): PlayerStoppedMovingEvent => {
        return {
            instanceId: 0,
            playerId: 0,
            x: 0,
            y: 0,
            z: 0,
        }
    },

    [ EventType.CharacterStoppedMoving ]: (): CharacterStoppedMovingEvent => {
        return {
            characterId: 0,
            instanceId: 0,
            npcId: 0,
            x: 0,
            y: 0,
            z: 0,
        }
    },

    [ EventType.VoiceCommand ]: (): VoiceCommandEvent => {
        return {
            command: '',
            parameters: '',
            playerId: 0,
        }
    },

    [ EventType.PlayerLearnSkill ]: (): PlayerLearnSkillEvent => {
        return {
            playerId: 0,
            skillId: 0,
            skillLevel: 0,
            trainerObjectId: 0,
            type: undefined,
        }
    },

    [ EventType.CreatureAvoidAttack ]: (): CreatureAvoidAttackEvent => {
        return {
            attackerId: 0,
            isDOT: false,
            targetId: 0,
        }
    },

    [ EventType.PlayerSummonTalk ]: (): PlayerSummonTalkEvent => {
        return {
            playerId: 0,
            shouldInteract: false,
            targetId: 0,
        }
    },

    [ EventType.PlayerStartsLogout ]: (): PlayerStartsLogoutEvent => {
        return {
            instanceId: 0,
            playerId: 0,
            x: 0,
            y: 0,
            z: 0,
        }
    },

    [ EventType.PlayerLoggedOut ]: (): PlayerLoggedOutEvent => {
        return {
            sessionDurationMs: 0,
            playerId: 0,
            playerName: undefined
        }
    },

    [ EventType.PlayerStandup ]: (): PlayerStandEvent => {
        return {
            playerId: 0,
        }
    },

    [ EventType.PlayerSit ]: (): PlayerSitEvent => {
        return {
            playerId: 0,
        }
    },

    [ EventType.PlayerJoinsClan ]: (): PlayerClanJoinEvent => {
        return {
            clanLevel: 0,
            clanId: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerClanLevelup ]: (): PlayerClanLevelUpEvent => {
        return {
            clanId: 0,
            newLevel: 0,
            oldLevel: 0,
        }
    },

    [ EventType.PlayerLeavesClan ]: (): PlayerLeftClanEvent => {
        return {
            clanId: 0,
            clanLevel: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerClanLeaderChange ]: (): PlayerClanLeaderChangeEvent => {
        return {
            clanId: 0,
            newLeaderId: 0,
            oldLeaderId: 0,
        }
    },

    [ EventType.PlayerClanLeaderChangeScheduled ]: (): PlayerClanLeaderScheduledEvent => {
        return {
            clanId: 0,
            newLeaderId: 0,
            oldLeaderId: 0,
        }
    },

    [ EventType.PlayerAddHenna ]: (): PlayerAddHennaEvent => {
        return {
            dyeId: 0,
            classId: 0,
            dyeItemId: 0,
            playerId: 0
        }
    },

    [ EventType.PlayerStartCreateClan ]: (): PlayerStartCreateClanEvent => {
        return {
            clanName: undefined,
            playerId: 0,
        }
    },

    [ EventType.PlayerCreatedClan ]: (): PlayerCreatedClanEvent => {
        return {
            clanName: undefined,
            clanId: 0,
            leaderId: 0,
        }
    },

    [ EventType.PlayerStartDestroyClan ]: (): PlayerStartDestroyClanEvent => {
        return {
            clanId: 0,
            clanLevel: 0,
            clanName: undefined,
            clanSize: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerDestroyedClan ]: (): PlayerDestroysClanEvent => {
        return {
            clanId: 0,
            clanLevel: 0,
            clanName: undefined,
            clanSize: 0,
            playerId: 0
        }
    },

    [ EventType.NpcAttemptsDying ]: (): NpcAttemptsDyingEvent => {
        return {
            currentHp: 0,
            objectId: 0,
            reduceHpAmount: 0,
            npcId: 0
        }
    },

    [ EventType.PlayerAttemptsDying ]: (): PlayerDieEvent => {
        return {
            currentHp: 0,
            objectId: 0,
            reduceHpAmount: 0,
        }
    },

    [ EventType.PlayerSelectCharacter ]: (): PlayerSelectCharacterEvent => {
        return {
            accountName: undefined,
            slot: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerPartyRequest ]: (): PlayerPartyRequestEvent => {
        return {
            partyType: 0,
            requesterId: 0,
            targetId: 0,
        }
    },

    [ EventType.NpcManorBypass ]: (): NpcManorBypassEvent => {
        return {
            characterId: 0,
            isNextPeriod: false,
            manorId: 0,
            playerId: 0,
            requestId: 0,
        }
    },

    [ EventType.PlayerRemoveHenna ]: (): PlayerRemoveHennaEvent => {
        return {
            cancelCount: 0,
            cancelFee: 0,
            dyeId: 0,
            dyeItemId: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerPKAmountChanged ]: (): PlayerPKChangedEvent => {
        return {
            newValue: 0,
            oldValue: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerLogin ]: (): PlayerLoginEvent => {
        return {
            level: 0,
            race: 0,
            playerId: 0,
            classId: 0,
        }
    },

    [ EventType.NpcBypass ]: (): NpcBypassEvent => {
        return {
            isBusy: false,
            command: undefined,
            instanceType: 0,
            npcId: 0,
            originatorId: 0,
            playerId: 0,
        }
    },

    [ EventType.PlayerAddItemToClanWarehouse ]: (): PlayerAddItemToClanWarehouseEvent => {
        return {
            amount: 0,
            clanId: 0,
            itemObjectId: 0,
            itemTemplateId: 0,
            playerId: 0,
            reason: undefined,
        }
    },

    [ EventType.PlayerDestroyItemInClanWarehouse ]: (): PlayerDestroyItemInClanWarehouseEvent => {
        return {
            amount: 0,
            clanId: 0,
            itemObjectId: 0,
            itemTemplateId: 0,
            playerId: 0,
            reason: undefined,
        }
    },

    [ EventType.PlayerTransferItemFromClanWarehouse ]: (): PlayerTransferItemFromClanWarehouseEvent => {
        return {
            amount: 0,
            clanId: 0,
            itemObjectId: 0,
            itemTemplateId: 0,
            playerId: 0,
            reason: undefined,
        }
    },

    [ EventType.PacketDataViolation ]: () : PacketDataViolationEvent => {
        return {
            errorId: undefined,
            message: undefined,
            packetName: undefined,
            playerId: 0,
            values: undefined
        }
    },

    [ EventType.PlayableKilled ]: () : PlayableKilledEvent => {
        return {
            attackerId: 0,
            isPlayer: false,
            targetId: 0
        }
    },

    [ EventType.ClanChangeLevel ]: (): ClanChangeLevelEvent => {
        return {
            clanId: 0,
            nextLevel: 0,
            previousLevel: 0
        }
    },

    [ EventType.TWPlayerLogin ]: () : TWPlayerLoginEvent => {
        return {
            classId: 0,
            playerId: 0,
            territoryId: 0
        }
    },

    [ EventType.TWPlayerDeath ]: () : TWPlayerDeathEvent => {
        return {
            attackerId: 0,
            partyPlayerIds: undefined,
            targetClassId: 0,
            targetId: 0,
            territoryId: 0
        }
    },

    [ EventType.AttackableInitialAttack ]: () : AttackableInitialAttackEvent => {
        return {
            attackerId: 0,
            attackerInstanceType: 0,
            targetId: 0,
            targetNpcId: 0
        }
    },

    [ EventType.PlayerCreated ]: () : PlayerCreatedEvent => {
        return {
            classId: 0,
            playerId: 0,
            playerName: undefined,
            race: 0
        }
    },

    [ EventType.NpcSpawn ]: () : NpcSpawnEvent => {
        return {
            characterId: 0,
            npcId: 0
        }
    },

    [ EventType.NpcTeleport ]: () : NpcTeleportEvent => {
        return {
            characterId: 0,
            npcId: 0
        }
    }
}