import { PunishmentEffect } from './PunishmentEffect'
import { PunishmentType } from './PunishmentType'
import { DatabaseManager } from '../../../database/manager'
import Timeout = NodeJS.Timeout
import { PunishmentManager } from '../../instancemanager/PunishmentManager'

export class PunishmentRecord {
    id: number = Date.now()
    targetId: string
    effect: PunishmentEffect
    type: PunishmentType
    expirationTime: number
    reason: string
    isStored: boolean
    punishedBy: string
    task: Timeout

    constructor( targetId: string, effect: PunishmentEffect, type: PunishmentType, expirationTime: number, reason: string, punishedBy: string, isStored: boolean ) {
        this.targetId = targetId
        this.effect = effect
        this.type = type
        this.expirationTime = expirationTime

        this.reason = reason
        this.isStored = isStored
        this.punishedBy = punishedBy
    }

    isExpired() {
        return this.expirationTime !== 0 && Date.now() > this.expirationTime
    }

    startPunishmentTask() : void {
        if ( this.expirationTime > 0 ) {
            this.task = setTimeout( this.runPunishmentTask.bind( this ), this.expirationTime - Date.now() )
        }
    }

    stopPunishmentTask() : void {
        if ( this.task ) {
            clearTimeout( this.task )
        }

        this.task = null
    }

    runPunishmentTask() : void {
        PunishmentManager.stopPunishment( this.targetId, this.effect, this.type )
    }
}