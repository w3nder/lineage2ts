export interface BotReport {
    time: number
    reporterId: number
}

export interface BotReporter {
    time: number
    reportPoints: number
}