export enum ItemAuctionState {
    CREATED,
    STARTED,
    FINISHED
}