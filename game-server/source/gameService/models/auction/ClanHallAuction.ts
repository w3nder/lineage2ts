import { DatabaseManager } from '../../../database/manager'
import { ClanCache } from '../../cache/ClanCache'
import { L2Clan } from '../L2Clan'
import { L2lanHallAuctionBidTableData } from '../../../database/interface/ClanHallAuctionBidTableApi'
import { ItemContainer } from '../itemcontainer/ItemContainer'
import { ItemTypes } from '../../values/InventoryValues'
import { AuctionManager } from '../../instancemanager/AuctionManager'
import { ClanHallManager } from '../../instancemanager/ClanHallManager'
import { L2World } from '../../L2World'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { L2ClanHallAuctionItem } from '../../../database/interface/ClanHallAuctionsTableApi'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import aigle from 'aigle'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'
import Timeout = NodeJS.Timeout
import { L2AuctionTemplateTypes } from '../../../data/interface/AuctionTemplateDataApi'
import { ConfigManager } from '../../../config/ConfigManager'

export function getTaxRatio() : number {
    return Math.min( 1, Math.abs( ConfigManager.clanhall.getAuctionTransactionTax() ) )
}

// TODO : move any database update queries into auction manager via debounced update
export class ClanHallAuction {
    highestBidder: L2lanHallAuctionBidTableData
    bidders: { [ key: number ]: L2lanHallAuctionBidTableData } = {}
    autoEndTask: Timeout
    data: L2ClanHallAuctionItem

    static fromClan( id: number, clan: L2Clan, durationMillis: number, startingBid: number, name: string ): ClanHallAuction {
        let auction = new ClanHallAuction()

        auction.data = {
            currentBid: 0,
            endDate: Date.now() + durationMillis,
            id,
            itemId: id,
            itemName: name,
            itemObjectId: 0,
            itemQuantity: 0,
            itemType: L2AuctionTemplateTypes.ClanHall,
            sellerClanName: clan.getName(),
            sellerId: clan.getLeaderId(),
            sellerName: clan.getLeaderName(),
            startingBid

        }

        return auction
    }

    setProperties( data: L2ClanHallAuctionItem ) : void {
        this.data = structuredClone( data )
    }

    getId() {
        return this.data.id
    }

    async cancelBid( bidderId: number ): Promise<void> {
        await DatabaseManager.getAuctionBids().removeAuction( this.getId(), bidderId )
        let currentBidder: L2lanHallAuctionBidTableData = this.bidders[ bidderId ]
        let clanName: string = currentBidder.clanName
        await this.returnAdena( clanName, currentBidder.maxBidAmount, true )

        let clan: L2Clan = ClanCache.getClanByName( clanName )
        if ( clan ) {
            await clan.setAuctionBidAt( 0, true )
        }

        // TODO : why reload all bidders if only one has cancelled bid?
        this.bidders = {}
        return this.loadBids()
    }

    async loadBids(): Promise<void> {
        this.highestBidder = null

        let items: Array<L2lanHallAuctionBidTableData> = await DatabaseManager.getAuctionBids().getBidsByAuctionId( this.getId() )

        if ( items.length > 0 ) {
            this.highestBidder = items[ 0 ]
            for ( const currentItem of items ) {
                this.bidders[ currentItem.bidderId ] = currentItem

                if ( currentItem.maxBidAmount > this.highestBidder.maxBidAmount ) {
                    this.highestBidder = currentItem
                }
            }
        }
    }

    async returnAdena( clanName: string, amount: number, shouldApplyPenalty: boolean ): Promise<void> {
        let quantity = amount
        if ( shouldApplyPenalty ) {
            quantity = Math.floor( quantity *= 0.9 ) // take 10% tax fee if needed
        }

        let clan: L2Clan = ClanCache.getClanByName( clanName )
        if ( !clan ) {
            return
        }

        let warehouse: ItemContainer = clan.getWarehouse()
        if ( !warehouse ) {
            return
        }

        let limit = getMaxAdena() - warehouse.getAdenaAmount()
        quantity = Math.min( quantity, limit )

        await warehouse.addItem( ItemTypes.Adena, quantity, null, 'Auction.returnAdena' )
    }

    startAutoTask( shouldSave: boolean = false ): void {
        let currentTime = Date.now()
        let taskDelay = 0

        if ( this.data.endDate <= currentTime ) {
            this.data.endDate = currentTime + ( 7 * 24 * 3600000 )

            if ( shouldSave ) {
                this.saveAuctionDate()
            }
        } else {
            taskDelay = this.data.endDate - currentTime
        }

        this.autoEndTask = setTimeout( this.endAuction.bind( this ), taskDelay )
    }

    async endAuction(): Promise<void> {
        if ( !this.highestBidder ) {
            if ( this.data.sellerId === 0 ) {
                return this.startAutoTask()
            }

            if ( this.data.sellerId > 0 ) {
                AuctionManager.removeAuction( this.getId() )
                return
            }
        }

        if ( this.data.sellerId > 0 ) {
            await this.returnItem( this.data.sellerClanName, this.highestBidder.maxBidAmount, true )
            await this.returnItem( this.data.sellerClanName, ClanHallManager.getClanHallById( this.data.itemId ).getLeaseAmount(), false )
        }

        await this.deleteAuctionFromDatabase()

        let clan: L2Clan = ClanCache.getClanByName( this.bidders[ this.highestBidder.bidderId ].clanName )

        delete this.bidders[ this.highestBidder.bidderId ]
        this.highestBidder = null

        await clan.setAuctionBidAt( 0, true )
        await this.removeBids()

        ClanHallManager.setOwner( this.data.itemId, clan )
    }

    deleteAuctionFromDatabase(): Promise<void> {
        return DatabaseManager.getClanHallAuctions().removeAuction( this.data.itemId )
    }

    async removeBids(): Promise<void> {
        await DatabaseManager.getAuctionBids().removeByAuctionId( this.getId() )

        let currentAuction = this
        await aigle.resolve( this.bidders ).eachLimit( 10, async ( bidder: L2lanHallAuctionBidTableData ) => {
            let clan: L2Clan = ClanCache.getClanByName( bidder.clanName )

            if ( !clan ) {
                return
            }

            if ( clan.getHideoutId() === 0 ) {
                await currentAuction.returnItem( bidder.clanName, bidder.maxBidAmount, true ) // 10 % tax
            } else {
                let player: L2PcInstance = L2World.getPlayer( bidder.bidderId )
                if ( player ) {
                    // TODO : should this message be sent to whole clan?
                    player.sendMessage( `Congratulation you have won Clan Hall "${ currentAuction.data.itemName }"` )
                }
            }

            await clan.setAuctionBidAt( 0, true )
        } )
    }

    async returnItem( clanName: string, quantity: number, applyPenalty: boolean ): Promise<void> {
        let amount = quantity

        if ( applyPenalty ) {
            amount *= ( 1 - getTaxRatio() )
        }

        let clan: L2Clan = ClanCache.getClanByName( clanName )
        if ( !clan ) {
            return
        }

        let clanWarehouse: ItemContainer = clan.getWarehouse()
        if ( !clanWarehouse ) {
            return
        }

        let limit = getMaxAdena() - clanWarehouse.getAdenaAmount()
        amount = Math.min( amount, limit )

        await clanWarehouse.addItem( ItemTypes.Adena, amount, null, 'Auction.returnItem' )
    }

    saveAuctionDate(): Promise<void> {
        return DatabaseManager.getClanHallAuctions().updateEndDate( this.getId(), this.data.endDate )
    }

    addAuctionToDatabase(): Promise<void> {
        return DatabaseManager.getClanHallAuctions().addAuction( this.data )
    }

    async removeAll(): Promise<void> {
        await this.removeBids()
        return this.deleteAuctionFromDatabase()
    }

    async setBidAmount( player: L2PcInstance, value: number ): Promise<void> {
        let requiredAdena = value
        let clan: L2Clan = player.getClan()

        if ( !clan || clan.getLeaderId() !== player.getObjectId() || clan.getLevel() < 2 ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.AUCTION_ONLY_CLAN_LEVEL_2_HIGHER ) )
        }

        if ( this.highestBidder.clanName === clan.getLeaderName() ) {
            requiredAdena = value - this.highestBidder.maxBidAmount
        }

        if ( ( this.highestBidder && value > this.highestBidder.maxBidAmount ) || ( !this.highestBidder && value >= this.data.startingBid ) ) {
            if ( clan.getWarehouse().getAdenaAmount() >= requiredAdena ) {
                /*
                    If player cannot remove adena from warehouse, that would mean that either adena amount is insufficient
                    or action is intercepted and refused by any event listeners.
                 */
                if ( !await clan.getWarehouse().destroyItemByItemId( ItemTypes.Adena, requiredAdena, player.getObjectId() ) ) {
                    return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_PARTICIPATE_IN_AN_AUCTION ) )
                }

                await this.updateHighestBidder( clan, requiredAdena )

                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.BID_IN_CLANHALL_AUCTION ) )
                return clan.setAuctionBidAt( this.data.id, true )
            }

            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA_IN_CWH ) )
            return
        }

        if ( value < this.data.startingBid || value <= this.highestBidder.maxBidAmount ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.BID_PRICE_MUST_BE_HIGHER ) )
        }
    }

    async updateHighestBidder( clan: L2Clan, bidAmount: number ): Promise<void> {
        if ( !this.highestBidder ) {
            this.highestBidder = {
                auctionId: this.getId(),
                bidTime: Date.now(),
                bidderId: clan.getId(),
                clanName: clan.getName(),
                maxBidAmount: bidAmount,
                name: clan.getLeaderName(),
            }

            this.bidders[ this.highestBidder.bidderId ] = this.highestBidder

            return DatabaseManager.getAuctionBids().addBid( this.highestBidder )
        }

        this.highestBidder.maxBidAmount = bidAmount
        this.highestBidder.bidTime = Date.now()

        return DatabaseManager.getAuctionBids().updateBid( this.highestBidder )
    }
}