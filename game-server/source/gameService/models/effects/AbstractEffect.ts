import { FunctionTemplate } from '../stats/functions/FunctionTemplate'
import { EffectFlag } from '../../enums/EffectFlag'
import { BuffInfo } from '../skills/BuffInfo'
import { Skill } from '../Skill'
import { L2Character } from '../actor/L2Character'
import { AbstractFunction } from '../stats/functions/AbstractFunction'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { ConfigManager } from '../../../config/ConfigManager'
import { BlockedAction } from '../../enums/BlockedAction'

export class AbstractEffect {
    functionTemplates: Array<FunctionTemplate> = []
    ticks: number = 0
    effectFlags: EffectFlag = EffectFlag.NONE
    effectType: L2EffectType = L2EffectType.None

    isInstant() : boolean {
        return false
    }

    calculateSuccess( info: BuffInfo ) : boolean {
        return true
    }

    onStart( info: BuffInfo ) : Promise<void> {
        return Promise.resolve()
    }

    canStart( info: BuffInfo ) : boolean {
        return true
    }

    getTicks() {
        return this.ticks
    }

    getStatFunctions( caster: L2Character, target: L2Character, skill: Skill ): Array<AbstractFunction> {
        let effect = this
        return this.functionTemplates.reduce( ( functions: Array<AbstractFunction>, currentTemplate: FunctionTemplate ): Array<AbstractFunction> => {
            let currentFunction = currentTemplate.createFunction( caster, target, skill, effect )
            if ( currentFunction ) {
                functions.push( currentFunction )
            }

            return functions
        }, [] )
    }

    onExit( buff: BuffInfo ): Promise<void> {
        return Promise.resolve()
    }

    onTick( info: BuffInfo ): Promise<boolean> {
        return Promise.resolve( true )
    }

    getTicksMultiplier(): number {
        return ( this.getTicks() * ConfigManager.character.getEffectTickRatio() ) / 1000
    }

    blocksAction( action: BlockedAction ): boolean {
        return true
    }
}