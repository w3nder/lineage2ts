import { GeneralAnnouncement } from './GeneralAnnouncement'
import { AnnouncementType } from './AnnouncementType'
import Timeout = NodeJS.Timeout
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { DatabaseManager } from '../../../database/manager'
import _ from 'lodash'
import { NpcSayType } from '../../enums/packets/NpcSayType'

export class AutoAnnouncement extends GeneralAnnouncement {
    initial: number
    delay: number
    repeat: number
    currentState: number
    task: Timeout
    lines: Array<string>

    constructor( type: AnnouncementType, author: string, content: string, initial: number, delay: number, repeat: number, id: number = null ) {
        super( type, author, content )
        this.initial = initial
        this.delay = delay
        this.repeat = repeat
        this.lines = _.split( content, /\r?\n/ )

        this.restartTask()
    }

    restartTask() {
        this.stopTask()

        this.currentState = this.repeat
        this.startTask()
    }

    startTask() {
        this.stopTask()

        this.task = setTimeout( this.runTask.bind( this ), this.initial )
    }

    stopTask() {
        if ( !this.task ) {
            clearTimeout( this.task )
            this.task = null
        }
    }

    runTask() {
        if ( this.currentState === -1 || this.currentState > 0 ) {
            let isCritical = this.type === AnnouncementType.CRITICAL ? NpcSayType.CriticalAnnouncement : NpcSayType.Announce
            _.each( this.lines, ( line: string ) => {
                BroadcastHelper.toAllOnlinePlayersData( CreatureSay.fromText( 0, 0, isCritical, '', line ) )
            } )

            if ( this.currentState !== 1 ) {
                this.currentState--
            }

            this.task = setTimeout( this.runTask.bind( this ), this.delay )
        }
    }

    async deleteMe() {
        this.stopTask()
        return super.deleteMe()
    }

    async storeMe(): Promise<void> {
        this.id = await DatabaseManager.getAnnouncements().createAuto( this )
    }

    async updateMe(): Promise<void> {
        return DatabaseManager.getAnnouncements().updateAuto( this )
    }

    isAuto() : boolean {
        return true
    }
}