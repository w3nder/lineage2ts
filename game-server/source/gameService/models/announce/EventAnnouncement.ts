import { AnnouncementType } from './AnnouncementType'
import { GeneralAnnouncement } from './GeneralAnnouncement'

export class EventAnnouncement extends GeneralAnnouncement {

    async deleteMe(): Promise<void> {
        return
    }

    getType(): AnnouncementType {
        return AnnouncementType.EVENT
    }

    async storeMe(): Promise<void> {
        return
    }

    async updateMe(): Promise<void> {
        return
    }
}