import { Inventory } from './Inventory'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { ListenerCache } from '../../cache/ListenerCache'
import {
    EventType,
    PlayerAddItemEvent,
    PlayerCanDestroyItemEvent,
    PlayerCanDropItemEvent,
    PlayerDestroyItemEvent,
    PlayerDropItemEvent,
    PlayerTransferItemEvent,
} from '../events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { L2World } from '../../L2World'
import { ItemContainer } from './ItemContainer'
import { InventoryUpdateBuilder } from '../../packets/send/builder/InventoryUpdate'
import { PacketDispatcher } from '../../PacketDispatcher'
import { StatusUpdate, StatusUpdateProperty } from '../../packets/send/builder/StatusUpdate'
import { InventoryUpdateStatus } from '../../enums/InventoryUpdateStatus'

// TODO : review flow, since some of logic calling drop items already have messages that would be shown and may interfere
// with message presented to player via terminated listeners
// so either keep listener check here or move to above layers
export class PlayableInventory extends Inventory {
    inventoryUpdate : InventoryUpdateBuilder

    async destroyItem( item: L2ItemInstance, count: number, byPlayerId: number = 0, reason: string = null ): Promise<L2ItemInstance> {
        let playerId = this.getPlayerOwnerId()

        if ( ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerCanDestroyItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerCanDestroyItem ) as PlayerCanDestroyItemEvent

            eventData.playerId = playerId
            eventData.amount = count
            eventData.itemObjectId = item.getObjectId()
            eventData.itemTemplateId = item.getId()

            let player = L2World.getPlayer( playerId )
            let result = await ListenerCache.getTerminatedItemEventResult( item, EventType.PlayerCanDestroyItem, player, eventData )
            if ( result && result.terminate ) {
                return
            }
        }

        let deletedItem: L2ItemInstance = await super.destroyItem( item, count, playerId, reason )

        if ( deletedItem && ListenerCache.hasItemTemplateEvent( deletedItem.getId(), EventType.PlayerDestroyItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerDestroyItem ) as PlayerDestroyItemEvent

            eventData.playerId = playerId
            eventData.amount = count
            eventData.itemObjectId = deletedItem.getObjectId()
            eventData.itemTemplateId = deletedItem.getId()

            await ListenerCache.sendItemTemplateEvent( deletedItem.getId(), EventType.PlayerDestroyItem, eventData )
        }

        return deletedItem
    }

    async addExistingItem( item: L2ItemInstance, reason: string ): Promise<L2ItemInstance> {
        let amount = item.getCount()
        let previousOwnerId = item.getOwnerId()
        let existingItem = await super.addExistingItem( item, reason )

        if ( existingItem && ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerAddItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerAddItem ) as PlayerAddItemEvent

            eventData.amount = amount
            eventData.itemTemplateId = existingItem.getId()
            eventData.playerId = this.getPlayerOwnerId()
            eventData.itemObjectId = existingItem.getObjectId()
            eventData.reason = reason
            eventData.previousOwnerId = previousOwnerId

            await ListenerCache.sendItemTemplateEvent( item.getId(), EventType.PlayerAddItem, eventData )
        }

        return existingItem
    }

    async dropItem( item: L2ItemInstance ): Promise<L2ItemInstance> {
        let playerId = this.getPlayerOwnerId()
        if ( ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerCanDropItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerCanDropItem ) as PlayerCanDropItemEvent

            eventData.playerId = playerId
            eventData.amount = item.getCount()
            eventData.itemObjectId = item.getObjectId()
            eventData.itemTemplateId = item.getId()

            let player = L2World.getPlayer( playerId )
            let result = await ListenerCache.getTerminatedItemEventResult( item, EventType.PlayerCanDropItem, player, eventData )
            if ( result && result.terminate ) {
                return
            }
        }

        let droppedItem = await super.dropItem( item )

        if ( item && ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerDropItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerDropItem ) as PlayerDropItemEvent

            eventData.playerId = playerId
            eventData.itemObjectId = item.getObjectId()
            eventData.itemTemplateId = item.getId()
            eventData.locationX = item.getX()
            eventData.locationY = item.getY()
            eventData.locationZ = item.getZ()
            eventData.amount = item.getCount()

            await ListenerCache.sendItemTemplateEvent( item.getId(), EventType.PlayerDropItem, eventData )
        }

        return droppedItem
    }

    async dropItemCount( objectId: number, count: number, reason: string = null ): Promise<L2ItemInstance> {
        let existingItem: L2ItemInstance = this.getItemByObjectId( objectId )
        let playerId = this.getPlayerOwnerId()

        if ( existingItem && ListenerCache.hasItemTemplateEvent( existingItem.getId(), EventType.PlayerCanDropItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerCanDropItem ) as PlayerCanDropItemEvent

            eventData.playerId = playerId
            eventData.amount = count
            eventData.itemObjectId = objectId
            eventData.itemTemplateId = existingItem.getId()

            let player = L2World.getPlayer( playerId )
            let result = await ListenerCache.getTerminatedItemEventResult( existingItem, EventType.PlayerCanDropItem, player, eventData )
            if ( result && result.terminate ) {
                return
            }
        }

        let item: L2ItemInstance = await super.dropItemCount( objectId, count, reason )

        if ( item && ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerDropItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerDropItem ) as PlayerDropItemEvent

            eventData.playerId = playerId
            eventData.itemObjectId = item.getObjectId()
            eventData.itemTemplateId = item.getId()
            eventData.locationX = item.getX()
            eventData.locationY = item.getY()
            eventData.locationZ = item.getZ()
            eventData.amount = item.getCount()

            await ListenerCache.sendItemTemplateEvent( item.getId(), EventType.PlayerDropItem, eventData )
        }

        return item
    }

    async transferItem( objectId: number, amount: number, container: ItemContainer, referenceId: number = 0, reason: string = null ): Promise<L2ItemInstance> {
        let oldItem: L2ItemInstance = this.getItemByObjectId( objectId )
        if ( !oldItem || amount > oldItem.getCount() ) {
            return null
        }

        let item: L2ItemInstance = await super.transferItem( objectId, amount, container, referenceId )

        if ( ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerTransferItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerTransferItem ) as PlayerTransferItemEvent

            eventData.itemTemplateId = item.getId()
            eventData.playerId = this.getPlayerOwnerId()
            eventData.itemObjectId = item.getObjectId()
            eventData.amount = amount
            eventData.destinationType = container.getName()
            eventData.destinationPlayerId = container.getPlayerOwnerId()

            await ListenerCache.sendItemTemplateEvent( item.getId(), EventType.PlayerTransferItem, eventData )
        }

        return item
    }

    async addItem( itemId: number, amount: number, referenceId: number, reason: string, enchantLevel: number = -1 ): Promise<L2ItemInstance> {
        let item: L2ItemInstance = await super.addItem( itemId, amount, referenceId, reason, enchantLevel )

        if ( ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerAddItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerAddItem ) as PlayerAddItemEvent

            eventData.amount = amount
            eventData.itemTemplateId = itemId
            eventData.playerId = this.getPlayerOwnerId()
            eventData.itemObjectId = item ? item.getObjectId() : 0
            eventData.reason = reason
            eventData.previousOwnerId = 0

            await ListenerCache.sendItemTemplateEvent( itemId, EventType.PlayerAddItem, eventData )
        }

        return item
    }

    inventoryUpdateOperation() : void {
        if ( !this.inventoryUpdate || this.inventoryUpdate.items.length === 0 ) {
            return
        }

        PacketDispatcher.sendOwnedData( this.getPlayerOwnerId(), this.inventoryUpdate.getBuffer() )
        PacketDispatcher.sendOwnedData( this.getPlayerOwnerId(), StatusUpdate.forValue( this.getOwnerId(), StatusUpdateProperty.InventoryWeight, this.getTotalWeight() ) )
    }

    markItem( item: L2ItemInstance, status : InventoryUpdateStatus ) : void {
        /*
            When items are initially loaded to inventory, it is possible to schedule database update
            when in reality no update is needed. Hence, we must guard against any update that is not needed
            until time when inventory update is enabled.
         */
        if ( this.inventoryUpdate ) {
            item.setInventoryStatus( status )
            item.scheduleDatabaseUpdate()
        }

        this.refreshWeight()

        if ( item.getOwnerId() !== this.getOwnerId() || !this.inventoryUpdate ) {
            return
        }

        this.inventoryUpdate.addItem( item, status )
    }

    deleteMe(): Promise<void> {
        if ( this.inventoryUpdate ) {
            this.inventoryUpdate.reset()
            this.inventoryUpdate = null
        }

        return super.deleteMe()
    }

    resetInventoryUpdate() : void {
        if ( !this.inventoryUpdate || this.inventoryUpdate.items.length === 0 ) {
            return
        }

        return this.inventoryUpdate.reset()
    }
}