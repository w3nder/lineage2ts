import { ItemContainer } from './ItemContainer'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { ItemLocation } from '../../enums/ItemLocation'
import { ConfigManager } from '../../../config/ConfigManager'
import { Stats } from '../stats/Stats'

export class PcFreight extends ItemContainer {
    isPlayerOwned: boolean

    constructor( objectId: number, isPlayer: boolean = false ) {
        super()

        this.ownerId = objectId
        this.isPlayerOwned = isPlayer
    }

    getOwner(): L2PcInstance {
        if ( this.isPlayerOwned ) {
            return L2World.getPlayer( this.ownerId )
        }

        return null
    }

    getBaseLocation(): ItemLocation {
        return ItemLocation.FREIGHT
    }

    getName() {
        return 'Freight'
    }

    validateCapacity( slots: number ) : boolean {
        let player = this.getOwner()
        let currentSlots = player ? ConfigManager.character.getMaximumFreightSlots() : ConfigManager.character.getMaximumFreightSlots() + Math.floor( player.getStat().calculateStat( Stats.FREIGHT_LIM, 0, null, null ) )

        return ( this.getSize() + currentSlots ) <= currentSlots
    }
}