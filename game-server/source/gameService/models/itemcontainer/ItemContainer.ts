import { ConfigManager } from '../../../config/ConfigManager'
import { L2Item } from '../items/L2Item'
import { L2World } from '../../L2World'
import { L2Character } from '../actor/L2Character'
import { ItemLocation } from '../../enums/ItemLocation'
import { DatabaseManager } from '../../../database/manager'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { L2ItemsTableItem } from '../../../database/interface/ItemsTableApi'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { ItemTypes } from '../../values/InventoryValues'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { InventoryUpdateStatus } from '../../enums/InventoryUpdateStatus'
import aigle from 'aigle'
import _ from 'lodash'
import HashArray from 'hasharray'

const adenaUpdateInterval: number = GeneralHelper.minutesToMillis( 30 )

export class ItemContainer {
    lastAdenaUpdate: number = 0
    ownerId: number = 0
    allItems: typeof HashArray = new HashArray( [ 'objectId', 'itemId' ] )

    async addExistingItem( item: L2ItemInstance, reason: string = null ): Promise<L2ItemInstance> {
        let oldItem: L2ItemInstance = this.getItemByItemId( item.getId() )

        if ( oldItem && oldItem.isStackable() ) {
            let amount = item.getCount()
            oldItem.changeAmount( amount )

            this.markItem( oldItem, InventoryUpdateStatus.Modified )

            if ( oldItem.getId() === ItemTypes.Adena ) {
                let currentUpdateTime = Date.now()

                if ( this.lastAdenaUpdate < currentUpdateTime ) {
                    this.lastAdenaUpdate = currentUpdateTime + adenaUpdateInterval
                }
            }

            item.markDestroyed()

            return oldItem
        }

        await item.setOwnership( this.getOwnerId(), this.getBaseLocation() )
        await this.addToInventory( item )

        return item
    }

    async addItem( itemId: number, amount: number, referenceId: number, reason: string, enchantLevel: number = -1 ): Promise<L2ItemInstance> {
        let item: L2ItemInstance = this.getItemByItemId( itemId )

        if ( item && item.isStackable() ) {
            item.changeAmount( amount )
            this.markItem( item, InventoryUpdateStatus.Modified )

            if ( enchantLevel > -1 ) {
                await item.setEnchantLevel( enchantLevel )
            }
        } else {
            let template: L2Item = ItemManagerCache.getTemplate( itemId )
            if ( !template ) {
                return null
            }

            let totalAmount = ( template.isStackable() || !ConfigManager.general.multipleItemDrop() ) ? 1 : amount
            let container = this
            let playerId = this.getPlayerOwnerId()

            await aigle.resolve( totalAmount ).times( () : Promise<void> => {
                let currentItem = ItemManagerCache.createItem( itemId, reason, template.isStackable() ? amount : 1, playerId )

                currentItem.setOwnershipDirect( container.getOwnerId(), container.getBaseLocation() )
                currentItem.setEnchantLevelDirect( enchantLevel > -1 ? enchantLevel : template.getDefaultEnchantLevel() )

                item = currentItem
                return container.addToInventory( currentItem )
            } )
        }

        return item
    }

    async addToInventory( item: L2ItemInstance ): Promise<void> {
        this.allItems.addOne( item )
        this.markItem( item, InventoryUpdateStatus.Added )
    }

    async deleteMe(): Promise<void> {
        if ( this.getOwnerId() > 0 ) {
            await ItemManagerCache.performAllItemsUpdate()
        }

        this.resetInventory()
    }

    resetInventory(): void {
        this.getItems().forEach( ( item: L2ItemInstance ) => {
            item.resetProperties()
            L2World.removeObject( item )
        } )

        this.emptyInventory()
    }

    emptyInventory() : void {
        this.allItems.removeAll()
    }

    async destroyAllItems( playerId: number, reason: string ): Promise<void> {
        await aigle.resolve( this.getItems() ).each( async ( item: L2ItemInstance ) => {
            if ( item ) {
                return this.destroyItem( item, item.getCount(), playerId, reason )
            }
        } )
    }

    async destroyItem( item: L2ItemInstance, count: number, byPlayerId: number = 0, reason: string = null ): Promise<L2ItemInstance> {
        if ( !this.hasItemWithId( item.getObjectId() ) ) {
            return null
        }

        if ( item.getCount() > count ) {
            item.changeAmount( -count )
            this.markItem( item, InventoryUpdateStatus.Modified )

            return item
        }

        if ( item.getCount() < count ) {
            return null
        }

        await this.removeItem( item )
        item.markDestroyed()

        return item
    }

    async destroyItemByItemId( itemId: number, count: number, byPlayerId: number = 0, reason: string = null ): Promise<L2ItemInstance> {
        let item: L2ItemInstance = this.getItemByItemId( itemId )
        if ( !item ) {
            return null
        }

        return this.destroyItem( item, count, byPlayerId, reason )
    }

    getAdenaAmount() {
        let item: L2ItemInstance = this.getItemByItemId( ItemTypes.Adena )

        if ( item ) {
            return item.getCount()
        }

        return 0
    }

    getBaseLocation(): ItemLocation {
        return null
    }

    getInventoryItemCount( id: number, enchantLevel: number, includeEquipped: boolean = true ): number {
        return this.allItems.getAsArray( id ).reduce( ( total: number, currentItem: L2ItemInstance ) => {
            if ( ( currentItem.getEnchantLevel() === enchantLevel || enchantLevel < 0 )
                    && ( includeEquipped || !currentItem.isEquipped() ) ) {
                return total + ( currentItem.isStackable() ? currentItem.getCount() : 1 )
            }

            return total
        }, 0 )
    }

    getItemByItemId( id: number ): L2ItemInstance {
        let item: Array<L2ItemInstance> | L2ItemInstance = this.allItems.get( id )

        if ( _.isArray( item ) ) {
            return item[ 0 ]
        }

        return item as L2ItemInstance
    }

    getWithItemIds( itemIds: Array<number> ): Array<L2ItemInstance> {
        return this.allItems.getAll( itemIds )
    }

    getItemByObjectId( objectId: number ): L2ItemInstance {
        let item = this.allItems.get( objectId )

        /*
            Due to how itemIds and objectIds are mashed together,
            it is possible to request an id that would cause HashArray
            to return an array of multiple items.
         */
        if ( Array.isArray( item ) ) {
            return null
        }

        return item
    }

    /*
        Warning: Current implementation references HashArray contents
        directly. Using iteration that modifies contents (transfer or removal of item from inventory)
        can cause unintended consequences where not all items will be processed.
     */
    getItems(): ReadonlyArray<L2ItemInstance> {
        return this.allItems.all
    }

    getItemsByItemId( id: number ): ReadonlyArray<L2ItemInstance> {
        return this.allItems.getAsArray( id )
    }

    getName() {
        return 'ItemContainer'
    }

    getOwner(): L2Character {
        return L2World.getObjectById( this.ownerId ) as L2Character
    }

    getOwnerId(): number {
        return this.ownerId
    }

    getSize() : number {
        return this.allItems.all.length
    }

    isClanWarehouse(): boolean {
        return false
    }

    isPrivate(): boolean {
        return false
    }

    refreshWeight() {

    }

    async removeItem( item: L2ItemInstance ): Promise<void> {
        this.allItems.remove( item )
        this.markItem( item, InventoryUpdateStatus.Removed )
    }

    async restore() {
        let ownerId = this.getOwnerId()
        let databaseItems: Array<L2ItemsTableItem> = await DatabaseManager.getItems().getItemsByLocation( ownerId, this.getBaseLocation() )
        let container = this

        await aigle.resolve( databaseItems ).each( async ( data: L2ItemsTableItem ) => {
            let item: L2ItemInstance = L2ItemInstance.createFromData( ownerId, data )
            if ( !item ) {
                return
            }

            if ( item.isEquipable() ) {
                await item.restoreElementals()
            }

            if ( item.isStackable() && container.hasItemWithId( item.getId() ) ) {
                return container.addExistingItem( item, 'ItemContainer.restore' )
            }

            return container.addToInventory( item )
        } )

        this.refreshWeight()
    }

    async transferItem( objectId: number, count: number, container: ItemContainer, referenceId: number = 0, reason: string = null ): Promise<L2ItemInstance> {
        if ( !container ) {
            return null
        }

        let sourceItem: L2ItemInstance = this.getItemByObjectId( objectId )
        if ( !sourceItem ) {
            return null
        }

        if ( sourceItem.isAugmented() ) {
            let player = L2World.getPlayer( this.getPlayerOwnerId() )

            if ( player ) {
                sourceItem.getAugmentation().removeBonus( player )
            }
        }

        let adjustedAmount = Math.min( count , sourceItem.getCount() )
        let targetItem: L2ItemInstance

        if ( sourceItem.getCount() === adjustedAmount ) {
            await this.removeItem( sourceItem )
            await sourceItem.resetOwnership()

            targetItem = await container.addExistingItem( sourceItem, reason )
        } else {
            sourceItem.changeAmount( -count )
            this.markItem( sourceItem, InventoryUpdateStatus.Modified )

            targetItem = sourceItem.isStackable() ? container.getItemByItemId( sourceItem.getId() ) : null

            if ( !targetItem ) {
                targetItem = await container.addItem( sourceItem.getId(), count, referenceId, reason )
            } else {
                targetItem.changeAmount( count )
                this.markItem( targetItem, InventoryUpdateStatus.Modified )
            }
        }

        return targetItem
    }

    validateCapacity( slots: number ): boolean {
        return true
    }

    validateWeight( weight: number ): boolean {
        return true
    }

    hasItemWithId( itemId: number ): boolean {
        return this.allItems.has( itemId )
    }

    getPlayerOwnerId(): number {
        return 0
    }

    async destroyItemByObjectId( objectId: number, count: number, byPlayerId: number = 0, reason: string = null ): Promise<L2ItemInstance> {
        let item: L2ItemInstance = this.getItemByObjectId( objectId )

        if ( item ) {
            return this.destroyItem( item, count, byPlayerId, reason )
        }

        return null
    }

    markItem( item: L2ItemInstance, status : InventoryUpdateStatus ) : void {
        item.setInventoryStatus( status )
        item.scheduleDatabaseUpdate()

        this.refreshWeight()
    }
}