import { ItemContainer } from './ItemContainer'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { ItemLocation } from '../../enums/ItemLocation'

export class PcWarehouse extends ItemContainer {
    owner: number

    constructor( player: L2PcInstance ) {
        super()
        this.owner = player.getObjectId()
    }

    getName(): string {
        return 'Warehouse'
    }

    getOwner(): L2PcInstance {
        return L2World.getPlayer( this.owner )
    }

    getBaseLocation(): ItemLocation {
        return ItemLocation.WAREHOUSE
    }

    validateCapacity( slots: number ): boolean {
        return ( this.getSize() + slots ) <= this.getOwner().getWareHouseLimit()
    }
}