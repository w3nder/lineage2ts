import { ItemContainer } from './ItemContainer'
import { L2Clan } from '../L2Clan'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { ItemLocation } from '../../enums/ItemLocation'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import {
    EventType,
    PlayerAddItemToClanWarehouseEvent,
    PlayerCanDestroyItemInClanWarehouseEvent,
    PlayerDestroyItemInClanWarehouseEvent,
    PlayerTransferItemFromClanWarehouseEvent,
} from '../events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ClanCache } from '../../cache/ClanCache'
import { EventPoolCache } from '../../cache/EventPoolCache'

export class ClanWarehouse extends ItemContainer {

    constructor( clan: L2Clan ) {
        super()
        this.ownerId = clan.getId()
    }

    async addItem( itemId: number, amount: number, referenceId: number, reason: string, enchantLevel: number = -1 ): Promise<L2ItemInstance> {
        let addedItem: L2ItemInstance = await super.addItem( itemId, amount, referenceId, reason, enchantLevel )

        if ( ListenerCache.hasItemTemplateEvent( itemId, EventType.PlayerAddItemToClanWarehouse ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerAddItemToClanWarehouse ) as PlayerAddItemToClanWarehouseEvent

            eventData.playerId = referenceId
            eventData.itemObjectId = addedItem.getObjectId()
            eventData.itemTemplateId = addedItem.getId()
            eventData.clanId = this.ownerId
            eventData.amount = amount
            eventData.reason = reason

            await ListenerCache.sendItemTemplateEvent( itemId, EventType.PlayerAddItemToClanWarehouse, eventData )
        }

        return addedItem
    }

    async destroyItem( item: L2ItemInstance, amount: number, byPlayerId: number = 0, reason: string = null ): Promise<L2ItemInstance> {
        if ( ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerCanDestroyItemInClanWarehouse ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerCanDestroyItemInClanWarehouse ) as PlayerCanDestroyItemInClanWarehouseEvent

            eventData.playerId = byPlayerId
            eventData.amount = amount
            eventData.itemObjectId = item.getObjectId()
            eventData.itemTemplateId = item.getId()
            eventData.clanId = this.ownerId

            let result = await ListenerCache.getTerminatedItemEventResult( item, EventType.PlayerCanDestroyItemInClanWarehouse, this.getOwner(), eventData )
            if ( result && result.terminate ) {
                return
            }
        }

        let deletedItem = await super.destroyItem( item, amount, byPlayerId, reason )

        if ( ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerDestroyItemInClanWarehouse ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerDestroyItemInClanWarehouse ) as PlayerDestroyItemInClanWarehouseEvent

            eventData.itemTemplateId = item.getId()
            eventData.playerId = byPlayerId
            eventData.itemObjectId = item.getObjectId()
            eventData.clanId = this.ownerId
            eventData.amount = amount
            eventData.reason = reason

            await ListenerCache.sendItemTemplateEvent( item.getId(), EventType.PlayerDestroyItemInClanWarehouse, eventData )
        }


        return deletedItem
    }

    getBaseLocation(): ItemLocation {
        return ItemLocation.CLANWH
    }

    getName(): string {
        return 'ClanWarehouse'
    }

    getOwner(): L2PcInstance {
        let clan: L2Clan = ClanCache.getClan( this.ownerId )
        return clan ? clan.getLeader().getPlayerInstance() : null
    }

    isClanWarehouse(): boolean {
        return true
    }

    async transferItem( objectId: number, amount: number, container: ItemContainer, referenceId: number = 0, reason: string = null ): Promise<L2ItemInstance> {
        let item: L2ItemInstance = this.getItemByObjectId( objectId )

        if ( item && ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerTransferItemFromClanWarehouse ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerTransferItemFromClanWarehouse ) as PlayerTransferItemFromClanWarehouseEvent

            eventData.itemTemplateId = item.getId()
            eventData.playerId = container.getPlayerOwnerId()
            eventData.itemObjectId = objectId
            eventData.clanId = this.ownerId
            eventData.amount = amount
            eventData.reason = reason

            await ListenerCache.sendItemTemplateEvent( item.getId(), EventType.PlayerTransferItemFromClanWarehouse, eventData )
        }

        return super.transferItem( objectId, amount, container )
    }

    validateCapacity( slots: number ): boolean {
        return ( ( this.getSize() + slots ) <= ConfigManager.character.getMaximumWarehouseSlotsForClan() )
    }
}