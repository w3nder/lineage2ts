import { ItemContainer } from './ItemContainer'
import { DatabaseManager } from '../../../database/manager'
import { L2Item } from '../items/L2Item'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { PaperdollListener } from './PaperdollListener'
import { ItemLocation } from '../../enums/ItemLocation'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { EtcItemType } from '../../enums/items/EtcItemType'
import { WeaponType } from '../items/type/WeaponType'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { DataManager } from '../../../data/manager'
import { InventoryInstanceType } from './InventoryTypes'
import { L2ItemsTableItem } from '../../../database/interface/ItemsTableApi'
import { InventorySlot } from '../../values/InventoryValues'
import _, { DebouncedFunc } from 'lodash'
import { InventoryHelper } from './InventoryHelper'
import { InventoryUpdateStatus } from '../../enums/InventoryUpdateStatus'
import aigle from 'aigle'
import { ListenerCache } from '../../cache/ListenerCache'
import { DataIntegrityViolationEvent, DataIntegrityViolationType, EventType } from '../events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { L2ItemSlots } from '../../enums/L2ItemSlots'

const selfResurrectionItemIds : Array<number> = [
    10649,
    13128,
    13300
]

export class Inventory extends ItemContainer {
    paperdoll: Array<L2ItemInstance> = new Array( InventorySlot.TotalSlots )
    paperdollListeners: Array<PaperdollListener>
    totalWeight: number = 0
    instanceType: InventoryInstanceType = InventoryInstanceType.NONE

    refreshWeight: DebouncedFunc<() => void>
    itemTrackers: Set<Set<L2ItemInstance>> = new Set<Set<L2ItemInstance>>()

    constructor() {
        super()

        this.refreshWeight = _.debounce( this.refreshWeightOperation.bind( this ), 300, {
            trailing: true,
            maxWait: 600,
        } )
    }

    async addToInventory( item: L2ItemInstance ): Promise<void> {
        await super.addToInventory( item )

        if ( item.isEquipped() ) {
            await this.equipItem( item )
        }
    }

    refreshWeightOperation(): void {
        let weight = this.getItems().reduce( ( total, item: L2ItemInstance ) => {
            if ( item && item.getItem() ) {
                total = total + item.getItem().getWeight() * item.getCount()
            }

            return total
        }, 0 )

        this.totalWeight = Math.min( weight, Number.MAX_SAFE_INTEGER )
    }

    async removeItem( item: L2ItemInstance ): Promise<void> {
        let index: number = this.paperdoll.findIndex( ( equippedItem: L2ItemInstance ): boolean => {
            return equippedItem && equippedItem.getObjectId() === item.getObjectId()
        } )

        if ( index > -1 ) {
            await this.unEquipItemInSlot( index )
        }

        return super.removeItem( item )
    }

    async restore() : Promise<void> {
        let character = this.getOwner()
        let databaseItems: Array<L2ItemsTableItem> = await DatabaseManager.getItems().getInventoryItems( this.getOwnerId(), this.getBaseLocation(), this.getEquipLocation() )

        await aigle.resolve( databaseItems ).each( async ( data: L2ItemsTableItem ) => {
            // TODO : figure out how to clean up such items
            if ( !data.objectId || !data.itemId ) {
                return
            }

            let item: L2ItemInstance = L2ItemInstance.createFromData( character.getObjectId(), data )
            if ( !item ) {
                return
            }

            if ( item.isEquipable() ) {
                await item.restoreElementals()
            }

            if ( item.isHeroItem()
                    && data.itemLocation !== this.getBaseLocation()
                    && character.isPlayer()
                    && !( character as L2PcInstance ).hasActionOverride( PlayerActionOverride.ItemAction )
                    && !( character as L2PcInstance ).isHero() ) {
                item.location = this.getBaseLocation()
            }

            return this.addToInventory( item )
        } )

        this.refreshWeightOperation()
    }

    async dropItem( item: L2ItemInstance ): Promise<L2ItemInstance> {
        if ( !item || !this.hasItem( item ) ) {
            return null
        }


        await this.removeItem( item )
        await item.resetOwnership()
        this.refreshWeight()

        return item
    }

    async dropItemCount( objectId: number, count: number, reason: string = null ): Promise<L2ItemInstance> {
        let item: L2ItemInstance = this.getItemByObjectId( objectId )
        if ( !item ) {
            return null
        }

        if ( !this.hasItem( item ) ) {
            return null
        }

        if ( item.getCount() > count ) {
            item.changeAmount( -count )
            this.markItem( item, InventoryUpdateStatus.Modified )

            let duplicatedItem = ItemManagerCache.createItem( item.getId(), reason, count, this.getPlayerOwnerId() )
            duplicatedItem.scheduleDatabaseUpdate()

            this.refreshWeight()
            return duplicatedItem
        }

        return this.dropItem( item )
    }

    async equipItem( item: L2ItemInstance ): Promise<void> {
        let targetSlot: number = item.getItem().getBodyPart()

        let formal: L2ItemInstance = this.getPaperdollItem( InventorySlot.Chest )
        if ( formal && item.getId() !== 21163 && formal.getItem().getBodyPart() === L2ItemSlots.AllDressSlots ) {
            switch ( targetSlot ) {
                case L2ItemSlots.AllHandSlots:
                case L2ItemSlots.LeftHand:
                case L2ItemSlots.RightHand:
                case L2ItemSlots.Legs:
                case L2ItemSlots.Feet:
                case L2ItemSlots.Gloves:
                case L2ItemSlots.Head:
                    return
            }
        }

        let promises : Array<Promise<L2ItemInstance>> = []

        switch ( targetSlot ) {
            case L2ItemSlots.AllHandSlots:
                promises.push( this.setPaperdollItem( InventorySlot.LeftHand, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.RightHand, item ) )

                break

            case L2ItemSlots.LeftHand:
                let rightHandItem: L2ItemInstance = this.getPaperdollItem( InventorySlot.RightHand )
                if ( rightHandItem
                        && rightHandItem.getItem().getBodyPart() === L2ItemSlots.AllHandSlots
                        && !( ( rightHandItem.getItemType() === WeaponType.BOW && item.getItemType() === EtcItemType.ARROW )
                                || ( rightHandItem.getItemType() === WeaponType.CROSSBOW && item.getItemType() === EtcItemType.BOLT )
                                || ( rightHandItem.getItemType() === WeaponType.FISHINGROD && item.getItemType() === EtcItemType.LURE ) ) ) {
                    promises.push( this.setPaperdollItem( InventorySlot.RightHand, null ) )
                }

                promises.push( this.setPaperdollItem( InventorySlot.LeftHand, item ) )
                break

            case L2ItemSlots.RightHand:
                promises.push( this.setPaperdollItem( InventorySlot.RightHand, item ) )
                break

            case L2ItemSlots.LeftEar:
            case L2ItemSlots.RightEar:
            case L2ItemSlots.AllEarSlots:
                if ( !this.paperdoll[ InventorySlot.LeftEar ] ) {
                    promises.push( this.setPaperdollItem( InventorySlot.LeftEar, item ) )
                    break
                }

                if ( !this.paperdoll[ InventorySlot.RightEar ] ) {
                    promises.push( this.setPaperdollItem( InventorySlot.RightEar, item ) )
                    break
                }

                if ( this.getPaperdollItemId( InventorySlot.RightEar ) === item.getId() ) {
                    promises.push( this.setPaperdollItem( InventorySlot.LeftEar, item ) )
                    break
                }

                if ( this.getPaperdollItemId( InventorySlot.LeftEar ) == item.getId() ) {
                    promises.push( this.setPaperdollItem( InventorySlot.RightEar, item ) )
                    break
                }

                promises.push( this.setPaperdollItem( InventorySlot.LeftEar, item ) )
                break

            case L2ItemSlots.LeftFingers:
            case L2ItemSlots.RightFinger:
            case L2ItemSlots.AllFingerSlots:
                if ( !this.paperdoll[ InventorySlot.LeftFinger ] ) {
                    promises.push( this.setPaperdollItem( InventorySlot.LeftFinger, item ) )
                    break
                }

                if ( !this.paperdoll[ InventorySlot.RightFinger ] ) {
                    promises.push( this.setPaperdollItem( InventorySlot.RightFinger, item ) )
                    break
                }

                if ( this.getPaperdollItemId( InventorySlot.RightFinger ) === item.getId() ) {
                    promises.push( this.setPaperdollItem( InventorySlot.LeftFinger, item ) )
                    break
                }

                if ( this.getPaperdollItemId( InventorySlot.LeftFinger ) == item.getId() ) {
                    promises.push( this.setPaperdollItem( InventorySlot.RightFinger, item ) )
                    break
                }

                promises.push( this.setPaperdollItem( InventorySlot.LeftFinger, item ) )
                break

            case L2ItemSlots.Neck:
                promises.push( this.setPaperdollItem( InventorySlot.Neck, item ) )
                break

            case L2ItemSlots.FullArmor:
                promises.push( this.setPaperdollItem( InventorySlot.Legs, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.Chest, item ) )
                break

            case L2ItemSlots.Chest:
                promises.push( this.setPaperdollItem( InventorySlot.Chest, item ) )
                break

            case L2ItemSlots.Legs:
                let chest: L2ItemInstance = this.getPaperdollItem( InventorySlot.Chest )
                if ( chest && chest.getItem().getBodyPart() === L2ItemSlots.FullArmor ) {
                    promises.push( this.setPaperdollItem( InventorySlot.Chest, null ) )
                }

                promises.push( this.setPaperdollItem( InventorySlot.Legs, item ) )
                break

            case L2ItemSlots.Feet:
                promises.push( this.setPaperdollItem( InventorySlot.Feet, item ) )
                break

            case L2ItemSlots.Gloves:
                promises.push( this.setPaperdollItem( InventorySlot.Gloves, item ) )
                break

            case L2ItemSlots.Head:
                promises.push( this.setPaperdollItem( InventorySlot.Head, item ) )
                break

            case L2ItemSlots.Hair:
                let hair: L2ItemInstance = this.getPaperdollItem( InventorySlot.Hair )
                let slot = hair && hair.getItem().getBodyPart() === L2ItemSlots.AllHairSlots ? InventorySlot.HairSecondary : InventorySlot.Hair

                promises.push( this.setPaperdollItem( slot, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.Hair, item ) )
                break

            case L2ItemSlots.HairSecondary:
                let hair2: L2ItemInstance = this.getPaperdollItem( InventorySlot.Hair )
                let slot2 = hair2 && hair2.getItem().getBodyPart() === L2ItemSlots.AllHairSlots ? InventorySlot.Hair : InventorySlot.HairSecondary

                promises.push( this.setPaperdollItem( slot2, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.HairSecondary, item ) )
                break

            case L2ItemSlots.AllHairSlots:
                promises.push( this.setPaperdollItem( InventorySlot.HairSecondary, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.Hair, item ) )
                break

            case L2ItemSlots.Underware:
                promises.push( this.setPaperdollItem( InventorySlot.Under, item ) )
                break

            case L2ItemSlots.Back:
                promises.push( this.setPaperdollItem( InventorySlot.Cloak, item ) )
                break

            case L2ItemSlots.LeftBracelet:
                promises.push( this.setPaperdollItem( InventorySlot.LeftBracelet, item ) )
                break

            case L2ItemSlots.RightBracelet:
                promises.push( this.setPaperdollItem( InventorySlot.RightBracelet, item ) )
                break

            case L2ItemSlots.Decoration:
                promises.push( this.equipTalisman( item ) )
                break

            case L2ItemSlots.Belt:
                promises.push( this.setPaperdollItem( InventorySlot.Belt, item ) )
                break

            case L2ItemSlots.AllDressSlots:
                promises.push( this.setPaperdollItem( InventorySlot.Legs, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.LeftHand, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.RightHand, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.RightHand, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.LeftHand, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.Head, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.Feet, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.Gloves, null ) )
                promises.push( this.setPaperdollItem( InventorySlot.Chest, item ) )

                break

            default:
                if ( ListenerCache.hasGeneralListener( EventType.DataIntegrityViolation ) ) {
                    let data = EventPoolCache.getData( EventType.DataIntegrityViolation ) as DataIntegrityViolationEvent

                    data.errorId = '40f9b81d-4a91-46d0-871c-1ae1e3d8e914'
                    data.message = `Unknown body slot = ${ targetSlot } for itemId = ${ item.getId() }`
                    data.type = DataIntegrityViolationType.Item
                    data.playerId = this.ownerId
                    data.ids = [ targetSlot ]

                    await ListenerCache.sendGeneralEvent( EventType.DataIntegrityViolation, data )
                }

                return
        }

        await Promise.all( promises )
    }

    equipTalisman( item: L2ItemInstance ) : Promise<L2ItemInstance> {
        return Promise.resolve( null )
    }

    getTalismanWithSkill( skillId: number ) : L2ItemInstance {
        return null
    }

    findArrowForBow( bow: L2Item ): L2ItemInstance {
        return this.getItems().find( ( item: L2ItemInstance ) => {
            return item.isArrow() && item.getItem().getItemGradeSPlus() === bow.getItemGradeSPlus()
        } )
    }

    findBoltForCrossBow( crossbow: L2Item ): L2ItemInstance {
        return this.getItems().find( ( item: L2ItemInstance ) => {
            return item.isBolt() && item.getItem().getItemGradeSPlus() === crossbow.getItemGradeSPlus()
        } )
    }

    getEquipLocation(): ItemLocation {
        return null
    }

    getPaperdollAugmentationId( slot: number ) {
        let item: L2ItemInstance = this.paperdoll[ slot ]
        return item && item.getAugmentation() ? item.getAugmentation().getAugmentationId() : 0
    }

    getPaperdollItem( slot: number ): L2ItemInstance {
        return this.paperdoll[ slot ]
    }

    getPaperdollItemBySlot( slot: L2ItemSlots ): L2ItemInstance {
        let index = InventoryHelper.getPaperdollIndex( slot )
        if ( index === InventorySlot.None ) {
            return null
        }

        return this.paperdoll[ index ]
    }

    getPaperdollItemId( slot: InventorySlot ) {
        let item: L2ItemInstance = this.paperdoll[ slot ]
        if ( item ) {
            return item.getId()
        }

        return 0
    }

    getPaperdollObjectId( slot: InventorySlot ): number {
        let item = this.paperdoll[ slot ]
        return item ? item.objectId : 0
    }

    getTotalWeight() {
        return this.totalWeight
    }

    getWearedMask() {
        let mask = 0

        for ( let slot = 0; slot < InventorySlot.TotalSlots; slot++ ) {
            let wearItem: L2ItemInstance = this.paperdoll[ slot ]
            if ( wearItem ) {
                mask |= wearItem.getItem().getItemMask()
            }
        }

        return mask
    }

    hasItem( item: L2ItemInstance ): boolean {
        return this.hasItemWithId( item.getObjectId() )
    }

    isInstanceType( type: InventoryInstanceType ): boolean {
        return this.instanceType === type
    }

    isPaperdollSlotEmpty( slot: number ): boolean {
        return !this.paperdoll[ slot ]
    }

    reloadEquippedItems(): Promise<any> {
        let inventory = this
        let promises: Array<Promise<void>> = []

        this.paperdoll.forEach( ( item: L2ItemInstance ) => {
            if ( !item ) {
                return
            }

            let slot = item.getLocationSlot()

            inventory.paperdollListeners.forEach( ( listener: PaperdollListener ) => {
                if ( !listener.isForItem( item ) ) {
                    return
                }

                promises.push( listener.notifyUnequiped( slot, item, inventory ).then( () => {
                    return listener.notifyEquiped( slot, item, inventory )
                } ) )
            } )
        } )

        return Promise.all( promises )
    }

    async setPaperdollItem( slot: InventorySlot, item: L2ItemInstance ): Promise<L2ItemInstance> {
        let oldItem: L2ItemInstance = this.paperdoll[ slot ]

        if ( oldItem && item && oldItem.getObjectId() === item.getObjectId() ) {
            return oldItem
        }

        let inventory = this

        if ( oldItem ) {
            this.paperdoll[ slot ] = null

            oldItem.setItemLocationProperties( this.getBaseLocation() )

            await aigle.resolve( this.paperdollListeners ).eachSeries( ( listener: PaperdollListener ) => {
                if ( !listener.isForItem( oldItem ) ) {
                    return
                }

                return listener.notifyUnequiped( slot, oldItem, inventory )
            } )

            this.itemTrackers.forEach( ( tracker: Set<L2ItemInstance> ) => tracker.add( oldItem ) )
            this.markItem( oldItem, InventoryUpdateStatus.Modified )
        }

        if ( item ) {
            this.paperdoll[ slot ] = item
            item.setItemLocationProperties( this.getEquipLocation(), slot )

            await aigle.resolve( this.paperdollListeners ).eachSeries( ( listener: PaperdollListener ) => {
                if ( !listener.isForItem( item ) ) {
                    return
                }

                return listener.notifyEquiped( slot, item, inventory )
            } )

            this.itemTrackers.forEach( ( tracker: Set<L2ItemInstance> ) => tracker.add( item ) )
            this.markItem( item, InventoryUpdateStatus.Modified )
        }

        return oldItem
    }

    async unEquipItemInBodySlot( slot: L2ItemSlots ): Promise<L2ItemInstance> {
        let paperdollSlot = InventoryHelper.getPaperdollIndex( slot )
        if ( paperdollSlot === InventorySlot.None ) {
            return
        }

        return this.setPaperdollItem( paperdollSlot, null )
    }

    async unEquipItemInBodySlotAndRecord( slot: L2ItemSlots ): Promise<Array<L2ItemInstance>> {
        let changedItems : Set<L2ItemInstance> = new Set<L2ItemInstance>()

        this.itemTrackers.add( changedItems )
        await this.unEquipItemInBodySlot( slot )
        this.itemTrackers.delete( changedItems )

        changedItems.forEach( this.onItemUnEquipped.bind( this ) )

        return Array.from( changedItems )
    }

    async unEquipItemInSlot( slot: InventorySlot ) : Promise<void> {
        let changedItems : Set<L2ItemInstance> = new Set<L2ItemInstance>()

        this.itemTrackers.add( changedItems )
        await this.setPaperdollItem( slot, null )
        this.itemTrackers.delete( changedItems )

        changedItems.forEach( this.onItemUnEquipped.bind( this ) )
    }

    async unEquipSlots( slots: Array<InventorySlot> ) : Promise<void> {
        let changedItems : Set<L2ItemInstance> = new Set<L2ItemInstance>()

        this.itemTrackers.add( changedItems )
        await Promise.all( slots.map( ( slot: InventorySlot ) => this.setPaperdollItem( slot, null ) ) )
        this.itemTrackers.delete( changedItems )

        changedItems.forEach( this.onItemUnEquipped.bind( this ) )
    }

    validateWeightForItem( item: L2ItemInstance, amount: number ): boolean {
        let template: L2Item = DataManager.getItems().getTemplate( item.getId() )
        if ( !template ) {
            return false
        }

        return this.validateWeight( template.getWeight() * amount )
    }

    deleteMe(): Promise<void> {
        this.refreshWeight.cancel()

        return super.deleteMe()
    }

    private onItemUnEquipped( item: L2ItemInstance ) {
        item.unChargeAllShots()
        this.markItem( item, InventoryUpdateStatus.Modified )
    }

    haveItemForSelfResurrection(): boolean {
        for ( const itemId of selfResurrectionItemIds ) {
            if ( this.hasItemWithId( itemId ) ) {
                return true
            }
        }

        return false
    }
}