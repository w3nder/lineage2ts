import { PaperdollListener } from '../PaperdollListener'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { Inventory } from '../Inventory'

export const StatsListener : PaperdollListener = {
    isForItem( item: L2ItemInstance ): boolean {
        return item.getItem().hasStatFunctions()
    },

    async notifyEquiped( slot: number, item: L2ItemInstance, inventory: Inventory ) : Promise<void> {
        return inventory.getOwner().addStatFunctions( item.getStatFunctions(), true )
    },

    async notifyUnequiped( slot: number, item: L2ItemInstance, inventory: Inventory ) : Promise<void> {
        return inventory.getOwner().removeStatsOwner( item )
    }
}