import { PaperdollListener } from '../PaperdollListener'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { Inventory } from '../Inventory'
import { InventorySlot } from '../../../values/InventoryValues'
import { L2ItemSlots } from '../../../enums/L2ItemSlots'

const decorationSlots : Array<InventorySlot> = [
    InventorySlot.Decoration1,
    InventorySlot.Decoration2,
    InventorySlot.Decoration3,
    InventorySlot.Decoration4,
    InventorySlot.Decoration5,
    InventorySlot.Decoration6
]

export const BraceletListener: PaperdollListener = {
    isForItem( item: L2ItemInstance ): boolean {
        return item.getItem().getBodyPart() === L2ItemSlots.RightBracelet
    },

    notifyEquiped: () => Promise.resolve(),

    notifyUnequiped( slot: number, item: L2ItemInstance, inventory: Inventory ): Promise<void> {
        /*
            Avoiding potentially costly operation on inventory when there are no items to be removed.
         */
        if ( decorationSlots.some( slot => !!inventory.getPaperdollItem( slot ) ) ) {
            return inventory.unEquipSlots( decorationSlots )
        }
    }
}