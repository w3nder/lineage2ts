import { PaperdollListener } from '../PaperdollListener'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { Inventory } from '../Inventory'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { Skill } from '../../Skill'
import { L2Item } from '../../items/L2Item'
import { SkillCoolTime } from '../../../packets/send/SkillCoolTime'

export const ItemSkillsListener: PaperdollListener = {
    isForItem(): boolean {
        return true
    },

    async notifyEquiped( slot: number, item: L2ItemInstance, inventory: Inventory ) : Promise<void> {
        let player = inventory.getOwner() as L2PcInstance

        if ( item.isAugmented() ) {
            item.getAugmentation().applyBonus( player )
        }

        await item.rechargeShots( true, true )
        item.updateElementAttributesBonus( player )

        let sendUpdate = false
        let updateCooldown = false
        let currentItem: L2Item = item.getItem()

        if ( item.getEnchantLevel() >= 4 ) {
            let skill = currentItem.getEnchant4Skill()

            if ( skill ) {
                await player.addSkill( skill, false )
                sendUpdate = true
            }
        }

        if ( item.shouldApplyEnchantStats() ) {
            await item.applyEnchantStats()
        }

        if ( currentItem.hasSkills() ) {
            sendUpdate = true
            await Promise.all( currentItem.getSkills().map( ( skill: Skill ) : Promise<Skill> => {
                if ( skill.isActive() ) {
                    if ( !player.hasSkillActiveReuse( skill ) ) {
                        let equipDelay = item.getEquipReuseDelay()
                        if ( equipDelay > 0 ) {
                            player.addSkillReuse( skill, equipDelay )
                            player.disableSkill( skill, equipDelay )
                        }
                    }

                    updateCooldown = true
                }

                return player.addSkill( skill, false )
            } ) )
        }

        if ( sendUpdate ) {
            player.sendSkillList()

            if ( updateCooldown ) {
                player.sendDebouncedPacket( SkillCoolTime )
            }
        }
    },

    async notifyUnequiped( slot: number, item: L2ItemInstance, inventory: Inventory ): Promise<void> {
        let player = inventory.getOwner() as L2PcInstance

        let currentItem: L2Item = item.getItem()
        let update: boolean = false
        let updateCooldown: boolean = false

        if ( item.isAugmented() ) {
            item.getAugmentation().removeBonus( player )
        }

        item.unChargeAllShots()
        item.removeElementAttributesBonus( player )

        if ( item.getEnchantLevel() >= 4 ) {
            let enchant4Skill = currentItem.getEnchant4Skill()

            if ( enchant4Skill ) {
                await player.removeSkill( enchant4Skill, false, enchant4Skill.isPassive() )
                update = true
            }
        }

        if ( item.hasEnchantStats() ) {
            await item.clearEnchantStats()
        }

        if ( currentItem.hasSkills() ) {
            update = true
            await Promise.all( currentItem.getSkills().map( skill => player.removeSkill( skill, false, skill.isPassive() ) ) )
        }

        if ( item.isArmor() ) {
            let promises : Array<Promise<Skill>> = inventory.getItems().reduce( ( allPromises: Array<Promise<Skill>>, armorItem: L2ItemInstance ) : Array<Promise<Skill>> => {
                if ( !armorItem.isEquipped() || !armorItem.getItem().hasSkills() || armorItem === item ) {
                    return allPromises
                }

                armorItem.getItem().getSkills().forEach( ( skill : Skill ) : void => {
                    if ( player.getSkillLevel( skill.getId() ) !== -1 ) {
                        return
                    }

                    allPromises.push( player.addSkill( skill ) )

                    if ( skill.isActive() ) {
                        if ( !player.hasSkillActiveReuse( skill ) ) {
                            let equipDelay = item.getEquipReuseDelay()
                            if ( equipDelay > 0 ) {
                                player.addSkillReuse( skill, equipDelay )
                                player.disableSkill( skill, equipDelay )
                            }
                        }
                        updateCooldown = true
                    }
                }, [] )

                return allPromises
            }, [] )

            if ( promises.length > 0 ) {
                update = true
                await Promise.all( promises )
            }
        }

        let unequipSkill: Skill = currentItem.getUnequipSkill()
        if ( unequipSkill ) {
            await unequipSkill.activateCasterSkill( player, [ player ], true )
        }

        if ( update ) {
            player.sendSkillList()

            if ( updateCooldown ) {
                player.sendDebouncedPacket( SkillCoolTime )
            }
        }
    }
}