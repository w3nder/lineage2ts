import { PaperdollListener } from '../PaperdollListener'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { Inventory } from '../Inventory'
import { WeaponType } from '../../items/type/WeaponType'
import { InventorySlot } from '../../../values/InventoryValues'

export const BowCrossRodListener : PaperdollListener = {
    isForItem( item: L2ItemInstance ): boolean {
        if ( item.getLocationSlot() !== InventorySlot.RightHand ) {
            return false
        }

        return item.getItemType() === WeaponType.BOW
                || item.getItemType() === WeaponType.CROSSBOW
                || item.getItemType() === WeaponType.FISHINGROD
    },

    async notifyEquiped( slot: number, item: L2ItemInstance, inventory: Inventory ) : Promise<void> {
        if ( slot !== InventorySlot.RightHand ) {
            return
        }

        if ( item.getItemType() === WeaponType.BOW ) {
            let arrow : L2ItemInstance = inventory.findArrowForBow( item.getItem() )

            if ( arrow ) {
                await inventory.setPaperdollItem( InventorySlot.LeftHand, arrow )
            }

            return
        }

        if ( item.getItemType() === WeaponType.CROSSBOW ) {
            let bolts : L2ItemInstance = inventory.findBoltForCrossBow( item.getItem() )

            if ( bolts ) {
                await inventory.setPaperdollItem( InventorySlot.LeftHand, bolts )
            }
        }
    },

    async notifyUnequiped( slot: number, item: L2ItemInstance, inventory: Inventory ) : Promise<void> {
        if ( slot !== InventorySlot.RightHand ) {
            return
        }

        if ( item.getItemType() === WeaponType.BOW ) {
            let arrow : L2ItemInstance = inventory.getPaperdollItem( InventorySlot.LeftHand )

            if ( arrow ) {
                await inventory.setPaperdollItem( InventorySlot.LeftHand, null )
            }

            return
        }

        if ( item.getItemType() === WeaponType.CROSSBOW ) {
            let bolts : L2ItemInstance = inventory.getPaperdollItem( InventorySlot.LeftHand )

            if ( bolts ) {
                await inventory.setPaperdollItem( InventorySlot.LeftHand, null )
            }

            return
        }

        if ( item.getItemType() === WeaponType.FISHINGROD ) {
            let lure : L2ItemInstance = inventory.getPaperdollItem( InventorySlot.LeftHand )

            if ( lure ) {
                await inventory.setPaperdollItem( InventorySlot.LeftHand, null )
            }

            return
        }
    }
}