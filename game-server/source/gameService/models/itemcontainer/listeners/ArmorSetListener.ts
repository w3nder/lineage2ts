import { PaperdollListener } from '../PaperdollListener'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { Inventory } from '../Inventory'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { DataManager } from '../../../../data/manager'
import { L2ArmorSet } from '../../L2ArmorSet'
import { Skill } from '../../Skill'
import { SkillCoolTime } from '../../../packets/send/SkillCoolTime'
import { InventorySlot } from '../../../values/InventoryValues'
import _ from 'lodash'

export const ArmorSetListener: PaperdollListener = {
    isForItem( item: L2ItemInstance ): boolean {
        return item.isArmor() && !!DataManager.getArmorSetsData().getArmorSet( item.getId() )
    },

    async notifyEquiped( slot: InventorySlot, item: L2ItemInstance, inventory: Inventory ): Promise<void> {
        let chestItem: L2ItemInstance = inventory.getPaperdollItem( InventorySlot.Chest )
        if ( !chestItem ) {
            return
        }

        let armorSet: L2ArmorSet = DataManager.getArmorSetsData().getArmorSet( chestItem.getId() )
        if ( !armorSet ) {
            return
        }

        let updateTimeStamp = false

        let player = inventory.getOwner() as L2PcInstance
        let skillPromises : Array<Promise<Skill>> = []
        let shieldSkill = armorSet.getShieldSkill()

        if ( armorSet.containItem( slot, item.getId() ) ) {
            if ( armorSet.containAll( player ) ) {

                let setSkill = armorSet.getSetSkill()
                if ( setSkill ) {
                    skillPromises.push( player.addSkill( setSkill ) )

                    if ( setSkill.isActive() ) {
                        if ( !player.hasSkillActiveReuse( setSkill ) ) {
                            let equipDelay = item.getEquipReuseDelay()
                            if ( equipDelay > 0 ) {
                                player.addSkillReuse( setSkill, equipDelay )
                                player.disableSkill( setSkill, equipDelay )
                            }
                        }
                        updateTimeStamp = true
                    }
                }

                if ( shieldSkill && armorSet.containShieldByPlayer( player ) ) {
                    skillPromises.push( player.addSkill( shieldSkill ) )
                }

                let enchant6Skill = armorSet.getEnchant6Skill()
                if ( enchant6Skill && armorSet.isEnchanted6( player ) ) {
                    skillPromises.push( player.addSkill( enchant6Skill ) )
                }

                if ( skillPromises.length > 0 ) {
                    await Promise.all( skillPromises )
                }
            }
        } else if ( shieldSkill && armorSet.containShieldById( item.getId() ) ) {
            skillPromises.push( player.addSkill( shieldSkill ) )
        }

        if ( skillPromises.length > 0 ) {
            await Promise.all( skillPromises )

            player.sendSkillList()

            if ( updateTimeStamp ) {
                player.sendDebouncedPacket( SkillCoolTime )
            }
        }
    },

    async notifyUnequiped( slot: InventorySlot, item: L2ItemInstance, inventory: Inventory ): Promise<void> {
        let player = inventory.getOwner() as L2PcInstance
        let skills: Readonly<Array<Skill>>

        if ( slot === InventorySlot.Chest ) {
            let armorSet: L2ArmorSet = DataManager.getArmorSetsData().getArmorSet( item.getId() )
            if ( !armorSet ) {
                return
            }

            skills = _.compact( [
                armorSet.getSetSkill(),
                armorSet.getShieldSkill(),
                armorSet.getEnchant6Skill() ] )
        } else {
            let chestItem: L2ItemInstance = inventory.getPaperdollItem( InventorySlot.Chest )
            if ( !chestItem ) {
                return
            }

            let armorSet: L2ArmorSet = DataManager.getArmorSetsData().getArmorSet( chestItem.getId() )
            if ( !armorSet ) {
                return
            }

            let shieldSkill = armorSet.getShieldSkill()
            if ( armorSet.containItem( slot, item.getId() ) ) {
                skills = _.compact( [
                    armorSet.getSetSkill(),
                    shieldSkill,
                    armorSet.getEnchant6Skill() ] )
            } else if ( shieldSkill && armorSet.containShieldById( item.getId() ) ) {
                skills = [ shieldSkill ]
            }
        }

        if ( !skills || skills.length === 0 ) {
            return
        }

        await Promise.all( skills.map( skill => player.removeSkill( skill, false, skill.isPassive() ) ) )

        player.sendSkillList()
        return player.checkItemRestriction()
    },
}