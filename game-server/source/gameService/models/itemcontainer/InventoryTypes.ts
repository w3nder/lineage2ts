export enum InventoryInstanceType {
    NONE,
    PcInventory,
    PetInventory
}