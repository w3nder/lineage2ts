import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { Inventory } from './Inventory'
import { InventorySlot } from '../../values/InventoryValues'

export interface PaperdollListener {
    notifyEquiped( slot: InventorySlot, item: L2ItemInstance, inventory: Inventory ) : Promise<void>
    notifyUnequiped( slot: InventorySlot, item: L2ItemInstance, inventory: Inventory ) : Promise<void>
    isForItem( item: L2ItemInstance ) : boolean
}