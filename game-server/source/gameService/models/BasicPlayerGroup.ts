import { L2PcInstance } from './actor/instance/L2PcInstance'
import { PlayerGroupType } from '../enums/PlayerGroupType'

let nextPartyId : number = 1

function getPartyId() : number {
    return nextPartyId++
}

export abstract class BasicPlayerGroup {
    id: number

    abstract getMembers() : Array<number>

    getLeader() : L2PcInstance {
        return null
    }

    getLeaderObjectId() : number {
        return null
    }

    abstract setLeader( leader: L2PcInstance ) : void

    isLeader( player: L2PcInstance ) {
        return this.getLeaderObjectId() === player.getObjectId()
    }

    getMemberCount() {
        return this.getMembers().length
    }

    getType() : PlayerGroupType {
        return PlayerGroupType.None
    }

    getId() : number {
        return this.id
    }

    protected refreshId() : void {
        this.id = getPartyId()
    }
}