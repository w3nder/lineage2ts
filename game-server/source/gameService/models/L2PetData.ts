import { L2PetLevelData } from './L2PetLevelData'
import { PetSkills } from './pet/PetSkills'

export class L2PetData extends PetSkills {
    levelStats: { [ key: number ]: L2PetLevelData } = {}
    npcId: number
    itemId: number
    hungryLimit: number = 1
    syncLevel: boolean = false
    food: Array<number> = []
    minimumLevel : number = 0
    highestLevel : number = 0

    constructor( id: number, itemId: number ) {
        super()

        this.npcId = id
        this.itemId = itemId
    }

    isSyncLevel(): boolean {
        return this.syncLevel
    }

    getHungryLimit() {
        return this.hungryLimit
    }

    getPetLevelData( level: number ) {
        return this.levelStats[ Math.min( level, this.highestLevel ) ]
    }

    getFood(): Array<number> {
        return this.food
    }

    getNpcId() {
        return this.npcId
    }
}