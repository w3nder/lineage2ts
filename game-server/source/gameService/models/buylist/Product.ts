import { L2Item } from '../items/L2Item'
import { BuyListManager } from '../../cache/BuyListManager'
import { ConfigManager } from '../../../config/ConfigManager'

export class Product {
    id: number
    listId: number
    item: L2Item
    price: number
    restockDelay: number
    maxCount: number
    count: number
    futureRestockTime: number
    limitedStock: boolean
    isMercenaryTicket: boolean

    constructor( productId: number, listId: number, item: L2Item, price: number, restockDelay: number, maxCount: number ) {
        this.id = productId
        this.listId = listId
        this.item = item
        this.price = price < 0 ? this.item.getReferencePrice() : price

        this.restockDelay = restockDelay * 60000
        this.maxCount = maxCount
        this.limitedStock = this.maxCount > -1
        this.isMercenaryTicket = this.item.getId() >= 3960 && this.item.getId() <= 4026

        this.count = this.limitedStock ? maxCount : 0
        this.futureRestockTime = 0
    }

    hasLimitedStock() {
        return this.limitedStock
    }

    getItem() : L2Item {
        return this.item
    }

    getItemId() : number {
        return this.item.getId()
    }

    getComputedPrice() : number {
        if ( this.isMercenaryTicket ) {
            return Math.min( 1, ConfigManager.rates.getRateSiegeGuardsPrice() * this.price )
        }

        return this.price
    }

    decreaseCount( amount: number ) : boolean {
        if ( !this.limitedStock ) {
            return true
        }

        if ( this.count <= 0 || amount > this.count ) {
            return false
        }

        this.count -= amount

        if ( !this.futureRestockTime ) {
            this.scheduleRestock()
        }

        return this.count >= 0
    }

    private scheduleRestock() : void {
        let delay = Math.floor( this.restockDelay * ConfigManager.tuning.getBuylistProductRestockMultiplier() )

        if ( delay <= 0 ) {
            return this.restockAll()
        }

        this.futureRestockTime = Date.now() + delay
        BuyListManager.scheduleProductUpdate( this.id )
    }

    restockAll() : void {
        this.count = this.maxCount
        this.futureRestockTime = 0
    }

    restockFully() {
        this.restockAll()
        BuyListManager.scheduleProductUpdate( this.id )
    }

    attemptToRestock( time: number ) : void {
        if ( this.shouldRestock( time ) ) {
            return this.restockFully()
        }
    }

    shouldRestock( time: number ) : boolean {
        return this.futureRestockTime > 0 && this.futureRestockTime <= time
    }
}