import { Product } from './Product'
import { ConfigManager } from '../../../config/ConfigManager'

export class L2BuyList {
    listId: number
    itemIdToProduct: { [ key: number ]: Product }
    allowedNpcId: number
    products: Set<Product>

    constructor( id: number ) {
        this.listId = id
    }

    getListId() : number {
        return this.listId
    }

    getProducts() : Set<Product> {
        return this.products
    }

    getProductByItemId( id: number ) : Product {
        return this.itemIdToProduct[ id ]
    }

    isNpcAllowed( id: number ) : boolean {
        return this.allowedNpcId === id
    }
}

export class L2DisabledBuyList extends L2BuyList {
    isNpcAllowed( id: number ): boolean {
        return ConfigManager.npc.getDisabledMerchantIds().includes( id )
    }
}