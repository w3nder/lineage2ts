import { Skill } from './Skill'

export interface ClanPledge {
    type: number
    name: string
    leaderId: number
    skills: Array<Skill>
}