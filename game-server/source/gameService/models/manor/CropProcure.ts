import { SeedProduction } from './SeedProduction'

export class CropProcure extends SeedProduction {
    rewardType: number

    constructor( cropId: number, amount: number, type: number, startAmount: number, price: number ) {
        super( cropId, amount, price, startAmount )
        this.rewardType = type
    }

    getReward() {
        return this.rewardType
    }
}