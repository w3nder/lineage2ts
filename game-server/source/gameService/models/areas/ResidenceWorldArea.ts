import { L2WorldArea } from './WorldArea'
import { ResidenceTeleportType } from '../../enums/ResidenceTeleportType'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { Location } from '../Location'
import _ from 'lodash'
import { L2AreaItem } from '../../../data/interface/AreaDataApi'

interface ResidenceAreaProperties {
    residenceId: number
    name: string
    teleports: Record<string, Array<Location>>
}

export abstract class L2ResidenceWorldArea extends L2WorldArea {
    properties: ResidenceAreaProperties

    teleportPlayers( type: ResidenceTeleportType, predicate: ( player: L2PcInstance ) => boolean ) : Promise<Array<Awaited<void>>> {
        let teleportPromises : Array<Promise<void>> = []

        L2World.forPlayersByBox( this.getSpatialIndex(), ( player: L2PcInstance ) => {
            if ( !predicate( player ) ) {
                return
            }

            // TODO: use party teleport geometry per location
            let location = this.getRandomTeleportLocation( type )
            teleportPromises.push( player.teleportToLocation( location ) )

            let summon = player.getSummon()
            if ( summon ) {
                teleportPromises.push( summon.teleportToLocation( location, true ) )
            }
        } )

        return Promise.all( teleportPromises )
    }

    banishOutsiders( clanId: number ) : Promise<unknown> {
        return this.teleportPlayers( ResidenceTeleportType.Banishment, ( player: L2PcInstance ) => {
            return player.getClanId() !== clanId
        } )
    }

    getRandomTeleportLocation( type: ResidenceTeleportType ) : Location {
        return _.sample( this.properties.teleports[ type ] )
    }

    abstract loadProperties( data: L2AreaItem ) : void

    getResidenceId() : number {
        return this.properties.residenceId
    }
}