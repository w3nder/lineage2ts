export enum WorldAreaActions {
    Enter,
    Exit,
    Death,
    Activation,
    Deactivation,
    RegionActivation
}