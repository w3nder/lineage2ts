import { EnterExitActions, L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { WorldAreaActions } from '../WorldAreaActions'
import { L2Object } from '../../L2Object'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'

export class PVPArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.PVP

    protected getSupportedActions(): ReadonlySet<WorldAreaActions> {
        return EnterExitActions
    }

    canAffectObject( object: L2Object ): boolean {
        return object.isPlayer() && !( object as L2PcInstance ).isInArea( AreaType.PVP )
    }

    onEnter( object: L2Object ) {
        ( object as L2PcInstance ).sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ENTERED_COMBAT_ZONE ) )
    }

    onExit( object: L2Object ) {
        let player = object as L2PcInstance
        if ( !player.isInArea( AreaType.PVP ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LEFT_COMBAT_ZONE ) )
        }
    }
}