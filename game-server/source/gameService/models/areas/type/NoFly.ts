import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

interface NoFlyAreaProperties {
    messageId: number
}

export class NoFlyArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.NoFly
    properties : NoFlyAreaProperties

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            messageId: data.properties.messageId as number
        }
    }
}