import { AreaType } from '../AreaType'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'
import { ResidenceTeleportType } from '../../../enums/ResidenceTeleportType'
import _ from 'lodash'
import { createTeleportLocation } from '../WorldArea'
import { L2ResidenceWorldArea } from '../ResidenceWorldArea'

export class FortArea extends L2ResidenceWorldArea {
    type: Readonly<AreaType> = AreaType.Fort

    loadProperties( data: L2AreaItem ) : void {
        this.properties = {
            name: data.properties.name as string,
            residenceId: data.properties.fortId as number,
            teleports: {
                [ ResidenceTeleportType.Banishment ]: _.map( data.properties.teleports[ 'banish' ], createTeleportLocation ),
                [ ResidenceTeleportType.PVP ]: _.map( data.properties.teleports[ 'chaotic' ], createTeleportLocation ),
                [ ResidenceTeleportType.Normal ]: _.map( data.properties.teleports[ 'normal' ], createTeleportLocation ),
                [ ResidenceTeleportType.Other ]: _.map( data.properties.teleports[ 'other' ], createTeleportLocation ),
            }
        }
    }
}