import { AreaType } from '../AreaType'
import { L2WorldArea } from '../WorldArea'

export class L2UnknownArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.Unknown
}