import { createTeleportLocation } from '../WorldArea'
import { AreaType } from '../AreaType'
import { ResidenceTeleportType } from '../../../enums/ResidenceTeleportType'
import _ from 'lodash'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'
import { L2ResidenceWorldArea } from '../ResidenceWorldArea'

export class CastleArea extends L2ResidenceWorldArea {
    type: Readonly<AreaType> = AreaType.Castle

    loadProperties( data: L2AreaItem ) : void {
        this.properties = {
            name: data.properties.name as string,
            residenceId: data.properties.castleId as number,
            teleports: {
                [ ResidenceTeleportType.Banishment ]: _.map( data.properties.teleports[ 'banish' ], createTeleportLocation ),
                [ ResidenceTeleportType.PVP ]: _.map( data.properties.teleports[ 'chaotic' ], createTeleportLocation ),
                [ ResidenceTeleportType.Normal ]: _.map( data.properties.teleports[ 'normal' ], createTeleportLocation ),
                [ ResidenceTeleportType.Other ]: _.map( data.properties.teleports[ 'other' ], createTeleportLocation ),
            }
        }
    }
}