import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { Race, ReverseRaceMapping } from '../../../enums/Race'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

export class RespawnArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.Respawn
    properties : Record<Partial<Race>, string>

    getRestartName( player: L2PcInstance ) : string {
        let name = this.properties[ ReverseRaceMapping[ player.getRace() ] ]
        if ( name ) {
            return name
        }

        return this.properties[ 'anyRace' ]
    }

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            ...data.properties as Record<Partial<Race>, string>
        }
    }
}