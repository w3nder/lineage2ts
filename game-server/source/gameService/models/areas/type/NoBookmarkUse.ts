import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'

export class NoBookmarkUseArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.NoBookmarkUse
}