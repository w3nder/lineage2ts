import { EnterExitActions, L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { WorldAreaActions } from '../WorldAreaActions'
import { L2Object } from '../../L2Object'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { ServerObjectInfo } from '../../../packets/send/ServerObjectInfo'
import { NpcInfo } from '../../../packets/send/NpcInfo'
import { L2World } from '../../../L2World'
import { PacketDispatcher } from '../../../PacketDispatcher'
import { L2Npc } from '../../actor/L2Npc'

export class WaterArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.Water

    protected getSupportedActions(): ReadonlySet<WorldAreaActions> {
        return EnterExitActions
    }

    onEnter( object: L2Object ) : void {
        if ( object.isPlayer() ) {
            let player: L2PcInstance = object as L2PcInstance
            if ( player.isTransformed() && !player.getTransformation().canSwim() ) {
                player.stopTransformation( true )
                return
            }

            return player.broadcastUserInfo()
        }

        return this.notifyAboutNpc( object )
    }

    onExit( object: L2Object ) : void {
        if ( object.isPlayer() ) {
            return ( object as L2PcInstance ).broadcastUserInfo()
        }

        return this.notifyAboutNpc( object )
    }

    notifyAboutNpc( object: L2Object ) {
        if ( !object.isNpc() ) {
            return
        }

        let packetMethod = ( object as L2Npc ).getRunSpeed() === 0 ? ServerObjectInfo : NpcInfo
        L2World.getAllVisiblePlayerIds( object, object.getBroadcastRadius() ).forEach( ( playerId: number ) => {
            PacketDispatcher.sendOwnedData( playerId, packetMethod( object.getObjectId(), playerId ) )
        } )
    }

    canAffectObject( object: L2Object ): boolean {
        return object.isPlayer() || object.isNpc()
    }
}