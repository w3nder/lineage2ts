import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'

export class GrandBossArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.Boss
}