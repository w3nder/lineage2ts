import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'

export class NoBookmarkSaveArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.NoBookmarkSave
}