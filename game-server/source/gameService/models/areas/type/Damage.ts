import { ActivationActions, L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { WorldAreaActions } from '../WorldAreaActions'
import { L2Object } from '../../L2Object'
import { L2World } from '../../../L2World'
import { L2Playable } from '../../actor/L2Playable'
import { Stats } from '../../stats/Stats'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

/*
    Damage areas represent simple reduction of hp or mp values, based
    on interval. However, virtually all of them are in disabled mode and
    require additional integration to enable its usage.
    Virtually all areas are associated with siege, hence are event based.

    TODO : add listener integration for siege start per area name
    TODO : add zones to grid only if siege starts, remove when siege ends
    TODO : review devil_damage areas since these employ time duration logic
 */

interface DamageAreaProperties {
    intervalMs: number
    messageId: number
    damageOnMp: number
    damageOnHp: number
    isEnabled: boolean
}

export class DamageArea extends L2WorldArea {
    type: Readonly<AreaType>
    interval: NodeJS.Timeout
    properties: DamageAreaProperties

    protected getSupportedActions(): ReadonlySet<WorldAreaActions> {
        return ActivationActions
    }

    onActivation() {
        if ( this.interval ) {
            return
        }

        this.interval = setInterval( this.runDamagePlayable.bind( this ), this.properties.intervalMs )
    }

    onDeactivation() {
        if ( !this.interval ) {
            return
        }

        clearInterval( this.interval )
        this.interval = null
    }

    runDamagePlayable() : void {
        L2World.forObjectsByBox( this.getSpatialIndex(), ( object: L2Object ) => {
            if ( !object.isPlayable() ) {
                return
            }

            let playable = object as L2Playable

            if ( playable.isDead() ) {
                return
            }

            let multiplier = 1 + ( playable.calculateStat( Stats.DAMAGE_ZONE_VULN, 0, null, null ) / 100 )

            if ( this.properties.damageOnHp > 0 ) {
                playable.reduceCurrentHp( this.properties.damageOnHp * multiplier, playable, null )
            }

            if ( this.properties.damageOnMp > 0 ) {
                playable.reduceCurrentMp( this.properties.damageOnMp * multiplier )
            }
        } )
    }

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            damageOnHp: data.properties.damageOnHp as number ?? 0,
            damageOnMp: data.properties.damageOnMp as number ?? 0,
            intervalMs: data.properties.unitTick ? data.properties.unitTick as number * 1000 : 5000,
            messageId: data.properties.messageNo as number,
            isEnabled: data.properties.isEnabled === true
        }
    }

    addToInitialGrid(): boolean {
        return this.properties.isEnabled
    }
}