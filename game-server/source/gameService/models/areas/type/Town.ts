import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

interface TownAreaProperties {
    townId: number
    taxById: number
    name: string
}

export class TownArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.Town
    properties: TownAreaProperties

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            townId: data.properties.townId as number,
            taxById: data.properties.taxById as number,
            name: data.properties.name as string
        }
    }
}