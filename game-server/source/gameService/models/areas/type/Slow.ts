import { EnterExitActions, L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { WorldAreaActions } from '../WorldAreaActions'
import { L2Object } from '../../L2Object'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { SiegeRole } from '../../../enums/SiegeRole'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'

/*
    Swamp areas are used for slowing down player characters. As with
    damage areas, these are used together with siege.

    TODO : add listener integration to add/remove zones from grid
 */

interface SlowAreaProperties {
    isEnabled: boolean
    eventId: number
    moveBonus: number
}

export class SlowArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.Slow
    properties: SlowAreaProperties

    protected getSupportedActions(): ReadonlySet<WorldAreaActions> {
        return EnterExitActions
    }

    canAffectObject( object: L2Object ): boolean {
        return object.isPlayer()
    }

    onEnter( object: L2Object ) {
        let player = object as L2PcInstance
        if ( player.isInSiege() && player.getSiegeRole() === SiegeRole.Defender ) {
            return
        }

        player.broadcastUserInfo()
    }

    onExit( object: L2Object ) {
        ( object as L2PcInstance ).broadcastUserInfo()
    }

    loadProperties( data: L2AreaItem ) {
        this.properties = {
            isEnabled: data.properties.isEnabled === true,
            eventId: data.properties.eventId as number ?? 0,
            moveBonus: data.properties.moveBonus as number ?? 0
        }
    }

    addToInitialGrid(): boolean {
        return this.properties.isEnabled
    }
}