import { L2WorldArea } from '../WorldArea'
import { AreaType } from '../AreaType'
import { Location } from '../../Location'
import { L2AreaItem } from '../../../../data/interface/AreaDataApi'
import { teleportAreaPlayersToCoordinates } from '../../../helpers/TeleportHelper'
import { LocationProperties } from '../../LocationProperties'

interface CastleTeleportAreaProperties {
    name: string
    residenceId: number
    banishLocation: LocationProperties
}

export class CastleTeleportArea extends L2WorldArea {
    type: Readonly<AreaType> = AreaType.CastleTeleport
    properties: CastleTeleportAreaProperties

    loadProperties( data: L2AreaItem ) {
        let location = data.properties.teleports[ 'normal' ]

        this.properties = {
            name: data.properties.name as string,
            residenceId: data.properties.castleId as number,
            banishLocation: new Location( location.x, location.y, location.z )
        }
    }

    banishAllPlayers() : Promise<void> {
        let location = this.properties.banishLocation
        return teleportAreaPlayersToCoordinates( this, location.x, location.y, location.z )
    }
}