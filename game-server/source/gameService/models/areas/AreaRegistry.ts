import { AreaForm } from './AreaForm'
import { L2WorldArea } from './WorldArea'
import { WaterArea } from './type/Water'
import { PVPArea } from './type/PVP'
import { PeaceArea } from './type/Peace'
import { NoRestartArea } from './type/NoRestart'
import { BlessingArea } from './type/Blessing'
import { MotherTreeArea } from './type/MotherTree'
import { TownArea } from './type/Town'
import { DamageArea } from './type/Damage'
import { SlowArea } from './type/Slow'
import { SkillEffectArea } from './type/SkillEffect'
import { NoFlyArea } from './type/NoFly'
import { NoSummonPlayerArea } from './type/NoSummonPlayer'
import { NoItemDropArea } from './type/NoItemDrop'
import { NoBookmarkSaveArea } from './type/NoBookmarkSave'
import { NoBookmarkUseArea } from './type/NoBookmarkUse'
import { TeleportArea } from './type/Teleport'
import { AirshipLandingArea } from './type/AirshipLanding'
import { GrandBossArea } from './type/GrandBoss'
import { SevenSignsArea } from './type/SevenSigns'
import { CastleArea } from './type/Castle'
import { DominionArea } from './type/Dominion'
import { CastleHqArea } from './type/CastleHq'
import { FortSiegeArea } from './type/FortSiege'
import { ClanHallArea } from './type/ClanHall'
import { RespawnArea } from './type/Respawn'
import { SkillReferenceArea } from './type/SkillReference'
import { CastleSiegeArea } from './type/CastleSiege'
import { CastleTeleportArea } from './type/CastleTeleport'
import { FortArea } from './type/Fort'
import { JailArea } from './type/Jail'
import { OlympiadStadiumArea } from './type/OlympiadStadium'
import { QuestArea } from './type/Quest'
import { L2AreaItem } from '../../../data/interface/AreaDataApi'

export type AreaCreator = new ( id: number, form: AreaForm, data: L2AreaItem ) => L2WorldArea

export const AreaRegistry : Record<string, AreaCreator> = {
    water: WaterArea,
    battleZone: PVPArea,
    peaceZone: PeaceArea,
    noRestart: NoRestartArea,
    bless: BlessingArea,
    motherTree: MotherTreeArea,
    noFly: NoFlyArea,
    noSummonPlayer: NoSummonPlayerArea,
    noItemDrop: NoItemDropArea,
    noBookmarkSave: NoBookmarkSaveArea,
    noBookmarkUse: NoBookmarkUseArea,
    town: TownArea,
    damage: DamageArea,
    swamp: SlowArea,
    poison: SkillEffectArea,
    instantSkill: SkillEffectArea,
    teleport: TeleportArea, // TODO : figure out if teleport happens from instance to normal world location
    landing: AirshipLandingArea,
    dragonLair: GrandBossArea,
    ssqZone: SevenSignsArea,
    dominion: DominionArea,
    castleHq: CastleHqArea,
    fort: FortArea,
    fortSiege: FortSiegeArea,
    clanHall: ClanHallArea,
    respawn: RespawnArea,
    skillReference: SkillReferenceArea,
    castle: CastleArea,
    castleSiege: CastleSiegeArea,
    castleTeleport: CastleTeleportArea,
    jail: JailArea,
    olympiadStadium: OlympiadStadiumArea,
    quest: QuestArea
}