import {
    Box,
    Polyline,
    polylineClose,
    polylineContainsPointInside,
    polylineGetBounds,
    polylineIsClosed,
    polylineNearestDistanceSqToPoint
} from 'math2d'

export class AreaForm {
    polygon: Polyline
    polygonBoundingBox: Box
    minimumZ: number
    maximumZ: number

    constructor( coordinates: Polyline, minZ: number, maxZ: number ) {
        this.polygon = coordinates
        this.minimumZ = Math.min( minZ, maxZ )
        this.maximumZ = Math.max( minZ, maxZ )

        if ( !polylineIsClosed( this.polygon ) ) {
            this.polygon = polylineClose( this.polygon )
        }

        this.polygonBoundingBox = polylineGetBounds( this.polygon )
    }

    getBoundingRectangle(): Readonly<Box> {
        return this.polygonBoundingBox
    }

    getDistanceToPoint( x: number, y: number ): number {
        let result = polylineNearestDistanceSqToPoint( this.polygon, { x, y } )
        return result.distanceValue
    }

    isPointInside( x: number, y: number, z: number ): boolean {
        return polylineContainsPointInside( this.polygon, { x, y } ) && ( z >= this.minimumZ && z <= this.maximumZ )
    }
}

function getPolylineCoordinates( maxX: number, maxY: number, minX: number, minY: number ): Array<number> {
    /*
    See documentation about polyline coordinates. These are sequence of x, y, x, y etc... numbers.
     */
    return [
        minX,
        minY,
        maxX,
        minY,
        maxX,
        maxY,
        minX,
        maxY
    ]
}

export class SquareAreaForm extends AreaForm {
    constructor( oneX: number, oneY: number, twoX: number, twoY: number, minZ: number, maxZ: number ) {
        let maxX = Math.max( oneX, twoX )
        let maxY = Math.max( oneY, twoY )
        let minX = Math.min( oneX, twoX )
        let minY = Math.min( oneY, twoY )

        super( getPolylineCoordinates( maxX, maxY, minX, minY ), minZ, maxZ )

        this.polygonBoundingBox = {
            maxX,
            maxY,
            minX,
            minY,
        }
    }
}