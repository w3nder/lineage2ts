import { DataManager } from '../../data/manager'
import { EnchantSkillHolder, L2EnchantSkillGroup } from './L2EnchantSkillGroup'
import _ from 'lodash'

export class L2EnchantSkillLearn {
    id: number
    baseLevel: number
    enchantRoutes: { [ key: number ]: number } = {}

    constructor( id: number, level: number ) {
        this.id = id
        this.baseLevel = level
    }

    getBaseLevel() {
        return this.baseLevel
    }

    addEnchantRoute( route: number, group: number ) {
        this.enchantRoutes[ route ] = group
    }

    getFirstRouteGroup() {
        return DataManager.getSkillData().getEnchantSkillGroupById( _.head( _.values( this.enchantRoutes ) ) )
    }

    getAllRoutes() : Array<number> {
        return _.map( _.keys( this.enchantRoutes ), _.parseInt )
    }

    isMaxEnchant( level: number ) {
        let enchantType = this.getEnchantRoute( level )
        if ( ( enchantType < 1 ) || !this.enchantRoutes[ enchantType ] ) {
            return false
        }

        let index = this.getEnchantIndex( level )

        return ( index + 1 ) >= DataManager.getSkillData().getEnchantSkillGroupById( this.enchantRoutes[ enchantType ] ).getEnchantGroupDetails().length
    }

    getEnchantRoute( level: number ) : number {
        return Math.floor( level / 100 )
    }

    getEnchantIndex( level: number ) {
        return ( level % 100 ) - 1
    }

    getEnchantSkillHolder( level: number ) : EnchantSkillHolder {
        let enchantType = this.getEnchantRoute( level )
        if ( ( enchantType < 1 ) || !this.enchantRoutes[ enchantType ] ) {
            return null
        }

        let group : L2EnchantSkillGroup = DataManager.getSkillData().getEnchantSkillGroupById( this.enchantRoutes[ enchantType ] )
        let index = _.clamp( this.getEnchantIndex( level ), 0, group.getEnchantGroupDetails().length - 1 )

        return group.getEnchantGroupDetails()[ index ]
    }

    getMinSkillLevel( level: number ) : number {
        if ( ( level % 100 ) === 1 ) {
            return this.baseLevel
        }

        return level - 1
    }
}