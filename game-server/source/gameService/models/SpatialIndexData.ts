import { ILocational } from './Location'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { L2WorldRegion } from './L2WorldRegion'
import { BBox } from 'rbush'
import { AbstractResidence } from './entity/AbstractResidence'
import { L2WorldArea } from './areas/WorldArea'

const objectPool = new ObjectPool<ISpatialBoundingBox>( 'SpatialIndexSearchData', () : ISpatialBoundingBox => {
    return {
        maxX: 0,
        maxY: 0,
        minX: 0,
        minY: 0,
    }
}, 500 )

export interface ISpatialIndexData extends ISpatialBoundingBox {
    objectId: number
}

export interface ISpatialIndexArea extends ISpatialBoundingBox {
    area: L2WorldArea
}

export interface ISpatialIndexRegion extends ISpatialBoundingBox {
    region: L2WorldRegion
}

export interface ISpacialIndexResidence extends ISpatialIndexArea {
    residence: AbstractResidence
}

export interface ISpatialBoundingBox extends BBox {
    minX: number
    minY: number
    maxX: number
    maxY: number
}

export const SpatialIndexHelper = {
    getBoundingBox( object: ILocational, radius: number ): ISpatialBoundingBox {
        return this.getBox( object.getX(), object.getY(), radius )
    },

    getBox( x: number, y: number, radius: number ): ISpatialBoundingBox {
        let boundingBox = objectPool.getValue()

        boundingBox.maxX = x + radius
        boundingBox.maxY = y + radius
        boundingBox.minX = x - radius
        boundingBox.minY = y - radius

        return boundingBox
    },

    returnBox( box: ISpatialBoundingBox ): void {
        objectPool.recycleValue( box )
    },

    getPlainBoundingBox( minX: number, minY: number, maxX: number, maxY: number ): ISpatialBoundingBox {
        let boundingBox = objectPool.getValue()

        boundingBox.maxX = Math.max( maxX, minX )
        boundingBox.maxY = Math.max( maxY, minY )
        boundingBox.minX = Math.min( minX, maxX )
        boundingBox.minY = Math.min( minY, maxY )

        return boundingBox
    },

    getPlainBox( minX: number, minY: number, maxX: number, maxY: number ): ISpatialBoundingBox {
        let boundingBox = objectPool.getValue()

        boundingBox.maxX = maxX
        boundingBox.maxY = maxY
        boundingBox.minX = minX
        boundingBox.minY = minY

        return boundingBox
    },
}