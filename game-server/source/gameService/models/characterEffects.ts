import { AbnormalType } from './skills/AbnormalType'
import { BuffInfo } from './skills/BuffInfo'
import { Skill } from './Skill'
import { AbstractEffect } from './effects/AbstractEffect'
import { L2Character } from './actor/L2Character'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { L2Summon } from './actor/L2Summon'
import { OlympiadGameManager } from './olympiad/OlympiadGameManager'
import { OlympiadGameTask } from './olympiad/OlympiadGameTask'
import { AbnormalStatusUpdate } from '../packets/send/builder/AbnormalStatusUpdate'
import { PartySpelled } from '../packets/send/builder/PartySpelled'
import { ExOlympiadSpelledInfo } from '../packets/send/builder/ExOlympiadSpelledInfo'
import { ConfigManager } from '../../config/ConfigManager'
import { EffectFlag } from '../enums/EffectFlag'
import { EffectScope } from './skills/EffectScope'
import { L2EffectType } from '../enums/effects/L2EffectType'
import _, { DebouncedFunc } from 'lodash'
import aigle from 'aigle'
import { RoaringBitmap32 } from 'roaring'
import { BuffDefinition, BuffProperties } from './skills/BuffDefinition'

/*
    TODO : consider MinHeap to manage timers
    add all start times of effects to MinHeap,
    schedule only one timer to remove next item off heap,
    on timer fire remove items that are over current time (these should be already sorted in heap, so easy removal),
    repeat scheduling timer for next item at top of heap

    Above strategy drastically reduces need for timer creation/tracking.
 */
export class CharacterEffects {
    buffs: Array<BuffInfo> = []
    triggered: Array<BuffInfo> = []
    dances: Array<BuffInfo> = []
    toggles: Array<BuffInfo> = []
    debuffs: Array<BuffInfo> = []
    passives: Array<BuffInfo> = []
    stackedEffects: { [skillAbnormalType: number]: BuffInfo } = {}
    blockedBuffSlots: RoaringBitmap32 = new RoaringBitmap32()
    removedOnAnyAction: Set<BuffInfo> = new Set<BuffInfo>()
    removedOnDamage: Set<BuffInfo> = new Set<BuffInfo>()
    effectFlags: EffectFlag = EffectFlag.NONE
    character: L2Character
    hiddenBuffs: number = 0
    skills: Record<number, BuffInfo> = {}
    updatePlayerEffectIcons: DebouncedFunc<any>

    constructor( character: L2Character ) {
        this.character = character

        this.updatePlayerEffectIcons = _.debounce( this.updatePlayerEffectIconsTask.bind( this ), 300, {
            trailing: true,
            maxWait: 600
        } )
    }

    /*
        Since BuffInfo is coming from object pool and new buff may be ignored by code,
        we have to take care to recycle buff properly.
     */
    async add( info: BuffInfo ) : Promise<void> {
        let target = info.getAffected()
        let skill: Skill = info.getSkill()

        if ( this.blockedBuffSlots.has( skill.getAbnormalType() ) ) {
            return info.recycle( this )
        }


        /*
            TODO : use normal creation of BuffInfo instead of object pool
         */
        if ( skill.isPassive() ) {

            if ( !skill.checkCondition( info.getEffector(), target ) ) {
                return info.recycle( this )
            }

            _.remove( this.passives, ( currentBuff: BuffInfo ) => {
                if ( currentBuff.getSkill().getId() === skill.getId() ) {
                    currentBuff.setInUse( false )
                    currentBuff.removeStats()
                    return true
                }

                return false
            } )

            this.passives.push( info )
            this.skills[ skill.getId() ] = info

            return info.initializeEffects()
        }

        if ( target && target.isDead() ) {
            return info.recycle( this )
        }

        let updateFlags : boolean = false
        if ( skill.getAbnormalType() === AbnormalType.NONE ) {
            await this.stopSkillEffects( true, skill.getId() )
        } else {
            let stackedBuff: BuffInfo = this.stackedEffects[ skill.getAbnormalType() ]
            if ( stackedBuff ) {
                if ( skill.getAbnormalLevel() < stackedBuff.getSkill().getAbnormalLevel() ) {
                    return info.recycle( this )
                }

                if ( skill.isAbnormalInstant ) {
                    if ( stackedBuff.getSkill().isAbnormalInstant ) {
                        await this.stopSkillEffectsWithType( true, skill.getAbnormalType() )
                        stackedBuff = this.stackedEffects[ skill.getAbnormalType() ]
                    }

                    if ( stackedBuff ) {
                        stackedBuff.setInUse( false )
                        stackedBuff.removeStats()
                        this.hiddenBuffs++
                        updateFlags = true
                    }
                } else {
                    await this.stopSkillEffectsWithType( true, skill.getAbnormalType() )
                    updateFlags = true
                }
            }

            this.stackedEffects[ skill.getAbnormalType() ] = info
        }

        let effects: Array<BuffInfo> = this.getEffectList( skill )
        if ( !skill.isDebuff() && !skill.isToggle() && !skill.is7Signs() && !this.doesStack( skill ) ) {
            let amountToRemove = -1
            if ( skill.isDance() ) {
                amountToRemove = this.getDanceCount() - ConfigManager.character.getMaxDanceAmount()
            }

            if ( skill.isTrigger() ) {
                amountToRemove = this.getTriggered().length - ConfigManager.character.getMaxTriggeredBuffAmount()
            }

            if ( !skill.isHealingPotionSkill() ) {
                amountToRemove = this.getBuffCount() - this.character.getStat().getMaxBuffCount()
            }

            let buffsToRemove = []
            _.each( effects, ( currentEffect: BuffInfo ) => {
                if ( buffsToRemove.length >= amountToRemove ) {
                    return false
                }

                if ( !currentEffect.isInUse || currentEffect.getSkill().getAbnormalType() === AbnormalType.SUMMON_CONDITION ) {
                    return
                }

                buffsToRemove.push( currentEffect )
            } )

            if ( buffsToRemove.length > 0 ) {
                await this.stopAndRemoveMany( buffsToRemove )
                updateFlags = true
            }
        }

        effects.push( info )
        this.skills[ skill.getId() ] = info

        await info.initializeEffects()
        this.sendEffectUpdates( updateFlags )

        if ( !updateFlags ) {
            this.computeRemovedBuffs( info )
        }
    }

    computeEffectFlags(): void {
        let allFlags = EffectFlag.NONE
        const computeFlags = ( currentBuff: BuffInfo ) => {
            allFlags |= currentBuff.getEffectFlags()
        }

        this.getBuffs().forEach( computeFlags )
        this.getTriggered().forEach( computeFlags )
        this.getDebuffs().forEach( computeFlags )
        this.getDances().forEach( computeFlags )
        this.getToggles().forEach( computeFlags )

        this.effectFlags = allFlags
    }

    resetEffectFlags() : void {
        this.effectFlags = EffectFlag.NONE
    }

    doesStack( skill: Skill ): boolean {
        let type: AbnormalType = skill.getAbnormalType()
        if ( type === AbnormalType.NONE ) {
            return false
        }

        return this.getEffectList( skill ).some( ( info: BuffInfo ) => info && info.getSkill().getAbnormalType() === type )
    }

    getBuffCount(): number {
        let buffCount = 0
        for ( const buff of this.getBuffs() ) {
            if ( !buff.isShortBuff() ) {
                buffCount++
            }
        }

        return buffCount - this.hiddenBuffs
    }

    getBuffInfoByAbnormalType( type: AbnormalType ) : BuffInfo {
        return this.stackedEffects[ type ]
    }

    getBuffInfoBySkillId( skillId: number ) : BuffInfo {
        return this.skills[ skillId ]
    }

    getBuffs() {
        return this.buffs
    }

    getDanceCount(): number {
        return this.getDances().length
    }

    getDances() {
        return this.dances
    }

    getDebuffs() {
        return this.debuffs
    }

    private getEffectList( skill: Skill ) : Array<BuffInfo> {
        if ( !skill ) {
            return []
        }

        if ( skill.isPassive() ) {
            return this.getPassives()
        }

        if ( skill.isDebuff() ) {
            return this.getDebuffs()
        }

        if ( skill.isTrigger() ) {
            return this.getTriggered()
        }

        if ( skill.isDance() ) {
            return this.getDances()
        }

        if ( skill.isToggle() ) {
            return this.getToggles()
        }

        return this.getBuffs()
    }

    getBuffProperties(): Array<BuffProperties> {
        return this.mapBuffs( ( buff : BuffInfo ) => buff.getProperties() )
    }

    getBuffDefinitions(): Array<BuffDefinition> {
        return this.mapBuffs( ( buff : BuffInfo ) => buff.getDefinition() )
    }

    private mapBuffs<T>( method: ( buff : BuffInfo ) => T ) : Array<T> {
        let allBuffs = [
            this.getBuffs().map( method ),
            this.getTriggered().map( method ),
            this.getDances().map( method ),
            this.getToggles().map( method ),
            this.getDebuffs().map( method )
        ]

        return _.flatten( allBuffs )
    }

    applyToEffects( method: ( info: BuffInfo ) => void ) : void {
        this.getBuffs().forEach( method )
        this.getTriggered().forEach( method )
        this.getDebuffs().forEach( method )
        this.getDances().forEach( method )
        this.getToggles().forEach( method )
    }

    getPassives() {
        return this.passives
    }

    getToggles() {
        return this.toggles
    }

    getTriggered() {
        return this.triggered
    }

    hasDances(): boolean {
        return this.dances.length > 0
    }

    hasDebuffs(): boolean {
        return this.debuffs.length > 0
    }

    hasToggles(): boolean {
        return this.toggles.length > 0
    }

    hasTriggered(): boolean {
        return this.triggered.length > 0
    }

    isAffected( flag: EffectFlag ) : boolean {
        return ( this.effectFlags & flag ) !== 0
    }

    isAffectedBySkill( skillId: number ) {
        return !!this.getBuffInfoBySkillId( skillId )
    }

    isEmpty(): boolean {
        return this.getBuffs().length === 0
                && !this.hasTriggered()
                && !this.hasDances()
                && !this.hasDebuffs()
                && !this.hasToggles()
    }

    async remove( isRemoved: boolean, info: BuffInfo, shouldUpdate: boolean, updateFlags: boolean = false ) : Promise<void> {
        if ( !info ) {
            return
        }

        await this.stopAndRemoveCompletely( isRemoved, info, this.getEffectList( info.getSkill() ) )

        if ( shouldUpdate ) {
            this.sendEffectUpdates( updateFlags )
        }
    }

    private async stopAllBuffs() : Promise<unknown> {
        return aigle.resolve( this.getBuffs() ).each( ( buff ) => {
            return this.stopAndRemoveCompletely( true, buff, null )
        } )
    }

    private async stopAllTriggered() : Promise<unknown> {
        return aigle.resolve( this.getTriggered() ).each( ( buff ) => {
            return this.stopAndRemoveCompletely( true, buff, null )
        } )
    }

    private async stopAllDances() : Promise<unknown> {
        return aigle.resolve( this.getDances() ).each( ( buff: BuffInfo ) => {
            return this.stopAndRemoveCompletely( true, buff, null )
        } )
    }

    private async stopAllDebuffs() : Promise<unknown> {
        return aigle.resolve( this.getDebuffs() ).each( ( buff: BuffInfo ) => {
            return this.stopAndRemoveCompletely( true, buff, null )
        } )
    }

    private async stopAllToggles() : Promise<unknown> {
        return aigle.resolve( this.getToggles() ).each( ( buff: BuffInfo ) => {
            return this.stopAndRemoveCompletely( true, buff, null )
        } )
    }

    /*
        Stopping all effects whist making sure we only remove buff effects, leaving
        all buffs "un-removed". Only then recycle all buffs.
     */
    async stopAllEffects( updateFlags: boolean = true ): Promise<void> {
        if ( this.buffs.length > 0 ) {
            await this.stopAllBuffs()
        }

        if ( this.triggered.length > 0 ) {
            await this.stopAllTriggered()
        }

        if ( this.dances.length > 0 ) {
            await this.stopAllDances()
        }

        if ( this.toggles.length > 0 ) {
            await this.stopAllToggles()
        }

        if ( this.debuffs.length > 0 ) {
            await this.stopAllDebuffs()
        }

        if ( !_.isEmpty( this.stackedEffects ) ) {
            this.stackedEffects = {}
        }

        await this.recycleAllBuffs()
        this.skills = {}

        if ( updateFlags ) {
            this.updateEffectIcons( true )
        }

        this.resetEffectFlags()
    }

    async stopAllEffectsExceptThoseThatLastThroughDeath() : Promise<void> {
        const predicate = ( info: BuffInfo ) : boolean => {
            return !info.getSkill().isStayAfterDeath()
        }

        return this.stopAndRemoveByPredicate( predicate )
    }

    async stopTogglEffects( sendUpdate: boolean = true ) : Promise<void> {
        let allToggles = this.getToggles()
        if ( allToggles.length === 0 ) {
            return
        }

        await aigle.resolve( allToggles ).each( ( toggle: BuffInfo ) => {
            return this.stopAndRemove( toggle, allToggles )
        } )

        if ( sendUpdate ) {
            this.sendEffectUpdates( true )
        }
    }

    stopAndRemove( info: BuffInfo, effects: Array<BuffInfo> = this.getEffectList( info.getSkill() ) ): Promise<void> {
        return this.stopAndRemoveCompletely( true, info, effects )
    }

    private async stopAndRemoveMany( buffs: Array<BuffInfo> ) : Promise<unknown> {
        return aigle.resolve( buffs ).each( ( buff: BuffInfo ) => this.stopAndRemove( buff ) )
    }

    private async stopAndRemoveCompletely( showRemovedMessage: boolean, buff: BuffInfo, effects: Array<BuffInfo> ) : Promise<void> {
        if ( !buff ) {
            return
        }

        let skill = buff.getSkill()
        delete this.skills[ skill.getId() ]
        await buff.stopAllEffects( showRemovedMessage )

        if ( !buff.isInUse ) {
            this.hiddenBuffs--
        }

        delete this.stackedEffects[ skill.getAbnormalType() ]

        if ( skill.isAbnormalInstant ) {
            this.getBuffs().forEach( ( info: BuffInfo ) : void => {
                if ( info
                        && ( info.getSkill().getAbnormalType() === skill.getAbnormalType() )
                        && !info.isInUse ) {
                    info.setInUse( true )
                    info.addStats()

                    this.stackedEffects[ info.getSkill().getAbnormalType() ] = info
                    this.hiddenBuffs--
                }
            } )
        }

        if ( !showRemovedMessage && skill.hasEffects( EffectScope.END ) ) {
            await skill.applyEffectScope( EffectScope.END, buff, true, false )
        }

        if ( effects ) {
            _.pull( effects, buff )
        }

        return buff.recycle( this )
    }

    async stopEffects( type: L2EffectType ) : Promise<void> {
        const predicate = ( info: BuffInfo ) : boolean => {
            return info.getEffects().some( ( effect: AbstractEffect ) : boolean => {
                return effect.effectType === type
            } )
        }

        return this.stopAndRemoveByPredicate( predicate )
    }

    async stopEffectsOnDamage() : Promise<void> {
        await this.stopAndRemoveMany( Array.from( this.removedOnDamage ) )
        this.removedOnDamage.clear()
        this.sendEffectUpdates( false )
    }

    stopSkillEffects( isRemoved: boolean, skillId: number ) : Promise<void> {
        let info: BuffInfo = this.getBuffInfoBySkillId( skillId )
        if ( info ) {
            return this.remove( isRemoved, info, true )
        }
    }

    async stopSkillEffectsWithType( isRemoved: boolean, type: AbnormalType ): Promise<boolean> {
        let buff: BuffInfo = this.stackedEffects[ type ]
        if ( buff ) {
            delete this.stackedEffects[ type ]
            await this.stopSkillEffects( isRemoved, buff.getSkill().getId() )
            return true
        }

        return false
    }

    updateRemovedActionBuffs(): void {
        this.removedOnAnyAction.clear()
        this.removedOnDamage.clear()

        const computeBuffs = this.computeRemovedBuffs.bind( this )

        this.getBuffs().forEach( computeBuffs )
        this.getTriggered().forEach( computeBuffs )
        this.getToggles().forEach( computeBuffs )
        this.getDebuffs().forEach( computeBuffs )
    }

    computeRemovedBuffs( info: BuffInfo ): void {
        let skill = info.getSkill()
        if ( skill.isRemovedOnAnyActionExceptMove() ) {
            this.removedOnAnyAction.add( info )
        }

        if ( skill.isRemovedOnDamage() ) {
            this.removedOnDamage.add( info )
        }
    }

    updateEffectIcons( updateActionBuffs: boolean ) {
        if ( !this.character ) {
            return
        }

        if ( updateActionBuffs ) {
            this.updateRemovedActionBuffs()
        }

        if ( !this.character.isPlayable() ) {
            return
        }

        this.updatePlayerEffectIcons()
    }

    private updatePlayerEffectIconsTask() : void {
        if ( !this.character ) {
            return
        }

        let statusUpdatePacket: AbnormalStatusUpdate = null
        let normalPartyPacket: PartySpelled = null
        let summonPartyPacket: PartySpelled = null
        let olympiadInfoPacket: ExOlympiadSpelledInfo = null
        let isSummon = false

        if ( this.character.isPlayer() ) {
            if ( this.character.isInParty() ) {
                normalPartyPacket = new PartySpelled( this.character )
            } else {
                statusUpdatePacket = new AbnormalStatusUpdate()
            }

            let player = this.character as L2PcInstance
            if ( player.isInOlympiadMode() && player.isOlympiadStarted ) {
                olympiadInfoPacket = new ExOlympiadSpelledInfo( player )
            }
        } else if ( this.character.isSummon() ) {
            isSummon = true
            normalPartyPacket = new PartySpelled( this.character )
            summonPartyPacket = new PartySpelled( this.character )
        }

        const addIcon = ( info: BuffInfo ) => {
            if ( !info || !info.isInUse ) {
                return
            }

            if ( statusUpdatePacket ) {
                statusUpdatePacket.addSkill( info )
            }

            let skill: Skill = info.getSkill()
            if ( normalPartyPacket && ( isSummon || !skill.isToggle() ) ) {
                normalPartyPacket.addSkill( info )
            }

            if ( summonPartyPacket && !skill.isToggle() ) {
                summonPartyPacket.addSkill( info )
            }

            if ( olympiadInfoPacket ) {
                olympiadInfoPacket.addSkill( info )
            }
        }

        this.getBuffs().forEach( ( currentBuff: BuffInfo ) => {
            if ( currentBuff.isShortBuff() ) {
                return currentBuff.sendShortBuffStatusUpdate()
            }

            return addIcon( currentBuff )
        } )

        this.getTriggered().forEach( addIcon )
        this.getDances().forEach( addIcon )
        this.getToggles().forEach( addIcon )
        this.getDebuffs().forEach( addIcon )

        /*
            It is possible to have empty status update packet due to all buffs being removed.
         */
        if ( statusUpdatePacket ) {
            this.character.sendOwnedData( statusUpdatePacket.getBuffer() )
        }

        if ( normalPartyPacket ) {
            if ( summonPartyPacket ) {
                let summonOwner: L2PcInstance = ( this.character as L2Summon ).getOwner()
                if ( summonOwner ) {
                    if ( summonOwner.isInParty() ) {
                        summonOwner.getParty().broadcastDataToPartyMembers( summonOwner.objectId, summonPartyPacket.getBuffer() )
                    }

                    summonOwner.sendOwnedData( normalPartyPacket.getBuffer() )
                }
            } else {
                this.character.getParty().broadcastPacket( normalPartyPacket.getBuffer() )
            }
        }

        if ( olympiadInfoPacket ) {
            let gameTask: OlympiadGameTask = OlympiadGameManager.getOlympiadTask( this.character.getActingPlayer().getOlympiadGameId() )
            if ( gameTask && gameTask.isBattleStarted() ) {
                gameTask.broadcastPacketToObservers( olympiadInfoPacket.getBuffer() )
            }
        }
    }

    updateEffectIconsList() : void {
        return this.sendEffectUpdates( false )
    }

    sendEffectUpdates( updateFlags: boolean ) {
        this.updateEffectIcons( updateFlags )
        this.computeEffectFlags()
    }

    async stopEffectsOnAction() : Promise<void> {
        await this.stopAndRemoveMany( Array.from( this.removedOnAnyAction ) )
        this.removedOnAnyAction.clear()
        this.sendEffectUpdates( false )
    }

    hasEffectsRemovedOnAction() : boolean {
        return this.removedOnAnyAction.size > 0
    }

    hasEffectsRemovedOnDamage() : boolean {
        return this.removedOnDamage.size > 0
    }

    addBlockedBuffSlots( slots: Array<AbnormalType> ) {
        this.blockedBuffSlots.addMany( slots )
    }

    removeBlockedBuffSlots( slots: Array<AbnormalType> ) {
        this.blockedBuffSlots.removeMany( slots )
    }

    getFirstEffect( type: L2EffectType ) : BuffInfo {
        let outcome : BuffInfo = null

        const processBuff = ( info: BuffInfo ) => {
            if ( !info ) {
                return false
            }

            return info.getEffects().some( ( effect: AbstractEffect ) => {
                return effect && effect.effectType === type
            } )
        }

        let allMethods : Array<Function> = [
                this.getBuffs.bind( this ),
                this.getTriggered.bind( this ),
                this.getDebuffs.bind( this ),
                this.getDances.bind( this ),
                this.getToggles.bind( this ),
        ]

        allMethods.some( ( method: () => Array<BuffInfo> ) => {
            outcome = method().find( processBuff )

            return !!outcome
        } )

        return outcome
    }

    async stopAllEffectsNotStayOnSubclassChange() : Promise<void> {
        const predicate = ( info: BuffInfo ) : boolean => {
            return !info.getSkill().isStayOnSubclassChange()
        }

        return this.stopAndRemoveByPredicate( predicate )
    }

    getHiddenBuffsCount() {
        return this.hiddenBuffs
    }

    getAllBlockedBuffSlots() : Array<number> {
        return this.blockedBuffSlots.toArray()
    }

    private async stopAndRemoveByPredicate( predicate : ( buff : BuffInfo ) => boolean ) : Promise<void> {
        let buffs : Array<BuffInfo> = []
        const getBuffs = ( info: BuffInfo, ) : void => {
            if ( predicate( info ) ) {
                buffs.push( info )
            }
        }

        this.getBuffs().forEach( getBuffs )
        this.getTriggered().forEach( getBuffs )
        this.getDebuffs().forEach( getBuffs )
        this.getDances().forEach( getBuffs )
        this.getToggles().forEach( getBuffs )

        if ( buffs.length > 0 ) {
            await this.stopAndRemoveMany( buffs )
            this.sendEffectUpdates( true )
        }
    }

    /*
        Similar to other version of stopping all effects, however focused on clean up any and all used data.
     */
    async unloadAllEffects() : Promise<void> {
        if ( this.buffs.length > 0 ) {
            await aigle.resolve( this.buffs ).each( buff => buff.unloadAllEffects() )
        }

        if ( this.triggered.length > 0 ) {
            await aigle.resolve( this.triggered ).each( buff => buff.unloadAllEffects() )
        }

        if ( this.dances.length > 0 ) {
            await aigle.resolve( this.dances ).each( buff => buff.unloadAllEffects() )
        }

        if ( this.toggles.length > 0 ) {
            await aigle.resolve( this.toggles ).each( buff => buff.unloadAllEffects() )
        }

        if ( this.debuffs.length > 0 ) {
            await aigle.resolve( this.debuffs ).each( buff => buff.unloadAllEffects() )
        }

        if ( !_.isEmpty( this.stackedEffects ) ) {
            this.stackedEffects = {}
        }

        this.skills = {}
        await this.recycleAllBuffs()

        this.hiddenBuffs = 0
        this.effectFlags = EffectFlag.NONE
    }

    private async recycleAllBuffs() : Promise<void> {
        if ( this.buffs.length > 0 ) {
            for ( const buff of this.buffs ) {
                await buff.recycle( this )
            }
        }

        if ( this.triggered.length > 0 ) {
            for ( const buff of this.triggered ) {
                await buff.recycle( this )
            }
        }

        if ( this.dances.length > 0 ) {
            for ( const buff of this.dances ) {
                await buff.recycle( this )
            }
        }

        if ( this.toggles.length > 0 ) {
            for ( const buff of this.toggles ) {
                await buff.recycle( this )
            }
        }

        if ( this.debuffs.length > 0 ) {
            for ( const buff of this.debuffs ) {
                await buff.recycle( this )
            }
        }

        this.buffs.length = 0
        this.triggered.length = 0
        this.dances.length = 0
        this.toggles.length = 0
        this.debuffs.length = 0

        this.blockedBuffSlots.clear()
        this.removedOnAnyAction.clear()
        this.removedOnDamage.clear()
    }
}