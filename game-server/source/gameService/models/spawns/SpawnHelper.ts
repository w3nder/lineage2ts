import { L2Character } from '../actor/L2Character'
import { L2NpcInstance } from '../actor/instance/L2NpcInstance'
import { L2MonsterInstance } from '../actor/instance/L2MonsterInstance'
import { L2MerchantInstance } from '../actor/instance/L2MerchantInstance'
import { L2WarehouseInstance } from '../actor/instance/L2WarehouseInstance'
import { L2ChestInstance } from '../actor/instance/L2ChestInstance'
import { L2ControlTowerInstance } from '../actor/instance/L2ControlTowerInstance'
import { L2TeleporterInstance } from '../actor/instance/L2TeleporterInstance'
import { L2TerrainObjectInstance } from '../actor/instance/L2TerrainObjectInstance'
import { L2GuardInstance } from '../actor/instance/L2GuardInstance'
import { L2OlympiadManagerInstance } from '../actor/instance/L2OlympiadManagerInstance'
import { L2VillageMasterKamaelInstance } from '../actor/instance/L2VillageMasterKamaelInstance'
import { L2TrainerInstance } from '../actor/instance/L2TrainerInstance'
import { L2VillageMasterOrcInstance } from '../actor/instance/L2VillageMasterOrcInstance'
import { L2VillageMasterPriestInstance } from '../actor/instance/L2VillageMasterPriestInstance'
import { L2VillageMasterFighterInstance } from '../actor/instance/L2VillageMasterFighterInstance'
import { L2VillageMasterDwarfInstance } from '../actor/instance/L2VillageMasterDwarfInstance'
import { L2VillageMasterDElfInstance } from '../actor/instance/L2VillageMasterDElfInstance'
import { L2AdventurerInstance } from '../actor/instance/L2AdventurerInstance'
import { L2ObservationInstance } from '../actor/instance/L2ObservationInstance'
import { L2PetManagerInstance } from '../actor/instance/L2PetManagerInstance'
import { L2VillageMasterMysticInstance } from '../actor/instance/L2VillageMasterMysticInstance'
import { L2FriendlyMobInstance } from '../actor/instance/L2FriendlyMobInstance'
import { L2RiftInvaderInstance } from '../actor/instance/L2RiftInvaderInstance'
import { L2FeedableBeastInstance } from '../actor/instance/L2FeedableBeastInstance'
import { L2FlyTerrainObjectInstance } from '../actor/instance/L2FlyTerrainObjectInstance'
import { L2AuctioneerInstance } from '../actor/instance/L2AuctioneerInstance'
import { L2ClanHallManagerInstance } from '../actor/instance/L2ClanHallManagerInstance'
import { L2ClanHallDoormenInstance } from '../actor/instance/L2ClanHallDoormenInstance'
import { L2DoormenInstance } from '../actor/instance/L2DoormenInstance'
import { L2CastleDoormenInstance } from '../actor/instance/L2CastleDoormenInstance'
import { L2SepulcherNpcInstance } from '../actor/instance/L2SepulcherNpcInstance'
import { L2RaidBossInstance } from '../actor/instance/L2RaidBossInstance'
import { L2ArtefactInstance } from '../actor/instance/L2ArtefactInstance'
import { L2RaceManagerInstance } from '../actor/instance/L2RaceManagerInstance'
import { L2FortManagerInstance } from '../actor/instance/L2FortManagerInstance'
import { L2FortDoormenInstance } from '../actor/instance/L2FortDoormenInstance'
import { L2FortLogisticsInstance } from '../actor/instance/L2FortLogisticsInstance'
import { L2FortCommanderInstance } from '../actor/instance/L2FortCommanderInstance'
import { L2DefenderInstance } from '../actor/instance/L2DefenderInstance'
import { L2QuestGuardInstance } from '../actor/instance/L2QuestGuardInstance'
import { L2NpcTemplate } from '../actor/templates/L2NpcTemplate'
import { L2GrandBossInstance } from '../actor/instance/L2GrandBossInstance'
import { NpcTemplateType } from '../../enums/NpcTemplateType'

const nullMethod = () => null
export type SpawnMethod = ( template: L2NpcTemplate ) => L2Character

const spawnMethodMap : Record<number, SpawnMethod> = {
    [ NpcTemplateType.L2Npc ]: L2NpcInstance.fromTemplate,
    [ NpcTemplateType.L2Monster ]: L2MonsterInstance.fromTemplate,
    [ NpcTemplateType.L2Merchant ]: L2MerchantInstance.fromTemplate,
    [ NpcTemplateType.L2Warehouse ]: L2WarehouseInstance.fromTemplate,
    [ NpcTemplateType.L2Chest ]: L2ChestInstance.fromTemplate,
    [ NpcTemplateType.L2ControlTower ]: L2ControlTowerInstance.fromTemplate,
    [ NpcTemplateType.L2Teleporter ]: L2TeleporterInstance.fromTemplate,
    [ NpcTemplateType.L2TerrainObject ]: L2TerrainObjectInstance.fromTemplate,
    [ NpcTemplateType.L2Guard ]: L2GuardInstance.fromTemplate,
    [ NpcTemplateType.L2OlympiadManager ]: L2OlympiadManagerInstance.fromTemplate,
    [ NpcTemplateType.L2VillageMasterKamael ]: L2VillageMasterKamaelInstance.fromTemplate,
    [ NpcTemplateType.L2Trainer ]: L2TrainerInstance.fromTemplate,
    [ NpcTemplateType.L2VillageMasterOrc ]: L2VillageMasterOrcInstance.fromTemplate,
    [ NpcTemplateType.L2VillageMasterPriest ]: L2VillageMasterPriestInstance.fromTemplate,
    [ NpcTemplateType.L2VillageMasterFighter ]: L2VillageMasterFighterInstance.fromTemplate,
    [ NpcTemplateType.L2VillageMasterDwarf ]: L2VillageMasterDwarfInstance.fromTemplate,
    [ NpcTemplateType.L2VillageMasterDElf ]: L2VillageMasterDElfInstance.fromTemplate,
    [ NpcTemplateType.L2Adventurer ]: L2AdventurerInstance.fromTemplate,
    [ NpcTemplateType.L2Observation ]: L2ObservationInstance.fromTemplate,
    [ NpcTemplateType.L2FestivalGuide ]: L2NpcInstance.fromTemplate, // TODO : refactor data to remove reference here
    [ NpcTemplateType.L2PetManager ]: L2PetManagerInstance.fromTemplate,
    [ NpcTemplateType.L2VillageMasterMystic ]: L2VillageMasterMysticInstance.fromTemplate,
    [ NpcTemplateType.L2FriendlyMob ]: L2FriendlyMobInstance.fromTemplate,
    [ NpcTemplateType.L2RiftInvader ]: L2RiftInvaderInstance.fromTemplate,
    [ NpcTemplateType.L2FeedableBeast ]: L2FeedableBeastInstance.fromTemplate,
    [ NpcTemplateType.L2FlyTerrainObject ]: L2FlyTerrainObjectInstance.fromTemplate,
    [ NpcTemplateType.L2Auctioneer ]: L2AuctioneerInstance.fromTemplate,
    [ NpcTemplateType.L2ClanHallManager ]: L2ClanHallManagerInstance.fromTemplate,
    [ NpcTemplateType.L2ClanHallDoormen ]: L2ClanHallDoormenInstance.fromTemplate,
    [ NpcTemplateType.L2Doormen ]: L2DoormenInstance.fromTemplate,
    [ NpcTemplateType.L2CastleDoormen ]: L2CastleDoormenInstance.fromTemplate,
    [ NpcTemplateType.L2SepulcherNpc ]: L2SepulcherNpcInstance.fromTemplate,
    [ NpcTemplateType.L2RaidBoss ]: L2RaidBossInstance.fromTemplate,
    [ NpcTemplateType.L2Artefact ]: L2ArtefactInstance.fromTemplate,
    [ NpcTemplateType.L2RaceManager ]: L2RaceManagerInstance.fromTemplate,
    [ NpcTemplateType.L2FortManager ]: L2FortManagerInstance.fromTemplate,
    [ NpcTemplateType.L2FortDoormen ]: L2FortDoormenInstance.fromTemplate,
    [ NpcTemplateType.L2FortLogistics ]: L2FortLogisticsInstance.fromTemplate,
    [ NpcTemplateType.L2FortCommander ]: L2FortCommanderInstance.fromTemplate,
    [ NpcTemplateType.L2Defender ]: L2DefenderInstance.fromTemplate,
    [ NpcTemplateType.L2QuestGuard ]: L2QuestGuardInstance.fromTemplate,
    [ NpcTemplateType.L2GrandBoss ]: L2GrandBossInstance.fromTemplate,
    [ NpcTemplateType.L2SepulcherMonster ]: nullMethod, // TODO : implement me
    [ NpcTemplateType.L2Block ]: nullMethod, // TODO : implement me

}

export function getSpawnMethod( type: NpcTemplateType ) : SpawnMethod {
    let spawnMethod = spawnMethodMap[ type ]

    if ( spawnMethod ) {
        return spawnMethod
    }

    return nullMethod
}