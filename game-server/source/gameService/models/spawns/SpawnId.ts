let globalSpawnId : number = Date.now()

/*
    Id is made to be string due to all spawn admin commands
    requiring spawn id and to simplify command handling,
    to avoid any string to number conversions.
 */
export function getNextSpawnId() : string {
    globalSpawnId++
    return globalSpawnId.toString( 36 )
}