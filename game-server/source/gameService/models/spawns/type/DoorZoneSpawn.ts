import { ZoneEntry } from './ZoneEntry'
import { ListenerCache } from '../../../cache/ListenerCache'
import { DoorInteractionEvent, EventType } from '../../events/EventType'
import { ListenerRegisterType } from '../../../enums/ListenerRegisterType'
import { DataManager } from '../../../../data/manager'

export class DoorZoneSpawn extends ZoneEntry {

    protected initializeSpawn() {
        super.initializeSpawn()

        let doorName = this.maker.aiParameters?.doorName as string
        let doorId = DataManager.getDoorData().getIdByName( doorName )
        if ( !doorId ) {
            throw new Error( `${DoorZoneSpawn.name} : unable to find door with name=${doorName}` )
        }

        ListenerCache.registerListener( ListenerRegisterType.NpcId, EventType.DoorInteraction, this, this.onDoorEvent.bind( this ), [ doorId ] )
    }

    protected onDoorEvent( data: DoorInteractionEvent ) : void {
        if ( data.isOpen ) {
            this.processStartSpawn()
            return
        }

        return this.startDespawn()
    }
}