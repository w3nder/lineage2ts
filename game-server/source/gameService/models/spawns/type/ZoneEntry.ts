import { L2DefaultSpawn } from './L2DefaultSpawn'
import { SpawnEventManager } from '../SpawnEventManager'
import { defaultTriggerIds, SpawnEventType, ZoneEntrySpawn } from '../SpawnEventType'
import _ from 'lodash'
import { EventListenerMethod } from '../../events/listeners/EventListenerData'

export class ZoneEntry extends L2DefaultSpawn {
    readyToSpawn: boolean = false
    callback: EventListenerMethod

    protected initializeSpawn() {
        this.callback = this.onEvent.bind( this )
        this.registerEvent()
    }

    deInitializeSpawn() {
        this.unRegisterEvent()
    }

    protected getTriggerIds() : Set<number> {
        let ids : Array<number> = [
            this.maker.aiParameters?.inzoneTypeParam as number,
            this.maker.aiParameters?.inzoneClusterId as number
        ].filter( value => _.isNumber( value ) )

        if ( ids.length > 0 ) {
            return new Set<number>( ids )
        }

        return defaultTriggerIds
    }

    startSpawn(): number {
        this.readyToSpawn = true

        return 0
    }

    onEvent( data: ZoneEntrySpawn ) : void {
        if ( data.inZone ) {
            this.processStartSpawn()
            return
        }

        this.startDespawn()
    }

    protected registerEvent() : void {
        /*
            There are two parameters that are passed as part of registration call:
            - spawnEventId
            - despawnEventId

            However, distinction between these ids are yet to be found.
         */
        SpawnEventManager.registerCallback( SpawnEventType.ZoneEntry, this.callback, this.getTriggerIds() )
    }

    protected unRegisterEvent() : void {
        SpawnEventManager.unRegisterCallback( SpawnEventType.ZoneEntry, this.callback )
    }
}