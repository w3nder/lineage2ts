import { L2DefaultSpawn } from './L2DefaultSpawn'
import { SpawnLogicEventData } from '../ISpawnLogic'
import Timeout = NodeJS.Timeout
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

export class DefaultCrataeSpawn extends L2DefaultSpawn {
    despawnTimer: Timeout

    processSpawnEvent( eventType: SpawnLogicEventType, data: SpawnLogicEventData ) {
        if ( eventType === SpawnLogicEventType.Spawn ) {
            return this.onDefaultSpawnEvent( eventType, data )
        }

        if ( this.despawnTimer ) {
            return
        }

        this.despawnTimer = setTimeout( this.onDespawnTimer.bind( this ), 15000 )
    }

    onDespawnTimer() : void {
        this.despawnTimer = null
        this.startDespawn()
    }

    startDespawn() : void {
        if ( this.despawnTimer ) {
            clearTimeout( this.despawnTimer )
            this.despawnTimer = null
        }

        return this.processDespawn()
    }

    startSpawn() {
        return 0
    }
}