import { ZoneEntry } from './ZoneEntry'
import { SpawnEventManager } from '../SpawnEventManager'
import { defaultTriggerIds, OlympiadZoneSpawn, SpawnEventType } from '../SpawnEventType'
import { EventListenerMethod } from '../../events/listeners/EventListenerData'

export class OlympiadManager extends ZoneEntry {

    callback: EventListenerMethod

    protected initializeSpawn() {
        this.callback = this.onOlympiadZoneEvent.bind( this )

        this.registerEvent()
        super.initializeSpawn()
    }

    deInitializeSpawn() {
        this.unRegisterEvent()
    }

    protected getZoneTriggers() : Set<number> {
        if ( this.maker.aiParameters?.inzoneTypeParam ) {
            return new Set<number>( [ this.maker.aiParameters?.inzoneTypeParam as number ] )
        }

        return defaultTriggerIds
    }

    onOlympiadZoneEvent( data : OlympiadZoneSpawn ) : void {
        if ( data.enabled ) {
            this.processStartSpawn()
            return
        }

        return this.processDespawn()
    }

    protected registerEvent() : void {
        SpawnEventManager.registerCallback( SpawnEventType.OlympiadZone, this.callback, this.getZoneTriggers() )
    }

    protected unRegisterEvent() : void {
        SpawnEventManager.unRegisterCallback( SpawnEventType.OlympiadZone, this.callback )
    }
}