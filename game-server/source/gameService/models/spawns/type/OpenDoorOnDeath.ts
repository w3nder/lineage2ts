import { NoRespawnOnDeath } from './NoRespawnOnDeath'
import { L2Npc } from '../../actor/L2Npc'
import { DoorManager } from '../../../cache/DoorManager'

export class OpenDoorOnDeath extends NoRespawnOnDeath {
    startDecay( npc: L2Npc ) {
        this.remove( npc )

        if ( this.parameters.count !== 0 ) {
            return
        }

        let doorName = this.maker.aiParameters?.doorName as string ?? 'door_of_throne'
        let door = DoorManager.getDoorByName( doorName )
        if ( !door ) {
            return
        }

        door.openMe()
    }
}