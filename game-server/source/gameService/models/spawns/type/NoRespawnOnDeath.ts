import { ZoneEntry } from './ZoneEntry'
import { L2Npc } from '../../actor/L2Npc'

export class NoRespawnOnDeath extends ZoneEntry {
    startDecay( npc: L2Npc ) {
        this.remove( npc )
    }
}