import { ZoneEntry } from './ZoneEntry'
import { L2Npc } from '../../actor/L2Npc'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

export class GuardControlSpawn extends ZoneEntry {
    protected startNormalDecay( npc: L2Npc ) {
        this.decrementCount()

        if ( this.parameters.count === 0 ) {
            return this.sendEvent( this.maker.aiParameters?.controlMaker as string, SpawnLogicEventType.Despawn )
        }

        return this.processNormalDecay( npc )
    }
}