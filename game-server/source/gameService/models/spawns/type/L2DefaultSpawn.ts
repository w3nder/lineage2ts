import {
    ISpawnLogic,
    RespawnEventData,
    SpawnLogicEventData,
    SpawnSharedParameters
} from '../ISpawnLogic'
import { L2SpawnNpcDataItem } from '../../../../data/interface/SpawnNpcDataApi'
import { getSpawnMethod, SpawnMethod } from '../SpawnHelper'
import { L2SpawnLogicItem } from '../../../../data/interface/SpawnLogicDataApi'
import { L2Npc } from '../../actor/L2Npc'
import { L2Attackable } from '../../actor/L2Attackable'
import { L2NpcTemplate } from '../../actor/templates/L2NpcTemplate'
import { DataManager } from '../../../../data/manager'
import { ConfigManager } from '../../../../config/ConfigManager'
import { GeoPolygonCache } from '../../../cache/GeoPolygonCache'
import _ from 'lodash'
import { SpawnMakerCache } from '../../../cache/SpawnMakerCache'
import logSymbols from 'log-symbols'
import { L2World } from '../../../L2World'
import { clearTimeout } from 'timers'
import { PointGeometry } from '../../drops/PointGeometry'
import { GeometryId } from '../../../enums/GeometryId'
import { ILocational } from '../../Location'
import { getNextSpawnId } from '../SpawnId'
import { AITraitCache } from '../../../cache/AITraitCache'
import Timeout = NodeJS.Timeout
import { TraitSettings } from '../../../aicontroller/interface/IAITrait'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'
import { L2NpcMinion } from '../../actor/templates/L2NpcMinion'

interface L2DefaultMinions {
    spawnedObjectIds: Set<number>
    masters: Map<number, number> // minion objectId vs leader objectId
    data: Map<number, L2NpcMinion> // objectId vs minion spec
    minionIds: Map<number, Set<number>> // leader objectId vs minion objectIds
}

export class L2DefaultSpawn implements ISpawnLogic {
    data: L2SpawnNpcDataItem
    count: number = 0
    instanceId: number = 0
    maker: L2SpawnLogicItem
    parameters: SpawnSharedParameters
    spawnedObjectIds: Set<number> = new Set<number>()
    minions: L2DefaultMinions
    decayTimeouts: Record<number, Timeout> = {}
    lastX: number
    lastY: number
    lastZ: number
    lastHeading: number
    respawnTimes: Record<number, number>
    id: string

    constructor( data: L2SpawnNpcDataItem, maker: L2SpawnLogicItem, parameters: SpawnSharedParameters ) {
        this.id = getNextSpawnId()
        this.data = data
        this.maker = maker
        this.parameters = parameters

        if ( this.data.position ) {
            this.lastX = this.data.position.x
            this.lastY = this.data.position.y
            this.lastZ = this.data.position.z
            this.lastHeading = this.data.position.heading
        }

        this.initializeSpawn()
    }

    protected initializeSpawn(): void {
        /*
            Method intended to be used for various initialization and event subscription logic,
            avoiding to re-declare constructor.
         */
    }

    deInitializeSpawn() : void {
        /*
            Method intended to be used to remove any initialization and event subscription logic.
         */
    }

    startSpawn(): number {
        return this.processStartSpawn()
    }

    protected processStartSpawn(): number {
        if ( !this.canSpawn() ) {
            return 0
        }

        let amount = this.getAmount()
        return this.createNpcs( amount )
    }

    protected canSpawn(): boolean {
        if ( this.spawnedObjectIds.size > 0 || this.maker.aiParameters?.onStartSpawn === 0 ) {
            return false
        }

        return this.maker.maxAmount >= ( this.parameters.count + this.data.amount )
    }

    getX(): number {
        return this.lastX
    }

    getY(): number {
        return this.lastY
    }

    getZ(): number {
        return this.lastZ
    }

    getHeading(): number {
        return this.lastHeading
    }

    getInstanceId(): number {
        return this.instanceId
    }

    private createNpcs( amount: number ): number {
        const template = this.getTemplate()
        const spawnMethod: SpawnMethod = getSpawnMethod( template.getType() )
        let aiTraits: TraitSettings

        for ( let index = 0; index < amount; index++ ) {
            let npc = spawnMethod( template ) as L2Npc

            npc.setNpcSpawn( this )

            this.spawnedObjectIds.add( npc.getObjectId() )
            this.parameters.count++

            /*
                A small optimization to re-use traits between known npcs due to same npc id being used.
                A caution must be made to modify such traits due to all other npcs being affected.
             */
            if ( !aiTraits ) {
                aiTraits = AITraitCache.getNpcTraits( npc )
            }

            npc.setDefaultTraits( aiTraits )
            this.resetNpc( npc )
            this.assignSpawnLocation( npc, this.getSpawnTerritoryId() )
        }

        return amount + this.spawnMinions()
    }

    hasMinions(): boolean {
        return this.getMinionData().length > 0
    }

    private getMinionData() : ReadonlyArray<L2NpcMinion> {
        /*
            Not all minions can come from spawn definition,
            these can come from npc template directly.
         */
        let template = this.getTemplate()
        if ( template.getMinions().length > 0 ) {
            return template.getMinions()
        }

        if ( this.data.minions && this.data.minions.length > 0 ) {
            return this.data.minions
        }

        return []
    }

    spawnMinions(): number {
        if ( !this.hasMinions() ) {
            return 0
        }

        if ( !this.minions ) {
            this.minions = {
                data: new Map<number, L2NpcMinion>(),
                masters: new Map<number, number>(),
                minionIds: new Map<number, Set<number>>(),
                spawnedObjectIds: new Set<number>()
            }
        }

        let spawnedAmount: number = 0
        let geometry = PointGeometry.acquire( GeometryId.RaidbossSpawnSmall )

        /*
            Needed to freshly reload any missing geometry points that have been previously used.
         */
        geometry.refresh()

        /*
            Simple logic to spawn all minions PER main npc spawned. Looking at npc data ex
            it is possible to have more than one main npc, hence each such npc will have
            all possible minions.
         */
        Array.from( this.spawnedObjectIds ).forEach( ( objectId: number ) => {
            let target = L2World.getObjectById( objectId )

            for ( const minionData of this.getMinionData() ) {
                const template = DataManager.getNpcData().getTemplate( minionData.npcId )
                const spawnMethod: SpawnMethod = getSpawnMethod( template.getType() )
                let aiTraits: TraitSettings

                for ( let index = 0; index < minionData.amount; index++ ) {
                    let npc = spawnMethod( template ) as L2Npc
                    if ( !aiTraits ) {
                        aiTraits = AITraitCache.getNpcTraits( npc )
                    }

                    npc.setNpcSpawn( this )
                    npc.setDefaultTraits( aiTraits )
                    npc.setInstanceId( target.getInstanceId() )
                    this.resetNpc( npc )

                    this.minions.spawnedObjectIds.add( npc.getObjectId() )
                    this.minions.data.set( npc.getObjectId(), minionData )
                    this.minions.masters.set( npc.getObjectId(), target.getObjectId() )
                    this.addMinionId( target.getObjectId(), npc.getObjectId() )

                    this.assignMinionSpawnLocation( npc, geometry, target )
                }

                spawnedAmount += minionData.amount
            }
        } )

        geometry.release()

        return spawnedAmount
    }

    getRandomHeading(): number {
        return Math.floor( Math.random() * 65536 )
    }

    runRespawnTask( npc: L2Npc ): void {
        delete this.decayTimeouts[ npc.getObjectId() ]
        this.spawnedObjectIds.delete( npc.getObjectId() )

        npc.refreshId()
        this.spawnedObjectIds.add( npc.getObjectId() )

        if ( this.minions ) {
            this.minions.minionIds.delete( npc.getObjectId() )
        }

        this.resetNpc( npc )
        this.assignSpawnLocation( npc, this.getRespawnTerritoryId() )
    }

    runMinionRespawnTask( npc: L2Npc ): Promise<void> {
        delete this.decayTimeouts[ npc.getObjectId() ]
        this.minions.spawnedObjectIds.delete( npc.getObjectId() )

        let data = this.minions.data.get( npc.getObjectId() )
        this.minions.data.delete( npc.getObjectId() )

        let masterId: number = this.getMinionLeaderId( npc.getObjectId() )
        if ( !masterId ) {
            return
        }

        let master = L2World.getObjectById( masterId ) as L2Npc
        if ( !master || master.isAlikeDead() ) {
            return npc.deleteMe()
        }

        let minionIds = this.minions.minionIds.get( masterId )
        if ( !minionIds ) {
            return
        }

        minionIds.delete( npc.getObjectId() )

        npc.refreshId()
        this.minions.spawnedObjectIds.add( npc.getObjectId() )
        this.minions.data.set( npc.getObjectId(), data )
        minionIds.add( npc.getObjectId() )

        this.resetNpc( npc )
        npc.setInstanceId( master.getInstanceId() )

        let geometry = PointGeometry.acquire( GeometryId.RaidbossSpawnSmall )
        this.assignMinionSpawnLocation( npc, geometry, master )
        geometry.release()
    }

    protected resetNpc( npc: L2Npc ): void {
        npc.setIsDead( false )
        npc.setDecayed( false )

        this.setStats( npc )

        npc.setInstanceId( this.instanceId )

        if ( npc.isAttackable() && ConfigManager.customs.championEnable() ) {
            ( npc as L2Attackable ).setChampion( this.isChampion( npc ) )
        }

        npc.setSummoner( 0 )
        npc.resetSummonedNpcs()
    }

    protected setStats( npc: L2Npc ): void {
        if ( npc.hasLoadedSkills() ) {
            npc.setMaxStats()
        }
    }

    protected assignSpawnLocation( npc: L2Npc, territoryId: string, heading: number = this.getRandomHeading() ): void {
        if ( this.data.position ) {
            let z = GeoPolygonCache.getZ( this.data.position.x, this.data.position.y, this.data.position.z + npc.getTemplate().getCollisionHeight() )
            return this.spawnAtLocation( npc, this.data.position.x, this.data.position.y, z, this.data.position.heading )
        }

        let territory = SpawnMakerCache.getTerritory( territoryId )
        if ( !territory || territory.hasNoGeometry() ) {
            console.log( logSymbols.error, 'Unable to find spawn location for territoryId=', territoryId )
            return
        }

        let x: number
        let y: number
        let z: number
        let counter : number = 0

        do {
            territory.generatePoint()

            x = territory.getX()
            y = territory.getY()
            z = GeoPolygonCache.getZ( x, y, territory.getZ() + 20 )

            if ( counter > 100 ) {
                throw new Error( `Unable to spawn npcId=${ npc.getId() } using territory id=${ territory.data.id }` )
            }

            counter++
        } while ( GeoPolygonCache.isInvalidZ( z ) )

        return this.spawnAtLocation( npc, territory.getX(), territory.getY(), territory.getZ(), heading )
    }

    protected assignMinionSpawnLocation( npc: L2Npc, geometry: PointGeometry, target: ILocational ) : void {
        let x: number
        let y: number
        let z: number

        do {
            geometry.prepareNextPoint()

            x = target.getX() + geometry.getX()
            y = target.getY() + geometry.getY()
            z = GeoPolygonCache.getZ( x, y, target.getZ() + 20 )
        } while ( GeoPolygonCache.isInvalidZ( z ) )

        this.spawnAtLocation( npc, x, y, z, this.getRandomHeading() )
    }

    protected getSpawnTerritoryId(): string {
        return this.maker.spawnTerritoryIds.length === 1 ? this.maker.spawnTerritoryIds[ 0 ] : _.sample( this.maker.spawnTerritoryIds )
    }

    protected getRespawnTerritoryId(): string {
        return this.getSpawnTerritoryId()
    }

    protected spawnAtLocation( npc: L2Npc, x: number, y: number, z: number, heading: number ): void {
        this.lastX = x
        this.lastY = y
        this.lastZ = z
        this.lastHeading = heading

        npc.setHeading( heading )
        npc.spawnMe( x, y, z )
    }

    protected isChampion( npc: L2Npc ): boolean {
        return npc.isMonster()
            && !npc.getTemplate().isUndying()
            && !npc.isRaid()
            && !npc.isRaidMinion()
            && ConfigManager.customs.getChampionFrequency() > 0
            && npc.getLevel() >= ConfigManager.customs.getChampionMinLevel()
            && npc.getLevel() <= ConfigManager.customs.getChampionMaxLevel()
            && ( this.instanceId === 0 || ConfigManager.customs.championEnableInInstances() )
            && ( Math.random() * 100 ) < ConfigManager.customs.getChampionFrequency()
    }

    startDecay( npc: L2Npc ) {
        if ( this.isMinion( npc ) ) {
            return this.startMinionDecay( npc )
        }

        return this.startNormalDecay( npc )
    }

    private startMinionDecay( npc: L2Npc ): void {
        let masterId: number = this.getMinionLeaderId( npc.getObjectId() )
        if ( !masterId ) {
            return
        }

        let data = this.minions.data.get( npc.getObjectId() )
        if ( !data ) {
            return
        }

        let master = L2World.getObjectById( masterId ) as L2Npc
        if ( !master || master.isAlikeDead() ) {
            return
        }

        let delay = this.getMinionRespawnDelay( npc, master, data.respawnMs )
        if ( delay > 0 ) {
            this.decayTimeouts[ npc.getObjectId() ] = setTimeout( this.runMinionRespawnTask.bind( this ), delay, npc )
        }
    }

    private getMinionRespawnDelay( minion: L2Npc, master: L2Npc, defaultDelay: number = 0 ) : number {
        if ( master.isRaid() ) {
            return defaultDelay === 0 ? ConfigManager.npc.getRaidMinionRespawnTime() : defaultDelay
        }

        let respawnTime: number = ConfigManager.npc.getCustomMinionsRespawnTime()[ minion.getId() ]
        if ( !respawnTime || respawnTime < 0 ) {
            return defaultDelay
        }

        return respawnTime
    }

    isMinion( npc: L2Npc ): boolean {
        if ( this.minions ) {
            return this.minions.spawnedObjectIds.has( npc.getObjectId() )
        }

        return false
    }

    getMinionLeaderId( objectId: number ) : number {
        return this.minions.masters.get( objectId )
    }

    protected startNormalDecay( npc: L2Npc ): void {
        this.decrementCount()
        this.processNormalDecay( npc )
    }

    protected processNormalDecay( npc: L2Npc ) : void {
        if ( this.respawnTimes ) {
            let respawnTimeMs = this.respawnTimes[ npc.getObjectId() ]
            if ( respawnTimeMs ) {
                this.decayTimeouts[ npc.getObjectId() ] = setTimeout( this.runRespawnTask.bind( this ), respawnTimeMs, npc )
                return
            }
        }

        let delay = this.getRespawnDelay()
        if ( delay > 0 ) {
            this.decayTimeouts[ npc.getObjectId() ] = setTimeout( this.runRespawnTask.bind( this ), delay, npc )
        }
    }

    protected decrementCount(): void {
        this.parameters.count--
    }

    protected getRespawnDelay(): number {
        if ( this.data.respawnMs > 0 ) {
            return this.data.respawnMs + Math.random() * this.data.respawnExtraMs
        }

        return 0
    }

    getTemplate(): L2NpcTemplate {
        return DataManager.getNpcData().getTemplate( this.data.npcId )
    }

    stopRespawn(): void {
        Object.values( this.decayTimeouts ).forEach( value => clearTimeout( value ) )

        this.decayTimeouts = {}
        this.respawnTimes = null
    }

    getMakerData(): L2SpawnLogicItem {
        return this.maker
    }

    getSpawnData(): L2SpawnNpcDataItem {
        return this.data
    }

    remove( npc: L2Npc ) {
        clearTimeout( this.decayTimeouts[ npc.getObjectId() ] )
        delete this.decayTimeouts[ npc.getObjectId() ]

        if ( this.isMinion( npc ) ) {
            this.minions.spawnedObjectIds.delete( npc.getObjectId() )

            this.minions.data.delete( npc.getObjectId() )
            this.minions.masters.delete( npc.getObjectId() )

            return
        }

        if ( this.spawnedObjectIds.delete( npc.getObjectId() ) ) {
            this.decrementCount()
        }

        if ( this.minions ) {
            this.removeMinions( npc )
        }
    }

    hasSpawned(): boolean {
        return this.spawnedObjectIds.size > 0
    }

    getNpcs(): Array<L2Npc> {
        let npcs: Array<L2Npc> = []

        this.spawnedObjectIds.forEach( ( objectId: number ) => npcs.push( L2World.getObjectById( objectId ) as L2Npc ) )

        return _.compact( npcs )
    }

    getNpcObjectIds(): Array<number> {
        return Array.from( this.spawnedObjectIds )
    }

    startDespawn(): void {
        this.processDespawn()
    }

    protected processDespawn(): void {
        this.spawnedObjectIds.forEach( ( objectId: number ) => {
            this.decrementCount()

            let npc = L2World.getObjectById( objectId ) as L2Npc
            if ( !npc ) {
                return
            }

            npc.deleteMe()
        } )

        this.spawnedObjectIds.clear()

        if ( this.minions ) {
            this.minions.spawnedObjectIds.forEach( ( objectId: number ) => {
                let npc = L2World.getObjectById( objectId ) as L2Npc
                if ( !npc ) {
                    return
                }

                npc.deleteMe()
            } )

            this.minions.spawnedObjectIds.clear()
            this.minions.data.clear()
            this.minions.masters.clear()
            this.minions.minionIds.clear()
        }

        this.stopRespawn()
    }

    getAmount(): number {
        return this.data.amount
    }

    processSpawnEvent( eventType: SpawnLogicEventType, data: SpawnLogicEventData = null ): void {
        return this.onDefaultSpawnEvent( eventType, data )
    }

    protected onDefaultSpawnEvent( eventType: SpawnLogicEventType, data: SpawnLogicEventData = null ): void {
        switch ( eventType ) {
            case SpawnLogicEventType.Despawn:
                return this.startDespawn()

            case SpawnLogicEventType.Spawn:
                if ( this.hasSpawned() ) {
                    return
                }

                this.processStartSpawn()

                if ( !this.respawnTimes ) {
                    this.respawnTimes = {}
                }

                let respawnMs = data ? ( data as RespawnEventData ).respawnMs : 0
                this.spawnedObjectIds.forEach( ( objectId: number ) => {
                    this.respawnTimes[ objectId ] = respawnMs === 0 ? this.getRespawnDelay() : respawnMs
                } )
        }
    }

    protected sendEvent( makerName: string, eventType: SpawnLogicEventType, data: SpawnLogicEventData = null ): void {
        let makers = SpawnMakerCache.getMakerSpawn( makerName )

        if ( !makers ) {
            return
        }

        makers.forEach( ( logic: ISpawnLogic ) => logic.processSpawnEvent( eventType, data ) )
    }

    hasSpawnedMinions() : boolean {
        return this.minions && this.minions.spawnedObjectIds.size > 0
    }

    onMasterTeleported( master: L2Npc ) : void {
        let geometry = PointGeometry.acquire( GeometryId.RaidbossSpawnSmall )
        let x: number
        let y: number
        let z: number

        /*
            Small variation of minion spawn checks.
         */
        this.minions.masters.forEach( ( masterId: number, minionId: number ) => {
            if ( master.getObjectId() !== masterId ) {
                return
            }

            let minion = L2World.getObjectById( minionId ) as L2Npc
            if ( !minion ) {
                return
            }

            let counter : number = 0
            do {
                geometry.prepareNextPoint()

                x = master.getX() + geometry.getX()
                y = master.getY() + geometry.getY()
                z = GeoPolygonCache.getZ( x, y, master.getZ() + 20 )

                if ( counter > 100 ) {
                    throw new Error( `Unable to teleport minion npcId=${ minion.getId() } using ${ geometry.getGeometryId() } geometry from location (${ master.getX() },${ master.getY() },${ master.getZ() })` )
                }

                counter++
            } while ( GeoPolygonCache.isInvalidZ( z ) )

            minion.teleportToLocationCoordinates( x, y, z, minion.getHeading(), master.getInstanceId() )
        } )

        geometry.release()
    }

    removeMinions( npc: L2Npc ) : void {
        let npcsToDelete : Array<L2Npc> = []
        let minionIds = this.getMinionIds( npc )
        if ( !minionIds ) {
            return
        }

        minionIds.forEach( ( minionId: number ) => {
            let minion = L2World.getObjectById( minionId ) as L2Npc
            if ( !minion ) {
                return
            }

            npcsToDelete.push( minion )
        } )

        npcsToDelete.forEach( ( npc: L2Npc ) => npc.deleteMe() )
    }

    getMinionIds( npc: L2Npc ): Set<number> {
        return this.minions.minionIds.get( npc.getObjectId() )
    }

    private addMinionId( leaderId: number, minionId: number ) : void {
        let ids = this.minions.minionIds.get( leaderId )
        if ( !ids ) {
            ids = new Set<number>()
            this.minions.minionIds.set( leaderId, ids )
        }

        ids.add( minionId )
    }

    getId(): string {
        return this.id
    }
}