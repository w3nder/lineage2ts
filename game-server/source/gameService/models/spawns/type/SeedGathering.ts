import { L2DefaultSpawn } from './L2DefaultSpawn'
import { CycleStepManager } from '../../../cache/CycleStepManager'

export class SeedGathering extends L2DefaultSpawn {
    protected getRespawnDelay(): number {
        if ( this.data.respawnMs > 0 ) {
            let step = CycleStepManager.getStep( this.maker.aiParameters.fieldCycleId as number )
            let respawnTime = this.data.respawnMs * ( step === 2 ? 0.5 : 1 )
            return respawnTime + Math.random() * this.data.respawnExtraMs
        }

        return 0
    }
}