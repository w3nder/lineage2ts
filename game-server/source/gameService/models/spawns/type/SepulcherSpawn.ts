import { L2Npc } from '../../actor/L2Npc'
import { GameTime } from '../../../cache/GameTime'
import { L2InstancedSpawn } from './L2InstancedSpawn'

// TODO : need to add timer to despawn all npcs on minute 54 after each hour
// should be implemented using instance system to track if npcs exist
// instance should direct to spawn npcs or not

export class SepulcherSpawn extends L2InstancedSpawn {
    startDecay( npc: L2Npc ): void {
        if ( this.isSpawnProhibited() ) {
            return
        }

        return super.startDecay( npc )
    }

    protected assignSpawnLocation( npc: L2Npc, territoryId: string, heading: number = this.getRandomHeading() ) {
        if ( this.isSpawnProhibited() ) {
            return
        }

        super.assignSpawnLocation( npc, territoryId, heading )
    }

    protected isSpawnProhibited() : boolean {
        let minutes = GameTime.getMinutes()
        return minutes > 49 || minutes < 59
    }

    protected canSpawn(): boolean {
        if ( this.isSpawnProhibited() ) {
            return false
        }

        return super.canSpawn()
    }
}