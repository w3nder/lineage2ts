import { L2DefaultSpawn } from './L2DefaultSpawn'
import { SpawnEventManager } from '../SpawnEventManager'
import { FortEventSpawn } from '../SpawnEventType'
import { L2Npc } from '../../actor/L2Npc'
import { FortManager } from '../../../instancemanager/FortManager'
import { DoorManager } from '../../../cache/DoorManager'
import { EventListenerMethod } from '../../events/listeners/EventListenerData'
import { FortSpawnOperation } from '../../../enums/FortSpawnOperation'
import { FortEvent } from '../../../enums/FortEvent'

const doorNameProperties : Array<string> = [
    'fortressOutdoor1',
    'fortressOutdoor2'
]

export class FortressSpawn extends L2DefaultSpawn {
    respawnOperation: FortSpawnOperation
    callback: EventListenerMethod
    eventOperations: Record<number, number> // eventId to spawn operation mapping

    protected initializeSpawn() {
        let fortId = this.maker.aiParameters.fortressId as number
        this.callback = this.onEvent.bind( this )
        this.eventOperations = {}

        // TODO : implement un-registering for fortress events
        for ( let index = 1; index < 5; index++ ) {
            let eventId = this.maker.aiParameters[ `eventId${index}` ] as FortEvent
            if ( eventId ) {
                let spawnOperation = this.maker.aiParameters[ `isSpawn${index}` ] as FortSpawnOperation

                if ( Number.isInteger( spawnOperation ) ) {
                    this.eventOperations[ eventId ] = spawnOperation
                }

                SpawnEventManager.registerFortressEvent( fortId, eventId, this.callback )
            }
        }
    }

    onEvent( data: FortEventSpawn ) : void {
        let fortId = this.maker.aiParameters.fortressId as number
        let fort = FortManager.getFortById( fortId )

        if ( !fort ) {
            return
        }

        if ( data.eventId < 3 ) {
            if ( this.maker.aiParameters.isNpcOwn === 1 && fort.getOwnerClan() ) {
                return
            }

            if ( this.maker.aiParameters.isNpcOwn === -1 ) {
                return
            }
        }

        this.respawnOperation = this.eventOperations[ data.eventId ]

        /*
            Possible types:
            - 0, reinforcements level
            - 1, supply level
            - 2, door level
            - 3, cannon level
            - 4, scout level

            Possible levels:
            - 1
            - 2
            - 3
         */

        let featureLevel = this.maker.aiParameters.facilityLevel as number
        let featureType = this.maker.aiParameters.facilityType as number
        if ( featureLevel > -1 && featureType > -1 && fort.getFeatureLevel( featureType ) < featureLevel ) {
            return
        }

        if ( this.maker.aiParameters.castleContract as number === -1
            && data.operation === FortSpawnOperation.Normal
            && fort.getContractedCastleId() ) {
            return
        }

        return this.performFortOperations()
    }

    protected performFortOperations() : void {
        switch ( this.respawnOperation ) {
            case FortSpawnOperation.Despawn:
                return this.startDespawn()

            case FortSpawnOperation.Normal:
                this.startSpawn()
                return

            case FortSpawnOperation.Doors:
                return this.openDoors()
        }
    }

    startDecay( npc: L2Npc ) {
        if ( this.respawnOperation === 0 ) {
            return
        }

        super.startDecay( npc )
    }

    openDoors() : void {
        for ( const propertyName of doorNameProperties ) {
            let name = this.maker.aiParameters[ propertyName ]
            if ( !name ) {
                return
            }

            let door = DoorManager.getDoorByName( name as string )
            if ( !door || door.isOpen() ) {
                return
            }

            door.openMe()
        }
    }
}