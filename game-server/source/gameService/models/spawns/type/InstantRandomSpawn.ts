import { L2DefaultSpawn } from './L2DefaultSpawn'
import { RespawnEventData } from '../ISpawnLogic'
import _ from 'lodash'
import { L2Npc } from '../../actor/L2Npc'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

export class InstantRandomSpawn extends L2DefaultSpawn {
    startDecay( npc: L2Npc ) {
        this.remove( npc )

        let data : RespawnEventData = {
            respawnMs: ( this.maker.aiParameters.respawnTime as number ?? 0 ) * 1000
        }

        this.sendEvent( this.getRandomMaker(), SpawnLogicEventType.Spawn, data )
    }

    getRandomMaker() : string {
        let makerNames : Array<string> = [
            this.maker.aiParameters.makerName1 as string,
            this.maker.aiParameters.makerName2 as string,
            this.maker.aiParameters.makerName3 as string,
        ].filter( name => !!name )

        return _.sample( makerNames )
    }
}