import { L2DefaultSpawn } from './L2DefaultSpawn'
import { IInstanceSpawn } from '../ISpawnLogic'

export class L2InstancedSpawn extends L2DefaultSpawn implements IInstanceSpawn {
    private shouldSpawn: boolean = false

    setCanSpawn( value: boolean ): void {
        this.shouldSpawn = value
    }

    setInstanceId( instanceId: number ): void {
        this.instanceId = instanceId
    }

    protected canSpawn(): boolean {
        return this.shouldSpawn && super.canSpawn()
    }
}