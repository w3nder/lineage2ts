import { L2DefaultSpawn } from './L2DefaultSpawn'

export class InstantRespawn extends L2DefaultSpawn {
    protected getRespawnDelay(): number {
        return 0
    }
}