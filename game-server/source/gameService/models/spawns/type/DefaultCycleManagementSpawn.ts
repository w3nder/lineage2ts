import { L2DefaultSpawn } from './L2DefaultSpawn'
import { SpawnEventManager } from '../SpawnEventManager'
import { SpawnEventType } from '../SpawnEventType'
import _ from 'lodash'
import { CycleStepManager } from '../../../cache/CycleStepManager'
import { EventListenerMethod } from '../../events/listeners/EventListenerData'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

export class DefaultCycleManagementSpawn extends L2DefaultSpawn {

    callback: EventListenerMethod

    protected initializeSpawn() {
        this.callback = this.onHellboundCycleChange.bind( this )
        this.registerEvent()
    }

    deInitializeSpawn() {
        this.unRegisterEvent()
    }

    startSpawn(): number {
        if ( !this.shouldSpawn() ) {
            return 0
        }

        return this.processStartSpawn()
    }

    protected shouldSpawn() : boolean {
        let cycle = this.getRequiredCycle()
        let step = CycleStepManager.getStep( cycle )
        let minStep = this.maker.aiParameters?.thresholdMin as number ?? 0
        let maxStep = this.maker.aiParameters?.thresholdMax as number ?? 0

        if ( step < minStep || step > maxStep ) {
            return false
        }

        let maxPoints = this.maker.aiParameters?.thresholdMax as number ?? Number.MAX_SAFE_INTEGER
        let minPoints = this.maker.aiParameters?.thresholdMax as number ?? -1
        let points = CycleStepManager.getPoints( cycle )

        return minPoints >= points && points <= maxPoints
    }

    protected onHellboundCycleChange() : void {
        if ( !this.shouldSpawn() ) {
            return this.processSpawnEvent( SpawnLogicEventType.Despawn, null )
        }

        this.processSpawnEvent( SpawnLogicEventType.Spawn, null )
    }

    protected getRequiredCycle() : number {
        let cycle = this.maker.aiParameters?.fieldCycle as number
        if ( !cycle || !_.isNumber( cycle ) ) {
            return 0
        }

        return cycle
    }

    protected registerEvent() : void {
        SpawnEventManager.registerCallback( SpawnEventType.CycleStepChange, this.callback, new Set<number>( [ this.getRequiredCycle() ] ) )
    }

    protected unRegisterEvent() : void {
        SpawnEventManager.unRegisterCallback( SpawnEventType.CycleStepChange, this.callback )
    }
}