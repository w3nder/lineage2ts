import { ZoneEntry } from './ZoneEntry'
import { SignalParametersData } from '../ISpawnLogic'
import { DoorManager } from '../../../cache/DoorManager'
import { L2Npc } from '../../actor/L2Npc'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

export class FrintessaDoorControlA extends ZoneEntry {

    counter: number = 0

    processSpawnEvent( eventType: SpawnLogicEventType, data: SignalParametersData ) {
        if ( eventType !== SpawnLogicEventType.SignalParameters || !data ) {
            return
        }

        this.counter++

        if ( this.counter !== 0 ) {
            return
        }

        switch ( data.signalId ) {
            case 10001003:
                return this.onAllDie()
        }
    }

    private onAllDie() : void {
        this.doorOperation( [
            'frintessa_new_door_151',
            'frintessa_new_door_251',
            'frintessa_new_door_252',
        ], true )
    }

    private doorOperation( doorNames: Array<string>, isOpen: boolean ) : void {
        return doorNames.forEach( ( name: string ) : void => {
            let door = DoorManager.getDoorByName( name )
            if ( door ) {
                isOpen ? door.openMe() : door.closeMe()
            }
        } )
    }

    startDespawn() {
        this.counter = 0
        this.processDespawn()
    }

    startDecay( npc: L2Npc ) {
        this.counter = 0

        super.startDecay( npc )
    }
}