import { ZoneEntry } from './ZoneEntry'
import { SpawnLogicEventData } from '../ISpawnLogic'
import { SpawnLogicEventType } from '../../../enums/spawn/SpawnLogicEventType'

export class GuardSpawn extends ZoneEntry {

    processSpawnEvent( eventType: SpawnLogicEventType, data: SpawnLogicEventData = null ) : void {
        if ( eventType === SpawnLogicEventType.Despawn ) {
            /*
                TODO : see script boostup_73lv_tele_trap
                - despawn trap
                - drop items
             */
            return this.startDespawn()
        }
    }
}