import { L2Clan } from '../L2Clan'
import { L2ArtefactInstance } from '../actor/instance/L2ArtefactInstance'
import { AbstractResidence } from './AbstractResidence'
import { L2Object } from '../L2Object'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { Territory } from '../../instancemanager/territory/Territory'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { SkillCache } from '../../cache/SkillCache'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ClanCache } from '../../cache/ClanCache'
import { ConfigManager } from '../../../config/ConfigManager'
import { CastleManager } from '../../instancemanager/CastleManager'
import { MountType } from '../../enums/MountType'
import { L2World } from '../../L2World'
import { FortManager } from '../../instancemanager/FortManager'
import { Skill } from '../Skill'
import { DatabaseManager } from '../../../database/manager'
import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { L2DoorInstance } from '../actor/instance/L2DoorInstance'
import { L2CastleFunctionsTableData } from '../../../database/interface/CastleFunctionsTableApi'
import { L2CastleDoorUpgradeTableData } from '../../../database/interface/CastleDoorUpgradeTableApi'
import { SiegeManager } from '../../instancemanager/SiegeManager'
import { TowerSpawn } from '../TowerSpawn'
import { Siege } from './Siege'
import { ItemTypes } from '../../values/InventoryValues'
import aigle from 'aigle'
import _ from 'lodash'
import { DoorManager } from '../../cache/DoorManager'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'
import { CastleUpdateType } from '../../enums/CastleUpdateType'
import { AreaCache } from '../../cache/AreaCache'
import { AreaType } from '../areas/AreaType'
import { CastleSiegeArea } from '../areas/type/CastleSiege'
import { CastleTeleportArea } from '../areas/type/CastleTeleport'
import { CastleArea } from '../areas/type/Castle'
import { FameOperations } from './FameOperations'
import { ResidenceTeleportType } from '../../enums/ResidenceTeleportType'
import { Location } from '../Location'
import Timeout = NodeJS.Timeout
import { ResidenceType } from '../../enums/ResidenceType'

const enum CastleIds {
    Aden = 5,
    Goddart = 7,
    Rune = 8,
    Schuttgart = 9
}

export class Castle extends AbstractResidence implements FameOperations {
    doors: Array<number> = []
    ownerId: number = 0
    siege: Siege = null
    siegeDate: number
    isTimeRegistrationOver: boolean = true // true if Castle Lords set the time, or 24h is elapsed after the siege
    siegeTimeRegistrationEndDate: number // last siege end date + 1 day
    taxPercent: number = 0
    taxRate: number = 0
    treasury: number = 0
    showNpcCrest: boolean = false
    formerOwner: L2Clan
    artefacts: Array<number> = []
    functions: { [ key: number ]: CastleFunction }
    ticketBuyCount: number = 0

    isLoaded: boolean = false
    private updateTypes: Set<CastleUpdateType> = new Set<CastleUpdateType>()
    addToRuneTax: boolean
    addToAdenTax: boolean
    siegeActive: boolean = false

    /*
        Assigned castle areas.
     */
    siegeArea: CastleSiegeArea
    residenceArea: CastleArea
    teleportArea: CastleTeleportArea

    constructor( residenceId: number ) {
        super( residenceId )

        this.type = ResidenceType.Castle
        this.addToRuneTax = this.getResidenceId() === CastleIds.Schuttgart || this.getResidenceId() === CastleIds.Goddart
        this.addToAdenTax = this.getResidenceId() !== CastleIds.Schuttgart
            && this.getResidenceId() !== CastleIds.Goddart
            && this.getResidenceId() !== CastleIds.Aden
            && this.getResidenceId() !== CastleIds.Rune
    }

    private scheduleUpdate( type: CastleUpdateType ) : void {
        this.updateTypes.add( type )
        CastleManager.scheduleUpdate( this )
    }

    addToTreasury( amount: number ) : void {

        if ( this.getOwnerId() <= 0 ) {
            return
        }

        if ( this.addToRuneTax ) {
            let rune: Castle = CastleManager.getCastleById( CastleIds.Rune )
            if ( rune != null ) {
                let runeTax = Math.floor( amount * rune.getTaxRate() )
                if ( rune.getOwnerId() > 0 ) {
                    rune.addToTreasury( runeTax )
                }
                amount -= runeTax
            }
        }

        if ( this.addToAdenTax ) {
            let aden: Castle = CastleManager.getCastleById( CastleIds.Aden )
            if ( aden ) {
                let adenTax = Math.floor( amount * aden.getTaxRate() )
                if ( aden.getOwnerId() > 0 ) {
                    aden.addToTreasury( adenTax )
                }

                amount -= adenTax
            }
        }

        this.addToTreasuryNoTax( amount )
    }

    addToTreasuryNoTax( amount: number ) : void {
        if ( this.getOwnerId() <= 0 ) {
            return
        }

        if ( amount < 0 ) {
            amount *= -1
            if ( this.treasury < amount ) {
                return
            }

            this.treasury -= amount
        } else {
            let maxAdena = getMaxAdena()
            if ( this.treasury >= maxAdena ) {
                return
            }

            if ( ( maxAdena - this.treasury ) >= amount ) {
                this.treasury = maxAdena
            } else {
                this.treasury += amount
            }
        }

        this.scheduleUpdate( CastleUpdateType.Treasury )
    }

    isInSiegeArea( x: number, y: number, z: number ): boolean {
        return this.siegeArea.isInsideWithCoordinates( x, y, z )
    }

    closeDoor( player: L2PcInstance, doorId: number ): void {
        this.openCloseDoor( player, doorId, false )
    }

    engrave( clan: L2Clan, target: L2Object ) {
        if ( !this.artefacts.includes( target.objectId ) ) {
            return
        }

        this.setOwner( clan )
        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.CLAN_S1_ENGRAVED_RULER )
                .addString( clan.getName() )
                .getBuffer()
        this.getSiege().announceDataToPlayer( packet, true )
    }

    getArtefacts() {
        return this.artefacts
    }

    getDoor( doorId: number ): L2DoorInstance {
        if ( doorId <= 0 ) {
            return null
        }

        let id : number = this.getDoors().find( ( objectId: number ) => {
            let door = L2World.getObjectById( objectId ) as L2DoorInstance
            return door && door.getId() === doorId
        } )

        return L2World.getObjectById( id ) as L2DoorInstance
    }

    /*
        TODO : use spatial lookup by DoorManager to cache valid doors in current castle zone
     */
    getDoors() {
        return this.doors
    }

    getFunction( type: number ): CastleFunction {
        return this.functions[ type ]
    }

    getIsTimeRegistrationOver() {
        return this.isTimeRegistrationOver
    }

    getOwnerId() {
        return this.ownerId
    }

    async giveResidentialSkills( player: L2PcInstance ) : Promise<void> {
        let territory: Territory = TerritoryWarManager.getTerritory( this.getResidenceId() )
        if ( territory && territory.getOwnedWardIds().includes( this.getResidenceId() + 80 ) ) {
            await aigle.resolve( territory.getOwnedWardIds() ).each( ( wardId: number ) => {
                let skills: Array<Skill> = SkillCache.getAvailableResidentialSkills( wardId )

                return aigle.resolve( skills ).each( ( skill: Skill ) => {
                    return player.addSkill( skill )
                } )
            } )
        }

        return super.giveResidentialSkills( player )
    }

    async removeResidentialSkills( player: L2PcInstance ) : Promise<any> {
        let territory = TerritoryWarManager.getTerritory( this.getResidenceId() )
        if ( territory ) {
            await aigle.resolve( territory.getOwnedWardIds() ).each( ( wardId: number ) => {
                let territorySkills: Array<Skill> = SkillCache.getAvailableResidentialSkills( wardId )

                return aigle.resolve( territorySkills ).each( ( skill: Skill ) => {
                    return player.removeSkill( skill, false, true )
                } )
            } )
        }

        return super.removeResidentialSkills( player )
    }

    getShowNpcCrest() {
        return this.showNpcCrest
    }

    getSiege() {
        if ( !this.siege ) {
            this.siege = new Siege( this )
        }

        return this.siege
    }

    getSiegeDate() {
        return this.siegeDate
    }

    getTaxPercent() {
        return this.taxPercent
    }

    getTaxRate() {
        return this.taxRate
    }

    getTicketBuyCount() {
        return this.ticketBuyCount
    }

    getTimeRegistrationOverDate() {
        if ( !this.siegeTimeRegistrationEndDate ) {
            this.siegeTimeRegistrationEndDate = Date.now()
        }

        return this.siegeTimeRegistrationEndDate
    }

    initAreas() : void {
        this.siegeArea = AreaCache.getAreaByResidenceId( this.getResidenceId(), AreaType.CastleSiege ) as CastleSiegeArea
        this.teleportArea = AreaCache.getAreaByResidenceId( this.getResidenceId(), AreaType.CastleTeleport ) as CastleTeleportArea
        this.residenceArea = AreaCache.getAreaByResidenceId( this.getResidenceId(), AreaType.Castle ) as CastleArea
    }

    async initialize() {
        await Promise.all( [
            this.loadFunctions(),
            this.loadDoorUpgrade()
        ] )

        this.isLoaded = true
    }

    async loadDoorUpgrade() : Promise<void> {
        let data: Array<L2CastleDoorUpgradeTableData> = await DatabaseManager.getCastleDoorUpgrade().getAll( this.residenceId )

        await aigle.resolve( data ).eachSeries( async ( item: L2CastleDoorUpgradeTableData ) => {
            return this.setDoorUpgrade( item.doorId, item.ratio, false )
        } )
    }

    async loadFunctions() {
        let residenceId = this.residenceId
        let data: Array<L2CastleFunctionsTableData> = await DatabaseManager.getCastleFunctions().getAll( residenceId )

        this.functions = data.reduce( ( finalMap: { [ type: number ]: CastleFunction }, item: L2CastleFunctionsTableData ) => {
            finalMap[ item.type ] = new CastleFunction( item.type, item.level, item.fee, 0, item.rate, item.endTime, true, residenceId )

            return finalMap
        }, {} )
    }

    openCloseDoor( player: L2PcInstance, doorId: number, isOpen: boolean ) {
        if ( player.getClanId() !== this.getOwnerId() ) {
            return
        }

        let door: L2DoorInstance = this.getDoor( doorId )
        if ( door ) {
            isOpen ? door.openMe() : door.closeMe()
        }
    }

    openDoor( player: L2PcInstance, doorId: number ) {
        this.openCloseDoor( player, doorId, true )
    }

    removeAllPlayers(): Promise<void> {
        return this.teleportArea.banishAllPlayers()
    }

    registerArtifact( artefact: L2ArtefactInstance ) {
        this.artefacts.push( artefact.getObjectId() )
    }

    async removeFunction( type: number ): Promise<void> {
        _.unset( this.functions, type )
        await DatabaseManager.getCastleFunctions().remove( this.getResidenceId(), type )
    }

    async setDoorUpgrade( doorId: number, ratio: number, shouldSave: boolean = false ): Promise<void> {
        let door: L2DoorInstance = DoorManager.getDoor( doorId )
        if ( !door ) {
            return
        }

        door.getStat().setUpgradeHpRatio( ratio )
        door.setCurrentHp( door.getMaxHp() )

        if ( shouldSave ) {
            await DatabaseManager.getCastleDoorUpgrade().update( this.getResidenceId(), doorId, ratio )
        }
    }

    setIsTimeRegistrationOver( value: boolean ) {
        this.isTimeRegistrationOver = value
    }

    async setOwner( clan: L2Clan ) : Promise<void> {
        let ownerClanId = this.getOwnerId()
        let castle = this

        if ( ( ownerClanId > 0 ) && ( !clan || ( clan.getId() !== ownerClanId ) ) ) {
            let oldOwner = ClanCache.getClan( ownerClanId )
            if ( oldOwner ) {
                if ( !this.formerOwner ) {
                    this.formerOwner = oldOwner
                    if ( ConfigManager.character.removeCastleCirclets() ) {
                        await CastleManager.removeCirclet( this.formerOwner, this.getResidenceId() )
                    }
                }

                let oldleader: L2PcInstance = oldOwner.getLeader().getPlayerInstance()
                if ( oldleader ) {
                    if ( oldleader.getMountType() === MountType.WYVERN ) {
                        await oldleader.dismount()
                    }
                }


                oldOwner.setCastleId( 0 ) // Unset has castle flag for old owner
                await aigle.resolve( oldOwner.getOnlineMembers( 0 ) ).each( async ( memberId: number ) => {
                    let member = L2World.getPlayer( memberId )
                    await castle.removeResidentialSkills( member )
                    member.sendSkillList()
                } )
            }
        }

        await this.updateOwnerInDatabase( clan )
        this.setShowNpcCrest( false )

        // if clan have fortress, remove it
        if ( clan && ( clan.getFortId() > 0 ) ) {
            await FortManager.getFortByOwner( clan ).removeOwner()
        }

        if ( this.getSiege().isInProgress() ) {
            await this.getSiege().midVictory() // Mid victory phase of siege
        }

        TerritoryWarManager.getTerritory( this.getResidenceId() ).setOwnerClan( clan )

        if ( clan ) {
            await aigle.resolve( clan.getOnlineMembers( 0 ) ).each( async ( memberId: number ) => {
                let member = L2World.getPlayer( memberId )
                await castle.giveResidentialSkills( member )
                member.sendSkillList()
            } )
        }
    }

    setShowNpcCrest( value: boolean ) {
        /*
            TODO : should existing npcs be updated to show no crest?
         */
        if ( this.showNpcCrest !== value ) {
            this.showNpcCrest = value
            this.scheduleUpdate( CastleUpdateType.CrestStatus )
        }
    }

    setSiegeDate( value: number ) {
        this.siegeDate = value
    }

    async setTaxPercent( value: number ) {
        this.taxPercent = value
        this.taxRate = value / 100

        return this.scheduleUpdate( CastleUpdateType.Tax )
    }

    setTicketBuyCount( value: number ) : void {
        this.ticketBuyCount = value
        this.scheduleUpdate( CastleUpdateType.TicketCount )
    }

    async updateFunctions( player: L2PcInstance, type: number, level: number, fee: number, rate: number, shouldAdd: boolean ): Promise<boolean> {
        if ( !player ) {
            return false
        }

        if ( fee > 0 ) {
            if ( !await player.destroyItemByItemId( ItemTypes.Adena, fee, true, 'Castle.updateFunctions' ) ) {
                return false
            }
        }

        if ( shouldAdd ) {
            this.functions[ type ] = new CastleFunction( type, level, fee, 0, rate, 0, false, this.getResidenceId() )
            return true
        }

        if ( ( level === 0 ) && ( fee === 0 ) ) {
            await this.removeFunction( type )
            return true
        }

        let feeDifference = fee - this.functions[ type ].fee
        if ( feeDifference > 0 ) {
            _.unset( this.functions, type )
            this.functions[ type ] = new CastleFunction( type, level, fee, 0, rate, -1, false, this.getResidenceId() )
        } else {
            let currentFunction: CastleFunction = this.functions[ type ]

            currentFunction.fee = fee
            currentFunction.level = level
            await currentFunction.saveData()
        }

        return true
    }

    async updateOwnerInDatabase( clan: L2Clan ) {
        if ( clan ) {
            this.ownerId = clan.getId() // Update owner id property
        } else {
            this.ownerId = 0 // Remove owner
            await CastleManorManager.resetManorData( this.getResidenceId() )
        }

        await DatabaseManager.getClanDataTable().resetCastleStatus( this.getResidenceId() )
        await DatabaseManager.getClanDataTable().setCastleOwnership( this.getResidenceId(), this.getOwnerId() )
    }

    banishForeigners() : Promise<unknown> {
        return this.residenceArea.banishOutsiders( this.getOwnerId() )
    }

    getTreasury() {
        return this.treasury
    }

    getTrapUpgradeLevel( index: number ) : number {
        let spawn : TowerSpawn = SiegeManager.getFlameTowers( this.getResidenceId() )[ index ]
        return spawn ? spawn.getUpgradeLevel() : 0
    }

    async setTrapUpgrade( index: number, level: number, shouldSave: boolean ) : Promise<void> {
        if ( shouldSave ) {
            await DatabaseManager.getCastleTrapUpgrade().update( this.getResidenceId(), index, level )
        }

        let spawn : TowerSpawn = _.nth( SiegeManager.getFlameTowers( this.getResidenceId() ), index )
        if ( spawn ) {
            spawn.setUpgradeLevel( level )
        }
    }

    async removeTrapUpgrade() : Promise<void> {
        _.each( SiegeManager.getFlameTowers( this.getResidenceId() ), ( spawn: TowerSpawn ) => {
            spawn.setUpgradeLevel( 0 )
        } )

        await DatabaseManager.getCastleTrapUpgrade().remove( this.getResidenceId() )
    }

    getOwner() {
        return this.hasOwner() ? ClanCache.getClan( this.ownerId ) : null
    }

    hasOwner() : boolean {
        return this.ownerId !== 0
    }

    isActivated() : boolean {
        return this.isLoaded
    }

    hasUpdateType( type: CastleUpdateType ) : boolean {
        return this.updateTypes.has( type )
    }

    clearUpdates() : void {
        this.updateTypes.clear()
    }

    canGiveFame(): boolean {
        return true
    }

    getFameAmount(): number {
        return ConfigManager.character.getCastleFameIntervalPoints()
    }

    getFameInterval(): number {
        return ConfigManager.character.getCastleFameInterval()
    }

    isSiegeActive(): boolean {
        return this.siegeActive
    }

    getResidenceTeleport( type: ResidenceTeleportType ) : Location {
        return this.residenceArea.getRandomTeleportLocation( type )
    }
}

export class CastleFunction {
    type: number
    level: number
    fee: number
    temporaryFee: number
    rate: number
    endDate: number
    inDebt: number
    hasClanWarehouse: boolean
    task: Timeout
    residenceId: number

    constructor( type: number, level: number, fee: number, temporaryFee: number, rate: number, endDate: number, hasWarehouse: boolean, residenceId: number ) {
        this.type = type
        this.level = level
        this.fee = fee
        this.temporaryFee = temporaryFee
        this.rate = rate
        this.endDate = endDate
        this.hasClanWarehouse = hasWarehouse
        this.residenceId = residenceId

        this.startTask()
    }

    getLevel() {
        return this.level
    }

    getOwnerId(): number {
        return CastleManager.getCastleById( this.residenceId ).getOwnerId()
    }

    async runTask(): Promise<void> {
        let ownerId = this.getOwnerId()
        if ( ownerId <= 0 ) {
            return
        }

        let clan : L2Clan = ClanCache.getClan( ownerId )
        if ( clan && ( clan.getWarehouse().getAdenaAmount() >= this.fee || !this.hasClanWarehouse ) ) {
            let fee = this.fee
            if ( this.endDate === -1 ) {
                fee = this.temporaryFee
            }

            this.endDate = Date.now() + this.rate
            await this.saveData()

            if ( this.hasClanWarehouse ) {
                await clan.getWarehouse().destroyItemByItemId( ItemTypes.Adena, fee, clan.getLeaderId(), 'CastleFunction.runTask' )
            }

            this.task = setTimeout( this.runTask.bind( this ), this.rate )
            return
        }

        await CastleManager.getCastleById( this.residenceId ).removeFunction( this.type )
    }

    saveData(): Promise<void> {
        return DatabaseManager.getCastleFunctions().update( this.residenceId, this.type, this.level, this.fee, this.rate, this.endDate )
    }

    startTask() {
        if ( this.getOwnerId() <= 0 ) {
            return
        }

        let currentTime = Date.now()
        if ( this.endDate > currentTime ) {
            this.task = setTimeout( this.runTask.bind( this ), this.endDate - currentTime )
            return
        }

        setImmediate( this.runTask.bind( this ) )
    }
}