import { Siegable, SiegableType } from '../Siegable'
import { L2SiegeClan } from '../../L2SiegeClan'
import { SiegableHall } from './SiegableHall'
import { L2Clan } from '../../L2Clan'
import { ListenerLogic } from '../../ListenerLogic'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { SiegeStatus } from '../../../enums/SiegeStatus'
import { ISpawnLogic } from '../../spawns/ISpawnLogic'
import Timeout = NodeJS.Timeout
import { SiegeClanType } from '../../../enums/SiegeClanType'

export const ClanHallSiegeEngineValues = {
    FORTRESS_RESSISTANCE: 21,
    DEVASTATED_CASTLE: 34,
    BANDIT_STRONGHOLD: 35,
    RAINBOW_SPRINGS: 62,
    BEAST_FARM: 63,
    FORTRESS_OF_DEAD: 64,
}

export class ClanHallSiegeEngine extends ListenerLogic implements Siegable {
    attackers: { [ key: number] : L2SiegeClan } = {}
    guards: Array<ISpawnLogic>
    hall: SiegableHall
    siegeTask: Timeout
    missionAccomplished: boolean = false

    checkIsAttacker( clan: L2Clan ): boolean {
        return false
    }

    checkIsDefender( clan: L2Clan ): boolean {
        return false
    }

    async endSiege(): Promise<void> {
    }

    getAttackerClan( clan: L2Clan ): L2SiegeClan {
        return this.attackers[ clan.getId() ]
    }

    getAttackerClans(): Array<L2SiegeClan> {
        return undefined
    }

    getAttackersInZone(): Array<number> {
        return undefined
    }

    getDefenderClan( clan: L2Clan ): L2SiegeClan {
        return undefined
    }

    getDefenderClans(): Array<L2SiegeClan> {
        return undefined
    }

    getFameAmount(): number {
        return 0
    }

    getFameFrequency(): number {
        return 0
    }

    getFlag( clan: L2Clan ): Array<number> {
        let siegeClan : L2SiegeClan = this.getAttackerClan( clan )
        if ( siegeClan ) {
            return siegeClan.getFlag()
        }

        return null
    }

    getSiegeDate(): number {
        return 0
    }

    giveFame(): boolean {
        return false
    }

    async startSiege(): Promise<void> {
    }

    updateSiege(): void {
        this.cancelSiegeTask()

        this.siegeTask = setTimeout( this.runPrepareOwnerTask.bind( this ), this.hall.getNextSiegeTime() - 3600000 )
    }

    getInnerSpawnLocation( player: L2PcInstance ) {
        return null
    }

    getAttackers() {
        return this.attackers
    }

    isInstance( type: SiegableType ): boolean {
        return type === SiegableType.ClanHallSiegeEngine
    }

    cancelSiegeTask() {
        if ( this.siegeTask ) {
            clearTimeout( this.siegeTask )
        }

        this.siegeTask = null
    }

    async runPrepareOwnerTask() : Promise<void> {
        if ( this.hall.getOwnerId() > 0 ) {
            this.attackers[ this.hall.getOwnerId() ] = new L2SiegeClan( this.hall.getOwnerId(), SiegeClanType.Attacker )
        }

        this.hall.free()
        await this.hall.banishForeigners()

        let message : Buffer = new SystemMessageBuilder( SystemMessageIds.REGISTRATION_TERM_FOR_S1_ENDED )
                .addString( this.getName() )
                .getBuffer()
        BroadcastHelper.toAllOnlinePlayersData( message )

        this.hall.updateSiegeStatus( SiegeStatus.WAITING_BATTLE )
        this.siegeTask = setTimeout( this.runSiegeStartTask.bind( this ), 3600000 )
    }

    runSiegeStartTask() {
        // TODO : implement me
    }
}