import { ClanHall } from '../ClanHall'
import { L2Clan } from '../../L2Clan'
import { ClanCache } from '../../../cache/ClanCache'
import { DatabaseManager } from '../../../../database/manager'
import { ClanHallManager } from '../../../instancemanager/ClanHallManager'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { AuctionManager } from '../../../instancemanager/AuctionManager'
import { ItemTypes } from '../../../values/InventoryValues'
import _ from 'lodash'
import Timeout = NodeJS.Timeout
import moment from 'moment'
import { ClanHallFunction } from '../../clan/ClanHallFunction'

const dayInterval = 3600000 * 24

export class AuctionableHall extends ClanHall {
    paidUntil: number
    grade: number
    isPaidOff: boolean
    lease: number
    chargeInterval: number = 604800000 // 7 days
    freeingTask: Timeout

    free() {
        super.free()
        this.paidUntil = 0
        this.isPaidOff = false
    }

    /*
        TODO: aggregate clan function retrieval and perform only one database query
        - move base initialization logic out of promise returning function
     */
    async initialize(): Promise<void> {
        await super.initialize()

        if ( this.getOwnerId() !== 0 ) {
            this.isFree = false
            this.initializeTask( false )
            let currentFunctions: Array<ClanHallFunction> = await DatabaseManager.getClanhallFunctions().getById( this.getId() )

            this.functions = _.keyBy( currentFunctions, ( value: ClanHallFunction ) => value.type )
        }
    }

    getGrade() {
        return this.grade
    }

    getLeaseAmount() {
        return this.lease
    }

    isPaid() {
        return this.isPaidOff
    }

    initializeTask( isForced: boolean ) {
        let currentTime = Date.now()

        if ( this.paidUntil > currentTime ) {
            this.freeingTask = setTimeout( this.runFreeTask.bind( this ), this.paidUntil - currentTime )
            return
        }

        if ( !this.isPaidOff && !isForced ) {
            if ( ( currentTime + dayInterval ) <= ( this.paidUntil + this.chargeInterval ) ) {
                this.freeingTask = setTimeout( this.runFreeTask.bind( this ), currentTime + dayInterval )
            } else {
                this.freeingTask = setTimeout( this.runFreeTask.bind( this ), ( this.paidUntil + this.chargeInterval ) - currentTime )
            }

            return
        }

        if ( !this.isFree ) {
            this.runFreeTask()
        }
    }

    // TODO : review paidUntil logic with usage of this.chargeInterval
    // Simplify and reorganize to make logic understandable, since in current state it is hard to reason about names
    async runFreeTask(): Promise<void> {
        if ( this.isFree ) {
            return
        }

        let currentTime = Date.now()
        if ( this.paidUntil > currentTime ) {
            this.freeingTask = setTimeout( this.runFreeTask.bind( this ), this.paidUntil - currentTime )
            return
        }

        let clan: L2Clan = ClanCache.getClan( this.getOwnerId() )

        if ( !clan ) {
            return
        }

        if ( clan.getWarehouse().getAdenaAmount() >= this.getLeaseAmount() ) {
            if ( this.paidUntil !== 0 && this.paidUntil <= currentTime ) {
                this.paidUntil += Math.ceil( ( currentTime - this.paidUntil ) / this.chargeInterval ) * this.chargeInterval
            } else {
                this.paidUntil = currentTime + this.chargeInterval
            }

            if ( await clan.getWarehouse().destroyItemByItemId( ItemTypes.Adena, this.getLeaseAmount(), clan.getLeaderId(), 'AuctionableHall.runFreeTask' ) ) {
                this.isPaidOff = true

                this.freeingTask = setTimeout( this.runFreeTask.bind( this ), this.paidUntil - currentTime )
                return ClanHallManager.scheduleHallUpdate( this.getId() )
            }
        }

        this.isPaidOff = false
        ClanHallManager.scheduleHallUpdate( this.getId() )

        let expirationTime = this.paidUntil + this.chargeInterval
        if ( currentTime > expirationTime ) {
            await AuctionManager.initNPC( this.getId() )

            ClanHallManager.setFree( this.getId() )
            clan.broadcastDataToOnlineMembers( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_CLAN_HALL_FEE_IS_ONE_WEEK_OVERDUE_THEREFORE_THE_CLAN_HALL_OWNERSHIP_HAS_BEEN_REVOKED ) )
            return
        }

        if ( ( currentTime + dayInterval ) <= expirationTime ) {
            clan.broadcastDataToOnlineMembers( this.getDisplayNoticePacket() )

            this.freeingTask = setTimeout( this.runFreeTask.bind( this ), expirationTime - ( currentTime + dayInterval ) )
            return
        }

        this.freeingTask = setTimeout( this.runFreeTask.bind( this ), expirationTime - currentTime )
    }

    setOwner( clan: L2Clan ): void {
        super.setOwner( clan )

        this.paidUntil = Date.now()
        this.initializeTask( true )
    }

    getDescription() {
        return this.description
    }

    getDisplayNoticePacket() : Buffer {
        let expirationTime = this.paidUntil + this.chargeInterval
        return new SystemMessageBuilder( SystemMessageIds.PAYMENT_FOR_YOUR_CLAN_HALL_HAS_NOT_BEEN_MADE_PLEASE_MAKE_PAYMENT_TO_YOUR_CLAN_WAREHOUSE_BY_S1_TOMORROW )
                .addString( moment( expirationTime ).format( 'HH A' ) )
                .getBuffer()
    }
}