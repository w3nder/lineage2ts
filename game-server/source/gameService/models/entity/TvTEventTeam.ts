import { Location } from '../Location'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import _ from 'lodash'

export class TvTEventTeam {
    /** The name of the team. */
    name: string
    /** The team spot coordinates. */
    location: Location
    /** The points of the team. */
    points: number
    /** Name and instance of all participated players in map. */
    participatingPlayers: Array<number> = []

    containsPlayer( playerId: number ) {
        return this.participatingPlayers.includes( playerId )
    }

    addPlayer( player: L2PcInstance ) : boolean {
        if ( !player ) {
            return false
        }

        this.participatingPlayers.push( player.getObjectId() )

        return true
    }

    getLocation() : Location {
        return this.location
    }

    removePlayer( objectId: number ) {
        _.pull( this.participatingPlayers, objectId )
    }

    increasePoints() {
        this.points = this.points + 1
    }
}