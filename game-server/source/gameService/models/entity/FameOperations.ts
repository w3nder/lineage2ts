export interface FameOperations {
    canGiveFame() : boolean
    getFameInterval() : number
    getFameAmount() : number
}