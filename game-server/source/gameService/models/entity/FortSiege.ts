import { Siegable, SiegableType } from './Siegable'
import { L2Clan } from '../L2Clan'
import { L2SiegeClan } from '../L2SiegeClan'
import { Fort } from './Fort'
import { FortSiegeManager } from '../../instancemanager/FortSiegeManager'
import { ClanCache } from '../../cache/ClanCache'
import { PacketDispatcher } from '../../PacketDispatcher'
import { DatabaseManager } from '../../../database/manager'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { FortManager } from '../../instancemanager/FortManager'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { EventType, FortSiegeFinishEvent, FortSiegeStartEvent } from '../events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { CombatFlag } from '../CombatFlag'
import { L2World } from '../../L2World'
import { FortTeleportWhoType } from '../../enums/FortTeleportWhoType'
import { TeleportWhereType } from '../../enums/TeleportWhereType'
import { FortSiegeSpawn } from '../FortSiegeSpawn'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { L2FortCommanderInstance } from '../actor/instance/L2FortCommanderInstance'
import { NpcStringIds } from '../../packets/NpcStringIds'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { SiegeRole } from '../../enums/SiegeRole'
import _ from 'lodash'
import { PlayerRelationStatus } from '../../cache/PlayerRelationStatusCache'
import { ISpawnLogic } from '../spawns/ISpawnLogic'
import Timeout = NodeJS.Timeout
import { SiegeClanType } from '../../enums/SiegeClanType'
import { NpcSayType } from '../../enums/packets/NpcSayType'

export class FortSiege implements Siegable {
    attackerClans: Array<L2SiegeClan> = []
    commanders: Array<ISpawnLogic> = []
    fort: Fort
    isInProgressValue: boolean = false
    siegeEndTask: Timeout
    siegeRestoreTask: Timeout
    siegeStartTask: Timeout

    constructor( fort: Fort ) {
        this.fort = fort

        // TODO : figure out this.checkAutoTask()
        FortSiegeManager.addSiege( this )
    }

    checkIsAttacker( clan: L2Clan ): boolean {
        return false
    }

    checkIsDefender( clan: L2Clan ): boolean {
        return false
    }

    async endSiege(): Promise<void> {
        if ( !this.isInProgress() ) {
            return
        }

        this.isInProgressValue = false
        this.removeFlags() // Removes all flags. Note: Remove flag before teleporting players
        this.unSpawnFlags()

        this.updatePlayerSiegeStateFlags( true )

        let currentFort = this.getFort()

        await currentFort.onSiegeEnd()

        this.resetSiegeDate()
        await this.clearSiegeClans()

        this.setSiegeDateTime( true )

        if ( this.siegeEndTask ) {
            clearTimeout( this.siegeEndTask )
            this.siegeEndTask = null
        }

        this.stopSiegeRestoreTask()

        if ( ListenerCache.hasGeneralListener( EventType.FortSiegeFinish ) ) {
            let eventData: FortSiegeFinishEvent = {
                fortId: currentFort.getResidenceId(),
            }

            await ListenerCache.sendGeneralEvent( EventType.FortSiegeFinish, eventData, currentFort.getResidenceId() )
        }
    }

    getAttackerClan( clan: L2Clan ): L2SiegeClan {
        return undefined
    }

    getAttackerClans(): Array<L2SiegeClan> {
        return undefined
    }

    getAttackersInZone(): Array<number> {
        return undefined
    }

    getDefenderClan( clan: L2Clan ): L2SiegeClan {
        return undefined
    }

    getDefenderClans(): Array<L2SiegeClan> {
        return undefined
    }

    getFameAmount(): number {
        return 0
    }

    getFameFrequency(): number {
        return 0
    }

    getFlag( clan: L2Clan ): Array<number> {
        return undefined
    }

    getSiegeDate(): number {
        return 0
    }

    giveFame(): boolean {
        return true
    }

    async startSiege(): Promise<void> {
        if ( this.isInProgress() ) {
            return
        }

        if ( this.siegeStartTask ) {
            clearTimeout( this.siegeStartTask )
            this.siegeStartTask = null
        }

        if ( _.isEmpty( this.getAttackerClans() ) ) {
            return
        }

        this.isInProgressValue = true

        await this.loadSiegeClans()
        this.updatePlayerSiegeStateFlags( false )
        await this.teleportPlayers( FortTeleportWhoType.Attacker, TeleportWhereType.TOWN )

        // TODO : send event to siege spawns to create npcs
        let fort = this.getFort()
        await fort.onSiegeStart()

        this.siegeEndTask = setTimeout( this.runEndSiegeTask.bind( this ), ConfigManager.fortSiege.getSiegeLength() * 60000 )

        let message: Buffer = new SystemMessageBuilder( SystemMessageIds.THE_FORTRESS_BATTLE_S1_HAS_BEGUN )
                .addCastleId( fort.getResidenceId() )
                .getBuffer()
        this.announceDataToPlayers( message )
        this.resetSiegeDate()

        if ( ListenerCache.hasGeneralListener( EventType.FortSiegeStart ) ) {
            let eventData: FortSiegeStartEvent = {
                fortId: fort.getResidenceId(),
            }

            await ListenerCache.sendGeneralEvent( EventType.FortSiegeStart, eventData, fort.getResidenceId() )
        }
    }

    runEndSiegeTask(): Promise<void> {
        if ( !this.isInProgress() ) {
            return
        }

        this.siegeEndTask = null
        return this.endSiege()
    }

    updateSiege(): void {
    }

    getFort() {
        return this.fort
    }

    isInProgress() {
        return this.isInProgressValue
    }

    checkIfInZone( x: number, y: number, z: number ) {
        return this.isInProgress() && this.getFort().checkIfInZone( x, y, z )
    }

    announceDataToPlayers( packet: Buffer ) {
        _.each( this.getAttackerClans(), ( siegeClan: L2SiegeClan ) => {
            let clan: L2Clan = ClanCache.getClan( siegeClan.getClanId() )
            _.each( clan.getOnlineMembers( 0 ), ( playerId: number ) => {
                PacketDispatcher.sendCopyData( playerId, packet )
            } )
        } )

        if ( this.getFort().getOwnerClan() ) {
            let clan = ClanCache.getClan( this.getFort().getOwnerClan().getId() )
            _.each( clan.getOnlineMembers( 0 ), ( playerId: number ) => {
                PacketDispatcher.sendCopyData( playerId, packet )
            } )
        }
    }

    getCommanders() {
        return this.commanders
    }

    isInstance( type: SiegableType ): boolean {
        return type === SiegableType.FortSiege
    }

    async removeAttackerClan( clan: L2Clan ): Promise<void> {
        if ( !clan
                || clan.getFortId() === this.getFort().getResidenceId()
                || !await FortSiegeManager.checkIsRegistered( clan, this.getFort().getResidenceId() ) ) {
            return
        }

        await this.removeSiegeClan( clan.getId() )
    }

    async removeSiegeClan( clanId: number ): Promise<void> {
        if ( clanId !== 0 ) {
            await DatabaseManager.getFortSiegeClans().removeClanRegistration( this.getFort().getResidenceId(), clanId )
        } else {
            await DatabaseManager.getFortSiegeClans().removeAll( this.getFort().getResidenceId() )
        }

        await this.loadSiegeClans()

        if ( _.isEmpty( this.getAttackerClans() ) ) {
            if ( this.isInProgress() ) {
                await this.endSiege()
            } else {
                this.resetSiegeDate()
            }

            this.clearSiegeStartTask()
        }
    }

    clearSiegeStartTask(): void {
        if ( this.siegeStartTask ) {
            clearTimeout( this.siegeStartTask )
            this.siegeStartTask = null
        }
    }

    resetSiegeDate(): void {
        this.getFort().setSiegeDate( 0 )
    }

    async loadSiegeClans() {
        this.attackerClans = []

        let clanIds: Array<number> = await DatabaseManager.getFortSiegeClans().getRegisteredClanIds( this.getFort().getResidenceId() )
        _.each( clanIds, this.addAttackerClanId.bind( this ) )
    }

    addAttackerClanId( clanId: number ): void {
        this.attackerClans.push( new L2SiegeClan( clanId, SiegeClanType.Attacker ) )
    }

    async addAttackerPlayer( player: L2PcInstance, checkConditions: boolean ): Promise<number> {
        if ( player.getClan() == null ) {
            return 0 // Player dont have clan
        }

        if ( checkConditions ) {
            if ( _.isEmpty( this.getFort().getSiege().getAttackerClans() ) && ( player.getInventory().getAdenaAmount() < 250000 ) ) {
                return 1 // Player dont havee enough adena to register
            }

            if ( Date.now() < TerritoryWarManager.getTWStartTime() && TerritoryWarManager.getIsRegistrationOver() ) {
                return 2 // Is not right time to register Fortress now
            }

            if ( Date.now() > TerritoryWarManager.getTWStartTime() && TerritoryWarManager.isTWChannelOpen ) {
                return 2 // Is not right time to register Fortress now
            }

            let isRegistered: boolean = FortManager.getForts().some( ( fort: Fort ): boolean => {
                return !!fort.getSiege().getAttackerClanById( player.getClanId() )
                        || ( ( fort.getOwnerClan() === player.getClan() )
                                && ( fort.getSiege().isInProgress() || !!fort.getSiege().siegeStartTask ) )
            } )

            if ( isRegistered ) {
                return 3 // Players clan is already registred to siege
            }
        }

        await this.saveSiegeClan( player.getClan() )
        if ( this.attackerClans.length === 1 ) {
            if ( checkConditions ) {
                await player.reduceAdena( 250000, true, 'FortSiege.addAttackerPlayer' )
            }

            await this.startAutoTask( true )
        }

        return 4 // Players clan is successfully registred to siege
    }

    getAttackerClanById( clanId: number ) {
        return _.find( this.getAttackerClans(), ( siegeClan: L2SiegeClan ) => {
            return siegeClan && siegeClan.getClanId() === clanId
        } )
    }

    startAutoTask( setTime: boolean ) {
        if ( this.siegeStartTask ) {
            return
        }

        if ( setTime ) {
            this.setSiegeDateTime( false )
        }

        if ( this.getFort().getOwnerClan() ) {
            this.getFort().getOwnerClan().broadcastDataToOnlineMembers( SystemMessageBuilder.fromMessageId( SystemMessageIds.A_FORTRESS_IS_UNDER_ATTACK ) )
        }

        return this.scheduleStartSiegeTask( 3600 )
    }

    scheduleStartSiegeTask( seconds: number ): Promise<void> {
        if ( seconds === 3600 ) {
            this.siegeStartTask = setTimeout( this.scheduleStartSiegeTask.bind( this ), 50 * 60 * 1000, 600 ) // Prepare task for 10 minutes left.
            return
        }

        if ( seconds === 600 ) {
            // TODO : send signal to suspicious spawn merchant to despawn
            let message = new SystemMessageBuilder( SystemMessageIds.S1_MINUTES_UNTIL_THE_FORTRESS_BATTLE_STARTS )
                    .addNumber( 10 )
                    .getBuffer()
            this.announceDataToPlayers( message )
            this.siegeStartTask = setTimeout( this.scheduleStartSiegeTask.bind( this ), 5 * 60 * 1000, 300 ) // Prepare task for 5 minutes left.
            return
        }

        if ( seconds === 300 ) {
            let message = new SystemMessageBuilder( SystemMessageIds.S1_MINUTES_UNTIL_THE_FORTRESS_BATTLE_STARTS )
                    .addNumber( 5 )
                    .getBuffer()
            this.announceDataToPlayers( message )
            this.siegeStartTask = setTimeout( this.scheduleStartSiegeTask.bind( this ), 240000, 60 ) // Prepare task for 1 minute left.
            return
        }

        if ( seconds === 60 ) {
            let message = new SystemMessageBuilder( SystemMessageIds.S1_MINUTES_UNTIL_THE_FORTRESS_BATTLE_STARTS )
                    .addNumber( 1 )
                    .getBuffer()
            this.announceDataToPlayers( message )
            this.siegeStartTask = setTimeout( this.scheduleStartSiegeTask.bind( this ), 30000, 30 ) // Prepare task for 30 seconds left.
            return
        }

        if ( seconds === 30 ) {
            let message = new SystemMessageBuilder( SystemMessageIds.S1_SECONDS_UNTIL_THE_FORTRESS_BATTLE_STARTS )
                    .addNumber( 30 )
                    .getBuffer()
            this.announceDataToPlayers( message )
            this.siegeStartTask = setTimeout( this.scheduleStartSiegeTask.bind( this ), 20000, 10 ) // Prepare task for 10 seconds left.
            return
        }
        if ( seconds === 10 ) {
            let message = new SystemMessageBuilder( SystemMessageIds.S1_SECONDS_UNTIL_THE_FORTRESS_BATTLE_STARTS )
                    .addNumber( 10 )
                    .getBuffer()
            this.announceDataToPlayers( message )
            this.siegeStartTask = setTimeout( this.scheduleStartSiegeTask.bind( this ), 5000, 5 ) // Prepare task for 5 seconds left.
            return
        }

        if ( seconds === 5 ) {
            let message = new SystemMessageBuilder( SystemMessageIds.S1_SECONDS_UNTIL_THE_FORTRESS_BATTLE_STARTS )
                    .addNumber( 5 )
                    .getBuffer()
            this.announceDataToPlayers( message )
            this.siegeStartTask = setTimeout( this.scheduleStartSiegeTask.bind( this ), 4000, 1 ) // Prepare task for 1 seconds left.
        }

        if ( seconds === 1 ) {
            let message = new SystemMessageBuilder( SystemMessageIds.S1_SECONDS_UNTIL_THE_FORTRESS_BATTLE_STARTS )
                    .addNumber( 1 )
                    .getBuffer()
            this.announceDataToPlayers( message )
            this.siegeStartTask = setTimeout( this.scheduleStartSiegeTask.bind( this ), 1000 ) // Prepare task start siege.
            return
        }

        return this.startSiege()
    }

    async saveSiegeClan( clan: L2Clan ): Promise<void> {
        if ( !clan || this.getAttackerClans().length >= ConfigManager.fortSiege.getAttackerMaxClans() ) {
            return
        }

        await DatabaseManager.getFortSiegeClans().addClanRegistration( this.getFort().getResidenceId(), clan.getId() )
    }

    setSiegeDateTime( isForMerchant: boolean ) {
        let currentTime = Date.now()
        let additionalMinutes = isForMerchant ? ConfigManager.fortSiege.getSuspiciousMerchantRespawnDelay() : 60

        this.getFort().setSiegeDate( currentTime + additionalMinutes * 60000 )
    }

    removeAttacker( clan: L2Clan ): Promise<void> {
        if ( !clan
                || clan.getFortId() === this.getFort().getResidenceId()
                || !FortSiegeManager.checkIsRegistered( clan, this.getFort().getResidenceId() ) ) {
            return
        }

        return this.removeSiegeClan( clan.getId() )
    }

    removeFlags() {
        _.each( this.getAttackerClans(), ( clan: L2SiegeClan ) => {
            if ( clan ) {
                clan.removeFlags()
            }
        } )
    }

    unSpawnFlags() {
        _.each( FortSiegeManager.getFlagList( this.getFort().getResidenceId() ), ( flag: CombatFlag ) => {
            flag.unSpawnMe()
        } )
    }

    updatePlayerSiegeStateFlags( shouldClearState: boolean ) {

        let siege = this
        const updateClanMembers = ( clanId ) => {
            let clan = ClanCache.getClan( clanId )

            if ( !clan ) {
                return
            }

            _.each( clan.getOnlineMembers(), ( playerId: number ) => {
                let player = L2World.getPlayer( playerId )

                if ( !player ) {
                    return
                }

                if ( shouldClearState ) {
                    player.setSiegeRole( SiegeRole.None )
                    player.setSiegeSide( 0 )
                    player.setIsInSiege( false )
                    player.stopFameTask()
                } else {
                    player.setSiegeRole( SiegeRole.Attacker )
                    player.setSiegeSide( siege.getFort().getResidenceId() )
                    if ( siege.checkIfInZone( player.getX(), player.getY(), player.getZ() ) ) {
                        player.setIsInSiege( true )
                        player.startFameTask( ConfigManager.character.getFortressFameInterval(), ConfigManager.character.getFortressFameIntervalPoints() )
                    }
                }

                player.broadcastUserInfo()
                PlayerRelationStatus.computeRelationStatus( player )
            } )
        }
        _.each( this.getAttackerClans(), ( siegeClan: L2SiegeClan ) => {
            updateClanMembers( siegeClan.getClanId() )
        } )

        if ( this.getFort().getOwnerClan() ) {
            updateClanMembers( this.getFort().getOwnerClan().getId() )
        }
    }

    async clearSiegeClans(): Promise<void> {
        await DatabaseManager.getFortSiegeClans().removeAll( this.getFort().getResidenceId() )

        let ownerClan = this.getFort().getOwnerClan()
        if ( ownerClan ) {
            await DatabaseManager.getFortSiegeClans().removeClan( ownerClan.getId() )
        }

        this.attackerClans = []

        if ( this.isInProgress() ) {
            await this.endSiege()
        }

        if ( this.siegeStartTask ) {
            clearTimeout( this.siegeStartTask )
            this.siegeStartTask = null
        }
    }

    private getPlayersForTeleportType( targetType: FortTeleportWhoType ): ReadonlyArray<L2PcInstance> {
        if ( targetType === FortTeleportWhoType.Owner ) {
            return this.getOwnersInZone()
        }

        if ( targetType === FortTeleportWhoType.Attacker ) {
            return this.getAttackersInZone().map( ( playerId ) => {
                return L2World.getPlayer( playerId )
            } )
        }

        return this.getFort().getPlayersInsideSiegeArea()
    }

    async teleportPlayers( targetType: FortTeleportWhoType, destinationType: TeleportWhereType ): Promise<void> {
        await Promise.all( this.getPlayersForTeleportType( targetType ).map( ( player: L2PcInstance ) => {
            if ( !player || player.hasActionOverride( PlayerActionOverride.FortressAccess ) || player.isJailed() ) {
                return
            }

            return player.teleportToLocationType( destinationType )
        } ) )
    }

    getOwnersInZone(): Array<L2PcInstance> {
        let ownerClan = this.getFort().getOwnerClan()
        if ( ownerClan ) {
            let clan: L2Clan = ClanCache.getClan( ownerClan.getId() )

            return _.map( clan.getOnlineMembers( 0 ), ( playerId: number ) => {
                let player: L2PcInstance = L2World.getPlayer( playerId )
                if ( player.isInSiege() ) {
                    return player
                }

                return null
            } )
        }

        return null
    }

    killedCommander( commander: L2FortCommanderInstance ) {
        if ( this.commanders.length === 0 || !this.getFort() ) {
            return
        }

        let spawn: ISpawnLogic = commander.getNpcSpawn()

        if ( !spawn ) {
            return
        }

        let commanderSpawns: Array<FortSiegeSpawn> = _.filter( FortSiegeManager.getCommanderSpawnList( this.getFort().getResidenceId() ), ( siegeSpawn: FortSiegeSpawn ) => {
            return siegeSpawn.getId() === spawn.getTemplate().getId()
        } )

        _.each( commanderSpawns, ( siegeSpawn: FortSiegeSpawn ) => {
            let messageId: number

            switch ( siegeSpawn.getMessageId() ) {
                case 1:
                    messageId = NpcStringIds.YOU_MAY_HAVE_BROKEN_OUR_ARROWS_BUT_YOU_WILL_NEVER_BREAK_OUR_WILL_ARCHERS_RETREAT
                    break

                case 2:
                    messageId = NpcStringIds.AIIEEEE_COMMAND_CENTER_THIS_IS_GUARD_UNIT_WE_NEED_BACKUP_RIGHT_AWAY
                    break

                case 3:
                    messageId = NpcStringIds.AT_LAST_THE_MAGIC_FIELD_THAT_PROTECTS_THE_FORTRESS_HAS_WEAKENED_VOLUNTEERS_STAND_BACK
                    break

                case 4:
                    messageId = NpcStringIds.I_FEEL_SO_MUCH_GRIEF_THAT_I_CANT_EVEN_TAKE_CARE_OF_MYSELF_THERE_ISNT_ANY_REASON_FOR_ME_TO_STAY_HERE_ANY_LONGER
                    break
            }

            if ( messageId ) {
                BroadcastHelper.broadcastNpcSayStringId( commander, NpcSayType.NpcShout, messageId )
            }
        } )

        _.pull( this.commanders, spawn )

        if ( this.commanders.length === 0 ) {
            this.stopSiegeRestoreTask()
            this.getFort().getSiege().announceDataToPlayers( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALL_BARRACKS_OCCUPIED ) )
            return
        }

        if ( !this.siegeRestoreTask ) {
            this.getFort().getSiege().announceDataToPlayers( SystemMessageBuilder.fromMessageId( SystemMessageIds.SEIZED_BARRACKS ) )
            this.siegeRestoreTask = setTimeout( this.runScheduleSiegeRestore.bind( this ), GeneralHelper.minutesToMillis( ConfigManager.fortSiege.getCountDownLength() ) )
            return
        }

        this.getFort().getSiege().announceDataToPlayers( SystemMessageBuilder.fromMessageId( SystemMessageIds.SEIZED_BARRACKS ) )
    }

    stopSiegeRestoreTask() {
        if ( this.siegeRestoreTask ) {
            clearTimeout( this.siegeRestoreTask )
            this.siegeRestoreTask = null
        }
    }

    async runScheduleSiegeRestore() : Promise<void> {
        if ( !this.isInProgressValue ) {
            return
        }

        this.siegeRestoreTask = null
        await this.getFort().resetDoors()
        // TODO : trigger barracks npc respawn

        this.announceDataToPlayers( SystemMessageBuilder.fromMessageId( SystemMessageIds.BARRACKS_FUNCTION_RESTORED ) )
    }
}