import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { SkillCache } from '../../cache/SkillCache'
import aigle from 'aigle'
import { Skill } from '../Skill'
import { ResidenceType } from '../../enums/ResidenceType'

export abstract class AbstractResidence {
    residenceId: number
    name: string
    type: ResidenceType = ResidenceType.Unknown

    constructor( id: number ) {
        this.residenceId = id
    }

    getResidenceId() : number {
        return this.residenceId
    }

    getName() : string {
        return this.name
    }

    async giveResidentialSkills( player: L2PcInstance ) : Promise<any> {
        let skills = SkillCache.getAvailableResidentialSkills( this.residenceId )
        if ( skills.length === 0 ) {
            return
        }

        return aigle.resolve( skills ).each( ( skill: Skill ) => {
            return player.addSkill( skill )
        } )
    }

    async removeResidentialSkills( player: L2PcInstance ) : Promise<any> {
        let skills = SkillCache.getAvailableResidentialSkills( this.residenceId )
        if ( skills.length === 0 ) {
            return
        }

        return aigle.resolve( skills ).each( ( skill: Skill ) => {
            return player.removeSkill( skill, false )
        } )
    }

    getType() : ResidenceType {
        return this.type
    }

    abstract isSiegeActive() : boolean
}