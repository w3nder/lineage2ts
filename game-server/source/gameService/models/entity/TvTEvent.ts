import { TvTEventTeam } from './TvTEventTeam'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { TvTEventTeleporter } from './TvTEventTeleporter'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2World } from '../../L2World'
import { Skill } from '../Skill'
import { L2TvTConfigurationLocation, L2TvTConfigurationSkill } from '../../../config/interface/TvTConfigurationApi'
import { L2Character } from '../actor/L2Character'
import { L2Summon } from '../actor/L2Summon'
import { EventType, TvTKillEvent } from '../events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { SkillCache } from '../../cache/SkillCache'
import { PlayerEventStatusCache } from '../../cache/PlayerEventStatusCache'
import aigle from 'aigle'
import _ from 'lodash'
import { ISpawnLogic } from '../spawns/ISpawnLogic'
import { NpcSayType } from '../../enums/packets/NpcSayType'

const htmlPath: string = 'datapack/custom/events/TvT/TvTManager/'

enum EventState {
    INACTIVE,
    INACTIVATING,
    PARTICIPATING,
    STARTING,
    STARTED,
    REWARDING
}

class Event {
    teams: Array<TvTEventTeam> = []
    state: number = EventState.INACTIVE
    npcSpawn: ISpawnLogic
    lastNpcSpawn: number
    TvTEventInstance: number = 0

    checkForTvTSkill( source: L2PcInstance, target: L2PcInstance, skill: Skill ): boolean {
        if ( !this.isStarted() ) {
            return true
        }

        let sourcePlayerId = source.getObjectId()
        let targetPlayerId = target.getObjectId()
        let isSourceParticipant = this.isPlayerParticipant( sourcePlayerId )
        let isTargetParticipant = this.isPlayerParticipant( targetPlayerId )

        // both players not participating
        if ( !isSourceParticipant && !isTargetParticipant ) {
            return true
        }
        // one player not participating
        if ( !( isSourceParticipant && isTargetParticipant ) ) {
            return false
        }
        // players in the different teams ?
        if ( this.getParticipantTeamId( sourcePlayerId ) !== this.getParticipantTeamId( targetPlayerId ) ) {
            return skill.isBad()
        }

        return true
    }

    getParticipantEnemyTeam( objectId: number ): TvTEventTeam {
        if ( this.teams[ 0 ].containsPlayer( objectId ) ) {
            return this.teams[ 1 ]
        }

        if ( this.teams[ 1 ].containsPlayer( objectId ) ) {
            return this.teams[ 0 ]
        }

        return null
    }

    getParticipantTeamId( objectId: number ): number {
        return this.teams[ 0 ].containsPlayer( objectId ) ? 0 : this.teams[ 1 ].containsPlayer( objectId ) ? 1 : -1
    }

    isParticipating() {
        return this.state === EventState.PARTICIPATING
    }

    isPlayerParticipant( playerId: number ) {
        if ( !this.isParticipating() && !this.isStarting() && !this.isStarted() ) {
            return false
        }

        return this.teams[ 0 ].containsPlayer( playerId ) || this.teams[ 1 ].containsPlayer( playerId )
    }

    isStarted(): boolean {
        return this.state === EventState.STARTED
    }

    isStarting() {
        return this.state === EventState.STARTING
    }

    onEscapeUse( objectId: number ): boolean {
        if ( !this.isStarted() ) {
            return true
        }

        return !this.isPlayerParticipant( objectId )
    }

    onItemSummon( objectId: number ): boolean {
        if ( !this.isStarted() ) {
            return true
        }

        return !this.isPlayerParticipant( objectId ) || ConfigManager.tvt.allowTargetTeamMember()
    }

    onLogin( player: L2PcInstance ): void {
        if ( !player || ( !this.isStarting() && !this.isStarted() ) ) {
            return
        }

        let teamId = this.getParticipantTeamId( player.getObjectId() )

        if ( teamId === -1 ) {
            return
        }

        this.teams[ teamId ].addPlayer( player )
        new TvTEventTeleporter( player, this.teams[ teamId ].getLocation(), true, false )
    }

    onLogout( player: L2PcInstance ): void {
        if ( player && ( this.isStarting() || this.isStarted() || this.isParticipating() ) ) {
            if ( this.removeParticipant( player.getObjectId() ) ) {
                let location: L2TvTConfigurationLocation = ConfigManager.tvt.getParticipationNpcLocation()
                player.setXYZInvisible( ( location.x + _.random( 101 ) ) - 50, ( location.y + _.random( 101 ) ) - 50, location.z )
            }
        }
    }

    isScrollUseAllowed( objectId: number ): boolean {
        if ( !this.isStarted() ) {
            return true
        }

        return !this.isPlayerParticipant( objectId ) || ConfigManager.tvt.allowScroll()
    }

    removeParticipant( objectId: number ) {
        // Get the teamId of the player
        let teamId = this.getParticipantTeamId( objectId )

        // Check if the player is participant
        if ( teamId !== -1 ) {
            // Remove the player from team
            this.teams[ teamId ].removePlayer( objectId )

            PlayerEventStatusCache.removePlayerStatus( objectId, this.getEventStatusName() )
            return true
        }

        return false
    }

    onAction( player: L2PcInstance, targetId: number ) : boolean {
        if ( !player || !this.isStarted() || player.isGM() ) {
            return true
        }

        let playerTeamId = this.getParticipantTeamId( player.getObjectId() )
        let targetedPlayerTeamId = this.getParticipantTeamId( targetId )

        if ( ( ( playerTeamId !== -1 ) && ( targetedPlayerTeamId === -1 ) ) || ( ( playerTeamId === -1 ) && ( targetedPlayerTeamId !== -1 ) ) ) {
            return false
        }

        return targetedPlayerTeamId === -1
                || playerTeamId !== targetedPlayerTeamId
                || player.getObjectId() === targetId
                || ConfigManager.tvt.allowTargetTeamMember()
    }

    onKill( attacker: L2Character, target: L2PcInstance ) : void {
        if ( !target || !this.isStarted() ) {
            return
        }

        let killedTeamId = this.getParticipantTeamId( target.getObjectId() )
        if ( killedTeamId === -1 ) {
            return
        }

        new TvTEventTeleporter( target, this.teams[ killedTeamId ].getLocation(), false, false )

        if ( !attacker ) {
            return
        }

        let killerPlayerInstance : L2PcInstance

        if ( attacker.isPet() || attacker.isServitor() ) {
            killerPlayerInstance = ( attacker as L2Summon ).getOwner()

        }

        if ( attacker.isPlayer() ) {
            killerPlayerInstance = attacker.getActingPlayer()
        }

        if ( !killerPlayerInstance ) {
            return
        }

        let killerTeamId = this.getParticipantTeamId( killerPlayerInstance.getObjectId() )

        if ( ( killerTeamId !== -1 ) && ( killerTeamId !== killedTeamId ) ) {
            let killerTeam : TvTEventTeam = this.teams[ killerTeamId ]

            killerTeam.increasePoints()

            _.each( killerTeam.participatingPlayers, ( playerId: number ) => {
                let player = L2World.getPlayer( playerId )
                if ( player ) {
                    player.sendOwnedData( CreatureSay.fromText( playerId, killerPlayerInstance.getObjectId(), NpcSayType.Tell, killerPlayerInstance.getName(), `I have killed ${target.getName()}!` ) )
                }
            } )

            if ( ListenerCache.hasGeneralListener( EventType.TvTPlayerKilled ) ) {
                let eventData : TvTKillEvent = {
                    attackerId: killerPlayerInstance.getObjectId(),
                    targetId: target.getObjectId(),
                    teamId: killedTeamId
                }

                ListenerCache.sendGeneralEvent( EventType.TvTPlayerKilled, eventData )
            }
        }
    }

    isInactive() : boolean {
        return this.state === EventState.INACTIVE
    }

    async onTeleported( player: L2PcInstance ) : Promise<any> {
        if ( !this.isStarted() || !player || !this.isPlayerParticipant( player.getObjectId() ) ) {
            return
        }

        return aigle.resolve( player.isMageClass() ? ConfigManager.tvt.getMageBuffs() : ConfigManager.tvt.getFighterBuffs() ).each( ( data: L2TvTConfigurationSkill ) => {
            let skill : Skill = SkillCache.getSkill( data.id, data.level )
            if ( skill ) {
                return skill.applyEffects( player, player )
            }
        } )
    }

    getEventStatusName() : string {
        return 'TvTEventStatus'
    }
}

export const TvTEvent = new Event()