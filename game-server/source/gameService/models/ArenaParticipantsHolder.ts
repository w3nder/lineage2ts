import { BlockCheckerEngine } from './entity/BlockCheckerEngine'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { PacketDispatcher } from '../PacketDispatcher'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { BlockCheckerManager } from '../instancemanager/BlockCheckerManager'
import { L2World } from '../L2World'
import _ from 'lodash'

export class ArenaParticipantsHolder {
    arena: number
    redPlayers: Array<number> = []
    bluePlayers: Array<number> = []
    engine: BlockCheckerEngine

    getPlayerTeam( player: L2PcInstance ) : number {
        if ( this.redPlayers.includes( player.objectId ) ) {
            return 0
        }

        if ( this.bluePlayers.includes( player.objectId ) ) {
            return 1
        }

        return -1
    }

    getEvent() {
        return this.engine
    }

    broadCastPacketToTeam( data: Buffer ) {
        this.redPlayers.forEach( ( playerId : number ) => {
            PacketDispatcher.sendCopyData( playerId, data )
        } )

        this.bluePlayers.forEach( ( playerId : number ) => {
            PacketDispatcher.sendCopyData( playerId, data )
        } )
    }

    getRedPlayers() {
        return this.redPlayers
    }

    getBluePlayers() {
        return this.bluePlayers
    }

    removePlayer( player: L2PcInstance, team: number ) {
        if ( team === 0 ) {
            _.pull( this.redPlayers, player.getObjectId() )
            return
        }

        _.pull( this.bluePlayers, player.getObjectId() )
    }

    getRedTeamSize() {
        return this.redPlayers.length
    }

    getBlueTeamSize() {
        return this.bluePlayers.length
    }

    addPlayer( player: L2PcInstance, team: number ) {
        if ( team === 0 ) {
            this.redPlayers.push( player.getObjectId() )
            return
        }

        this.bluePlayers.push( player.getObjectId() )
    }

    checkAndShuffle() : void {
        let redSize = this.redPlayers.length
        let blueSize = this.bluePlayers.length

        if ( redSize > ( blueSize + 1 ) ) {
            this.broadCastPacketToTeam( SystemMessageBuilder.fromMessageId( SystemMessageIds.TEAM_ADJUSTED_BECAUSE_WRONG_POPULATION_RATIO ) )

            let needed = redSize - ( blueSize + 1 )
            let holder = this
            _.times( needed + 1, ( index: number ) => {
                let player : L2PcInstance = L2World.getPlayer( holder.redPlayers[ index ] )
                if ( !player ) {
                    return
                }

                BlockCheckerManager.changePlayerToTeam( player, holder.arena )
            } )
            return
        }

        if ( blueSize > ( redSize + 1 ) ) {
            this.broadCastPacketToTeam( SystemMessageBuilder.fromMessageId( SystemMessageIds.TEAM_ADJUSTED_BECAUSE_WRONG_POPULATION_RATIO ) )

            let needed = blueSize - ( redSize + 1 )
            let holder = this
            _.times( needed + 1, ( index: number ) => {
                let player : L2PcInstance = L2World.getPlayer( holder.bluePlayers[ index ] )
                if ( !player ) {
                    return
                }

                BlockCheckerManager.changePlayerToTeam( player, holder.arena )
            } )
            return
        }
    }

    getAllPlayerCount() {
        return this.bluePlayers.length + this.redPlayers.length
    }
}