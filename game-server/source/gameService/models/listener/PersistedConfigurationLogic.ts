import { ListenerLogic } from '../ListenerLogic'
import { ServerVariablesManager } from '../../variables/ServerVariablesManager'
import _ from 'lodash'
import logSymbols from 'log-symbols'

export type PersistedConfigurationLogicType = { [ key: string ]: any }

export abstract class PersistedConfigurationLogic<Type extends PersistedConfigurationLogicType> extends ListenerLogic {
    configuration: Type

    getConfigName(): string {
        return `Listener:${ this.name }`
    }

    abstract getDefaultConfiguration(): Type

    getExistingConfiguration(): Type {
        if ( !this.getConfigName() ) {
            console.log( logSymbols.warning, 'No config name specified for', this.name, ', using default configuration.' )
            return this.getDefaultConfiguration()
        }

        let config: unknown = ServerVariablesManager.getValue( this.getConfigName() )

        if ( !config ) {
            ServerVariablesManager.setValue( this.getConfigName(), this.getDefaultConfiguration() )
            return this.getDefaultConfiguration()
        }

        return config as Type
    }

    async load(): Promise<void> {
        this.configuration = _.defaults( this.getExistingConfiguration(), this.getDefaultConfiguration() )
    }

    saveConfiguration(): void {
        if ( !this.configuration || !this.getConfigName() ) {
            return
        }

        ServerVariablesManager.setValue( this.getConfigName(), this.configuration )
    }
}