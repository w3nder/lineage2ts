import { QuestHelper } from '../../../listeners/helpers/QuestHelper'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { L2World } from '../../L2World'
import { L2Npc } from '../actor/L2Npc'
import { AnnouncementManager } from '../announce/AnnouncementManager'
import { AnnouncementType } from '../announce/AnnouncementType'
import { EventAnnouncement } from '../announce/EventAnnouncement'
import { AdditionalDropManager } from '../../cache/AdditionalDropManager'
import { PersistedConfigurationLogic, PersistedConfigurationLogicType } from './PersistedConfigurationLogic'
import Timeout = NodeJS.Timeout
import { IDFactoryCache } from '../../cache/IDFactoryCache'
import aigle from 'aigle'
import _ from 'lodash'

export interface PersistedEventNpcConfiguration {
    npc: number
    x: number
    y: number
    z: number
    heading: number
}

export interface PersistedEventItemConfiguration {
    itemId: number
    minCount: number
    maxCount: number
    chance: number
}

export interface PersistedEventConfiguration extends PersistedConfigurationLogicType {
    startDate: number
    endDate: number
    npcs: Array<PersistedEventNpcConfiguration>
    onEndMessage: string
    onStartMessage: string
    eventName: string
    drops?: Array<PersistedEventItemConfiguration>
    isEnabled: boolean
}

export abstract class PersistedEventLogic<Type extends PersistedEventConfiguration> extends PersistedConfigurationLogic<Type> {
    spawnedObjectIds: Array<number>
    eventDropIds: Array<number>
    eventTask: Timeout
    announcementId: number

    getConfigName(): string {
        return `Event:${ this.name }`
    }

    async initialize(): Promise<void> {
        return this.startEvent()
    }

    async runEndEvent(): Promise<void> {
        await aigle.resolve( this.spawnedObjectIds ).eachLimit( 10, async ( objectId: number ) => {
            let npc = L2World.getObjectById( objectId ) as L2Npc
            if ( !npc || !npc.isNpc() ) {
                return
            }

            await npc.deleteMe()
            IDFactoryCache.releaseId( objectId )
        } )

        AdditionalDropManager.removeGenericDrops( this.eventDropIds )

        BroadcastHelper.broadcastTextToAllPlayers( this.configuration.eventName, this.configuration.onEndMessage )

        if ( _.isNumber( this.announcementId ) ) {
            await AnnouncementManager.deleteAnnouncement( this.announcementId )
        }
    }

    async runStartEvent(): Promise<void> {
        let hasEndDate = this.configuration.endDate > 0
        let duration = hasEndDate ? this.configuration.endDate - Date.now() : 0

        this.spawnedObjectIds = _.map( this.configuration.npcs, ( data: PersistedEventNpcConfiguration ): number => {
            let npc: L2Npc = QuestHelper.addGenericSpawn( null, data.npc, data.x, data.y, data.z, data.heading, true, duration, false, 0 )
            return npc.getObjectId()
        } )

        this.eventDropIds = _.map( this.configuration.drops, ( drop: PersistedEventItemConfiguration ): number => {
            return AdditionalDropManager.addGenericDrop( drop.itemId, drop.minCount, drop.maxCount, drop.chance )
        } )

        if ( duration > 0 ) {
            this.eventTask = setTimeout( this.runEndEvent.bind( this ), duration )
        }

        BroadcastHelper.broadcastTextToAllPlayers( this.configuration.eventName, this.configuration.onStartMessage )

        this.announcementId = await AnnouncementManager.addAnnouncement( new EventAnnouncement( AnnouncementType.EVENT, this.name, this.configuration.onStartMessage ) )
    }

    async startEvent(): Promise<void> {
        if ( !this.configuration.isEnabled ) {
            return
        }

        let currentDate = Date.now()
        let timeDifference = this.configuration.startDate - currentDate
        if ( timeDifference >= 0 ) {
            this.eventTask = setTimeout( this.runStartEvent.bind( this ), timeDifference )
            return
        }

        await this.runStartEvent()
    }
}