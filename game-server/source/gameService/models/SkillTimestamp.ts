export interface L2ReuseTime {
    getRemainingMs() : number
    getId() : number
    getLevel() : number
    isActive() : boolean
    getExpirationTime() : number
    getReuseMs() : number
    getReuseSeconds() : number
}

export class L2Timestamp implements L2ReuseTime {
    id: number
    reuseMs: number
    reuseSeconds: number
    expirationTime: number
    level: number

    getRemainingMs() : number {
        return Math.max( this.expirationTime - Date.now(), 0 )
    }

    getId() : number {
        return this.id
    }

    getLevel() : number {
        return this.level
    }

    getReuseMs() : number {
        return this.reuseMs
    }

    isActive() {
        return Date.now() < this.expirationTime
    }

    getExpirationTime() {
        return this.expirationTime
    }

    getItemId() {
        return this.id
    }

    getReuseSeconds(): number {
        return this.reuseSeconds
    }
}