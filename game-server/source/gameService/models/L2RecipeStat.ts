import { StatType } from '../enums/StatType'

export interface L2RecipeStat {
    type: StatType
    value: number
}