import { L2Character } from '../actor/L2Character'
import { InstanceManager } from '../../instancemanager/InstanceManager'
import { Instance } from '../entity/Instance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

type InstanceWorldValue = string | number | boolean

export class InstanceWorld {
    instanceId: number
    templateId: number = -1
    allowedObjectIds: Set<number> = new Set<number>()
    status: number
    instanceName: string
    variables: { [ name: string ]: InstanceWorldValue } = {}

    isAllowed( playerId: number ) {
        return this.allowedObjectIds.has( playerId )
    }

    removeAllowedId( playerId: number ) {
        this.allowedObjectIds.delete( playerId )
    }

    getInstanceId() {
        return this.instanceId
    }

    getTemplateId() {
        return this.templateId
    }

    onDeath( victim: L2Character ) {
        if ( !victim || !victim.isPlayer() ) {
            return
        }

        let instance: Instance = this.getInstance()
        if ( instance ) {
            let message = new SystemMessageBuilder( SystemMessageIds.YOU_WILL_BE_EXPELLED_IN_S1 )
                    .addNumber( Math.floor( instance.getEjectTime() / 60 / 1000 ) )
                    .getBuffer()
            victim.sendOwnedData( message )
            instance.addEjectDeadTask( victim.getActingPlayer() )
        }
    }

    isStatus( value: number ) {
        return this.status === value
    }

    getInstance(): Instance {
        return InstanceManager.getInstance( this.instanceId )
    }

    getInstanceName(): string {
        return this.instanceName
    }

    getStatus(): number {
        return this.status
    }

    getVariable( name: string ): InstanceWorldValue {
        return this.variables[ name ]
    }

    setVariable( name: string, value: InstanceWorldValue ): void {
        this.variables[ name ] = value
    }

    removeVariable( name: string ): void {
        delete this.variables[ name ]
    }

    addAllowedId( playerId: number ) {
        this.allowedObjectIds.add( playerId )
    }
}