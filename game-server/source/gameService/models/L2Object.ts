import { InstanceType, InstanceTypeHelper } from '../enums/InstanceType'
import { L2WorldRegion } from './L2WorldRegion'
import { L2World } from '../L2World'
import { L2Character } from './actor/L2Character'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { ILocational } from './Location'
import { ShotType } from '../enums/ShotType'
import { PlayerActionOverride } from '../values/PlayerConditions'
import { DebouncedPacketMethod, PacketDispatcher } from '../PacketDispatcher'
import { ActionFailed } from '../packets/send/ActionFailed'
import { IActionHandler } from '../handler/IActionHandler'
import { ActionManager } from '../handler/managers/ActionManager'
import { IActionShiftHandler } from '../handler/IActionShiftHandler'
import { ActionShiftManager } from '../handler/managers/ActionShiftManager'
import { ISpatialIndexData } from './SpatialIndexData'
import { DataFlag, SharedData } from '../threads/models/SharedData'
import { InstanceManager } from '../instancemanager/InstanceManager'
import { Instance } from './entity/Instance'
import { L2Npc } from './actor/L2Npc'
import { PolymorphData } from '../variables/VariableTypes'
import { AIEffectHelper } from '../aicontroller/helpers/AIEffectHelper'
import { IIdentifiable } from './IIdentifiable'
import { L2Region } from '../enums/L2Region'
import { GeoPolygonCache } from '../cache/GeoPolygonCache'
import { L2WorldLimits } from '../enums/L2MapTile'

export class L2Object implements ILocational, IIdentifiable {
    name: string
    objectId: number
    worldRegion: L2WorldRegion
    instanceType: InstanceType
    heading: number = 0
    instanceId: number = 0
    isInvisibleValue: boolean = false

    spatialIndex: ISpatialIndexData
    sharedData: SharedData
    polymorphData : PolymorphData

    constructor( objectId: number ) {
        this.instanceType = InstanceType.L2Object
        this.objectId = objectId

        this.sharedData = this.createSharedData()

        if ( objectId > 0 ) {
            L2World.storeObject( this )
        }

        this.spatialIndex = {
            objectId,
            maxX: 0,
            maxY: 0,
            minX: 0,
            minY: 0
        }

        this.polymorphData = {
            polyId: 0,
            polyType: ''
        }
    }

    isMoveCapable() {
        return false
    }

    createSharedData() {
        let data = SharedData.create()

        data.setObjectId( this.getObjectId() )

        data.setX( 0 )
        data.setY( 0 )
        data.setZ( 0 )
        data.setDataFlags( 0 )
        data.setMoveSpeed( 0 )
        data.setHeading( 0 )

        return data
    }

    getBroadcastRadius() {
        return L2Region.Length
    }

    broadcastInfo(): void {
        L2World.getVisiblePlayers( this, this.getBroadcastRadius() ).forEach( ( player: L2PcInstance ) => {
            return this.shouldDescribeState( player )
        } )
    }

    calculateDistance( location: ILocational, includeZAxis: boolean = false ) : number {
        return this.calculateDistanceWithCoordinates( location.getX(), location.getY(), location.getZ(), includeZAxis )
    }

    calculateDistanceWithCoordinates( x: number, y: number, z: number, includeZAxis: boolean ) {
        if ( includeZAxis ) {
            return Math.floor( Math.hypot( x - this.getX(), y - this.getY(), z - this.getZ() ) )
        }

        return Math.floor( Math.hypot( x - this.getX(), y - this.getY() ) )
    }

    canBeAttacked(): boolean {
        return false
    }

    decayMe(): void {
        this.setIsVisible( false )
        L2World.removeSpatialIndex( this )
    }

    getActingPlayer(): L2PcInstance {
        return null
    }

    getActingPlayerId() : number {
        return 0
    }

    getHeading(): number {
        return this.sharedData.getHeading()
    }

    getInstanceId(): number {
        return this.instanceId
    }

    getX(): number {
        return this.sharedData.getX()
    }

    getY(): number {
        return this.sharedData.getY()
    }

    getZ(): number {
        return this.sharedData.getZ()
    }

    setX( value: number ) : void {
        this.sharedData.setX( value )
    }

    setY( value: number ) : void {
        this.sharedData.setY( value )
    }

    setZ( value: number ) : void {
        this.sharedData.setZ( value )
    }

    getId() {
        return 0
    }

    getName(): string {
        return this.name
    }

    getObjectId() {
        return this.objectId
    }

    isPolymorphed() : boolean {
        return this.polymorphData.polyType.length > 0
    }

    getPolymorphTemplateId() : number {
        return this.polymorphData.polyId
    }

    getPolymorphName() : string {
        return this.polymorphData.polyType
    }

    resetPolymorphData() : void {
        this.polymorphData.polyId = 1
        this.polymorphData.polyType = ''
    }

    getWorldRegion(): L2WorldRegion {
        return this.worldRegion
    }

    isAttackable(): boolean {
        return false
    }

    isAutoAttackable( character: L2Character ): boolean {
        return false
    }

    isCharacter(): boolean {
        return false
    }

    isChargedShot( type: ShotType ): boolean {
        return false
    }

    isDoor(): boolean {
        return false
    }

    isInstanceType( type: InstanceType ): boolean {
        return this.instanceType === type || InstanceTypeHelper.includesType( this.instanceType, type )
    }

    isInstanceTypes( types: Array<InstanceType> ): boolean {
        return types.includes( this.instanceType ) || InstanceTypeHelper.includesTypes( this.instanceType, types )
    }

    isInvisible() {
        return this.isInvisibleValue
    }

    isItem(): boolean {
        return false
    }

    isMonster(): boolean {
        return false
    }

    isNpc(): boolean {
        return false
    }

    isPet(): boolean {
        return false
    }

    isPlayable(): boolean {
        return false
    }

    isPlayer(): boolean {
        return false
    }

    isServitor(): boolean {
        return false
    }

    isSummon(): boolean {
        return false
    }

    isTrap(): boolean {
        return false
    }

    isVehicle(): boolean {
        return false
    }

    isVisible() {
        return this.sharedData.hasDataFlag( DataFlag.Visible )
    }

    isAirShip() : boolean {
        return false
    }

    isBoat() {
        return false
    }

    isVisibleFor( player: L2PcInstance ) : boolean {
        if ( player.cannotSeeInstance( this.instanceType ) ) {
            return false
        }

        return !this.isInvisible() || player.hasActionOverride( PlayerActionOverride.SeeInvisible )
    }

    onSpawn() {

    }

    sendCopyData( packet: Buffer ) {}

    shouldDescribeState( player: L2PcInstance ) : boolean {
        return false
    }

    sendDebouncedPacket( method: DebouncedPacketMethod ) {
        PacketDispatcher.sendDebouncedPacket( this.getActingPlayerId(), method )
    }

    setChargedShot( type: ShotType, isCharged: boolean ): void {

    }

    setHeading( heading: number ) {
        let correctedHeading = heading < 0 ? ( heading + 65536 ) : ( heading % 65536 )
        this.sharedData.setHeading( correctedHeading )
    }

    setInstanceId( id: number ): void {
        if ( id < 0 || this.getInstanceId() === id ) {
            return
        }

        let oldInstance : Instance = InstanceManager.getInstance( this.getInstanceId() )
        let newInstance : Instance = InstanceManager.getInstance( id )

        if ( !newInstance ) {
            return
        }

        if ( this.isPlayer() ) {
            let player : L2PcInstance = this.getActingPlayer()
            if ( this.getInstanceId() > 0 && oldInstance ) {
                oldInstance.removePlayer( this.getObjectId() )

                if ( oldInstance.isShowTimer() ) {
                    player.sendInstanceUpdate( oldInstance, true )
                }
            }

            if ( id > 0 ) {
                newInstance.addPlayer( this.getObjectId() )
                if ( newInstance.isShowTimer() ) {
                    player.sendInstanceUpdate( newInstance, false )
                }
            }

            if ( player.hasSummon() ) {
                player.getSummon().setInstanceId( id )
            }

            return
        }

        if ( this.isNpc() ) {
            let npc : L2Npc = this as unknown as L2Npc
            if ( this.getInstanceId() > 0 && oldInstance ) {
                oldInstance.removeNpc( npc )
            }

            if ( id > 0 ) {
                newInstance.addNpc( npc.getObjectId() )
            }
        }

        this.instanceId = id
        if ( this.sharedData && this.isVisible() && !this.isPlayer() ) {
            this.decayMe()
            this.spawnMeNow()
        }
    }

    setIsVisible( value: boolean ) {
        value ? this.sharedData.addDataFlag( DataFlag.Visible ) : this.sharedData.removeDataFlag( DataFlag.Visible )

        if ( !value ) {
            this.setWorldRegion( null )
        }
    }

    setLocation( location: ILocational ) {
        this.setX( location.getX() )
        this.setY( location.getY() )
        this.setZ( location.getZ() )

        this.heading = location.getHeading()
        this.instanceId = location.getInstanceId()
    }

    setLocationInvisible( location: ILocational ) {
        this.setXYZInvisible( location.getX(), location.getY(), location.getZ() )
    }

    setName( name: string ): void {
        this.name = name
    }

    setWorldRegion( region: L2WorldRegion ): void {
        this.worldRegion = region
    }

    setXYZ( x: number, y: number, z: number ): void {
        this.setX( Math.floor( x ) )
        this.setY( Math.floor( y ) )
        this.setZ( GeoPolygonCache.getZ( x, y, z ) )

        this.updateSpatialIndex()

        if ( !this.getWorldRegion() || !this.getWorldRegion().isInRegion( x, y ) ) {
            this.updateWorldRegion()
        }
    }

    updateSpatialIndex() : void {
        L2World.removeSpatialIndex( this )

        let currentX = this.getX()
        this.spatialIndex.maxX = currentX
        this.spatialIndex.minX = currentX

        let currentY = this.getY()
        this.spatialIndex.maxY = currentY
        this.spatialIndex.minY = currentY

        L2World.addSpatialIndex( this )
    }

    setXYZInvisible( startX: number, startY: number, startZ: number ) {
        let x = startX
        let y = startY
        let z = startZ

        if ( x > L2WorldLimits.MaxX ) {
            x = L2WorldLimits.MaxX - 5000
        }
        if ( x < L2WorldLimits.MinX ) {
            x = L2WorldLimits.MinX + 5000
        }
        if ( y > L2WorldLimits.MaxY ) {
            y = L2WorldLimits.MaxY - 5000
        }
        if ( y < L2WorldLimits.MinY ) {
            y = L2WorldLimits.MinY + 5000
        }

        this.setXYZ( x, y, z )
        this.setIsVisible( false )
    }

    setXYZLocation( position: ILocational ): void {
        this.setXYZ( position.getX(), position.getY(), position.getZ() )
    }

    spawnMe( x: number, y: number, z: number ) {
        if ( this.getWorldRegion() ) {
            return
        }

        this.sharedData.addDataFlag( DataFlag.Visible )
        if ( x > L2WorldLimits.MaxX ) {
            x = L2WorldLimits.MaxX - 5000
        }

        if ( x < L2WorldLimits.MinX ) {
            x = L2WorldLimits.MinX + 5000
        }

        if ( y > L2WorldLimits.MaxY ) {
            y = L2WorldLimits.MaxY - 5000
        }

        if ( y < L2WorldLimits.MinY ) {
            y = L2WorldLimits.MinY + 5000
        }

        // coordinate update forces spatial index update
        this.setXYZ( x, y, z )
        this.onSpawn()
    }

    spawnMeNow(): boolean {
        this.setIsVisible( true )
        this.updateWorldRegion()

        L2World.addSpatialIndex( this )

        this.onSpawn()

        return true
    }

    updateWorldRegion() : void {
        if ( !this.isVisible() ) {
            return
        }

        let foundRegion = L2World.getRegion( this.getX(), this.getY() )
        let currentRegion = this.getWorldRegion()
        if ( foundRegion === currentRegion ) {
            return
        }

        /*
            Order of changing regions is critical due to region shutdown procedure
            looking for any players in neighbouring regions. If order is reversed,
            all npc AIs can temporarily be shut down and restarted again (aka reset of sorts,
            though causing un-necessary AI thrashing).
         */
        this.setWorldRegion( foundRegion )
        foundRegion.addVisibleObject( this )

        if ( currentRegion && this.isPlayer() ) {
            currentRegion.removeVisibleObject( this )
        }
    }

    isTargetable() : boolean {
        return true
    }

    async onInteraction( player: L2PcInstance, shouldInteract: boolean ) : Promise<void> {
        let handler : IActionHandler = ActionManager.getHandler( this.getInstanceType() )
        if ( handler && handler.canInteract( player, this ) ) {
            if ( !handler.isReadyToInteract( player, this ) ) {
                player.sendOwnedData( ActionFailed() )
                AIEffectHelper.notifyInteractWithTarget( player, this.getObjectId(), shouldInteract )

                return
            }

            await handler.performActions( player, this, shouldInteract )
        }

        player.sendOwnedData( ActionFailed() )
    }

    getInstanceType() : InstanceType {
        return this.instanceType
    }

    async onActionShift( player: L2PcInstance, interact: boolean = true ) {
        let handler : IActionShiftHandler = ActionShiftManager.getHandler( this.getInstanceType() )
        if ( handler ) {
            await handler.onAction( player, this, interact )
        }

        player.sendOwnedData( ActionFailed() )
    }

    onForcedAttack( player: L2PcInstance ) : void {
        player.sendOwnedData( ActionFailed() )
    }

    removeStatusListener( objectId: number ) {

    }

    addStatusListener( objectId: number ) {

    }

    isDefender() : boolean {
        return false
    }

    isGuard() : boolean {
        return false
    }

    getSpatialIndex() : ISpatialIndexData {
        return this.spatialIndex
    }

    rechargeShots( isPhysical: boolean, isMagical: boolean ) : Promise<void> {
        return Promise.resolve()
    }

    isChampion() {
        return false
    }

    setDestinationCoordinates( x: number, y: number, z: number ) : void {
        this.sharedData.setDestinationX( x )
        this.sharedData.setDestinationY( y )
        this.sharedData.setDestinationZ( z )
    }

    prepareSkills() : Promise<void> {
        return Promise.resolve()
    }

    hasLoadedSkills() : boolean {
        return true
    }

    getSharedData() : SharedData {
        return this.sharedData
    }

    isMerchant() : boolean {
        return false
    }

    isStaticObject() : boolean {
        return false
    }

    isChest() : boolean {
        return false
    }

    onObjectIdChanged() : void {
        L2World.storeObject( this )

        this.spatialIndex.objectId = this.getObjectId()
        this.sharedData.setObjectId( this.getObjectId() )
    }
}