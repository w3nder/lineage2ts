import { L2ItemInstance } from './items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { L2World } from '../L2World'
import { CursedWeaponManager } from '../instancemanager/CursedWeaponManager'
import { PacketDispatcher } from '../PacketDispatcher'
import { Skill } from './Skill'
import { L2PcInstance } from './actor/instance/L2PcInstance'
import { SkillCache } from '../cache/SkillCache'
import { DatabaseManager } from '../../database/manager'
import { L2Attackable } from './actor/L2Attackable'
import { L2Character } from './actor/L2Character'
import { ExRedSky } from '../packets/send/ExRedSky'
import { Earthquake } from '../packets/send/Earthquake'
import { L2PartyMessageType } from '../enums/L2PartyMessageType'
import { SocialAction, SocialActionType } from '../packets/send/SocialAction'
import { CommonSkill } from './holders/SkillHolder'
import { BroadcastHelper } from '../helpers/BroadcastHelper'
import { TransformHelper } from '../helpers/TransformHelper'
import { ILocational } from './Location'
import _ from 'lodash'
import Timeout = NodeJS.Timeout
import { L2ItemSlots } from '../enums/L2ItemSlots'

export class CursedWeapon {
    name: string
    itemId: number
    skillId: number
    skillMaxLevel: number

    dropRate: number
    duration: number
    durationLost: number
    disappearChance: number
    stageKills: number

    // this should be false unless if the cursed weapon is dropped, in that case it would be true.
    isDropped: boolean = false
    // this sets the cursed weapon status to true only if a player has the cursed weapon, otherwise this should be false.
    isActivatedValue: boolean = false
    removeTask: Timeout

    nbKills: number = 0
    endTime: number = 0

    playerId: number = 0
    item: L2ItemInstance = null
    playerKarma: number = 0
    playerPkKills: number = 0
    transformationId: number = 0

    async activate( player: L2PcInstance, item: L2ItemInstance ): Promise<void> {
        if ( player.isMounted() ) {
            await player.dismount()
            PacketDispatcher.sendOwnedData( player.objectId, SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_TO_PICKUP_S1 ) )
            await player.dropItem( item, true, false )

            return
        }

        this.isActivatedValue = true

        this.playerId = player.objectId
        this.playerKarma = player.getKarma()
        this.playerPkKills = player.getPkKills()
        await DatabaseManager.getCursedWeapons().deleteWeapon( this )
        await DatabaseManager.getCursedWeapons().saveWeapon( this )

        player.setCursedWeaponEquippedId( this.itemId )
        player.setKarma( 9999999 )
        player.setPkKills( 0 )
        if ( player.isInParty() ) {
            player.getParty().removePartyMember( player, L2PartyMessageType.Expelled )
        }

        await this.doTransform()
        await this.giveSkill()
        this.item = item

        await player.getInventory().equipItem( this.item )

        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_EQUIPPED )
                .addItemInstanceName( this.item )
                .getBuffer()
        PacketDispatcher.sendOwnedData( this.playerId, packet )

        player.setMaxStats()
        player.setCurrentCp( player.getMaxCp() )

        player.broadcastUserInfo()
        BroadcastHelper.dataToSelfBasedOnVisibility( player, SocialAction( this.playerId, SocialActionType.CursedWeaponAcquired ) )

        let announcementPacket: Buffer = new SystemMessageBuilder( SystemMessageIds.THE_OWNER_OF_S2_HAS_APPEARED_IN_THE_S1_REGION )
                .regionFromCoordinates( player.getX(), player.getY(), player.getZ() )
                .addItemInstanceName( this.item )
                .getBuffer()
        CursedWeaponManager.announce( announcementPacket )
    }

    cancelTask() {
        if ( this.removeTask ) {
            clearInterval( this.removeTask )
            this.removeTask = null
        }
    }

    performDrop( attackable: L2Attackable, player: L2PcInstance ): Promise<void> {
        this.endTime = Date.now() + this.duration * 60000
        this.removeTask = setInterval( this.runRemoveTask.bind( this ), this.durationLost * 12000 )

        return this.dropIt( attackable, player )
    }

    async cursedOnLogin() : Promise<void> {
        await this.doTransform()
        await this.giveSkill()

        let player = L2World.getPlayer( this.playerId )
        let weaponEquippedId = player.getCursedWeaponEquippedId()
        let announcementPacket = new SystemMessageBuilder( SystemMessageIds.S2_OWNER_HAS_LOGGED_INTO_THE_S1_REGION )
                .regionFromCoordinates( player.getX(), player.getY(), player.getZ() )
                .addItemNameWithId( weaponEquippedId )
                .getBuffer()
        CursedWeaponManager.announce( announcementPacket )

        let weapon: CursedWeapon = CursedWeaponManager.getCursedWeapon( weaponEquippedId )
        let timeRemainingPacket = new SystemMessageBuilder( SystemMessageIds.S2_MINUTE_OF_USAGE_TIME_ARE_LEFT_FOR_S1 )
                .addItemNameWithId( weaponEquippedId )
                .addNumber( Math.floor( weapon.getTimeLeft() / 60000 ) )
                .getBuffer()

        PacketDispatcher.sendOwnedData( this.playerId, timeRemainingPacket )
    }

    async doTransform() : Promise<void> {
        if ( this.itemId === 8689 ) {
            this.transformationId = 302
        } else if ( this.itemId === 8190 ) {
            this.transformationId = 301
        }

        let player = L2World.getPlayer( this.playerId )
        if ( player.getTransformation() ) {
            await player.stopTransformation( true )
            setTimeout( () => TransformHelper.transformPlayer( this.transformationId, player ), 500 )

            return
        }

        return TransformHelper.transformPlayer( this.transformationId, player )
    }

    async dropIt( attackable: L2Attackable, player: L2PcInstance, killer: L2Character = null, fromMonster: boolean = true ): Promise<void> {
        this.isActivatedValue = false
        let currentPlayer: L2PcInstance = L2World.getPlayer( this.playerId )

        if ( fromMonster ) {
            this.item = attackable.dropSingleItem( this.itemId, 1, player.getObjectId() )
            if ( !this.item ) {
                return
            }

            this.item.setDropTime( 0 )

            L2World.broadcastDataToOnlinePlayers( ExRedSky( 10 ) )
            L2World.broadcastDataToOnlinePlayers( Earthquake( this.item.getX(), this.item.getY(), this.item.getZ(), 14, 3 ) )

        } else if ( currentPlayer ) {
            this.item = currentPlayer.getInventory().getItemByItemId( this.itemId )
            if ( !this.item ) {
                return
            }

            await currentPlayer.dropItem( this.item, true, false )
            currentPlayer.setKarma( this.playerKarma )
            currentPlayer.setPkKills( this.playerPkKills )
            currentPlayer.setCursedWeaponEquippedId( 0 )
            await this.removeSkill( currentPlayer )
            currentPlayer.abortAttack()
        }

        this.isDropped = true
        let packet = new SystemMessageBuilder( SystemMessageIds.S2_WAS_DROPPED_IN_THE_S1_REGION )
        if ( player ) {
            packet.regionFromCoordinates( player.getX(), player.getY(), player.getZ() )
        } else if ( currentPlayer ) {
            packet.regionFromCoordinates( currentPlayer.getX(), currentPlayer.getY(), currentPlayer.getZ() )
        } else {
            packet.regionFromCoordinates( killer.getX(), killer.getY(), killer.getZ() )
        }

        packet.addItemNameWithId( this.itemId )
        CursedWeaponManager.announce( packet.getBuffer() )
    }

    async endOfLife() {
        let player: L2PcInstance = L2World.getPlayer( this.playerId )
        if ( this.isActivatedValue ) {
            if ( player && player.isOnline() ) {
                player.abortAttack()
                player.setKarma( this.playerKarma )
                player.setPkKills( this.playerPkKills )

                player.setCursedWeaponEquippedId( 0 )
                await this.removeSkill( player )

                await player.getInventory().unEquipItemInBodySlot( L2ItemSlots.AllHandSlots )
                await player.storeMe()
                await player.getInventory().destroyItemByItemId( this.itemId, 1, 0, 'CursedWeapon.endOfLife' )

                player.broadcastUserInfo()
            } else {
                await DatabaseManager.getItems().deleteItemFromOwner( this.playerId, this.itemId )
                await DatabaseManager.getCharacterTable().updateKarmaStatus( this.playerKarma, this.playerPkKills, this.playerId )
            }
        } else {
            if ( player && player.getInventory().getItemByItemId( this.itemId ) ) {

                await player.getInventory().destroyItemByItemId( this.itemId, 1, 0, 'CursedWeapon.endOfLife' )
                player.broadcastUserInfo()
            } else if ( this.item ) {
                this.item.decayMe()
                this.item.markDestroyed()
            }
        }

        await DatabaseManager.getCursedWeapons().deleteWeapon( this )

        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_HAS_DISAPPEARED )
                .addItemNameWithId( this.itemId )
                .getBuffer()
        CursedWeaponManager.announce( packet )

        this.cancelTask()

        this.isActivatedValue = false
        this.isDropped = false
        this.endTime = 0
        this.playerId = null
        this.playerKarma = 0
        this.playerPkKills = 0
        this.item = null
        this.nbKills = 0
    }

    getLevel() {
        if ( this.nbKills > ( this.stageKills * this.skillMaxLevel ) ) {
            return this.skillMaxLevel
        }
        return ( this.nbKills / this.stageKills )
    }

    getStageKills() {
        return this.stageKills
    }

    getTimeLeft() {
        return this.endTime - Date.now()
    }

    async giveSkill(): Promise<void> {
        let level = 1 + ( this.nbKills / this.stageKills )
        if ( level > this.skillMaxLevel ) {
            level = this.skillMaxLevel
        }

        let player = L2World.getPlayer( this.playerId )
        let skill: Skill = SkillCache.getSkill( this.skillId, level )
        await player.addSkill( skill, false )

        // Void Burst, Void Flow
        await player.addSkill( CommonSkill.VOID_BURST.getSkill(), false )
        player.addTransformSkill( CommonSkill.VOID_BURST.getSkill() )

        await player.addSkill( CommonSkill.VOID_FLOW.getSkill(), false )
        player.addTransformSkill( CommonSkill.VOID_FLOW.getSkill() )

        player.sendSkillList()
    }

    async increaseKills() : Promise<void> {
        this.nbKills++

        let player: L2PcInstance = L2World.getPlayer( this.playerId )
        if ( player && player.isOnline() ) {
            player.setPkKills( this.nbKills )
            player.broadcastUserInfo()

            if ( ( ( this.nbKills % this.stageKills ) === 0 ) && ( this.nbKills <= ( this.stageKills * ( this.skillMaxLevel - 1 ) ) ) ) {
                await this.giveSkill()
            }
        }

        this.endTime -= this.durationLost * 60000
        await DatabaseManager.getCursedWeapons().deleteWeapon( this )
        return DatabaseManager.getCursedWeapons().saveWeapon( this )
    }

    reActivate() {
        this.isActivatedValue = true
        if ( ( this.endTime - Date.now() ) <= 0 ) {
            return this.endOfLife()
        }

        this.removeTask = setInterval( this.runRemoveTask.bind( this ), this.durationLost * 12000 )
    }

    async removeSkill( player: L2PcInstance ): Promise<void> {
        await Promise.all( [
            player.removeSkillById( this.skillId ),
            player.removeSkillById( CommonSkill.VOID_BURST.getSkill().getId() ),
            player.removeSkillById( CommonSkill.VOID_FLOW.getSkill().getId() ),
            player.untransform(),
        ] )

        player.sendSkillList()
    }

    runRemoveTask() {
        if ( Date.now() >= this.endTime ) {
            this.endOfLife()
        }
    }

    setNbKills( value: number ) {
        this.nbKills = value
    }

    setPlayer( player: L2PcInstance ) {
        this.playerId = player ? player.getObjectId() : null
    }

    isActivated() {
        return this.isActivatedValue
    }

    getWorldPosition(): ILocational {
        let player = this.getPlayer()
        if ( this.isActivated() && player ) {
            return player
        }

        if ( this.isDropped && this.item ) {
            return this.item
        }

        return
    }

    getItemId() {
        return this.itemId
    }

    isActive() {
        return this.isActivatedValue || this.isDropped
    }

    async dropBy( attacker: L2Character ): Promise<void> {
        if ( _.random( 100 ) <= this.disappearChance ) {
            await this.endOfLife()
            return
        }

        await this.dropIt( null, null, attacker, false )

        let player = this.getPlayer()

        player.setKarma( this.playerKarma )
        player.setPkKills( this.playerPkKills )
        player.setCursedWeaponEquippedId( 0 )

        await this.removeSkill( player )

        player.abortAttack()
        player.broadcastUserInfo()
    }

    getPlayer(): L2PcInstance {
        return L2World.getPlayer( this.playerId )
    }
}