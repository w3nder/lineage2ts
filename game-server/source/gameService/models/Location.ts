import { L2Object } from './L2Object'
import { LocationProperties } from './LocationProperties'

export interface ILocational {
    getX(): number
    getY(): number
    getZ(): number
    getHeading(): number
    getInstanceId(): number
}

export interface IPositionable extends ILocational {
    setX( x: number ): void
    setY( y: number ): void
    setZ( z: number ): void
    setXYZCoordinates( x: number, y: number, z: number ): void
    setXYZ( location: ILocational ): void
    setHeading( heading: number ): void
    setInstanceId( instanceId: number ): void
}

export class Location implements IPositionable, LocationProperties {
    x: number
    y: number
    z: number
    heading: number
    instanceId: number

    static fromObject( object: L2Object ) : Location {
        return new Location(
            object.getX(),
            object.getY(),
            object.getZ(),
            object.getHeading(),
            object.getInstanceId()
        )
    }

    constructor( x: number, y: number, z: number, heading: number = 0, instanceId: number = 0 ) {
        this.x = x
        this.y = y
        this.z = z
        this.heading = heading
        this.instanceId = instanceId
    }

    setXYZ( location: ILocational ): void {
        this.x = location.getX()
        this.y = location.getY()
        this.z = location.getZ()
    }

    copy( location: ILocational ): void {
        this.x = location.getX()
        this.y = location.getY()
        this.z = location.getZ()
        this.heading = location.getHeading()
        this.instanceId = location.getInstanceId()
    }

    getHeading(): number {
        return this.heading
    }

    getInstanceId(): number {
        return this.instanceId
    }

    getX(): number {
        return this.x
    }

    getY(): number {
        return this.y
    }

    getZ(): number {
        return this.z
    }

    setHeading( heading: number ): void {
        this.heading = heading
    }

    setInstanceId( instanceId: number ): void {
        this.instanceId = instanceId
    }

    setX( x: number ): void {
        this.x = x
    }

    setY( y: number ): void {
        this.y = y
    }

    setZ( z: number ): void {
        this.z = z
    }

    setXYZCoordinates( x: number, y: number, z: number ): void {
        this.x = x
        this.y = y
        this.z = z
    }
}