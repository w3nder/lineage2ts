import { DataManager } from '../../../data/manager'
import { L2World } from '../../L2World'
import { PacketDispatcher } from '../../PacketDispatcher'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { MusicPacket } from '../../packets/send/Music'
import { ExFishingStartCombat, FishDeceptiveMode, FishingMode } from '../../packets/send/ExFishingStartCombat'
import { ExFishingHpRegen } from '../../packets/send/ExFishingHpRegen'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { QuestHelper } from '../../../listeners/helpers/QuestHelper'
import { FishingChampionshipManager } from '../../cache/FishingChampionshipManager'
import { L2FishingMonsterDataItem } from '../../../data/interface/FishingMonsterDataApi'
import { ConfigManager } from '../../../config/ConfigManager'
import { PlayerVariablesManager } from '../../variables/PlayerVariablesManager'
import { MultipliersVariableNames, PremiumMultipliersPropertyName } from '../../values/L2PcValues'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { InventoryAction } from '../../enums/InventoryAction'
import _ from 'lodash'
import { FishDataItem } from '../../../data/interface/FishDataApi'
import { FishGrade } from '../../enums/FishGrade'
import Timeout = NodeJS.Timeout

const enum FishingActionStatus {
    None,
    Success,
    Failure
}

const lowGradeLures = new Set<number>( [ 6519, 6522, 6525, 8505, 8508, 8511 ] )
const normalGradeLures = new Set<number>( [ 6520, 6523, 6526 ] )
const highGradeLures = new Set<number>( [ 6521, 6524, 6527, 8507, 8510, 8513 ] )

export function getLureDelayMultiplier( itemId: number ) : number {
    if ( lowGradeLures.has( itemId ) ) {
        return 133
    }

    if ( normalGradeLures.has( itemId )
        || ( itemId >= 8505 && itemId <= 8513 )
        || ( itemId >= 7610 && itemId <= 7613 )
        || ( itemId >= 7807 && itemId <= 7809 )
        || ( itemId >= 8484 && itemId <= 8486 ) ) {
        return 100
    }

    if ( highGradeLures.has( itemId ) ) {
        return 66
    }

    return 0
}

export class L2Fishing {
    playerId: number // object id of L2PcInstance
    timeLeftSeconds: number
    stop: number = 0
    actionStatus: FishingActionStatus = FishingActionStatus.None
    isAnimated: number = 0
    mode: FishingMode
    deceptiveMode: FishDeceptiveMode = FishDeceptiveMode.None
    fishingTask: Timeout
    shouldWait: boolean
    fishId: number
    maxHp: number
    currentHp: number
    hpLimit: number
    regenerateHp: number
    isUpperGrade: boolean
    lureId: number

    constructor( player: L2PcInstance, fish: FishDataItem ) {
        this.playerId = player.objectId
        this.maxHp = fish.hp
        this.currentHp = this.maxHp
        this.regenerateHp = fish.hpRegeneration

        this.fishId = fish.itemId
        this.timeLeftSeconds = Math.floor( fish.combatDuration * ConfigManager.general.getFishingDurationMultiplier() )
        this.isUpperGrade = fish.grade === FishGrade.Upper
        this.lureId = player.getLure().getId()

        if ( this.isUpperGrade ) {
            this.deceptiveMode = Math.random() >= 0.9 ? FishDeceptiveMode.Enabled : FishDeceptiveMode.None
        }

        this.mode = Math.random() >= 0.8 ? FishingMode.Alternate : FishingMode.Standard
        this.hpLimit = this.maxHp * 2

        BroadcastHelper.dataToSelfBasedOnVisibility( player, ExFishingStartCombat( this.playerId, this.timeLeftSeconds, this.maxHp, this.mode, fish.grade, this.deceptiveMode ) )
        player.sendCopyData( MusicPacket.SF_S_01 )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.GOT_A_BITE ) )

        this.fishingTask = setInterval( this.runAITask.bind( this ), 1000 )
    }

    async changeHp( hp: number, penetrationModifier: number ): Promise<void> {
        this.currentHp = Math.max( 0, this.currentHp - hp )

        let player = L2World.getPlayer( this.playerId )
        BroadcastHelper.dataToSelfBasedOnVisibility( player, ExFishingHpRegen( this.playerId, this.timeLeftSeconds, this.currentHp, this.mode, this.actionStatus, this.isAnimated, penetrationModifier, this.deceptiveMode ) )

        this.isAnimated = 0

        if ( this.currentHp > this.hpLimit ) {
            this.currentHp = this.hpLimit
            return this.endFishing( false )
        }

        if ( this.currentHp === 0 ) {
            return this.endFishing( true )
        }
    }

    async endFishing( isWin: boolean ): Promise<void> {
        this.clearFishingTask()

        let player: L2PcInstance = L2World.getPlayer( this.playerId )
        if ( !player ) {
            return
        }

        if ( isWin ) {
            let fishingMonster: L2FishingMonsterDataItem = DataManager.getFishingMonsterData().getFishingMonster( player.getLevel() )
            if ( fishingMonster ) {
                if ( _.random( 100 ) <= fishingMonster.chance ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CAUGHT_SOMETHING_SMELLY_THROW_IT_BACK ) )

                    let monster = QuestHelper.addSpawnAtLocation( fishingMonster.id, player )
                    AIEffectHelper.notifyAttacked( monster, player )
                } else {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CAUGHT_SOMETHING ) )

                    await FishingChampionshipManager.addNewFish( player, this.lureId )
                    await player.addItem( this.fishId, 1, -1, player.getObjectId(), InventoryAction.Fishing )

                    if ( ConfigManager.general.isAllowFishingExperience() ) {
                        let playerModifier: number = _.get( PlayerVariablesManager.get( player.getObjectId(), PremiumMultipliersPropertyName ), MultipliersVariableNames.fishingExperienceBonus, 1 ) as number
                        let expValue: number = Math.floor( ( this.maxHp / 10 ) * ConfigManager.general.getFishingExperienceMultiplier() * playerModifier )
                        let spValue: number = Math.floor( ( expValue / 10 ) * ConfigManager.general.getFishingExperienceMultiplier() * playerModifier )

                        if ( expValue > 0 || spValue > 0 ) {
                            await player.addExpAndSp( Math.max( expValue, 0 ), Math.max( spValue, 0 ) )
                        }
                    }
                }
            }
        }

        player.endFishing( isWin )
        this.playerId = 0
    }

    unloadFishing() : void {
        this.clearFishingTask()
        this.playerId = 0
    }

    clearFishingTask() : void {
        if ( this.fishingTask ) {
            clearInterval( this.fishingTask )
            this.fishingTask = null
        }
    }

    runAITask(): Promise<void> {
        if ( !this.playerId ) {
            return
        }

        if ( this.currentHp >= this.hpLimit ) {
            PacketDispatcher.sendOwnedData( this.playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.BAIT_STOLEN_BY_FISH ) )
            return this.endFishing( false )
        }

        if ( this.timeLeftSeconds <= 0 ) {
            PacketDispatcher.sendOwnedData( this.playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.FISH_SPIT_THE_HOOK ) )
            return this.endFishing( false )
        }

        if ( this.shouldWait ) {
            return
        }

        this.shouldWait = true
        this.timeLeftSeconds--

        if ( ( this.mode === FishingMode.Alternate && this.deceptiveMode === FishDeceptiveMode.None )
                || ( this.mode === FishingMode.Standard && this.deceptiveMode === FishDeceptiveMode.Enabled ) ) {
            this.currentHp += this.regenerateHp
        }

        if ( this.stop === 0 ) {
            this.stop = 1

            if ( Math.random() >= 0.7 ) {
                this.mode = this.mode === FishingMode.Standard ? FishingMode.Alternate : FishingMode.Standard
            }

            if ( this.isUpperGrade && Math.random() >= 0.9 ) {
                this.deceptiveMode = this.deceptiveMode === FishDeceptiveMode.None ? FishDeceptiveMode.Enabled : FishDeceptiveMode.None
            }

        } else {
            this.stop--
        }

        this.shouldWait = false

        if ( this.isAnimated !== 0 ) {
            let player = L2World.getPlayer( this.playerId )
            if ( !player ) {
                return
            }

            let packet: Buffer = ExFishingHpRegen( this.playerId, this.timeLeftSeconds, this.currentHp, this.mode, 0, this.isAnimated, 0, this.deceptiveMode )

            player.sendCopyData( packet )
            BroadcastHelper.dataBasedOnVisibility( player, packet )
            return
        }

        PacketDispatcher.sendOwnedData( this.playerId, ExFishingHpRegen( this.playerId, this.timeLeftSeconds, this.currentHp, this.mode, 0, this.isAnimated, 0, this.deceptiveMode ) )
    }

    usePumping( damage: number, penetrationModifier: number ): Promise<void> {
        this.isAnimated = 1

        if ( _.random( 100 ) > 90 ) {
            PacketDispatcher.sendOwnedData( this.playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.FISH_RESISTED_ATTEMPT_TO_BRING_IT_IN ) )
            this.actionStatus = FishingActionStatus.None
            return this.changeHp( 0, penetrationModifier )
        }

        if ( this.mode === FishingMode.Standard ) {
            if ( this.deceptiveMode === FishDeceptiveMode.None ) {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.PUMPING_SUCCESFUL_S1_DAMAGE )
                        .addNumber( damage )
                        .getBuffer()
                PacketDispatcher.sendOwnedData( this.playerId, packet )

                if ( penetrationModifier > 0 ) {
                    let penetrationPacket: Buffer = new SystemMessageBuilder( SystemMessageIds.PUMPING_SUCCESSFUL_PENALTY_S1 )
                            .addNumber( penetrationModifier )
                            .getBuffer()
                    PacketDispatcher.sendOwnedData( this.playerId, penetrationPacket )
                }

                this.actionStatus = FishingActionStatus.Success
                return this.changeHp( damage, penetrationModifier )
            }

            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.FISH_RESISTED_PUMPING_S1_HP_REGAINED )
                    .addNumber( damage )
                    .getBuffer()
            PacketDispatcher.sendOwnedData( this.playerId, packet )
            this.actionStatus = FishingActionStatus.Failure

            return this.changeHp( -damage, penetrationModifier )
        }

        if ( this.deceptiveMode === FishDeceptiveMode.None ) {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.FISH_RESISTED_PUMPING_S1_HP_REGAINED )
                    .addNumber( damage )
                    .getBuffer()
            PacketDispatcher.sendOwnedData( this.playerId, packet )
            this.actionStatus = FishingActionStatus.Failure

            return this.changeHp( -damage, penetrationModifier )
        }

        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.PUMPING_SUCCESFUL_S1_DAMAGE )
                .addNumber( damage )
                .getBuffer()
        PacketDispatcher.sendOwnedData( this.playerId, packet )
        if ( penetrationModifier > 0 ) {
            let penetrationPacket: Buffer = new SystemMessageBuilder( SystemMessageIds.PUMPING_SUCCESSFUL_PENALTY_S1 )
                    .addNumber( penetrationModifier )
                    .getBuffer()
            PacketDispatcher.sendOwnedData( this.playerId, penetrationPacket )
        }
        this.actionStatus = FishingActionStatus.Success

        return this.changeHp( damage, penetrationModifier )
    }

    useReeling( damage: number, penetrationModifier: number ): Promise<void> {
        this.isAnimated = 2
        if ( Math.random() > 0.9 ) {
            PacketDispatcher.sendOwnedData( this.playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.FISH_RESISTED_ATTEMPT_TO_BRING_IT_IN ) )
            this.actionStatus = FishingActionStatus.None

            return this.changeHp( 0, penetrationModifier )
        }

        if ( this.mode === FishingMode.Alternate ) {
            if ( this.deceptiveMode === FishDeceptiveMode.None ) {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.REELING_SUCCESFUL_S1_DAMAGE )
                        .addNumber( damage )
                        .getBuffer()
                PacketDispatcher.sendOwnedData( this.playerId, packet )
                if ( penetrationModifier > 0 ) {
                    let penetrationPacket: Buffer = new SystemMessageBuilder( SystemMessageIds.REELING_SUCCESSFUL_PENALTY_S1 )
                            .addNumber( penetrationModifier )
                            .getBuffer()
                    PacketDispatcher.sendOwnedData( this.playerId, penetrationPacket )
                }
                this.actionStatus = FishingActionStatus.Success
                return this.changeHp( damage, penetrationModifier )
            }

            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.FISH_RESISTED_REELING_S1_HP_REGAINED )
                    .addNumber( damage )
                    .getBuffer()
            PacketDispatcher.sendOwnedData( this.playerId, packet )
            this.actionStatus = FishingActionStatus.Failure
            return this.changeHp( -damage, penetrationModifier )
        }

        if ( this.deceptiveMode === FishDeceptiveMode.None ) {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.FISH_RESISTED_REELING_S1_HP_REGAINED )
                    .addNumber( damage )
                    .getBuffer()
            PacketDispatcher.sendOwnedData( this.playerId, packet )
            this.actionStatus = FishingActionStatus.Failure
            return this.changeHp( -damage, penetrationModifier )
        }

        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.REELING_SUCCESFUL_S1_DAMAGE )
                .addNumber( damage )
                .getBuffer()
        PacketDispatcher.sendOwnedData( this.playerId, packet )
        if ( penetrationModifier > 0 ) {
            let penetrationPacket: Buffer = new SystemMessageBuilder( SystemMessageIds.REELING_SUCCESSFUL_PENALTY_S1 )
                    .addNumber( penetrationModifier )
                    .getBuffer()
            PacketDispatcher.sendOwnedData( this.playerId, penetrationPacket )
        }
        this.actionStatus = FishingActionStatus.Success
        return this.changeHp( damage, penetrationModifier )
    }
}