export interface L2FishingRod {
    id: number
    itemId: number
    level: number
    name: string
    damage: number
}