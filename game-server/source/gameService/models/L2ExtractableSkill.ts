import { ItemData } from '../interface/ItemData'

export interface L2ExtractableSkill {
    hash: number
    products: Array<L2ExtractableProductItem>
}

export interface L2ExtractableProductItem {
    items: Array<ItemData>
    chance: number
}