import { L2RecipeItem } from './l2RecipeItem'
import { L2RecipeStat } from './L2RecipeStat'

export interface L2RecipeDefinition {
    items: Array<L2RecipeItem>
    statConsume: L2RecipeStat
    id: number
    level: number
    recipeItemId: number
    recipeName: string
    successRate: number
    productionItemId: number
    productionAmount: number
    rareItemId: number
    rareCount: number
    rarity: number
    isDwarvenRecipe: boolean
}