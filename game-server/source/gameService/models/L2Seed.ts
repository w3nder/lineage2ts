import { ConfigManager } from '../../config/ConfigManager'
import { ItemManagerCache } from '../cache/ItemManagerCache'
import { L2Item } from './items/L2Item'
import _ from 'lodash'

export class L2Seed {
    seedId: number
    cropId: number // crop type
    level: number // seed level
    matureId: number // mature crop type
    reward1: number
    reward2: number
    castleId: number // id of manor (castle id) where seed can be farmed
    isAlternativeValue: boolean
    seedLimit: number
    cropLimit: number
    seedReferencePrice: number
    cropReferencePrice: number

    isAlternative() {
        return this.isAlternativeValue
    }

    getLevel() {
        return this.level
    }

    getSeedId() {
        return this.seedId
    }

    getCropId() {
        return this.cropId
    }

    getReward( type: number ) {
        return type === 1 ? this.reward1 : this.reward2
    }

    getSeedLimit() {
        return this.seedLimit * ConfigManager.rates.getRateDropManor()
    }

    getSeedMinPrice() {
        return Math.floor( this.getSeedReferencePrice() * 0.6 )
    }

    getSeedMaxPrice() {
        return this.getSeedReferencePrice() * 10
    }

    getCastleId() {
        return this.castleId
    }

    getCropLimit() {
        return this.cropLimit * ConfigManager.rates.getRateDropManor()
    }

    getCropMinPrice() {
        return Math.floor( this.getCropReferencePrice() * 0.6 )
    }

    getCropMaxPrice() {
        return this.getCropReferencePrice() * 10
    }

    getSeedReferencePrice() {
        if ( _.isUndefined( this.cropReferencePrice ) ) {
            this.seedReferencePrice = 0

            let template : L2Item = ItemManagerCache.getTemplate( this.seedId )
            if ( template ) {
                this.seedReferencePrice = template.getReferencePrice()
            }
        }

        return this.seedReferencePrice
    }

    getCropReferencePrice() {
        if ( _.isUndefined( this.cropReferencePrice ) ) {
            this.cropReferencePrice = 0

            let template : L2Item = ItemManagerCache.getTemplate( this.cropId )
            if ( template ) {
                this.cropReferencePrice = template.getReferencePrice()
            }
        }

        return this.cropReferencePrice
    }

    getMatureId() {
        return this.matureId
    }
}