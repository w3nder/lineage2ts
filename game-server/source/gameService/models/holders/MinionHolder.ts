export interface MinionHolder {
    id: number
    count: number
    respawnTime: number
}