import { SkillHolder } from './SkillHolder'

export class InvulnerableSkillHolder extends SkillHolder {
    instances: number = 1

    constructor( id: number, level: number ) {
        super( id, level )
    }
}