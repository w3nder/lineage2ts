export interface IIdentifiable {
    getObjectId() : number
}