import { WeaponType, WeaponTypeMask } from './type/WeaponType'
import { L2Item } from './L2Item'
import { Skill } from '../Skill'
import { ItemInstanceType } from './ItemInstanceType'
import { L2Character } from '../actor/L2Character'
import { Formulas, FormulasValues } from '../stats/Formulas'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2Object } from '../L2Object'
import { L2World } from '../../L2World'
import { EventType, NpcSeeSkillEvent } from '../events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventPoolCache } from '../../cache/EventPoolCache'
import _ from 'lodash'
import { ItemType1 } from '../../enums/items/ItemType1'
import { ItemType2 } from '../../enums/items/ItemType2'

export class L2Weapon extends L2Item {
    type: WeaponType
    isMagicWeaponValue: boolean
    randomDamage: number = 0
    soulShotCount: number = 0
    spiritShotCount: number = 0
    mpConsume: number = 0
    baseAttackRange: number = 0
    baseAttackAngle: number = 0
    /**
     * Skill that activates when item is enchanted +4 (for duals).
     */
    enchant4Skill: Skill
    changeWeaponId: number = 0

    skillOnMagic: Skill
    skillsOnMagicChance: number = 1
    skillOnCrit: Skill

    reducedSoulshot: number = 0
    reducedSoulshotChance: number = 0

    reducedMpConsume: number = 0
    reducedMpConsumeChance: number = 0

    isForceEquip: boolean = false
    isAttackWeapon: boolean = true
    useWeaponSkillsOnly: boolean = false

    constructor() {
        super()
        this.instanceType = ItemInstanceType.L2Weapon

        this.type1 = ItemType1.WEAPON_RING_EARRING_NECKLACE
        this.type2 = ItemType2.WEAPON
    }

    getBaseAttackAngle() {
        return this.baseAttackAngle
    }

    getBaseAttackRange() {
        return this.baseAttackRange
    }

    getEnchant4Skill(): Skill {
        return this.enchant4Skill
    }

    getItemMask(): number {
        return WeaponTypeMask( this.getItemType() )
    }

    getItemType(): WeaponType {
        return this.type
    }

    getChangeWeaponId() {
        return this.changeWeaponId
    }

    getRandomDamage() {
        return this.randomDamage
    }

    hasCastOnMagicSkill(): boolean {
        return !!this.skillOnMagic
    }

    async castOnMagicSkill( caster: L2Character, target: L2Character, trigger: Skill ): Promise<void> {
        if ( !this.skillOnMagic || trigger.isToggle() ) {
            return
        }

        if ( trigger.isBad() !== this.skillOnMagic.isBad() ) {
            return
        }

        if ( trigger.isMagic() !== this.skillOnMagic.isMagic() ) {
            return
        }

        if ( this.skillsOnMagicChance < 1 && Math.random() > this.skillsOnMagicChance ) {
            return
        }

        if ( !this.skillOnMagic.checkCondition( caster, target ) ) {
            return
        }

        if ( this.skillOnMagic.isBad() && ( Formulas.calculateShieldUse( caster, target, this.skillOnMagic ) === FormulasValues.SHIELD_DEFENSE_PERFECT_BLOCK ) ) {
            return
        }

        await this.skillOnMagic.activateCasterSkill( caster, [ target ], true )

        if ( caster.isPlayable() ) {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_HAS_BEEN_ACTIVATED )
                    .addSkillName( this.skillOnMagic )
                    .getBuffer()
            caster.sendOwnedData( packet )
        }

        // TODO : add simple time check to trigger such event only on specific time interval, aka debounce on 3 second interval
        L2World.forEachObjectByPredicate( caster, 1000, ( object: L2Object ) => {
            if ( object.isNpc() && ListenerCache.hasNpcTemplateListeners( object.getId(), EventType.NpcSeeSkill ) ) {
                let eventData = EventPoolCache.getData( EventType.NpcSeeSkill ) as NpcSeeSkillEvent

                eventData.originatorId = caster.getObjectId()
                eventData.playerId = caster.getActingPlayerId()
                eventData.receiverId = object.getObjectId()
                eventData.receiverNpcId = object.getId()
                eventData.receiverIsChampion = object.isChampion()
                eventData.skillId = this.skillOnMagic.getId()
                eventData.skillLevel = this.skillOnMagic.getLevel()
                eventData.targetIds = [ target.getObjectId() ]

                ListenerCache.sendNpcTemplateEvent( object.getId(), EventType.NpcSeeSkill, eventData )
            }
        } )
    }

    isMagicWeapon(): boolean {
        return this.isMagicWeaponValue
    }

    getSpiritShotCount() {
        return this.spiritShotCount
    }

    getSoulShotCount() {
        return this.soulShotCount
    }

    getReducedSoulShot() {
        return this.reducedSoulshot
    }

    getReducedSoulShotChance() {
        return this.reducedSoulshotChance
    }

    isRanged() {
        return this.isBow() || this.isCrossBow()
    }

    isBow(): boolean {
        return this.type === WeaponType.BOW
    }

    isCrossBow(): boolean {
        return this.type === WeaponType.CROSSBOW
    }

    getMpConsume() {
        return this.mpConsume
    }

    getReducedMpConsume() {
        return this.reducedMpConsume
    }

    getReducedMpConsumeChance() {
        return this.reducedMpConsumeChance
    }

    hasSkillOnCrit(): boolean {
        return !!this.skillOnCrit
    }

    castOnCriticalSkill( attacker: L2Character, target: L2Character ): Promise<void> {
        if ( !this.skillOnCrit ) {
            return
        }

        if ( !this.skillOnCrit.checkCondition( attacker, target ) ) {
            return
        }

        return this.skillOnCrit.activateCasterSkill( attacker, [ target ], true )
    }

    getItemVariety(): string {
        return _.capitalize( WeaponType[ this.type ] )
    }
}