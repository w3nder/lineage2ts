import { L2Item } from '../L2Item'

export class EnchantRateItem {
    name: string
    slot: number
    isMagicWeapon: boolean

    validate( item: L2Item ) : boolean {
        if ( ( this.slot !== 0 ) && ( ( item.getBodyPart() & this.slot ) === 0 ) ) {
            return false
        }

        return ( item.isMagicWeapon() == this.isMagicWeapon )
    }
}