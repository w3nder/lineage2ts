import { CrystalType } from '../type/CrystalType'
import { L2ItemInstance } from '../instance/L2ItemInstance'
import { EnchantSupportItem } from './EnchantSupportItem'
import { ItemType2 } from '../../../enums/items/ItemType2'
import { L2Item } from '../L2Item'
import { ItemManagerCache } from '../../../cache/ItemManagerCache'

export class AbstractEnchantItem {
    id: number
    grade: CrystalType
    maxEnchantLevel: number
    bonusRate: number

    isValid( item: L2ItemInstance, supportItem: EnchantSupportItem ) : boolean {
        if ( !item || !item.isEnchantable() ) {
            return false
        }

        if ( !this.isValidItemType( item.getItem().getType2() ) ) {
            return false
        }

        if ( ( this.maxEnchantLevel > 0 ) && ( item.getEnchantLevel() > this.maxEnchantLevel ) ) {
            return false
        }

        return this.grade === item.getItem().getItemGradeSPlus()
    }

    isValidItemType( type: ItemType2 ) {
        if ( type === ItemType2.WEAPON ) {
            return this.isWeapon()
        }

        if ( ( type === ItemType2.SHIELD_ARMOR ) || ( type === ItemType2.ACCESSORY ) ) {
            return !this.isWeapon()
        }

        return false
    }

    isWeapon() : boolean {
        return false
    }

    getItem() : L2Item {
        return ItemManagerCache.getTemplate( this.id )
    }
}