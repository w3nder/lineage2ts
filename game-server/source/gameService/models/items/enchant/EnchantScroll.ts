import { AbstractEnchantItem } from './AbstractEnchantItem'
import { EnchantSupportItem } from './EnchantSupportItem'
import { L2ItemInstance } from '../instance/L2ItemInstance'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { DataManager } from '../../../../data/manager'
import { EnchantItemGroup } from './EnchantItemGroup'
import { EtcItemType } from '../../../enums/items/EtcItemType'
import { EnchantResultType } from '../../../enums/items/EnchantResultType'
import { PlayerVariablesManager } from '../../../variables/PlayerVariablesManager'
import { MultipliersVariableNames, PremiumMultipliersPropertyName } from '../../../values/L2PcValues'
import _ from 'lodash'
import { recordDataViolation } from '../../../helpers/PlayerViolations'
import { DataIntegrityViolationType } from '../../events/EventType'

export class EnchantScroll extends AbstractEnchantItem {
    isWeaponValue: boolean
    isBlessedValue: boolean
    isSafeValue: boolean
    groupId: number
    items: Array<number>

    isWeapon(): boolean {
        return this.isWeaponValue
    }

    isBlessed() : boolean {
        return this.isBlessedValue
    }

    isSafe() : boolean {
        return this.isSafeValue
    }

    isValid( item: L2ItemInstance, supportItem: EnchantSupportItem ): boolean {
        if ( this.items && !this.items.includes( item.getId() ) ) {
            return false
        }

        if ( supportItem ) {
            if ( this.isBlessed() ) {
                return false
            }

            if ( !supportItem.isValid( item, supportItem ) ) {
                return false
            }

            if ( supportItem.isWeapon() !== this.isWeapon() ) {
                return false
            }
        }

        return super.isValid( item, supportItem )
    }

    getChance( player: L2PcInstance, item: L2ItemInstance ) : number {
        if ( !DataManager.getEnchantItemGroups().getScrollGroup( this.groupId ) ) {
            recordDataViolation( player.getObjectId(), '66b4a90f-e4f6-4888-ab5f-a80d83d010af', DataIntegrityViolationType.ItemEnchant, 'Non-existent enchant scroll group specified by id', [ this.groupId, item.getId() ] )
            return Number.MIN_SAFE_INTEGER
        }

        let group : EnchantItemGroup = DataManager.getEnchantItemGroups().getItemGroupByItem( item.getItem(), this.groupId )
        if ( !group ) {
            recordDataViolation( player.getObjectId(), 'db3adbeb-5f00-47e8-b09a-e7d5b6bd1e89', DataIntegrityViolationType.ItemEnchant, 'Non-existent enchant item group specified by id', [ this.groupId, item.getId() ] )
            return Number.MIN_SAFE_INTEGER
        }

        let playerBonus : number = _.get( PlayerVariablesManager.get( player.getObjectId(), PremiumMultipliersPropertyName ), MultipliersVariableNames.enchantScrollBonus, 0 ) as number
        return group.getChance( item.getEnchantLevel() ) + playerBonus
    }

    setFlags() {
        let type : number = this.getItem().getItemType()

        this.isWeaponValue = ( type === EtcItemType.ANCIENT_CRYSTAL_ENCHANT_WP ) || ( type === EtcItemType.BLESS_SCRL_ENCHANT_WP ) || ( type === EtcItemType.SCRL_ENCHANT_WP )
        this.isBlessedValue = ( type === EtcItemType.BLESS_SCRL_ENCHANT_AM ) || ( type === EtcItemType.BLESS_SCRL_ENCHANT_WP )
        this.isSafeValue = ( type === EtcItemType.ANCIENT_CRYSTAL_ENCHANT_AM ) || ( type === EtcItemType.ANCIENT_CRYSTAL_ENCHANT_WP )
    }

    calculateSuccess( player: L2PcInstance, item: L2ItemInstance, support: EnchantSupportItem ) : EnchantResultType {
        if ( !this.isValid( item, support ) ) {
            return EnchantResultType.ERROR
        }

        let chance = this.getChance( player, item )
        if ( chance < 0 ) {
            return EnchantResultType.ERROR
        }

        let supportBonusRate : number = support ? support.bonusRate : 0
        let finalChance : number = chance + this.bonusRate + supportBonusRate

        return ( _.random( 100 ) <= finalChance ) ? EnchantResultType.SUCCESS : EnchantResultType.FAILURE
    }
}