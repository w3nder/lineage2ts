export enum CrystalType {
    NONE,
    D,
    C,
    B,
    A,
    S,
    S80,
    S84
}

export interface CrystalTypeValue {
    id: number,
    itemId: number,
    bonusArmor: number,
    bonusWeapon: number
}

const CrystalTypeMap : Record<CrystalType, CrystalTypeValue> = {
    [ CrystalType.NONE ]: {
        id: 0,
        itemId: 0,
        bonusArmor: 0,
        bonusWeapon: 0
    },
    [ CrystalType.D ]: {
        id: 1,
        itemId: 1458,
        bonusArmor: 11,
        bonusWeapon: 90
    },
    [ CrystalType.C ]: {
        id: 2,
        itemId: 1459,
        bonusArmor: 6,
        bonusWeapon: 45
    },
    [ CrystalType.B ]: {
        id: 3,
        itemId: 1460,
        bonusArmor: 11,
        bonusWeapon: 67
    },
    [ CrystalType.A ]: {
        id: 4,
        itemId: 1461,
        bonusArmor: 20,
        bonusWeapon: 145
    },
    [ CrystalType.S ]: {
        id: 5,
        itemId: 1462,
        bonusArmor: 25,
        bonusWeapon: 250
    },
    [ CrystalType.S80 ]: {
        id: 6,
        itemId: 1462,
        bonusArmor: 25,
        bonusWeapon: 250
    },
    [ CrystalType.S84 ]: {
        id: 7,
        itemId: 1462,
        bonusArmor: 25,
        bonusWeapon: 250
    }
}

export const CrystalTypeHelper = {
    getCrystalValues( value: CrystalType ): CrystalTypeValue {
        return CrystalTypeMap[ value ]
    }
}