import { CrystalType, CrystalTypeHelper } from './type/CrystalType'
import { ActionType } from './type/ActionType'
import { Elementals } from '../Elementals'
import { FunctionTemplate } from '../stats/functions/FunctionTemplate'
import { Condition } from '../conditions/Condition'
import { ConfigManager } from '../../../config/ConfigManager'
import { ItemType2 } from '../../enums/items/ItemType2'
import { ItemType1 } from '../../enums/items/ItemType1'
import { L2Character } from '../actor/L2Character'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { L2Summon } from '../actor/L2Summon'
import { SystemMessageBuilder, SystemMessageHelper } from '../../packets/send/SystemMessage'
import { PacketDispatcher } from '../../PacketDispatcher'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { EtcItemType } from '../../enums/items/EtcItemType'
import { L2ItemInstance } from './instance/L2ItemInstance'
import { AbstractFunction } from '../stats/functions/AbstractFunction'
import { Skill } from '../Skill'
import { ItemInstanceType } from './ItemInstanceType'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import { ElementalType } from '../../enums/ElementalType'
import { L2ItemDefaults } from '../../enums/L2ItemDefaults'
import { L2ItemSlots } from '../../enums/L2ItemSlots'

export const EmptyElementals : ReadonlyArray<Elementals> = []
export const EmptyFunctionTemplates : ReadonlyArray<FunctionTemplate> = []

export class L2Item {
    itemId: number
    name: string
    icon: string
    weight: number
    stackable: boolean
    sortOrder: number
    crystalType: CrystalType
    equipReuseDelay: number
    duration: number
    time: number
    bodyPart: L2ItemSlots
    referencePrice: number
    referencePriceX2: number
    crystalCount: number
    sellable: boolean
    dropable: boolean
    destroyable: boolean
    tradeable: boolean
    depositable: boolean
    enchantable: boolean
    elementable: boolean
    questItem: boolean
    freightable: boolean
    isOlympiadRestricted: boolean
    forNpc: boolean
    common: boolean
    heroItem: boolean
    pvpItem: boolean
    immediateEffect: boolean
    exImmediateEffect: boolean
    defaultEnchantLevel: number
    defaultAction: ActionType

    type1: ItemType1
    type2: ItemType2
    elementals: ReadonlyArray<Elementals> = []
    functionTemplates: ReadonlyArray<FunctionTemplate>

    useConditions: Array<Condition> = []
    equipConditions: Array<Condition> = []

    skills: Array<Skill> = []
    skillIds: Set<number> = null
    unequipSkill: Skill

    manaConsumeOnSkill: number
    reuseDelay: number
    sharedReuseGroup: number
    instanceType: ItemInstanceType
    passiveSkills: boolean = false
    elementalDefenceAttributes: Array<number>
    referenceId: string

    constructor() {
        this.instanceType = ItemInstanceType.L2Item
    }

    getElementals() {
        return this.elementals
    }

    getBodyPart() {
        return this.bodyPart
    }

    getWeight(): number {
        return this.weight
    }

    getItemGradeSPlus(): CrystalType {
        switch ( this.getItemGrade() ) {
            case CrystalType.S80:
            case CrystalType.S84:
                return CrystalType.S
            default:
                return this.getItemGrade()
        }
    }

    getItemGrade(): CrystalType {
        return this.getCrystalType()
    }

    getCrystalType(): CrystalType {
        return this.crystalType
    }

    getItemType(): number {
        return 0
    }

    isStackable(): boolean {
        return this.stackable
    }

    isOlympiadRestrictedItem(): boolean {
        return this.isOlympiadRestricted || ConfigManager.olympiad.getRestrictedItems().includes( this.itemId )
    }

    getType2(): ItemType2 {
        return this.type2
    }

    getType1(): ItemType1 {
        return this.type1
    }

    isQuestItem(): boolean {
        return this.questItem
    }

    getName() {
        return this.name
    }

    hasSkills() {
        return this.skills.length > 0
    }

    getSkills(): Array<Skill> {
        return this.skills
    }

    hasSkillWithId( value: number ) : boolean {
        return this.skillIds && this.skillIds.has( value )
    }

    getItemMask() {
        return 0
    }

    checkUseConditions( character: L2Character, sendMessage: boolean ): boolean {
        if ( character.hasActionOverride( PlayerActionOverride.ItemAction ) && !ConfigManager.general.gmItemRestriction() ) {
            return true
        }

        if ( character.isPlayer()
                && ( character as L2PcInstance ).isInOlympiadMode()
                && ( this.isOlympiadRestrictedItem() || this.isHeroItem() ) ) {

            character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_ITEM_IS_NOT_AVAILABLE_FOR_THE_OLYMPIAD_EVENT ) )

            return false
        }

        if ( !this.hasUseConditions() ) {
            return true
        }

        for ( const useCondition of this.useConditions ) {
            if ( useCondition.test( character, character ) ) {
                return false
            }

            if ( character.isSummon() ) {
                let ownerId = ( character as L2Summon ).getOwnerId()
                PacketDispatcher.sendOwnedData( ownerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_CANNOT_USE_ITEM ) )

                return true
            }

            if ( sendMessage ) {
                let message = useCondition.getMessage()
                let messageId = useCondition.getMessageId()
                if ( message ) {
                    character.sendOwnedData( SystemMessageHelper.sendString( message ) )
                } else if ( messageId !== 0 ) {
                    let messageUpdate = new SystemMessageBuilder( messageId )
                    if ( useCondition.addMessageName ) {
                        messageUpdate.addItemNameWithId( this.itemId )
                    }

                    character.sendOwnedData( messageUpdate.getBuffer() )
                }
            }
        }

        return true
    }

    checkEquipConditions( character: L2Character, sendMessage : boolean, defaultMessageId : number = null ) : boolean {
        if ( character.hasActionOverride( PlayerActionOverride.ItemAction ) && !ConfigManager.general.gmItemRestriction() ) {
            return true
        }

        if ( character.isPlayer()
            && ( character as L2PcInstance ).isInOlympiadMode()
            && ( this.isOlympiadRestrictedItem() || this.isHeroItem() ) ) {

            character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_ITEM_CANT_BE_EQUIPPED_FOR_THE_OLYMPIAD_EVENT ) )

            return false
        }

        if ( !this.equipConditions ) {
            return true
        }

        for ( const equipCondition of this.equipConditions ) {
            if ( equipCondition.test( character, character ) ) {
                continue
            }

            if ( sendMessage ) {
                let message = equipCondition.getMessage()
                let messageId = equipCondition.getMessageId() ?? defaultMessageId
                if ( message ) {
                    character.sendOwnedData( SystemMessageHelper.sendString( message ) )
                } else if ( messageId !== 0 ) {
                    let messageUpdate = new SystemMessageBuilder( messageId )
                    if ( equipCondition.addMessageName ) {
                        messageUpdate.addItemNameWithId( this.itemId )
                    }

                    character.sendOwnedData( messageUpdate.getBuffer() )
                }
            }

            return false
        }

        return true
    }

    hasUseConditions() {
        return this.useConditions?.length > 0
    }

    hasEquipConditions() : boolean {
        return this.equipConditions?.length > 0
    }

    isHeroItem() {
        return this.heroItem
    }

    isEquipable() {
        return this.getBodyPart() !== 0 && !( this.getItemMask() === 0 )
    }

    getDuration() {
        return this.duration
    }

    getTime() {
        return this.time
    }

    getSharedReuseGroup() {
        return this.sharedReuseGroup
    }

    getDefaultEnchantLevel() {
        return this.defaultEnchantLevel
    }

    isPetItem() {
        return this.getItemType() === EtcItemType.PET_COLLAR
    }

    getEquipReuseDelay() {
        return this.equipReuseDelay
    }

    /*
        While L2J has function templates that can have condition attached,
        there appears to be no actual conditions in item data. However,
        since we already have equip and use conditions, there is no point
        in checking for function template conditions.
     */
    generateStatFunctions( item: L2ItemInstance ): Array<AbstractFunction> {
        return this.functionTemplates.map( ( template: FunctionTemplate ) => template.generateFunction( item ) )
    }

    getEnchant4Skill(): Skill {
        return null
    }

    getUnequipSkill() : Skill {
        return this.unequipSkill
    }

    getId() {
        return this.itemId
    }

    isInstanceType( type: ItemInstanceType ): boolean {
        return this.instanceType === type
    }

    getReuseDelay() {
        return this.reuseDelay
    }

    isTradeable(): boolean {
        return this.tradeable
    }

    isFreightable() {
        return this.freightable
    }

    getReferencePrice() {
        return this.referencePrice
    }

    getDoubleReferencePrice() : number {
        return this.referencePriceX2
    }

    getDefaultAction(): ActionType {
        return this.defaultAction
    }

    attachCondition( condition: Condition ) {
        this.useConditions.push( condition )
    }

    hasExImmediateEffect() {
        return this.exImmediateEffect
    }

    isDropable() {
        return this.dropable
    }

    isCrystallizable() {
        return this.crystalType !== CrystalType.NONE && this.crystalCount > 0
    }

    getCrystalCount( enchantLevel: number = 0 ) : number {
        if ( enchantLevel > 3 ) {
            switch ( this.type2 ) {
                case ItemType2.SHIELD_ARMOR:
                case ItemType2.ACCESSORY:
                    return this.crystalCount + ( CrystalTypeHelper.getCrystalValues( this.crystalType ).bonusArmor * ( ( 3 * enchantLevel ) - 6 ) )
                case ItemType2.WEAPON:
                    return this.crystalCount + ( CrystalTypeHelper.getCrystalValues( this.crystalType ).bonusWeapon * ( ( 2 * enchantLevel ) - 3 ) )
            }

            return this.crystalCount
        }

        if ( enchantLevel > 0 ) {
            switch ( this.type2 ) {
                case ItemType2.SHIELD_ARMOR:
                case ItemType2.ACCESSORY:
                    return this.crystalCount + ( CrystalTypeHelper.getCrystalValues( this.crystalType ).bonusArmor * enchantLevel )
                case ItemType2.WEAPON:
                    return this.crystalCount + ( CrystalTypeHelper.getCrystalValues( this.crystalType ).bonusWeapon * enchantLevel )
            }
        }

        return this.crystalCount
    }

    getCrystalItemId() {
        return CrystalTypeHelper.getCrystalValues( this.crystalType ).itemId
    }

    isSellable() {
        return this.sellable
    }

    isDepositable() {
        return this.depositable
    }

    isEnchantable(): boolean {
        return ConfigManager.character.getEnchantBlacklist().includes( this.getId() ) ? false : this.enchantable
    }

    isMagicWeapon() {
        return false
    }

    isDestroyable(): boolean {
        return this.destroyable
    }

    isForNpc(): boolean {
        return this.forNpc
    }

    isCommon(): boolean {
        return this.common
    }

    isPvpItem() {
        return this.pvpItem
    }

    isElementable() {
        return this.elementable
    }

    hasImmediateEffect() {
        return this.immediateEffect
    }

    isPotion() {
        return this.isEtcItem() && this.getItemType() === EtcItemType.POTION
    }

    isElixir() {
        return this.isEtcItem() && this.getItemType() === EtcItemType.ELIXIR
    }

    isScroll() {
        return this.isEtcItem() && this.getItemType() === EtcItemType.SCROLL
    }

    getMaterialSortOrder() : number {
        return this.sortOrder
    }

    isRecipe(): boolean {
        return this.isEtcItem() && this.getItemType() === EtcItemType.RECIPE
    }

    isEtcItem(): boolean {
        return this.isInstanceType( ItemInstanceType.L2EtcItem )
    }

    isArmorItem(): boolean {
        return this.isInstanceType( ItemInstanceType.L2Armor )
    }

    isWeaponItem(): boolean {
        return this.isInstanceType( ItemInstanceType.L2Weapon )
    }

    hasStatFunctions(): boolean {
        return this.functionTemplates.length !== 0
    }

    getDefenceElementAttribute( index: ElementalType ): number | null {
        if ( !this.isArmorItem() || index === ElementalType.NONE ) {
            return L2ItemDefaults.DefenceElementAttribute
        }

        if ( this.getElementals().length > 0 ) {
            return this.elementalDefenceAttributes[ index ]
        }

        return null
    }

    getElementalValue( type: ElementalType ) : number {
        let data: Elementals = this.getElemental( type )
        if ( data ) {
            return data.getValue()
        }

        return null
    }

    private getElemental( type: ElementalType ): Elementals {
        return this.elementals.find( ( element: Elementals ) => {
            return element.getElement() === type
        } )
    }

    getAttackElementType() : number | null {
        if ( !this.isWeaponItem() ) {
            return L2ItemDefaults.AttackElementType
        }

        if ( this.getElementals().length > 0 ) {
            return this.getElementals()[ 0 ].getElement()
        }

        return null
    }

    getAttackElementPower() : number | null {
        if ( !this.isWeaponItem() ) {
            return L2ItemDefaults.AttackElementPower
        }

        if ( this.getElementals().length > 0 ) {
            return this.getElementals()[ 0 ].getValue()
        }

        return null
    }

    getItemVariety() : string {
        return 'No Variety'
    }

    hasPassiveSkills() : boolean {
        return this.passiveSkills
    }
}