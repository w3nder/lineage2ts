export interface L2DimensionalTransferItem {
    itemId: number
    amount: number
    sender: string
    createDate: number
    updateDate: number
}