import { L2Object } from '../../L2Object'
import { L2Item } from '../L2Item'
import { ItemLocation } from '../../../enums/ItemLocation'
import { L2Augmentation } from '../../L2Augmentation'
import { Elementals } from '../../Elementals'
import { Options } from '../../options/Options'
import { L2Weapon } from '../L2Weapon'
import { DataManager } from '../../../../data/manager'
import { DefaultEnchantOptions, EnchantOptionsParameters, getEnchantOptions } from '../../options/EnchantOptions'
import { ConfigManager } from '../../../../config/ConfigManager'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { L2World } from '../../../L2World'
import { EtcItemType } from '../../../enums/items/EtcItemType'
import { DatabaseManager } from '../../../../database/manager'
import { InstanceType } from '../../../enums/InstanceType'
import { L2EtcItem } from '../L2EtcItem'
import { L2Character } from '../../actor/L2Character'
import { AbstractFunction } from '../../stats/functions/AbstractFunction'
import { ShotType, ShotTypeHelper } from '../../../enums/ShotType'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { ItemInstanceType } from '../ItemInstanceType'
import { ItemType2 } from '../../../enums/items/ItemType2'
import { ItemType1 } from '../../../enums/items/ItemType1'
import { IDFactoryCache } from '../../../cache/IDFactoryCache'
import { ItemManagerCache } from '../../../cache/ItemManagerCache'
import { ItemsOnGroundManager } from '../../../cache/ItemsOnGroundManager'
import { L2ItemsTableItem } from '../../../../database/interface/ItemsTableApi'
import { SpawnItem } from '../../../packets/send/SpawnItem'
import { DropItem } from '../../../packets/send/DropItem'
import { MercenaryTicketManager } from '../../../instancemanager/MercenaryTicketManager'
import { PickItemUp } from '../../../packets/send/pickItemUp'
import { DeleteObject } from '../../../packets/send/DeleteObject'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import {
    EventType,
    ItemAmountChangeEvent,
    ItemBypassEvent,
    ItemEndOfLifeEvent,
    ItemTalkingEvent,
    PlayerPickupItemEvent,
} from '../../events/EventType'
import { ListenerCache } from '../../../cache/ListenerCache'
import { InventorySlot, ItemTypes } from '../../../values/InventoryValues'
import { EventPoolCache } from '../../../cache/EventPoolCache'
import { L2ItemUpdateStatus } from '../../../enums/L2ItemUpdateStatus'
import { ExRpItemLink } from '../../../packets/send/ExRpItemLink'
import { ItemProtectionCache } from '../../../cache/ItemProtectionCache'
import { ProximalDiscoveryManager } from '../../../cache/ProximalDiscoveryManager'
import { InventoryUpdateStatus } from '../../../enums/InventoryUpdateStatus'
import { PacketHelper } from '../../../packets/PacketVariables'
import aigle from 'aigle'
import { PlayerActionOverride } from '../../../values/PlayerConditions'
import { GeoPolygonCache } from '../../../cache/GeoPolygonCache'
import { Skill } from '../../Skill'
import { PacketDispatcher } from '../../../PacketDispatcher'
import { L2Region } from '../../../enums/L2Region'
import { L2AgathionDataItem } from '../../../../data/interface/AgathionDataApi'
import { getItemLimit } from '../../../helpers/ConfigurationHelper'
import { MsInDays, MsInMinutes } from '../../../enums/MsFromTime'
import { L2ItemDefaults } from '../../../enums/L2ItemDefaults'
import { DefaultDefenceElementAttributes, DefenceElementTypes, ElementalType } from '../../../enums/ElementalType'
import { ElementalAndEnchantProperties } from '../ElementalAndEnchantProperties'
import Timeout = NodeJS.Timeout

const manaMessages: Record<number, number> = {
    1: SystemMessageIds.S1S_REMAINING_MANA_IS_NOW_1,
    5: SystemMessageIds.S1S_REMAINING_MANA_IS_NOW_5,
    10: SystemMessageIds.S1S_REMAINING_MANA_IS_NOW_10,
}

export class L2ItemInstance extends L2Object implements ElementalAndEnchantProperties {
    ownerId: number = 0
    count: number = 1
    remainingLifetime: number = -1
    itemId: number
    item: L2Item
    location: ItemLocation = ItemLocation.VOID
    inventorySlot: number = InventorySlot.None
    enchantLevel: number = -1
    augmentation: L2Augmentation
    remainingManaPoints: number
    consumingManaTimeout: Timeout

    type1: number
    type2: number
    dropTime: number

    publishedData: Buffer
    publishedDataTask: Timeout

    protected: boolean = false
    lastChange: InventoryUpdateStatus = InventoryUpdateStatus.Unchanged

    existsInDb: boolean = false

    elementals: Array<Elementals> = []
    lifeTimeTask: Timeout

    shotsMask: number
    enchantStats: Array<Options> = []
    enchantOptions: EnchantOptionsParameters
    elementalDefenceAttributes: ReadonlyArray<number>
    statFunctions: Array<AbstractFunction>

    /*
        Agathion data
     */
    agathionEnergy: number = 0
    agathion : L2AgathionDataItem

    constructor( objectId: number, itemId: number ) {
        super( objectId )

        this.instanceType = InstanceType.L2ItemInstance
        this.itemId = itemId
        this.item = ItemManagerCache.getTemplate( itemId )

        if ( itemId === 0 || !this.item ) {
            throw new Error( `Item creation failed: itemId = ${ itemId }` )
        }

        this.setName( this.item.getName() )
        this.setCount( 1 )

        this.location = ItemLocation.VOID
        this.enchantLevel = this.item.getDefaultEnchantLevel()
        this.type1 = 0
        this.type2 = 0

        this.dropTime = 0
        this.remainingManaPoints = this.item.getDuration()
        this.remainingLifetime = this.item.getTime() === -1 ? -1 : Date.now() + this.item.getTime() * 60000
        this.computeEnchantOptions()

        let agathion = DataManager.getAgathionData().getByItemId( itemId )
        if ( agathion && agathion.energy > 0 ) {
            this.agathion = agathion
            this.agathionEnergy = agathion.energy
        }

        this.scheduleLifeTimeTask()
        this.computeElementalDefenceAttributes()
    }

    static createFromData( ownerId: number, currentItem: L2ItemsTableItem ): L2ItemInstance {
        let objectId = currentItem.objectId
        let itemId = currentItem.itemId
        let template: L2Item = DataManager.getItems().getTemplate( itemId )
        if ( !template ) {
            return null
        }

        let item = new L2ItemInstance( objectId, itemId )

        item.ownerId = ownerId
        item.count = Math.max( 0, Math.floor( currentItem.count ) )
        item.enchantLevel = currentItem.enchantLevel
        item.type1 = currentItem.customTypeOne
        item.type2 = currentItem.customTypeTwo

        item.location = currentItem.itemLocation
        item.inventorySlot = currentItem.locationData
        item.existsInDb = true

        item.remainingManaPoints = currentItem.manaRemaining
        item.remainingLifetime = currentItem.timeRemaining
        item.agathionEnergy = currentItem.agathionEnergy
        item.computeEnchantOptions()

        return item
    }

    static fromItemId( itemId: number ): L2ItemInstance {
        return new L2ItemInstance( IDFactoryCache.getNextId(), itemId )
    }

    applyAttribute( type: ElementalType, value: number ) {
        let elemental: Elementals = this.getElemental( type )
        if ( elemental ) {
            elemental.setValue( value )
            return
        }

        this.elementals.push( new Elementals( type, value, this.isArmor() ) )
    }

    shouldApplyEnchantStats() : boolean {
        return this.isEquipped() && this.getEnchantOptions() !== DefaultEnchantOptions
    }

    async applyEnchantStats() : Promise<void> {
        let player = this.getActingPlayer()
        if ( !player || !this.shouldApplyEnchantStats() ) {
            return
        }

        await aigle.resolve( this.getEnchantOptions() ).each( ( id: number ) => {
            let option: Options = DataManager.getOptionsData().getOptions( id )
            if ( option ) {
                this.enchantStats.push( option )
                return option.apply( player )
            }
        } )
    }

    changeAmount( amount: number ): void {
        if ( amount === 0 ) {
            return
        }

        let oldAmount = this.getCount()
        let maxAmount = getItemLimit( this.getId() )

        if ( maxAmount <= oldAmount ) {
            return
        }

        if ( amount > 0 && ( maxAmount - oldAmount ) < amount ) {
            return
        }

        let updatedCount = oldAmount + amount

        if ( amount > 0 ) {
            this.setCount( Math.min( maxAmount, updatedCount ) )
        } else {
            this.setCount( Math.max( 0, updatedCount ) )
        }

        if ( ListenerCache.hasItemTemplateEvent( this.getId(), EventType.ItemAmountChange ) ) {
            let data = EventPoolCache.getData( EventType.ItemAmountChange ) as ItemAmountChangeEvent

            data.itemId = this.getId()
            data.oldAmount = oldAmount
            data.newAmount = this.getCount()
            data.objectId = this.getObjectId()

            ListenerCache.sendItemTemplateEvent( this.getId(), EventType.ItemAmountChange, data )
        }
    }

    hasEnchantStats() : boolean {
        return this.enchantStats.length > 0
    }

    async clearEnchantStats() : Promise<void> {
        let player = this.getActingPlayer()
        if ( !player ) {
            this.enchantStats.length = 0
            return
        }

        await aigle.resolve( this.enchantStats ).each( ( option: Options ) => {
            return option.remove( player )
        } )

        this.enchantStats.length = 0
    }

    async decreaseMana( manaPoints: number = ConfigManager.tuning.getShadowItemManaConsumeAmount() ): Promise<void> {
        if ( !this.isShadowItem() ) {
            return
        }

        this.remainingManaPoints = Math.max( this.remainingManaPoints - manaPoints, 0 )

        let player: L2PcInstance = this.getActingPlayer()
        if ( !player ) {
            return
        }

        let message = manaMessages[ this.remainingManaPoints ]

        if ( message ) {
            let packet = new SystemMessageBuilder( message )
                    .addItemName( this.item )
                    .getBuffer()

            player.sendOwnedData( packet )
        }

        let isInPlayerInventory = this.getItemLocation() !== ItemLocation.WAREHOUSE
        if ( this.remainingManaPoints === 0 ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1S_REMAINING_MANA_IS_NOW_0 )
                    .addItemName( this.item )
                    .getBuffer()

            player.sendOwnedData( packet )

            if ( this.isEquipped() ) {
                await player.getInventory().unEquipItemInSlot( this.getLocationSlot() )
                player.broadcastUserInfo()
            }

            if ( isInPlayerInventory ) {
                await player.getInventory().destroyItem( this, this.getCount(), 0, 'L2ItemInstance.decreaseMana' )
            } else {
                await player.initializeWarehouse()
                await player.getWarehouse().destroyItem( this, this.getCount(), 0, 'L2ItemInstance.decreaseMana' )
            }

            return ItemManagerCache.destroyItem( this )
        }

        if ( this.isEquipped() ) {
            this.scheduleConsumeManaTask()
        } else {
            this.stopConsumeManaTask()
        }

        if ( isInPlayerInventory ) {
            player.getInventory().markItem( this, InventoryUpdateStatus.Modified )
        }
    }

    resetProperties() : void {
        this.resetLifetimeTask()
        if ( this.publishedDataTask ) {
            clearTimeout( this.publishedDataTask )
        }

        this.publishedDataTask = null
        this.publishedData = null

        ItemProtectionCache.removeProtection( this.getObjectId() )
    }

    resetLifetimeTask() {
        if ( this.lifeTimeTask ) {
            clearTimeout( this.lifeTimeTask )
        }

        this.lifeTimeTask = null
    }

    dropMe( ownerObjectId: number, x: number, y: number, z: number, instanceId: number, autoDestroy: boolean ): void {
        this.setInstanceId( instanceId )
        this.setIsVisible( true )
        this.setXYZ( x, y, GeoPolygonCache.getZ( x, y, z ) )
        this.setDropTime( Date.now() )
        this.setOwnerId( ownerObjectId )

        ItemsOnGroundManager.save( this )

        this.updateSpatialIndex()

        if ( autoDestroy ) {
            ItemManagerCache.autoDestroyItem( this )
        }

        /*
            TODO : increase radius to region size to increase item visibility
            - in case of proximal discovery rework based on movement
         */
        L2World.getAllVisiblePlayers( this, 500 ).forEach( ( player: L2PcInstance ) => {
            ProximalDiscoveryManager.addKnownObject( player, this )
        } )
    }

    async endOfLife(): Promise<void> {
        this.resetLifetimeTask()

        let player: L2PcInstance = this.getActingPlayer()
        if ( !player ) {
            return
        }

        if ( this.isEquipped() ) {
            await player.getInventory().unEquipItemInSlot( this.getLocationSlot() )
            player.broadcastUserInfo()
        }

        if ( this.getItemLocation() !== ItemLocation.WAREHOUSE ) {
            await player.getInventory().destroyItem( this, this.getCount(), 0, 'L2ItemInstance.endOfLife' )
        } else {
            // TODO : inspect use cases of warehouse items and use alternative logic without loading whole warehouse
            // also, consider multiple player warehouses and how current use of single warehouse be impacted (possible no item would be deleted)
            await player.initializeWarehouse()
            await player.getWarehouse().destroyItem( this, this.getCount(), 0, 'L2ItemInstance.endOfLife' )
        }

        if ( ListenerCache.hasItemTemplateEvent( this.getId(), EventType.ItemEndOfLife ) ) {

            let eventData = EventPoolCache.getData( EventType.ItemEndOfLife ) as ItemEndOfLifeEvent

            eventData.objectId = this.getObjectId()
            eventData.itemId = this.getItem().getId()
            eventData.playerId = player.getObjectId()

            await ListenerCache.sendItemTemplateEvent( this.getId(), EventType.ItemEndOfLife, eventData )
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TIME_LIMITED_ITEM_DELETED ) )

        return ItemManagerCache.destroyItem( this )
    }

    getAgathionRemainingEnergy() {
        return this.agathionEnergy
    }

    getAttackElementPower() : number {
        let value = this.item.getAttackElementPower()
        if ( value !== null ) {
            return value
        }

        if ( this.elementals.length > 0 ) {
            return this.elementals[ 0 ].getValue()
        }

        return L2ItemDefaults.AttackElementPower
    }

    getAttackElementType() : number {
        let value = this.item.getAttackElementType()
        if ( value !== null ) {
            return value
        }

        if ( this.elementals.length > 0 ) {
            return this.elementals[ 0 ].getElement()
        }

        return L2ItemDefaults.AttackElementType
    }

    getAugmentation(): L2Augmentation {
        return this.augmentation
    }

    getCount(): number {
        return this.count
    }

    getCustomType1() {
        return this.type1
    }

    getCustomType2() {
        return this.type2
    }

    getDropTime() {
        return this.dropTime
    }

    private getElementDefenceAttribute( index: ElementalType ): number {
        let value = this.item.getDefenceElementAttribute( index )
        if ( value !== null ) {
            return value
        }

        if ( this.elementals.length > 0 ) {
            let data: Elementals = this.getElemental( index )
            if ( data ) {
                return data.getValue()
            }
        }

        return L2ItemDefaults.DefenceElementAttribute
    }

    getElemental( type: ElementalType ) : Elementals {
        if ( this.elementals.length === 0 ) {
            return null
        }

        return this.elementals.find( ( currentItem: Elementals ) : boolean => currentItem.getElement() === type )
    }

    getElementals() : ReadonlyArray<Elementals> {
        return this.elementals
    }

    getEnchantLevel(): number {
        return this.enchantLevel
    }

    getEnchantOptions(): Readonly<EnchantOptionsParameters> {
        return this.enchantOptions
    }

    private computeEnchantOptions() : void {
        this.enchantOptions = getEnchantOptions( this.itemId, this.enchantLevel )
    }

    getEquipReuseDelay() {
        return this.item.getEquipReuseDelay()
    }

    getEtcItem(): L2EtcItem {
        if ( this.isEtcItem() ) {
            return this.item as L2EtcItem
        }

        return null
    }

    getId() {
        return this.itemId
    }

    isItem(): boolean {
        return true
    }

    setChargedShot( type: ShotType, isCharged: boolean ): void {
        if ( isCharged ) {
            this.shotsMask |= ShotTypeHelper.getMask( type )
        } else {
            this.shotsMask &= ~ShotTypeHelper.getMask( type )
        }
    }

    getItem(): L2Item {
        return this.item
    }

    getItemLocation() {
        return this.location
    }

    getItemType(): number {
        return this.item.getItemType()
    }

    getInventoryStatus(): InventoryUpdateStatus {
        return this.lastChange
    }

    resetInventoryStatus() : void {
        this.lastChange = InventoryUpdateStatus.Unchanged
    }

    getLocationData() {
        return this.inventorySlot
    }

    getLocationSlot() : InventorySlot {
        return this.inventorySlot
    }

    getMana() {
        return this.remainingManaPoints
    }

    getOlympiadEnchantLevel(): number {
        let player = this.getActingPlayer()
        let enchantLevel = this.getEnchantLevel()

        if ( !player ) {
            return enchantLevel
        }

        let enchantLimit = ConfigManager.olympiad.getEnchantLimit()
        if ( player.isInOlympiadMode() && enchantLimit >= 0 && enchantLevel > enchantLimit ) {
            enchantLevel = enchantLimit
        }

        return enchantLevel
    }

    getOwnerId() {
        return this.ownerId
    }

    getReferencePrice() {
        return this.item.getReferencePrice()
    }

    getRemainingTime(): number {
        return this.remainingLifetime - Date.now()
    }

    getRemainingExpirationSeconds() : number {
        return this.item.getTime() === -1 ? -9999 : Math.floor( this.getRemainingTime() / 1000 )
    }

    getReuseDelay() {
        return this.item.getReuseDelay()
    }

    getSharedReuseGroup() {
        return this.item.getSharedReuseGroup()
    }

    getStatFunctions(): ReadonlyArray<AbstractFunction> {
        if ( !this.statFunctions ) {
            this.statFunctions = this.item.generateStatFunctions( this )
        }

        return this.statFunctions
    }

    getTime() {
        return this.remainingLifetime
    }

    getWeaponItem(): L2Weapon {
        if ( this.item.isInstanceType( ItemInstanceType.L2Weapon ) ) {
            return this.item as L2Weapon
        }

        return null
    }

    giveSkillsToOwner(): Promise<Array<Skill>> {
        let player = this.getActingPlayer()
        if ( !player ) {
            return
        }

        let promises : Array<Promise<Skill>> = []
        this.getItem().getSkills().forEach( ( skill: Skill ) => {
            if ( skill.isPassive() ) {
                promises.push( player.addSkill( skill, false ) )
            }
        } )

        if ( promises.length > 0 ) {
            return Promise.all( promises )
        }
    }

    hasRuneSkills() : boolean {
        return this.getItemType() === EtcItemType.RUNE
            && this.getItemLocation() === ItemLocation.INVENTORY
            && this.getOwnerId() > 0
            && this.getItem().hasPassiveSkills()
    }

    isArmor(): boolean {
        return this.item.isArmorItem()
    }

    isAugmented() {
        return !!this.augmentation
    }

    isAvailable( player: L2PcInstance, allowAdena: boolean, allowNonTradeable: boolean ) {
        return !this.isEquipped() // Not equipped
                && ( this.getItem().getType2() !== ItemType2.QUEST )
                && ( ( this.getItem().getType2() !== ItemType2.MONEY ) || ( this.getItem().getType1() !== ItemType1.SHIELD_ARMOR ) )
                && ( !player.hasSummon() || ( this.getObjectId() !== player.getSummon().getControlObjectId() ) )
                && ( player.getActiveEnchantItemId() !== this.getObjectId() ) // Not momentarily used enchant scroll
                && ( player.getActiveEnchantSupportItemId() !== this.getObjectId() ) // Not momentarily used enchant support item
                && ( player.getActiveEnchantAttributeItemId() !== this.getObjectId() ) // Not momentarily used enchant attribute item
                && ( allowAdena || ( this.getId() !== ItemTypes.Adena ) )
                && ( !player.isCastingNow() || ( player.getMainCast().skill.getItemConsumeId() !== this.getId() ) )
                && ( !player.isCastingSimultaneouslyNow() || ( player.getSimultaneousCast().skill.getItemConsumeId() !== this.getId() ) )
                && ( allowNonTradeable || ( this.isTradeable() && ( !( this.getItem().getItemType() === EtcItemType.PET_COLLAR && player.havePetInventoryItems() ) ) ) )
    }

    isDropable() {
        return !this.isAugmented() && this.item.isDropable()
    }

    isEquipable() {
        return !( this.item.getBodyPart() === 0
                || ( this.isEtcItem() && ( ( this.item.getItemType() === EtcItemType.ARROW )
                        || ( this.item.getItemType() === EtcItemType.BOLT )
                        || ( this.item.getItemType() === EtcItemType.LURE ) ) ) )
    }

    isEquipped(): boolean {
        return ( ItemLocation.PAPERDOLL === this.location ) || ( ItemLocation.PET_EQUIP === this.location )
    }

    isEtcItem() {
        return this.item.isEtcItem()
    }

    isFreightable() {
        return this.getItem().isFreightable()
    }

    isHeroItem() {
        return this.item.isHeroItem()
    }

    isNightLure() {
        return ( this.itemId >= 8505 && this.itemId <= 8513 ) || this.itemId === 8485
    }

    isQuestItem() {
        return this.getItem().isQuestItem()
    }

    isShadowItem(): boolean {
        return this.remainingManaPoints >= 0
    }

    isStackable(): boolean {
        return this.item.isStackable()
    }

    isTimeLimitedItem(): boolean {
        return this.remainingLifetime > 0
    }

    isTradeable() {
        return !this.isAugmented() && this.item.isTradeable()
    }

    isWeapon(): boolean {
        return this.item.isWeaponItem()
    }

    removeElementAttributesBonus( player: L2PcInstance ) {
        this.elementals.forEach( ( element: Elementals ) => {
            element.removeBonus( player )
        } )
    }

    removeSkillsFromOwner(): Promise<Array<Skill>> {
        if ( !this.hasRuneSkills() ) {
            return
        }

        let player = this.getActingPlayer()
        if ( !player ) {
            return
        }

        let promises : Array<Promise<Skill>> = []
        this.getItem().getSkills().forEach( ( skill: Skill ) => {
            if ( skill.isPassive() ) {
                promises.push( player.removeSkill( skill, false, true ) )
            }
        } )

        if ( promises.length > 0 ) {
            return Promise.all( promises )
        }
    }

    async restoreElementals(): Promise<void> {
        let items = await DatabaseManager.getItemElementalsTable().getAttributes( this.getObjectId() )
        if ( !items || items.length === 0 ) {
            return
        }

        items.forEach( ( data ) => {
            if ( data.type !== -1 && data.value !== -1 ) {
                this.applyAttribute( data.type, data.value )
            }
        } )

        this.computeElementalDefenceAttributes()
    }

    scheduleConsumeManaTask() {
        if ( this.consumingManaTimeout ) {
            return
        }

        let interval = Math.min( 10000, ConfigManager.tuning.getShadowItemManaConsumeInterval() )
        this.consumingManaTimeout = setTimeout( this.decreaseMana.bind( this ), interval )
    }

    scheduleLifeTimeTask(): void {
        if ( !this.isTimeLimitedItem() ) {
            return
        }

        if ( this.getRemainingTime() <= 0 ) {
            this.endOfLife()
            return
        }

        this.resetLifetimeTask()

        let remainingMs = this.getRemainingTime()
        if ( remainingMs < MsInDays.One ) {
            this.lifeTimeTask = setTimeout( this.endOfLife.bind( this ), remainingMs )
            return
        }

        this.lifeTimeTask = setTimeout( this.scheduleLifeTimeTask.bind( this ), MsInDays.One )
    }

    setAgathionRemainingEnergy( amount: number ) {
        this.agathionEnergy = amount
    }

    setCount( amount: number ): void {
        this.count = Math.max( 0, amount )
    }

    setDropTime( value: number ) {
        this.dropTime = value
    }

    setElementAttribute( element: ElementalType, value: number ) : void {
        this.applyAttribute( element, value )
        this.computeElementalDefenceAttributes()
    }

    async setEnchantLevel( value: number ) : Promise<void> {
        if ( this.enchantLevel === value ) {
            return
        }

        this.enchantLevel = value
        this.computeEnchantOptions()

        if ( this.hasEnchantStats() ) {
            await this.clearEnchantStats()
        }

        if ( this.shouldApplyEnchantStats() ) {
            await this.applyEnchantStats()
        }

        return this.scheduleDatabaseUpdate()
    }

    async setItemLocationForNewOwner( location: ItemLocation, inventorySlot: number = 0 ): Promise<void> {
        if ( location === this.location && inventorySlot === this.inventorySlot ) {
            return
        }

        if ( this.hasRuneSkills() && this.getActingPlayer() ) {
            await this.removeSkillsFromOwner()
            await this.giveSkillsToOwner()
        }

        return this.setItemLocationProperties( location, inventorySlot )
    }

    setItemLocationProperties( location: ItemLocation, inventorySlot: number = 0 ) : void {
        if ( location === this.location && inventorySlot === this.inventorySlot ) {
            return
        }

        this.location = location
        this.inventorySlot = inventorySlot
        this.setInventoryStatus( InventoryUpdateStatus.Modified )
    }

    setInventoryStatus( value: InventoryUpdateStatus ) {
        this.lastChange = value
    }

    async setOwnership( ownerId: number, location: ItemLocation ): Promise<void> {
        if ( this.hasRuneSkills() ) {
            await this.removeSkillsFromOwner()
        }

        this.setOwnershipDirect( ownerId, location )

        if ( this.hasRuneSkills() ) {
            await this.giveSkillsToOwner()
        }
    }

    setOwnershipDirect( ownerId: number, location: ItemLocation ) : void {
        this.ownerId = ownerId
        this.location = location
        this.inventorySlot = 0
    }

    async resetOwnership(): Promise<void> {
        if ( this.ownerId === 0 ) {
            return
        }

        if ( this.hasRuneSkills() ) {
            await this.removeSkillsFromOwner()
        }

        this.location = ItemLocation.VOID
        this.inventorySlot = 0
    }

    setOwnerId( ownerId: number ): void {
        if ( this.ownerId === ownerId ) {
            return
        }

        this.ownerId = ownerId
    }

    setDeletionProtected( value: boolean ) {
        this.protected = value
    }

    unChargeAllShots() {
        this.shotsMask = 0
    }

    updateDatabase(): Promise<void> {
        ItemManagerCache.unScheduleItemUpdate( this )

        switch ( this.getUpdateStatus() ) {
            case L2ItemUpdateStatus.Delete:
                return ItemManagerCache.deleteItems( [ this ] )

            case L2ItemUpdateStatus.Update:
                return ItemManagerCache.updateItems( [ this ] )

            case L2ItemUpdateStatus.Create:
                return ItemManagerCache.insertItems( [ this ] )
        }
    }

    scheduleDatabaseUpdate(): void {
        ItemManagerCache.scheduleItemUpdate( this )
    }

    getUpdateStatus(): L2ItemUpdateStatus {
        if ( this.shouldDelete() ) {
            return L2ItemUpdateStatus.Delete
        }

        if ( this.shouldCreate() ) {
            return L2ItemUpdateStatus.Create
        }

        if ( this.shouldUpdate() ) {
            return L2ItemUpdateStatus.Update
        }

        return L2ItemUpdateStatus.Undecided
    }

    updateElementAttributesBonus( player: L2PcInstance ): void {
        if ( this.elementals.length === 0 ) {
            return
        }

        let stats : Array<AbstractFunction> = []

        for ( const element of this.elementals ) {
            element.removeBonus( player )
            let stat = element.getStat()
            if ( stat ) {
                stats.push( stat )
            }
        }

        return player.addStatFunctions( stats, true )
    }

    getManaConsumeOnSkill() {
        return this.getItem().manaConsumeOnSkill
    }

    shouldCreate() {
        return !this.existsInDb && !( this.ownerId === 0
                || this.location === ItemLocation.VOID
                || this.location === ItemLocation.REFUND
                || ( this.getCount() === 0 && this.location !== ItemLocation.LEASE ) )
    }

    shouldDelete() {
        return this.existsInDb && ( this.ownerId === 0
                || this.location === ItemLocation.VOID
                || this.location === ItemLocation.REFUND
                || ( this.getCount() === 0 && this.location !== ItemLocation.LEASE ) )
    }

    shouldUpdate() {
        return this.existsInDb && this.getInventoryStatus() !== InventoryUpdateStatus.Unchanged
    }

    getCrystalCount() {
        return this.item.getCrystalCount( this.enchantLevel )
    }

    isSellable() {
        return !this.isAugmented() && this.item.isSellable()
    }

    isDepositable( isPrivateWarehouse: boolean ) {
        if ( this.isEquipped() || !this.item.isDepositable() ) {
            return false
        }

        if ( !isPrivateWarehouse ) {
            return this.isTradeable() && !this.isShadowItem()
        }

        return true
    }

    isEnchantable(): boolean {
        if ( ItemLocation.INVENTORY === this.getItemLocation() || ItemLocation.PAPERDOLL === this.getItemLocation() ) {
            return this.getItem().isEnchantable()
        }

        return false
    }

    isDestroyable(): boolean {
        return this.item.isDestroyable()
    }

    getActingPlayer(): L2PcInstance {
        return L2World.getPlayer( this.getOwnerId() )
    }

    setAugmentation( augmentation: L2Augmentation ): void {
        if ( this.augmentation ) {
            return
        }

        this.augmentation = augmentation
        this.scheduleDatabaseUpdate()
    }

    isPublished() {
        return !!this.publishedData
    }

    clearElementAttribute( element: number ) : void {
        if ( element === -1 ) {
            this.elementals = []
            return this.scheduleDatabaseUpdate()
        }

        if ( this.elementals.length === 0 && !this.getElemental( element ) ) {
            return
        }

        this.elementals = this.elementals.filter( ( elemental: Elementals ) => {
            return elemental.getElement() !== element
        } )

        return this.scheduleDatabaseUpdate()
    }

    isCommonItem(): boolean {
        return this.item.isCommon()
    }

    isPvp() {
        return this.item.isPvpItem()
    }

    canEnchantElemental() {
        if ( ( this.getItemLocation() === ItemLocation.INVENTORY ) || ( this.getItemLocation() === ItemLocation.PAPERDOLL ) ) {
            return this.getItem().isElementable()
        }

        return false
    }

    removeAugmentation(): void {
        if ( !this.augmentation ) {
            return
        }

        this.augmentation = null
        this.scheduleDatabaseUpdate()
    }

    decayMe(): void {
        ItemsOnGroundManager.removeObject( this )
        L2World.removeObject( this )
        super.decayMe()
    }

    isPotion(): boolean {
        return this.item.isPotion()
    }

    isElixir(): boolean {
        return this.item.isElixir()
    }

    isScroll(): boolean {
        return this.item.isScroll()
    }

    onBypassFeedback( player: L2PcInstance, command: string ): Promise<void> {
        let name = command.substring( 6 )
        let index = name.indexOf( ' ' )
        if ( index > 0
                && name.substring( index ).trim()
                && ListenerCache.hasItemTemplateEvent( this.getId(), EventType.ItemBypass ) ) {
            let eventData = EventPoolCache.getData( EventType.ItemBypass ) as ItemBypassEvent

            eventData.objectId = this.getObjectId()
            eventData.playerId = player.getObjectId()
            eventData.eventName = command
            eventData.itemId = this.getId()
            eventData.isArmor = this.isArmor()
            eventData.isWeapon = this.isWeapon()
            eventData.isEtc = this.isEtcItem()

            return ListenerCache.sendItemTemplateEvent( this.getId(), EventType.ItemBypass, eventData )
        }

        if ( ListenerCache.hasItemTemplateEvent( this.getId(), EventType.ItemTalking ) ) {
            let eventData = EventPoolCache.getData( EventType.ItemTalking ) as ItemTalkingEvent

            eventData.objectId = this.getObjectId()
            eventData.playerId = player.getObjectId()
            eventData.itemId = this.getId()
            eventData.isArmor = this.isArmor()
            eventData.isWeapon = this.isWeapon()
            eventData.isEtc = this.isEtcItem()

            return ListenerCache.sendItemTemplateEvent( this.getId(), EventType.ItemTalking, eventData )
        }
    }

    shouldDescribeState( player: L2PcInstance ): boolean {
        if ( this.ownerId !== 0 ) {
            player.sendOwnedData( DropItem( this, this.ownerId ) )
        } else {
            player.sendOwnedData( SpawnItem( this ) )
        }

        return false
    }

    async pickupMeBy( character: L2Character ): Promise<void> {
        L2World.removeSpatialIndex( this )

        BroadcastHelper.dataToSelfBasedOnVisibility( character, PickItemUp( this, character.getObjectId() ) )

        let deletePacket = DeleteObject( this.getObjectId() )

        /*
            We must send out packet informing client to erase visible object. However, since
            proximal discovery can also determine to delete object, same packet can be sent twice.
            Moreover, it is possible for item to be dropped immediately causing proximal discovery
            to ignore item for updates (makes item invisible to client).
         */
        L2World.getAllVisiblePlayerIds( character, L2Region.Length ).forEach( ( playerId: number ) => {
            PacketDispatcher.sendCopyData( playerId, deletePacket )
            ProximalDiscoveryManager.forgetObjectId( playerId, this.getObjectId() )
        } )

        if ( MercenaryTicketManager.getTicketCastleId( this.getId() ) > 0 ) {
            MercenaryTicketManager.removeTicket( this )
        }

        ItemProtectionCache.removeProtection( this.getObjectId() )
        this.setIsVisible( false )

        if ( !character.isPlayer() && !character.isSummon() ) {
            return
        }

        if ( ListenerCache.hasItemTemplateEvent( this.getId(), EventType.PlayerPickupItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerPickupItem ) as PlayerPickupItemEvent

            eventData.playerId = character.getActingPlayerId()
            eventData.itemObjectId = this.getObjectId()
            eventData.itemId = this.getId()
            eventData.amount = this.getCount()

            await ListenerCache.sendItemTemplateEvent( this.getId(), EventType.PlayerPickupItem, eventData )
        }

        /*
            Used exclusively by Tutorial quest
         */
        if ( ListenerCache.hasObjectListener( character.getActingPlayerId(), EventType.PlayerPickupItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerPickupItem ) as PlayerPickupItemEvent

            eventData.playerId = character.getActingPlayerId()
            eventData.itemObjectId = this.getObjectId()
            eventData.itemId = this.getItem().getId()
            eventData.amount = this.getCount()

            await ListenerCache.sendObjectEvent( character.getActingPlayerId(), EventType.PlayerPickupItem, eventData, this.getId() )
        }
    }

    isChargedShot( type: ShotType ): boolean {
        let mask = ShotTypeHelper.getMask( type )
        return ( this.shotsMask & mask ) === mask
    }

    getDefaultEnchantLevel() {
        return this.item.getDefaultEnchantLevel()
    }

    setCustomType1( value: number ): void {
        this.type1 = value
    }

    setCustomType2( value: number ): void {
        this.type2 = value
    }

    markPublished() {
        this.publishedData = PacketHelper.preservePacket( ExRpItemLink( this ) )
        this.publishedDataTask = setTimeout( this.clearPublishedData.bind( this ), MsInMinutes.Fifteen )
    }

    isDeletionProtected(): boolean {
        return this.protected
    }

    clearPublishedData(): void {
        this.publishedData = null
    }

    getPublishedData(): Buffer {
        return this.publishedData
    }

    canBePickedUpBy( player: L2PcInstance ): boolean {
        if ( player.getInstanceId() !== this.getInstanceId() ) {
            return false
        }

        if ( !this.ownerId || this.ownerId === player.getObjectId() || player.hasActionOverride( PlayerActionOverride.ItemAction ) ) {
            return true
        }

        if ( player.isGM() ) {
            return true
        }

        return player.isInLooterParty( this.ownerId ) || !ItemProtectionCache.isProtected( this.getObjectId() )
    }

    setEnchantLevelDirect( level: number ) : void {
        this.enchantLevel = level
    }

    isBolt() : boolean {
        return this.item.isEtcItem() && ( this.item as L2EtcItem ).getItemType() === EtcItemType.BOLT
    }

    isArrow() : boolean {
        return this.item.isEtcItem() && ( this.item as L2EtcItem ).getItemType() === EtcItemType.ARROW
    }

    markDestroyed() : void {
        this.existsInDb = true
        this.location = ItemLocation.VOID
        this.scheduleDatabaseUpdate()
    }

    private stopConsumeManaTask() : void {
        if ( this.consumingManaTimeout ) {
            clearInterval( this.consumingManaTimeout )
            this.consumingManaTimeout = null
        }
    }

    private computeElementalDefenceAttributes() : void {
        if ( !this.isArmor() ) {
            this.elementalDefenceAttributes = DefaultDefenceElementAttributes
            return
        }

        this.elementalDefenceAttributes = DefenceElementTypes.map( ( element: ElementalType ) => this.getElementDefenceAttribute( element ) ?? L2ItemDefaults.DefenceElementAttribute )
    }
}