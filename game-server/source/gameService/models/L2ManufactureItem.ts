import { DataManager } from '../../data/manager'

export function createManufactureItem( recipeId: number, adenaCost: number ) : L2ManufactureItem {
    return {
        recipeId,
        adenaCost,
        isDwarven: DataManager.getRecipeData().getRecipeList( recipeId ).isDwarvenRecipe
    }
}

export interface L2ManufactureItem {
    recipeId: number
    adenaCost: number
    isDwarven: boolean
}