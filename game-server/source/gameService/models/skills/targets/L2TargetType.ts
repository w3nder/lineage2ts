import { Skill } from '../../Skill'
import { L2Character } from '../../actor/L2Character'
import { L2Object } from '../../L2Object'
import { AdvanceBase } from './verifiers/AdvanceBase'
import { Artillery } from './verifiers/Artillery'
import { DoorOrTreasureChest } from './verifiers/DoorOrTreasureChest'
import { AnyEnemy } from './verifiers/AnyEnemy'
import { NotEnemy } from './verifiers/NotEnemy'
import { OnlyEnemy } from './verifiers/OnlyEnemy'
import { FortressFlagpole } from './verifiers/FortressFlagpole'
import { Ground } from './verifiers/Ground'
import { HolySiegeArtifact } from './verifiers/HolySiegeArtifact'
import { Item } from './verifiers/Item'
import { NpcCorpse } from './verifiers/NpcCorpse'
import { AllOthers } from './verifiers/AllOthers'
import { PlayerCorpse } from './verifiers/PlayerCorpse'
import { Servitor } from './verifiers/Servitor'
import { Target } from './verifiers/Target'
import { WyvernTarget } from './verifiers/WyvernTarget'

export enum L2TargetType {
    /** Advance Head Quarters (Outposts). */
    ADVANCE_BASE,
    /** Enemies in high terrain or protected by castle walls and doors. */
    ARTILLERY,
    /** Doors or treasure chests. */
    DOOR_TREASURE,
    /** Any enemies (included allies). */
    ENEMY,
    /** Friendly. */
    ENEMY_NOT,
    /** Only enemies (not included allies). */
    ENEMY_ONLY,
    /** Fortress's Flagpole. */
    FORTRESS_FLAGPOLE,
    /** Ground. */
    GROUND,
    /** Holy Artifacts from sieges. */
    HOLYTHING,
    /** Items. */
    ITEM,
    /** Nothing. */
    NONE,
    /** NPC corpses. */
    NPC_BODY,
    /** Others, except caster. */
    OTHERS,
    /** Player corpses. */
    PC_BODY,
    /** Self. */
    SELF,
    /** Servitor, not pet. */
    SUMMON,
    /** Anything targetable. */
    TARGET,
    /** Wyverns. */
    WYVERN_TARGET,

    AREA,

    AREA_CORPSE_MOB,

    AREA_FRIENDLY,

    AREA_SUMMON,

    AREA_UNDEAD,

    AURA,

    AURA_CORPSE_MOB,

    AURA_FRIENDLY,

    AURA_UNDEAD_ENEMY,

    BEHIND_AREA,

    BEHIND_AURA,

    CLAN,

    CLAN_MEMBER,

    COMMAND_CHANNEL,

    CORPSE,

    CORPSE_CLAN,

    CORPSE_MOB,

    ENEMY_SUMMON,

    FLAGPOLE,

    FRONT_AREA,

    FRONT_AURA,

    HOLY,

    ONE,

    OWNER_PET,

    PARTY,

    PARTY_CLAN,

    PARTY_MEMBER,

    PARTY_NOTME,

    PARTY_OTHER,

    PET,

    SERVITOR,

    TARGET_PARTY,

    UNDEAD,

    UNLOCKABLE
}

export type TargetVerifierMethodType = ( skill : Skill, caster: L2Character, target: L2Object ) => L2Object
export const ChooseCaster : TargetVerifierMethodType = ( skill : Skill, caster: L2Character ) : L2Object => caster

export const L2TargetTypeVerifiers : {[ type: number ] : TargetVerifierMethodType } = {
    /** Advance Head Quarters/Outposts */
    [ L2TargetType.ADVANCE_BASE ]: AdvanceBase,

    /** Enemies in high terrain or protected by castle walls and doors */
    [ L2TargetType.ARTILLERY ]: Artillery,

    /** Doors or treasure chests */
    [ L2TargetType.DOOR_TREASURE ]: DoorOrTreasureChest,

    /** Any enemies (included allies) */
    [ L2TargetType.ENEMY ]: AnyEnemy,

    /** Only Friendly allies */
    [ L2TargetType.ENEMY_NOT ]: NotEnemy,

    /** Only enemies (not included allies) */
    [ L2TargetType.ENEMY_ONLY ]: OnlyEnemy,

    /** Fortress's Flagpole */
    [ L2TargetType.FORTRESS_FLAGPOLE ]: FortressFlagpole,

    /** Ground */
    [ L2TargetType.GROUND ]: Ground,

    /** Holy Artifacts from sieges. */
    [ L2TargetType.HOLYTHING ]: HolySiegeArtifact,

    /** Any Item */
    [ L2TargetType.ITEM ]: Item,

    /** Nothing, meaning it is caster. */
    [ L2TargetType.NONE ]: ChooseCaster,

    /** NPC corpses */
    [ L2TargetType.NPC_BODY ]: NpcCorpse,

    /** Everybody except caster */
    [ L2TargetType.OTHERS ]: AllOthers,

    /** Player corpses. */
    [ L2TargetType.PC_BODY ]: PlayerCorpse,

    /** Caster as target */
    [ L2TargetType.SELF ]: ChooseCaster,

    /** Servitor only */
    [ L2TargetType.SUMMON ]: Servitor,
    [ L2TargetType.SERVITOR ]: Servitor,

    /** Anything targetable. */
    [ L2TargetType.TARGET ]: Target,

    /** Wyverns */
    [ L2TargetType.WYVERN_TARGET ]: WyvernTarget,

    /** should no longer be used, clean up when possible */
    [ L2TargetType.ONE ]: Target,
    [ L2TargetType.PARTY ]: ChooseCaster
}