import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2Object } from '../../../L2Object'
import { L2World } from '../../../../L2World'
import { AffectSelectionMethodType } from '../AffectSelectionMethods'

export const Square: AffectSelectionMethodType = ( caster: L2Character, target: L2Character, skill: Skill ): Array<L2Object> => {
    let affectLimit = skill.getAffectLimit()
    let targets: Array<L2Object> = []

    if ( affectLimit <= 0 ) {
        return targets
    }

    L2World.getVisibleCharacters( target, skill.getAffectRange(), true ).some( ( character: L2Character ) => {
        if ( !skill.affectObjectMethod( caster, character ) ) {
            return false
        }

        targets.push( character )

        return targets.length >= affectLimit
    } )

    return targets
}