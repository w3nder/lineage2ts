import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2Object } from '../../../L2Object'
import { Party } from './Party'
import { Pledge } from './Pledge'
import { AffectSelectionMethodType } from '../AffectSelectionMethods'
import _ from 'lodash'

export const PartyPledge: AffectSelectionMethodType = ( caster: L2Character, target: L2Object, skill: Skill ): Array<L2Object> => {
    let targetList = [
        Party( caster, target, skill ),
        Pledge( caster, target, skill ),
    ]

    return _.uniq( _.flatten( targetList ) )
}