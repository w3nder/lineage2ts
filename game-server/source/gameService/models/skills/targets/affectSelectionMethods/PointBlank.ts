import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2Object } from '../../../L2Object'
import { L2World } from '../../../../L2World'
import { AffectSelectionMethodType } from '../AffectSelectionMethods'

export const PointBlank: AffectSelectionMethodType = ( caster: L2Character, target: L2Object, skill: Skill ): Array<L2Object> => {
    let affectLimit = skill.getAffectLimit()
    let targets: Array<L2Object> = []

    // TODO : consider using knn to get progressively further targets
    // TODO : consider applying same concept to other affect selection methods
    L2World.getVisibleCharacters( caster, skill.getAffectRange() ).some( ( object: L2Character ) : boolean => {
        if ( skill.affectObjectMethod( caster, object ) ) {
            targets.push( object )
        }

        return affectLimit > 0 && targets.length >= affectLimit
    } )

    return targets
}