import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2Object } from '../../../L2Object'
import { L2PcInstance } from '../../../actor/instance/L2PcInstance'
import { L2World } from '../../../../L2World'
import { TvTEvent } from '../../../entity/TvTEvent'
import { AffectSelectionMethodType } from '../AffectSelectionMethods'
import _ from 'lodash'

export const DeadPledge: AffectSelectionMethodType = ( character: L2Character, target: L2Object, skill: Skill ): Array<L2Object> => {
    if ( !character.isPlayable() ) {
        return
    }

    let player: L2PcInstance = character.getActingPlayer()
    let clanId: number = player.getClanId()
    if ( clanId === 0 ) {
        return
    }

    let limit = skill.getAffectLimit()
    let targets: Array<L2Object> = []
    let inDuel: boolean = player.isInDuel()

    _.each( L2World.getVisibleObjects( target, skill.getAffectRange() ), ( object: L2Object ) => {
        if ( !object.isPlayable() ) {
            return
        }

        let targetPlayer: L2PcInstance = object.getActingPlayer()
        if ( !targetPlayer ) {
            return
        }

        if ( clanId !== targetPlayer.getClanId() ) {
            return
        }

        if ( inDuel ) {
            if ( player.getDuelId() !== targetPlayer.getDuelId() ) {
                return
            }

            if ( player.isInParty() && targetPlayer.isInParty() && player.getParty().getLeaderObjectId() !== targetPlayer.getParty().getLeaderObjectId() ) {
                return
            }
        }

        if ( !player.checkPvpSkill( targetPlayer, skill ) ) {
            return
        }

        if ( !TvTEvent.checkForTvTSkill( player, targetPlayer, skill ) ) {
            return
        }

        if ( player.isInOlympiadMode() ) {
            if ( player.getOlympiadGameId() !== targetPlayer.getOlympiadGameId() ) {
                return
            }

            if ( player.getOlympiadSide() !== targetPlayer.getOlympiadGameId() ) {
                return
            }
        }

        if ( targetPlayer.isInSiegeArea() && !targetPlayer.isInSiege() ) {
            return
        }

        if ( !skill.affectObjectMethod( player, targetPlayer ) ) {
            return
        }

        targets.push( targetPlayer )

        if ( limit > 0 && targets.length >= limit ) {
            return false
        }
    } )

    return targets
}