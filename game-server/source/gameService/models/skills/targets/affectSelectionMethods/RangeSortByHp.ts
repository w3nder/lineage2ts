import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2Object } from '../../../L2Object'
import { L2World } from '../../../../L2World'
import { AffectSelectionMethodType } from '../AffectSelectionMethods'
import _ from 'lodash'

export const RangeSortByHp: AffectSelectionMethodType = ( caster: L2Character, target: L2Object, skill: Skill ): Array<L2Object> => {
    let targets: Array<L2Character> = L2World.getVisibleObjectsByPredicate( target, skill.getAffectRange(), ( character: L2Character ) => {
        return character.isCharacter() && !character.isDead() && skill.affectObjectMethod( caster, character )
    },true ).sort( compareHpValues ) as Array<L2Character>

    let limit = skill.getAffectLimit()

    if ( limit > 0 ) {
        return _.take( targets, limit )
    }

    return targets
}

function compareHpValues( one: L2Character, two: L2Character ): number {
    let valueOne = one.getCurrentHp() / one.getMaxHp()
    let valueTwo = two.getCurrentHp() / two.getMaxHp()

    if ( valueOne === valueTwo ) {
        return 0
    }

    if ( valueOne < valueTwo ) {
        return -1
    }

    return 1
}