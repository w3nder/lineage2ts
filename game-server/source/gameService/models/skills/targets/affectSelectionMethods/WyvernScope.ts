import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2Object } from '../../../L2Object'
import { L2World } from '../../../../L2World'
import { L2Npc } from '../../../actor/L2Npc'
import { AffectSelectionMethodType } from '../AffectSelectionMethods'

export const WyvernScope: AffectSelectionMethodType = ( caster: L2Character, target: L2Object, skill: Skill ): Array<L2Object> => {
    let affectLimit = skill.getAffectLimit()
    let targets: Array<L2Object> = []

    L2World.getVisibleNpcs( caster, skill.getAffectRange(), false ).some( ( npc: L2Npc ) => {
        // Wyvern npc
        if ( npc.getId() !== 12621 || !skill.affectObjectMethod( caster, npc ) ) {
            return false
        }

        targets.push( npc )

        return affectLimit > 0 && targets.length >= affectLimit
    } )

    return targets
}