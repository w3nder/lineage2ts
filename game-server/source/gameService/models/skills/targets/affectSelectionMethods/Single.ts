import { L2Character } from '../../../actor/L2Character'
import { Skill } from '../../../Skill'
import { L2Object } from '../../../L2Object'
import { AffectSelectionMethodType } from '../AffectSelectionMethods'

export const Single: AffectSelectionMethodType = ( caster: L2Character, target: L2Object, skill: Skill ): Array<L2Object> => {
    if ( !skill.affectObjectMethod( caster, target ) ) {
        return []
    }

    return [ target ]
}