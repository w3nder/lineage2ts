import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { AffectObjectMethodType } from '../AffectMethodRegistry'

export const Friend: AffectObjectMethodType = ( caster: L2Character, object: L2Object ): boolean => {
    return !object.isAutoAttackable( caster )
}