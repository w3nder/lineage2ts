import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { AffectObjectMethodType } from '../AffectMethodRegistry'

export const NotFriend: AffectObjectMethodType = ( caster: L2Character, object: L2Object ): boolean => {
    if ( object.isCharacter() ) {
        if ( ( object as L2Character ).isDead() ) {
            return false
        }

        if ( caster.isNpc() ) {
            return object.isPlayable()
        }
    }

    return object.isAutoAttackable( caster )
}