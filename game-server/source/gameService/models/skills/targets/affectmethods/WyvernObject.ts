import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { AffectObjectMethodType } from '../AffectMethodRegistry'

export const WyvernObject: AffectObjectMethodType = ( caster: L2Character, object: L2Object ): boolean => {
    if ( !object.isNpc() ) {
        return false
    }

    return 12621 === object.getId()
}