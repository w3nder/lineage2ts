import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { AffectObjectMethodType } from '../AffectMethodRegistry'
import { L2PcInstance } from '../../../actor/instance/L2PcInstance'

export const Invisible: AffectObjectMethodType = ( caster: L2Character, object: L2Object ): boolean => {
    if ( caster.isPlayer() ) {
        return !object.isVisibleFor( caster as L2PcInstance ) || object.isInvisible()
    }

    return object.isInvisible()
}