import { AffectMethodScope } from './AffectMethodScope'
import { Clan } from './affectmethods/Clan'
import { Friend } from './affectmethods/Friend'
import { Invisible } from './affectmethods/Invisible'
import { NotFriend } from './affectmethods/NotFriend'
import { ObjectDeadNpcBody } from './affectmethods/ObjectDeadNpcBody'
import { UndeadRealEnemy } from './affectmethods/UndeadRealEnemy'
import { WyvernObject } from './affectmethods/WyvernObject'
import { L2Character } from '../../actor/L2Character'
import { L2Object } from '../../L2Object'

export type AffectObjectMethodType = ( caster: L2Character, object: L2Object ) => boolean
export const ReturnFalseMethod : AffectObjectMethodType = () => false
const ReturnTrueMethod : AffectObjectMethodType = () => true

export const AffectMethodRegistry: { [ type: number ]: AffectObjectMethodType } = {
    [ AffectMethodScope.ALL ]: ReturnTrueMethod,
    [ AffectMethodScope.CLAN ]: Clan,
    [ AffectMethodScope.FRIEND ]: Friend,
    [ AffectMethodScope.HIDDEN_PLACE ]: ReturnFalseMethod,
    [ AffectMethodScope.INVISIBLE ]: Invisible,
    [ AffectMethodScope.NONE ]: ReturnFalseMethod,
    [ AffectMethodScope.NOT_FRIEND ]: NotFriend,
    [ AffectMethodScope.OBJECT_DEAD_NPC_BODY ]: ObjectDeadNpcBody,
    [ AffectMethodScope.UNDEAD_REAL_ENEMY ]: UndeadRealEnemy,
    [ AffectMethodScope.WYVERN_OBJECT ]: WyvernObject,
}