import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { InstanceType } from '../../../../enums/InstanceType'

export const HolySiegeArtifact: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    if ( !target || !target.isInstanceType( InstanceType.L2ArtefactInstance ) ) {
        return
    }

    return target
}