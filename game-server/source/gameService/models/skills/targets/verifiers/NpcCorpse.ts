import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { L2Npc } from '../../../actor/L2Npc'
import { SystemMessageBuilder } from '../../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../../packets/SystemMessageIdValues'
import { L2EffectType } from '../../../../enums/effects/L2EffectType'
import { L2Attackable } from '../../../actor/L2Attackable'
import { ConfigManager } from '../../../../../config/ConfigManager'

export const NpcCorpse: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    if ( !target
            || !target.isNpc()
            || !( target as L2Npc ).isDead() ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    if ( skill.hasEffectType( L2EffectType.Summon )
            && target.isServitor()
            && target.getActingPlayerId() === caster.getObjectId() ) {
        return
    }

    if ( target.isAttackable()
            && skill.hasEffectType( L2EffectType.HpDrain )
            && ( target as L2Attackable ).isOldCorpse( caster.getActingPlayer() ) ) {
        return
    }

    return target
}