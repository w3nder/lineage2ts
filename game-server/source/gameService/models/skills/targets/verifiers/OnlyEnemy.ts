import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { SystemMessageBuilder } from '../../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../../actor/instance/L2PcInstance'
import { DuelManager } from '../../../../instancemanager/DuelManager'
import { AreaType } from '../../../areas/AreaType'

export const OnlyEnemy: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    if ( !target
            || !target.isCharacter()
            || ( target as L2Character ).isDead()
            || !target.isAutoAttackable( caster )
            || caster.getObjectId() === target.getObjectId() ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    if ( target.isNpc() ) {
        if ( target.isAttackable() ) {
            return target
        }
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    let player : L2PcInstance = caster.getActingPlayer()
    if ( !player ) {
        return
    }

    if ( player.isInArea( AreaType.PVP ) ) {
        return target
    }

    if ( player.isAtWarWith( target as L2Character ) ) {
        return target
    }

    if ( player.isInOlympiadMode() ) {
        let targetPlayer : L2PcInstance = target.getActingPlayer()
        if ( !targetPlayer || player.getOlympiadSide() === targetPlayer.getOlympiadSide() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
            return
        }

        return target
    }

    if ( player.isInDuelWith( target as L2Character ) ) {
        let targetPlayer = target.getActingPlayer()
        let duel = DuelManager.getDuel( player.getDuelId() )
        let teamA : Array<number> = duel.getTeamA()
        let teamB : Array<number> = duel.getTeamB()

        if ( ( teamA.includes( player.getObjectId() ) || teamB.includes( player.getObjectId() ) )
                && ( teamA.includes( targetPlayer.getObjectId() ) || teamB.includes( targetPlayer.getObjectId() ) ) ) {
            return target
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    if ( player.isInPartyWith( target as L2Character )
            || player.isInClanWith( target as L2Character )
            || player.isInAllyWith( target as L2Character )
            || player.isInCommandChannelWith( target as L2Character )
            || player.isOnSameSiegeSideWith( target as L2Character )
            || !player.checkIfPvP( target as L2Character ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    return target
}