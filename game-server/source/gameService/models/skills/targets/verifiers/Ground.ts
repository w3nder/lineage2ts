import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { SystemMessageBuilder } from '../../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../../packets/SystemMessageIdValues'
import { PathFinding } from '../../../../../geodata/PathFinding'
import { AreaType } from '../../../areas/AreaType'

export const Ground: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    let player = caster.getActingPlayer()
    if ( !player ) {
        return
    }

    let worldPosition = player.getGroundSkillLocation()
    if ( !worldPosition ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_SEE_TARGET ) )
        return
    }

    if ( !PathFinding.canSeeTargetWithPosition( player, worldPosition ) ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_SEE_TARGET ) )
        return
    }

    if ( skill.isBad() && caster.isInArea( AreaType.Peace ) ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.A_MALICIOUS_SKILL_CANNOT_BE_USED_IN_PEACE_ZONE ) )
        return
    }

    return caster
}