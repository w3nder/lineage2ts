import { TargetVerifierMethodType } from '../L2TargetType'
import { Skill } from '../../../Skill'
import { L2Character } from '../../../actor/L2Character'
import { L2Object } from '../../../L2Object'
import { L2Npc } from '../../../actor/L2Npc'
import { SystemMessageBuilder } from '../../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../../packets/SystemMessageIdValues'

export const AdvanceBase: TargetVerifierMethodType = ( skill: Skill, caster: L2Character, target: L2Object ): L2Object => {
    // Outpost npc
    if ( !target
            || !target.isNpc()
            || target.getId() !== 36590
            || ( target as L2Npc ).isDead() ) {
        caster.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    return target
}