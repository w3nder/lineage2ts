export enum AffectSelectionScope {
    /** Affects Valakas. */
    BALAKAS_SCOPE,
    /** Affects dead clan mates. */
    DEAD_PLEDGE,
    /** Affects fan area. */
    FAN,
    /** Affects nothing. */
    NONE,
    /** Affects party members. */
    PARTY,
    /** Affects party and clan mates. */
    PARTY_PLEDGE,
    /** Affects clan mates. */
    PLEDGE,
    /** Affects point blank targets, using caster as point of origin. */
    POINT_BLANK,
    /** Affects ranged targets, using selected target as point of origin. */
    RANGE,
    /** Affects ranged targets sorted by HP, using selected target as point of origin. */
    RANGE_SORT_BY_HP,
    /** Affects ranged targets, using selected target as point of origin. */
    RING_RANGE,
    /** Affects a single target. */
    SINGLE,
    /** Affects targets inside an square area, using selected target as point of origin. */
    SQUARE,
    /** Affects targets inside an square area, using caster as point of origin. */
    SQUARE_PB,
    /** Affects static object targets. */
    STATIC_OBJECT_SCOPE,
    /** Affects wyverns. */
    WYVERN_SCOPE,
    /** Affects dead command channel mates of the target. */
    DEAD_UNION
}

