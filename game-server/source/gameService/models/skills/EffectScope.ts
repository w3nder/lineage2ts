export const enum EffectScope {
    NONE,
    GENERAL,
    START,
    SELF,
    PASSIVE,
    CHANNELING,
    PVP,
    PVE,
    END
}