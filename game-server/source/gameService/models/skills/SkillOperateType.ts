export enum SkillOperateType {
    /**
     * Active Skill with "Instant Effect" (for example damage skills heal/pdam/mdam/cpdam skills).
     */
    A1,

    /**
     * Active Skill with "Continuous effect + Instant effect" (for example buff/debuff or damage/heal over time skills).
     */
    A2,

    /**
     * Active Skill with "Instant effect + Continuous effect"
     */
    A3,

    /**
     * Active Skill with "Instant effect + ?" used for special event herb.
     */
    A4,

    /**
     * Continuous Active Skill with "instant effect" (instant effect casted by ticks).
     */
    CA1,

    /**
     * Continuous Active Skill with "continuous effect" (continuous effect casted by ticks).
     */
    CA5,

    /**
     * Directional Active Skill with "Charge/Rush instant effect".
     */
    DA1,

    /**
     * Directional Active Skill with "Charge/Rush Continuous effect".
     */
    DA2,

    /**
     * Passive Skill.
     */
    P,

    /**
     * Toggle Skill.
     */
    T
}

const activeTypes = new Set<SkillOperateType>( [
    SkillOperateType.A1,
    SkillOperateType.A1,
    SkillOperateType.A2,
    SkillOperateType.A3,
    SkillOperateType.A4,
    SkillOperateType.CA1,
    SkillOperateType.CA5,
    SkillOperateType.DA1,
    SkillOperateType.DA2,
] )

const continuousTypes = new Set<SkillOperateType>( [
    SkillOperateType.A2,
    SkillOperateType.A4,
    SkillOperateType.DA2
] )

const channelingTypes = new Set<SkillOperateType>( [
    SkillOperateType.CA1,
    SkillOperateType.CA5
] )

const flyTypes = new Set<SkillOperateType>( [
    SkillOperateType.DA1,
    SkillOperateType.DA2
] )

export const SkillOperateTypeOperations = {
    isActive( value : SkillOperateType ) {
        return activeTypes.has( value )
    },

    isContinuous( value : SkillOperateType ) {
        return continuousTypes.has( value )
    },

    isSelfContinuous( value : SkillOperateType ) {
        return value === SkillOperateType.A3
    },

    isPassive( value: SkillOperateType ) {
        return value === SkillOperateType.P
    },

    isToggle( value: SkillOperateType ) {
        return value === SkillOperateType.T
    },

    isChanneling( value: SkillOperateType ) {
        return channelingTypes.has( value )
    },

    isFlyType( value: SkillOperateType ) {
        return flyTypes.has( value )
    }
}