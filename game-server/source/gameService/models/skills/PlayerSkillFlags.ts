export interface PlayerSkillFlags {
    isCtrlPressed: boolean
    isShiftPressed: boolean
}