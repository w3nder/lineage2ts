export interface SkillItem {
    id: number
    level: number
}

export interface PetSkillItem extends SkillItem {
    minLevel: number
}