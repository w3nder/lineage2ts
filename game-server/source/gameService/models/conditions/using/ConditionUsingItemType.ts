import { Condition } from '../Condition'
import { ArmorType, ArmorTypeMask } from '../../../enums/items/ArmorType'
import { L2Character } from '../../actor/L2Character'
import { WeaponTypeMask } from '../../items/type/WeaponType'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { InventorySlot } from '../../../values/InventoryValues'
import { L2ItemSlots } from '../../../enums/L2ItemSlots'

export class ConditionUsingItemType extends Condition {
    mask: number
    isArmor: boolean

    constructor( mask: number ) {
        super()
        this.mask = mask
        this.isArmor = ( mask & ( ArmorTypeMask( ArmorType.MAGIC ) | ArmorTypeMask( ArmorType.LIGHT ) | ArmorTypeMask( ArmorType.HEAVY ) ) ) !== 0
    }

    runTest( caster: L2Character ): boolean {
        if ( !caster ) {
            return false
        }

        if ( !caster.isPlayer() ) {
            return !this.isArmor && ( this.mask & WeaponTypeMask( caster.getAttackType() ) ) !== 0
        }

        let inventory = caster.getInventory()
        if ( this.isArmor ) {
            let chest : L2ItemInstance = inventory.getPaperdollItem( InventorySlot.Chest )
            if ( !chest ) {
                return false
            }

            let chestMask = chest.getItem().getItemMask()
            if ( ( this.mask & chestMask ) === 0 ) {
                return false
            }

            let chestBodyPart = chest.getItem().getBodyPart()
            if ( chestBodyPart === L2ItemSlots.FullArmor ) {
                return true
            }

            let legs : L2ItemInstance = inventory.getPaperdollItem( InventorySlot.Legs )
            if ( !legs ) {
                return false
            }

            let legMask = legs.getItem().getItemMask()
            return ( this.mask & legMask ) !== 0
        }

        return ( this.mask & inventory.getWearedMask() ) !== 0
    }
}