import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2Weapon } from '../../items/L2Weapon'

export class ConditionChangeWeapon extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        if ( !caster || !caster.isPlayer() ) {
            return false
        }

        if ( this.value ) {
            let weapon: L2Weapon = caster.getActiveWeaponItem()
            if ( !weapon ) {
                return false
            }

            if ( weapon.getChangeWeaponId() === 0 ) {
                return false
            }

            return !caster.getActingPlayer().isEnchanting()
        }

        return true
    }
}