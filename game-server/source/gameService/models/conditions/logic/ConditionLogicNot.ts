import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'
import { L2Item } from '../../items/L2Item'

export class ConditionLogicNot extends Condition {
    condition: Condition

    constructor( condition: Condition ) {
        super()
        this.condition = condition
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill = null, item: L2Item = null ): boolean {
        return !this.condition.test( caster, target, skill, item )
    }
}