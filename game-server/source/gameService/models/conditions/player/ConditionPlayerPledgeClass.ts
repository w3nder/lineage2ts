import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerPledgeClass extends Condition {
    class: number

    constructor( value: number ) {
        super()
        this.class = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( !player || !player.getClan() ) {
            return false
        }

        if ( this.class === -1 ) {
            return player.isClanLeader()
        }

        return player.getPledgeClass() >= this.class
    }
}