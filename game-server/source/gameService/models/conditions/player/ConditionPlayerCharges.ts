import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerCharges extends Condition {
    charges: number

    constructor( value: number ) {
        super()
        this.charges = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        return player && player.getCharges() >= this.charges
    }
}