import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'
import { L2Item } from '../../items/L2Item'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'

export class ConditionPlayerInClanAcademy extends Condition {
    inAcademy: boolean

    constructor( inAcademy: boolean ) {
        super()

        this.inAcademy = inAcademy
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill = undefined, item: L2Item = undefined ): boolean {
        if ( caster.isPlayer() ) {
            return false
        }

        return ( caster as L2PcInstance ).getSponsor() > 0
    }
}