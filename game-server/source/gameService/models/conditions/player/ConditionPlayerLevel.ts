import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerLevel extends Condition {
    level: number

    constructor( level: number ) {
        super()
        this.level = level
    }

    runTest( caster: L2Character ): boolean {
        return caster.getLevel() >= this.level
    }
}