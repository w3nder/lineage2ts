import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerPkCount extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        return player && player.getPkKills() <= this.value
    }
}