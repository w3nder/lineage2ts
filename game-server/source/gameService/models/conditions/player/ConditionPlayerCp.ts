import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerCp extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        return caster && ( ( ( caster.getCurrentCp() * 100 ) / caster.getMaxCp() ) >= this.value )
    }
}