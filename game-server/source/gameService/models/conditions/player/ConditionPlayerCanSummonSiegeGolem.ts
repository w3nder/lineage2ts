import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { Castle } from '../../entity/Castle'
import { CastleManager } from '../../../instancemanager/CastleManager'
import { Fort } from '../../entity/Fort'
import { FortManager } from '../../../instancemanager/FortManager'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { SevenSigns } from '../../../directives/SevenSigns'
import { SevenSignsSeal, SevenSignsSide } from '../../../values/SevenSignsValues'

export class ConditionPlayerCanSummonSiegeGolem extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    checkPlayer( player: L2PcInstance ) {
        let clan = player.getClan()
        if ( player.isAlikeDead() || player.isCursedWeaponEquipped() || !clan ) {
            return false
        }

        let castle: Castle = CastleManager.getCastle( player )
        let fort: Fort = FortManager.getFort( player )

        if ( !castle || !fort ) {
            return false
        }

        if ( ( fort && ( fort.getResidenceId() === 0 ) ) || ( castle && ( castle.getResidenceId() === 0 ) ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
            return false
        }

        if ( ( castle && !castle.getSiege().isInProgress() ) || ( fort && !fort.getSiege().isInProgress() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
            return false
        }

        if ( clan && ( ( castle && !castle.getSiege().getAttackerClan( clan ) ) || ( fort && !fort.getSiege().getAttackerClan( clan ) ) ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
            return false
        }

        return !this.checkSummonConditions( player )
    }

    runTest( caster: L2Character ): boolean {
        if ( !caster || !caster.isPlayer() ) {
            return !this.value
        }

        let player = caster.getActingPlayer()
        return this.checkPlayer( player ) === this.value
    }

    checkSummonConditions( player: L2PcInstance ): boolean {
        if ( !player ) {
            return false
        }

        /*
            Golems cannot be summoned by Dusk when the Seal of Strife is controlled by the Dawn
         */
        if ( SevenSigns.isSealValidationPeriod()
            && SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) === SevenSignsSide.Dawn
            && SevenSigns.getPlayerSide( player.getObjectId() ) === SevenSignsSide.Dusk ) {

            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SEAL_OF_STRIFE_FORBIDS_SUMMONING ) )
            return true
        }

        return false
    }
}