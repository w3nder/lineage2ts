import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { Castle } from '../../entity/Castle'
import { CastleManager } from '../../../instancemanager/CastleManager'
import { Fort } from '../../entity/Fort'
import { FortManager } from '../../../instancemanager/FortManager'
import { TerritoryWarManager } from '../../../instancemanager/TerritoryWarManager'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { AreaType } from '../../areas/AreaType'

export class ConditionPlayerCanCreateOutpost extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    checkPlayer( player: L2PcInstance ): boolean {
        if ( player.isAlikeDead() || player.isCursedWeaponEquipped() || !player.getClan() ) {
            return false
        }

        let castle: Castle = CastleManager.getCastle( player )
        let fort: Fort = FortManager.getFort( player )

        if ( !castle || !fort ) {
            return false
        }

        if ( ( fort && fort.getResidenceId() === 0 ) || ( castle && castle.getResidenceId() === 0 ) ) {
            player.sendMessage( 'You must be on fort or castle ground to construct an outpost or flag.' )
            return false
        }

        if ( ( fort && !fort.isSiegeActive() ) || ( castle && !castle.isSiegeActive() ) ) {
            player.sendMessage( 'You can only construct an outpost or flag on siege field.' )
            return false
        }

        if ( !player.isClanLeader() ) {
            player.sendMessage( 'You must be a clan leader to construct an outpost or flag.' )
            return false
        }

        if ( TerritoryWarManager.getSiegeFlagForClan( player.getClan() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ANOTHER_HEADQUARTERS ) )
            return false
        }

        if ( TerritoryWarManager.getFlagForClan( player.getClan() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.A_FLAG_IS_ALREADY_BEING_DISPLAYED_ANOTHER_FLAG_CANNOT_BE_DISPLAYED ) )
            return false
        }

        if ( !player.isInArea( AreaType.CastleHq ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_SET_UP_BASE_HERE ) )
            return false
        }

        return true
    }

    runTest( caster: L2Character ): boolean {
        if ( !caster || !caster.isPlayer() ) {
            return !this.value
        }

        let player = caster.getActingPlayer()
        return this.checkPlayer( player ) === this.value
    }
}