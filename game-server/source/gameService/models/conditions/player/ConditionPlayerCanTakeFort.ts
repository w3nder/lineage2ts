import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'
import { Fort } from '../../entity/Fort'
import { FortManager } from '../../../instancemanager/FortManager'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { GeneralHelper } from '../../../helpers/GeneralHelper'

export class ConditionPlayerCanTakeFort extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill = null ): boolean {
        if ( !caster || !caster.isPlayer() ) {
            return false
        }

        let player = caster.getActingPlayer()
        return this.checkPlayer( player, target, skill ) === this.value
    }

    checkPlayer( player: L2PcInstance, target: L2Character, skill: Skill ) : boolean {
        if ( player.isAlikeDead() || player.isCursedWeaponEquipped() || !player.isClanLeader() ) {
            return false
        }

        let fort: Fort = FortManager.getFort( player )
        if ( !fort || ( fort.getResidenceId() <= 0 ) || !fort.getSiege().isInProgress() || !fort.getSiege().getAttackerClan( player.getClan() ) ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                .addSkillName( skill )
                .getBuffer()
            player.sendOwnedData( packet )
            return false
        }

        if ( fort.getFlagPole().getObjectId() !== target.getObjectId() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
            return false
        }

        if ( !GeneralHelper.checkIfInRange( 200, player, target, true ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DIST_TOO_FAR_CASTING_STOPPED ) )
            return false
        }

        return true
    }
}