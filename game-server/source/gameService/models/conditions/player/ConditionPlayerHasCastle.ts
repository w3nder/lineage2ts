import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2Clan } from '../../L2Clan'

export const enum PlayerHasCastleType {
    AnyCastle = -1,
    NoClanRequired = 0
}

export class ConditionPlayerHasCastle extends Condition {
    castleId: number

    constructor( id: number | PlayerHasCastleType = PlayerHasCastleType.AnyCastle ) {
        super()
        this.castleId = id
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( !player ) {
            return false
        }

        let clan: L2Clan = player.getClan()
        if ( !clan ) {
            return this.castleId === PlayerHasCastleType.NoClanRequired
        }

        let castleId = clan.getCastleId()
        if ( this.castleId === PlayerHasCastleType.AnyCastle ) {
            return castleId > 0
        }

        return castleId === this.castleId
    }
}