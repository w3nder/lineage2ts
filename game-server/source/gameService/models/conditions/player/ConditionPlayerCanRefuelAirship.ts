import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { InstanceType } from '../../../enums/InstanceType'

export class ConditionPlayerCanRefuelAirship extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( !player
                || !player.getAirShip()
                || !player.getAirShip().isInstanceType( InstanceType.L2ControllableAirShipInstance )
                || ( player.getAirShip().getFuel() + this.value ) > player.getAirShip().getMaxFuel() ) {
            return false
        }

        return true
    }
}