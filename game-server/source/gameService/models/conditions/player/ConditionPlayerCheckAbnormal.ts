import { Condition } from '../Condition'
import { AbnormalType } from '../../skills/AbnormalType'
import { L2Character } from '../../actor/L2Character'
import { BuffInfo } from '../../skills/BuffInfo'

export class ConditionPlayerCheckAbnormal extends Condition {
    type: AbnormalType
    level: number
    defaultValue: boolean

    constructor( type: AbnormalType, level: number, defaultValue: boolean = true ) {
        super()
        this.type = type
        this.level = level
        this.defaultValue = defaultValue
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        let info : BuffInfo = player.getEffectList().getBuffInfoByAbnormalType( this.type )
        if ( !info ) {
            return !this.defaultValue
        }

        return this.defaultValue && ( ( this.level === -1 ) || ( this.level >= info.getSkill().getAbnormalLevel() ) )
    }
}