import { Condition } from '../Condition'
import { Race } from '../../../enums/Race'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerRace extends Condition {
    races: Array<Race>

    constructor( races: Array<Race>, messageId? : number ) {
        super()
        this.races = races
        this.messageId = messageId
    }

    runTest( caster: L2Character ): boolean {
        return caster.isPlayer() && this.races.includes( caster.getActingPlayer().getRace() )
    }
}