import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerTransformationId extends Condition {
    id: number

    constructor( id: number ) {
        super()
        this.id = id
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        if ( !player ) {
            return false
        }

        if ( this.id === -1 ) {
            return player.isTransformed()
        }

        return player.getTranformationId() === this.id
    }
}