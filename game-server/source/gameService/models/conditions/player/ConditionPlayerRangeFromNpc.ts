import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2World } from '../../../L2World'

export class ConditionPlayerRangeFromNpc extends Condition {
    ids: Array<number>
    radius: number
    shouldExist: boolean

    constructor( ids: Array<number>, radius: number, shouldExist: boolean ) {
        super()
        this.ids = ids
        this.radius = radius
        this.shouldExist = shouldExist
    }

    runTest( caster: L2Character ): boolean {
        let condition = this
        if ( this.ids.length > 0 && this.radius > 0 ) {
            return L2World.getVisibleCharacters( caster, this.radius ).some( ( character: L2Character ) => {
                return character.isNpc() && condition.ids.includes( character.getId() )
            } )
        }

        return false
    }
}