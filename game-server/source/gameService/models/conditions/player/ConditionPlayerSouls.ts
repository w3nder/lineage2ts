import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerSouls extends Condition {
    souls: number

    constructor( value: number ) {
        super()
        this.souls = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        return player && player.getChargedSouls() >= this.souls
    }
}