import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { Castle } from '../../entity/Castle'
import { CastleManager } from '../../../instancemanager/CastleManager'
import { Fort } from '../../entity/Fort'
import { FortManager } from '../../../instancemanager/FortManager'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { Skill } from '../../Skill'
import { SiegeManager } from '../../../instancemanager/SiegeManager'
import { ConfigManager } from '../../../../config/ConfigManager'
import { AreaType } from '../../areas/AreaType'

export class ConditionPlayerCanCreateBase extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill ): boolean {
        if ( !caster || !caster.isPlayer() ) {
            return !this.value
        }

        let player = caster.getActingPlayer()

        return this.checkPlayer( player, skill ) === this.value
    }

    checkPlayer( player: L2PcInstance, skill: Skill ) : boolean {
        if ( player.isAlikeDead() || player.isCursedWeaponEquipped() || !player.getClan() ) {
            return false
        }

        let castle : Castle = CastleManager.getCastle( player )
        let fort : Fort = FortManager.getFort( player )
        let message = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED ).addSkillName( skill )

        if ( !castle || !fort ) {
            player.sendOwnedData( message.getBuffer() )
            return false
        }

        if ( ( castle && !castle.getSiege().isInProgress() ) || ( fort && !fort.getSiege().isInProgress() ) ) {
            player.sendOwnedData( message.getBuffer() )
            return false
        }

        if ( ( castle && !castle.getSiege().getAttackerClan( player.getClan() ) ) || ( fort && !fort.getSiege().getAttackerClan( player.getClan() ) ) ) {
            player.sendOwnedData( message.getBuffer() )
            return false
        }

        if ( !player.isClanLeader() ) {
            player.sendOwnedData( message.getBuffer() )
            return false
        }

        if ( ( castle && ( castle.getSiege().getAttackerClan( player.getClan() ).getNumFlags() >= SiegeManager.getFlagMaxCount() ) )
            || ( fort && ( fort.getSiege().getAttackerClan( player.getClan() ).getNumFlags() >= ConfigManager.fortSiege.getMaxFlags() ) ) ) {
            player.sendOwnedData( message.getBuffer() )
            return false
        }

        if ( !player.isInArea( AreaType.CastleHq ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_SET_UP_BASE_HERE ) )
            return false
        }

        return true
    }
}