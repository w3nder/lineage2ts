import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionPlayerCloakStatus extends Condition {
    value: boolean

    constructor( value: boolean ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character ): boolean {
        let player = caster.getActingPlayer()
        return player && player.getInventory().canEquipCloak() === this.value
    }
}