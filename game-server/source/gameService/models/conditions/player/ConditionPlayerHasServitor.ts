import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'

export class ConditionPlayerHasServitor extends Condition {
    runTest( caster: L2Character ): boolean {
        if ( !caster || !caster.isPlayer() ) {
            return false
        }

        let player = caster.getActingPlayer()
        if ( !player.hasSummon() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_USE_SKILL_WITHOUT_SERVITOR ) )
            return false
        }

        return true
    }
}