import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionTargetAbnormal extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        return ( target.getAbnormalVisualEffects() & this.value ) !== 0
    }
}