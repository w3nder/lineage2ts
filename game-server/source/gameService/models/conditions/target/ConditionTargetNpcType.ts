import { Condition } from '../Condition'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Character } from '../../actor/L2Character'

export class ConditionTargetNpcType extends Condition {
    types: Array<InstanceType>

    constructor( types: Array<InstanceType> ) {
        super()
        this.types = types
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        if ( !target ) {
            return false
        }

        return target.isInstanceTypes( this.types )
    }
}