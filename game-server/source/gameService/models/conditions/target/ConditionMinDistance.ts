import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionMinDistance extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        return target && caster.calculateDistance( target, true ) >= this.value
    }
}