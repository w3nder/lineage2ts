import { Condition } from '../Condition'
import { Skill } from '../../Skill'
import { L2Character } from '../../actor/L2Character'
import { L2PcInstance } from '../../actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'

const currentPartyType = 'EXCEPT_ME'

export class ConditionTargetMyParty extends Condition {
    type: string

    constructor( type: string ) {
        super()
        this.type = type
    }

    runTest( caster: L2Character, target: L2Character, skill: Skill = null ): boolean {
        let player = caster.getActingPlayer()
        if ( !player ) {
            return false
        }

        if ( currentPartyType === this.type && player.objectId === target.objectId ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_USE_ON_YOURSELF ) )
            return false
        }

        if ( !player.isInParty() || !player.getParty().getMembers().includes( target.objectId ) ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                    .addSkillName( skill )
                    .getBuffer()

            player.sendOwnedData( packet )

            return false
        }

        let targetPlayer : L2PcInstance = target.getActingPlayer()
        if ( targetPlayer && player.getObjectId() !== targetPlayer.getObjectId() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                    .addSkillName( skill )
                    .getBuffer()

            player.sendOwnedData( packet )

            return false
        }

        return true
    }
}