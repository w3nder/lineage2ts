import { Condition } from '../Condition'
import { Race } from '../../../enums/Race'
import { L2Character } from '../../actor/L2Character'

export class ConditionTargetRace extends Condition {
    value: Race

    constructor( value: Race ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        return target.getRace() === this.value
    }
}