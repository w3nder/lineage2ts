import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { BuffInfo } from '../../skills/BuffInfo'

export class ConditionTargetActiveEffectId extends Condition {
    id: number
    level: number

    constructor( id: number, level: number = -1 ) {
        super()
        this.id = id
        this.level = level
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        let info: BuffInfo = target.getEffectList().getBuffInfoBySkillId( this.id )
        return info && ( ( this.level === -1 ) || ( this.level <= info.getSkill().getLevel() ) )
    }
}