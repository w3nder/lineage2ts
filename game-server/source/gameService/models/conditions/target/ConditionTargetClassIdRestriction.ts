import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionTargetClassIdRestriction extends Condition {
    ids: Array<number>

    constructor( ids: Array<number> ) {
        super()
        this.ids = ids
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        if ( !target.isPlayer() ) {
            return false
        }

        return this.ids.includes( target.getActingPlayer().getClassId() )
    }
}