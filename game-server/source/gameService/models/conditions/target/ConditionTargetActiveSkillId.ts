import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { Skill } from '../../Skill'

export class ConditionTargetActiveSkillId extends Condition {
    id: number
    level: number

    constructor( id: number, level: number = -1 ) {
        super()
        this.id = id
        this.level = level
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        let condition = this

        return target.getAllSkills().some( ( skill: Skill ) => {
            return skill
                && ( skill.getId() === condition.id )
                && ( ( condition.level === -1 ) || ( condition.level <= skill.getLevel() ) )
        } )
    }
}