import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'
import { L2Weapon } from '../../items/L2Weapon'
import { WeaponTypeMask } from '../../items/type/WeaponType'

export class ConditionTargetUsesWeaponKind extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        if ( !target ) {
            return false
        }

        let weapon : L2Weapon = target.getActiveWeaponItem()
        if ( !weapon ) {
            return false
        }

        return ( WeaponTypeMask( weapon.getItemType() ) & this.value ) !== 0
    }
}