import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionTargetInventorySize extends Condition {
    value: number

    constructor( value: number ) {
        super()
        this.value = value
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        if ( !target || !target.isPlayer() ) {
            return false
        }

        let player = target.getActingPlayer()
        return player.getInventory().getFullSize( false ) <= ( player.getInventoryLimit() - this.value )
    }
}