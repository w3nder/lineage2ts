import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionTargetLevelRange extends Condition {
    minLevel: number
    maxLevel: number

    constructor( minLevel: number, maxLevel: number ) {
        super()
        this.minLevel = minLevel
        this.maxLevel = maxLevel
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        if ( !target ) {
            return false
        }

        let level = target.getLevel()
        return level <= this.maxLevel && level >= this.minLevel
    }
}