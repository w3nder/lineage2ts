import { Condition } from '../Condition'
import { L2Character } from '../../actor/L2Character'

export class ConditionTargetNpcId extends Condition {
    ids: Array<number>

    constructor( ids: Array<number> ) {
        super()
        this.ids = ids
    }

    runTest( caster: L2Character, target: L2Character ): boolean {
        if ( target && ( target.isNpc() || target.isDoor() ) ) {
            return this.ids.includes( target.getId() )
        }

        return false
    }
}