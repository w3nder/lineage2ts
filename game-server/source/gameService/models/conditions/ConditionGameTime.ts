import { Condition } from './Condition'
import { GameTime } from '../../cache/GameTime'

export enum ConditionGameTimePeriods {
    NIGHT,
    DAY
}

export class ConditionGameTime extends Condition {

    time: ConditionGameTimePeriods
    isRequired: boolean

    constructor( time: ConditionGameTimePeriods, isRequired: boolean ) {
        super()
        this.time = time
        this.isRequired = isRequired
    }

    runTest(): boolean {
        switch ( this.time ) {
            case ConditionGameTimePeriods.NIGHT:
                return GameTime.isNight() === this.isRequired

            default:
                return !this.isRequired
        }
    }
}