import { ISpawnLogic } from './spawns/ISpawnLogic'
import { L2ManualSpawn, L2ManualSpawnModifiers } from './spawns/type/L2ManualSpawn'
import { L2SpawnNpcDataItem } from '../../data/interface/SpawnNpcDataApi'
import { DataManager } from '../../data/manager'
import { GeneralHelper } from '../helpers/GeneralHelper'

export interface L2FestivalRoom {
    initialize( spawnData: Record<L2FestivalPhase, ReadonlyArray<L2FestivalSpawn>> ) : void
    isInitialized() : boolean
    reset() : void
    spawnPhase( phase: L2FestivalPhase ) : void
}

export const enum L2FestivalPhase {
    Initial,
    AdditionalDifficulty,
    Final
}

const allPhases : Array<L2FestivalPhase> = [
    L2FestivalPhase.Initial,
    L2FestivalPhase.AdditionalDifficulty,
    L2FestivalPhase.Final
]

export interface L2FestivalSpawn {
    x: number
    y: number
    z: number
    npcId: number
}

type FestivalSpawns = Record<L2FestivalPhase, ReadonlyArray<ISpawnLogic>>

export class L2DarknessFestival implements L2FestivalRoom {
    private spawns: FestivalSpawns
    private phase: L2FestivalPhase
    private spawnedPhases: Set<L2FestivalPhase> = new Set<L2FestivalPhase>()

    initialize( spawnData: Record<L2FestivalPhase, ReadonlyArray<L2FestivalSpawn>> ) : void {
        if ( this.isInitialized() ) {
            return
        }

        let modifierData : L2ManualSpawnModifiers = {
            instanceId: 0,
            isSummonSpawn: false,
            isChampion: false
        }

        this.spawns = allPhases.reduce( ( spawns : FestivalSpawns, currentPhase: L2FestivalPhase ) : FestivalSpawns => {
            spawns[ currentPhase ] = spawnData[ currentPhase ].map( ( currentSpawn: L2FestivalSpawn ) : ISpawnLogic => {
                let template = DataManager.getNpcData().getTemplate( currentSpawn.npcId )
                let spawnData : L2SpawnNpcDataItem = {
                    recordId: null,
                    aiName: null,
                    aiParameters: null,
                    amount: 1,
                    makerId: '',
                    minions: null,
                    npcId: currentSpawn.npcId,
                    position: {
                        x: currentSpawn.x,
                        y: currentSpawn.y,
                        z: currentSpawn.z,
                        heading: GeneralHelper.getRandomHeading()
                    },
                    respawnExtraMs: 0,
                    respawnMs: 0,
                    returnDistance: 0
                }

                return new L2ManualSpawn( template, spawnData, modifierData )
            } )

            return spawns
        }, {} as FestivalSpawns )

        this.phase = L2FestivalPhase.Initial
    }

    // TODO : add global coordinator that switches phase to final
    spawnPhase( phase: L2FestivalPhase ) : void {
        if ( this.spawnedPhases.has( phase ) ) {
            return
        }

        this.phase = phase
        return this.spawnNpcs()
    }

    isInitialized(): boolean {
        return !!this.spawns
    }

    reset() : void {
        if ( !this.isInitialized() ) {
            return
        }

        this.deSpawnAll()
        this.spawnedPhases.clear()
        this.phase = L2FestivalPhase.Initial
    }

    private spawnNpcs() : void {
        let spawns = this.spawns[ this.phase ]
        if ( !spawns ) {
            return
        }

        for ( const spawn of spawns ) {
            spawn.startSpawn()
        }

        this.spawnedPhases.add( this.phase )
    }

    private deSpawnAll() : void {
        for ( const spawns of Object.values( this.spawns ) ) {
            for ( const spawn of spawns ) {
                spawn.startDespawn()
            }
        }
    }
}