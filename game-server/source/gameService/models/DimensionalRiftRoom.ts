import { Location } from './Location'
import { boxContainsPoint, Box, IntervalMode, Vec } from 'math2d'
import _ from 'lodash'
import { ISpawnLogic } from './spawns/ISpawnLogic'

export class DimensionalRiftRoom {
    type: number
    room: number
    xMin: number
    xMax: number
    yMin: number
    yMax: number
    zMin: number
    zMax: number
    teleportCoordinates: Location
    shape: Box
    isBossRoom: boolean
    roomSpawns: Array<ISpawnLogic> = []
    isPartyInside: boolean = false

    constructor( type: number,
                roomId: number,
                xMin: number,
                xMax: number,
                yMin: number,
                yMax: number,
                zMin: number,
                zMax: number,
                xTeleport: number,
                yTeleport: number,
                zTeleport: number,
                isBossRoom: boolean ) {
        this.type = type
        this.room = roomId

        this.xMin = ( xMin + 128 )
        this.xMax = ( xMax - 128 )
        this.yMin = ( yMin + 128 )
        this.yMax = ( yMax - 128 )

        this.shape = {
            maxX: xMax,
            maxY: yMax,
            minX: xMin,
            minY: yMin
        }

        this.zMin = zMin
        this.zMax = zMax

        this.teleportCoordinates = new Location( xTeleport, yTeleport, zTeleport )
        this.isBossRoom = isBossRoom
    }

    // TODO : instead of direct lookup, use grid for rooms to determine both room and rift type
    checkIfInZone( x: number, y: number, z: number ) : boolean {
        let point : Vec = {
            x,
            y
        }

        return boxContainsPoint( this.shape, point, IntervalMode.CLOSED ) && z >= this.zMin && z <= this.zMax
    }

    getTeleportCoordinates() : Location {
        return this.teleportCoordinates
    }

    unspawn() : DimensionalRiftRoom {
        this.roomSpawns.forEach( ( spawn: ISpawnLogic ) => {
            spawn.stopRespawn()
            spawn.startDespawn()
        } )

        return this
    }

    setPartyInside( value: boolean ) {
        this.isPartyInside = value
    }

    spawn() {
        this.roomSpawns.forEach( ( spawn: ISpawnLogic ) => {
            spawn.startSpawn()
        } )
    }

    getType() {
        return this.type
    }

    getRoom() {
        return this.room
    }

    getRandomX() {
        return _.random( this.xMin, this.xMax )
    }

    getRandomY() {
        return _.random( this.yMin, this.yMax )
    }
}