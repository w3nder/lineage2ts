export class RegionActivation {
    private activatedRegions: Set<number> = new Set<number>()

    isActivated( code: number ): boolean {
        return this.activatedRegions.has( code )
    }

    clear() : void {
        this.activatedRegions.clear()
    }

    markActivated( id: number ) : void {
        this.activatedRegions.add( id )
    }
}