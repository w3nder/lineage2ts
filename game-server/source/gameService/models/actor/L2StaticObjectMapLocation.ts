export interface L2StaticObjectMapLocation {
    name: string
    x: number
    y: number
}