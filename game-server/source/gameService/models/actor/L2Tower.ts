import { L2Npc } from './L2Npc'
import { L2NpcTemplate } from './templates/L2NpcTemplate'
import { InstanceType } from '../../enums/InstanceType'
import { L2Character } from './L2Character'
import { L2PcInstance } from './instance/L2PcInstance'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { PathFinding } from '../../../geodata/PathFinding'

export class L2Tower extends L2Npc {
    constructor( template: L2NpcTemplate ) {
        super( template )

        this.instanceType = InstanceType.L2Tower
        this.setIsInvulnerable( false )
    }

    canBeAttacked(): boolean {
        return this.getCastle() && ( this.getCastle().getResidenceId() > 0 ) && this.getCastle().getSiege().isInProgress()
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        return attacker
                && attacker.isPlayer()
                && this.getCastle()
                && this.getCastle().getResidenceId() > 0
                && this.getCastle().getSiege().isInProgress()
                && this.getCastle().getSiege().checkIsAttacker( attacker.getClan() )
    }

    onInteraction( player: L2PcInstance, interact: boolean ): Promise<void> {
        if ( !this.canTarget( player ) ) {
            return
        }

        if ( this.getObjectId() !== player.getTargetId() ) {
            player.setTarget( this )

        } else if ( interact ) {
            if ( this.isAutoAttackable( player ) && ( Math.abs( player.getZ() - this.getZ() ) < 100 )
                    && PathFinding.canSeeTargetWithPosition( player, this ) ) {
                AIEffectHelper.notifyForceAttack( player, this )
            }
        }

        player.sendOwnedData( ActionFailed() )
    }

    onForcedAttack( player: L2PcInstance ) {
        return this.onInteraction( player, true )
    }
}