import { L2Character } from './L2Character'
import { L2NpcTemplate } from './templates/L2NpcTemplate'
import { InstanceType } from '../../enums/InstanceType'
import { ConfigManager } from '../../../config/ConfigManager'
import { AIType } from '../../enums/AIType'
import { L2World } from '../../L2World'
import { Castle } from '../entity/Castle'
import { CastleManager } from '../../instancemanager/CastleManager'
import { ShotType, ShotTypeHelper } from '../../enums/ShotType'
import { IDFactoryCache } from '../../cache/IDFactoryCache'
import { L2Item } from '../items/L2Item'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { ItemInstanceType } from '../items/ItemInstanceType'
import { L2Weapon } from '../items/L2Weapon'
import { DecayTaskManager } from '../../taskmanager/DecayTaskManager'
import { L2PcInstance } from './instance/L2PcInstance'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { DataManager } from '../../../data/manager'
import { MagicSkillUseWithCharacters } from '../../packets/send/MagicSkillUse'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { IBypassHandler } from '../../handler/IBypassHandler'
import { BypassManager } from '../../handler/managers/BypassManager'
import { OlympiadValues } from '../../values/OlympiadValues'
import { SiegableHall } from '../entity/clanhall/SiegableHall'
import { ClanHallSiegeManager } from '../../instancemanager/ClanHallSiegeManager'
import { Fort } from '../entity/Fort'
import { FortManager } from '../../instancemanager/FortManager'
import { NpcStats } from './stat/NpcStats'
import { ServerObjectInfoWithCharacters } from '../../packets/send/ServerObjectInfo'
import { NpcInfoWithCharacters } from '../../packets/send/NpcInfo'
import { ExChangeNpcState } from '../../packets/send/ExChangeNpcState'
import { ListenerCache } from '../../cache/ListenerCache'
import {
    CharacterStoppedMovingEvent,
    EventTerminationResult,
    EventType,
    NpcBypassEvent,
    NpcSpawnEvent,
    NpcTeleportEvent,
    TerminatedNpcStartsAttackEvent,
} from '../events/EventType'
import { TraitRunnerAIController } from '../../aicontroller/types/TraitRunnerAIController'
import { Race } from '../../enums/Race'
import { SocialAction } from '../../packets/send/SocialAction'
import { IPositionable, Location } from '../Location'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { NpcVariablesManager } from '../../variables/NpcVariablesManager'
import _ from 'lodash'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { Stats } from '../stats/Stats'
import { QuestHelper } from '../../../listeners/helpers/QuestHelper'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { PointGeometry } from '../drops/PointGeometry'
import { PathWalkingTrait } from '../../aicontroller/traits/npc/PathWalkingTrait'
import { ISpawnLogic } from '../spawns/ISpawnLogic'
import { NpcTemplateType } from '../../enums/NpcTemplateType'
import { L2NpcRouteType } from '../../../data/interface/NpcRoutesDataApi'
import { NpcWalkParameters } from '../NpcWalkParameters'
import { getDefaultDamageParameters } from '../PhysicalDamageParameters'
import { GeometryId } from '../../enums/GeometryId'
import { TraitSettings } from '../../aicontroller/interface/IAITrait'
import { AreaCache } from '../../cache/AreaCache'
import { TownArea } from '../areas/type/Town'
import { AreaType } from '../areas/AreaType'
import { AreaDiscoveryManager } from '../../cache/AreaDiscoveryManager'
import { CalculatorHelper } from '../../helpers/CalculatorHelper'
import Timeout = NodeJS.Timeout
import { SkillLoadStatus } from '../../enums/SkillLoadStatus'

export class L2Npc extends L2Character {
    npcSpawn: ISpawnLogic
    isBusyValue: boolean = false
    busyMessage: string
    isDecayedValue: boolean = false
    castleId: number = 0
    fortId: number
    eventMob: boolean = false
    isInTown: boolean = false
    isTalking: boolean = true

    currentEnchant: number
    currentCollisionRadius: number

    soulshotamount: number = 0
    spiritshotamount: number = 0
    displayEffect: number = 0

    shotsMask: number = 0
    killingBlowWeaponId: number
    summonedNpcs: Array<number>
    aiController: TraitRunnerAIController
    walkRoute: NpcWalkParameters = null
    defaultTraits: TraitSettings
    skillLoadStatus: SkillLoadStatus = SkillLoadStatus.Initialized
    summonerId: number = 0
    spawnLocation: Location = new Location( Number.MIN_SAFE_INTEGER, Number.MIN_SAFE_INTEGER, Number.MIN_SAFE_INTEGER )
    despawnTimer: Timeout

    constructor( template: L2NpcTemplate ) {
        super( IDFactoryCache.getNextId(), template )
        this.instanceType = InstanceType.L2Npc

        this.generateEnchantLevel()
        this.resetCollisionRadius()
        this.setIsFlying( template.isFlying() )

        AreaDiscoveryManager.addCharacter( this, false )
    }

    createMethods() {
        super.createMethods()

        this.debounceAreaRevalidation = _.debounce( this.runDirectAreaRevalidation.bind( this ), 300, {
            trailing: true,
            maxWait: 600,
        } )
    }

    createAIController(): TraitRunnerAIController {
        return new TraitRunnerAIController( this, this.defaultTraits )
    }

    getAIController(): TraitRunnerAIController {
        if ( !this.physicalDamageData ) {
            this.physicalDamageData = getDefaultDamageParameters()
        }

        if ( !this.aiController ) {
            this.aiController = this.createAIController()
        }

        return this.aiController
    }

    hasAIController() {
        return !!this.aiController
    }

    setDefaultTraits( traits: TraitSettings ) {
        this.defaultTraits = traits
    }

    setAIController( value: TraitRunnerAIController ): void {
        if ( !value ) {
            this.resetPhysicalDamageData()
        }

        let controller = this.aiController
        this.aiController = value

        if ( !controller ) {
            return
        }

        return controller.deactivate()
    }

    assignCalculators() {
        this.calculators = CalculatorHelper.getStdNPCCalculators()
    }

    addSummonedNpc( npc: L2Npc ) : void {
        if ( !this.summonedNpcs ) {
            this.summonedNpcs = []
        }

        this.summonedNpcs.push( npc.getObjectId() )
        npc.setSummoner( this.getObjectId() )
    }

    isMoveCapable () : boolean {
        return this.getTemplate().canMove()
    }

    canTarget( player: L2PcInstance ): boolean {
        if ( player.isOutOfControl() ) {
            player.sendOwnedData( ActionFailed() )
            return false
        }

        if ( player.isLockedTarget() && ( player.getLockedTarget().getObjectId() !== this.objectId ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_CHANGE_TARGET ) )
            player.sendOwnedData( ActionFailed() )
            return false
        }

        return true
    }

    async deleteMe() : Promise<void> {
        this.onDecay()

        if ( this.isChannelized() ) {
            this.getSkillChannelized().abortChannelization()
        }

        if ( this.npcSpawn ) {
            this.npcSpawn.remove( this )
        }

        this.setAIController( null )

        L2World.removeObject( this )

        return super.deleteMe()
    }

    protected dropItem( ownerObjectId: number, itemId: number, itemCount: number, geometry: PointGeometry ): L2ItemInstance {
        let template: L2Item = DataManager.getItems().getTemplate( itemId )
        if ( !template ) {
            return null
        }

        let item: L2ItemInstance
        let totalItems = ( template.isStackable() || !ConfigManager.general.multipleItemDrop() ) ? 1 : itemCount

        for ( let index = 0; index < totalItems; index++ ) {
            geometry.prepareNextPoint()

            item = ItemManagerCache.createLootItem( itemId, 'L2Npc.dropItem', itemCount, ownerObjectId, this )
            if ( !item ) {
                break
            }

            item.dropMe( ownerObjectId, this.getX() + geometry.getX(), this.getY() + geometry.getY(), this.getZ() + 20, this.getInstanceId(), true )
            item.setDeletionProtected( false )
        }

        return item
    }

    dropSingleItem( itemId: number, amount: number, ownerObjectId: number = 0 ) : L2ItemInstance {
        let geometry = PointGeometry.acquire( this.getGeometryId() )
        let item = this.dropItem( ownerObjectId, itemId, amount, geometry )

        geometry.release()

        return item
    }

    endDecayTask(): void {
        if ( !this.isDecayed() ) {
            DecayTaskManager.cancel( this )
            this.onDecay()
        }
    }

    /*
        TODO : consider removing and using skill based computed AI type
     */
    getAIType(): number {
        return this.getTemplate().getAIType()
    }


    getAggroRange() {
        let aggroRange = this.getTemplate().getAggroRange()
        if ( ConfigManager.npc.getMaxAggroRange() > 0 ) {
            return Math.min( aggroRange, ConfigManager.npc.getMaxAggroRange() )
        }

        return aggroRange
    }

    getBusyMessage() {
        return this.busyMessage
    }

    getCastle(): Castle {
        return CastleManager.getCastleById( this.castleId )
    }

    getCollisionHeight() {
        return this.getTemplate().getFCollisionHeight()
    }

    getCollisionRadius() {
        return this.currentCollisionRadius
    }

    getColorEffect() {
        return 0
    }

    getDisplayEffect() {
        return this.displayEffect
    }

    getEnchantEffect() {
        return this.currentEnchant
    }

    getExpReward() {
        return this.getTemplate().getExpReward() * ConfigManager.rates.getRateXp()
    }

    getHtmlPath( npcId: number, value: number ): string {
        let fileName: string = npcId.toString()

        if ( value !== 0 ) {
            fileName = `${ fileName }-${ value }`
        }

        let path: string = 'data/html/default/' + fileName + '.htm'

        if ( DataManager.getHtmlData().hasItem( path ) ) {
            return path
        }

        return 'data/html/npcdefault.htm'
    }

    getId(): number {
        return this.getTemplate().getId()
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        return false
    }

    isNpc(): boolean {
        return true
    }

    isTargetable() {
        return this.getTemplate().isTargetable()
    }

    setChargedShot( type: ShotType, charged: boolean ): void {
        if ( charged ) {
            this.shotsMask |= ShotTypeHelper.getMask( type )
        } else {
            this.shotsMask &= ~ShotTypeHelper.getMask( type )
        }
    }

    getSecondaryWeaponItem(): L2Weapon {
        // Get the weapon identifier equipped in the right hand of the L2NpcInstance
        let weaponId = this.getTemplate().getLHandId()
        if ( weaponId < 1 ) {
            return null
        }

        // Get the weapon item equipped in the right hand of the L2NpcInstance
        let item: L2Item = ItemManagerCache.getTemplate( weaponId )

        if ( !item.isInstanceType( ItemInstanceType.L2Weapon ) ) {
            return null
        }

        return item as L2Weapon
    }

    getTemplate(): L2NpcTemplate {
        return this.template as L2NpcTemplate
    }

    isMovementDisabled() {
        return super.isMovementDisabled() || !this.isMoveCapable() || this.getAIType() === AIType.CORPSE
    }

    onDecay() {
        if ( this.isDecayed() ) {
            return
        }

        this.debounceAreaRevalidation.cancel()
        this.isDecayedValue = true
        super.onDecay()

        if ( this.npcSpawn ) {
            this.npcSpawn.startDecay( this )
        }

        if ( !this.getSummonerId() ) {
            return
        }

        let summoner: L2Character = L2World.getObjectById( this.getSummonerId() ) as L2Character
        if ( summoner && summoner.isNpc() ) {
            ( summoner as L2Npc ).removeSummonedNpc( this.objectId )
        }
    }

    rechargeShots( isPhysical: boolean, isMagical: boolean ) : Promise<void> {
        if ( isPhysical && this.soulshotamount > 0 ) {
            if ( _.random( 100 ) > this.getSoulShotChance() ) {
                return
            }

            this.soulshotamount--

            BroadcastHelper.dataInLocation( this, MagicSkillUseWithCharacters( this, this, 2154, 1, 0, 0 ), 600 )
            this.setChargedShot( ShotType.Soulshot, true )
        }

        if ( isMagical && this.spiritshotamount > 0 ) {
            if ( _.random( 100 ) > this.getSpiritShotChance() ) {
                return
            }

            this.spiritshotamount--

            BroadcastHelper.dataInLocation( this, MagicSkillUseWithCharacters( this, this, 2061, 1, 0, 0 ), 600 )
            this.setChargedShot( ShotType.Spiritshot, true )
        }

        return Promise.resolve()
    }

    getLeftHandItemId() {
        return this.getTemplate().getLHandId()
    }

    getRightHandItemId() {
        return this.getTemplate().getRHandId()
    }

    getSoulShotChance() {
        return this.getTemplate().getSoulshotChance()
    }

    getSpReward(): number {
        return Math.floor( this.getTemplate().getSP() * ConfigManager.rates.getRateSp() )
    }

    getSpiritShotChance() {
        return this.getTemplate().getSpiritShotChance()
    }

    hasRandomAnimation(): boolean {
        /*
            TODO : use npc parameters to determine if random animation is possible and what value
            - MoveAroundSocial
            - MoveAroundSocial1
            - MoveAroundSocial2
         */
        return ConfigManager.general.getMaxNPCAnimation() > 0
                && this.hasRandomWalk()
                && !( this.getAIType() === AIType.CORPSE )
                && this.getRace() !== Race.ETC
    }

    isAggressive() {
        return false
    }

    isDecayed() {
        return this.isDecayedValue
    }

    isEventMob(): boolean {
        return this.eventMob
    }

    setEventMob( value: boolean ): void {
        this.eventMob = value
    }

    isShowName() {
        return this.getTemplate().isShowName()
    }

    isWarehouse() {
        return false
    }

    async onBypassFeedback( player: L2PcInstance, command: string ): Promise<void> {
        if ( ListenerCache.hasNpcTemplateListeners( this.getId(), EventType.NpcBypass ) ) {
            let parameters = EventPoolCache.getData( EventType.NpcBypass ) as NpcBypassEvent

            parameters.npcId = this.getId()
            parameters.command = command
            parameters.playerId = player.getObjectId()
            parameters.originatorId = this.getObjectId()
            parameters.instanceType = this.getInstanceType()
            parameters.isBusy = this.isBusy()

            let result = await ListenerCache.getNpcTerminatedResult( this, player, EventType.NpcBypass, parameters )
            if ( result && result.terminate ) {
                return
            }
        }

        if ( this.isBusy() ) {
            return this.showBusyMessage( player )
        }

        let handler: IBypassHandler = BypassManager.getHandler( command )
        if ( handler ) {
            await handler.onStart( command, player, this )
        }
    }

    removeSummonedNpc( objectId: number ) {
        _.pull( this.summonedNpcs, objectId )
    }

    resetSummonedNpcs() {
        this.summonedNpcs = null
    }

    scheduleDespawn( timeMs: number ) : void {
        if ( this.despawnTimer ) {
            clearTimeout( this.despawnTimer )
        }

        this.despawnTimer = setTimeout( this.runDespawnTask.bind( this ), timeMs )
    }

    runDespawnTask() : Promise<void> {
        if ( !this.isDecayed() ) {
            return this.deleteMe()
        }
    }

    setCollisionRadius( value: number ) {
        this.currentCollisionRadius = value
    }

    setDecayed( value: boolean ) {
        this.isDecayedValue = value
    }

    showChatWindowWithPath( player: L2PcInstance, path: string ) {
        let html = DataManager.getHtmlData().getItem( path )
        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), this.getObjectId() ) )
        player.sendOwnedData( ActionFailed() )
    }

    showChatWindow( player: L2PcInstance, value: number = 0 ): void {
        if ( !this.isTalking ) {
            player.sendOwnedData( ActionFailed() )
            return
        }

        let target = player.getTarget()
        if ( player.isCursedWeaponEquipped()
                && ( target && ( !target.isInstanceType( InstanceType.L2ClanHallManagerInstance ) || !target.isInstanceType( InstanceType.L2DoormenInstance ) ) ) ) {
            player.setTarget( player )
            return
        }

        if ( player.getKarma() > 0 ) {
            if ( !ConfigManager.character.karmaPlayerCanShop() && this.isMerchant() ) {
                if ( this.showPkDenyChatWindow( player, 'merchant' ) ) {
                    return
                }
            } else if ( !ConfigManager.character.karmaPlayerCanUseGK() && ( this.isInstanceType( InstanceType.L2TeleporterInstance ) ) ) {
                if ( this.showPkDenyChatWindow( player, 'teleporter' ) ) {
                    return
                }
            } else if ( !ConfigManager.character.karmaPlayerCanUseWareHouse() && ( this.isInstanceType( InstanceType.L2WarehouseInstance ) ) ) {
                if ( this.showPkDenyChatWindow( player, 'warehouse' ) ) {
                    return
                }
            }
        }

        // TODO : review and extract/move into auctioneer
        if ( this.getTemplate().getType() === NpcTemplateType.L2Auctioneer && value === 0 ) {
            return
        }

        let npcId = this.getTemplate().getId()

        let path: string = ''

        switch ( npcId ) {
            case 31688:
                if ( player.isNoble() ) {
                    path = OlympiadValues.OLYMPIAD_HTML_PATH + 'noble_main.htm'
                } else {
                    path = this.getHtmlPath( npcId, value )
                }
                break

            case 31690:
            case 31769:
            case 31770:
            case 31771:
            case 31772:
                if ( player.isHero() || player.isNoble() ) {
                    path = OlympiadValues.OLYMPIAD_HTML_PATH + 'hero_main.htm'
                } else {
                    path = this.getHtmlPath( npcId, value )
                }
                break

            case 36402:
                if ( player.getOlympiadBuffCount() > 0 ) {
                    path = player.getOlympiadBuffCount() === ConfigManager.olympiad.getMaxBuffs() ? OlympiadValues.OLYMPIAD_HTML_PATH + 'olympiad_buffs.htm' : OlympiadValues.OLYMPIAD_HTML_PATH + 'olympiad_5buffs.htm'
                } else {
                    path = OlympiadValues.OLYMPIAD_HTML_PATH + 'olympiad_nobuffs.htm'
                }
                break

            case 30298: // Blacksmith Pinter
                if ( player.isAcademyMember() ) {
                    path = this.getHtmlPath( npcId, 1 )
                } else {
                    path = this.getHtmlPath( npcId, value )
                }
                break

            default:
                if ( ( npcId >= 31093 && npcId <= 31094 )
                        || ( npcId >= 31172 && npcId <= 31201 )
                        || ( npcId >= 31239 && npcId <= 31254 ) ) {
                    return
                }

                path = this.getHtmlPath( npcId, value )
                break
        }

        let html: string = DataManager.getHtmlData().getItem( path )
        if ( this.isMerchant() && ConfigManager.general.allowRentPet() && ConfigManager.npc.getPetRentNPCs().includes( npcId ) ) {
            html = html.replace( ' Quest', ' RentPet">Rent Pet</a><br><a action="bypass -h Quest' )
            path = ''
        }

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), this.getObjectId() ) )
        player.sendOwnedData( ActionFailed() )
    }

    showHtml( player: L2PcInstance, path: string ) {
        let html = DataManager.getHtmlData().getItem( path )
        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), this.getObjectId() ) )
        player.sendOwnedData( ActionFailed() )
    }

    showNoTeachHtml( player: L2PcInstance ) {
        let npcId = this.getId()
        let html: string
        let htmlPath: string

        if ( this.isInstanceType( InstanceType.L2WarehouseInstance ) ) {
            htmlPath = `data/html/warehouse/${ npcId }-noteach.htm`
            html = DataManager.getHtmlData().getItem( htmlPath )
        } else if ( this.isInstanceType( InstanceType.L2TrainerInstance ) ) {
            htmlPath = `data/html/trainer/${ npcId }-noteach.htm`

            if ( !DataManager.getHtmlData().hasItem( htmlPath ) ) {
                htmlPath = `datapack/ai/npc/Trainers/HealerTrainer/${ npcId }-noteach.html`
            }

            html = DataManager.getHtmlData().getItem( htmlPath )
        }

        if ( !html ) {
            html = '<html><body>I cannot teach you any skills.<br>You must find your current class teachers.</body></html>'
            htmlPath = null
        }

        player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), this.getObjectId() ) )
    }

    showPkDenyChatWindow( player: L2PcInstance, messageType: string ): boolean {
        let path = `data/html/${ messageType }/${ this.getId() }-pk.htm`

        if ( DataManager.getHtmlData().hasItem( path ) ) {
            player.sendOwnedData( NpcHtmlMessagePath( QuestHelper.getNoQuestMessage(), path, player.getObjectId(), this.getObjectId() ) )
            player.sendOwnedData( ActionFailed() )
            return true
        }

        return false
    }

    onRandomAnimation( id: number ): void {
        BroadcastHelper.dataInLocation( this, SocialAction( this.getObjectId(), id ), this.getBroadcastRadius() )
    }

    isInMyClan( npc: L2Npc ): boolean {
        return this.getTemplate().isClan( npc.getTemplate().getClans() )
    }

    getConquerableHall(): SiegableHall {
        return ClanHallSiegeManager.getNearbyClanHallWithCoordinates( this.getX(), this.getY(), 10000 )
    }

    isBusy() {
        return this.isBusyValue
    }

    setIsBusy( value: boolean ): void {
        this.isBusyValue = value
    }

    getFort(): Fort {
        if ( !this.fortId ) {
            this.fortId = FortManager.findNearestFort( this )?.getResidenceId()
        }

        if ( !this.fortId ) {
            return null
        }

        return FortManager.getFortById( this.fortId )
    }

    getStat(): NpcStats {
        return super.getStat() as NpcStats
    }

    createStats(): NpcStats {
        return new NpcStats( this )
    }

    shouldDescribeState( player: L2PcInstance ): boolean {
        if ( this.isVisibleFor( player ) ) {
            let method = this.getRunSpeed() === 0 ? ServerObjectInfoWithCharacters : NpcInfoWithCharacters
            player.sendOwnedData( method( this, player ) )
            return true
        }

        return false
    }

    getLevel(): number {
        return this.getTemplate().getLevel()
    }

    onSpawn() {
        AreaDiscoveryManager.findAreas( this )

        this.spawnLocation.copy( this )
        this.soulshotamount = this.getTemplate().getSoulShot()
        this.spiritshotamount = this.getTemplate().getSpiritShot()
        this.killingBlowWeaponId = 0

        if ( this.hasCastleInfo() ) {
            this.assignCastleInfo()
        }

        if ( this.hasLoadedSkills() ) {
            let region = this.getWorldRegion()
            if ( region && region.isActive() ) {
                this.getAIController().activate()
            }
        }

        if ( this.isTeleporting() ) {
            let eventData: NpcTeleportEvent = {
                characterId: this.getObjectId(),
                npcId: this.getId(),
            }

            ListenerCache.sendNpcTemplateEvent( this.getId(), EventType.NpcTeleport, eventData )
            return
        }

        if ( ListenerCache.hasNpcTemplateListeners( this.getId(), EventType.NpcSpawn ) ) {
            let data = EventPoolCache.getData( EventType.NpcSpawn ) as NpcSpawnEvent

            data.characterId = this.getObjectId()
            data.npcId = this.getId()

            ListenerCache.sendNpcTemplateEvent( this.getId(), EventType.NpcSpawn, data )
        }
    }

    deactivateAI(): void {
        if ( !this.hasAIController() ) {
            return
        }

        return this.setAIController( null )
    }

    hasWalkingRoute(): boolean {
        return !!this.walkRoute
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( !await super.doDie( killer ) ) {
            return false
        }

        this.resetCollisionRadius()
        this.generateEnchantLevel()

        let weapon: L2Weapon = killer ? killer.getActiveWeaponItem() : null
        this.killingBlowWeaponId = weapon ? weapon.getId() : 0

        DecayTaskManager.add( this )
        return true
    }

    isChaos() {
        return this.getTemplate().isChaos()
    }

    showChatWindowDefault( player: L2PcInstance ): void {
        return this.showChatWindow( player, 0 )
    }

    isChargedShot( type: ShotType ): boolean {
        return ( this.shotsMask & ShotTypeHelper.getMask( type ) ) === ShotTypeHelper.getMask( type )
    }

    canBeAttacked(): boolean {
        return ConfigManager.npc.attackableNpcs()
    }

    getName(): string {
        return this.getTemplate().getName()
    }

    getActiveWeaponInstance(): L2ItemInstance {
        return null
    }

    getActiveWeaponItem(): L2Weapon {
        let weaponId: number = this.getTemplate().getRHandId()

        if ( weaponId < 1 ) {
            return null
        }

        let item: L2Item = DataManager.getItems().getTemplate( weaponId )

        if ( !item || !item.isInstanceType( ItemInstanceType.L2Weapon ) ) {
            return null
        }

        return item as L2Weapon
    }

    setDisplayEffect( value: number ) {
        if ( this.displayEffect !== value ) {
            this.displayEffect = value
            BroadcastHelper.dataInLocation( this, ExChangeNpcState( this.getObjectId(), value ), this.getBroadcastRadius() )
        }
    }

    isLord( player: L2PcInstance ): boolean {
        if ( player.isClanLeader() ) {
            let castleId = this.getCastle() ? this.getCastle().getResidenceId() : -1
            let fortId = this.getFort() ? this.getFort().getResidenceId() : -1
            return player.getClan().getCastleId() === castleId || player.getClan().getFortId() === fortId
        }

        return false
    }

    isSevenSignsNpc(): boolean {
        return this.getTemplate().isSevenSignsNpc()
    }

    getWalkRoute(): NpcWalkParameters {
        return this.walkRoute
    }

    async startWalkingRoute( name: string, routeType: L2NpcRouteType = L2NpcRouteType.Follow ): Promise<void> {
        if ( this.isDead() ) {
            return
        }

        let route = DataManager.getNpcRoutes().getRouteByName( name )
        if ( !route ) {
            return
        }

        this.walkRoute = {
            forward: false,
            route,
            index: 0,
            routeType
        }

        if ( this.hasAIController() && this.getAIController().isActive() ) {
            this.getAIController().deactivate()
        }

        if ( this.defaultTraits && this.defaultTraits[ AIIntent.WAITING ] !== PathWalkingTrait ) {
            this.defaultTraits = {
                ...this.defaultTraits,
                [ AIIntent.WAITING ]: PathWalkingTrait
            }
        }

        this.setAIController( null )
        return this.getAIController().activate()
    }

    getKillingBlowWeapon(): number {
        return this.killingBlowWeaponId
    }

    getSummonedNpcCount(): number {
        return _.size( this.summonedNpcs )
    }

    isDominionOfLord( id: number ): boolean {
        let castle = this.getCastle()
        let castleId = castle ? castle.getResidenceId() : -1
        if ( id === castleId ) {
            return true
        }

        let fort = this.getFort()
        let fortId = fort ? fort.getResidenceId() : -1
        return id === fortId
    }

    setTalking( value: boolean ): void {
        this.isTalking = value
    }

    setIsMortal( value: boolean ): void {
        this.isMortalValue = value
    }

    canGreetPlayer(): boolean {
        return this.getTemplate().canGreetPlayer()
    }

    refreshId() {
        NpcVariablesManager.emptyData( this.getObjectId() )
        L2World.removeObject( this )
        IDFactoryCache.releaseId( this.getObjectId() )

        this.objectId = IDFactoryCache.getNextId()
        this.onObjectIdChanged()

        if ( this.hasAIController() ) {
            this.getAIController().ownerId = this.getObjectId()
        }
    }

    async prepareSkills(): Promise<void> {
        if ( this.hasLoadedSkills() ) {
            return
        }

        this.skillLoadStatus = SkillLoadStatus.Loading

        await this.loadTemplateSkills( this.template as L2NpcTemplate )
        AreaDiscoveryManager.addCharacter( this, false )

        /*
            Direct Hp/Mp operation is shorter form without any additional checks.
            Normally skills are prepared only when new character has been spawned.
            Hence, additional checks may complicate things when repeated assignment
            of values is happening elsewhere in spawn chain.
         */
        this.getStatus().setDirectHp( this.getMaxHp() )
        this.getStatus().setDirectMp( this.getMaxMp() )

        this.skillLoadStatus = SkillLoadStatus.Finished
    }

    hasLoadedSkills(): boolean {
        return this.skillLoadStatus === SkillLoadStatus.Finished
    }

    sendStopMovingEvent(): void {
        if ( ListenerCache.hasNpcTemplateListeners( this.getId(), EventType.CharacterStoppedMoving ) ) {
            let data = EventPoolCache.getData( EventType.CharacterStoppedMoving ) as CharacterStoppedMovingEvent

            data.characterId = this.getObjectId()
            data.npcId = this.getId()
            data.x = this.getX()
            data.y = this.getY()
            data.z = this.getZ()
            data.instanceId = this.getInstanceId()

            ListenerCache.sendNpcTemplateEvent( this.getId(), EventType.CharacterStoppedMoving, data )
        }
    }

    broadcastModifiedStats(): void {
        let broadcastFullUpdate = this.modifiedStats.has( Stats.MOVE_SPEED )

        if ( broadcastFullUpdate ) {
            let players: Array<L2PcInstance> = L2World.getVisiblePlayers( this, this.getBroadcastRadius() )
            let isStationary: boolean = this.getRunSpeed() === 0

            players.forEach( ( otherPlayer: L2PcInstance ) => {
                if ( !otherPlayer || !this.isVisibleFor( otherPlayer ) ) {
                    return
                }

                if ( isStationary ) {
                    return otherPlayer.sendOwnedData( ServerObjectInfoWithCharacters( this, otherPlayer ) )
                }

                otherPlayer.sendOwnedData( NpcInfoWithCharacters( this, otherPlayer ) )
            } )

            this.modifiedStats.clear()
            return
        }

        this.broadcastStatusAttributes()
    }

    async doAttack( target: L2Character ): Promise<void> {
        if ( ListenerCache.hasNpcTemplateListeners( this.getId(), EventType.TerminatedNpcStartsAttack ) ) {

            let eventData = EventPoolCache.getData( EventType.TerminatedNpcStartsAttack ) as TerminatedNpcStartsAttackEvent

            eventData.attackerId = this.getObjectId()
            eventData.targetId = target.getObjectId()
            eventData.npcId = this.getId()
            eventData.targetType = target.getInstanceType()

            let result: EventTerminationResult = await ListenerCache.getNpcTerminatedResult( this, this.getActingPlayer(), EventType.TerminatedNpcStartsAttack, eventData )
            if ( result && result.terminate ) {
                return AIEffectHelper.notifyTargetDead( this, target )
            }
        }

        return super.doAttack( target )
    }

    showBusyMessage( player: L2PcInstance ): void {
        let message = this.getBusyMessage()
        if ( !message ) {
            return
        }

        let path = 'data/html/npcbusy.htm'
        let html: string = DataManager.getHtmlData().getItem( path )
                                      .replace( /%busymessage%/g, message )
                                      .replace( /%npcname%/g, this.getName() )
                                      .replace( /%playername%/g, player.getName() )

        return player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), this.getObjectId() ) )
    }

    setIsRunning( value: boolean, shouldSendUpdate: boolean = true ): void {
        if ( !this.hasLoadedSkills() || !this.getWorldRegion() ) {
            this.isRunningValue = value
            return
        }

        super.setIsRunning( value, shouldSendUpdate )

        if ( !shouldSendUpdate ) {
            return
        }

        let players: Array<L2PcInstance> = L2World.getVisiblePlayers( this, this.getBroadcastRadius() )

        if ( players.length === 0 ) {
            return
        }

        let currentSpeed = this.getRunSpeed()
        for ( const currentPlayer of players ) {
            if ( !currentPlayer || !this.isVisibleFor( currentPlayer ) ) {
                return
            }

            if ( currentSpeed === 0 ) {
                return currentPlayer.sendOwnedData( ServerObjectInfoWithCharacters( this, currentPlayer ) )
            }

            currentPlayer.sendOwnedData( NpcInfoWithCharacters( this, currentPlayer ) )
        }
    }

    getGeneratedTitle() : string {
        if ( ConfigManager.npc.showNpcCustomTitle() && this.isMonster() ) {
            if ( !this.getTemplate().getCustomTitle() ) {
                let title = ConfigManager.npc.showNpcCustomTitleTemplate()
                    .replace( /#level#/g, this.getLevel().toString() )
                    .replace( /#agressive#/g, this.isAggressive() ? ConfigManager.npc.showNpcCustomTitleAggressiveMark() : '' )
                    .replace( /#title#/g, this.getTitle() )
                    .replace( /#maxhp#/g, this.getMaxHp().toString() )
                    .replace( /#maxmp#/g, this.getMaxMp().toString() )
                    .trim()

                this.getTemplate().setCustomTitle( title )
            }

            return this.getTemplate().getCustomTitle()
        }

        if ( this.isInvisible() ) {
            return 'Invisible'
        }

        if ( ConfigManager.customs.championEnable() && this.isChampion() ) {
            return ConfigManager.customs.getChampionTitle()
        }

        if ( this.getTemplate().isUsingServerSideTitle() ) {
            return this.getTemplate().getTitle()
        }

        return this.getTitle()
    }

    getSummonerId(): number {
        return this.summonerId
    }

    setSummoner( objectId: number ) {
        this.summonerId = objectId
    }

    async runPhysicalDamageTask(): Promise<void> {
        let parameters = this.physicalDamageData.hits.pop()
        if ( !parameters ) {
            return
        }

        let isMiss = parameters.isMiss
        let isCrit = parameters.isCrit
        let target = parameters.target

        if ( isMiss ) {
            this.notifyAttackAvoided( target )
        }

        await this.rechargeShots( true, false )

        if ( this.isTrap() ) {
            this.sendDamageMessage( target, parameters.damage, false, isCrit, isMiss )
        }

        if ( isMiss || parameters.damage < 1 ) {
            return
        }

        return this.applyDamageToTarget( target, parameters.damage, isCrit )
    }

    setNpcSpawn( spawn: ISpawnLogic ) : void {
        this.npcSpawn = spawn
        this.setWalkRoute()
    }

    private setWalkRoute(): string {
        let spawnData = this.getNpcSpawn()?.getSpawnData()
        if ( !spawnData ) {
            return
        }

        let route = DataManager.getNpcRoutes().getRouteByName( spawnData.aiParameters?.superPointName as string ) ?? null
        if ( !route ) {
            return
        }

        this.walkRoute = {
            forward: false,
            route,
            index: 0,
            routeType: ( spawnData.aiParameters?.superPointMethod as string ?? route.type ) as L2NpcRouteType
        }
    }

    getNpcSpawn() {
        return this.npcSpawn
    }

    getSpawnLocation() : IPositionable {
        return this.spawnLocation
    }

    hasRandomWalk(): boolean {
        return this.getTemplate().moveGeometryId !== GeometryId.None
    }

    async onTeleported(): Promise<void> {
        if ( ListenerCache.hasNpcTemplateListeners( this.getId(), EventType.NpcTeleport ) ) {
            let data = EventPoolCache.getData( EventType.NpcTeleport ) as NpcTeleportEvent

            data.characterId = this.getObjectId()
            data.npcId = this.getId()

            await ListenerCache.sendNpcTemplateEvent( this.getId(), EventType.NpcTeleport, data )
        }

        return super.onTeleported()
    }

    generateEnchantLevel() : void {
        this.currentEnchant = ConfigManager.npc.randomEnchantEffect() ? _.random( 4, 21 ) : this.getTemplate().getWeaponEnchant()
    }

    resetCollisionRadius() : void {
        this.currentCollisionRadius = this.getTemplate().getFCollisionRadius()
    }

    hasCastleInfo() : boolean {
        return true
    }

    assignCastleInfo() : void {
        let town: TownArea = AreaCache.getFirstAreaForType( this, AreaType.Town ) as TownArea

        if ( town ) {
            this.castleId = town.properties.taxById
            this.isInTown = true

            return
        }

        this.isInTown = false
        this.castleId = CastleManager.findNearestCastleId( this )
    }

    getClassesToTeach(): Array<number> {
        return this.getTemplate().getTeachableClassIds()
    }

    runDirectAreaRevalidation(): void {
        AreaDiscoveryManager.findAreas( this )
    }

    isInArea( type: AreaType ) : boolean {
        return AreaDiscoveryManager.hasAreaType( this.getObjectId(), type )
    }

    revalidateZone( force: boolean ) {
        if ( !this.getWorldRegion() ) {
            return
        }

        if ( force ) {
            this.debounceAreaRevalidation.cancel()
            this.runDirectAreaRevalidation()
            return
        }

        this.debounceAreaRevalidation()
    }
}