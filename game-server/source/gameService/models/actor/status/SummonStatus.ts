import { L2Summon } from '../L2Summon'
import { L2Character } from '../L2Character'
import { L2PcInstance } from '../instance/L2PcInstance'
import { DuelState } from '../../../enums/DuelState'
import { GeneralHelper } from '../../../helpers/GeneralHelper'
import { Stats } from '../../stats/Stats'
import { L2World } from '../../../L2World'
import { L2Party } from '../../L2Party'
import { CharacterStatus } from './CharacterStatus'

export class SummonStatus extends CharacterStatus {

    character : L2Summon

    async reduceHp( value: number, attacker: L2Character, isAwake: boolean, isDOT: boolean, isHpConsumed: boolean ): Promise<void> {
        if ( !attacker || this.character.isDead() ) {
            return
        }

        let attackerPlayer: L2PcInstance = attacker.getActingPlayer()
        if ( attackerPlayer && ( !this.character.getOwner() || this.character.getOwner().getDuelId() !== attackerPlayer.getDuelId() ) ) {
            attackerPlayer.setDuelState( DuelState.Interrupted )
        }

        let caster: L2PcInstance = this.character.getTransferringDamageTo()

        if ( !caster ) {
            return super.reduceHp( value, attacker, isAwake, isDOT, isHpConsumed )
        }

        let party : L2Party = this.character.getParty()
        if ( party
                && GeneralHelper.checkIfInRange( 1000, this.character, caster, true )
                && !caster.isDead()
                && party.getMembers().includes( caster.getObjectId() ) ) {
            let damage = ( value * this.character.getStat().calculateStat( Stats.TRANSFER_DAMAGE_TO_PLAYER, 0, null, null ) ) / 100
            damage = Math.floor( Math.min( caster.getCurrentHp() - 1, damage ) )

            if ( damage > 0 ) {
                if ( attacker && attacker.isPlayable() && caster.getCurrentCp() > 0 ) {
                    if ( caster.getCurrentCp() > damage ) {
                        caster.getStatus().reduceCp( damage )
                    } else {
                        damage = Math.floor( damage - caster.getCurrentCp() )
                        caster.getStatus().reduceCp( caster.getCurrentCp() )
                    }
                }
                let objectIds: Array<number> = L2World.getVisiblePlayerIdsByPredicate( caster, 1000, ( playerId: number ) => caster.getParty().getMembers().includes( playerId ) )
                if ( objectIds.length > 0 ) {
                    await caster.reduceCurrentHp( Math.floor( damage / objectIds.length ), attacker, null )
                    value -= damage
                }
            }

            return super.reduceHp( value, attacker, isAwake, isDOT, isHpConsumed )
        }

        if ( !caster.isDead()
                && caster.getObjectId() === this.character.getOwnerId()
                && GeneralHelper.checkIfInRange( 1000, this.character, caster, true ) ) {
            let damage: number = ( value * this.character.getStat().calculateStat( Stats.TRANSFER_DAMAGE_TO_PLAYER, 0, null, null ) ) / 100
            damage = Math.floor( Math.min( caster.getCurrentHp() - 1, damage ) )

            if ( damage > 0 ) {
                if ( attacker
                        && attacker.isPlayable()
                        && ( caster.getCurrentCp() > 0 ) ) {
                    if ( caster.getCurrentCp() > damage ) {
                        caster.getStatus().reduceCp( damage )
                    } else {
                        damage = Math.floor( damage - caster.getCurrentCp() )
                        caster.getStatus().reduceCp( caster.getCurrentCp() )
                    }
                }

                await caster.reduceCurrentHp( damage, attacker, null )
                value -= damage
            }
        }

        return super.reduceHp( value, attacker, isAwake, isDOT, isHpConsumed )
    }

    notifyHpChanged() : void {
        let player = this.character.getOwner()
        if ( player ) {
            return player.activateCubicsOnHpDamage()
        }
    }
}