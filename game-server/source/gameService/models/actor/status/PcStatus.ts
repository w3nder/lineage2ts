import { CharacterStatus } from './CharacterStatus'
import { L2PcInstance } from '../instance/L2PcInstance'
import { L2Character } from '../L2Character'
import { ConfigManager } from '../../../../config/ConfigManager'
import { L2GameClientRegistry } from '../../../L2GameClientRegistry'
import { PrivateStoreType } from '../../../enums/PrivateStoreType'
import { DuelState } from '../../../enums/DuelState'
import { L2Summon } from '../L2Summon'
import { GeneralHelper } from '../../../helpers/GeneralHelper'
import { Stats } from '../../stats/Stats'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { L2World } from '../../../L2World'
import { DuelManager } from '../../../instancemanager/DuelManager'
import { Formulas } from '../../stats/Formulas'
import { EventTerminationResult, EventType, PlayerDieEvent } from '../../events/EventType'
import { ListenerCache } from '../../../cache/ListenerCache'
import { AIEffectHelper } from '../../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../../aicontroller/enums/AIIntent'
import { EventPoolCache } from '../../../cache/EventPoolCache'
import _ from 'lodash'
import { PlayerPermission } from '../../../enums/PlayerPermission'
import { RegenerationFlags } from '../../../enums/CharacterRegeneration'

export class PcStatus extends CharacterStatus {
    currentCp: number = 0
    character: L2PcInstance

    getCurrentCp(): number {
        return this.currentCp
    }

    async reduceHp( value: number, attacker: L2Character, isAwake: boolean, isDOT: boolean, isHpConsumed: boolean ): Promise<void> {
        return this.reduceHpWithCp( value, attacker, isAwake, isDOT, isHpConsumed, false )
    }

    async reduceHpWithCp( value: number, attacker: L2Character, isAwake: boolean, isDOT: boolean, isHpConsumed: boolean, ignoreCp: boolean = false ): Promise<void> {
        if ( this.character.isDead() ) {
            return
        }

        /*
            TODO : ensure that offline check is performed before attack is possible
            - inspect target handler and check for offline player there
            - check here can be for offline client, since if hp/cp reduction is possible it must have been "approved" earlier
         */
        let client = L2GameClientRegistry.getClientByPlayerId( this.character.getObjectId() )
        if ( client
                && client.isDetached()
                && ConfigManager.customs.offlineModeNoDamage()
                && ( ( ConfigManager.customs.offlineTradeEnable()
                                && ( ( this.character.getPrivateStoreType() === PrivateStoreType.Sell )
                                        || ( this.character.getPrivateStoreType() === PrivateStoreType.Buy ) ) )
                        || ( ConfigManager.customs.offlineCraftEnable()
                                && ( this.character.isInCraftMode() || ( this.character.getPrivateStoreType() === PrivateStoreType.Manufacture ) ) ) ) ) {
            return
        }

        if ( ( this.character.isInvulnerable() || this.character.isHpBlocked() ) && !( isDOT || isHpConsumed ) ) {
            return
        }

        if ( !isHpConsumed ) {
            await this.character.stopEffectsOnDamage( isAwake )
            if ( this.character.isInCraftMode() || this.character.isInStoreMode() ) {
                this.character.setPrivateStoreType( PrivateStoreType.None )
                await this.character.standUp()
                this.character.broadcastUserInfo()
            } else if ( this.character.isSitting() ) {
                await this.character.standUp()
            }

            if ( !isDOT ) {
                if ( this.character.isStunned() && ( _.random( 10 ) === 0 ) ) {
                    await this.character.stopStunning( true )
                }
            }
        }

        let transferDamage = 0

        if ( attacker && ( attacker.getObjectId() !== this.character.getObjectId() ) ) {
            let attackerPlayer: L2PcInstance = attacker.getActingPlayer()
            let transferDamageValue = value

            if ( attackerPlayer ) {
                if ( attackerPlayer.isGM() && !attackerPlayer.getAccessLevel().hasPermission( PlayerPermission.GiveDamage ) ) {
                    return
                }

                if ( this.character.isInDuel() ) {
                    if ( this.character.getDuelState() === DuelState.Dead || DuelState.Winner === this.character.getDuelState() ) {
                        return
                    }

                    if ( attackerPlayer.getDuelId() !== this.character.getDuelId() ) {
                        this.character.setDuelState( DuelState.Interrupted )
                    }
                }
            }

            let summon: L2Summon = this.character.getSummon()
            if ( this.character.hasServitor() && GeneralHelper.checkIfInRange( 1000, this.character, summon, true ) ) {
                transferDamage = Math.floor( value * this.character.getStat().calculateStat( Stats.TRANSFER_DAMAGE_PERCENT, 0, null, null ) ) / 100

                transferDamage = Math.min( Math.floor( summon.getCurrentHp() ) - 1, transferDamage )
                if ( transferDamage > 0 ) {
                    await summon.reduceCurrentHp( transferDamage, attacker, null )
                    value -= transferDamage
                    transferDamageValue = value
                }
            }

            let mpDam = Math.floor( ( value * this.character.getStat().calculateStat( Stats.MANA_SHIELD_PERCENT, 0, null, null ) ) / 100 )
            if ( mpDam > 0 ) {
                mpDam = Math.floor( value - mpDam )
                if ( mpDam > this.character.getCurrentMp() ) {
                    this.character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MP_BECAME_0_ARCANE_SHIELD_DISAPPEARING ) )
                    await this.character.stopSkillEffects( true, 1556 )
                    value = mpDam - this.character.getCurrentMp()
                    this.character.setCurrentMp( 0 )
                } else {
                    this.character.reduceCurrentMp( mpDam )

                    let packet = new SystemMessageBuilder( SystemMessageIds.ARCANE_SHIELD_DECREASED_YOUR_MP_BY_S1_INSTEAD_OF_HP )
                            .addNumber( mpDam )
                            .getBuffer()
                    this.character.sendOwnedData( packet )
                    return
                }
            }

            let caster: L2PcInstance = this.character.getTransferringDamageTo()
            if ( caster
                    && this.character.getParty()
                    && GeneralHelper.checkIfInRange( 1000, this.character, caster, true )
                    && !caster.isDead()
                    && ( this.character.getObjectId() !== caster.getObjectId() )
                    && this.character.getParty().getMembers().includes( caster.getObjectId() ) ) {
                let transferDmg = Math.floor( ( value * this.character.getStat().calculateStat( Stats.TRANSFER_DAMAGE_TO_PLAYER, 0, null, null ) ) / 100 )
                transferDmg = Math.min( Math.floor( caster.getCurrentHp() ) - 1, transferDmg )
                if ( transferDmg > 0 ) {
                    let membersInRange = 0

                    caster.getParty().getMembers().forEach( ( memberId: number ) => {
                        let member = L2World.getPlayer( memberId )
                        if ( GeneralHelper.checkIfInRange( 1000, member, caster, false ) && ( memberId !== caster.getObjectId() ) ) {
                            membersInRange++
                        }
                    } )

                    if ( attacker.isPlayable() && ( caster.getCurrentCp() > 0 ) ) {
                        if ( caster.getCurrentCp() > transferDmg ) {
                            caster.getStatus().reduceCp( transferDmg )
                        } else {
                            transferDmg = Math.floor( transferDmg - caster.getCurrentCp() )
                            caster.getStatus().reduceCp( Math.floor( caster.getCurrentCp() ) )
                        }
                    }

                    if ( membersInRange > 0 ) {
                        await caster.reduceCurrentHp( transferDmg / membersInRange, attacker, null )
                        value -= transferDmg
                        transferDamageValue = value
                    }
                }
            }

            if ( !ignoreCp && attacker.isPlayable() ) {
                if ( this.getCurrentCp() >= value ) {
                    this.setCurrentCp( this.getCurrentCp() - value )
                    value = 0
                } else {
                    value -= this.getCurrentCp()
                    this.setCurrentCp( 0 )
                }
            }

            if ( transferDamageValue > 0 && !isDOT ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.C1_RECEIVED_DAMAGE_OF_S3_FROM_C2 )
                        .addPlayerCharacterName( this.character )
                        .addCharacterName( attacker )
                        .addNumber( transferDamageValue )
                        .getBuffer()
                this.character.sendOwnedData( packet )

                if ( ( transferDamage > 0 ) && attackerPlayer ) {
                    let attackerPacket = new SystemMessageBuilder( SystemMessageIds.GIVEN_S1_DAMAGE_TO_YOUR_TARGET_AND_S2_DAMAGE_TO_SERVITOR )
                            .addNumber( transferDamageValue )
                            .addNumber( transferDamage )
                            .getBuffer()
                    attackerPlayer.sendOwnedData( attackerPacket )
                }
            }
        }

        if ( value <= 0 ) {
            return
        }

        let currentHp = this.getCurrentHp()
        let modifiedHp = currentHp - value
        if ( modifiedHp <= 0 ) {
            if ( this.character.isInDuel() ) {
                this.character.disableAllSkills()
                this.stopHpMpRegeneration()

                if ( attacker ) {
                    AIEffectHelper.notifyTargetDead( attacker, this.character )
                }

                DuelManager.onPlayerDefeat( this.character )
                modifiedHp = 1
            } else {
                modifiedHp = 0
            }
        }

        this.setCurrentHp( modifiedHp )

        if ( modifiedHp < 0.5 && !isHpConsumed ) {
            this.character.abortAttack()
            this.character.abortCast( false )

            if ( this.character.isInOlympiadMode() ) {
                this.stopHpMpRegeneration()
                this.character.setIsDead( true )
                this.character.setIsPendingRevive( true )

                await AIEffectHelper.setNextIntent( this.character, AIIntent.WAITING )

                if ( this.character.hasSummon() ) {
                    AIEffectHelper.scheduleNextIntent( this.character.getSummon(), AIIntent.WAITING )
                }

                return
            }

            if ( ListenerCache.hasGeneralListener( EventType.PlayerAttemptsDying ) ) {
                let data = EventPoolCache.getData( EventType.PlayerAttemptsDying ) as PlayerDieEvent

                data.currentHp = Math.floor( currentHp )
                data.objectId = this.character.getObjectId()
                data.reduceHpAmount = value

                let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayerAttemptsDying, data )
                if ( result && result.terminate ) {
                    return
                }
            }

            await this.character.doDie( attacker )
            return
        }
    }

    doRegeneration(): void {
        if ( this.getCurrentCp() < this.character.getMaxRecoverableCp() ) {
            this.setCurrentCp( this.getCurrentCp() + Formulas.calculateCpRegeneration( this.character ) )
        }

        if ( this.getCurrentHp() < this.character.getMaxRecoverableHp() ) {
            this.setCurrentHp( this.getCurrentHp() + Formulas.calculateHpRegeneration( this.character ) )
        }

        if ( this.getCurrentMp() < this.character.getMaxRecoverableMp() ) {
            this.setCurrentMp( this.getCurrentMp() + Formulas.calculateMpRegeneration( this.character ) )
        }

        if ( !this.shouldRegenerate() ) {
            this.stopHpMpRegeneration()
        }
    }

    shouldRegenerate() : boolean {
        return this.getCurrentHp() < this.character.getMaxRecoverableHp()
            || this.getCurrentMp() < this.character.getMaxMp()
            || this.getCurrentCp() < this.character.getMaxRecoverableCp()
    }

    reduceCp( value: number ) {
        if ( this.getCurrentCp() > value ) {
            this.setCurrentCp( this.getCurrentCp() - value )
            return
        }

        this.setCurrentCp( 0 )
    }

    setStatPropertyValue( value: number, flag: RegenerationFlags ) : void {
        switch ( flag ) {
            case RegenerationFlags.Hp:
                this.currentHp = value
                return

            case RegenerationFlags.Mp:
                this.currentMp = value
                return

            case RegenerationFlags.Cp:
                this.currentCp = value
                return
        }
    }

    getStatPropertyValue( flag: RegenerationFlags ) : number {
        switch ( flag ) {
            case RegenerationFlags.Hp:
                return this.character.getCurrentHp()

            case RegenerationFlags.Mp:
                return this.character.getCurrentMp()

            case RegenerationFlags.Cp:
                return this.character.getCurrentCp()
        }
    }

    notifyHpChanged() {
        return this.character.activateCubicsOnHpDamage()
    }
}