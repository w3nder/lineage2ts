import { L2Character } from '../L2Character'
import { Formulas } from '../../stats/Formulas'
import { L2PcInstance } from '../instance/L2PcInstance'
import { EventTerminationResult, EventType, NpcAttemptsDyingEvent } from '../../events/EventType'
import { ListenerCache } from '../../../cache/ListenerCache'
import { EventPoolCache } from '../../../cache/EventPoolCache'
import _ from 'lodash'
import { PlayerPermission } from '../../../enums/PlayerPermission'
import { RegenerationFlags } from '../../../enums/CharacterRegeneration'
import Timeout = NodeJS.Timeout

export class CharacterStatus {
    character: L2Character
    currentHp: number = 0
    currentMp: number = 0

    statusListeners: Set<number> = new Set<number>() // object ids
    regenerationTask: Timeout
    regenerationFlag: RegenerationFlags

    constructor( character: L2Character ) {
        this.character = character
    }

    addStatusListener( objectId: number ) {
        if ( objectId !== this.character.getObjectId() ) {
            this.statusListeners.add( objectId )
        }
    }

    removeStatusListener( objectId: number ) {
        this.statusListeners.delete( objectId )
    }

    reduceCp( value: number ) {

    }

    getCurrentHp() {
        return this.currentHp
    }

    getCurrentCp() {
        return 0
    }

    getCurrentMp() {
        return this.currentMp
    }

    setCurrentCp( value: number ) {
        return this.setStatProperty( value, RegenerationFlags.Cp )
    }

    setCurrentHp( value: number ): void {
        return this.setStatProperty( value, RegenerationFlags.Hp )
    }

    prepareToStopRegeneration( flag: RegenerationFlags ): void {
        this.regenerationFlag &= ~flag

        if ( this.regenerationFlag === RegenerationFlags.None ) {
            this.stopHpMpRegeneration()
        }
    }

    prepareToStartRegeneration( flag: RegenerationFlags ): void {
        this.regenerationFlag |= flag
        this.startHpMpRegeneration()
    }

    setStatProperty( desiredAmount: number, flag: RegenerationFlags ): void {
        if ( this.character.isDead() ) {
            return
        }

        let maxValue = this.getMaxStatProperty( flag )
        let currentValue = this.getStatPropertyValue( flag )
        let hasChanged: boolean

        if ( desiredAmount >= maxValue ) {
            hasChanged = currentValue !== maxValue
            this.setStatPropertyValue( maxValue, flag )
            this.prepareToStopRegeneration( flag )
        } else {
            hasChanged = currentValue !== desiredAmount
            this.setStatPropertyValue( desiredAmount, flag )
            this.prepareToStartRegeneration( flag )
        }

        if ( hasChanged && this.character.canBroadcastUpdate() ) {
            this.character.broadcastStatusUpdate()
        }
    }

    getMaxStatProperty( flag: RegenerationFlags ): number {
        switch ( flag ) {
            case RegenerationFlags.Hp:
                return this.character.getMaxHp()

            case RegenerationFlags.Mp:
                return this.character.getMaxMp()

            case RegenerationFlags.Cp:
                return this.character.getMaxCp()
        }
    }

    setStatPropertyValue( value: number, flag: RegenerationFlags ): void {
        switch ( flag ) {
            case RegenerationFlags.Hp:
                this.currentHp = value
                return

            case RegenerationFlags.Mp:
                this.currentMp = value
                return
        }
    }

    setDirectHp( value: number ) : void {
        this.currentHp = value
    }

    setDirectMp( value: number ) : void {
        this.currentMp = value
    }

    getStatPropertyValue( flag: RegenerationFlags ): number {
        switch ( flag ) {
            case RegenerationFlags.Hp:
                return this.character.getCurrentHp()

            case RegenerationFlags.Mp:
                return this.character.getCurrentMp()
        }
    }

    setCurrentMp( amount: number ): void {
        return this.setStatProperty( amount, RegenerationFlags.Mp )
    }

    stopHpMpRegeneration(): void {
        if ( this.regenerationTask ) {
            clearInterval( this.regenerationTask )

            this.regenerationTask = null
            this.regenerationFlag = 0
        }
    }

    startHpMpRegeneration(): void {
        if ( !this.regenerationTask && !this.character.isDead() ) {
            let period = Formulas.getRegenerationPeriod( this.character )
            this.regenerationTask = setInterval( this.doRegeneration.bind( this ), period )
        }
    }

    getStatusListeners(): Set<number> {
        return this.statusListeners
    }

    setMaxStats() {
        this.setCurrentHp( this.character.getMaxHp() )
        this.setCurrentMp( this.character.getMaxMp() )
    }

    setMissingStats() : void {
        let hp = this.getCurrentHp()
        if ( !hp || hp > this.character.getMaxHp() ) {
            this.setCurrentHp( this.character.getMaxHp() )
        }

        let mp = this.getCurrentMp()
        if ( !mp || mp > this.character.getMaxMp() ) {
            this.setCurrentMp( this.character.getMaxMp() )
        }
    }

    async reduceHp( value: number, attacker: L2Character, isAwake: boolean, isDOT: boolean, isHpConsumed: boolean ): Promise<void> {
        if ( this.character.isDead() ) {
            return
        }

        if ( ( this.character.isInvulnerable() || this.character.isHpBlocked() ) && !( isDOT || isHpConsumed ) ) {
            return
        }

        if ( attacker ) {
            let attackerPlayer: L2PcInstance = attacker.getActingPlayer()
            if ( attackerPlayer && attackerPlayer.isGM() && !attackerPlayer.getAccessLevel().hasPermission( PlayerPermission.GiveDamage ) ) {
                return
            }
        }

        if ( !isDOT && !isHpConsumed ) {
            await this.character.stopEffectsOnDamage( isAwake )
            if ( this.character.isStunned() && _.random( 10 ) === 0 ) {
                await this.character.stopStunning( true )
            }
        }

        let currentHp = this.getCurrentHp()
        let futureHp = value > 0 ? Math.max( Math.floor( this.getCurrentHp() - value ), 0 ) : currentHp

        if ( futureHp < 0.5 && this.character.isMortal() ) {
            if ( this.character.isNpc() && ListenerCache.hasGeneralListener( EventType.NpcAttemptsDying ) ) {
                let data = EventPoolCache.getData( EventType.NpcAttemptsDying ) as NpcAttemptsDyingEvent

                data.npcId = this.character.getId()
                data.currentHp = currentHp
                data.objectId = this.character.getObjectId()
                data.reduceHpAmount = value


                let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.NpcAttemptsDying, data )
                if ( result && result.terminate ) {
                    return
                }
            }

            this.setCurrentHp( 0 )
            await this.character.doDie( attacker )
            return
        }

        if ( currentHp !== futureHp ) {
            this.setCurrentHp( futureHp )
            this.notifyHpChanged()
        }
    }

    reduceMp( value: number ) {
        let currentMp = this.getCurrentMp()
        let outcome = Math.max( Math.floor( this.getCurrentMp() - value ), 0 )

        if ( currentMp !== outcome ) {
            this.setCurrentMp( outcome )
            this.notifyMpChanged()
        }
    }

    doRegeneration(): void {

        if ( this.getCurrentHp() < this.character.getMaxRecoverableHp() ) {
            this.setCurrentHp( this.getCurrentHp() + Formulas.calculateHpRegeneration( this.character ) )
        }

        if ( this.getCurrentMp() < this.character.getMaxRecoverableMp() ) {
            this.setCurrentMp( this.getCurrentMp() + Formulas.calculateMpRegeneration( this.character ) )
        }

        if ( !this.shouldRegenerate() ) {
            this.stopHpMpRegeneration()
        }
    }

    shouldRegenerate() : boolean {
        return this.getCurrentHp() < this.character.getMaxRecoverableHp() || this.getCurrentMp() < this.character.getMaxMp()
    }

    notifyHpChanged() : void {}

    notifyMpChanged() : void {}
}