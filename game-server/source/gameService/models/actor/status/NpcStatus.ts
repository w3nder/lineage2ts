import { CharacterStatus } from './CharacterStatus'
import { L2Npc } from '../L2Npc'
import { L2Character } from '../L2Character'
import { DuelState } from '../../../enums/DuelState'
import { L2PcInstance } from '../instance/L2PcInstance'

export class NpcStatus extends CharacterStatus {

    character : L2Npc

    async reduceHp( value: number, attacker: L2Character, isAwake: boolean, isDOT: boolean, isHpConsumed: boolean ) : Promise<void> {
        if ( this.character.isDead() ) {
            return
        }

        if ( attacker ) {
            let attackerPlayer : L2PcInstance = attacker.getActingPlayer()
            if ( ( attackerPlayer ) && attackerPlayer.isInDuel() ) {
                attackerPlayer.setDuelState( DuelState.Interrupted )
            }
        }

        return super.reduceHp( value, attacker, isAwake, isDOT, isHpConsumed )
    }
}