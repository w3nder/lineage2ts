import { L2MerchantInstance } from './L2MerchantInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { DataManager } from '../../../../data/manager'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'


export class L2FortLogisticsInstance extends L2MerchantInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )

        this.instanceType = InstanceType.L2FortLogisticsInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2FortLogisticsInstance {
        return new L2FortLogisticsInstance( template )
    }

    getHtmlPath( npcId: number, value: number ): string {
        let path: string = value === 0 ? 'logistics' : `logistics-${ value }`
        return `data/html/fortress/${ path }.htm`
    }

    hasRandomAnimation(): boolean {
        return false
    }

    showChatWindowDefault( player: L2PcInstance, value: number = 0 ): void {
        player.sendOwnedData( ActionFailed() )

        let ownerClan = this.getFort().getOwnerClan()
        let path = value === 0 ? 'data/html/fortress/logistics.htm' : `data/html/fortress/logistics-${ value }.htm`
        let html: string = DataManager.getHtmlData().getItem( path )
                                      .replace( /%npcId%/g, this.getId().toString() )
                                      .replace( /%clanname%/g, ownerClan ? ownerClan.getName() : 'NPC' )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), this.getObjectId() ) )
    }
}