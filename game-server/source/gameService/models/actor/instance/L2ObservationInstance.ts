import { L2Npc } from '../L2Npc'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../../data/manager'

export class L2ObservationInstance extends L2Npc {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2ObservationInstance
    }

    static fromTemplate( template: L2NpcTemplate ) : L2ObservationInstance {
        return new L2ObservationInstance( template )
    }

    showChatWindow( player: L2PcInstance, value: number = 0 ): void {
        let suffix = ''
        if ( this.isInsideRadiusCoordinates( -79884, 86529, 0, 50, false )
                || this.isInsideRadiusCoordinates( -78858, 111358, 0, 50, false )
                || this.isInsideRadiusCoordinates( -76973, 87136, 0, 50, false )
                || this.isInsideRadiusCoordinates( -75850, 111968, 0, 50, false ) ) {
            suffix = '-Oracle'
        }

        if ( value !== 0 ) {
            suffix = `${suffix}-${value}`
        }

        let path = `data/html/observation/${this.getId()}${suffix}.htm`
        let html : string = DataManager.getHtmlData().getItem( path )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), this.getObjectId() ) )
    }
}