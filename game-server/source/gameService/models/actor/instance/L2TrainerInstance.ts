import { L2NpcInstance } from './L2NpcInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2TrainerInstance extends L2NpcInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2TrainerInstance
    }

    static fromTemplate( template: L2NpcTemplate ) : L2TrainerInstance {
        return new L2TrainerInstance( template )
    }

    getHtmlPath( npcId: number, value: number ): string {
        let fileName : string = npcId.toString()

        if ( value !== 0 ) {
            fileName = `${fileName}-${value}`
        }

        return 'data/html/trainer/' + fileName + '.htm'
    }
}