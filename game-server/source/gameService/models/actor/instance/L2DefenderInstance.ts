import { L2Attackable } from '../L2Attackable'
import { Castle } from '../../entity/Castle'
import { Fort } from '../../entity/Fort'
import { SiegableHall } from '../../entity/clanhall/SiegableHall'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Character } from '../L2Character'
import { L2PcInstance } from './L2PcInstance'
import { TerritoryWarManager } from '../../../instancemanager/TerritoryWarManager'
import { FortManager } from '../../../instancemanager/FortManager'
import { CastleManager } from '../../../instancemanager/CastleManager'
import { L2Object } from '../../L2Object'
import { AIEffectHelper } from '../../../aicontroller/helpers/AIEffectHelper'
import { SiegeRole } from '../../../enums/SiegeRole'
import { ILocational } from '../../Location'

export class L2DefenderInstance extends L2Attackable {
    castle: Castle = null // the castle which the instance should defend
    fort: Fort = null // the fortress which the instance should defend
    hall: SiegableHall = null // the siegable hall which the instance should defend

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2DefenderInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2DefenderInstance {
        return new L2DefenderInstance( template )
    }

    isDefender(): boolean {
        return true
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        if ( !attacker.isPlayable() ) {
            return false
        }

        let player: L2PcInstance = attacker.getActingPlayer()
        if ( ( this.fort && this.fort.isSiegeActive() )
                || ( this.castle && this.castle.isSiegeActive() )
                || ( this.hall && this.hall.isSiegeActive() ) ) {
            let activeSiegeId = ( this.fort ? this.fort.getResidenceId() : ( this.castle ? this.castle.getResidenceId() : ( this.hall ? this.hall.getId() : 0 ) ) )

            return player && ( ( player.getSiegeRole() === SiegeRole.Defender && !player.isRegisteredOnThisSiegeField( activeSiegeId ) )
                    || ( player.getSiegeRole() === SiegeRole.Attacker && !TerritoryWarManager.isAllyField( player, activeSiegeId ) )
                    || player.getSiegeRole() === SiegeRole.None )
        }

        return false
    }

    canAddAggro( attacker: L2Object, damage: number, aggroAmount: number ): boolean {
        if ( !attacker || attacker.isInstanceType( InstanceType.L2DefenderInstance ) ) {
            return false
        }

        if ( damage === 0
                && aggroAmount <= 1
                && attacker.isPlayable()
                && ( ( this.fort && this.fort.isSiegeActive() )
                        || ( this.castle && this.castle.isSiegeActive() )
                        || ( this.hall && this.hall.isSiegeActive() ) ) ) {

            let player: L2PcInstance = attacker.getActingPlayer()
            let activeSiegeId = ( this.fort ? this.fort.getResidenceId() : ( this.castle ? this.castle.getResidenceId() : ( this.hall ? this.hall.getId() : 0 ) ) )

            if ( player
                    && ( ( player.getSiegeRole() === SiegeRole.Defender && player.isRegisteredOnThisSiegeField( activeSiegeId ) )
                            || ( player.getSiegeRole() === SiegeRole.Attacker && TerritoryWarManager.isAllyField( player, activeSiegeId ) ) ) ) {
                return false
            }
        }

        return true
    }

    onSpawn() {
        super.onSpawn()

        this.fort = FortManager.findNearestFort( this )
        this.castle = CastleManager.getCastleById( this.castleId )
        this.hall = this.getConquerableHall()
    }

    hasRandomAnimation(): boolean {
        return false
    }

    /*
        TODO : move to AITrait
     */
    returnHome() {
        if ( this.getWalkSpeed() <= 0 ) {
            return
        }

        let spawn: ILocational = this.getSpawnLocation()
        if ( this.isInsideRadius( spawn, 40 ) ) {
            return
        }

        this.setIsReturningToSpawnPoint( true )
        this.clearOverhitData()

        if ( this.hasAIController() ) {
            AIEffectHelper.notifyMove( this, spawn )
        }
    }
}