import { L2MonsterInstance } from './L2MonsterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Character } from '../L2Character'
import { L2PcInstance } from './L2PcInstance'
import { L2Summon } from '../L2Summon'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { RaidBossPointsManager } from '../../../cache/RaidBossPointsManager'
import { HeroCache } from '../../../cache/HeroCache'
import { L2World } from '../../../L2World'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import aigle from 'aigle'
import _ from 'lodash'

export class L2GrandBossInstance extends L2MonsterInstance {

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2GrandBossInstance
        this.setIsRaid( true )
        this.setLethalable( true )
    }

    static fromTemplate( template: L2NpcTemplate ): L2GrandBossInstance {
        return new L2GrandBossInstance( template )
    }

    getVitalityPoints( damage: number ): number {
        return -super.getVitalityPoints( damage ) / 100
    }

    useVitalityRate(): boolean {
        return false
    }

    hasRaidCurse() : boolean {
        return true
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( !await super.doDie( killer ) ) {
            return false
        }

        let player : L2PcInstance

        if ( killer.isPlayer() ) {
            player = killer as L2PcInstance
        } else if ( killer.isSummon() ) {
            player = ( killer as L2Summon ).getOwner()
        }

        if ( player ) {
            BroadcastHelper.dataBasedOnVisibility( this, SystemMessageBuilder.fromMessageId( SystemMessageIds.RAID_WAS_SUCCESSFUL ) )

            if ( player.getParty() ) {

                let boss = this
                await aigle.resolve( player.getParty().getMembers() ).each( async ( objectId: number ) : Promise<void> => {
                    let member : L2PcInstance = L2World.getPlayer( objectId )
                    if ( !member ) {
                        return
                    }

                    await RaidBossPointsManager.addPoints( objectId, boss.getId(), ( boss.getLevel() / 2 ) + _.random( -5, 5 ) )
                    if ( member.isNoble() ) {
                        return HeroCache.setRaidBossKilled( member.getObjectId(), boss.getId() )
                    }
                } )
            } else {
                await RaidBossPointsManager.addPoints( player.getObjectId(), this.getId(), ( this.getLevel() / 2 ) + _.random( -5, 5 ) )
                if ( player.isNoble() ) {
                    await HeroCache.setRaidBossKilled( player.getObjectId(), this.getId() )
                }
            }
        }

        return true
    }

    hasRandomWalk(): boolean {
        return false
    }

    hasRandomAnimation(): boolean {
        return false
    }
}