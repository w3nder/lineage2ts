import { L2MerchantInstance } from './L2MerchantInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { PlayerActionOverride } from '../../../values/PlayerConditions'
import { ClanHall } from '../../entity/ClanHall'
import { ClanHallManager } from '../../../instancemanager/ClanHallManager'
import { ClanHallSiegeManager } from '../../../instancemanager/ClanHallSiegeManager'
import { DataManager } from '../../../../data/manager'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'
import { SiegableHall } from '../../entity/clanhall/SiegableHall'

const enum ValidationCondition {
    OwnerFalse,
    AllFalse,
    BusyDueToSiege,
    Owner
}

export class L2ClanHallManagerInstance extends L2MerchantInstance {
    clanHallId: number = -1

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2ClanHallManagerInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2ClanHallManagerInstance {
        return new L2ClanHallManagerInstance( template )
    }

    isWarehouse(): boolean {
        return true
    }

    async onBypassFeedback( player: L2PcInstance, command: string ): Promise<void> {
        let clanHall = this.getClanHall()
        if ( clanHall.isSiegableHall() && ( clanHall as SiegableHall ).isInSiege() ) {
            return
        }

        if ( this.validateCondition( player ) !== ValidationCondition.Owner ) {
            return
        }

        return super.onBypassFeedback( player, command )
    }

    showChatWindowDefault( player: L2PcInstance ): void {
        player.sendOwnedData( ActionFailed() )

        let htmlPath = 'data/html/clanHallManager/chamberlain-no.htm'
        let condition: ValidationCondition = this.validateCondition( player )
        if ( condition === ValidationCondition.Owner ) {
            htmlPath = `data/html/clanHallManager/chamberlain-${ this.getId() }.htm`
            if ( !DataManager.getHtmlData().hasItem( htmlPath ) ) {
                htmlPath = 'data/html/clanHallManager/chamberlain.htm'
            }

        }

        if ( condition === ValidationCondition.OwnerFalse ) {
            htmlPath = 'data/html/clanHallManager/chamberlain-of.htm'
        }

        let html: string = DataManager.getHtmlData().getItem( htmlPath )
                                      .replace( /%npcId%/g, this.getId().toString() )

        player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), this.getObjectId() ) )
    }

    validateCondition( player: L2PcInstance ): ValidationCondition {
        if ( !this.getClanHall() ) {
            return ValidationCondition.AllFalse
        }

        if ( player.hasActionOverride( PlayerActionOverride.ClanHallAccess ) ) {
            return ValidationCondition.Owner
        }

        if ( player.getClan() ) {
            if ( this.getClanHall().getOwnerId() === player.getClanId() ) {
                return ValidationCondition.Owner
            }

            return ValidationCondition.OwnerFalse
        }

        return ValidationCondition.AllFalse
    }

    getClanHall(): ClanHall {
        if ( this.clanHallId < 0 ) {
            let hall: ClanHall = ClanHallManager.getNearbyClanHall( this.getX(), this.getY() )
            if ( !hall ) {
                hall = ClanHallSiegeManager.getNearbyClanHall( this )
            }

            if ( hall ) {
                this.clanHallId = hall.getId()
            }
        }

        return ClanHallManager.getClanHallById( this.clanHallId )
    }
}