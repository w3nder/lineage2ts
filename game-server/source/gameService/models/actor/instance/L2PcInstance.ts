import { PartyDistributionType } from '../../../enums/PartyDistributionType'
import { DuelState } from '../../../enums/DuelState'
import { MountType, MountTypeHelper } from '../../../enums/MountType'
import { PcAppearance } from '../appearance/PcAppearance'
import { DataManager } from '../../../../data/manager'
import { L2Playable } from '../L2Playable'
import { InstanceType } from '../../../enums/InstanceType'
import { Transform } from '../Transform'
import { L2Character } from '../L2Character'
import { L2PcTemplate } from '../templates/L2PcTemplate'
import { SubClass } from '../../base/SubClass'
import { PcStats } from '../stat/PcStats'
import { DatabaseManager } from '../../../../database/manager'
import { Formulas } from '../../stats/Formulas'
import { ConfirmDialog, SystemMessageBuilder, SystemMessageHelper } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { GameClient } from '../../../GameClient'
import { ConfigManager } from '../../../../config/ConfigManager'
import { Stats } from '../../stats/Stats'
import { PcInventory } from '../../itemcontainer/PcInventory'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { L2Weapon } from '../../items/L2Weapon'
import { Inventory } from '../../itemcontainer/Inventory'
import { Skill } from '../../Skill'
import { L2GameClientRegistry } from '../../../L2GameClientRegistry'
import { Race } from '../../../enums/Race'
import { ILocational, Location } from '../../Location'
import { ItemContainer } from '../../itemcontainer/ItemContainer'
import { L2Clan } from '../../L2Clan'
import { L2Object } from '../../L2Object'
import { L2World } from '../../../L2World'
import { L2CubicInstance } from './L2CubicInstance'
import { L2PetLevelData } from '../../L2PetLevelData'
import { TradeList } from '../../TradeList'
import { PreparedListContainer } from '../../multisell/PreparedListContainer'
import { L2Summon } from '../L2Summon'
import { L2TrapInstance } from './L2TrapInstance'
import { L2TamedBeastInstance } from './L2TamedBeastInstance'
import { L2Vehicle } from '../L2Vehicle'
import { L2Party } from '../../L2Party'
import { ClassId } from '../../base/ClassId'
import { DebouncedPacketMethod, PacketDispatcher } from '../../../PacketDispatcher'
import { StatusUpdate, StatusUpdateProperty } from '../../../packets/send/builder/StatusUpdate'
import { PartySmallWindowUpdate } from '../../../packets/send/PartySmallWindowUpdate'
import { QuestState } from '../../quest/QuestState'
import { CharacterInformation, PlayerInformation } from '../../../packets/send/CharacterInformation'
import { ExBrExtraUserInfo } from '../../../packets/send/ExBrExtraUserInfo'
import { RelationChanged, RelationChangedType } from '../../../packets/send/RelationChanged'
import { GetOnVehicle } from '../../../packets/send/GetOnVehicle'
import { ExGetOnAirShip } from '../../../packets/send/ExGetOnAirShip'
import { BuyStoreTypes, PrivateStoreType, SellStoreTypes } from '../../../enums/PrivateStoreType'
import { RecipeShopMessage } from '../../../packets/send/RecipeShopMesssage'
import { PrivateStoreMessageBuy } from '../../../packets/send/PrivateStoreMessageBuy'
import { ExPrivateStoreSetWholesaleMessage } from '../../../packets/send/ExPrivateStoreSetWholesaleMessage'
import { PrivateStoreMessageSell } from '../../../packets/send/PrivateStoreMessageSell'
import { L2BoatInstance } from './L2BoatInstance'
import { L2AirShipInstance } from './L2AirShipInstance'
import { TerritoryWarManager } from '../../../instancemanager/TerritoryWarManager'
import { BlockCheckerManager } from '../../../instancemanager/BlockCheckerManager'
import { DuelManager } from '../../../instancemanager/DuelManager'
import { Siege } from '../../entity/Siege'
import { SiegeManager } from '../../../instancemanager/SiegeManager'
import { EtcStatusUpdate } from '../../../packets/send/EtcStatusUpdate'
import { PunishmentType } from '../../punishment/PunishmentType'
import { PunishmentManager } from '../../../instancemanager/PunishmentManager'
import { BaseStats } from '../../stats/BaseStats'
import { ShortcutType } from '../../../enums/ShortcutType'
import { NicknameChanged } from '../../../packets/send/NicknameChanged'
import { UserInfo } from '../../../packets/send/UserInfo'
import { EtcItemType } from '../../../enums/items/EtcItemType'
import { PlayerActionOverride } from '../../../values/PlayerConditions'
import { L2PetInstance } from './L2PetInstance'
import { ExVoteSystemInfoWithPlayer } from '../../../packets/send/ExVoteSystemInfo'
import { SocialAction, SocialActionType } from '../../../packets/send/SocialAction'
import { L2SkillLearn } from '../../L2SkillLearn'
import { ExDominionWarStart } from '../../../packets/send/ExDominionWarStart'
import { OlympiadGameManager } from '../../olympiad/OlympiadGameManager'
import { OlympiadGameTask } from '../../olympiad/OlympiadGameTask'
import { ExDuelUpdateUserInfo } from '../../../packets/send/ExDuelUpdateUserInfo'
import { L2Item } from '../../items/L2Item'
import { L2EnchantSkillLearn } from '../../L2EnchantSkillLearn'
import { SkillListBuilder } from '../../../packets/send/builder/SkillListBuilder'
import { CommonSkill } from '../../holders/SkillHolder'
import { PledgeShowMemberListUpdate } from '../../../packets/send/PledgeShowMemberListUpdate'
import { ShotType } from '../../../enums/ShotType'
import { GeneralHelper } from '../../../helpers/GeneralHelper'
import { GameGuardQuery } from '../../../packets/send/GameGuardQuery'
import { GameTime } from '../../../cache/GameTime'
import { L2Npc } from '../L2Npc'
import { L2Henna } from '../../items/L2Henna'
import { SkillCoolTime } from '../../../packets/send/SkillCoolTime'
import { SetupGauge, SetupGaugeColors } from '../../../packets/send/SetupGauge'
import { Ride } from '../../../packets/send/Ride'
import { Snoop } from '../../../packets/send/Snoop'
import { AbnormalType } from '../../skills/AbnormalType'
import { PcWarehouse } from '../../itemcontainer/PcWarehouse'
import { SevenSigns } from '../../../directives/SevenSigns'
import { TeleportWhereType } from '../../../enums/TeleportWhereType'
import { createManufactureItem, L2ManufactureItem } from '../../L2ManufactureItem'
import { FortManager } from '../../../instancemanager/FortManager'
import { Fort, FortFunction, FortFunctionType } from '../../entity/Fort'
import { FortSiegeManager } from '../../../instancemanager/FortSiegeManager'
import { OlympiadManager } from '../../olympiad/OlympiadManager'
import { L2ClanMember } from '../../L2ClanMember'
import { InstanceManager } from '../../../instancemanager/InstanceManager'
import { Instance } from '../../entity/Instance'
import { TvTEvent } from '../../entity/TvTEvent'
import { CursedWeaponManager } from '../../../instancemanager/CursedWeaponManager'
import { PcRefund } from '../../itemcontainer/PcRefund'
import { PcFreight } from '../../itemcontainer/PcFreight'
import { TradeDone, TradeDoneStatus } from '../../../packets/send/TradeDone'
import { TradeStart } from '../../../packets/send/TradeStart'
import { ExAutoSoulShot, ExAutoSoulShotStatus } from '../../../packets/send/ExAutoSoulShot'
import { L2PartyMessageType } from '../../../enums/L2PartyMessageType'
import { Team } from '../../../enums/Team'
import { PcStatus } from '../status/PcStatus'
import { PartyMatchWaitingList } from '../../../cache/PartyMatchWaitingList'
import { PartyMatchManager } from '../../../cache/partyMatchManager'
import { CastleManager } from '../../../instancemanager/CastleManager'
import { RecipeController } from '../../../taskmanager/RecipeController'
import { SkillCache } from '../../../cache/SkillCache'
import { ChangeWaitType, ChangeWaitTypeValue } from '../../../packets/send/ChangeWaitType'
import { L2EffectType } from '../../../enums/effects/L2EffectType'
import { getLureDelayMultiplier, L2Fishing } from '../../fishing/L2Fishing'
import { ItemManagerCache } from '../../../cache/ItemManagerCache'
import { TerritoryWard } from '../../../instancemanager/territory/TerritoryWard'
import { L2EtcItem } from '../../items/L2EtcItem'
import { ItemHandlerMethod } from '../../../handler/ItemHandler'
import { ExFishingEnd } from '../../../packets/send/ExFishingEnd'
import { ExFishingStart } from '../../../packets/send/ExFishingStart'
import { MusicPacket } from '../../../packets/send/Music'
import { DimensionalRiftManager } from '../../../instancemanager/DimensionalRiftManager'
import { L2Decoy } from '../L2Decoy'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { L2TargetType } from '../../skills/targets/L2TargetType'
import { L2DoorInstance } from './L2DoorInstance'
import { L2EventMonsterInstance } from './L2EventMonsterInstance'
import { ActionType } from '../../items/type/ActionType'
import { L2Attackable } from '../L2Attackable'
import { PunishmentEffect } from '../../punishment/PunishmentEffect'
import { EffectFlag } from '../../../enums/EffectFlag'
import { ExGetBookMarkInfoPacketWithPlayer } from '../../../packets/send/ExGetBookMarkInfoPacket'
import { ClassLevel } from '../../base/ClassLevel'
import { MagicSkillUseWithCharacters } from '../../../packets/send/MagicSkillUse'
import { getPlayerClassById } from '../../base/PlayerClass'
import { PledgeShowMemberListDelete } from '../../../packets/send/PledgeShowMemberListDelete'
import { HennaInfo } from '../../../packets/send/HennaInfo'
import { ClanPriviledgeHelper } from '../../ClanPrivilege'
import { AdminManager } from '../../../cache/AdminManager'
import { ExBRAgathionEnergyInfo, ExBRAgathionEnergyItem } from '../../../packets/send/ExBRAgathionEnergyInfo'
import { ExStorageMaxCount } from '../../../packets/send/ExStorageMaxCount'
import { TradeOtherDone } from '../../../packets/send/TradeOtherDone'
import { L2Request } from '../../L2Request'
import { L2PcInstanceValues } from '../../../values/L2PcInstanceValues'
import { PrivateStoreManageListBuy } from '../../../packets/send/PrivateStoreManageListBuy'
import { PrivateStoreManageListSell } from '../../../packets/send/PrivateStoreManageListSell'
import { ValidateLocation } from '../../../packets/send/ValidateLocation'
import { TargetSelected } from '../../../packets/send/TargetSelected'
import { MyTargetSelectedWithCharacters } from '../../../packets/send/MyTargetSelected'
import { ObservationReturn } from '../../../packets/send/ObservationReturn'
import { ObservationMode } from '../../../packets/send/ObservationMode'
import { ConfirmationAction } from '../../../enums/confirmationAction'
import { ExOlympiadMode, ExOlympiadModeType } from '../../../packets/send/ExOlympiadMode'
import { CharacterNamesCache } from '../../../cache/CharacterNamesCache'
import { ExSetCompassZoneCode, ExSetCompassZoneType } from '../../../packets/send/ExSetCompassZoneCode'
import { Castle } from '../../entity/Castle'
import { ItemsOnGroundManager } from '../../../cache/ItemsOnGroundManager'
import { WeaponType } from '../../items/type/WeaponType'
import { ShortCutInit } from '../../../packets/send/ShortCutInit'
import { isValidPlayerKill, L2PcInstanceHelper } from '../../../helpers/L2PcInstanceHelper'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { ListenerCache } from '../../../cache/ListenerCache'
import {
    DataIntegrityViolationEvent,
    DataIntegrityViolationType,
    EventTerminationResult,
    EventType,
    NpcSeeSkillEvent,
    PlayerAccessLevelChangedEvent,
    PlayerAddHennaEvent,
    PlayerCancelProfessionEvent,
    PlayerChangedFameEvent,
    PlayerChangedLevelEvent,
    PlayerChangeProfessionEvent,
    PlayerDropItemEvent,
    PlayerEquipItemEvent,
    PlayerKarmaChangedEvent,
    PlayerKillsChangedEvent,
    PlayerLoggedOutEvent,
    PlayerPKChangedEvent,
    PlayerPvpKillEvent,
    PlayerSitEvent,
    PlayerStandEvent,
    PlayerStartsAttackEvent,
    PlayerStartsLogoutEvent,
    PlayerStoppedMovingEvent,
    PlayerTransformEvent,
} from '../../events/EventType'
import { ItemType2 } from '../../../enums/items/ItemType2'
import { L2Event } from '../../entity/L2Event'
import { InventorySlot, ItemTypes } from '../../../values/InventoryValues'
import { L2ClanValues } from '../../../values/L2ClanValues'
import { PlayerVariablesManager } from '../../../variables/PlayerVariablesManager'
import { AccountVariablesManager } from '../../../variables/AccountVariablesManager'
import { ProximalDiscoveryManager } from '../../../cache/ProximalDiscoveryManager'
import { QuestStateCache } from '../../../cache/QuestStateCache'
import { VitalityPointsPerLevel } from '../../../enums/VitalityLevels'
import { ExVitalityPointInfo } from '../../../packets/send/ExVitalityPointInfo'
import { HtmlActionCache } from '../../../cache/HtmlActionCache'
import { NevitManager } from '../../../cache/NevitManager'
import { CommonVariables, MultipliersVariableNames, PremiumMultipliersPropertyName } from '../../../values/L2PcValues'
import { ClanCache } from '../../../cache/ClanCache'
import { PlayerEventStatus, PlayerEventStatusCache } from '../../../cache/PlayerEventStatusCache'
import { ExStartScenePlayer } from '../../../packets/send/ExStartScenePlayer'
import { PlayerRadarCache } from '../../../cache/PlayerRadarCache'
import { PlayerGroupCache } from '../../../cache/PlayerGroupCache'
import { DimensionalTransferManager } from '../../../cache/DimensionalTransferManager'
import _, { DebouncedFunc } from 'lodash'
import { EventPoolCache } from '../../../cache/EventPoolCache'
import { PlayerUICache } from '../../../cache/PlayerUICache'
import * as Buffer from 'buffer'
import { PlayerShortcutCache } from '../../../cache/PlayerShortcutCache'
import { PlayerContactCache } from '../../../cache/PlayerContactCache'
import { PlayerMacrosCache } from '../../../cache/PlayerMacrosCache'
import { AIIntent } from '../../../aicontroller/enums/AIIntent'
import { AIEffectHelper } from '../../../aicontroller/helpers/AIEffectHelper'
import { ItemProtectionCache } from '../../../cache/ItemProtectionCache'
import { DeferredMethods } from '../../../helpers/DeferredMethods'
import { InventoryUpdateStatus } from '../../../enums/InventoryUpdateStatus'
import { TeleportBookmarkCache } from '../../../cache/TeleportBookmarkCache'
import { FightStanceCache } from '../../../cache/FightStanceCache'
import { TargetUnselected } from '../../../packets/send/TargetUnselected'
import aigle from 'aigle'
import { InventoryAction } from '../../../enums/InventoryAction'
import { StatsCache } from '../../../cache/StatsCache'
import { PetInfoAnimation } from '../../../packets/send/PetInfo'
import { CharacterSummonCache } from '../../../cache/CharacterSummonCache'
import { L2NpcValues } from '../../../values/L2NpcValues'
import { ClanHall } from '../../entity/ClanHall'
import { PlayerLoadState } from '../../../enums/PlayerLoadState'
import { MovementType } from '../../../enums/MovementType'
import { SiegeRole } from '../../../enums/SiegeRole'
import { PlayerPvpFlag } from '../../../enums/PlayerPvpFlag'
import { FishDataItem } from '../../../../data/interface/FishDataApi'
import { OlympiadSide } from '../../../enums/OlympiadSide'
import { getSlotFromItem } from '../../itemcontainer/ItemSlotHelper'
import { recordItemViolation } from '../../../helpers/PlayerViolations'
import { PathFinding } from '../../../../geodata/PathFinding'
import { GeoPolygonCache } from '../../../cache/GeoPolygonCache'
import { MagicProcess } from '../../MagicProcess'
import { PlayerSkillFlags } from '../../skills/PlayerSkillFlags'
import { L2Region } from '../../../enums/L2Region'
import { DialogAnswerValue } from '../../../enums/DialogAnswerValue'
import { CharacterEffectsCache } from '../../../cache/CharacterEffectsCache'
import { PlayerRelationStatus } from '../../../cache/PlayerRelationStatusCache'
import { TransformSkill } from '../../../enums/TransformSkill'
import { PointGeometry } from '../../drops/PointGeometry'
import { GeometryId } from '../../../enums/GeometryId'
import { OptionsSkillHolder } from '../../options/OptionsSkillHolder'
import { OptionsSkillType } from '../../../enums/OptionsSkillType'
import { RestartPointType } from '../../../enums/RestartPointType'
import { ClanHallManager } from '../../../instancemanager/ClanHallManager'
import { L2SiegeClan } from '../../L2SiegeClan'
import { SiegableHall } from '../../entity/clanhall/SiegableHall'
import { ClanHallSiegeManager } from '../../../instancemanager/ClanHallSiegeManager'
import { L2SiegeFlagInstance } from './L2SiegeFlagInstance'
import { ExQuestItemList } from '../../../packets/send/ExQuestItemList'
import { ItemList } from '../../../packets/send/ItemList'
import { CastleFunctions } from '../../../enums/CastleFunctions'
import { getDefaultDamageParameters } from '../../PhysicalDamageParameters'
import { L2MoveManager } from '../../../L2MoveManager'
import { ItemReuseTime } from '../../ReuseTime'
import { ItemDropState } from '../../../enums/ItemDropState'
import { getTeleportLocation } from '../../../helpers/TeleportHelper'
import { AreaType } from '../../areas/AreaType'
import { ClanHallArea } from '../../areas/type/ClanHall'
import { FortSiegeArea } from '../../areas/type/FortSiege'
import { CastleArea } from '../../areas/type/Castle'
import { AreaDiscoveryManager } from '../../../cache/AreaDiscoveryManager'
import { AreaFlags } from '../../areas/AreaFlags'
import { PlayerMovementActionCache } from '../../../cache/PlayerMovementActionCache'
import { PlayerAccess } from '../../PlayerAccess'
import { PlayerAccessCache } from '../../../cache/PlayerAccessCache'
import { PlayerPermission } from '../../../enums/PlayerPermission'
import { ClanPrivilege } from '../../../enums/ClanPriviledge'
import { L2ItemSlots } from '../../../enums/L2ItemSlots'
import { ClanHallFunctionType } from '../../../enums/clanhall/ClanHallFunctionType'
import { AggroCache } from '../../../cache/AggroCache'
import { CalculatorHelper } from '../../../helpers/CalculatorHelper'
import { PlayerRecipeCache } from '../../../cache/PlayerRecipeCache'
import { PlayerHennaCache } from '../../../cache/PlayerHennaCache'
import { PlayerFriendsCache } from '../../../cache/PlayerFriendsCache'
import { MailManager } from '../../../instancemanager/MailManager'
import { CommonSkillIds } from '../../../enums/skills/CommonSkillIds'
import Timeout = NodeJS.Timeout

const SLOT_MULTI_ALLWEAPONS = L2ItemSlots.AllHandSlots | L2ItemSlots.RightHand

export class L2PcInstance extends L2Playable {
    accountName: string
    subclassLock: boolean = false
    appearance: PcAppearance
    lastLocation: Location = new Location( 0, 0, 0 )
    inventory: PcInventory
    freight: PcFreight
    snoopListeners: Array<number> = []
    snoopedPlayers: Array<number> = []
    charges: number = 0
    request: L2Request
    characters: Map<number, string>
    cubics: Map<number, L2CubicInstance> = new Map<number, L2CubicInstance>()
    activeSoulShots: Array<number> = []
    lottery: { [ key: number ]: number } = {}
    race: Array<number> = []

    partyDistributionType: PartyDistributionType
    deleteTime: number = 0
    createDate: number = 0
    isPlayerOnline: boolean = true
    onlineTime: number = 0
    onlineBeginTime: number = 0
    lastAccess: number = 0
    uptime: number
    baseClass: number = 0
    activeClass: number = 0
    classIndex: number = 0
    controlItemId: number = 0
    leveldata: L2PetLevelData
    currentFeed: number = 0
    hasPetItems: boolean = false
    subClasses: { [ key: number ]: SubClass } = {}
    expBeforeDeath: number = 0
    karma: number = 0
    pvpKills: number = 0
    pkKills: number = 0
    pvpFlag: PlayerPvpFlag = PlayerPvpFlag.None
    fame: number = 0
    siegeRole: number = 0
    siegeSide: number = 0
    currentWeightPenalty: number = 0
    lastCompassZone: number
    isIn7sDungeonValue: boolean = false
    availableBookmarkSlots: number = 0
    canFeed: boolean = false
    isInSiegeValue: boolean = false
    isInHideoutSiege: boolean = false
    inOlympiadMode: boolean = false
    isOlympiadStarted: boolean = false
    olympiadGameId: number = -1
    olympiadSide: OlympiadSide = OlympiadSide.None
    olympiadBuffCount: number = 0
    duelState: DuelState = DuelState.None
    duelId: number
    vehicle: L2Vehicle
    inVehiclePosition: Location
    mountType: MountType = MountType.NONE
    mountNpcId: number = 0
    mountLevel: number = 0
    mountObjectId: number = 0
    inCrystallize: boolean = false
    inCraftMode: boolean = false
    transformSkills: { [ key: number ]: Skill } = {}
    waitTypeSitting: boolean = false
    observerMode: boolean = false
    recommendationsHave: number = 0
    recommendationsLeft: number = 0
    recommendationsBonusEnd: number = 0
    recomendationsTwoHoursGivenValue: boolean = false
    overrides: number = 0

    // TODO : move warehouse/inventory to separate cache
    warehouse: PcWarehouse
    refund: PcRefund

    privateStoreType: PrivateStoreType = PrivateStoreType.None
    activeTradeList: TradeList
    activeWarehouse: ItemContainer
    manufactureItems: Array<L2ManufactureItem> = []
    storeName: string
    sellList: TradeList
    buyList: TradeList
    currentMultiSell: PreparedListContainer
    noble: boolean = false
    hero: boolean = false
    lastFolkNpc: number = 0// object id
    questNpcId: number = 0
    hennaSTR: number = 0
    hennaINT: number = 0
    hennaDEX: number = 0
    hennaMEN: number = 0
    hennaWIT: number = 0
    hennaCON: number = 0
    summon: L2Summon
    decoy: L2Character
    trap: L2TrapInstance
    agathionId: number = 0
    tamedBeasts: Array<number> = []
    minimapAllowed: boolean = false
    clanId: number = 0
    apprentice: number = 0
    sponsorId: number = 0
    clanJoinExpiryTime: number = 0
    clanCreateExpiryTime: number = 0
    powerGrade: number = 0
    clanPrivileges: number = 0
    pledgeClass: number = 0
    pledgeType: number = 0
    levelJoinedAcademy: number = 0
    wantsPeace: number = 0
    deathPenaltyBuffLevel: number = 0
    groundSkillLocation: Location
    accessLevel: PlayerAccess
    silenceMode: boolean = false
    silenceModeExcluded: Array<number> = []
    weightlessMode: boolean = false
    tradeRefusal: boolean = false

    activeRequesterId: number = 0
    requestExpiration: number = 0
    protectEndTime: number = 0
    lure: L2ItemInstance
    teleportProtectEndTime: number = 0
    recentFakeDeathEndTime: number = 0
    isFakeDeathValue: boolean = false
    fistsWeaponItem: L2Weapon
    expertiseArmorPenalty: number = 0
    expertiseWeaponPenalty: number = 0
    expertisePenaltyBonus: number = 0
    isEnchantingValue: boolean = false
    activeEnchantItemId: number = L2PcInstanceValues.EmptyId
    activeEnchantSupportItemId: number = L2PcInstanceValues.EmptyId
    activeEnchantAttributeItemId: number = L2PcInstanceValues.EmptyId
    inventoryDisable: boolean = false
    blockCheckerEventArena: number = -1

    // TODO : extract fish related data into separate cache layer
    fishCombat: L2Fishing
    fishing: boolean = false
    fishX: number = 0
    fishY: number = 0
    fishZ: number = 0

    mainSkillFlags: PlayerSkillFlags
    petSkillFlags: PlayerSkillFlags

    cursedWeaponEquippedId: number = 0
    combatFlagEquippedId: boolean = false

    canReviveValue: boolean = true
    reviveRequested: number = 0
    revivePower: number = 0
    reviveRecovery: number = 0
    revivePet: boolean = false
    itemDropState: ItemDropState = ItemDropState.NoRestrictions
    expPenalty: number = 0

    // character coordinates from client
    clientX: number = 0
    clientY: number = 0
    clientZ: number = 0
    clientHeading: number = 0
    clientUpdateTime: number = 0

    fallingTimestamp: number
    multiSocialTarget: number
    multiSociaAction: number
    movieId: number
    adminConfirmCommand: string
    pvpFlagEndTime: number
    notMoveUntil: number

    customSkills: { [ key: number ]: Skill } = {}
    confirmationActions: Set<ConfirmationAction> = new Set<ConfirmationAction>()
    servitorShare: { [ key: string ]: number } = {}
    lastPetitionGmName: string | null
    hasCharmOfCourageValue: boolean = false

    learningClass: number

    proximityScanStep: number = 0
    fish: FishDataItem
    hiddenInstanceTypes: Set<InstanceType> = new Set<InstanceType>()
    skillsToStore: Map<number, Array<Skill>> = new Map<number, Array<Skill>>()
    loadState: PlayerLoadState = PlayerLoadState.None
    rangedAttackEndTime: number = 0
    souls: number = 0
    triggerSkills: Array<OptionsSkillHolder> = []
    transformation: Transform
    areaFlags: Set<AreaFlags> = new Set<AreaFlags>()

    /*
        Timers section
     */

    warnUserTakeBreakTask: NodeJS.Timeout
    readyForAttackTimeout: NodeJS.Timeout
    proximityScan: NodeJS.Timeout
    autoSave: NodeJS.Timeout
    pvpFlagTimeout: NodeJS.Timeout
    taskRentPet: Timeout
    waterDamageTask: Timeout
    wearItemTimeout: Timeout
    chargeTask: Timeout
    soulTask: Timeout
    warehouseResetTask: Timeout
    recommendationsGiveTask: Timeout
    recommendationsBonusTask: Timeout
    lookForFishTask: Timeout
    fameTask: Timeout
    vitalityTask: Timeout
    teleportTask: Timeout
    mountFeedTask: Timeout
    dismountTask: Timeout
    stanUpTimeout: Timeout
    deathTeleportTimeout: Timeout

    /*
        Throttled or debounced methods.
     */

    broadcastUserInfo: DebouncedFunc<() => void>
    sendSkillList: DebouncedFunc<() => void>
    refreshExpertisePenalty: DebouncedFunc<() => void>
    refreshOverloadPenalty: DebouncedFunc<() => void>
    clearHiddenInstanceTypes: DebouncedFunc<() => void>
    debounceSkillSave: DebouncedFunc<() => Promise<void>>
    broadcastKarmaInfo: DebouncedFunc<() => void>
    debounceEngagePvPFlag: DebouncedFunc<( flag: PlayerPvpFlag ) => void>

    constructor( objectId: number, classId: number, accountName: string, appearance: PcAppearance ) {
        super( objectId, DataManager.getPlayerTemplateData().getTemplateById( classId ) )

        this.instanceType = InstanceType.L2PcInstance
        this.accountName = accountName

        this.mainSkillFlags = {
            isCtrlPressed: false,
            isShiftPressed: false
        }

        this.petSkillFlags = {
            isCtrlPressed: false,
            isShiftPressed: false
        }

        appearance.setOwner( objectId )
        this.appearance = appearance

        this.inventory = new PcInventory( objectId )
        this.freight = new PcFreight( objectId, true )
        this.learningClass = this.getClassId()
        L2World.addPlayer( this )
    }

    createMethods() {
        super.createMethods()

        this.broadcastUserInfo = _.debounce( this.runBroadcastUserInfo.bind( this ), 300, {
            trailing: true,
            maxWait: 600,
        } )

        this.debounceAreaRevalidation = _.debounce( this.runDirectAreaRevalidation.bind( this ), 500, {
            trailing: true,
            maxWait: 1000,
        } )

        this.sendSkillList = _.debounce( this.runSendSkillList.bind( this ), 300, {
            trailing: true,
            maxWait: 600,
        } )

        this.refreshExpertisePenalty = _.debounce( this.runRefreshExpertisePenalty.bind( this ), 300, {
            trailing: true,
            maxWait: 600,
        } )

        this.refreshOverloadPenalty = _.debounce( this.runRefreshOverloadPenalty.bind( this ), 300, {
            trailing: true,
            maxWait: 600,
        } )

        this.clearHiddenInstanceTypes = _.debounce( this.runClearHiddenInstanceTypes.bind( this ), ConfigManager.customs.getTreasureChestVisibility() * 1000, {
            trailing: true,
        } )

        this.debounceSkillSave = _.debounce( this.runSkillSave.bind( this ), 500, {
            trailing: true,
            maxWait: 1000,
        } )

        this.broadcastKarmaInfo = _.debounce( this.runBroadcastKarmaInfo.bind( this ), 300, {
            trailing: true,
            maxWait: 600,
        } )

        this.autoSave = setInterval( this.runAutoSave.bind( this ), GeneralHelper.minutesToMillis( 15 ) )

        this.debounceEngagePvPFlag = _.debounce( this.runEngagePvPFlag.bind( this ), 2000, {
            leading: true,
            trailing: true
        } )

        this.physicalDamageData = getDefaultDamageParameters()
    }

    cancelMethods() {
        super.cancelMethods()

        this.broadcastUserInfo.cancel()
        this.debounceAreaRevalidation.cancel()
        this.sendSkillList.cancel()
        this.refreshExpertisePenalty.cancel()
        this.refreshOverloadPenalty.cancel()
        this.clearHiddenInstanceTypes.cancel()
        this.debounceSkillSave.cancel()
        this.broadcastKarmaInfo.cancel()
        this.debounceEngagePvPFlag.cancel()
    }

    assignCalculators() {
        this.calculators = CalculatorHelper.getPlayerCalculators()
    }

    async addAdena( amount: number, reason: string = '', referenceId: number = 0, shouldSendMessage: boolean = false ): Promise<void> {
        if ( shouldSendMessage ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.EARNED_S1_ADENA )
                    .addBigNumber( amount )
                    .getBuffer()
            this.sendOwnedData( packet )
        }

        if ( amount > 0 ) {
            await this.inventory.addAdena( amount, referenceId, reason )
        }
    }

    addCubic( cubic : L2CubicInstance ): void {
        let existingCubic = this.cubics.get( cubic.getId() )
        if ( existingCubic ) {
            existingCubic.terminate()
        }

        this.cubics.set( cubic.getId(), cubic )
    }

    addCustomSkill( skill: Skill ) {
        if ( skill && skill.getDisplayId() !== skill.getId() ) {
            this.customSkills[ skill.getDisplayId() ] = skill
        }
    }

    addExpAndSp( addExp: number, addSp: number ): Promise<void> {
        return this.processAddExpAndSp( addExp, addSp, false )
    }

    async addSkill( skill: Skill, store: boolean = false, classIndex = this.classIndex ): Promise<Skill> {
        this.addCustomSkill( skill )
        let oldSkill: Skill = await super.addSkill( skill )

        if ( store ) {
            this.storeSkill( skill, classIndex )
        }

        return oldSkill
    }

    runBroadcastStatusUpdate(): void {
        let packet: Buffer = new StatusUpdate( this.objectId )
                .addAttribute( StatusUpdateProperty.MaxHP, this.getMaxHp() )
                .addAttribute( StatusUpdateProperty.CurrentHP, this.getCurrentHp() )

                .addAttribute( StatusUpdateProperty.MaxMP, this.getMaxMp() )
                .addAttribute( StatusUpdateProperty.CurrentMP, this.getCurrentMp() )

                .addAttribute( StatusUpdateProperty.MaxCP, this.getMaxCp() )
                .addAttribute( StatusUpdateProperty.CurrentCP, this.getCurrentCp() )
                .getBuffer()

        this.sendCopyData( packet )
        BroadcastHelper.dataBasedOnVisibility( this, packet )

        if ( this.isInParty() ) {
            this.getParty().broadcastDataToPartyMembers( this.objectId, PartySmallWindowUpdate( this ) )
        }

        if ( this.isInOlympiadMode() && this.isOlympiadStart() ) {
            let game: OlympiadGameTask = OlympiadGameManager.getOlympiadTask( this.getOlympiadGameId() )
            if ( game && game.isBattleStarted() ) {
                game.broadcastStatusUpdate( this )
            }
        }

        if ( this.isInDuel() ) {
            DuelManager.broadcastToOpposingTeam( this, ExDuelUpdateUserInfo, this.objectId )
        }
    }

    canRevive(): boolean {
        if ( !this.canReviveValue ) {
            return false
        }

        return !_.some( PlayerEventStatusCache.getPlayerStatus( this.getObjectId() ), ( status: PlayerEventStatus ) => !status.canRevive )
    }

    createStats(): PcStats {
        return new PcStats( this )
    }

    createStatus(): PcStatus {
        return new PcStatus( this )
    }

    async deleteMe(): Promise<void> {
        await this.storeMe()
        await this.unload()
        return super.deleteMe()
    }

    async destroyItemByObjectId( objectId: number, count: number, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        let item: L2ItemInstance = this.inventory.getItemByObjectId( objectId )

        if ( !item ) {
            if ( sendMessage ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
            }

            return false
        }

        return this.destroyItemWithCount( item, count, sendMessage, reason )
    }

    async destroyItem( item: L2ItemInstance, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        return this.destroyItemWithCount( item, item.getCount(), sendMessage, reason )
    }

    async destroyItemByItemId( itemId: number, count: number, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        if ( itemId === ItemTypes.Adena ) {
            return this.reduceAdena( count, sendMessage, reason )
        }

        let item: L2ItemInstance = await this.inventory.destroyItemByItemId( itemId, count, this.getObjectId(), reason )
        if ( !item ) {
            if ( sendMessage ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
            }

            return false
        }

        if ( sendMessage ) {
            if ( count > 1 ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                        .addItemNameWithId( itemId )
                        .addNumber( count )
                        .getBuffer()

                this.sendOwnedData( packet )
            } else {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                        .addItemNameWithId( itemId )
                        .getBuffer()

                this.sendOwnedData( packet )
            }
        }

        return true
    }

    async doCast( skill: Skill ): Promise<void> {
        if ( !this.checkUseMagicConditions( skill ) ) {
            if ( skill.isToggle() && this.isAffectedBySkill( skill.getId() ) ) {
                return this.stopSkillEffects( true, skill.getId() )
            }

            return
        }

        await super.doCast( skill )
        this.setRecentFakeDeath( false )
    }

    doRevive(): void {
        super.doRevive()

        this.updateEffectIcons()
        this.sendDebouncedPacket( EtcStatusUpdate )
        this.revivePet = false
        this.resetReviveProperties()

        if ( this.isMounted() ) {
            this.startFeedMount( this.mountNpcId )
        }

        let party = this.getParty()
        if ( party
            && party.isInDimensionalRift()
            && !DimensionalRiftManager.checkIfInPeaceZone( this.getX(), this.getY(), this.getZ() ) ) {
            party.getDimensionalRift().memberResurrected( this )
        }

        if ( this.getInstanceId() > 0 ) {
            let instance: Instance = InstanceManager.getInstance( this.getInstanceId() )
            if ( instance ) {
                instance.cancelEjectDeadPlayer( this )
            }
        }

        this.getAIController().activate()
    }

    doReviveWithPercentage( value: number ) {
        this.doRevive()
        this.restoreExp( value )
    }

    getAccessLevel() : PlayerAccess {
        if ( ConfigManager.general.everybodyHasAdminRights() ) {
            return PlayerAccessCache.getAdminLevel()
        }

        if ( !this.accessLevel ) {
            this.setAccessLevel( 0, 'Access level not initialized' )
        }

        return this.accessLevel
    }

    addHenna( henna: L2Henna ): boolean {
        let slot = PlayerHennaCache.getFreeSlot( this.getObjectId() )
        if ( slot < 0 ) {
            return false
        }

        PlayerHennaCache.addHenna( this.getObjectId(), this.getClassIndex(), slot, henna.getDyeId() )

        this.sendOwnedData( HennaInfo( this ) )
        this.sendDebouncedPacket( UserInfo )
        this.sendDebouncedPacket( ExBrExtraUserInfo )

        if ( ListenerCache.hasGeneralListener( EventType.PlayerAddHenna ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerAddHenna ) as PlayerAddHennaEvent

            eventData.playerId = this.getObjectId()
            eventData.dyeItemId = henna.getDyeItemId()
            eventData.classId = this.getClassId()
            eventData.dyeId = henna.getDyeId()

            ListenerCache.sendGeneralEvent( EventType.PlayerAddHenna, eventData )
        }

        return true
    }

    async addItem( itemId: number, count: number, enchantLevel: number, referenceId: number, type: InventoryAction ): Promise<L2ItemInstance> {
        if ( count <= 0 ) {
            return null
        }

        let item: L2Item = ItemManagerCache.getTemplate( itemId )
        if ( !item ) {
            if ( ListenerCache.hasGeneralListener( EventType.DataIntegrityViolation ) ) {
                let data = EventPoolCache.getData( EventType.DataIntegrityViolation ) as DataIntegrityViolationEvent

                data.errorId = '19b0608f-a06d-4a5e-9d67-c7d78262afaf'
                data.message = 'Player adding non-existent item id'
                data.type = DataIntegrityViolationType.Item
                data.playerId = this.getObjectId()
                data.ids = [ itemId ]

                await ListenerCache.sendGeneralEvent( EventType.DataIntegrityViolation, data )
            }

            return null
        }


        if ( type !== InventoryAction.None ) {
            let isQuestOrSweeper: boolean = InventoryAction.QuestReward === type || InventoryAction.Sweeper === type
            if ( count > 1 ) {
                let messageId = isQuestOrSweeper ? SystemMessageIds.EARNED_S2_S1_S : SystemMessageIds.YOU_PICKED_UP_S1_S2
                let packet = new SystemMessageBuilder( messageId )
                        .addItemNameWithId( itemId )
                        .addNumber( count )
                        .getBuffer()
                this.sendOwnedData( packet )
            } else {
                let messageId = isQuestOrSweeper ? SystemMessageIds.EARNED_ITEM_S1 : SystemMessageIds.YOU_PICKED_UP_S1
                let packet = new SystemMessageBuilder( messageId )
                        .addItemNameWithId( itemId )
                        .getBuffer()
                this.sendOwnedData( packet )
            }
        }

        if ( item.hasExImmediateEffect() ) {
            let handler: ItemHandlerMethod = item.isEtcItem() ? ( item as L2EtcItem ).getItemHandler() : null
            if ( handler ) {
                let createdItem: L2ItemInstance = L2ItemInstance.fromItemId( itemId )
                await handler( this, createdItem, false )
                await ItemManagerCache.destroyItem( createdItem )
            }

            return null
        }

        let createdItem: L2ItemInstance = await this.inventory.addItem( itemId, count, referenceId, 'L2PcInstance.addItem', enchantLevel )

        if ( !this.hasActionOverride( PlayerActionOverride.ItemAction )
                && !this.inventory.validatePlayerCapacity( 0, item.isQuestItem() )
                && createdItem.isDropable()
                && ( !createdItem.isStackable() || ( createdItem.getInventoryStatus() !== InventoryUpdateStatus.Modified ) ) ) {
            await this.dropItem( createdItem, true, false )
        } else if ( CursedWeaponManager.isCursedItem( createdItem.getId() ) ) {
            await CursedWeaponManager.activate( this, createdItem )
        } else if ( ( createdItem.getId() >= 13560 ) && ( createdItem.getId() <= 13568 ) ) { // Territory Ward
            let ward: TerritoryWard = TerritoryWarManager.getTerritoryWardById( createdItem.getId() - 13479 )
            if ( ward ) {
                await ward.activate( this, createdItem )
            }
        }

        return createdItem
    }


    async addLevel( levels: number ): Promise<void> {
        let currentLevel = this.getLevel()
        let maximumLevel = this.getMaxLevel()

        if ( ( currentLevel + levels ) > maximumLevel ) {
            return
        }

        let isLevelIncreased = this.getSubStat().addLevel( levels )
        await this.onLevelChange( isLevelIncreased )

        if ( ListenerCache.hasGeneralListener( EventType.PlayerChangedLevel ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerChangedLevel ) as PlayerChangedLevelEvent
            eventData.playerId = this.getObjectId()
            eventData.oldLevelValue = currentLevel
            eventData.newLevelValue = currentLevel + levels

            return ListenerCache.sendGeneralEvent( EventType.PlayerChangedLevel, eventData )
        }
    }

    getExp(): number {
        if ( this.isSubClassActive() ) {
            return this.getSubClasses()[ this.getClassIndex() ].getStat().getExp()
        }

        return this.getStat().getExp()
    }

    getKarma(): number {
        return this.karma
    }

    addManufactureItem( recipeId: number, price: number ) : void {
        this.manufactureItems.push( createManufactureItem( recipeId, price ) )
    }

    addSp( amount: number ) {
        this.getSubStat().addSp( amount )
    }

    addTimestampItem( item: L2ItemInstance, reuseDelay: number ) {
        this.itemReuse.add( item, reuseDelay )
    }

    getItemReuse() : ItemReuseTime {
        return this.itemReuse
    }

    addTransformSkill( skill: Skill ): void {
        this.transformSkills[ skill.getId() ] = skill

        if ( skill.isPassive() ) {
            this.addSkill( skill, false )
        }
    }

    getRelationUpdateRadius() : number {
        return L2Region.Length
    }

    broadcastCharacterInformation() {
        if ( !this.getWorldRegion() ) {
            return
        }

        let players = L2World.getVisiblePlayers( this, this.getRelationUpdateRadius() )

        if ( players.length === 0 ) {
            return
        }

        let summon = this.getSummon()
        let packet = new CharacterInformation( this )
        let party = this.getParty()
        let clan = this.getClan()

        players.forEach( ( otherPlayer: L2PcInstance ) => {
            if ( !otherPlayer || !this.isVisibleFor( otherPlayer ) ) {
                return
            }

            otherPlayer.sendOwnedData( packet.getBuffer( otherPlayer ) )

            /*
                TODO : move relation calculations to separate cache
                - recompute on specific ms interval (no debounced method, use epoch time as tracking)
                - inspect when such relation must be cleared (clan/party/block-checker arena has changed state)
             */
            let relation = this.getRelation( otherPlayer, party, clan )
            let isAttackPossible = this.isAutoAttackable( otherPlayer as unknown as L2Character )

            otherPlayer.sendOwnedData( RelationChanged( this, relation, isAttackPossible ) )
            if ( summon ) {
                otherPlayer.sendOwnedData( RelationChanged( summon, relation, isAttackPossible ) )
            }
        } )
    }

    broadcastSnoop( type: number, characterName: string, text: string ) {
        if ( this.snoopListeners.length > 0 ) {
            let packet: Buffer = Snoop( this.objectId, this.getName(), type, characterName, text )
            _.each( this.snoopListeners, ( playerId: number ) => {
                PacketDispatcher.sendCopyData( playerId, packet )
            } )
        }
    }

    broadcastSocialAction( id: number ) {
        BroadcastHelper.dataBasedOnVisibility( this, SocialAction( this.objectId, id ) )
    }

    broadcastTitleInfo() {
        this.sendDebouncedPacket( UserInfo )
        this.sendDebouncedPacket( ExBrExtraUserInfo )

        BroadcastHelper.dataBasedOnVisibility( this, NicknameChanged( this ) )
    }

    runBroadcastUserInfo() {
        this.sendDebouncedPacket( UserInfo )
        this.sendDebouncedPacket( ExBrExtraUserInfo )

        BroadcastHelper.dataBasedOnVisibility( this, ExBrExtraUserInfo( this.objectId ) )

        this.broadcastCharacterInformation()

        if ( TerritoryWarManager.isTWInProgress
                && ( TerritoryWarManager.checkIsRegistered( -1, this.objectId ) || TerritoryWarManager.checkIsRegisteredClan( -1, this.getClan() ) ) ) {
            BroadcastHelper.dataToSelfBasedOnVisibility( this, ExDominionWarStart( this ) )
        }
    }

    calculateDeathPenaltyBuffLevel( killer: L2Character ): void {
        if ( !killer ) {
            return
        }

        if ( this.isResurrectSpecialAffected()
            || this.isLucky()
            || this.isBlockedFromDeathPenalty()
            || this.isInArea( AreaType.PVP )
            || this.isInSiegeArea()
            || this.hasActionOverride( PlayerActionOverride.DeathPenalty ) ) {
            return
        }
        let percent = 1.0

        if ( killer.isRaid() ) {
            percent *= this.calculateStat( Stats.REDUCE_DEATH_PENALTY_BY_RAID, 1, null, null )
        } else if ( killer.isMonster() ) {
            percent *= this.calculateStat( Stats.REDUCE_DEATH_PENALTY_BY_MOB, 1, null, null )
        } else if ( killer.isPlayable() ) {
            percent *= this.calculateStat( Stats.REDUCE_DEATH_PENALTY_BY_PVP, 1, null, null )
        }

        if ( ( !killer.isPlayable() || ( this.getKarma() > 0 ) )
                && _.random( 1, 100 ) <= ( ConfigManager.character.getDeathPenaltyChance() * percent ) ) {
            this.increaseDeathPenaltyBuffLevel()
        }
    }

    canMakeSocialAction() {
        return ( ( this.getPrivateStoreType() === PrivateStoreType.None )
                && !this.getActiveRequester()
                && !this.isAlikeDead()
                && !this.isAllSkillsDisabled()
                && !this.isCastingNow()
                && !this.isCastingSimultaneouslyNow()
                && this.getAIController().isIntent( AIIntent.WAITING ) )
    }

    canSummonTarget( target: L2PcInstance ) {
        if ( !target || target.objectId === this.objectId ) {
            return false
        }

        if ( target.isAlikeDead() ) {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_IS_DEAD_AT_THE_MOMENT_AND_CANNOT_BE_SUMMONED )
                    .addPlayerCharacterName( target )
                    .getBuffer()

            this.sendOwnedData( packet )
            return false
        }

        if ( target.isInStoreMode() ) {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_CURRENTLY_TRADING_OR_OPERATING_PRIVATE_STORE_AND_CANNOT_BE_SUMMONED )
                    .addPlayerCharacterName( target )
                    .getBuffer()

            this.sendOwnedData( packet )
            return false
        }

        if ( target.isRooted() || target.isInCombat() ) {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_IS_ENGAGED_IN_COMBAT_AND_CANNOT_BE_SUMMONED )
                    .addPlayerCharacterName( target )
                    .getBuffer()

            this.sendOwnedData( packet )
            return false
        }

        if ( target.isInOlympiadMode() || OlympiadManager.isRegisteredInCompetition( target ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_SUMMON_PLAYERS_WHO_ARE_IN_OLYMPIAD ) )
            return false
        }

        if ( target.isInArea( AreaType.SevenSigns )
            || target.isFlyingMounted()
            || target.isCombatFlagEquipped()
            || !TvTEvent.onEscapeUse( target.getObjectId() ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_TARGET_IS_IN_AN_AREA_WHICH_BLOCKS_SUMMONING ) )
            return false
        }

        if ( target.inObserverMode() ) {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_STATE_FORBIDS_SUMMONING )
                    .addPlayerCharacterName( target )
                    .getBuffer()

            this.sendOwnedData( packet )
            return false

        }

        if ( target.isInArea( AreaType.NoSummonPlayer ) || target.isInArea( AreaType.Jail ) ) {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_IN_SUMMON_BLOCKING_AREA )
                    .addPlayerCharacterName( target )
                    .getBuffer()

            this.sendOwnedData( packet )
            return false
        }

        if ( this.isInArea( AreaType.NoSummonPlayer ) || this.isInArea( AreaType.Jail ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_TARGET_IS_IN_AN_AREA_WHICH_BLOCKS_SUMMONING ) )
            return false
        }

        if ( this.getInstanceId() > 0 && ( !ConfigManager.general.allowSummonInInstance() || !InstanceManager.getInstance( this.getInstanceId() ).isSummonAllowed() ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_MAY_NOT_SUMMON_FROM_YOUR_CURRENT_LOCATION ) )
            return false
        }

        // TODO: on retail character can enter 7s dungeon with summon friend, but should be teleported away by mobs
        if ( this.isIn7sDungeon() && SevenSigns.isSealValidationPeriod() && !SevenSigns.hasWinningSide( this.getObjectId() ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_TARGET_IS_IN_AN_AREA_WHICH_BLOCKS_SUMMONING ) )
            return false
        }

        return true
    }

    cancelActiveTrade() {
        if ( !this.activeTradeList ) {
            return
        }

        let partner: L2PcInstance = this.activeTradeList.getPartner()
        if ( partner ) {
            partner.onTradeCancel( this )
        }

        this.onTradeCancel( this )
    }

    changeKarma( exp: number ) {
        if ( !this.isCursedWeaponEquipped() && this.getKarma() > 0 && ( this.isGM() || !this.isInArea( AreaType.PVP ) ) ) {
            let karmaLost: number = Formulas.calculateKarmaLost( this, exp )
            if ( karmaLost > 0 ) {
                this.setKarma( this.getKarma() - karmaLost )

                let packet = new SystemMessageBuilder( SystemMessageIds.YOUR_KARMA_HAS_BEEN_CHANGED_TO_S1 )
                        .addNumber( this.getKarma() )
                        .getBuffer()

                this.sendOwnedData( packet )
            }
        }
    }

    checkItemManipulation( objectId: number, count: number ) {
        if ( !L2World.getObjectById( objectId ) ) {
            return null
        }

        let item: L2ItemInstance = this.getInventory().getItemByObjectId( objectId )

        if ( !item || ( item.getOwnerId() !== this.getObjectId() ) ) {
            return null
        }

        if ( ( count < 0 ) || ( ( count > 1 ) && !item.isStackable() ) ) {
            return null
        }

        if ( count > item.getCount() ) {
            return null
        }

        if ( ( this.hasSummon() && ( this.getSummon().getControlObjectId() === objectId ) ) || ( this.getMountObjectId() === objectId ) ) {
            return null
        }

        if ( this.getActiveEnchantItemId() === objectId ) {
            return null
        }

        if ( item.isAugmented() && ( this.isCastingNow() || this.isCastingSimultaneouslyNow() ) ) {
            return null
        }

        return item
    }

    async checkItemRestriction(): Promise<void> {
        let player = this

        return aigle.resolve( InventorySlot.TotalSlots ).times( async ( slot: number ) => {
            let item: L2ItemInstance = player.getInventory().getPaperdollItem( slot )
            if ( !item || item.getItem().checkEquipConditions( player, false ) ) {
                return
            }

            await player.getInventory().unEquipItemInSlot( slot )

            if ( item.getItem().getBodyPart() === L2ItemSlots.Back ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLOAK_REMOVED_BECAUSE_ARMOR_SET_REMOVED ) )
                return
            }

            let removeMessage: SystemMessageBuilder
            if ( item.getEnchantLevel() > 0 ) {
                removeMessage = new SystemMessageBuilder( SystemMessageIds.EQUIPMENT_S1_S2_REMOVED )
                        .addNumber( item.getEnchantLevel() )
                        .addItemInstanceName( item )
            } else {
                removeMessage = new SystemMessageBuilder( SystemMessageIds.S1_DISARMED )
                        .addItemInstanceName( item )
            }

            player.sendOwnedData( removeMessage.getBuffer() )
        } ) as unknown as Promise<void>
    }

    checkLandingState(): boolean {
        if ( !this.isInArea( AreaType.AirLanding ) ) {
            return true
        }

        let clan = this.getClan()
        return this.isInSiegeArea()
                && !( clan && ( CastleManager.getCastle( this ) === CastleManager.getCastleByOwner( clan ) ) && ( this.objectId === clan.getLeaderId() ) )
    }

    async delevelPlayerSkills(): Promise<void> {
        if ( this.hasActionOverride( PlayerActionOverride.SkillAction ) || !ConfigManager.character.decreaseSkillOnDelevel() ) {
            return
        }

        let player = this
        let playerLevel = this.getLevel()
        let skillMap = SkillCache.getCompleteClassSkillIdMap( this.getClassId() )

        await aigle.resolve( this.getSkills() ).each( ( skill: Skill ): Promise<void | Skill> => {
            let learnedSkill: L2SkillLearn = SkillCache.getClassSkill( skill.getId(), skill.getLevel() % 100, player.getClassId() )
            if ( !learnedSkill ) {
                return
            }

            let levelDifference = skill.getId() === CommonSkill.EXPERTISE.getId() ? 0 : 9
            if ( playerLevel >= ( learnedSkill.getGetLevel() - levelDifference ) ) {
                return
            }

            let nextLevel = -1
            _.each( skillMap[ skill.getId() ], ( possibleSkill: L2SkillLearn ) => {
                if ( nextLevel < possibleSkill.getSkillLevel()
                        && playerLevel >= ( possibleSkill.getGetLevel() - levelDifference ) ) {
                    nextLevel = possibleSkill.getSkillLevel()
                }
            } )

            if ( nextLevel < 0 ) {
                return player.removeSkill( skill, true )
            }

            /*
                Attempting to decrease skill level
             */
            return player.addSkill( SkillCache.getSkill( skill.getId(), nextLevel ), true )
        } )
    }

    checkPvpSkill( target: L2Object, skill: Skill ) {
        if ( !target || !skill || !target.isCharacter() ) {
            return false
        }

        if ( this.objectId === target.objectId ) {
            return false
        }

        if ( !target.isPlayable() || ( target as L2Character ).isInArea( AreaType.Peace ) ) {
            return true
        }

        if ( skill.isDebuff() || skill.hasEffectType( L2EffectType.Steal ) || skill.isBad() ) {
            let targetPlayer: L2PcInstance = target.getActingPlayer()

            if ( this.getSiegeRole() !== SiegeRole.None
                    && this.getSiegeRole() === targetPlayer.getSiegeRole()
                    && this.getSiegeSide() === targetPlayer.getSiegeSide() ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FORCED_ATTACK_IS_IMPOSSIBLE_AGAINST_SIEGE_SIDE_TEMPORARY_ALLIED_MEMBERS ) )
                return false
            }

            if ( this.isInDuel() && targetPlayer.isInDuel() && this.getDuelId() === targetPlayer.getDuelId() ) {
                return true
            }

            let isSameTarget = this.getTarget().objectId === target.objectId
            let isCtrlPressed: boolean = this.mainSkillFlags.isCtrlPressed
            let isTargetInRange = ( skill.getEffectRange() > 0 ) && isCtrlPressed && isSameTarget && skill.isDamage()

            if ( this.isInParty() && targetPlayer.isInParty() ) {
                if ( this.getParty().getLeaderObjectId() === targetPlayer.getParty().getLeaderObjectId() ) {
                    return isTargetInRange
                }

                let commandChannel = this.getParty().getCommandChannel()
                if ( commandChannel && commandChannel.includesPlayer( targetPlayer ) ) {
                    return isTargetInRange
                }
            }

            // You can debuff anyone except party members while in an arena...
            if ( this.isInArea( AreaType.PVP ) && targetPlayer.isInArea( AreaType.PVP ) ) {
                return true
            }

            // Olympiad
            if ( this.isInOlympiadMode()
                    && targetPlayer.isInOlympiadMode()
                    && this.getOlympiadGameId() === targetPlayer.getOlympiadGameId() ) {
                return true
            }

            let aClan: L2Clan = this.getClan()
            let tClan: L2Clan = targetPlayer.getClan()

            if ( aClan && tClan ) {
                if ( aClan.isAtWarWith( tClan.getId() ) && tClan.isAtWarWith( aClan.getId() ) ) {
                    if ( ( skill.isAOE() && ( skill.getEffectRange() > 0 ) ) && isCtrlPressed && isSameTarget ) {
                        return true
                    }

                    return isCtrlPressed
                }

                if ( ( this.getClanId() === targetPlayer.getClanId() ) || ( ( this.getAllyId() > 0 ) && ( this.getAllyId() === targetPlayer.getAllyId() ) ) ) {
                    return isTargetInRange
                }
            }

            /*
                Do not debuff a player who has not engaged in PvP and has no karma.
             */
            if ( !targetPlayer.hasPvPFlag() && targetPlayer.getKarma() === 0 ) {
                return isTargetInRange
            }

            return true
        }

        return true
    }

    async initializeRecommendationBonus() {
        let taskTime: number = await DatabaseManager.getRecommendationBonus().loadPlayer( this )
        if ( taskTime > 0 ) {
            this.recommendationsBonusEnd = Date.now() + taskTime
            if ( taskTime === 3600000 ) {
                this.setRecommendationsLeft( this.getRecommendationsLeft() + 20 )
            }

            this.recommendationsBonusTask = setTimeout( this.runRecommendationsBonusTask.bind( this ), taskTime )
        }

        // TODO : L2J has 2x increase on initial run, then steady at 3600 seconds
        this.recommendationsGiveTask = setInterval( this.runRecommendationsGiveTask.bind( this ), 3600000 )
    }

    checkUseMagicConditions( skill: Skill ): boolean {
        if ( this.isOutOfControl() || this.isStunned() || this.isSleeping() ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.isDead() ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.isFishing() && !skill.isFishing() ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_FISHING_SKILLS_NOW ) )
            return false
        }

        if ( this.inObserverMode() ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.OBSERVERS_CANNOT_PARTICIPATE ) )
            this.abortCast()
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.isSitting() ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_MOVE_SITTING ) )
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( skill.isToggle() && this.isAffectedBySkill( skill.getId() ) ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        /*
            Fake death must be checked after toggles.
         */
        if ( this.isFakeDeath() ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        let target: L2Object
        let skillTargetType: L2TargetType = skill.getTargetType()
        let worldPosition = this.getGroundSkillLocation()

        if ( ( skillTargetType === L2TargetType.GROUND ) && !worldPosition ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        switch ( skillTargetType ) {
            case L2TargetType.AURA:
            case L2TargetType.FRONT_AURA:
            case L2TargetType.BEHIND_AURA:
            case L2TargetType.PARTY:
            case L2TargetType.CLAN:
            case L2TargetType.PARTY_CLAN:
            case L2TargetType.GROUND:
            case L2TargetType.SELF:
            case L2TargetType.AREA_SUMMON:
            case L2TargetType.AURA_CORPSE_MOB:
            case L2TargetType.COMMAND_CHANNEL:
            case L2TargetType.AURA_FRIENDLY:
            case L2TargetType.AURA_UNDEAD_ENEMY:
                target = this
                break
            case L2TargetType.PET:
            case L2TargetType.SERVITOR:
            case L2TargetType.SUMMON:
                target = this.getSummon()
                break
            default:
                target = this.getTarget()
                break
        }

        if ( !target ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        // skills can be used on Walls and Doors only during siege
        if ( target.isDoor() ) {
            let door: L2DoorInstance = target as L2DoorInstance

            if ( door.getCastle() && door.getCastle().getResidenceId() > 0 ) {
                if ( !door.getCastle().getSiege().isInProgress() ) {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
                    return false
                }
            } else if ( door.getFort() && door.getFort().getResidenceId() > 0 ) {
                if ( !door.getFort().getSiege().isInProgress() || !door.getIsShowHp() ) {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
                    return false
                }
            }
        }

        // Are the target and the player in the same duel?
        if ( this.isInDuel() ) {
            let otherPlayer: L2PcInstance = target.getActingPlayer()
            if ( otherPlayer && ( otherPlayer.getDuelId() !== this.getDuelId() ) ) {
                this.sendMessage( 'You cannot do this while duelling.' )
                this.sendOwnedData( ActionFailed() )
                return false
            }
        }

        if ( this.isSkillDisabled( skill ) ) {
            let remainingTimeMs = this.getSkillReuseRemainingMs( skill )
            if ( remainingTimeMs > 0 ) {
                let remainingSeconds = Math.floor( remainingTimeMs / 1000 )
                let hours = remainingSeconds / 3600
                let minutes = ( remainingSeconds % 3600 ) / 60
                let seconds = ( remainingSeconds % 60 )
                if ( hours > 0 ) {
                    let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S2_HOURS_S3_MINUTES_S4_SECONDS_REMAINING_FOR_REUSE_S1 )
                            .addSkillName( skill )
                            .addNumber( hours )
                            .addNumber( minutes )
                            .addNumber( seconds )
                            .getBuffer()
                    this.sendOwnedData( packet )
                } else if ( minutes > 0 ) {
                    let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S2_MINUTES_S3_SECONDS_REMAINING_FOR_REUSE_S1 )
                            .addSkillName( skill )
                            .addNumber( minutes )
                            .addNumber( seconds )
                            .getBuffer()
                    this.sendOwnedData( packet )
                } else {
                    let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S2_SECONDS_REMAINING_FOR_REUSE_S1 )
                            .addSkillName( skill )
                            .addNumber( seconds )
                            .getBuffer()
                    this.sendOwnedData( packet )
                }
            } else {
                let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_PREPARED_FOR_REUSE )
                        .addSkillName( skill )
                        .getBuffer()
                this.sendOwnedData( packet )
            }

            return false
        }

        if ( !skill.checkCondition( this, target ) ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( skill.isBad() ) {
            if ( ( this.isInsidePeaceZoneWithTarget( this, target ) ) && !this.getAccessLevel().hasPermission( PlayerPermission.AllowPeaceAttack ) ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IN_PEACEZONE ) )
                this.sendOwnedData( ActionFailed() )
                return false
            }

            if ( this.isInOlympiadMode() && !this.isOlympiadStart() ) {
                this.sendOwnedData( ActionFailed() )
                return false
            }

            let targetPlayer = target.getActingPlayer()
            if ( targetPlayer
                    && this.getSiegeRole() !== SiegeRole.None
                    && this.isInSiegeArea()
                    && targetPlayer.getSiegeRole() === this.getSiegeRole()
                    && targetPlayer.objectId !== this.objectId
                    && targetPlayer.getSiegeSide() === this.getSiegeSide() ) {
                if ( TerritoryWarManager.isTWInProgress ) {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_ATTACK_A_MEMBER_OF_THE_SAME_TERRITORY ) )
                } else {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FORCED_ATTACK_IS_IMPOSSIBLE_AGAINST_SIEGE_SIDE_TEMPORARY_ALLIED_MEMBERS ) )
                }
                this.sendOwnedData( ActionFailed() )
                return false
            }

            if ( !target.canBeAttacked() && !this.getAccessLevel().hasPermission( PlayerPermission.AllowPeaceAttack ) && !target.isDoor() ) {
                this.sendOwnedData( ActionFailed() )
                return false
            }

            if ( ( target.isInstanceType( InstanceType.L2EventMobInstance ) ) && ( target as L2EventMonsterInstance ).eventSkillAttackBlocked() ) {
                this.sendOwnedData( ActionFailed() )
                return false
            }

            if ( !target.isAutoAttackable( this ) && !this.mainSkillFlags.isCtrlPressed ) {
                switch ( skillTargetType ) {
                    case L2TargetType.AURA:
                    case L2TargetType.FRONT_AURA:
                    case L2TargetType.BEHIND_AURA:
                    case L2TargetType.AURA_CORPSE_MOB:
                    case L2TargetType.CLAN:
                    case L2TargetType.PARTY:
                    case L2TargetType.SELF:
                    case L2TargetType.GROUND:
                    case L2TargetType.AREA_SUMMON:
                    case L2TargetType.UNLOCKABLE:
                    case L2TargetType.AURA_FRIENDLY:
                    case L2TargetType.AURA_UNDEAD_ENEMY:
                        break
                    default:
                        this.sendOwnedData( ActionFailed() )
                        return false
                }
            }

            if ( this.mainSkillFlags.isShiftPressed ) {
                if ( skillTargetType === L2TargetType.GROUND ) {
                    if ( !this.isInsideRadiusCoordinates( worldPosition.getX(), worldPosition.getY(), worldPosition.getZ(), skill.getCastRange() + this.getTemplate().getCollisionRadius(), false ) ) {
                        this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_TOO_FAR ) )
                        this.sendOwnedData( ActionFailed() )
                        return false
                    }
                }

                if ( ( skill.getCastRange() > 0 ) && !this.isInsideRadius( target, skill.getCastRange() + this.getTemplate().getCollisionRadius() ) ) {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_TOO_FAR ) )
                    this.sendOwnedData( ActionFailed() )
                    return false
                }
            }
        }

        if ( skill.getEffectPoint() > 0 && target.isMonster() && !this.mainSkillFlags.isCtrlPressed ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        switch ( skillTargetType ) {
            case L2TargetType.PARTY:
            case L2TargetType.CLAN:
            case L2TargetType.PARTY_CLAN:
            case L2TargetType.AURA:
            case L2TargetType.FRONT_AURA:
            case L2TargetType.BEHIND_AURA:
            case L2TargetType.AREA_SUMMON:
            case L2TargetType.AURA_UNDEAD_ENEMY:
            case L2TargetType.GROUND:
            case L2TargetType.SELF:
            case L2TargetType.ENEMY:
                break
            default:
                if ( target.isPlayable()
                    && !this.getAccessLevel().hasPermission( PlayerPermission.AllowPeaceAttack )
                    && !this.checkPvpSkill( target, skill ) ) {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
                    this.sendOwnedData( ActionFailed() )
                    return false
                }
        }

        if ( skill.getCastRange() > 0 ) {

            if ( skillTargetType === L2TargetType.GROUND ) {
                if ( !PathFinding.canSeeTargetWithPosition( this, worldPosition ) ) {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_SEE_TARGET ) )
                    this.sendOwnedData( ActionFailed() )
                    return false
                }
            }

            if ( !PathFinding.canSeeTargetWithPosition( this, target ) ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_SEE_TARGET ) )
                this.sendOwnedData( ActionFailed() )
                return false
            }
        }

        if ( ( skill.isFlyType() ) && !PathFinding.canMoveBetweenLocations( this, target ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_TARGET_IS_LOCATED_WHERE_YOU_CANNOT_CHARGE ) )
            return false
        }

        return true
    }

    async unload() {
        if ( ListenerCache.hasGeneralListener( EventType.PlayerStartsLogout ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerStartsLogout ) as PlayerStartsLogoutEvent

            eventData.playerId = this.getObjectId()
            eventData.x = this.getX()
            eventData.y = this.getY()
            eventData.z = this.getZ()
            eventData.instanceId = this.getInstanceId()

            let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayerStartsLogout, eventData )
            if ( result && result.terminate ) {
                return
            }
        }

        this.loadState = PlayerLoadState.Unloading
        this.stopAutosave()
        let player = this

        if ( !this.isOnline() ) {
            console.log( 'L2PcInstance.unload called when user is offline!' )
        }

        await this.setOnlineStatus( false, true )

        if ( ConfigManager.general.enableBlockCheckerEvent() && this.getBlockCheckerArena() !== -1 ) {
            await BlockCheckerManager.onDisconnect( this )
        }

        await AIEffectHelper.setNextIntent( this, AIIntent.WAITING )
        this.isPlayerOnline = false

        let combatFlagItem = this.getInventory().getItemByItemId( 9819 )
        if ( combatFlagItem ) {
            let fort: Fort = FortManager.getFort( this )
            if ( fort ) {
                FortSiegeManager.dropCombatFlag( this, fort.getResidenceId() )
            } else {
                let slot = getSlotFromItem( combatFlagItem )
                await this.getInventory().unEquipItemInBodySlot( slot )
                await this.destroyItem( combatFlagItem, true, 'L2PcInstance.unload' )
            }
        } else if ( this.isCombatFlagEquipped() ) {
            await TerritoryWarManager.dropCombatFlag( this, false, false )
        }

        PartyMatchWaitingList.removePlayer( this )
        PartyMatchManager.removePlayer( this.getObjectId() )

        if ( this.isFlying() ) {
            await this.removeSkill( SkillCache.getSkill( 4289, 1 ) )
        }

        if ( this.isMounted() ) {
            await DatabaseManager.getPets().updateFood( this, this.mountNpcId )
        }

        if ( this.isFishing() && this.getFishCombat() ) {
            this.getFishCombat().unloadFishing()
        }

        await this.storeRecommendations()
        await this.stopAllTimers()

        this.setIsTeleporting( false )

        RecipeController.removeMaker( this.getObjectId() )

        this.setTarget( null )

        if ( this.isChannelized() ) {
            this.getSkillChannelized().abortChannelization()
        }

        AreaDiscoveryManager.exitAllAreas( this )
        await this.getEffectList().unloadAllEffects()
        this.stopCubics()

        AreaDiscoveryManager.removeCharacter( this )
        PlayerGroupCache.leaveParty( this )

        if ( OlympiadManager.isRegisteredNoble( this ) || ( this.getOlympiadGameId() !== -1 ) ) {
            OlympiadManager.removeDisconnectedCompetitor( this )
            OlympiadManager.unRegisterNoble( this )
        }


        let summon = this.getSummon()
        if ( summon ) {
            await summon.unSummon( this )

            if ( summon.isDead() ) {
                summon.sendSummonInfo( 0 )
            }
        }

        let clan = this.getClan()
        if ( clan ) {
            let clanMember: L2ClanMember = clan.getClanMember( this.getObjectId() )
            if ( clanMember ) {
                clanMember.setPlayerInstance( null )
            }
        }

        if ( this.getActiveRequester() ) {
            this.setActiveRequester( null )
            this.cancelActiveTrade()
        }

        if ( this.isGM() ) {
            AdminManager.deleteGm( this.getObjectId() )
        }

        if ( this.inObserverMode() ) {
            this.setLocationInvisible( this.lastLocation )
        }

        if ( this.getVehicle() ) {
            this.getVehicle().ejectPlayer( this )
        }

        let instanceId = this.getInstanceId()
        if ( ( instanceId !== 0 ) && !ConfigManager.general.restorePlayerInstance() ) {
            let instance: Instance = InstanceManager.getInstance( instanceId )
            if ( instance ) {
                instance.removePlayer( this.getObjectId() )
                let location: Location = instance.getExitLocation()
                if ( location ) {
                    let x: number = location.getX() + _.random( -30, 30 )
                    let y: number = location.getY() + _.random( -30, 30 )
                    this.setXYZInvisible( x, y, location.getZ() )
                    if ( summon ) {
                        await summon.teleportToLocation( location, true )
                        summon.setInstanceId( 0 )
                    }
                }
            }
        }

        TvTEvent.onLogout( this )
        await NevitManager.onPlayerLogout( this )

        await this.getInventory().deleteMe()
        await this.clearWarehouse()

        await this.getFreight().deleteMe()
        await this.clearRefund()

        await this.runSkillSave()

        if ( this.isCursedWeaponEquipped() ) {
            CursedWeaponManager.getCursedWeapon( this.cursedWeaponEquippedId ).setPlayer( null )
        }

        ProximalDiscoveryManager.removePlayer( this )
        await PlayerUICache.removeData( this.getObjectId() )
        await PlayerRecipeCache.unLoadPlayer( this.getObjectId() )
        await PlayerHennaCache.unLoadPlayer( this.getObjectId() )
        await MailManager.unloadPlayer( this.getObjectId() )
        await PlayerFriendsCache.unLoadPlayer( this.getObjectId(), this.getName() )

        if ( this.getClanId() > 0 ) {
            this.getClan().broadcastDataToOtherOnlineMembers( PledgeShowMemberListUpdate( this ), this )
        }

        _.each( this.snoopedPlayers, ( playerId: number ) => {
            let otherPlayer: L2PcInstance = L2World.getPlayer( playerId )
            if ( otherPlayer ) {
                otherPlayer.removeSnooper( player )
            }
        } )

        _.each( this.snoopListeners, ( playerId: number ) => {
            let otherPlayer: L2PcInstance = L2World.getPlayer( playerId )
            if ( otherPlayer ) {
                otherPlayer.removeSnooped( player )
            }
        } )

        PlayerMovementActionCache.clearAction( this.getObjectId() )
        PlayerVariablesManager.scheduleDeletion( this.getObjectId() )
        PlayerRadarCache.removeRadar( this.getObjectId() )
        AccountVariablesManager.emptyData( this.getObjectId() )
        QuestStateCache.unLoadForPlayer( this.getObjectId() )
        HtmlActionCache.removeActions( this.getObjectId() )
        DimensionalTransferManager.unLoadPlayer( this.getObjectId() )
        PlayerEventStatusCache.removeAll( this.getObjectId() )
        PlayerShortcutCache.unloadPlayer( this.getObjectId() )
        PlayerContactCache.unloadPlayer( this.getObjectId() )
        PlayerMacrosCache.unloadPlayer( this.getObjectId() )
        TeleportBookmarkCache.unloadPlayer( this.getObjectId() )
        StatsCache.deleteStats( this.getObjectId() )
        L2MoveManager.deleteMovementHooks( this.getMovementId() )
        SevenSigns.onLogout( this )

        this.cancelMethods()
        L2World.removePlayer( this )
        L2World.removeObject( this )
        PlayerRelationStatus.removeStatus( this.getObjectId() )

        this.removeSharedData()
        this.loadState = PlayerLoadState.Unloaded

        if ( ListenerCache.hasGeneralListener( EventType.PlayerLoggedOut ) ) {
            let exitData = EventPoolCache.getData( EventType.PlayerLoggedOut ) as PlayerLoggedOutEvent

            exitData.playerId = this.getObjectId()
            exitData.playerName = this.getName()
            exitData.sessionDurationMs = Date.now() - ( this.getClient()?.sessionStartTime ?? 0 )

            return ListenerCache.sendGeneralEvent( EventType.PlayerLoggedOut, exitData )
        }
    }

    clearCharges() {
        this.charges = 0
        this.sendDebouncedPacket( EtcStatusUpdate )
    }

    clearManufatureItems() : void {
        this.manufactureItems = []
    }

    async clearRefund(): Promise<void> {
        if ( this.refund ) {
            await this.refund.deleteMe()
        }

        this.refund = null
    }

    clearSouls() {
        this.souls = 0
        this.stopSoulTask()
        this.sendDebouncedPacket( EtcStatusUpdate )
    }

    async clearWarehouse(): Promise<void> {
        if ( this.warehouse ) {
            clearInterval( this.warehouseResetTask )
            await this.warehouse.deleteMe()
        }

        this.warehouse = null
        this.warehouseResetTask = null
    }

    createFishTask( checkDelay: number ) {
        let endTaskTime = Date.now() + ( this.fish.startCombatTime * 1000 ) + 10000
        this.lookForFishTask = setTimeout( this.runLookForFishTask.bind( this ), 10000, endTaskTime, checkDelay )
    }

    runLookForFishTask( endTaskTime: number, delay: number ) {
        if ( Date.now() >= endTaskTime ) {
            this.endFishing( false )
            return
        }

        if ( this.fish.gutsCheckProbability > _.random( 100 ) ) {
            this.startFishCombat()

            return
        }

        this.lookForFishTask = setTimeout( this.runLookForFishTask.bind( this ), delay, endTaskTime, delay )
    }

    getActingPlayer(): L2PcInstance {
        return this
    }

    getActingPlayerId(): number {
        return this.getObjectId()
    }

    getActiveWeaponInstance(): L2ItemInstance {
        return this.getInventory().getPaperdollItem( InventorySlot.RightHand )
    }

    getActiveWeaponItem(): L2Weapon {
        let weapon = this.getActiveWeaponInstance()

        if ( !weapon ) {
            return this.getFistsWeaponItem()
        }

        return weapon.item as L2Weapon
    }

    getClan(): L2Clan {
        return ClanCache.getClan( this.clanId )
    }

    getCurrentCp(): number {
        return this.getStatus().getCurrentCp()
    }

    getCurrentLoad(): number {
        return this.getInventory().getTotalWeight()
    }

    getDuelId(): number {
        return this.duelId
    }

    getInventory(): PcInventory {
        return this.inventory
    }

    getId() {
        return this.getClassId()
    }

    getLevel(): number {
        if ( this.isSubClassActive() ) {
            return this.getSubClasses()[ this.getClassIndex() ].getStat().getLevel()
        }

        return this.getSubStat().getLevel()
    }

    getLevelModifier(): number {
        if ( this.isTransformed() ) {
            let modifier = this.getTransformation().getLevelModifier( this )
            if ( modifier > -1 ) {
                return modifier
            }
        }

        return super.getLevelModifier()
    }

    getMaxLoad(): number {
        return this.calculateStat( Stats.WEIGHT_LIMIT, Math.floor( BaseStats.CON( this ) * 69000 * ConfigManager.character.getWeightLimit() ), this, null )
    }

    getParty(): L2Party {
        return PlayerGroupCache.getParty( this.getObjectId() )
    }

    getRace(): Race {
        if ( !this.isSubClassActive() ) {
            return this.getTemplate().getRace()
        }

        return DataManager.getPlayerTemplateData().getTemplateById( this.baseClass ).getRace()
    }

    getSecondaryWeaponItem(): L2Item {
        let item: L2ItemInstance = this.getInventory().getPaperdollItem( InventorySlot.LeftHand )
        if ( item ) {
            return item.getItem()
        }

        return null
    }

    getSiegeSide() {
        return this.siegeSide
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        if ( !attacker
                || attacker === this
                || attacker === this.getSummon()
                || attacker.isInstanceType( InstanceType.L2FriendlyMobInstance ) ) {
            return false
        }

        if ( attacker.isMonster() ) {
            return true
        }

        if ( attacker.isPlayable()
                && this.getDuelState() === DuelState.Action
                && this.getDuelId() === attacker.getActingPlayer().getDuelId() ) {
            let duel = DuelManager.getDuel( this.getDuelId() )
            return !( ( duel.getTeamA().includes( this.objectId ) && duel.getTeamA().includes( attacker.objectId ) ) ||
                    ( duel.getTeamB().includes( this.objectId ) && duel.getTeamB().includes( attacker.objectId ) ) )
        }

        if ( this.isInParty() && this.getParty().getMembers().includes( attacker.objectId ) ) {
            return false
        }

        if ( attacker.isPlayer() && attacker.getActingPlayer().isInOlympiadMode() ) {
            return this.isInOlympiadMode()
                    && this.isOlympiadStart()
                    && ( attacker as L2PcInstance ).getOlympiadGameId() === this.getOlympiadGameId()
        }

        if ( this.isOnEvent() ) {
            return true
        }

        if ( attacker.isPlayable() ) {
            if ( this.isInArea( AreaType.Peace ) ) {
                return false
            }

            let attackerPlayer: L2PcInstance = attacker.getActingPlayer()

            if ( this.getClan() ) {
                let siege: Siege = SiegeManager.getSiege( this.getX(), this.getY(), this.getZ() )
                if ( siege ) {
                    if ( siege.checkIsDefender( attackerPlayer.getClan() ) && siege.checkIsDefender( this.getClan() ) ) {
                        return false
                    }

                    if ( siege.checkIsAttacker( attackerPlayer.getClan() ) && siege.checkIsAttacker( this.getClan() ) ) {
                        return false
                    }
                }

                if ( this.getClan()
                        && attackerPlayer.getClan()
                        && this.getClan().isAtWarWith( attackerPlayer.getClanId() )
                        && attackerPlayer.getClan().isAtWarWith( this.getClanId() )
                        && this.getWantsPeace() === 0
                        && attackerPlayer.getWantsPeace() === 0
                        && !this.isAcademyMember() ) {
                    return true
                }
            }


            let bothInPvp = this.isInArea( AreaType.PVP ) && attackerPlayer.isInArea( AreaType.PVP )
            let bothInSiege = this.isInSiegeArea() && attackerPlayer.isInSiegeArea()
            if ( bothInPvp && !bothInSiege ) {
                return true
            }

            if ( this.getClan() && this.getClan().isMember( attacker.getObjectId() ) ) {
                return false
            }

            if ( attacker.isPlayer() && this.getAllyId() !== 0 && this.getAllyId() === attackerPlayer.getAllyId() ) {
                return false
            }

            if ( bothInPvp && bothInSiege ) {
                return true
            }
        } else if ( attacker.isInstanceType( InstanceType.L2DefenderInstance ) ) {
            if ( this.getClan() ) {
                let siege: Siege = SiegeManager.getSiegeFromObject( this )
                return siege && siege.checkIsAttacker( this.getClan() )
            }
        }

        return this.getKarma() > 0 || this.hasPvPFlag()
    }

    isPlayer(): boolean {
        return true
    }

    sendCopyData( packet: Buffer ) {
        PacketDispatcher.sendCopyData( this.objectId, packet )
    }

    /*
        TODO: implement in-place packet size calculations and
        ensure that packet does not get copied in the process of sending it to client
     */
    sendOwnedData( packet: Buffer ) {
        PacketDispatcher.sendOwnedData( this.objectId, packet )
    }

    getPvpFlag(): PlayerPvpFlag {
        return this.pvpFlag
    }

    hasPvPFlag() : boolean {
        return this.pvpFlag !== PlayerPvpFlag.None
    }

    getSp() {
        return this.getSubStat().getSp()
    }

    getStat(): PcStats {
        return super.getStat() as PcStats
    }

    getStatus(): PcStatus {
        return super.getStatus() as PcStatus
    }

    async onLevelChange( isIncreased: boolean ): Promise<void> {
        let maxCp = this.getMaxCp()
        let maxHp = this.getMaxHp()
        let maxMp = this.getMaxMp()

        if ( isIncreased ) {
            if ( ConfigManager.tuning.getLevelupRestoreCP() > 0 ) {
                let value: number = Math.floor( maxCp * ConfigManager.tuning.getLevelupRestoreCP() )
                this.setCurrentCp( Math.min( maxCp, this.getCurrentCp() + value ) )
            }

            if ( ConfigManager.tuning.getLevelupRestoreHP() > 0 ) {
                let value: number = Math.floor( maxHp * ConfigManager.tuning.getLevelupRestoreHP() )
                this.setCurrentHp( Math.min( maxHp, this.getCurrentHp() + value ) )
            }

            if ( ConfigManager.tuning.getLevelupRestoreMP() > 0 ) {
                let value = Math.floor( maxMp * ConfigManager.tuning.getLevelupRestoreMP() )
                this.setCurrentMp( Math.min( maxMp, this.getCurrentMp() + value ) )
            }

            BroadcastHelper.dataToSelfBasedOnVisibility( this, SocialAction( this.objectId, SocialActionType.LevelUp ) )

            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_INCREASED_YOUR_LEVEL ) )
        }

        await this.rewardSkills()

        let clan: L2Clan = this.getClan()
        if ( clan ) {
            clan.updateClanMember( this )
            clan.broadcastDataToOnlineMembers( PledgeShowMemberListUpdate( this ) )
        }

        if ( this.isInParty() ) {
            this.getParty().recalculatePartyLevel()
        }

        if ( this.getTransformation() ) {
            await this.getTransformation().onLevelUp( this )
        }

        let pet: L2PetInstance = this.getSummon() as L2PetInstance
        if ( pet
                && pet.getPetData().isSyncLevel()
                && pet.getLevel() !== this.getLevel() ) {
            let previousLevel = pet.getLevel()

            pet.getStat().setLevel( this.getLevel() )
            pet.getStat().getExpForLevel( this.getLevel() )

            await pet.onLevelChange( this.getLevel() > previousLevel )
        }

        let update = new StatusUpdate( this.getObjectId() )
                .addAttribute( StatusUpdateProperty.Level, this.getLevel() )
                .addAttribute( StatusUpdateProperty.MaxCP, maxCp )
                .addAttribute( StatusUpdateProperty.MaxHP, maxHp )
                .addAttribute( StatusUpdateProperty.MaxMP, maxMp )
                .getBuffer()

        this.sendOwnedData( update )

        this.broadcastStatusUpdate()
        this.refreshOverloadPenalty()
        this.refreshExpertisePenalty()

        this.sendDebouncedPacket( UserInfo )
        this.sendDebouncedPacket( ExBrExtraUserInfo )
        this.sendOwnedData( ExVoteSystemInfoWithPlayer( this ) )

        return NevitManager.playerLevelup( this.getObjectId() )
    }

    async restoreEffects(): Promise<void> {
        return CharacterEffectsCache.restorePlayerEffects( this )
    }

    async storeEffect( storeEffects: boolean ): Promise<void> {
        if ( !ConfigManager.character.storeSkillCooltime() ) {
            return
        }

        return CharacterEffectsCache.updatePlayerEffects( this, storeEffects )
    }

    async storeMe(): Promise<void> {
        return this.storeInDatabase( true )
    }

    getSummon(): L2Summon {
        return this.summon
    }

    getTemplate(): L2PcTemplate {
        return super.getTemplate() as L2PcTemplate
    }

    getTransformation(): Transform {
        return this.transformation
    }

    isAlikeDead(): boolean {
        return this.isDead() || this.isFakeDeath()
    }

    isGM(): boolean {
        return this.getAccessLevel().hasPermission( PlayerPermission.IsGm )
    }

    isInDuel(): boolean {
        return this.duelState !== DuelState.None
    }

    isInParty(): boolean {
        return !!PlayerGroupCache.getParty( this.getObjectId() )
    }

    isOnEvent() {
        return !!PlayerEventStatusCache.getPlayerStatus( this.getObjectId() )
    }

    isTransformed(): boolean {
        return !!this.transformation && !this.transformation.isStance()
    }

    async rechargeShots( isPhysical: boolean = true, isMagical: boolean = true ) : Promise<void> {

        let player = this
        await aigle.resolve( this.activeSoulShots ).eachSeries( ( itemId: number ) => {
            let item: L2ItemInstance = player.getInventory().getItemByItemId( itemId )

            if ( !item ) {
                player.removeAutoSoulShot( itemId )
                return
            }

            let handler: ItemHandlerMethod = item.getEtcItem().getItemHandler()

            if ( !handler ) {
                return
            }

            if ( isPhysical && item.getItem().getDefaultAction() === ActionType.SOULSHOT ) {
                return handler( player, item, false )
            }

            if ( isMagical && item.getItem().getDefaultAction() === ActionType.SPIRITSHOT ) {
                return handler( player, item, false )
            }
        } )
    }

    async removeSkill( skill: Skill, store: boolean = true, cancelEffect: boolean = true ): Promise<Skill> {
        this.removeCustomSkill( skill )

        if ( !store ) {
            return super.removeSkill( skill, cancelEffect )
        }

        let oldSkill: Skill = await super.removeSkill( skill, cancelEffect )
        if ( oldSkill ) {
            await DatabaseManager.getCharacterSkills().deleteSkill( this, oldSkill )
        }

        if ( this.getTransformationId() || this.isCursedWeaponEquipped() ) {
            return oldSkill
        }

        if ( skill && !( skill.getId() >= 3080 && skill.getId() <= 3259 ) ) {
            PlayerShortcutCache.deleteById( this, skill.getId(), ShortcutType.SKILL )
        }

        return oldSkill
    }

    sendDamageMessage( target: L2Character, damage: number, isMagicCrit: boolean, isPowerCrit: boolean, isMiss: boolean ) {
        if ( isMiss ) {
            if ( target.isPlayer() ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.C1_EVADED_C2_ATTACK )
                        .addPlayerCharacterName( target.getActingPlayer() )
                        .addCharacterName( this )
                        .getBuffer()

                target.sendOwnedData( packet )
            }

            let packet = new SystemMessageBuilder( SystemMessageIds.C1_ATTACK_WENT_ASTRAY )
                    .addPlayerCharacterName( this )
                    .getBuffer()

            this.sendOwnedData( packet )
            return
        }

        if ( isPowerCrit ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.C1_HAD_CRITICAL_HIT )
                    .addPlayerCharacterName( this )
                    .getBuffer()
            this.sendOwnedData( packet )
        }

        if ( isMagicCrit ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CRITICAL_HIT_MAGIC ) )
        }

        if ( this.isInOlympiadMode() && target.isPlayer() && target.getActingPlayer().isInOlympiadMode() && ( target.getActingPlayer().getOlympiadGameId() === this.getOlympiadGameId() ) ) {
            OlympiadGameManager.notifyCompetitorDamage( this, damage )
        }


        if ( ( target.isInvulnerable() || target.isHpBlocked() ) && !target.isNpc() ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACK_WAS_BLOCKED ) )
            return
        }

        if ( target.isDoor() || target.isInstanceType( InstanceType.L2ControlTowerInstance ) ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.YOU_DID_S1_DMG )
                    .addNumber( damage )
                    .getBuffer()
            this.sendOwnedData( packet )
            return
        }

        let packet = new SystemMessageBuilder( SystemMessageIds.C1_DONE_S3_DAMAGE_TO_C2 )
                .addPlayerCharacterName( this )
                .addCharacterName( target )
                .addNumber( damage )
                .getBuffer()

        this.sendOwnedData( packet )
    }

    shouldDescribeState( player: L2PcInstance ): boolean {
        if ( !this.isVisibleFor( player ) ) {
            return false
        }

        if ( this.isInBoat() ) {
            this.setXYZLocation( this.getBoat() )
            PacketDispatcher.sendOwnedData( player.objectId, GetOnVehicle( this.objectId, this.getBoat().getObjectId(), this.getInVehiclePosition() ) )
        } else if ( this.isInAirShip() ) {
            this.setXYZLocation( this.getAirShip() )
            PacketDispatcher.sendOwnedData( player.objectId, ExGetOnAirShip( this.objectId, this.getAirShip().getObjectId(), this.getInVehiclePosition() ) )
        }

        this.updateRelations( player )

        switch ( this.getPrivateStoreType() ) {
            case PrivateStoreType.Sell:
                PacketDispatcher.sendOwnedData( player.objectId, PrivateStoreMessageSell( this ) )
                break
            case PrivateStoreType.PackageSell:
                PacketDispatcher.sendOwnedData( player.objectId, ExPrivateStoreSetWholesaleMessage( this ) )
                break
            case PrivateStoreType.Buy:
                PacketDispatcher.sendOwnedData( player.objectId, PrivateStoreMessageBuy( this ) )
                break
            case PrivateStoreType.Manufacture:
                PacketDispatcher.sendOwnedData( player.objectId, RecipeShopMessage( this ) )
                break
        }

        if ( this.isTransformed() ) {
            this.sendOwnedData( PlayerInformation( player, this ) )
        }

        return true
    }

    sendMessage( message: string ) {
        this.sendOwnedData( SystemMessageHelper.sendString( message ) )
    }

    setIsTeleporting( teleport: boolean, useWatchDog: boolean = true ) {
        super.setIsTeleporting( teleport )

        if ( !useWatchDog ) {
            return
        }

        if ( teleport ) {
            if ( !this.teleportTask && ( ConfigManager.character.getTeleportWatchdogTimeout() > 0 ) ) {
                this.teleportTask = setTimeout( this.runTeleportTask.bind( this ), ConfigManager.character.getTeleportWatchdogTimeout() )
            }

            return
        }

        if ( this.teleportTask ) {
            clearTimeout( this.teleportTask )
        }

        this.teleportTask = null
    }

    runTeleportTask(): Promise<void> {
        if ( !this.isTeleporting() ) {
            return
        }

        ProximalDiscoveryManager.forgetAllObjects( this.getObjectId() )
        return this.onTeleported()
    }

    setTeam( team: Team ) : void {
        this.team = team

        this.broadcastUserInfo()
        if ( this.hasSummon() ) {
            this.getSummon().broadcastStatusUpdate()
        }
    }

    teleportToLocation( location: ILocational, allowRandomOffset: boolean = true, instanceId: number = 0 ): Promise<void> {
        let currentVehicle = this.getVehicle()
        if ( currentVehicle && currentVehicle.isTeleporting() ) {
            this.setVehicle( null )
        }

        if ( this.isFlyingMounted() && location.getZ() < -1005 ) {
            return this.teleportToLocationCoordinates(
                    location.getX(),
                    location.getY(),
                    -1005,
                    location.getHeading(),
                    instanceId ? instanceId : location.getInstanceId(),
            )
        }

        return super.teleportToLocation( location, allowRandomOffset, instanceId )
    }

    onUpdateAbnormalEffect(): void {
        this.broadcastUserInfo()
    }
    sendDebouncedPacket( method: DebouncedPacketMethod ) {
        PacketDispatcher.sendDebouncedPacket( this.objectId, method )
    }

    cancelDebouncedPacket( method: DebouncedPacketMethod ) : void {
        PacketDispatcher.cancelDebouncedPacket( this.objectId, method )
    }

    setChargedShot( type: ShotType, isCharged: boolean ): void {
        let weapon: L2ItemInstance = this.getActiveWeaponInstance()
        if ( weapon ) {
            weapon.setChargedShot( type, isCharged )
        }
    }

    decreaseCharges( count: number ) {
        if ( this.charges < count ) {
            return false
        }

        this.charges -= count
        if ( this.charges === 0 ) {
            this.stopChargeTask()
        } else {
            this.restartChargeTask()
        }

        this.sendDebouncedPacket( EtcStatusUpdate )
        return true
    }

    decreaseSouls( amount: number ): number {
        if ( this.souls === 0 ) {
            return 0
        }

        let consumedSouls
        if ( this.souls <= amount ) {
            consumedSouls = this.souls
            this.souls = 0
            this.stopSoulTask()
        } else {
            this.souls -= amount
            consumedSouls = amount
            this.restartSoulTask()
        }

        this.sendDebouncedPacket( EtcStatusUpdate )
        return consumedSouls
    }

    async destroyItemWithCount( item: L2ItemInstance, count: number, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        let destroyedItem: L2ItemInstance = await this.inventory.destroyItem( item, count, this.getObjectId(), reason )

        if ( !destroyedItem ) {
            if ( sendMessage ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
            }
            return false
        }

        if ( sendMessage ) {
            let packet: SystemMessageBuilder
            if ( count > 1 ) {
                packet = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                        .addItemInstanceName( destroyedItem )
                        .addNumber( count )
            } else {
                packet = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                        .addItemInstanceName( destroyedItem )
            }

            this.sendOwnedData( packet.getBuffer() )
        }

        return true
    }

    disableAutoShot( itemId: number ) {
        if ( this.activeSoulShots.includes( itemId ) ) {
            this.removeAutoSoulShot( itemId )
            this.sendOwnedData( ExAutoSoulShot( itemId, ExAutoSoulShotStatus.Disabled ) )

            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.AUTO_USE_OF_S1_CANCELLED )
                    .addItemNameWithId( itemId )
                    .getBuffer()
            this.sendOwnedData( packet )
            return true
        }

        return false
    }

    async disarmWeapons() : Promise<boolean> {
        let weapon: L2ItemInstance = this.getInventory().getPaperdollItem( InventorySlot.RightHand )
        if ( !weapon ) {
            return true
        }

        if ( this.isCursedWeaponEquipped() ) {
            return false
        }

        if ( this.isCombatFlagEquipped() ) {
            return false
        }

        if ( weapon.getWeaponItem().isForceEquip ) {
            return false
        }

        let unequipedItems: Array<L2ItemInstance> = await this.getInventory().unEquipItemInBodySlotAndRecord( weapon.getItem().getBodyPart() )

        this.abortAttack()
        this.broadcastUserInfo()

        if ( unequipedItems.length > 0 ) {
            if ( unequipedItems[ 0 ].getEnchantLevel() > 0 ) {
                let message = new SystemMessageBuilder( SystemMessageIds.EQUIPMENT_S1_S2_REMOVED )
                        .addNumber( unequipedItems[ 0 ].getEnchantLevel() )
                        .addItemInstanceName( unequipedItems[ 0 ] )
                        .getBuffer()

                this.sendOwnedData( message )
            } else {
                let message = new SystemMessageBuilder( SystemMessageIds.S1_DISARMED )
                        .addItemInstanceName( unequipedItems[ 0 ] )
                        .getBuffer()

                this.sendOwnedData( message )
            }
        }

        return true
    }

    async dismount(): Promise<void> {
        let wasFlying = this.isFlyingValue

        this.sendOwnedData( SetupGauge( this.getObjectId(), SetupGaugeColors.Other, 0, 0 ) )

        let petId = this.mountNpcId
        await this.setMount( 0, 0 )
        this.stopFeed()
        if ( wasFlying ) {
            await this.removeSkill( CommonSkill.WYVERN_BREATH.getSkill() )
        }

        BroadcastHelper.dataToSelfBasedOnVisibility( this, Ride( this ) )
        this.setMountObjectID( 0 )

        await DatabaseManager.getPets().updateFood( this, petId )

        this.broadcastUserInfo()
    }

    async dropItem( item: L2ItemInstance, sendMessage: boolean, protectItem: boolean ): Promise<void> {
        let droppedItem = await this.inventory.dropItem( item )

        if ( !droppedItem ) {
            if ( sendMessage ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
            }

            return
        }

        let geometry = PointGeometry.acquire( GeometryId.PlayerDrop )
        geometry.prepareNextPoint()

        droppedItem.dropMe(
            this.getObjectId(),
            this.getX() + geometry.getX(),
            this.getY() + geometry.getY(),
            this.getZ() + 20,
            this.getInstanceId(),
            false )

        geometry.release()

        this.performDropItemNotifications( droppedItem, sendMessage, protectItem )
    }

    async dropItemAtCoordinates( objectId: number, count: number, x: number, y: number, z: number, sendMessage: boolean, protectItem: boolean ): Promise<L2ItemInstance> {
        let item: L2ItemInstance = await this.inventory.dropItemCount( objectId, count )

        if ( !item ) {
            if ( sendMessage ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
            }

            return null
        }

        item.dropMe( this.getObjectId(), x, y, z, this.getInstanceId(), false )

        this.performDropItemNotifications( item, sendMessage, protectItem )

        return item
    }

    performDropItemNotifications( item: L2ItemInstance, sendMessage: boolean, applyDropProtection: boolean ): void {
        if ( applyDropProtection ) {
            ItemProtectionCache.addProtection( item.getObjectId(), false )
        }

        if ( sendMessage ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.YOU_DROPPED_S1 )
                    .addItemInstanceName( item )
                    .getBuffer()
            this.sendOwnedData( packet )
        }

        if ( ListenerCache.hasItemTemplateEvent( item.getId(), EventType.PlayerDropItem ) ) {

            let eventData = EventPoolCache.getData( EventType.PlayerDropItem ) as PlayerDropItemEvent

            eventData.itemObjectId = this.getObjectId()
            eventData.itemTemplateId = item.getId()
            eventData.locationX = item.getX()
            eventData.locationY = item.getY()
            eventData.locationZ = item.getZ()
            eventData.playerId = this.getObjectId()
            eventData.amount = item.getCount()

            ListenerCache.sendItemTemplateEvent( this.getId(), EventType.PlayerDropItem, eventData )
        }
    }

    endFishing( isWin: boolean ) {
        this.stopLookingForFishTask()

        this.fishing = false
        this.fishX = 0
        this.fishY = 0
        this.fishZ = 0

        if ( !this.fishCombat ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.BAIT_LOST_FISH_GOT_AWAY ) )
        }

        this.fishCombat = null
        this.lure = null

        this.sendOwnedData( ExFishingEnd( this.objectId, isWin ) )
        this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.REEL_LINE_AND_STOP_FISHING ) )

        this.setIsImmobilized( false )
    }

    getAccountName(): string {
        return this.accountName
    }

    getActiveClass(): number {
        return this.activeClass
    }

    getActiveEnchantAttributeItemId() {
        return this.activeEnchantAttributeItemId
    }

    getActiveEnchantItemId() {
        return this.activeEnchantItemId
    }

    getActiveEnchantSupportItemId() {
        return this.activeEnchantSupportItemId
    }

    getActiveRequester(): L2PcInstance {
        let otherPlayer = L2World.getPlayer( this.activeRequesterId )
        if ( otherPlayer && otherPlayer.isRequestExpired() && !this.activeTradeList ) {
            this.activeRequesterId = 0
            return null
        }

        return otherPlayer
    }

    getActiveTradeList() {
        return this.activeTradeList
    }

    getActiveWarehouse(): ItemContainer {
        return this.activeWarehouse
    }

    getAdena(): number {
        return this.inventory.getAdenaAmount()
    }

    getAgathionId() {
        return this.agathionId
    }

    getAirShip(): L2AirShipInstance {
        return this.vehicle as L2AirShipInstance
    }

    getAllyCrestId(): number {
        if ( this.getClanId() === 0 || this.getClan().getAllyId() === 0 ) {
            return 0
        }

        return this.getClan().getAllyCrestId()
    }

    getAllyId(): number {
        let clan = this.getClan()
        if ( !clan ) {
            return 0
        }

        return clan.getAllyId()
    }

    getAncientAdena(): number {
        return this.inventory.getAncientAdenaCount()
    }

    getAppearance(): PcAppearance {
        return this.appearance
    }

    getApprentice(): number {
        return this.apprentice
    }

    getAutoSoulShot(): Array<number> {
        return this.activeSoulShots
    }

    getBaseClass() {
        return this.baseClass
    }

    getBaseExp(): number {
        return this.getStat().getExp()
    }

    getBaseLevel(): number {
        return this.getStat().getLevel()
    }

    getBaseSp(): number {
        return this.getStat().getSp()
    }

    getBaseTemplate(): L2PcTemplate {
        return DataManager.getPlayerTemplateData().getTemplateById( this.baseClass )
    }

    getBlockCheckerArena(): number {
        return this.blockCheckerEventArena
    }

    getBoat(): L2BoatInstance {
        return this.vehicle as L2BoatInstance
    }

    getBonusWeightPenalty() {
        return this.calculateStat( Stats.WEIGHT_PENALTY, 1, this, null )
    }

    getAvailableBookmarkSlots() {
        return this.availableBookmarkSlots
    }

    getBuyList() {
        if ( !this.buyList ) {
            this.buyList = new TradeList( this )
        }

        return this.buyList
    }

    getChargedSouls(): number {
        return this.souls
    }

    getCharges(): number {
        return this.charges
    }

    getChestArmorInstance(): L2ItemInstance {
        return this.getInventory().getPaperdollItem( InventorySlot.Chest )
    }

    getClanCreateExpiryTime() {
        return this.clanCreateExpiryTime
    }

    getClanCrestId(): number {
        let clan = this.getClan()
        if ( clan ) {
            return clan.getCrestId()
        }

        return 0
    }

    getClanCrestLargeId() {
        let clan = this.getClan()
        if ( clan && ( clan.getCastleId() !== 0 || clan.getHideoutId() !== 0 ) ) {
            return clan.getCrestLargeId()
        }

        return 0
    }

    getClanId(): number {
        return this.clanId
    }

    getClanJoinExpiryTime() {
        return this.clanJoinExpiryTime
    }

    getClanPriviledgesBitmask(): number {
        return this.clanPrivileges
    }

    getClassId(): number {
        return this.getTemplate().classId
    }

    getClassIndex() {
        return this.classIndex
    }

    getCollisionHeight() {
        if ( this.isMounted() && ( this.mountNpcId > 0 ) ) {
            return DataManager.getNpcData().getTemplate( this.getMountNpcId() ).getFCollisionHeight()
        }

        if ( this.isTransformed() ) {
            return this.getTransformation().getCollisionHeight( this )
        }

        let baseTemplate = this.getBaseTemplate()
        if ( this.appearance.isFemale ) {
            return baseTemplate.fCollisionHeightFemale
        }

        return baseTemplate.collisionHeight
    }

    getCollisionRadius(): number {
        if ( this.isMounted() && this.mountNpcId > 0 ) {
            return DataManager.getNpcData().getTemplate( this.getMountNpcId() ).getFCollisionRadius()
        }

        if ( this.isTransformed() ) {
            return this.getTransformation().getCollisionRadius( this )
        }

        let baseTemplate = this.getBaseTemplate()
        if ( this.appearance.isFemale ) {
            return baseTemplate.fCollisionRadiusFemale
        }

        return baseTemplate.collisionRadius
    }

    getCommonRecipeLimit() : number {
        return ConfigManager.character.getCommonRecipeLimit() + Math.floor( this.getStat().calculateStat( Stats.REC_C_LIM, 0, null, null ) )
    }

    getControlItemId() {
        return this.controlItemId
    }

    getCreateDate(): number {
        return this.createDate
    }

    getCubics(): Map<number, L2CubicInstance> {
        return this.cubics
    }

    getCurrentFeed() {
        return this.currentFeed
    }

    getGroundSkillLocation(): ILocational {
        return this.groundSkillLocation
    }

    getCursedWeaponEquippedId() {
        return this.cursedWeaponEquippedId
    }

    getCustomSkill( skillId: number ) {
        return this.customSkills[ skillId ]
    }

    getDeathPenaltyBuffLevel(): number {
        return this.deathPenaltyBuffLevel
    }

    getDeleteTimer() {
        return this.deleteTime
    }

    getWeightlessMode() {
        return this.weightlessMode
    }

    getDuelState(): DuelState {
        return this.duelState
    }

    getDwarfRecipeLimit() : number {
        return ConfigManager.character.getDwarfRecipeLimit() + Math.round( this.getStat().calculateStat( Stats.REC_D_LIM, 0, null, null ) )
    }

    getEnchantEffect() {
        let weapon: L2ItemInstance = this.getActiveWeaponInstance()
        if ( weapon ) {
            return Math.min( 127, weapon.getEnchantLevel() )
        }

        return 0
    }

    getExpBeforeDeath() {
        return this.expBeforeDeath
    }

    getExperiseLevel(): number {
        let level = this.getSkillLevel( 239 )
        if ( level < 0 ) {
            level = 0
        }

        return level
    }

    getExpertiseArmorPenalty(): number {
        return this.expertiseArmorPenalty
    }

    getExpertisePenaltyBonus(): number {
        return this.expertisePenaltyBonus
    }

    getExpertiseWeaponPenalty(): number {
        return this.expertiseWeaponPenalty
    }

    getFame(): number {
        return this.fame
    }

    getFeedConsume() {
        if ( this.isAttackingNow() ) {
            return this.getPetLevelData( this.mountNpcId ).getPetFeedBattle()
        }
        return this.getPetLevelData( this.mountNpcId ).getPetFeedNormal()
    }

    getFishCombat(): L2Fishing {
        return this.fishCombat
    }

    getFishX() {
        return this.fishX
    }

    getFishY() {
        return this.fishY
    }

    getFishZ() {
        return this.fishZ
    }

    getFistsWeaponItem(): L2Weapon {
        return this.fistsWeaponItem
    }

    getFreight() {
        return this.freight
    }

    getHennaStat( stat: string ): number {
        switch ( stat ) {
            case Stats.STAT_STR:
                return this.hennaSTR
            case Stats.STAT_CON:
                return this.hennaCON
            case Stats.STAT_DEX:
                return this.hennaDEX
            case Stats.STAT_INT:
                return this.hennaINT
            case Stats.STAT_WIT:
                return this.hennaWIT
            case Stats.STAT_MEN:
                return this.hennaMEN
        }

        return 0
    }

    getHennaStatCON() {
        return this.hennaCON
    }

    getHennaStatDEX() {
        return this.hennaDEX
    }

    getHennaStatINT() {
        return this.hennaINT
    }

    getHennaStatMEN() {
        return this.hennaMEN
    }

    getHennaStatSTR() {
        return this.hennaSTR
    }

    getHennaStatWIT() {
        return this.hennaWIT
    }

    getIPAddress() {
        let client: GameClient = this.getClient()
        if ( client && !client.isDetached() ) {
            return client.connection.remoteAddress ?? 'none'
        }

        return 'none'
    }

    getInVehiclePosition(): Location {
        return this.inVehiclePosition
    }

    getInventoryLimit(): number {
        let limit: number
        if ( this.isGM() ) {
            limit = ConfigManager.character.getMaximumSlotsForGMPlayer()
        } else if ( this.getRace() === Race.DWARF ) {
            limit = ConfigManager.character.getMaximumSlotsForDwarf()
        } else {
            limit = ConfigManager.character.getMaximumSlotsForNoDwarf()
        }

        return limit + Math.floor( this.getStat().calculateStat( Stats.INV_LIM, 0, null, null ) )
    }

    getLastAccess() {
        return this.lastAccess
    }

    getLastFolkNPC(): number {
        return this.lastFolkNpc
    }

    getLastLocation() {
        return this.lastLocation
    }

    getLevelJoinedAcademy() {
        return this.levelJoinedAcademy
    }

    getManufactureItems() : Array<L2ManufactureItem> {
        return this.manufactureItems
    }

    getMaxFeed() {
        return this.getPetLevelData( this.mountNpcId ).getPetMaxFeed()
    }

    getMaxLevel(): number {
        return this.getSubStat().getMaxLevel()
    }

    getMountLevel() {
        return this.mountLevel
    }

    getMountNpcId() {
        return this.mountNpcId
    }

    getMountObjectId(): number {
        return this.mountObjectId
    }

    getMountType(): MountType {
        return this.mountType
    }

    getMultiSociaAction(): number {
        return this.multiSociaAction
    }

    getNotMoveUntil() {
        return this.notMoveUntil
    }

    getOlympiadGameId(): number {
        return this.olympiadGameId
    }

    getOlympiadSide() {
        return this.olympiadSide
    }

    getOnlineBeginTime() {
        return this.onlineBeginTime
    }

    getOnlineTime() {
        return this.onlineTime
    }

    getPartyDistributionType(): PartyDistributionType {
        return this.partyDistributionType
    }

    getPetLevelData( npcId: number ): L2PetLevelData {
        if ( !this.leveldata ) {
            this.leveldata = DataManager.getPetData().getPetDataByNpcId( npcId ).getPetLevelData( this.getMountLevel() )
        }

        return this.leveldata
    }

    getPkKills(): number {
        return this.pkKills
    }

    getPledgeClass() {
        return this.pledgeClass
    }

    getPledgeType(): number {
        return this.pledgeType
    }

    getPowerGrade() {
        return this.powerGrade
    }

    getPrivateBuyStoreLimit() {
        let limit: number
        if ( this.getRace() === Race.DWARF ) {
            limit = ConfigManager.character.getMaxPvtStoreBuySlotsDwarf()
        } else {
            limit = ConfigManager.character.getMaxPvtStoreBuySlotsOther()
        }

        return limit + Math.floor( this.getStat().calculateStat( Stats.P_BUY_LIM, 0, null, null ) )
    }

    getPrivateSellStoreLimit() {
        let limit: number
        if ( this.getRace() === Race.DWARF ) {
            limit = ConfigManager.character.getMaxPvtStoreSellSlotsDwarf()
        } else {
            limit = ConfigManager.character.getMaxPvtStoreSellSlotsOther()
        }

        return limit + Math.floor( this.getStat().calculateStat( Stats.P_SELL_LIM, 0, null, null ) )
    }

    getPrivateStoreType(): PrivateStoreType {
        return this.privateStoreType
    }

    getPvpFlagLasts() {
        return this.pvpFlagEndTime
    }

    getPvpKills(): number {
        return this.pvpKills
    }

    getQuestInventoryLimit(): number {
        return ConfigManager.character.getMaximumSlotsForQuestItems()
    }

    getRandomFishGrade() {
        switch ( this.lure.getId() ) {
            case 7807: // green for beginners
            case 7808: // purple for beginners
            case 7809: // yellow for beginners
            case 8486: // prize-winning for beginners
                return 0
            case 8485: // prize-winning luminous
            case 8506: // green luminous
            case 8509: // purple luminous
            case 8512: // yellow luminous
                return 2
            default:
                return 1
        }
    }

    getRandomFishGroup( grade: number ) {
        let check = _.random( 100 )
        let type = 1
        switch ( grade ) {
            case 0: // fish for novices
                switch ( this.lure.getId() ) {
                    case 7807: // green lure, preferred by fast-moving (nimble) fish (type 5)
                        if ( check <= 54 ) {
                            type = 5
                        } else if ( check <= 77 ) {
                            type = 4
                        } else {
                            type = 6
                        }
                        break
                    case 7808: // purple lure, preferred by fat fish (type 4)
                        if ( check <= 54 ) {
                            type = 4
                        } else if ( check <= 77 ) {
                            type = 6
                        } else {
                            type = 5
                        }
                        break
                    case 7809: // yellow lure, preferred by ugly fish (type 6)
                        if ( check <= 54 ) {
                            type = 6
                        } else if ( check <= 77 ) {
                            type = 5
                        } else {
                            type = 4
                        }
                        break
                    case 8486: // prize-winning fishing lure for beginners
                        if ( check <= 33 ) {
                            type = 4
                        } else if ( check <= 66 ) {
                            type = 5
                        } else {
                            type = 6
                        }
                        break
                }
                break
            case 1: // normal fish
                switch ( this.lure.getId() ) {
                    case 7610:
                    case 7611:
                    case 7612:
                    case 7613:
                        type = 3
                        break
                    case 6519: // all theese lures (green) are prefered by fast-moving (nimble) fish (type 1)
                    case 8505:
                    case 6520:
                    case 6521:
                    case 8507:
                        if ( check <= 54 ) {
                            type = 1
                        } else if ( check <= 74 ) {
                            type = 0
                        } else if ( check <= 94 ) {
                            type = 2
                        } else {
                            type = 3
                        }
                        break
                    case 6522: // all theese lures (purple) are prefered by fat fish (type 0)
                    case 8508:
                    case 6523:
                    case 6524:
                    case 8510:
                        if ( check <= 54 ) {
                            type = 0
                        } else if ( check <= 74 ) {
                            type = 1
                        } else if ( check <= 94 ) {
                            type = 2
                        } else {
                            type = 3
                        }
                        break
                    case 6525: // all theese lures (yellow) are prefered by ugly fish (type 2)
                    case 8511:
                    case 6526:
                    case 6527:
                    case 8513:
                        if ( check <= 55 ) {
                            type = 2
                        } else if ( check <= 74 ) {
                            type = 1
                        } else if ( check <= 94 ) {
                            type = 0
                        } else {
                            type = 3
                        }
                        break
                    case 8484: // prize-winning fishing lure
                        if ( check <= 33 ) {
                            type = 0
                        } else if ( check <= 66 ) {
                            type = 1
                        } else {
                            type = 2
                        }
                        break
                }
                break
            case 2: // upper grade fish, luminous lure
                switch ( this.lure.getId() ) {
                    case 8506: // green lure, preferred by fast-moving (nimble) fish (type 8)
                        if ( check <= 54 ) {
                            type = 8
                        } else if ( check <= 77 ) {
                            type = 7
                        } else {
                            type = 9
                        }
                        break
                    case 8509: // purple lure, preferred by fat fish (type 7)
                        if ( check <= 54 ) {
                            type = 7
                        } else if ( check <= 77 ) {
                            type = 9
                        } else {
                            type = 8
                        }
                        break
                    case 8512: // yellow lure, preferred by ugly fish (type 9)
                        if ( check <= 54 ) {
                            type = 9
                        } else if ( check <= 77 ) {
                            type = 8
                        } else {
                            type = 7
                        }
                        break
                    case 8485: // prize-winning fishing lure
                        if ( check <= 33 ) {
                            type = 7
                        } else if ( check <= 66 ) {
                            type = 8
                        } else {
                            type = 9
                        }
                        break
                }
        }

        return type
    }

    getRandomFishLevel(): number {
        let skillLevel = 0
        let info = this.getEffectList().getBuffInfoBySkillId( 2274 )
        if ( info ) {
            switch ( info.getSkill().getLevel() ) {
                case 1:
                    skillLevel = 2
                    break
                case 2:
                    skillLevel = 5
                    break
                case 3:
                    skillLevel = 8
                    break
                case 4:
                    skillLevel = 11
                    break
                case 5:
                    skillLevel = 14
                    break
                case 6:
                    skillLevel = 17
                    break
                case 7:
                    skillLevel = 20
                    break
                case 8:
                    skillLevel = 23
                    break
            }
        } else {
            skillLevel = this.getSkillLevel( 1315 )
        }

        if ( skillLevel <= 0 ) {
            return 1
        }

        let level
        let check = _.random( 100 )

        if ( check <= 50 ) {
            level = skillLevel
        } else if ( check <= 85 ) {
            level = skillLevel - 1
            if ( level <= 0 ) {
                level = 1
            }
        } else {
            level = skillLevel + 1
            if ( level > 27 ) {
                level = 27
            }
        }

        return level
    }

    getRecommendationsBonusTime() {
        return Math.max( 0, this.recommendationsBonusEnd - Date.now() )
    }

    getRecommendationsBonusType() {
        // to maintain return 1
        return 0
    }

    getRecommendationsHave() {
        return this.recommendationsHave
    }

    getRecommendationsLeft() {
        return this.recommendationsLeft
    }

    getRefund(): PcRefund {
        if ( !this.refund ) {
            this.refund = new PcRefund( this )
        }

        return this.refund
    }

    getRelation( target: L2PcInstance, currentParty: L2Party, currentClan: L2Clan ): RelationChangedType {
        let result = PlayerRelationStatus.getStatus( this.getObjectId() )

        if ( currentParty && currentParty === target.getParty() ) {
            result |= RelationChangedType.InParty
            result |= currentParty.getMemberRelation( target.getObjectId() )
        }

        if ( target.getPledgeType() !== L2ClanValues.SUBUNIT_ACADEMY
                && this.getPledgeType() !== L2ClanValues.SUBUNIT_ACADEMY ) {

            let targetClan = target.getClan()
            if ( currentClan && targetClan && targetClan.isAtWarWith( currentClan.getId() ) ) {
                result |= RelationChangedType.ClanAgression
                if ( currentClan.isAtWarWith( targetClan.getId() ) ) {
                    result |= RelationChangedType.ClanWar
                }
            }
        }


        return result
    }

    getRequest(): L2Request {
        return this.request
    }

    getSellList() {
        if ( !this.sellList ) {
            this.sellList = new TradeList( this )
        }

        return this.sellList
    }

    getSiegeRole(): SiegeRole {
        return this.siegeRole
    }

    getSkillReuseRemainingMs( skill: Skill ) : number {
        let data = this.skillReuse.getItem( skill )
        if ( data ) {
            return 0
        }

        return data.getRemainingMs()
    }

    getSponsor() {
        return this.sponsorId
    }

    getStoreName() {
        return this.storeName
    }

    getSubClasses(): { [ key: number ]: SubClass } {
        return this.subClasses
    }

    getSubStat(): PcStats {
        if ( this.isSubClassActive() ) {
            let subclass: SubClass = this.getSubClasses()[ this.classIndex ]
            return subclass.stats
        }

        return this.getStat()
    }

    getTotalSubClasses(): number {
        return _.keys( this.getSubClasses() ).length
    }

    getTradeRefusal() {
        return this.tradeRefusal
    }

    setTradeRefusal( value: boolean ): void {
        this.tradeRefusal = value
    }

    getTranformationId(): number {
        return this.isTransformed() ? this.getTransformation().getId() : 0
    }

    getTransformSkill( skillId: number ) {
        return this.transformSkills[ skillId ]
    }

    getTransformationDisplayId() {
        return this.isTransformed() ? this.getTransformation().getDisplayId() : 0
    }

    getTransformationId() {
        return this.isTransformed() ? this.getTransformation().getId() : 0
    }

    getTrap() {
        return this.trap
    }

    getVehicle(): L2Vehicle {
        return this.vehicle
    }

    getVitalityPoints() {
        return this.getStat().getVitalityPoints()
    }

    getWantsPeace(): number {
        return this.wantsPeace
    }

    getWareHouseLimit() {
        let limit: number
        if ( this.getRace() === Race.DWARF ) {
            limit = ConfigManager.character.getMaximumWarehouseSlotsForDwarf()
        } else {
            limit = ConfigManager.character.getMaximumWarehouseSlotsForNoDwarf()
        }

        return limit + Math.floor( this.getStat().calculateStat( Stats.WH_LIM, 0, null, null ) )
    }

    getWarehouse() {
        return this.warehouse
    }

    getWeightPenalty(): number {
        if ( this.weightlessMode ) {
            return 0
        }

        return this.currentWeightPenalty
    }

    async giveAvailableAutoGetSkills(): Promise<any> {
        let autoGetSkills: Array<L2SkillLearn> = SkillCache.getAvailableAutoGetSkills( this )
        let player = this

        return aigle.resolve( autoGetSkills ).each( ( learnable: L2SkillLearn ) => {
            let skill = SkillCache.getSkill( learnable.getSkillId(), learnable.getSkillLevel() )
            if ( skill ) {
                return player.addSkill( skill, true )
            }
        } )
    }

    async giveAvailableSkills( includeFSSkills: boolean, includAutoGet: boolean ): Promise<number> {
        let counter = 0
        let skills: Array<Skill> = SkillCache.getAllAvailableSkills( this, this.getClassId(), includeFSSkills, includAutoGet )

        let player = this
        await aigle.resolve( skills ).each( async ( currentSkill: Skill ): Promise<void | Skill> => {
            if ( player.getKnownSkill( currentSkill.getId() ) === currentSkill ) {
                return
            }

            if ( player.getSkillLevel( currentSkill.getId() ) === -1 ) {
                counter++
            }

            if ( currentSkill.isToggle() && player.isAffectedBySkill( currentSkill.getId() ) ) {
                await player.stopSkillEffects( true, currentSkill.getId() )
            }

            return player.addSkill( currentSkill, true )
        } )

        if ( ConfigManager.character.autoLearnSkills() && counter > 0 ) {
            this.sendMessage( `You have learned ${ counter } new skills.` )
        }

        return counter
    }

    hasClanPrivilege( privilege: ClanPrivilege ) {
        return ClanPriviledgeHelper.includes( this.clanPrivileges, privilege )
    }

    hasDwarvenCraft(): boolean {
        return this.getDwarvenCraft() >= 1
    }

    getDwarvenCraft(): number {
        return this.getSkillLevel( CommonSkillIds.CreateDwarvenItems )
    }

    hasManufactureShop() {
        return this.manufactureItems.length > 0
    }

    hasQuestCompleted( name: string ): boolean {
        let state: QuestState = QuestStateCache.getQuestState( this.getObjectId(), name )
        return state && state.isCompleted()
    }

    hasAnyQuestsCompleted( ...names: Array<string> ): boolean {
        let ownerId = this.getObjectId()

        return _.some( names, ( name: string ) => {
            let state: QuestState = QuestStateCache.getQuestState( ownerId, name )
            return state && state.isCompleted()
        } )
    }

    hasRefund(): boolean {
        return this.refund && this.refund.getSize() > 0 && ConfigManager.general.allowRefund()
    }

    hasTransformSkill( id: number ): boolean {
        return !!this.transformSkills[ id ]
    }

    havePetInventoryItems() {
        return this.hasPetItems
    }

    inObserverMode() {
        return this.observerMode
    }

    increaseCharges( count: number, max: number ) {
        if ( this.charges >= max ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FORCE_MAXLEVEL_REACHED ) )
            return
        }

        this.restartChargeTask()

        this.charges += count
        if ( this.charges >= max ) {
            this.charges = max
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FORCE_MAXLEVEL_REACHED ) )
        } else {
            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.FORCE_INCREASED_TO_S1 )
                    .addNumber( this.charges )
                    .getBuffer()

            this.sendOwnedData( packet )
        }

        this.sendDebouncedPacket( EtcStatusUpdate )
    }

    async increaseDeathPenaltyBuffLevel(): Promise<void> {
        let penaltyLevel = this.getDeathPenaltyBuffLevel()
        if ( penaltyLevel >= 15 ) {
            return
        }

        if ( penaltyLevel !== 0 ) {
            let skill: Skill = SkillCache.getSkill( 5076, penaltyLevel )

            if ( skill ) {
                await this.removeSkill( skill, true )
            }
        }

        this.deathPenaltyBuffLevel++

        await this.addSkill( SkillCache.getSkill( 5076, this.getDeathPenaltyBuffLevel() ), false )
        this.sendDebouncedPacket( EtcStatusUpdate )

        let packet = new SystemMessageBuilder( SystemMessageIds.DEATH_PENALTY_LEVEL_S1_ADDED )
                .addNumber( this.getDeathPenaltyBuffLevel() )
                .getBuffer()
        this.sendOwnedData( packet )
    }

    increaseSouls( count: number ) {
        this.souls += count

        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.YOUR_SOUL_HAS_INCREASED_BY_S1_SO_IT_IS_NOW_AT_S2 )
                .addNumber( count )
                .addNumber( this.souls )
                .getBuffer()

        this.sendOwnedData( packet )

        this.restartSoulTask()
        this.sendDebouncedPacket( EtcStatusUpdate )
    }

    async initializeWarehouse() {
        if ( this.warehouse ) {
            this.initializeWarehouseResetTask()
            return
        }

        this.warehouse = new PcWarehouse( this )
        await this.warehouse.restore()

        this.initializeWarehouseResetTask()
    }

    initializeWarehouseResetTask() {
        /*
            Reset task that clears warehouse, since this method can be called to initialize warehouse in first place
            when warehouse is already available. It is possible that next tick of VM can clear warehouse before warehouse
            access can occur, hence we must re-initialize reset task if possible.
         */
        if ( this.warehouseResetTask ) {
            clearInterval( this.warehouseResetTask )
            this.warehouseResetTask = null
        }

        if ( ConfigManager.tuning.getWarehouseCacheIntervalMinutes() > 0 ) {
            this.warehouseResetTask = setInterval( this.runWarehouseResetTask.bind( this ), GeneralHelper.minutesToMillis( ConfigManager.tuning.getWarehouseCacheIntervalMinutes() ) )
        }
    }

    isAcademyMember(): boolean {
        return this.levelJoinedAcademy > 0
    }

    isChatBanned() {
        return PunishmentManager.hasPunishment( this.objectId, PunishmentEffect.CHARACTER, PunishmentType.JAIL )
                || PunishmentManager.hasPunishment( this.getAccountName(), PunishmentEffect.ACCOUNT, PunishmentType.JAIL )
                || PunishmentManager.hasPunishment( this.getIPAddress(), PunishmentEffect.IP, PunishmentType.JAIL )
    }

    isClanLeader(): boolean {
        let clan = this.getClan()
        if ( !clan ) {
            return false
        }

        return clan.isLeader( this.objectId )
    }

    isCombatFlagEquipped() {
        return this.combatFlagEquippedId
    }

    isCursedWeaponEquipped() {
        return this.cursedWeaponEquippedId !== 0
    }

    isEnchanting(): boolean {
        return this.isEnchantingValue
    }

    isFakeDeath() {
        return this.isFakeDeathValue
    }

    isFishing() {
        return this.fishing
    }

    isFlyingMounted(): boolean {
        return !!this.isTransformed() && this.getTransformation().isFlying()
    }

    isHero() {
        return this.hero
    }

    isHungry(): boolean {
        if ( this.canFeed ) {
            let hungryLimit = DataManager.getPetData().getPetDataByNpcId( this.getMountNpcId() ).getHungryLimit()
            return this.getCurrentFeed() < ( ( hungryLimit / 100 ) * this.getPetLevelData( this.getMountNpcId() ).getPetMaxFeed() )
        }

        return false
    }

    isIn7sDungeon() {
        return this.isIn7sDungeonValue
    }

    isInAirShip() {
        return this.vehicle && this.vehicle.isAirShip()
    }

    isInAllyWith( target: L2Character ) {
        if ( ( this.getAllyId() === 0 ) || ( target.getAllyId() === 0 ) ) {
            return false
        }
        return this.getAllyId() === target.getAllyId()
    }

    isInBoat(): boolean {
        return this.vehicle && this.vehicle.isBoat()
    }

    isInClanWith( target: L2Character ) {
        if ( ( this.getClanId() === 0 ) || ( target.getClanId() === 0 ) ) {
            return false
        }
        return this.getClanId() === target.getClanId()
    }

    isInCraftMode() {
        return this.inCraftMode
    }

    isInCrystallize(): boolean {
        return this.inCrystallize
    }

    isInLooterParty( id: number ): boolean {
        let party = this.getParty()
        if ( !party ) {
            return false
        }

        if ( party.isInCommandChannel() ) {
            return party.getCommandChannel().getMembers().includes( id )
        }

        return party.getMembers().includes( id )
    }

    isInOfflineMode() {
        let client: GameClient = this.getClient()
        return !client || client.isDetached()
    }

    isInOlympiadMode(): boolean {
        return this.inOlympiadMode
    }

    isInPartyWith( target: L2Character ) {
        if ( !this.isInParty() || !target.isInParty() ) {
            return false
        }
        return this.getParty().getLeaderObjectId() === target.getParty().getLeaderObjectId()
    }

    isInSiege() {
        return this.isInSiegeValue
    }

    isInStance() {
        return this.isTransformed() && this.getTransformation().isStance()
    }

    isInStoreMode() {
        return this.getPrivateStoreType() !== PrivateStoreType.None
    }

    isInVehicle() {
        return !!this.vehicle
    }

    isInWater() {
        return !!this.waterDamageTask
    }

    isInventoryDisabled() {
        return this.inventoryDisable
    }

    isJailed() {
        return PunishmentManager.hasPunishment( this.getObjectId(), PunishmentEffect.CHARACTER, PunishmentType.JAIL )
                || PunishmentManager.hasPunishment( this.getAccountName(), PunishmentEffect.ACCOUNT, PunishmentType.JAIL )
                || PunishmentManager.hasPunishment( this.getIPAddress(), PunishmentEffect.IP, PunishmentType.JAIL )
    }

    isLucky(): boolean {
        return this.getLevel() <= 9 && this.isAffectedBySkill( CommonSkill.LUCKY.getId() )
    }

    isMageClass(): boolean {
        return ClassId.getClassIdByIdentifier( this.getClassId() ).isMage
    }

    isMinimapAllowed() {
        return this.minimapAllowed
    }

    isMounted(): boolean {
        return this.mountType !== MountType.NONE
    }

    isNoble() {
        return this.noble
    }

    isOlympiadStart() {
        return this.isOlympiadStarted
    }

    isOnline(): boolean {
        return this.isPlayerOnline
    }

    isOnlineValue(): number {
        let client: GameClient = this.getClient()
        if ( this.isOnline() && client ) {
            return client.isDetached() ? 2 : 1
        }

        return 0
    }

    isPartyBanned() {
        return PunishmentManager.hasPunishment( this.getObjectId(), PunishmentEffect.CHARACTER, PunishmentType.PARTY_BAN )
    }

    isPartyWaiting() {
        return PartyMatchWaitingList.getPlayers().includes( this.objectId )
    }

    isProcessingRequest(): boolean {
        return !!this.getActiveRequester() || this.requestExpiration > Date.now()
    }

    isProcessingTransaction(): boolean {
        return !!this.getActiveRequester()
                || !!this.getActiveTradeList()
                || this.requestExpiration > Date.now()
    }

    isRecomendationTwoHoursGiven() {
        return this.recomendationsTwoHoursGivenValue
    }

    isRegisteredOnThisSiegeField( residenceId: number ) {
        if ( ( this.siegeSide !== residenceId ) && ( ( this.siegeSide < 81 ) || ( this.siegeSide > 89 ) ) ) {
            return false
        }

        return true
    }

    isRequestExpired() {
        return Date.now() > this.requestExpiration
    }

    isReviveRequested() {
        return this.reviveRequested === 1
    }

    isRevivingPet() {
        return this.revivePet
    }

    isSilenceMode(): boolean {
        return this.silenceMode
    }

    isSitting() {
        return this.waitTypeSitting
    }

    isSpawnProtected() {
        return this.protectEndTime > Date.now()
    }

    isSubClassActive(): boolean {
        return this.classIndex > 0
    }

    onTradeCancel( partner: L2PcInstance ) {
        if ( !this.activeTradeList ) {
            return
        }

        this.activeTradeList.clear()
        this.activeTradeList = null

        this.sendOwnedData( TradeDone( TradeDoneStatus.Reject ) )

        let packet = new SystemMessageBuilder( SystemMessageIds.C1_CANCELED_TRADE )
                .addPlayerCharacterName( partner )
                .getBuffer()

        this.sendOwnedData( packet )
    }

    onTradeConfirm( partner: L2PcInstance ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_CONFIRMED_TRADE )
                .addPlayerCharacterName( partner )
                .getBuffer()

        this.sendOwnedData( packet )
        this.sendOwnedData( TradeOtherDone() )
    }

    onTradeFinish( isSuccess: boolean ): void {
        if ( this.activeTradeList ) {
            this.activeTradeList.clear()
        }

        this.activeTradeList = null
        this.sendOwnedData( TradeDone( TradeDoneStatus.Accept ) )

        if ( isSuccess ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TRADE_SUCCESSFUL ) )
        }
    }

    onTradeStart( partner: L2PcInstance ): void {
        if ( this.activeTradeList ) {
            this.activeTradeList.clear()
        }

        this.activeTradeList = new TradeList( this )
        this.activeTradeList.setPartner( partner )

        let packet = new SystemMessageBuilder( SystemMessageIds.BEGIN_TRADE_WITH_C1 )
                .addPlayerCharacterName( partner )
                .getBuffer()

        this.sendOwnedData( packet )
        this.sendOwnedData( TradeStart( this ) )
    }

    onTransactionRequest( player: L2PcInstance ) {
        this.requestExpiration = Date.now() + L2PcInstanceValues.RequestDurationMillis
        player.setActiveRequester( this )
    }

    onTransactionResponse() {
        this.requestExpiration = 0
    }

    async processAddExpAndSp( addExp: number, addSp: number, useBonuses: boolean ): Promise<void> {
        if ( !this.getAccessLevel().hasPermission( PlayerPermission.GainExp ) ) {
            return
        }

        this.changeKarma( addExp )

        let baseExp = addExp
        let baseSp = addSp

        addExp *= _.get( PlayerVariablesManager.get( this.getObjectId(), PremiumMultipliersPropertyName ), MultipliersVariableNames.exp, 1 ) as number
        addSp *= _.get( PlayerVariablesManager.get( this.getObjectId(), PremiumMultipliersPropertyName ), MultipliersVariableNames.sp, 1 ) as number

        if ( useBonuses ) {
            addExp *= this.getStat().getExpBonusMultiplier()
            addSp *= this.getStat().getSpBonusMultiplier()
        }

        if ( !this.isInArea( AreaType.Peace ) && addExp > 0 && ConfigManager.character.isNevitEnabled() ) {
            NevitManager.startAdventTask( this.getObjectId() )

            if ( !this.getEffectList().getFirstEffect( L2EffectType.Nevit ) ) {
                NevitManager.setPauseHourglassStatus( this.getObjectId(), false )
            }
        }

        if ( this.hasPet() && GeneralHelper.checkIfInShortRadius( ConfigManager.character.getPartyRange(), this, this.getSummon(), false ) ) {
            let pet: L2PetInstance = this.getSummon() as L2PetInstance
            let ratioTakenByPlayer = pet.getPetLevelData().getOwnerXpRatio()

            if ( !pet.isDead() && ratioTakenByPlayer < 1 ) {
                await pet.addExpAndSp( addExp * ( 1 - ratioTakenByPlayer ) )
            }

            baseExp = addExp * ratioTakenByPlayer
            baseSp = addSp * ratioTakenByPlayer
            addExp = addExp * ratioTakenByPlayer
            addSp = addSp * ratioTakenByPlayer
        }

        let isSuccess: boolean = await this.getSubStat().addExp( addExp )
        if ( !isSuccess ) {
            return
        }

        this.getSubStat().addSp( addSp )

        let packet = new SystemMessageBuilder( SystemMessageIds.YOU_EARNED_S1_EXP_BONUS_S2_AND_S3_SP_BONUS_S4 )
                .addNumber( addExp )
                .addNumber( addExp - baseExp )
                .addNumber( addSp )
                .addNumber( addSp - baseSp )
                .getBuffer()

        this.sendOwnedData( packet )

        if ( addExp !== 0 || addSp !== 0 ) {
            let status = new StatusUpdate( this.getObjectId() )
                    .addAttribute( StatusUpdateProperty.Exp, this.getExp() )
                    .addAttribute( StatusUpdateProperty.SP, this.getSp() )
                    .getBuffer()

            this.sendOwnedData( status )
        }
    }

    queryGameGuard() {
        let client: GameClient = this.getClient()
        if ( client ) {
            client.setGameGuardOk( false )
            this.sendOwnedData( GameGuardQuery() )
        }

        // TODO : Gameguard check on interval
    }

    recalculateHennaStats() {
        this.hennaINT = 0
        this.hennaSTR = 0
        this.hennaCON = 0
        this.hennaMEN = 0
        this.hennaWIT = 0
        this.hennaDEX = 0

        let hennas = PlayerHennaCache.getHennas( this.getObjectId() )
        for ( const henna of hennas ) {
            if ( !henna ) {
                continue
            }

            this.hennaINT += ( ( this.hennaINT + henna.getStatINT() ) > 5 ) ? 5 - this.hennaINT : henna.getStatINT()
            this.hennaSTR += ( ( this.hennaSTR + henna.getStatSTR() ) > 5 ) ? 5 - this.hennaSTR : henna.getStatSTR()
            this.hennaMEN += ( ( this.hennaMEN + henna.getStatMEN() ) > 5 ) ? 5 - this.hennaMEN : henna.getStatMEN()
            this.hennaCON += ( ( this.hennaCON + henna.getStatCON() ) > 5 ) ? 5 - this.hennaCON : henna.getStatCON()
            this.hennaWIT += ( ( this.hennaWIT + henna.getStatWIT() ) > 5 ) ? 5 - this.hennaWIT : henna.getStatWIT()
            this.hennaDEX += ( ( this.hennaDEX + henna.getStatDEX() ) > 5 ) ? 5 - this.hennaDEX : henna.getStatDEX()
        }
    }

    async reduceAdena( count: number, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        if ( count > this.getAdena() ) {
            if ( sendMessage ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
            }
            return false
        }

        if ( count > 0 ) {
            let isAdenaReduced: boolean = await this.inventory.reduceAdena( count, reason )
            if ( !isAdenaReduced ) {
                return false
            }

            if ( sendMessage ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED_ADENA )
                        .addNumber( count )
                        .getBuffer()
                this.sendOwnedData( packet )
            }
        }

        return true
    }

    async reduceDeathPenaltyBuffLevel(): Promise<void> {
        let level = this.getDeathPenaltyBuffLevel()
        if ( level <= 0 ) {
            return
        }

        let skill: Skill = SkillCache.getSkill( 5076, level )

        if ( skill ) {
            await this.removeSkill( skill, true )
        }

        this.deathPenaltyBuffLevel--

        if ( this.getDeathPenaltyBuffLevel() > 0 ) {
            await this.addSkill( SkillCache.getSkill( 5076, this.getDeathPenaltyBuffLevel() ), false )
            this.sendDebouncedPacket( EtcStatusUpdate )

            let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.DEATH_PENALTY_LEVEL_S1_ADDED )
                    .addNumber( this.getDeathPenaltyBuffLevel() )
                    .getBuffer()
            this.sendOwnedData( packet )
            return
        }

        this.sendDebouncedPacket( EtcStatusUpdate )
        this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DEATH_PENALTY_LIFTED ) )
    }

    async runRefreshExpertisePenalty(): Promise<void> {
        if ( !ConfigManager.character.expertisePenalty() ) {
            return
        }

        let expertiseLevel = this.getExperiseLevel()
        let armorPenalty = 0
        let weaponPenalty = 0
        let crystalType

        this.getInventory().getItems().forEach( ( item: L2ItemInstance ) => {
            if ( item
                    && item.isEquipped()
                    && ![ EtcItemType.ARROW, EtcItemType.BOLT ].includes( item.getItemType() ) ) {
                crystalType = item.getItem().getCrystalType()

                if ( crystalType > expertiseLevel ) {
                    if ( item.isWeapon() && crystalType > weaponPenalty ) {
                        weaponPenalty = crystalType
                    } else if ( crystalType > armorPenalty ) {
                        armorPenalty = crystalType
                    }
                }
            }
        } )

        let isChanged = false
        let bonus = this.getExpertisePenaltyBonus()

        weaponPenalty = weaponPenalty - expertiseLevel - bonus
        weaponPenalty = Math.min( Math.max( weaponPenalty, 0 ), 4 )

        if ( this.getExpertiseWeaponPenalty() !== weaponPenalty
            || ( this.getKnownSkill( CommonSkillIds.WeaponGradePenalty ) && this.getKnownSkill( CommonSkillIds.WeaponGradePenalty ).getLevel() !== weaponPenalty ) ) {
            this.expertiseWeaponPenalty = weaponPenalty
            if ( this.expertiseWeaponPenalty > 0 ) {
                await this.addSkill( SkillCache.getSkill( CommonSkillIds.WeaponGradePenalty, this.expertiseWeaponPenalty ) )
            } else {
                await this.removeSkill( this.getKnownSkill( CommonSkillIds.WeaponGradePenalty ), false, true )
            }

            isChanged = true
        }

        armorPenalty = armorPenalty - expertiseLevel - bonus
        armorPenalty = Math.min( Math.max( armorPenalty, 0 ), 4 )

        if ( this.getExpertiseArmorPenalty() !== armorPenalty
            || ( this.getKnownSkill( CommonSkillIds.ArmorGradePenalty ) && this.getKnownSkill( CommonSkillIds.ArmorGradePenalty ).getLevel() !== armorPenalty ) ) {
            this.expertiseArmorPenalty = armorPenalty
            if ( this.expertiseArmorPenalty > 0 ) {
                await this.addSkill( SkillCache.getSkill( CommonSkillIds.ArmorGradePenalty, this.expertiseArmorPenalty ) )
            } else {
                await this.removeSkill( this.getKnownSkill( CommonSkillIds.ArmorGradePenalty ), false, true )
            }
            isChanged = true
        }

        if ( isChanged && this.canBroadcastUpdate() ) {
            this.sendDebouncedPacket( EtcStatusUpdate )
        }
    }

    async runRefreshOverloadPenalty(): Promise<void> {
        let maxLoad = this.getMaxLoad()
        if ( maxLoad > 0 ) {
            let currentLoad = this.getCurrentLoad()
            let weightproc = ( ( currentLoad - this.getBonusWeightPenalty() ) * 1000 ) / maxLoad
            let newWeightPenalty = 0
            if ( ( weightproc < 500 ) || this.weightlessMode ) {
                newWeightPenalty = 0
            } else if ( weightproc < 666 ) {
                newWeightPenalty = 1
            } else if ( weightproc < 800 ) {
                newWeightPenalty = 2
            } else if ( weightproc < 1000 ) {
                newWeightPenalty = 3
            } else {
                newWeightPenalty = 4
            }

            if ( this.currentWeightPenalty !== newWeightPenalty ) {
                this.currentWeightPenalty = newWeightPenalty

                if ( ( newWeightPenalty > 0 ) && !this.weightlessMode ) {
                    await this.addSkill( SkillCache.getSkill( 4270, newWeightPenalty ) )
                    this.setIsOverloaded( currentLoad > maxLoad )
                } else {
                    await this.removeSkill( this.getKnownSkill( 4270 ), false, true )
                    this.setIsOverloaded( false )
                }

                if ( !this.canBroadcastUpdate() ) {
                    return
                }

                this.sendDebouncedPacket( UserInfo )
                this.sendDebouncedPacket( EtcStatusUpdate )
                this.broadcastCharacterInformation()
                BroadcastHelper.dataBasedOnVisibility( this, ExBrExtraUserInfo( this.objectId ) )
            }
        }
    }

    removeAllTransformSkills() {
        this.transformSkills = {}
    }

    removeAutoSoulShot( itemId: number ) {
        return _.pull( this.activeSoulShots, itemId )
    }

    removeCubic( id: number ) : void {
        let cubic = this.cubics.get( id )
        if ( !cubic ) {
            return
        }

        cubic.terminate()

        this.cubics.delete( id )
    }

    removeCustomSkill( skill: Skill ): void {
        if ( skill && skill.getDisplayId() !== skill.getId() ) {
            _.unset( this.customSkills, skill.getDisplayId() )
        }
    }

    async removeExp( exp: number ) : Promise<void> {
        this.changeKarma( exp )
        await this.getSubStat().removeExp( exp )

        this.sendDebouncedPacket( UserInfo )
        this.sendDebouncedPacket( ExBrExtraUserInfo )
    }

    removeSnooped( player: L2PcInstance ) {
        _.pull( this.snoopedPlayers, player.getObjectId() )
    }

    removeSnooper( player: L2PcInstance ) {
        _.pull( this.snoopListeners, player.getObjectId() )
    }

    restartChargeTask() {
        this.stopChargeTask()
        this.chargeTask = setTimeout( this.clearCharges.bind( this ), 600000 )
    }

    restartSoulTask() {
        this.stopSoulTask()
        this.soulTask = setTimeout( this.clearSouls.bind( this ), 600000 )
    }

    restoreDeathPenaltyBuffLevel() {
        if ( this.getDeathPenaltyBuffLevel() > 0 ) {
            this.addSkill( SkillCache.getSkill( 5076, this.getDeathPenaltyBuffLevel() ), false )
        }
    }

    restoreExp( percent: number ): void {
        if ( this.getExpBeforeDeath() > 0 ) {
            this.getSubStat().addExp( Math.round( ( ( this.getExpBeforeDeath() - this.getExp() ) * percent ) / 100 ) )
            this.setExpBeforeDeath( 0 )
        }
    }

    reviveRequest( reviver: L2PcInstance, isPet: boolean, power: number, recoveryValue: number ) {
        if ( this.isResurrectionBlocked() ) {
            return
        }

        if ( this.reviveRequested === 1 ) {
            if ( this.revivePet === isPet ) {
                reviver.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.RES_HAS_ALREADY_BEEN_PROPOSED ) )
            } else {
                if ( isPet ) {
                    reviver.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_RES_PET2 ) )
                } else {
                    reviver.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MASTER_CANNOT_RES ) )
                }
            }
            return
        }

        if ( ( isPet && this.hasPet() && this.getSummon().isDead() ) || ( !isPet && this.isDead() ) ) {
            this.reviveRequested = 1
            this.reviveRecovery = recoveryValue

            this.revivePower = Formulas.calculateSkillResurrectRestorePercent( power, reviver )
            let restoreExp: number = Math.floor( ( ( this.getExpBeforeDeath() - this.getExp() ) * this.revivePower ) / 100 )
            this.revivePet = isPet

            if ( this.hasCharmOfCourageValue ) {
                let packet: Buffer = new ConfirmDialog( SystemMessageIds.RESURRECT_USING_CHARM_OF_COURAGE )
                        .addTime( 60000 )
                        .getBuffer()
                this.sendOwnedData( packet )
                return
            }

            let packet: Buffer = new ConfirmDialog( SystemMessageIds.RESURRECTION_REQUEST_BY_C1_FOR_S2_XP )
                    .addPlayerCharacterName( reviver )
                    .addString( Math.abs( restoreExp ).toString() )
                    .getBuffer()
            this.sendOwnedData( packet )
        }
    }

    async rewardSkills(): Promise<void> {
        if ( ConfigManager.character.autoLearnSkills() ) {
            await this.giveAvailableSkills( ConfigManager.character.autoLearnForgottenScrollSkills(), true )
        } else {
            await this.giveAvailableAutoGetSkills()
        }

        await this.delevelPlayerSkills()
        await this.checkItemRestriction()
        this.sendSkillList()
    }

    runRecommendationsBonusTask() {
        this.sendOwnedData( ExVoteSystemInfoWithPlayer( this ) )
    }

    runRecommendationsGiveTask() {
        // 10 recommendations to give out after 2 hours of being logged in
        // 1 more recommendation to give out every hour after that.
        let amountToGive = 1
        if ( !this.isRecomendationTwoHoursGiven() ) {
            amountToGive = 10
            this.setRecommendationTwoHoursGiven( true )
        }

        this.setRecommendationsLeft( this.getRecommendationsLeft() + amountToGive )

        let packet = new SystemMessageBuilder( SystemMessageIds.YOU_OBTAINED_S1_RECOMMENDATIONS )
                .addNumber( amountToGive )
                .getBuffer()
        this.sendOwnedData( packet )
        this.sendDebouncedPacket( UserInfo )
    }

    runSendSkillList(): void {
        let isDisabled = false
        let player = this
        let builder = new SkillListBuilder()

        _.each( this.getSkills(), ( skill: Skill ) => {
            if ( !skill ) {
                return
            }

            if ( ( player.isTransformed() && !skill.isPassive() ) || ( player.hasTransformSkill( skill.getId() ) && skill.isPassive() ) ) {
                return
            }

            let clan: L2Clan = player.getClan()
            if ( clan ) {
                isDisabled = skill.isClanSkill() && clan.getReputationScore() < 0
            }

            let isEnchantable = SkillCache.isEnchantable( skill.getId() )
            if ( isEnchantable ) {
                let learned: L2EnchantSkillLearn = DataManager.getSkillData().getSkillEnchantmentBySkillId( skill.getId() )
                if ( !learned || ( skill.getLevel() < learned.getBaseLevel() ) ) {
                    isEnchantable = false
                }
            }

            builder.addSkill( skill.getDisplayId(), skill.getDisplayLevel(), skill.isPassive(), isDisabled, isEnchantable )
        } )

        if ( this.transformation ) {
            let transformationSkills = new Map<number, number>()
            let currentTemplate = this.transformation.getTemplate( this )

            currentTemplate.getSkills().forEach( ( skill: Skill ) => {
                let level = transformationSkills.get( skill.getId() ) ?? 0
                if ( level < skill.getLevel() ) {
                    transformationSkills.set( skill.getId(), skill.getLevel() )
                }
            } )

            currentTemplate.getAdditionalSkills().forEach( ( data :TransformSkill ) => {
                if ( player.getLevel() >= data.minimumLevel ) {
                    let level = transformationSkills.get( data.skill.getId() ) ?? 0
                    if ( level < data.skill.getLevel() ) {
                        transformationSkills.set( data.skill.getId(), data.skill.getLevel() )
                    }
                }
            } )

            _.each( SkillCache.getCollectSkillTree(), ( learned: L2SkillLearn ) => {
                if ( player.getKnownSkill( learned.getSkillId() ) ) {
                    player.addTransformSkill( SkillCache.getSkill( learned.getSkillId(), learned.getSkillLevel() ) )
                }
            } )

            transformationSkills.forEach( ( value: number, skillId: number ) => {
                let skill: Skill = SkillCache.getSkill( skillId, value )
                player.addTransformSkill( skill )
                builder.addSkill( skillId, value, false, false, false )
            } )
        }

        this.sendOwnedData( builder.getBuffer() )
    }

    setAccessLevel( level: number, reason: string = '' ) : void {
        let oldData = this.accessLevel
        let newAccessLevel = PlayerAccessCache.getLevel( level )

        if ( !newAccessLevel ) {
            return
        }

        this.accessLevel = newAccessLevel
        this.getAppearance().setNameColor( this.accessLevel.nameColor )
        this.getAppearance().setTitleColor( this.accessLevel.titleColor )

        CharacterNamesCache.addPlayer( this )

        if ( this.loadState === PlayerLoadState.Unloaded
            || this.loadState === PlayerLoadState.Unloading
            || this.loadState === PlayerLoadState.None ) {
            return
        }

        this.broadcastUserInfo()

        if ( oldData && ListenerCache.hasGeneralListener( EventType.PlayerAccessLevelChanged ) ) {
            let data : PlayerAccessLevelChangedEvent = {
                playerId: this.getObjectId(),
                accountName: this.accountName,
                oldLevel: oldData.level,
                newLevel: level,
                reason
            }

            ListenerCache.sendGeneralEvent( EventType.PlayerAccessLevelChanged, data )
        }
    }

    setActiveClass( classId: number ) {
        this.activeClass = classId
    }

    setActiveEnchantItemId( objectId: number ) {
        // If we don't have a Enchant Item, we are not enchanting.
        if ( objectId === L2PcInstanceValues.EmptyId ) {
            this.setActiveEnchantSupportItemId( L2PcInstanceValues.EmptyId )
            this.setIsEnchanting( false )
        }

        this.activeEnchantItemId = objectId
    }

    setActiveEnchantSupportItemId( objectId: number ) {
        this.activeEnchantSupportItemId = objectId
    }

    setActiveRequester( player: L2PcInstance ) {
        this.activeRequesterId = player ? player.getObjectId() : null
    }

    setActiveWarehouse( value: ItemContainer ) {
        this.activeWarehouse = value
    }

    setAgathionId( npcId: number ) {
        this.agathionId = npcId
    }

    setApprentice( value: number ) {
        this.apprentice = value
    }

    setBaseClass( value: number ) {
        this.baseClass = value
    }

    setAvailableBookmarkSlots( value: number ) {
        this.availableBookmarkSlots = value
        this.sendOwnedData( ExGetBookMarkInfoPacketWithPlayer( this ) )
    }

    setClan( clan: L2Clan ) {
        if ( clan && clan.isMember( this.objectId ) ) {
            this.clanId = clan.getId()
            return
        }

        this.clanId = 0
        this.clanPrivileges = 0
        this.pledgeType = 0
        this.powerGrade = 0
        this.levelJoinedAcademy = 0
        this.apprentice = 0
        this.sponsorId = 0
        this.activeWarehouse = null
    }

    setClanCreateExpiryTime( value: number ) {
        this.clanCreateExpiryTime = value
    }

    setClanId( value: number ) {
        this.clanId = value
    }

    setClanJoinExpiryTime( value: number ) {
        this.clanJoinExpiryTime = value
    }

    setClanPrivileges( mask: number ) {
        this.clanPrivileges = mask
    }

    async setClassId( value: number ): Promise<void> {
        if ( ( this.getLevelJoinedAcademy() !== 0 ) && this.getClan() && ( getPlayerClassById( value ).level === ClassLevel.Third ) ) {
            if ( this.getLevelJoinedAcademy() >= 39 ) {
                await this.getClan().addReputationScore( ConfigManager.clan.getCompleteAcademyMinPoints(), true )
            } else {
                let modifier = _.clamp( this.getLevelJoinedAcademy() - 16, 0, 38 ) * 20
                await this.getClan().addReputationScore( ConfigManager.clan.getCompleteAcademyMaxPoints() - modifier, true )
            }

            this.setLevelJoinedAcademy( 0 )

            let packet = new SystemMessageBuilder( SystemMessageIds.CLAN_MEMBER_S1_EXPELLED )
                    .addPlayerCharacterName( this )
                    .getBuffer()

            this.getClan().broadcastDataToOnlineMembers( packet )
            this.getClan().broadcastDataToOnlineMembers( PledgeShowMemberListDelete( this.getName() ) )
            await this.getClan().removeClanMember( this.objectId, 0 )

            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ACADEMY_MEMBERSHIP_TERMINATED ) )
            await this.getInventory().addItem( 8181, 1, null, 'Gift' ) // give academy circlet
        }

        if ( this.isSubClassActive() ) {
            this.getSubClasses()[ this.classIndex ].setClassId( value )
        }

        this.setTarget( this )
        BroadcastHelper.dataToSelfBasedOnVisibility( this, MagicSkillUseWithCharacters( this, this, 5103, 1, 1000, 0 ) )

        this.setClassTemplate( value )

        if ( getPlayerClassById( this.getClassId() ).level === ClassLevel.Fourth ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIRD_CLASS_TRANSFER ) )
        } else {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLASS_TRANSFER ) )
        }

        if ( this.isInParty() ) {
            this.getParty().broadcastPacket( PartySmallWindowUpdate( this ) )
        }

        if ( this.getClan() ) {
            this.getClan().broadcastDataToOnlineMembers( PledgeShowMemberListUpdate( this ) )
        }

        await this.rewardSkills()
        return this.delevelPlayerSkills()
    }

    setClassIndex( value: number ) {
        this.classIndex = value
    }

    setClassTemplate( classId: number ) {
        let template: L2PcTemplate = DataManager.getPlayerTemplateData().getTemplateById( classId )
        if ( !template ) {
            throw new Error( `Missing player template data for classId = ${ classId }` )
        }

        this.activeClass = classId
        this.setTemplate( template )

        if ( ListenerCache.hasGeneralListener( EventType.PlayerChangeProfession ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerChangeProfession ) as PlayerChangeProfessionEvent
            eventData.playerId = this.getObjectId()
            eventData.classId = classId
            eventData.isSubclassActive = this.isSubClassActive()

            ListenerCache.sendGeneralEvent( EventType.PlayerChangeProfession, eventData )
        }
    }

    setCombatFlagEquipped( value: boolean ) {
        this.combatFlagEquippedId = value
    }

    setCreateDate( value: number ) {
        this.createDate = value
    }

    setCurrentFeed( value: number ) {
        let lastHungryState = this.isHungry()
        this.currentFeed = Math.min( value, this.getMaxFeed() )

        this.sendOwnedData( SetupGauge( this.getObjectId(), SetupGaugeColors.Other, Math.floor( ( this.getCurrentFeed() * 10000 ) / this.getFeedConsume() ), Math.floor( ( this.getMaxFeed() * 10000 ) / this.getFeedConsume() ) ) )

        if ( lastHungryState !== this.isHungry() ) {
            this.broadcastUserInfo()
        }
    }

    setCursedWeaponEquippedId( value: number ) {
        this.cursedWeaponEquippedId = value
    }

    setDeathPenaltyBuffLevel( value: number ) {
        this.deathPenaltyBuffLevel = value
    }

    setDecoy( decoy: L2Decoy ) {
        this.decoy = decoy
    }

    setDeleteTime( value: number ) {
        this.deleteTime = value
    }

    setWeightlessMode( mode: boolean ) {
        this.weightlessMode = mode
    }

    setExp( value: number ) {
        if ( value < 0 ) {
            value = 0
        }

        this.getSubStat().setExp( value )

        if ( this.canBroadcastUpdate() ) {
            this.sendOwnedData( StatusUpdate.forValue( this.getObjectId(), StatusUpdateProperty.Exp, this.getExp() ) )
        }
    }

    setExpBeforeDeath( value: number ) {
        this.expBeforeDeath = value
    }

    setExpertisePenaltyBonus( grade: number ) {
        this.expertisePenaltyBonus = grade
    }

    // TODO : rename to addFame and use modifier value to avoid callee getting fame and setting whole new value
    setFame( value: number ): void {
        let normalizedValue = _.clamp( value, ConfigManager.character.getMaxPersonalFamePoints() )

        if ( ListenerCache.hasGeneralListener( EventType.PlayerFameChanged ) ) {
            let eventData: PlayerChangedFameEvent = {
                playerId: this.getObjectId(),
                oldValue: this.fame,
                newValue: normalizedValue,
            }

            ListenerCache.sendGeneralEvent( EventType.PlayerFameChanged, eventData )
        }


        this.fame = normalizedValue
    }

    setFistsWeaponItem( fistWeapon: L2Weapon ) {
        this.fistsWeaponItem = fistWeapon
    }

    async setHero( value: boolean, forceUpdate: boolean = true ): Promise<void> {
        let heroSkills: Array<Skill> = SkillCache.getHeroSkills()
        let operation: Function = ( value && this.baseClass === this.activeClass ) ? this.addSkill.bind( this ) : this.removeSkill.bind( this )

        await aigle.resolve( heroSkills ).each( ( skill: Skill ): Promise<void> => {
            return operation( skill, false, true )
        } )

        this.hero = value

        if ( forceUpdate ) {
            this.sendSkillList()
        }
    }

    setInCrystallize( value: boolean ) {
        this.inCrystallize = value
    }

    setInVehiclePosition( position: Location ) {
        this.inVehiclePosition = position
    }

    setInventoryBlockingStatus( value: boolean ) {
        this.inventoryDisable = value

        if ( value ) {
            setTimeout( this.setInventoryBlockingStatus.bind( this ), 1500, false )
        }
    }

    setIsEnchanting( value: boolean ) {
        this.isEnchantingValue = value
    }

    setIsFakeDeath( value: boolean ) {
        this.isFakeDeathValue = value
    }

    setIsIn7sDungeon( value: boolean ) {
        this.isIn7sDungeonValue = value
    }

    setIsInHideoutSiege( value: boolean ) {
        this.isInHideoutSiege = value
    }

    setIsSitting( value: boolean ) {
        this.waitTypeSitting = value
    }

    setKarma( amount: number ): void {
        if ( amount < 0 ) {
            amount = 0
        }

        if ( ListenerCache.hasGeneralListener( EventType.PlayerKarmaChanged ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerKarmaChanged ) as PlayerKarmaChangedEvent

            eventData.playerId = this.getObjectId()
            eventData.oldKarma = this.getKarma()
            eventData.newKarma = amount

            ListenerCache.sendGeneralEvent( EventType.PlayerKarmaChanged, eventData )
        }

        this.karma = amount
        this.sendOwnedData( StatusUpdate.forValue( this.getObjectId(), StatusUpdateProperty.Karma, this.getKarma() ) )

        this.broadcastKarmaInfo()
    }

    runBroadcastKarmaInfo(): void {
        let currentSummon = this.getSummon()
        let players: Array<L2PcInstance> = L2World.getVisiblePlayers( this, this.getBroadcastRadius(), false )
        let party = this.getParty()
        let clan = this.getClan()

        players.forEach( ( otherPlayer: L2PcInstance ) => {
            let relation = this.getRelation( otherPlayer, party, clan )
            let isAttackable: boolean = this.isAutoAttackable( otherPlayer )

            otherPlayer.sendOwnedData( RelationChanged( this, relation, isAttackable ) )

            if ( currentSummon ) {
                otherPlayer.sendOwnedData( RelationChanged( currentSummon, relation, isAttackable ) )
            }
        } )
    }

    setLastAccess( value: number ) {
        this.lastAccess = value
    }

    setLevel( value: number ) {
        this.getSubStat().setLevel( Math.min( value, this.getMaxLevel() ) )
    }

    setLevelJoinedAcademy( value: number ) {
        this.levelJoinedAcademy = value
    }

    setLure( item: L2ItemInstance ) {
        this.lure = item
    }

    async setMount( npcId: number, level: number ) : Promise<void> {
        let type: MountType = MountTypeHelper.findByNpcId( npcId )
        switch ( type ) {
            case MountType.NONE:
                this.setIsFlying( false )
                break
            case MountType.STRIDER:
                if ( this.isNoble() ) {
                    await this.addSkill( CommonSkill.STRIDER_SIEGE_ASSAULT.getSkill() )
                }
                break
            case MountType.WYVERN:
                this.setIsFlying( true )
                break
        }

        this.mountType = type
        this.mountNpcId = npcId
        this.mountLevel = level
    }

    setMountObjectID( id: number ) {
        this.mountObjectId = id
    }

    setMultiSocialAction( id: number, targetId: number ): void {
        this.multiSociaAction = id
        this.multiSocialTarget = targetId
    }

    async setNoble( value: boolean ): Promise<void> {
        if ( this.noble === value ) {
            return
        }

        let nobleSkills: Array<Skill> = SkillCache.getNobleSkills()

        let operation: Function = value ? this.addSkill.bind( this ) : this.removeSkill.bind( this )

        await aigle.resolve( nobleSkills ).each( ( skill: Skill ): Promise<void> => {
            return operation( skill, false, true )
        } )

        this.noble = value
        this.sendSkillList()
    }

    setOnlineStatus( isOnline: boolean, updateDatabase: boolean = false ) : Promise<void> {
        if ( this.isPlayerOnline !== isOnline ) {
            this.isPlayerOnline = isOnline
        }

        if ( updateDatabase ) {
            return DatabaseManager.getCharacterTable().updateOnlineStatus( this )
        }
    }

    setOnlineTime( value: number ) {
        this.onlineTime = value
        this.onlineBeginTime = Date.now()
    }

    setOverrideCondition( mask: number ) {
        this.overrides = mask
    }

    setPartyDistributionType( type: PartyDistributionType ) {
        this.partyDistributionType = type
    }

    setPet( summon: L2Summon ) {
        this.summon = summon
    }

    setPetInventoryItems( value: boolean ) {
        this.hasPetItems = value
    }

    setPkKills( amount: number ): void {
        if ( ListenerCache.hasGeneralListener( EventType.PlayerPKAmountChanged ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerPKAmountChanged ) as PlayerPKChangedEvent

            eventData.playerId = this.getObjectId()
            eventData.oldValue = this.pkKills
            eventData.newValue = amount

            ListenerCache.sendGeneralEvent( EventType.PlayerPKAmountChanged, eventData )
        }

        this.pkKills = amount
    }

    setPledgeClass( value: number ) {
        this.pledgeClass = value
        this.checkItemRestriction()
    }

    setPledgeType( value: number ) {
        this.pledgeType = value
    }

    setPowerGrade( value: number ) {
        this.powerGrade = value
    }

    setPrivateStoreType( type: PrivateStoreType ): void {
        this.privateStoreType = type

        if ( !ConfigManager.customs.offlineDisconnectFinished() || type !== PrivateStoreType.None ) {
            return
        }

        let client = this.getClient()
        if ( !client || client.isDetached() ) {
            this.deleteMe()
        }
    }

    setProtection( value: boolean ) {
        this.protectEndTime = value ? ConfigManager.character.getPlayerSpawnProtection() * 1000 + Date.now() : 0
    }

    setPvpFlag( value: number ) {
        this.pvpFlag = value
    }

    /*
        Due to having various durations of pvp engagements (hitting non-flagged player vs flagged),
        it is possible to first receive duration longer than second time. Causing flag duration
        to be significantly reduced ( a cheat of sorts ), hence care must be taken to always
        ensure that previous flag duration is preserved if it is indeed longer.
     */
    runEngagePvPFlag( duration: number ) : void {
        let correctedDuration = Math.max( 20000, duration )
        let flagEndTime = Math.max( this.pvpFlagEndTime, Date.now() + correctedDuration )

        if ( this.pvpFlagEndTime === flagEndTime ) {
            return
        }

        this.updatePvPFlag( PlayerPvpFlag.Flagged )

        if ( !this.pvpFlagTimeout ) {
            this.pvpFlagTimeout = setTimeout( this.runUpdatePvpFlag.bind( this ), 1000 )
        }
    }

    setPvpKills( amount: number ): void {
        if ( ListenerCache.hasGeneralListener( EventType.PlayerKillsChanged ) ) {
            let eventData: PlayerKillsChangedEvent = {
                playerId: this.getObjectId(),
                oldValue: this.pvpKills,
                newValue: amount,
            }

            ListenerCache.sendGeneralEvent( EventType.PlayerKillsChanged, eventData )
        }

        this.pvpKills = amount
    }

    resetQueuedSkills() {
        // TODO : reset any queued player skills
    }

    setRecentFakeDeath( shouldProtect: boolean ) {
        let value = 0
        if ( shouldProtect ) {
            value = Date.now() + ConfigManager.character.getPlayerFakeDeathUpProtection() * 1000
        }

        this.recentFakeDeathEndTime = value
    }

    setRecommendationTwoHoursGiven( value: boolean ) {
        this.recomendationsTwoHoursGivenValue = value
    }

    setRecommendationsHave( value: number ) {
        this.recommendationsHave = Math.min( Math.max( value, 0 ), 255 )
    }

    setRecommendationsLeft( value: number ) {
        this.recommendationsLeft = Math.min( Math.max( value, 0 ), 255 )
    }

    setServitorShare( stats: { [ key: string ]: number } ) {
        this.servitorShare = stats
    }

    setSiegeSide( side: number ) {
        this.siegeSide = side
    }

    setSiegeRole( role: SiegeRole ) {
        this.siegeRole = role
    }

    setSilenceMode( mode: boolean ) {
        this.silenceMode = mode

        if ( !_.isEmpty( this.silenceModeExcluded ) ) {
            this.silenceModeExcluded = []
        }

        this.sendDebouncedPacket( EtcStatusUpdate )
    }

    setSp( spAmount: number ) : void {
        if ( spAmount < 0 ) {
            spAmount = 0
        }

        this.getSubStat().setSp( spAmount )

        if ( this.canBroadcastUpdate() ) {
            this.sendOwnedData( StatusUpdate.forValue( this.getObjectId(), StatusUpdateProperty.SP, this.getSp() ) )
        }
    }

    setSponsor( value: number ) {
        this.sponsorId = value
    }

    setSubClasses( value: any ) {
        this.subClasses = value
    }

    setTrap( trap: L2TrapInstance ) {
        this.trap = trap
    }

    setUptime( value: number ) {
        this.uptime = value
    }

    setVehicle( value: L2Vehicle ): void {
        if ( value && this.vehicle ) {
            this.vehicle.removePassenger( this )
        }

        this.vehicle = value
    }

    setVitalityPoints( points: number, sendUpdate: boolean = false ) {
        this.getStat().setVitalityPoints( points, sendUpdate )
    }

    setWantsPeace( value: number ) {
        this.wantsPeace = value
    }

    async sitDown( checkIfCasting: boolean = true ): Promise<void> {
        if ( ListenerCache.hasGeneralListener( EventType.PlayerSit ) || ListenerCache.hasObjectListener( this.getObjectId(), EventType.PlayerSit ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerSit ) as PlayerSitEvent

            eventData.playerId = this.getObjectId()

            let result: EventTerminationResult = await ListenerCache.getTerminatedResult( this, EventType.PlayerSit, eventData )
            if ( result && result.terminate ) {
                return
            }
        }

        if ( checkIfCasting && this.isCastingNow() ) {
            return
        }

        if ( !this.isSitting()
                && !this.isAttackingDisabled()
                && !this.isOutOfControl()
                && !this.isImmobilized()
                && !this.isMoving() ) {
            this.notifyAttackInterrupted( false )
            this.setIsSitting( true )

            AIEffectHelper.scheduleNextIntent( this, AIIntent.REST )
            BroadcastHelper.dataToSelfBasedOnVisibility( this, ChangeWaitType( this, ChangeWaitTypeValue.Sitting ) )

            this.startStunning()
            setTimeout( this.stopStunning.bind( this ), 2500, false )
        }
    }

    async standUp(): Promise<void> {
        if ( ListenerCache.hasGeneralListener( EventType.PlayerStandup ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerStandup ) as PlayerStandEvent

            eventData.playerId = this.getObjectId()

            let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayerStandup, eventData )
            if ( result && result.terminate ) {
                return
            }
        }

        if ( this.isSitting() && !this.isInStoreMode() && !this.isAlikeDead() ) {
            if ( this.getEffectList().isAffected( EffectFlag.RELAXING ) ) {
                await this.stopEffects( L2EffectType.Relaxing )
            }

            BroadcastHelper.dataToSelfBasedOnVisibility( this, ChangeWaitType( this, ChangeWaitTypeValue.Standing ) )
            this.stanUpTimeout = setTimeout( this.runStandUp.bind( this ), 2500 )
        }
    }

    startFeedMount( npcId: number ) {
        this.canFeed = npcId > 0
        if ( !this.isMounted() ) {
            return
        }

        let summon = this.getSummon() as L2PetInstance
        if ( summon ) {
            let feedValue = summon.getCurrentFeed()
            this.setCurrentFeed( feedValue )
            this.controlItemId = this.getSummon().getControlObjectId()

            this.sendOwnedData( SetupGauge( this.getObjectId(), SetupGaugeColors.Other, ( this.getCurrentFeed() * 10000 ) / this.getFeedConsume(), ( this.getMaxFeed() * 10000 ) / this.getFeedConsume() ) )
            if ( !this.isDead() ) {
                this.mountFeedTask = setInterval( DeferredMethods.playerFeedMount, 10000, this )
            }

            return
        }

        if ( this.canFeed ) {
            this.setCurrentFeed( this.getMaxFeed() )

            this.sendOwnedData( SetupGauge( this.getObjectId(), SetupGaugeColors.Other, ( this.getCurrentFeed() * 10000 ) / this.getFeedConsume(), ( this.getMaxFeed() * 10000 ) / this.getFeedConsume() ) )
            if ( !this.isDead() ) {
                this.mountFeedTask = setInterval( DeferredMethods.playerFeedMount, 10000, this )
            }
        }
    }

    startFishCombat() : void {
        this.fishCombat = new L2Fishing( this, this.fish )
    }

    async startFishing( x: number, y: number, z: number ): Promise<void> {
        this.abortMoving()
        this.setIsImmobilized( true )

        this.fishing = true
        this.fishX = x
        this.fishY = y
        this.fishZ = z

        let level = this.getRandomFishLevel()
        let grade = this.getRandomFishGrade()
        let group = this.getRandomFishGroup( grade )

        let allFish: FishDataItem = DataManager.getFishData().getFish( level, group, grade )
        if ( !allFish ) {
            this.sendMessage( 'No fish found for this pond' )
            this.endFishing( false )
            return
        }

        this.fish = allFish

        if ( this.lure.isNightLure() && !GameTime.isNight() ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FISHING_ATTEMPT_CANCELLED ) )
            return
        }

        if ( this.isDead() || this.lookForFishTask ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FISHING_ATTEMPT_CANCELLED ) )
            return
        }

        this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CAST_LINE_AND_START_FISHING ) )

        BroadcastHelper.dataToSelfBasedOnVisibility( this, ExFishingStart( this.objectId, this.fish.groupId, this.fishX, this.fishY, this.fishZ, this.lure.isNightLure() ) )
        this.sendCopyData( MusicPacket.SF_P_01 )

        this.createFishTask( this.lure ? ( this.fish.gutsCheckTime * getLureDelayMultiplier( this.lure.getId() ) ) : 0 )
    }

    startTrade( partner: L2PcInstance ): void {
        this.onTradeStart( partner )
        partner.onTradeStart( this )
    }

    startWarnUserToTakeBreak() {
        if ( !this.warnUserTakeBreakTask ) {
            this.warnUserTakeBreakTask = setInterval( this.runWarnUserTakeBreakTask.bind( this ), 7200000 )
        }
    }

    runWarnUserTakeBreakTask() {
        this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PLAYING_FOR_LONG_TIME ) )
    }

    async stopAllTimers() : Promise<void> {
        this.stopHpMpRegeneration()
        this.stopWarnUserTakeBreak()
        this.stopWaterTask()
        this.stopFeed()
        await this.stopRentPet()
        this.stopPvPTimer()
        this.stopSoulTask()
        this.stopChargeTask()
        this.stopFameTask()
        this.stopVitalityTask()
        this.stopRecoBonusTask()
        this.stopRecoGiveTask()
        this.stopLookingForFishTask()
        this.stopAttackTimers()
        this.clearDismountTask()

        clearTimeout( this.readyForAttackTimeout )
        clearInterval( this.proximityScan )
        clearTimeout( this.wearItemTimeout )
        clearTimeout( this.stanUpTimeout )
        clearTimeout( this.deathTeleportTimeout )
    }

    stopChargeTask() {
        if ( this.chargeTask ) {
            clearTimeout( this.chargeTask )
            this.chargeTask = null
        }
    }

    stopFameTask() {
        if ( this.fameTask ) {
            clearInterval( this.fameTask )
            this.fameTask = null
        }
    }

    stopFeed() {
        if ( this.mountFeedTask ) {
            clearInterval( this.mountFeedTask )
            this.mountFeedTask = null
        }
    }

    stopLookingForFishTask() {
        if ( this.lookForFishTask ) {
            clearInterval( this.lookForFishTask )
        }

        this.lookForFishTask = null
    }

    stopPvPFlag() {
        this.stopPvPTimer()
        this.updatePvPFlag( PlayerPvpFlag.None )
    }

    stopPvPTimer() {
        if ( this.pvpFlagTimeout ) {
            clearTimeout( this.pvpFlagTimeout )
            this.pvpFlagTimeout = null
        }
    }

    stopRecoBonusTask() {
        if ( this.recommendationsBonusTask ) {
            clearTimeout( this.recommendationsBonusTask )
            this.recommendationsBonusTask = null
        }
    }

    stopRecoGiveTask() {
        if ( this.recommendationsGiveTask ) {
            clearInterval( this.recommendationsGiveTask )
            this.recommendationsGiveTask = null
        }
    }

    async stopRentPet(): Promise<void> {
        if ( this.taskRentPet ) {
            await this.dismount()

            if ( this.checkLandingState() && this.getMountType() === MountType.WYVERN ) {
                await this.teleportToLocationType( TeleportWhereType.TOWN )
            }

            clearInterval( this.taskRentPet )
            this.taskRentPet = null
        }
    }

    stopSoulTask(): void {
        if ( this.soulTask ) {
            clearTimeout( this.soulTask )
            this.soulTask = null
        }
    }

    stopVitalityTask() {
        if ( this.vitalityTask ) {
            clearInterval( this.vitalityTask )
            this.vitalityTask = null
        }
    }

    stopWarnUserTakeBreak() {
        if ( this.warnUserTakeBreakTask ) {
            clearInterval( this.warnUserTakeBreakTask )
            this.warnUserTakeBreakTask = null
        }
    }

    stopWaterTask() {
        if ( this.waterDamageTask ) {
            clearTimeout( this.waterDamageTask )
            this.waterDamageTask = null

            this.sendOwnedData( SetupGauge( this.getObjectId(), SetupGaugeColors.Cyan, 0, 0 ) )
        }
    }

    async storeInDatabase( storeActiveEffects: boolean ) : Promise<void> {
        /*
            Following are excluded due to separate automatic saving systems in place:
            - items in inventory/warehouse/freight
            - account and player variables

            Hence, there is no need to manage all these via direct save.
        */
        // TODO : break apart into data caches and save on deman, to get rid of auto storing
        await Promise.all( [
            DatabaseManager.getCharacterTable().updatePlayerCharacter( this ),
            DatabaseManager.getCharacterSubclasses().updatePlayer( this ),
            DatabaseManager.getCharacterItemReuseSave().createTimes( this ),

            this.storeEffect( storeActiveEffects ),
            this.storeRecommendations(),
        ] )

        if ( this.hasManufactureShop() && ConfigManager.character.storeRecipeShopList() ) {
            await DatabaseManager.getCharacterRecipeShoplist().deletePlayer( this )
            await DatabaseManager.getCharacterRecipeShoplist().createPlayer( this )
        }
    }

    storeRecommendations(): Promise<void> {
        let endTime = this.getRecommendationsBonusTime()
        return DatabaseManager.getRecommendationBonus().storePlayer( this, endTime )
    }

    storeSkill( skill: Skill, classIndex: number ): void {
        if ( !skill ) {
            return
        }

        let classSkills = this.skillsToStore.get( classIndex )
        if ( !classSkills ) {
            this.skillsToStore.set( classIndex, [ skill ] )
        } else {
            classSkills.push( skill )
        }

        this.debounceSkillSave()
    }

    async tryOpenPrivateBuyStore(): Promise<void> {
        if ( this.canOpenPrivateStore() ) {
            if ( BuyStoreTypes.has( this.getPrivateStoreType() ) ) {
                this.setPrivateStoreType( PrivateStoreType.None )
            }

            if ( this.getPrivateStoreType() === PrivateStoreType.None ) {
                if ( this.isSitting() ) {
                    await this.standUp()
                }

                this.setPrivateStoreType( PrivateStoreType.PrepareBuy )
                this.sendOwnedData( PrivateStoreManageListBuy ( this ) )
            }

            return
        }

        if ( this.hasAreaFlags( AreaFlags.NoOpenStore ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_PRIVATE_STORE_HERE ) )
        }

        this.sendOwnedData( ActionFailed() )
    }

    async tryOpenPrivateSellStore( isPackageSale: boolean ): Promise<void> {
        if ( this.canOpenPrivateStore() ) {
            if ( SellStoreTypes.has( this.getPrivateStoreType() ) ) {
                this.setPrivateStoreType( PrivateStoreType.None )
            }

            if ( this.getPrivateStoreType() === PrivateStoreType.None ) {
                if ( this.isSitting() ) {
                    await this.standUp()
                }

                this.setPrivateStoreType( PrivateStoreType.PrepareSell )
                this.sendOwnedData( PrivateStoreManageListSell( this, isPackageSale ) )
            }

            return
        }

        if ( this.hasAreaFlags( AreaFlags.NoOpenStore ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_PRIVATE_STORE_HERE ) )
        }

        this.sendOwnedData( ActionFailed() )
    }

    async transform( transformation: Transform ): Promise<void> {
        if ( this.transformation ) {
            return this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ALREADY_POLYMORPHED_AND_CANNOT_POLYMORPH_AGAIN ) )
        }

        this.resetQueuedSkills()
        if ( this.isMounted() ) {
            await this.dismount()
        }

        this.transformation = transformation
        await this.getEffectList().stopTogglEffects()
        transformation.onTransform( this )
        this.sendSkillList()

        this.sendDebouncedPacket( SkillCoolTime )
        this.broadcastUserInfo()

        if ( ListenerCache.hasGeneralListener( EventType.PlayerTransform ) ) {
            let eventData: PlayerTransformEvent = {
                playerId: this.getObjectId(),
                transformId: transformation.getId(),
            }

            return ListenerCache.sendGeneralEvent( EventType.PlayerTransform, eventData )
        }
    }

    async untransform(): Promise<void> {
        if ( !this.transformation ) {
            return
        }

        this.resetQueuedSkills()
        await this.transformation.onUntransform( this )
        this.transformation = null

        await this.getEffectList().stopTogglEffects( false )
        await this.getEffectList().stopSkillEffects( false, AbnormalType.TRANSFORM )
        this.sendSkillList()

        this.sendDebouncedPacket( SkillCoolTime )
        this.broadcastUserInfo()

        if ( ListenerCache.hasGeneralListener( EventType.PlayerTransform ) ) {
            let eventData: PlayerTransformEvent = {
                playerId: this.getObjectId(),
                transformId: 0,
            }

            return ListenerCache.sendGeneralEvent( EventType.PlayerTransform, eventData )
        }
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( !await super.doDie( killer ) ) {
            return false
        }

        if ( this.rangedAttackEndTime < Date.now() ) {
            let item = this.getActiveWeaponItem()
            if ( item && ( item.isBow() || item.isCrossBow() ) ) {
                this.rangedAttackEndTime = 0

                this.sendOwnedData( SetupGauge( this.getObjectId(), SetupGaugeColors.Red, 0 ) )
            }
        }

        if ( killer ) {
            let playerKiller: L2PcInstance = killer.getActingPlayer()
            if ( playerKiller ) {
                if ( ListenerCache.hasGeneralListener( EventType.PlayerPvpKill ) ) {
                    let eventData: PlayerPvpKillEvent = {
                        attackerId: killer.getObjectId(),
                        targetId: this.getObjectId(),
                    }

                    await ListenerCache.sendGeneralEvent( EventType.PlayerPvpKill, eventData )
                }

                TvTEvent.onKill( killer, this )
                L2Event.addKill( playerKiller.getObjectId(), this.getObjectId() )
                await TerritoryWarManager.onDeath( playerKiller, this )
            }

            this.broadcastStatusUpdate()
            this.setExpBeforeDeath( 0 )

            if ( this.isCursedWeaponEquipped() ) {
                await CursedWeaponManager.dropWeapon( this.cursedWeaponEquippedId, killer )
            } else if ( this.isCombatFlagEquipped() ) {
                if ( TerritoryWarManager.isTWInProgress ) {
                    TerritoryWarManager.dropCombatFlag( this, true, false )
                } else {
                    let fort: Fort = FortManager.getFort( this )
                    if ( fort ) {
                        FortSiegeManager.dropCombatFlag( this, fort.getResidenceId() )
                    } else {
                        let combatFlagItem: L2ItemInstance = this.getInventory().getItemByItemId( 9819 )
                        let slot = getSlotFromItem( combatFlagItem )
                        await this.getInventory().unEquipItemInBodySlot( slot )
                        await this.destroyItem( combatFlagItem, true, 'L2PcInstance.doDie' )
                    }
                }
            } else {
                if ( !playerKiller || !playerKiller.isCursedWeaponEquipped() ) {
                    await this.onDieDropItem( killer )

                    let insidePvpZone: boolean = this.isInArea( AreaType.PVP )
                    let insideSiegeZone: boolean = this.isInSiegeArea()

                    if ( !insidePvpZone && !insideSiegeZone && playerKiller ) {
                        let playerClan = this.getClan()
                        let oppositeClan = playerKiller.getClan()
                        if ( oppositeClan
                                && playerClan
                                && !this.isAcademyMember()
                                && !playerKiller.isAcademyMember() ) {
                            let canAddScore : boolean = ( playerClan.isAtWarWith( playerKiller.getClanId() ) && oppositeClan.isAtWarWith( playerClan.getId() ) )
                                    || ( this.isInSiege() && playerKiller.isInSiege() )
                            if ( canAddScore && isValidPlayerKill( this ) ) {
                                if ( playerClan.getReputationScore() > 0 ) {
                                    await oppositeClan.addReputationScore( ConfigManager.clan.getReputationScorePerKill(), false )
                                }

                                if ( oppositeClan.getReputationScore() > 0 ) {
                                    await playerClan.takeReputationScore( ConfigManager.clan.getReputationScorePerKill(), false )
                                }
                            }
                        }
                    }

                    if ( ( ConfigManager.character.delevel() || !this.isLucky() )
                            && ( insideSiegeZone || !insidePvpZone ) ) {
                        await this.calculateDeathExpPenalty( killer, this.isAtWarWith( playerKiller ) )
                    }
                }
            }
        }

        if ( this.isMounted() ) {
            this.stopFeed()
        }

        if ( this.isFakeDeath() ) {
            await this.stopFakeDeath( true )
        }


        this.stopCubics()

        if ( this.isChannelized() ) {
            this.getSkillChannelized().abortChannelization()
        }

        if ( this.isInParty() && this.getParty().isInDimensionalRift() ) {
            this.getParty().getDimensionalRift().deadPlayers.push( this.getObjectId() )
        }

        if ( this.getAgathionId() !== 0 ) {
            this.setAgathionId( 0 )
        }

        this.calculateDeathPenaltyBuffLevel( killer )

        await this.stopRentPet()
        this.stopWaterTask()

        return true
    }

    updateAndBroadcastStatus( type: number ) {
        this.refreshOverloadPenalty()
        this.refreshExpertisePenalty()

        switch ( type ) {
            case 1:
                this.sendDebouncedPacket( UserInfo )
                this.sendDebouncedPacket( ExBrExtraUserInfo )
                break
            case 2:
                this.broadcastUserInfo()
                break
        }
    }

    updatePvPFlag( value: PlayerPvpFlag ) {
        if ( this.getPvpFlag() === value ) {
            return
        }

        this.setPvpFlag( value )

        this.sendDebouncedPacket( UserInfo )
        this.sendDebouncedPacket( ExBrExtraUserInfo )

        let party = this.getParty()
        let clan = this.getClan()

        let summon = this.getSummon()
        if ( summon ) {
            this.sendOwnedData( RelationChanged( summon, PlayerRelationStatus.getStatus( this.getObjectId() ), false ) )
        }

        /*
            TODO : consider other players may not need updates on your character
            - information sent to other players about you
            - you have already sent information before, so why send same data again?
            - keep track of which players received your update and if it changed
            - compute relation values, just ensure that no additional packets are sent
         */
        L2World.getVisiblePlayers( this, this.getRelationUpdateRadius() ).forEach( ( otherPlayer: L2PcInstance ) => {
            let relation = this.getRelation( otherPlayer, party, clan )
            let isAttackable = this.isAutoAttackable( otherPlayer )

            otherPlayer.sendOwnedData( RelationChanged( this, relation, isAttackable ) )

            if ( summon ) {
                otherPlayer.sendOwnedData( RelationChanged( summon, relation, isAttackable ) )
            }
        } )
    }

    updatePvPStatus() : void {
        if ( this.isInArea( AreaType.PVP ) ) {
            return
        }

        return this.debounceEngagePvPFlag( ConfigManager.pvp.getPvPVsNormalTime() )
    }

    updatePvPStatusWithTarget( target: L2Character ) : void {
        let player: L2PcInstance = target.getActingPlayer()

        if ( !player ) {
            return
        }

        if ( this.isInDuel() && ( player.getDuelId() === this.getDuelId() ) ) {
            return
        }

        if ( ( !this.isInArea( AreaType.PVP ) || !player.isInArea( AreaType.PVP ) ) && player.getKarma() === 0 ) {
            if ( this.checkIfPvP( player ) ) {
                return this.debounceEngagePvPFlag( ConfigManager.pvp.getPvPVsPvPTime() )
            }

            return this.debounceEngagePvPFlag( ConfigManager.pvp.getPvPVsNormalTime() )
        }
    }

    updateRelations( player: L2PcInstance ): void {
        player.sendOwnedData( PlayerInformation( this, player ) )
        player.sendDebouncedPacket( ExBrExtraUserInfo )

        let toPlayer = this.getRelation( player, this.getParty(), this.getClan() )
        let fromPlayer = player.getRelation( this, player.getParty(), player.getClan() )

        let isAttackableForPlayer = this.isAutoAttackable( player )
        player.sendOwnedData( RelationChanged( this, toPlayer, isAttackableForPlayer ) )
        if ( this.hasSummon() ) {
            player.sendOwnedData( RelationChanged( this.getSummon(), toPlayer, isAttackableForPlayer ) )
        }

        let isAttackableForMe = player.isAutoAttackable( this )
        this.sendOwnedData( RelationChanged( player, fromPlayer, isAttackableForMe ) )
        if ( player.hasSummon() ) {
            this.sendOwnedData( RelationChanged( player.getSummon(), fromPlayer, isAttackableForMe ) )
        }
    }

    updateVitalityPoints( points: number, useRates: boolean, sendUpdate: boolean ): void {
        this.getStat().updateVitalityPoints( points, useRates, sendUpdate )

        if ( this.canBroadcastUpdate() ) {
            this.sendOwnedData( ExVitalityPointInfo( this.getVitalityPoints() ) )
        }
    }

    async useEquippableItem( objectId: number, abortAttack: boolean ): Promise<void> {
        let item: L2ItemInstance = this.getInventory().getItemByObjectId( objectId )
        if ( !item ) {
            return
        }

        let oldLimit = this.getInventoryLimit()

        if ( item.isEquipped() ) {
            if ( item.getEnchantLevel() > 0 ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.EQUIPMENT_S1_S2_REMOVED )
                        .addNumber( item.getEnchantLevel() )
                        .addItemInstanceName( item )
                        .getBuffer()
                this.sendOwnedData( packet )
            } else {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISARMED )
                        .addItemInstanceName( item )
                        .getBuffer()
                this.sendOwnedData( packet )
            }

            let slot = getSlotFromItem( item )
            if ( slot === L2ItemSlots.Decoration ) {
                await this.getInventory().unEquipItemInSlot( item.getLocationSlot() )
            } else {
                await this.getInventory().unEquipItemInBodySlot( slot )
            }
        } else {
            await this.getInventory().equipItem( item )

            if ( item.isEquipped() ) {
                if ( item.getEnchantLevel() > 0 ) {
                    let packet = new SystemMessageBuilder( SystemMessageIds.S1_S2_EQUIPPED )
                            .addNumber( item.getEnchantLevel() )
                            .addItemInstanceName( item )
                            .getBuffer()
                    this.sendOwnedData( packet )
                } else {
                    let packet = new SystemMessageBuilder( SystemMessageIds.S1_EQUIPPED )
                            .addItemInstanceName( item )
                            .getBuffer()
                    this.sendOwnedData( packet )
                }

                if ( item.isShadowItem() ) {
                    await item.decreaseMana()
                }

                if ( ( item.getItem().getBodyPart() & SLOT_MULTI_ALLWEAPONS ) !== 0 ) {
                    await this.rechargeShots( true, true )
                }
            } else {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_EQUIP_ITEM_DUE_TO_BAD_CONDITION ) )
            }
        }

        this.refreshExpertisePenalty()
        this.broadcastUserInfo()

        if ( abortAttack ) {
            this.abortAttack()
        }

        if ( this.getInventoryLimit() !== oldLimit ) {
            this.sendOwnedData( ExStorageMaxCount( this ) )
        }

        if ( item.agathion ) {
            this.sendOwnedData( ExBRAgathionEnergyItem( item ) )
        }

        if ( ListenerCache.hasGeneralListener( EventType.PlayerEquipItem ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerEquipItem ) as PlayerEquipItemEvent

            eventData.playerId = this.getObjectId()
            eventData.objectId = objectId
            eventData.itemId = item.getId()

            return ListenerCache.sendGeneralEvent( EventType.PlayerEquipItem, eventData )
        }
    }

    validateItemManipulation( objectId: number ): boolean {
        let item: L2ItemInstance = this.getInventory().getItemByObjectId( objectId )

        if ( !item || ( item.getOwnerId() !== this.getObjectId() ) ) {
            recordItemViolation(
                    this.getObjectId(),
                    '78d84818-027d-4e53-ada9-3189a1ae5714',
                    'Player manipulates non-owned item',
                    item ? item.getId() : 0,
                    item ? item.getCount() : 0 )

            return false
        }

        if ( ( this.hasSummon() && ( this.getSummon().getControlObjectId() === objectId ) ) || ( this.getMountObjectId() === objectId ) ) {
            return false
        }

        if ( this.getActiveEnchantItemId() === objectId ) {
            return false
        }

        return !CursedWeaponManager.isCursedItem( item.getId() )
    }

    canOpenPrivateStore() {
        return !this.isAlikeDead()
                && !this.isInOlympiadMode()
                && !this.isMounted()
                && !this.hasAreaFlags( AreaFlags.NoOpenStore )
                && !this.isCastingNow()
    }

    removeManufactureItems( isDwarven: boolean ) {
        let player = this
        _.remove( this.manufactureItems, ( item: L2ManufactureItem ) : boolean => {
            return item.isDwarven !== isDwarven || !player.hasRecipeList( item.recipeId )
        } )
    }

    hasRecipeList( recipeId: number ): boolean {
        let books = PlayerRecipeCache.getBooks( this.getObjectId() )
        return books.common.has( recipeId ) || books.dwarven.has( recipeId )
    }

    async mountSummon( pet: L2Summon ): Promise<boolean> {
        if ( pet && pet.isMountable() && !this.isMounted() && !this.isBetrayed() ) {
            if ( this.isDead() ) {
                this.sendOwnedData( ActionFailed() )
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.STRIDER_CANT_BE_RIDDEN_WHILE_DEAD ) )
                return false
            }

            if ( pet.isDead() ) {
                this.sendOwnedData( ActionFailed() )
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DEAD_STRIDER_CANT_BE_RIDDEN ) )
                return false
            }

            if ( pet.isInCombat() || pet.isRooted() ) {
                this.sendOwnedData( ActionFailed() )
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.STRIDER_IN_BATLLE_CANT_BE_RIDDEN ) )
                return
            }

            if ( this.isInCombat() ) {
                this.sendOwnedData( ActionFailed() )
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.STRIDER_CANT_BE_RIDDEN_WHILE_IN_BATTLE ) )
                return false
            }

            if ( this.isSitting() ) {
                this.sendOwnedData( ActionFailed() )
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.STRIDER_CAN_BE_RIDDEN_ONLY_WHILE_STANDING ) )
                return false
            }

            if ( this.isFishing() ) {
                this.sendOwnedData( ActionFailed() )
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DO_WHILE_FISHING_2 ) )
                return false
            }

            if ( this.isTransformed() || this.isCursedWeaponEquipped() ) {
                this.sendOwnedData( ActionFailed() )
                return false
            }

            if ( this.getInventory().getItemByItemId( 9819 ) ) {
                this.sendOwnedData( ActionFailed() )
                this.sendMessage( 'You cannot mount properly whilst holding a flag.' )
                return false
            }

            if ( pet.isHungry() ) {
                this.sendOwnedData( ActionFailed() )
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.HUNGRY_STRIDER_NOT_MOUNT ) )
                return false
            }

            if ( !GeneralHelper.checkIfInRange( 200, this, pet, true ) ) {
                this.sendOwnedData( ActionFailed() )
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TOO_FAR_AWAY_FROM_FENRIR_TO_MOUNT ) )
                return false
            }

            if ( !pet.isDead() && !this.isMounted() ) {
                await this.mount( pet )
            }

            return true
        }

        if ( this.isRentedPet() ) {
            await this.stopRentPet()
            return true
        }

        if ( this.isMounted() ) {
            if ( ( this.getMountType() === MountType.WYVERN ) && !this.isInArea( AreaType.AirLanding ) ) {
                this.sendOwnedData( ActionFailed() )
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_DISMOUNT_HERE ) )
                return false

            }

            if ( this.isHungry() ) {
                this.sendOwnedData( ActionFailed() )
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.HUNGRY_STRIDER_NOT_MOUNT ) )
                return false
            }

            await this.dismount()
            return false
        }

        return true
    }

    async mount( pet: L2Summon ): Promise<boolean> {
        if ( this.isTransformed() || !( await this.disarmWeapons() ) || !( await this.disarmShield() ) ) {
            return false
        }

        await this.getEffectList().stopTogglEffects()
        await this.setMount( pet.getId(), pet.getLevel() )
        this.setMountObjectID( pet.getControlObjectId() )
        this.startFeedMount( pet.getId() )

        BroadcastHelper.dataToSelfBasedOnVisibility( this, Ride( this ) )
        this.broadcastUserInfo()

        await pet.unSummon( this )
        return true
    }

    async disarmShield() : Promise<boolean> {
        let shieldItem: L2ItemInstance = this.getInventory().getPaperdollItem( InventorySlot.LeftHand )
        if ( shieldItem ) {
            let unequipedItems: Array<L2ItemInstance> = await this.getInventory().unEquipItemInBodySlotAndRecord( shieldItem.getItem().getBodyPart() )

            this.abortAttack()
            this.broadcastUserInfo()

            if ( unequipedItems.length > 0 ) {
                if ( unequipedItems[ 0 ].getEnchantLevel() > 0 ) {
                    let itemUpdate = new SystemMessageBuilder( SystemMessageIds.EQUIPMENT_S1_S2_REMOVED )
                            .addNumber( unequipedItems[ 0 ].getEnchantLevel() )
                            .addItemInstanceName( unequipedItems[ 0 ] )
                            .getBuffer()
                    this.sendOwnedData( itemUpdate )

                } else {
                    let itemUpdate = new SystemMessageBuilder( SystemMessageIds.S1_DISARMED )
                            .addItemInstanceName( unequipedItems[ 0 ] )
                            .getBuffer()
                    this.sendOwnedData( itemUpdate )
                }
            }
        }

        return true
    }

    isRentedPet(): boolean {
        return !!this.taskRentPet
    }

    startRentPet( seconds: number ): void {
        if ( !this.taskRentPet ) {
            this.taskRentPet = setInterval( this.stopRentPet.bind( this ), seconds * 1000, this.getObjectId() )
        }
    }

    isBlockedFromExit(): boolean {
        return _.some( PlayerEventStatusCache.getPlayerStatus( this.getObjectId() ), ( status: PlayerEventStatus ) => status.isBlockingExit )
    }

    isBlockedFromDeathPenalty(): boolean {
        return _.some( PlayerEventStatusCache.getPlayerStatus( this.getObjectId() ), ( status: PlayerEventStatus ) => status.isBlockingDeathPenalty )
    }

    isLocked() {
        return this.subclassLock
    }

    /*
        TODO : add check for moving worker on interval to remove dependency on ValidatePosition packet
     */
    isFalling( value: number ) : boolean {
        if ( this.isDead()
            || this.isFlying()
            || this.isFlyingMounted()
            || this.isInArea( AreaType.Water )
            || this.isTeleporting() ) {
            return false
        }

        if ( Date.now() < this.fallingTimestamp ) {
            return true
        }

        let deltaZ = this.getZ() - value
        if ( deltaZ <= this.getBaseTemplate().getSafeFallHeight() ) {
            return false
        }

        /*
            Falling can be following cases:
            - character somehow fell under geo polygon (should not happen, technically only through ValidatePosition assignment)
            - Z value is above geo polygon (free fall, above geo polygon since Z value of polygon will be smaller)

            TODO : Movement thread should detect falling and must:
            - prevent any movement until hitting Z (unless water zone encountered)
            - set falling flag
            - send event about reducing hp (which can re-calculate final Z value in water zone)
         */
        if ( GeoPolygonCache.getObjectZ( this ) !== this.getZ() ) {
            return false
        }

        let damage = Math.floor( Formulas.calculateFallDamage( this, deltaZ ) )
        if ( damage > 0 ) {
            this.reduceCurrentHp( Math.min( damage, this.getCurrentHp() - 1 ), null, null, false, true )

            let packet = new SystemMessageBuilder( SystemMessageIds.FALL_DAMAGE_S1 )
                    .addNumber( damage )
                    .getBuffer()

            this.sendOwnedData( packet )
        }

        this.setFalling()

        return false
    }

    setFalling() {
        this.fallingTimestamp = Date.now() + L2PcInstanceValues.FALLING_VALIDATION_DELAY
    }

    // TODO : add checks to notify cubics when cubics are using AIController
    async reduceCurrentHp( damage: number, attacker: L2Character, skill: Skill, isAwake: boolean = true, isDOT: boolean = false ): Promise<void> {
        if ( skill ) {
            await this.getStatus().reduceHpWithCp( damage, attacker, isAwake, isDOT, skill.isToggle(), skill.getDamageDirectlyToHP() )
        } else {
            await this.getStatus().reduceHpWithCp( damage, attacker, isAwake, isDOT, false, false )
        }

        let pet = this.getSummon()
        if ( pet && attacker && attacker.getObjectId() !== pet.getOwnerId() ) {
            AIEffectHelper.notifyOwnerHpReduced( pet, damage, attacker.getObjectId() )
        }

        this.tamedBeasts.forEach( ( objectId: number ) => {
            let tamedBeast = L2World.getObjectById( objectId ) as L2TamedBeastInstance
            tamedBeast && AIEffectHelper.notifyOwnerHpReduced( tamedBeast, damage, attacker?.getObjectId() ?? 0 )
        } )
    }

    // TODO : add checks to notify cubics when cubics are using AIController
    reduceCurrentMp( value: number ) : void {
        super.reduceCurrentMp( value )

        let pet = this.getSummon()
        if ( pet ) {
            AIEffectHelper.notifyOwnerMpReduced( pet, value )
        }

        this.tamedBeasts.forEach( ( objectId: number ) => {
            let tamedBeast = L2World.getObjectById( objectId ) as L2TamedBeastInstance
            tamedBeast && AIEffectHelper.notifyOwnerMpReduced( tamedBeast, value )
        } )
    }

    setDuelState( value: DuelState ) {
        this.duelState = value
    }

    hasTamedBeasts() {
        return this.tamedBeasts.length > 0
    }

    getHennaEmptySlots(): number {
        let totalSlots = ClassId.getClassIdByIdentifier( this.getClassId() ).level === 1 ? 2 : 3
        let hennas = PlayerHennaCache.getHennas( this.getObjectId() )

        for ( const henna of hennas ) {
            if ( henna ) {
                totalSlots--
            }
        }

        return Math.max( totalSlots, 0 )
    }

    async removeHenna( slot: number ): Promise<void> {
        let henna = PlayerHennaCache.removeHenna( this.getObjectId(), this.getClassIndex(), slot )
        if ( !henna ) {
            return
        }

        this.recalculateHennaStats()

        this.sendOwnedData( HennaInfo( this ) )
        this.sendDebouncedPacket( UserInfo )
        this.sendDebouncedPacket( ExBrExtraUserInfo )

        await this.getInventory().addItem( henna.getDyeItemId(), henna.getCancelCount(), null, 'L2PcInstance.removeHenna' )
        await this.reduceAdena( henna.getCancelFee(), false, 'L2PcInstance.removeHenna' )

        let packet = new SystemMessageBuilder( SystemMessageIds.EARNED_S2_S1_S )
                .addItemNameWithId( henna.getDyeItemId() )
                .addNumber( henna.getCancelCount() )
                .getBuffer()
        this.sendOwnedData( packet )
        this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SYMBOL_DELETED ) )

        if ( ListenerCache.hasGeneralListener( EventType.PlayerRemoveHenna ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerRemoveHenna )

            eventData.playerId = this.getObjectId()
            eventData.dyeItemId = henna.getDyeItemId()
            eventData.cancelCount = henna.getCancelCount()
            eventData.cancelFee = henna.getCancelFee()
            eventData.dyeId = henna.getDyeId()

            return ListenerCache.sendGeneralEvent( EventType.PlayerRemoveHenna, eventData )
        }
    }

    getLearningClass(): number {
        return this.learningClass
    }

    setLearningClass( classId: number ) {
        this.learningClass = classId
    }

    async transferItem( objectId: number, amount: number, target: Inventory ): Promise<L2ItemInstance> {
        let oldItem: L2ItemInstance = this.checkItemManipulation( objectId, amount )
        if ( !oldItem ) {
            return null
        }

        return this.getInventory().transferItem( objectId, amount, target )
    }

    setMultiSell( value: PreparedListContainer ) {
        this.currentMultiSell = value
    }

    getMultiSell(): PreparedListContainer {
        return this.currentMultiSell
    }

    async unregisterRecipeList( recipeId: number ): Promise<void> {
        PlayerRecipeCache.removeRecipe( this.getObjectId(), recipeId, this.getClassIndex() )
        PlayerShortcutCache.deleteById( this, recipeId, ShortcutType.RECIPE )
    }

    setIsInCraftMode( value: boolean ) {
        this.inCraftMode = value
    }

    setStoreName( name: string ) {
        this.storeName = _.defaultTo( name, '' )
    }

    async leaveObserverMode(): Promise<void> {
        this.setTarget( null )

        await this.teleportToLocation( this.lastLocation, false )
        this.unsetLastLocation()
        this.sendOwnedData( ObservationReturn( this ) )

        if ( !this.isGM() ) {
            this.setInvisible( false )
            this.setIsInvulnerable( false )
        }

        this.setFalling() // prevent receive falling damage
        this.observerMode = false

        this.broadcastUserInfo()
    }

    unsetLastLocation() {
        this.lastLocation.setXYZCoordinates( 0, 0, 0 )
    }

    async enterObserverMode( location: Location ): Promise<void> {
        this.setLastLocation()

        await this.getEffectList().stopSkillEffects( true, AbnormalType.HIDE )

        this.observerMode = true
        this.setTarget( null )
        this.startStunning()
        this.setIsInvulnerable( true )
        this.setInvisible( true )
        this.sendOwnedData( ObservationMode( location ) )

        await this.teleportToLocation( location, false )

        this.broadcastUserInfo()
    }

    setTarget( desiredTarget: L2Object ) {
        let target = desiredTarget

        if ( target ) {
            let isInParty: boolean = target.isPlayer() && this.isInParty() && this.getParty().getMembers().includes( target.getActingPlayer().getObjectId() )

            if ( !isInParty && ( Math.abs( target.getZ() - this.getZ() ) > 1000 ) ) {
                target = null
            }

            if ( target && !isInParty && !target.isVisible() ) {
                target = null
            }

            if ( !this.isGM() && target && target.isVehicle() ) {
                target = null
            }
        }

        let oldTarget: L2Object = this.getTarget()

        if ( oldTarget ) {
            if ( target && oldTarget.getObjectId() === target.getObjectId() ) {
                if ( target.getObjectId() !== this.getObjectId() ) {
                    this.sendOwnedData( ValidateLocation( target ) )
                }

                return
            }

            oldTarget.removeStatusListener( this.getObjectId() )
        }

        if ( target && target.isCharacter() ) {

            if ( target.getObjectId() !== this.getObjectId() ) {
                this.sendOwnedData( ValidateLocation( target ) )
                target.addStatusListener( this.getObjectId() )

                let targetCharacter: L2Character = target as L2Character
                let packet = new StatusUpdate( target.getObjectId() )
                        .addAttribute( StatusUpdateProperty.MaxHP, targetCharacter.getMaxHp() )
                        .addAttribute( StatusUpdateProperty.CurrentHP, targetCharacter.getCurrentHp() )

                if ( this.isGM() ) {
                    packet.addAttribute( StatusUpdateProperty.MaxMP, targetCharacter.getMaxMp() )
                            .addAttribute( StatusUpdateProperty.CurrentMP, targetCharacter.getCurrentMp() )
                }

                this.sendOwnedData( packet.getBuffer() )
            }

            this.sendOwnedData( MyTargetSelectedWithCharacters( this, target as L2Character ) )
            BroadcastHelper.dataBasedOnVisibility( this, TargetSelected( this, target.getObjectId() ) )
        } else {
            BroadcastHelper.dataToSelfBasedOnVisibility( this, TargetUnselected( this ) )
        }

        super.setTarget( target )
    }

    setLastLocation() {
        this.lastLocation.setXYZCoordinates( this.getX(), this.getY(), this.getZ() )
    }

    reviveAnswer( answer: DialogAnswerValue ): void {
        if ( ( this.reviveRequested !== 1 )
                || ( !this.isDead() && !this.revivePet )
                || ( this.revivePet && this.hasPet() && !this.getSummon().isDead() ) ) {
            return
        }

        if ( answer === DialogAnswerValue.Yes ) {
            let multiplier = this.reviveRecovery / 100
            let summon = this.getSummon()

            if ( !this.revivePet ) {
                if ( this.revivePower !== 0 ) {
                    this.doReviveWithPercentage( this.revivePower )
                } else {
                    this.doRevive()
                }

                if ( this.reviveRecovery !== 0 ) {
                    this.setCurrentHp( this.getMaxHp() * multiplier )
                    this.setCurrentMp( this.getMaxMp() * multiplier )
                    this.setCurrentCp( 0 )
                }
            } else if ( this.hasPet() ) {
                if ( this.revivePower !== 0 ) {
                    summon.doReviveWithPercentage( this.revivePower )
                } else {
                    summon.doRevive()
                }

                if ( this.reviveRecovery !== 0 ) {
                    summon.setCurrentHp( summon.getMaxHp() * multiplier )
                    summon.setCurrentMp( summon.getMaxMp() * multiplier )
                }
            }
        }

        this.revivePet = false
        this.reviveRequested = 0
        this.revivePower = 0
        this.reviveRecovery = 0
    }

    // TODO : combine confirmation action and admin confirm command into same structure, store it in separate cache
    removeConfirmationAction( action: ConfirmationAction ): boolean {
        return this.confirmationActions.delete( action )
    }

    getAdminConfirmCommand(): string {
        return this.adminConfirmCommand
    }

    setAdminConfirmCommmand( command: string ): void {
        this.adminConfirmCommand = command
    }

    getLastPetitionGmName() {
        return this.lastPetitionGmName
    }

    addAutoSoulShot( itemId: number ) {
        this.activeSoulShots.push( itemId )
    }

    isAllowedToEnchantSkills(): boolean {
        if ( this.isLocked()
                || this.getTransformation()
                || this.isUnderAttack()
                || this.isCastingNow()
                || this.isCastingSimultaneouslyNow() ) {
            return false
        }

        return !( this.isInBoat() || this.isInAirShip() )
    }

    removeSp( amount: number ): boolean {
        return this.getSubStat().removeSp( amount )
    }

    setIsInDuel( duelId: number ) {
        if ( duelId > 0 ) {
            this.duelState = DuelState.Action
            this.duelId = duelId
            return
        }

        if ( this.duelState === DuelState.Dead ) {
            this.enableAllSkills()
            this.getStatus().startHpMpRegeneration()
        }

        this.duelState = DuelState.None
        this.duelId = 0
    }

    async leaveOlympiadObserverMode(): Promise<void> {
        if ( this.olympiadGameId === -1 ) {
            return
        }

        this.olympiadGameId = -1
        this.observerMode = false
        this.setTarget( null )
        this.sendOwnedData( ExOlympiadMode( ExOlympiadModeType.Exit ) )

        this.setInstanceId( 0 )
        await this.teleportToLocation( this.lastLocation, true )

        if ( !this.isGM() ) {
            this.setInvisible( false )
            this.setIsInvulnerable( false )
        }

        this.unsetLastLocation()
        this.broadcastUserInfo()
    }

    setActiveEnchantAttributeItemId( objectId: number ) {
        this.activeEnchantAttributeItemId = objectId
    }

    setGroundSkillLocation( location: Location ): void {
        this.groundSkillLocation = location
    }

    isInventoryUnder90( includeQuestItems: boolean = false ) {
        return this.getInventory().getFullSize( includeQuestItems ) <= ( this.getInventoryLimit() * 0.9 )
    }

    isInventoryUnder80( includeQuestItems: boolean = false ) {
        return this.getInventory().getFullSize( includeQuestItems ) <= ( this.getInventoryLimit() * 0.8 )
    }

    setMovieId( value: number ) {
        this.movieId = value
    }

    getMovieId(): number {
        return this.movieId
    }

    getMultiSocialTarget() {
        return this.multiSocialTarget
    }

    giveRecommendations( target: L2PcInstance ) {
        target.incrementRecommendationsHave()
        this.decrementRecommendationsLeft()
    }

    decrementRecommendationsLeft() {
        if ( this.recommendationsLeft > 0 ) {
            this.recommendationsLeft--
        }
    }

    incrementRecommendationsHave() {
        if ( this.recommendationsHave < 255 ) {
            this.recommendationsHave++
        }
    }

    getServitorShareBonus( stat: string ) {
        return _.get( this.servitorShare, stat, 1 )
    }

    runDirectAreaRevalidation() {
        AreaDiscoveryManager.findAreas( this )
        this.startVitalityRegeneration()

        if ( ConfigManager.general.allowWater() ) {
            this.checkWaterState()
        }

        if ( this.isInSiegeArea() ) {
            if ( this.lastCompassZone === ExSetCompassZoneType.WarZoneEnd ) {
                return
            }

            this.lastCompassZone = ExSetCompassZoneType.WarZoneEnd
            this.sendOwnedData( ExSetCompassZoneCode( this.lastCompassZone ) )
            return
        }

        if ( this.isInArea( AreaType.PVP ) ) {
            if ( this.lastCompassZone === ExSetCompassZoneType.Pvp ) {
                return
            }

            this.lastCompassZone = ExSetCompassZoneType.Pvp
            this.sendOwnedData( ExSetCompassZoneCode( this.lastCompassZone ) )
            return
        }

        if ( this.isIn7sDungeon() ) {
            if ( this.lastCompassZone === ExSetCompassZoneType.SevenSigns ) {
                return
            }

            this.lastCompassZone = ExSetCompassZoneType.SevenSigns
            this.sendOwnedData( ExSetCompassZoneCode( this.lastCompassZone ) )
            return
        }

        if ( this.isInArea( AreaType.Peace ) ) {
            if ( this.lastCompassZone === ExSetCompassZoneType.Peace ) {
                return
            }

            this.lastCompassZone = ExSetCompassZoneType.Peace
            this.sendOwnedData( ExSetCompassZoneCode( this.lastCompassZone ) )
            return
        }

        if ( this.lastCompassZone === ExSetCompassZoneType.General ) {
            return
        }

        if ( this.lastCompassZone === ExSetCompassZoneType.WarZoneEnd ) {
            this.updatePvPStatus()
        }

        this.lastCompassZone = ExSetCompassZoneType.General
        this.sendOwnedData( ExSetCompassZoneCode( this.lastCompassZone ) )
    }

    checkWaterState(): void {
        if ( this.isInArea( AreaType.Water ) ) {
            return this.startWaterTask()
        }

        return this.stopWaterTask()
    }

    startWaterTask(): void {
        if ( this.isDead() || this.waterDamageTask ) {
            return
        }

        let safeTimeInWater: number = this.calculateStat( Stats.BREATH, 60000, this, null )

        this.sendOwnedData( SetupGauge( this.getObjectId(), SetupGaugeColors.Cyan, safeTimeInWater ) )
        this.waterDamageTask = setTimeout( this.runSetupWaterDamageTask.bind( this ), safeTimeInWater )
    }

    runSetupWaterDamageTask(): void {
        clearInterval( this.waterDamageTask )
        this.waterDamageTask = setInterval( this.runWaterTask.bind( this ), 1000 )
    }

    async runWaterTask(): Promise<void> {
        let value: number = Math.floor( Math.max( this.getMaxHp() / 100, 1 ) )

        await this.reduceCurrentHp( value, this, null, false, false )

        let packet = new SystemMessageBuilder( SystemMessageIds.DROWN_DAMAGE_S1 )
                .addNumber( value )
                .getBuffer()
        this.sendOwnedData( packet )
    }

    setCharmOfCourage( value: boolean ) {
        this.hasCharmOfCourageValue = value
    }

    isCastleLord( castleId: number ): boolean {
        let clan: L2Clan = this.getClan()
        if ( clan && clan.getLeaderId() === this.getObjectId() ) {
            let castle: Castle = CastleManager.getCastleByOwner( clan )
            if ( castle && castle.getResidenceId() === castleId ) {
                return true
            }
        }

        return false
    }

    hasCommonCraft(): boolean {
        return this.getCommonCraft() >= 1
    }

    getCommonCraft(): number {
        return this.getSkillLevel( CommonSkillIds.CreateCommonItems )
    }

    setAdminConfirmCommand( command: string ) {
        this.adminConfirmCommand = command
    }

    addConfirmationAction( action: ConfirmationAction ): void {
        this.confirmationActions.add( action )
    }

    getOlympiadBuffCount() {
        return this.olympiadBuffCount
    }

    getAccountCharacters(): Map<number, string> {
        return this.characters
    }

    setLottery( index: number, value: number ) {
        this.lottery[ index ] = value
    }

    getLottery( index: number ): number {
        return this.lottery[ index ]
    }

    async enterOlympiadObserverMode( location: ILocational, id: number ): Promise<void> {
        if ( this.hasSummon() ) {
            await this.getSummon().unSummon( this )
        }

        await this.getEffectList().stopSkillEffects( true, AbnormalType.HIDE )

        if ( this.getParty() ) {
            this.getParty().removePartyMember( this, L2PartyMessageType.Expelled )
        }

        this.olympiadGameId = id

        if ( this.isSitting() ) {
            await this.standUp()
        }
        if ( !this.observerMode ) {
            this.setLastLocation()
        }

        this.observerMode = true
        this.setTarget( null )
        this.setIsInvulnerable( true )
        this.setInvisible( true )
        this.stopCubics()

        await this.teleportToLocation( location, false )
        this.sendOwnedData( ExOlympiadMode( ExOlympiadModeType.Observer ) )
    }

    async mountWithId( petId: number, itemObjectId: number, useFood: boolean ): Promise<boolean> {
        if ( this.isTransformed() || !( await this.disarmWeapons() ) || !( await this.disarmShield() ) ) {
            return false
        }

        await this.getEffectList().stopTogglEffects()
        await this.setMount( petId, this.getLevel() )
        this.setMountObjectID( itemObjectId )

        BroadcastHelper.dataToSelfBasedOnVisibility( this, Ride( this ) )
        this.broadcastUserInfo()

        if ( useFood ) {
            this.startFeedMount( petId )
        }

        return true
    }

    getExpertiseLevel(): number {
        return _.clamp( this.getSkillLevel( 239 ), 0, Number.MAX_SAFE_INTEGER )
    }

    setLastFolkNPC( npc: L2Npc ) {
        this.lastFolkNpc = npc.getObjectId()
    }

    updateNotMoveUntil() {
        this.notMoveUntil = Date.now() + ConfigManager.character.getNpcTalkBlockingTime() * 1000
    }

    setLastQuestNpc( value: number ) {
        this.questNpcId = value
    }

    onActionRequest() : void {
        if ( this.isSpawnProtected() ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NO_LONGER_PROTECTED_FROM_AGGRESSIVE_MONSTERS ) )
        }

        if ( this.isTeleportProtected() ) {
            this.sendMessage( 'Teleport spawn protection ended.' )
        }

        this.setProtection( false )
        this.setTeleportProtection( false )
    }

    setTeleportProtection( value: boolean ) {
        if ( value && ConfigManager.character.getPlayerTeleportProtection() > 0 ) {
            this.teleportProtectEndTime = ConfigManager.character.getPlayerTeleportProtection() * 1000 + Date.now()
            return
        }

        this.teleportProtectEndTime = 0
    }

    isTeleportProtected() {
        return this.teleportProtectEndTime > Date.now()
    }

    isInDuelWith( target: L2Character ): boolean {
        if ( !this.isInDuel() || !target.isInDuel() ) {
            return false
        }

        return this.getDuelId() === target.getDuelId()
    }

    isOnSameSiegeSideWith( target: L2Character ): boolean {

        return this.getSiegeRole() !== SiegeRole.None
                && this.isInSiegeArea()
                && this.getSiegeRole() === target.getSiegeRole()
                && this.getSiegeSide() === target.getSiegeSide()
    }

    isInCommandChannelWith( target: L2Character ): boolean {
        if ( !this.isInParty() || !target.isInParty() ) {
            return false
        }

        if ( !this.getParty().isInCommandChannel() || !target.getParty().isInCommandChannel() ) {
            return false
        }

        return this.getParty().getCommandChannel().getLeaderObjectId() === target.getParty().getCommandChannel().getLeaderObjectId()

    }

    isAtWarWith( target: L2Character ): boolean {
        if ( !target ) {
            return false
        }

        let clan = this.getClan()
        if ( clan && !this.isAcademyMember() ) {
            let targetClanId = target.getClanId()
            if ( targetClanId > 0 && !target.isAcademyMember() ) {
                return clan.isAtWarWith( targetClanId )
            }
        }

        return false
    }

    startDismountTask( delay: number ) {
        this.dismountTask = setTimeout( this.dismount.bind( this ), delay )
    }

    clearDismountTask() {
        if ( this.dismountTask ) {
            clearTimeout( this.dismountTask )
        }

        this.dismountTask = null
    }

    setIsInSiege( value: boolean ) {
        this.isInSiegeValue = value
    }

    startFameTask( delay: number, fameAmount: number ) {
        if ( this.getLevel() < 40 || ClassId.getLevelByClassId( this.getClassId() ) < 2 ) {
            return
        }

        if ( !this.fameTask ) {
            this.fameTask = setInterval( this.runReceiveFame.bind( this ), delay, fameAmount )
        }
    }

    async useMagic( skill: Skill, isControlPressed: boolean, isShiftPressed: boolean ): Promise<boolean> {
        if ( skill.isPassive() ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.isCastingNow() ) {
            if ( this.getMainCast().skill.getId() === skill.getId() ) {
                this.sendOwnedData( ActionFailed() )
                return false
            }

            if ( this.isSkillDisabled( skill ) ) {
                this.sendOwnedData( ActionFailed() )
                return false
            }

            // TODO : queue skill action via AIController
            this.sendOwnedData( ActionFailed() )
            return false
        }

        /*
            Fast path for toggle skills, since these will simply be either switched on or off.
         */
        if ( skill.isToggle() ) {
            this.mainSkillFlags.isCtrlPressed = isControlPressed
            this.mainSkillFlags.isShiftPressed = isShiftPressed

            await this.doCast( skill )
            return true
        }

        let target: L2Object

        if ( L2TargetType.GROUND === skill.getTargetType() || L2TargetType.SELF === skill.getTargetType() ) {
            target = this
        } else {
            target = await skill.getSingleTargetOf( this )
        }

        if ( !target ) {
            return false
        }

        AIEffectHelper.notifyMustCastSpell( this, target as L2Character, skill.getId(), skill.getLevel(), isControlPressed, isShiftPressed )
        return true
    }

    setCastInterruptTime( value: number ) {
        this.castInterruptTime = value - 400
    }

    async doAttack( target: L2Character ): Promise<void> {
        // TODO : convert to npc id listener, modify tutorial quest
        if ( ListenerCache.hasObjectListener( this.getObjectId(), EventType.PlayerStartsAttack ) ) {

            let eventData = EventPoolCache.getData( EventType.PlayerStartsAttack ) as PlayerStartsAttackEvent

            eventData.playerId = this.getObjectId()
            eventData.targetId = target.getObjectId()
            eventData.targetType = target.getInstanceType()

            await ListenerCache.sendObjectEvent( this.getObjectId(), EventType.PlayerStartsAttack, eventData )
        }

        this.activateCubicsOnAttack()
        await super.doAttack( target )
        this.setRecentFakeDeath( false )
    }

    async checkAndEquipArrows(): Promise<void> {
        if ( !this.getInventory().getPaperdollItem( InventorySlot.LeftHand ) ) {
            let arrowItem = this.getInventory().findArrowForBow( this.getActiveWeaponItem() )

            if ( arrowItem ) {
                await this.getInventory().setPaperdollItem( InventorySlot.LeftHand, arrowItem )
            }
        }
    }

    async reduceArrowCount(): Promise<void> {
        let arrows: L2ItemInstance = this.getInventory().getPaperdollItem( InventorySlot.LeftHand )

        if ( !arrows ) {
            return
        }

        if ( arrows.getCount() > 1 ) {
            arrows.changeAmount( -1 )
            this.getInventory().markItem( arrows, InventoryUpdateStatus.Modified )

            return
        }

        await this.getInventory().destroyItem( arrows, arrows.getCount() )
        return this.getInventory().unEquipItemInSlot( InventorySlot.LeftHand )
    }

    async doAutoLoot( target: L2Attackable, itemId: number, count: number ): Promise<void | L2ItemInstance> {
        if ( this.isInParty() && !DataManager.getItems().getTemplate( itemId ).hasExImmediateEffect() ) {
            return this.getParty().distributeItem( this, itemId, count, false, target )
        }

        if ( itemId === ItemTypes.Adena ) {
            return this.addAdena( count, 'L2PcInstance.doAutoLoot', target.getObjectId(), true )
        }

        return this.addItem( itemId, count, -1, target.getObjectId(), InventoryAction.PlayerAutoloot )
    }

    isRecentFakeDeath() {
        return this.recentFakeDeathEndTime > Date.now()
    }

    cannotPickItem(): boolean {
        return this.isAlikeDead() || this.isInvisible()
    }

    async doPickupItem( target: L2ItemInstance ): Promise<void> {
        if ( this.cannotPickItem() ) {
            return
        }

        if ( !target.isVisible() ) {
            this.sendOwnedData( ActionFailed() )
            return
        }

        if ( FortSiegeManager.isCombatFlag( target.getId() ) && !FortSiegeManager.checkIfCanPickup( this ) ) {
            return
        }

        let party = this.getParty()
        if ( !target.canBePickedUpBy( this ) || ( party && party.getDistributionType() !== PartyDistributionType.FindersKeepers ) ) {
            let message: SystemMessageBuilder

            if ( target.getId() === ItemTypes.Adena ) {
                message = new SystemMessageBuilder( SystemMessageIds.FAILED_TO_PICKUP_S1_ADENA )
                        .addNumber( target.getCount() )
            } else if ( target.getCount() > 1 ) {
                message = new SystemMessageBuilder( SystemMessageIds.FAILED_TO_PICKUP_S2_S1_S )
                        .addItemInstanceName( target )
                        .addNumber( target.getCount() )
            } else {
                message = new SystemMessageBuilder( SystemMessageIds.FAILED_TO_PICKUP_S1 )
                        .addItemInstanceName( target )
            }

            this.sendOwnedData( ActionFailed() )
            this.sendOwnedData( message.getBuffer() )
            return
        }

        if ( !this.getInventory().validateCapacityForItem( target ) ) {
            this.sendOwnedData( ActionFailed() )
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
            return
        }

        ItemsOnGroundManager.removeObject( target )
        await target.pickupMeBy( this )

        if ( target.getItem().hasExImmediateEffect() && target.isEtcItem() ) {
            let handler: ItemHandlerMethod = target.getEtcItem().getItemHandler()
            if ( handler ) {
                await handler( this, target, false )
            }

            return ItemManagerCache.destroyItem( target )
        }

        if ( CursedWeaponManager.isCursedItem( target.getId() ) ) {
            return this.addItemWithInstance( target, null, true, 'L2PcInstance.doPickupItem' )
        }

        if ( FortSiegeManager.isCombatFlag( target.getId() ) ) {
            return this.addItemWithInstance( target, null, true, 'L2PcInstance.doPickupItem' )
        }

        if ( target.isArmor() || target.isWeapon() ) {

            let message: SystemMessageBuilder
            if ( target.getEnchantLevel() > 0 ) {
                message = new SystemMessageBuilder( SystemMessageIds.ANNOUNCEMENT_C1_PICKED_UP_S2_S3 )
                        .addPlayerCharacterName( this )
                        .addNumber( target.getEnchantLevel() )
                        .addItemInstanceName( target )
            } else {
                message = new SystemMessageBuilder( SystemMessageIds.ANNOUNCEMENT_C1_PICKED_UP_S2 )
                        .addPlayerCharacterName( this )
                        .addItemInstanceName( target )
            }

            BroadcastHelper.dataToSelfBasedOnVisibility( this, message.getBuffer(), 1400 )
        }

        if ( party ) {
            return party.distributeExistingItem( this, target )
        }

        if ( target.getId() === ItemTypes.Adena ) {
            await this.addAdena( target.getCount(), 'L2PcInstance.doPickupItem', target.getObjectId(), true )
            return ItemManagerCache.destroyItem( target )
        }

        await this.addItemWithInstance( target, null, true, 'L2PcInstance.doPickupItem' )

        let etcItem: L2EtcItem = target.getEtcItem()
        if ( !etcItem ) {
            return
        }

        let weapon: L2ItemInstance = this.getInventory().getPaperdollItem( InventorySlot.RightHand )
        if ( !weapon ) {
            return
        }

        let itemType: EtcItemType = etcItem.getItemType()
        if ( weapon.getItemType() === WeaponType.BOW && itemType === EtcItemType.ARROW ) {
            return this.checkAndEquipArrows()
        }

        if ( weapon.getItemType() === WeaponType.CROSSBOW && itemType === EtcItemType.BOLT ) {
            return this.checkAndEquipBolts()
        }
    }

    async addItemWithInstance( item: L2ItemInstance, reference: number = null, sendMessage: boolean = false, reason: string = null ): Promise<void> {
        if ( item.getCount() <= 0 ) {
            return
        }

        if ( sendMessage ) {
            let message: SystemMessageBuilder

            if ( item.getCount() > 1 ) {
                message = new SystemMessageBuilder( SystemMessageIds.YOU_PICKED_UP_S1_S2 )
                        .addItemInstanceName( item )
                        .addNumber( item.getCount() )

            } else if ( item.getEnchantLevel() > 0 ) {

                message = new SystemMessageBuilder( SystemMessageIds.YOU_PICKED_UP_A_S1_S2 )
                        .addNumber( item.getEnchantLevel() )
                        .addItemInstanceName( item )
            } else {
                message = new SystemMessageBuilder( SystemMessageIds.YOU_PICKED_UP_S1 )
                        .addItemInstanceName( item )
            }

            this.sendOwnedData( message.getBuffer() )
        }

        let updatedItem: L2ItemInstance = await this.getInventory().addExistingItem( item, reason )
        if ( !this.hasActionOverride( PlayerActionOverride.ItemAction )
                && !this.getInventory().validatePlayerCapacity( 0, item.isQuestItem() )
                && updatedItem.isDropable()
                && ( !updatedItem.isStackable() || ( updatedItem.getInventoryStatus() !== InventoryUpdateStatus.Modified ) ) ) {
            await this.dropItem( updatedItem, true, true )
            return
        }

        if ( CursedWeaponManager.isCursedItem( updatedItem.getId() ) ) {
            return CursedWeaponManager.activate( this, updatedItem )
        }

        if ( FortSiegeManager.isCombatFlag( item.getId() ) && FortSiegeManager.activateCombatFlag( this, item ) ) {
            let fort: Fort = FortManager.getFort( this )

            let message: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_ACQUIRED_THE_FLAG )
                    .addPlayerCharacterName( this )
                    .getBuffer()
            fort.getSiege().announceDataToPlayers( message )

            return
        }

        if ( ( item.getId() >= 13560 ) && ( item.getId() <= 13568 ) ) {
            let ward: TerritoryWard = TerritoryWarManager.getTerritoryWard( item.getId() - 13479 )
            if ( ward ) {
                await ward.activate( this, item )
            }

            return
        }
    }

    async checkAndEquipBolts() : Promise<void> {
        if ( !this.getInventory().getPaperdollItem( InventorySlot.LeftHand ) ) {
            let boltItem = this.getInventory().findBoltForCrossBow( this.getActiveWeaponItem() )
            if ( boltItem ) {
                await this.getInventory().setPaperdollItem( InventorySlot.LeftHand, boltItem )
            }
        }
    }

    isChargedShot( type: ShotType ): boolean {
        let weapon: L2ItemInstance = this.getActiveWeaponInstance()

        if ( weapon ) {
            return weapon.isChargedShot( type )
        }

        return false
    }

    async changeActiveClass( classIndex: number ): Promise<boolean> {
        if ( this.transformation ) {
            return false
        }

        let player = this
        _.each( this.getInventory().getAugmentedItems(), ( item: L2ItemInstance ) => {
            if ( item && item.isEquipped() ) {
                item.getAugmentation().removeBonus( player )
            }
        } )

        this.abortCast()

        if ( this.isChannelized() ) {
            this.getSkillChannelized().abortChannelization()
        }

        await this.storeInDatabase( ConfigManager.character.subclassStoreSkillCooltime() )
        this.resetSkillReuse()
        this.clearCharges()
        this.stopChargeTask()

        if ( this.hasServitor() ) {
            await this.getSummon().unSummon( this )
        }

        if ( classIndex === 0 ) {
            this.setClassTemplate( this.getBaseClass() )
        } else {
            let subclass = this.getSubClasses()[ classIndex ]
            if ( subclass ) {
                this.setClassTemplate( subclass.getClassId() )
            }
        }

        this.classIndex = classIndex
        this.setLearningClass( this.getClassId() )

        if ( this.isInParty() ) {
            this.getParty().recalculatePartyLevel()
        }

        // Update the character's change in class status.
        // 1. Remove any active cubics from the player.
        // 2. Renovate the characters table in the database with the new class info, storing also buff/effect data.
        // 3. Remove all existing skills.
        // 4. Restore all the learned skills for the current class from the database.
        // 5. Restore effect/buff data for the new class.
        // 6. Restore henna data for the class, applying the new stat modifiers while removing existing ones.
        // 7. Reset HP/MP/CP stats and send Server->Client character status packet to reflect changes.
        // 8. Restore shortcut data related to this class.
        // 9. Resend a class change animation effect to broadcast to all nearby players.
        await aigle.resolve( this.getAllSkills() ).each( ( skill: Skill ) => {
            return player.removeSkill( skill, false, true )
        } )

        await this.stopAllEffectsExceptThoseThatLastThroughDeath()
        await this.stopAllEffectsNotStayOnSubclassChange()
        this.stopCubics()

        await PlayerRecipeCache.loadPlayer( this.getObjectId(), this.getClassIndex() )
        this.restoreDeathPenaltyBuffLevel()

        await L2PcInstanceHelper.loadPlayerSkills( this )

        await this.rewardSkills()
        await this.reGiftTemporarySkills()
        this.resetDisabledSkills()
        await this.restoreEffects()

        this.sendDebouncedPacket( EtcStatusUpdate )

        let state: QuestState = QuestStateCache.getQuestState( this.getObjectId(), 'Q00422_RepentYourSins' )
        if ( state ) {
            await state.exitQuest( true )
        }

        await PlayerHennaCache.loadPlayer( this.getObjectId(), this.getClassIndex() )

        this.recalculateHennaStats()
        this.sendOwnedData( HennaInfo( this ) )

        if ( this.getCurrentHp() > this.getMaxHp() ) {
            this.setCurrentHp( this.getMaxHp() )
        }
        if ( this.getCurrentMp() > this.getMaxMp() ) {
            this.setCurrentMp( this.getMaxMp() )
        }
        if ( this.getCurrentCp() > this.getMaxCp() ) {
            this.setCurrentCp( this.getMaxCp() )
        }

        this.refreshOverloadPenalty()
        this.refreshExpertisePenalty()
        this.broadcastUserInfo()
        this.setExpBeforeDeath( 0 )

        await PlayerShortcutCache.loadPlayer( this )
        this.sendOwnedData( ShortCutInit( this ) )

        BroadcastHelper.dataToSelfBasedOnVisibility( this, SocialAction( this.getObjectId(), SocialActionType.LevelUp ) )
        this.sendDebouncedPacket( SkillCoolTime )
        this.sendOwnedData( ExStorageMaxCount( this ) )

        return true
    }

    stopCubics(): void {
        if ( this.cubics.size > 0 ) {
            this.cubics.forEach( cubic => cubic.terminate() )

            this.cubics.clear()
            this.broadcastUserInfo()
        }
    }

    stopAllEffectsNotStayOnSubclassChange() : Promise<void> {
        this.updateAndBroadcastStatus( 2 )
        return this.getEffectList().stopAllEffectsNotStayOnSubclassChange()
    }

    async reGiftTemporarySkills(): Promise<void> {
        if ( this.isNoble() ) {
            await this.setNoble( true )
        }

        if ( this.isHero() ) {
            await this.setHero( true )
        }

        if ( this.getClan() ) {
            let clan = this.getClan()
            await clan.addSkillEffects( this )

            if ( ( clan.getLevel() >= SiegeManager.getSiegeClanMinLevel() ) && this.isClanLeader() ) {
                await SiegeManager.addSiegeSkills( this )
            }

            if ( clan.getCastleId() > 0 ) {
                await CastleManager.getCastleByOwner( clan ).giveResidentialSkills( this )
            }

            if ( clan.getFortId() > 0 ) {
                await FortManager.getFortByOwner( clan ).giveResidentialSkills( this )
            }
        }

        await this.getInventory().reloadEquippedItems()

        this.restoreDeathPenaltyBuffLevel()
    }

    hasCharmOfCourage() {
        return this.hasCharmOfCourageValue
    }

    onKillUpdatePvPKarma( target: L2Character ) : Promise<void> {
        if ( !target || !target.isPlayable() ) {
            return
        }

        let targetPlayer: L2PcInstance = target.getActingPlayer()
        if ( !targetPlayer || targetPlayer.getObjectId() === this.getObjectId() ) {
            return
        }

        if ( this.isCursedWeaponEquipped() && target.isPlayer() ) {
            return CursedWeaponManager.increaseKills( this.cursedWeaponEquippedId )
        }

        if ( this.isInDuel() && targetPlayer.isInDuel() ) {
            return
        }

        let isPvpZone = this.isInArea( AreaType.PVP ) || targetPlayer.isInArea( AreaType.PVP )
        if ( isPvpZone ) {
            if ( this.getSiegeRole() !== SiegeRole.None
                    && targetPlayer.getSiegeRole() !== SiegeRole.None
                    && this.getSiegeRole() !== targetPlayer.getSiegeRole() ) {
                let killerClan: L2Clan = this.getClan()
                let targetClan: L2Clan = targetPlayer.getClan()

                if ( killerClan && targetClan ) {
                    killerClan.addSiegeKill()
                    targetClan.addSiegeDeath()
                }
            }

            return
        }

        if ( ( this.checkIfPvP( target ) && targetPlayer.hasPvPFlag() ) || isPvpZone ) {
            this.increasePvpKills( target )
            return
        }

        if ( targetPlayer.getPledgeType() !== L2ClanValues.SUBUNIT_ACADEMY
                && this.getPledgeType() !== L2ClanValues.SUBUNIT_ACADEMY
                && targetPlayer.getClan()
                && this.getClan()
                && this.getClan().isAtWarWith( targetPlayer.getClanId() )
                && targetPlayer.getClan().isAtWarWith( this.getClanId() ) ) {
            this.increasePvpKills( target )
            return
        }

        if ( targetPlayer.getKarma() > 0 ) {
            if ( ConfigManager.pvp.awardPKKillPVPPoint() ) {
                this.increasePvpKills( target )
            }

            return
        }

        if ( !targetPlayer.hasPvPFlag() ) {
            this.setKarma( this.getKarma() + Formulas.calculateKarmaGain( this.getPkKills(), target.isSummon() ) )

            if ( target.isPlayer() ) {
                this.setPkKills( this.getPkKills() + 1 )
            }

            this.sendDebouncedPacket( UserInfo )
            this.sendDebouncedPacket( ExBrExtraUserInfo )

            this.stopPvPFlag()
            return this.checkItemRestriction()
        }
    }

    increasePvpKills( target: L2Character ) {
        if ( target.isPlayer() && isValidPlayerKill( target as L2PcInstance ) ) {
            this.setPvpKills( this.getPvpKills() + 1 )

            this.sendDebouncedPacket( UserInfo )
            this.sendDebouncedPacket( ExBrExtraUserInfo )
        }
    }

    async addQuestExpAndSp( expAmount: number, spAmount: number ): Promise<void> {
        let clan = this.getClan()
        let clanHall = clan ? clan.getClanHall() : null

        if ( expAmount !== 0 ) {
            let value = expAmount
            if ( ConfigManager.rates.isQuestExpUsePlayerBonusModifier() ) {
                value = Math.floor( expAmount * this.getStat().getExpBonusMultiplier() )
            }

            if ( clanHall ) {
                let xpFunction = clanHall.getFunction( ClanHallFunctionType.QuestXPBonus )
                if ( xpFunction && xpFunction.level > 0 ) {
                    value = value * ( xpFunction.level / 100 )
                }
            }

            let isSuccess = await this.getSubStat().addExp( value )
            if ( !isSuccess ) {
                return
            }

            let message = new SystemMessageBuilder( SystemMessageIds.EARNED_S1_EXPERIENCE )
                    .addNumber( value )
                    .getBuffer()
            this.sendOwnedData( message )
        }

        if ( spAmount !== 0 ) {
            let value: number = spAmount
            if ( ConfigManager.rates.isQuestSpUsePlayerBonusModifier() ) {
                value = Math.floor( spAmount * this.getStat().getSpBonusMultiplier() )
            }

            if ( clanHall ) {
                let spFunction = clanHall.getFunction( ClanHallFunctionType.QuestSPBonus )
                if ( spFunction && spFunction.level > 0 ) {
                    value = value * ( spFunction.level / 100 )
                }
            }

            this.getSubStat().addSp( value )
            let message = new SystemMessageBuilder( SystemMessageIds.ACQUIRED_S1_SP )
                    .addNumber( value )
                    .getBuffer()
            this.sendOwnedData( message )
        }

        this.sendDebouncedPacket( UserInfo )
        this.sendDebouncedPacket( ExBrExtraUserInfo )
    }

    async onDieDropItem( character: L2Character ): Promise<void> {
        if ( L2Event.isParticipant( this.getObjectId() ) || !character ) {
            return
        }

        let playerKiller: L2PcInstance = character.getActingPlayer()
        if ( this.getKarma() <= 0
                && playerKiller
                && playerKiller.getClan()
                && this.getClan()
                && playerKiller.getClan().isAtWarWith( this.getClanId() ) ) {
            return
        }

        if ( this.isInArea( AreaType.SevenSigns ) && this.itemDropState !== ItemDropState.NoRestrictions ) {
            return
        }

        if ( ( !this.isInArea( AreaType.PVP ) || !playerKiller ) && ( !this.isGM() || ConfigManager.pvp.canGMDropEquipment() ) ) {
            let isKillerNpc = character.isNpc()
            let pkLimit = ConfigManager.pvp.getMinimumPKRequiredToDrop()

            let dropEquip = 0
            let dropEquipWeapon = 0
            let dropItem = 0
            let dropLimit = 0
            let dropPercent = 0

            if ( this.getKarma() > 0 && this.getPkKills() >= pkLimit ) {
                dropPercent = ConfigManager.rates.getKarmaRateDrop()
                dropEquip = ConfigManager.rates.getKarmaRateDropEquipment()
                dropEquipWeapon = ConfigManager.rates.getKarmaRateDropEquippedWeapon()
                dropItem = ConfigManager.rates.getKarmaRateDropItem()
                dropLimit = ConfigManager.rates.getKarmaDropLimit()
            } else if ( isKillerNpc && this.getLevel() > 4 ) {
                dropPercent = ConfigManager.rates.getPlayerRateDrop()
                dropEquip = ConfigManager.rates.getPlayerRateDropEquipment()
                dropEquipWeapon = ConfigManager.rates.getPlayerRateDropEquippedWeapon()
                dropItem = ConfigManager.rates.getPlayerRateDropItem()
                dropLimit = ConfigManager.rates.getPlayerDropLimit()
            }

            if ( ( dropPercent > 0 ) && ( _.random( 100 ) < dropPercent ) ) {
                let dropCount = 0
                let itemDropPercent = 0

                let currentPlayer = this
                await aigle.resolve( this.getInventory().getItems() ).someSeries( async ( itemDrop: L2ItemInstance ) => {
                    if ( itemDrop.isShadowItem()
                            || itemDrop.isTimeLimitedItem()
                            || !itemDrop.isDropable()
                            || itemDrop.getId() === ItemTypes.Adena
                            || itemDrop.getItem().getType2() === ItemType2.QUEST
                            || ( currentPlayer.hasSummon() && ( currentPlayer.getSummon().getControlObjectId() === itemDrop.getObjectId() ) )
                            || ConfigManager.pvp.getNonDroppableItems().includes( itemDrop.getId() )
                            || ConfigManager.pvp.getPetItems().includes( itemDrop.getId() ) ) {
                        return false
                    }

                    if ( itemDrop.isEquipped() ) {
                        itemDropPercent = itemDrop.getItem().getType2() === ItemType2.WEAPON ? dropEquipWeapon : dropEquip
                        await currentPlayer.getInventory().unEquipItemInSlot( itemDrop.getLocationSlot() )
                    } else {
                        itemDropPercent = dropItem
                    }

                    // each time an item is dropped, the chance of another item being dropped gets lesser (dropCount * 2)
                    if ( _.random( 100 ) < itemDropPercent ) {
                        await currentPlayer.dropItem( itemDrop, true, false )

                        if ( ++dropCount >= dropLimit ) {
                            return true
                        }
                    }

                    return false
                } )
            }
        }
    }

    calculateDeathExpPenalty( character: L2Character, isAtWar: boolean ): Promise<void> {
        let level = this.getLevel()
        let percentLost = DataManager.getXPLostData().getXpPercent( this.getLevel() )

        if ( character ) {
            if ( character.isRaid() ) {
                percentLost *= this.calculateStat( Stats.REDUCE_EXP_LOST_BY_RAID, 1 )
            } else if ( character.isMonster() ) {
                percentLost *= this.calculateStat( Stats.REDUCE_EXP_LOST_BY_MOB, 1 )
            } else if ( character.isPlayable() ) {
                percentLost *= this.calculateStat( Stats.REDUCE_EXP_LOST_BY_PVP, 1 )
            }
        }

        if ( this.getKarma() > 0 ) {
            percentLost *= ConfigManager.rates.getRateKarmaExpLost()
        }

        let lostExperience = 0
        if ( !NevitManager.isBlessingActive( this.getObjectId() ) ) {
            if ( !L2Event.isParticipant( this.getObjectId() ) ) {
                if ( level < ConfigManager.character.getMaxPlayerLevel() ) {
                    lostExperience = Math.round( ( ( this.getStat().getExpForLevel( level + 1 ) - this.getStat().getExpForLevel( level ) ) * percentLost ) / 100 )
                } else {
                    lostExperience = Math.round( ( ( this.getStat().getExpForLevel( ConfigManager.character.getMaxPlayerLevel() + 1 ) - this.getStat().getExpForLevel( ConfigManager.character.getMaxPlayerLevel() ) ) * percentLost ) / 100 )
                }
            }

            if ( this.isInArea( AreaType.SevenSigns ) ) {
                lostExperience = Math.round( ( this.expPenalty * percentLost ) / 1000 )
            }

            if ( isAtWar ) {
                lostExperience /= 4.0
            }
        }

        this.setExpBeforeDeath( this.getExp() )
        return this.removeExp( lostExperience )
    }

    async modifySubClass( classIndex: number, classId: number ): Promise<boolean> {
        await Promise.all( [
            DatabaseManager.getCharacterHennas().deleteByClassIndex( this.getObjectId(), classIndex ),
            DatabaseManager.getCharacterSkills().deleteByClassIndex( this.getObjectId(), classIndex ),
            DatabaseManager.getCharacterShortcuts().deleteByClassIndex( this.getObjectId(), classIndex ),
            DatabaseManager.getCharacterSkillsSave().deleteByClassIndex( this.getObjectId(), classIndex ),
            DatabaseManager.getCharacterSubclasses().deleteByClassIndex( this.getObjectId(), classIndex ),
        ] )

        if ( ListenerCache.hasGeneralListener( EventType.PlayerCancelProfession ) ) {
            let subClass = this.getSubClasses()[ classIndex ]

            let eventData: PlayerCancelProfessionEvent = {
                playerId: this.getObjectId(),
                classId: subClass ? subClass.getClassId() : -1,
            }

            await ListenerCache.sendGeneralEvent( EventType.PlayerCancelProfession, eventData )
        }

        _.unset( this.subClasses, classIndex )

        return this.addSubClass( classId, classIndex )
    }

    async addSubClass( classId: number, classIndex: number ): Promise<boolean> {
        if ( this.getTotalSubClasses() === ConfigManager.character.getMaxSubclass() || classIndex === 0 ) {
            return false
        }

        if ( this.getSubClasses()[ classIndex ] ) {
            return false
        }

        let amount = DataManager.getExperienceData().getExpForLevel( ConfigManager.character.getBaseSubclassLevel() )
        let newClass: SubClass = new SubClass( this, amount )
        newClass.setClassId( classId )
        newClass.setClassIndex( classIndex )

        await DatabaseManager.getCharacterSubclasses().addSubclass( this.getObjectId(), newClass )

        this.getSubClasses()[ newClass.getClassIndex() ] = newClass

        let skills: { [ id: number ]: L2SkillLearn } = SkillCache.getCompleteClassSkillTree( classId )
        let skillLevelMap: { [ skillId: number ]: Skill } = {}

        Object.values( skills ).forEach( ( skillInfo: L2SkillLearn ) : void => {
            if ( skillInfo.getGetLevel() > 40 ) {
                return
            }

            let oldSkill: Skill = skillLevelMap[ skillInfo.getSkillId() ]
            let newSkill: Skill = SkillCache.getSkill( skillInfo.getSkillId(), skillInfo.getSkillLevel() )

            if ( oldSkill && oldSkill.getLevel() > newSkill.getLevel() ) {
                return
            }

            skillLevelMap[ newSkill.getId() ] = newSkill
            return this.storeSkill( newSkill, classIndex )
        } )

        return true
    }

    isSilenceModeForPlayer( objectId: number ): boolean {
        if ( ConfigManager.character.silenceModeExclude() && this.silenceMode && this.silenceModeExcluded.length > 0 ) {
            return !this.silenceModeExcluded.includes( objectId )
        }

        return this.silenceMode
    }

    addSilenceModeExcluded( objectId: number ) {
        this.silenceModeExcluded.push( objectId )
    }

    async onTeleported(): Promise<void> {
        await super.onTeleported()

        if ( this.isInAirShip() ) {
            this.getAirShip().shouldDescribeState( this )
        }

        this.revalidateZone( true )
        await this.checkItemRestriction()

        await DatabaseManager.getCharacterTable().updatePlayerCharacter( this )

        if ( ConfigManager.character.getPlayerTeleportProtection() > 0 && !this.isInOlympiadMode() ) {
            this.setTeleportProtection( true )
        }

        // TODO : remove tamed beast object ids
        if ( this.tamedBeasts.length > 0 ) {
            this.tamedBeasts.forEach( ( objectId: number ) => {
                let beast: L2TamedBeastInstance = L2World.getObjectById( objectId ) as L2TamedBeastInstance
                if ( beast ) {
                    beast.deleteMe()
                }
            } )
        }

        let summon: L2Summon = this.getSummon()
        if ( summon ) {
            // TODO : do we really need all that follow status ?

            await AIEffectHelper.setNextIntent( summon, AIIntent.WAITING )
            await summon.teleportToLocationCoordinates( ...CharacterSummonCache.getSpawnCoordinates( this ), 0, this.getInstanceId() )
            await summon.updateAndBroadcastStatus( PetInfoAnimation.None )
            AIEffectHelper.notifyFollow( summon, summon.getOwnerId() )
        }

        await TvTEvent.onTeleported( this )

        /*
            Invalidates and forces reload of all npcs when teleporting back and forth between GKs.
            Allows keeping two processes separate: discover new vs remove old visible objects.
         */
        ProximalDiscoveryManager.forgetAllObjects( this.getObjectId() )
    }

    getMaxExpLevel(): number {
        return this.getSubStat().getMaxExpLevel()
    }

    getLure(): L2ItemInstance {
        return this.lure
    }

    runVitalityTask() : void {
        this.updateVitalityPoints( ConfigManager.vitality.getIntervalPoints(), false, true )
    }

    canRegenerateVitalityPoints() : boolean {
        if ( this.loadState !== PlayerLoadState.Loaded ) {
            return false
        }

        if ( !ConfigManager.vitality.enabled() ) {
            return false
        }

        if ( ConfigManager.vitality.requirePeaceArea() && !this.isInArea( AreaType.Peace ) ) {
            return false
        }

        return this.getVitalityPoints() < VitalityPointsPerLevel.Top
    }

    startVitalityRegeneration() {
        if ( this.canRegenerateVitalityPoints() ) {
            if ( this.vitalityTask ) {
                return
            }

            this.vitalityTask = setInterval( this.runVitalityTask.bind( this ), Math.max( 10000, ConfigManager.vitality.getRecoveryInterval() ) )
            return
        }

        this.stopVitalityTask()
    }

    async addAncientAdena( amount: number, sendMessage: boolean, reason: string = null, referenceId: number = 0 ): Promise<void> {
        if ( sendMessage ) {
            let message = new SystemMessageBuilder( SystemMessageIds.EARNED_S2_S1_S )
                    .addItemNameWithId( ItemTypes.AncientAdena )
                    .addNumber( amount )
                    .getBuffer()

            this.sendOwnedData( message )
        }

        if ( amount > 0 ) {
            await this.inventory.addAncientAdena( amount, referenceId, reason )
        }
    }

    async reduceAncientAdena( amount: number, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        if ( amount > this.getAncientAdena() ) {
            if ( sendMessage ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
            }

            return false
        }

        if ( amount > 0 ) {
            if ( !await this.inventory.reduceAncientAdena( amount, reason ) ) {
                return false
            }

            if ( sendMessage ) {
                if ( amount > 1 ) {
                    let message = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                            .addItemNameWithId( ItemTypes.AncientAdena )
                            .addNumber( amount )
                            .getBuffer()

                    this.sendOwnedData( message )
                } else {
                    let message = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                            .addItemNameWithId( ItemTypes.AncientAdena )
                            .getBuffer()

                    this.sendOwnedData( message )
                }
            }
        }

        return true
    }

    async showQuestMovie( movieId: number ): Promise<void> {
        if ( this.movieId > 0 ) {
            return
        }

        await AIEffectHelper.setNextIntent( this, AIIntent.WAITING )

        this.movieId = movieId
        this.sendOwnedData( ExStartScenePlayer( movieId ) )
    }

    hasCrystallizeSkill(): boolean {
        return this.getSkillLevel( CommonSkillIds.CrystallizeItems ) >= 1
    }

    runWarehouseResetTask(): Promise<void> {
        if ( this.warehouse && this.getActiveWarehouse() === this.warehouse ) {
            return
        }

        return this.clearWarehouse()
    }

    broadcastStatusUpdate() {
        this.debounceBroadcastStatusUpdate()
    }

    sendStopMovingEvent(): void {
        if ( ListenerCache.hasGeneralListener( EventType.PlayerStoppedMoving ) ) {
            let data = EventPoolCache.getData( EventType.PlayerStoppedMoving ) as PlayerStoppedMovingEvent

            data.playerId = this.getObjectId()
            data.x = this.getX()
            data.y = this.getY()
            data.z = this.getZ()
            data.instanceId = this.getInstanceId()

            ListenerCache.sendGeneralEvent( EventType.PlayerStoppedMoving, data )
        }
    }

    isUnderAttack(): boolean {
        return FightStanceCache.hasPlayableStance( this.getObjectId() )
    }

    hasActionOverride( mask: PlayerActionOverride ): boolean {
        return ( this.overrides & mask ) === mask
    }

    canInteract( npc: L2Npc ): boolean {
        if ( npc.isBusy() ) {
            return false
        }

        if ( this.getInstanceId() !== npc.getInstanceId() && this.getInstanceId() !== 0 ) {
            return false
        }

        if ( this.isCastingNow() || this.isCastingSimultaneouslyNow() ) {
            return false
        }

        if ( this.isDead() || this.isFakeDeath() ) {
            return false
        }

        if ( this.isSitting() ) {
            return false
        }

        if ( this.getPrivateStoreType() !== PrivateStoreType.None ) {
            return false
        }

        return this.isInsideRadius( npc, L2NpcValues.interactionDistance, true )
    }

    async broadcastModifiedStats(): Promise<void> {
        let broadcastFullUpdate = this.modifiedStats.has( Stats.MOVE_SPEED )

        if ( broadcastFullUpdate ) {
            this.updateAndBroadcastStatus( 2 )
            this.modifiedStats.clear()
        } else {
            this.updateAndBroadcastStatus( 1 )

            this.broadcastStatusAttributes()
        }

        if ( this.hasSummon() && this.isAffected( EffectFlag.SERVITOR_SHARE ) ) {
            this.getSummon().broadcastStatusUpdate()
        }
    }

    // TODO : move to clan and cache values
    getClanFunctionMultiplier( type: ClanHallFunctionType ): number {
        let clan = this.getClan()
        if ( this.isInArea( AreaType.ClanHall ) && clan && clan.getHideoutId() > 0 ) {
            let area = AreaDiscoveryManager.getEnteredAreaByType( this.getObjectId(), AreaType.ClanHall ) as ClanHallArea
            let residenceId = !area ? -1 : area.getResidenceId()
            let hallId = clan.getHideoutId()

            if ( hallId === residenceId ) {
                let clansHall: ClanHall = clan.getClanHall()
                if ( clansHall ) {
                    let currentFunction = clansHall.getFunction( type )
                    if ( currentFunction ) {
                        return 1 + ( currentFunction.level / 100 )
                    }
                }
            }
        }

        return 1
    }

    // TODO : move to castle and cache values
    getCastleFunctionMultiplier( type: number ): number {
        let clan = this.getClan()
        if ( this.isInArea( AreaType.Castle ) && clan && clan.getCastleId() > 0 ) {
            let area = AreaDiscoveryManager.getEnteredAreaByType( this.getObjectId(), AreaType.Castle ) as CastleArea
            let residenceId = !area ? -1 : area.getResidenceId()
            let castleIndex = clan.getCastleId()

            if ( castleIndex > 0 && castleIndex === residenceId ) {
                let castle: Castle = CastleManager.getCastleById( castleIndex )
                if ( castle ) {
                    let currentFunction = castle.getFunction( type )
                    if ( currentFunction ) {
                        return 1 + ( currentFunction.getLevel() / 100 )
                    }
                }
            }
        }

        return 1
    }

    // TODO : move to fort and cache values
    getFortFunctionMultiplier( type: number ): number {
        let clan = this.getClan()

        if ( this.isInArea( AreaType.Fort ) && clan && clan.getFortId() > 0 ) {
            let area = AreaDiscoveryManager.getEnteredAreaByType( this.getObjectId(), AreaType.FortSiege ) as FortSiegeArea
            let residenceId = !area ? -1 : area.getResidenceId()
            let fortIndex = clan.getFortId()

            if ( fortIndex > 0 && fortIndex === residenceId ) {
                let fort: Fort = FortManager.getFortById( fortIndex )
                if ( fort ) {
                    let currentFunction: FortFunction = fort.getFunction( type )
                    if ( currentFunction ) {
                        return 1 + ( currentFunction.level / 100 )
                    }
                }
            }
        }

        return 1
    }

    getClanHallFunctionLevel( type: ClanHallFunctionType ): number {
        let clan = this.getClan()
        let clanHall = clan ? clan.getClanHall() : null

        if ( clanHall ) {
            let currentFunction = clanHall.getFunction( type )
            if ( currentFunction && currentFunction.level > 0 ) {
                return currentFunction.level
            }
        }

        return 0
    }

    cannotSeeInstance( type: InstanceType ): boolean {
        return this.hiddenInstanceTypes.has( type )
    }

    runClearHiddenInstanceTypes(): void {
        this.hiddenInstanceTypes.clear()
    }

    async runSkillSave(): Promise<void> {
        let promises : Array<Promise<void>> = []

        for ( let [ classIndex, skills ] of this.skillsToStore.entries() ) {
            /*
                Due to async operation, it is possible to insert additional skills
                before database operation completes, thus reset skills first then store.
             */
            this.skillsToStore.delete( classIndex )
            promises.push( DatabaseManager.getCharacterSkills().upsertSkills( this.getObjectId(), classIndex, skills ) )
        }

        await Promise.all( promises )
    }

    getMovementType(): MovementType {
        return this.isInArea( AreaType.Water ) ? MovementType.Swimming : ( this.isFlyingMounted() ? MovementType.Flying : MovementType.Walking )
    }

    prepareAttackByCrossBow( millisToAttack: number ) : Promise<void> {
        this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CROSSBOW_PREPARING_TO_FIRE ) )
        this.sendOwnedData( SetupGauge( this.getObjectId(), SetupGaugeColors.Red, millisToAttack ) )

        this.rangedAttackEndTime = millisToAttack + Date.now()
        return this.reduceArrowCount()
    }

    prepareAttackByBow( millisToAttack: number ): Promise<void> {
        this.sendOwnedData( SetupGauge( this.getObjectId(), SetupGaugeColors.Red, millisToAttack ) )

        this.rangedAttackEndTime = millisToAttack + Date.now()
        return this.reduceArrowCount()
    }

    abortAttack() {
        if ( this.isAttackingNow() || this.isCasting() ) {
            AIEffectHelper.scheduleNextIntent( this, AIIntent.WAITING )
            this.sendOwnedData( ActionFailed() )
        }
    }

    notifyAttackInterrupted( resetAIController: boolean = true ) {
        if ( this.isAttackingNow() ) {
            if ( resetAIController ) {
                AIEffectHelper.scheduleNextIntent( this, AIIntent.WAITING )
            }

            this.sendOwnedData( ActionFailed() )
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACK_FAILED ) )
        }
    }

    getTalismanSlots(): number {
        return this.getStat().getTalismanSlots()
    }

    canBroadcastUpdate() : boolean {
        return this.loadState === PlayerLoadState.EnteredWorld
    }

    isAutoLootEnabled() {
        return !!PlayerVariablesManager.get( this.objectId, CommonVariables.AutoLootEnabled )
    }

    shouldLootItem(): boolean {
        return !!PlayerVariablesManager.get( this.objectId, CommonVariables.AutoLootItemsEnabled )
    }

    shouldLootHerb(): boolean {
        return !!PlayerVariablesManager.get( this.objectId, CommonVariables.AutoLootHerbsEnabled )
    }

    getMainCast() : MagicProcess {
        return this.skillCast
    }

    getSimultaneousCast() : MagicProcess {
        return this.skillCastSimultaneous
    }

    clearMainSkillFlags() : void {
        this.mainSkillFlags.isCtrlPressed = false
        this.mainSkillFlags.isShiftPressed = false
    }

    clearPetSkillFlags() : void {
        this.petSkillFlags.isCtrlPressed = false
        this.petSkillFlags.isShiftPressed = false
    }

    getRangedWeaponUseOutcome() : boolean {
        let weaponItem: L2Weapon = this.getActiveWeaponItem()
        let projectiles = this.getInventory().getPaperdollItem( InventorySlot.LeftHand )

        if ( !projectiles || ( projectiles.isBolt() && weaponItem.isBow() ) || ( projectiles.isArrow() && weaponItem.isCrossBow() ) ) {
            AIEffectHelper.scheduleNextIntent( this, AIIntent.WAITING )
            this.sendOwnedData( ActionFailed() )
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( weaponItem.isBow() ? SystemMessageIds.NOT_ENOUGH_ARROWS : SystemMessageIds.NOT_ENOUGH_BOLTS ) )

            return false
        }

        let currentTime = Date.now()
        if ( this.rangedAttackEndTime <= currentTime ) {
            let mpConsume = weaponItem.getMpConsume()
            if ( weaponItem.getReducedMpConsume() > 0 && Math.random() < weaponItem.getReducedMpConsumeChance() ) {
                mpConsume = weaponItem.getReducedMpConsume()
            }

            mpConsume = Math.floor( this.calculateStat( Stats.BOW_MP_CONSUME_RATE, mpConsume, null, null ) )

            if ( this.getCurrentMp() < mpConsume ) {
                this.scheduleReadyForAttack( 1000 )

                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_MP ) )
                this.sendOwnedData( ActionFailed() )
                return false
            }

            if ( mpConsume > 0 ) {
                this.getStatus().reduceMp( mpConsume )
            }

            return true
        }

        this.scheduleReadyForAttack( this.rangedAttackEndTime - currentTime )
        this.sendOwnedData( ActionFailed() )

        return false
    }

    scheduleReadyForAttack( value: number ) : void {
        if ( this.readyForAttackTimeout ) {
            return
        }

        this.readyForAttackTimeout = setTimeout( () => {
            AIEffectHelper.notifyReadyForAttack( this )
            this.readyForAttackTimeout = null
        }, value )
    }

    runProximityScan() : void {
        if ( this.isTeleporting() ) {
            return
        }

        ProximalDiscoveryManager.findObjects( this )

        this.proximityScanStep++

        /*
            Approximate ratio of scanning vs forgetting objects.
            - assuming minimum scan interval of one second
            - every eight seconds we clear objects that expire (decayed and out of view objects)
         */
        if ( this.proximityScanStep === 8 ) {
            this.proximityScanStep = 0
            ProximalDiscoveryManager.forgetObjects( this )
        }
    }

    hasArrowsEquipped(): boolean {
        let projectiles = this.getInventory().getPaperdollItem( InventorySlot.LeftHand )
        if ( !projectiles ) {
            return false
        }

        return projectiles.isArrow()
    }

    hasBoltsEquipped(): boolean {
        let projectiles = this.getInventory().getPaperdollItem( InventorySlot.LeftHand )
        if ( !projectiles ) {
            return false
        }

        return projectiles.isBolt()
    }

    activateCubicsOnHpDamage() : void {
        if ( this.cubics.size === 0 ) {
            return
        }

        this.cubics.forEach( cubic => {
            if ( cubic.activatedOnHpDamage ) {
                cubic.activate()
            }
        } )
    }

    activateCubicsOnAttack() : void {
        if ( this.cubics.size === 0 ) {
            return
        }

        this.cubics.forEach( cubic => {
            if ( cubic.activatedOnAttack ) {
                cubic.activate()
            }
        } )
    }

    async runAutoSave() : Promise<void> {
        await this.storeMe()

        let summon = this.getSummon()
        if ( summon ) {
            await summon.storeMe()
        }
    }

    stopAutosave() : void {
        if ( this.autoSave ) {
            clearInterval( this.autoSave )
            this.autoSave = null
        }
    }

    private runReceiveFame( amount: number ): void {
        if ( this.isDead() && !ConfigManager.character.fameForDeadPlayers() ) {
            return
        }

        if ( !this.isOnline() && !ConfigManager.customs.offlineFame() ) {
            return
        }

        this.setFame( this.getFame() + amount )

        if ( !this.isOnline() ) {
            return
        }

        let packet = new SystemMessageBuilder( SystemMessageIds.ACQUIRED_S1_REPUTATION_SCORE )
                .addNumber( amount )
                .getBuffer()
        this.sendOwnedData( packet )
        this.sendDebouncedPacket( UserInfo )
    }

    /*
        TODO : review how often this needs to be called, currently it is set to execute every one second
        - problem is in update pvp flag logic, since it sends relation updates in wide area
        - may need to implement relationship caching first
        - consider when player sees another player, relationship is computed, hence why update relationship so often?
          Easiest way is to set broadcast radius to be region size, however that may not be desirable in sieges.
     */
    private runUpdatePvpFlag(): void {
        let timeLeft = this.getPvpFlagLasts() - Date.now()

        /*
            Due to odd nature of pvp flag, it can be renewed at any time while we're waiting for
            current timer execution (a type of debounce with different end times possible)
         */
        if ( timeLeft < 1000 ) {
            this.stopPvPTimer()
            return this.updatePvPFlag( PlayerPvpFlag.None )
        }

        if ( timeLeft < 20000 && this.getPvpFlag() !== PlayerPvpFlag.FlaggedEnding ) {
            this.updatePvPFlag( PlayerPvpFlag.FlaggedEnding )
        }

        this.pvpFlagTimeout.refresh()
    }

    scheduleWearItemRemoval() : void {
        this.wearItemTimeout = setTimeout( this.runRemoveWearItems.bind( this ), ConfigManager.general.getWearDelay() )
    }

    private runRemoveWearItems() {
        this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_LONGER_TRYING_ON ) )
        this.sendDebouncedPacket( UserInfo )
    }

    private runStandUp() {
        if ( this.isOnline() ) {
            this.setIsSitting( false )
            AIEffectHelper.scheduleNextIntent( this, AIIntent.WAITING )
        }
    }

    startDiscoveringObjects() : void {
        if ( this.proximityScan ) {
            return
        }

        ProximalDiscoveryManager.addPlayer( this )
        this.proximityScan = setInterval( this.runProximityScan.bind( this ), Math.min( 1000, ConfigManager.general.getPlayerObjectScanInterval() ) )
    }

    getClient() : GameClient {
        return L2GameClientRegistry.getClientByPlayerId( this.objectId )
    }

    async applyTriggerSkills( target: L2Character, isCrit: boolean ) : Promise<void> {
        let skills : Array<OptionsSkillHolder> = this.triggerSkills.filter( ( holder: OptionsSkillHolder ) : boolean => {
            if ( _.random( 100 ) >= holder.getChance() ) {
                return false
            }

            if ( isCrit ) {
                return holder.getSkillType() === OptionsSkillType.Critical
            }

            return holder.getSkillType() === OptionsSkillType.Attack
        } )

        if ( skills.length > 0 ) {
            await aigle.resolve( skills ).each( ( holder: OptionsSkillHolder ) : Promise<void> => this.makeTriggerCast( holder.getSkill(), target ) )
        }
    }

    async applySkillTriggerSkills( skill: Skill, target: L2Character ) : Promise<void> {
        let skills : Array<OptionsSkillHolder> = this.triggerSkills.filter( ( holder: OptionsSkillHolder ) : boolean => {
            if ( _.random( 100 ) >= holder.getChance() ) {
                return false
            }

            if ( skill.isMagic() ) {
                return holder.getSkillType() === OptionsSkillType.Magic
            }

            return skill.isPhysical() && holder.getSkillType() === OptionsSkillType.Attack
        } )

        if ( skills.length > 0 ) {
            await aigle.resolve( skills ).each( ( holder: OptionsSkillHolder ) : Promise<void> => this.makeTriggerCast( holder.getSkill(), target ) )
        }
    }

    hasTriggerSkills() : boolean {
        return this.triggerSkills.length > 0
    }

    addTriggerSkill( holder: OptionsSkillHolder ) {
        this.triggerSkills.push( holder )
    }

    removeTriggerSkill( holder: OptionsSkillHolder ) {
        _.pull( this.triggerSkills, holder )
    }

    resetReviveProperties() : void {
        this.reviveRequested = 0
        this.revivePower = 0
    }

    onRestartPoint( type: RestartPointType ) : Promise<void> {
        if ( this.deathTeleportTimeout ) {
            return
        }

        let castle: Castle = CastleManager.getCastleByCoordinates( this.getX(), this.getY(), this.getZ() )
        if ( castle && castle.getSiege().isInProgress() ) {
            if ( this.getClan() && castle.getSiege().checkIsAttacker( this.getClan() ) ) {

                let delay = castle.getSiege().getAttackerRespawnDelay()
                this.deathTeleportTimeout = setTimeout( this.runDeathTeleport.bind( this ), delay, type )

                if ( delay > 0 ) {
                    this.sendMessage( `You will respawn in ${GeneralHelper.formatMillis( delay ).join( ', ' )}` )
                }

                return
            }
        }

        return this.runDeathTeleport( type )
    }

    async runDeathTeleport( type: RestartPointType ) : Promise<void> {
        this.deathTeleportTimeout = null

        if ( this.isJailed() ) {
            // TODO : add jail teleport points as data
            return this.onDeathTeleport( -114356, -249645, -2984 )
        }

        if ( type === RestartPointType.Festival ) {
            if ( !this.isGM() && !this.getInventory().haveItemForSelfResurrection() ) {

                return
            }

            let existingItemId = [ 10649, 13300, 13128 ].find( ( itemId: number ) : boolean => {
                return this.getInventory().hasItemWithId( itemId )
            } )

            if ( this.isGM() || ( existingItemId && await this.destroyItemByItemId( existingItemId, 1, false, 'L2PcInstance.runDeathTeleport' ) ) ) {
                this.doReviveWithPercentage( 100 )
                return
            }

            /*
                While teleport to same coordinates does not move player, additional logic is resetting other fields.
             */
            return this.onDeathTeleportLocation( this )
        }

        let clan = this.getClan()

        switch ( type ) {
            case RestartPointType.ClanHall:
                if ( !clan || clan.getHideoutId() === 0 ) {
                    return
                }

                let clanHall: ClanHall = ClanHallManager.getClanHallByOwner( clan )
                if ( clanHall && clanHall.getFunction( ClanHallFunctionType.RestoreXp ) ) {
                    this.restoreExp( clanHall.getFunction( ClanHallFunctionType.RestoreXp ).level )
                }

                return this.onDeathTeleportLocation( getTeleportLocation( this, TeleportWhereType.CLANHALL ) )

            case RestartPointType.Castle:
                if ( !clan || clan.getCastleId() === 0 ) {
                    return
                }

                let castle: Castle = CastleManager.getCastle( this )
                let castleLocation : ILocational

                if ( castle && castle.getSiege().isInProgress() ) {
                    if ( !castle.getSiege().isParticipating( clan ) ) {
                        return
                    }

                    if ( castle.getSiege().checkIsDefender( clan ) ) {
                        castleLocation = getTeleportLocation( this, TeleportWhereType.CASTLE )
                    } else if ( castle.getSiege().checkIsAttacker( clan ) ) {
                        castleLocation = getTeleportLocation( this, TeleportWhereType.TOWN )
                    }

                } else {
                    castleLocation = getTeleportLocation( this, TeleportWhereType.CASTLE )
                }

                let ownedCastle = CastleManager.getCastleByOwner( clan )
                if ( ownedCastle && ownedCastle.getFunction( CastleFunctions.RestoreExpOnDeath ) ) {
                    this.restoreExp( ownedCastle.getFunction( CastleFunctions.RestoreExpOnDeath ).getLevel() )
                }

                return this.onDeathTeleportLocation( castleLocation )

            case RestartPointType.Fortress:
                if ( !clan || clan.getFortId() === 0 ) {
                    return
                }

                let fortClan = FortManager.getFortByOwner( this.getClan() )
                if ( fortClan && fortClan.getFunction( FortFunctionType.RestoreExp ) ) {
                    this.restoreExp( fortClan.getFunction( FortFunctionType.RestoreExp ).level )
                }

                return this.onDeathTeleportLocation( getTeleportLocation( this, TeleportWhereType.FORTRESS ) )

            case RestartPointType.SiegeHQ:
                if ( !clan ) {
                    return
                }

                let siegeClan: L2SiegeClan
                let currentCastle = CastleManager.getCastle( this )
                let currentFort = FortManager.getFort( this )
                let hall: SiegableHall = ClanHallSiegeManager.getNearbyClanHall( this )
                let flag: L2SiegeFlagInstance = TerritoryWarManager.getSiegeFlagForClan( clan )

                if ( currentCastle && currentCastle.getSiege().isInProgress() ) {
                    siegeClan = currentCastle.getSiege().getAttackerClan( clan )
                } else if ( currentFort && currentFort.getSiege().isInProgress() ) {
                    siegeClan = currentFort.getSiege().getAttackerClan( clan )
                } else if ( hall && hall.isInSiege() ) {
                    siegeClan = hall.getSiege().getAttackerClan( clan )
                }

                if ( ( !siegeClan || siegeClan.getFlag().length === 0 ) && !flag ) {
                    if ( hall ) {
                        return this.onDeathTeleportLocation( hall.getSiege().getInnerSpawnLocation( this ) )
                    }

                    return
                }

                return this.onDeathTeleportLocation( getTeleportLocation( this, TeleportWhereType.SIEGEFLAG ) )

            case RestartPointType.Agathion:
                /*
                    TODO: implement me
                    - inspect if agathion is equipped in bracer slot
                    - inspect agathion item skills for resurrection
                 */
                return
        }

        return this.onDeathTeleportLocation( getTeleportLocation( this, TeleportWhereType.TOWN ) )
    }

    onDeathTeleportLocation( location: ILocational ) : Promise<void> {
        if ( !location ) {
            return
        }

        return this.onDeathTeleport( location.getX(), location.getY(), location.getZ(), location.getInstanceId() )
    }

    onDeathTeleport( x: number, y: number, z: number, instanceId: number = 0 ) : Promise<void> {
        this.setInstanceId( instanceId )
        this.setIsIn7sDungeon( false )
        this.setIsPendingRevive( true )

        return this.teleportToLocationCoordinates( x, y, z, this.getHeading(), instanceId )
    }

    sendInventoryUpdate( showWindow: boolean = false ) : void {
        let items : Array<L2ItemInstance> = []
        let agathions : Array<L2ItemInstance> = []
        let questItems : Array<L2ItemInstance> = []

        this.getInventory().getItems().forEach( ( item: L2ItemInstance ) => {
            if ( item.agathion ) {
                agathions.push( item )
            }

            if ( !item.isQuestItem() ) {
                items.push( item )
            } else {
                questItems.push( item )
            }
        } )

        this.getInventory().resetInventoryUpdate()
        this.sendOwnedData( ItemList( items, this.getInventory(), showWindow ) )
        this.sendOwnedData( ExQuestItemList( this, questItems ) )

        if ( agathions.length > 0 ) {
            this.sendOwnedData( ExBRAgathionEnergyInfo( agathions ) )
        }
    }

    setItemDropState( state: ItemDropState ) : void {
        this.itemDropState = state
    }

    setExpPenalty( penalty: number ) : void {
        this.expPenalty = penalty
    }

    hasAreaFlags( type: AreaFlags ) : boolean {
        return this.areaFlags.has( type )
    }

    addAreaFlag( type: AreaFlags ) : void {
        this.areaFlags.add( type )
    }

    removeAreaFlag( type: AreaFlags ) : void {
        this.areaFlags.delete( type )
    }

    engagePvPFlag( duration : number ) : void {
        return this.debounceEngagePvPFlag( duration )
    }

    async onCallSkill( skill: Skill, targets: Array<L2Object> ) : Promise<void> {
        let player: L2PcInstance = this.getActingPlayer()
        if ( player ) {
            let summon = player.getSummon()
            targets.forEach( ( target: L2Object ) => {
                if ( this.getObjectId() === target.getObjectId()
                    || player.getObjectId() === target.getObjectId() ) {
                    return
                }

                if ( skill.getEffectPoint() <= 0 ) {
                    if ( target.getObjectId() === this.getObjectId() && ( target.isPlayable() || target.isTrap() ) ) {
                        if ( summon
                            && summon.getObjectId() !== target.getObjectId()
                            && !this.isTrap()
                            && skill.isBad() ) {
                            player.updatePvPStatusWithTarget( target as L2Character )
                        }

                    } else if ( target.isAttackable() ) {
                        switch ( skill.getId() ) {
                            case 51: // Lure
                            case 511: // Temptation
                                break

                            default:
                                AggroCache.addAggro( target.getObjectId(), this.getObjectId(), 1, 1 )
                                break
                        }
                    }

                    if ( ( target as L2Character ).hasAIController() && !skill.hasEffectType( L2EffectType.Hate ) ) {
                        AIEffectHelper.notifyAttacked( target as L2Character, this )
                    }
                } else {
                    if ( target.isPlayer() ) {
                        if ( !( target.getObjectId() === this.getObjectId() || target.getObjectId() === player.getObjectId() )
                            && ( ( target as L2PcInstance ).hasPvPFlag() || ( ( target as L2PcInstance ).getKarma() > 0 ) ) ) {
                            player.updatePvPStatus()
                        }
                    } else if ( target.isAttackable() ) {
                        player.updatePvPStatus()
                    }
                }
            } )

            if ( targets.length > 1 || !targets.includes( summon ) ) {
                let objects: Array<L2Object> = L2World.getVisibleObjectsByPredicate( player, 1000, ( object: L2Object ): boolean => {
                    return object.isNpc() && !( object as L2Npc ).isDead() && Math.abs( player.getZ() - object.getZ() ) <= 1000
                } )

                let targetIds = targets.map( target => target.getObjectId() )
                objects.forEach( ( object: L2Npc ) => {
                    if ( ListenerCache.hasNpcTemplateListeners( object.getId(), EventType.NpcSeeSkill ) ) {
                        let eventData = EventPoolCache.getData( EventType.NpcSeeSkill ) as NpcSeeSkillEvent

                        eventData.originatorId = this.getObjectId()
                        eventData.targetIds = targetIds
                        eventData.playerId = player.getObjectId()
                        eventData.receiverId = object.getObjectId()
                        eventData.receiverNpcId = object.getId()
                        eventData.receiverIsChampion = object.isChampion()
                        eventData.skillId = skill.getId()
                        eventData.skillLevel = skill.getLevel()

                        ListenerCache.sendNpcTemplateEvent( object.getId(), EventType.NpcSeeSkill, eventData, skill.getId() )
                    }

                    if ( object.isAttackable() ) {
                        let skillEffectPoint = skill.getEffectPoint()

                        if ( skillEffectPoint > 0 ) {
                            let npcTargetId: number = ( object as L2Attackable ).getTargetId()
                            let originalCaster: L2Character = this.isSummon() ? this.getSummon() : player

                            targets.forEach( ( currentTarget: L2Character ) => {
                                if ( npcTargetId === currentTarget.getObjectId()
                                    || object.getObjectId() === currentTarget.getObjectId() ) {

                                    AggroCache.addAggro( object.getObjectId(), originalCaster.getObjectId(), 0, ( skillEffectPoint * 150 ) / ( ( object as L2Attackable ).getLevel() + 7 ) )
                                }
                            } )
                        }
                    }
                } )
            }
        }
    }

    isInCombat(): boolean {
        return FightStanceCache.hasPlayableStance( this.getObjectId() )
    }

    hasLoadedSkills() : boolean {
        return this.canBroadcastUpdate()
    }
}