import { L2Npc } from '../L2Npc'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { ActionFailed } from '../../../packets/send/ActionFailed'

export class L2TerrainObjectInstance extends L2Npc {
    constructor( template: L2NpcTemplate ) {
        super( template )

        this.instanceType = InstanceType.L2TerrainObjectInstance
    }

    static fromTemplate( template: L2NpcTemplate ) : L2TerrainObjectInstance {
        return new L2TerrainObjectInstance( template )
    }

    onInteraction( player: L2PcInstance, interact: boolean = true ): Promise<void> {
        player.sendOwnedData( ActionFailed() )
        return
    }

    async onActionShift( player: L2PcInstance, interact: boolean = true ): Promise<void> {
        if ( player.isGM() ) {
            return super.onActionShift( player, interact )
        }

        player.sendOwnedData( ActionFailed() )
    }
}