import { L2MonsterInstance } from './L2MonsterInstance'
import { ArenaParticipantsHolder } from '../../ArenaParticipantsHolder'
import { L2PcInstance } from './L2PcInstance'
import { BlockCheckerEngine } from '../../entity/BlockCheckerEngine'
import { NpcInfo } from '../../../packets/send/NpcInfo'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { ExCubeGameChangePoints } from '../../../packets/send/ExCubeGameChangePoints'
import { ExCubeGameExtendedChangePoints } from '../../../packets/send/ExCubeGameExtendedChangePoints'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { ItemManagerCache } from '../../../cache/ItemManagerCache'
import { L2Character } from '../L2Character'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import _ from 'lodash'

export class L2BlockInstance extends L2MonsterInstance {
    colorEffect: number

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2BlockInstance
    }

    getColorEffect(): number {
        return this.colorEffect
    }

    async changeColor( attacker: L2PcInstance, holder: ArenaParticipantsHolder, team: number ) {
        let engine: BlockCheckerEngine = holder.getEvent()

        this.colorEffect = this.colorEffect === 0x53 ? 0 : 0x53

        this.increaseTeamPointsAndSend( attacker, team, engine )
        BroadcastHelper.dataBasedOnVisibility( this, NpcInfo( this.getObjectId(), attacker.getObjectId() ) )

        let random = _.random( 100 )
        if ( ( random > 69 ) && ( random <= 84 ) ) {
            this.dropItemForAttacker( 13787, engine, attacker )
        } else if ( random > 84 ) {
            this.dropItemForAttacker( 13788, engine, attacker )
        }
    }

    increaseTeamPointsAndSend( player: L2PcInstance, team: number, engine: BlockCheckerEngine ) {
        engine.increasePlayerPoints( player, team )

        let timeLeft = Math.floor( ( engine.getStartedTime() - Date.now() ) / 1000 )
        let isRed = engine.getHolder().getRedPlayers().includes( player.getObjectId() )

        let changePoints: Buffer = ExCubeGameChangePoints( timeLeft, engine.getBluePoints(), engine.getRedPoints() )
        let secretPoints: Buffer = ExCubeGameExtendedChangePoints( timeLeft, engine.getBluePoints(), engine.getRedPoints(), isRed, player.getObjectId(), engine.getPlayerPoints( player, isRed ) )

        engine.getHolder().broadCastPacketToTeam( changePoints )
        engine.getHolder().broadCastPacketToTeam( secretPoints )
    }

    dropItemForAttacker( itemId: number, engine: BlockCheckerEngine, attacker: L2PcInstance ) {
        let item: L2ItemInstance = ItemManagerCache.createLootItem( itemId, 'L2BlockInstance.dropItemForAttacker', 1, attacker.getObjectId(), this )

        let x = this.getX() + _.random( 50 )
        let y = this.getY() + _.random( 50 )
        let z = this.getZ()

        item.dropMe( attacker.getObjectId(), x, y, z, this.getInstanceId(), true )

        engine.addNewDrop( item )
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        if ( attacker.isPlayer() ) {
            return attacker.getActingPlayer() && attacker.getActingPlayer().getBlockCheckerArena() > -1
        }

        return true
    }
}