import { L2Attackable } from '../L2Attackable'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2World } from '../../../L2World'
import { L2Character } from '../L2Character'
import { Skill } from '../../Skill'
import { AggroCache } from '../../../cache/AggroCache'

export class L2MonsterInstance extends L2Attackable {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2MonsterInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2MonsterInstance {
        return new L2MonsterInstance( template )
    }

    hasRaidCurse() : boolean {
        let leader = this.getLeader()
        return this.isRaidMinion() && leader ? leader.hasRaidCurse() : super.hasRaidCurse()
    }

    hasMinions() : boolean {
        return this.npcSpawn && this.npcSpawn.hasSpawnedMinions()
    }

    isAggressive() {
        return this.getTemplate().isAggressive && !this.isEventMob()
    }

    isMonster(): boolean {
        return true
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        return !this.isEventMob() && ( !attacker || !attacker.isMonster() )
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( !await super.doDie( killer ) ) {
            return false
        }

        if ( this.npcSpawn && this.npcSpawn.hasSpawnedMinions() && this.isRaid() ) {
            this.npcSpawn.removeMinions( this )
        }

        return true
    }

    async onTeleported() : Promise<void> {
        await super.onTeleported()

        if ( this.npcSpawn && this.npcSpawn.hasSpawnedMinions() ) {
            return this.npcSpawn.onMasterTeleported( this )
        }
    }

    hasRandomWalk(): boolean {
        return !this.isMinion() && super.hasRandomWalk()
    }

    async reduceCurrentHp( damage: number, attacker: L2Character, skill: Skill, isAwake: boolean = true, isDOT: boolean = false ): Promise<void> {
        await super.reduceCurrentHp( damage, attacker, skill, isAwake, isDOT )

        if ( !this.hasMinions() || !attacker ) {
            return
        }

        return this.onAssist( this, attacker )
    }

    onAssist( caller: L2Character, attacker: L2Character ) {
        let master = this.getLeader()
        if ( master && !master.isAlikeDead() && !master.isInCombat() ) {
            AggroCache.addAggro( master.getObjectId(), attacker.getObjectId(), 0, 1 )
        }

        let callerIsMaster = caller === this
        let aggroAmount = callerIsMaster ? 10 : 1
        if ( master && master.isRaid() ) {
            aggroAmount *= 10
        }

        this.getMinions().forEach( ( objectId: number ) => {
            let minion = L2World.getObjectById( objectId ) as L2MonsterInstance
            if ( minion && !minion.isDead() && ( callerIsMaster || !minion.isInCombat() ) ) {
                AggroCache.addAggro( minion.getObjectId(), attacker.getObjectId(), 0, aggroAmount )
            }
        } )
    }

    getMinions() : Set<number> {
        return this.npcSpawn.getMinionIds( this )
    }

    hasCastleInfo(): boolean {
        return false
    }
}