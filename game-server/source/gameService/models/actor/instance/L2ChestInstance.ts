import { L2MonsterInstance } from './L2MonsterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Character } from '../L2Character'
import { DataManager } from '../../../../data/manager'
import _ from 'lodash'

const specialDrops: { [ templateId: number ]: number } = {
    18287: 21671,
    18288: 21671,

    18289: 21694,
    18290: 21694,

    18291: 21717,
    18292: 21717,

    18293: 21740,
    18294: 21740,

    18295: 21763,
    18296: 21763,

    18297: 21786,
    18298: 21786,
}

export class L2ChestInstance extends L2MonsterInstance {
    useDropTemplate: boolean = false

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2ChestInstance
    }

    onSpawn() {
        super.onSpawn()

        this.useDropTemplate = false
        this.setMustRewardExpSp( true )
    }

    setSpecialDrop() {
        this.useDropTemplate = true
    }

    performItemDrop( template: L2NpcTemplate, character: L2Character ) {
        return super.performItemDrop( DataManager.getNpcData().getTemplate( this.getItemDropTemplateId() ), character )
    }

    isMovementDisabled() {
        return true
    }

    hasRandomAnimation(): boolean {
        return false
    }

    static fromTemplate( template: L2NpcTemplate ): L2ChestInstance {
        return new L2ChestInstance( template )
    }

    getItemDropTemplateId(): number {
        let id = this.getTemplate().getId()

        if ( this.useDropTemplate ) {
            return id
        }

        if ( id >= 18265 && id <= 18286 ) {
            return id + 3536
        }

        return _.get( specialDrops, id, id )
    }

    isMoveCapable(): boolean {
        return false
    }

    isChest(): boolean {
        return true
    }

    hasRandomWalk(): boolean {
        return false
    }
}