import { L2Npc } from '../L2Npc'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../../data/manager'
import { TeleportRestriction } from '../../../enums/TeleportRestriction'

export class L2TeleporterInstance extends L2Npc {
    constructor( template: L2NpcTemplate ) {
        super( template )

        this.instanceType = InstanceType.L2TeleporterInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2TeleporterInstance {
        return new L2TeleporterInstance( template )
    }

    getHtmlPath( npcId: number, value: number ): string {
        let fileName: string = npcId.toString()

        if ( value !== 0 ) {
            fileName = `${ fileName }-${ value }`
        }

        return 'data/html/teleporter/' + fileName + '.htm'
    }

    showChatWindowDefault( player: L2PcInstance ): void {
        let restriction = this.getTeleportRestriction( player )
        if ( restriction === TeleportRestriction.None ) {
            return this.showChatWindow( player, 0 )
        }

        let htmlPath = this.getRestrictionHtmlPath( restriction )
        let html: string = DataManager.getHtmlData().getItem( htmlPath )
                                      .replace( /%npcname%/g, this.getName() )

        player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), this.getObjectId() ) )
    }

    getTeleportRestriction( player: L2PcInstance ): TeleportRestriction {
        if ( !this.isInCastle() ) {
            return TeleportRestriction.None
        }

        let castle = this.getCastle()
        if ( !castle ) {
            return TeleportRestriction.None
        }

        if ( castle.getSiege().isInProgress() ) {
            return TeleportRestriction.Busy
        }

        if ( player.getClanId() > 0 && castle.getOwnerId() === player.getClanId() ) {
            return TeleportRestriction.OnlyOwner
        }

        return TeleportRestriction.AccessDisabled
    }

    getRestrictionHtmlPath( restriction: TeleportRestriction ): string {
        switch ( restriction ) {
            case TeleportRestriction.Busy:
                return 'data/html/teleporter/castleteleporter-busy.htm'

            case TeleportRestriction.OnlyOwner:
                return this.getHtmlPath( this.getId(), 0 )
        }

        return 'data/html/teleporter/castleteleporter-no.htm'
    }

    isInCastle() : boolean {
        if ( this.isInTown ) {
            return false
        }

        let castle = this.getCastle()
        if ( !castle ) {
            return false
        }

        return castle.isInSiegeArea( this.getX(), this.getY(), this.getZ() )
    }
}