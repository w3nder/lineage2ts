import { L2Tower } from '../L2Tower'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2FlameTowerInstance extends L2Tower {
    upgradeLevel: number = 0
    zoneList: Array<number> = []

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2FlameTowerInstance
    }
}