import { L2Summon } from '../L2Summon'
import { L2PetData } from '../../L2PetData'
import { L2PetLevelData } from '../../L2PetLevelData'
import { DataManager } from '../../../../data/manager'
import { PetStats } from '../stat/PetStats'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { L2PcInstance } from './L2PcInstance'
import { InstanceType } from '../../../enums/InstanceType'
import { BaseStats } from '../../stats/BaseStats'
import { ConfigManager } from '../../../../config/ConfigManager'
import { Stats } from '../../stats/Stats'
import { L2World } from '../../../L2World'
import { DatabaseManager } from '../../../../database/manager'
import { CharacterEffectsCache } from '../../../cache/CharacterEffectsCache'
import { L2Object } from '../../L2Object'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { PacketDispatcher } from '../../../PacketDispatcher'
import { ExChangeNpcState } from '../../../packets/send/ExChangeNpcState'
import { ItemHandlerMethod } from '../../../handler/ItemHandler'
import { Skill } from '../../Skill'
import { L2Character } from '../L2Character'
import { StatusUpdate, StatusUpdateProperty } from '../../../packets/send/builder/StatusUpdate'
import { SocialAction, SocialActionType } from '../../../packets/send/SocialAction'
import { DecayTaskManager } from '../../../taskmanager/DecayTaskManager'
import { ItemLocation } from '../../../enums/ItemLocation'
import { L2Weapon } from '../../items/L2Weapon'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { PetInventory } from '../../itemcontainer/PetInventory'
import { InventorySlot, ItemTypes } from '../../../values/InventoryValues'
import _ from 'lodash'
import { MountTypeHelper } from '../../../enums/MountType'
import { SkillCache } from '../../../cache/SkillCache'
import { L2NpcValues } from '../../../values/L2NpcValues'
import { CharacterSummonCache } from '../../../cache/CharacterSummonCache'
import { PetInfoAnimation } from '../../../packets/send/PetInfo'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { FortSiegeManager } from '../../../instancemanager/FortSiegeManager'
import { CursedWeaponManager } from '../../../instancemanager/CursedWeaponManager'
import { PartyDistributionType } from '../../../enums/PartyDistributionType'
import { ItemsOnGroundManager } from '../../../cache/ItemsOnGroundManager'
import { ItemManagerCache } from '../../../cache/ItemManagerCache'
import { PetNameManager } from '../../../cache/PetNameManager'
import { InventoryUpdateStatus } from '../../../enums/InventoryUpdateStatus'
import { AreaType } from '../../areas/AreaType'
import Timeout = NodeJS.Timeout
import { L2SummonType } from '../../../enums/L2SummonType'
import { L2ItemSlots } from '../../../enums/L2ItemSlots'

export class L2PetInstance extends L2Summon {
    currentFeed: number
    inventory: PetInventory
    controlObjectId: number
    mountable: boolean
    feedTask: Timeout
    lastFeedItemId: number = 0

    /** The Experience before the last Death Penalty */
    expBeforeDeath: number
    currentWeightPenalty: number
    hungerLimit: number

    constructor( template: L2NpcTemplate, owner: L2PcInstance, control: L2ItemInstance, level: number = -1 ) {
        super( template, owner )

        if ( level === -1 ) {
            level = template.getDisplayId() === L2NpcValues.sinEaterNpcId ? owner.getLevel() : template.getLevel()
        }

        this.instanceType = InstanceType.L2PetInstance
        this.controlObjectId = control.getObjectId()
        this.getStat().level = Math.max( level, this.getMinimumLevel() )

        this.inventory = new PetInventory( this.getObjectId() )
        this.mountable = MountTypeHelper.isMountable( template.getId() )
        this.computeHungerLimit()
    }

    static fromTemplate( template: L2NpcTemplate, owner: L2PcInstance, item: L2ItemInstance ): L2PetInstance {
        let level = template.getDisplayId() === L2NpcValues.sinEaterNpcId ? owner.getLevel() : template.getLevel()
        return new L2PetInstance( template, owner, item, level )
    }

    async destroyItemByItemId( itemId: number, count: number, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        let item: L2ItemInstance = await this.inventory.destroyItemByItemId( itemId, count, this.getOwnerId(), reason )

        if ( !item ) {
            if ( sendMessage ) {
                PacketDispatcher.sendOwnedData( this.getOwnerId(), SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
            }
            return false
        }

        if ( sendMessage ) {
            if ( count > 1 ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                        .addItemNameWithId( item.getId() )
                        .addNumber( count )
                        .getBuffer()

                PacketDispatcher.sendOwnedData( this.getOwnerId(), packet )
            } else {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                        .addItemNameWithId( item.getId() )
                        .getBuffer()

                PacketDispatcher.sendOwnedData( this.getOwnerId(), packet )
            }
        }

        return true
    }

    getMaxLoad() {
        return Math.floor( this.calculateStat( Stats.WEIGHT_LIMIT, Math.floor( BaseStats.CON( this ) * 34500 * ConfigManager.character.getWeightLimit() ), this, null ) )
    }

    getMinimumLevel(): number {
        return DataManager.getPetData().getPetMinLevel( this.getTemplate().getId() )
    }

    getMoveSpeed(): number {
        if ( this.isInArea( AreaType.Water ) ) {
            return this.isRunning() ? this.getSwimRunSpeed() : this.getSwimWalkSpeed()
        }

        return this.isRunning() ? this.getRunSpeed() : this.getWalkSpeed()
    }

    getRunSpeed(): number {
        return super.getRunSpeed() * ( this.isUncontrollable() ? 0.5 : 1 )
    }

    getArmor(): number {
        let weapon: L2ItemInstance = this.getInventory().getPaperdollItem( InventorySlot.Chest )
        if ( weapon != null ) {
            return weapon.getId()
        }
        return 0
    }

    getControlObjectId(): number {
        return this.controlObjectId
    }

    getExpForNextLevel(): number {
        return this.getStat().getExpForLevel( this.getLevel() + 1 )
    }

    getExpForThisLevel(): number {
        return this.getStat().getExpForLevel( this.getLevel() )
    }

    getSoulShotsPerHit(): number {
        return this.getPetLevelData().getPetSoulShot()
    }

    getSpiritShotsPerHit(): number {
        return this.getPetLevelData().getPetSpiritShot()
    }

    getWeapon() {
        let weapon: L2ItemInstance = this.getInventory().getPaperdollItem( InventorySlot.RightHand )
        if ( weapon != null ) {
            return weapon.getId()
        }

        return 0
    }

    isMountable(): boolean {
        return this.mountable
    }

    async unSummon( player: L2PcInstance ): Promise<void> {
        this.stopFeeding()
        this.stopHpMpRegeneration()
        await super.unSummon( player )

        if ( !this.isDead() ) {
            if ( this.getInventory() ) {
                await this.getInventory().deleteMe()
            }

            L2World.removePet( this.getOwnerId() )
            CharacterSummonCache.removeRestoredPet( this.getOwnerId() )
        }
    }

    getControlItem(): L2ItemInstance {
        return this.getOwner().getInventory().getItemByObjectId( this.controlObjectId )
    }

    getCurrentFeed(): number {
        return this.currentFeed
    }

    getExp(): number {
        return this.getStat().getExp()
    }

    getStat(): PetStats {
        return super.getStat() as PetStats
    }

    createStats(): PetStats {
        return new PetStats( this )
    }

    getWalkSpeed(): number {
        return super.getWalkSpeed() * ( this.isUncontrollable() ? 0.5 : 1 )
    }

    async restoreEffects(): Promise<void> {
        return CharacterEffectsCache.restorePetEffects( this )
    }

    async storeEffect( store: boolean ): Promise<void> {
        if ( !ConfigManager.character.summonStoreSkillCooltime() ) {
            return
        }

        return CharacterEffectsCache.updatePetEffects( this, store )
    }

    async storeMe(): Promise<void> {
        if ( this.getControlObjectId() === 0 ) {
            return
        }

        await DatabaseManager.getPets().updatePet( this )

        let item: L2ItemInstance = this.getControlItem()
        if ( item && item.getEnchantLevel() !== this.getStat().getLevel() ) {
            await item.setEnchantLevel( this.getStat().getLevel() )
            item.scheduleDatabaseUpdate()
        }

        CharacterSummonCache.removeRestoredPet( this.owner )
    }

    getFeedConsume() {
        if ( this.isAttackingNow() ) {
            return this.getPetLevelData().getPetFeedBattle()
        }

        return this.getPetLevelData().getPetFeedNormal()
    }

    getMaxFeed(): number {
        return this.getStat().getMaxFeed()
    }

    getPetData(): L2PetData {
        return DataManager.getPetData().getPetDataByNpcId( this.getTemplate().getId() )
    }

    getPetLevelData(): L2PetLevelData {
        return DataManager.getPetData().getPetLevelData( this.getTemplate().getId(), this.getStat().getLevel() )
    }

    isHungry() {
        return this.getCurrentFeed() < this.hungerLimit
    }

    isPet(): boolean {
        return true
    }

    isUncontrollable() {
        return this.getCurrentFeed() <= 0
    }

    async runFeedTask(): Promise<void> {
        let owner: L2PcInstance = this.getOwner()
        if ( !owner || !owner.hasSummon() || ( owner.getSummon().getObjectId() !== this.getObjectId() ) ) {
            this.stopFeeding()
            return
        }

        if ( this.getCurrentFeed() > this.getFeedConsume() ) {
            this.setCurrentFeed( this.getCurrentFeed() - this.getFeedConsume() )
        } else {
            this.setCurrentFeed( 0 )
        }

        this.broadcastStatusUpdate()

        let foodIds: Array<number> = this.getPetData().getFood()
        if ( !foodIds || foodIds.length === 0 ) {
            if ( this.isUncontrollable() ) {
                if ( this.getTemplate().getId() === 16050 ) { // Owl Monk pet
                    owner.setPkKills( Math.max( 0, owner.getPkKills() - _.random( 1, 6 ) ) )
                }

                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_HELPER_PET_LEAVING ) )
                return this.deleteMe()
            }

            if ( this.isHungry() ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THERE_NOT_MUCH_TIME_REMAINING_UNTIL_HELPER_LEAVES ) )
            }

            return
        }

        let food: L2ItemInstance = this.lastFeedItemId > 0 ? this.getInventory().getItemByItemId( this.lastFeedItemId ) : null

        if ( !food ) {
            foodIds.some( ( currentId: number ) => {
                food = this.getInventory().getItemByItemId( currentId )
                return !!food
            } )
        }

        if ( food && this.isHungry() && food.isEtcItem() ) {
            this.lastFeedItemId = food.getId()
            let handler: ItemHandlerMethod = food.getEtcItem().getItemHandler()
            if ( handler ) {
                await handler( this, food, true )
                let packet = new SystemMessageBuilder( SystemMessageIds.PET_TOOK_S1_BECAUSE_HE_WAS_HUNGRY )
                        .addItemNameWithId( food.getId() )
                        .getBuffer()
                this.sendOwnedData( packet )
            }
        }

        if ( this.isUncontrollable() ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_PET_IS_STARVING_AND_WILL_NOT_OBEY_UNTIL_IT_GETS_ITS_FOOD_FEED_YOUR_PET ) )
        }
    }

    setCurrentFeed( value: number ) {
        if ( value <= 0 ) {
            PacketDispatcher.sendOwnedData( this.getOwnerId(), ExChangeNpcState( this.objectId, 0x64 ) )
        } else if ( this.currentFeed <= 0 ) {
            PacketDispatcher.sendOwnedData( this.getOwnerId(), ExChangeNpcState( this.objectId, 0x65 ) )

        }

        this.currentFeed = Math.min( value, this.getMaxFeed() )
    }

    setExp( value: number ) {
        this.getStat().setExp( value )
    }

    setSp( value: number ) {
        this.getStat().setSp( value )
    }

    startFeeding() {
        this.stopFeeding()

        let interval: number = this.getFeedInterval()
        if ( !this.isDead() && ( this.getOwner().getSummon().objectId === this.objectId ) && interval > 0 ) {
            this.feedTask = setInterval( this.runFeedTask.bind( this ), interval )
        }
    }

    getFeedInterval(): number {
        return Math.max( 0, ConfigManager.tuning.getPetFeedInterval() )
    }

    stopFeeding(): void {
        if ( this.feedTask ) {
            clearInterval( this.feedTask )
            this.feedTask = null
        }
    }

    getInventoryLimit(): number {
        return ConfigManager.npc.getMaximumSlotsForPet()
    }

    getCriticalHit( target: L2Character, skill: Skill ): number {
        return this.getStat().getCriticalHit( target, skill )
    }

    async onLevelChange( isIncreased: boolean ): Promise<void> {
        if ( isIncreased ) {
            if ( ConfigManager.tuning.getLevelupRestoreHP() > 0 ) {
                let value: number = Math.floor( this.getMaxHp() * ConfigManager.tuning.getLevelupRestoreHP() )
                this.setCurrentHp( Math.min( this.getMaxHp(), this.getCurrentHp() + value ) )
            }

            if ( ConfigManager.tuning.getLevelupRestoreMP() > 0 ) {
                let value = Math.floor( this.getMaxMp() * ConfigManager.tuning.getLevelupRestoreMP() )
                this.setCurrentMp( Math.min( this.getMaxMp(), this.getCurrentMp() + value ) )
            }

            BroadcastHelper.dataBasedOnVisibility( this, SocialAction( this.getObjectId(), SocialActionType.LevelUp ) )
        }

        await this.updateAndBroadcastStatus( PetInfoAnimation.StatUpgrade )
        this.broadcastStatusUpdate()

        let statusPacket = new StatusUpdate( this.getOwnerId() )
                .addAttribute( StatusUpdateProperty.Level, this.getLevel() )
                .addAttribute( StatusUpdateProperty.MaxHP, this.getMaxHp() )
                .addAttribute( StatusUpdateProperty.MaxMP, this.getMaxMp() )
                .getBuffer()

        PacketDispatcher.sendOwnedData( this.owner, statusPacket )

        let item = this.getControlItem()
        if ( item ) {
            await item.setEnchantLevel( this.getLevel() )
        }

        this.computeHungerLimit()
    }

    canEatFoodId( itemId: number ): boolean {
        return this.getPetData().getFood().includes( itemId )
    }

    getSummonType(): L2SummonType {
        return L2SummonType.Pet
    }

    getExpForLevel( level: number ): number {
        return this.getStat().getExpForLevel( level )
    }

    addDirectXp( value: number ): Promise<boolean> {
        return this.getStat().addExp( value )
    }

    async destroyControlItem( player: L2PcInstance, isUpgraded: boolean ): Promise<void> {
        L2World.removePet( player.getObjectId() )

        let removedItem: L2ItemInstance = await player.getInventory().destroyItemByObjectId( this.getControlObjectId(), 1, 0, 'L2PetInstance.destroyControlItem' )
        if ( removedItem ) {
            if ( !isUpgraded ) {
                let message = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                        .addItemInstanceName( removedItem )
                        .getBuffer()
                player.sendOwnedData( message )
            }

            player.broadcastUserInfo()

            L2World.removeObject( removedItem )
        }

        return DatabaseManager.getPets().deleteByControlId( this.getControlObjectId() )
    }

    getLevel(): number {
        return this.getStat().getLevel()
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        let owner = this.getOwner()
        if ( owner && !owner.isInDuel() && ( !this.isInArea( AreaType.PVP ) || this.isInSiegeArea() ) ) {
            await this.applyDeathPenalty()
        }

        if ( !await super.doDie( killer ) ) {
            return false
        }

        this.stopFeeding()
        this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MAKE_SURE_YOU_RESSURECT_YOUR_PET_WITHIN_24_HOURS ) )
        DecayTaskManager.add( this )

        return true
    }

    applyDeathPenalty() : Promise<void> {
        let currentLevel = this.getStat().getLevel()
        let percentLost = -0.07 * currentLevel + 6.5
        let lostExp = Math.round( ( ( this.getStat().getExpForLevel( currentLevel + 1 ) - this.getStat().getExpForLevel( currentLevel ) ) * percentLost ) / 100 )

        this.expBeforeDeath = this.getStat().getExp()
        return this.getStat().removeExp( lostExp )
    }

    getInventory(): PetInventory {
        return this.inventory
    }

    getActiveWeaponInstance(): L2ItemInstance {
        return this.getInventory().getItems().find( ( item: L2ItemInstance ) => {
            return item.getItemLocation() === ItemLocation.PET_EQUIP && item.getItem().getBodyPart() === L2ItemSlots.RightHand
        } )
    }

    getActiveWeaponItem(): L2Weapon {
        let weapon: L2ItemInstance = this.getActiveWeaponInstance()
        return weapon ? weapon.getItem() as L2Weapon : null
    }

    getSecondaryWeaponInstance(): L2ItemInstance {
        return null
    }

    getSecondaryWeaponItem(): L2Weapon {
        return null
    }

    async updateAndBroadcastStatus( value: PetInfoAnimation ): Promise<void> {
        let maxLoad = this.getMaxLoad()

        if ( maxLoad > 0 ) {
            let weightDifference = ( ( this.getCurrentLoad() - this.getBonusWeightPenalty() ) * 1000 ) / maxLoad
            let penaltyValue

            if ( ( weightDifference < 500 ) || this.getOwner().getWeightlessMode() ) {
                penaltyValue = 0
            } else if ( weightDifference < 666 ) {
                penaltyValue = 1
            } else if ( weightDifference < 800 ) {
                penaltyValue = 2
            } else if ( weightDifference < 1000 ) {
                penaltyValue = 3
            } else {
                penaltyValue = 4
            }

            if ( this.currentWeightPenalty !== penaltyValue ) {
                this.currentWeightPenalty = penaltyValue
                if ( penaltyValue > 0 ) {
                    await this.addSkill( SkillCache.getSkill( 4270, penaltyValue ) )
                    this.setIsOverloaded( this.getCurrentLoad() >= maxLoad )
                } else {
                    await this.removeSkill( this.getKnownSkill( 4270 ), true )
                    this.setIsOverloaded( false )
                }
            }
        }

        return super.updateAndBroadcastStatus( value )
    }

    getBonusWeightPenalty(): number {
        return this.calculateStat( Stats.WEIGHT_PENALTY, 1, this, null )
    }

    setName( name: string, force: boolean = false ) {
        let controlItem: L2ItemInstance = this.getControlItem()
        if ( controlItem && ( force || controlItem.getCustomType2() === ( name ? 0 : 1 ) ) ) {
            let type = name ? 1 : 0
            controlItem.setCustomType2( type )
            this.getInventory().markItem( controlItem, InventoryUpdateStatus.Modified )
        }

        this.name = name
    }

    async doPickupItem( object: L2Object ): Promise<void> {
        if ( this.isDead() || !object || !object.isItem() || !object.isVisible() ) {
            return this.sendOwnedData( ActionFailed() )
        }

        if ( CursedWeaponManager.isCursedItem( object.getId() ) ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.FAILED_TO_PICKUP_S1 )
                    .addItemNameWithId( object.getId() )
                    .getBuffer()

            this.sendOwnedData( packet )
            return
        }

        if ( FortSiegeManager.isCombatFlag( object.getId() ) ) {
            return
        }

        let item = object as L2ItemInstance
        if ( !this.getInventory().validateCapacityForItem( item ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOUR_PET_CANNOT_CARRY_ANY_MORE_ITEMS ) )
            return this.sendOwnedData( ActionFailed() )
        }

        let player = this.getOwner()
        let party = this.getParty()
        if ( !item.canBePickedUpBy( player ) || ( party && party.getDistributionType() !== PartyDistributionType.FindersKeepers ) ) {
            let message: SystemMessageBuilder

            if ( item.getId() === ItemTypes.Adena ) {
                message = new SystemMessageBuilder( SystemMessageIds.FAILED_TO_PICKUP_S1_ADENA )
                        .addNumber( item.getCount() )
            } else if ( item.getCount() > 1 ) {
                message = new SystemMessageBuilder( SystemMessageIds.FAILED_TO_PICKUP_S2_S1_S )
                        .addItemInstanceName( item )
                        .addNumber( item.getCount() )
            } else {
                message = new SystemMessageBuilder( SystemMessageIds.FAILED_TO_PICKUP_S1 )
                        .addItemInstanceName( item )
            }

            this.sendOwnedData( ActionFailed() )
            this.sendOwnedData( message.getBuffer() )
            return
        }

        ItemsOnGroundManager.removeObject( item )
        await item.pickupMeBy( this )

        if ( item.getItem().hasExImmediateEffect() && item.isEtcItem() ) {
            let handler: ItemHandlerMethod = item.getEtcItem().getItemHandler()
            if ( handler ) {
                await handler( this, item, false )
            }

            this.broadcastStatusUpdate()
            return ItemManagerCache.destroyItem( item )
        }

        let packet: SystemMessageBuilder
        if ( item.getId() === ItemTypes.Adena ) {
            packet = new SystemMessageBuilder( SystemMessageIds.PET_PICKED_S1_ADENA )
                    .addNumber( item.getCount() )
        } else if ( item.getEnchantLevel() > 0 ) {
            packet = new SystemMessageBuilder( SystemMessageIds.PET_PICKED_S1_S2 )
                    .addNumber( item.getEnchantLevel() )
                    .addItemInstanceName( item )
        } else if ( item.getCount() > 1 ) {
            packet = new SystemMessageBuilder( SystemMessageIds.PET_PICKED_S2_S1_S )
                    .addNumber( item.getCount() )
                    .addItemInstanceName( item )
        } else {
            packet = new SystemMessageBuilder( SystemMessageIds.PET_PICKED_S1 )
                    .addItemInstanceName( item )
        }

        this.sendOwnedData( packet.getBuffer() )

        if ( party ) {
            return party.distributeExistingItem( player, item )
        }

        await this.getInventory().addExistingItem( item, 'L2PetInstance.doPickupItem' )
    }

    addLevel( amount: number ): Promise<void> {
        if ( ( this.getLevel() + amount ) > this.getStat().getMaxLevel() ) {
            return
        }

        let levelIncreased = this.getStat().addLevel( amount )
        return this.onLevelChange( levelIncreased )
    }

    computeHungerLimit(): void {
        this.hungerLimit = ( this.getPetData().getHungryLimit() / 100 ) * this.getPetLevelData().getPetMaxFeed()
    }

    async attemptNameChange( name: string, player: L2PcInstance, force: boolean = false ): Promise<void> {
        if ( !name && !force ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_NAME_TRY_AGAIN ) )
            return
        }

        if ( name.length > 16 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NAMING_CHARNAME_UP_TO_16CHARS ) )
            return
        }

        if ( name.includes( ' ' ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NAMING_THERE_IS_A_SPACE ) )
            return
        }

        if ( await PetNameManager.isNameTaken( name ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NAMING_ALREADY_IN_USE_BY_ANOTHER_PET ) )
            return
        }

        if ( !PetNameManager.isValidPetName( name ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NAMING_PETNAME_CONTAINS_INVALID_CHARS ) )
            return
        }

        this.setName( name, force )
        await this.updateAndBroadcastStatus( PetInfoAnimation.StatUpgrade )
        return this.storeMe()
    }

    addExpAndSp( addExp: number ): Promise<void> {
        let rate = this.isSinEater() ? ConfigManager.rates.getSinEaterXpRate() : ConfigManager.rates.getPetXpRate()
        return this.getStat().addExp( Math.round( addExp * rate ) ) as unknown as Promise<void>
    }

    getHungerLimit(): number {
        return this.hungerLimit
    }

    getSkillLevel( skillId: number ): number {
        return this.getPetData().getAvailableLevel( skillId, this.getLevel() )
    }

    async destroyItemByObjectId( objectId: number, count: number, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        let existingItem: L2ItemInstance = this.inventory.getItemByObjectId( objectId )
        let item: L2ItemInstance = existingItem ? await this.inventory.destroyItem( existingItem, count, this.getOwnerId(), reason ) : null

        if ( !item ) {
            if ( sendMessage ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
            }

            return false
        }


        if ( sendMessage ) {
            if ( count > 1 ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.S2_S1_DISAPPEARED )
                        .addItemInstanceName( item )
                        .addNumber( count )
                        .getBuffer()
                this.sendOwnedData( packet )
            } else {
                let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED )
                        .addItemInstanceName( item )
                        .getBuffer()
                this.sendOwnedData( packet )
            }
        }

        return true
    }

    doRevive() {
        this.getOwner().resetReviveProperties()

        super.doRevive()

        DecayTaskManager.cancel( this )
        this.startFeeding()
        if ( !this.isHungry() ) {
            this.setRunning()
        }

        this.getAIController().activate()
    }
}