import { L2DoormenInstance } from './L2DoormenInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { DataManager } from '../../../../data/manager'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'

export class L2FortDoormenInstance extends L2DoormenInstance {

    constructor( template: L2NpcTemplate ) {
        super( template )

        this.instanceType = InstanceType.L2FortDoormenInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2FortDoormenInstance {
        return new L2FortDoormenInstance( template )
    }

    showChatWindowDefault( player: L2PcInstance ): void {
        player.sendOwnedData( ActionFailed() )

        let path : string = `data/html/doormen/${this.getTemplate().getId()}.htm`

        if ( !this.isOwnerClan( player ) ) {
            path = `data/html/doormen/${this.getTemplate().getId()}-no.htm`
        } else if ( this.isUnderSiege() ) {
            path = `data/html/doormen/${this.getTemplate().getId()}-busy.htm`
        }

        let html : string = DataManager.getHtmlData().getItem( path )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), this.getObjectId() ) )
    }

    isOwnerClan( player: L2PcInstance ): boolean {
        if ( player.getClan() && this.getFort() && this.getFort().getOwnerClan() ) {
            return player.getClanId() === this.getFort().getOwnerClanId()
        }

        return false
    }

    isUnderSiege(): boolean {
        return this.getFort().isSiegeActive()
    }
}