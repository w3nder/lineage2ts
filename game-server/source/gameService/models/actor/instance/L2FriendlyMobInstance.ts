import { L2Attackable } from '../L2Attackable'
import { L2Character } from '../L2Character'
import { L2PcInstance } from './L2PcInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2FriendlyMobInstance extends L2Attackable {

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2FriendlyMobInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2FriendlyMobInstance {
        return new L2FriendlyMobInstance( template )
    }

    isAutoAttackable( attacker: L2Character ) : boolean {
        if ( attacker.isPlayer() ) {
            return ( attacker as L2PcInstance ).getKarma() > 0
        }

        return false
    }

    isAggressive() : boolean {
        return true
    }
}