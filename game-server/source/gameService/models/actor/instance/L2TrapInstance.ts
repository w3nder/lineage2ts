import { L2Npc } from '../L2Npc'
import { AISkillData, L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Character } from '../L2Character'
import { L2World } from '../../../L2World'
import { OlympiadGameManager } from '../../olympiad/OlympiadGameManager'
import { L2PcInstance } from './L2PcInstance'
import { PacketDispatcher } from '../../../PacketDispatcher'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { L2TargetType } from '../../skills/targets/L2TargetType'
import { SocialAction } from '../../../packets/send/SocialAction'
import { Skill } from '../../Skill'
import { TrapInfo } from '../../../packets/send/TrapInfo'
import { L2ItemInstance } from '../../items/instance/L2ItemInstance'
import { L2Weapon } from '../../items/L2Weapon'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { EventType, TrapActionEvent } from '../../events/EventType'
import { TrapAction } from '../../../enums/TrapAction'
import { ListenerCache } from '../../../cache/ListenerCache'
import { SkillCache } from '../../../cache/SkillCache'
import { AreaType } from '../../areas/AreaType'
import { L2Playable } from '../L2Playable'
import Timeout = NodeJS.Timeout

const taskInterval = 1000

export class L2TrapInstance extends L2Npc {
    hasLifeTime: boolean
    isInArena: boolean = false
    isTriggeredValue: boolean
    lifeTime: number
    owner: number // object id from L2Object
    playersWhoDetectedMe: Array<number>
    skillData: AISkillData
    remainingTime: number
    trapTask: Timeout

    constructor( template: L2NpcTemplate, instanceId: number, lifetime: number ) {
        super( template )

        this.instanceType = InstanceType.L2TrapInstance
        this.instanceId = instanceId
        this.name = template.getName()
        this.setIsInvulnerable( false )

        this.owner = null
        this.isTriggeredValue = false
        this.skillData = this.getTemplate().getSkillParameters()[ 'trap_skill' ]

        this.hasLifeTime = lifetime > 0
        this.lifeTime = lifetime !== 0 ? lifetime : 30000
        this.remainingTime = lifetime

        if ( this.skillData ) {
            this.trapTask = setInterval( this.runTrapTask.bind( this ), taskInterval )
        }
    }

    static fromOwner( template: L2NpcTemplate, player: L2PcInstance, lifeTime: number ): L2TrapInstance {
        let trap = new L2TrapInstance( template, player.getInstanceId(), lifeTime )
        trap.owner = player.objectId

        return trap
    }

    checkTarget( target: L2Character ): boolean {
        // Range seems to be reduced from Freya(300) to H5(150)
        if ( !target.isInsideRadius( this, 150 ) ) {
            return false
        }

        let skill = this.getSkill()
        if ( skill && !Skill.checkForAreaOffensiveSkills( this, target, skill, this.isInArena ) ) {
            return false
        }

        // observers
        if ( target.isPlayer() && target.getActingPlayer().inObserverMode() ) {
            return false
        }

        // olympiad own team and their summons not attacked
        let owner = this.getOwner()
        if ( owner && owner.isInOlympiadMode() ) {
            let player: L2PcInstance = target.getActingPlayer()
            if ( player && player.isInOlympiadMode() && ( player.getOlympiadSide() === owner.getOlympiadSide() ) ) {
                return false
            }
        }

        if ( this.isInArena ) {
            return true
        }

        if ( owner ) {
            if ( target.isAttackable() ) {
                return true
            }

            let player: L2PcInstance = target.getActingPlayer()
            return player && ( player.hasPvPFlag() || player.getKarma() > 0 )
        }

        return true
    }

    deleteMe(): Promise<void> {
        this.resetOwner()
        return super.deleteMe()
    }

    sendDamageMessage( target: L2Character, damage: number, mcrit: boolean, powerCrit: boolean, isMiss: boolean ) {
        let currentOwner = L2World.getObjectById( this.owner ) as L2PcInstance
        if ( isMiss || !currentOwner ) {
            return
        }

        if ( currentOwner.isInOlympiadMode()
                && target.isPlayer()
                && ( target as L2PcInstance ).isInOlympiadMode()
                && ( ( target as L2PcInstance ).getOlympiadGameId() === currentOwner.getOlympiadGameId() ) ) {
            OlympiadGameManager.notifyCompetitorDamage( currentOwner, damage )
        }

        if ( ( target.isInvulnerable() || target.isHpBlocked() ) && !target.isNpc() ) {
            PacketDispatcher.sendOwnedData( this.owner, SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACK_WAS_BLOCKED ) )
            return
        }

        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_DONE_S3_DAMAGE_TO_C2 )
                .addCharacterName( this )
                .addCharacterName( target )
                .addNumber( damage )
                .getBuffer()

        PacketDispatcher.sendOwnedData( this.owner, packet )
    }

    getKarma() {
        let owner = this.getOwner()
        return owner ? owner.getKarma() : 0
    }

    getLifeTime() {
        return this.lifeTime
    }

    getOwner(): L2PcInstance {
        return L2World.getPlayer( this.owner )
    }

    getOwnerId() : number {
        return this.owner
    }

    getPvpFlag() {
        let owner = this.getOwner()
        return owner ? owner.getPvpFlag() : 0
    }

    getRemainingTime() {
        return this.remainingTime
    }

    getSkill() : Skill {
        return this.skillData ? SkillCache.getSkill( this.skillData.id, this.skillData.level ) : null
    }

    isTrap(): boolean {
        return true
    }

    isTriggered() {
        return this.isTriggeredValue
    }

    resetOwner() {
        let owner = this.getOwner()
        if ( owner ) {
            owner.setTrap( null )
            this.owner = null
        }
    }

    runTrapTask() {
        if ( !this.isTriggered() ) {
            if ( this.hasLifeTime ) {
                this.setRemainingTime( this.getRemainingTime() - taskInterval )

                if ( this.getRemainingTime() < ( this.getLifeTime() - 15000 ) ) {
                    BroadcastHelper.dataBasedOnVisibility( this, SocialAction( this.getObjectId(), 2 ) )
                }

                if ( this.getRemainingTime() <= 0 ) {

                    switch ( this.getSkill().getTargetType() ) {
                        case L2TargetType.AURA:
                        case L2TargetType.FRONT_AURA:
                        case L2TargetType.BEHIND_AURA:
                            this.triggerTrap( this )
                        default:
                            this.unSummon()
                            break
                    }

                    return
                }
            }

            L2World.forEachObjectByPredicate( this, 1500, ( target: L2Character ) => {
                if ( !target.isCharacter() ) {
                    return
                }

                if ( this.checkTarget( target ) ) {
                    this.triggerTrap( target )
                    return false
                }
            } )
        }
    }

    async runTrapTriggerTask() : Promise<void> {
        let skill = this.getSkill()
        if ( !skill ) {
            return
        }

        await this.doCast( skill )
        setTimeout( this.unSummon.bind( this ), skill.getHitTime() + 300 )
    }

    setRemainingTime( value: number ) {
        this.remainingTime = value
    }

    stopTrapTask() {
        if ( this.trapTask ) {
            clearInterval( this.trapTask )
            this.trapTask = null
        }
    }

    triggerTrap( target: L2Character ) {
        this.stopTrapTask()

        this.isTriggeredValue = true
        BroadcastHelper.dataBasedOnVisibility( this, TrapInfo( this, null ) )
        this.setTarget( target )

        if ( ListenerCache.hasGeneralListener( EventType.TrapTriggered ) ) {
            let eventData : TrapActionEvent = {
                trapObjectId: this.getObjectId(),
                triggerObjectId: target.getObjectId(),
                action: TrapAction.TRAP_TRIGGERED
            }

            ListenerCache.sendGeneralEvent( EventType.TrapTriggered, eventData )
        }

        setTimeout( this.runTrapTriggerTask.bind( this ), 500 )
    }

    unSummon() {
        this.stopTrapTask()
        this.resetOwner()

        if ( !this.isDead() ) {
            this.deleteMe()
        }
    }

    setDetected( detector: L2Character ) {
        if ( this.isInArena ) {
            if ( detector.isPlayable() ) {
                this.shouldDescribeState( detector.getActingPlayer() )
            }

            return
        }

        let owner = this.getOwner()
        if ( owner && !owner.hasPvPFlag() && owner.getKarma() === 0 ) {
            return
        }

        this.playersWhoDetectedMe.push( detector.getObjectId() )

        if ( ListenerCache.hasNpcTemplateListeners( this.getId(), EventType.TrapTriggered ) ) {
            let eventData : TrapActionEvent = {
                trapObjectId: this.getObjectId(),
                triggerObjectId: detector.getObjectId(),
                action: TrapAction.TRAP_DETECTED
            }

            ListenerCache.sendNpcTemplateEvent( this.getId(), EventType.TrapTriggered, eventData )
        }

        if ( detector.isPlayable() ) {
            this.shouldDescribeState( detector.getActingPlayer() )
        }
    }

    shouldDescribeState( player: L2PcInstance ) : boolean {
        if ( this.isTriggered() || this.isVisibleFor( player ) ) {
            player.sendOwnedData( TrapInfo( this, player ) )
        }

        return false
    }

    onSpawn() {
        super.onSpawn()
        this.isInArena = this.isInArea( AreaType.PVP ) && !this.isInArea( AreaType.CastleSiege ) && !this.isInArea( AreaType.FortSiege )
        this.playersWhoDetectedMe = []
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        return this.canBeSeenBy( attacker )
    }

    getActiveWeaponInstance(): L2ItemInstance {
        return null
    }

    getActiveWeaponItem(): L2Weapon {
        return null
    }

    getSecondaryWeaponInstance(): L2ItemInstance {
        return null
    }

    getSecondaryWeaponItem(): L2Weapon {
        return null
    }

    isVisibleFor( player: L2PcInstance ): boolean {
        if ( !player || player.inObserverMode() ) {
            return false
        }

        if ( this.isInArena ) {
            return true
        }

        if ( this.playersWhoDetectedMe.includes( player.getObjectId() ) ) {
            return true
        }

        let owner = this.getOwner()
        if ( owner === player ) {
            return true
        }

        if ( owner.isInOlympiadMode() && player.isInOlympiadMode() && ( player.getOlympiadSide() !== owner.getOlympiadSide() ) ) {
            return false
        }

        return this.isInOwnerParty( owner, player )
    }

    private isInOwnerParty( owner: L2PcInstance, playable: L2Playable ) : boolean {
        let ownerParty = owner.getParty()
        if ( !ownerParty ) {
            return false
        }

        let otherParty = playable.getParty()
        if ( !otherParty ) {
            return false
        }

        return ownerParty.getLeaderObjectId() === otherParty.getLeaderObjectId()
    }

    canBeSeenBy( character: L2Character ) : boolean {
        if ( !character ) {
            return false
        }

        if ( this.isInArena ) {
            return true
        }

        if ( character.isPlayer() ) {
            return this.isVisibleFor( character as L2PcInstance )
        }

        if ( character.isPlayable() ) {
            return this.isInOwnerParty( this.getOwner(), character as L2Playable )
        }

        return false
    }
}