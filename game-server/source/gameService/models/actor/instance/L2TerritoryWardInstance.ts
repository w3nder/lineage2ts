import { L2Attackable } from '../L2Attackable'
import { L2Character } from '../L2Character'
import { Skill } from '../../Skill'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { TerritoryWarManager } from '../../../instancemanager/TerritoryWarManager'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { InventoryAction } from '../../../enums/InventoryAction'

export class L2TerritoryWardInstance extends L2Attackable {

    constructor( template: L2NpcTemplate ) {
        super( template )

        this.instanceType = InstanceType.L2TerritoryWardInstance
    }

    async reduceCurrentHpByDOT( power: number, attacker: L2Character, skill: Skill ) : Promise<void> {
        return
    }

    isAutoAttackable( attacker: L2Character ): boolean {
        if ( this.isInvulnerable() ) {
            return false
        }

        if ( !this.getCastle() || !this.getCastle().isSiegeActive() ) {
            return false
        }

        let player : L2PcInstance = attacker.getActingPlayer()
        if ( !player ) {
            return false
        }

        if ( player.getSiegeSide() === 0 ) {
            return false
        }

        return !TerritoryWarManager.isAllyField( player, this.getCastle().getResidenceId() )
    }

    hasRandomAnimation(): boolean {
        return false
    }

    async reduceCurrentHp( damage: number, attacker: L2Character, skill: Skill, isAwake: boolean = true, isDOT: boolean = false ): Promise<void> {
        if ( skill || !TerritoryWarManager.isTWInProgress ) {
            return
        }

        let actingPlayer : L2PcInstance = attacker.getActingPlayer()
        if ( !actingPlayer ) {
            return
        }

        if ( actingPlayer.isCombatFlagEquipped() ) {
            return
        }

        if ( actingPlayer.getSiegeSide() === 0 ) {
            return
        }
        if ( !this.getCastle() ) {
            return
        }

        if ( TerritoryWarManager.isAllyField( actingPlayer, this.getCastle().getResidenceId() ) ) {
            return
        }

        return super.reduceCurrentHp( damage, attacker, skill, isDOT, isAwake )
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( !await super.doDie( killer ) || !this.getCastle() || !TerritoryWarManager.isTWInProgress ) {
            return false
        }

        if ( killer && killer.isPlayer() ) {
            if ( ( killer.getSiegeSide() > 0 ) && !( killer as L2PcInstance ).isCombatFlagEquipped() ) {
                await ( killer as L2PcInstance ).addItem( this.getId() - 23012, 1, -1, killer.getObjectId(), InventoryAction.None )
            } else {
                TerritoryWarManager.getTerritoryWard( this.getId() - 36491 ).spawnMe()
            }

            let packet = new SystemMessageBuilder( SystemMessageIds.THE_S1_WARD_HAS_BEEN_DESTROYED_C2_HAS_THE_WARD )
                    .addString( this.getName().replace( / Ward/g, '' ) )
                    .addPlayerCharacterName( killer as L2PcInstance )
                    .getBuffer()

            TerritoryWarManager.announceToParticipants( packet, 0, 0 )
        } else {
            TerritoryWarManager.getTerritoryWard( this.getId() - 36491 ).spawnMe()
        }

        this.decayMe()
        return true
    }
}