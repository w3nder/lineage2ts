import { L2Summon } from '../L2Summon'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { L2PcInstance } from './L2PcInstance'
import { InstanceType } from '../../../enums/InstanceType'
import { DatabaseManager } from '../../../../database/manager'
import { ConfigManager } from '../../../../config/ConfigManager'
import { CharacterEffectsCache } from '../../../cache/CharacterEffectsCache'
import { L2Object } from '../../L2Object'
import { Stats } from '../../stats/Stats'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { SetSummonRemainTime } from '../../../packets/send/SetSummonRemainTime'
import { L2Character } from '../L2Character'
import { Skill } from '../../Skill'
import { CharacterSummonCache } from '../../../cache/CharacterSummonCache'
import { ServitorSkillCache } from '../../../cache/ServitorSkillCache'
import { ItemDefinition } from '../../../interface/ItemDefinition'
import Timeout = NodeJS.Timeout
import { L2SummonType } from '../../../enums/L2SummonType'

const lifeTaskInterval: number = 5000

export class L2ServitorInstance extends L2Summon {
    expMultiplier: number = 0
    itemConsume: ItemDefinition
    lifeTime: number
    lifeTimeRemaining: number
    consumeItemInterval: number
    consumeItemIntervalRemaining: number
    summonLifeTask: Timeout
    summonSkillId: number

    constructor( template: L2NpcTemplate, owner: L2PcInstance ) {
        super( template, owner )

        this.instanceType = InstanceType.L2ServitorInstance
        this.setShowSummonAnimation( true )
    }

    async destroyItemByItemId( itemId: number, count: number, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        return this.getOwner().destroyItemByItemId( itemId, count, sendMessage, reason )
    }

    async destroyItemByObjectId( objectId: number, count: number, sendMessage: boolean, reason: string = null ): Promise<boolean> {
        return this.getOwner().destroyItemByObjectId( objectId, count, sendMessage, reason )
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( !await super.doDie( killer ) ) {
            return false
        }

        this.stopLifeTask()

        await CharacterSummonCache.removeRestoredServitor( this.getOwnerId() )
        return true
    }

    getAttackElementValue( attackAttribute: number ): number {
        let owner = this.getOwner()
        if ( owner ) {
            return owner.getAttackElementValue( attackAttribute )
        }

        return super.getAttackElementValue( attackAttribute )
    }

    getCriticalHit( target: L2Character, skill: Skill ): number {
        let player = this.getActingPlayer()
        return Math.floor( super.getCriticalHit( target, skill ) + player.getCriticalHit( target, skill ) * ( player.getServitorShareBonus( Stats.CRITICAL_RATE ) - 1.0 ) )
    }

    getDefenceElementValue( defenceAttribute: number ): number {
        let owner = this.getOwner()
        if ( owner ) {
            return owner.getDefenceElementValue( defenceAttribute )
        }

        return super.getDefenceElementValue( defenceAttribute )
    }

    getLevel(): number {
        if ( this.getTemplate() ) {
            return this.getTemplate().getLevel()
        }

        return 0
    }

    // TODO : L2J doesn't have magical crit override?
    getMCriticalHit( target: L2Character, skill: Skill ): number {
        let player = this.getActingPlayer()
        return Math.floor( super.getCriticalHit( target, skill ) + player.getCriticalHit( target, skill ) * ( player.getServitorShareBonus( Stats.CRITICAL_RATE ) - 1.0 ) )
    }

    getMagicAttack( target: L2Character, skill: Skill ): number {
        let player = this.getActingPlayer()
        return super.getMagicAttack( target, skill ) + player.getMagicAttack( target, skill ) * ( player.getServitorShareBonus( Stats.MAGIC_ATTACK ) - 1.0 )
    }

    getMagicAttackSpeed(): number {
        let player = this.getActingPlayer()
        return Math.floor( super.getMagicAttackSpeed() + player.getMagicAttackSpeed() * ( player.getServitorShareBonus( Stats.MAGIC_ATTACK_SPEED ) - 1.0 ) )
    }

    getMagicDefence( target: L2Character, skill: Skill ): number {
        let player = this.getActingPlayer()
        return super.getMagicDefence( target, skill ) + player.getMagicDefence( target, skill ) * ( player.getServitorShareBonus( Stats.MAGIC_DEFENCE ) - 1.0 )
    }

    getMaxHp(): number {
        let player = this.getActingPlayer()
        return Math.floor( super.getMaxHp() + player.getMaxHp() * ( player.getServitorShareBonus( Stats.MAX_HP ) - 1.0 ) )
    }

    getMaxMp(): number {
        let player = this.getActingPlayer()
        return Math.floor( super.getMaxMp() + player.getMaxMp() * ( player.getServitorShareBonus( Stats.MAX_MP ) - 1.0 ) )
    }

    getMaxRecoverableHp(): number {
        return Math.floor( this.calculateStat( Stats.MAX_RECOVERABLE_HP, this.getMaxHp() ) )
    }

    getMaxRecoverableMp(): number {
        return Math.floor( this.calculateStat( Stats.MAX_RECOVERABLE_MP, this.getMaxMp() ) )
    }

    getPowerAttack( target: L2Character ): number {
        let player = this.getActingPlayer()
        return super.getPowerAttack( target ) + player.getPowerAttack( target ) * ( player.getServitorShareBonus( Stats.POWER_ATTACK ) - 1.0 )
    }

    getPowerAttackSpeed(): number {
        let player = this.getActingPlayer()
        return super.getPowerAttackSpeed() + player.getPowerAttackSpeed() * ( player.getServitorShareBonus( Stats.POWER_ATTACK_SPEED ) - 1.0 )
    }

    getPowerDefence( target: L2Character ): number {
        let player = this.getActingPlayer()
        return super.getPowerDefence( target ) + player.getPowerDefence( target ) * ( player.getServitorShareBonus( Stats.POWER_DEFENCE ) - 1.0 )
    }

    async doPickupItem( object: L2Object ): Promise<void> {

    }

    async restoreEffects(): Promise<void> {
        if ( this.getOwner().isInOlympiadMode() ) {
            return
        }

        return CharacterEffectsCache.restoreServitorEffects( this )
    }

    async storeEffect( store: boolean ): Promise<void> {
        if ( !ConfigManager.character.summonStoreSkillCooltime()
                || !this.getOwner()
                || this.getOwner().isInOlympiadMode() ) {
            return
        }

        return CharacterEffectsCache.updateServitorEffects( this, store )
    }

    async storeMe(): Promise<void> {
        if ( this.summonSkillId === 0 || this.isDead() ) {
            return
        }

        if ( ConfigManager.character.restoreServitorOnReconnect() && this.getLifeTimeRemaining() > 0 ) {
            return DatabaseManager.getServitors().saveServitor( this )
        }
    }

    getExpMultiplier() {
        return this.expMultiplier
    }

    getItemConsume() : ItemDefinition {
        return this.itemConsume
    }

    getLifeTime() {
        return this.lifeTime
    }

    getLifeTimeRemaining() {
        return this.lifeTimeRemaining
    }

    getSummonSkillId() {
        return this.summonSkillId
    }

    getSummonType(): L2SummonType {
        return L2SummonType.Servitor
    }

    async unSummon( player: L2PcInstance ): Promise<void> {
        this.stopLifeTask()

        await super.unSummon( player )
        CharacterSummonCache.removeRestoredServitor( this.getOwnerId() )

        if ( this.getLifeTimeRemaining() <= 0 ) {
            return DatabaseManager.getServitors().removeServitor( this.getOwnerId() )
        }
    }

    isServitor(): boolean {
        return true
    }

    onSpawn() {
        super.onSpawn()
        if ( !this.summonLifeTask ) {
            this.summonLifeTask = setInterval( this.runLifeTask.bind( this ), lifeTaskInterval )
        }
    }

    async runLifeTask(): Promise<void> {
        this.lifeTimeRemaining -= lifeTaskInterval

        if ( this.isDead() || !this.isVisible() ) {
            return this.stopLifeTask()
        }

        let player = this.getOwner()
        if ( this.lifeTimeRemaining <= 0 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SERVITOR_PASSED_AWAY ) )
            return this.unSummon( player )
        }

        if ( this.consumeItemInterval > 0 ) {
            this.consumeItemIntervalRemaining -= lifeTaskInterval

            let consumedItem = this.getItemConsume()
            if ( consumedItem
                    && this.consumeItemIntervalRemaining <= 0
                    && consumedItem.count > 0
                    && consumedItem.id > 0 ) {
                if ( await this.destroyItemByItemId( consumedItem.id, consumedItem.count, false, 'L2ServitorInstance.runLifeTask' ) ) {
                    let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.SUMMONED_MOB_USES_S1 )
                            .addItemNameWithId( consumedItem.id )
                            .getBuffer()

                    player.sendOwnedData( packet )
                    this.consumeItemIntervalRemaining = this.consumeItemInterval
                } else {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SERVITOR_DISAPPEARED_NOT_ENOUGH_ITEMS ) )
                    await this.unSummon( player )
                }
            }
        }

        player.sendOwnedData( SetSummonRemainTime( this.getLifeTime(), this.lifeTimeRemaining ) )
        this.updateEffectIcons()
    }

    setExpMultiplier( value: number ) {
        this.expMultiplier = value
    }

    setItemConsume( item: ItemDefinition ) {
        this.itemConsume = item
    }

    setItemConsumeInterval( value: number ) {
        this.consumeItemInterval = value
        this.consumeItemIntervalRemaining = value
    }

    setLifeTime( value: number ) {
        this.lifeTime = value
        this.lifeTimeRemaining = value
    }

    setLifeTimeRemaining( value: number ) {
        this.lifeTimeRemaining = value
    }

    setReferenceSkill( id: number ) {
        this.summonSkillId = id
    }

    stopLifeTask() {
        if ( this.summonLifeTask ) {
            clearInterval( this.summonLifeTask )
        }

        this.summonLifeTask = null
    }

    getSkillLevel( skillId: number ): number {
        return ServitorSkillCache.getAvailableLevel( this.getId(), this.getLevel(), skillId )
    }
}