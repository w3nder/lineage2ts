import { L2Npc } from '../L2Npc'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../../data/manager'
import { L2AuctioneerState } from '../../../enums/L2AuctioneerState'

export class L2AuctioneerInstance extends L2Npc {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2AuctioneerInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2AuctioneerInstance {
        return new L2AuctioneerInstance( template )
    }

    showChatWindowDefault( player: L2PcInstance ): void {
        let path = this.getCurrentState() === L2AuctioneerState.Siege ? 'overrides/html/clanhall/auction/busy.htm' : 'overrides/html/clanhall/auction/auction.htm'
        let html: string = DataManager.getHtmlData().getItem( path )

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), this.getObjectId() ) )
    }

    getCurrentState(): L2AuctioneerState {
        let castle = this.getCastle()
        if ( castle && castle.getResidenceId() > 0 ) {
            if ( castle.getSiege().isInProgress() ) {
                return L2AuctioneerState.Siege
            }

            return L2AuctioneerState.Normal
        }

        return L2AuctioneerState.Disabled
    }
}