
// This class is here mostly for convenience and for avoidance of hardcoded IDs.
// It refers to Beast (mobs) that can be attacked but can also be fed
// For example, the Beast Farm's Alpen Buffalo.
// This class is only truly used by the handlers in order to check the correctness
// of the target.  However, no additional tasks are needed, since they are all
// handled by scripted AI.

import { L2MonsterInstance } from './L2MonsterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2FeedableBeastInstance extends L2MonsterInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2FeedableBeastInstance
    }

    static fromTemplate( template: L2NpcTemplate ) : L2FeedableBeastInstance {
        return new L2FeedableBeastInstance( template )
    }
}