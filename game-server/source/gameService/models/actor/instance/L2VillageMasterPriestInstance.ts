import { L2VillageMasterInstance } from './L2VillageMasterInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { Race } from '../../../enums/Race'
import { getPlayerClassById } from '../../base/PlayerClass'
import { ClassType } from '../../base/ClassType'

export class L2VillageMasterPriestInstance extends L2VillageMasterInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2VillageMasterPriestInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2VillageMasterPriestInstance {
        return new L2VillageMasterPriestInstance( template )
    }

    checkVillageMasterRace( classId: number ): boolean {
        return [ Race.HUMAN, Race.ELF ].includes( getPlayerClassById( classId ).race )
    }

    checkVillageMasterTeachType( classId: number ): boolean {
        return getPlayerClassById( classId ).type === ClassType.Priest
    }
}