import { L2NpcInstance } from './L2NpcInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'

export class L2AdventurerInstance extends L2NpcInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2AdventurerInstance
    }

    static fromTemplate( template: L2NpcTemplate ) : L2AdventurerInstance {
        return new L2AdventurerInstance( template )
    }

    getHtmlPath( npcId: number, value: number ): string {
        let fileName : string = npcId.toString()

        if ( value !== 0 ) {
            fileName = `${fileName}-${value}`
        }

        return 'data/html/adventurer_guildsman/' + fileName + '.htm'
    }
}