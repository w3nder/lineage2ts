import { L2NpcInstance } from './L2NpcInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../../data/manager'
import { L2SkillLearn } from '../../L2SkillLearn'
import { SkillCache } from '../../../cache/SkillCache'
import { AcquireSkillType } from '../../../enums/AcquireSkillType'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { AcquireSkillList } from '../../../packets/send/builder/AcquireSkillList'
import { ConfigManager } from '../../../../config/ConfigManager'

export class L2VillageMasterInstance extends L2NpcInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2VillageMasterInstance
    }

    static showPledgeSkillList( player: L2PcInstance ): void {
        if ( !player.isClanLeader() ) {
            let path = 'data/html/villagemaster/NotClanLeader.htm'
            let html: string = DataManager.getHtmlData().getItem( path )
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId() ) )
            player.sendOwnedData( ActionFailed() )

            return
        }

        let skills: Array<L2SkillLearn> = SkillCache.getAvailablePledgeSkills( player.getClan() )
        let skillList: AcquireSkillList = new AcquireSkillList( AcquireSkillType.Pledge )

        skills.forEach( ( skill: L2SkillLearn ) => {
            skillList.addSkill( skill.getSkillId(), skill.getSkillLevel(), skill.getLevelUpSp(), skill.getSocialClass() )
        } )

        if ( skillList.getCount() > 0 ) {
            player.sendOwnedData( skillList.getBuffer() )
            player.sendOwnedData( ActionFailed() )
            return
        }

        if ( player.getClan().getLevel() < 8 ) {
            let limit = player.getClan().getLevel() < 5 ? 5 : player.getClan().getLevel() + 1

            let message = new SystemMessageBuilder( SystemMessageIds.DO_NOT_HAVE_FURTHER_SKILLS_TO_LEARN_S1 )
                    .addNumber( limit )
                    .getBuffer()

            player.sendOwnedData( message )
            player.sendOwnedData( ActionFailed() )
            return
        }

        let path = 'data/html/villagemaster/NoMoreSkills.htm'
        let html: string = DataManager.getHtmlData().getItem( path )
        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId() ) )
        player.sendOwnedData( ActionFailed() )
    }

    checkQuests( player: L2PcInstance ): boolean {
        return player.isNoble()
                || player.hasQuestCompleted( 'Q00234_FatesWhisper' )
                || player.hasQuestCompleted( 'Q00235_MimirsElixir' )
    }

    checkVillageMasterRace( classId: number ): boolean {
        return true
    }

    checkVillageMasterTeachType( classId: number ): boolean {
        return true
    }

    getHtmlPath( npcId: number, value: number ): string {
        let fileName: string = npcId.toString()

        if ( value !== 0 ) {
            fileName = `${ fileName }-${ value }`
        }

        return 'data/html/villagemaster/' + fileName + '.htm'
    }

    checkVillageMaster( classId: number ): boolean {
        if ( ConfigManager.character.subclassEverywhere() ) {
            return true
        }

        return this.checkVillageMasterRace( classId ) && this.checkVillageMasterTeachType( classId )
    }
}