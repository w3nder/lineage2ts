import { L2Decoy } from '../L2Decoy'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { L2PcInstance } from './L2PcInstance'
import { InstanceType } from '../../../enums/InstanceType'
import { SkillCache } from '../../../cache/SkillCache'
import { L2Character } from '../L2Character'
import { DecayTaskManager } from '../../../taskmanager/DecayTaskManager'
import { Skill } from '../../Skill'
import Timeout = NodeJS.Timeout

export class L2DecoyInstance extends L2Decoy {
    totalLifeTime: number
    timeRemaining: number
    decoyLifeTask: Timeout
    hateSpamTask: Timeout
    decoyLifeTaskInterval: 1000
    hateSpamTaskInterval: 5000

    constructor( template: L2NpcTemplate, owner: L2PcInstance, totalLifeTime ) {
        super( template, owner )
        this.instanceType = InstanceType.L2DecoyInstance
        this.totalLifeTime = totalLifeTime
        this.timeRemaining = totalLifeTime

        this.decoyLifeTask = setInterval( this.runLifeTimeTask.bind( this ), 1000 )

        let skillLevel = template.getDisplayId() - 13070
        let skill = SkillCache.getSkill( 5272, skillLevel )
        this.hateSpamTask = setInterval( this.runHateSpamTask.bind( this ), 5000, skill )
    }

    decreaseTimeRemaining() {
        this.timeRemaining -= this.decoyLifeTaskInterval
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( !await super.doDie( killer ) ) {
            return false
        }

        this.stopHateSpamTask()
        this.totalLifeTime = 0
        DecayTaskManager.add( this )
        return true
    }

    getTimeRemaining() {
        return this.timeRemaining
    }

    runHateSpamTask( skill: Skill ) {
        let player = this.getOwner()
        player.setTarget( player )
        player.doCast( skill )
    }

    runLifeTimeTask() {
        this.decreaseTimeRemaining()
        let newTimeRemaining = this.getTimeRemaining()
        if ( newTimeRemaining < 0 ) {
            this.unSummon()
        }
    }

    stopHateSpamTask() {
        if ( this.hateSpamTask ) {
            clearInterval( this.hateSpamTask )
            this.hateSpamTask = null
        }
    }

    stopLifeTask() {
        if ( this.decoyLifeTask ) {
            clearInterval( this.decoyLifeTask )
            this.decoyLifeTask = null
        }
    }

    unSummon() : Promise<void> {
        this.stopLifeTask()
        this.stopHateSpamTask()
        return super.unSummon()
    }
}