import { L2MerchantInstance } from './L2MerchantInstance'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { DataManager } from '../../../../data/manager'
import { ConfigManager } from '../../../../config/ConfigManager'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'

export class L2PetManagerInstance extends L2MerchantInstance {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2PetManagerInstance
    }

    static fromTemplate( template: L2NpcTemplate ): L2PetManagerInstance {
        return new L2PetManagerInstance( template )
    }

    getHtmlPath( npcId: number, value: number ): string {
        let fileName: string = npcId.toString()

        if ( value !== 0 ) {
            fileName = `${ fileName }-${ value }`
        }

        return 'data/html/petmanager/' + fileName + '.htm'
    }

    showChatWindowDefault( player: L2PcInstance ): void {
        let path = `data/html/petmanager/${ this.getId() }.htm`
        if ( this.getId() === 36478 && player.hasSummon() ) { // Mickey pet
            path = 'data/html/petmanager/restore-unsummonpet.htm'
        }

        let html: string = DataManager.getHtmlData().getItem( path )

        if ( ConfigManager.general.allowRentPet() && ConfigManager.npc.getPetRentNPCs().includes( this.getId() ) ) {
            html = html.replace( ' Quest', ' RentPet">Rent Pet</a><br><a action="bypass -h Quest' )
            path = ''
        }

        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), this.getObjectId() ) )
    }
}