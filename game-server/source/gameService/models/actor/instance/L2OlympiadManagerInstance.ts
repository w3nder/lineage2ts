import { L2Npc } from '../L2Npc'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { OlympiadValues } from '../../../values/OlympiadValues'
import _ from 'lodash'

export class L2OlympiadManagerInstance extends L2Npc {
    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2OlympiadManagerInstance
    }

    static fromTemplate( template: L2NpcTemplate ) : L2OlympiadManagerInstance {
        return new L2OlympiadManagerInstance( template )
    }

    showChatWindowExtended( player: L2PcInstance, value: number, suffix: string ): void {
        let path = `${OlympiadValues.OLYMPIAD_HTML_PATH}noble_desc${value}${_.defaultTo( suffix, '' )}.htm`

        if ( path === `${OlympiadValues.OLYMPIAD_HTML_PATH}noble_desc0.htm` ) {
            path = `${OlympiadValues.OLYMPIAD_HTML_PATH}noble_main.htm`
        }

        this.showChatWindowWithPath( player, path )
    }
}