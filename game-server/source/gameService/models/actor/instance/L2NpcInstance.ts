import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2Npc } from '../L2Npc'

export class L2NpcInstance extends L2Npc {
    constructor( template: L2NpcTemplate ) {
        super( template )

        this.instanceType = InstanceType.L2NpcInstance
        this.setIsInvulnerable( false )
    }

    static fromTemplate( template: L2NpcTemplate ): L2NpcInstance {
        return new L2NpcInstance( template )
    }
}