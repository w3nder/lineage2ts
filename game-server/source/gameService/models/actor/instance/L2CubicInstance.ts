import { Skill } from '../../Skill'
import { L2World } from '../../../L2World'
import { L2PcInstance } from './L2PcInstance'
import { L2Character } from '../L2Character'
import { L2Party } from '../../L2Party'
import { DuelManager } from '../../../instancemanager/DuelManager'
import { Formulas } from '../../stats/Formulas'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { TvTEvent } from '../../entity/TvTEvent'
import { TvTEventTeam } from '../../entity/TvTEventTeam'
import { Stats } from '../../stats/Stats'
import { AggroCache } from '../../../cache/AggroCache'
import _, { DebouncedFunc } from 'lodash'
import { SiegeRole } from '../../../enums/SiegeRole'
import { MagicSkillUseWithCharacters } from '../../../packets/send/MagicSkillUse'
import { BuffInfo } from '../../skills/BuffInfo'
import { BroadcastHelper } from '../../../helpers/BroadcastHelper'
import { L2EffectType } from '../../../enums/effects/L2EffectType'
import logSymbols from 'log-symbols'
import { AreaType } from '../../areas/AreaType'
import Timeout = NodeJS.Timeout

const enum L2CubicType {
    Storm = 1,
    Vampiric = 2,
    Life = 3,
    Viper = 4,
    Poltergeist = 5,
    Binding = 6,
    Aqua = 7,
    Spark = 8,
    Attract = 9,
    EvaTemplar = 10,
    ShillienTemplar = 11,
    ArcanaLord = 12,
    ElementalMaster = 13,
    SpectralMaster = 14,
}

const enum CubicValues {
    MaximumRange = 900,
    HealSkillId = 4051,
    CureSkillId = 5579,
}

export class L2CubicInstance {
    ownerId: number
    cubicId: number
    power: number
    actionDelaySeconds: number
    chance: number
    maxUseCount: number
    givenByOther: boolean
    skills: Array<Skill>
    disappearTask: Timeout
    healTask: Timeout
    private debouncedAction: DebouncedFunc<() => void>
    currentUseCount: number = 0
    startTime: number
    expireTime: number
    useChances: Array<number>
    healSkill: Skill
    activatedOnAttack: boolean
    activatedOnHpDamage: boolean

    constructor( owner: L2PcInstance,
            cubicId: number,
            skills: Array<Skill>,
            power: number,
            delaySeconds: number,
            skillChance: number,
            maxUse: number,
            durationSeconds: number,
            useChances: Array<number>,
            givenByOther: boolean ) {
        this.ownerId = owner.objectId
        this.cubicId = cubicId
        this.power = power
        this.actionDelaySeconds = delaySeconds

        this.chance = skillChance
        this.maxUseCount = maxUse
        this.givenByOther = givenByOther
        this.skills = skills

        let durationMs = durationSeconds * 1000
        let actionMs = delaySeconds * 1000

        this.disappearTask = setTimeout( this.onCubicEnd.bind( this ), durationMs, this )
        this.startTime = Date.now()
        this.expireTime = this.startTime + durationMs
        this.useChances = useChances

        this.debouncedAction = _.debounce( this.getActionMethod(), actionMs, {
            leading: true,
            trailing: false,
        } )

        this.healSkill = this.getSkills().find( ( skill: Skill ): boolean => {
            return skill.getId() === CubicValues.HealSkillId
        } )
        this.activatedOnHpDamage = this.cubicId === L2CubicType.Life
        this.activatedOnAttack = !this.activatedOnHpDamage

        /*
            Reasoning to have additional healing interval:
            - healing is triggered by reduced hp on player, is prioritized
            - healing still needs to happen for summon and party members on delay interval
         */
        if ( this.cubicId === L2CubicType.Life ) {
            this.activate()
            this.healTask = setInterval( this.onHealTask.bind( this ), actionMs )
        }
    }

    private isInCubicRange( owner: L2Character, target: L2Character ): boolean {
        if ( !owner || !target ) {
            return false
        }

        return owner.isInsideRadius( target, CubicValues.MaximumRange, true )
    }

    stopAutomaticRemoval(): void {
        if ( this.disappearTask ) {
            clearTimeout( this.disappearTask )

            this.disappearTask = null
        }
    }

    private findTargetForHeal( player: L2PcInstance ): L2Character {
        let target: L2Character
        let party: L2Party = player.isInDuel() && !DuelManager.getDuel( player.getDuelId() ).isPartyDuel() ? null : player.getParty()
        let ratio = 1

        if ( party && !player.isInOlympiadMode() ) {
            party.getMembers().forEach( ( memberId: number ) => {
                let partyMember = L2World.getPlayer( memberId )

                if ( !partyMember ) {
                    return
                }

                if ( !partyMember.isDead() && this.isInCubicRange( player, partyMember ) ) {
                    let memberRatio = partyMember.getCurrentHp() / partyMember.getMaxHp()
                    if ( ratio > memberRatio ) {
                        ratio = memberRatio
                        target = partyMember
                    }
                }

                let summon = partyMember.getSummon()
                if ( summon && !summon.isDead() && this.isInCubicRange( player, summon ) ) {
                    let summonRatio = summon.getCurrentHp() / summon.getMaxHp()
                    if ( ratio > summonRatio ) {
                        ratio = summonRatio
                        target = summon
                    }
                }
            } )
        }

        let playerRatio = player.getCurrentHp() / player.getMaxHp()
        if ( ratio > playerRatio ) {
            ratio = playerRatio
            target = player
        }

        let summon = player.getSummon()
        if ( summon && !summon.isDead() && this.isInCubicRange( player, summon ) && ratio > ( summon.getCurrentHp() / summon.getMaxHp() ) ) {
            target = summon
        }

        return target
    }

    private getActionMethod(): () => Promise<void> {
        switch ( this.cubicId ) {
            case L2CubicType.Aqua:
            case L2CubicType.Binding:
            case L2CubicType.Spark:
            case L2CubicType.Storm:
            case L2CubicType.Poltergeist:
            case L2CubicType.Vampiric:
            case L2CubicType.Viper:
            case L2CubicType.Attract:
            case L2CubicType.ArcanaLord:
            case L2CubicType.ElementalMaster:
            case L2CubicType.SpectralMaster:
            case L2CubicType.EvaTemplar:
            case L2CubicType.ShillienTemplar:
                return this.onDamageAction.bind( this )

            case L2CubicType.Life:
                return this.onHealAction.bind( this )
        }
    }

    activate(): void {
        if ( !this.debouncedAction ) {
            return
        }

        this.debouncedAction()
    }

    private findEnemyTarget( player: L2PcInstance ): L2Character {
        let ownerTarget = player.getTarget() as L2Character
        if ( !ownerTarget || ownerTarget.isDead() || !ownerTarget.isCharacter() ) {
            return null
        }

        if ( TvTEvent.isStarted() && TvTEvent.isPlayerParticipant( player.getObjectId() ) ) {
            let enemyTeam: TvTEventTeam = TvTEvent.getParticipantEnemyTeam( player.getObjectId() )
            let target: L2PcInstance = ownerTarget.getActingPlayer()

            if ( target ) {
                let targetId = target.getObjectId()
                if ( enemyTeam.containsPlayer( targetId ) && !target.isDead() && this.isInCubicRange( player, target ) ) {
                    return target
                }
            }

            return null
        }

        if ( player.isInDuel() ) {
            let duelId = player.getDuelId()
            let playerA: L2PcInstance = DuelManager.getDuel( duelId ).getTeamLeaderA()
            let playerB: L2PcInstance = DuelManager.getDuel( duelId ).getTeamLeaderB()
            let target: L2Character

            if ( DuelManager.getDuel( duelId ).isPartyDuel() ) {
                let partyA: L2Party = playerA.getParty()
                let partyB: L2Party = playerB.getParty()
                let partyEnemy: L2Party = null

                if ( partyA ) {
                    if ( partyA.getMembers().includes( player.getObjectId() ) ) {
                        if ( partyB ) {
                            partyEnemy = partyB
                        } else {
                            target = playerB
                        }
                    } else {
                        partyEnemy = partyA
                    }
                } else {
                    if ( playerA.getObjectId() === player.getObjectId() ) {
                        if ( partyB ) {
                            partyEnemy = partyB
                        } else {
                            target = playerB
                        }
                    } else {
                        target = playerA
                    }
                }

                if ( ( target === playerA || target === playerB ) && target === ownerTarget && this.isInCubicRange( player, ownerTarget ) ) {
                    return target
                }

                if ( partyEnemy ) {
                    if ( partyEnemy.getMembers().includes( ownerTarget.getObjectId() ) && this.isInCubicRange( player, ownerTarget ) ) {
                        return ownerTarget
                    }

                    return
                }
            }

            if ( playerA !== player && ownerTarget === playerA && this.isInCubicRange( player, ownerTarget ) ) {
                return playerA
            }

            if ( playerB !== player && ownerTarget === playerB && this.isInCubicRange( player, ownerTarget ) ) {
                return playerB
            }

            return null
        }

        if ( player.isInOlympiadMode() ) {
            if ( player.isOlympiadStart() && ownerTarget.isPlayable() ) {
                let targetPlayer: L2PcInstance = ownerTarget.getActingPlayer()

                if ( !targetPlayer ) {
                    return
                }

                if ( targetPlayer.getOlympiadGameId() === player.getOlympiadGameId()
                        && targetPlayer.getOlympiadSide() !== player.getOlympiadSide() ) {

                    if ( this.isInCubicRange( player, ownerTarget ) ) {
                        return ownerTarget
                    }

                    if ( this.isInCubicRange( player, targetPlayer ) ) {
                        return targetPlayer
                    }
                }
            }

            return null
        }

        let summonId = player.getSummon() ? player.getSummon().getObjectId() : 0
        if ( !ownerTarget.isCharacter()
                || ownerTarget.getObjectId() === summonId
                || ownerTarget.getObjectId() === player.getObjectId()
                || !this.isInCubicRange( player, ownerTarget ) ) {
            return null
        }

        if ( ownerTarget.isAttackable() && (
                AggroCache.hasAggro( ownerTarget.getObjectId(), player.getObjectId() )
                || AggroCache.hasAggro( ownerTarget.getObjectId(), summonId ) ) ) {
            return ownerTarget
        }

        let target: L2PcInstance = ownerTarget.getActingPlayer()

        if ( !target || !this.isInCubicRange( player, target ) ) {
            return null
        }

        let isPvPMode: boolean = ( player.hasPvPFlag() && !player.isInArea( AreaType.Peace ) ) || player.isInArea( AreaType.PVP )
        return ( isPvPMode && this.isCubicAttackTarget( target, player ) ) ? target : null
    }

    isCubicAttackTarget( target: L2PcInstance, player: L2PcInstance ): boolean {
        if ( !target.hasPvPFlag() && !target.isInArea( AreaType.PVP ) ) {
            return false
        }

        if ( target.isInArea( AreaType.Peace ) ) {
            return false
        }

        if ( player.getSiegeRole() !== SiegeRole.None && player.getSiegeRole() === target.getSiegeRole() ) {
            return false
        }

        if ( !target.isVisible() ) {
            return false
        }

        let party = player.getParty()
        if ( party ) {
            if ( party.getMembers().includes( target.getObjectId() ) ) {
                return false
            }

            let channel = party.getCommandChannel()
            if ( channel && channel.getMembers().includes( target.getObjectId() ) ) {
                return false
            }
        }

        let clan = player.getClan()
        if ( clan && !player.isInArea( AreaType.PVP ) ) {
            if ( clan.isMember( target.getObjectId() ) ) {
                return false
            }

            if ( player.getAllyId() > 0 && target.getAllyId() > 0 && player.getAllyId() === target.getAllyId() ) {
                return false
            }
        }

        return true
    }

    getCubicPower() {
        return this.power
    }

    getId() {
        return this.cubicId
    }

    getOwner(): L2PcInstance {
        return L2World.getPlayer( this.ownerId )
    }

    getOwnerId() {
        return this.ownerId
    }

    getSkills() {
        return this.skills
    }

    disEngage(): void {
        if ( this.healTask ) {
            clearInterval( this.healTask )
            this.healTask = null
        }

        this.debouncedAction.cancel()
        this.debouncedAction = null
        this.currentUseCount = 0
    }

    useCubicContinuous( skill: Skill, target: L2Character, player: L2PcInstance ): Promise<void> {
        if ( skill.isBad() ) {
            let shieldUse = Formulas.calculateShieldUse( player, target, skill )
            let shouldApply = Formulas.calculateCubicSkillSuccess( this, target, skill, shieldUse )
            if ( !shouldApply ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACK_FAILED ) )
                return
            }
        }

        return skill.applyEffects( player, target, false, false, true, 0 )
    }

    useCubicDisabler( skill: Skill, target: L2Character, player: L2PcInstance ): Promise<void> {
        let shieldUse = Formulas.calculateShieldUse( player, target, skill )

        if ( Formulas.calculateCubicSkillSuccess( this, target, skill, shieldUse ) ) {
            return skill.applyEffects( player, target, false, false, true, 0 )
        }
    }

    async useCubicDrain( skill: Skill, target: L2Character, player: L2PcInstance ): Promise<void> {
        if ( target.isAlikeDead() ) {
            return
        }

        let magicCrit = Formulas.calculateMagicCrit( player, target, skill )
        let shieldUse = Formulas.calculateShieldUse( player, target, skill )
        let damage = Formulas.calculateCubicMagicDamage( this, target, skill, magicCrit, shieldUse )

        let hpAdd = ( 0.4 * damage )
        let hp = ( ( player.getCurrentHp() + hpAdd ) > player.getMaxHp() ? player.getMaxHp() : ( player.getCurrentHp() + hpAdd ) )

        player.setCurrentHp( hp )

        if ( ( damage > 0 ) && !target.isDead() ) {
            await target.reduceCurrentHp( damage, player, skill )

            if ( !target.isRaid() && Formulas.calculateAttackBreak( target, damage ) ) {
                target.notifyAttackInterrupted()
                target.attemptCastStop()
            }

            player.sendDamageMessage( target, damage, magicCrit, false, false )
        }
    }

    async useCubicMagicDamage( skill: Skill, target: L2Character, player: L2PcInstance ): Promise<void> {
        if ( target.isAlikeDead() && target.isPlayer() ) {
            await target.stopFakeDeath( true )
        }

        let magicCrit = Formulas.calculateMagicCrit( player, target, skill )
        let shieldUse = Formulas.calculateShieldUse( player, target, skill )
        let damage = Formulas.calculateCubicMagicDamage( this, target, skill, magicCrit, shieldUse )

        if ( damage > 0 ) {
            if ( !target.isRaid() && Formulas.calculateAttackBreak( target, damage ) ) {
                target.notifyAttackInterrupted()
                target.attemptCastStop()
            }

            if ( target.calculateStat( Stats.VENGEANCE_SKILL_MAGIC_DAMAGE, 0, target, skill ) <= _.random( 100 ) ) {
                player.sendDamageMessage( target, damage, magicCrit, false, false )
                return target.reduceCurrentHp( damage, player, skill )
            }
        }
    }

    getActionSkill(): Skill {
        if ( this.skills.length === 1 ) {
            if ( this.chance > 0 ) {
                return _.random( 1, 100 ) < this.chance ? this.skills[ 0 ] : null
            }

            return this.skills[ 0 ]
        }

        if ( this.chance > 0 && _.random( 1, 100 ) > this.chance ) {
            return null
        }

        return _.sample( this.skills )
    }

    private onHealAction(): Promise<void> {
        let player: L2PcInstance = this.getOwner()
        if ( !this.healSkill || !player || player.isDead() || !player.isOnline() ) {
            this.onCubicEnd()
            return
        }

        let target: L2Character = this.findTargetForHeal( player )

        if ( !target || target.isDead() ) {
            return
        }

        // TODO : remove logging
        console.log( logSymbols.info, `Cubic heal action: target=${target.getName()} using skillId=${this.healSkill.getId()}` )

        BroadcastHelper.dataToSelfBasedOnVisibility( player, MagicSkillUseWithCharacters( player, target, this.healSkill.getId(), this.healSkill.getLevel(), 0, 0 ) )
        return this.healSkill.activateCasterSkill( player, [ target ], false )
    }

    private incrementUseCount() : void {
        this.currentUseCount++

        if ( this.maxUseCount > 0 && this.currentUseCount >= this.maxUseCount ) {
            return this.disEngage()
        }
    }

    private async onDamageAction(): Promise<void> {
        let player = this.getOwner()
        if ( !player || player.isDead() || !player.isOnline() ) {
            return this.onCubicEnd()
        }

        let useCubicCure: boolean = false
        if ( ( this.getId() >= L2CubicType.EvaTemplar ) && ( this.getId() <= L2CubicType.SpectralMaster ) ) {
            let buffsToRemove: Array<BuffInfo> = player.getEffectList().getDebuffs().filter( buff => !buff.getSkill().isIrreplaceableBuff() )
            if ( buffsToRemove.length > 0 ) {
                useCubicCure = true
                await Promise.all( buffsToRemove.map( ( info: BuffInfo ): Promise<void> => info.getAffected().getEffectList().stopSkillEffects( true, info.getSkill().getId() ) ) )
            }
        }

        if ( useCubicCure ) {
            BroadcastHelper.dataToSelfBasedOnVisibility( player, MagicSkillUseWithCharacters( player, player, CubicValues.CureSkillId, 1, 0, 0 ) )
            return this.incrementUseCount()
        }


        let skill = this.getActionSkill()
        if ( !skill ) {
            return
        }

        let target: L2Character = skill.getId() === CubicValues.HealSkillId ? this.findTargetForHeal( player ) : this.findEnemyTarget( player )
        if ( !target
                || target.isDead()
                || !target.isCharacter()
                || target.isAffectedBySkill( skill.getId() )
                || !this.canApply( target ) ) {
            return
        }

        // TODO : remove logging
        console.log( logSymbols.info, `Cubic damage action: target=${target.getName()} using skillId=${skill.getId()}` )
        BroadcastHelper.dataToSelfBasedOnVisibility( player, MagicSkillUseWithCharacters( player, target, skill.getId(), skill.getLevel(), 0, 0 ) )

        if ( skill.isContinuousType() ) {
            await this.useCubicContinuous( skill, target, player )
        } else {
            await skill.activateCasterSkill( player, [ target ], false )
        }

        this.incrementUseCount()

        if ( skill.hasEffectType( L2EffectType.MagicalAttack ) ) {
            return this.useCubicMagicDamage( skill, target, player )
        }

        if ( skill.hasEffectType( L2EffectType.HpDrain ) ) {
            return this.useCubicDrain( skill, target, player )
        }

        if ( skill.isDisabler() ) {
            return this.useCubicDisabler( skill, target, player )
        }

        if ( skill.hasEffectType( L2EffectType.DamageOverTime ) ) {
            return this.useCubicContinuous( skill, target, player )
        }
    }

    private onCubicEnd(): void {
        let player = this.getOwner()

        if ( player ) {
            player.removeCubic( this.getId() )
            player.broadcastUserInfo()

            return
        }

        return this.terminate()
    }

    terminate() : void {
        this.disEngage()
        this.stopAutomaticRemoval()
    }

    /*
        Mode of operation:
        - any cubic action is debounced with inactivation interval
        - heal action can be triggered by player being damaged
     */
    onHealTask(): void {
        let target: L2Character = this.findTargetForHeal( this.getOwner() )

        if ( target && !target.isDead() ) {
            this.activate()
        }
    }

    canApply( target: L2Character ) : boolean {
        if ( this.useChances.length === 0 ) {
            return true
        }

        let chance = 100 * Math.random()
        let hpRatio = target.getCurrentHp() / target.getMaxHp()

        if ( hpRatio > 0.6 ) {
            return chance <= this.useChances[ 0 ]
        }

        if ( hpRatio > 0.3 ) {
            return chance <= this.useChances[ 1 ]
        }

        return chance <= this.useChances[ 2 ]
    }
}