import { L2Npc } from '../L2Npc'
import { L2NpcTemplate } from '../templates/L2NpcTemplate'
import { InstanceType } from '../../../enums/InstanceType'
import { L2PcInstance } from './L2PcInstance'
import { ActionFailed } from '../../../packets/send/ActionFailed'
import { NpcHtmlMessagePath } from '../../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../../data/manager'
import { FourSepulchersManager } from '../../../cache/FourSepulchersManager'
import { clearTimeout } from 'timers'
import { DoorManager } from '../../../cache/DoorManager'
import Timeout = NodeJS.Timeout

const commonHtmlPath = 'data/html/SepulcherNpc/'
export const hallsKeyItemId: number = 7260
export const spawnMonsterIds: Array<number> = [ 31468, 31469, 31470, 31471, 31472, 31473, 31474, 31475, 31476, 31477, 31478, 31479, 31480, 31481, 31482, 31483, 31484, 31485, 31486, 31487 ]
export const addQuestKeyIds: Array<number> = [ 31455, 31456, 31457, 31458, 31459, 31460, 31461, 31462, 31463, 31464, 31465, 31466, 31467 ]

export class L2SepulcherNpcInstance extends L2Npc {

    closeTask: Timeout
    spawnMonsterTask: Timeout

    constructor( template: L2NpcTemplate ) {
        super( template )
        this.instanceType = InstanceType.L2SepulcherNpcInstance

        this.setShowSummonAnimation( true )
    }

    static fromTemplate( template: L2NpcTemplate ): L2SepulcherNpcInstance {
        return new L2SepulcherNpcInstance( template )
    }

    onSpawn() {
        super.onSpawn()
        this.setShowSummonAnimation( false )
    }

    stopMonsterTask() {
        if ( this.spawnMonsterTask ) {
            clearTimeout( this.spawnMonsterTask )
        }

        this.spawnMonsterTask = null
    }

    deleteMe(): Promise<void> {
        this.resetCloseDoorTask()
        this.stopMonsterTask()

        return super.deleteMe()
    }

    getHtmlPath( npcId: number, value: number ): string {
        let fileName: string = npcId.toString()

        if ( value !== 0 ) {
            fileName = `${ fileName }-${ value }`
        }

        return `${ commonHtmlPath }${ fileName }.htm`
    }

    showChatWindow( player: L2PcInstance, value: number = 0 ): void {
        let htmlPath = this.getHtmlPath( this.getId(), value )
        let html: string = DataManager.getHtmlData().getItem( htmlPath )

        player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), this.getObjectId() ) )
        player.sendOwnedData( ActionFailed() )
    }

    showHtml( player: L2PcInstance, path: string ) {
        let finalPath = `${ commonHtmlPath }${ path }`
        let html = DataManager.getHtmlData().getItem( finalPath )

        player.sendOwnedData( NpcHtmlMessagePath( html, finalPath, player.getObjectId(), this.getObjectId() ) )
    }

    runSpawnMonsterTask() {
        // TODO : implement
    }

    resetCloseDoorTask(): void {
        if ( this.closeTask ) {
            clearTimeout( this.closeTask )
            this.closeTask = null
        }
    }

    openNextDoor(): void {
        let doorId = FourSepulchersManager.getAssociatedDoorId( this.getId() )
        DoorManager.getDoor( doorId )?.openMe()

        this.resetCloseDoorTask()

        this.closeTask = setTimeout( this.runCloseNextDoor, 10000, doorId )
        FourSepulchersManager.spawnMysteriousBox( this.getId() )
    }

    runCloseNextDoor( doorId: number ): void {
        DoorManager.getDoor( doorId )?.closeMe()
    }

    restartSpawnMonsterTask() {
        this.stopMonsterTask()
        this.spawnMonsterTask = setTimeout( this.runSpawnMonsterTask.bind( this ), 3500 )
    }
}
