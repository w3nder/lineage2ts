import { L2Character } from './L2Character'
import { VehiclePathPoint } from '../VehiclePathPoint'
import { L2PcInstance } from './instance/L2PcInstance'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { L2Weapon } from '../items/L2Weapon'
import { L2Item } from '../items/L2Item'
import { L2World } from '../../L2World'
import { L2CharacterTemplate } from './templates/L2CharacterTemplate'
import { InstanceType } from '../../enums/InstanceType'
import { IDFactoryCache } from '../../cache/IDFactoryCache'
import { VehicleStats } from './stat/VehicleStat'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { ILocational, Location } from '../Location'
import { TeleportWhereType } from '../../enums/TeleportWhereType'
import { VehicleAIController } from '../../aicontroller/types/VehicleAIController'
import { TraitRegistry } from '../../aicontroller/TraitRegistry'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { MoveToLocationWithCharacter } from '../../packets/send/MoveToLocation'
import _ from 'lodash'
import { getTeleportLocation } from '../../helpers/TeleportHelper'

export class L2Vehicle extends L2Character {
    dockId: number
    passengers: Array<number> = []
    ejectLocation: Location

    // TODO : review and remove
    engine: Function

    currentPath: Array<VehiclePathPoint>
    runState: number
    aiController: VehicleAIController

    constructor( template: L2CharacterTemplate ) {
        super( IDFactoryCache.getNextId(), template )
        this.instanceType = InstanceType.L2Vehicle
        this.setIsFlying( true )
    }

    createStats(): VehicleStats {
        return new VehicleStats( this )
    }

    removePassenger( player: L2PcInstance ) {
        _.pull( this.passengers, player.getObjectId() )
    }

    ejectPlayer( player: L2PcInstance ) {
        player.setVehicle( null )
        player.setInVehiclePosition( null )
        this.removePassenger( player )
    }

    removePassengers() : void {
        let vehicle = this
        this.passengers.forEach( ( objectId: number ) => {
            let player = L2World.getPlayer( objectId )
            if ( player ) {
                vehicle.ejectPlayer( player )
            }
        } )

        this.passengers = []
    }

    isVehicle(): boolean {
        return true
    }

    getLevel(): number {
        return 0
    }

    isAutoAttackable(): boolean {
        return false
    }

    onUpdateAbnormalEffect(): void {}

    getActiveWeaponInstance(): L2ItemInstance {
        return null
    }

    getActiveWeaponItem(): L2Weapon {
        return null
    }

    getSecondaryWeaponInstance(): L2ItemInstance {
        return null
    }

    getSecondaryWeaponItem(): L2Item {
        return null
    }

    isInDock() {
        return this.dockId > 0
    }

    getStat(): VehicleStats {
        return super.getStat() as VehicleStats
    }

    // TODO : add logic to move worker to recognize type of data based on instance type
    runUpdateMovingParametersTask() {
        super.runUpdateMovingParametersTask()

        let vehicle = this
        _.each( this.passengers, ( playerId: number ) => {
            let player : L2PcInstance = L2World.getPlayer( playerId )
            if ( player && player.getVehicle().getObjectId() === vehicle.getObjectId() ) {
                player.setXYZ( vehicle.getX(), vehicle.getY(), vehicle.getZ() )
                player.revalidateZone( false )
            }
        } )
    }

    async moveToNextRoutePoint(): Promise<boolean> {

        if ( this.currentPath ) {
            this.runState++
            if ( this.runState < this.currentPath.length ) {
                let point : VehiclePathPoint = this.currentPath[ this.runState ]
                if ( !this.isMovementDisabled() ) {
                    if ( point.getMoveSpeed() === 0 ) {
                        point.setHeading( point.getRotationSpeed() )
                        await this.teleportToLocation( point, false )
                        this.currentPath = null
                    } else {
                        if ( point.getMoveSpeed() > 0 ) {
                            this.getStat().setMoveSpeed( point.getMoveSpeed() )
                        }
                        if ( point.getRotationSpeed() > 0 ) {
                            this.getStat().setRotationSpeed( point.getRotationSpeed() )
                        }

                        this.setDestinationCoordinates( point.getX(), point.getY(), point.getZ() )

                        // TODO : remove, heading setting should be in Moving Manager
                        let distance = Math.hypot( point.getX() - this.getX(), point.getY() - this.getY() )
                        if ( distance > 1 ) {
                            this.setHeading( GeneralHelper.calculateHeadingFromLocations( this, point ) )
                        }

                        this.startMoving()
                        return true
                    }
                }
            } else {
                this.currentPath = null
            }
        }

        this.runEngine( 10 )
        return false
    }

    executePath( destinations: Array<VehiclePathPoint> ) {
        this.runState = 0
        this.currentPath = destinations

        if ( !_.isEmpty( this.currentPath ) ) {
            let point : VehiclePathPoint = this.currentPath[ 0 ]
            if ( point.getMoveSpeed() > 0 ) {
                this.getStat().setMoveSpeed( point.getMoveSpeed() )
            }

            if ( point.getRotationSpeed() > 0 ) {
                this.getStat().setRotationSpeed( point.getRotationSpeed() )
            }

            return AIEffectHelper.notifyMove( this, point )
        }

        return AIEffectHelper.setNextIntent( this, AIIntent.WAITING )
    }

    runEngine( delay: number ) {
        if ( this.engine ) {
            setTimeout( this.engine, delay )
        }
    }

    getPlayerEjectLocation() : ILocational {
        return this.ejectLocation ? this.ejectLocation : getTeleportLocation( this, TeleportWhereType.TOWN )
    }


    detachAI() {

    }

    async deleteMe(): Promise<void> {
        this.engine = null

        this.abortMoving()
        this.removePassengers()
        this.decayMe()

        return super.deleteMe()
    }

    isEmpty() : boolean {
        return this.passengers.length === 0
    }

    async teleportToLocation( location: ILocational, allowRandomOffset: boolean, instanceId : number = 0 ) : Promise<void> {
        this.abortMoving()
        this.setIsTeleporting( true )

        _.each( this.passengers, ( playerId: number ) => {
            let player = L2World.getPlayer( playerId )
            if ( player ) {
                player.teleportToLocation( location, allowRandomOffset, instanceId )
            }
        } )

        this.decayMe()
        this.setXYZ( location.getX(), location.getY(), location.getZ() )
        this.setInstanceId( instanceId ? instanceId : location.getInstanceId() )

        if ( location.getHeading() !== 0 ) {
            this.setHeading( location.getHeading() )
        }

        await this.onTeleported()
        this.revalidateZone( true )
    }

    createAIController() : VehicleAIController {
        return new VehicleAIController( this.getObjectId(), TraitRegistry.getVehicleTraits( this ) )
    }

    getAIController() : VehicleAIController {
        if ( !this.aiController ) {
            this.aiController = this.createAIController()
        }

        return this.aiController
    }

    hasAIController() {
        return !!this.aiController
    }

    setAIController( value: VehicleAIController ) : void {
        this.aiController = value
    }

    async moveToNextRoutePointCoordinates( x: number, y: number, z: number ): Promise<boolean> {
        if ( !this.isOnGeodataPath() ) {
            return false
        }

        let speed = this.getMoveSpeed()
        if ( speed <= 0 ) {
            return false
        }

        this.setDestinationCoordinates( x, y, z )

        let distance = Math.hypot( x - this.getX(), y - this.getY() )
        if ( distance !== 0 ) {
            this.setHeading( GeneralHelper.calculateHeadingFromCoordinates( this.getX(), this.getY(), x, y ) )
        }

        BroadcastHelper.dataToSelfBasedOnVisibility( this, MoveToLocationWithCharacter( this ) )

        this.startMoving()
        return true
    }
}