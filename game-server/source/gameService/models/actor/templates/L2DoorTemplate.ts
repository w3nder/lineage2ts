import { L2CharacterTemplate } from './L2CharacterTemplate'

export class L2DoorTemplate extends L2CharacterTemplate {
    doorId: number
    nodeX: Array<number> = []
    nodeY: Array<number> = []
    nodeZ: number
    height: number
    position: [ number, number, number ]
    emitter: number
    childDoorId: number
    name: string
    groupName: string
    showHp: boolean
    isWall: boolean
    // -1 close, 0 nothing, 1 open
    masterDoorClose: number
    masterDoorOpen: number

    isTargetable: boolean
    defaultStatus: boolean

    openTime: number
    randomTime: number
    closeTime: number
    level: number
    openType: number
    checkCollision: number
    isAttackableDoor: boolean
    clanhallId: number
    stealth: number

    getId() : number {
        return this.doorId
    }

    getOpenType() {
        return this.openType
    }

    getLevel() {
        return this.level
    }

    getGroupName() {
        return this.groupName
    }

    getCloseTime() {
        return this.closeTime
    }

    isShowHp() {
        return this.showHp
    }

    getEmitter() {
        return this.emitter
    }

    getNodeX() {
        return this.nodeX
    }

    isCheckCollision() {
        return this.checkCollision
    }

    getNodeY() {
        return this.nodeY
    }

    getNodeZ() {
        return this.nodeZ
    }

    getHeight() {
        return this.height
    }

    isStealth() {
        return this.stealth
    }

    getClanHallId() {
        return this.clanhallId
    }

    getOpenTime() {
        return this.openType
    }

    getRandomTime() {
        return this.randomTime
    }

    getX() {
        return this.position[ 0 ]
    }

    getY() {
        return this.position[ 1 ]
    }

    getZ() {
        return this.position[ 2 ]
    }
}