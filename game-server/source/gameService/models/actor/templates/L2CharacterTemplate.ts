import { WeaponType } from '../../items/type/WeaponType'
import { Race } from '../../../enums/Race'
import { MoveType } from '../../stats/MoveType'
import { Skill } from '../../Skill'
import { AbstractFunction } from '../../stats/functions/AbstractFunction'
import _ from 'lodash'
import { GeometryId } from '../../../enums/GeometryId'

export type L2CharacterSkillMap = { [ skillId: number ]: Skill }

export class L2CharacterTemplate {
    baseSTR: number = 0
    baseCON: number = 0
    baseDEX: number = 0
    baseINT: number = 0
    baseWIT: number = 0
    baseMEN: number = 0
    baseHpMax: number = 0
    baseCpMax: number = 0
    baseMpMax: number = 0
    baseHpReg: number = 0
    baseMpReg: number = 0
    basePAtk: number = 0
    baseMAtk: number = 0
    basePDef: number = 0
    baseMDef: number = 0
    basePAtkSpd: number = 300
    baseMAtkSpd: number = 333
    baseAttackRange: number = 40
    randomDamage: number = 0
    baseAttackType: WeaponType = WeaponType.FIST
    baseShldDef: number = 0
    baseShldRate: number = 0
    baseCritRate: number = 0
    baseMCritRate: number = 0

    baseBreath: number = 100
    baseFire: number = 0
    baseWind: number = 0
    baseWater: number = 0
    baseEarth: number = 0
    baseHoly: number = 0
    baseDark: number = 0
    baseFireRes: number = 0
    baseWindRes: number = 0
    baseWaterRes: number = 0
    baseEarthRes: number = 0
    baseHolyRes: number = 0
    baseDarkRes: number = 0
    baseElementRes: number = 0

    collisionRadius: number = 0
    collisionHeight: number = 0
    fCollisionRadius: number = 0
    fCollisionHeight: number = 0

    moveType: { [key: number]: number } = {
        [ MoveType.RUN ]: 120,
        [ MoveType.WALK ]: 50,
        [ MoveType.FAST_SWIM ]: 120,
        [ MoveType.SLOW_SWIM ]: 50,
        [ MoveType.FAST_FLY ]: 120,
        [ MoveType.SLOW_FLY ]: 50
    }

    race: Race
    attackReuseDelay: number = 0
    dropGeometryId: GeometryId

    getBaseAttackRange() {
        return this.baseAttackRange
    }

    getBaseAttackType(): WeaponType {
        return this.baseAttackType
    }

    getBaseCON(): number {
        return this.baseCON
    }

    getBaseCritRate() {
        return this.baseCritRate
    }

    getBaseDEX(): number {
        return this.baseDEX
    }

    getBaseDark() {
        return this.baseDark
    }

    getBaseDarkRes() {
        return this.baseDarkRes
    }

    getBaseEarth() {
        return this.baseEarth
    }

    getBaseEarthRes() {
        return this.baseEarthRes
    }

    getBaseElementRes() {
        return this.baseElementRes
    }

    getBaseFire() {
        return this.baseFire
    }

    getBaseFireRes() {
        return this.baseFireRes
    }

    getBaseHoly() {
        return this.baseHoly
    }

    getBaseHolyRes() {
        return this.baseHolyRes
    }

    getBaseHpMax() {
        return this.baseHpMax
    }

    getBaseMpMax() {
        return this.baseMpMax
    }

    getBaseINT(): number {
        return this.baseINT
    }

    getBaseMEN(): number {
        return this.baseMEN
    }

    getBaseMagicAttack() {
        return this.baseMAtk
    }

    getBaseMagicAttackSpeed() {
        return this.baseMAtkSpd
    }

    getBaseMagicDefence() {
        return this.baseMDef
    }

    getBaseMoveSpeed( type: MoveType ) : number {
        return _.defaultTo( this.moveType[ type ], 1 )
    }

    getBasePowerAttack(): number {
        return this.basePAtk
    }

    getBasePowerAttackSpeed(): number {
        return this.basePAtkSpd
    }

    getBasePowerDefence() {
        return this.basePDef
    }

    getBaseSTR(): number {
        return this.baseSTR
    }

    getBaseWIT(): number {
        return this.baseWIT
    }

    getBaseWater() {
        return this.baseWater
    }

    getBaseWaterRes() {
        return this.baseWaterRes
    }

    getBaseWind() {
        return this.baseWind
    }

    getBaseWindRes() {
        return this.baseWindRes
    }

    getCollisionHeight(): number {
        return this.collisionHeight
    }

    getCollisionRadius(): number {
        return this.collisionRadius
    }

    getFCollisionHeight(): number {
        return this.fCollisionHeight
    }

    getFCollisionRadius(): number {
        return this.fCollisionRadius
    }

    getRace(): Race {
        return this.race
    }

    getSkills() : Array<Skill> {
        return
    }

    getSkillMap() : L2CharacterSkillMap {
        return
    }

    getCalculatorFunctions() : Array<AbstractFunction> {
        return
    }

    getPassiveSkills() : Array<Skill> {
        return
    }

    getHpRegeneration() {
        return this.baseHpReg
    }

    getMpRegeneration() {
        return this.baseMpReg
    }

    getAttackReuseDelay() {
        return this.attackReuseDelay
    }
}

export const emptyTemplate = new L2CharacterTemplate()