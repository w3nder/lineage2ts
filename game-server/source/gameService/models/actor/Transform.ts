import { L2PcInstance } from './instance/L2PcInstance'
import { AdditionalItem, TransformTemplate } from './transform/TransformTemplate'
import { TransformLevelData } from './transform/TransformLevelData'
import { ExBasicActionList } from '../../packets/send/ExBasicActionList'
import { SkillCache } from '../../cache/SkillCache'
import { Skill } from '../Skill'
import { TransformSkill } from '../../enums/TransformSkill'
import { InventoryBlockMode } from '../../enums/InventoryBlockMode'
import { TransformType } from '../../enums/TransformType'

export class Transform {
    id: number
    displayId: number
    type: TransformType
    canSwimValue: boolean = false
    spawnHeight: number
    canAttackValue: boolean = false
    name: string
    title: string

    maleTemplate: TransformTemplate
    femaleTemplate: TransformTemplate

    getTemplate( player: L2PcInstance ): TransformTemplate {
        return player.appearance.isFemale ? player.transformation.femaleTemplate : player.transformation.maleTemplate
    }

    getStat( player: L2PcInstance, statName: string ): number {
        let currentTemplate: TransformTemplate = this.getTemplate( player )

        if ( currentTemplate ) {
            let levelData: TransformLevelData = currentTemplate.data[ player.getLevel() ]
            if ( levelData ) {
                return levelData.getStats( statName )
            }

            return currentTemplate.getStats( statName )
        }

        return 0
    }

    getBaseAttackRange( player: L2PcInstance ): number {
        let currentTemplate = this.getTemplate( player )

        if ( currentTemplate ) {
            return currentTemplate.getBaseAttackRange()
        }

        return player.getTemplate().getBaseAttackRange()
    }

    getCollisionRadius( player: L2PcInstance ): number {
        let currentTemplate = this.getTemplate( player )

        if ( currentTemplate ) {
            return currentTemplate.getCollisionRadius()
        }

        return player.getCollisionRadius()
    }

    getCollisionHeight( player: L2PcInstance ): number {
        let currentTemplate = this.getTemplate( player )

        if ( currentTemplate ) {
            return currentTemplate.getCollisionHeight()
        }

        return player.getCollisionHeight()
    }

    getBaseDefenceBySlot( player: L2PcInstance, slot: number ) {
        let currentTemplate = this.getTemplate( player )

        if ( currentTemplate ) {
            return currentTemplate.getDefence( slot )
        }

        return player.getTemplate().getBaseDefBySlot( slot )
    }

    isStance() {
        return this.type === TransformType.MODE_CHANGE
    }

    getLevelModifier( player: L2PcInstance ): number {
        let template = this.getTemplate( player )
        if ( template ) {
            let data = template.getData( player.getLevel() )
            if ( data ) {
                return data.getLevelModifier()
            }
        }

        return -1
    }

    isFlying(): boolean {
        return this.type === TransformType.FLYING
    }

    getId() {
        return this.id
    }

    async onLevelUp( player: L2PcInstance ): Promise<void> {
        let template: TransformTemplate = this.getTemplate( player )

        if ( template && template.getAdditionalSkills().length > 0 ) {
            let promises : Array<Promise<Skill>> = []
            template.getAdditionalSkills().forEach( ( data: TransformSkill ) => {
                if ( player.getLevel() >= data.minimumLevel && player.getSkillLevel( data.skill.getId() ) < data.skill.getLevel() ) {
                    promises.push( player.addSkill( data.skill, false ) )
                    player.addTransformSkill( data.skill )
                }
            } )

            if ( promises.length > 0 ) {
                await Promise.all( promises )
            }
        }
    }

    getDisplayId() {
        return this.displayId
    }

    onTransform( player: L2PcInstance ) {
        let template: TransformTemplate = this.getTemplate( player )
        if ( template ) {
            if ( this.isFlying() ) {
                player.setIsFlying( true )
            }

            if ( this.getName() ) {
                player.getAppearance().setVisibleName( this.getName() )
            }
            if ( this.getTitle() ) {
                player.getAppearance().setVisibleTitle( this.getTitle() )
            }

            if ( template.getAdditionalItems().length > 0 ) {
                let allowed: Array<number> = []
                let notAllowed: Array<number> = []
                template.getAdditionalItems().forEach( ( data: AdditionalItem ) => {
                    if ( data.allowed ) {
                        allowed.push( data.itemId )
                    } else {
                        notAllowed.push( data.itemId )
                    }
                } )

                if ( allowed.length !== 0 ) {
                    player.getInventory().setInventoryBlock( allowed, InventoryBlockMode.AllowedSome )
                }

                if ( notAllowed.length !== 0 ) {
                    player.getInventory().setInventoryBlock( notAllowed, InventoryBlockMode.DisallowedSome )
                }
            }

            if ( template.hasBasicActionPacket() ) {
                player.sendCopyData( template.getBasicActionPacket() )
            }
        }
    }

    getName() {
        return this.name
    }

    getTitle() {
        return this.title
    }

    async onUntransform( player: L2PcInstance ) : Promise<void> {
        let template: TransformTemplate = this.getTemplate( player )
        if ( template ) {

            if ( this.isFlying() ) {
                player.setIsFlying( false )
            }

            if ( this.getName() ) {
                player.getAppearance().setVisibleName( null )
            }

            if ( this.getTitle() ) {
                player.getAppearance().setVisibleTitle( null )
            }

            let promises : Array<Promise<Skill>> = []

            template.getSkills().forEach( ( skill: Skill ) => {
                if ( !SkillCache.isSkillAllowed( player, skill ) ) {
                    promises.push( player.removeSkill( skill, false, skill.isPassive() ) )
                }
            } )

            template.getAdditionalSkills().forEach( ( data: TransformSkill ) => {
                if ( player.getLevel() >= data.minimumLevel && !SkillCache.isSkillAllowed( player, data.skill ) ) {
                    promises.push( player.removeSkill( data.skill, false, data.skill.isPassive() ) )
                }
            } )

            if ( promises.length > 0 ) {
                await Promise.all( promises )
            }

            player.removeAllTransformSkills()

            if ( template.getAdditionalItems().length > 0 ) {
                player.getInventory().unblock()
            }

            player.sendOwnedData( ExBasicActionList() )
        }
    }

    canSwim() {
        return this.canSwimValue
    }

    isCombat() {
        return this.type === TransformType.COMBAT
    }

    canAttack() {
        return this.canAttackValue
    }
}