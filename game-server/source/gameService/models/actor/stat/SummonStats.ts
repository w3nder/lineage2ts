import { L2Summon } from '../L2Summon'
import { PlayableStats } from './PlayableStats'

export class SummonStats extends PlayableStats {

    getActiveCharacter() : L2Summon {
        return this.character as L2Summon
    }

    getMaxBuffCount(): number {
        return this.getActiveCharacter().getOwner().getStat().getMaxBuffCount()
    }
}