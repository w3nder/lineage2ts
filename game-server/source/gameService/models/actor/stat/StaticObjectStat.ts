import { CharacterStats } from './CharacterStats'
import { L2StaticObjectInstance } from '../instance/L2StaticObjectInstance'

export class StaticObjectStat extends CharacterStats {
    getActiveCharacter(): L2StaticObjectInstance {
        return this.character as L2StaticObjectInstance
    }

    getLevel(): number {
        return this.getActiveCharacter().getLevel()
    }
}