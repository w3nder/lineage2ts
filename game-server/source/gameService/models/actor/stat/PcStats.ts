import { PlayableStats } from './PlayableStats'
import { Stats } from '../../stats/Stats'
import { L2PcInstance } from '../instance/L2PcInstance'
import { MoveType } from '../../stats/MoveType'
import { TransformTemplate } from '../transform/TransformTemplate'
import { L2PetLevelData } from '../../L2PetLevelData'
import { DataManager } from '../../../../data/manager'
import { RecommendationBonus } from '../../../../data/RecommendationBonus'
import { ConfigManager } from '../../../../config/ConfigManager'
import { SystemMessageIds } from '../../../packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../../packets/send/SystemMessage'
import { VitalityLevel, VitalityPointsPerLevel } from '../../../enums/VitalityLevels'
import { ExVitalityPointInfo } from '../../../packets/send/ExVitalityPointInfo'
import { PlayerVariablesManager } from '../../../variables/PlayerVariablesManager'
import { MultipliersVariableNames, StatsMultipliersPropertyName } from '../../../values/L2PcValues'
import { NevitManager } from '../../../cache/NevitManager'
import _ from 'lodash'
import { ListenerCache } from '../../../cache/ListenerCache'
import { EventType, PlayerVitalityChangedEvent } from '../../events/EventType'
import { EventPoolCache } from '../../../cache/EventPoolCache'

export class PcStats extends PlayableStats {
    oldMaxHp: number
    oldMaxMp: number
    oldMaxCp: number
    vitalityPoints: number = 1
    vitalityLevel: VitalityLevel = VitalityLevel.None
    startingXp: number = 0
    maxCubicCount: number = 0
    talismanSlots: number = 0
    cloakSlot: boolean = false

    character: L2PcInstance

    addTalismanSlots( value: number ) {
        this.talismanSlots += value
    }

    canEquipCloak() {
        return this.cloakSlot
    }

    getActiveCharacter(): L2PcInstance {
        return super.getActiveCharacter() as L2PcInstance
    }

    getBaseMoveSpeed( type: MoveType ): number {
        let player: L2PcInstance = this.getActiveCharacter()
        if ( player.isTransformed() ) {
            let template: TransformTemplate = player.getTransformation().getTemplate( player )
            if ( template ) {
                return template.getBaseMoveSpeed( type )
            }
        } else if ( player.isMounted() ) {
            let data: L2PetLevelData = DataManager.getPetData().getPetLevelData( player.getMountNpcId(), player.getMountLevel() )
            if ( data ) {
                return data.getSpeedOnRide( type )
            }
        }

        return super.getBaseMoveSpeed( type )
    }

    getMaxCp(): number {
        let player = this.getActiveCharacter()
        let value = !player ? 1 : Math.floor( this.calculateStat( Stats.MAX_CP, player.getTemplate().getBaseCpMaxByLevel( player.getLevel() ) ) )
        if ( value !== this.oldMaxCp ) {
            this.oldMaxCp = value

            // TODO : figure out better way to trigger start of regeneration
            if ( player.getStatus().getCurrentCp() !== value ) {
                player.getStatus().setCurrentCp( player.getStatus().getCurrentCp() )
            }
        }

        return value
    }

    getMaxHp(): number {
        let player = this.getActiveCharacter()
        let value = !player ? 1 : Math.floor( this.calculateStat( Stats.MAX_HP, player.getTemplate().getBaseHpMaxByLevel( player.getLevel() ) ) )
        if ( value !== this.oldMaxHp ) {
            this.oldMaxHp = value

            // TODO : figure out better way to trigger start of regeneration
            if ( player.getStatus().getCurrentHp() !== value ) {
                player.getStatus().setCurrentHp( player.getStatus().getCurrentHp() )
            }
        }

        return value
    }

    getMaxMp(): number {
        let player = this.getActiveCharacter()
        let value = !player ? 1 : Math.floor( this.calculateStat( Stats.MAX_MP, player.getTemplate().getBaseMpMaxByLevel( player.getLevel() ) ) )
        if ( value !== this.oldMaxMp ) {
            this.oldMaxMp = value

            // TODO : figure out better way to trigger start of regeneration
            if ( player.getStatus().getCurrentMp() !== value ) {
                player.getStatus().setCurrentMp( player.getStatus().getCurrentMp() )
            }
        }

        return value
    }

    getExpBonusMultiplier() {
        let bonus = 1

        let vitality = this.getVitalityMultiplier()
        let recommendationMultiplier = RecommendationBonus.getMultiplier( this.getActiveCharacter() )
        let bonusExp = 1 + ( this.calculateStat( Stats.BONUS_EXP, 0, null, null ) / 100 )
        let playerCharacterMultiplier = _.get( PlayerVariablesManager.get( this.getActiveCharacterId(), StatsMultipliersPropertyName ), MultipliersVariableNames.exp, 0 ) as number
        let nevitBlessing = NevitManager.isBlessingActive( this.character.getObjectId() ) ? ConfigManager.character.getNevitBlessedExpBonus() : 0

        if ( vitality > 1 ) {
            bonus += ( vitality - 1 )
        }

        if ( recommendationMultiplier > 1 ) {
            bonus += ( recommendationMultiplier - 1 )
        }

        if ( bonusExp > 1 ) {
            bonus += ( bonusExp - 1 )
        }

        return Math.min( bonus + playerCharacterMultiplier + nevitBlessing, ConfigManager.character.getMaxExpBonus() )
    }

    getMaxCubicCount() {
        return this.maxCubicCount
    }

    getSpBonusMultiplier(): number {
        let bonus = 1
        let vitality = this.getVitalityMultiplier()
        let recommendationMultiplier = RecommendationBonus.getMultiplier( this.getActiveCharacter() )
        let nevitBlessing = NevitManager.isBlessingActive( this.character.getObjectId() ) ? ConfigManager.character.getNevitBlessedSpBonus() : 0

        let bonusSp = 1 + ( this.calculateStat( Stats.BONUS_SP, 0, null, null ) / 100 )
        let playerCharacterMultiplier = _.get( PlayerVariablesManager.get( this.getActiveCharacterId(), StatsMultipliersPropertyName ), MultipliersVariableNames.sp, 0 ) as number

        if ( vitality > 1 ) {
            bonus += ( vitality - 1 )
        }

        if ( recommendationMultiplier > 1 ) {
            bonus += ( recommendationMultiplier - 1 )
        }

        if ( bonusSp > 1 ) {
            bonus += ( bonusSp - 1 )
        }

        return Math.min( bonus + playerCharacterMultiplier + nevitBlessing, ConfigManager.character.getMaxSpBonus() )
    }

    getTalismanSlots() {
        return this.talismanSlots
    }

    getVitalityLevel() {
        if ( NevitManager.isBlessingActive( this.character.getObjectId() ) ) {
            return VitalityLevel.Four
        }

        return this.vitalityLevel
    }

    getVitalityMultiplier(): number {
        let playerMultiplier = _.get( PlayerVariablesManager.get( this.getActiveCharacterId(), StatsMultipliersPropertyName ), MultipliersVariableNames.vitality, 0 ) as number
        if ( ConfigManager.vitality.enabled() ) {
            switch ( this.getVitalityLevel() ) {
                case 1:
                    return ConfigManager.vitality.getRateVitalityLevel1() + playerMultiplier
                case 2:
                    return ConfigManager.vitality.getRateVitalityLevel2() + playerMultiplier
                case 3:
                    return ConfigManager.vitality.getRateVitalityLevel3() + playerMultiplier
                case 4:
                    return ConfigManager.vitality.getRateVitalityLevel4() + playerMultiplier
            }
        }

        return 1
    }

    getVitalityPoints() {
        return this.vitalityPoints
    }

    setCloakSlotStatus( value: boolean ) {
        this.cloakSlot = value
    }

    setMaxCubicCount( value: number ) {
        this.maxCubicCount = value
    }

    setVitalityPoints( points: number, sendUpdate: boolean ) : void {
        let adjustedPoints = Math.min( Math.max( points, VitalityPointsPerLevel.None ), VitalityPointsPerLevel.Top )
        if ( adjustedPoints === this.vitalityPoints ) {
            return
        }

        this.vitalityPoints = Math.floor( adjustedPoints )
        this.updateVitalityLevel( sendUpdate )

        if ( this.character.canBroadcastUpdate() ) {
            this.character.sendOwnedData( ExVitalityPointInfo( adjustedPoints ) )
        }
    }

    private computeVitalityLevel() : VitalityLevel {
        if ( this.vitalityPoints <= VitalityPointsPerLevel.One ) {
            return VitalityLevel.None
        }

        if ( this.vitalityPoints <= VitalityPointsPerLevel.Two ) {
            return VitalityLevel.One
        }

        if ( this.vitalityPoints <= VitalityPointsPerLevel.Three ) {
            return VitalityLevel.Two
        }

        if ( this.vitalityPoints <= VitalityPointsPerLevel.Four ) {
            return VitalityLevel.Three
        }

        return VitalityLevel.Four
    }

    updateVitalityLevel( sendUpdate: boolean ) : void {
        if ( !sendUpdate ) {
            this.vitalityLevel = this.computeVitalityLevel()
            return
        }

        let level = this.computeVitalityLevel()
        if ( level !== this.vitalityLevel ) {
            let messageId = level < this.vitalityLevel ? SystemMessageIds.VITALITY_HAS_DECREASED : SystemMessageIds.VITALITY_HAS_INCREASED

            this.character.sendOwnedData( SystemMessageBuilder.fromMessageId( messageId ) )

            switch ( level ) {
                case VitalityLevel.None:
                    this.character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.VITALITY_IS_EXHAUSTED ) )
                    break

                case VitalityLevel.Four:
                    this.character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.VITALITY_IS_AT_MAXIMUM ) )
                    break
            }
        }

        this.vitalityLevel = level
    }

    updateVitalityPoints( initialPoints: number, useRates: boolean, sendUpdate: boolean ): void {
        if ( initialPoints === 0 || !ConfigManager.vitality.enabled() ) {
            return
        }

        let updatedPoints = initialPoints

        if ( useRates ) {

            let player = this.getActiveCharacter()
            if ( player.isLucky() ) {
                return
            }

            if ( initialPoints < 0 ) {
                let value = NevitManager.isBlessingActive( this.character.getObjectId() ) ? -10 : Math.floor( this.calculateStat( Stats.VITALITY_CONSUME_RATE, 1, player, null ) )

                if ( value === 0 ) {
                    return
                }

                if ( value < 0 ) {
                    let multiplier = _.get( PlayerVariablesManager.get( this.getActiveCharacterId(), StatsMultipliersPropertyName ), MultipliersVariableNames.vitalityPoints, 1 ) as number
                    updatedPoints = Math.abs( initialPoints ) * multiplier
                }
            }

            if ( updatedPoints > 0 ) {
                updatedPoints *= ConfigManager.vitality.getRateVitalityGain()
            } else {
                updatedPoints *= ConfigManager.vitality.getRateVitalityLost()
            }
        }

        updatedPoints = Math.floor( updatedPoints )

        if ( updatedPoints > 0 ) {
            updatedPoints = Math.min( this.vitalityPoints + updatedPoints, VitalityPointsPerLevel.Top )
        } else {
            updatedPoints = Math.max( this.vitalityPoints + updatedPoints, VitalityPointsPerLevel.None )
        }

        if ( updatedPoints === this.vitalityPoints ) {
            return
        }

        if ( ListenerCache.hasGeneralListener( EventType.PlayerVitalityChanged ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerVitalityChanged ) as PlayerVitalityChangedEvent

            eventData.playerId = this.character.getObjectId()
            eventData.previousPoints = this.vitalityPoints
            eventData.nextPoints = updatedPoints

            ListenerCache.sendGeneralEvent( EventType.PlayerVitalityChanged, eventData )
        }

        this.vitalityPoints = updatedPoints
        this.updateVitalityLevel( sendUpdate )
    }

    getStartingExp() {
        return this.startingXp
    }

    getMaxExpLevel(): number {
        return this.getActiveCharacter().isSubClassActive() ?
                ConfigManager.character.getMaxSubclassLevel() + 1 : ConfigManager.character.getMaxPlayerLevel() + 1
    }

    getMaxLevel(): number {
        return this.getActiveCharacter().isSubClassActive() ?
                ConfigManager.character.getMaxSubclassLevel() : ConfigManager.character.getMaxPlayerLevel()
    }
}