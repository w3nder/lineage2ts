import { CharacterStats } from './CharacterStats'

export class VehicleStats extends CharacterStats {
    moveSpeed: number = 0
    rotationSpeed: number = 0

    getMoveSpeed(): number {
        return this.moveSpeed
    }

    setMoveSpeed( value: number ) {
        this.moveSpeed = value
    }

    getRotationSpeed() {
        return this.rotationSpeed
    }

    setRotationSpeed( value: number ) {
        this.rotationSpeed = value
    }
}