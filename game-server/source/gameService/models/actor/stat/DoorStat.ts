import { CharacterStats } from './CharacterStats'
import { L2DoorInstance } from '../instance/L2DoorInstance'

export class DoorStat extends CharacterStats {
    upgradeHpRatio: number = 1

    getActiveCharacter(): L2DoorInstance {
        return this.character as L2DoorInstance
    }

    getMaxHp(): number {
        return super.getMaxHp() * this.upgradeHpRatio
    }

    setUpgradeHpRatio( value: number ) {
        this.upgradeHpRatio = value
    }

    getUpgradeHpRatio() {
        return this.upgradeHpRatio
    }
}