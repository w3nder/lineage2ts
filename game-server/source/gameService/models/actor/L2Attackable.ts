import { L2Npc } from './L2Npc'
import { L2Seed } from '../L2Seed'
import { L2CommandChannel } from '../L2CommandChannel'
import { L2Character } from './L2Character'
import { L2PcInstance } from './instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { L2Party } from '../L2Party'
import { L2ServitorInstance } from './instance/L2ServitorInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2World } from '../../L2World'
import { L2NpcTemplate } from './templates/L2NpcTemplate'
import { DecayTaskManager } from '../../taskmanager/DecayTaskManager'
import { L2Item } from '../items/L2Item'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { Skill } from '../Skill'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { InstanceType } from '../../enums/InstanceType'
import { L2MonsterInstance } from './instance/L2MonsterInstance'
import { AttackableStatus } from './status/AttackableStatus'
import { CursedWeaponManager } from '../../instancemanager/CursedWeaponManager'
import { DataManager } from '../../../data/manager'
import { AdditionalDropManager, CustomDrop } from '../../cache/AdditionalDropManager'
import { L2Object } from '../L2Object'
import { ListenerCache } from '../../cache/ListenerCache'
import {
    AttackableAttackedEvent,
    AttackableInitialAttackEvent,
    AttackableKillEvent,
    EventType,
    NpcHasDropItemsEvent,
    NpcHasSweepItemsEvent,
} from '../events/EventType'
import { ProximalDiscoveryManager } from '../../cache/ProximalDiscoveryManager'
import { L2Summon } from './L2Summon'
import { L2Playable } from './L2Playable'
import { DimensionalRiftManager } from '../../instancemanager/DimensionalRiftManager'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { PlayerGroupCache } from '../../cache/PlayerGroupCache'
import { AggroCache, AggroData, CharacterAggroData } from '../../cache/AggroCache'
import { ItemData, ItemDataHelper } from '../../interface/ItemData'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import _ from 'lodash'
import { isGlobalAutolootEnabled, isPlayerAutolootEnabled, shouldDropChampionLoot } from '../../helpers/LootHelper'
import { TraitRunnerAIController } from '../../aicontroller/types/TraitRunnerAIController'
import { ObjectPool } from '../../helpers/ObjectPoolHelper'
import { PathFinding } from '../../../geodata/PathFinding'
import { PointGeometry } from '../drops/PointGeometry'
import { AreaDiscoveryManager } from '../../cache/AreaDiscoveryManager'
import { AreaType } from '../areas/AreaType'
import { PlayerPermission } from '../../enums/PlayerPermission'
import Timeout = NodeJS.Timeout
import { DropListScope } from '../../enums/drops/DropListScope'
import { IDropItem } from '../drops/IDropItem'
import { NpcSayType } from '../../enums/packets/NpcSayType'

interface DamageDoneInfo {
    attackerId: number
    totalDamage: number
}

export interface SkillUsageParameters {
    index: number
}

const damageDonePool = new ObjectPool( 'DamageDoneInfo', () : DamageDoneInfo => {
    return {
        attackerId: 0,
        totalDamage: 0
    }
} )

const skillUsageObjectPool = new ObjectPool( 'SkillUsageParameters', () : SkillUsageParameters => {
    return {
        index: 0
    }
} )

const harvestSkillMultipliers: { [ skillId: number ]: number } = {
    4303: 2, // Strong type x2
    4304: 3, // Strong type x3
    4305: 4, // Strong type x4
    4306: 5, // Strong type x5
    4307: 6, // Strong type x6
    4308: 7, // Strong type x7
    4309: 8, // Strong type x8
    4310: 9, // Strong type x9
}

export class L2Attackable extends L2Npc {
    raid: boolean = false
    raidMinion: boolean = false
    champion: boolean = false

    // TODO : review/remove since flags belong to AIController
    isReturningToSpawnPointValue: boolean = false
    canReturnToSpawnPointValue: boolean = true

    seeThroughSilentMove: boolean = false
    seeded: boolean = false
    seed: L2Seed = null
    seederObjectId: number = 0
    harvestItem: ItemData

    spoilerObjectId: number = 0
    sweepItems: Array<ItemData> = null

    overhit: boolean
    overhitDamage: number
    overhitAttacker: number

    // TODO : replace with command channel id
    firstCommandChannelAttacked: L2CommandChannel
    commandChannelTimer: Timeout
    commandChannelLastAttack: number = 0
    mustGiveExpSp: boolean
    private skillUsage: SkillUsageParameters
    private dropGeometry: PointGeometry

    constructor( template: L2NpcTemplate ) {
        super( template )

        this.instanceType = InstanceType.L2Attackable
        this.isInvulnerableValue = false
        this.mustGiveExpSp = true
    }

    onSpawn() {
        super.onSpawn()
        this.setSpoilerObjectId( 0 )
        this.clearOverhitData()
        this.emptyHarvestItem()

        this.seeded = false
        this.seed = null
        this.seederObjectId = 0

        this.overhitEnabled( false )
        this.setWalking()
        this.emptySweepItems()

        if ( this.npcSpawn && this.npcSpawn.isMinion( this ) ) {
            let leaderId = this.npcSpawn?.getMinionLeaderId( this.getObjectId() ) ?? 0
            let leader = L2World.getObjectById( leaderId )
            this.raidMinion = leader && ( leader as L2Attackable ).isRaid()
        }
    }

    onDecay(): void {
        AggroCache.clearData( this.getObjectId() )

        if ( this.skillUsage ) {
            skillUsageObjectPool.recycleValue( this.skillUsage )
            this.skillUsage = null
        }

        if ( !this.getEffectList().isEmpty() ) {
            this.stopAllEffects()
        }

        if ( this.isMoveCapable() ) {
            AreaDiscoveryManager.reset( this.getObjectId() )
        }

        return super.onDecay()
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( !await super.doDie( killer ) ) {
            return false
        }

        if ( killer
                && killer.isPlayable()
                && ListenerCache.hasNpcTemplateListeners( this.getId(), EventType.AttackableKilled ) ) {

            let eventData = EventPoolCache.getData( EventType.AttackableKilled ) as AttackableKillEvent

            eventData.isChampion = this.isChampion()
            eventData.playerId = killer.getActingPlayerId()
            eventData.attackerId = killer.getObjectId()
            eventData.targetId = this.getObjectId()
            eventData.npcId = this.getId()
            eventData.instanceId = this.getInstanceId()

            await ListenerCache.sendNpcTemplateEvent( this.getId(), EventType.AttackableKilled, eventData )
        }

        return true
    }

    calculateExpAndSp( levelDifference: number, damage: number, totalDamage: number ): [ number, number ] {
        if ( levelDifference < -5 ) {
            levelDifference = -5
        }

        let xp = ( this.getExpReward() * damage ) / totalDamage
        if ( ConfigManager.character.getExponentXp() ) {
            xp *= Math.pow( 2., -levelDifference / ConfigManager.character.getExponentXp() )
        }

        let sp = ( this.getSpReward() * damage ) / totalDamage
        if ( ConfigManager.character.getExponentSp() ) {
            sp *= Math.pow( 2., -levelDifference / ConfigManager.character.getExponentSp() )
        }

        if ( ConfigManager.character.getExponentXp() && ConfigManager.character.getExponentSp() ) {
            if ( levelDifference > 5 ) {
                let modifier = Math.pow( 5 / 6, levelDifference - 5 )
                xp = xp * modifier
                sp = sp * modifier
            }

            if ( xp <= 0 ) {
                xp = 0
                sp = 0
            } else if ( sp <= 0 ) {
                sp = 0
            }
        }

        return [ Math.floor( xp ), Math.floor( sp ) ]
    }

    calculateOverhitExp( normalExp: number ) {
        // Get the percentage based on the total of extra (over-hit) damage done relative to the total (maximum) amount of HP on the L2Attackable
        let overhitPercentage = ( this.getOverhitDamage() * 100 ) / this.getMaxHp()

        // Over-hit damage percentages are limited to 25% max
        if ( overhitPercentage > 25 ) {
            overhitPercentage = 25
        }

        // Get the overhit exp bonus according to the above over-hit damage percentage
        // (1/1 basis - 13% of over-hit damage, 13% of extra exp is given, and so on...)
        return Math.round( ( overhitPercentage / 100 ) * normalExp )
    }

    async calculateRewards( lastAttacker: L2Character ) {
        let aggroData: CharacterAggroData = AggroCache.getOwnedData( this.getObjectId() )
        if ( _.isEmpty( aggroData ) ) {
            return
        }

        let rewards: Array<DamageDoneInfo> = this.getMustRewardExpSP() ? [] : null
        let maxDealer: L2PcInstance
        let maxDamage = 0
        let totalDamage = 0

        _.each( aggroData, ( info: AggroData ) => {
            if ( !info || info.totalDamage <= 1 ) {
                return
            }

            let aggroAttacker = L2World.getObjectById( info.attackerId )
            if ( !aggroAttacker ) {
                return
            }

            let player: L2PcInstance = aggroAttacker.getActingPlayer()
            if ( !player ) {
                return
            }

            if ( !ProximalDiscoveryManager.isDiscoveredBy( player.getObjectId(), this.getObjectId() )
                    || !GeneralHelper.checkIfInRange( ConfigManager.character.getPartyRange(), this, player, true ) ) {
                return
            }

            totalDamage += info.totalDamage

            if ( info.totalDamage > maxDamage ) {
                maxDealer = player
                maxDamage = info.totalDamage
            }

            if ( !rewards ) {
                return
            }

            let rewardData = damageDonePool.getValue()

            rewardData.attackerId = player.getObjectId()
            rewardData.totalDamage = info.totalDamage

            rewards.push( rewardData )
        } )


        let attacker = maxDealer ? ( maxDealer.isOnline() ? maxDealer : lastAttacker ) : lastAttacker

        await this.dropRewardItems( attacker )

        if ( !rewards ) {
            return
        }

        await Promise.all( rewards.map( ( reward: DamageDoneInfo ) : Promise<void> => {
            if ( !reward ) {
                return
            }

            let attacker: L2PcInstance = L2World.getPlayer( reward.attackerId )
            if ( !attacker ) {
                return
            }

            let attackerParty: L2Party = PlayerGroupCache.getParty( reward.attackerId )
            if ( !attackerParty ) {
                // Calculate the difference of level between this attacker (player or servitor owner) and the L2Attackable
                // mob = 24, atk = 10, diff = -14 (full xp)
                // mob = 24, atk = 28, diff = 4 (some xp)
                // mob = 24, atk = 50, diff = 26 (no xp)

                let damage = reward.totalDamage
                let levelDifference = attacker.getLevel() - this.getLevel()

                let [ exp, sp ] = this.calculateExpAndSp( levelDifference, damage, totalDamage )

                if ( this.isChampion() && ConfigManager.customs.championEnable() ) {
                    exp *= ConfigManager.customs.getChampionRewardsExpSp()
                    sp *= ConfigManager.customs.getChampionRewardsExpSp()
                }

                exp *= attacker.hasServitor() ? ( attacker.getSummon() as L2ServitorInstance ).getExpMultiplier() : 1

                let overhitAttacker: L2Character = this.getOverhitAttacker()
                if ( this.isOverhit()
                        && overhitAttacker
                        && overhitAttacker.getActingPlayerId() > 0
                        && ( attacker.objectId === overhitAttacker.getActingPlayerId() ) ) {
                    attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.OVER_HIT ) )
                    exp += this.calculateOverhitExp( exp )
                }

                if ( !attacker.isDead() ) {
                    if ( exp > 0 ) {
                        attacker.updateVitalityPoints( this.getVitalityPoints( damage ), true, false )
                    }

                    return attacker.processAddExpAndSp( exp, sp, this.useVitalityRate() )
                }

                return
            }

            let partyDamage = 0
            let partyMultiplier = 1
            let partyLevel = 0

            let rewardedMembers: Array<number> = []
            let groupMembers: Array<number> = attackerParty.isInCommandChannel() ? attackerParty.getCommandChannel().getMembers() : attackerParty.getMembers()

            groupMembers.forEach( ( playerId: number ) => {
                let partyPlayer = L2World.getPlayer( playerId )
                if ( !partyPlayer || partyPlayer.isDead() ) {
                    return
                }

                if ( GeneralHelper.checkIfInRange( ConfigManager.character.getPartyRange(), this, partyPlayer, true ) ) {
                    rewardedMembers.push( playerId )

                    let currentReward: DamageDoneInfo = rewards[ playerId ]
                    if ( currentReward ) {
                        partyDamage += currentReward.totalDamage
                    }

                    if ( partyPlayer.getLevel() > partyLevel ) {
                        if ( attackerParty.isInCommandChannel() ) {
                            partyLevel = attackerParty.getCommandChannel().getLevel()
                        } else {
                            partyLevel = partyPlayer.getLevel()
                        }
                    }
                }
            } )

            if ( partyDamage < totalDamage ) {
                partyMultiplier = partyDamage / totalDamage
            }

            let levelDiff = partyLevel - this.getLevel()
            let [ exp, sp ]: [ number, number ] = this.calculateExpAndSp( levelDiff, partyDamage, totalDamage )

            if ( ConfigManager.customs.championEnable() && this.isChampion() ) {
                exp *= ConfigManager.customs.getChampionRewardsExpSp()
                sp *= ConfigManager.customs.getChampionRewardsExpSp()
            }

            exp *= partyMultiplier
            sp *= partyMultiplier

            // Check for an over-hit enabled strike
            // (When in party, the over-hit exp bonus is given to the whole party and split proportionally through the party members)
            let overhitAttacker: L2Character = this.getOverhitAttacker()
            if ( this.isOverhit()
                    && overhitAttacker
                    && reward.attackerId === overhitAttacker.getActingPlayerId() ) {
                attacker.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.OVER_HIT ) )
                exp += this.calculateOverhitExp( exp )
            }

            if ( partyDamage > 0 ) {
                return attackerParty.distributeXpAndSp( exp, sp, rewardedMembers, partyLevel, partyDamage, this )
            }
        } ) )

        damageDonePool.recycleValues( rewards )
    }

    isChampion(): boolean {
        return this.champion
    }

    isRaid(): boolean {
        return this.raid
    }

    isSweepActive(): boolean {
        return this.sweepItems && this.sweepItems.length > 0
    }

    clearOverhitData(): void {
        this.overhit = false
        this.overhitDamage = 0
        this.overhitAttacker = null
    }

    async doAdditionalDrop( lastAttacker: L2Character ) : Promise<void> {
        if ( !lastAttacker ) {
            return
        }

        let player: L2PcInstance = lastAttacker.getActingPlayer()

        if ( !player ) {
            return
        }

        if ( ( player.getLevel() - this.getLevel() ) > 9 ) {
            return
        }

        let autoLootPromises : Array<Promise<unknown>> = []

        const performDrop = ( drop: CustomDrop ) => {
            if ( _.random( 0, 100, true ) >= drop.chance ) {
                return
            }

            let itemCount = _.random( drop.minCount, drop.maxCount )

            if ( ConfigManager.character.autoLoot()
                    || ( player.isAutoLootEnabled() && !player.shouldLootItem() )
                    || ( player.shouldLootItem() && ConfigManager.customs.getAutoLootItemsList().includes( drop.itemId ) ) ) {
                autoLootPromises.push( player.doAutoLoot( this, drop.itemId, itemCount ) )
            }

            this.dropItem( player.getObjectId(), drop.itemId, itemCount, this.dropGeometry )
        }

        _.each( AdditionalDropManager.getGenericDrops(), performDrop )
        _.each( AdditionalDropManager.getTemplateDrops( this.getId() ), performDrop )

        if ( autoLootPromises.length > 0 ) {
            await Promise.all( autoLootPromises )
        }
    }

    async doItemDrop( character: L2Character ) : Promise<void> {
        return this.performItemDrop( this.getTemplate(), character )
    }

    getMustRewardExpSP() {
        return this.mustGiveExpSp
    }

    getOverhitAttacker(): L2Character {
        return L2World.getObjectById( this.overhitAttacker ) as L2Character
    }

    getOverhitDamage(): number {
        return this.overhitDamage
    }

    getVitalityPoints( damage: number ) : number {
        if ( damage <= 0 || this.getTemplate().getVitalityPointsPerHp() === 0 ) {
            return 0
        }

        return -Math.floor( Math.min( this.getMaxHp() , damage ) * this.getTemplate().getVitalityPointsPerHp() )
    }

    isOverhit(): boolean {
        return this.overhit
    }

    async performItemDrop( template: L2NpcTemplate, character: L2Character ) {
        if ( !character ) {
            return
        }

        let player: L2PcInstance = character.getActingPlayer()
        if ( !player ) {
            return
        }

        if ( CursedWeaponManager.isDropReady( this ) ) {
            await CursedWeaponManager.performDrop( this, player )
        }

        if ( this.isSpoiled() ) {
            this.sweepItems = this.calculateDrops( template, DropListScope.CORPSE, player )

            AdditionalDropManager.addSpoils( this.getId(), this.sweepItems )

            if ( this.sweepItems.length > 0 && ListenerCache.hasNpcTemplateListeners( template.getId(), EventType.NpcHasSweepItems ) ) {
                let eventData = EventPoolCache.getData( EventType.NpcHasSweepItems ) as NpcHasSweepItemsEvent

                eventData.npcId = template.getId()
                eventData.npcObjectId = this.getObjectId()
                eventData.itemIds = this.sweepItems.map( item => item.itemId )

                await ListenerCache.sendNpcTemplateEvent( template.getId(), EventType.NpcHasSweepItems, eventData )
            }
        }

        let droppedItems: Array<ItemData> = this.calculateDrops( template, DropListScope.DEATH, player )

        if ( this.isChampion() && shouldDropChampionLoot( this, player ) ) {

            let itemId: number = ConfigManager.customs.getChampionRewardItemID()
            let itemAmount: number = Math.floor( Math.random() * ConfigManager.customs.getChampionRewardItemQuantity() + 1 )

            droppedItems.push( ItemDataHelper.createItem( itemId, itemAmount ) )
        }

        if ( droppedItems.length === 0 ) {
            return
        }

        if ( ListenerCache.hasNpcTemplateListeners( template.getId(), EventType.NpcHasDropItems ) ) {
            let eventData = EventPoolCache.getData( EventType.NpcHasDropItems ) as NpcHasDropItemsEvent

            eventData.npcId = template.getId()
            eventData.npcObjectId = this.getObjectId()
            eventData.itemIds = droppedItems.map( item => item.itemId )

            await ListenerCache.sendNpcTemplateEvent( template.getId(), EventType.NpcHasDropItems, eventData )
        }

        let isRaidBoss = this.isRaid() && !this.isRaidMinion()
        let autoLootPromises : Array<Promise<unknown>> = []

        droppedItems.forEach( ( itemData: ItemData ) : void => {
            let item: L2Item = DataManager.getItems().getTemplate( itemData.itemId )

            if ( isRaidBoss ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.C1_DIED_DROPPED_S3_S2 )
                        .addCharacterName( this )
                        .addItemName( item )
                        .addNumber( itemData.amount )
                        .getBuffer()
                BroadcastHelper.dataBasedOnVisibility( this, packet )
            }

            if ( isGlobalAutolootEnabled( item, this ) || isPlayerAutolootEnabled( item, this, player ) ) {
                autoLootPromises.push( player.doAutoLoot( this, itemData.itemId, itemData.amount ) )
                return
            }

            this.dropItem( player.getObjectId(), itemData.itemId, itemData.amount, this.dropGeometry )
        } )

        if ( autoLootPromises.length > 0 ) {
            await Promise.all( autoLootPromises )
        }

        ItemDataHelper.recycleMany( droppedItems )
    }

    setChampion( value: boolean ) {
        this.champion = value
    }

    useVitalityRate(): boolean {
        return this.isChampion() ? ConfigManager.customs.championEnableVitality() : true
    }

    isRaidMinion(): boolean {
        return this.raidMinion
    }

    isMinion(): boolean {
        return !!this.npcSpawn?.isMinion( this )
    }

    getLeader(): L2Attackable {
        if ( !this.isMinion() ) {
            return null
        }

        return L2World.getObjectById( this.npcSpawn.getMinionLeaderId( this.getObjectId() ) ) as L2Attackable
    }

    isSpoiled() {
        return this.spoilerObjectId !== 0
    }

    checkSpoilOwner( player: L2PcInstance, sendMessage: boolean ) {
        if ( player.getObjectId() !== this.spoilerObjectId && !player.isInLooterParty( this.spoilerObjectId ) ) {
            if ( sendMessage ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SWEEP_NOT_ALLOWED ) )
            }

            return false
        }

        return true
    }

    isOldCorpse( player: L2PcInstance ) : boolean {
        if ( this.isDead() && DecayTaskManager.getRemainingTime( this ) < ConfigManager.npc.getCorpseConsumeSkillAllowedTimeBeforeDecay() ) {
            if ( player ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CORPSE_TOO_OLD_SKILL_NOT_USED ) )
            }

            return true
        }

        return false
    }

    getSpoilLootItems(): Array<L2Item> {
        return _.map( this.sweepItems, ( item: ItemData ) => {
            return ItemManagerCache.getTemplate( item.itemId )
        } )
    }

    setMustRewardExpSp( value: boolean ) {
        this.mustGiveExpSp = value
    }

    runCommandChannelTimerTask(): void {
        if ( ( Date.now() - this.commandChannelLastAttack ) > ConfigManager.character.getRaidLootRightsInterval() ) {
            this.commandChannelTimer = null
            this.firstCommandChannelAttacked = null
            this.commandChannelLastAttack = 0
            return
        }

        this.commandChannelTimer = setTimeout( this.runCommandChannelTimerTask.bind( this ), 10000 )
    }

    async reduceCurrentHp( damage: number, attacker: L2Character, skill: Skill, isAwake: boolean = true, isDOT: boolean = false ): Promise<void> {
        if ( this.isRaid() && !this.isMinion() && attacker && attacker.getParty() && attacker.getParty().isInCommandChannel() && attacker.getParty().getCommandChannel().meetRaidWarCondition( this ) ) {
            if ( !this.firstCommandChannelAttacked ) {
                this.firstCommandChannelAttacked = attacker.getParty().getCommandChannel()
                if ( this.firstCommandChannelAttacked ) {
                    this.commandChannelTimer = setTimeout( this.runCommandChannelTimerTask.bind( this ), 10000 )
                    this.commandChannelLastAttack = Date.now()
                    this.firstCommandChannelAttacked.broadcastPacket( CreatureSay.fromText( this.objectId, 0, NpcSayType.PartyroomAll, '', 'You have looting rights!' ) )
                }
            } else if ( attacker.getParty().getCommandChannel() === this.firstCommandChannelAttacked ) {
                this.commandChannelLastAttack = Date.now()
            }
        }

        if ( this.isEventMob() ) {
            return
        }

        /*
            Special event type used currently by Territory Wars protection quests.
         */
        if ( attacker && this.getCurrentHp() === this.getMaxHp() && ListenerCache.hasNpcTemplateListeners( this.getId(), EventType.AttackableInitialAttack ) ) {
            let data = EventPoolCache.getData( EventType.AttackableInitialAttack ) as AttackableInitialAttackEvent

            data.attackerId = attacker.getObjectId()
            data.attackerInstanceType = attacker.getInstanceType()
            data.targetId = this.getObjectId()
            data.targetNpcId = this.getId()

            await ListenerCache.sendNpcTemplateEvent( this.getId(), EventType.AttackableInitialAttack, data, attacker.getInstanceType() )
        }

        /*
            We must reduce hp first to ensure we do not activate AIController if mob becomes dead.
         */
        await super.reduceCurrentHp( damage, attacker, skill, isAwake, isDOT )

        if ( attacker ) {
            this.notifyAboutDamage( attacker, Math.floor( damage ), skill )
        }
    }

    canAddAggro( attacker: L2Object, damage: number, aggroAmount: number ): boolean {
        return true
    }

    notifyAboutDamage( attacker: L2Character, damage: number, skill: Skill ): void {
        if ( !attacker || attacker === this ) {
            return
        }

        let player: L2PcInstance = attacker.getActingPlayer()
        if ( player && ListenerCache.hasNpcTemplateListeners( this.getId(), EventType.AttackableAttacked ) ) {
            let data = EventPoolCache.getData( EventType.AttackableAttacked ) as AttackableAttackedEvent

            data.attackerId = attacker.getObjectId()
            data.attackerInstanceType = attacker.getInstanceType()
            data.targetNpcId = this.getId()
            data.attackerPlayerId = player.getObjectId()
            data.targetId = this.getObjectId()
            data.damage = damage
            data.skillId = skill ? skill.getId() : 0
            data.skillLevel = skill ? skill.getLevel() : 0
            data.isChampion = this.isChampion()

            ListenerCache.sendNpcTemplateEvent( this.getId(), EventType.AttackableAttacked, data, attacker.getInstanceType() )
        }

        if ( this.isDead() ) {
            return
        }

        AIEffectHelper.notifyAttackedWithTargetId( this, attacker.getObjectId(), damage, ( damage * 100 ) / ( this.getLevel() + 7 ) )
    }

    getMostHated(): L2Character {
        if ( this.isAlikeDead() ) {
            return
        }

        return AggroCache.getMostHated( this )
    }

    stopHating( target: L2Object ) {
        if ( !target ) {
            return
        }

        AggroCache.resetAggro( this.getObjectId(), target.getObjectId(), 0 )
    }

    getHating( target: L2Character ) {
        return AggroCache.getAggroAmount( this.getObjectId(), target.getObjectId() )
    }

    getSeederId() {
        return this.seederObjectId
    }

    setSeeded( seed: L2Seed, seeder: L2PcInstance ): void {
        if ( !this.seeded ) {
            this.seed = seed
            this.seederObjectId = seeder.objectId
        }
    }

    setSeededByPlayer( seeder: L2PcInstance ) {
        if ( this.seed && this.seederObjectId === seeder.getObjectId() ) {

            this.seeded = true
            let amount: number = 1

            _.each( this.getTemplate().getSkills(), ( skill: Skill ) => {
                let multiplier = harvestSkillMultipliers[ skill.getId() ]
                if ( multiplier ) {
                    amount = amount * multiplier
                }
            } )

            let levelBonus: number = this.getLevel() - this.seed.getLevel() - 5
            if ( levelBonus > 0 ) {
                amount += levelBonus
            }

            this.harvestItem = ItemDataHelper.createItem( this.seed.getCropId(), Math.max( 1, amount * ConfigManager.rates.getRateDropManor() ) )
        }
    }

    isSeeded() {
        return this.seeded
    }

    getSeed(): L2Seed {
        return this.seed
    }

    emptyHarvestItem(): void {
        if ( this.harvestItem ) {
            ItemDataHelper.recycle( this.harvestItem )
        }

        this.harvestItem = null
    }

    getHarvestItem(): ItemData {
        return this.harvestItem
    }

    isAttackable(): boolean {
        return true
    }

    setIsRaid( value: boolean ) {
        this.raid = value
    }

    overhitEnabled( value: boolean ) {
        this.overhit = value
    }

    getSweepItems(): Array<ItemData> {
        return this.sweepItems
    }

    emptySweepItems(): void {
        if ( this.sweepItems ) {
            ItemDataHelper.recycleMany( this.sweepItems )
        }

        this.sweepItems = null
    }

    createStatus(): AttackableStatus {
        return new AttackableStatus( this )
    }

    getFirstCommandChannelAttacked(): L2CommandChannel {
        return this.firstCommandChannelAttacked
    }

    setIsReturningToSpawnPoint( value: boolean ) {
        this.isReturningToSpawnPointValue = value
    }

    returnHome() {
        this.clearOverhitData()

        // TODO : whole logic of this function should happen when attack trait is finished, thus should be inside trait
        if ( this.hasAIController() ) {
            AIEffectHelper.notifyMove( this, this.getSpawnLocation() )
        }
    }

    canReturnToSpawnPoint() {
        return this.canReturnToSpawnPointValue
    }

    setOverhitValues( attacker: L2Character, damage: number ) {
        let totalDamage = -( this.getCurrentHp() - damage )
        if ( totalDamage < 0 ) {
            // we have not killed the mob with the over-hit strike. (it wasn't really an over-hit strike)
            // let's just clear all the over-hit related values
            this.overhitEnabled( false )
            this.overhitDamage = 0
            this.overhitAttacker = null
            return
        }

        this.overhitEnabled( true )
        this.overhitDamage = totalDamage
        this.overhitAttacker = attacker.getObjectId()
    }

    canSeeThroughSilentMove() {
        return this.seeThroughSilentMove
    }

    canBeAttacked(): boolean {
        return true
    }

    hasRandomAnimation(): boolean {
        return ConfigManager.general.getMaxMonsterAnimation() > 0 && this.hasRandomWalk()
    }

    deactivateAI(): void {
        this.setTarget( null )
        this.clearOverhitData()

        return super.deactivateAI()
    }

    runAggroScanTask(): void {
        let attacker: L2Character = L2World.getFirstObjectByPredicate( this, this.getAggroRange(), ( object: L2Object ): boolean => {
            return object.isCharacter() && this.canInitiateAttack( object as L2Character )
        } ) as L2Character

        if ( attacker ) {
            AIEffectHelper.notifyAttacked( this, attacker, 1 )
        }
    }

    canInitiateAttack( target: L2Character ): boolean {
        if ( target.isInvulnerable() ) {
            if ( target.isPlayer() && target.isGM() ) {
                return false
            }

            if ( target.isSummon() && ( target as L2Summon ).getOwner().isGM() ) {
                return false
            }
        }

        if ( target.isDoor() ) {
            return false
        }

        if ( target.isAlikeDead() || ( target.isPlayable() && Math.abs( target.getZ() - this.getZ() ) > this.getAggroRange() ) ) {
            return false
        }

        if ( target.isPlayable() ) {
            if ( !this.isRaid() && !this.canSeeThroughSilentMove() && ( target as L2Playable ).isSilentMovingAffected() ) {
                return false
            }
        }

        let player: L2PcInstance = target.getActingPlayer()
        if ( player ) {
            if ( player.isGM() && !player.getAccessLevel().hasPermission( PlayerPermission.TakeAggro ) ) {
                return false
            }

            if ( player.isRecentFakeDeath() || player.isSpawnProtected() || player.isTeleportProtected() ) {
                return false
            }

            if ( player.isInParty() && player.getParty().isInDimensionalRift() ) {
                let riftType = player.getParty().getDimensionalRift().getType()
                let riftRoom = player.getParty().getDimensionalRift().getCurrentRoom()

                if ( this.isInstanceType( InstanceType.L2RiftInvaderInstance )
                        && !DimensionalRiftManager.getRoom( riftType, riftRoom ).checkIfInZone( this.getX(), this.getY(), this.getZ() ) ) {
                    return false
                }
            }
        }

        // TODO : move to instance check
        if ( this.isGuard() ) {
            if ( player && ( player.getKarma() > 0 ) ) {
                return PathFinding.canSeeTargetWithPosition( this, player )
            }
            if ( target.isMonster() && ConfigManager.npc.guardAttackAggroMob() ) {
                return ( target as L2MonsterInstance ).isAggressive() && PathFinding.canSeeTargetWithPosition( this, target )
            }

            return false
        }

        // TODO : move to instance check
        if ( this.isInstanceType( InstanceType.L2FriendlyMobInstance ) ) {
            if ( target.isNpc() ) {
                return false
            }

            if ( target.isPlayer() && ( ( target as L2PcInstance ).getKarma() > 0 ) ) {
                return PathFinding.canSeeTargetWithPosition( this, target )
            }

            return false
        }

        if ( target.isAttackable() ) {
            if ( !target.isAutoAttackable( this ) ) {
                return false
            }

            if ( this.isChaos() && ( Math.abs( target.getZ() - this.getZ() ) <= this.getAggroRange() ) ) {
                if ( ( target as L2Attackable ).isInMyClan( this ) ) {
                    return false
                }

                return PathFinding.canSeeTargetWithPosition( this, target )
            }
        }

        if ( target.isNpc() ) {
            return false
        }

        if ( !ConfigManager.npc.mobAggroInPeaceZone() && target.isInArea( AreaType.Peace ) ) {
            return false
        }

        if ( this.isChampion() && ConfigManager.customs.championPassive() ) {
            return false
        }

        return this.isAggressive() && PathFinding.canSeeTargetWithPosition( this, target )
    }

    setSpoilerObjectId( value: number ): void {
        this.spoilerObjectId = value
    }

    setSeeThroughSilentMove( value: boolean ): void {
        this.seeThroughSilentMove = value
    }

    getMaxHp(): number {
        let value = this.getStat().getMaxHp()

        if ( ConfigManager.npc.isNpcHpMultiplierEnabled() ) {
            value = Math.min( 1, Math.floor( ConfigManager.npc.getNpcHpMultiplierRatio() * value ) )
        }

        if ( this.isChampion() && ConfigManager.customs.championEnable() ) {
            value = Math.floor( ConfigManager.customs.getChampionHp() * value )
        }

        return value
    }

    createAIController(): TraitRunnerAIController {
        // TODO : review regeneration, it should only start when character has received damage
        if ( this.getStatus().shouldRegenerate() ) {
            this.getStatus().startHpMpRegeneration()
        }

        return new TraitRunnerAIController( this, this.defaultTraits )
    }

    getSkillUsage() : SkillUsageParameters {
        if ( !this.skillUsage ) {
            this.skillUsage = skillUsageObjectPool.getValue()
            this.skillUsage.index = 0
        }


        return this.skillUsage
    }

    prepareDropGeometry() : void {
        if ( this.dropGeometry ) {
            return
        }

        this.dropGeometry = PointGeometry.acquire( this.getGeometryId() )
    }

    clearDropGeometry() : void {
        if ( this.dropGeometry ) {
            this.dropGeometry.release()
            this.dropGeometry = null
        }
    }

    async dropRewardItems( character: L2Character ) : Promise<void> {
        this.prepareDropGeometry()

        await Promise.all( [
            this.doItemDrop( character ),
            this.doAdditionalDrop( character )
        ] )

        this.clearDropGeometry()
    }

    private calculateDrops( template: L2NpcTemplate, scope: DropListScope, player: L2PcInstance ): Array<ItemData> {
        let itemDrops = template.getDrops( scope )
        if ( !itemDrops ) {
            return []
        }

        return itemDrops.reduce( ( allItems: Array<ItemData>, item: IDropItem ): Array<ItemData> => {
            let drop = item.generateDrop( this, player )

            if ( drop ) {
                allItems.push( drop )
            }

            return allItems
        }, [] )
    }
}