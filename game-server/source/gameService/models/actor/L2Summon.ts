import { L2Playable } from './L2Playable'
import { L2PcInstance } from './instance/L2PcInstance'
import { L2World } from '../../L2World'
import { PacketDispatcher } from '../../PacketDispatcher'
import { L2Party } from '../L2Party'
import { ExPartyPetWindowUpdate } from '../../packets/send/ExPartyPetWindowUpdate'
import { L2NpcTemplate } from './templates/L2NpcTemplate'
import { SummonInfo } from '../../packets/send/SummonInfo'
import { InstanceType } from '../../enums/InstanceType'
import { DataManager } from '../../../data/manager'
import { Skill } from '../Skill'
import { PetInfo, PetInfoAnimation } from '../../packets/send/PetInfo'
import { ConfigManager } from '../../../config/ConfigManager'
import { PetStatusUpdate } from '../../packets/send/PetStatusUpdate'
import { TeleportToLocation } from '../../packets/send/TeleportToLocation'
import { Race } from '../../enums/Race'
import { PetItemList } from '../../packets/send/PetItemList'
import { ShotType, ShotTypeHelper } from '../../enums/ShotType'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2EtcItem } from '../items/L2EtcItem'
import { ExPartyPetWindowDeleteWithSummon } from '../../packets/send/ExPartyPetWindowDelete'
import { PetDelete } from '../../packets/send/PetDelete'
import { IDFactoryCache } from '../../cache/IDFactoryCache'
import { L2Character } from './L2Character'
import { OlympiadGameManager } from '../olympiad/OlympiadGameManager'
import { ItemHandlerMethod } from '../../handler/ItemHandler'
import { ActionType } from '../items/type/ActionType'
import { SummonStats } from './stat/SummonStats'
import { SummonStatus } from './status/SummonStatus'
import { L2Object } from '../L2Object'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { RelationChanged } from '../../packets/send/RelationChanged'
import { ExPartyPetWindowAddWithSummon } from '../../packets/send/ExPartyPetWindowAdd'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { DecayTaskManager } from '../../taskmanager/DecayTaskManager'
import { L2Attackable } from './L2Attackable'
import { L2ItemInstance } from '../items/instance/L2ItemInstance'
import { L2Weapon } from '../items/L2Weapon'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, PlayerSummonSpawnEvent } from '../events/EventType'
import { AggroCache, AggroData } from '../../cache/AggroCache'
import { L2TargetType } from '../skills/targets/L2TargetType'
import { L2Clan } from '../L2Clan'
import { BeastSoulShot } from '../../handler/itemhandlers/BeastSoulShot'
import { BeastSpiritShot } from '../../handler/itemhandlers/BeastSpiritShot'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import _ from 'lodash'
import aigle from 'aigle'
import { L2NpcValues } from '../../values/L2NpcValues'
import { PlayerGroupCache } from '../../cache/PlayerGroupCache'
import { PetSkillStance } from '../../enums/PetSkillStance'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { DeleteObject } from '../../packets/send/DeleteObject'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { SiegeRole } from '../../enums/SiegeRole'
import { PlayerPvpFlag } from '../../enums/PlayerPvpFlag'
import { PathFinding } from '../../../geodata/PathFinding'
import { FightStanceCache } from '../../cache/FightStanceCache'
import { PlayerRelationStatus } from '../../cache/PlayerRelationStatusCache'
import { AreaDiscoveryManager } from '../../cache/AreaDiscoveryManager'
import { PlayerPermission } from '../../enums/PlayerPermission'
import { L2SummonType } from '../../enums/L2SummonType'
import { CalculatorHelper } from '../../helpers/CalculatorHelper'

const PASSIVE_SUMMONS : Set<number> = new Set<number>( [
    12564, 12621, 14702, 14703, 14704, 14705, 14706, 14707, 14708, 14709, 14710, 14711,
    14712, 14713, 14714, 14715, 14716, 14717, 14718, 14719, 14720, 14721, 14722, 14723,
    14724, 14725, 14726, 14727, 14728, 14729, 14730, 14731, 14732, 14733, 14734, 14735, 14736,
] )

export class L2Summon extends L2Playable {
    owner: number // object id from L2Object
    attackRange: number // Melee range
    followOwner: boolean
    shotsMask: number
    skillStance: PetSkillStance

    constructor( template: L2NpcTemplate, owner: L2PcInstance ) {
        super( IDFactoryCache.getNextId(), template )

        this.instanceType = InstanceType.L2Summon
        this.setInstanceId( owner.getInstanceId() )
        this.setShowSummonAnimation( true )
        this.setXYZInvisible( owner.getX() + _.random( -100, 100 ), owner.getY() + _.random( -100, 100 ), owner.getZ() )

        this.owner = owner.getObjectId()
        this.skillStance = template.hasInitialSkillStance() ? PetSkillStance.Support : PetSkillStance.Attack
    }

    createMethods() {
        super.createMethods()

        this.debounceAreaRevalidation = _.debounce( this.runDirectAreaRevalidation.bind( this ), 300, {
            trailing: true,
            maxWait: 600,
        } )
    }

    getTemplate(): L2NpcTemplate {
        return this.template as L2NpcTemplate
    }

    assignCalculators() {
        this.calculators = CalculatorHelper.getSummonCalculators()
    }

    sendSummonInfo( status: PetInfoAnimation ): void {
        let summon = this
        _.each( L2World.getAllVisiblePlayerIds( this, this.getBroadcastRadius() ), ( playerId: number ) => {
            if ( playerId === summon.owner ) {
                return
            }

            PacketDispatcher.sendOwnedData( playerId, SummonInfo( playerId, summon, status ) )
        } )
    }

    canAttack( isControlPressed: boolean ) {
        let owner: L2PcInstance = this.getOwner()
        if ( !owner ) {
            return false
        }

        if ( owner.isInOlympiadMode() && !owner.isOlympiadStart() ) {
            owner.sendOwnedData( ActionFailed() )
            return false
        }

        let target: L2Object = owner.getTarget()
        if ( !target || ( this.getObjectId() === target.getObjectId() ) || ( owner.getObjectId() === target.getObjectId() ) ) {
            return false
        }

        let npcId = this.getId()
        if ( PASSIVE_SUMMONS.has( npcId ) ) {
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.isBetrayed() ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_REFUSING_ORDER ) )
            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( this.isAttackingDisabled() ) {
            if ( !this.isAttackingNow() ) {
                return false
            }

            AIEffectHelper.notifyAttacked( this, target )
        }

        if ( this.isPet() && ( ( this.getLevel() - owner.getLevel() ) > 20 ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_TOO_HIGH_TO_CONTROL ) )
            this.sendOwnedData( ActionFailed() )
            return false
        }

        let targetPlayer = target.getActingPlayer()
        if ( targetPlayer
                && owner.getSiegeRole() !== SiegeRole.None
                && owner.isInSiegeArea()
                && ( targetPlayer.getSiegeSide() === owner.getSiegeSide() ) ) {
            if ( TerritoryWarManager.isTWInProgress ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_ATTACK_A_MEMBER_OF_THE_SAME_TERRITORY ) )
            } else {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FORCED_ATTACK_IS_IMPOSSIBLE_AGAINST_SIEGE_SIDE_TEMPORARY_ALLIED_MEMBERS ) )
            }

            this.sendOwnedData( ActionFailed() )
            return false
        }

        if ( !owner.getAccessLevel().hasPermission( PlayerPermission.AllowPeaceAttack ) && owner.isInsidePeaceZoneWithObjects( this, target ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IN_PEACEZONE ) )
            return false
        }

        if ( this.isLockedTarget() ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_CHANGE_TARGET ) )
            return false
        }

        if ( !target.isAutoAttackable( owner ) && !isControlPressed && !target.isNpc() ) {
            this.resetFollowOwner()
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
            return AIEffectHelper.notifyFollow( this, target.getObjectId() )
        }

        return !( target.isDoor() && ( this.getTemplate().getRace() !== Race.SIEGE_WEAPON ) )
    }

    stopCurrentAction(): void {
        if ( !this.isMovementDisabled() ) {
            return AIEffectHelper.notifyFollow( this, this.getOwnerId() )
        }

        return AIEffectHelper.scheduleNextIntent( this, AIIntent.WAITING )
    }

    getId(): number {
        return this.getTemplate().getId()
    }

    createStats(): SummonStats {
        return new SummonStats( this )
    }

    createStatus(): SummonStatus {
        return new SummonStatus( this )
    }

    attackTarget( target : L2Object ): void {
        if ( !target || this.getObjectId() === target.getObjectId() ) {
            return
        }

        this.setTarget( target )
        AIEffectHelper.notifyAttacked( this, target )
    }

    getArmor() {
        return 0
    }

    getControlObjectId() {
        return 0
    }

    getExpForNextLevel() {
        if ( this.getLevel() >= ConfigManager.character.getMaxPetLevel() ) {
            return 0
        }

        return DataManager.getExperienceData().getExpForLevel( this.getLevel() + 1 )
    }

    getExpForThisLevel() {
        if ( this.getLevel() >= ( ConfigManager.character.getMaxPetLevel() + 1 ) ) {
            return 0
        }

        return DataManager.getExperienceData().getExpForLevel( this.getLevel() )
    }

    getFormId(): number {
        let formId = 0
        let npcId = this.getId()
        let level = this.getLevel()
        if ( ( npcId === 16041 ) || ( npcId === 16042 ) ) {
            if ( level > 69 ) {
                formId = 3
            } else if ( level > 64 ) {
                formId = 2
            } else if ( level > 59 ) {
                formId = 1
            }
        } else if ( ( npcId === 16025 ) || ( npcId === 16037 ) ) {
            if ( level > 69 ) {
                formId = 3
            } else if ( level > 64 ) {
                formId = 2
            } else if ( level > 59 ) {
                formId = 1
            }
        }
        return formId
    }

    getKarma(): number {
        let owner = this.getOwner()
        return owner ? owner.getKarma() : 0
    }

    getPvpFlag(): PlayerPvpFlag {
        let owner = this.getOwner()
        return owner ? owner.getPvpFlag() : PlayerPvpFlag.None
    }

    getStat(): SummonStats {
        return super.getStat() as SummonStats
    }

    isInCombat(): boolean {
        return FightStanceCache.hasPlayableStance( this.getObjectId() ) || FightStanceCache.hasPlayableStance( this.getOwnerId() )
    }

    isUndead(): boolean {
        return this.getTemplate().getRace() === Race.UNDEAD
    }

    async onTeleported(): Promise<void> {
        await super.onTeleported()
        PacketDispatcher.sendOwnedData( this.owner, TeleportToLocation( this.objectId, this.getX(), this.getY(), this.getZ(), this.getHeading() ) )
    }

    async rechargeShots( isPhysical: boolean, isMagical: boolean ) {

        let player = this.getOwner()
        await aigle.resolve( player.getAutoSoulShot() ).eachSeries( async ( itemId: number ) => {
            let item = player.getInventory().getItemByItemId( itemId )

            if ( isMagical
                    && item
                    && item.isEtcItem()
                    && [ ActionType.SUMMON_SPIRITSHOT, ActionType.SUMMON_SOULSHOT ].includes( item.getItem().getDefaultAction() ) ) {

                let handler: ItemHandlerMethod = item.getEtcItem().getItemHandler()
                if ( handler ) {
                    await handler( player, item, false )
                }

                return
            }

            player.removeAutoSoulShot( itemId )
        } )
    }

    sendDamageMessage( target: L2Character, damage: number, mcrit: boolean, powerCrit: boolean, isMiss: boolean ) {
        if ( isMiss || !this.owner || target.getObjectId() === this.getOwnerId() ) {
            return
        }

        if ( powerCrit || mcrit ) {
            if ( this.isServitor() ) {
                PacketDispatcher.sendOwnedData( this.owner, SystemMessageBuilder.fromMessageId( SystemMessageIds.CRITICAL_HIT_BY_SUMMONED_MOB ) )
            } else {
                PacketDispatcher.sendOwnedData( this.owner, SystemMessageBuilder.fromMessageId( SystemMessageIds.CRITICAL_HIT_BY_PET ) )
            }
        }

        let currentOwner = this.getOwner()
        if ( currentOwner.isInOlympiadMode()
                && target.isPlayer()
                && ( target as L2PcInstance ).isInOlympiadMode()
                && ( ( target as L2PcInstance ).getOlympiadGameId() === currentOwner.getOlympiadGameId() ) ) {
            OlympiadGameManager.notifyCompetitorDamage( currentOwner, damage )
        }

        if ( target.isInvulnerable() && !target.isPlayer() ) {
            PacketDispatcher.sendOwnedData( this.owner, SystemMessageBuilder.fromMessageId( SystemMessageIds.ATTACK_WAS_BLOCKED ) )
            return
        }

        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.C1_DONE_S3_DAMAGE_TO_C2 )
                .addSummonName( this )
                .addCharacterName( target )
                .addNumber( damage )
                .getBuffer()

        PacketDispatcher.sendOwnedData( this.owner, packet )
    }

    isSummon(): boolean {
        return true
    }

    sendCopyData( packet: Buffer ) {
        PacketDispatcher.sendCopyData( this.getOwnerId(), packet )
    }

    sendOwnedData( packet: Buffer ) {
        PacketDispatcher.sendOwnedData( this.owner, packet )
    }

    shouldDescribeState( player: L2PcInstance ): boolean {
        if ( player.objectId === this.owner ) {
            player.sendOwnedData( PetInfo( this, PetInfoAnimation.None ) )

            this.updateEffectIcons()

            if ( this.isPet() ) {
                player.sendOwnedData( PetItemList( this ) )
            }
            return true
        }

        player.sendOwnedData( SummonInfo( player.objectId, this, PetInfoAnimation.None ) )

        return true
    }

    setChargedShot( type: ShotType, charged: boolean ): void {
        if ( charged ) {
            this.shotsMask |= ShotTypeHelper.getMask( type )
        } else {
            this.shotsMask &= ~ShotTypeHelper.getMask( type )
        }
    }

    getOwner(): L2PcInstance {
        return L2World.getPlayer( this.owner )
    }

    getOwnerId() {
        return this.owner
    }

    getSoulShotsPerHit() {
        if ( this.getTemplate().getSoulShot() > 0 ) {
            return this.getTemplate().getSoulShot()
        }

        return 1
    }

    getSpiritShotsPerHit() {
        if ( this.getTemplate().getSpiritShot() > 0 ) {
            return this.getTemplate().getSpiritShot()
        }

        return 1
    }

    getSummonType(): L2SummonType {
        return L2SummonType.None
    }

    getWeapon() {
        return 0
    }

    isHungry() {
        return false
    }

    isMountable() {
        return false
    }

    setAIFollow( value: boolean ): void {
        this.followOwner = value
        if ( this.followOwner ) {
            return AIEffectHelper.notifyFollow( this, this.getOwnerId() )
        }

        return AIEffectHelper.scheduleNextIntent( this, AIIntent.WAITING )
    }

    resetFollowOwner(): void {
        this.followOwner = false
    }

    setOwner( player: L2PcInstance ) {
        this.owner = player.getObjectId()
    }

    async unSummon( player: L2PcInstance ): Promise<void> {
        if ( !this.isVisible() || this.isDead() ) {
            return
        }

        this.setAIController( null )

        if ( player ) {
            player.sendOwnedData( PetDelete( this.getSummonType(), this.getObjectId() ) )

            let party: L2Party = player.getParty()
            if ( party ) {
                party.broadcastDataToPartyMembers( player.getObjectId(), ExPartyPetWindowDeleteWithSummon( this ) )
            }

            let playerOwner = this.getOwner()
            if ( this.getInventory() && this.getInventory().getSize() > 0 ) {
                playerOwner.setPetInventoryItems( true )
                playerOwner.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEMS_IN_PET_INVENTORY ) )
            } else {
                playerOwner.setPetInventoryItems( false )
            }
        }

        this.decayMe()

        await this.storeMe()
        await this.storeEffect( true )
        if ( player ) {
            player.setPet( null )
        }

        await this.stopAllEffects()
        L2World.removeObject( this )
        this.removeSharedData()

        this.debounceAreaRevalidation.cancel()
        AreaDiscoveryManager.removeCharacter( this )

        this.setTarget( null )
        if ( player ) {
            player.getAutoSoulShot().forEach( ( itemId: number ) => {
                let item = DataManager.getItems().getTemplate( itemId )
                if ( !item || !item.isEtcItem() ) {
                    return
                }

                if ( [ BeastSoulShot, BeastSpiritShot ].includes( ( item as L2EtcItem ).getItemHandler() as any ) ) {
                    player.disableAutoShot( itemId )
                }
            } )
        }

        BroadcastHelper.dataInLocation( this, DeleteObject( this.getObjectId() ), this.getBroadcastRadius() )
    }

    async updateAndBroadcastStatus( value: PetInfoAnimation ): Promise<void> {
        PacketDispatcher.sendOwnedData( this.owner, PetInfo( this, value ) )
        PacketDispatcher.sendOwnedData( this.owner, PetStatusUpdate( this ) )

        if ( this.isVisible() ) {
            this.sendSummonInfo( value )
        }

        let party: L2Party = PlayerGroupCache.getParty( this.owner )
        if ( party ) {
            party.broadcastDataToPartyMembers( this.owner, ExPartyPetWindowUpdate( this ) )
        }

        this.updateEffectIcons()
    }

    isFollowingOwner(): boolean {
        return this.followOwner
    }

    getStatus(): SummonStatus {
        return super.getStatus() as SummonStatus
    }

    async doCast( skill: Skill ): Promise<void> {
        let player: L2PcInstance = this.getActingPlayer()
        if ( !player.checkPvpSkill( this.getTarget(), skill ) && !player.getAccessLevel().hasPermission( PlayerPermission.AllowPeaceAttack ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
            player.sendOwnedData( ActionFailed() )

            return
        }

        return super.doCast( skill )
    }

    onSpawn() {
        this.revalidateZone( true )

        let owner = this.getOwner()
        owner.sendOwnedData( RelationChanged( this, PlayerRelationStatus.getStatus( this.getOwnerId() ), false ) )

        let party: L2Party = owner.getParty()
        if ( party ) {
            party.broadcastDataToPartyMembers( owner.getObjectId(), ExPartyPetWindowAddWithSummon( this ) )
        }

        this.setShowSummonAnimation( false )

        if ( ListenerCache.hasNpcTemplateListeners( this.getId(), EventType.PlayerSummonSpawn ) ) {
            let eventData: PlayerSummonSpawnEvent = {
                objectId: this.getObjectId(),
                npcId: this.getId(),
                ownerId: this.getOwnerId()
            }

            ListenerCache.sendNpcTemplateEvent( this.getId(), EventType.PlayerSummonSpawn, eventData )
        }
    }

    async doDie( killer: L2Character ): Promise<boolean> {
        if ( this.isNoblesseBlessedAffected() ) {
            await this.stopEffects( L2EffectType.NoblesseBlessing )
            await this.storeEffect( true )
        } else {
            await this.storeEffect( false )
        }

        if ( !await super.doDie( killer ) ) {
            return false
        }

        let ownerId: number = this.getOwnerId()
        let summonId: number = this.getObjectId()

        L2World.getVisibleObjects( this, this.getBroadcastRadius() ).forEach( ( object: L2Object ) => {
            if ( !object.isAttackable()
                    || object.getObjectId() === summonId
                    || ( object as L2Attackable ).isDead() ) {
                return
            }

            let data: AggroData = AggroCache.getAttackerData( object.getObjectId(), summonId )
            if ( data ) {
                AggroCache.addAggro( ownerId, object.getObjectId(), data.totalDamage, data.hateAmount )
            }
        } )

        DecayTaskManager.add( this )
        return true
    }

    isChargedShot( type: ShotType ): boolean {
        return ( this.shotsMask & ShotTypeHelper.getMask( type ) ) === ShotTypeHelper.getMask( type )
    }

    isAutoAttackable( character: L2Character ): boolean {
        let owner = this.getOwner()
        return owner && owner.isAutoAttackable( character )
    }

    getActiveWeaponInstance(): L2ItemInstance {
        return null
    }

    getActiveWeaponItem(): L2Weapon {
        return null
    }

    getSecondaryWeaponInstance(): L2ItemInstance {
        return null
    }

    getSecondaryWeaponItem(): L2Weapon {
        return null
    }

    async useMagic( skill: Skill, isControlPressed: boolean, isShiftPressed: boolean ): Promise<boolean> {
        if ( !skill || this.isDead() || !this.getOwnerId() ) {
            return false
        }

        if ( skill.isPassive() ) {
            return false
        }

        if ( this.isCastingNow() ) {
            return false
        }

        let player = this.getOwner()
        if ( !player ) {
            return
        }

        player.petSkillFlags.isCtrlPressed = isControlPressed
        player.petSkillFlags.isShiftPressed = isShiftPressed

        let target: L2Object
        switch ( skill.getTargetType() ) {
            case L2TargetType.PARTY:
            case L2TargetType.SELF:
                target = this

                break

            default:
                target = skill.getFirstTargetOf( this )
                break
        }

        if ( !target ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_CANT_FOUND ) )
            return false
        }

        if ( this.isSkillDisabled( skill ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_SKILL_CANNOT_BE_USED_RECHARCHING ) )
            return false
        }

        if ( this.getCurrentMp() < ( this.getStat().getMpConsume1( skill ) + this.getStat().getMpConsume2( skill ) ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_MP ) )
            return false
        }

        if ( this.getCurrentHp() <= skill.getHpConsume() ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_HP ) )
            return false
        }

        if ( this !== target
                && skill.isPhysical()
                && !PathFinding.canSeeTargetWithPosition( this, target ) ) {
            this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_SEE_TARGET ) )
            return false
        }

        if ( skill.isBad() ) {
            if ( player === target ) {
                return false
            }

            if ( this.isInsidePeaceZoneWithObjects( this, target ) && !player.getAccessLevel().hasPermission( PlayerPermission.AllowPeaceAttack ) ) {
                this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IN_PEACEZONE ) )
                return false
            }

            if ( player.isInOlympiadMode() && !player.isOlympiadStart() ) {
                this.sendOwnedData( ActionFailed() )
                return false
            }

            let targetPlayer = target.getActingPlayer()
            if ( targetPlayer
                    && player.getSiegeRole() !== SiegeRole.None
                    && player.isInSiegeArea()
                    && targetPlayer.getSiegeRole() === player.getSiegeRole()
                    && targetPlayer !== player
                    && targetPlayer.getSiegeSide() === player.getSiegeSide() ) {

                if ( TerritoryWarManager.isTWInProgress ) {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_ATTACK_A_MEMBER_OF_THE_SAME_TERRITORY ) )
                } else {
                    this.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FORCED_ATTACK_IS_IMPOSSIBLE_AGAINST_SIEGE_SIDE_TEMPORARY_ALLIED_MEMBERS ) )
                }

                this.sendOwnedData( ActionFailed() )
                return false
            }

            if ( target.isDoor() ) {
                if ( !target.isAutoAttackable( player ) ) {
                    return false
                }
            } else {
                if ( !target.canBeAttacked() && !player.getAccessLevel().hasPermission( PlayerPermission.AllowPeaceAttack ) ) {
                    return false
                }

                if ( !target.isAutoAttackable( this )
                        && !isControlPressed
                        && !target.isNpc()
                        && skill.getTargetType() !== L2TargetType.PARTY
                        && skill.getTargetType() !== L2TargetType.SELF ) {
                    return false
                }
            }
        }

        AIEffectHelper.notifyMustCastSpell( this, target, skill.getId(), skill.getLevel(), isControlPressed, isShiftPressed )
        return true
    }

    getClanId(): number {
        return this.getOwner().getClanId()
    }

    getClan(): L2Clan {
        return this.getOwner().getClan()
    }

    getParty(): L2Party {
        return PlayerGroupCache.getParty( this.getOwnerId() )
    }

    isSinEater(): boolean {
        return this.getTemplate().getId() === L2NpcValues.sinEaterNpcId
    }

    getActingPlayer(): L2PcInstance {
        return this.getOwner()
    }

    notifyFollowStatusChange(): void {
        if ( this.isInCombat() )
        return this.setAIFollow( !this.followOwner )
    }

    broadcastStatusUpdate() {
        super.broadcastStatusUpdate()
        this.updateAndBroadcastStatus( PetInfoAnimation.StatUpgrade )
    }

    hasSupportStance(): boolean {
        return this.skillStance === PetSkillStance.Support
    }

    switchStance(): void {
        this.skillStance = this.hasSupportStance() ? PetSkillStance.Attack : PetSkillStance.Support
    }

    onUpdateAbnormalEffect() {
        let summon = this
        L2World.getAllVisiblePlayerIds( this, this.getBroadcastRadius() ).forEach( ( playerId: number ) => {
            PacketDispatcher.sendOwnedData( playerId, SummonInfo( playerId, summon, PetInfoAnimation.StatUpgrade ) )
        } )
    }

    getSkillLevel( skillId: number ): number {
        return 0
    }

    hasActionOverride( mask: PlayerActionOverride ) : boolean {
        return this.getOwner().hasActionOverride( mask )
    }

    broadcastModifiedStats(): void {
        this.updateAndBroadcastStatus( PetInfoAnimation.StatUpgrade )
    }

    abortAttack() {
        if ( this.isAttackingNow() ) {
            AIEffectHelper.scheduleNextIntent( this, AIIntent.WAITING )
            this.sendOwnedData( ActionFailed() )
        }
    }

    addFightStance() : void {
        if ( !FightStanceCache.hasPlayableStance( this.getOwnerId() ) ) {
            FightStanceCache.addPlayableStance( this.getOwner() )
        }
    }

    runDirectAreaRevalidation(): void {
        AreaDiscoveryManager.findAreas( this )
    }
}