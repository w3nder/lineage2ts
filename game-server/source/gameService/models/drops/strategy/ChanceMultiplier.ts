import { GeneralDropItem } from '../GeneralDropItem'
import { L2Character } from '../../actor/L2Character'
import { ConfigManager } from '../../../../config/ConfigManager'
import { ItemManagerCache } from '../../../cache/ItemManagerCache'
import { ItemInstanceType } from '../../items/ItemInstanceType'
import { ItemTypes } from '../../../values/InventoryValues'
import _ from 'lodash'
import { ChanceMultiplierStrategy } from '../../../enums/drops/ChanceMultiplierStrategy'

function getChanceMultiplier( defaultMultiplier: number, item: GeneralDropItem, target: L2Character ) : number {
    let dropChanceMultiplier = ConfigManager.rates.getDropChanceMultiplierByItemId()[ item.getItemId() ]
    if ( dropChanceMultiplier ) {
        return dropChanceMultiplier
    }

    let template = ItemManagerCache.getTemplate( item.getItemId() )
    if ( template && template.hasExImmediateEffect() ) {
        return ConfigManager.rates.getHerbDropChanceMultiplier()
    }

    if ( template && template.isInstanceType( ItemInstanceType.L2EtcItem ) ) {
        return _.random( ConfigManager.rates.getDropEtcChanceMinMultiplier(), ConfigManager.rates.getDropEtcChanceMaxMultiplier(), true )
    }

    if ( target.isRaid() ) {
        return ConfigManager.rates.getRaidDropChanceMultiplier()
    }

    return defaultMultiplier
}

function chanceStrategy( multiplierMethod: Function, item: GeneralDropItem, target: L2Character ) : number {
    let multiplier = 1
    if ( target.isChampion() ) {
        multiplier *= item.getItemId() !== ItemTypes.Adena ? ConfigManager.customs.getChampionRewardsChance() : ConfigManager.customs.getChampionAdenasRewardsChance()
    }

    return multiplier * getChanceMultiplier( multiplierMethod(), item, target )
}

function questChanceStrategy( item: GeneralDropItem, target: L2Character ) : number {
    if ( ConfigManager.customs.championEnable() && target && target.isChampion() ) {
        let championMultiplier = ConfigManager.customs.getChampionRewardsChance()

        if ( item.getItemId() === ItemTypes.Adena || ItemTypes.AncientAdena === item.getItemId() ) {
            championMultiplier = ConfigManager.customs.getChampionAdenasRewardsChance()
        }

        return ConfigManager.rates.getRateQuestDropChance() * championMultiplier
    }

    return ConfigManager.rates.getRateQuestDropChance()

}

type MultiplierMethod = ( item: GeneralDropItem, target: L2Character ) => number
export const ChanceMultiplierFunctions : Record<number, MultiplierMethod> = {
    [ ChanceMultiplierStrategy.DROP ]: chanceStrategy.bind( null, ConfigManager.rates.getDeathDropChanceMultiplier ),
    [ ChanceMultiplierStrategy.SPOIL ]: chanceStrategy.bind( null, ConfigManager.rates.getCorpseDropChanceMultiplier ),
    [ ChanceMultiplierStrategy.STATIC ]: _.constant( 1 ),
    [ ChanceMultiplierStrategy.QUEST ]: questChanceStrategy
}