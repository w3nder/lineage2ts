import { L2Character } from '../../actor/L2Character'
import { GeneralDropItem } from '../GeneralDropItem'
import { ConfigManager } from '../../../../config/ConfigManager'
import { ItemManagerCache } from '../../../cache/ItemManagerCache'
import { ItemInstanceType } from '../../items/ItemInstanceType'
import { ItemTypes } from '../../../values/InventoryValues'
import _ from 'lodash'
import { AmountMultiplierStrategy } from '../../../enums/AmountMultiplierStrategy'

function getDropMultiplier( defaultMultiplier: number, etcItemMultiplier: number, item: GeneralDropItem, target: L2Character ): number {
    let dropAmountMultiplier = ConfigManager.rates.getDropAmountMultiplierByItemId()[ item.getItemId() ]
    if ( dropAmountMultiplier ) {
        return dropAmountMultiplier
    }

    let template = ItemManagerCache.getTemplate( item.getItemId() )
    if ( template && template.hasExImmediateEffect() ) {
        return ConfigManager.rates.getHerbDropAmountMultiplier()
    }

    if ( template && template.isInstanceType( ItemInstanceType.L2EtcItem ) ) {
        return etcItemMultiplier
    }

    if ( target.isRaid() ) {
        return ConfigManager.rates.getRaidDropAmountMultiplier()
    }

    return defaultMultiplier
}

function dropStrategy( multiplierMethod: Function, etcItemMultiplierMethod: Function, item: GeneralDropItem, target: L2Character ): number {
    let multiplier = 1
    if ( target.isChampion() ) {
        multiplier *= item.getItemId() !== ItemTypes.Adena ? ConfigManager.customs.getChampionRewardsAmount() : ConfigManager.customs.getChampionAdenasRewardsAmount()
    }

    return multiplier * getDropMultiplier( multiplierMethod(), etcItemMultiplierMethod(), item, target )
}

type AmountMultiplierMethod = ( item: GeneralDropItem, target: L2Character ) => number
export const MaxAmountMultiplierFunctions : Record<number, AmountMultiplierMethod> = {
    [ AmountMultiplierStrategy.DROP ]: dropStrategy.bind( null, ConfigManager.rates.getDeathDropAmountMaxMultiplier, ConfigManager.rates.getEtcDropAmountMaxMultiplier ),
    [ AmountMultiplierStrategy.SPOIL ]: dropStrategy.bind( null, ConfigManager.rates.getCorpseDropAmountMaxMultiplier, ConfigManager.rates.getEtcDropAmountMaxMultiplier ),
    [ AmountMultiplierStrategy.STATIC ]: _.constant( 1 ),
}

export const MinAmountMultiplierFunctions : Record<number, AmountMultiplierMethod> = {
    [ AmountMultiplierStrategy.DROP ]: dropStrategy.bind( null, ConfigManager.rates.getDeathDropAmountMinMultiplier, ConfigManager.rates.getEtcDropAmountMinMultiplier ),
    [ AmountMultiplierStrategy.SPOIL ]: dropStrategy.bind( null, ConfigManager.rates.getCorpseDropAmountMinMultiplier, ConfigManager.rates.getEtcDropAmountMinMultiplier ),
    [ AmountMultiplierStrategy.STATIC ]: _.constant( 1 ),
}