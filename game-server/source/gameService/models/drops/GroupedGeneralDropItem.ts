import { IDropItem } from './IDropItem'
import { GeneralDropItem } from './GeneralDropItem'
import { L2Character } from '../actor/L2Character'
import { AttackerChanceModifierFunctions } from './strategy/AttackerChanceModifier'
import { PreciseDeterminationFunctions } from './strategy/PreciseDetermination'
import { ItemData } from '../../interface/ItemData'
import { createDropItem } from './DropItemHelper'
import { AmountMultiplierStrategy } from '../../enums/AmountMultiplierStrategy'
import { AttackerChanceModifierStrategy } from '../../enums/drops/AttackerChanceModifierStrategy'
import { ChanceMultiplierStrategy } from '../../enums/drops/ChanceMultiplierStrategy'
import { PreciseDeterminationStrategy } from '../../enums/drops/PreciseDeterminationStrategy'

/*
    Used specifically to restrict operations to
    avoid generating normalized item on already normalized item.
 */
export interface NormalizedDropItem {
    getChance() : number
    getItems(): Array<GeneralDropItem>
}

export class GroupedGeneralDropItem implements IDropItem, NormalizedDropItem {
    chance: number
    items: Array<GeneralDropItem> = []

    normalizedGroup: GroupedGeneralDropItem

    attackerStrategy: AttackerChanceModifierStrategy
    preciseStrategy: PreciseDeterminationStrategy

    constructor( chance: number,
                attackerStrategy: AttackerChanceModifierStrategy = AttackerChanceModifierStrategy.DEFAULT,
                preciseStrategy: PreciseDeterminationStrategy = PreciseDeterminationStrategy.DEFAULT ) {
        this.chance = chance
        this.attackerStrategy = attackerStrategy
        this.preciseStrategy = preciseStrategy
    }

    generateDrop( target: L2Character, attacker: L2Character ): ItemData {
        let normalized : NormalizedDropItem = this.getNormalized( target, attacker )
        let chance = normalized.getChance()

        if ( chance > ( Math.random() * 100 ) ) {
            let totalChance = 0
            let itemChance = Math.random() * 100
            let hasAdditionalItems = this.isPreciseCalculated() && chance > 100

            for ( const item of normalized.getItems() ) {
                totalChance += item.getChance()

                if ( totalChance > itemChance ) {
                    let possibleTries = 1
                    if ( hasAdditionalItems ) {
                        let currentChance = item.getChance() * ( chance / 100 )

                        if ( currentChance >= 100 ) {
                            possibleTries = Math.floor( chance / 100 )

                            if ( ( chance % 100 ) > itemChance ) {
                                possibleTries++
                            }
                        }
                    }

                    return createDropItem( possibleTries, item, target )
                }
            }
        }

        return null
    }

    getAttackerChanceModifier( target: L2Character, attacker: L2Character ) {
        return AttackerChanceModifierFunctions[ this.attackerStrategy ]( null, target, attacker )
    }

    getChance() {
        return this.chance
    }

    getItems() {
        return this.items
    }

    getNormalized( target: L2Character, attacker: L2Character ) : NormalizedDropItem {
        let chanceModifier = this.getAttackerChanceModifier( target, attacker )
        let groupedItemChance = this.getChance()
        let groupedChance = ( groupedItemChance * chanceModifier ) / 100

        /*
            Due to constant usage of drops a small optimization is possible to permanently create
            normalized items and then set chances and amounts. This allows simple re-use, however
            may create a problem if normalized item is passed around inside promises. All due to
            fact that async usage of two drop calculations would modify existing items in place.

            Current usage does not utilize promises.
         */
        if ( !this.normalizedGroup ) {
            this.createNormalizedItem()
        }

        let updatedChance = 0
        for ( const item of this.getItems() ) {
            updatedChance = updatedChance + ( item.getTargetChance( target ) * groupedChance )
        }

        for ( let index = 0; index < this.items.length; index++ ) {
            let originalItem = this.items[ index ]
            let normalizedItem = this.normalizedGroup.items[ index ]

            normalizedItem.min = originalItem.getMinDropCount( target )
            normalizedItem.max = originalItem.getMaxDropCount( target )
            normalizedItem.chance = ( originalItem.getTargetChance( target ) * groupedItemChance * chanceModifier ) / updatedChance
        }

        this.normalizedGroup.chance = updatedChance

        return this.normalizedGroup
    }

    private createNormalizedItem() : void {
        this.normalizedGroup = new GroupedGeneralDropItem(
            0,
            AttackerChanceModifierStrategy.NORULES,
            this.preciseStrategy
        )

        this.normalizedGroup.items = this.getItems().map( ( item: GeneralDropItem ) : GeneralDropItem => {
            return new GeneralDropItem(
                item.getItemId(),
                0,
                0,
                0,
                AmountMultiplierStrategy.STATIC,
                ChanceMultiplierStrategy.STATIC,
                this.preciseStrategy,
                AttackerChanceModifierStrategy.NORULES
            )
        } )
    }

    isPreciseCalculated() {
        return PreciseDeterminationFunctions[ this.preciseStrategy ]()
    }

    isGroupItem(): boolean {
        return true
    }
}