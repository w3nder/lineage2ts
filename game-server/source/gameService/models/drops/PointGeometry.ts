import { ObjectPool } from '../../helpers/ObjectPoolHelper'
import { RoaringBitmap32 } from 'roaring'
import { GeometryId } from '../../enums/GeometryId'
import { DataManager } from '../../../data/manager'
import {
    L2CircleGeometryParameters,
    L2GeometryDataItem,
    L2GeometryDataType, L2SquareGeometryParameters
} from '../../../data/interface/GeometryDataApi'

export class PointGeometry {
    points: RoaringBitmap32 = new RoaringBitmap32()
    xOffset: number = 0
    yOffset: number = 0
    x: number = 0
    y: number = 0
    geometry: GeometryId = GeometryId.None

    initialize( type: GeometryId ): void {
        if ( this.geometry === type ) {
            return
        }

        this.geometry = type
        this.refresh()
    }

    refresh(): void {
        let geometry = DataManager.getGeometry().getItem( this.geometry )

        this.points.copyFrom( geometry.data )
        this.updateOffset( geometry )
    }

    prepareNextPoint(): void {
        if ( this.points.size === 0 ) {
            this.refresh()
        }

        let value = this.points.at( this.points.size * Math.random() )
        this.points.remove( value )

        this.x = ( 0x0000FFFF & value ) - this.xOffset
        this.y = ( value >> 16 ) - this.yOffset
    }

    hasPoints() : boolean {
        return this.points.size > 0
    }

    getX(): number {
        return this.x
    }

    getY(): number {
        return this.y
    }

    static acquire( id: GeometryId ) : PointGeometry {
        let geometry = PointGeometryPool.getValue()

        geometry.initialize( id )

        return geometry
    }

    release() : void {
        PointGeometryPool.recycleValue( this )
    }

    getGeometryId() : GeometryId {
        return this.geometry
    }

    private updateOffset( geometry: L2GeometryDataItem ) : number {
        switch ( geometry.type ) {
            case L2GeometryDataType.PointCircle:
                this.xOffset = ( geometry.parameters as L2CircleGeometryParameters ).maxRadius
                this.yOffset = ( geometry.parameters as L2CircleGeometryParameters ).maxRadius
                return

            case L2GeometryDataType.PointSquare:
                this.xOffset = Math.floor( ( geometry.parameters as L2SquareGeometryParameters ).xLength / 2 )
                this.yOffset = Math.floor( ( geometry.parameters as L2SquareGeometryParameters ).yLength / 2 )
                return
        }
    }

    size() : number {
        return this.points.size
    }
}

export const PointGeometryPool = new ObjectPool<PointGeometry>( 'DropGeometry', () => {
    return new PointGeometry()
} )