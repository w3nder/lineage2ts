import { L2Character } from '../actor/L2Character'
import { ItemData } from '../../interface/ItemData'

export interface IDropItem {
    generateDrop( target: L2Character, attacker: L2Character ): ItemData
    isGroupItem(): boolean
}
