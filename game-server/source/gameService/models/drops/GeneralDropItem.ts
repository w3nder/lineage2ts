import { IDropItem } from './IDropItem'
import { L2Character } from '../actor/L2Character'
import {
    MaxAmountMultiplierFunctions,
    MinAmountMultiplierFunctions,
} from './strategy/AmountMultiplier'
import { ChanceMultiplierFunctions } from './strategy/ChanceMultiplier'
import { PreciseDeterminationFunctions } from './strategy/PreciseDetermination'
import { AttackerChanceModifierFunctions } from './strategy/AttackerChanceModifier'
import { ItemData } from '../../interface/ItemData'
import { createDropItem } from './DropItemHelper'
import { AmountMultiplierStrategy } from '../../enums/AmountMultiplierStrategy'
import { AttackerChanceModifierStrategy } from '../../enums/drops/AttackerChanceModifierStrategy'
import { ChanceMultiplierStrategy } from '../../enums/drops/ChanceMultiplierStrategy'
import { PreciseDeterminationStrategy } from '../../enums/drops/PreciseDeterminationStrategy'

export class GeneralDropItem implements IDropItem {
    itemId: number
    min: number
    max: number
    chance: number

    amountMultiplierStrategy: AmountMultiplierStrategy
    chanceMultiplierStrategy: ChanceMultiplierStrategy
    preciseStrategy: PreciseDeterminationStrategy
    attackerMultiplierStrategy: AttackerChanceModifierStrategy

    constructor( itemId: number,
                min: number,
                max: number,
                chance: number,
                amountStrategy: AmountMultiplierStrategy,
                chanceStrategy: ChanceMultiplierStrategy,
                preciseStrategy: PreciseDeterminationStrategy,
                attackerModifierStrategy: AttackerChanceModifierStrategy ) {
        this.itemId = itemId
        this.min = min
        this.max = max
        this.chance = chance

        this.amountMultiplierStrategy = amountStrategy
        this.chanceMultiplierStrategy = chanceStrategy
        this.preciseStrategy = preciseStrategy
        this.attackerMultiplierStrategy = attackerModifierStrategy
    }

    generateDrop( target: L2Character, attacker: L2Character ): ItemData {
        let chance = this.getTargetAttackerChance( target, attacker )
        let itemChance = Math.random() * 100

        if ( chance > itemChance ) {
            let possibleTries = 1
            if ( this.isPreciseCalculated() && chance > 100 ) {
                possibleTries = Math.floor( chance / 100 )

                if ( ( chance % 100 ) > itemChance ) {
                    possibleTries++
                }
            }

            return createDropItem( possibleTries, this, target )
        }

        return null
    }

    getMinAmountMultiplier( target: L2Character ) {
        return MinAmountMultiplierFunctions[ this.amountMultiplierStrategy ]( this, target )
    }

    getMaxAmountMultiplier( target: L2Character ) {
        return MaxAmountMultiplierFunctions[ this.amountMultiplierStrategy ]( this, target )
    }

    getAttackerChanceModifier( target: L2Character, attacker: L2Character ): number {
        return AttackerChanceModifierFunctions[ this.attackerMultiplierStrategy ]( this, target, attacker )
    }

    getChance() {
        return this.chance
    }

    getChanceMultiplier( target: L2Character ) {
        return ChanceMultiplierFunctions[ this.chanceMultiplierStrategy ]( this, target )
    }

    getItemId() {
        return this.itemId
    }

    getMaxDropCount( target: L2Character ) {
        return this.max * this.getMaxAmountMultiplier( target )
    }

    getMinDropCount( target: L2Character ) {
        return this.min * this.getMinAmountMultiplier( target )
    }

    getTargetAttackerChance( target: L2Character, attacker: L2Character ) : number {
        return this.getAttackerChanceModifier( target, attacker ) * this.getTargetChance( target )
    }

    getTargetChance( target: L2Character ): number {
        return this.getChance() * this.getChanceMultiplier( target )
    }

    isPreciseCalculated() : boolean {
        return PreciseDeterminationFunctions[ this.preciseStrategy ]()
    }

    isGroupItem(): boolean {
        return false
    }
}