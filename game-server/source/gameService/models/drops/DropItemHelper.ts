import { ItemData, ItemDataHelper } from '../../interface/ItemData'
import { GeneralDropItem } from './GeneralDropItem'
import { L2Character } from '../actor/L2Character'

export function createDropItem( amount: number, item: GeneralDropItem, target: L2Character ) : ItemData {
    let possibleTries = amount
    let minAmount = item.getMinDropCount( target )
    let maxAmount = item.getMaxDropCount( target )
    let min = Math.min( minAmount, maxAmount )
    let max = Math.max( maxAmount, minAmount )

    if ( min === max ) {
        return ItemDataHelper.createItem( item.getItemId(), Math.floor( amount * min ) )
    }

    let finalAmount = 0

    while ( possibleTries > 0 ) {
        /*
            Final amount must always be an integer, since any non-integer values
            cause problems in server packet generation.
         */
        finalAmount = finalAmount + Math.floor( min + ( Math.random() * ( max - min + 1 ) ) )
        possibleTries--
    }

    if ( finalAmount > 0 ) {
        return ItemDataHelper.createItem( item.getItemId(), finalAmount )
    }

    return null
}