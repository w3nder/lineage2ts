import { GeneralDropItem } from './GeneralDropItem'
import { GroupedGeneralDropItem } from './GroupedGeneralDropItem'
import { AmountMultiplierStrategy } from '../../enums/AmountMultiplierStrategy'
import { AttackerChanceModifierStrategy } from '../../enums/drops/AttackerChanceModifierStrategy'
import { ChanceMultiplierStrategy } from '../../enums/drops/ChanceMultiplierStrategy'
import { DropListScope } from '../../enums/drops/DropListScope'
import { PreciseDeterminationStrategy } from '../../enums/drops/PreciseDeterminationStrategy'

export type DropListScopeMethod = ( itemId: number, min: number, max: number, chance: number ) => GeneralDropItem
export type DropItemProducerMethod = ( chance: number ) => GroupedGeneralDropItem

export const DropListScopeFunctions = {
    onDeath( itemId: number, min: number, max: number, chance: number ): GeneralDropItem {
        return new GeneralDropItem(
                itemId,
                min,
                max,
                chance,
                AmountMultiplierStrategy.DROP,
                ChanceMultiplierStrategy.DROP,
                PreciseDeterminationStrategy.DEFAULT,
                AttackerChanceModifierStrategy.NONGROUP
        )
    },

    onCorpse( itemId: number, min: number, max: number, chance: number ): GeneralDropItem {
        return new GeneralDropItem(
                itemId,
                min,
                max,
                chance,
                AmountMultiplierStrategy.SPOIL,
                ChanceMultiplierStrategy.SPOIL,
                PreciseDeterminationStrategy.DEFAULT,
                AttackerChanceModifierStrategy.NONGROUP
        )
    },

    onStatic( itemId: number, min: number, max: number, chance: number ): GeneralDropItem {
        return new GeneralDropItem(
                itemId,
                min,
                max,
                chance,
                AmountMultiplierStrategy.STATIC,
                ChanceMultiplierStrategy.STATIC,
                PreciseDeterminationStrategy.ALWAYS,
                AttackerChanceModifierStrategy.NORULES
        )
    },

    onQuest( itemId: number, min: number, max: number, chance: number ): GeneralDropItem {
        return new GeneralDropItem(
                itemId,
                min,
                max,
                chance,
                AmountMultiplierStrategy.STATIC,
                ChanceMultiplierStrategy.QUEST,
                PreciseDeterminationStrategy.ALWAYS,
                AttackerChanceModifierStrategy.NORULES
        )
    },

    createDefaultGroupedItem( chance: number ): GroupedGeneralDropItem {
        return new GroupedGeneralDropItem( chance )
    },

    createStaticGroupedItem( chance: number ): GroupedGeneralDropItem {
        return new GroupedGeneralDropItem(
                chance,
                AttackerChanceModifierStrategy.NORULES,
                PreciseDeterminationStrategy.ALWAYS
        )
    }
}

export const DropListScopeMap : { [ type: number] : [ DropListScopeMethod, DropItemProducerMethod ]} = {
    [ DropListScope.DEATH ]: [ DropListScopeFunctions.onDeath, DropListScopeFunctions.createDefaultGroupedItem ],
    [ DropListScope.CORPSE ]: [ DropListScopeFunctions.onCorpse, DropListScopeFunctions.createDefaultGroupedItem ],
    [ DropListScope.STATIC ]: [ DropListScopeFunctions.onStatic, DropListScopeFunctions.createStaticGroupedItem ],
    [ DropListScope.QUEST ]: [ DropListScopeFunctions.onQuest, DropListScopeFunctions.createStaticGroupedItem ]
}