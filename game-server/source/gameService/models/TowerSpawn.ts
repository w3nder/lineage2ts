import { Location } from './Location'

export class TowerSpawn {
    npcId: number
    location: Location
    zoneList: Array<number> = []
    upgradeLevel: number = 0

    constructor( npcId: number, location: Location, zoneList: Array<number> = [] ) {
        this.npcId = npcId
        this.location = location
        this.zoneList = zoneList
    }

    getId() : number {
        return this.npcId
    }

    getLocation() : Location {
        return this.location
    }

    getZoneList() : Array<number> {
        return this.zoneList
    }

    setUpgradeLevel( level: number ) : void {
        this.upgradeLevel = level
    }

    getUpgradeLevel() : number {
        return this.upgradeLevel
    }
}