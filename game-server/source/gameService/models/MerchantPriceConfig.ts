import { Castle } from './entity/Castle'
import { CastleManager } from '../instancemanager/CastleManager'

export class MerchantPriceConfig {
    name: string
    baseTax: number
    castleId: number
    townId: number

    getBaseTaxRate() : number {
        return this.baseTax / 100.0
    }

    getCastle() : Castle {
        return CastleManager.getCastleById( this.castleId )
    }

    getCastleTaxRate() : number {
        return this.hasCastle() ? this.getCastle().getTaxRate() : 0.0
    }

    hasCastle() : boolean {
        return !!this.getCastle()
    }

    getTotalTax() : number {
        return this.hasCastle() ? ( this.getCastle().getTaxPercent() + this.getBaseTax() ) : this.getBaseTax()
    }

    getBaseTax() {
        return this.baseTax
    }

    getTotalTaxRate() : number {
        return this.getTotalTax() / 100.0
    }
}