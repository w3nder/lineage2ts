import { ListenerLogic } from '../ListenerLogic'
import { ListenerManager } from '../../instancemanager/ListenerManager'
import { L2World } from '../../L2World'
import { PacketDispatcher } from '../../PacketDispatcher'
import { QuestList } from '../../packets/send/QuestList'
import { QuestStateValues } from './State'
import { SoundPacket } from '../../packets/send/SoundPacket'
import { ExShowQuestMark } from '../../packets/send/ExShowQuestMark'
import { QuestType } from '../../enums/QuestType'
import { QuestStateCache } from '../../cache/QuestStateCache'
import { PlayerRadarCache } from '../../cache/PlayerRadarCache'
import { QuestHelper } from '../../../listeners/helpers/QuestHelper'
import { L2PcInstance } from '../actor/instance/L2PcInstance'
import _ from 'lodash'
import moment from 'moment'

// TODO : remove nR* variables and replace these with normal managed variables inside each listener
// TODO : remove memoStateEx* variables and replace these with normal managed variables inside each listener
// TODO : remove nRFlagJournal variable and replace it with normal managed variable inside each listener
const enum QuestStateVariables {
    memoState = 'memoState',
    memoStateEx = 'memoStateEx',
    memoStateEx1 = 'memoStateEx1',
    memoStateEx2 = 'memoStateEx2',
    memoStateEx3 = 'memoStateEx3',
    memoStateEx4 = 'memoStateEx4',
    memoStateEx5 = 'memoStateEx5',
    nRMemoState = 'NRmemoState',
    nRMemoState1 = 'NRmemoState1',
    nRMemoState2 = 'NRmemoState2',
    nRMemoState3 = 'NRmemoState3',
    nRMemoState4 = 'NRmemoState4',
    nRMemoState5 = 'NRmemoState5',
    nRmemoStateEx = 'NRmemoStateEx',
    nRmemoStateEx1 = 'NRmemoStateEx1',
    nRmemoStateEx2 = 'NRmemoStateEx2',
    nRmemoStateEx3 = 'NRmemoStateEx3',
    nRmemoStateEx4 = 'NRmemoStateEx4',
    nRmemoStateEx5 = 'NRmemoStateEx5',
    nRFlagJournal = 'NRFlagJournal',
    nRmemo = 'NRmemo',
    restartTime = 'qs-restartTime',
}

function getMemoStateExVariable( value: number ): string {
    switch ( value ) {
        case 1:
            return QuestStateVariables.memoStateEx1

        case 2:
            return QuestStateVariables.memoStateEx2

        case 3:
            return QuestStateVariables.memoStateEx3

        case 4:
            return QuestStateVariables.memoStateEx4

        case 5:
            return QuestStateVariables.memoStateEx5

        default:
            return `${ QuestStateVariables.memoStateEx }${ value }`
    }
}

function getNRMemoStateVariable( value: number ): string {
    switch ( value ) {
        case 1:
            return QuestStateVariables.nRMemoState1

        case 2:
            return QuestStateVariables.nRMemoState2

        case 3:
            return QuestStateVariables.nRMemoState3

        case 4:
            return QuestStateVariables.nRMemoState4

        case 5:
            return QuestStateVariables.nRMemoState5

        default:
            return `${ QuestStateVariables.nRMemoState }${ value }`
    }
}

function getNRMemoStateExVariable( value: number ): string {
    switch ( value ) {
        case 1:
            return QuestStateVariables.nRmemoStateEx1

        case 2:
            return QuestStateVariables.nRmemoStateEx2

        case 3:
            return QuestStateVariables.nRmemoStateEx3

        case 4:
            return QuestStateVariables.nRmemoStateEx4

        case 5:
            return QuestStateVariables.nRmemoStateEx5

        default:
            return `${ QuestStateVariables.nRmemoStateEx }${ value }`
    }
}

export type QuestStateVariableType = number | string | boolean

export class QuestState {
    questName: string
    questId: number
    playerId: number
    hasProperQuestValue: boolean = false
    state: QuestStateValues
    variables: { [ key: string ]: QuestStateVariableType } = {}
    condition: number = 0
    stateFlags: number = -1

    constructor( ownerId: number, quest: ListenerLogic, currentState: QuestStateValues ) {
        this.questName = quest.getName()
        this.playerId = ownerId
        this.state = currentState
        this.questId = quest.getId()
        this.hasProperQuestValue = this.questId <= 19999 || this.questId > 1
    }

    addRadar( x: number, y: number, z: number ) {
        PlayerRadarCache.getRadar( this.playerId ).addMarker( x, y, z )
    }

    clearRadar() {
        PlayerRadarCache.getRadar( this.playerId ).removeAllMarkers()
    }

    async exitQuest( isRepeatable: boolean, playExitQuest: boolean = false ): Promise<void> {
        if ( !this.isStarted() ) {
            return
        }

        let player = L2World.getPlayer( this.playerId )
        let quest = this.getQuest()

        if ( quest ) {
            await quest.removeRegisteredQuestItems( player )
        }

        this.variables = {}

        if ( isRepeatable ) {
            QuestStateCache.scheduleDeletion( this.playerId, this.questName )
            PacketDispatcher.sendDebouncedPacket( this.playerId, QuestList )
        } else {
            this.setState( QuestStateValues.COMPLETED )
            this.persistStateVariables()
        }

        if ( playExitQuest ) {
            PacketDispatcher.sendCopyData( this.playerId, SoundPacket.ITEMSOUND_QUEST_FINISH )
        }

        return
    }

    async exitQuestType( type: QuestType ): Promise<void> {
        if ( type === QuestType.DAILY ) {
            await this.exitQuest( false )
            this.setRestartTime()
            return
        }

        return this.exitQuest( type === QuestType.REPEATABLE )
    }

    async exitQuestWithType( type: QuestType, shouldPlaySound: boolean ): Promise<void> {
        await this.exitQuestType( type )

        if ( shouldPlaySound ) {
            PacketDispatcher.sendCopyData( this.playerId, SoundPacket.ITEMSOUND_QUEST_FINISH )
        }
    }

    getCondition(): number {
        if ( this.isStarted() ) {
            return this.condition
        }

        return 0
    }

    getMemoState(): number {
        if ( this.isStarted() ) {
            return this.getVariable( QuestStateVariables.memoState )
        }

        return -1
    }

    getMemoStateEx( slot: number ) {
        return this.getVariable( getMemoStateExVariable( slot ) )
    }

    getNRMemoState( slot: number ) {
        return this.getVariable( getNRMemoStateVariable( slot ) )
    }

    getNRMemoStateEx( slot: number ) {
        return this.getVariable( getNRMemoStateExVariable( slot ) )
    }

    getPlayer() : L2PcInstance {
        return L2World.getPlayer( this.playerId )
    }

    getPlayerId() : number {
        return this.playerId
    }

    getQuest(): ListenerLogic {
        return ListenerManager.getListenerByName( this.questName )
    }

    getQuestName() : string {
        return this.questName
    }

    getState(): QuestStateValues {
        return this.state
    }

    getStateFlags(): number {
        return this.stateFlags
    }

    getVariable( name: string ): any {
        return _.defaultTo( this.variables[ name ], 0 )
    }

    hasProperQuest(): boolean {
        return this.hasProperQuestValue
    }

    haveNRMemo( slot: number ) {
        return this.getVariable( QuestStateVariables.nRmemo ) === slot
    }

    isCompleted(): boolean {
        return this.state === QuestStateValues.COMPLETED
    }

    isCondition( condition: number ) {
        return this.condition === condition
    }

    isCreated(): boolean {
        return this.state === QuestStateValues.CREATED
    }

    isMemoState( value: number ): boolean {
        return this.getVariable( QuestStateVariables.memoState ) === value
    }

    isNowAvailable(): boolean {
        return Date.now() > this.getVariable( QuestStateVariables.restartTime )
    }

    isStarted(): boolean {
        return this.state === QuestStateValues.STARTED
    }

    removeNRMemo() {
        this.unsetVariable( QuestStateVariables.nRmemo )
    }

    removeRadar( x: number, y: number, z: number ) {
        PlayerRadarCache.getRadar( this.playerId ).removeMarker( x, y, z )
    }

    resetCondition( value: number ): void {
        if ( !this.computeConditionStateFlags( value ) ) {
            return
        }

        this.condition = value
    }

    setConditionWithSound( value: number, playSound: boolean = false ): void {
        if ( !this.isStarted() || !this.computeConditionStateFlags( value ) ) {
            return
        }

        this.condition = value

        QuestStateCache.scheduleUpdate( this )

        if ( playSound ) {
            PacketDispatcher.sendCopyData( this.playerId, SoundPacket.ITEMSOUND_QUEST_MIDDLE )
        }
    }

    setMemoState( value: number ): void {
        this.setVariable( QuestStateVariables.memoState, value )
    }

    setMemoStateEx( slot: number, value: number ) {
        this.setVariable( `${ QuestStateVariables.memoStateEx }${ slot }`, value )
    }

    setNRFlagJournal( id: number ) {
        this.setVariable( QuestStateVariables.nRFlagJournal, id )
    }

    setNRMemo( value: number ) {
        this.setVariable( QuestStateVariables.nRmemo, value )
    }

    setNRMemoState( slot: number, value: number ) {
        this.setVariable( `${ QuestStateVariables.nRMemoState }${ slot }`, value )
    }

    setNRMemoStateEx( slot: number, value: number ) {
        this.setVariable( `${ QuestStateVariables.nRmemoStateEx }${ slot }`, value )
    }

    setRestartTime(): void {
        let questTime = moment()
        if ( questTime.hours() >= this.getQuest().getResetHour() ) {
            questTime.add( 1, 'day' )
        }

        questTime.hours( this.getQuest().getResetHour() )
        questTime.minutes( this.getQuest().getResetMinutes() )

        this.setVariable( QuestStateVariables.restartTime, questTime.valueOf() )
    }

    setState( state: QuestStateValues, saveInDatabase: boolean = true ): void {
        if ( this.state === state ) {
            return
        }

        this.state = state

        if ( saveInDatabase ) {
            QuestStateCache.scheduleUpdate( this )
        }

        PacketDispatcher.sendDebouncedPacket( this.playerId, QuestList )
    }

    setVariable( name: string, value: any ) {
        this.variables[ name ] = value
        this.persistStateVariables()
    }

    incrementVariable( name: string, incrementValue: number ): void {
        let value = _.get( this.variables, name, 0 ) as number
        this.variables[ name ] = value + incrementValue
        this.persistStateVariables()
    }

    showQuestionMark( id: number ): void {
        QuestHelper.showQuestionMark( this.playerId, id )
    }

    startQuest( playSound: boolean = true, condition: number = 1 ) : void {
        if ( !this.isCreated() ) {
            return
        }

        this.setState( QuestStateValues.STARTED, false )
        this.resetCondition( condition )

        QuestStateCache.scheduleUpdate( this )

        if ( playSound ) {
            PacketDispatcher.sendCopyData( this.playerId, SoundPacket.ITEMSOUND_QUEST_ACCEPT )
        }
    }

    unsetVariable( name: string ): void {
        if ( _.has( this.variables, name ) ) {
            _.unset( this.variables, name )
            this.persistStateVariables()
        }
    }

    unsetVariables( ...names: Array<string> ): void {
        this.variables = _.omit( this.variables, names )
        this.persistStateVariables()
    }

    private computeConditionStateFlags( currentValue: number ): boolean {
        let previousValue: number = this.condition
        if ( currentValue === previousValue ) {
            return false
        }

        let completedStateFlags = 0
        // cond 0 and 1 do not need completedStateFlags. Also, if cond > 1, the 1st step must
        // always exist (i.e. it can never be skipped). So if cond is 2, we can still safely
        // assume no steps have been skipped.
        // Finally, more than 31 steps CANNOT be supported in any way with skipping.
        if ( ( currentValue < 3 ) || ( currentValue > 31 ) ) {
            this.stateFlags = -1
        } else {
            completedStateFlags = this.stateFlags
        }

        // case 1: No steps have been skipped so far...
        if ( completedStateFlags === 0 ) {
            // check if this step also doesn't skip anything. If so, no further work is needed
            // also, in this case, no work is needed if the state is being reset to a smaller value
            // in those cases, skip forward to informing the client about the change...

            // ELSE, if we just now skipped for the first time...prepare the flags!!!
            if ( currentValue > ( previousValue + 1 ) ) {
                // set the most significant bit to 1 (indicates that there exist skipped states)
                // also, ensure that the least significant bit is an 1 (the first step is never skipped, no matter
                // what the cond says)
                completedStateFlags = 0x80000001

                // since no flag had been skipped until now, the least significant bits must all
                // be set to 1, up until "old" number of bits.
                completedStateFlags |= ( ( 1 << previousValue ) - 1 )

                // now, just set the bit corresponding to the passed cond to 1 (current step)
                completedStateFlags |= ( 1 << ( currentValue - 1 ) )
                this.stateFlags = completedStateFlags
            }
        } else if ( currentValue < previousValue ) {
            // case 2: There were exist previously skipped steps
            // if this is a push back to a previous step, clear all completion flags ahead
            // note, this also unsets the flag indicating that there exist skips
            completedStateFlags &= ( ( 1 << currentValue ) - 1 )

            // now, check if this resulted in no steps being skipped any more
            if ( completedStateFlags === ( ( 1 << currentValue ) - 1 ) ) {
                this.stateFlags = -1
            } else {
                // set the most significant bit back to 1 again, to correctly indicate that this skips states.
                // also, ensure that the least significant bit is an 1 (the first step is never skipped, no matter
                // what the cond says)
                completedStateFlags |= 0x80000001
                this.stateFlags = completedStateFlags
            }
        } else {
            // If this moves forward, it changes nothing on previously skipped steps.
            // Just mark this state and we are done.
            completedStateFlags |= ( 1 << ( currentValue - 1 ) )
            this.stateFlags = completedStateFlags
        }

        PacketDispatcher.sendDebouncedPacket( this.playerId, QuestList )

        if ( this.hasProperQuest() && ( currentValue > 0 ) ) {
            PacketDispatcher.sendOwnedData( this.playerId, ExShowQuestMark( this.questId ) )
        }

        return true
    }

    private persistStateVariables() : void {
        QuestStateCache.scheduleVariablesUpdate( this )
    }
}