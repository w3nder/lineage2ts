export const enum QuestStateValues {
    CREATED,
    STARTED,
    COMPLETED
}

export const enum QuestStateLimits {
    MaximumQuestsAllowed = 40
}