import { GameClient } from '../../GameClient'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2Event } from '../../models/entity/L2Event'
import { LeaveWorld } from '../send/LeaveWorld'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder, SystemMessageHelper } from '../send/SystemMessage'
import { FastRateLimit } from 'fast-ratelimit'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, GeneralViolationEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 15, // time-to-live value of token bucket (in seconds)
} )

export function Logout( client : GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        client.abortConnection()
        client.unloadPlayer()
        return
    }

    /*
        Player is logged in, however has not chosen any character and choose option to 'Re-Login'.
        Bottom right location in L2 client.
     */
    let player : L2PcInstance = client.player
    if ( !player ) {
        client.abortConnection()
        client.unloadPlayer()
        return
    }

    if ( ( player.activeEnchantItemId !== -1 ) || ( player.activeEnchantAttributeItemId !== -1 ) ) {
        if ( ListenerCache.hasGeneralListener( EventType.GeneralViolation ) ) {
            let data = EventPoolCache.getData( EventType.GeneralViolation ) as GeneralViolationEvent

            data.errorId = 'a0683a18-6ff9-4a1e-8d25-5a0416128932'
            data.playerId = player.getObjectId()
            data.message = 'Player tried to logout while enchanting'
        }

        return player.sendOwnedData( ActionFailed() )
    }

    if ( player.isLocked() ) {
        if ( ListenerCache.hasGeneralListener( EventType.GeneralViolation ) ) {
            let data = EventPoolCache.getData( EventType.GeneralViolation ) as GeneralViolationEvent

            data.errorId = '4b53f139-8b25-4610-a312-be6974730395'
            data.playerId = player.getObjectId()
            data.message = 'Player tried to logout during class change'
        }

        return player.sendOwnedData( ActionFailed() )
    }

    if ( player.isUnderAttack() ) {
        if ( player.isGM() && ConfigManager.general.gmRestartFighting() ) {
            return
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_LOGOUT_WHILE_FIGHTING ) )
        return player.sendOwnedData( ActionFailed() )
    }

    if ( L2Event.isParticipant( player.getObjectId() ) ) {
        player.sendOwnedData( SystemMessageHelper.sendString( 'A superior server power doesn\'t allow you to leave the event.' ) )
        return player.sendOwnedData( ActionFailed() )
    }

    client.sendPacket( LeaveWorld() )
}