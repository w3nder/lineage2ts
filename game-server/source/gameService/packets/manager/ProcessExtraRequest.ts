import { GameClient } from '../../GameClient'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 5, // time-to-live value of token bucket (in seconds)
} )

export async function ProcessExtraRequest( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let id : number = new ReadableClientPacket( packetData ).readH()

    switch ( id ) {
        case 0x36:
            if ( client.hasCharacterSelection() ) {
                await client.updateCharacterSelection()
            }

            client.sendCharacterSelection()
            return

        case 0x93:
            // TODO : RequestEx2ndPasswordCheck
            return
        case 0x94:
            // TODO : RequestEx2ndPasswordVerify
            return
        case 0x95:
            // TODO: RequestEx2ndPasswordReq
            return
    }
}