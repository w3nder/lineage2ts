import { GameClient } from '../../GameClient'
import { EventTerminationResult, EventType, PlayerRestoredEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 3, // time-to-live value of token bucket (in seconds)
} )

export async function CharacterRestore( client: GameClient, packetData: Buffer ): Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let characterSlot: number = new ReadableClientPacket( packetData ).readD()

    if ( ListenerCache.hasGeneralListener( EventType.PlayerRestored ) ) {
        let characterPackage = client.getCharacterSelection( characterSlot )

        let data: PlayerRestoredEvent = {
            accessLevel: characterPackage.accessLevel,
            level: characterPackage.level,
            sex: characterPackage.sex,
            classId: characterPackage.classId,
            playerId: characterPackage.objectId,
            playerName: characterPackage.name,

        }

        let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayerRestored, data )
        if ( result?.terminate ) {
            return
        }
    }

    await client.markCharacterAsRestored( characterSlot )
    await client.updateCharacterSelection()
    client.sendCharacterSelection()
}