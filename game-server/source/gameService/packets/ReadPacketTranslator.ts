import { GameClient } from '../GameClient'
import { Logout } from './manager/Logout'
import { AttackPacket } from './manager/AttackPacket'
import { RequestStartPledgeWar } from './receive/RequestStartPledgeWar'
import { RequestReplyStartPledgeWar } from './receive/RequestReplyStartPledgeWar'
import { RequestStopPledgeWar } from './receive/RequestStopPledgeWar'
import { RequestReplyStopPledgeWar } from './receive/RequestReplyStopPledgeWar'
import { RequestSurrenderPledgeWar } from './receive/RequestSurrenderPledgeWar'
import { RequestReplySurrenderPledgeWar } from './receive/RequestReplySurrenderPledgeWar'
import { RequestSetPledgeCrest } from './receive/RequestSetPledgeCrest'
import { RequestGiveNickName } from './receive/RequestGiveNickName'
import { MoveToLocation } from './receive/MoveToLocation'
import { RequestItemList } from './receive/RequestItemList'
import { RequestUnEquipItem } from './receive/RequestUnEquipItem'
import { RequestDropItem } from './receive/RequestDropItem'
import { UseItem } from './receive/UseItem'
import { TradeRequest } from './receive/TradeRequest'
import { AddTradeItem } from './receive/AddTradeItem'
import { TradeDonePacket } from './receive/TradeDonePacket'
import { ActionReceive } from './receive/ActionReceive'
import { RequestLinkHtml } from './receive/RequestLinkHtml'
import { RequestBypassToServer } from './receive/RequestBypassToServer'
import { RequestBBSWrite } from './receive/RequestBBSWrite'
import { RequestJoinPledge } from './receive/RequestJoinPledge'
import { RequestAnswerJoinPledge } from './receive/RequestAnswerJoinPledge'
import { RequestWithdrawalPledge } from './receive/RequestWithdrawalPledge'
import { RequestOustPledgeMember } from './receive/RequestOustPledgeMember'
import { RequestGetItemFromPet } from './receive/RequestGetItemFromPet'
import { RequestAllyInfo } from './receive/RequestAllyInfo'
import { RequestCrystallizeItem } from './receive/RequestCrystallizeItem'
import { RequestPrivateStoreManageSell } from './receive/RequestPrivateStoreManageSell'
import { SetPrivateStoreListSell } from './receive/SetPrivateStoreListSell'
import { AttackRequest } from './receive/AttackRequest'
import { RequestSellItem } from './receive/RequestSellItem'
import { RequestMagicSkillUse } from './receive/RequestMagicSkillUse'
import { Appearing } from './receive/Appearing'
import { SendWareHouseDepositList } from './receive/SendWareHouseDepositList'
import { SendWareHouseWithDrawList } from './receive/SendWareHouseWithDrawList'
import { RequestShortCutRegistration } from './receive/requestShortCutRegistration'
import { RequestShortCutDelete } from './receive/requestShortCutDelete'
import { RequestBuyItem } from './receive/RequestBuyItem'
import { RequestJoinParty } from './receive/RequestJoinParty'
import { RequestAnswerJoinParty } from './receive/RequestAnswerJoinParty'
import { RequestWithDrawalParty } from './receive/RequestWithDrawalParty'
import { RequestOustPartyMember } from './receive/RequestOustPartyMember'
import { CannotMoveAnymore } from './receive/CannotMoveAnymore'
import { RequestTargetCancel } from './receive/RequestTargetCanceld'
import { Say2 } from './receive/Say2'
import { RequestPledgeMemberList } from './receive/RequestPledgeMemberList'
import { RequestSkillList } from './receive/RequestSkillList'
import { RequestGetOnVehicle } from './receive/RequestGetOnVehicle'
import { RequestGetOffVehicle } from './receive/RequestGetOffVehicle'
import { AnswerTradeRequest } from './receive/AnswerTradeRequest'
import { RequestActionUse } from './receive/RequestActionUse'
import { RequestRestart } from './receive/RequestRestart'
import { ValidatePosition } from './receive/ValidatePosition'
import { StartRotating } from './receive/StartRotating'
import { FinishRotating } from './receive/FinishRotating'
import { RequestShowBoard } from './receive/RequestShowBoard'
import { RequestEnchantItem } from './receive/RequestEnchantItem'
import { RequestDestroyItem } from './receive/RequestDestroyItem'
import { RequestQuestList } from './receive/RequestQuestList'
import { RequestQuestAbort } from './receive/RequestQuestAbort'
import { RequestPledgeInfo } from './receive/RequestPledgeInfo'
import { RequestPledgeCrest } from './receive/RequestPledgeCrest'
import { RequestSendFriendMessage } from './receive/RequestSendFriendMessage'
import { RequestShowMiniMap } from './receive/RequestShowMiniMap'
import { RequestRecordInfo } from './receive/RequestRecordInfo'
import { RequestHennaEquip } from './receive/RequestHennaEquip'
import { RequestHennaRemoveList } from './receive/RequestHennaRemoveList'
import { RequestHennaItemRemoveInfo } from './receive/RequestHennaItemRemoveInfo'
import { RequestHennaRemove } from './receive/RequestHennaRemove'
import { RequestAcquireSkillInfo } from './receive/RequestAcquireSkillInfo'
import { SendBypassBuildCommand } from './receive/SendBypassBuildCommand'
import { RequestMoveToLocationInVehicle } from './receive/RequestMoveToLocationInVehicle'
import { CannotMoveAnymoreInVehicle } from './receive/CannotMoveAnymoreInVehicle'
import { RequestFriendInvite } from './receive/RequestFriendInvite'
import { RequestAnswerFriendInvite } from './receive/RequestAnswerFriendInvite'
import { RequestFriendList } from './receive/RequestFriendList'
import { RequestFriendDelete } from './receive/RequestFriendDelete'
import { RequestAcquireSkill } from './receive/RequestAcquireSkill'
import { RequestRestartPoint } from './receive/RequestRestartPoint'
import { RequestGMCommand } from './receive/RequestGMCommand'
import { RequestPartyMatchConfig } from './receive/RequestPartyMatchConfig'
import { RequestPartyMatchList } from './receive/RequestPartyMatchList'
import { RequestPartyMatchDetail } from './receive/RequestPartyMatchDetail'
import { RequestPrivateStoreBuy } from './receive/RequestPrivateStoreBuy'
import { RequestTutorialLinkHtml } from './receive/RequestTutorialLinkHtml'
import { RequestTutorialPassCmdToServer } from './receive/RequestTutorialPassCmdToServer'
import { RequestTutorialQuestionMark } from './receive/RequestTutorialQuestionMark'
import { RequestTutorialClientEvent } from './receive/RequestTutorialClientEvent'
import { RequestPetition } from './receive/RequestPetition'
import { RequestPetitionCancel } from './receive/RequestPetitionCancel'
import { RequestGmList } from './receive/RequestGmList'
import { RequestJoinAlly } from './receive/RequestJoinAlly'
import { RequestAnswerJoinAlly } from './receive/RequestAnswerJoinAlly'
import { AllyLeave } from './receive/AllyLeave'
import { AllyDismiss } from './receive/AllyDismiss'
import { RequestDismissAlly } from './receive/RequestDismissAlly'
import { RequestSetAllyCrest } from './receive/RequestSetAllyCrest'
import { RequestAllyCrest } from './receive/RequestAllyCrest'
import { RequestChangePetName } from './receive/RequestChangePetName'
import { RequestPetUseItem } from './receive/RequestPetUseItem'
import { RequestGiveItemToPet } from './receive/RequestGiveItemToPet'
import { RequestPrivateStoreQuitSell } from './receive/RequestPrivateStoreQuitSell'
import { SetPrivateStoreMessageSell } from './receive/SetPrivateStoreMessageSell'
import { RequestPetGetItem } from './receive/RequestPetGetItem'
import { RequestPrivateStoreManageBuy } from './receive/RequestPrivateStoreManageBuy'
import { SetPrivateStoreListBuy } from './receive/SetPrivateStoreListBuy'
import { RequestPrivateStoreQuitBuy } from './receive/RequestPrivateStoreQuitBuy'
import { SetPrivateStoreMessageBuy } from './receive/SetPrivateStoreMessageBuy'
import { RequestPrivateStoreSell } from './receive/RequestPrivateStoreSell'
import { RequestPackageSendableItemList } from './receive/RequestPackageSendableItemList'
import { RequestPackageSend } from './receive/RequestPackageSend'
import { RequestBlock } from './receive/RequestBlock'
import { RequestSiegeAttackerList } from './receive/RequestSiegeAttackerList'
import { RequestSiegeDefenderList } from './receive/RequestSiegeDefenderList'
import { RequestJoinSiege } from './receive/RequestJoinSiege'
import { RequestConfirmSiegeWaitingList } from './receive/RequestConfirmSiegeWaitingList'
import { RequestSetCastleSiegeTime } from './receive/RequestSetCastleSiegeTime'
import { MultiSellChoose } from './receive/MultiSellChoose'
import { BypassUserCommand } from './receive/BypassUserCommand'
import { SnoopQuit } from './receive/SnoopQuit'
import { RequestRecipeBookOpen } from './receive/RequestRecipeBookOpen'
import { RequestRecipeBookDestroy } from './receive/RequestRecipeBookDestroy'
import { RequestRecipeItemMakeInfo } from './receive/RequestRecipeItemMakeInfo'
import { RequestRecipeItemMakeSelf } from './receive/RequestRecipeItemMakeSelf'
import { RequestRecipeShopMessageSet } from './receive/RequestRecipeShopMessageSet'
import { RequestRecipeShopListSet } from './receive/RequestRecipeShopListSet'
import { RequestRecipeShopManageQuit } from './receive/RequestRecipeShopManageQuit'
import { RequestRecipeShopMakeInfo } from './receive/RequestRecipeShopMakeInfo'
import { RequestRecipeShopMakeItem } from './receive/RequestRecipeShopMakeItem'
import { RequestRecipeShopManagePreview } from './receive/RequestRecipeShopManagePreview'
import { ObserverReturn } from './receive/ObserverReturn'
import { RequestHennaItemList } from './receive/RequestHennaItemList'
import { RequestHennaItemInfo } from './receive/RequestHennaItemInfo'
import { RequestBuySeed } from './receive/RequestBuySeed'
import { DialogAnswer } from './receive/DialogAnswer'
import { RequestPreviewItem } from './receive/RequestPreviewItem'
import { RequestSevenSignStatus } from './receive/requestSevenSignStatus'
import { RequestPetitionFeedback } from './receive/RequestPetitionFeedback'
import { GameGuardReply } from './receive/GameGuardReply'
import { RequestPledgePower } from './receive/RequestPledgePower'
import { RequestMakeMacro } from './receive/RequestMakeMacro'
import { RequestDeleteMacro } from './receive/RequestDeleteMacro'
import { RequestManorList } from './receive/RequestManorList'
import { RequestProcureCropList } from './receive/RequestProcureCropList'
import { RequestSetSeed } from './receive/RequestSetSeed'
import { RequestSetCrop } from './receive/RequestSetCrop'
import { RequestWriteHeroWords } from './receive/RequestWriteHeroWords'
import { RequestExAskJoinMPCC } from './receive/RequestExAskJoinMPCC'
import { RequestExAcceptJoinMPCC } from './receive/RequestExAcceptJoinMPCC'
import { RequestExOustFromMPCC } from './receive/RequestExOustFromMPCC'
import { RequestRemoveFromPartyRoom } from './receive/requestRemoveFromPartyRoom'
import { RequestDismissPartyRoom } from './receive/RequestDismissPartyRoom'
import { RequestWithdrawPartyRoom } from './receive/RequestWithdrawPartyRoom'
import { RequestChangePartyLeader } from './receive/RequestChangePartyLeader'
import { RequestAutoSoulShot } from './receive/RequestAutoSoulShot'
import { RequestExEnchantSkillInfo } from './receive/RequestExEnchantSkillInfo'
import { RequestExEnchantSkill } from './receive/RequestExEnchantSkill'
import { RequestExPledgeCrestLarge } from './receive/RequestExPledgeCrestLarge'
import { RequestExSetPledgeCrestLarge } from './receive/RequestExSetPledgeCrestLarge'
import { RequestPledgeSetAcademyMaster } from './receive/RequestPledgeSetAcademyMaster'
import { RequestPledgePowerGradeList } from './receive/RequestPledgePowerGradeList'
import { RequestPledgeMemberPowerInfo } from './receive/RequestPledgeMemberPowerInfo'
import { RequestPledgeSetMemberPowerGrade } from './receive/RequestPledgeSetMemberPowerGrade'
import { RequestPledgeMemberInfo } from './receive/RequestPledgeMemberInfo'
import { RequestPledgeWarList } from './receive/RequestPledgeWarList'
import { RequestExFishRanking } from './receive/RequestExFishRanking'
import { RequestDuelStart } from './receive/RequestDuelStart'
import { RequestDuelAnswerStart } from './receive/RequestDuelAnswerStart'
import { RequestExRqItemLink } from './receive/RequestExRqItemLink'
import { MoveToLocationInAirShip } from './receive/MoveToLocationInAirShip'
import { RequestKeyMapping } from './receive/RequestKeyMapping'
import { RequestSaveKeyMapping } from './receive/RequestSaveKeyMapping'
import { RequestExRemoveItemAttribute } from './receive/RequestExRemoveItemAttribute'
import { RequestSaveInventoryOrder } from './receive/RequestSaveInventoryOrder'
import { RequestExitPartyMatchingWaitingRoom } from './receive/RequestExitPartyMatchingWaitingRoom'
import { RequestConfirmTargetItem } from './receive/RequestConfirmTargetItem'
import { RequestConfirmRefinerItem } from './receive/RequestConfirmRefinerItem'
import { RequestConfirmGemStone } from './receive/RequestConfirmGemStone'
import { RequestOlympiadObserverEnd } from './receive/RequestOlympiadObserverEnd'
import { RequestCursedWeaponList } from './receive/RequestCursedWeaponList'
import { RequestCursedWeaponLocation } from './receive/RequestCursedWeaponLocation'
import { RequestPledgeReorganizeMember } from './receive/RequestPledgeReorganizeMember'
import { RequestExMPCCShowPartyMembersInfo } from './receive/RequestExMPCCShowPartyMembersInfo'
import { RequestOlympiadMatchList } from './receive/RequestOlympiadMatchList'
import { RequestAskJoinPartyRoom } from './receive/RequestAskJoinPartyRoom'
import { AnswerJoinPartyRoom } from './receive/AnswerJoinPartyRoom'
import { RequestListPartyMatchingWaitingRoom } from './receive/RequestListPartyMatchingWaitingRoom'
import { RequestExEnchantSkillSafe } from './receive/RequestExEnchantSkillSafe'
import { RequestExEnchantSkillUntrain } from './receive/RequestExEnchantSkillUntrain'
import { RequestExEnchantSkillRouteChange } from './receive/RequestExEnchantSkillRouteChange'
import { RequestExEnchantItemAttribute } from './receive/RequestExEnchantItemAttribute'
import { RequestExGetOnAirShip } from './receive/RequestExGetOnAirShip'
import { MoveToLocationAirShip } from './receive/MoveToLocationAirShip'
import { RequestBidItemAuction } from './receive/RequestBidItemAuction'
import { RequestInfoItemAuction } from './receive/RequestInfoItemAuction'
import { RequestAllCastleInfo } from './receive/RequestAllCastleInfo'
import { RequestAllFortressInfo } from './receive/RequestAllFortressInfo'
import { RequestAllAgitInfo } from './receive/RequestAllAgitInfo'
import { RequestFortressSiegeInfo } from './receive/RequestFortressSiegeInfo'
import { RequestGetBossRecord } from './receive/RequestGetBossRecord'
import { RequestRefine } from './receive/RequestRefine'
import { RequestConfirmCancelItem } from './receive/RequestConfirmCancelItem'
import { RequestRefineCancel } from './receive/RequestRefineCancel'
import { RequestExMagicSkillUseGround } from './receive/RequestExMagicSkillUseGround'
import { RequestDuelSurrender } from './receive/RequestDuelSurrender'
import { RequestExEnchantSkillInfoDetail } from './receive/RequestExEnchantSkillInfoDetail'
import { RequestFortressMapInfo } from './receive/RequestFortressMapInfo'
import { SetPrivateStoreWholesaleMessage } from './receive/SetPrivateStoreWholesaleMessage'
import { RequestDispel } from './receive/RequestDispel'
import { RequestExTryToPutEnchantTargetItem } from './receive/RequestExTryToPutEnchantTargetItem'
import { RequestExTryToPutEnchantSupportItem } from './receive/RequestExTryToPutEnchantSupportItem'
import { RequestExCancelEnchantItem } from './receive/RequestExCancelEnchantItem'
import { RequestChangeNicknameColor } from './receive/RequestChangeNicknameColor'
import { RequestResetNickname } from './receive/RequestResetNickname'
import { RequestBookMarkSlotInfo } from './receive/RequestBookMarkSlotInfo'
import { RequestSaveBookMarkSlot } from './receive/RequestSaveBookMarkSlot'
import { RequestModifyBookMarkSlot } from './receive/RequestModifyBookMarkSlot'
import { RequestDeleteBookMarkSlot } from './receive/RequestDeleteBookMarkSlot'
import { RequestTeleportBookMark } from './receive/RequestTeleportBookMark'
import { RequestWithdrawDimesionalItem } from './receive/RequestWithdrawDimesionalItem'
import { RequestJoinDominionWar } from './receive/RequestJoinDominionWar'
import { RequestDominionInfo } from './receive/RequestDominionInfo'
import { RequestExCubeGameChangeTeam } from './receive/RequestExCubeGameChangeTeam'
import { EndScenePlayer } from './receive/EndScenePlayer'
import { RequestExCubeGameReadyAnswer } from './receive/RequestExCubeGameReadyAnswer'
import { RequestSeedPhase } from './receive/RequestSeedPhase'
import { RequestPostItemList } from './receive/RequestPostItemList'
import { RequestSendPost } from './receive/RequestSendPost'
import { RequestReceivedPostList } from './receive/RequestReceivedPostList'
import { RequestDeleteReceivedPost } from './receive/RequestDeleteReceivedPost'
import { RequestReceivedPost } from './receive/RequestReceivedPost'
import { RequestPostAttachment } from './receive/RequestPostAttachment'
import { RequestRejectPostAttachment } from './receive/RequestRejectPostAttachment'
import { RequestSentPostList } from './receive/RequestSentPostList'
import { RequestDeleteSentPost } from './receive/RequestDeleteSentPost'
import { RequestSentPost } from './receive/RequestSentPost'
import { RequestCancelPostAttachment } from './receive/RequestCancelPostAttachment'
import { RequestRefundItem } from './receive/RequestRefundItem'
import { RequestBuySellUIClose } from './receive/RequestBuySellUIClose'
import { RequestPartyLootModification } from './receive/RequestPartyLootModification'
import { AnswerPartyLootModification } from './receive/AnswerPartyLootModification'
import { AnswerCoupleAction } from './receive/AnswerCoupleAction'
import { HalloweenEventRankerList } from './receive/HalloweenEventRankerList'
import { RequestVoteNew } from './receive/RequestVoteNew'
import { RequestExAddContactToContactList } from './receive/RequestExAddContactToContactList'
import { RequestExDeleteContactFromContactList } from './receive/RequestExDeleteContactFromContactList'
import { RequestExShowContactList } from './receive/RequestExShowContactList'
import { RequestExFriendListExtended } from './receive/RequestExFriendListExtended'
import { RequestExOlympiadMatchListRefresh } from './receive/RequestExOlympiadMatchListRefresh'
import _ from 'lodash'
import { ConfigManager } from '../../config/ConfigManager'
import chalk from 'chalk'
import logSymbols from 'log-symbols'

export type PacketHandlingMethod = ( client: GameClient, data?: Buffer ) => Promise<void> | void
export type PacketMethodMap = { [ code: number ]: PacketHandlingMethod }
export const PacketTranslatorExtension: PacketHandlingMethod = ( client: GameClient, data: Buffer ) => {
    /*
        First byte is opcode.
        Two more bytes are required for sub-opcode.
     */
    if ( data.length < 3 ) {
        return
    }

    let packetId: number = data.readInt16LE( 1 )
    let method = HighFiveExtendedPacketMethods[ packetId ]

    if ( ConfigManager.diagnostic.showIncomingPackets() ) {
        console.log( logSymbols.info, `${ chalk.green( method.name )} subOpCode = 0x${ chalk.yellow( packetId.toString( 16 ) )}` )
    }

    return method( client, data.subarray( 2 ) )
}

export const PacketTranslatorBookmarks: PacketHandlingMethod = ( client: GameClient, data: Buffer ) => {
    if ( data.length < 4 ) {
        return
    }

    let bookmarkId = data.readInt32LE( 1 )
    let method = HighFiveBookmarkMethods[ bookmarkId ]

    if ( ConfigManager.diagnostic.showIncomingPackets() ) {
        console.log( logSymbols.info, `${ chalk.green( method.name )} bookmarkId =${ chalk.yellow( bookmarkId.toString( 16 ) )}` )
    }

    return method( client, data.subarray( 4 ) )
}

export const HighFivePacketMethods: PacketMethodMap = {
    0x00: Logout,
    0x01: AttackPacket,
    0x03: RequestStartPledgeWar,
    0x04: RequestReplyStartPledgeWar,
    0x05: RequestStopPledgeWar,
    0x06: RequestReplyStopPledgeWar,
    0x07: RequestSurrenderPledgeWar,
    0x08: RequestReplySurrenderPledgeWar,
    0x09: RequestSetPledgeCrest,
    0x0b: RequestGiveNickName,
    0x0f: MoveToLocation,
    0x12: _.noop, // CharacterSelect, in case of player spam clicks on login screen
    0x14: RequestItemList,
    0x15: _.noop, // RequestEquipItem
    0x16: RequestUnEquipItem,
    0x17: RequestDropItem,
    0x19: UseItem,
    0x1a: TradeRequest,
    0x1b: AddTradeItem,
    0x1c: TradeDonePacket,
    0x1f: ActionReceive,
    0x22: RequestLinkHtml,
    0x23: RequestBypassToServer,
    0x24: RequestBBSWrite,
    0x25: _.noop, // RequestCreatePledge
    0x26: RequestJoinPledge,
    0x27: RequestAnswerJoinPledge,
    0x28: RequestWithdrawalPledge,
    0x29: RequestOustPledgeMember,
    0x2c: RequestGetItemFromPet,
    0x2e: RequestAllyInfo,
    0x2f: RequestCrystallizeItem,
    0x30: RequestPrivateStoreManageSell,
    0x31: SetPrivateStoreListSell,
    0x32: AttackRequest,
    0x33: _.noop, // RequestTeleportPacket
    0x34: _.noop, // RequestSocialAction
    0x35: _.noop, // ChangeMoveType
    0x36: _.noop, // ChangeWaitType
    0x37: RequestSellItem,
    0x38: _.noop, // RequestMagicSkillList
    0x39: RequestMagicSkillUse,
    0x3a: Appearing,
    0x3b: SendWareHouseDepositList,
    0x3c: SendWareHouseWithDrawList,
    0x3d: RequestShortCutRegistration,
    0x3f: RequestShortCutDelete,
    0x40: RequestBuyItem,
    0x41: _.noop, // RequestDismissPledge
    0x42: RequestJoinParty,
    0x43: RequestAnswerJoinParty,
    0x44: RequestWithDrawalParty,
    0x45: RequestOustPartyMember,
    0x46: _.noop, // RequestDismissParty
    0x47: CannotMoveAnymore,
    0x48: RequestTargetCancel,
    0x49: Say2,
    0x4a: _.noop, // TODO : SuperCmdCharacterInfo, SuperCmdSummonCmd, SuperCmdServerStatus, SendL2ParamSetting
    0x4d: RequestPledgeMemberList,
    0x4f: _.noop, // RequestMagicList
    0x50: RequestSkillList,
    0x52: _.noop, // TODO : implement MoveWithDelta
    0x53: RequestGetOnVehicle,
    0x54: RequestGetOffVehicle,
    0x55: AnswerTradeRequest,
    0x56: RequestActionUse,
    0x57: RequestRestart,
    0x58: _.noop, // TODO : implement RequestSiegeInfo
    0x59: ValidatePosition,
    0x5a: _.noop, // TODO : implement RequestSEKCustom
    0x5b: StartRotating,
    0x5c: FinishRotating,
    0x5e: RequestShowBoard,
    0x5f: RequestEnchantItem,
    0x60: RequestDestroyItem,
    0x62: RequestQuestList,
    0x63: RequestQuestAbort,
    0x65: RequestPledgeInfo,
    0x66: _.noop, // TODO : implement RequestPledgeExtendedInfo
    0x67: RequestPledgeCrest,
    0x6b: RequestSendFriendMessage,
    0x6c: RequestShowMiniMap,
    0x6d: _.noop, // RequestSendMSNChatLog
    0x6e: RequestRecordInfo,
    0x6f: RequestHennaEquip,
    0x70: RequestHennaRemoveList,
    0x71: RequestHennaItemRemoveInfo,
    0x72: RequestHennaRemove,
    0x73: RequestAcquireSkillInfo,
    0x74: SendBypassBuildCommand,
    0x75: RequestMoveToLocationInVehicle,
    0x76: CannotMoveAnymoreInVehicle,
    0x77: RequestFriendInvite,
    0x78: RequestAnswerFriendInvite,
    0x79: RequestFriendList,
    0x7a: RequestFriendDelete,
    0x7c: RequestAcquireSkill,
    0x7d: RequestRestartPoint,
    0x7e: RequestGMCommand,
    0x7f: RequestPartyMatchConfig,
    0x80: RequestPartyMatchList,
    0x81: RequestPartyMatchDetail,
    0x83: RequestPrivateStoreBuy,
    0x85: RequestTutorialLinkHtml,
    0x86: RequestTutorialPassCmdToServer,
    0x87: RequestTutorialQuestionMark,
    0x88: RequestTutorialClientEvent,
    0x89: RequestPetition,
    0x8a: RequestPetitionCancel,
    0x8b: RequestGmList,
    0x8c: RequestJoinAlly,
    0x8d: RequestAnswerJoinAlly,
    0x8e: AllyLeave,
    0x8f: AllyDismiss,
    0x90: RequestDismissAlly,
    0x91: RequestSetAllyCrest,
    0x92: RequestAllyCrest,
    0x93: RequestChangePetName,
    0x94: RequestPetUseItem,
    0x95: RequestGiveItemToPet,
    0x96: RequestPrivateStoreQuitSell,
    0x97: SetPrivateStoreMessageSell,
    0x98: RequestPetGetItem,
    0x99: RequestPrivateStoreManageBuy,
    0x9a: SetPrivateStoreListBuy,
    0x9c: RequestPrivateStoreQuitBuy,
    0x9d: SetPrivateStoreMessageBuy,
    0x9f: RequestPrivateStoreSell,
    0xa0: _.noop, // SendTimeCheckPacket
    0xa6: _.noop, // RequestSkillCoolTime
    0xa7: RequestPackageSendableItemList,
    0xa8: RequestPackageSend,
    0xa9: RequestBlock,
    0xaa: _.noop, // TODO : implement RequestSiegeInfo
    0xab: RequestSiegeAttackerList,
    0xac: RequestSiegeDefenderList,
    0xad: RequestJoinSiege,
    0xae: RequestConfirmSiegeWaitingList,
    0xaf: RequestSetCastleSiegeTime,
    0xb0: MultiSellChoose,
    0xb1: _.noop, // NetPing
    0xb2: _.noop, // RequestRemainTime
    0xb3: BypassUserCommand,
    0xb4: SnoopQuit,
    0xb5: RequestRecipeBookOpen,
    0xb6: RequestRecipeBookDestroy,
    0xb7: RequestRecipeItemMakeInfo,
    0xb8: RequestRecipeItemMakeSelf,
    0xb9: _.noop, // RequestRecipeShopManageList
    0xba: RequestRecipeShopMessageSet,
    0xbb: RequestRecipeShopListSet,
    0xbc: RequestRecipeShopManageQuit,
    0xbd: _.noop, // RequestRecipeShopManageCancel
    0xbe: RequestRecipeShopMakeInfo,
    0xbf: RequestRecipeShopMakeItem,
    0xc0: RequestRecipeShopManagePreview,
    0xc1: ObserverReturn,
    0xc2: _.noop, // RequestEvaluate/VoteSocially
    0xc3: RequestHennaItemList,
    0xc4: RequestHennaItemInfo,
    0xc5: RequestBuySeed,
    0xc6: DialogAnswer,
    0xc7: RequestPreviewItem,
    0xc8: RequestSevenSignStatus,
    0xc9: RequestPetitionFeedback,
    0xcb: GameGuardReply,
    0xcc: RequestPledgePower,
    0xcd: RequestMakeMacro,
    0xce: RequestDeleteMacro,
    0xcf: _.noop, // RequestProcureCrop
    0xd0: PacketTranslatorExtension,
}

const HighFiveExtendedPacketMethods: PacketMethodMap = {
    0x01: RequestManorList,
    0x02: RequestProcureCropList,
    0x03: RequestSetSeed,
    0x04: RequestSetCrop,
    0x05: RequestWriteHeroWords,
    0x06: RequestExAskJoinMPCC,
    0x07: RequestExAcceptJoinMPCC,
    0x08: RequestExOustFromMPCC,
    0x09: RequestRemoveFromPartyRoom,
    0x0a: RequestDismissPartyRoom,
    0x0b: RequestWithdrawPartyRoom,
    0x0c: RequestChangePartyLeader,
    0x0d: RequestAutoSoulShot,
    0x0e: RequestExEnchantSkillInfo,
    0x0f: RequestExEnchantSkill,
    0x10: RequestExPledgeCrestLarge,
    0x11: RequestExSetPledgeCrestLarge,
    0x12: RequestPledgeSetAcademyMaster,
    0x13: RequestPledgePowerGradeList,
    0x14: RequestPledgeMemberPowerInfo,
    0x15: RequestPledgeSetMemberPowerGrade,
    0x16: RequestPledgeMemberInfo,
    0x17: RequestPledgeWarList,
    0x18: RequestExFishRanking,
    0x19: _.noop, // TODO : implement RequestPCCafeCouponUse
    0x1b: RequestDuelStart,
    0x1c: RequestDuelAnswerStart,
    0x1d: _.noop, // RequestExSetTutorial
    0x1e: RequestExRqItemLink,
    0x1f: _.noop, // CanNotMoveAnymoreAirShip
    0x20: MoveToLocationInAirShip,
    0x21: RequestKeyMapping,
    0x22: RequestSaveKeyMapping,
    0x23: RequestExRemoveItemAttribute,
    0x24: RequestSaveInventoryOrder,
    0x25: RequestExitPartyMatchingWaitingRoom,
    0x26: RequestConfirmTargetItem,
    0x27: RequestConfirmRefinerItem,
    0x28: RequestConfirmGemStone,
    0x29: RequestOlympiadObserverEnd,
    0x2a: RequestCursedWeaponList,
    0x2b: RequestCursedWeaponLocation,
    0x2c: RequestPledgeReorganizeMember,
    0x2d: RequestExMPCCShowPartyMembersInfo,
    0x2e: RequestOlympiadMatchList,
    0x2f: RequestAskJoinPartyRoom,
    0x30: AnswerJoinPartyRoom,
    0x31: RequestListPartyMatchingWaitingRoom,
    0x32: RequestExEnchantSkillSafe,
    0x33: RequestExEnchantSkillUntrain,
    0x34: RequestExEnchantSkillRouteChange,
    0x35: RequestExEnchantItemAttribute,
    0x36: RequestExGetOnAirShip,
    0x38: MoveToLocationAirShip,
    0x39: RequestBidItemAuction,
    0x3a: RequestInfoItemAuction,
    0x3b: _.noop, // TODO : implement RequestExChangeName
    0x3c: RequestAllCastleInfo,
    0x3d: RequestAllFortressInfo,
    0x3e: RequestAllAgitInfo,
    0x3f: RequestFortressSiegeInfo,
    0x40: RequestGetBossRecord,
    0x41: RequestRefine,
    0x42: RequestConfirmCancelItem,
    0x43: RequestRefineCancel,
    0x44: RequestExMagicSkillUseGround,
    0x45: RequestDuelSurrender,
    0x46: RequestExEnchantSkillInfoDetail,
    0x48: RequestFortressMapInfo,
    0x49: _.noop, // RequestPVPMatchRecord
    0x4a: SetPrivateStoreWholesaleMessage,
    0x4b: RequestDispel,
    0x4c: RequestExTryToPutEnchantTargetItem,
    0x4d: RequestExTryToPutEnchantSupportItem,
    0x4e: RequestExCancelEnchantItem,
    0x4f: RequestChangeNicknameColor,
    0x50: RequestResetNickname,
    0x51: PacketTranslatorBookmarks,
    0x52: RequestWithdrawDimesionalItem,
    0x53: _.noop, // RequestJump
    0x54: _.noop, // RequestStartShowKrateiCubeRank
    0x55: _.noop, // RequestStopShowKrateiCubeRank
    0x56: _.noop, // NotifyStartMiniGame
    0x57: RequestJoinDominionWar,
    0x58: RequestDominionInfo,
    0x59: _.noop, // RequestExCleftEnter
    0x5a: RequestExCubeGameChangeTeam,
    0x5b: EndScenePlayer,
    0x5c: RequestExCubeGameReadyAnswer,
    0x5d: _.noop, // TODO : RequestListMpccWaiting chddd
    0x5e: _.noop, // TODO : RequestManageMpccRoom chdddddS
    0x5f: _.noop, // TODO : RequestJoinMpccRoom chdd
    0x63: RequestSeedPhase,
    0x65: RequestPostItemList,
    0x66: RequestSendPost,
    0x67: RequestReceivedPostList,
    0x68: RequestDeleteReceivedPost,
    0x69: RequestReceivedPost,
    0x6a: RequestPostAttachment,
    0x6b: RequestRejectPostAttachment,
    0x6c: RequestSentPostList,
    0x6d: RequestDeleteSentPost,
    0x6e: RequestSentPost,
    0x6f: RequestCancelPostAttachment,
    0x70: _.noop, // RequestShowNewUserPetition
    0x71: _.noop, // RequestShowStepThree
    0x72: _.noop, // RequestShowStepTwo
    0x73: _.noop, // ExRaidReserveResult
    0x75: RequestRefundItem,
    0x76: RequestBuySellUIClose,
    0x77: _.noop, // RequestEventMatchObserverEnd
    0x78: RequestPartyLootModification,
    0x79: AnswerPartyLootModification,
    0x7a: AnswerCoupleAction,
    0x7b: HalloweenEventRankerList,
    0x7c: _.noop, // AskMembership
    0x7d: _.noop, // RequestAddExpandQuestAlarm
    0x7e: RequestVoteNew,
    0x84: RequestExAddContactToContactList,
    0x85: RequestExDeleteContactFromContactList,
    0x86: RequestExShowContactList,
    0x87: RequestExFriendListExtended,
    0x88: RequestExOlympiadMatchListRefresh,
    0x89: _.noop, // RequestBRGamePoint
    0x8A: _.noop, // RequestBRProductList
    0x8B: _.noop, // RequestBRProductInfo
    0x8C: _.noop, // RequestBRBuyProduct
    0x8D: _.noop, // RequestBRRecentProductList
    0x8E: _.noop, // BrMinigameLoadScores
    0x8F: _.noop, // BrMinigameInsertScore
    0x90: _.noop, // BrLectureMark
    0x91: _.noop, // RequestGoodsInventoryInfo
    0x92: _.noop, // RequestUseGoodsInventoryItem
}

const HighFiveBookmarkMethods: PacketMethodMap = {
    0x00: RequestBookMarkSlotInfo,
    0x01: RequestSaveBookMarkSlot,
    0x02: RequestModifyBookMarkSlot,
    0x03: RequestDeleteBookMarkSlot,
    0x04: RequestTeleportBookMark,
    0x05: _.noop, // RequestChangeBookMarkSlot
}