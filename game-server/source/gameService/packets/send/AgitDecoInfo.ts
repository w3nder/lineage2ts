import { AuctionableHall } from '../../models/entity/clanhall/AuctionableHall'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { ClanHallFunctionType } from '../../enums/clanhall/ClanHallFunctionType'
import { ClanHallFunction } from '../../models/clan/ClanHallFunction'

type InsufficientLevelMethod = ( clanHall: AuctionableHall, data: ClanHallFunction ) => boolean

interface OrderData {
    type: ClanHallFunctionType
    isInsufficientLevel: InsufficientLevelMethod
}

const restoreMp : OrderData = {
    type: ClanHallFunctionType.RestoreMp,
    isInsufficientLevel: ( clanHall: AuctionableHall, data: ClanHallFunction ): boolean => {
        return ( ( ( clanHall.getGrade() === 0 )
                        || ( clanHall.getGrade() === 1 ) ) && ( data.level < 25 ) )
                || ( clanHall.getGrade() === 2 && data.level < 30 )
                || ( clanHall.getGrade() === 3 && data.level < 40 )
    },
}

const supportBuff : OrderData = {
    type: ClanHallFunctionType.SupportBuffs,
    isInsufficientLevel: ( clanHall: AuctionableHall, data: ClanHallFunction ): boolean => {
        return ( clanHall.getGrade() === 0 && data.level < 2 )
                || ( clanHall.getGrade() === 1 && data.level < 4 )
                || ( clanHall.getGrade() === 2 && data.level < 5 )
                || ( clanHall.getGrade() === 3 && data.level < 8 )
    },
}

const clanFunctionOrder: Array<OrderData> = [
    {
        type: ClanHallFunctionType.RestoreHp,
        isInsufficientLevel: ( clanHall: AuctionableHall, data: ClanHallFunction ): boolean => {
            return ( clanHall.getGrade() === 0 && data.level < 220 )
                    || ( clanHall.getGrade() === 1 && data.level < 160 )
                    || ( clanHall.getGrade() === 2 && data.level < 260 )
                    || ( clanHall.getGrade() === 3 && data.level < 300 )
        },
    },
    restoreMp,
    restoreMp,
    {
        type: ClanHallFunctionType.RestoreXp,
        isInsufficientLevel: ( clanHall: AuctionableHall, data: ClanHallFunction ): boolean => {
            return ( clanHall.getGrade() === 0 && data.level < 25 )
                    || ( clanHall.getGrade() === 1 && data.level < 30 )
                    || ( clanHall.getGrade() === 2 && data.level < 40 )
                    || ( clanHall.getGrade() === 3 && data.level < 50 )
        },
    },
    {
        type: ClanHallFunctionType.Teleport,
        isInsufficientLevel: ( clanHall: AuctionableHall, data: ClanHallFunction ): boolean => {
            return data.level < 2
        },
    },
    {
        type: ClanHallFunctionType.DecorationCurtains,
        isInsufficientLevel: ( clanHall: AuctionableHall, data: ClanHallFunction ): boolean => {
            return data.level < 2
        },
    },
    {
        type: ClanHallFunctionType.CreateItem,
        isInsufficientLevel: ( clanHall: AuctionableHall, data: ClanHallFunction ): boolean => {
            return ( clanHall.getGrade() === 0 && data.level < 2 )
                    || data.level < 3
        },
    },
    supportBuff,
    supportBuff,
    {
        type: ClanHallFunctionType.DecorationFront,
        isInsufficientLevel: ( clanHall: AuctionableHall, data: ClanHallFunction ): boolean => {
            return data.level < 2
        },
    },
    {
        type: ClanHallFunctionType.CreateItem,
        isInsufficientLevel: ( clanHall: AuctionableHall, data: ClanHallFunction ): boolean => {
            return ( clanHall.getGrade() === 0 && data.level < 2 )
                    || data.level < 3
        },
    },
]

export function AgitDecoInfo( clanHall: AuctionableHall ): Buffer {
    let packet = new DeclaredServerPacket( 25 )
            .writeC( 0xfd )
            .writeD( clanHall.getId() )

    clanFunctionOrder.forEach( ( data : OrderData ) => {
        let hallFunction: ClanHallFunction = clanHall.getFunction( data.type )
        if ( !hallFunction || hallFunction.level === 0 ) {
            packet.writeC( 0 )
            return
        }

        packet.writeC( data.isInsufficientLevel( clanHall, hallFunction ) ? 1 : 2 )
    } )

    packet
            .writeD( 0 )
            .writeD( 0 )

    return packet.getBuffer()
}