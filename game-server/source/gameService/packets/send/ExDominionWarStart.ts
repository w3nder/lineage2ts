import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function ExDominionWarStart( player: L2PcInstance ): Buffer {
    let territoryId = TerritoryWarManager.getRegisteredTerritoryId( player )
    let isDisguised = TerritoryWarManager.isDisguised( player.getObjectId() )

    return new DeclaredServerPacket( 23 )
            .writeC( 0xFE )
            .writeH( 0xA3 )
            .writeD( player.getObjectId() )
            .writeD( 0x01 )

            .writeD( territoryId )
            .writeD( isDisguised ? 1 : 0 )
            .writeD( isDisguised ? territoryId : 0 )
            .getBuffer()
}