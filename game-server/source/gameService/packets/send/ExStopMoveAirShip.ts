import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2AirShipInstance } from '../../models/actor/instance/L2AirShipInstance'

export function ExStopMoveAirShip( ship : L2AirShipInstance ) : Buffer {
    return new DeclaredServerPacket( 23 )
            .writeC( 0xFE )
            .writeH( 0x66 )
            .writeD( ship.getObjectId() )
            .writeD( ship.getX() )

            .writeD( ship.getY() )
            .writeD( ship.getZ() )
            .writeD( ship.getHeading() )
            .getBuffer()
}