import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExFishingEnd( objectId: number, isSuccess: boolean ): Buffer {
    return new DeclaredServerPacket( 8 )
            .writeC( 0xFE )
            .writeH( 0x1F )
            .writeD( objectId )
            .writeC( isSuccess ? 1 : 0 )
            .getBuffer()
}