import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ServerClose(): Buffer {
    return new DeclaredServerPacket( 1 ).writeC( 0x20 ).getBuffer()
}