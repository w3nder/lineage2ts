import { L2DimensionalTransferItem } from '../../models/items/L2DimensionalTransferItem'
import { DimensionalTransferManager } from '../../cache/DimensionalTransferManager'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

// TODO : in sender field add expiration in remaining days
export function ExGetDimensionalItemList( playerId: number ): Buffer {
    let items : Array<L2DimensionalTransferItem> = DimensionalTransferManager.getItems( playerId )
    let dynamicSize = items.reduce( ( totalAmount: number, item: L2DimensionalTransferItem ): number => {
        return totalAmount + getStringSize( item.sender ) + 24
    }, 0 )

    let packet = new DeclaredServerPacket( 7 + dynamicSize )
            .writeC( 0xFE )
            .writeH( 0x86 )
            .writeD( items.length )

    items.forEach( ( item: L2DimensionalTransferItem, index: number ) => {
        packet
                .writeD( index )
                .writeD( playerId )
                .writeD( item.itemId )
                .writeQ( item.amount )

                .writeD( 0 )
                .writeS( item.sender )
    } )

    return packet.getBuffer()
}