import { L2SkillLearn } from '../../models/L2SkillLearn'
import { SkillCache } from '../../cache/SkillCache'
import { AcquireSkillList } from './builder/AcquireSkillList'
import { AcquireSkillType } from '../../enums/AcquireSkillType'
import { SystemMessageBuilder } from './SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function RequestShowSubUnitSkillList( player: L2PcInstance ): Buffer {
    let clan = player.getClan()
    if ( clan ) {
        let skillList: AcquireSkillList = new AcquireSkillList( AcquireSkillType.SubPledge )

        SkillCache.getAvailableSubPledgeSkills( clan ).forEach( ( skill: L2SkillLearn ) => {
            if ( SkillCache.getSkill( skill.getSkillId(), skill.getSkillLevel() ) ) {
                skillList.addSkill( skill.getSkillId(), skill.getSkillLevel(), skill.getLevelUpSp(), 0 )
            }
        } )

        if ( skillList.getCount() ) {
            return skillList.getBuffer()
        }
    }

    return SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_MORE_SKILLS_TO_LEARN )
}