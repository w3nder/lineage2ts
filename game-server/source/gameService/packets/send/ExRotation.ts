import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExRotation( objectId: number, heading: number ) : Buffer {
    return new DeclaredServerPacket( 11 )
            .writeC( 0xFE )
            .writeH( 0xC1 )
            .writeD( objectId )
            .writeD( heading )
            .getBuffer()
}