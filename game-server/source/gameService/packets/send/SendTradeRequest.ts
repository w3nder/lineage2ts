import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function SendTradeRequest( objectId: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x70 )
            .writeD( objectId )
            .getBuffer()
}