import { DeclaredServerPacket, getStringArraySize, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExSendUIEvent( playerId: number,
        shouldHide: boolean,
        isCountingUp: boolean,
        startTime: number,
        endTime: number,
        npcStringId: number = -1,
        ...textLines: Array<string> ): Buffer {

    let countDirection = isCountingUp ? '1' : '0'
    let startMinutes = Math.floor( startTime / 60 ).toString()
    let startSeconds = ( startTime % 60 ).toString()
    let endMinutes = Math.floor( endTime / 60 ).toString()
    let endSeconds = ( endTime % 60 ).toString()

    let dynamicSize = getStringSize( countDirection ) + getStringSize( startMinutes ) + getStringSize( startSeconds ) + getStringSize( endMinutes ) + getStringSize( endSeconds )
    let packet = new DeclaredServerPacket( getStringArraySize( textLines ) + dynamicSize + 23 )
            .writeC( 0xFE )
            .writeH( 0x8E )
            .writeD( playerId )
            .writeD( shouldHide ? 1 : 0 ) // 0 = show, 1 = hide (there is 2 = pause and 3 = resume also but they don't work well you can only pause count down and you cannot resume it because resume hides the counter).

            .writeD( 0 )// unknown
            .writeD( 0 )// unknown
            .writeS( countDirection ) // 0 = count down, 1 = count up
            .writeS( startMinutes )

            .writeS( startSeconds )
            .writeS( endMinutes )
            .writeS( endSeconds )
            .writeD( npcStringId )

    textLines.forEach( ( line: string ) => {
        packet.writeS( line )
    } )

    return packet.getBuffer()
}