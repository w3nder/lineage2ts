import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

const staticPacket: Buffer = PacketHelper.preservePacket( new DeclaredServerPacket( 5 )
        .writeC( 0x0f )
        .writeD( 0x01 )
        .getBuffer() )

export function CharacterCreationOk(): Buffer {
    return PacketHelper.copyPacket( staticPacket )
}