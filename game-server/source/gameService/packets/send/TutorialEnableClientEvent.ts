import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function TutorialEnableClientEvent( id: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0xA8 )
            .writeD( id )
            .getBuffer()
}