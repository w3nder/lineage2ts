import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExPutEnchantTargetItemResult( objectId: number ) : Buffer {
    return new DeclaredServerPacket( 7 )
            .writeC( 0xFE )
            .writeH( 0x81 )
            .writeD( objectId )
            .getBuffer()
}