import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2Object } from '../../models/L2Object'

export const enum ChangeWaitTypeValue {
    Sitting,
    Standing,
    StartingFakeDeath,
    StoppingFakeDeath,
}

export function ChangeWaitType( object: L2Object, moveType: ChangeWaitTypeValue ) {
    return new DeclaredServerPacket( 21 )
            .writeC( 0x29 )
            .writeD( object.getObjectId() )
            .writeD( moveType )
            .writeD( object.getX() )

            .writeD( object.getY() )
            .writeD( object.getZ() )
            .getBuffer()
}