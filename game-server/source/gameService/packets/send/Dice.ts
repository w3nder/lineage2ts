import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function Dice( objectId: number, itemId: number, value: number, x: number, y: number, z: number ) : Buffer {
    return new DeclaredServerPacket( 25 )
            .writeC( 0xDA )
            .writeD( objectId )
            .writeD( itemId )
            .writeD( value )

            .writeD( x )
            .writeD( y )
            .writeD( z )
            .getBuffer()
}