import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum CharacterDeleteReasons {
    GeneralFailure = 0x01,
    MayNotDeleteClanMember = 0x02,
    ClanLeaderMayNotBeDeleted = 0x03,
}

export function CharacterDeleteFail( reason: CharacterDeleteReasons ): Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x1e )
            .writeD( reason )
            .getBuffer()
}