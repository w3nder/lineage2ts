import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { WarehouseDepositType } from '../../values/warehouseDepositType'
import { ItemContainer } from '../../models/itemcontainer/ItemContainer'

export function WareHouseWithdrawalList( inventory: ItemContainer, type: WarehouseDepositType, adena: number ): Buffer {

    let items = inventory.getItems()
    let allItemsSize: number = items.length * ( ItemPacketHelper.writeItemSize() + 4 )

    let packet = new DeclaredServerPacket( 13 + allItemsSize )
            .writeC( 0x42 )
            .writeH( type )
            .writeQ( adena )
            .writeH( items.length )

    items.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
        packet.writeD( item.getObjectId() )
    } )

    return packet.getBuffer()
}