import { L2World } from '../../L2World'
import { ILocational } from '../../models/Location'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2Object } from '../../models/L2Object'

export function MagicSkillUse( attackerId: number,
        targetId: number,
        skillId: number,
        skillLevel: number,
        hitTime: number,
        reuseDelay: number ): Buffer {
    let character = L2World.getObjectById( attackerId )
    let target = L2World.getObjectById( targetId )

    return MagicSkillUseWithCharacters( character, target, skillId, skillLevel, hitTime, reuseDelay )
}

export function MagicSkillUseWithCharacters( caster: L2Object,
        target: L2Object,
        skillId: number,
        skillLevel: number,
        hitTime: number,
        reuseDelay: number ): Buffer {
    let location: ILocational = caster.isPlayer() ? caster.getActingPlayer().getGroundSkillLocation() : null

    let packet = new DeclaredServerPacket( 53 + ( location ? 12 : 0 ) )
            .writeC( 0x48 )
            .writeD( caster.getObjectId() )
            .writeD( target.getObjectId() )
            .writeD( skillId )

            .writeD( skillLevel )
            .writeD( hitTime )
            .writeD( reuseDelay )
            .writeD( caster.getX() )

            .writeD( caster.getY() )
            .writeD( caster.getZ() )
            .writeH( 0 )
            .writeH( location ? 1 : 0 )

    if ( location ) {
        packet
                .writeD( location.getX() )
                .writeD( location.getY() )
                .writeD( location.getZ() )
    }

    packet
            .writeD( target.getX() )
            .writeD( target.getY() )
            .writeD( target.getZ() )

    return packet.getBuffer()
}