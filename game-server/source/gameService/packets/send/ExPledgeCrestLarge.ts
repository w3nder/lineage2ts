import { L2Crest } from '../../models/L2Crest'
import { CrestCache } from '../../cache/CrestCache'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExPledgeCrestLarge( crestId: number ) : Buffer {
    let crest : L2Crest = CrestCache.getCrest( crestId )
    let dynamicSize : number = crest && crest.getData() ? crest.getData().length : 0

    let packet = new DeclaredServerPacket( 15 + dynamicSize )
            .writeC( 0xFE )
            .writeH( 0x1B )
            .writeD( 0 )
            .writeD( crestId )
            .writeD( dynamicSize )

    if ( dynamicSize > 0 ) {
        packet.writeB( crest.getData() )
    }

    return packet.getBuffer()
}