import { L2Clan } from '../../models/L2Clan'
import { L2ClanMember } from '../../models/L2ClanMember'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

function getClanNameSize( clan: L2Clan ): number {
    return getStringSize( clan.getName() ) + getStringSize( clan.getLeaderName() ) + getStringSize( clan.getAllyName() )
}

export function PledgeShowMemberListAll( clan: L2Clan ): Buffer {
    let pledgeType = 0
    let memberSize = 0
    let clanSize: number = clan.getMembers().reduce( ( total: number, member: L2ClanMember ): number => {
        if ( member.getPledgeType() !== pledgeType ) {
            return total
        }

        memberSize++
        return total + getStringSize( member.getName() ) + 24
    }, 0 )

    let clanNameSize: number = getClanNameSize( clan )

    let packet = new DeclaredServerPacket( 13 + 8 + 32 + 16 + clanSize + clanNameSize )
            .writeC( 0x5a )
            .writeD( 0 )
            .writeD( clan.getId() )
            .writeD( pledgeType )

            .writeS( clan.getName() )
            .writeS( clan.getLeaderName() )
            .writeD( clan.getCrestId() )
            .writeD( clan.getLevel() )

            .writeD( clan.getCastleId() )
            .writeD( clan.getHideoutId() )
            .writeD( clan.getFortId() )
            .writeD( clan.getRank() )

            .writeD( clan.getReputationScore() )
            .writeD( 0x00 )
            .writeD( 0x00 )
            .writeD( clan.getAllyId() )

            .writeS( clan.getAllyName() )
            .writeD( clan.getAllyCrestId() )
            .writeD( clan.isAtWar() ? 1 : 0 )
            .writeD( 0x00 )

            .writeD( memberSize )

    clan.getMembers().forEach( ( member: L2ClanMember ) => {
        if ( member.getPledgeType() !== pledgeType ) {
            return
        }

        packet
                .writeS( member.getName() )
                .writeD( member.getLevel() )
                .writeD( member.getClassId() )

        let player = member.getPlayerInstance()
        if ( player ) {
            packet
                    .writeD( player.getAppearance().getSex() ? 1 : 0 )
                    .writeD( player.getRace() )
        } else {
            packet
                    .writeD( 0x01 )
                    .writeD( 0x01 )
        }

        packet
                .writeD( member.isOnline() ? member.getObjectId() : 0 )
                .writeD( member.getSponsor() !== 0 ? 1 : 0 )
    } )

    return packet.getBuffer()
}