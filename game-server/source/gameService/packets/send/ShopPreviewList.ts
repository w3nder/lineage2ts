import { L2BuyList } from '../../models/buylist/L2BuyList'
import { Product } from '../../models/buylist/Product'
import { ItemType1 } from '../../enums/items/ItemType1'
import { ConfigManager } from '../../../config/ConfigManager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ShopPreviewList( list: L2BuyList, adena: number, expertiseLevel: number ): Buffer {
    let items: Array<Product> = []

    list.getProducts().forEach( ( product: Product ): void => {
        if ( product.getItem().getCrystalType() <= expertiseLevel && product.getItem().isEquipable() ) {
            items.push( product )
        }
    } )

    let packet = new DeclaredServerPacket( 19 + items.length * 16 )
            .writeC( 0xF5 )
            .writeC( 0xC0 )
            .writeC( 0x13 )
            .writeC( 0x00 )

            .writeC( 0x00 )
            .writeQ( adena )
            .writeD( list.getListId() )
            .writeH( items.length )

    items.forEach( ( product: Product ) => {
        packet
                .writeD( product.getItemId() )
                .writeH( product.getItem().getType2() )

        if ( product.getItem().getType1() !== ItemType1.ITEM_QUESTITEM_ADENA ) {
            packet.writeH( product.getItem().getBodyPart() ) // rev 415 slot 0006-lr.ear 0008-neck 0030-lr.finger 0040-head 0080-?? 0100-l.hand 0200-gloves 0400-chest 0800-pants 1000-feet 2000-?? 4000-r.hand 8000-r.hand
        } else {
            packet.writeH( 0x00 ) // rev 415 slot 0006-lr.ear 0008-neck 0030-lr.finger 0040-head 0080-?? 0100-l.hand 0200-gloves 0400-chest 0800-pants 1000-feet 2000-?? 4000-r.hand 8000-r.hand
        }

        packet.writeQ( ConfigManager.general.getWearPrice() )
    } )

    return packet.getBuffer()
}