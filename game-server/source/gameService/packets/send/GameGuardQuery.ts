import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

const staticPacket: Buffer = PacketHelper.preservePacket( new DeclaredServerPacket( 17 )
        .writeC( 0x74 )
        .writeD( 0x27533DD9 )
        .writeD( 0x2E72A51D )
        .writeD( 0x2017038B )

        .writeD( -1017438557 )
        .getBuffer() )

export function GameGuardQuery(): Buffer {
    return PacketHelper.copyPacket( staticPacket )
}