import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function PledgeShowMemberListDelete( name: string ): Buffer {
    return new DeclaredServerPacket( 1 + getStringSize( name ) )
            .writeC( 0x5D )
            .writeS( name )
            .getBuffer()
}