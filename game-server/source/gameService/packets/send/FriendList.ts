import { L2World } from '../../L2World'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PlayerFriendsCache } from '../../cache/PlayerFriendsCache'

export function FriendList( player: L2PcInstance ): Buffer {
    let friendsSize: number = 0
    let playerFriendsIds = PlayerFriendsCache.getFriends( player.getObjectId() )

    playerFriendsIds.forEach( ( playerId: number ): void => {
        friendsSize = friendsSize + getStringSize( CharacterNamesCache.getCachedNameById( playerId ) ) + 12
    } )

    let packet = new DeclaredServerPacket( 5 + friendsSize )
            .writeC( 0x75 )
            .writeD( playerFriendsIds.size )

    playerFriendsIds.forEach( ( playerId: number ) => {
        let name: string = CharacterNamesCache.getCachedNameById( playerId )
        let friendPlayer = L2World.getPlayer( playerId )
        let isOnline: boolean = friendPlayer && friendPlayer.isOnline()

        packet
                .writeD( playerId )
                .writeS( name )
                .writeD( isOnline ? 0x01 : 0x00 )
                .writeD( isOnline ? playerId : 0x00 )
    } )

    return packet.getBuffer()
}