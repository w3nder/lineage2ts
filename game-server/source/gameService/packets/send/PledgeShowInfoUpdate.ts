import { L2Clan } from '../../models/L2Clan'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function PledgeShowInfoUpdate( clan: L2Clan ): Buffer {
    return new DeclaredServerPacket( 53 + getStringSize( clan.getAllyName() ) )
            .writeC( 0x8E )
            .writeD( clan.getId() )
            .writeD( clan.getCrestId() )
            .writeD( clan.getLevel() )

            .writeD( clan.getCastleId() )
            .writeD( clan.getHideoutId() )
            .writeD( clan.getFortId() )
            .writeD( clan.getRank() )

            .writeD( clan.getReputationScore() )
            .writeD( 0 )
            .writeD( 0 )
            .writeD( clan.getAllyId() )

            .writeS( clan.getAllyName() )
            .writeD( clan.getAllyCrestId() )
            .writeD( clan.isAtWar() ? 1 : 0 )
            .getBuffer()
}