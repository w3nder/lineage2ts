import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { TerritoryWard } from '../../instancemanager/territory/TerritoryWard'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Npc } from '../../models/actor/L2Npc'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExShowOwnedThingsPosition(): Buffer {
    let territoryWardList: Array<TerritoryWard> = TerritoryWarManager.isInProgress() ? TerritoryWarManager.getAllTerritoryWards() : []
    let packet = new DeclaredServerPacket( 7 + territoryWardList.length * 16 )
            .writeC( 0xFE )
            .writeH( 0x93 )
            .writeD( territoryWardList.length )

    territoryWardList.forEach( ( ward: TerritoryWard ) => {
        packet.writeD( ward.getTerritoryId() )

        let npc: L2Npc = ward.getNpc()
        if ( npc ) {
            packet
                    .writeD( npc.getX() )
                    .writeD( npc.getY() )
                    .writeD( npc.getZ() )
            return
        }

        let player: L2PcInstance = ward.getPlayer()
        if ( player ) {
            packet
                    .writeD( player.getX() )
                    .writeD( player.getY() )
                    .writeD( player.getZ() )
            return
        }

        packet
                .writeD( 0x00 )
                .writeD( 0x00 )
                .writeD( 0x00 )
    } )

    return packet.getBuffer()
}