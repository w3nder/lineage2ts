import { L2Clan } from '../../models/L2Clan'
import { L2ClanValues } from '../../values/L2ClanValues'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { ClanPledge } from '../../models/ClanPledge'

function getLeaderName( pledge: ClanPledge, clan: L2Clan ) {
    let leaderId = pledge.leaderId
    if ( pledge.type === L2ClanValues.SUBUNIT_ACADEMY || !leaderId || !clan.getClanMember( leaderId ) ) {
        return ''
    }

    return clan.getClanMember( leaderId ).getName()
}

export function SubPledgeInfo( pledge: ClanPledge, clan: L2Clan ): Buffer {
    let leaderName = getLeaderName( pledge, clan )
    return new DeclaredServerPacket( 11 + getStringSize( pledge.name ) + getStringSize( leaderName ) )
            .writeC( 0xfe )
            .writeH( 0x40 )
            .writeD( 0x01 )
            .writeD( pledge.type )

            .writeS( pledge.name )
            .writeS( leaderName )
            .getBuffer()
}