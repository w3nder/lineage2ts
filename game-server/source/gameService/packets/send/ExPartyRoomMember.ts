import { L2World } from '../../L2World'
import { PartyMatchManager, PartyMatchRoom } from '../../cache/partyMatchManager'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { PlayerGroupCache } from '../../cache/PlayerGroupCache'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'

export const enum ExPartyRoomMemberOperation {
    Remove,
    Add,
    View
}

const enum ExPartyRoomMemberType {
    OtherPartyMember,
    Leader,
    LeaderPartyMember
}

function getMemberType( leaderId: number, memberId: number ): ExPartyRoomMemberType {
    if ( leaderId === memberId ) {
        return ExPartyRoomMemberType.Leader
    }

    let leaderParty = PlayerGroupCache.getParty( leaderId )
    let memberParty = PlayerGroupCache.getParty( memberId )

    if ( !leaderParty || !memberParty || leaderParty.getLeaderObjectId() !== memberParty.getLeaderObjectId() ) {
        return ExPartyRoomMemberType.OtherPartyMember
    }

    return ExPartyRoomMemberType.LeaderPartyMember
}

export function ExPartyRoomMember( room: PartyMatchRoom, operation: ExPartyRoomMemberOperation ): Buffer {
    let locationId = PartyMatchManager.getRoomLocation( room )
    let leaderId = room.leaderId

    let membersSize: number = 0
    room.members.forEach( ( memberId: number ) : void => {
        membersSize += 24 + getStringSize( CharacterNamesCache.getCachedNameById( memberId ) )
    }, 0 )

    let packet = new DeclaredServerPacket( 11 + membersSize )
            .writeC( 0xFE )
            .writeH( 0x08 )
            .writeD( operation )
            .writeD( room.members.size )

    room.members.forEach( ( memberId: number ) => {
        let member = L2World.getPlayer( memberId )
        packet
                .writeD( member.getObjectId() )
                .writeS( member.getName() )
                .writeD( member.getActiveClass() )
                .writeD( member.getLevel() )

                .writeD( locationId )
                .writeD( getMemberType( leaderId, memberId ) )
                .writeD( 0x00 )
    } )

    return packet.getBuffer()
}