import { Fort } from '../../models/entity/Fort'
import { FortSiegeSpawn } from '../../models/FortSiegeSpawn'
import { FortSiegeManager } from '../../instancemanager/FortSiegeManager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import _ from 'lodash'

export function ExShowFortressSiegeInfo( fort: Fort ): Buffer {
    let commanders: Array<FortSiegeSpawn> = FortSiegeManager.getCommanderSpawnList( fort.getResidenceId() )
    let packet = new DeclaredServerPacket( 11 + fort.getFortSize() * 4 )
            .writeC( 0xFE )
            .writeH( 0x17 )
            .writeD( fort.getResidenceId() )
            .writeD( fort.getFortSize() )

    _.times( fort.getFortSize(), ( index: number ) => {
        packet.writeD( Math.min( 0, commanders.length - index ) )
    } )

    return packet.getBuffer()
}