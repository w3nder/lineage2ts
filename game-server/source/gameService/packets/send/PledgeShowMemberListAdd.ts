import { L2ClanMember } from '../../models/L2ClanMember'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

function createPacket( name: string, level: number, classId: number, isOnline: number, pledgeType: number ): Buffer {
    return new DeclaredServerPacket( 25 + getStringSize( name ) )
            .writeC( 0x5c )
            .writeS( name )
            .writeD( level )
            .writeD( classId )

            .writeD( 0x00 )
            .writeD( 0x01 )
            .writeD( isOnline )
            .writeD( pledgeType )
            .getBuffer()
}

export const PledgeShowMemberListAdd = {
    fromClanMember( member: L2ClanMember ): Buffer {
        return createPacket(
                member.getName(),
                member.getLevel(),
                member.getClassId(),
                member.isOnline() ? member.getObjectId() : 0,
                member.getPledgeType(),
        )
    },

    fromPlayer( player: L2PcInstance ): Buffer {
        return createPacket(
                player.getName(),
                player.getLevel(),
                player.getClassId(),
                player.isOnline() ? player.getObjectId() : 0,
                player.getPledgeType(),
        )
    },
}