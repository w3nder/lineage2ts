import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { CrystalType } from '../../models/items/type/CrystalType'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function ExShowBaseAttributeCancelWindow( player: L2PcInstance ): Buffer {
    let items: Array<L2ItemInstance> = player.getInventory().getElementItems()

    let size = items.length
    let packet = new DeclaredServerPacket( 3 + size * 12 )
            .writeC( 0xFE )
            .writeH( 0x74 )
            .writeD( size )

    items.forEach( ( item: L2ItemInstance ) => {
        packet
                .writeD( item.getObjectId() )
                .writeQ( getPrice( item ) )
    } )

    return packet.getBuffer()
}

// TODO : introduce config for these values
function getPrice( item: L2ItemInstance ): number {
    switch ( item.getItem().getCrystalType() ) {
        case CrystalType.S:
            if ( item.isWeapon() ) {
                return 50000
            }

            return 40000

        case CrystalType.S80:
            if ( item.isWeapon() ) {
                return 100000
            }

            return 80000

        case CrystalType.S84:
            if ( item.isWeapon() ) {
                return 200000
            }

            return 160000
    }

    return 0
}