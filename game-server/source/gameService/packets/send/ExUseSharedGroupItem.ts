import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExUseSharedGroupItem( itemId: number, groupId: number, remainingTime: number, expirationTime: number ) : Buffer {
    return new DeclaredServerPacket( 19 )
        .writeC( 0xFE )
        .writeH( 0x4A )
        .writeD( itemId )
        .writeD( groupId )

        .writeD( remainingTime )
        .writeD( expirationTime )
        .getBuffer()
}