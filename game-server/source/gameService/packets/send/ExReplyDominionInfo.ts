import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { Territory } from '../../instancemanager/territory/Territory'
import { CastleManager } from '../../instancemanager/CastleManager'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExReplyDominionInfo(): Buffer {
    let territoryList: Array<Territory> = TerritoryWarManager.getAllTerritories()
    let startTime = Math.floor( TerritoryWarManager.getTWStartTime() / 1000 )
    let size = territoryList.reduce( ( total: number, item: Territory ): number => {
        let ownedWardIds = item.getOwnedWardIds()
        return total + 12 + ownedWardIds.length * 4 + getStringSize( item.getOwnerClan().getName() ) + getStringSize( CastleManager.getCastleById( item.getCastleId() ).getName() + '_dominion' )
    }, 0 )

    let packet = new DeclaredServerPacket( 7 + size )
            .writeC( 0xfe )
            .writeH( 0x92 )
            .writeD( territoryList.length )

    territoryList.forEach( ( territory: Territory ) => {
        let ownedWardIds = territory.getOwnedWardIds()
        packet
                .writeD( territory.getTerritoryId() )
                .writeS( CastleManager.getCastleById( territory.getCastleId() ).getName().toLowerCase() + '_dominion' ) // territory name
                .writeS( territory.getOwnerClan().getName() )
                .writeD( ownedWardIds.length ) // Emblem Count

        ownedWardIds.forEach( ( id: number ) => {
            packet.writeD( id )
        } )

        packet.writeD( startTime )
    } )

    return packet.getBuffer()
}