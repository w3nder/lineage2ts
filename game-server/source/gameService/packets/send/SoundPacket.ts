import { PlaySound } from './builder/PlaySound'
import { PacketHelper } from '../PacketVariables'

function createStaticSoundPacket( soundName: string ): Buffer {
    return PacketHelper.preservePacket( new PlaySound( soundName ).getBuffer() )
}

export const SoundPacket: Record<string, Buffer> = {
    ITEMSOUND_QUEST_ACCEPT: createStaticSoundPacket( 'ItemSound.quest_accept' ),
    ITEMSOUND_QUEST_MIDDLE: createStaticSoundPacket( 'ItemSound.quest_middle' ),
    ITEMSOUND_QUEST_FINISH: createStaticSoundPacket( 'ItemSound.quest_finish' ),
    ITEMSOUND_QUEST_ITEMGET: createStaticSoundPacket( 'ItemSound.quest_itemget' ),
    ITEMSOUND_QUEST_TUTORIAL: createStaticSoundPacket( 'ItemSound.quest_tutorial' ),
    // Quests 107, 363, 364
    ITEMSOUND_QUEST_GIVEUP: createStaticSoundPacket( 'ItemSound.quest_giveup' ),
    // Quests 212, 217, 224, 226, 416
    ITEMSOUND_QUEST_BEFORE_BATTLE: createStaticSoundPacket( 'ItemSound.quest_before_battle' ),
    // Quests 211, 258, 266, 330
    ITEMSOUND_QUEST_JACKPOT: createStaticSoundPacket( 'ItemSound.quest_jackpot' ),
    // Quests 508, 509 and 510
    ITEMSOUND_QUEST_FANFARE_1: createStaticSoundPacket( 'ItemSound.quest_fanfare_1' ),
    ITEMSOUND_QUEST_FANFARE_2: createStaticSoundPacket( 'ItemSound.quest_fanfare_2' ),
    // Quest 336
    ITEMSOUND_QUEST_FANFARE_MIDDLE: createStaticSoundPacket( 'ItemSound.quest_fanfare_middle' ),
    // Quest 114
    ITEMSOUND_ARMOR_WOOD: createStaticSoundPacket( 'ItemSound.armor_wood_3' ),
    // Quest 21
    ITEMSOUND_ARMOR_CLOTH: createStaticSoundPacket( 'ItemSound.item_drop_equip_armor_cloth' ),
    AMDSOUND_ED_CHIMES: createStaticSoundPacket( 'AmdSound.ed_chimes_05' ),
    HORROR_01: createStaticSoundPacket( 'horror_01' ),
    // Quest 22
    AMBSOUND_HORROR_01: createStaticSoundPacket( 'AmbSound.dd_horror_01' ),
    AMBSOUND_HORROR_03: createStaticSoundPacket( 'AmbSound.d_horror_03' ),
    AMBSOUND_HORROR_15: createStaticSoundPacket( 'AmbSound.d_horror_15' ),
    // Quest 23
    ITEMSOUND_ARMOR_LEATHER: createStaticSoundPacket( 'ItemSound.itemdrop_armor_leather' ),
    ITEMSOUND_WEAPON_SPEAR: createStaticSoundPacket( 'ItemSound.itemdrop_weapon_spear' ),
    AMBSOUND_MT_CREAK: createStaticSoundPacket( 'AmbSound.mt_creak01' ),
    AMBSOUND_EG_DRON: createStaticSoundPacket( 'AmbSound.eg_dron_02' ),
    SKILLSOUND_HORROR_02: createStaticSoundPacket( 'SkillSound5.horror_02' ),
    CHRSOUND_MHFIGHTER_CRY: createStaticSoundPacket( 'ChrSound.MHFighter_cry' ),
    // Quest 24
    AMDSOUND_WIND_LOOT: createStaticSoundPacket( 'AmdSound.d_wind_loot_02' ),
    INTERFACESOUND_CHARSTAT_OPEN: createStaticSoundPacket( 'InterfaceSound.charstat_open_01' ),
    // Quest 25
    AMDSOUND_HORROR_02: createStaticSoundPacket( 'AmdSound.dd_horror_02' ),
    CHRSOUND_FDELF_CRY: createStaticSoundPacket( 'ChrSound.FDElf_Cry' ),
    // Quest 115
    AMBSOUND_WINGFLAP: createStaticSoundPacket( 'AmbSound.t_wingflap_04' ),
    AMBSOUND_THUNDER: createStaticSoundPacket( 'AmbSound.thunder_02' ),
    // Quest 120
    AMBSOUND_DRONE: createStaticSoundPacket( 'AmbSound.ed_drone_02' ),
    AMBSOUND_CRYSTAL_LOOP: createStaticSoundPacket( 'AmbSound.cd_crystal_loop' ),
    AMBSOUND_PERCUSSION_01: createStaticSoundPacket( 'AmbSound.dt_percussion_01' ),
    AMBSOUND_PERCUSSION_02: createStaticSoundPacket( 'AmbSound.ac_percussion_02' ),
    // Quest 648 and treasure chests
    ITEMSOUND_BROKEN_KEY: createStaticSoundPacket( 'ItemSound2.broken_key' ),
    // Quest 184
    ITEMSOUND_SIREN: createStaticSoundPacket( 'ItemSound3.sys_siren' ),
    // Quest 648
    ITEMSOUND_ENCHANT_SUCCESS: createStaticSoundPacket( 'ItemSound3.sys_enchant_success' ),
    ITEMSOUND_ENCHANT_FAILED: createStaticSoundPacket( 'ItemSound3.sys_enchant_failed' ),
    // Best farm mobs
    ITEMSOUND_SOW_SUCCESS: createStaticSoundPacket( 'ItemSound3.sys_sow_success' ),
    // Quest 25
    SKILLSOUND_HORROR_1: createStaticSoundPacket( 'SkillSound5.horror_01' ),
    // Quests 21 and 23
    SKILLSOUND_HORROR_2: createStaticSoundPacket( 'SkillSound5.horror_02' ),
    // Quest 22
    SKILLSOUND_ANTARAS_FEAR: createStaticSoundPacket( 'SkillSound3.antaras_fear' ),
    // Quest 505
    SKILLSOUND_JEWEL_CELEBRATE: createStaticSoundPacket( 'SkillSound2.jewel.celebrate' ),
    // Quest 373
    SKILLSOUND_LIQUID_MIX: createStaticSoundPacket( 'SkillSound5.liquid_mix_01' ),
    SKILLSOUND_LIQUID_SUCCESS: createStaticSoundPacket( 'SkillSound5.liquid_success_01' ),
    SKILLSOUND_LIQUID_FAIL: createStaticSoundPacket( 'SkillSound5.liquid_fail_01' ),
    // Quest 111
    ETCSOUND_ELROKI_SONG_FULL: createStaticSoundPacket( 'EtcSound.elcroki_song_full' ),
    ETCSOUND_ELROKI_SONG_1ST: createStaticSoundPacket( 'EtcSound.elcroki_song_1st' ),
    ETCSOUND_ELROKI_SONG_2ND: createStaticSoundPacket( 'EtcSound.elcroki_song_2nd' ),
    ETCSOUND_ELROKI_SONG_3RD: createStaticSoundPacket( 'EtcSound.elcroki_song_3rd' ),
    ITEMSOUND2_RACE_START: createStaticSoundPacket( 'ItemSound2.race_start' ),
    // Ships
    ITEMSOUND_SHIP_ARRIVAL_DEPARTURE: createStaticSoundPacket( 'itemsound.ship_arrival_departure' ),
    ITEMSOUND_SHIP_5MIN: createStaticSoundPacket( 'itemsound.ship_5min' ),
    ITEMSOUND_SHIP_1MIN: createStaticSoundPacket( 'itemsound.ship_1min' )
}

export const SoundNames = {
    ITEMSOUND_QUEST_ACCEPT: 'ItemSound.quest_accept',
    ITEMSOUND_QUEST_MIDDLE: 'ItemSound.quest_middle',
    ITEMSOUND_QUEST_FINISH: 'ItemSound.quest_finish',
    ITEMSOUND_QUEST_ITEMGET: 'ItemSound.quest_itemget',
    ITEMSOUND_QUEST_TUTORIAL: 'ItemSound.quest_tutorial',
    ITEMSOUND_QUEST_GIVEUP: 'ItemSound.quest_giveup',
    ITEMSOUND_QUEST_BEFORE_BATTLE: 'ItemSound.quest_before_battle',
    ITEMSOUND_QUEST_JACKPOT: 'ItemSound.quest_jackpot',
    ITEMSOUND_QUEST_FANFARE_1: 'ItemSound.quest_fanfare_1',
    ITEMSOUND_QUEST_FANFARE_2: 'ItemSound.quest_fanfare_2',
    ITEMSOUND_QUEST_FANFARE_MIDDLE: 'ItemSound.quest_fanfare_middle',
    ITEMSOUND_ARMOR_WOOD: 'ItemSound.armor_wood_3',
    ITEMSOUND_ARMOR_CLOTH: 'ItemSound.item_drop_equip_armor_cloth',
    AMDSOUND_ED_CHIMES: 'AmdSound.ed_chimes_05',
    HORROR_01: 'horror_01',
    AMBSOUND_HORROR_01: 'AmbSound.dd_horror_01',
    AMBSOUND_HORROR_03: 'AmbSound.d_horror_03',
    AMBSOUND_HORROR_15: 'AmbSound.d_horror_15',
    ITEMSOUND_ARMOR_LEATHER: 'ItemSound.itemdrop_armor_leather',
    ITEMSOUND_WEAPON_SPEAR: 'ItemSound.itemdrop_weapon_spear',
    AMBSOUND_MT_CREAK: 'AmbSound.mt_creak01',
    AMBSOUND_EG_DRON: 'AmbSound.eg_dron_02',
    SKILLSOUND_HORROR_02: 'SkillSound5.horror_02',
    CHRSOUND_MHFIGHTER_CRY: 'ChrSound.MHFighter_cry',
    AMDSOUND_WIND_LOOT: 'AmdSound.d_wind_loot_02',
    INTERFACESOUND_CHARSTAT_OPEN: 'InterfaceSound.charstat_open_01',
    AMDSOUND_HORROR_02: 'AmdSound.dd_horror_02',
    CHRSOUND_FDELF_CRY: 'ChrSound.FDElf_Cry',
    AMBSOUND_WINGFLAP: 'AmbSound.t_wingflap_04',
    AMBSOUND_THUNDER: 'AmbSound.thunder_02',
    AMBSOUND_DRONE: 'AmbSound.ed_drone_02',
    AMBSOUND_CRYSTAL_LOOP: 'AmbSound.cd_crystal_loop',
    AMBSOUND_PERCUSSION_01: 'AmbSound.dt_percussion_01',
    AMBSOUND_PERCUSSION_02: 'AmbSound.ac_percussion_02',
    ITEMSOUND_BROKEN_KEY: 'ItemSound2.broken_key',
    ITEMSOUND_SIREN: 'ItemSound3.sys_siren',
    ITEMSOUND_ENCHANT_SUCCESS: 'ItemSound3.sys_enchant_success',
    ITEMSOUND_ENCHANT_FAILED: 'ItemSound3.sys_enchant_failed',
    ITEMSOUND_SOW_SUCCESS: 'ItemSound3.sys_sow_success',
    SKILLSOUND_HORROR_1: 'SkillSound5.horror_01',
    SKILLSOUND_HORROR_2: 'SkillSound5.horror_02',
    SKILLSOUND_ANTARAS_FEAR: 'SkillSound3.antaras_fear',
    SKILLSOUND_JEWEL_CELEBRATE: 'SkillSound2.jewel.celebrate',
    SKILLSOUND_LIQUID_MIX: 'SkillSound5.liquid_mix_01',
    SKILLSOUND_LIQUID_SUCCESS: 'SkillSound5.liquid_success_01',
    SKILLSOUND_LIQUID_FAIL: 'SkillSound5.liquid_fail_01',
    ETCSOUND_ELROKI_SONG_FULL: 'EtcSound.elcroki_song_full',
    ETCSOUND_ELROKI_SONG_1ST: 'EtcSound.elcroki_song_1st',
    ETCSOUND_ELROKI_SONG_2ND: 'EtcSound.elcroki_song_2nd',
    ETCSOUND_ELROKI_SONG_3RD: 'EtcSound.elcroki_song_3rd',
    ITEMSOUND2_RACE_START: 'ItemSound2.race_start',
    ITEMSOUND_SHIP_ARRIVAL_DEPARTURE: 'itemsound.ship_arrival_departure',
    ITEMSOUND_SHIP_5MIN: 'itemsound.ship_5min',
    ITEMSOUND_SHIP_1MIN: 'itemsound.ship_1min',
}