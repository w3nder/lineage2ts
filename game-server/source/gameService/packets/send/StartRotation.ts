import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function StartRotation( objectId: number, degree: number, side: number, speed: number ) : Buffer {
    return new DeclaredServerPacket( 17 )
            .writeC( 0x7A )
            .writeD( objectId )
            .writeD( degree )
            .writeD( side )

            .writeD( speed )
            .getBuffer()
}