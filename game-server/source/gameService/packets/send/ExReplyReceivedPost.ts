import { MailMessage } from '../../models/mail/MailMessage'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { ConfigManager } from '../../../config/ConfigManager'

export function ExReplyReceivedPost( message: MailMessage ): Buffer {
    let attachments = message.getLoadedAttachments()
    let items: ReadonlyArray<L2ItemInstance> = attachments ? attachments.getItems() : []
    let allItemsSize: number = items.length * ( ItemPacketHelper.writeItemSize() + 4 )

    let senderName: string = CharacterNamesCache.getCachedNameById( message.getSenderId() ) ?? ConfigManager.general.getMailSystemName()
    let packetSize = 35 +
            getStringSize( senderName ) +
            getStringSize( message.getSubject() ) +
            getStringSize( message.getContent() ) +
            allItemsSize

    let packet = new DeclaredServerPacket( packetSize )
            .writeC( 0xFE )
            .writeH( 0xab )
            .writeD( message.getId() )
            .writeD( message.isCOD() ? 1 : 0 )

            .writeD( 0x00 ) // Unknown
            .writeS( senderName )
            .writeS( message.getSubject() )
            .writeS( message.getContent() )

            .writeD( items.length )

    items.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
        packet.writeD( item.getObjectId() )
    } )

    packet
            .writeQ( message.getCODAdena() )
            .writeD( message.hasAttachments() ? 1 : 0 )
            .writeD( message.getSendBySystem() )

    return packet.getBuffer()
}