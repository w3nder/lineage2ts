import { QuestState } from '../../models/quest/QuestState'
import { QuestStateCache } from '../../cache/QuestStateCache'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function GmViewQuestInfo( player: L2PcInstance ): Buffer {
    let states: Array<QuestState> = QuestStateCache.getAllActiveStates( player.getObjectId() )
    let packet = new DeclaredServerPacket( getStringSize( player.getName() ) + 3 + states.length * 8 )
            .writeC( 0x99 )
            .writeS( player.getName() )
            .writeH( states.length )

    states.forEach( ( state: QuestState ) => {
        packet
                .writeD( state.questId )
                .writeD( state.condition )
    } )

    return packet.getBuffer()
}