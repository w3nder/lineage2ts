import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExAskCoupleAction( objectId: number, actionId: number ) : Buffer {
    return new DeclaredServerPacket( 11 )
            .writeC( 0xFE )
            .writeH( 0xBB )
            .writeD( actionId )
            .writeD( objectId )
            .getBuffer()
}