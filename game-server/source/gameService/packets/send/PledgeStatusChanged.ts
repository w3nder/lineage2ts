import { L2Clan } from '../../models/L2Clan'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function PledgeStatusChanged( clan: L2Clan ) : Buffer {
    return new DeclaredServerPacket( 29 )
        .writeC( 0xCD )
        .writeD( clan.getLeaderId() )
        .writeD( clan.getId() )
        .writeD( clan.getCrestId() )

        .writeD( clan.getAllyId() )
        .writeD( clan.getAllyCrestId() )
        .writeD( 0x00 )
        .writeD( 0x00 )
        .getBuffer()
}