import { InventoryUpdateBuilder } from './InventoryUpdate'

export class PetInventoryUpdateBuilder extends InventoryUpdateBuilder {
    signature: number = 0xB4
}