import { BuffInfo } from '../../../models/skills/BuffInfo'
import { DeclaredServerPacket } from '../../../../packets/DeclaredServerPacket'
import { IBuilderPacket } from '../../IBuilderPacket'
import { BuffDefinition } from '../../../models/skills/BuffDefinition'

export class AbnormalStatusUpdate implements IBuilderPacket {
    effects: Array<BuffDefinition> = []

    addSkill( info: BuffInfo ) {
        if ( !info.getSkill().isHealingPotionSkill() ) {
            this.effects.push( info.getDefinition() )
        }
    }

    getBuffer(): Buffer {

        let packet = new DeclaredServerPacket( 3 + this.effects.length * 10 )
                .writeC( 0x85 )
                .writeH( this.effects.length )

        this.effects.forEach( ( buff: BuffDefinition ) => {
            packet
                    .writeD( buff.skill.getDisplayId() )
                    .writeH( buff.skill.getDisplayLevel() )
                    .writeD( buff.timeLeftSeconds )
        } )

        return packet.getBuffer()
    }
}