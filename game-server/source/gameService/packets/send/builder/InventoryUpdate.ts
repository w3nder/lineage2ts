import { L2ItemInstance } from '../../../models/items/instance/L2ItemInstance'
import { ItemPacketHelper } from '../helpers/ItemPacketHelper'
import { DeclaredServerPacket, ManagedServerPacket } from '../../../../packets/DeclaredServerPacket'
import { IBuilderPacket } from '../../IBuilderPacket'
import { ObjectPool } from '../../../helpers/ObjectPoolHelper'
import { InventoryUpdateStatus } from '../../../enums/InventoryUpdateStatus'

const defaultItemBlockSize = ItemPacketHelper.writeItemSize() + 2
const InventoryUpdatePool = new ObjectPool( 'InventoryUpdateParameters', () : Buffer => {
    return Buffer.allocUnsafeSlow( defaultItemBlockSize )
} )

/*
    TODO : is it worthwhile to add deduplication logic for items that already have been added
    given short interval of update?
 */
export class InventoryUpdateBuilder implements IBuilderPacket {
    items: Array<Buffer> = []
    signature: number = 0x21
    packet: ManagedServerPacket = new ManagedServerPacket()

    addItem( item: L2ItemInstance, status : InventoryUpdateStatus ): void {
        /*
            Item added can be already removed, hence all the values we need to write must be immediately
            saved in order for us not to retain removed/deleted item as part of main builder packet.
         */
        this.packet.setBuffer( InventoryUpdatePool.getValue() )

        /*
            Status is set separately since item referenced value is used for determining
            item update status.
         */
        this.packet.writeH( status )
        ItemPacketHelper.writeItem( this.packet, item )

        this.items.push( this.packet.getBuffer() )
        this.packet.setBuffer( null )
    }

    getBuffer() : Buffer {
        let allItemsSize : number = this.items.length * defaultItemBlockSize

        let packet = new DeclaredServerPacket( 3 + allItemsSize )
            .writeC( this.signature )
            .writeH( this.items.length )

        this.items.forEach( ( data : Buffer ) => {
            packet.writeB( data )
            InventoryUpdatePool.recycleValue( data )
        } )

        this.items.length = 0

        return packet.getBuffer()
    }

    reset() : void {
        this.items.forEach( ( data: Buffer ) => {
            InventoryUpdatePool.recycleValue( data )
        } )

        this.items.length = 0
    }
}

