import { L2World } from '../../../L2World'
import { IBuilderPacket } from '../../IBuilderPacket'
import { DeclaredServerPacket, getStringArraySize, getStringSize } from '../../../../packets/DeclaredServerPacket'

export class CreatureSay implements IBuilderPacket {
    objectId: number
    textType: number
    characterName: string
    characterId: number = 0
    text: string
    npcString: number = -1
    parameters: Array<string> = []

    static fromText( playerId: number, objectId: number, messageType: number, characterName: string, text: string ) : Buffer {
        let packet = new CreatureSay()

        packet.objectId = objectId
        packet.textType = messageType
        packet.characterName = characterName
        packet.text = text

        return packet.getBuffer( playerId )
    }

    static fromParameters( objectId: number, messageType: number, characterName: string, text: string ) : CreatureSay {
        let packet = new CreatureSay()

        packet.objectId = objectId
        packet.textType = messageType
        packet.characterName = characterName
        packet.text = text

        return packet
    }

    static fromSystemMessage( objectId: number, messageType: number, characterId: number, messageId: number ) : CreatureSay {
        let packet = new CreatureSay()

        packet.objectId = objectId
        packet.textType = messageType
        packet.characterId = characterId
        packet.npcString = messageId

        return packet
    }

    static fromNpcName( objectId: number, messageType: number, name: string, messageId: number ) : CreatureSay {
        let packet = new CreatureSay()

        packet.objectId = objectId
        packet.textType = messageType
        packet.characterName = name
        packet.npcString = messageId

        return packet
    }

    addParameter( text: string ) {
        this.parameters.push( text )

        return this
    }

    getBuffer( playerId: number = 0 ) : Buffer {
        let player = L2World.getPlayer( playerId )
        if ( player && this.characterName && this.text ) {
            player.broadcastSnoop( this.textType, this.characterName, this.text )
        }

        let packet = new DeclaredServerPacket( 13 + ( this.characterName ? getStringSize( this.characterName ) : 4 ) + ( this.text ? getStringSize( this.text ) : getStringArraySize( this.parameters ) ) )
            .writeC( 0x4a )
            .writeD( this.objectId )
            .writeD( this.textType )

        if ( this.characterName ) {
            packet.writeS( this.characterName )
        } else {
            packet.writeD( this.characterId )
        }

        packet.writeD( this.npcString )

        if ( this.text ) {
            packet.writeS( this.text )
        } else if ( this.parameters.length > 0 ) {
            this.parameters.forEach( ( value: string ) => {
                packet.writeS( value )
            } )
        }

        return packet.getBuffer()
    }
}