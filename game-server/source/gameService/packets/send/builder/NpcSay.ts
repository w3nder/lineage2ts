import { DeclaredServerPacket, getStringSize } from '../../../../packets/DeclaredServerPacket'
import _ from 'lodash'
import { IBuilderPacket } from '../../IBuilderPacket'
import { NpcSayType } from '../../../enums/packets/NpcSayType'

export class NpcSay implements IBuilderPacket {
    objectId: number
    textType: NpcSayType
    npcId: number
    text: string = ''
    npcStringId: number = -1
    parameters: Array<string>

    static fromNpcString( objectId: number, messageType: NpcSayType, npcId: number, stringId: number, ...parameters: Array<string> ): NpcSay {
        let builder = new NpcSay()

        builder.objectId = objectId
        builder.textType = messageType
        builder.npcId = 1000000 + npcId
        builder.npcStringId = stringId
        builder.parameters = parameters

        return builder
    }

    static fromNpcText( objectId: number, messageType: NpcSayType, npcId: number, text: string, ...parameters: Array<string> ): NpcSay {
        let builder = new NpcSay()

        builder.objectId = objectId
        builder.textType = messageType
        builder.npcId = 1000000 + npcId
        builder.text = text
        builder.parameters = parameters

        return builder
    }

    getBuffer(): Buffer {
        let stringSize: number = this.npcStringId === -1 ? getStringSize( this.text ) : _.reduce( this.parameters, ( size: number, line: string ): number => {
            return size + getStringSize( line )
        }, 0 )

        let packet = new DeclaredServerPacket( 17 + stringSize )
                .writeC( 0x30 )
                .writeD( this.objectId )
                .writeD( this.textType )
                .writeD( this.npcId )

                .writeD( this.npcStringId )

        if ( this.npcStringId === -1 ) {
            packet.writeS( this.text )
        } else {
            _.each( this.parameters, ( line: string ) => {
                packet.writeS( line )
            } )
        }

        return packet.getBuffer()
    }
}