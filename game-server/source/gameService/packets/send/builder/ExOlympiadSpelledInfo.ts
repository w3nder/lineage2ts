import { BuffInfo } from '../../../models/skills/BuffInfo'
import { L2PcInstance } from '../../../models/actor/instance/L2PcInstance'
import { DeclaredServerPacket } from '../../../../packets/DeclaredServerPacket'
import { IBuilderPacket } from '../../IBuilderPacket'
import { BuffDefinition } from '../../../models/skills/BuffDefinition'

export class ExOlympiadSpelledInfo implements IBuilderPacket {
    effects: Array<BuffDefinition> = []
    playerId: number

    constructor( player: L2PcInstance ) {
        this.playerId = player.objectId
    }

    addSkill( info: BuffInfo ) {
        if ( info && info.isInUse ) {
            this.effects.push( info.getDefinition() )
        }
    }

    getBuffer(): Buffer {
        let packet = new DeclaredServerPacket( 9 + this.effects.length * 12 )
                .writeC( 0xFE )
                .writeH( 0x7B )
                .writeD( this.playerId )
                .writeH( this.effects.length )

        this.effects.forEach( ( buff: BuffDefinition ) => {
            packet
                    .writeD( buff.skill.getDisplayId() )
                    .writeD( buff.skill.getDisplayLevel() )
                    .writeD( buff.timeLeftSeconds )
        } )

        return packet.getBuffer()
    }
}