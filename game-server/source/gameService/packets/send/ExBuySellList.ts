import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

const emptyRefundItems = []

export function ExBuySellList( player: L2PcInstance, showInventory: boolean ): Buffer {

    let sellableItems: Array<L2ItemInstance> = player.getInventory().getAvailableItems( false, false, false, false )
    let refundItems: ReadonlyArray<L2ItemInstance> = player.hasRefund() ? player.getRefund().getItems() : emptyRefundItems

    let sellableSize: number = sellableItems.length * ( ItemPacketHelper.writeItemSize() + 8 )
    let refundSize: number = refundItems.length * ( ItemPacketHelper.writeItemSize() + 12 )

    let packet = new DeclaredServerPacket( 12 + sellableSize + refundSize )
            .writeC( 0xFE )
            .writeH( 0xB7 )
            .writeD( 0x01 )
            .writeH( sellableItems.length )

    sellableItems.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
        packet.writeQ( Math.floor( item.getItem().getReferencePrice() / 2 ) )
    } )

    packet.writeH( refundItems.length )

    refundItems.forEach( ( item: L2ItemInstance, index: number ) => {
        ItemPacketHelper.writeItem( packet, item )
        packet
                .writeD( index )
                .writeQ( Math.floor( ( item.getItem().getReferencePrice() / 2 ) * item.getCount() ) )
    } )

    packet.writeC( showInventory ? 1 : 0 )

    return packet.getBuffer()
}