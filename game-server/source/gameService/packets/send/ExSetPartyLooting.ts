import { PartyDistributionType } from '../../enums/PartyDistributionType'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum ExSetPartyLootingResult {
    Cancelled,
    Success
}

export function ExSetPartyLooting( result: ExSetPartyLootingResult, type: PartyDistributionType ) : Buffer {
    return new DeclaredServerPacket( 11 )
            .writeC( 0xFE )
            .writeH( 0xC0 )
            .writeD( result )
            .writeD( type )
            .getBuffer()
}