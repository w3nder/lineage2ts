import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExDuelAskStart( name: string, isPartyDuel: number ) : Buffer {
    return new DeclaredServerPacket( 7 + getStringSize( name ) )
            .writeC( 0xFE )
            .writeH( 0x4C )
            .writeS( name )
            .writeD( isPartyDuel )
            .getBuffer()
}