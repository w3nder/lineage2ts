import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExQuestItemList( player: L2PcInstance, items: Array<L2ItemInstance> ): Buffer {
    let allItemsSize: number = items.length * ItemPacketHelper.writeItemSize()

    let packet = new DeclaredServerPacket( 5 + allItemsSize + ItemPacketHelper.writeInventoryBlockSize( player.getInventory() ) )
            .writeC( 0xFE )
            .writeH( 0xC6 )
            .writeH( items.length )

    items.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
    } )

    ItemPacketHelper.writeInventoryBlock( packet, player.getInventory() )

    return packet.getBuffer()
}