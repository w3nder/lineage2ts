import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExAttributeEnchantResult( value: number ) : Buffer {
    return new DeclaredServerPacket( 7 )
            .writeC( 0xFE )
            .writeH( 0x61 )
            .writeD( value )
            .getBuffer()
}