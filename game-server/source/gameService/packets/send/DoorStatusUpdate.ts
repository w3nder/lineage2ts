import { L2World } from '../../L2World'
import { L2DoorInstance } from '../../models/actor/instance/L2DoorInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function DoorStatusUpdate( objectId: number ) : Buffer {
    let door = L2World.getObjectById( objectId ) as L2DoorInstance

    return new DeclaredServerPacket( 29 )
            .writeC( 0x4D )
            .writeD( objectId )
            .writeD( door.isOpen() ? 0 : 1 )
            .writeD( door.getDamage() )

            .writeD( door.isEnemy() ? 1 : 0 )
            .writeD( door.getId() )
            .writeD( door.getCurrentHp() )
            .writeD( door.getMaxHp() )

            .getBuffer()
}