import { AcquireSkillType } from '../../enums/AcquireSkillType'
import { L2SkillLearn } from '../../models/L2SkillLearn'
import { ConfigManager } from '../../../config/ConfigManager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { ItemDefinition } from '../../interface/ItemDefinition'
import { CommonSkillIds } from '../../enums/skills/CommonSkillIds'

export function AcquireSkillInfo( type: AcquireSkillType, skill: L2SkillLearn ) : Buffer {

    let hasRequiredItems : boolean = ( ( type !== AcquireSkillType.Pledge ) || ConfigManager.character.lifeCrystalNeeded() )
            && ( ConfigManager.character.divineInspirationSpBookNeeded() || ( skill.getSkillId() !== CommonSkillIds.DivineInspiration ) )

    let itemCount : number = !hasRequiredItems ? 0 : skill.getRequiredItems().length

    let packet = new DeclaredServerPacket( 21 + itemCount * 20 )
            .writeC( 0x91 )
            .writeD( skill.getSkillId() )
            .writeD( skill.getSkillLevel() )
            .writeD( skill.getLevelUpSp() )

            .writeD( type )
            .writeD( itemCount )

    if ( itemCount > 0 ) {
        skill.getRequiredItems().forEach( ( item: ItemDefinition ) => {
            packet
                    .writeD( 99 )
                    .writeD( item.id )
                    .writeQ( item.count )
                    .writeD( 50 )
        } )
    }

    return packet.getBuffer()
}

export function AcquireSkillInfoWithSp( type: AcquireSkillType, skill: L2SkillLearn, spAmount: number ) : Buffer {
    let itemCount : number = skill.getRequiredItems().length

    let packet = new DeclaredServerPacket( 21 + itemCount * 20 )
            .writeC( 0x91 )
            .writeD( skill.getSkillId() )
            .writeD( skill.getSkillLevel() )
            .writeD( spAmount )

            .writeD( type )
            .writeD( itemCount )

    skill.getRequiredItems().forEach( ( item: ItemDefinition ) => {
        packet
                .writeD( 99 )
                .writeD( item.id )
                .writeQ( item.count )
                .writeD( 50 )
    } )

    return packet.getBuffer()
}