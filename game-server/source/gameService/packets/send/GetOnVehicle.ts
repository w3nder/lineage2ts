import { Location } from '../../models/Location'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function GetOnVehicle( objectId: number, boatObjectId: number, position: Location ) : Buffer {
    return new DeclaredServerPacket( 21 )
        .writeC( 0x6e )
        .writeD( objectId )
        .writeD( boatObjectId )
        .writeD( position.getX() )

        .writeD( position.getY() )
        .writeD( position.getZ() )
        .getBuffer()
}