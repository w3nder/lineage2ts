import { L2World } from '../../L2World'
import { L2RecipeDefinition } from '../../models/l2RecipeDefinition'
import { DataManager } from '../../../data/manager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function RecipeItemMakeInfo( recipeId: number, objectId: number, isSuccess: boolean ) : Buffer {
    let player = L2World.getPlayer( objectId )
    return RecipeItemMakeInfoWithPlayer( recipeId, player, isSuccess )
}

export function RecipeItemMakeInfoWithPlayer( recipeId: number, player: L2PcInstance, isSuccess: boolean ) : Buffer {
    let recipe : L2RecipeDefinition = DataManager.getRecipeData().getRecipeList( recipeId )

    return new DeclaredServerPacket( 21 )
            .writeC( 0xDD )
            .writeD( recipeId )
            .writeD( recipe.isDwarvenRecipe ? 0 : 1 )
            .writeD( player.getCurrentMp() )

            .writeD( player.getMaxMp() )
            .writeD( isSuccess ? 1 : 0 )
            .getBuffer()
}