import { PacketHelper } from '../PacketVariables'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

const staticPacket = PacketHelper.preservePacket( new DeclaredServerPacket( 1 ).writeC( 0x1d ).getBuffer() )

export function CharacterDeleteSuccess(): Buffer {
    return PacketHelper.copyPacket( staticPacket )
}