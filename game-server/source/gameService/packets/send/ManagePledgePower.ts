import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ManagePledgePower( privileges: number ) : Buffer {
    return new DeclaredServerPacket( 13 )
            .writeC( 0x2A )
            .writeD( 0 )
            .writeD( 0 )
            .writeD( privileges )
            .getBuffer()
}