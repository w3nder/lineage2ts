import { Elementals } from '../../models/Elementals'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { ElementalType } from '../../enums/ElementalType'

export function ExChooseInventoryAttributeItem( itemId : number ) : Buffer {
    let attribute : number = Elementals.getItemElement( itemId )
    let level : number = Elementals.getMaxElementLevel( itemId )

    return new DeclaredServerPacket( 35 )
            .writeC( 0xfe )
            .writeH( 0x62 )
            .writeD( itemId )
            .writeD( attribute === ElementalType.FIRE ? 1 : 0 ) // Fire

            .writeD( attribute === ElementalType.WATER ? 1 : 0 ) // Water
            .writeD( attribute === ElementalType.WIND ? 1 : 0 ) // Wind
            .writeD( attribute === ElementalType.EARTH ? 1 : 0 ) // Earth
            .writeD( attribute === ElementalType.HOLY ? 1 : 0 ) // Holy

            .writeD( attribute === ElementalType.DARK ? 1 : 0 ) // Unholy
            .writeD( level ) // Item max attribute level
            .getBuffer()
}