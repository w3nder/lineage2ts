import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ShowCalculator( id: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0xE2 )
            .writeD( id )
            .getBuffer()
}