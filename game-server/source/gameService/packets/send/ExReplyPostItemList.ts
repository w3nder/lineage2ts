import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function ExReplyPostItemList( player: L2PcInstance ): Buffer {
    let itemList: Array<L2ItemInstance> = player.getInventory().getAvailableItems( true, false, false, true )

    let dynamicSize: number = itemList.length * ItemPacketHelper.writeItemSize()

    let packet = new DeclaredServerPacket( 7 + dynamicSize )
            .writeC( 0xFE )
            .writeH( 0xB2 )
            .writeD( itemList.length )

    itemList.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
    } )

    return packet.getBuffer()
}