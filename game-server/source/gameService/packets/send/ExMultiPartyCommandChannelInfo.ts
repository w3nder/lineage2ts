import { L2CommandChannel } from '../../models/L2CommandChannel'
import { L2Party } from '../../models/L2Party'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExMultiPartyCommandChannelInfo( channel: L2CommandChannel ): Buffer {
    let partySize = channel.getParties().reduce( ( size: number, party: L2Party ): number => {
        return size + getStringSize( party.getLeader().getName() ) + 8
    }, 0 )

    let packet = new DeclaredServerPacket( 15 + getStringSize( channel.getLeader().getName() ) + partySize )
            .writeC( 0xFE )
            .writeH( 0x31 )
            .writeS( channel.getLeader().getName() )
            .writeD( 0 )

            .writeD( channel.getMemberCount() )
            .writeD( channel.getParties().length )

    channel.getParties().forEach( ( party: L2Party ) => {
        packet
                .writeS( party.getLeader().getName() )
                .writeD( party.getLeaderObjectId() )
                .writeD( party.getMemberCount() )
    } )

    return packet.getBuffer()
}