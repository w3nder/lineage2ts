import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PlayerRecipeCache } from '../../cache/PlayerRecipeCache'

export function RecipeBookItemList( player: L2PcInstance, isDwarvenCraft: boolean ): Buffer {
    let books = PlayerRecipeCache.getBooks( player.getObjectId() )
    let recipes: Set<number> = isDwarvenCraft ? books.dwarven : books.common

    let packet = new DeclaredServerPacket( 13 + recipes.size * 8 )
            .writeC( 0xDC )
            .writeD( isDwarvenCraft ? 0 : 1 )
            .writeD( player.getMaxMp() )
            .writeD( recipes.size )

    let index = 0
    recipes.forEach( ( recipeId: number ) : void => {
        packet
                .writeD( recipeId )
                .writeD( index + 1 )
        index++
    } )

    return packet.getBuffer()
}