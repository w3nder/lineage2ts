import { L2Summon } from '../../models/actor/L2Summon'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExPartyPetWindowDeleteWithSummon( summon : L2Summon ) : Buffer {
    return new DeclaredServerPacket( 11 + getStringSize( summon.getName() ) )
            .writeC( 0xFE )
            .writeH( 0x6A )
            .writeD( summon.getObjectId() )
            .writeD( summon.getOwnerId() )

            .writeS( summon.getName() )
            .getBuffer()
}