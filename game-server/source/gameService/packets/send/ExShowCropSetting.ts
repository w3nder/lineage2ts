import { L2Seed } from '../../models/L2Seed'
import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { CropProcure } from '../../models/manor/CropProcure'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExShowCropSetting( manorId: number ): Buffer {
    let allSeeds: Array<L2Seed> = CastleManorManager.getSeedsForCastle( manorId )

    let packet = new DeclaredServerPacket( 11 + allSeeds.length * 68 )
            .writeC( 0xFE )
            .writeH( 0x2b )
            .writeD( manorId )
            .writeD( allSeeds.length )

    allSeeds.forEach( ( seed: L2Seed ) => {
        packet
                .writeD( seed.getCropId() ) // crop id
                .writeD( seed.getLevel() ) // seed level
                .writeC( 1 )
                .writeD( seed.getReward( 1 ) ) // reward 1 id

                .writeC( 1 )
                .writeD( seed.getReward( 2 ) ) // reward 2 id
                .writeD( seed.getCropLimit() ) // next sale limit
                .writeD( 0 ) // ???

                .writeD( seed.getCropMinPrice() ) // min crop price
                .writeD( seed.getCropMaxPrice() ) // max crop price

        let currentCrop: CropProcure = CastleManorManager.getCropProcure( manorId, seed.getCropId(), false )
        packet
                .writeQ( currentCrop ? currentCrop.getStartAmount() : 0 )
                .writeQ( currentCrop ? currentCrop.getPrice() : 0 )
                .writeC( currentCrop ? currentCrop.getReward() : 0 )

        let nextCrop: CropProcure = CastleManorManager.getCropProcure( manorId, seed.getCropId(), true )
        packet
                .writeQ( nextCrop ? nextCrop.getStartAmount() : 0 )
                .writeQ( nextCrop ? nextCrop.getPrice() : 0 )
                .writeC( nextCrop ? nextCrop.getReward() : 0 )
    } )

    return packet.getBuffer()
}