import { L2EnchantSkillLearn } from '../../models/L2EnchantSkillLearn'
import { DataManager } from '../../../data/manager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExEnchantSkillInfo( skillId: number, skillLevel: number ): Buffer {
    let skillLearn: L2EnchantSkillLearn = DataManager.getSkillData().getSkillEnchantmentBySkillId( skillId )
    let routes: Array<number> = []
    let isMaxEnchanted: boolean = false

    if ( skillLevel > 100 ) {
        isMaxEnchanted = skillLearn.isMaxEnchant( skillLevel )
        let holder = skillLearn.getEnchantSkillHolder( skillLevel )

        if ( holder ) {
            routes.push( skillLevel )
        }

        let level = skillLevel % 100

        skillLearn.getAllRoutes().forEach( ( route: number ) => {
            let routeLevel = ( route * 100 ) + level
            if ( routeLevel === skillLevel ) {
                return
            }

            routes.push( routeLevel )
        } )
    } else {
        skillLearn.getAllRoutes().forEach( ( route: number ) => {
            routes.push( ( route * 100 ) + 1 )
        } )
    }

    let packet = new DeclaredServerPacket( 23 + routes.length * 4 )
            .writeC( 0xFE )
            .writeH( 0x2A )
            .writeD( skillId )
            .writeD( skillLevel )

            .writeD( isMaxEnchanted ? 0 : 1 )
            .writeD( skillLevel > 100 ? 1 : 0 )
            .writeD( routes.length )

    routes.forEach( ( level: number ) => {
        packet.writeD( level )
    } )

    return packet.getBuffer()
}