import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function PickItemUp( item: L2ItemInstance, characterObjectId: number ) : Buffer {
    return new DeclaredServerPacket( 21 )
            .writeC( 0x17 )
            .writeD( characterObjectId )
            .writeD( item.getObjectId() )
            .writeD( item.getX() )

            .writeD( item.getY() )
            .writeD( item.getZ() )
            .getBuffer()
}