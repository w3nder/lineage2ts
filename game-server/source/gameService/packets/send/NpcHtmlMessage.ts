import { HtmlActionScope } from '../../enums/HtmlActionScope'
import { HtmlActionCache } from '../../cache/HtmlActionCache'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

// TODO : remove this wrapper and ensure that only html string is sent
function ensureHtml( text: string ) {
    if ( !text.startsWith( '<html>' ) ) {
        return `<html><body>${ text }</body></html>`
    }

    return text
}

export function NpcHtmlMessage( html: string, playerId: number, npcObjectId: number = 0, itemId: number = 0 ): Buffer {
    let currentHtml = html

    if ( playerId !== 0 ) {
        let scope = itemId === 0 ? HtmlActionScope.Npc : HtmlActionScope.Item
        currentHtml = HtmlActionCache.buildCache( playerId, scope, npcObjectId, html )
    }

    let correctedHtml: string = ensureHtml( currentHtml )

    return new DeclaredServerPacket( 9 + getStringSize( correctedHtml ) )
            .writeC( 0x19 )
            .writeD( npcObjectId )
            .writeS( correctedHtml )
            .writeD( itemId )
            .getBuffer()

}

/*
    Alternative version of NpcHtmlMessage that is using cached values for actions.
    However, if actions do NOT need to be generated (as it consumes CPU/resources), pass htmlPath with value of 'null'.
    Usage is specifically for html that either has path or when there are no links to be inspected. It is recommended to
    always provide path.
 */
export function NpcHtmlMessagePath( html: string, htmlPath: string, playerId: number = 0, npcObjectId: number = 0, itemId: number = 0 ): Buffer {
    let currentHtml = html

    if ( playerId !== 0 ) {
        let scope = itemId === 0 ? HtmlActionScope.Npc : HtmlActionScope.Item
        if ( htmlPath ) {
            currentHtml = HtmlActionCache.buildCacheWithPath( playerId, scope, npcObjectId, html, htmlPath )
        }
    }

    let correctedHtml: string = ensureHtml( currentHtml )

    return new DeclaredServerPacket( 9 + getStringSize( correctedHtml ) )
            .writeC( 0x19 )
            .writeD( npcObjectId )
            .writeS( correctedHtml )
            .writeD( itemId )
            .getBuffer()
}