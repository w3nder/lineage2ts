import { TradeItem } from '../../models/TradeItem'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { ItemInfo } from '../../models/ItemInfo'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function TradeOwnAdd( item: TradeItem ): Buffer {
    let itemInfo = new ItemInfo().fromTradeItem( item )
    let packet = new DeclaredServerPacket( 35 + ItemPacketHelper.writeItemElementalAndEnchantSize() )
            .writeC( 0x1A )
            .writeH( 1 ) // item count
            .writeH( 0 )
            .writeD( item.objectId )

            .writeD( item.itemTemplate.itemId )
            .writeQ( item.count )
            .writeH( item.itemTemplate.getType2() )
            .writeH( item.type1 )

            .writeD( item.itemTemplate.getBodyPart() )
            .writeH( item.enchant )
            .writeH( 0 )
            .writeH( item.type2 )

    ItemPacketHelper.writeItemElementalAndEnchant( packet, itemInfo )

    return packet.getBuffer()
}