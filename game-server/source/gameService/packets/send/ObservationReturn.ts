import { ILocational } from '../../models/Location'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ObservationReturn( location: ILocational ) : Buffer {
    return new DeclaredServerPacket( 13 )
            .writeC( 0xEC )
            .writeD( location.getX() )
            .writeD( location.getY() )
            .writeD( location.getZ() )
            .getBuffer()
}