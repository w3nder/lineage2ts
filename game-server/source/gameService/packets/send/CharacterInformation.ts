import { L2World } from '../../L2World'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Decoy } from '../../models/actor/L2Decoy'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { L2NpcTemplate } from '../../models/actor/templates/L2NpcTemplate'
import { DataManager } from '../../../data/manager'
import { AbnormalVisualEffect, AbnormalVisualEffectMap } from '../../models/skills/AbnormalVisualEffect'
import { L2CubicInstance } from '../../models/actor/instance/L2CubicInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { CursedWeaponManager } from '../../instancemanager/CursedWeaponManager'
import { PartyMatchManager } from '../../cache/partyMatchManager'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { InventorySlot } from '../../values/InventoryValues'
import { PacketHelper } from '../PacketVariables'

/*
    Paperdoll order is specific to current packet since it has
    different order of slots than other packets.
 */
const PaperdollOrder : Array<InventorySlot> = [
    InventorySlot.Under,
    InventorySlot.Head,
    InventorySlot.RightHand,
    InventorySlot.LeftHand,
    InventorySlot.Gloves,
    InventorySlot.Chest,
    InventorySlot.Legs,
    InventorySlot.Feet,
    InventorySlot.Cloak,
    InventorySlot.RightHand,
    InventorySlot.Hair,
    InventorySlot.HairSecondary,
    InventorySlot.RightBracelet,
    InventorySlot.LeftBracelet,
    InventorySlot.Decoration1,
    InventorySlot.Decoration2,
    InventorySlot.Decoration3,
    InventorySlot.Decoration4,
    InventorySlot.Decoration5,
    InventorySlot.Decoration6,
    InventorySlot.Belt
]

const stealthEffect = AbnormalVisualEffectMap[ AbnormalVisualEffect.STEALTH ].mask

export class CharacterInformation {
    objectId: number
    x: number
    y: number
    z: number
    vehicleId: number = 0
    heading: number
    magicAttackSpeed: number
    powerAttackSpeed: number
    attackSpeedMultiplier: number
    isInvisible: boolean
    moveMultiplier: number
    runSpeed: number
    walkSpeed: number
    swimRunSpeed: number
    swimWalkSpeed: number
    flyRunSpeed: number
    flyWalkSpeed: number
    polymorphTemplate: L2NpcTemplate
    normalPacket: Buffer
    invisiblePacket: Buffer

    constructor( player: L2PcInstance ) {
        this.objectId = player.getObjectId()

        if ( player.getVehicle() && player.getInVehiclePosition() ) {
            this.x = player.getInVehiclePosition().getX()
            this.y = player.getInVehiclePosition().getY()
            this.z = player.getInVehiclePosition().getZ()
        } else {
            this.x = player.getX()
            this.y = player.getY()
            this.z = player.getZ()
        }

        this.heading = player.getHeading()
        this.magicAttackSpeed = Math.floor( player.getMagicAttackSpeed() )
        this.powerAttackSpeed = player.getPowerAttackSpeed()
        this.attackSpeedMultiplier = player.getAttackSpeedMultiplier()
        this.isInvisible = player.isInvisible()

        let moveMultiplier = player.getMovementSpeedMultiplier()
        this.moveMultiplier = moveMultiplier
        this.runSpeed = Math.round( player.getRunSpeed() / moveMultiplier )
        this.walkSpeed = Math.round( player.getWalkSpeed() / moveMultiplier )
        this.swimRunSpeed = Math.round( player.getSwimRunSpeed() / moveMultiplier )
        this.swimWalkSpeed = Math.round( player.getSwimWalkSpeed() / moveMultiplier )
        this.flyRunSpeed = player.isFlying() ? this.runSpeed : 0
        this.flyWalkSpeed = player.isFlying() ? this.walkSpeed : 0

        this.polymorphTemplate = player.isPolymorphed() ? DataManager.getNpcData().getTemplate( player.getPolymorphTemplateId() ) : null
    }

    private getTemplatePacket( seeInvisible : boolean ) : Buffer {
        if ( !seeInvisible ) {
            if ( !this.invisiblePacket ) {
                this.invisiblePacket = this.buildWithTemplate( true )
            }

            return this.invisiblePacket
        }

        if ( !this.normalPacket ) {
            this.normalPacket = this.buildWithTemplate( false )
        }

        return this.normalPacket
    }

    private getNonTemplatePacket( seeInvisible : boolean ) : Buffer {
        if ( !seeInvisible ) {
            if ( !this.invisiblePacket ) {
                this.invisiblePacket = this.buildWithoutTemplate( true )
            }

            return this.invisiblePacket
        }

        if ( !this.normalPacket ) {
            this.normalPacket = this.buildWithoutTemplate( false )
        }

        return this.normalPacket
    }

    getBuffer( receivingPlayer: L2PcInstance ): Buffer {
        let gmSeeInvisible = receivingPlayer.hasActionOverride( PlayerActionOverride.SeeInvisible )

        if ( this.polymorphTemplate ) {
            return PacketHelper.copyPacket( this.getTemplatePacket( gmSeeInvisible ) )
        }

        return PacketHelper.copyPacket( this.getNonTemplatePacket( gmSeeInvisible ) )
    }

    buildWithTemplate( gmSeeInvisible: boolean ): Buffer {
        let player = L2World.getPlayer( this.objectId )

        let invisibleTitle = gmSeeInvisible ? 'Invisible' : player.getAppearance().getVisibleTitle()
        return new DeclaredServerPacket( 203 + getStringSize( player.getAppearance().getVisibleName() ) + getStringSize( invisibleTitle ) )
                .writeC( 0x0C )
                .writeD( this.objectId )
                .writeD( this.polymorphTemplate.getId() + 1000000 )
                .writeD( player.getKarma() > 0 ? 1 : 0 )

                .writeD( this.x )
                .writeD( this.y )
                .writeD( this.z )
                .writeD( this.heading )

                .writeD( 0x00 )
                .writeD( this.magicAttackSpeed )
                .writeD( this.powerAttackSpeed )
                .writeD( this.runSpeed )

                .writeD( this.walkSpeed )
                .writeD( this.swimRunSpeed )
                .writeD( this.swimWalkSpeed )
                .writeD( this.flyRunSpeed )

                .writeD( this.flyWalkSpeed )
                .writeD( this.flyRunSpeed )
                .writeD( this.flyWalkSpeed )
                .writeF( this.moveMultiplier )

                .writeF( this.attackSpeedMultiplier )
                .writeF( this.polymorphTemplate.getFCollisionRadius() )
                .writeF( this.polymorphTemplate.getFCollisionHeight() )
                .writeD( this.polymorphTemplate.getRHandId() )

                .writeD( this.polymorphTemplate.getChest() )
                .writeD( this.polymorphTemplate.getLHandId() )
                .writeC( 1 )
                .writeC( player.isRunning() ? 1 : 0 )

                .writeC( player.isInCombat() ? 1 : 0 )
                .writeC( player.isAlikeDead() ? 1 : 0 )
                .writeC( !gmSeeInvisible && this.isInvisible ? 1 : 0 )
                .writeD( -1 ) // High Five NPCString ID

                .writeS( player.getAppearance().getVisibleName() )
                .writeD( -1 ) // High Five NPCString ID
                .writeS( invisibleTitle )
                .writeD( player.getAppearance().getTitleColor() )

                .writeD( player.getPvpFlag() )
                .writeD( player.getKarma() )
                .writeD( gmSeeInvisible ? ( player.getAbnormalVisualEffects() | stealthEffect ) : player.getAbnormalVisualEffects() )
                .writeD( player.getClanId() )

                .writeD( player.getClanCrestId() )
                .writeD( player.getAllyId() )
                .writeD( player.getAllyCrestId() )
                .writeC( player.isFlying() ? 2 : 0 )

                .writeC( player.getTeam() )
                .writeF( this.polymorphTemplate.getFCollisionRadius() )
                .writeF( this.polymorphTemplate.getFCollisionHeight() )
                .writeD( 0x00 ) // enchant effect

                .writeD( player.isFlying() ? 2 : 0 )
                .writeD( 0x00 )
                .writeD( 0x00 ) // CT1.5 Pet form and skills, Color effect
                .writeC( this.polymorphTemplate.isTargetable() ? 1 : 0 )

                .writeC( this.polymorphTemplate.isShowName() ? 1 : 0 )
                .writeC( player.getAbnormalVisualEffectSpecial() )
                .writeD( 0x00 )
                .getBuffer()

    }

    buildWithoutTemplate( gmSeeInvisible: boolean ): Buffer {
        let player = L2World.getPlayer( this.objectId )
        let invisibleTitle = gmSeeInvisible ? 'Invisible' : player.getAppearance().getVisibleTitle()
        let cubicSize = player.getCubics().size

        let packet = new DeclaredServerPacket( getStringSize( player.getAppearance().getVisibleName() ) + getStringSize( invisibleTitle ) + 247 + PaperdollOrder.length * 8 + cubicSize * 2 )
                .writeC( 0x31 )
                .writeD( this.x )
                .writeD( this.y )
                .writeD( this.z )

                .writeD( this.vehicleId )
                .writeD( this.objectId )
                .writeS( player.getAppearance().getVisibleName() )
                .writeD( player.getRace() )

                .writeD( player.getAppearance().getSex() ? 1 : 0 )
                .writeD( player.getBaseClass() )

        PaperdollOrder.forEach( ( slot: number ) => {
            packet.writeD( player.getInventory().getPaperdollItemId( slot ) )
        } )

        PaperdollOrder.forEach( ( slot: number ) => {
            packet.writeD( player.getInventory().getPaperdollAugmentationId( slot ) )
        } )

        packet
                .writeD( player.getTalismanSlots() )
                .writeD( player.getInventory().canEquipCloak() ? 1 : 0 )
                .writeD( player.getPvpFlag() )
                .writeD( player.getKarma() )

                .writeD( this.magicAttackSpeed )
                .writeD( this.powerAttackSpeed )
                .writeD( 0x00 )
                .writeD( this.runSpeed )

                .writeD( this.walkSpeed )
                .writeD( this.swimRunSpeed )
                .writeD( this.swimWalkSpeed )
                .writeD( this.flyRunSpeed )

                .writeD( this.flyWalkSpeed )
                .writeD( this.flyRunSpeed )
                .writeD( this.flyWalkSpeed )
                .writeF( this.moveMultiplier )

                .writeF( this.attackSpeedMultiplier )
                .writeF( player.getCollisionRadius() )
                .writeF( player.getCollisionHeight() )
                .writeD( player.getAppearance().getHairStyle() )

                .writeD( player.getAppearance().getHairColor() )
                .writeD( player.getAppearance().getFace() )
                .writeS( invisibleTitle )

        if ( !player.isCursedWeaponEquipped() ) {
            packet
                    .writeD( player.getClanId() )
                    .writeD( player.getClanCrestId() )
                    .writeD( player.getAllyId() )
                    .writeD( player.getAllyCrestId() )
        } else {
            packet
                    .writeD( 0 )
                    .writeD( 0 )
                    .writeD( 0 )
                    .writeD( 0 )
        }

        packet
                .writeC( player.isSitting() ? 0 : 1 )
                .writeC( player.isRunning() ? 1 : 0 )
                .writeC( player.isInCombat() ? 1 : 0 )
                .writeC( !player.isInOlympiadMode() && player.isAlikeDead() ? 1 : 0 )

                .writeC( !gmSeeInvisible && this.isInvisible ? 1 : 0 )
                .writeC( player.getMountType() )
                .writeC( player.getPrivateStoreType() )
                .writeH( cubicSize )

        player.getCubics().forEach( ( value: L2CubicInstance, cubicId: number ) => {
            packet.writeH( cubicId )
        } )

        packet
                .writeC( PartyMatchManager.getPlayerRoom( player.getObjectId() ) ? 1 : 0 )
                .writeD( gmSeeInvisible ? ( player.getAbnormalVisualEffects() | stealthEffect ) : player.getAbnormalVisualEffects() )
                .writeC( player.getMovementType() )
                .writeH( player.getRecommendationsHave() )

                .writeD( player.getMountNpcId() + 1000000 )
                .writeD( player.getClassId() )
                .writeD( 0x00 )
                .writeC( player.isMounted() ? 0 : player.getEnchantEffect() )

                .writeC( player.getTeam() )
                .writeD( player.getClanCrestLargeId() )
                .writeC( player.isNoble() ? 1 : 0 )
                .writeC( player.isHero() || ( player.isGM() && ConfigManager.general.gmHeroAura() ) ? 1 : 0 )

                .writeC( player.isFishing() ? 1 : 0 )
                .writeD( player.getFishX() )
                .writeD( player.getFishY() )
                .writeD( player.getFishZ() )

                .writeD( player.getAppearance().getNameColor() )
                .writeD( this.heading )
                .writeD( player.getPledgeClass() )
                .writeD( player.getPledgeType() )

                .writeD( player.getAppearance().getTitleColor() )
                .writeD( player.isCursedWeaponEquipped() ? CursedWeaponManager.getLevel( player.getCursedWeaponEquippedId() ) : 0 )
                .writeD( player.getClanId() > 0 ? player.getClan().getReputationScore() : 0 )
                .writeD( player.getTransformationDisplayId() )

                .writeD( player.getAgathionId() )
                .writeD( 1 )
                .writeD( player.getAbnormalVisualEffectSpecial() )

        return packet.getBuffer()
    }
}

export function PlayerInformation( otherPlayer: L2PcInstance, whoIsSending: L2PcInstance ): Buffer {
    return new CharacterInformation( otherPlayer ).getBuffer( whoIsSending )
}

export function DecoyInformation( decoy: L2Decoy, receivingPlayer: L2PcInstance ): Buffer {
    let info = new CharacterInformation( decoy.getActingPlayer() )

    info.objectId = decoy.getObjectId()
    info.x = decoy.getX()
    info.y = decoy.getY()
    info.z = decoy.getZ()
    info.heading = decoy.getHeading()

    return info.getBuffer( receivingPlayer )
}