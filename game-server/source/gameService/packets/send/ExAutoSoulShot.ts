import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum ExAutoSoulShotStatus {
    Disabled,
    Enabled
}

export function ExAutoSoulShot( itemId: number, status: ExAutoSoulShotStatus ) : Buffer {
    return new DeclaredServerPacket( 11 )
        .writeC( 0xFE )
        .writeH( 0x0c )
        .writeD( itemId )
        .writeD( status )
        .getBuffer()
}