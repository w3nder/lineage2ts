import { L2World } from '../../L2World'
import { L2ReuseTime, L2Timestamp } from '../../models/SkillTimestamp'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

/*
    Need to use player id since packet is debounced
 */
export function SkillCoolTime( objectId: number ): Buffer {
    let player = L2World.getPlayer( objectId )
    let skillReuse = player.getSkillReuse()

    skillReuse.removeInactive()

    let packet = new DeclaredServerPacket( 5 + skillReuse.getAllItems().size * 16 )
            .writeC( 0xC7 )
            .writeD( skillReuse.getAllItems().size )

    skillReuse.getAllItems().forEach( ( data: L2ReuseTime ) => {
        packet
                .writeD( data.getId() )
                .writeD( data.getLevel() )
                .writeD( data.getReuseMs() / 1000 )
                .writeD( data.getReuseSeconds() )
    } )

    return packet.getBuffer()
}