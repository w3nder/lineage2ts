import { L2Character } from '../../models/actor/L2Character'
import { L2SiegeClan } from '../../models/L2SiegeClan'
import { CastleManager } from '../../instancemanager/CastleManager'
import { ClanHallSiegeManager } from '../../instancemanager/ClanHallSiegeManager'
import { Castle } from '../../models/entity/Castle'
import { FortManager } from '../../instancemanager/FortManager'
import { Fort } from '../../models/entity/Fort'
import { SiegableHall } from '../../models/entity/clanhall/SiegableHall'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { OlympiadManager } from '../../models/olympiad/OlympiadManager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PlayerPermission } from '../../enums/PlayerPermission'

export function Die( character: L2Character ): Buffer {
    let canTeleport = character.canRevive() && !character.isPendingRevive()
    let clan = character.getClan()
    let isJailed = false
    let staticResurrection = false
    let isSweepActive = character.isSweepActive()

    if ( character.isPlayer() ) {
        if ( !OlympiadManager.isRegisteredNoble( character.getActingPlayer() ) && !character.isOnEvent() ) {
            staticResurrection = character.getInventory().haveItemForSelfResurrection()
        }

        if ( character.getAccessLevel().hasPermission( PlayerPermission.AllowFixedRes ) ) {
            staticResurrection = true
        }

        isJailed = character.getActingPlayer().isJailed()
    }

    let packet = new DeclaredServerPacket( 33 )
            .writeC( 0x00 )
            .writeD( character.getObjectId() )
            .writeD( canTeleport ? 0x01 : 0x00 )

    if ( canTeleport && clan && !isJailed ) {
        let isInCastleDefense = false
        let isInFortDefense = false

        let siegeClan: L2SiegeClan = null
        let castle: Castle = CastleManager.getCastle( character )
        let fort: Fort = FortManager.getFort( character )
        let hall: SiegableHall = ClanHallSiegeManager.getNearbyClanHall( character )
        if ( castle && castle.getSiege().isInProgress() ) {
            // siege in progress
            siegeClan = castle.getSiege().getAttackerClan( clan )
            if ( !siegeClan && castle.getSiege().checkIsDefender( clan ) ) {
                isInCastleDefense = true
            }
        } else if ( fort && fort.getSiege().isInProgress() ) {
            // siege in progress
            siegeClan = fort.getSiege().getAttackerClan( clan )
            if ( !siegeClan && fort.getSiege().checkIsDefender( clan ) ) {
                isInFortDefense = true
            }
        }

        let hasSiegeHq : boolean = !!TerritoryWarManager.getSiegeFlagForClan( clan )
                || ( siegeClan && !isInCastleDefense && !isInFortDefense && siegeClan.getFlag().length !== 0 )
                || ( hall && hall.getSiege().checkIsAttacker( clan ) )
        packet
                .writeD( clan.getHideoutId() > 0 ? 0x01 : 0x00 ) // escape to hide away
                .writeD( ( clan.getCastleId() > 0 ) || isInCastleDefense ? 0x01 : 0x00 ) // escape to castle
                .writeD( hasSiegeHq ? 0x01 : 0x00 ) // escape to siege HQ
                .writeD( isSweepActive ? 0x01 : 0x00 ) // is sweepable
                .writeD( staticResurrection ? 0x01 : 0x00 ) // self resurrection
                .writeD( ( clan.getFortId() > 0 ) || isInFortDefense ? 0x01 : 0x00 ) // escape to fortress
    } else {
        packet
                .writeD( 0x00 ) // to hide away
                .writeD( 0x00 ) // to castle
                .writeD( 0x00 ) // to siege HQ
                .writeD( isSweepActive ? 0x01 : 0x00 ) // is sweepable
                .writeD( staticResurrection ? 0x01 : 0x00 ) // self resurrection
                .writeD( 0x00 ) // escape to fortress
    }

    return packet.getBuffer()
}