import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ShowXMasSeal( itemId: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0xF8 )
            .writeD( itemId )
            .getBuffer()
}