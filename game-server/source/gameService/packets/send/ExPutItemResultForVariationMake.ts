import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExPutItemResultForVariationMake( objectId: number, itemId: number ) : Buffer {
    return new DeclaredServerPacket( 15 )
            .writeC( 0xFE )
            .writeH( 0x53 )
            .writeD( objectId )
            .writeD( itemId )

            .writeD( 1 )
            .getBuffer()
}