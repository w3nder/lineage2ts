import { Location } from '../../models/Location'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ObservationMode( location: Location ) {
    return new DeclaredServerPacket( 16 )
            .writeC( 0xEB )
            .writeD( location.getX() )
            .writeD( location.getY() )
            .writeD( location.getZ() )

            .writeC( 0 )
            .writeC( 0 )
            .writeC( 0 )
            .getBuffer()
}