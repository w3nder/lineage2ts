import { CursedWeaponManager } from '../../instancemanager/CursedWeaponManager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

// TODO : consider caching packet in memory if used often
export function ExCursedWeaponList(): Buffer {
    let ids: Array<number> = CursedWeaponManager.getCursedWeaponIds()

    let packet = new DeclaredServerPacket( 7 + ids.length * 4 )
            .writeC( 0xFE )
            .writeH( 0x46 )
            .writeD( ids.length )

    ids.forEach( ( id: number ) => {
        packet.writeD( id )
    } )

    return packet.getBuffer()
}