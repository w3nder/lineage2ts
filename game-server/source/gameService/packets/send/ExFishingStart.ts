import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { FishGroup } from '../../enums/FishGroup'

export function ExFishingStart( objectId: number,
                               group: FishGroup,
                               x: number,
                               y: number,
                               z: number,
                               isNightTime: boolean ) : Buffer {
    return new DeclaredServerPacket( 25 )
            .writeC( 0xFE )
            .writeH( 0x1E )
            .writeD( objectId )
            .writeD( group )

            .writeD( x )
            .writeD( y )
            .writeD( z )
            .writeC( isNightTime ? 0x01 : 0x00 )

            .writeC( 0x00 ) // show fish rank result button
            .getBuffer()
}