import { L2AirShipInstance } from '../../models/actor/instance/L2AirShipInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExAirShipInfo( airship : L2AirShipInstance ) : Buffer {
    let packet = new DeclaredServerPacket( 71 )
            .writeC( 0xfe )
            .writeH( 0x60 )
            .writeD( airship.getObjectId() )
            .writeD( airship.getX() )

            .writeD( airship.getY() )
            .writeD( airship.getZ() )
            .writeD( airship.getHeading() )
            .writeD( airship.getCaptainId() )

            .writeD( airship.getStat().getMoveSpeed() )
            .writeD( airship.getStat().getRotationSpeed() )
            .writeD( airship.getHelmObjectId() )

    if ( airship.getHelmObjectId() !== 0 ) {
        packet
                .writeD( 0x16e ) // Controller X
                .writeD( 0x00 ) // Controller Y
                .writeD( 0x6b ) // Controller Z
                .writeD( 0x15c ) // Captain X

                .writeD( 0x00 ) // Captain Y
                .writeD( 0x69 ) // Captain Z
    } else {
        packet
                .writeD( 0x00 )
                .writeD( 0x00 )
                .writeD( 0x00 )
                .writeD( 0x00 )

                .writeD( 0x00 )
                .writeD( 0x00 )
    }

    packet
            .writeD( airship.getFuel() )
            .writeD( airship.getMaxFuel() )

    return packet.getBuffer()
}