import { L2ManufactureItem } from '../../models/L2ManufactureItem'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PlayerRecipeCache } from '../../cache/PlayerRecipeCache'

export function RecipeShopManageList( player: L2PcInstance, isDwarven: boolean ): Buffer {
    let books = PlayerRecipeCache.getBooks( player.getObjectId() )
    let recipes: Set<number> = ( isDwarven && player.hasDwarvenCraft() ) ? books.dwarven : books.common

    if ( player.hasManufactureShop() ) {
        player.removeManufactureItems( isDwarven )
    }

    let manufactureSize = player.getManufactureItems().length

    let packet = new DeclaredServerPacket( 21 + recipes.size * 8 + manufactureSize * 16 )
            .writeC( 0xDE )
            .writeD( player.getObjectId() )
            .writeD( player.getAdena() )
            .writeD( isDwarven ? 0 : 1 )

            .writeD( recipes.size )

    let index = 0
    recipes.forEach( ( recipeId: number ) => {
        packet
                .writeD( recipeId )
                .writeD( index + 1 )

        index++
    } )

    packet.writeD( manufactureSize )
    player.getManufactureItems().forEach( ( item: L2ManufactureItem ) => {
        packet
                .writeD( item.recipeId )
                .writeD( 0 )
                .writeQ( item.adenaCost )
    } )

    return packet.getBuffer()
}