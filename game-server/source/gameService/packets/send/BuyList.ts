import { L2BuyList } from '../../models/buylist/L2BuyList'
import { Product } from '../../models/buylist/Product'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

const paddingSize : number = 18
const zeroes : Buffer = Buffer.allocUnsafeSlow( paddingSize ).fill( 0 )

export function BuyList( buylist : L2BuyList, currentMoney: number, taxRate: number ) : Buffer {
    let currentTime = Date.now()
    let productsToSell : Array<Product> = []
    let fullTaxRate = 1 + taxRate

    buylist.getProducts().forEach( ( product : Product ) : void => {
        product.attemptToRestock( currentTime )

        if ( product.count > 0 || !product.hasLimitedStock() ) {
            productsToSell.push( product )
        }
    } )

    const packet = new DeclaredServerPacket( 21 + productsToSell.length * ( paddingSize + 58 ) )
            .writeC( 0xFE )
            .writeH( 0xB7 )
            .writeD( 0 )
            .writeQ( currentMoney )

            .writeD( buylist.listId )
            .writeH( productsToSell.length )

    productsToSell.forEach( ( product: Product ) => {
        packet
                .writeD( product.getItemId() )
                .writeD( product.getItemId() )
                .writeD( 0 )
                .writeQ( product.count < 0 ? 0 : product.count )

                .writeH( product.getItem().getType2() )
                .writeH( product.getItem().getType1() )
                .writeH( 0x00 ) // isEquipped
                .writeD( product.getItem().getBodyPart() ) // Body Part

                .writeH( product.getItem().getDefaultEnchantLevel() ) // Enchant
                .writeH( 0x00 ) // Custom Type
                .writeD( 0x00 ) // Augment
                .writeD( -1 ) // Mana

                .writeD( -9999 ) // Time
                .writeH( 0x00 ) // Element Type
                .writeH( 0x00 ) // Element Power
                .writeB( zeroes )

        // TODO : figure out if tax prices can be cached and recompute on special events like castle tax rate set
        packet.writeQ( Math.floor( product.getComputedPrice() * fullTaxRate ) )
    } )

    return packet.getBuffer()
}