import { SevenSigns } from '../../directives/SevenSigns'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ShowMiniMap( id: number ) : Buffer {
    return new DeclaredServerPacket( 6 )
            .writeC( 0xA3 )
            .writeD( id )
            .writeC( SevenSigns.getCurrentPeriod() )
            .getBuffer()
}