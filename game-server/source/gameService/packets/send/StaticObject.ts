import { L2DoorInstance } from '../../models/actor/instance/L2DoorInstance'
import { L2StaticObjectInstance } from '../../models/actor/instance/L2StaticObjectInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function StaticDoorObject( door: L2DoorInstance, isTargetable: boolean ) : Buffer {
    let isTarget = ( door.isTargetable() || isTargetable )

    return new DeclaredServerPacket( 45 )
            .writeC( 0x9F )
            .writeD( door.getId() )
            .writeD( door.getObjectId() )
            .writeD( 1 )

            .writeD( isTarget ? 1 : 0 )
            .writeD( door.getMeshIndex() )
            .writeD( !door.isOpen() ? 1 : 0 )
            .writeD( door.isEnemy() ? 1 : 0 )

            .writeD( door.getCurrentHp() )
            .writeD( door.getMaxHp() )
            .writeD( door.getIsShowHp() ? 1 : 0 )
            .writeD( door.getDamage() )

            .getBuffer()
}

export function StaticObjectInstance( object: L2StaticObjectInstance ) : Buffer {
    return new DeclaredServerPacket( 45 )
            .writeC( 0x9F )
            .writeD( object.getId() )
            .writeD( object.getObjectId() )
            .writeD( 0 )

            .writeD( 1 )
            .writeD( object.getMeshIndex() )
            .writeD( 0 )
            .writeD( 0 )

            .writeD( object.getCurrentHp() )
            .writeD( object.getMaxHp() )
            .writeD( 0 )
            .writeD( 0 )

            .getBuffer()
}