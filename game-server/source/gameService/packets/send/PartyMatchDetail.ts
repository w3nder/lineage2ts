import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { PartyMatchManager, PartyMatchRoom } from '../../cache/partyMatchManager'

export function PartyMatchDetail( room: PartyMatchRoom ): Buffer {
    return new DeclaredServerPacket( 27 + getStringSize( room.title ) )
            .writeC( 0x9D )
            .writeD( room.id )
            .writeD( room.maximumMembers )
            .writeD( room.minimumLevel )

            .writeD( room.maximumLevel )
            .writeD( room.lootType )
            .writeD( PartyMatchManager.getRoomLocation( room ) )
            .writeS( room.title )

            .writeH( 59064 )
            .getBuffer()
}