import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function NicknameChanged( player: L2PcInstance ): Buffer {
    return new DeclaredServerPacket( 5 + getStringSize( player.getTitle() ) )
            .writeC( 0xcc )
            .writeD( player.getObjectId() )
            .writeS( player.getTitle() )
            .getBuffer()
}