import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2StaticObjectMapLocation } from '../../models/actor/L2StaticObjectMapLocation'

export function ShowTownMap( data: L2StaticObjectMapLocation ) : Buffer {
    return new DeclaredServerPacket( 9 + getStringSize( data.name ) )
            .writeC( 0xEA )
            .writeS( data.name )
            .writeD( data.x )
            .writeD( data.y )
            .getBuffer()
}