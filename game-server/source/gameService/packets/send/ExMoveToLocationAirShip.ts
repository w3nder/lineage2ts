import { L2Character } from '../../models/actor/L2Character'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExMoveToLocationAirShip( object: L2Character ): Buffer {
    return new DeclaredServerPacket( 31 )
            .writeC( 0xFE )
            .writeH( 0x65 )
            .writeD( object.getObjectId() )
            .writeD( object.getXDestination() )

            .writeD( object.getYDestination() )
            .writeD( object.getZDestination() )
            .writeD( object.getX() )
            .writeD( object.getY() )

            .writeD( object.getZ() )
            .getBuffer()
}