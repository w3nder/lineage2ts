import { Location } from '../../models/Location'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExGetOnAirShip( playerObjectId: number, shipObjectId: number, position: Location ) : Buffer {
    return new DeclaredServerPacket( 23 )
        .writeC( 0xFE )
        .writeH( 0x63 )
        .writeD( playerObjectId )
        .writeD( shipObjectId )

        .writeD( position.getX() )
        .writeD( position.getY() )
        .writeD( position.getZ() )
        .getBuffer()
}