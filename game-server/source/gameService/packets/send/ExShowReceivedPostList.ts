import { MailMessage } from '../../models/mail/MailMessage'
import { MailManager } from '../../instancemanager/MailManager'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2MessagesTableItemStatus } from '../../../database/interface/MessagesTableApi'
import { MailMessageStatus } from '../../enums/MailMessageStatus'

export function ExShowReceivedPostList( objectId: number ): Buffer {
    let inboxMessages: Array<MailMessage> = MailManager.getInbox( objectId )
    let dynamicSize: number = inboxMessages.reduce( ( totalAmount: number, message: MailMessage ): number => {
        let name = CharacterNamesCache.getCachedNameById( message.getSenderId() ) ?? ConfigManager.general.getMailSystemName()
        return totalAmount + getStringSize( message.getSubject() ) + getStringSize( name ) + 36
    }, 0 )

    let packet = new DeclaredServerPacket( 11 + dynamicSize )
            .writeC( 0xFE )
            .writeH( 0xAA )
            .writeD( Math.floor( Date.now() / 1000 ) )
            .writeD( inboxMessages.length )

    inboxMessages.forEach( ( message: MailMessage ) => {
        packet
                .writeD( message.getId() )
                .writeS( message.getSubject() )
                .writeS( CharacterNamesCache.getCachedNameById( message.getSenderId() ) ?? ConfigManager.general.getMailSystemName() )
                .writeD( message.isCOD() ? 0x01 : 0x00 )

                .writeD( message.getExpirationSeconds() )
                .writeD( message.getStatus() === L2MessagesTableItemStatus.Unread ? MailMessageStatus.Unread : MailMessageStatus.Read )
                .writeD( 0x01 )
                .writeD( message.hasAttachments() ? 0x01 : 0x00 )

                .writeD( message.isReturned() ? 0x01 : 0x00 )
                .writeD( message.getSendBySystem() )
                .writeD( 0x00 )
    } )

    return packet.getBuffer()
}