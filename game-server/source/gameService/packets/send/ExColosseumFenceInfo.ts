import { L2ColosseumFence } from '../../models/actor/instance/L2ColosseumFence'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExColosseumFenceInfo( fence: L2ColosseumFence ): Buffer {
    return new DeclaredServerPacket( 31 )
            .writeC( 0xfe )
            .writeH( 0x03 )
            .writeD( fence.getObjectId() )
            .writeD( fence.getState() )

            .writeD( fence.getX() )
            .writeD( fence.getY() )
            .writeD( fence.getZ() )
            .writeD( fence.width )

            .writeD( fence.height )
            .getBuffer()
}