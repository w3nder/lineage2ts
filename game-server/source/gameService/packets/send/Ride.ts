import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function Ride( player: L2PcInstance ) : Buffer {
    return new DeclaredServerPacket( 29 )
        .writeC( 0x8C )
        .writeD( player.getObjectId() )
        .writeD( player.isMounted() ? 1 : 0 )
        .writeD( player.getMountType() )

        .writeD( player.getMountNpcId() + 1000000 )
        .writeD( player.getX() )
        .writeD( player.getY() )
        .writeD( player.getZ() )

        .getBuffer()
}