import { ListContainer } from '../../models/multisell/ListContainer'
import { MultisellValues } from '../../values/MultisellValues'
import { Entry } from '../../models/multisell/Entry'
import { IServerPacket } from '../../../packets/IServerPacket'
import { Ingredient } from '../../models/multisell/Ingredient'
import { ConfigManager } from '../../../config/ConfigManager'
import { DataManager } from '../../../data/manager'
import _ from 'lodash'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

const maxValue = -2
const entryZeroes: Buffer = Buffer.allocUnsafeSlow( 14 ).fill( 0 )
const writeIngredientZeroes: Buffer = Buffer.allocUnsafeSlow( 26 ).fill( 0 )

function getDataSize( entries: Array<Entry>, size: number, startIndex: number ) : number {
    let index = startIndex
    let itemSize = size
    let dataSize = 0

    while ( itemSize > 0 ) {
        let entry = entries[ index++ ]
        dataSize = dataSize + 35 + entry.getProducts().length * 44 + getIngredients( entry ).length * 40
        itemSize--
    }

    return dataSize
}

function getIngredients( entry: Entry ) : Array<Ingredient> {
    return ConfigManager.tuning.isManufactureRemoveRecipeIngredients() ? entry.getIngredients().filter( isNotRecipeIngredient ) : entry.getIngredients()
}

export function MultiSellList( list: ListContainer, index: number ): Buffer {
    let size = list.getEntries().length - index
    let isFinished = true

    if ( size > MultisellValues.PAGE_SIZE ) {
        isFinished = false
        size = MultisellValues.PAGE_SIZE
    }


    let packet = new DeclaredServerPacket( 21 + getDataSize( list.getEntries(), size, index ) )
            .writeC( 0xD0 )
            .writeD( list.getListId() )
            .writeD( 1 + Math.floor( index / MultisellValues.PAGE_SIZE ) )
            .writeD( isFinished ? 1 : 0 )

            .writeD( MultisellValues.PAGE_SIZE )
            .writeD( size )

    _.times( size, () => {
        const entry: Entry = list.getEntries()[ index++ ]
        const ingredients: Array<Ingredient> = getIngredients( entry )

        packet
                .writeD( entry.getEntryId() )
                .writeC( entry.isStackable() ? 1 : 0 )
                .writeH( 0x00 )
                .writeD( 0x00 )

                .writeD( 0x00 )
                .writeH( maxValue )
                .writeB( entryZeroes )
                .writeH( entry.getProducts().length )

                .writeH( ingredients.length )

        entry.getProducts().forEach( writeProduct.bind( null, packet ) )
        ingredients.forEach( writeIngredient.bind( null, packet ) )
    } )

    return packet.getBuffer()
}

function writeProduct( packet: IServerPacket, ingredient: Ingredient ): void {
    packet.writeD( ingredient.getItemId() )

    if ( ingredient.getTemplate() ) {
        packet
                .writeD( ingredient.getTemplate().getBodyPart() )
                .writeH( ingredient.getTemplate().getType2() )
    } else {
        packet
                .writeD( 0 )
                .writeH( maxValue )
    }

    packet.writeQ( ingredient.getItemCount() )

    if ( ingredient.getItemInfo() ) {
        packet
                .writeH( ingredient.getItemInfo().getEnchantLevel() ) // enchant level
                .writeD( ingredient.getItemInfo().getAugmentId() ) // augment id
                .writeD( 0x00 ) // mana
                .writeH( ingredient.getItemInfo().getElementId() ) // attack element

                .writeH( ingredient.getItemInfo().getElementPower() ) // element power
                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 0 ] ) // fire
                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 1 ] ) // water
                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 2 ] ) // wind

                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 3 ] ) // earth
                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 4 ] ) // holy
                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 5 ] ) // dark
        return
    }

    packet.writeB( writeIngredientZeroes )
}

function writeIngredient( packet: IServerPacket, ingredient: Ingredient ): void {
    packet
            .writeD( ingredient.getItemId() )
            .writeH( ingredient.getTemplate() ? ingredient.getTemplate().getType2() : maxValue )
            .writeQ( ingredient.getItemCount() )

    if ( ingredient.getItemInfo() ) {
        packet
                .writeH( ingredient.getItemInfo().getEnchantLevel() ) // enchant level
                .writeD( ingredient.getItemInfo().getAugmentId() ) // augment id
                .writeD( 0x00 ) // mana
                .writeH( ingredient.getItemInfo().getElementId() ) // attack element

                .writeH( ingredient.getItemInfo().getElementPower() ) // element power
                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 0 ] ) // fire
                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 1 ] ) // water
                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 2 ] ) // wind

                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 3 ] ) // earth
                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 4 ] ) // holy
                .writeH( ingredient.getItemInfo().getDefenseAttributes()[ 5 ] ) // dark
        return
    }

    packet.writeB( writeIngredientZeroes )
}

function isNotRecipeIngredient( ingredient: Ingredient ): boolean {
    return !DataManager.getItems().getTemplate( ingredient.getItemId() ).isRecipe()
}