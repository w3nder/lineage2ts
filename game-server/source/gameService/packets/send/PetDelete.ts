import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2SummonType } from '../../enums/L2SummonType'

export function PetDelete( petType: L2SummonType, objectId: number ) : Buffer {
    return new DeclaredServerPacket( 9 )
        .writeC( 0xB7 )
        .writeD( petType )
        .writeD( objectId )
        .getBuffer()
}