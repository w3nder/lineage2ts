import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExPutEnchantSupportItemResult( objectId: number ) : Buffer {
    return new DeclaredServerPacket( 7 )
            .writeC( 0xFE )
            .writeH( 0x82 )
            .writeD( objectId )
            .getBuffer()
}