import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function PrivateStoreMessageSell( player: L2PcInstance ): Buffer {
    let storeTitle = player.getSellList() ? player.getSellList().getTitle() : ''
    return new DeclaredServerPacket( 5 + getStringSize( storeTitle ) )
            .writeC( 0xA2 )
            .writeD( player.getObjectId() )
            .writeS( storeTitle )
            .getBuffer()
}