import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function ExMoveToLocationInAirShip( player: L2PcInstance ): Buffer {
    let destination = player.getInVehiclePosition()

    return new DeclaredServerPacket( 27 )
            .writeC( 0xfe )
            .writeH( 0x6D )
            .writeD( player.getObjectId() )
            .writeD( player.getAirShip().getObjectId() )

            .writeD( destination.getX() )
            .writeD( destination.getY() )
            .writeD( destination.getZ() )
            .writeD( player.getHeading() )
            .getBuffer()
}