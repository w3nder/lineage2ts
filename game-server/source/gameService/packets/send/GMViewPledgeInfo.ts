import { L2ClanMember } from '../../models/L2ClanMember'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import _ from 'lodash'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function GMViewPledgeInfo( player: L2PcInstance ): Buffer {
    let clan = player.getClan()
    let members: Array<L2ClanMember> = _.compact( clan.getMembers() )
    let size = members.reduce( ( total: number, member: L2ClanMember ): number => {
        return total + getStringSize( member.getName() ) + 24
    }, 0 )

    let packet = new DeclaredServerPacket( 65 + size + getStringSize( player.getName() ) + getStringSize( clan.getName() ) + getStringSize( clan.getLeaderName() ) + getStringSize( clan.getAllyName() ) )
            .writeC( 0x96 )
            .writeS( player.getName() )
            .writeD( clan.getId() )
            .writeD( 0 )

            .writeS( clan.getName() )
            .writeS( clan.getLeaderName() )
            .writeD( clan.getCrestId() )
            .writeD( clan.getLevel() )

            .writeD( clan.getCastleId() )
            .writeD( clan.getHideoutId() )
            .writeD( clan.getFortId() )
            .writeD( clan.getRank() )

            .writeD( clan.getReputationScore() )
            .writeD( 0 )
            .writeD( 0 )
            .writeD( clan.getAllyId() )

            .writeS( clan.getAllyName() )
            .writeD( clan.getAllyCrestId() )
            .writeD( clan.isAtWar() ? 1 : 0 )
            .writeD( 0 )

            .writeD( members.length )

    members.forEach( ( member: L2ClanMember ) => {
        packet
                .writeS( member.getName() )
                .writeD( member.getLevel() )
                .writeD( member.getClassId() )
                .writeD( member.getSex() ? 1 : 0 )

                .writeD( member.getRace() )
                .writeD( member.isOnline() ? member.getObjectId() : 0 )
                .writeD( member.getSponsor() !== 0 ? 1 : 0 )
    } )

    return packet.getBuffer()
}