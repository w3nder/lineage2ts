import { L2Summon } from '../../models/actor/L2Summon'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { L2ServitorInstance } from '../../models/actor/instance/L2ServitorInstance'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function PetStatusUpdate( summon: L2Summon ) : Buffer {
    let currentFeed, maxFeed = 0

    if ( summon.isPet() ) {
        let pet : L2PetInstance = summon as L2PetInstance
        currentFeed = pet.getCurrentFeed()
        maxFeed = pet.getMaxFeed()
    }

    if ( summon.isServitor() ) {
        let servitor : L2ServitorInstance = summon as L2ServitorInstance
        currentFeed = servitor.getLifeTimeRemaining()
        maxFeed = servitor.getLifeTime()
    }

    return new DeclaredServerPacket( 73 + getStringSize( summon.getTitle() ) )
        .writeC( 0xB6 )
        .writeD( summon.getSummonType() )
        .writeD( summon.getObjectId() )
        .writeD( summon.getX() )

        .writeD( summon.getY() )
        .writeD( summon.getZ() )
        .writeS( summon.getTitle() )
        .writeD( currentFeed )

        .writeD( maxFeed )
        .writeD( summon.getCurrentHp() )
        .writeD( summon.getMaxHp() )
        .writeD( summon.getCurrentMp() )

        .writeD( summon.getMaxMp() )
        .writeD( summon.getLevel() )
        .writeQ( summon.getExp() )
        .writeQ( summon.getExpForThisLevel() )

        .writeQ( summon.getExpForNextLevel() - 1 )
        .getBuffer()
}