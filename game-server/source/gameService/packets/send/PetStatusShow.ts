import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'

export function PetStatusShow( pet: L2PetInstance ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0xB1 )
            .writeD( pet.getSummonType() )
            .getBuffer()
}