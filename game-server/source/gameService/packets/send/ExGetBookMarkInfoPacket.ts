import { L2World } from '../../L2World'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { TeleportBookmarkCache } from '../../cache/TeleportBookmarkCache'
import { L2CharacterTeleportBookmarkItem } from '../../../database/interface/CharacterTeleportBookmarksTableApi'
import _ from 'lodash'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function ExGetBookMarkInfoPacket( objectId: number ): Buffer {
    let player = L2World.getPlayer( objectId )
    return ExGetBookMarkInfoPacketWithPlayer( player )
}

export function ExGetBookMarkInfoPacketWithPlayer( player: L2PcInstance ): Buffer {
    let bookmarks = TeleportBookmarkCache.get( player.getObjectId() )

    let dynamicSize = _.reduce( bookmarks, ( totalAmount: number, bookmark: L2CharacterTeleportBookmarkItem ): number => {
        return totalAmount + getStringSize( bookmark.name ) + getStringSize( bookmark.tag ) + 20
    }, 0 )

    let packet = new DeclaredServerPacket( 15 + dynamicSize )
            .writeC( 0xFE )
            .writeH( 0x84 )
            .writeD( 0x00 )
            .writeD( player.getAvailableBookmarkSlots() )
            .writeD( _.size( bookmarks ) )

    _.each( bookmarks, ( bookmark: L2CharacterTeleportBookmarkItem ) => {
        packet
                .writeD( bookmark.id )
                .writeD( bookmark.x )
                .writeD( bookmark.y )
                .writeD( bookmark.z )

                .writeS( bookmark.name )
                .writeD( bookmark.icon )
                .writeS( bookmark.tag )
    } )

    return packet.getBuffer()
}