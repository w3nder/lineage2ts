import { L2World } from '../../L2World'
import { RecommendationBonus } from '../../../data/RecommendationBonus'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function ExVoteSystemInfo( playerId: number ): Buffer {
    let player = L2World.getPlayer( playerId )
    return ExVoteSystemInfoWithPlayer( player )
}

export function ExVoteSystemInfoWithPlayer( player: L2PcInstance ): Buffer {
    let recommendationsLeft = player.getRecommendationsLeft()
    let recommendationsHave = player.getRecommendationsHave()
    let bonusTime = player.getRecommendationsBonusTime()
    let bonusValue = RecommendationBonus.getBonus( player )
    let bonusType = player.getRecommendationsBonusType()

    return new DeclaredServerPacket( 23 )
            .writeC( 0xFE )
            .writeH( 0xC9 )
            .writeD( recommendationsLeft )
            .writeD( recommendationsHave )

            .writeD( bonusTime )
            .writeD( bonusValue )
            .writeD( bonusType )
            .getBuffer()
}