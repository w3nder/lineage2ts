import { SMParam } from '../../interface/SMParam'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { IServerPacket } from '../../../packets/IServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { Skill } from '../../models/Skill'
import { L2Character } from '../../models/actor/L2Character'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2Summon } from '../../models/actor/L2Summon'
import { L2DoorInstance } from '../../models/actor/instance/L2DoorInstance'
import { L2NpcTemplate } from '../../models/actor/templates/L2NpcTemplate'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2Item } from '../../models/items/L2Item'
import { DataManager } from '../../../data/manager'
import { IBuilderPacket } from '../IBuilderPacket'
import { PacketHelper } from '../PacketVariables'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'

const enum ParameterType {
    SystemString = 13,
    PlayerName = 12,
    DoorName = 11,
    InstanceName = 10,
    ElementName = 9,
    PlaceName = 7,
    BigInteger = 6,
    CastleName = 5,
    SkillName = 4,
    ItemName = 3,
    NpcName = 2,
    Number = 1,
    Text = 0,
}

export const SystemMessageHelper = {
    sendString: ( message: string ): Buffer => {
        return new SystemMessageBuilder( SystemMessageIds.S1 ).addString( message ).getBuffer()
    },
}

const systemMessageCache: { [ messageId: number ]: Buffer } = {}

function getParameterSize( parameter: SMParam ): number {
    switch ( parameter.type ) {
        case ParameterType.Text:
        case ParameterType.PlayerName:
            return getStringSize( parameter.value )

        case ParameterType.BigInteger:
            return 8

        case ParameterType.ItemName:
        case ParameterType.CastleName:
        case ParameterType.Number:
        case ParameterType.NpcName:
        case ParameterType.ElementName:
        case ParameterType.SystemString:
        case ParameterType.InstanceName:
        case ParameterType.DoorName:
            return 4

        case ParameterType.SkillName:
            return 8

        case ParameterType.PlaceName:
            return 12
    }

    return 0
}

export class SystemMessageBuilder implements IBuilderPacket {
    parameters: Array<SMParam> = []
    messageId: number

    constructor( messageId: number ) {
        this.messageId = messageId
    }

    static fromMessageId( messageId: number ): Buffer {
        if ( !systemMessageCache[ messageId ] ) {
            let packet: Buffer = new SystemMessageBuilder( messageId ).getBuffer()

            let storedPacket: Buffer = Buffer.allocUnsafeSlow( packet.length )
            packet.copy( storedPacket )

            systemMessageCache[ messageId ] = storedPacket
        }

        return PacketHelper.copyPacket( systemMessageCache[ messageId ] )
    }

    // TODO : add object pool
    addValue( type: number, value: any ) {
        let item: SMParam = {
            type,
            value,
        }
        this.parameters.push( item )
        return this
    }

    addString( value: string ) {
        return this.addValue( ParameterType.Text, value )
    }

    addBigNumber( value: number ) {
        return this.addValue( ParameterType.BigInteger, value )
    }

    addNumber( value: number ) {
        return this.addValue( ParameterType.Number, value )
    }

    addCastleId( value: number ) {
        return this.addValue( ParameterType.CastleName, value )
    }

    addPlayerCharacterName( player: L2PcInstance ) {
        return this.addValue( ParameterType.PlayerName, player.getAppearance().getVisibleName() )
    }

    addPlayerNameWithId( playerId: number ) {
        return this.addValue( ParameterType.PlayerName, CharacterNamesCache.getCachedNameById( playerId ) )
    }

    addDoorName( value: number ) {
        return this.addValue( ParameterType.DoorName, value )
    }

    addSkillName( skill: Skill ) {
        return this.addValue( ParameterType.SkillName, [ skill.getId(), skill.getLevel() ] )
    }

    addSkillNameById( id: number ) {
        return this.addValue( ParameterType.SkillName, [ id, 1 ] )
    }

    addSystemString( value: number ) {
        return this.addValue( ParameterType.SystemString, value )
    }

    getBuffer(): Buffer {
        let packet = new DeclaredServerPacket( 9 + this.getPacketParameterSize() )
                .writeC( 0x62 )
                .writeD( this.messageId )
                .writeD( this.parameters.length )

        this.appendParameters( packet )

        return packet.getBuffer()
    }

    getPacketParameterSize() : number {
        return this.parameters.reduce( ( size: number, parameter: SMParam ): number => {
            return size + 4 + getParameterSize( parameter )
        }, 0 )
    }

    appendParameters( packet: IServerPacket ) : void {
        this.parameters.forEach( ( currentParameter: SMParam ) => {
            packet.writeD( currentParameter.type )

            switch ( currentParameter.type ) {
                case ParameterType.Text:
                case ParameterType.PlayerName:
                    packet.writeS( currentParameter.value )
                    break

                case ParameterType.BigInteger:
                    packet.writeQ( currentParameter.value )
                    break

                case ParameterType.ItemName:
                case ParameterType.CastleName:
                case ParameterType.Number:
                case ParameterType.NpcName:
                case ParameterType.ElementName:
                case ParameterType.SystemString:
                case ParameterType.InstanceName:
                case ParameterType.DoorName:
                    packet.writeD( currentParameter.value )
                    break

                case ParameterType.SkillName:
                    packet.writeD( currentParameter.value[ 0 ] )
                    packet.writeD( currentParameter.value[ 1 ] )
                    break

                case ParameterType.PlaceName:
                    packet.writeD( currentParameter.value[ 0 ] )
                    packet.writeD( currentParameter.value[ 1 ] )
                    packet.writeD( currentParameter.value[ 2 ] )
                    break
            }
        } )
    }

    addCharacterName( character: L2Character ) {
        if ( character.isNpc() ) {
            let npc = character as L2Npc
            if ( npc.getTemplate().isUsingServerSideName() ) {
                return this.addString( npc.getTemplate().getName() )
            }
            return this.addNpcName( npc )
        }

        if ( character.isPlayer() ) {
            return this.addPlayerCharacterName( character.getActingPlayer() )
        }

        if ( character.isSummon() ) {
            let summon = character as L2Summon
            if ( summon.getTemplate().isUsingServerSideName() ) {
                return this.addString( summon.getTemplate().getName() )
            }

            return this.addSummonName( summon )
        }

        if ( character.isDoor() ) {
            let door = character as L2DoorInstance
            return this.addDoorName( door.getId() )
        }

        return this.addString( character.getName() )
    }

    addNpcName( npc: L2Npc ) {
        return this.addNpcNameWithTemplate( npc.getTemplate() )
    }

    addNpcNameWithTemplate( template: L2NpcTemplate ) {
        if ( template.isUsingServerSideName() ) {
            return this.addString( template.getName() )
        }

        return this.addNpcNameWithTemplateId( template.getId() )
    }

    addNpcNameWithTemplateId( id: number ) {
        return this.addValue( ParameterType.NpcName, 1000000 + id )
    }

    addSummonName( summon: L2Summon ) {
        return this.addNpcNameWithTemplateId( summon.getId() )
    }

    addItemInstanceName( item: L2ItemInstance ) {
        return this.addItemNameWithId( item.getId() )
    }

    addItemName( item: L2Item ) {
        return this.addItemNameWithId( item.getId() )
    }

    addItemNameWithId( id: number ) {
        let item: L2Item = DataManager.getItems().getTemplate( id )
        if ( item.getId() !== id ) {
            return this.addString( item.getName() )
        }

        return this.addValue( ParameterType.ItemName, id )
    }

    regionFromCoordinates( x: number, y: number, z: number ) {
        return this.addValue( ParameterType.PlaceName, [ x, y, z ] )
    }

    addElemental( type: number ) {
        return this.addValue( ParameterType.ElementName, type )
    }

    addInstanceName( templateId: number ) {
        return this.addValue( ParameterType.InstanceName, templateId )
    }
}

export class ConfirmDialog extends SystemMessageBuilder {
    time: number = 0
    requesterId: number = 0

    addTime( value: number ) {
        this.time = value
        return this
    }

    addRequesterId( value: number ) {
        this.requesterId = value
        return this
    }

    getBuffer(): Buffer {
        let packet = new DeclaredServerPacket( 17 + this.getPacketParameterSize() )
                .writeC( 0xF3 )
                .writeD( this.messageId )
                .writeD( this.parameters.length )

        this.appendParameters( packet )

        packet
                .writeD( this.time )
                .writeD( this.requesterId )

        return packet.getBuffer()
    }

    static fromText( text: string ): ConfirmDialog {
        let message = new ConfirmDialog( SystemMessageIds.S1 )
        message.addString( text )

        return message
    }
}