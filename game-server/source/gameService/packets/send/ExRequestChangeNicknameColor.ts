import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExRequestChangeNicknameColor( objectId: number ) : Buffer {
    return new DeclaredServerPacket( 7 )
            .writeC( 0xFE )
            .writeH( 0x83 )
            .writeD( objectId )
            .getBuffer()
}