import { L2ClanMember } from '../../models/L2ClanMember'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function PledgeReceivePowerInfo( member: L2ClanMember ): Buffer {
    return new DeclaredServerPacket( 11 + getStringSize( member.getName() ) )
            .writeC( 0xfe )
            .writeH( 0x3d )
            .writeD( member.getPowerGrade() ) // power grade
            .writeS( member.getName() )

            .writeD( member.getClan().getRankPrivileges( member.getPowerGrade() ) ) // privileges
            .getBuffer()
}