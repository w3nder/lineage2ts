import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function AutoAttackStart( objectId: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x25 )
            .writeD( objectId )
            .getBuffer()
}