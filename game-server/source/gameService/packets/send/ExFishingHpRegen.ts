import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExFishingHpRegen( objectId: number,
                                 time: number,
                                 hpValue: number,
                                 hpMode: number,
                                 goodUse: number,
                                 animation: number,
                                 penalty: number,
                                 hpBarColor: number ) : Buffer {
    return new DeclaredServerPacket( 23 )
            .writeC( 0xFE )
            .writeH( 0x28 )
            .writeD( objectId )
            .writeD( time )

            .writeD( hpValue )
            .writeC( hpMode ) // 0 = HP stop, 1 = HP raise
            .writeC( goodUse ) // 0 = none, 1 = success, 2 = failed
            .writeC( animation ) // Anim: 0 = none, 1 = reeling, 2 = pumping

            .writeD( penalty )
            .writeC( hpBarColor ) // 0 = normal hp bar, 1 = purple hp bar
            .getBuffer()
}