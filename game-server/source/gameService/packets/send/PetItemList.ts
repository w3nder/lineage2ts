import { L2Summon } from '../../models/actor/L2Summon'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function PetItemList( summon: L2Summon ): Buffer {
    let items: ReadonlyArray<L2ItemInstance> = summon.getInventory().getItems()

    let allItemsSize: number = items.length * ItemPacketHelper.writeItemSize()

    let packet = new DeclaredServerPacket( 3 + allItemsSize )
            .writeC( 0xB3 )
            .writeH( items.length )

    items.forEach( ( currentItem: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, currentItem )
    } )

    return packet.getBuffer()
}