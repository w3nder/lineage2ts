import { AllianceInfo } from '../../models/clan/AllianceInfo'
import { ClanInfo } from '../../models/clan/ClanInfo'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

function getAllianceSize( allianceInfo: AllianceInfo ) : number {
    let allySize = allianceInfo.allies.reduce( ( size: number, info: ClanInfo ) : number => {
        return size + 16 + getStringSize( info.clan.getName() ) + getStringSize( info.clan.getLeaderName() )
    }, 0 )

    return allySize + getStringSize( allianceInfo.name ) +
            getStringSize( allianceInfo.subordinateLeader ) +
            getStringSize( allianceInfo.superiorLeader )
}

export function AllianceInfoPacket( allianceInfo: AllianceInfo ) : Buffer {
    let packet = new DeclaredServerPacket( 13 + getAllianceSize( allianceInfo ) )
            .writeC( 0xB5 )
            .writeS( allianceInfo.name )
            .writeD( allianceInfo.total )
            .writeD( allianceInfo.online )

            .writeS( allianceInfo.subordinateLeader )
            .writeS( allianceInfo.superiorLeader )
            .writeD( allianceInfo.allies.length )

    allianceInfo.allies.forEach( ( info: ClanInfo ) => {
        packet
                .writeS( info.clan.getName() )
                .writeD( 0 )
                .writeD( info.clan.getLevel() )
                .writeS( info.clan.getLeaderName() )

                .writeD( info.total )
                .writeD( info.online )
    } )

    return packet.getBuffer()
}