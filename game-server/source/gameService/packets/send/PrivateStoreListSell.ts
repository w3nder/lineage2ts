import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { TradeItem } from '../../models/TradeItem'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function PrivateStoreListSell( buyer: L2PcInstance, seller: L2PcInstance ): Buffer {
    let items: Array<TradeItem> = seller.getSellList().getItems()
    let allItemsSize: number = items.reduce( ( size: number, item: TradeItem ): number => {
        return size + ItemPacketHelper.writeTradeItemSize( item ) + 16
    }, 0 )

    let packet = new DeclaredServerPacket( 21 + allItemsSize )
            .writeC( 0xA1 )
            .writeD( seller.getObjectId() )
            .writeD( seller.getSellList().isPackaged() ? 1 : 0 )
            .writeQ( buyer.getAdena() )
            .writeD( items.length )

    items.forEach( ( item: TradeItem ) => {
        ItemPacketHelper.writeTradeItem( packet, item )
        packet
                .writeQ( item.price )
                .writeQ( item.itemTemplate.getDoubleReferencePrice() )
    } )

    return packet.getBuffer()
}