import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum KeyPacketStatus {
    UnsupportedProtocol,
    Success
}

export function KeyPacket( key: Buffer, flag: KeyPacketStatus ): Buffer {
    let packet = new DeclaredServerPacket( 23 )
            .writeC( 0x2e )
            .writeC( flag )
            .writeB( key.subarray( 0, 8 ) )
            .writeD( 0x01 )

            .writeD( 0x01 )
            .writeC( 0x01 )
            .writeD( 0x00 )

    return packet.getBuffer()
}