import { VehiclePathPoint } from '../../models/VehiclePathPoint'
import { AirShipTeleportList } from '../../models/airship/AirShipTeleportList'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExAirShipTeleportList( list: AirShipTeleportList ) : Buffer {
    let packet = new DeclaredServerPacket( 11 + list.routes.length * 20 )
            .writeC( 0xFE )
            .writeH( 0x9a )
            .writeD( list.location )
            .writeD( list.routes.length )

    list.routes.forEach( ( path: Array<VehiclePathPoint>, index: number ) => {
        let destination = path[ path.length - 1 ]
        packet
                .writeD( index - 1 )
                .writeD( list.fuel[ index ] )
                .writeD( destination.getX() )
                .writeD( destination.getY() )
                .writeD( destination.getZ() )
    } )

    return packet.getBuffer()
}