import { L2World } from '../../L2World'
import { L2Henna } from '../../models/items/L2Henna'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PlayerHennaCache } from '../../cache/PlayerHennaCache'

export function HennaRemoveList( objectId: number ): Buffer {
    let player = L2World.getPlayer( objectId )

    let size = 3 - player.getHennaEmptySlots()
    let packet = new DeclaredServerPacket( 17 + size * 28 )
            .writeC( 0xE6 )
            .writeQ( player.getAdena() )
            .writeD( 0 )
            .writeD( size )

    PlayerHennaCache.getHennas( player.getObjectId() ).forEach( ( henna: L2Henna ) => {
        if ( !henna ) {
            return
        }

        packet
                .writeD( henna.getDyeId() )
                .writeD( henna.getDyeItemId() )
                .writeD( henna.getCancelCount() )
                .writeD( 0x00 )

                .writeD( henna.getCancelFee() )
                .writeD( 0x00 )
                .writeD( 0x01 )
    } )

    return packet.getBuffer()
}