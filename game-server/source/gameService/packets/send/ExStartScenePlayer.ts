import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExStartScenePlayer( movieId: number ) : Buffer {
    return new DeclaredServerPacket( 7 )
            .writeC( 0xFE )
            .writeH( 0x99 )
            .writeD( movieId )
            .getBuffer()
}