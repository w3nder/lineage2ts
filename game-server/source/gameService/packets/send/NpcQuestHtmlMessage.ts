import { HtmlActionScope } from '../../enums/HtmlActionScope'
import { HtmlActionCache } from '../../cache/HtmlActionCache'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function NpcQuestHtmlMessage( html: string, htmlPath: string, playerId: number, npcObjectId: number, questId: number ) : Buffer {
    let currentHtml = html

    if ( playerId !== 0 && htmlPath ) {
        currentHtml = HtmlActionCache.buildCacheWithPath( playerId, HtmlActionScope.Quest, npcObjectId, html, htmlPath )
    }

    return new DeclaredServerPacket( 11 + getStringSize( currentHtml ) )
            .writeC( 0xFE )
            .writeH( 0x8D )
            .writeD( npcObjectId )
            .writeS( currentHtml )

            .writeD( questId )
            .getBuffer()
}