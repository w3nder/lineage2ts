import { L2Party } from '../../models/L2Party'
import { L2World } from '../../L2World'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function PartyMemberPosition( party: L2Party ): Buffer {
    let players: Array<L2PcInstance> = party.getMembers().reduce( ( players: Array<L2PcInstance>, memberId: number ): Array<L2PcInstance> => {
        let player = L2World.getPlayer( memberId )
        if ( player ) {
            players.push( player )
        }

        return players
    }, [] )

    let packet = new DeclaredServerPacket( 5 + players.length * 16 )
            .writeC( 0xBA )
            .writeD( players.length )

    players.forEach( ( player: L2PcInstance ) => {
        packet
                .writeD( player.getObjectId() )
                .writeD( player.getX() )
                .writeD( player.getY() )
                .writeD( player.getZ() )
    } )

    return packet.getBuffer()
}