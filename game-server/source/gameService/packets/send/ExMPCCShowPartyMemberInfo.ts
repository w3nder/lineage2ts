import { L2World } from '../../L2World'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2Party } from '../../models/L2Party'

export function ExMPCCShowPartyMemberInfo( party: L2Party ): Buffer {
    let totalMembers = 0
    let allPlayerSize = party.getMembers().reduce( ( size: number, playerId: number ): number => {
        let member = L2World.getPlayer( playerId )
        if ( !member ) {
            return size
        }

        totalMembers++
        return size + getStringSize( member.getName() ) + 8
    }, 0 )

    let packet = new DeclaredServerPacket( 7 + allPlayerSize )
            .writeC( 0xFE )
            .writeH( 0x4B )
            .writeD( totalMembers )

    party.getMembers().forEach( ( memberId: number ) => {
        let member = L2World.getPlayer( memberId )

        if ( !member ) {
            return
        }

        packet
                .writeS( member.getName() )
                .writeD( memberId )
                .writeD( member.getClassId() )
    } )

    return packet.getBuffer()
}