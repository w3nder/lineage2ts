import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function ExAskJoinMPCC( name: string ) : Buffer {
    return new DeclaredServerPacket( 3 + getStringSize( name ) )
            .writeC( 0xFE )
            .writeH( 0x1A )
            .writeS( name )
            .getBuffer()
}