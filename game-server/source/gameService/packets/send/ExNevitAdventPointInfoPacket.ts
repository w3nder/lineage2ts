import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExNevitAdventPointInfoPacket( points: number ) : Buffer {
    return new DeclaredServerPacket( 7 )
        .writeC( 0xFE )
        .writeH( 0xDF )
        .writeD( points )
        .getBuffer()
}