import { PacketHelper } from '../PacketVariables'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

const staticReturnPacket = PacketHelper.preservePacket( new DeclaredServerPacket( 4 )
        .writeC( 0xFE )
        .writeH( 0x7C )
        .writeC( 0 )
        .getBuffer() )

const staticStartPacket = PacketHelper.preservePacket( new DeclaredServerPacket( 4 )
        .writeC( 0xFE )
        .writeH( 0x7C )
        .writeC( 2 )
        .getBuffer() )

const staticSpectatePacket = PacketHelper.preservePacket( new DeclaredServerPacket( 4 )
        .writeC( 0xFE )
        .writeH( 0x7C )
        .writeC( 3 )
        .getBuffer() )

export const enum ExOlympiadModeType {
    Exit = 0,
    Arena = 2,
    Observer = 3
}

export function ExOlympiadMode( mode: ExOlympiadModeType ): Buffer {
    switch ( mode ) {
        case ExOlympiadModeType.Exit:
            return PacketHelper.copyPacket( staticReturnPacket )

        case ExOlympiadModeType.Arena:
            return PacketHelper.copyPacket( staticStartPacket )

        case ExOlympiadModeType.Observer:
            return PacketHelper.copyPacket( staticSpectatePacket )
    }

    return new DeclaredServerPacket( 4 )
            .writeC( 0xFE )
            .writeH( 0x7C )
            .writeC( mode )
            .getBuffer()
}
