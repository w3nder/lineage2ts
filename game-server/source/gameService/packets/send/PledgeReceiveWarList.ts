import { L2Clan } from '../../models/L2Clan'
import { ClanCache } from '../../cache/ClanCache'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export const enum PledgeReceiveStatus {
    Declared,
    UnderAttack
}

export function PledgeReceiveWarList( clan: L2Clan, value: PledgeReceiveStatus ): Buffer {
    let clanIds: Set<number> = value === 0 ? clan.getWarClanIds() : clan.getAttackClanIds()

    let clans: Array<L2Clan> = []
    let allClansSize = 0

    clanIds.forEach( ( clanId: number ) => {
        let clan: L2Clan = ClanCache.getClan( clanId )
        if ( !clan ) {
            return
        }

        clans.push( clan )
        allClansSize += getStringSize( clan.getName() ) + 8
    } )

    let packet = new DeclaredServerPacket( 15 + allClansSize )
            .writeC( 0xfe )
            .writeH( 0x3f )
            .writeD( value )
            .writeD( 0x00 ) // page
            .writeD( clans.length )

    clans.forEach( ( clan: L2Clan ) => {
        packet
                .writeS( clan.getName() )
                .writeD( value )
                .writeD( value )
    } )

    return packet.getBuffer()
}