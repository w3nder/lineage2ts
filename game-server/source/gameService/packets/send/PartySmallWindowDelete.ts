import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function PartySmallWindowDelete( objectId: number, name: string ) : Buffer {
    return new DeclaredServerPacket( 5 + getStringSize( name ) )
        .writeC( 0x51 )
        .writeD( objectId )
        .writeS( name )
        .getBuffer()
}