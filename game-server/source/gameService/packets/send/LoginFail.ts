import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum LoginFailReason {
    NoReasonSpecified,
    SystemErrorLoginLater,
    PasswordDoesNotMatchThisAccount,
    PasswordDoesNotMatchThisAccount2,
    AccessFailedTryLater,
    IncorrectAccountInfoContactCustomerSupport,
    AccessFailedTryLater2,
    AccountAlreadyInUse,
    AccessFailedTryLater3,
    AccessFailedTryLater4,
    AccessFailedTryLater5,
}

export function LoginFail( reason: LoginFailReason ): Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x0A )
            .writeD( reason )
            .getBuffer()
}