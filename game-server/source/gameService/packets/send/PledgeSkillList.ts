import { L2Clan } from '../../models/L2Clan'
import { Skill } from '../../models/Skill'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import _ from 'lodash'
import { ClanPledge } from '../../models/ClanPledge'

export function PledgeSkillList( clan: L2Clan ): Buffer {
    let skills: Array<Skill> = _.values( clan.getSkills() )
    let subSkillsAmount : number = _.reduce( clan.subPledges, ( total: number, pledge: ClanPledge ) : number => {
        return total + pledge.skills.length
    }, _.size( clan.subPledgeSkills ) )

    let packet = new DeclaredServerPacket( 11 + skills.length * 8 + subSkillsAmount * 12 )
            .writeC( 0xFE )
            .writeH( 0x3A )
            .writeD( skills.length )
            .writeD( subSkillsAmount )

    skills.forEach( ( skill: Skill ) => {
        packet
                .writeD( skill.getDisplayId() )
                .writeD( skill.getDisplayLevel() )
    } )

    _.each( clan.subPledgeSkills, ( skill: Skill ) => {
        packet
                .writeD( 0 )
                .writeD( skill.getId() )
                .writeD( skill.getLevel() )
    } )

    _.each( clan.subPledges, ( pledge: ClanPledge ) => {
        pledge.skills.forEach( ( skill: Skill ) => {
            packet
                    .writeD( pledge.type )
                    .writeD( skill.getId() )
                    .writeD( skill.getLevel() )
        } )
    } )

    return packet.getBuffer()
}