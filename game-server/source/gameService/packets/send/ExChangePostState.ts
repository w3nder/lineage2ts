import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { ChangePostState } from '../../enums/ChangePostState'

export function ExChangePostState( isForReceiver: boolean, messageIds: Array<number>, statusId: ChangePostState ): Buffer {
    let packet = new DeclaredServerPacket( 11 + messageIds.length * 8 )
            .writeC( 0xFE )
            .writeH( 0xB3 )
            .writeD( isForReceiver ? 1 : 0 )
            .writeD( messageIds.length )

    messageIds.forEach( ( messageId: number ) => {
        packet
                .writeD( messageId )
                .writeD( statusId )
    } )

    return packet.getBuffer()
}

export function ExChangePostStateSingle( isForReceiver: boolean, messageId: number, statusId: ChangePostState ): Buffer {
    let packet = new DeclaredServerPacket( 11 + 8 )
        .writeC( 0xFE )
        .writeH( 0xB3 )
        .writeD( isForReceiver ? 1 : 0 )
        .writeD( 1 )

        .writeD( messageId )
        .writeD( statusId )

    return packet.getBuffer()
}