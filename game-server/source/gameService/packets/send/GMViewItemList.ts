import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function GMViewItemList( player: L2PcInstance ): Buffer {
    let allItemsSize: number = player.getInventory().getItems().length * ItemPacketHelper.writeItemSize()

    let packet = new DeclaredServerPacket( 9 + getStringSize( player.getName() ) + allItemsSize )
            .writeC( 0x9A )
            .writeS( player.getName() )
            .writeD( player.getInventoryLimit() )
            .writeH( 0x01 )

            .writeH( player.getInventory().getSize() )

    player.getInventory().getItems().forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
    } )

    return packet.getBuffer()
}