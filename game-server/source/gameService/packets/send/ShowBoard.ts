import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import _ from 'lodash'
import { PacketHelper } from '../PacketVariables'

const ids: Array<string> = [
    'bypass _bbshome', // top
    'bypass _bbsgetfav', // favorite
    'bypass _bbsloc', // region
    'bypass _bbsclan', // clan
    'bypass _bbsmemo', // memo
    'bypass _bbsmail', // mail
    'bypass _bbsfriends', // friends
    'bypass bbs_add_fav', // add fav.
]

const dynamicPacket = new DeclaredServerPacket( _.sum( _.map( ids, getStringSize ) ) )

ids.forEach( ( id: string ) => {
    dynamicPacket.writeS( id )
} )

const createdData = dynamicPacket.getBuffer()
const separator = String.fromCharCode( 8 )

const staticData = PacketHelper.preservePacket( createdData )

export function ShowBoardPacket( content: string ): Buffer {
    return new DeclaredServerPacket( 2 + staticData.length + getStringSize( content ) )
            .writeC( 0x7B )
            .writeC( 1 ) // c4 1 to show community 00 to hide
            .writeB( staticData )
            .writeS( content )
            .getBuffer()
}

export function ShowBoard( html: string, id: string ): Buffer {
    return ShowBoardPacket( id + separator + html )
}

export function ShowBoardArray( lines: Array<string> ): Buffer {
    return ShowBoardPacket( '1002' + separator + lines.join( separator ) + separator )
}