import { L2Clan } from '../../models/L2Clan'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function GMViewWarehouseWithdrawList( player: L2PcInstance ): Buffer {
    let warehouse = player.getWarehouse()
    return createPacket( warehouse.getItems(), player.getName(), warehouse.getAdenaAmount() )
}

export function GMViewWarehouseWithdrawListForClan( clan: L2Clan ): Buffer {
    return createPacket( clan.getWarehouse().getItems(), clan.getLeaderName(), clan.getWarehouse().getAdenaAmount() )
}

function createPacket( items: ReadonlyArray<L2ItemInstance>, name: string, adena: number ): Buffer {
    let allItemsSize: number = items.length * ( ItemPacketHelper.writeItemSize() + 4 )

    let packet = new DeclaredServerPacket( 11 + getStringSize( name ) + allItemsSize )
            .writeC( 0x9B )
            .writeS( name )
            .writeQ( adena )
            .writeH( items.length )

    items.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
        packet.writeD( item.getObjectId() )
    } )

    return packet.getBuffer()
}