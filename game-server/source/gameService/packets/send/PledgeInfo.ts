import { L2Clan } from '../../models/L2Clan'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function PledgeInfo( clan: L2Clan ) : Buffer {
    return new DeclaredServerPacket( 5 + getStringSize( clan.getName() ) + getStringSize( clan.getAllyName() ) )
            .writeC( 0x89 )
            .writeD( clan.getId() )
            .writeS( clan.getName() )
            .writeS( clan.getAllyName() )
            .getBuffer()
}