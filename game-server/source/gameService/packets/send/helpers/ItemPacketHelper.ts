import { IServerPacket } from '../../../../packets/IServerPacket'
import { L2ItemInstance } from '../../../models/items/instance/L2ItemInstance'
import { ItemInfo } from '../../../models/ItemInfo'
import { TradeItem } from '../../../models/TradeItem'
import { PcInventory } from '../../../models/itemcontainer/PcInventory'
import _ from 'lodash'
import { DefaultEnchantOptions } from '../../../models/options/EnchantOptions'
import { ElementalAndEnchantProperties } from '../../../models/items/ElementalAndEnchantProperties'

export const ItemPacketHelper = {
    writeItem( packet: IServerPacket, item: L2ItemInstance ) {
        packet
                .writeD( item.getObjectId() )
                .writeD( item.getId() ) // ItemId
                .writeD( item.getLocationSlot() ) // T1
                .writeQ( item.getCount() ) // Quantity

                .writeH( item.getItem().getType2() ) // Item Type 2 : 00-weapon, 01-shield/armor, 02-ring/earring/necklace, 03-questitem, 04-adena, 05-item
                .writeH( item.getCustomType1() ) // Filler (always 0)
                .writeH( item.isEquipped() ? 1 : 0 ) // Equipped : 00-No, 01-yes
                .writeD( item.getItem().getBodyPart() ) // Slot : 0006-lr.ear, 0008-neck, 0030-lr.finger, 0040-head, 0100-l.hand, 0200-gloves, 0400-chest, 0800-pants, 1000-feet, 4000-r.hand, 8000-r.hand

                .writeH( item.getEnchantLevel() ) // Enchant level (pet level shown in control item)
                .writeH( item.getCustomType2() ) // Pet name exists or not shown in control item
                .writeD( item.isAugmented() ? item.getAugmentation().getAugmentationId() : 0 )
                .writeD( item.getMana() )

                .writeD( item.getRemainingExpirationSeconds() )

        ItemPacketHelper.writeItemElementalAndEnchant( packet, item )
    },

    writeTradeItem( packet: IServerPacket, item: TradeItem ) {
        packet
                .writeD( item.objectId )
                .writeD( item.itemTemplate.getId() ) // ItemId
                .writeD( item.location ) // T1
                .writeQ( item.count ) // Quantity

                .writeH( item.itemTemplate.getType2() ) // Item Type 2 : 00-weapon, 01-shield/armor, 02-ring/earring/necklace, 03-questitem, 04-adena, 05-item
                .writeH( item.type1 ) // Filler (always 0)
                .writeH( 0 ) // Equipped : 00-No, 01-yes
                .writeD( item.itemTemplate.getBodyPart() ) // Slot : 0006-lr.ear, 0008-neck, 0030-lr.finger, 0040-head, 0100-l.hand, 0200-gloves, 0400-chest, 0800-pants, 1000-feet, 4000-r.hand, 8000-r.hand

                .writeH( item.enchant ) // Enchant level (pet level shown in control item)
                .writeH( item.type2 ) // Pet name exists or not shown in control item
                .writeD( 0 )
                .writeD( -1 )

                .writeD( -9999 )
                .writeH( item.elementaryAttackType )
                .writeH( item.elementaryAttackPower )

        _.times( 6, ( index: number ) => {
            packet.writeH( item.elementalDefenceAttributes[ index ] )
        } )

        _.each( item.enchantOptions, ( option: number ) => {
            packet.writeH( option )
        } )
    },

    writeGeneralItem( packet: IServerPacket, item: ItemInfo ) {
        packet
                .writeD( item.getObjectId() )
                .writeD( item.getItem().getId() ) // ItemId
                .writeD( item.getLocation() ) // T1
                .writeQ( item.getCount() ) // Quantity

                .writeH( item.getItem().getType2() ) // Item Type 2 : 00-weapon, 01-shield/armor, 02-ring/earring/necklace, 03-questitem, 04-adena, 05-item
                .writeH( item.getCustomType1() ) // Filler (always 0)
                .writeH( item.getEquipped() ) // Equipped : 00-No, 01-yes
                .writeD( item.getItem().getBodyPart() ) // Slot : 0006-lr.ear, 0008-neck, 0030-lr.finger, 0040-head, 0100-l.hand, 0200-gloves, 0400-chest, 0800-pants, 1000-feet, 4000-r.hand, 8000-r.hand

                .writeH( item.getEnchant() ) // Enchant level (pet level shown in control item)
                .writeH( item.getCustomType2() ) // Pet name exists or not shown in control item
                .writeD( item.getAugmentationBonus() )
                .writeD( item.getMana() )

                .writeD( item.getTime() )

        ItemPacketHelper.writeItemElementalAndEnchant( packet, item )
    },

    writeItemSize(): number {
        return 46 + this.writeItemElementalAndEnchantSize()
    },

    writeTradeItemSize( item: TradeItem ): number {
        return 62 + item.enchantOptions.length * 2
    },

    writeItemElementalAndEnchantSize(): number {
        return 16 + DefaultEnchantOptions.length * 2
    },

    writeItemElementalAndEnchant( packet: IServerPacket, item: ElementalAndEnchantProperties ) {
        packet
                .writeH( item.getAttackElementType() )
                .writeH( item.getAttackElementPower() )

        for ( const value of item.elementalDefenceAttributes ) {
            packet.writeH( value )
        }

        item.getEnchantOptions().forEach( ( option: number ) => {
            packet.writeH( option )
        } )
    },

    writeInventoryBlock( packet: IServerPacket, inventory: PcInventory ) {
        if ( inventory.hasInventoryBlock() ) {
            packet
                    .writeH( inventory.getBlockItems().length )
                    .writeC( inventory.getBlockMode() )

            inventory.getBlockItems().forEach( ( item: number ) => {
                packet.writeD( item )
            } )
            return
        }

        packet.writeH( 0x00 )
    },

    writeInventoryBlockSize( inventory: PcInventory ): number {
        if ( !inventory.hasInventoryBlock() ) {
            return 2
        }

        return 3 + 4 * _.size( inventory.getBlockItems() )
    },
}