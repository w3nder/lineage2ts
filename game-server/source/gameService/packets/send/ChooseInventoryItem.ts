import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ChooseInventoryItem( itemId: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x7C )
            .writeD( itemId )
            .getBuffer()
}