import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { PacketHelper } from '../PacketVariables'

const staticPacket: Buffer = PacketHelper.preservePacket( new DeclaredServerPacket( 3 )
        .writeC( 0xFE )
        .writeH( 0x13 )
        .getBuffer() )

export function ExCloseMPCC(): Buffer {
    return PacketHelper.copyPacket( staticPacket )
}