import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function RecipeShopMessage( player: L2PcInstance ): Buffer {
    return new DeclaredServerPacket( 5 + getStringSize( player.getStoreName() ) )
            .writeC( 0xE1 )
            .writeD( player.getObjectId() )
            .writeS( player.getStoreName() )
            .getBuffer()
}