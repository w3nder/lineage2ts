import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExRpItemLink( item: L2ItemInstance ): Buffer {
    let packet = new DeclaredServerPacket( 65 + item.getEnchantOptions().length * 2 )
            .writeC( 0xFE )
            .writeH( 0x6C )
            .writeD( item.getObjectId() )
            .writeD( item.getId() )

            .writeD( item.getLocationSlot() )
            .writeQ( item.getCount() )
            .writeH( item.getItem().getType2() )
            .writeH( item.getCustomType1() )

            .writeH( item.isEquipped() ? 0x01 : 0x00 )
            .writeD( item.getItem().getBodyPart() )
            .writeH( item.getEnchantLevel() )
            .writeH( item.getCustomType2() )

    if ( item.isAugmented() ) {
        packet.writeD( item.getAugmentation().getAugmentationId() )
    } else {
        packet.writeD( 0x00 )
    }

    packet
            .writeD( item.getMana() )
            .writeD( item.isTimeLimitedItem() ? ( item.getRemainingTime() / 1000 ) : -9999 )
            .writeH( item.getAttackElementType() )
            .writeH( item.getAttackElementPower() )

    for ( const value of item.elementalDefenceAttributes ) {
        packet.writeH( value )
    }

    item.getEnchantOptions().forEach( ( option: number ) => {
        packet.writeH( option )
    } )

    return packet.getBuffer()
}