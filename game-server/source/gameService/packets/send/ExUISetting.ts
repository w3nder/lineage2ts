import { ActionKey } from '../../models/ActionKey'
import { PlayerUICache } from '../../cache/PlayerUICache'
import { UICategories, UIKeys } from '../../../data/interface/UIDataApi'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import _ from 'lodash'

export function ExUISetting( objectId: number ): Buffer {
    if ( !PlayerUICache.hasUIData( objectId ) ) {
        return
    }

    let categoryIndex = 0
    let uiCategories: UICategories = PlayerUICache.getCategories( objectId )
    let uiKeys: UIKeys = PlayerUICache.getKeys( objectId )
    let totalKeys: number = _.size( uiKeys )
    let keyPortionSize = 0

    _.times( totalKeys, ( index: number ) => {
        keyPortionSize += 6

        if ( uiCategories[ categoryIndex ] ) {
            keyPortionSize += uiCategories[ categoryIndex ].length
        }

        categoryIndex++

        if ( uiCategories[ categoryIndex ] ) {
            keyPortionSize += uiCategories[ categoryIndex ].length
        }

        categoryIndex++

        if ( uiKeys[ index ] ) {
            keyPortionSize += ( 20 * uiKeys[ index ].length )
            return
        }
    } )

    let packet = new DeclaredServerPacket( 23 + keyPortionSize )
            .writeC( 0xFE )
            .writeH( 0x70 )
            .writeD( keyPortionSize + 16 )
            .writeD( categoryIndex )
            .writeD( totalKeys )

    categoryIndex = 0
    _.times( totalKeys, ( index: number ) => {
        if ( uiCategories[ categoryIndex ] ) {
            let values: Array<number> = uiCategories[ categoryIndex ]
            packet.writeC( values.length )
            values.forEach( ( item: number ) => {
                packet.writeC( item )
            } )
        } else {
            packet.writeC( 0 )
        }

        categoryIndex++

        if ( uiCategories[ categoryIndex ] ) {
            let values: Array<number> = uiCategories[ categoryIndex ]
            packet.writeC( values.length )
            values.forEach( ( item: number ) => {
                packet.writeC( item )
            } )
        } else {
            packet.writeC( 0 )
        }

        categoryIndex++

        if ( uiKeys[ index ] ) {
            let list: Array<ActionKey> = uiKeys[ index ]
            packet.writeD( list.length )

            list.forEach( ( key: ActionKey ) => {
                packet
                        .writeD( key.commandId )
                        .writeD( key.key )
                        .writeD( key.toggleKeyOne )
                        .writeD( key.toggleKeyTwo )
                        .writeD( key.visibleStatus )
            } )

            return
        }

        packet.writeD( 0 )
    } )

    packet
            .writeD( 0x11 )
            .writeD( 0x10 )

    return packet.getBuffer()
}