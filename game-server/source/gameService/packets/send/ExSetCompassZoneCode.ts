import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum ExSetCompassZoneType {
    Altered = 0x08,
    WarZoneStart = 0x0A,
    WarZoneEnd = 0x0B,
    Peace = 0x0C,
    SevenSigns = 0x0D,
    Pvp = 0x0E,
    General = 0x0F,
}

export function ExSetCompassZoneCode( type: ExSetCompassZoneType ): Buffer {
    return new DeclaredServerPacket( 7 )
            .writeC( 0xFE )
            .writeH( 0x33 )
            .writeD( type )
            .getBuffer()
}