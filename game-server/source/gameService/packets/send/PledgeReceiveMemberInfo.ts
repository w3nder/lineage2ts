import { L2ClanMember } from '../../models/L2ClanMember'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function PledgeReceiveMemberInfo( member: L2ClanMember ): Buffer {
    let clanName = member.getPledgeType() !== 0 ? ( member.getClan().getPledge( member.getPledgeType() ) ).name : member.getClan().getName()
    let packet = new DeclaredServerPacket( 11 + getStringSize( member.getName() ) + getStringSize( member.getTitle() ) + getStringSize( clanName ) + getStringSize( member.getApprenticeOrSponsorName() ) )
            .writeC( 0xfe )
            .writeH( 0x3e )
            .writeD( member.getPledgeType() )
            .writeS( member.getName() )

            .writeS( member.getTitle() )
            .writeD( member.getPowerGrade() )
            .writeS( clanName )
            .writeS( member.getApprenticeOrSponsorName() )

    return packet.getBuffer()
}