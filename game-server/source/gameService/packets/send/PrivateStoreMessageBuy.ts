import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function PrivateStoreMessageBuy( player: L2PcInstance ): Buffer {
    let storeMessage = player ? player.getBuyList().getTitle() : ''

    return new DeclaredServerPacket( 5 + getStringSize( storeMessage ) )
            .writeC( 0xBF )
            .writeD( player.getObjectId() )
            .writeS( storeMessage )
            .getBuffer()
}