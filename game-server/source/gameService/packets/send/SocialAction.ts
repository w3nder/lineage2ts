import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum SocialActionType {
    Congratulate = 3,
    Bow = 7,
    CursedWeaponAcquired = 17,
    HeroClaimed = 20016,
    LevelUp = 2122
}

export function SocialAction( objectId: number, actionId: number ): Buffer {
    return new DeclaredServerPacket( 9 )
            .writeC( 0x27 )
            .writeD( objectId )
            .writeD( actionId )
            .getBuffer()
}