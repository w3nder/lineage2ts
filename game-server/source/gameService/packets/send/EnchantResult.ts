import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum EnchantResultOutcome {
    Success = 0,
    Failure = 1,
    Cancelled = 2,

    BlessedFailure = 3,
    UnrecoverableFailure = 4,
    SafeFailure = 5
}

export function EnchantResult( result: EnchantResultOutcome, crystalId: number, amount: number ) : Buffer {
    return new DeclaredServerPacket( 17 )
            .writeC( 0x87 )
            .writeD( result )
            .writeD( crystalId )
            .writeQ( amount )
            .getBuffer()
}