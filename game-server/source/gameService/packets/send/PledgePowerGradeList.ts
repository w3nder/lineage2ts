import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { RankPrivileges } from '../../models/RankPrivileges'

export function PledgePowerGradeList( privileges: Array<RankPrivileges> ): Buffer {
    let packet = new DeclaredServerPacket( 7 + privileges.length * 8 )
            .writeC( 0xFE )
            .writeH( 0x3C )
            .writeD( privileges.length )

    privileges.forEach( ( privilege: RankPrivileges ) => {
        packet
                .writeD( privilege.rank )
                .writeD( 0 ) // TODO : what is this value?
    } )

    return packet.getBuffer()
}