import { PartyMatchWaitingList } from '../../cache/PartyMatchWaitingList'
import { L2World } from '../../L2World'
import { InstanceManager, PlayerInstanceTime } from '../../instancemanager/InstanceManager'
import aigle from 'aigle'
import _ from 'lodash'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { RespawnRegionCache } from '../../cache/RespawnRegionCache'

const pageSize = 64

export async function ExListPartyMatchingWaitingRoom( page: number,
        minimumLevel: number,
        maximumLevel: number,
        classes: Array<number>,
        nameFilter: string ): Promise<Buffer> {

    let matchingPlayers: Array<number> = PartyMatchWaitingList.findPlayers( minimumLevel, maximumLevel, classes, nameFilter )
    let selectedPlayers: Array<number> = _.take( _.drop( matchingPlayers, ( page - 1 ) * pageSize ), page * pageSize )
    let players : Map<L2PcInstance, PlayerInstanceTime> = new Map<L2PcInstance, PlayerInstanceTime>()
    let packetSize = 0

    await aigle.eachSeries( selectedPlayers, async ( playerId: number ) => {
        let player = L2World.getPlayer( playerId )
        let instances: PlayerInstanceTime = await InstanceManager.getAllInstanceTimes( playerId )

        players.set( player, instances )

        packetSize = packetSize + getStringSize( player.getName() ) + 16 + 4 * instances.size
    } )

    let packet = new DeclaredServerPacket( 11 + packetSize )
            .writeC( 0xFE )
            .writeH( 0x36 )
            .writeD( matchingPlayers.length )
            .writeD( selectedPlayers.length )

    players.forEach( ( instances: PlayerInstanceTime, player : L2PcInstance ) => {
        packet
                .writeS( player.getName() )
                .writeD( player.getActiveClass() )
                .writeD( player.getLevel() )
                .writeD( RespawnRegionCache.getRespawnPoint( player ).bbs )

                .writeD( instances.size )

        instances.forEach( ( time: number, instanceId: number ) => {
            packet.writeD( instanceId )
        } )
    } )

    return packet.getBuffer()
}