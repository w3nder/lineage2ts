import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function DeleteObject( objectId: number ) : Buffer {
    return new DeclaredServerPacket( 9 )
        .writeC( 0x08 )
        .writeD( objectId )
        .writeD( 0x00 )
        .getBuffer()
}