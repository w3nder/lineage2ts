import { L2Seed } from '../../models/L2Seed'
import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { SeedProduction } from '../../models/manor/SeedProduction'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExShowSeedSetting( manorId: number ): Buffer {
    let allSeeds: Array<L2Seed> = CastleManorManager.getSeedsForCastle( manorId )

    let packet = new DeclaredServerPacket( 11 + allSeeds.length * 66 )
            .writeC( 0xFE )
            .writeH( 0x26 )
            .writeD( manorId )
            .writeD( allSeeds.length )

    allSeeds.forEach( ( seed: L2Seed ) => {
        packet
                .writeD( seed.getSeedId() ) // seed id
                .writeD( seed.getLevel() ) // level
                .writeC( 1 )
                .writeD( seed.getReward( 1 ) ) // reward 1 id

                .writeC( 1 )
                .writeD( seed.getReward( 2 ) ) // reward 2 id
                .writeD( seed.getSeedLimit() ) // next sale limit
                .writeD( seed.getSeedReferencePrice() ) // price for castle to produce 1

                .writeD( seed.getSeedMinPrice() ) // min seed price
                .writeD( seed.getSeedMaxPrice() ) // max seed price

        let currentProduction: SeedProduction = CastleManorManager.getSeedProduct( manorId, seed.getSeedId(), false )
        packet
                .writeQ( currentProduction ? currentProduction.getStartAmount() : 0 )
                .writeQ( currentProduction ? currentProduction.getPrice() : 0 )

        let nextProduction: SeedProduction = CastleManorManager.getSeedProduct( manorId, seed.getSeedId(), true )
        packet
                .writeQ( nextProduction ? nextProduction.getStartAmount() : 0 )
                .writeQ( nextProduction ? nextProduction.getPrice() : 0 )
    } )

    return packet.getBuffer()
}