import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExVitalityPointInfo( points: number ) : Buffer {
    return new DeclaredServerPacket( 7 )
        .writeC( 0xFE )
        .writeH( 0xA0 )
        .writeD( points )
        .getBuffer()
}