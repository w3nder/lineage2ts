import { PacketHelper } from '../PacketVariables'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

const staticPacket: Buffer = PacketHelper.preservePacket( new DeclaredServerPacket( 1 ).writeC( 0x94 ).getBuffer() )

export function AcquireSkillDone(): Buffer {
    return PacketHelper.copyPacket( staticPacket )
}