import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function RecipeShopItemInfo( player: L2PcInstance, recipeId: number ) : Buffer {
    return new DeclaredServerPacket( 21 )
            .writeC( 0xE0 )
            .writeD( player.getObjectId() )
            .writeD( recipeId )
            .writeD( player.getCurrentMp() )

            .writeD( player.getMaxMp() )
            .writeD( 0xFFFFFFFF )
            .getBuffer()
}