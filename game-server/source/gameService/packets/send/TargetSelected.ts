import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function TargetSelected( player: L2PcInstance, targetId: number ) : Buffer {
    return new DeclaredServerPacket( 25 )
            .writeC( 0x23 )
            .writeD( player.getObjectId() )
            .writeD( targetId )
            .writeD( player.getX() )

            .writeD( player.getY() )
            .writeD( player.getZ() )
            .writeD( 0 )
            .getBuffer()
}