import { InstanceType } from '../../enums/InstanceType'
import { L2ControllableAirShipInstance } from '../../models/actor/instance/L2ControllableAirShipInstance'
import { L2Character } from '../../models/actor/L2Character'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function MyTargetSelectedWithCharacters( player: L2PcInstance, target: L2Character ) : Buffer {
    let objectId = target.isInstanceType( InstanceType.L2ControllableAirShipInstance ) ? ( target as L2ControllableAirShipInstance ).getHelmObjectId() : target.getObjectId()
    let color = target.isAutoAttackable( player ) ? ( player.getLevel() - target.getLevel() ) : 0

    return new DeclaredServerPacket( 11 )
            .writeC( 0xB9 )
            .writeD( objectId )
            .writeH( color )
            .writeD( 0 )
            .getBuffer()
}