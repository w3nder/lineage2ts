import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExCubeGameExtendedChangePoints( timeLeft: number,
                                               bluePoints: number,
                                               redPoints: number,
                                               isRedTeam: boolean,
                                               playerId: number,
                                               playerPoints: number ) : Buffer {

    return new DeclaredServerPacket( 31 )
        .writeC( 0xfe )
        .writeH( 0x98 )
        .writeD( 0x00 )
        .writeD( timeLeft )

        .writeD( bluePoints )
        .writeD( redPoints )
        .writeD( isRedTeam ? 0x01 : 0x00 )
        .writeD( playerId )

        .writeD( playerPoints )
        .getBuffer()
}