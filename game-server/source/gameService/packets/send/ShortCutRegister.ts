import { ShortcutType } from '../../enums/ShortcutType'
import { L2PlayerShortcutsTableItem } from '../../../database/interface/CharacterShortcutsTableApi'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

function getSize( type: ShortcutType ) : number {
    switch ( type ) {
        case ShortcutType.ITEM:
            return 24

        case ShortcutType.SKILL:
            return 13

        case ShortcutType.ACTION:
        case ShortcutType.MACRO:
        case ShortcutType.RECIPE:
        case ShortcutType.BOOKMARK:
            return 8
    }

    return 0
}

export function ShortCutRegister( data: L2PlayerShortcutsTableItem ): Buffer {
    let packet = new DeclaredServerPacket( 9 + getSize( data.type ) )
            .writeC( 0x44 )
            .writeD( data.type )
            .writeD( data.index )

    switch ( data.type ) {
        case ShortcutType.ITEM:
            packet
                    .writeD( data.id )
                    .writeD( data.characterType )
                    .writeD( data.itemReuseGroup )
                    .writeD( 0x00 )
                    .writeD( 0x00 )
                    .writeD( 0x00 )
            break

        case ShortcutType.SKILL:
            packet
                    .writeD( data.id )
                    .writeD( data.level )
                    .writeC( 0x00 )
                    .writeD( data.characterType )
            break

        case ShortcutType.ACTION:
        case ShortcutType.MACRO:
        case ShortcutType.RECIPE:
        case ShortcutType.BOOKMARK:
            packet
                    .writeD( data.id )
                    .writeD( data.characterType )
    }

    return packet.getBuffer()
}