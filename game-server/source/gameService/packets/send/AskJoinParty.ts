import { PartyDistributionType } from '../../enums/PartyDistributionType'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

export function AskJoinParty( name: string, type: PartyDistributionType ) : Buffer {
    return new DeclaredServerPacket( 5 + getStringSize( name ) )
            .writeC( 0x39 )
            .writeS( name )
            .writeD( type )
            .getBuffer()
}