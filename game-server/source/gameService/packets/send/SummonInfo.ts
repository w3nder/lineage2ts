import { L2Summon } from '../../models/actor/L2Summon'
import { L2World } from '../../L2World'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { AbnormalVisualEffect, AbnormalVisualEffectMap } from '../../models/skills/AbnormalVisualEffect'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { PetInfoAnimation } from './PetInfo'
import { AreaType } from '../../models/areas/AreaType'

const stealthEffect = AbnormalVisualEffectMap[ AbnormalVisualEffect.STEALTH ].mask
export function SummonInfo( whoIsSending: number, summon: L2Summon, status: PetInfoAnimation ): Buffer {

    let gmSeeInvisibility = false
    let otherPlayer = L2World.getPlayer( whoIsSending )

    if ( summon.isInvisible() ) {
        gmSeeInvisibility = otherPlayer && otherPlayer.hasActionOverride( PlayerActionOverride.SeeInvisible )
    }

    let moveMultiplier = summon.getMovementSpeedMultiplier()
    let runSpeed = Math.round( summon.getRunSpeed() / moveMultiplier )
    let walkSpeed = Math.round( summon.getWalkSpeed() / moveMultiplier )
    let swimRunSpeed = Math.round( summon.getSwimRunSpeed() / moveMultiplier )
    let swimWalkSpeed = Math.round( summon.getSwimWalkSpeed() / moveMultiplier )
    let flyRunSpeed = summon.isFlying() ? runSpeed : 0
    let flyWalkSpeed = summon.isFlying() ? walkSpeed : 0

    let title = summon.getOwner() && summon.getOwner().isOnline() ? summon.getOwner().getName() : ''
    return new DeclaredServerPacket( 171 + getStringSize( summon.getName() ) + getStringSize( title ) )
            .writeC( 0x0c )
            .writeC( summon.getObjectId() )
            .writeD( summon.getTemplate().infoId )
            .writeD( summon.isAutoAttackable( otherPlayer ) ? 1 : 0 )

            .writeD( summon.getX() )
            .writeD( summon.getY() )
            .writeD( summon.getZ() )
            .writeD( summon.getHeading() )

            .writeD( 0x00 )
            .writeD( summon.getMagicAttackSpeed() )
            .writeD( summon.getPowerAttackSpeed() )
            .writeD( runSpeed )

            .writeD( walkSpeed )
            .writeD( swimRunSpeed )
            .writeD( swimWalkSpeed )
            .writeD( flyRunSpeed )

            .writeD( flyWalkSpeed )
            .writeD( flyRunSpeed )
            .writeD( flyWalkSpeed )
            .writeF( moveMultiplier )

            .writeF( summon.getAttackSpeedMultiplier() )
            .writeF( summon.getTemplate().getFCollisionRadius() )
            .writeF( summon.getTemplate().getFCollisionHeight() )
            .writeD( summon.getWeapon() ) // right hand weapon

            .writeD( summon.getArmor() )
            .writeD( 0 ) // left hand weapon
            .writeC( 0x01 )
            .writeC( 0x01 ) // always running 1=running 0=walking

            .writeC( summon.isInCombat() ? 1 : 0 )
            .writeC( summon.isAlikeDead() ? 1 : 0 )
            .writeC( status ) // invisible ?? 0=false 1=true 2=summoned (only works if model has a summon animation)
            .writeD( -1 )

            .writeS( summon.getName() )
            .writeD( -1 )
            .writeS( title )
            .writeD( 0x01 ) // Title color 0=client default

            .writeD( summon.getPvpFlag() )
            .writeD( summon.getKarma() )
            .writeD( gmSeeInvisibility ? summon.getAbnormalVisualEffects() | stealthEffect : summon.getAbnormalVisualEffects() )
            .writeD( 0x00 ) // clan id

            .writeD( 0x00 ) // crest id
            .writeD( 0x00 )
            .writeD( 0x00 )
            .writeC( summon.isInArea( AreaType.Water ) ? 1 : summon.isFlying() ? 2 : 0 )

            .writeC( summon.getTeam() )
            .writeF( summon.getTemplate().getFCollisionRadius() )
            .writeF( summon.getTemplate().getFCollisionHeight() )
            .writeD( summon.getTemplate().getWeaponEnchant() )

            .writeD( 0x00 )
            .writeD( 0x00 )
            .writeD( summon.getFormId() )
            .writeC( 0x01 )

            .writeC( 0x01 )
            .writeD( summon.getAbnormalVisualEffectSpecial() )
            .getBuffer()
}