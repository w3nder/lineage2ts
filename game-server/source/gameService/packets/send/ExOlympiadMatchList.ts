import { OlympiadGameManager } from '../../models/olympiad/OlympiadGameManager'
import { OlympiadGameTask } from '../../models/olympiad/OlympiadGameTask'
import { AbstractOlympiadGame } from '../../models/olympiad/AbstractOlympiadGame'
import { DeclaredServerPacket, getStringArraySize } from '../../../packets/DeclaredServerPacket'

export function ExOlympiadMatchList(): Buffer {
    let games: Array<OlympiadGameTask> = OlympiadGameManager.getAllTasks().filter( ( task: OlympiadGameTask ) => {
        return task.isGameStarted() || !task.isBattleFinished()
    } )

    let size = games.reduce( ( total: number, task: OlympiadGameTask ): number => {
        return total + 12 + getStringArraySize( task.getGame().getPlayerNames() )
    }, 0 )

    let packet = new DeclaredServerPacket( 15 + size )
            .writeC( 0xFE )
            .writeH( 0xD4 )
            .writeD( 0 ) // Type 0 = Match List, 1 = Match Result
            .writeD( games.length )

            .writeD( 0 )

    games.forEach( ( task: OlympiadGameTask ) => {
        let game: AbstractOlympiadGame = task.getGame()
        let playerNames = game.getPlayerNames()
        packet
                .writeD( game.getStadiumId() ) // Stadium Id (Arena 1 = 0)
                .writeD( game.getGameType() )
                .writeD( task.isRunning() ? 0x02 : 0x01 ) // (1 = Standby, 2 = Playing)
                .writeS( playerNames[ 0 ] ) // Player 1 Name
                .writeS( playerNames[ 1 ] ) // Player 2 Name
    } )

    return packet.getBuffer()
}