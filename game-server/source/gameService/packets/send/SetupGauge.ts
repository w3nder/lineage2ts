import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export const enum SetupGaugeColors {
    Blue = 0,
    Red = 1,
    Cyan = 2,
    Other = 3
}

export function SetupGauge( playerId: number, color: SetupGaugeColors, currentTime: number, maximumTime: number = currentTime ): Buffer {
    return new DeclaredServerPacket( 17 )
            .writeC( 0x6b )
            .writeD( playerId )
            .writeD( color )
            .writeD( currentTime )

            .writeD( maximumTime )
            .getBuffer()
}