import { PacketHelper } from '../PacketVariables'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

const staticPacket = PacketHelper.preservePacket( new DeclaredServerPacket( 1 ).writeC( 0x50 ).getBuffer() )

export function PartySmallWindowDeleteAll(): Buffer {
    return PacketHelper.copyPacket( staticPacket )
}