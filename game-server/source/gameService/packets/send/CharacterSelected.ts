import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { GameTime } from '../../cache/GameTime'
import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'

const paddingZeros = Buffer.alloc( 80, 0 )

export function CharacterSelected( player : L2PcInstance, sessionId: number ) : Buffer {
    let dynamicSize : number = getStringSize( player.getName() )
            + getStringSize( player.getTitle() )
            + paddingZeros.length

    let packet = new DeclaredServerPacket( 125 + dynamicSize )
            .writeC( 0x0b )
            .writeS( player.getName() )
            .writeD( player.getObjectId() )
            .writeS( player.getTitle() )

            .writeD( sessionId )
            .writeD( player.getClanId() )
            .writeD( 0 )
            .writeD( player.getAppearance().getSex() ? 1 : 0 )

            .writeD( player.getRace() )
            .writeD( player.getClassId() )
            .writeD( 0x01 )
            .writeD( player.getX() )

            .writeD( player.getY() )
            .writeD( player.getZ() )
            .writeF( player.getCurrentHp() )
            .writeF( player.getCurrentMp() )

            .writeD( player.getSp() )
            .writeQ( player.getExp() )
            .writeD( player.getLevel() )
            .writeD( player.getKarma() )

            .writeD( player.getPkKills() )
            .writeD( player.getINT() )
            .writeD( player.getSTR() )
            .writeD( player.getCON() )

            .writeD( player.getMEN() )
            .writeD( player.getDEX() )
            .writeD( player.getWIT() )
            .writeD( GameTime.getTotalMinutes() )

            .writeD( 0 )
            .writeD( player.getClassId() )
            .writeB( paddingZeros )
            .writeD( 0 )

    return packet.getBuffer()
}