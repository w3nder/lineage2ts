import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { WarehouseDepositType } from '../../values/warehouseDepositType'
import { ItemPacketHelper } from './helpers/ItemPacketHelper'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function WareHouseDepositList( player: L2PcInstance, type: WarehouseDepositType ): Buffer {

    let isPrivate = type === WarehouseDepositType.Private
    let items: Array<L2ItemInstance> = player.getInventory().getAvailableItems( true, isPrivate, false, true ).filter( ( item: L2ItemInstance ) => {
        return item && item.isDepositable( isPrivate )
    } )

    let allItemsSize: number = items.length * ( ItemPacketHelper.writeItemSize() + 4 )

    let packet = new DeclaredServerPacket( 13 + allItemsSize )
            .writeC( 0x41 )
            .writeH( type )
            .writeQ( player.getAdena() )
            .writeH( items.length )

    items.forEach( ( item: L2ItemInstance ) => {
        ItemPacketHelper.writeItem( packet, item )
        packet.writeD( item.getObjectId() )
    } )

    return packet.getBuffer()
}