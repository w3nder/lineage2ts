import { DeclaredServerPacket, getStringSize } from '../../../packets/DeclaredServerPacket'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function ExOlympiadUserInfo( player : L2PcInstance ) : Buffer {
    return new DeclaredServerPacket( 28 + getStringSize( player.getName() ) )
        .writeC( 0xFE )
        .writeH( 0x7A )
        .writeC( player.getOlympiadSide() )
        .writeD( player.getObjectId() )

        .writeS( player.getName() )
        .writeD( player.getClassId() )
        .writeD( player.getCurrentHp() )
        .writeD( player.getMaxHp() )

        .writeD( player.getCurrentCp() )
        .writeD( player.getMaxCp() )
        .getBuffer()
}