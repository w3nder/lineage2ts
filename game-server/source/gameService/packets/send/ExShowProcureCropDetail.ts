import { CropProcure } from '../../models/manor/CropProcure'
import { CastleManager } from '../../instancemanager/CastleManager'
import { Castle } from '../../models/entity/Castle'
import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { DeclaredServerPacket } from '../../../packets/DeclaredServerPacket'

export function ExShowProcureCropDetail( cropId: number ): Buffer {
    let crops: Map<number, CropProcure> = CastleManager.getCastles().reduce( ( map: Map<number, CropProcure>, castle: Castle ) => {
        let item: CropProcure = CastleManorManager.getCropProcure( castle.getResidenceId(), cropId, false )

        if ( item && item.getAmount() > 0 ) {
            map.set( castle.getResidenceId(), item )
        }

        return map
    }, new Map<number, CropProcure>() )

    let size = crops.size
    let packet = new DeclaredServerPacket( 11 + size * 21 )
            .writeC( 0xFE )
            .writeH( 0x78 )
            .writeD( cropId )
            .writeD( size )

    crops.forEach( ( crop: CropProcure, castleId : number ) => {
        packet
                .writeD( castleId ) // manor name
                .writeQ( crop.getAmount() ) // buy residual
                .writeQ( crop.getPrice() ) // buy price
                .writeC( crop.getReward() ) // reward type
    } )

    return packet.getBuffer()
}