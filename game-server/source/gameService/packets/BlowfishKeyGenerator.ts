import _ from 'lodash'

export function generateBlowfishKey() : Buffer {
    let key = Buffer.allocUnsafeSlow( 16 )

    for ( let index = 0; index < 8; index++ ) {
        key[ index ] = Math.random() * 255
    }

    key[ 8 ] = 0xc8
    key[ 9 ] = 0x27
    key[ 10 ] = 0x93
    key[ 11 ] = 0x01
    key[ 12 ] = 0xa1
    key[ 13 ] = 0x6c
    key[ 14 ] = 0x31
    key[ 15 ] = 0x97

    return key
}

class Generator {
    allKeys: Array<Buffer>

    constructor() {
        this.allKeys = _.times( 50, generateBlowfishKey )
    }

    getRandomKey() : Buffer {
        return _.sample( this.allKeys )
    }
}

export const BlowfishKeyGenerator = new Generator()