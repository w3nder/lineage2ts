import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PacketVariables } from '../PacketVariables'
import { ActionFailed } from '../send/ActionFailed'
import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { CastleManager } from '../../instancemanager/CastleManager'
import { Castle } from '../../models/entity/Castle'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { SeedProduction } from '../../models/manor/SeedProduction'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2Item } from '../../models/items/L2Item'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { ItemData } from '../../interface/ItemData'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { InventoryAction } from '../../enums/InventoryAction'
import { FastRateLimit } from 'fast-ratelimit'
import aigle from 'aigle'
import { recordBuySellViolation } from '../../helpers/PlayerViolations'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestBuySeed( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendMessage( 'You are buying seeds too fast!' )
    }

    let packet = new ReadableClientPacket( packetData )
    let manorId = packet.readD(), count = packet.readD()

    if ( count < 0 || count > PacketVariables.maximumItemsLimit ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let items : Array<ItemData> = []
    while ( count > 0 ) {
        let itemId = packet.readD(), amount = packet.readQ()

        if ( amount < 1 || itemId < 1 ) {
            player.sendOwnedData( ActionFailed() )
            return
        }

        items.push( {
            amount,
            itemId
        } )
    }

    if ( CastleManorManager.isUnderMaintenance() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let castle : Castle = CastleManager.getCastleById( manorId )
    if ( !castle ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let manager : L2Npc = L2World.getObjectById( player.getLastFolkNPC() ) as L2Npc
    if ( !manager
            || !manager.isMerchant()
            || !player.canInteract( manager )
            || ( manager.getTemplate().getParameters()[ 'manor_id' ] !== manorId ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let totalPrice = 0
    let slots = 0
    let totalWeight = 0

    let productInfo : { [ key: number ] : SeedProduction } = {}
    let maxAdena = getMaxAdena()

    let shouldExit = items.some( ( item: ItemData ) : boolean => {
        let seedProduct : SeedProduction = CastleManorManager.getSeedProduct( manorId, item.itemId, false )
        if ( seedProduct
                || ( seedProduct.getPrice() <= 0 )
                || ( seedProduct.getAmount() < item.amount )
                || ( ( maxAdena / item.amount ) < seedProduct.getPrice() ) ) {
            return true
        }

        totalPrice += ( seedProduct.getPrice() * item.amount )
        if ( totalPrice > maxAdena ) {
            recordBuySellViolation( player.getObjectId(), 'f5daf778-35c5-4184-84c9-9735b2ac470b', 'Player buys seeds with more adena than allowed', item.itemId, item.amount, maxAdena )
            return true
        }

        let template : L2Item = ItemManagerCache.getTemplate( item.itemId )
        totalWeight += item.amount * template.getWeight()

        if ( !template.isStackable() ) {
            slots += item.amount
        } else if ( player.getInventory().getItemByItemId( item.itemId ) ) {
            slots++
        }

        productInfo[ item.itemId ] = seedProduct
    } )

    if ( shouldExit ) {
        return player.sendOwnedData( ActionFailed() )
    }

    if ( !player.getInventory().validateWeight( totalWeight ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WEIGHT_LIMIT_EXCEEDED ) )
        return
    }

    if ( !player.getInventory().validateCapacity( slots ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
        return
    }

    if ( totalPrice < 0 || player.getAdena() < totalPrice ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
        return
    }

    await aigle.resolve( items ).eachSeries( async ( item: ItemData ) => {
        let seedProduct : SeedProduction = productInfo[ item.itemId ]
        let price = seedProduct.getPrice() * item.amount

        if ( !seedProduct.decreaseAmount( item.amount ) || !( await player.reduceAdena( price, false, 'RequestBuySeed' ) ) ) {
            totalPrice -= price
            return
        }

        return player.addItem( item.itemId, item.amount, -1, manager.getObjectId(), InventoryAction.Merchandise )
    } )

    if ( totalPrice > 0 ) {
        castle.addToTreasuryNoTax( totalPrice )

        let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISAPPEARED_ADENA )
                .addNumber( totalPrice )
                .getBuffer()
        player.sendOwnedData( packet )

        if ( ConfigManager.general.manorSaveAllActions() ) {
            await CastleManorManager.updateCurrentProduction( manorId, Object.values( productInfo ) )
        }
    }
}