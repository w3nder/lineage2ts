import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ActionFailed } from '../send/ActionFailed'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { AbnormalType } from '../../models/skills/AbnormalType'
import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2Object } from '../../models/L2Object'
import { L2World } from '../../L2World'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { SendTradeRequest } from '../send/SendTradeRequest'
import { ProximalDiscoveryManager } from '../../cache/ProximalDiscoveryManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { BlocklistCache } from '../../cache/BlocklistCache'
import { FastRateLimit } from 'fast-ratelimit'
import { BlockedAction } from '../../enums/BlockedAction'
import { PlayerPermission } from '../../enums/PlayerPermission'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 5, // time-to-live value of token bucket (in seconds)
} )

export function TradeRequest( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.sendMessage( 'Transactions are disabled for your current Access Level.' )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.isProcessingTransaction() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALREADY_TRADING ) )
        return
    }

    if ( !ConfigManager.character.karmaPlayerCanTrade() && ( player.getKarma() > 0 ) ) {
        player.sendMessage( 'You cannot trade while you are in a chaotic state.' )
        return
    }

    let info : BuffInfo = player.getEffectList().getBuffInfoByAbnormalType( AbnormalType.BOT_PENALTY )
    if ( info ) {
        let isTradeBlocked : boolean = info.getEffects().some( ( effect: AbstractEffect ) => {
            return effect.blocksAction( BlockedAction.Trade )
        } )

        if ( isTradeBlocked ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_BEEN_REPORTED_SO_ACTIONS_NOT_ALLOWED ) )
            player.sendOwnedData( ActionFailed() )
            return
        }
    }

    let objectId : number = new ReadableClientPacket( packetData ).readD()

    if ( objectId === player.getObjectId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
        return
    }

    let target : L2Object = L2World.getObjectById( objectId )
    if ( !target
            || !ProximalDiscoveryManager.isDiscoveredBy( player.getObjectId(), objectId )
            || target.getInstanceId() !== player.getInstanceId() ) {
        return
    }

    if ( !target.isPlayer() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    let partner : L2PcInstance = target as L2PcInstance
    if ( partner.isInOlympiadMode() || player.isInOlympiadMode() ) {
        player.sendMessage( 'A user currently participating in the Olympiad cannot accept or request a trade.' )
        return
    }

    info = partner.getEffectList().getBuffInfoByAbnormalType( AbnormalType.BOT_PENALTY )
    if ( info ) {
        let isTradeBlocked : boolean = info.getEffects().some( ( effect: AbstractEffect ) => {
            return effect.blocksAction( BlockedAction.Trade )
        } )

        if ( isTradeBlocked ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.C1_REPORTED_AND_IS_BEING_INVESTIGATED )
                    .addCharacterName( partner )
                    .getBuffer()
            player.sendOwnedData( packet )
            player.sendOwnedData( ActionFailed() )
            return
        }
    }

    if ( !ConfigManager.character.karmaPlayerCanTrade() && ( partner.getKarma() > 0 ) ) {
        player.sendMessage( 'You cannot request a trade while your target is in a chaotic state.' )
        return
    }

    if ( ConfigManager.general.jailDisableTransaction() && ( player.isJailed() || partner.isJailed() ) ) {
        player.sendMessage( 'You cannot trade while you are in in Jail.' )
        return
    }

    if ( ( player.getPrivateStoreType() !== PrivateStoreType.None ) || ( partner.getPrivateStoreType() !== PrivateStoreType.None ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_TRADE_DISCARD_DROP_ITEM_WHILE_IN_SHOPMODE ) )
        return
    }

    if ( partner.isProcessingRequest() || partner.isProcessingTransaction() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_BUSY_TRY_LATER )
                .addString( partner.getName() )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    if ( partner.getTradeRefusal() ) {
        player.sendMessage( 'That person is in trade refusal mode.' )
        return
    }

    if ( BlocklistCache.isBlocked( partner.getObjectId(), player.getObjectId() ) ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.S1_HAS_ADDED_YOU_TO_IGNORE_LIST )
                .addCharacterName( partner )
                .getBuffer()
        return player.sendOwnedData( packet )
    }

    if ( player.calculateDistance( partner, true ) > 150 ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_TOO_FAR ) )
    }

    player.onTransactionRequest( partner )
    partner.sendOwnedData( SendTradeRequest( player.getObjectId() ) )

    let packet = new SystemMessageBuilder( SystemMessageIds.REQUEST_C1_FOR_TRADE )
            .addString( partner.getName() )
            .getBuffer()
    player.sendOwnedData( packet )
}