import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2FriendSay } from '../send/L2FriendSay'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, PlayerPrivateChatEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerFriendsCache } from '../../cache/PlayerFriendsCache'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestSendFriendMessage( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let message = packet.readS(), receiverName = packet.readS()

    if ( !message || message.length > 300 ) {
        return
    }

    let targetId = CharacterNamesCache.getCachedIdByName( receiverName )
    let targetPlayer : L2PcInstance = L2World.getPlayer( targetId )
    if ( !targetPlayer ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_NOT_FOUND_IN_THE_GAME ) )
    }

    let targetFriendsIds = PlayerFriendsCache.getFriends( targetPlayer.getObjectId() )
    if ( !targetFriendsIds.has( player.getObjectId() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_USER_NOT_IN_FRIENDS_LIST ) )
        return
    }

    if ( ListenerCache.hasGeneralListener( EventType.PlayerPrivateChat ) ) {
        let data = EventPoolCache.getData( EventType.PlayerPrivateChat ) as PlayerPrivateChatEvent

        data.message = message
        data.receiverName = receiverName
        data.receiverId = targetPlayer.getObjectId()
        data.senderId = player.getObjectId()

        await ListenerCache.sendGeneralEvent( EventType.PlayerPrivateChat, data )
    }

    targetPlayer.sendOwnedData( L2FriendSay( player.getName(), receiverName, message ) )
}