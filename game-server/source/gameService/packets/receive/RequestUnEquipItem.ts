import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ItemInstanceType } from '../../models/items/ItemInstanceType'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { L2ItemSlots } from '../../enums/L2ItemSlots'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestUnEquipItem( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.isStunned() || player.isSleeping() || player.isAlikeDead() ) {
        return
    }

    if ( player.isAttackingNow() || player.isCastingNow() || player.isCastingSimultaneouslyNow() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_CHANGE_WEAPON_DURING_AN_ATTACK ) )
        return
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let slot : L2ItemSlots = packet.readD()

    ReadableClientPacketPool.recycleValue( packet )

    if ( slot === L2ItemSlots.AllHandSlots && ( player.isCursedWeaponEquipped() || player.isCombatFlagEquipped() ) ) {
        return
    }

    let item : L2ItemInstance = player.getInventory().getPaperdollItemBySlot( slot )
    if ( !item ) {
        return
    }

    if ( slot === L2ItemSlots.LeftHand && item.getItem().isInstanceType( ItemInstanceType.L2EtcItem ) ) {
        return
    }

    if ( !player.getInventory().canManipulateWithItemId( item.getId() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_CANNOT_BE_TAKEN_OFF ) )
        return
    }

    if ( item.isWeapon() && item.getWeaponItem().isForceEquip && !player.hasActionOverride( PlayerActionOverride.ItemAction ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_CANNOT_BE_TAKEN_OFF ) )
        return
    }

    let unequipped : Array<L2ItemInstance> = await player.getInventory().unEquipItemInBodySlotAndRecord( slot )
    player.broadcastUserInfo()

    if ( unequipped.length > 0 ) {
        if ( unequipped[ 0 ].getEnchantLevel() > 0 ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.EQUIPMENT_S1_S2_REMOVED )
                    .addNumber( unequipped[ 0 ].getEnchantLevel() )
                    .addItemInstanceName( unequipped[ 0 ] )
                    .getBuffer()

            player.sendOwnedData( packet )
        } else {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISARMED )
                    .addItemInstanceName( unequipped[ 0 ] )
                    .getBuffer()

            player.sendOwnedData( packet )
        }
    }
}