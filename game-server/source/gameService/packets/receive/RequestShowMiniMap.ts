import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ShowMiniMap } from '../send/ShowMiniMap'

export function RequestShowMiniMap( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    player.sendOwnedData( ShowMiniMap( 1665 ) )
}