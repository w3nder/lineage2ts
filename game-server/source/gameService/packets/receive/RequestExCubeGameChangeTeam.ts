import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { BlockCheckerManager } from '../../instancemanager/BlockCheckerManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestExCubeGameChangeTeam( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let arenaValue = packet.readD(), teamValue = packet.readD()

    arenaValue = arenaValue + 1

    if ( BlockCheckerManager.isArenaBeingUsed( arenaValue ) ) {
        return
    }

    switch ( teamValue ) {
        case 0:
        case 1:
            BlockCheckerManager.changePlayerToTeam( player, arenaValue )
            break
        case -1:
            let team = BlockCheckerManager.getHolder( arenaValue ).getPlayerTeam( player )
            // client sends two times this packet if click on exit
            // client did not send this packet on restart
            if ( team > -1 ) {
                BlockCheckerManager.removePlayer( player, arenaValue, team )
            }
    }
}