import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2DimensionalTransferItem } from '../../models/items/L2DimensionalTransferItem'
import { ExGetDimensionalItemList } from '../send/ExGetDimensionalItemList'
import { DimensionalTransferManager } from '../../cache/DimensionalTransferManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { InventoryAction } from '../../enums/InventoryAction'
import _ from 'lodash'
import { FastRateLimit } from 'fast-ratelimit'
import { recordReceivedPacketViolation } from '../../helpers/PlayerViolations'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestWithdrawDimesionalItem( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !DimensionalTransferManager.hasItems( player.getObjectId() ) ) {
        return recordReceivedPacketViolation( player.getObjectId(),'c364638f-c08e-4f2f-a2e3-e52921fb4d9d', 'Player attempts to withdraw non-existent dimensional items', RequestWithdrawDimesionalItem.name )
    }

    if ( ( player.getWeightPenalty() >= 3 ) || !player.isInventoryUnder90( false ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_RECEIVE_D_ITEM ) )
        return
    }

    if ( player.isProcessingTransaction() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_RECEIVE_D_ITEM_DURING_AN_EXCHANGE ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let itemIndex = packet.readD(), playerId = packet.readD(), amount = packet.readQ()

    if ( amount <= 0 ) {
        return
    }

    if ( player.getObjectId() !== playerId ) {
        return recordReceivedPacketViolation( player.getObjectId(), '673bab29-5a3a-4d21-bb42-0d9490be8466', 'Player attempts to withdraw dimentional item for different player id', RequestWithdrawDimesionalItem.name )
    }

    let item : L2DimensionalTransferItem = _.nth( DimensionalTransferManager.getItems( playerId ), itemIndex )
    if ( !item || item.amount < amount ) {
        return
    }

    let itemsLeft = item.amount - amount

    await player.addItem( item.itemId, amount, -1, player.getTargetId(), InventoryAction.PremiumItem )

    if ( itemsLeft > 0 ) {
        DimensionalTransferManager.updateItem( playerId, item.itemId, itemsLeft )
    } else {
        DimensionalTransferManager.removeItem( playerId, item.itemId )
    }

    if ( !DimensionalTransferManager.hasItems( player.getObjectId() ) ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THERE_ARE_NO_MORE_D_ITEMS_TO_BE_FOUND ) )
    }

    player.sendOwnedData( ExGetDimensionalItemList( player.getObjectId() ) )
}