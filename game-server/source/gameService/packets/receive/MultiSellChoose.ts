import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PreparedListContainer } from '../../models/multisell/PreparedListContainer'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2World } from '../../L2World'
import { L2NpcValues } from '../../values/L2NpcValues'
import { PcInventory } from '../../models/itemcontainer/PcInventory'
import { Ingredient } from '../../models/multisell/Ingredient'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2Augmentation } from '../../models/L2Augmentation'
import { Elementals } from '../../models/Elementals'
import { ConfigManager } from '../../../config/ConfigManager'
import { MultisellCache } from '../../cache/MultisellCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import aigle from 'aigle'
import _ from 'lodash'
import { FastRateLimit } from 'fast-ratelimit'
import { recordBuySellViolation } from '../../helpers/PlayerViolations'
import { ItemTypes } from '../../values/InventoryValues'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function MultiSellChoose( client: GameClient, packetData: Buffer ) : Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.setMultiSell( null )
    }

    let packet = new ReadableClientPacket( packetData )
    let listId = packet.readD(), entryId = packet.readD(), amount = packet.readQ()

    if ( amount < 1 || amount > 5000 ) {
        player.setMultiSell( null )
        return
    }

    let list : PreparedListContainer = player.getMultiSell()
    if ( !list || ( list.getListId() !== listId ) ) {
        player.setMultiSell( null )
        return
    }

    let entry = list.getEntry( entryId )
    if ( !entry ) {
        player.setMultiSell( null )
        return
    }

    if ( !entry.isStackable() && amount > 1 ) {
        player.setMultiSell( null )
        return
    }

    let npc : L2Npc = L2World.getObjectById( player.getLastFolkNPC() ) as L2Npc
    if ( ( npc && !list.isNpcAllowed( npc.getId() ) ) || ( !npc && list.isNpcOnly() ) ) {
        player.setMultiSell( null )
        return
    }

    if ( !player.isGM() && npc ) {
        if ( !player.isInsideRadius( npc, L2NpcValues.interactionDistance, true )
                || ( player.getInstanceId() !== npc.getInstanceId() ) ) {
            player.setMultiSell( null )
            return
        }
    }

    let slots = 0
    let weight = 0

    entry.getProducts().forEach( ( item: Ingredient ) => {
        if ( item.getItemId() < 0 ) {
            return
        }

        if ( !item.isStackable() ) {
            slots += item.getItemCount() * amount
        } else if ( !player.getInventory().getItemByItemId( item.getItemId() ) ) {
            slots++
        }

        weight += item.getItemCount() * amount * item.getWeight()
    } )

    let inventory : PcInventory = player.getInventory()
    if ( !inventory.validateWeight( weight ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WEIGHT_LIMIT_EXCEEDED ) )
        return
    }

    if ( !inventory.validateCapacity( slots ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
        return
    }

    let allIngredients : Array<Ingredient> = []

    for ( let ingredient of entry.getIngredients() ) {
        let isNewIngredient = true

        for ( let index = allIngredients.length; --index >= 0; ) {
            let currentIngredient : Ingredient = allIngredients[ index ]

            if ( ( currentIngredient.getItemId() === ingredient.getItemId() ) && ( currentIngredient.getEnchantLevel() === ingredient.getEnchantLevel() ) ) {
                if ( ( currentIngredient.getItemCount() + ingredient.getItemCount() ) > Number.MAX_SAFE_INTEGER ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_EXCEEDED_QUANTITY_THAT_CAN_BE_INPUTTED ) )
                    return
                }

                let clonedIngredient = currentIngredient.getCopy()
                clonedIngredient.setItemCount( currentIngredient.getItemCount() + ingredient.getItemCount() )
                allIngredients[ index ] = clonedIngredient
                isNewIngredient = false
                break
            }
        }

        if ( isNewIngredient ) {
            allIngredients.push( ingredient )
        }
    }

    for ( let ingredient of allIngredients ) {
        if ( ( ingredient.getItemCount() * amount ) > Number.MAX_SAFE_INTEGER ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_EXCEEDED_QUANTITY_THAT_CAN_BE_INPUTTED ) )
            return
        }

        if ( ingredient.getItemId() < 0 ) {
            if ( !MultisellCache.hasSpecialIngredient( ingredient.getItemId(), ingredient.getItemCount() * amount, player ) ) {
                return
            }
        } else {
            let requiredAmount = ( ( ConfigManager.character.blacksmithUseRecipes() || !ingredient.getMaintainIngredient() ) ? ( ingredient.getItemCount() * amount ) : ingredient.getItemCount() )
            if ( inventory.getInventoryItemCount( ingredient.getItemId(), list.getMaintainEnchantment() ? ingredient.getEnchantLevel() : -1, false ) < requiredAmount ) {
                if ( ingredient.getItemId() === ItemTypes.Adena ) {
                    return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
                }

                let packet = new SystemMessageBuilder( SystemMessageIds.S2_UNIT_OF_THE_ITEM_S1_REQUIRED )
                    .addItemName( ingredient.getTemplate() )
                    .addNumber( requiredAmount )
                    .getBuffer()
                player.sendOwnedData( packet )
                return
            }
        }
    }

    let augmentation : Array<L2Augmentation> = []
    let elementals : ReadonlyArray<Elementals>
    for ( let ingredient of entry.getIngredients() ) {
        if ( ingredient.getItemId() < 0 ) {
            if ( !( await MultisellCache.takeSpecialIngredient( ingredient.getItemId(), ingredient.getItemCount() * amount, player ) ) ) {
                return
            }
        } else {
            let itemToTake : L2ItemInstance = inventory.getItemByItemId( ingredient.getItemId() )
            if ( !itemToTake ) {
                player.setMultiSell( null )
                await recordBuySellViolation( player.getObjectId(), '1e794df6-2f9f-42cd-82ef-ec59e11fb96e', '' )
                return
            }

            if ( ConfigManager.character.blacksmithUseRecipes() || !ingredient.getMaintainIngredient() ) {
                // if it's a stackable item, just reduce the amount from the first (only) instance that is found in the inventory
                if ( itemToTake.isStackable() ) {
                    if ( !await player.destroyItemByObjectId( itemToTake.getObjectId(), ( ingredient.getItemCount() * amount ), true, 'Multisell' ) ) {
                        player.setMultiSell( null )
                        return
                    }
                } else {

                    if ( list.getMaintainEnchantment() ) {
                        let inventoryContents : Array<L2ItemInstance> = inventory.getAllItemsByItemIdWithEnchant( ingredient.getItemId(), ingredient.getEnchantLevel(), false )
                        for ( let index = 0; index < ( ingredient.getItemCount() * amount ); index++ ) {
                            if ( inventoryContents[ index ].isAugmented() ) {
                                augmentation.push( inventoryContents[ index ].getAugmentation() )
                            }
                            if ( inventoryContents[ index ].getElementals() ) {
                                elementals = inventoryContents[ index ].getElementals()
                            }

                            if ( !await player.destroyItemByObjectId( inventoryContents[ index ].getObjectId(), 1, true, 'Multisell' ) ) {
                                player.setMultiSell( null )
                                return
                            }
                        }
                    } else {

                        for ( let i = 1; i <= ( ingredient.getItemCount() * amount ); i++ ) {
                            let inventoryContents : Array<L2ItemInstance> = inventory.getAllItemsByItemId( ingredient.getItemId(), false )

                            itemToTake = inventoryContents[ 0 ]

                            if ( itemToTake.getEnchantLevel() > 0 ) {
                                _.each( inventoryContents, ( item: L2ItemInstance ) => {
                                    if ( item.getEnchantLevel() < itemToTake.getEnchantLevel() ) {
                                        itemToTake = item
                                        // nothing will have enchantment less than 0. If a zero-enchanted
                                        // item is found, just take it
                                        if ( itemToTake.getEnchantLevel() === 0 ) {
                                            return false
                                        }
                                    }
                                } )
                            }
                            if ( !await player.destroyItemByObjectId( itemToTake.getObjectId(), 1, true, 'Multisell' ) ) {
                                player.setMultiSell( null )
                                return
                            }
                        }
                    }
                }
            }
        }
    }

    await aigle.resolve( entry.getProducts() ).eachSeries( async ( product: Ingredient ) => {
        if ( product.getItemId() < 0 ) {
            return MultisellCache.giveSpecialProduct( product.getItemId(), product.getItemCount() * amount, player )
        }

        let targetId = player.getTargetId()
        if ( product.isStackable() ) {
            await inventory.addItem( product.getItemId(), product.getItemCount() * amount, targetId, 'Multisell' )
        } else {
            await aigle.resolve( product.getItemCount() * amount ).times( async ( index : number ) => {
                let item : L2ItemInstance = await inventory.addItem( product.getItemId(), 1, targetId, 'Multisell' )
                if ( item && list.getMaintainEnchantment() ) {
                    if ( index < augmentation.length ) {
                        item.setAugmentation( new L2Augmentation( augmentation[ index ].getAugmentationId() ) )
                    }

                    elementals.forEach( ( value: Elementals ) : void => {
                        item.setElementAttribute( value.getElement(), value.getValue() )
                    } )

                    await item.setEnchantLevel( product.getEnchantLevel() )
                }
            } )
        }

        if ( ( product.getItemCount() * amount ) > 1 ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.EARNED_S2_S1_S )
                .addItemNameWithId( product.getItemId() )
                .addNumber( product.getItemCount() * amount )
                .getBuffer()
            player.sendOwnedData( packet )
        } else {
            if ( list.getMaintainEnchantment() && product.getEnchantLevel() > 0 ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.ACQUIRED_S1_S2 )
                    .addNumber( product.getEnchantLevel() )
                    .addItemNameWithId( product.getItemId() )
                    .getBuffer()

                player.sendOwnedData( packet )
            } else {
                let packet = new SystemMessageBuilder( SystemMessageIds.EARNED_ITEM_S1 )
                    .addItemNameWithId( product.getItemId() )
                    .getBuffer()

                player.sendOwnedData( packet )
            }
        }
    } )


    if ( npc && entry.getTaxAmount() > 0 ) {
        npc.getCastle().addToTreasury( entry.getTaxAmount() * amount )
    }
}