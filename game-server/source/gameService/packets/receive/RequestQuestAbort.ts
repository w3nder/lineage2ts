import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ListenerLogic } from '../../models/ListenerLogic'
import { ListenerManager } from '../../instancemanager/ListenerManager'
import { QuestState } from '../../models/quest/QuestState'
import { QuestList } from '../send/QuestList'
import { QuestStateCache } from '../../cache/QuestStateCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestQuestAbort( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let questId : number = new ReadableClientPacket( packetData ).readD()

    let existingQuest : ListenerLogic = ListenerManager.getQuest( questId )
    if ( existingQuest ) {
        let state : QuestState = QuestStateCache.getQuestState( player.getObjectId(), existingQuest.getName() )
        if ( state ) {
            await state.exitQuest( true )
            player.sendDebouncedPacket( QuestList )
        }
    }
}