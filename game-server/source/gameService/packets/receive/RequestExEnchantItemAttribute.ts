import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemLocation } from '../../enums/ItemLocation'
import {
    ElementalItem,
    ElementalItemType,
    Elementals,
    ElementalsHelper,
    ElementalValues
} from '../../models/Elementals'
import { ConfigManager } from '../../../config/ConfigManager'
import { ExBrExtraUserInfo } from '../send/ExBrExtraUserInfo'
import { UserInfo } from '../send/UserInfo'
import { ExAttributeEnchantResult } from '../send/ExAttributeEnchantResult'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import _ from 'lodash'
import { ElementalType } from '../../enums/ElementalType'

export async function RequestExEnchantItemAttribute( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !player.isOnline() ) {
        player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
        return
    }

    if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_ADD_ELEMENTAL_POWER_WHILE_OPERATING_PRIVATE_STORE_OR_WORKSHOP ) )
        player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
        return
    }

    if ( player.getActiveRequester() ) {
        player.cancelActiveTrade()
        player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
        player.sendMessage( 'You cannot add elemental power while trading.' )
        return
    }

    let objectId : number = new ReadableClientPacket( packetData ).readD()

    if ( objectId === 0xFFFFFFFF ) {
        player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ELEMENTAL_ENHANCE_CANCELED ) )
        return
    }

    let item: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    let stone: L2ItemInstance = player.getInventory().getItemByObjectId( player.getActiveEnchantAttributeItemId() )
    if ( !item || !stone ) {
        player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ELEMENTAL_ENHANCE_CANCELED ) )
        return
    }

    if ( !item.canEnchantElemental() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ELEMENTAL_ENHANCE_REQUIREMENT_NOT_SUFFICIENT ) )
        player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
        return
    }

    switch ( item.getItemLocation() ) {
        case ItemLocation.INVENTORY:
        case ItemLocation.PAPERDOLL:
            if ( item.getOwnerId() !== player.getObjectId() ) {
                player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
                return
            }
            break
        default:
            player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
            // TODO : Util.handleIllegalPlayerAction(player, "Player " + player.getName() + " tried to use enchant Exploit!");
            return
    }

    let stoneId = stone.getId()
    let elementToAdd = Elementals.getItemElement( stoneId )

    if ( item.isArmor() ) {
        elementToAdd = Elementals.getOppositeElement( elementToAdd )
    }

    let oppositeElement = Elementals.getOppositeElement( elementToAdd )

    let oldElement: Elementals = item.getElemental( elementToAdd )
    let elementValue = !oldElement ? 0 : oldElement.getValue()
    let limit = getLimit( item, stoneId )
    let powerToAdd = getPowerToAdd( stoneId, elementValue, item )

    if ( ( item.isWeapon()
            && oldElement
            && oldElement.getElement() !== elementToAdd
            && oldElement.getElement() !== ElementalType.NONE ) ||
            ( item.isArmor()
                    && !item.getElemental( elementToAdd )
                    && item.getElementals()
                    && item.getElementals().length >= 3 ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ANOTHER_ELEMENTAL_POWER_ALREADY_ADDED ) )
        player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
        return
    }

    if ( item.isArmor() && item.getElementals() ) {
        let shouldExit = item.getElementals().some( ( element: Elementals ) : boolean => {
            return element.getElement() === oppositeElement
        } )

        if ( shouldExit ) {
            player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
            // TODO : violation, cannot add opposite element
            return
        }
    }

    let newPower = elementValue + powerToAdd
    if ( newPower > limit ) {
        newPower = limit
        powerToAdd = limit - elementValue
    }

    if ( powerToAdd <= 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ELEMENTAL_ENHANCE_CANCELED ) )
        player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
        return
    }

    if ( !( await player.destroyItemWithCount( stone, 1, true, 'RequestExEnchantItemAttribute' ) ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
        // TODO : log violation
        player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
        return
    }

    let success = _.random( 100 ) < getBaseChance( stoneId )
    if ( success ) {
        let realElement = item.isArmor() ? oppositeElement : elementToAdd

        let messagePacket: SystemMessageBuilder
        if ( item.getEnchantLevel() === 0 ) {
            if ( item.isArmor() ) {
                messagePacket = new SystemMessageBuilder( SystemMessageIds.THE_S2_ATTRIBUTE_WAS_SUCCESSFULLY_BESTOWED_ON_S1_RES_TO_S3_INCREASED )
            } else {
                messagePacket = new SystemMessageBuilder( SystemMessageIds.ELEMENTAL_POWER_S2_SUCCESSFULLY_ADDED_TO_S1 )
            }

            messagePacket
                    .addItemInstanceName( item )
                    .addElemental( realElement )

            if ( item.isArmor() ) {
                messagePacket.addElemental( Elementals.getOppositeElement( realElement ) )
            }

        } else {
            if ( item.isArmor() ) {
                messagePacket = new SystemMessageBuilder( SystemMessageIds.THE_S3_ATTRIBUTE_BESTOWED_ON_S1_S2_RESISTANCE_TO_S4_INCREASED )
            } else {
                messagePacket = new SystemMessageBuilder( SystemMessageIds.ELEMENTAL_POWER_S3_SUCCESSFULLY_ADDED_TO_S1_S2 )
            }

            messagePacket
                    .addNumber( item.getEnchantLevel() )
                    .addItemInstanceName( item )
                    .addElemental( realElement )

            if ( item.isArmor() ) {
                messagePacket.addElemental( Elementals.getOppositeElement( realElement ) )
            }
        }

        player.sendOwnedData( messagePacket.getBuffer() )
        item.setElementAttribute( elementToAdd, newPower )
        if ( item.isEquipped() ) {
            item.updateElementAttributesBonus( player )
        }
    } else {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_ADDING_ELEMENTAL_POWER ) )
    }

    player.sendOwnedData( ExAttributeEnchantResult( powerToAdd ) )
    player.sendDebouncedPacket( UserInfo )
    player.sendDebouncedPacket( ExBrExtraUserInfo )
    player.setActiveEnchantAttributeItemId( L2PcInstanceValues.EmptyId )
}

function getBaseChance( stoneId: number ): number {
    switch ( ElementalsHelper.getByItemId( stoneId ).maxLevel ) {
        case ElementalItemType.Stone:
        case ElementalItemType.Roughore:
            return ConfigManager.character.getEnchantChanceElementStone()
        case ElementalItemType.Crystal:
            return ConfigManager.character.getEnchantChanceElementCrystal()
        case ElementalItemType.Jewel:
            return ConfigManager.character.getEnchantChanceElementJewel()
        case ElementalItemType.Energy:
            return ConfigManager.character.getEnchantChanceElementEnergy()
    }
}

function getLimit( item: L2ItemInstance, stoneId: number ): number {
    let elementItem: ElementalItem = ElementalsHelper.getByItemId( stoneId )
    if ( !elementItem ) {
        return 0
    }

    let level = elementItem.maxLevel

    if ( item.isWeapon() ) {
        return ElementalValues.WEAPON_VALUES[ level ]
    }

    return ElementalValues.ARMOR_VALUES[ level ]
}

function getPowerToAdd( stoneId: number, value: number, item: L2ItemInstance ): number {
    if ( Elementals.getItemElement( stoneId ) !== ElementalType.NONE ) {
        if ( item.isWeapon() ) {
            if ( value === 0 ) {
                return ElementalValues.FIRST_WEAPON_BONUS
            }

            return ElementalValues.NEXT_WEAPON_BONUS
        }

        if ( item.isArmor() ) {
            return ElementalValues.ARMOR_BONUS
        }
    }

    return 0
}