import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { MailMessage } from '../../models/mail/MailMessage'
import { MailManager } from '../../instancemanager/MailManager'
import { ExChangePostStateSingle } from '../send/ExChangePostState'
import { L2World } from '../../L2World'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { AreaType } from '../../models/areas/AreaType'
import { ChangePostState } from '../../enums/ChangePostState'
import { SendBySystem } from '../../models/mail/SendBySystem'
import { recordReceivedPacketViolation } from '../../helpers/PlayerViolations'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestRejectPostAttachment( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !ConfigManager.general.allowMail() || !ConfigManager.general.allowAttachments() ) {
        return
    }

    if ( !player.isInArea( AreaType.Peace ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_USE_MAIL_OUTSIDE_PEACE_ZONE ) )
        return
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let messageId : number = packet.readD()
    ReadableClientPacketPool.recycleValue( packet )

    let message: MailMessage = MailManager.getMessage( messageId )
    if ( !message ) {
        return
    }

    if ( message.getReceiverId() !== player.getObjectId() ) {
        return recordReceivedPacketViolation( player.getObjectId(), '96613cf2-0f49-4c8e-8fa9-c0d361228aee', 'Player rejects unreceived mail attachment', RequestRejectPostAttachment.name, messageId )
    }

    if ( !message.hasAttachments() || message.getSendBySystem() !== SendBySystem.Player ) {
        return
    }

    let returnedMessage = MailMessage.asReturned( message )
    await MailManager.sendMessage( returnedMessage )

    MailManager.removeAttachments( messageId )
    MailManager.setDeleted( messageId )

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MAIL_SUCCESSFULLY_RETURNED ) )
    player.sendOwnedData( ExChangePostStateSingle( true, messageId, ChangePostState.Rejected ) )

    let sender: L2PcInstance = L2World.getPlayer( message.getSenderId() )
    if ( sender ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.S1_RETURNED_MAIL )
                .addPlayerCharacterName( player )
                .getBuffer()

        sender.sendOwnedData( packet )
    }
}