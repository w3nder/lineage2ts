import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { GameClient } from '../../GameClient'
import { ConfigManager } from '../../../config/ConfigManager'
import { FishingChampionshipManager } from '../../cache/FishingChampionshipManager'

export function RequestExFishRanking( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( ConfigManager.general.allowFishingChampionship() ) {
        FishingChampionshipManager.showMidResult( player )
    }
}