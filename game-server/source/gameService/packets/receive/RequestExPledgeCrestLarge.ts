import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExPledgeCrestLarge } from '../send/ExPledgeCrestLarge'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestExPledgeCrestLarge( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let crestId : number = new ReadableClientPacket( packetData ).readD()

    player.sendOwnedData( ExPledgeCrestLarge( crestId ) )
}