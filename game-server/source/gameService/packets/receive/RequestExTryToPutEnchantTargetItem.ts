import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { EnchantScroll } from '../../models/items/enchant/EnchantScroll'
import { DataManager } from '../../../data/manager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { ExPutEnchantTargetItemResult } from '../send/ExPutEnchantTargetItemResult'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function RequestExTryToPutEnchantTargetItem( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.isEnchanting() ) {
        return
    }

    let objectId : number = new ReadableClientPacket( packetData ).readD()

    let item: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    let scroll: L2ItemInstance = player.getInventory().getItemByObjectId( player.getActiveEnchantItemId() )
    if ( !item || !scroll ) {
        return
    }

    let scrollTemplate: EnchantScroll = DataManager.getEnchantItemData().getScrollById( scroll.getId() )
    if ( !scroll || !scrollTemplate.isValid( item, null ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DOES_NOT_FIT_SCROLL_CONDITIONS ) )
        player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
        player.sendOwnedData( ExPutEnchantTargetItemResult( 0 ) )
        return
    }

    player.setIsEnchanting( true )
    player.sendOwnedData( ExPutEnchantTargetItemResult( objectId ) )
}