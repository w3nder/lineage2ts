import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { EtcItemType } from '../../enums/items/EtcItemType'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { ItemType2 } from '../../enums/items/ItemType2'
import { AdminManager } from '../../cache/AdminManager'
import { ItemTypes } from '../../values/InventoryValues'
import { HtmlActionCache } from '../../cache/HtmlActionCache'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { recordItemViolation, recordReceivedPacketViolation } from '../../helpers/PlayerViolations'
import { ItemProtectionCache } from '../../cache/ItemProtectionCache'
import { AreaType } from '../../models/areas/AreaType'
import { PlayerPermission } from '../../enums/PlayerPermission'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestDropItem( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( ConfigManager.general.jailDisableTransaction() && player.isJailed() ) {
        player.sendMessage( 'You cannot drop items in Jail.' )
        return
    }

    if ( !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.sendMessage( 'Transactions are disabled for your Access Level.' )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOTHING_HAPPENED ) )
        return
    }

    if ( player.isProcessingTransaction() || ( player.getPrivateStoreType() !== PrivateStoreType.None ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_TRADE_DISCARD_DROP_ITEM_WHILE_IN_SHOPMODE ) )
        return
    }
    if ( player.isFishing() ) {
        // You can't mount, dismount, break and drop items while fishing
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DO_WHILE_FISHING_2 ) )
        return
    }

    if ( player.isFlying() ) {
        return
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let objectId = packet.readD(),
        count = packet.readQ(),
        x = packet.readD(),
        y = packet.readD(),
        z = packet.readD()

    ReadableClientPacketPool.recycleValue( packet )

    if ( count <= 0 ) {
        return recordReceivedPacketViolation(
            player.getObjectId(),
            '33e6c389-c044-4627-b784-c41e4e8bf2c4',
            'Player drops zero items',
            RequestDropItem.name,
            objectId, count )
    }

    let item: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    if ( !item ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_THIS_ITEM ) )
        return recordItemViolation( player.getObjectId(), '24b67545-8848-4a8c-9aea-c0a068c2710d', 'Player drops non-owned item', 0, count, objectId )
    }

    // TODO : add integration with area flags
    if ( !item
        || count === 0
        || !player.validateItemManipulation( objectId )
        || ( !ConfigManager.general.allowDiscardItem() && !player.hasActionOverride( PlayerActionOverride.DropItem ) )
        || ( !item.isDropable() && !( player.hasActionOverride( PlayerActionOverride.DropItem ) && ConfigManager.general.gmTradeRestrictedItems() ) )
        || ( ( item.getItemType() === EtcItemType.PET_COLLAR ) && player.havePetInventoryItems() )
        || player.isInArea( AreaType.NoItemDrop ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_THIS_ITEM ) )
        return
    }
    if ( item.isQuestItem() && !( player.hasActionOverride( PlayerActionOverride.DropItem ) && ConfigManager.general.gmTradeRestrictedItems() ) ) {
        return
    }

    if ( count > item.getCount() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_THIS_ITEM ) )
        return
    }

    if ( ConfigManager.character.getPlayerSpawnProtection() > 0
        && player.isInvulnerable()
        && !player.isGM() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_THIS_ITEM ) )
        return
    }

    if ( !item.isStackable() && count > 1 ) {
        return recordItemViolation(
            player.getObjectId(),
            'dfb08ab7-112a-4ac1-be29-26d35646b1a5',
            'Player drops multiple non-stackable items',
            item.getId(), count, objectId )
    }

    if ( player.isCastingNow() && player.getMainCast().skill.getItemConsumeId() === item.getId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_THIS_ITEM ) )
        return
    }

    if ( player.isCastingSimultaneouslyNow() && player.getSimultaneousCast().skill.getItemConsumeId() === item.getId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_THIS_ITEM ) )
        return
    }

    if ( ( ItemType2.QUEST === item.getItem().getType2() ) && !player.hasActionOverride( PlayerActionOverride.DropItem ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_EXCHANGE_ITEM ) )
        return
    }

    if ( !player.isInsideRadiusCoordinates( x, y, 0, 150, false ) || ( Math.abs( z - player.getZ() ) > 50 ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_DISTANCE_TOO_FAR ) )
        return
    }

    if ( !player.getInventory().canManipulateWithItemId( item.getId() ) ) {
        player.sendMessage( 'You cannot use this item.' )
        return
    }

    if ( item.isEquipped() ) {
        await player.getInventory().unEquipItemInSlot( item.getLocationSlot() )
        player.broadcastUserInfo()
    }

    HtmlActionCache.removeActions( player.getObjectId() )

    let droppedItem: L2ItemInstance = await player.dropItemAtCoordinates( objectId, count, x, y, z,false, false )
    if ( droppedItem && droppedItem.getId() === ItemTypes.Adena && droppedItem.getCount() >= 1000000 ) {
        const message = `Character '${ player.getName() }' has dropped adena = ${ droppedItem.getCount() } at (${ x }, ${ y }, ${ z }) location.`
        AdminManager.broadcastMessageToGMs( message )

        /*
            TODO : consider sending adena back to inventory of player or ensure that such item can only be picked up by admin
            Current solution is to mark dropped item as permanently protected from pick up by other people than a GM or owner player.
         */
        ItemProtectionCache.addPermanentProtection( droppedItem.getObjectId() )

        return recordItemViolation(
            player.getObjectId(),
            'c0870e0a-3353-4267-a6ab-857d17de6adf',
            'Player drops large amount of adena',
            droppedItem.getId(), count, droppedItem.getObjectId() )
    }
}