import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DatabaseManager } from '../../../database/manager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 5, // time-to-live value of token bucket (in seconds)
} )

export async function RequestPetitionFeedback( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !player.getLastPetitionGmName() ) {
        return
    }

    // rate: 4=VeryGood, 3=Good, 2=Fair, 1=Poor, 0=VeryPoor
    let packet = new ReadableClientPacket( packetData )
    packet.skipD( 1 )
    let rate = packet.readD(), message = packet.readS()

    if ( ( rate > 4 ) || ( rate < 0 ) ) {
        return
    }

    return DatabaseManager.getPetitionFeedback().addEntry( player, rate, message )
}