import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ClassId } from '../../models/base/ClassId'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2EnchantSkillLearn } from '../../models/L2EnchantSkillLearn'
import { DataManager } from '../../../data/manager'
import { Skill } from '../../models/Skill'
import { SkillCache } from '../../cache/SkillCache'
import { SkillItemValues } from '../../values/SkillItemValues'
import { EnchantSkillHolder } from '../../models/L2EnchantSkillGroup'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { ExEnchantSkillResult } from '../send/ExEnchantSkillResult'
import { UserInfo } from '../send/UserInfo'
import { ExBrExtraUserInfo } from '../send/ExBrExtraUserInfo'
import { EnchantType, ExEnchantSkillInfoDetail } from '../send/ExEnchantSkillInfoDetail'
import { ExEnchantSkillInfo } from '../send/ExEnchantSkillInfo'
import { ItemTypes } from '../../values/InventoryValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { PlayerShortcutCache } from '../../cache/PlayerShortcutCache'

export async function RequestExEnchantSkillUntrain( client: GameClient, packetData: Buffer ) : Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( ClassId.getClassIdByIdentifier( player.getClassId() ).level < 3 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_IN_THIS_CLASS ) )
        return
    }

    if ( player.getLevel() < 76 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_ON_THIS_LEVEL ) )
        return
    }

    if ( !player.isAllowedToEnchantSkills() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_ATTACKING_TRANSFORMED_BOAT ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let skillId = packet.readD(), skillLevel = packet.readD()

    if ( skillId < 0 || skillLevel <= 0 ) {
        return
    }

    let skillLearn: L2EnchantSkillLearn = DataManager.getSkillData().getSkillEnchantmentBySkillId( skillId )
    if ( !skillLearn ) {
        return
    }

    if ( ( skillLevel % 100 ) === 0 ) {
        skillLevel = skillLearn.getBaseLevel()
    }

    let skill: Skill = SkillCache.getSkill( skillId, skillLevel )
    if ( !skill ) {
        return
    }

    let reqItemId = SkillItemValues.UntrainEnchantBook
    let beforeUntrainSkillLevel = player.getSkillLevel( skillId )
    if ( ( ( beforeUntrainSkillLevel - 1 ) !== skillLevel )
            && ( ( ( beforeUntrainSkillLevel % 100 ) !== 1 ) || ( skillLevel !== skillLearn.getBaseLevel() ) ) ) {
        return
    }

    let skillHolder: EnchantSkillHolder = skillLearn.getEnchantSkillHolder( beforeUntrainSkillLevel )

    let requiredSp = skillHolder.getSpCost()
    let requireditems = skillHolder.getAdenaCost()

    let spellBook: L2ItemInstance = player.getInventory().getItemByItemId( reqItemId )
    if ( !spellBook && ConfigManager.character.enchantSkillSpBookNeeded() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_OF_THE_ITEMS_NEEDED_TO_ENCHANT_THAT_SKILL ) )
        return
    }

    if ( player.getInventory().getAdenaAmount() < requireditems ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_OF_THE_ITEMS_NEEDED_TO_ENCHANT_THAT_SKILL ) )
        return
    }

    let isSuccess: boolean = true
    if ( ConfigManager.character.enchantSkillSpBookNeeded() ) {
        isSuccess = await player.destroyItemByObjectId( spellBook.getObjectId(), 1, true, 'RequestExEnchantSkillUntrain' )
    }

    isSuccess = isSuccess && await player.destroyItemByItemId( ItemTypes.Adena, requireditems, true, 'RequestExEnchantSkillUntrain' )

    if ( !isSuccess ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_OF_THE_ITEMS_NEEDED_TO_ENCHANT_THAT_SKILL ) )
        return
    }

    player.addSp( Math.floor( requiredSp * 0.8 ) )

    await player.addSkill( skill, true )
    player.sendOwnedData( ExEnchantSkillResult( true ) )

    player.sendDebouncedPacket( UserInfo )
    player.sendDebouncedPacket( ExBrExtraUserInfo )

    if ( skillLevel > 100 ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.UNTRAIN_SUCCESSFUL_SKILL_S1_ENCHANT_LEVEL_DECREASED_BY_ONE )
                .addSkillNameById( skillId )
                .getBuffer()
        player.sendOwnedData( packet )
    } else {
        let packet = new SystemMessageBuilder( SystemMessageIds.UNTRAIN_SUCCESSFUL_SKILL_S1_ENCHANT_LEVEL_RESETED )
                .addSkillNameById( skillId )
                .getBuffer()
        player.sendOwnedData( packet )
    }

    player.sendSkillList()

    let afterUntrainSkillLevel = player.getSkillLevel( skillId )
    player.sendOwnedData( ExEnchantSkillInfo( skillId, afterUntrainSkillLevel ) )
    player.sendOwnedData( ExEnchantSkillInfoDetail( EnchantType.Untrain, skillId, afterUntrainSkillLevel - 1, player ) )

    return PlayerShortcutCache.updateSkillShortcut( player, skillId, afterUntrainSkillLevel )
}