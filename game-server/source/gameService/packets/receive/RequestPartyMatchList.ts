import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PartyMatchManager } from '../../cache/partyMatchManager'
import { PartyMatchWaitingList } from '../../cache/PartyMatchWaitingList'
import { L2Party } from '../../models/L2Party'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestPartyMatchList( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let roomId = packet.readD(),
            maxMembers = packet.readD(),
            minLevel = packet.readD(),
            maxLevel = packet.readD(),
            lootType = packet.readD(),
            roomTitle = packet.readS()


    if ( roomId > 0 ) {
        return PartyMatchManager.updateRoom( player.getObjectId(), roomId, maxMembers, minLevel, maxLevel, lootType, roomTitle )
    }

    PartyMatchWaitingList.removePlayer( player )
    PartyMatchManager.createRoom( player, maxMembers, lootType, minLevel, maxLevel, roomTitle )

    let party : L2Party = player.getParty()
    if ( party ) {
        PartyMatchManager.addPlayersToRoom( player.getObjectId(), party.getMembers() )
    }
}