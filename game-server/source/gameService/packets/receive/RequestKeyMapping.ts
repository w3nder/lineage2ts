import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { ExUISetting } from '../send/ExUISetting'

export function RequestKeyMapping( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( ConfigManager.character.storeUISettings() ) {
        player.sendOwnedData( ExUISetting( player.getObjectId() ) )
    }
}