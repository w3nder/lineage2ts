import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PartyMatchManager, PartyMatchRoom } from '../../cache/partyMatchManager'
import { ExClosePartyRoom } from '../send/ExClosePartyRoom'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestWithdrawPartyRoom( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let roomId : number = new ReadableClientPacket( packetData ).readD()

    let room : PartyMatchRoom = PartyMatchManager.getRoom( roomId )
    if ( !room ) {
        return
    }

    let party = player.getParty()
    if ( party && party.getMembers().includes( room.leaderId ) ) {
        player.broadcastUserInfo()
        return
    }

    PartyMatchManager.removePlayer( player.getObjectId() )

    player.sendOwnedData( ExClosePartyRoom() )
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PARTY_ROOM_EXITED ) )
}