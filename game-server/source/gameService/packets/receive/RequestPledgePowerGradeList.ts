import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { PledgePowerGradeList } from '../send/PledgePowerGradeList'
import { FastRateLimit } from 'fast-ratelimit'
import { RankPrivileges } from '../../models/RankPrivileges'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function RequestPledgePowerGradeList( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    let privileges : Array<RankPrivileges> = clan.getAllRankPrivileges()
    player.sendOwnedData( PledgePowerGradeList( privileges ) )
}