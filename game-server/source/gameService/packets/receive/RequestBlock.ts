import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { BlocklistCache } from '../../cache/BlocklistCache'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

const enum BlockType {
    BLOCK = 0,
    UNBLOCK = 1,
    BLOCKLIST = 2,
    ALLBLOCK = 3,
    ALLUNBLOCK = 4,
}

export async function RequestBlock( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let type: number = packet.readD()
    let name: string

    if ( type === BlockType.BLOCK || type === BlockType.UNBLOCK ) {
        name = packet.readS()
    }

    ReadableClientPacketPool.recycleValue( packet )

    switch ( type ) {
        case BlockType.BLOCK:
        case BlockType.UNBLOCK:
            let targetId = await CharacterNamesCache.getIdByName( name )
            if ( !targetId ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_TO_REGISTER_TO_IGNORE_LIST ) )
                return
            }

            let targetAccessLevel = await CharacterNamesCache.getAccessLevelById( targetId )
            if ( targetAccessLevel > 0 ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_MAY_NOT_IMPOSE_A_BLOCK_ON_GM ) )
                return
            }

            if ( player.getObjectId() === targetId ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_EXCLUDE_YOURSELF ) )
                return
            }

            if ( type === BlockType.BLOCK ) {
                return BlocklistCache.addToPlayerBlockList( player, targetId )
            }

            return BlocklistCache.removeFromPlayerBlockList( player.getObjectId(), targetId )

        case BlockType.BLOCKLIST:
            return BlocklistCache.describeBlocklist( player.getObjectId() )

        case BlockType.ALLBLOCK:
            BlocklistCache.setMessageRefusal( player.getObjectId(), true )
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MESSAGE_REFUSAL_MODE ) )
            break

        case BlockType.ALLUNBLOCK:
            BlocklistCache.setMessageRefusal( player.getObjectId(), false )
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MESSAGE_ACCEPTANCE_MODE ) )
            break
    }
}