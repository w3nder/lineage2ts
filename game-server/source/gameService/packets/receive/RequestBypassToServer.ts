import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { ActionFailed } from '../send/ActionFailed'
import { L2Object } from '../../models/L2Object'
import { L2World } from '../../L2World'
import { L2NpcValues } from '../../values/L2NpcValues'
import { NpcHtmlMessage } from '../send/NpcHtmlMessage'
import { IAdminCommand } from '../../handler/IAdminCommand'
import { ConfirmationAction } from '../../enums/confirmationAction'
import { ConfirmDialog } from '../send/SystemMessage'
import { CommunityBoardManager } from '../../cache/CommunityBoardManager'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { HeroCache } from '../../cache/HeroCache'
import { BypassManager } from '../../handler/managers/BypassManager'
import { IBypassHandler } from '../../handler/IBypassHandler'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2Character } from '../../models/actor/L2Character'
import { AdminCommandManager } from '../../handler/managers/AdminCommandManager'
import { EventType, PlayerBypassEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ProximalDiscoveryManager } from '../../cache/ProximalDiscoveryManager'
import { HtmlActionCache } from '../../cache/HtmlActionCache'
import { ErrorHelper } from '../../helpers/ErrorHelper'
import { OlympiadArenaChange } from '../../handler/bypasshandlers/OlympiadObservation'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import _ from 'lodash'
import to from 'await-to-js'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestBypassToServer( client: GameClient, packetData: Buffer ): Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let encodedCommand: string = packet.readS()

    ReadableClientPacketPool.recycleValue( packet )

    if ( encodedCommand.length === 0 || encodedCommand.length > 255 ) {
        client.logout()
        return
    }

    const [ bypassOriginId, command ] = HtmlActionCache.validateHtmlAction( player.getObjectId(), encodedCommand )
    if ( bypassOriginId === -1 ) {
        return
    }

    // TODO : find out all possible commands and use set lookup without iteration, possibly integrate validation with bypasses directly
    let requiresBypassValidation = !_.some( BypassManager.getCommandsWithoutRangeValidation(), ( start: string ) => {
        return command.startsWith( start )
    } )

    if ( requiresBypassValidation
            && bypassOriginId > 0
            && !GeneralHelper.isInsideRangeOfObjectId( player, bypassOriginId, L2NpcValues.interactionDistance ) ) {
        return
    }

    let processCommand: Function = requiresBypassValidation ? processValidatedCommand : processNonValidatedCommand
    let [ error ] = await to( processCommand( player, command, bypassOriginId ) )

    if ( error && player.isGM() ) {
        let message: string = `Bypass command failed: <font color='LEVEL'>${ command }</font>`
        player.sendOwnedData( NpcHtmlMessage( ErrorHelper.formatError( message, error ), 0 ) )
    }

    if ( ListenerCache.hasGeneralListener( EventType.PlayerBypass ) ) {
        let eventData = EventPoolCache.getData( EventType.PlayerBypass ) as PlayerBypassEvent

        eventData.playerId = player.getObjectId()
        eventData.command = command
        eventData.bypassOriginId = bypassOriginId

        return ListenerCache.sendGeneralEvent( EventType.PlayerBypass, eventData )
    }
}

async function processNonValidatedCommand( player: L2PcInstance, command: string, bypassOriginId: number ): Promise<void> {
    if ( CommunityBoardManager.isCommunityBoardCommand( command ) ) {
        return CommunityBoardManager.onParseCommand( command, player )
    }

    if ( command === 'come_here' && player.isGM() ) {
        return processComeHereCommand( player )
    }

    if ( command.startsWith( '_match' ) ) {
        return processMatchCommand( command, player )
    }

    if ( command.startsWith( '_diary' ) ) {
        return processDiaryCommand( command, player )
    }

    if ( command.startsWith( '_olympiad?command' ) ) {
        return processOlympiadCommand( command, player )
    }

    if ( command.startsWith( 'manor_menu_select' ) && ConfigManager.general.allowManor() ) {
        return processManorSelectCommand( command, player )
    }

    let handler: IBypassHandler = BypassManager.getHandler( command )
    if ( handler ) {
        let originator: L2Object = L2World.getObjectById( bypassOriginId )
        await handler.onStart( command, player, originator && originator.isCharacter() ? originator as L2Character : null )
        return
    }
}

async function processValidatedCommand( player: L2PcInstance, command: string, bypassOriginId: number ): Promise<void> {
    if ( command.startsWith( 'admin_' ) ) {
        return processAdminCommand( player, command )
    }

    if ( command.startsWith( 'npc_' ) ) {
        return processNpcCommand( command, player )
    }

    if ( command.startsWith( 'item_' ) ) {
        return processItemCommand( command, player )
    }

    let isInteractableObject: boolean = bypassOriginId > 0 && ProximalDiscoveryManager.isDiscoveredBy( player.getObjectId(), bypassOriginId )
    let handler: IBypassHandler = BypassManager.getHandler( command )
    if ( handler ) {
        if ( isInteractableObject ) {
            let originator: L2Object = L2World.getObjectById( bypassOriginId )
            await handler.onStart( command, player, originator && originator.isCharacter() ? originator as L2Character : null )
            return
        }

        await handler.onStart( command, player, null )
        return
    }

    if ( isInteractableObject ) {
        let npc: L2Object = L2World.getObjectById( bypassOriginId )
        if ( npc && npc.isNpc() && player.isInsideRadius( npc, L2NpcValues.interactionDistance ) ) {
            await ( npc as L2Npc ).onBypassFeedback( player, command )
        }
    }

    return player.sendOwnedData( ActionFailed() )
}

async function processAdminCommand( player: L2PcInstance, command: string ): Promise<void> {
    if ( !player.getAccessLevel().canExecute( command ) ) {
        player.sendMessage( 'You don\'t have the access rights to use this command!' )
        return
    }

    let commandKey: string = command.split( ' ' )[ 0 ]
    let handler: IAdminCommand = AdminCommandManager.getHandler( commandKey )

    if ( !handler ) {
        if ( player.isGM() ) {
            player.sendMessage( `Admin command '${ commandKey }' does not exist!` )
        }

        return
    }

    if ( player.getAccessLevel().requiresConfirmation( commandKey ) ) {
        player.setAdminConfirmCommand( commandKey )

        player.addConfirmationAction( ConfirmationAction.AdminCommand )
        let packet = ConfirmDialog.fromText( `Are you sure you want execute command //${ commandKey } ?` )
        player.sendOwnedData( packet.getBuffer() )
        return
    }

    /*
        Stripping off admin_ string makes it easier to understand which command can be
        executed inside handler method. It also avoids usages of matching for start of string,
        which do not allow normal switch statement to happen.
     */
    return handler.onCommand( command.substring( 6 ), player )
}

function processComeHereCommand( player: L2PcInstance ): void {
    let target = player.getTarget() as L2Npc
    if ( !target ) {
        return
    }

    if ( target.isNpc() ) {
        target.setTarget( player )

        return AIEffectHelper.notifyMove( target, player )
    }
}

async function processNpcCommand( command: string, player: L2PcInstance ): Promise<void> {
    let endOfId = command.indexOf( '_', 5 )
    let id: string

    if ( endOfId > 0 ) {
        id = command.substring( 4, endOfId )
    } else {
        id = command.substring( 4 )
    }

    let objectId = _.parseInt( id )

    if ( _.isNumber( objectId ) ) {
        let npc: L2Object = L2World.getObjectById( objectId )

        if ( npc && npc.isNpc() && ( endOfId > 0 ) && player.isInsideRadius( npc, L2NpcValues.interactionDistance ) ) {
            await ( npc as L2Npc ).onBypassFeedback( player, command.substring( endOfId + 1 ) )
        }
    }

    return player.sendOwnedData( ActionFailed() )
}

async function processItemCommand( command: string, player: L2PcInstance ): Promise<void> {
    let endOfId = command.indexOf( '_', 5 )
    let id: string

    if ( endOfId > 0 ) {
        id = command.substring( 5, endOfId )
    } else {
        id = command.substring( 5 )
    }

    let objectId = _.parseInt( id )

    if ( _.isNumber( objectId ) ) {
        let item: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
        if ( item && endOfId > 0 ) {
            await item.onBypassFeedback( player, command.substring( endOfId + 1 ) )
        }

        player.sendOwnedData( ActionFailed() )
    }

    return
}

function processMatchCommand( command: string, player: L2PcInstance ): void {
    let parameters: string = command.substring( command.indexOf( '?' ) + 1 )
    let [ heroClass, heroPage ] = _.map( _.split( parameters, '&' ), ( parameter: string ) => {
        return _.last( _.split( parameter, '=' ) )
    } )

    let heroid = HeroCache.getHeroByClass( _.parseInt( heroClass ) )
    if ( heroid > 0 ) {
        // TODO : big html generation method there, needs to be moved to html template, prefer caching
        //HeroCache.showHeroFights(player, heroClass, heroid, heroPage)
    }

    return
}

function processDiaryCommand( command: string, player: L2PcInstance ): void {
    let parameters: string = _.last( _.split( command, '?' ) )
    let [ heroClassId, heroPage ] = _.map( _.split( parameters, '&' ), ( parameter: string ) => {
        return _.last( _.split( parameter, '=' ) )
    } )

    let heroId = HeroCache.getHeroByClass( _.parseInt( heroClassId ) )
    if ( heroId > 0 ) {
        // TODO : big html generation method, move to html template, prefer caching
        //HeroCache.showHeroDiary(player, heroClassId, heroId, heroPage)
    }

    return
}

async function processOlympiadCommand( command: string, player: L2PcInstance ): Promise<void> {
    let arenaId = _.parseInt( _.nth( command.split( '=' ), 2 ) )
    let handler: IBypassHandler = BypassManager.getHandler( OlympiadArenaChange )
    if ( handler ) {
        await handler.onStart( `arenachange ${ arenaId - 1 }`, player, null )
    }
}

function processManorSelectCommand( command: string, player: L2PcInstance ): Promise<void> {
    let lastNpc: L2Npc = L2World.getObjectById( player.getLastFolkNPC() ) as L2Npc
    if ( lastNpc
            && lastNpc.isNpc()
            && player.canInteract( lastNpc )
            && ListenerCache.hasNpcTemplateListeners( lastNpc.getId(), EventType.NpcManorBypass ) ) {

        let chunks: Array<string> = command.substring( command.indexOf( '?' ) + 1 ).split( '&' )
        let eventData = EventPoolCache.getData( EventType.NpcManorBypass )

        eventData.playerId = player.getObjectId()
        eventData.characterId = lastNpc.getObjectId()
        eventData.requestId = _.parseInt( chunks[ 0 ].split( '=' )[ 1 ] )
        eventData.manorId = _.parseInt( chunks[ 1 ].split( '=' )[ 1 ] )
        eventData.isNextPeriod = chunks[ 2 ].split( '=' )[ 1 ] === '1'

        return ListenerCache.sendNpcTemplateEvent( lastNpc.getId(), EventType.NpcManorBypass, eventData )
    }
}