import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ActionFailed } from '../send/ActionFailed'
import { L2Henna } from '../../models/items/L2Henna'
import { DataManager } from '../../../data/manager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { recordItemViolation } from '../../helpers/PlayerViolations'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestHennaEquip( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.getHennaEmptySlots() === 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SYMBOLS_FULL ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    let symbolId: number = new ReadableClientPacket( packetData ).readD()

    let henna: L2Henna = DataManager.getHennas().get( symbolId )
    if ( !henna ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let amount = player.getInventory().getInventoryItemCount( henna.getDyeItemId(), -1 )
    if ( henna.isAllowedClass( player.getClassId() )
            && amount >= henna.getWearCount()
            && player.getAdena() >= henna.getWearFee()
            && player.addHenna( henna ) ) {

        await player.destroyItemByItemId( henna.getDyeItemId(), henna.getWearCount(), true, 'RequestHennaEquip' )
        await player.getInventory().reduceAdena( henna.getWearFee(), 'RequestHennaEquip' )

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SYMBOL_ADDED ) )
        return
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_DRAW_SYMBOL ) )
    player.sendOwnedData( ActionFailed() )

    return recordItemViolation(
            player.getObjectId(),
            '3829a863-203c-417d-a918-a1b78ce83710',
            'Player fails to add henna',
            henna.getDyeItemId(),
            henna.getWearCount() )
}