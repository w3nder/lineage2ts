import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { EventType, PlayerChatEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { L2World } from '../../L2World'
import { IChatHandler } from '../../handler/IChatHandler'
import { ChatTypeManager } from '../../handler/managers/ChatTypeManager'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import _ from 'lodash'
import { FastRateLimit } from 'fast-ratelimit'
import { recordReceivedPacketViolation } from '../../helpers/PlayerViolations'
import { NpcSayType } from '../../enums/packets/NpcSayType'

const linkedItemIdPattern = new RegExp( /ID=(\d+)/g )

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

const jailedChatTypes = new Set<number>( [ NpcSayType.Tell, NpcSayType.Shout, NpcSayType.Trade, NpcSayType.HeroVoice ] )

export async function Say2( client: GameClient, packetData: Buffer ) : Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DONT_SPAM ) )
    }

    let packet = new ReadableClientPacket( packetData )
    let text : string = packet.readSLimit( ConfigManager.general.getChatTextLimit() + 1 ),
            type : number = packet.readD()

    if ( type < 0
            || type >= NpcSayType.NpcShout
            || text.length === 0 ) {
        client.logout()
        return
    }

    if ( !player.isGM() && text.length >= ConfigManager.general.getChatTextLimit() ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.DONT_SPAM ) )
    }

    if ( player.isCursedWeaponEquipped() && ( ( type === NpcSayType.Trade ) || ( type === NpcSayType.Shout ) ) ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SHOUT_AND_TRADE_CHAT_CANNOT_BE_USED_WHILE_POSSESSING_CURSED_WEAPON ) )
    }

    if ( player.isChatBanned() ) { // L2J has also  && (text.charAt(0) !== '.'), however many chat handles check ban channels directly
        if ( player.getEffectList().getFirstEffect( L2EffectType.ChatBlock ) ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_BEEN_REPORTED_SO_CHATTING_NOT_ALLOWED ) )
        }

        if ( ConfigManager.general.getBanChatChannels().has( type ) ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CHATTING_IS_CURRENTLY_PROHIBITED ) )
        }

        return
    }

    if ( player.isJailed()
            && ConfigManager.general.jailDisableChat()
            && jailedChatTypes.has( type ) ) {
        return player.sendMessage( 'You can not chat with players outside of the jail.' )
    }

    if ( text.indexOf( '\b\t' ) >= 0 && !markItemsAsPublished( text, player ) ) {
        return
    }

    let targetName : string

    if ( type === NpcSayType.Tell && packet.hasMoreData() ) {
        targetName = packet.readSLimit( 50 )
    }

    let targetPlayer = L2World.getPlayerByName( targetName )
    if ( ConfigManager.general.useChatFilter() ) {
        applyChatFilter( text )
    }

    let handler : IChatHandler = ChatTypeManager.getHandler( type )
    if ( handler ) {
        await handler.applyMessage( text, type, player, targetPlayer )
    } else {
        await recordReceivedPacketViolation( player.getObjectId(), 'a585c146-746d-4779-b2b5-f78114cb8f2f', 'Player used un-handled chat type', Say2.name, type )
    }

    if ( ListenerCache.hasGeneralListener( EventType.PlayerChat ) ) {
        let data = EventPoolCache.getData( EventType.PlayerChat ) as PlayerChatEvent

        data.playerId = player.getObjectId()
        data.targetId = targetPlayer ? targetPlayer.getObjectId() : 0
        data.text = text
        data.type = type

        return ListenerCache.sendGeneralEvent( EventType.PlayerChat, data, type )
    }
}

function applyChatFilter( text : string ) {
    ConfigManager.general.getChatFilters().forEach( ( pattern: RegExp ) => {
        text = text.replace( pattern, ConfigManager.general.getChatFilterCharacters() )
    } )
}

function markItemsAsPublished( text: string, player: L2PcInstance ) : boolean {
    let idChunks : Array<string> = _.take( text.match( linkedItemIdPattern ), ConfigManager.general.getChatLinkedItemLimit() )

    return !idChunks.some( ( id: string ) : boolean => {
        let currentObjectId : number = _.parseInt( id.split( '=' )[ 1 ] )
        let item : L2ItemInstance = player.getInventory().getItemByObjectId( currentObjectId )
        if ( item ) {
            ( item as L2ItemInstance ).markPublished()
            return false
        }

        return true
    } )
}