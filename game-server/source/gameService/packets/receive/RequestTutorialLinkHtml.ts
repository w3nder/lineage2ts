import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { EventType, PlayerTutorialLinkEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { FastRateLimit } from 'fast-ratelimit'
import { HtmlActionCache } from '../../cache/HtmlActionCache'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestTutorialLinkHtml( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( ListenerCache.hasGeneralListener( EventType.PlayerTutorialLink ) ) {
        let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
        let action = packet.readS()
        ReadableClientPacketPool.recycleValue( packet )

        const [ bypassOriginId, command ] = HtmlActionCache.validateHtmlAction( player.getObjectId(), action )

        if ( bypassOriginId === -1 || !command ) {
            return
        }

        let data = EventPoolCache.getData( EventType.PlayerTutorialLink ) as PlayerTutorialLinkEvent

        data.playerId = player.getObjectId()
        data.command = command

        return ListenerCache.sendGeneralEvent( EventType.PlayerTutorialLink, data )
    }
}