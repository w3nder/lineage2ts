import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ActionFailed } from '../send/ActionFailed'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemType2 } from '../../enums/items/ItemType2'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2EffectType } from '../../enums/effects/L2EffectType'
import { FortSiegeManager } from '../../instancemanager/FortSiegeManager'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { L2Weapon } from '../../models/items/L2Weapon'
import { Race } from '../../enums/Race'
import { WeaponType } from '../../models/items/type/WeaponType'
import { ExUseSharedGroupItem } from '../send/ExUseSharedGroupItem'
import { ArmorType } from '../../enums/items/ArmorType'
import { ItemHandlerMethod } from '../../handler/ItemHandler'
import { InventorySlot } from '../../values/InventoryValues'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { EffectsCache } from '../../aicontroller/EffectCache'
import { AIEffect, AIInteractTargetEvent } from '../../aicontroller/enums/AIEffect'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { FastRateLimit } from 'fast-ratelimit'
import { Skill } from '../../models/Skill'
import { L2ItemSlots } from '../../enums/L2ItemSlots'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

const formalWareId = 6408

export async function UseItem( client: GameClient, packetData: Buffer ): Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.isStunned()
            || player.isSleeping()
            || player.isAfraid()
            || player.isAlikeDead() ) {
        return
    }

    if ( player.getActiveTradeList() ) {
        player.cancelActiveTrade()
    }

    if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_TRADE_DISCARD_DROP_ITEM_WHILE_IN_SHOPMODE ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let objectId = packet.readD(), controlKey = packet.readD()

    ReadableClientPacketPool.recycleValue( packet )

    let item: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    if ( !item ) {
        return
    }

    if ( item.getItem().getType2() === ItemType2.QUEST ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_USE_QUEST_ITEMS ) )
        return
    }

    let itemId = item.getId()

    if ( player.isDead() || !player.getInventory().canManipulateWithItemId( itemId ) ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                .addItemInstanceName( item )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    if ( player.isFishing() && ( itemId < 6535 || itemId > 6540 ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DO_WHILE_FISHING_3 ) )
        return
    }

    if ( !ConfigManager.character.karmaPlayerCanTeleport() && player.getKarma() > 0 ) {
        let shouldExit = item.getItem().getSkills().some( ( skill : Skill ) => {
            return skill.hasEffectType( L2EffectType.Teleport )
        } )

        if ( shouldExit ) {
            return
        }
    }

    let reuseDelay = item.getReuseDelay()
    let sharedReuseGroup = item.getSharedReuseGroup()
    if ( reuseDelay > 0 ) {
        let reuse = player.getItemRemainingReuseTime( item )
        if ( reuse > 0 ) {
            sendReuseTime( player, item, reuse )
            sendSharedGroupUpdate( player, itemId, sharedReuseGroup, reuse, reuseDelay )
            return
        }

        let reuseOnGroup = player.getReuseDelayOnGroup( sharedReuseGroup )
        if ( reuseOnGroup > 0 ) {
            sendReuseTime( player, item, reuseOnGroup )
            sendSharedGroupUpdate( player, itemId, sharedReuseGroup, reuseOnGroup, reuseDelay )
            return
        }
    }

    if ( item.isEquipable() ) {
        if ( player.isCursedWeaponEquipped() && itemId === formalWareId ) {
            return
        }

        if ( FortSiegeManager.isCombatFlag( itemId ) ) {
            return
        }

        if ( player.isCombatFlagEquipped() ) {
            return
        }

        switch ( item.getItem().getBodyPart() ) {
            case L2ItemSlots.AllHandSlots:
            case L2ItemSlots.LeftHand:
            case L2ItemSlots.RightHand:
                if ( player.getActiveWeaponItem() && player.getActiveWeaponItem().getId() === 9819 ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_EQUIP_ITEM_DUE_TO_BAD_CONDITION ) )
                    return
                }

                if ( player.isMounted() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_EQUIP_ITEM_DUE_TO_BAD_CONDITION ) )
                    return
                }
                if ( player.isDisarmed() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_EQUIP_ITEM_DUE_TO_BAD_CONDITION ) )
                    return
                }

                if ( player.isCursedWeaponEquipped() ) {
                    return
                }

                if ( !item.isEquipped() && item.isWeapon() && !player.hasActionOverride( PlayerActionOverride.ItemAction ) ) {
                    let weaponItem: L2Weapon = item.getItem() as L2Weapon

                    switch ( player.getRace() ) {
                        case Race.KAMAEL:
                            if ( weaponItem.getItemType() === WeaponType.NONE ) {
                                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_EQUIP_ITEM_DUE_TO_BAD_CONDITION ) )
                                return
                            }
                            break
                        case Race.HUMAN:
                        case Race.DWARF:
                        case Race.ELF:
                        case Race.DARK_ELF:
                        case Race.ORC:
                            switch ( weaponItem.getItemType() ) {
                                case WeaponType.RAPIER:
                                case WeaponType.CROSSBOW:
                                case WeaponType.ANCIENTSWORD:
                                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_EQUIP_ITEM_DUE_TO_BAD_CONDITION ) )
                                    return
                            }
                    }
                }

                break

            case L2ItemSlots.Chest:
            case L2ItemSlots.Back:
            case L2ItemSlots.Gloves:
            case L2ItemSlots.Feet:
            case L2ItemSlots.Head:
            case L2ItemSlots.FullArmor:
            case L2ItemSlots.Legs:
                if ( ( player.getRace() === Race.KAMAEL )
                        && item.isArmor()
                        && ( ( item.getItem().getItemType() === ArmorType.HEAVY ) || ( item.getItem().getItemType() === ArmorType.MAGIC ) ) ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_EQUIP_ITEM_DUE_TO_BAD_CONDITION ) )
                    return
                }

                break

            case L2ItemSlots.Decoration:
                if ( !item.isEquipped() && player.getTalismanSlots() === 0 ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_EQUIP_ITEM_DUE_TO_BAD_CONDITION ) )
                    return
                }

                break
        }

        if ( !item.isEquipped() && !item.getItem().checkEquipConditions( player, true, SystemMessageIds.CANNOT_EQUIP_ITEM_DUE_TO_BAD_CONDITION ) ) {
            return
        }

        if ( player.isCastingNow() || player.isCastingSimultaneouslyNow() ) {
            player.setAITraitAction( () => {
                let data : AIInteractTargetEvent = {
                    targetId: objectId,
                    shouldInteract: true
                }

                EffectsCache.setParameters( player.getObjectId(), AIEffect.InteractTarget, data )

                return AIIntent.INTERACT
            } )

            return
        }

        if ( player.isAttackingNow() ) {
            // TODO : integrated with stored action that is executed after attack is finished
            player.setAIEffectAction( () => player.useEquippableItem( objectId, false ), AIEffect.ReadyToAttack )
            return
        }

        return player.useEquippableItem( objectId, true )
    }

    let weaponItem: L2Weapon = player.getActiveWeaponItem()
    if ( ( weaponItem && ( weaponItem.getItemType() === WeaponType.FISHINGROD ) )
            && ( ( ( itemId >= 6519 ) && ( itemId <= 6527 ) )
                    || ( ( itemId >= 7610 ) && ( itemId <= 7613 ) )
                    || ( ( itemId >= 7807 ) && ( itemId <= 7809 ) )
                    || ( ( itemId >= 8484 ) && ( itemId <= 8486 ) )
                    || ( ( itemId >= 8505 ) && ( itemId <= 8513 ) ) ) ) {
        await player.getInventory().setPaperdollItem( InventorySlot.LeftHand, item )
        player.broadcastUserInfo()
        return
    }

    if ( !item.isEtcItem() ) {
        return
    }

    let handler : ItemHandlerMethod = item.getEtcItem().getItemHandler()
    if ( !handler ) {
        return
    }

    const isControlPressed = controlKey !== 0
    if ( await handler( player, item, isControlPressed ) ) {
        if ( reuseDelay > 0 ) {
            player.addTimestampItem( item, reuseDelay )
            sendSharedGroupUpdate( player, itemId, sharedReuseGroup, reuseDelay, reuseDelay )
        }
    }
}

function sendReuseTime( player: L2PcInstance, item: L2ItemInstance, remainingTime: number ): void {
    let hours = Math.floor( remainingTime / 3600000 )
    let minutes = Math.floor( ( remainingTime % 3600000 ) / 60000 )
    let seconds = Math.floor( ( remainingTime / 1000 ) % 60 )

    if ( hours > 0 ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.S2_HOURS_S3_MINUTES_S4_SECONDS_REMAINING_FOR_REUSE_S1 )
                .addItemInstanceName( item )
                .addNumber( hours )
                .addNumber( minutes )
                .addNumber( seconds )
                .getBuffer()

        return player.sendOwnedData( packet )
    }

    if ( minutes > 0 ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.S2_MINUTES_S3_SECONDS_REMAINING_FOR_REUSE_S1 )
                .addItemInstanceName( item )
                .addNumber( minutes )
                .addNumber( seconds )
                .getBuffer()

        return player.sendOwnedData( packet )
    }

    let packet = new SystemMessageBuilder( SystemMessageIds.S2_SECONDS_REMAINING_FOR_REUSE_S1 )
            .addItemInstanceName( item )
            .addNumber( seconds )
            .getBuffer()

    return player.sendOwnedData( packet )
}

function sendSharedGroupUpdate( player: L2PcInstance, itemId: number, group: number, remainingTime: number, expirationTime: number ) {
    if ( group <= 0 ) {
        return
    }

    return player.sendOwnedData( ExUseSharedGroupItem( itemId, group, remainingTime, expirationTime ) )
}