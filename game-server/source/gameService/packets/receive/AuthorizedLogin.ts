import { GameClient } from '../../GameClient'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export const enum AuthorizeLoginLanguage {
    English = 1,
    Russian = 8
}

export interface AuthorizedLoginData {
    loginName: string
    playKey1: number
    playKey2: number
    loginKey1: number
    loginKey2: number
    languageCode: AuthorizeLoginLanguage
}
export function AuthorizedLogin( client: GameClient, packetData: Buffer ) : AuthorizedLoginData {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let loginName = packet.readS().toLowerCase(),
            playKey2 = packet.readD(),
            playKey1 = packet.readD(),
            loginKey1 = packet.readD(),
            loginKey2 = packet.readD(),
        languageCode = packet.readD()

    return {
        loginName,
        playKey1,
        playKey2,
        loginKey1,
        loginKey2,
        languageCode
    }
}