import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { LifeStone, LifeStoneCache } from '../../cache/LifeStoneCache'
import { ExPutCommissionResultForVariationMake } from '../send/ExPutCommissionResultForVariationMake'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { canAugmentItem } from '../../helpers/ItemHelper'

export function RequestConfirmGemStone( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let targetId = packet.readD()

    let targetItem: L2ItemInstance = player.getInventory().getItemByObjectId( targetId )
    if ( !targetItem ) {
        return
    }

    let refinerId = packet.readD()
    let refinerItem: L2ItemInstance = player.getInventory().getItemByObjectId( refinerId )
    if ( !refinerItem ) {
        return
    }

    let gemstoneId = packet.readD()
    let gemStoneItem: L2ItemInstance = player.getInventory().getItemByObjectId( gemstoneId )
    if ( !gemStoneItem ) {
        return
    }

    if ( !canAugmentItem( player, targetItem, refinerItem, gemStoneItem ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_IS_NOT_A_SUITABLE_ITEM ) )
        return
    }

    let lifeStone: LifeStone = LifeStoneCache.getLifeStone( refinerItem.getId() )
    if ( !lifeStone ) {
        return
    }

    let count = packet.readQ()
    if ( count !== LifeStoneCache.getGemStoneCount( targetItem.getItem().getItemGrade(), lifeStone ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.GEMSTONE_QUANTITY_IS_INCORRECT ) )
        return
    }

    player.sendOwnedData( ExPutCommissionResultForVariationMake( gemstoneId, count, gemStoneItem.getId() ) )
}