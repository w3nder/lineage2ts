import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { RestartResponse } from '../send/RestartResponse'
import { FastRateLimit } from 'fast-ratelimit'
import { GameClientState } from '../../enums/GameClientState'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export async function RequestRestart( client: GameClient ): Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( ( player.getActiveEnchantItemId() !== L2PcInstanceValues.EmptyId )
            || ( player.getActiveEnchantAttributeItemId() !== L2PcInstanceValues.EmptyId ) ) {
        player.sendOwnedData( RestartResponse( false ) )
        return
    }

    if ( player.isLocked() ) {
        player.sendOwnedData( RestartResponse( false ) )
        return
    }

    if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
        player.sendMessage( 'Cannot restart while trading' )
        player.sendOwnedData( RestartResponse( false ) )
        return
    }

    if ( player.isUnderAttack() && !( player.isGM() && ConfigManager.general.gmRestartFighting() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_RESTART_WHILE_FIGHTING ) )
        player.sendOwnedData( RestartResponse( false ) )
        return
    }

    if ( player.isBlockedFromExit() ) {
        player.sendOwnedData( RestartResponse( false ) )
        return
    }

    await player.deleteMe()

    client.setPlayer( null )
    client.setState( GameClientState.Verified )
    client.sendPacket( RestartResponse( true ) )

    await client.updateCharacterSelection()
    client.sendCharacterSelection()
}