import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExBuySellList } from '../send/ExBuySellList'
import { PacketVariables } from '../PacketVariables'
import { ActionFailed } from '../send/ActionFailed'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2Character } from '../../models/actor/L2Character'
import { L2NpcValues } from '../../values/L2NpcValues'
import { L2BuyList } from '../../models/buylist/L2BuyList'
import { BuyListManager } from '../../cache/BuyListManager'
import { Product } from '../../models/buylist/Product'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2MerchantInstance } from '../../models/actor/instance/L2MerchantInstance'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import aigle from 'aigle'
import { FastRateLimit } from 'fast-ratelimit'
import { recordBuySellViolation } from '../../helpers/PlayerViolations'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'
import { ObjectPool } from '../../helpers/ObjectPoolHelper'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

interface BuyItem {
    itemId: number
    count: number
}

const BuyItemObjectPool = new ObjectPool<BuyItem>( 'RequestBuyItem', () : BuyItem => {
    return {
        count: 0,
        itemId: 0
    }
} )

export async function RequestBuyItem( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendMessage( 'You are buying too fast.' )
    }

    if ( !ConfigManager.character.karmaPlayerCanShop() && ( player.getKarma() > 0 ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    let packet = new ReadableClientPacket( packetData )
    let listId = packet.readD(), totalItems = packet.readD()

    let buyList: L2BuyList = BuyListManager.getBuyList( listId )
    if ( !buyList ) {
        await recordBuySellViolation( player.getObjectId(), '0e986ae2-5dc7-4e2c-85a0-d5e2f9191763', 'Player specified non-existent buylistId', listId )
        return player.sendOwnedData( ActionFailed() )
    }

    if ( ( totalItems < 1 ) || ( totalItems > PacketVariables.maximumItemsLimit ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let merchant: L2Character = player.getTarget() as L2Character
    if ( !player.isGM() && (
        !merchant
        || !merchant.isMerchant()
        || !player.isInsideRadius( merchant, L2NpcValues.interactionDistance, true )
        || player.getInstanceId() !== merchant.getInstanceId() ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( merchant && !buyList.isNpcAllowed( merchant.getId() ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let items: Array<BuyItem> = []

    while ( totalItems > 0 ) {
        let itemId = packet.readD(), count = packet.readQ()

        if ( count < 1 || itemId < 1 ) {
            BuyItemObjectPool.recycleValues( items )
            return player.sendOwnedData( ActionFailed() )
        }

        let buyItem = BuyItemObjectPool.getValue()

        buyItem.count = count
        buyItem.itemId = itemId

        items.push( buyItem )

        totalItems--
    }

    await processBuyItems( player, merchant as L2MerchantInstance, buyList, items )

    BuyItemObjectPool.recycleValues( items )
}

async function processBuyItems( player: L2PcInstance, merchant: L2MerchantInstance, buyList: L2BuyList, items: Array<BuyItem> ) : Promise<void> {
    let castleTaxRate = 0
    let baseTaxRate = 0

    if ( merchant ) {
        if ( merchant.isMerchant() ) {
            let config = merchant.getPriceConfig()
            castleTaxRate = config.getCastleTaxRate()
            baseTaxRate = config.getBaseTaxRate()
        } else {
            baseTaxRate = 0.5
        }
    }

    let subTotal = 0
    let slots = 0
    let weight = 0
    let maxAdena = getMaxAdena()

    let shouldExit = items.some( ( buyItem: BuyItem ): boolean => {
        let product: Product = buyList.getProductByItemId( buyItem.itemId )
        if ( !product ) {
            recordBuySellViolation( player.getObjectId(), '650205bd-5841-4181-8d2a-26368c3e2740', 'Player specified non-existent product id with buylistId', buyList.listId, buyItem.itemId )
            return true
        }

        if ( !product.getItem().isStackable() && ( buyItem.count > 1 ) ) {
            recordBuySellViolation( player.getObjectId(), '365eee51-1882-444e-b6ba-e0f1c631d760', 'Player specified to buy more than one of non-stackable product from buylist', buyList.listId, buyItem.itemId, buyItem.count )
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_EXCEEDED_QUANTITY_THAT_CAN_BE_INPUTTED ) )
            return true
        }

        let price = product.getComputedPrice()
        if ( price < 0 ) {
            player.sendOwnedData( ActionFailed() )
            return true
        }

        if ( !price && !player.isGM() && ConfigManager.general.onlyGMItemsFree() ) {
            recordBuySellViolation( player.getObjectId(), 'cef8a0d9-4cf2-463c-869f-00828135c198', 'Non-GM player buys items for zero adena', buyList.listId, buyItem.itemId, buyItem.count )
            return true
        }

        if ( product.hasLimitedStock() && buyItem.count > product.count ) {
            player.sendOwnedData( ActionFailed() )
            return true
        }

        if ( ( maxAdena / buyItem.count ) < price ) {
            recordBuySellViolation( player.getObjectId(), '84e2fd70-9bb0-45a5-8e72-da44b3267d36', 'Player tries to buy items totaling more than maximum adena allowed', buyList.listId, buyItem.itemId, buyItem.count, maxAdena )
            return true
        }

        price = Math.floor( price * ( 1 + castleTaxRate + baseTaxRate ) )
        subTotal += buyItem.count * price

        if ( subTotal > maxAdena ) {
            recordBuySellViolation( player.getObjectId(), 'f21586ed-e82f-4c8d-a3cb-9a694837bb2e', 'Player tries to buy items worth more than maximum allowed adena', buyList.listId, buyItem.itemId, buyItem.count )
            return true
        }

        weight += buyItem.count * product.getItem().getWeight()
        if ( !player.getInventory().getItemByItemId( product.getItemId() ) ) {
            slots++
        }

        return false
    } )

    if ( shouldExit ) {
        return
    }

    if ( !player.isGM() ) {
        if ( weight < 0 || !player.getInventory().validateWeight( weight ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WEIGHT_LIMIT_EXCEEDED ) )
            player.sendOwnedData( ActionFailed() )
            return
        }

        if ( slots < 0 || !player.getInventory().validateCapacity( slots ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SLOTS_FULL ) )
            player.sendOwnedData( ActionFailed() )
            return
        }
    }

    if ( subTotal < 0 || !( await player.reduceAdena( subTotal, true, 'RequestBuyItem' ) ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    await aigle.resolve( items ).eachSeries( async ( buyItem: BuyItem ) => {
        let product: Product = buyList.getProductByItemId( buyItem.itemId )

        if ( !product.hasLimitedStock() ) {
            return player.getInventory().addItem( buyItem.itemId, buyItem.count, merchant ? merchant.getObjectId() : 0, 'RequestBuyItem' )
        }

        /*
            While it is possible to check if product quantity is available, due to async nature of other players interacting
            with buylist items, quantity cannot be locked for single player. Hence, we must check if quantity did not change and only
            then approve item transfer to inventory. Otherwise, we risk minor item exploit where two or more players can buy more
            items than available per product buylist.
         */
        if ( product.decreaseCount( buyItem.count ) ) {
            return player.getInventory().addItem( buyItem.itemId, buyItem.count, merchant ? merchant.getObjectId() : 0, 'RequestBuyItem' )
        }
    } )

    if ( merchant && merchant.isMerchant() ) {
        ( merchant as L2MerchantInstance ).getCastle().addToTreasury( Math.floor( subTotal * castleTaxRate ) )
    }

    player.sendOwnedData( ExBuySellList( player, true ) )
}