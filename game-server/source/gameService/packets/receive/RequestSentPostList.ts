import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { ExShowSentPostList } from '../send/ExShowSentPostList'

export async function RequestSentPostList( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player || !ConfigManager.general.allowMail() ) {
        return
    }

    player.sendOwnedData( ExShowSentPostList( player.getObjectId() ) )
}