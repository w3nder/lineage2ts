import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ActionFailed } from '../send/ActionFailed'
import { TradeDone, TradeDoneStatus } from '../send/TradeDone'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerPermission } from '../../enums/PlayerPermission'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function AnswerTradeRequest( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    let partner: L2PcInstance = player.getActiveRequester()
    if ( !partner ) {
        player.sendOwnedData( TradeDone( TradeDoneStatus.Reject ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_NOT_FOUND_IN_THE_GAME ) )
        player.setActiveRequester( null )
        return
    }

    let response: number = new ReadableClientPacket( packetData ).readD()
    if ( response === 1 && !partner.isRequestExpired() ) {
        player.startTrade( partner )
    } else {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_DENIED_TRADE_REQUEST )
                .addPlayerCharacterName( partner )
                .getBuffer()
        partner.sendOwnedData( packet )
    }

    player.setActiveRequester( null )
    partner.onTransactionResponse()
}