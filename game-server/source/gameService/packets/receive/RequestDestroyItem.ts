import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { CursedWeaponManager } from '../../instancemanager/CursedWeaponManager'
import { DatabaseManager } from '../../../database/manager'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { recordItemViolation } from '../../helpers/PlayerViolations'
import { FastRateLimit } from 'fast-ratelimit'
import { L2World } from '../../L2World'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestDestroyItem( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendMessage( 'You are destroying items too fast.' )
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let objectId = packet.readD(), count = packet.readQ()

    ReadableClientPacketPool.recycleValue( packet )

    if ( count <= 0 ) {
        return
    }

    if ( player.isProcessingTransaction() || ( player.getPrivateStoreType() !== PrivateStoreType.None ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_TRADE_DISCARD_DROP_ITEM_WHILE_IN_SHOPMODE ) )
        return
    }

    let existingItem = L2World.getObjectById( objectId )
    if ( !existingItem ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_THIS_ITEM ) )
        return recordItemViolation(
            player.getObjectId(),
            '633afdac-72c5-4b0d-a06e-cff832a2c2e1',
            'Player attempts to destroy non-existing item',
            0,
            count,
            objectId )
    }

    let itemToRemove: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )

    if ( !itemToRemove || itemToRemove !== existingItem ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_THIS_ITEM ) )
        return recordItemViolation(
                player.getObjectId(),
                'e3d23d35-03f9-45b9-94dd-44f627080c25',
                'Player attempts to destroy non-owned item',
                0,
                count,
                objectId )
    }

    if ( player.isCastingNow() && player.getMainCast().skill.getItemConsumeId() === itemToRemove.getId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_THIS_ITEM ) )
        return
    }

    if ( player.isCastingSimultaneouslyNow() && player.getSimultaneousCast().skill.getItemConsumeId() === itemToRemove.getId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISCARD_THIS_ITEM ) )
        return
    }

    if ( ( !player.hasActionOverride( PlayerActionOverride.DestroyItem ) && !itemToRemove.isDestroyable() )
            || CursedWeaponManager.isCursedItem( itemToRemove.getId() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( itemToRemove.isHeroItem() ? SystemMessageIds.HERO_WEAPONS_CANT_DESTROYED : SystemMessageIds.CANNOT_DISCARD_THIS_ITEM ) )
        return
    }

    if ( !itemToRemove.isStackable() && count > 1 ) {
        return recordItemViolation(
                player.getObjectId(),
                '70fec580-8c98-4642-adba-9b497a1dd81a',
                'Player attempts to destroy non-stackable item with amount bigger than one',
                itemToRemove.getId(),
                count,
                itemToRemove.getObjectId() )
    }

    if ( !player.getInventory().canManipulateWithItemId( itemToRemove.getId() ) ) {
        player.sendMessage( 'You cannot use this item.' )
        return
    }

    if ( count > itemToRemove.getCount() ) {
        count = itemToRemove.getCount()
    }

    if ( itemToRemove.getItem().isPetItem() ) {
        if ( player.hasSummon() && ( player.getSummon().getControlObjectId() === objectId ) ) {
            await player.getSummon().unSummon( player )
        }

        await DatabaseManager.getPets().deleteByControlId( objectId )
    }

    if ( itemToRemove.isTimeLimitedItem() ) {
        await itemToRemove.endOfLife()
    }

    if ( itemToRemove.isEquipped() ) {
        if ( itemToRemove.getEnchantLevel() > 0 ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.EQUIPMENT_S1_S2_REMOVED )
                    .addNumber( itemToRemove.getEnchantLevel() )
                    .addItemInstanceName( itemToRemove )
                    .getBuffer()
            player.sendOwnedData( packet )
        } else {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISARMED )
                    .addItemInstanceName( itemToRemove )
                    .getBuffer()
            player.sendOwnedData( packet )
        }

        await player.getInventory().unEquipItemInSlot( itemToRemove.getLocationSlot() )
    }

    await player.getInventory().destroyItem( itemToRemove, count, 0, 'RequestDestroyItem' )
}