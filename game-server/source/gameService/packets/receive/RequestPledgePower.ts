import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ClanPriviledgeHelper } from '../../models/ClanPrivilege'
import { ManagePledgePower } from '../send/ManagePledgePower'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestPledgePower( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan = player.getClan()
    if ( !clan ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let rank : number = packet.readD(), action : number = packet.readD()

    if ( action === 2 ) {
        if ( clan.getLeaderId() === player.getObjectId() ) {
            let privileges : number = packet.readD()

            if ( rank === 9 ) {
                privileges = ClanPriviledgeHelper.add( privileges,
                    ClanPrivilege.WarehouseAccess,
                    ClanPrivilege.ClanHallOpenDoors,
                    ClanPrivilege.CastleOpenDoors )
            }

            await clan.setRankPrivileges( rank, privileges )
        }

        return
    }

    if ( action === 1 ) {
        player.sendOwnedData( ManagePledgePower( clan.getRankPrivileges( rank ) ) )
    }
}