import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { PetItemList } from '../send/PetItemList'
import { ItemHandlerMethod } from '../../handler/ItemHandler'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { PetInfoAnimation } from '../send/PetInfo'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestPetUseItem( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !player.hasPet() ) {
        return
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let objectId : number = packet.readD()

    ReadableClientPacketPool.recycleValue( packet )

    let pet: L2PetInstance = player.getSummon() as L2PetInstance
    let item: L2ItemInstance = pet.getInventory().getItemByObjectId( objectId )
    if ( !pet || !item ) {
        return
    }

    if ( !item.getItem().isForNpc() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_CANNOT_USE_ITEM ) )
        return
    }

    if ( player.isAlikeDead() || pet.isDead() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                .addItemInstanceName( item )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    let reuseDelay = item.getReuseDelay()
    if ( reuseDelay > 0 ) {
        let time = pet.getItemRemainingReuseTime( item )
        if ( time > 0 ) {
            return
        }
    }

    if ( item.isEquipable() ) {
        if ( item.isEquipped() ) {
            await pet.getInventory().unEquipItemInSlot( item.getLocationSlot() )
        } else {
            if ( item.getItem().hasEquipConditions() && !item.getItem().checkEquipConditions( pet, false ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_CANNOT_USE_ITEM ) )
                return
            }

            await pet.getInventory().equipItem( item )
        }

        player.sendOwnedData( PetItemList( pet ) )
        return pet.updateAndBroadcastStatus( PetInfoAnimation.StatUpgrade )
    }

    if ( item.getItem().hasUseConditions() && !item.getItem().checkUseConditions( pet, true ) ) {
        return
    }

    if ( item.isEtcItem() ) {
        let handler : ItemHandlerMethod = item.getEtcItem().getItemHandler()
        if ( handler ) {
            if ( await handler( pet, item, false ) ) {
                let reuseDelay = item.getReuseDelay()
                if ( reuseDelay > 0 ) {
                    player.addTimestampItem( item, reuseDelay )
                }

                return pet.updateAndBroadcastStatus( PetInfoAnimation.StatUpgrade )
            }

            return
        }
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_CANNOT_USE_ITEM ) )
}