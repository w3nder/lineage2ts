import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function RequestBuySellUIClose( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player || player.isInventoryDisabled() ) {
        return
    }

    // TODO : do we really need ? player.sendOwnedData( ItemList( player.getObjectId(), true ) )
}