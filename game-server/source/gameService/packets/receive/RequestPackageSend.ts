import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2World } from '../../L2World'
import { L2NpcValues } from '../../values/L2NpcValues'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { PcFreight } from '../../models/itemcontainer/PcFreight'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ItemTypes } from '../../values/InventoryValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import aigle from 'aigle'
import { recordGeneralViolation } from '../../helpers/PlayerViolations'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestPackageSend( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendMessage( 'You depositing items too fast.' )
    }

    let manager : L2Npc = L2World.getObjectById( player.getLastFolkNPC() ) as L2Npc
    if ( !manager || !player.isInsideRadius( manager, L2NpcValues.interactionDistance ) ) {
        return
    }

    if ( player.getActiveEnchantItemId() !== L2PcInstanceValues.EmptyId ) {
        return recordGeneralViolation( player.getObjectId(), '56a72d61-5c24-444c-85e9-54934b5318e3', 'Enchanting while mailing package' )
    }

    if ( player.getActiveTradeList() ) {
        return
    }

    if ( !ConfigManager.character.karmaPlayerCanUseWareHouse() && ( player.getKarma() > 0 ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let playerId = packet.readD(), itemAmount = packet.readD()

    if ( itemAmount < 1 ) {
        return
    }

    if ( !client.hasCharacter( playerId ) ) {
        return
    }

    let items : Array<SendItem> = []
    while ( itemAmount > 0 ) {
        let objectId = packet.readD(), count = packet.readQ()

        if ( objectId < 1 || count < 1 ) {
            return
        }

        items.push( {
            objectId,
            count
        } )

        count--
    }

    let fee = items.length * ConfigManager.character.getFreightPrice()
    let currentAdena = player.getAdena()
    let slots = 0

    let warehouse = new PcFreight( playerId )

    await warehouse.restore()

    let shouldExit = await aigle.resolve( items ).someSeries( async ( sendItem: SendItem ) => {
        let item : L2ItemInstance = player.checkItemManipulation( sendItem.objectId, sendItem.count )
        if ( !item ) {
            await warehouse.deleteMe()
            return true
        }

        if ( !item.isFreightable() ) {
            await warehouse.deleteMe()
            return true
        }

        if ( item.getId() === ItemTypes.Adena ) {
            currentAdena -= sendItem.count
        } else if ( !item.isStackable() ) {
            slots += sendItem.count
        } else if ( !warehouse.getItemByItemId( item.getId() ) ) {
            slots++
        }

        return false
    } )

    if ( shouldExit ) {
        return
    }

    if ( !warehouse.validateCapacity( slots ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_EXCEEDED_QUANTITY_THAT_CAN_BE_INPUTTED ) )
        await warehouse.deleteMe()
        return
    }

    if ( ( currentAdena < fee ) || !( await player.reduceAdena( fee, false, 'RequestPackageSend' ) ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
        await warehouse.deleteMe()
        return
    }

    shouldExit = await aigle.resolve( items ).someLimit( 10, async ( item: SendItem ) => {
        let oldItem : L2ItemInstance = player.checkItemManipulation( item.objectId, item.count )
        if ( !oldItem ) {
            await warehouse.deleteMe()
            return true
        }

        await player.getInventory().transferItem( item.objectId, item.count, warehouse )
        return false
    } )

    if ( shouldExit ) {
        return
    }

    await warehouse.deleteMe()
}

interface SendItem {
    objectId: number
    count: number
}