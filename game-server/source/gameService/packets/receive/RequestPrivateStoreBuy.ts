import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ActionFailed } from '../send/ActionFailed'
import { L2World } from '../../L2World'
import { L2NpcValues } from '../../values/L2NpcValues'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { TradeList } from '../../models/TradeList'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { recordTradeViolation } from '../../helpers/PlayerViolations'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerPermission } from '../../enums/PlayerPermission'
import { TradeListOutcome } from '../../enums/TradeListOutcome'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestPrivateStoreBuy( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        player.sendMessage( 'You are buying items too fast.' )
        return
    }

    if ( !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.sendMessage( 'Transactions are disabled for your Access Level.' )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.isCursedWeaponEquipped() ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let playerId = packet.readD()
    let storePlayer: L2PcInstance = L2World.getPlayer( playerId )
    if ( !storePlayer ) {
        return
    }

    if ( !player.isInsideRadius( storePlayer, L2NpcValues.interactionDistance ) ) {
        return
    }

    if ( ( player.getInstanceId() !== storePlayer.getInstanceId() ) && ( player.getInstanceId() !== -1 ) ) {
        return
    }

    if ( !( storePlayer.getPrivateStoreType() === PrivateStoreType.Sell
            || storePlayer.getPrivateStoreType() === PrivateStoreType.PackageSell ) ) {
        return
    }

    let storeList: TradeList = storePlayer.getSellList()
    if ( !storeList ) {
        return
    }

    let count = Math.max( 100, packet.readD() )

    if ( count <= 1 ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let items: Array<RequestPrivateStoreBuyItem> = []

    while ( count > 0 ) {
        let objectId = packet.readD(), count = packet.readQ(), price = packet.readQ()

        items.push( {
            objectId,
            count,
            price,
            itemId: 0,
        } )
    }

    if ( storePlayer.getPrivateStoreType() === PrivateStoreType.PackageSell && storeList.getItems().length > items.length ) {
        return recordTradeViolation(
                'b2d83802-c188-464b-980c-8792c9a72eef',
                'Incorrect amount of bought items requested per package sale',
                storePlayer.getObjectId(),
                player.getObjectId(),
                storePlayer.getPrivateStoreType(),
                items.map( item => item.objectId ) )
    }

    let result: TradeListOutcome = await storeList.privateStoreBuy( player, items )
    if ( result !== TradeListOutcome.Success ) {
        if ( result === TradeListOutcome.Violation ) {
            await recordTradeViolation(
                    '0dd14f15-3a81-4246-83a1-7d75ae26e3ea',
                    'Cannot transfer items from seller to buyer',
                    storePlayer.getObjectId(),
                    player.getObjectId(),
                    storePlayer.getPrivateStoreType(),
                    items.map( item => item.objectId ) )
        }

        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( storeList.getItems().length === 0 ) {
        storePlayer.setPrivateStoreType( PrivateStoreType.None )
        storePlayer.broadcastUserInfo()
    }
}

export interface RequestPrivateStoreBuyItem {
    objectId: number
    count: number
    price: number
    itemId: number
}