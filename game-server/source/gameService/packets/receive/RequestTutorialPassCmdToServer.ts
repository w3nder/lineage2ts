import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { EventType, PlayerTutorialCommandEvent, PlayerTutorialLinkEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestTutorialPassCmdToServer( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( ListenerCache.hasGeneralListener( EventType.PlayerTutorialCommand ) ) {
        let data = EventPoolCache.getData( EventType.PlayerTutorialCommand ) as PlayerTutorialCommandEvent

        data.playerId = player.getObjectId()
        data.command = new ReadableClientPacket( packetData ).readS()

        return ListenerCache.sendGeneralEvent( EventType.PlayerTutorialCommand, data )
    }
}