import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PacketVariables } from '../PacketVariables'
import { createManufactureItem, L2ManufactureItem } from '../../models/L2ManufactureItem'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { RecipeShopMessage } from '../send/RecipeShopMesssage'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'
import { AreaFlags } from '../../models/areas/AreaFlags'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestRecipeShopListSet( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let count = packet.readD()

    if ( count <= 0 || count > PacketVariables.maximumItemsLimit ) {
        player.setPrivateStoreType( PrivateStoreType.None )
        player.broadcastUserInfo()
        return
    }

    let items: Array<L2ManufactureItem> = []
    while ( count > 0 ) {
        let recipeId = packet.readD(), cost = packet.readQ()

        if ( cost < 0 ) {
            return
        }

        items.push( createManufactureItem( recipeId, cost ) )
        count--
    }

    if ( player.isUnderAttack() || player.isInDuel() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_OPERATE_PRIVATE_STORE_DURING_COMBAT ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.hasAreaFlags( AreaFlags.NoOpenStore ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_PRIVATE_WORKSHOP_HERE ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    player.clearManufatureItems()

    let shouldExit = items.some( ( item: L2ManufactureItem ) => {
        if ( !player.hasRecipeList( item.recipeId ) ) {
            // TODO : violation for un-available recipe
            return true
        }

        if ( item.adenaCost > getMaxAdena() ) {
            // TODO: violation for reaching max adena
            return true
        }

        player.manufactureItems.push( item )
        return false
    } )

    if ( shouldExit ) {
        return
    }

    player.setStoreName( !player.hasManufactureShop() ? '' : player.getStoreName() )
    player.setPrivateStoreType( PrivateStoreType.Manufacture )
    await player.sitDown()
    player.broadcastUserInfo()

    return BroadcastHelper.dataToSelfBasedOnVisibility( player, RecipeShopMessage( player ) )
}