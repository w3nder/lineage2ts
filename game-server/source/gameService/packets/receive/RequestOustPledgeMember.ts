import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { L2ClanMember } from '../../models/L2ClanMember'
import { ConfigManager } from '../../../config/ConfigManager'
import { PledgeShowMemberListDelete } from '../send/PledgeShowMemberListDelete'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { PacketVariables } from '../PacketVariables'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestOustPledgeMember( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !player.getClan() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_A_CLAN_MEMBER ) )
        return
    }

    if ( !player.hasClanPrivilege( ClanPrivilege.DismissMember ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
        return
    }

    let targetName : string = new ReadableClientPacket( packetData ).readS()

    if ( player.getName() === targetName ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_DISMISS_YOURSELF ) )
        return
    }


    let clan : L2Clan = player.getClan()
    let member : L2ClanMember = clan.getClanMemberByName( targetName )
    if ( !member ) {
        return
    }

    let targetPlayer : L2PcInstance = member.getPlayerInstance()

    if ( targetPlayer && targetPlayer.isOnline() && targetPlayer.isInCombat() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_MEMBER_CANNOT_BE_DISMISSED_DURING_COMBAT ) )
        return
    }

    let clanJoinExpiryTime = Date.now() + PacketVariables.millisInDay * ConfigManager.character.getDaysBeforeJoinAClan()
    await clan.removeClanMember( member.getObjectId(), clanJoinExpiryTime )
    clan.setCharacterPenaltyExpiryTime( clanJoinExpiryTime )
    clan.scheduleUpdate()

    let clanUpdate = new SystemMessageBuilder( SystemMessageIds.CLAN_MEMBER_S1_EXPELLED )
            .addString( member.getName() )
            .getBuffer()

    clan.broadcastDataToOnlineMembers( clanUpdate )

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_SUCCEEDED_IN_EXPELLING_CLAN_MEMBER ) )
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_MUST_WAIT_BEFORE_ACCEPTING_A_NEW_MEMBER ) )

    clan.broadcastDataToOnlineMembers( PledgeShowMemberListDelete( targetName ) )

    if ( targetPlayer && targetPlayer.isOnline() ) {
        targetPlayer.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_MEMBERSHIP_TERMINATED ) )
    }
}