import { GameClient } from '../../GameClient'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { TeleportBookmarkCache } from '../../cache/TeleportBookmarkCache'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestModifyBookMarkSlot( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let id = packet.readD(), name = packet.readS(), icon = packet.readD(), tag = packet.readS()

    return TeleportBookmarkCache.modify( client.getPlayerId(), id, icon, tag, name )
}