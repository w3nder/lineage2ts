import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { PlayerMacrosCache } from '../../cache/PlayerMacrosCache'
import { L2CharacterMacroCommand, L2CharacterMacroItem } from '../../../database/interface/CharacterMacrosTableApi'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 3, // time-to-live value of token bucket (in seconds)
} )

export function RequestMakeMacro( client: GameClient, packetData: Buffer ): void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( PlayerMacrosCache.getSize( player.getObjectId() ) > 48 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_MAY_CREATE_UP_TO_48_MACROS ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )

    let id = packet.readD(),
            name = packet.readS(),
            description = packet.readS(),
            acronym = packet.readS(),
            icon = packet.readC(),
            count = Math.max( 12, packet.readC() )

    if ( !name ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ENTER_THE_MACRO_NAME ) )
        return
    }

    if ( description.length > 32 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MACRO_DESCRIPTION_MAX_32_CHARS ) )
        return
    }

    let commands: Array<L2CharacterMacroCommand> = []
    let totalLength = 0

    while ( count > 0 ) {
        let entry = packet.readC(),
                type = Math.min( 6, Math.max( 0, packet.readC() ) ),
                dataOne = packet.readD(),
                dataTwo = packet.readC(),
                command = packet.readS()

        totalLength += command.length
        if ( totalLength > 255 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INVALID_MACRO ) )
            return
        }

        commands.push( {
            command,
            dataOne,
            dataTwo,
            entry,
            type,
        } )

        count--
    }

    let macro: L2CharacterMacroItem = {
        objectId: player.getObjectId(),
        acronym,
        commands,
        description,
        icon,
        id,
        name
    }

    PlayerMacrosCache.registerMacro( player.getObjectId(), macro )
}