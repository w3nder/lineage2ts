import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { RecipeController } from '../../taskmanager/RecipeController'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestRecipeItemMakeSelf( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let value : number = new ReadableClientPacket( packetData ).readD()

    if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
        player.sendMessage( 'You cannot create items while trading.' )
        return
    }

    if ( player.isInCraftMode() ) {
        player.sendMessage( 'You are currently in Craft Mode.' )
        return
    }

    RecipeController.requestMakeItem( player, value )
}