import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { MailManager } from '../../instancemanager/MailManager'
import { MailMessage } from '../../models/mail/MailMessage'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { MailAttachments } from '../../models/mail/MailAttachments'
import { ExNoticePostSent } from '../send/ExNoticePostSent'
import { ItemTypes } from '../../values/InventoryValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { BlocklistCache } from '../../cache/BlocklistCache'
import aigle from 'aigle'
import _ from 'lodash'
import { FastRateLimit } from 'fast-ratelimit'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'
import { AreaType } from '../../models/areas/AreaType'
import { PlayerPermission } from '../../enums/PlayerPermission'
import { PlayerAccessCache } from '../../cache/PlayerAccessCache'
import { PlayerAccess } from '../../models/PlayerAccess'

const nameLength = 16
const subjectLength = 128
const textLength = 512

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 60, // time-to-live value of token bucket (in seconds)
} )

const accessLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestSendPost( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !accessLimiter.consumeSync( client.accountName ) ) {
        return
    }

    if ( !packetLimiter.hasTokenSync( client.accountName ) && !player.isGM() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_FORWARD_LESS_THAN_MINUTE ) )
        return
    }

    if ( !ConfigManager.general.allowMail() ) {
        return
    }

    if ( !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.sendMessage( 'Transactions are disabled for your Access Level.' )
        return
    }

    if ( player.getActiveTradeList() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_FORWARD_DURING_EXCHANGE ) )
        return
    }

    if ( player.isEnchanting() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_FORWARD_DURING_ENCHANT ) )
        return
    }

    if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_FORWARD_PRIVATE_STORE ) )
        return
    }

    if ( !player.isInArea( AreaType.Peace ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_FORWARD_NOT_IN_PEACE_ZONE ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let receiver = packet.readS(), isCodValue = packet.readD(), subject = packet.readS(), text = packet.readS(), count = packet.readD()

    if ( receiver.length > nameLength ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALLOWED_LENGTH_FOR_RECIPIENT_EXCEEDED ) )
        return
    }

    if ( subject.length > subjectLength ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALLOWED_LENGTH_FOR_TITLE_EXCEEDED ) )
        return
    }

    if ( text.length > textLength ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALLOWED_LENGTH_FOR_TITLE_EXCEEDED ) )
        return
    }

    if ( count > Math.min( ConfigManager.tuning.getMailMessageAttachmentsLimit(), 8 ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_SELECTION_POSSIBLE_UP_TO_8 ) )
        return
    }

    let receiverId: number = await CharacterNamesCache.getIdByName( receiver )
    if ( !_.isNumber( receiverId ) || receiverId === 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.RECIPIENT_NOT_EXIST ) )
        return
    }

    if ( receiverId === player.getObjectId() && !ConfigManager.tuning.isMailToSelfPermitted() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANT_SEND_MAIL_TO_YOURSELF ) )
        return
    }

    if ( !player.isGM() ) {
        let level: number = await CharacterNamesCache.getAccessLevelById( receiverId )
        let accessLevel: PlayerAccess = PlayerAccessCache.getLevel( level )

        if ( accessLevel.hasPermission( PlayerPermission.IsGm ) ) {
            let messagePacket = new SystemMessageBuilder( SystemMessageIds.CANNOT_MAIL_GM_C1 )
                    .addString( receiver )
                    .getBuffer()
            player.sendOwnedData( messagePacket )
            return
        }
    }

    if ( player.isJailed() && ConfigManager.general.jailDisableTransaction() && ConfigManager.general.jailDisableChat() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_FORWARD_NOT_IN_PEACE_ZONE ) )
        return
    }

    if ( await BlocklistCache.isInBlockListByOwner( receiverId, player.getObjectId() ) ) {
        let messagePacket = new SystemMessageBuilder( SystemMessageIds.C1_BLOCKED_YOU_CANNOT_MAIL )
                .addString( receiver )
                .getBuffer()
        player.sendOwnedData( messagePacket )
        return
    }

    if ( MailManager.getOutboxSize( player.getObjectId() ) >= ConfigManager.tuning.getMailOutboxSize() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_FORWARD_MAIL_LIMIT_EXCEEDED ) )
        return
    }

    if ( MailManager.getInboxSize( receiverId ) >= ConfigManager.tuning.getMailInboxSize() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_FORWARD_MAIL_LIMIT_EXCEEDED ) )
        return
    }

    let isCod = isCodValue !== 0
    let requestedAdena = 0
    let items: Array<AttachmentItem> = []

    if ( ConfigManager.general.allowAttachments() ) {
        while ( count > 0 ) {
            let objectId = packet.readD(), amount = packet.readQ()
            if ( objectId < 1 || amount < 1 ) {
                return
            }

            items.push( {
                objectId,
                count: amount
            } )

            count--
        }

        let value : number = packet.readQ()

        if ( value < 0 || value > getMaxAdena() ) {
            // TODO : consider SystemMessageIds.EXCEECED_POCKET_ADENA_LIMIT
            return
        }

        if ( isCod ) {
            if ( value === 0 ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PAYMENT_AMOUNT_NOT_ENTERED ) )
                return
            }

            if ( items.length === 0 ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PAYMENT_REQUEST_NO_ITEM ) )
                return
            }
        }

        requestedAdena = value
    }

    let message: MailMessage = MailMessage.fromValues( player.getObjectId(), receiverId, subject, text, requestedAdena )
    if ( await removeItems( player, message, items ) ) {
        await MailManager.sendMessage( message )

        player.sendOwnedData( ExNoticePostSent( true ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MAIL_SUCCESSFULLY_SENT ) )

        packetLimiter.consumeSync( client.accountName )
    }
}

interface AttachmentItem {
    objectId: number
    count: number
}

async function removeItems( player: L2PcInstance, message: MailMessage, items: Array<AttachmentItem> ) {
    let currentAdena = player.getAdena()
    let fee : number = ConfigManager.tuning.getMailMessageFee()

    let shouldExit = items.some( ( attachment: AttachmentItem ) : boolean => {
        let item: L2ItemInstance = player.checkItemManipulation( attachment.objectId, attachment.count )
        if ( !item || !item.isTradeable() || item.isEquipped() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_FORWARD_BAD_ITEM ) )
            return true
        }

        fee += ConfigManager.tuning.getMailMessageAttachmentFee()

        if ( item.getId() === ItemTypes.Adena ) {
            currentAdena -= attachment.count
        }

        return false
    } )

    if ( shouldExit ) {
        return false
    }

    if ( currentAdena < fee
            || !( await player.reduceAdena( fee, false, 'RequestSendPost.removeItems' ) ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_FORWARD_NO_ADENA ) )
        return false
    }

    if ( items.length === 0 ) {
        return true
    }

    let attachments: MailAttachments = message.createAttachments()
    if ( !attachments ) {
        return false
    }

    shouldExit = await aigle.resolve( items ).someSeries( async ( attachment: AttachmentItem ) => {
        let oldItem: L2ItemInstance = player.checkItemManipulation( attachment.objectId, attachment.count )
        if ( !oldItem || !oldItem.isTradeable() || oldItem.isEquipped() ) {
            return true
        }

        let newItem: L2ItemInstance = await player.getInventory().transferItem( attachment.objectId, attachment.count, attachments )
        if ( !newItem ) {
            return false
        }

        await newItem.setItemLocationForNewOwner( newItem.getItemLocation(), message.getId() )
        return false
    } )

    return !shouldExit
}