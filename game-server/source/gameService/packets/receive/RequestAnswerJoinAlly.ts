import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2Clan } from '../../models/L2Clan'
import { RequestJoinAlly } from './RequestJoinAlly'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestAnswerJoinAlly( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let requester : L2PcInstance = player.getRequest().getPartner()
    if ( !requester ) {
        return
    }

    let responseValue : number = new ReadableClientPacket( packetData ).readD()

    if ( responseValue === 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DID_NOT_RESPOND_TO_ALLY_INVITATION ) )
        requester.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_RESPONSE_TO_ALLY_INVITATION ) )
    } else {
        if ( !( requester.getRequest().getRequestData().type === RequestJoinAlly ) ) {
            return
        }

        let clan : L2Clan = requester.getClan()
        if ( clan.checkAllyJoinCondition( requester, player ) ) {

            requester.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_SUCCEEDED_INVITING_FRIEND ) )
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ACCEPTED_ALLIANCE ) )

            player.getClan().setAllyId( clan.getAllyId() )
            player.getClan().setAllyName( clan.getAllyName() )
            player.getClan().setAllyPenaltyExpiryTime( 0, 0 )
            await player.getClan().changeAllyCrest( clan.getAllyCrestId(), true )
            player.getClan().scheduleUpdate()
        }
    }

    player.getRequest().onRequestResponse()
}