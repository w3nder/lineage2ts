import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExListPartyMatchingWaitingRoom } from '../send/ExListPartyMatchingWaitingRoom'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestListPartyMatchingWaitingRoom( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let page = packet.readD(), minLevel = packet.readD(), maxLevel = packet.readD(), classSize = packet.readD()

    let classIds: Array<number> = []
    while ( classSize > 0 ) {
        classIds.push( packet.readD() )
        classSize--
    }

    let filter = ''
    if ( packet.hasMoreData() ) {
        filter = packet.readS()
    }

    player.sendOwnedData( await ExListPartyMatchingWaitingRoom( page, minLevel, maxLevel, classIds, filter ) )
}