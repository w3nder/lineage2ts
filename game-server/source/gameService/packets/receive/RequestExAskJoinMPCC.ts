import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { L2Party } from '../../models/L2Party'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ExAskJoinMPCC } from '../send/ExAskJoinMPCC'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestExAskJoinMPCC( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !player.isInParty() ) {
        return
    }

    let name : string = new ReadableClientPacket( packetData ).readS()

    let targetPlayer : L2PcInstance = L2World.getPlayerByName( name )
    if ( !targetPlayer ) {
        return
    }

    if ( targetPlayer.isInParty() && player.getParty().getMembers().includes( targetPlayer.getObjectId() ) ) {
        return
    }

    if ( !targetPlayer.isInParty() ) {
        player.sendMessage( `${targetPlayer.getName()} doesn't have party and cannot be invited to Command Channel.` )
        return
    }

    if ( targetPlayer.getParty().isInCommandChannel() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_ALREADY_MEMBER_OF_COMMAND_CHANNEL )
                .addString( targetPlayer.getName() )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    let currentParty :L2Party = player.getParty()
    if ( currentParty.getLeaderObjectId() !== player.getObjectId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_INVITE_TO_COMMAND_CHANNEL ) )
        return
    }

    if ( currentParty.isInCommandChannel() && currentParty.getCommandChannel().getLeaderObjectId() !== player.getObjectId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_INVITE_TO_COMMAND_CHANNEL ) )
        return
    }

    let hasRight = player.isClanLeader() && ( player.getClan().getLevel() >= 5 )
        || player.getInventory().getItemByItemId( 8871 )
        || ( player.getPledgeClass() >= 5 ) && ( player.getKnownSkill( 391 ) )

    if ( !hasRight ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.COMMAND_CHANNEL_ONLY_BY_LEVEL_5_CLAN_LEADER_PARTY_LEADER ) )
        return
    }

    let targetLeader : L2PcInstance = targetPlayer.getParty().getLeader()
    if ( targetLeader.isProcessingRequest() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_BUSY_TRY_LATER )
                .addString( targetLeader.getName() )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    player.onTransactionRequest( targetLeader )

    let confirmPacket = new SystemMessageBuilder( SystemMessageIds.COMMAND_CHANNEL_CONFIRM_FROM_C1 )
            .addString( player.getName() )
            .getBuffer()
    targetLeader.sendOwnedData( confirmPacket )
    targetLeader.sendOwnedData( ExAskJoinMPCC( player.getName() ) )

    player.sendMessage( `You invited ${targetLeader.getName()} to your Command Channel.` )
}