import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { HennaEquipList } from '../send/HennaEquipList'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestHennaItemList( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    player.sendOwnedData( HennaEquipList( player.getObjectId() ) )
}