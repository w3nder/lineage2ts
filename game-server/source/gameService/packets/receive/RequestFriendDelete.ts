import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { FriendRemove } from '../send/FriendPacket'
import { L2World } from '../../L2World'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import _ from 'lodash'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerFriendsCache } from '../../cache/PlayerFriendsCache'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestFriendDelete( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let name : string = new ReadableClientPacket( packetData ).readS()

    let id = CharacterNamesCache.getCachedIdByName( name )
    let playerFriendsIds = PlayerFriendsCache.getFriends( player.getObjectId() )

    if ( !_.isNumber( id ) || !playerFriendsIds.has( id ) ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_NOT_ON_YOUR_FRIENDS_LIST )
                .addString( name )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    PlayerFriendsCache.removeFriend( player.getObjectId(), id )

    let packet = new SystemMessageBuilder( SystemMessageIds.S1_HAS_BEEN_DELETED_FROM_YOUR_FRIENDS_LIST )
            .addString( name )
            .getBuffer()
    player.sendOwnedData( packet )

    let friend = L2World.getPlayer( id )
    if ( friend ) {
        player.sendOwnedData( FriendRemove( id ) )
        friend.sendOwnedData( FriendRemove( player.getObjectId() ) )
        return
    }

    player.sendOwnedData( FriendRemove( id ) )
}