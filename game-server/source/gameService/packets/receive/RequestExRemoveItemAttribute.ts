import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { CrystalType } from '../../models/items/type/CrystalType'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { Elementals } from '../../models/Elementals'
import { ExBaseAttributeCancelResult } from '../send/ExBaseAttributeCancelResult'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export async function RequestExRemoveItemAttribute( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let objectId = packet.readD(), elementId = packet.readD()

    let targetItem: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    if ( !targetItem ) {
        return
    }

    let elemental = targetItem.getElemental( elementId )
    if ( !elemental ) {
        return
    }

    let isReduced : boolean = await player.reduceAdena( getPrice( targetItem ),true, 'RequestExRemoveItemAttribute' )
    if ( !isReduced ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DO_NOT_HAVE_ENOUGH_FUNDS_TO_CANCEL_ATTRIBUTE ) )
        return
    }

    if ( targetItem.isEquipped() ) {
        elemental.removeBonus( player )
    }

    targetItem.clearElementAttribute( elementId )
    player.broadcastUserInfo()

    let realElement = targetItem.isArmor() ? Elementals.getOppositeElement( elementId ) : elementId
    let message: SystemMessageBuilder
    if ( targetItem.getEnchantLevel() > 0 ) {
        if ( targetItem.isArmor() ) {
            message = new SystemMessageBuilder( SystemMessageIds.S1_S2_S3_ATTRIBUTE_REMOVED_RESISTANCE_TO_S4_DECREASED )
        } else {
            message = new SystemMessageBuilder( SystemMessageIds.S1_S2_ELEMENTAL_POWER_REMOVED )
        }

        message.addNumber( targetItem.getEnchantLevel() )
        message.addItemInstanceName( targetItem )

        if ( targetItem.isArmor() ) {
            message.addElemental( realElement )
            message.addElemental( Elementals.getOppositeElement( realElement ) )
        }
    } else {
        if ( targetItem.isArmor() ) {
            message = new SystemMessageBuilder( SystemMessageIds.S1_S2_ATTRIBUTE_REMOVED_RESISTANCE_S3_DECREASED )
        } else {
            message = new SystemMessageBuilder( SystemMessageIds.S1_ELEMENTAL_POWER_REMOVED )
        }

        message.addItemInstanceName( targetItem )
        if ( targetItem.isArmor() ) {
            message.addElemental( realElement )
            message.addElemental( Elementals.getOppositeElement( realElement ) )
        }
    }

    player.sendOwnedData( message.getBuffer() )
    player.sendOwnedData( ExBaseAttributeCancelResult( targetItem.getObjectId(), elementId ) )
}

function getPrice( item: L2ItemInstance ): number {
    switch ( item.getItem().getCrystalType() ) {
        case CrystalType.S:
            if ( item.isWeapon() ) {
                return 50000
            }

            return 40000

        case CrystalType.S80:
            if ( item.isWeapon() ) {
                return 100000
            }

            return 80000

        case CrystalType.S84:
            if ( item.isWeapon() ) {
                return 200000
            }

            return 160000
    }

    return 0
}