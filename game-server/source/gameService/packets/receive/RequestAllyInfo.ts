import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { AllianceInfo } from '../../models/clan/AllianceInfo'
import { AllianceInfoPacket } from '../send/AllianceInfoPacket'
import { ClanInfo } from '../../models/clan/ClanInfo'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestAllyInfo( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let allianceId = player.getAllyId()

    if ( allianceId <= 0 ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_CURRENT_ALLIANCES ) )
    }

    const allianceInfo = new AllianceInfo( allianceId )

    player.sendOwnedData( AllianceInfoPacket( allianceInfo ) )
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ALLIANCE_INFO_HEAD ) )

    let allianceName = new SystemMessageBuilder( SystemMessageIds.ALLIANCE_NAME_S1 )
            .addString( allianceInfo.name )
            .getBuffer()
    player.sendOwnedData( allianceName )

    let leaderStatus = new SystemMessageBuilder( SystemMessageIds.ALLIANCE_LEADER_S2_OF_S1 )
            .addString( allianceInfo.subordinateLeader )
            .addString( allianceInfo.superiorLeader )
            .getBuffer()
    player.sendOwnedData( leaderStatus )

    let totalStatus = new SystemMessageBuilder( SystemMessageIds.CONNECTION_S1_TOTAL_S2 )
            .addNumber( allianceInfo.online )
            .addNumber( allianceInfo.total )
            .getBuffer()
    player.sendOwnedData( totalStatus )

    let alliesStatus = new SystemMessageBuilder( SystemMessageIds.ALLIANCE_CLAN_TOTAL_S1 )
            .addNumber( allianceInfo.allies.length )
            .getBuffer()
    player.sendOwnedData( alliesStatus )

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_INFO_HEAD ) )

    allianceInfo.allies.forEach( ( info: ClanInfo, index: number ) => {
        let nameStatus = new SystemMessageBuilder( SystemMessageIds.CLAN_INFO_NAME_S1 )
                .addString( info.clan.getName() )
                .getBuffer()
        player.sendOwnedData( nameStatus )

        let leaderStatus = new SystemMessageBuilder( SystemMessageIds.CLAN_INFO_LEADER_S1 )
                .addString( info.clan.getLeaderName() )
                .getBuffer()
        player.sendOwnedData( leaderStatus )

        let levelStatus = new SystemMessageBuilder( SystemMessageIds.CLAN_INFO_LEVEL_S1 )
                .addNumber( info.clan.getLevel() )
                .getBuffer()
        player.sendOwnedData( levelStatus )

        let countStatus = new SystemMessageBuilder( SystemMessageIds.CONNECTION_S1_TOTAL_S2 )
                .addNumber( info.online )
                .addNumber( info.total )
                .getBuffer()
        player.sendOwnedData( countStatus )

        if ( index !== ( allianceInfo.allies.length - 1 ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_INFO_SEPARATOR ) )
        }
    } )

    return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_INFO_FOOT ) )
}