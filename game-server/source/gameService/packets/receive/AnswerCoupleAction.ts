import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { ExRotation } from '../send/ExRotation'
import { SocialAction } from '../send/SocialAction'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function AnswerCoupleAction( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let actionId = packet.readD(), answer = packet.readD(), objectId = packet.readD()

    let target: L2PcInstance = L2World.getPlayer( objectId )
    if ( !target ) {
        return
    }

    if ( ( target.getMultiSocialTarget() !== player.getObjectId() ) || ( target.getMultiSociaAction() !== actionId ) ) {
        return
    }

    if ( answer === 0 ) {
        target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.COUPLE_ACTION_DENIED ) )
    }

    if ( answer === 1 ) {
        let distance = player.calculateDistance( target )
        if ( ( distance > 125 ) || ( distance < 15 ) || ( player.getObjectId() === target.getObjectId() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_DO_NOT_MEET_LOC_REQUIREMENTS ) )
            target.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_DO_NOT_MEET_LOC_REQUIREMENTS ) )
            return
        }

        let playerHeading = GeneralHelper.calculateHeadingFromLocations( player, target )
        player.setHeading( playerHeading )

        let targetHeading = GeneralHelper.calculateHeadingFromLocations( target, player )
        target.setHeading( targetHeading )

        BroadcastHelper.multiplePacketsToSelfAndKnownPlayers( player, [ ExRotation( player.getObjectId(), playerHeading ), SocialAction( player.getObjectId(), actionId ) ] )
        BroadcastHelper.multiplePacketsToSelfAndKnownPlayers( target, [ ExRotation( target.getObjectId(), targetHeading ), SocialAction( target.getObjectId(), actionId ) ] )
    }

    if ( answer === -1 ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_SET_TO_REFUSE_COUPLE_ACTIONS )
                .addPlayerCharacterName( player )
                .getBuffer()
        target.sendOwnedData( packet )
    }

    target.setMultiSocialAction( 0, 0 )
}