import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { StopRotation } from '../send/StopRotation'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function FinishRotating( client: GameClient, packetData: Buffer ): void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let degree: number = new ReadableClientPacket( packetData ).readD()

    let airShip = player.getAirShip()
    if ( player.isInAirShip() && airShip.isCaptain( player ) ) {
        airShip.setHeading( degree )
        return BroadcastHelper.dataToSelfInRange( airShip, StopRotation( airShip.getObjectId(), degree, 0 ) )
    }

    return BroadcastHelper.dataToSelfBasedOnVisibility( player, StopRotation( player.getObjectId(), degree, 0 ) )
}