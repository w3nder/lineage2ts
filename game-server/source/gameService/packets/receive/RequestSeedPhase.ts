import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExShowSeedMapInfo } from '../send/ExShowSeedMapInfo'

export function RequestSeedPhase( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    player.sendOwnedData( ExShowSeedMapInfo() )
}