import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { IAdminCommand } from '../../handler/IAdminCommand'
import { AdminCommandManager } from '../../handler/managers/AdminCommandManager'
import { DataManager } from '../../../data/manager'
import { EventType, PlayerAdminCommandEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import _ from 'lodash'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function SendBypassBuildCommand( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let commandValue: string = packet.readS().trim()
    ReadableClientPacketPool.recycleValue( packet )

    if ( !commandValue ) {
        return
    }

    let currentCommand = _.head( _.split( commandValue, ' ' ) )
    let command = `admin_${ currentCommand }`

    let handler: IAdminCommand = AdminCommandManager.getHandler( command )

    if ( !handler ) {
        if ( player.isGM() ) {
            player.sendMessage( `command //${ currentCommand } does not exist!` )
        }

        return
    }

    let isExecuted = false

    if ( !player.getAccessLevel().canExecute( command ) ) {
        player.sendMessage( 'You don\'t have the access right to use this command!' )
    } else {
        await handler.onCommand( commandValue, player )
        isExecuted = true
    }

    if ( ListenerCache.hasGeneralListener( EventType.PlayerAdminCommand ) ) {
        let data: PlayerAdminCommandEvent = {
            playerId: player.getObjectId(),
            command: currentCommand,
            isExecuted,
        }

        return ListenerCache.sendGeneralEvent( EventType.PlayerAdminCommand, data )
    }
}