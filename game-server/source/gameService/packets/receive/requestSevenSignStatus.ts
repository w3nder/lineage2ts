import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SevenSigns } from '../../directives/SevenSigns'
import { SSQStatus, SSQStatusPage } from '../send/SSQStatus'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestSevenSignStatus( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let page : number = new ReadableClientPacket( packetData ).readC()

    if ( ( SevenSigns.isSealValidationPeriod() || SevenSigns.isCompareResultsPeriod() ) && page === 4 ) {
        return
    }

    if ( page < 1 || page > 4 ) {
        return
    }

    player.sendOwnedData( await SSQStatus( player.getObjectId(), page as SSQStatusPage ) )
}