import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { L2Clan } from '../../models/L2Clan'
import { ClanCache } from '../../cache/ClanCache'
import { GMViewCharacterInfo } from '../send/GMViewCharacterInfo'
import { GMHennaInfo } from '../send/GMHennaInfo'
import { GMViewPledgeInfo } from '../send/GMViewPledgeInfo'
import { GMViewSkillInfo } from '../send/GMViewSkillInfo'
import { GmViewQuestInfo } from '../send/GmViewQuestInfo'
import { GMViewItemList } from '../send/GMViewItemList'
import { GMViewWarehouseWithdrawList, GMViewWarehouseWithdrawListForClan } from '../send/GMViewWarehouseWithdrawList'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerPermission } from '../../enums/PlayerPermission'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

const enum GMCommand {
    None = 0,
    CharacterInfo,
    Clan,
    Skills,
    Quests,
    Inventory,
    Warehouse
}

export async function RequestGMCommand( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !player.isGM() || !player.getAccessLevel().hasPermission( PlayerPermission.AllowAltG ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let name = packet.readS(), commandId = packet.readD()

    let otherPlayer: L2PcInstance = L2World.getPlayerByName( name as string )
    if ( !otherPlayer && commandId !== 6 ) {
        return
    }

    switch ( commandId ) {
        case GMCommand.CharacterInfo:
            player.sendOwnedData( GMViewCharacterInfo( otherPlayer ) )
            player.sendOwnedData( GMHennaInfo( otherPlayer ) )
            break

        case GMCommand.Clan:
            if ( otherPlayer.getClan() ) {
                return player.sendOwnedData( GMViewPledgeInfo( otherPlayer ) )
            }

            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_JOINED_IN_ANY_CLAN ) )

        case GMCommand.Skills:
            player.sendOwnedData( GMViewSkillInfo( otherPlayer ) )
            break

        case GMCommand.Quests:
            player.sendOwnedData( GmViewQuestInfo( otherPlayer ) )
            break

        case GMCommand.Inventory:
            player.sendOwnedData( GMViewItemList( otherPlayer ) )
            player.sendOwnedData( GMHennaInfo( otherPlayer ) )
            break

        case GMCommand.Warehouse:
            if ( otherPlayer ) {
                await otherPlayer.initializeWarehouse()
                player.sendOwnedData( GMViewWarehouseWithdrawList( otherPlayer ) )
                return
            }

            let clan: L2Clan = ClanCache.getClanByName( name as string )
            if ( !clan ) {
                return
            }

            player.sendOwnedData( GMViewWarehouseWithdrawListForClan( clan ) )
            break
    }
}