import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { TargetUnselected } from '../send/TargetUnselected'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestTargetCancel( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.isLockedTarget() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_DISABLE_TARGET ) )
        return
    }

    let value : number = new ReadableClientPacket( packetData ).readH()

    if ( value === 0 ) {
        if ( player.isCastingNow() && player.canAbortCast() ) {
            player.abortCast()
        } else if ( player.getTarget() ) {
            player.setTarget( null )
        }

        return
    }

    if ( player.getTarget() ) {
        player.setTarget( null )
        return
    }

    if ( player.isInAirShip() ) {
        return BroadcastHelper.dataToSelfBasedOnVisibility( player, TargetUnselected( player ) )
    }
}