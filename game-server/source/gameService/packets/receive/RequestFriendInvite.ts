import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { FriendAddRequest } from '../send/FriendAddRequest'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { BlocklistCache } from '../../cache/BlocklistCache'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerFriendsCache } from '../../cache/PlayerFriendsCache'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestFriendInvite( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let name : string = new ReadableClientPacket( packetData ).readS()
    let friend : L2PcInstance = L2World.getPlayerByName( name )

    if ( !friend || !friend.isOnline() || friend.isInvisible() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_USER_YOU_REQUESTED_IS_NOT_IN_GAME ) )
        return
    }

    if ( friend.getObjectId() === player.getObjectId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_ADD_YOURSELF_TO_OWN_FRIEND_LIST ) )
        return
    }

    let playerFriendsIds = PlayerFriendsCache.getFriends( player.getObjectId() )
    if ( playerFriendsIds.size >= ConfigManager.character.getFriendListLimit() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CAN_ONLY_ENTER_UP_128_NAMES_IN_YOUR_FRIENDS_LIST ) )
        return
    }

    let responderFriendIds = PlayerFriendsCache.getFriends( friend.getObjectId() )
    if ( responderFriendIds.size >= ConfigManager.character.getFriendListLimit() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_FRIENDS_LIST_OF_THE_PERSON_YOU_ARE_TRYING_TO_ADD_IS_FULL_SO_REGISTRATION_IS_NOT_POSSIBLE ) )
        return
    }

    if ( player.isInOlympiadMode() || friend.isInOlympiadMode() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.A_USER_CURRENTLY_PARTICIPATING_IN_THE_OLYMPIAD_CANNOT_SEND_PARTY_AND_FRIEND_INVITATIONS ) )
        return
    }

    if ( BlocklistCache.isBlocked( friend.getObjectId(), player.getObjectId() ) ) {
        player.sendMessage( 'You are in target\'s block list.' )
        return
    }

    if ( BlocklistCache.isBlocked( player.getObjectId(), friend.getObjectId() ) ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.BLOCKED_C1 )
                .addCharacterName( friend )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    if ( playerFriendsIds.has( friend.getObjectId() ) ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.S1_ALREADY_IN_FRIENDS_LIST )
                .addString( name )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    if ( friend.isProcessingRequest() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_BUSY_TRY_LATER )
                .addString( name )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    player.onTransactionRequest( friend )
    friend.sendOwnedData( FriendAddRequest( player.getName() ) )

    let packet = new SystemMessageBuilder( SystemMessageIds.YOU_REQUESTED_C1_TO_BE_FRIEND )
            .addString( name )
            .getBuffer()
    player.sendOwnedData( packet )
}