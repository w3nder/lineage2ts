import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2PartyMessageType } from '../../enums/L2PartyMessageType'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export function RequestOustPartyMember( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player
            || !player.isInParty()
            || !player.getParty().isLeader( player ) ) {
        return
    }

    if ( player.getParty().isInDimensionalRift()
            && !player.getParty().getDimensionalRift().getRevivedAtWaitingRoom().includes( player.getObjectId() ) ) {
        player.sendMessage( 'You can\'t dismiss party member when you are in Dimensional Rift.' )
        return
    }

    let name : string = new ReadableClientPacket( packetData ).readS()
    player.getParty().removePartyMemberByName( name, L2PartyMessageType.Expelled )
}