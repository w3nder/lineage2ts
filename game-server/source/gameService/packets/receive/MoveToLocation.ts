import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { StopMove } from '../send/StopMove'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { GeoPolygonCache } from '../../cache/GeoPolygonCache'
import { ValidateLocation } from '../send/ValidateLocation'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { EnchantResult, EnchantResultOutcome } from '../send/EnchantResult'
import { TeleportMode } from '../../enums/TeleportMode'
import { PlayerMovementActionCache } from '../../cache/PlayerMovementActionCache'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

/*
    L2J maximum movement is capped by 9900 distance in both X and Y
 */
const enum MoveToLocationVariables {
    MovementLimit = 6000
}

export async function MoveToLocation( client: GameClient, packetData: Buffer ): Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    if ( ( ConfigManager.character.getNpcTalkBlockingTime() > 0 ) && !player.isGM() && ( player.getNotMoveUntil() > Date.now() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_MOVE_WHILE_SPEAKING_TO_AN_NPC ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.isOutOfControl() ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.getMoveSpeed() === 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_MOVE_TOO_ENCUMBERED ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.getActiveEnchantItemId() !== L2PcInstanceValues.EmptyId ) {
        player.setActiveEnchantItemId( L2PcInstanceValues.EmptyId )
        player.sendOwnedData( EnchantResult( EnchantResultOutcome.Cancelled, 0, 0 ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ENCHANT_SCROLL_CANCELLED ) )
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let targetX = packet.readD(),
        targetY = packet.readD(),
        targetZ = packet.readD(),
        originX = packet.readD(),
        originY = packet.readD(),
        originZ = packet.readD()

    ReadableClientPacketPool.recycleValue( packet )

    /*
        Server vs client Z value de-sync.
     */
    if ( ( player.getZ() - originZ ) > 1000 ) {
        player.sendOwnedData( ActionFailed() )
        return player.sendOwnedData( ValidateLocation( player ) )
    }

    if ( ( targetX === originX ) && ( targetY === originY ) && ( targetZ === originZ ) ) {
        player.sendOwnedData( StopMove( player ) )

        return player.stopMove()
    }

    let action = PlayerMovementActionCache.getAction( player.getObjectId() )
    if ( action ) {
        await action( player, targetX, targetY, targetZ )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( Math.abs( targetX - player.getX() ) > MoveToLocationVariables.MovementLimit
        || Math.abs( targetY - player.getY() ) > MoveToLocationVariables.MovementLimit ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let z = GeoPolygonCache.getZ( targetX, targetY, targetZ )
    if ( GeoPolygonCache.isInvalidZ( z ) ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    /*
        Going from low Z to high Z
     */
    if ( ( player.getZ() - z ) < -1000 ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    return AIEffectHelper.notifyMoveWithCoordinates( player, targetX, targetY, z )
}