import { GameClient } from '../../GameClient'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import * as crypto from 'crypto'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

const validReply: Buffer = Buffer.from( [
    0x88,
    0x40,
    0x1c,
    0xa7,
    0x83,
    0x42,
    0xe9,
    0x15,
    0xde,
    0xc3,
    0x68,
    0xf6,
    0x2d,
    0x23,
    0xf1,
    0x3f,
    0xee,
    0x68,
    0x5b,
    0xc5,
] )

const staticValidReply = Buffer.allocUnsafeSlow( validReply.length )
validReply.copy( staticValidReply )

export function GameGuardReply( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let firstChunk = packet.readB( 4 )
    packet.skipD( 1 )
    let secondChunk = packet.readB( 4 )

    let result = Buffer.concat( [ firstChunk, secondChunk ] )
    let outcome: Buffer = crypto.createHash( 'sha1' ).update( result ).digest()
    if ( Buffer.compare( staticValidReply, outcome ) === 0 ) {
        client.setGameGuardOk( true )
    }
}