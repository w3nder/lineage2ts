import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { NicknameColor } from '../../handler/itemhandlers/NicknameColor'

const availableColors: Array<number> = [
    0x9393FF, // Pink
    0x7C49FC, // Rose Pink
    0x97F8FC, // Lemon Yellow
    0xFA9AEE, // Lilac
    0xFF5D93, // Cobalt Violet
    0x00FCA0, // Mint Green
    0xA0A601, // Peacock Green
    0x7898AF, // Yellow Ochre
    0x486295, // Chocolate
    0x999999, // Silver
]

export async function RequestChangeNicknameColor( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let colorIndex = packet.readD()

    if ( colorIndex < 0 || colorIndex >= availableColors.length ) {
        return
    }

    let title = packet.readS(), objectId = packet.readD()

    let item: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    if ( !item
            || !item.getEtcItem()
            || item.getEtcItem().getItemHandler() === NicknameColor ) {
        return
    }

    if ( await player.destroyItemWithCount( item, 1, true, 'Consume' ) ) {
        player.setTitle( title )
        player.getAppearance().setTitleColor( availableColors[ colorIndex ] )
        player.broadcastUserInfo()
    }
}