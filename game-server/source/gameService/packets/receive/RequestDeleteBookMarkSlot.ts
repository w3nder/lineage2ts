import { GameClient } from '../../GameClient'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { TeleportBookmarkCache } from '../../cache/TeleportBookmarkCache'

export function RequestDeleteBookMarkSlot( client: GameClient, packetData: Buffer ) {
    let id : number = new ReadableClientPacket( packetData ).readD()
    return TeleportBookmarkCache.remove( client.getPlayerId(), id )
}