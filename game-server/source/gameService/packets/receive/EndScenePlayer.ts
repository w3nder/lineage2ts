import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 3, // time-to-live value of token bucket (in seconds)
} )

export function EndScenePlayer( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let movieId : number = new ReadableClientPacket( packetData ).readD()

    if ( movieId === 0 ) {
        return
    }
    if ( player.getMovieId() !== movieId ) {
        return
    }

    player.setMovieId( 0 )
    player.setIsTeleporting( true, false ) // avoid to get player removed from L2World
    player.decayMe()
    player.spawnMe( player.getX(), player.getY(), player.getZ() )
    player.setIsTeleporting( false, false )
}