import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { CastleManager } from '../../instancemanager/CastleManager'
import { Castle } from '../../models/entity/Castle'
import { SiegableHall } from '../../models/entity/clanhall/SiegableHall'
import { ClanHallSiegeManager } from '../../instancemanager/ClanHallSiegeManager'
import { SiegeInfoForCastle, SiegeInfoForHall } from '../send/SiegeInfo'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestJoinSiege( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !player.hasClanPrivilege( ClanPrivilege.CastleSiege ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
        return
    }

    let clan: L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let residenceId = packet.readD(), attackerValue = packet.readD(), joiningValue = packet.readD()

    let castle : Castle = CastleManager.getCastleById( residenceId )
    if ( castle != null ) {
        if ( joiningValue === 1 ) {
            if ( Date.now() < clan.getDissolvingExpiryTime() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_PARTICIPATE_IN_SIEGE_WHILE_DISSOLUTION_IN_PROGRESS ) )
                return
            }
            if ( attackerValue === 1 ) {
                await castle.getSiege().registerAttacker( player )
            } else {
                await castle.getSiege().registerDefender( player )
            }
        } else {
            await castle.getSiege().removeSiegeClan( player.getClan() )
        }

        player.sendOwnedData( SiegeInfoForCastle( castle, player ) )
    }


    let hall : SiegableHall = ClanHallSiegeManager.getSiegableHall( residenceId )
    if ( hall ) {
        if ( joiningValue === 1 ) {
            if ( Date.now() < clan.getDissolvingExpiryTime() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_PARTICIPATE_IN_SIEGE_WHILE_DISSOLUTION_IN_PROGRESS ) )
                return
            }
            ClanHallSiegeManager.registerClan( clan, hall, player )
        } else {
            ClanHallSiegeManager.unRegisterClan( clan, hall )
        }

        player.sendOwnedData( SiegeInfoForHall( hall, player ) )
    }
}