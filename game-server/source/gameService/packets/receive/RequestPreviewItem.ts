import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { L2NpcValues } from '../../values/L2NpcValues'
import { ActionFailed } from '../send/ActionFailed'
import { L2MerchantInstance } from '../../models/actor/instance/L2MerchantInstance'
import { L2BuyList } from '../../models/buylist/L2BuyList'
import { BuyListManager } from '../../cache/BuyListManager'
import { Product } from '../../models/buylist/Product'
import { L2Item } from '../../models/items/L2Item'
import { ItemInstanceType } from '../../models/items/ItemInstanceType'
import { WeaponType } from '../../models/items/type/WeaponType'
import { ArmorType } from '../../enums/items/ArmorType'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ShopPreviewInfo } from '../send/ShopPreviewInfo'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { InventoryHelper } from '../../models/itemcontainer/InventoryHelper'
import { DeferredMethods } from '../../helpers/DeferredMethods'
import _ from 'lodash'
import { FastRateLimit } from 'fast-ratelimit'
import { ListenerCache } from '../../cache/ListenerCache'
import { BuySellViolationEvent, EventType } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { recordBuySellViolation } from '../../helpers/PlayerViolations'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestPreviewItem( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendMessage( 'You are buying too fast.' )
    }

    if ( !ConfigManager.character.karmaPlayerCanShop() && ( player.getKarma() > 0 ) ) {
        return
    }

    let target: L2Object = player.getTarget()
    if ( !player.isGM()
            && ( !target || !target.isMerchant() || !player.isInsideRadius( target, L2NpcValues.interactionDistance ) ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    packet.skipD( 1 )
    let listId = packet.readD(), count = packet.readD()

    if ( ( count < 1 ) || ( listId >= 4000000 ) || count > 100 ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let buyList: L2BuyList = BuyListManager.getBuyList( listId )
    if ( !buyList ) {
        player.sendOwnedData( ActionFailed() )
        return recordBuySellViolation( player.getObjectId(), '7c1da881-86a4-4fe2-85ef-840e1b5bd78c', 'Player previews item from non-existent buylistId', listId )
    }

    let totalPrice = 0
    let itemList: Record<number, number> = {}
    let maxAdena = getMaxAdena()

    while ( count > 0 ) {
        let itemId = packet.readD()
        count--

        let product: Product = buyList.getProductByItemId( itemId )
        if ( !product ) {
            if ( ListenerCache.hasGeneralListener( EventType.BuySellViolation ) ) {
                let data = EventPoolCache.getData( EventType.BuySellViolation ) as BuySellViolationEvent

                data.ids = [ listId, itemId ]
                data.errorId = '64c7660f-74bb-4fa0-80b9-5ff35595bd18'
                data.playerId = player.getObjectId()
                data.message = 'Player previews non-existent product with itemId'

                await ListenerCache.sendGeneralEvent( EventType.BuySellViolation, data )
            }

            return
        }

        let template: L2Item = product.getItem()
        if ( !template ) {
            continue
        }

        let slot = InventoryHelper.getPaperdollIndex( template.getBodyPart() )
        if ( slot < 0 ) {
            continue
        }

        if ( template.isInstanceType( ItemInstanceType.L2Weapon ) ) {
            if ( player.getRace() === 5 ) {
                if ( template.getItemType() === WeaponType.NONE ) {
                    continue
                }

                if ( ( template.getItemType() === WeaponType.RAPIER )
                        || ( template.getItemType() === WeaponType.CROSSBOW )
                        || ( template.getItemType() === WeaponType.ANCIENTSWORD ) ) {
                    continue
                }
            }
        } else if ( template.isInstanceType( ItemInstanceType.L2Armor ) ) {
            if ( player.getRace() === 5 ) {
                if ( ( template.getItemType() === ArmorType.HEAVY ) || ( template.getItemType() === ArmorType.MAGIC ) ) {
                    continue
                }
            }
        }

        if ( itemList[ slot ] ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CAN_NOT_TRY_THOSE_ITEMS_ON_AT_THE_SAME_TIME ) )
            return
        }

        itemList[ slot ] = itemId

        if ( ( maxAdena - totalPrice ) >= ConfigManager.general.getWearPrice() ) {
            return
        }

        totalPrice += ConfigManager.general.getWearPrice()
    }

    if ( totalPrice <= 0 || !( await player.reduceAdena( totalPrice, true, 'RequestPreviewItem' ) ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
        return
    }

    if ( !_.isEmpty( itemList ) ) {
        player.sendOwnedData( ShopPreviewInfo( itemList ) )
        player.scheduleWearItemRemoval()
    }
}
