import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { MailMessage } from '../../models/mail/MailMessage'
import { MailManager } from '../../instancemanager/MailManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ItemContainer } from '../../models/itemcontainer/ItemContainer'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemLocation } from '../../enums/ItemLocation'
import { L2World } from '../../L2World'
import { ExChangePostStateSingle } from '../send/ExChangePostState'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import aigle from 'aigle'
import { FastRateLimit } from 'fast-ratelimit'
import { AreaType } from '../../models/areas/AreaType'
import { ChangePostState } from '../../enums/ChangePostState'
import { L2MessagesTableItemStatus } from '../../../database/interface/MessagesTableApi'
import { recordMailViolation } from '../../helpers/PlayerViolations'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestCancelPostAttachment( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player || !ConfigManager.general.allowMail() || !ConfigManager.general.allowAttachments() ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    if ( !player.isInArea( AreaType.Peace ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_CANCEL_NOT_IN_PEACE_ZONE ) )
        return
    }

    if ( player.getActiveTradeList() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_CANCEL_DURING_EXCHANGE ) )
        return
    }

    if ( player.isEnchanting() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_CANCEL_DURING_ENCHANT ) )
        return
    }

    if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_CANCEL_PRIVATE_STORE ) )
        return
    }

    let messageId: number = new ReadableClientPacket( packetData ).readD()

    let message: MailMessage = MailManager.getMessage( messageId )
    if ( !message ) {
        return
    }

    if ( message.getSenderId() !== player.getObjectId() ) {
        return recordMailViolation( player.getObjectId(), 'ba57dec5-ae16-4e79-a3a6-b6a4d557e8ee', message.getId(), message.getReceiverId(), message.getSenderId(), 'Player tried to receive not owned mail message', )

    }

    if ( message.isReturned() ) {
        return
    }

    if ( message.getStatus() !== L2MessagesTableItemStatus.Unread ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANT_CANCEL_RECEIVED_MAIL ) )
    }

    if ( message.hasAttachments() && !await processAttachments( message, player ) ) {
        return
    }

    let receiver: L2PcInstance = L2World.getPlayer( message.getReceiverId() )
    if ( receiver ) {
        let messagePacket = new SystemMessageBuilder( SystemMessageIds.S1_CANCELLED_MAIL )
                .addPlayerCharacterName( player )
                .getBuffer()
        receiver.sendOwnedData( messagePacket )
        receiver.sendOwnedData( ExChangePostStateSingle( true, messageId, ChangePostState.Deleted ) )
    }

    MailManager.removeAttachments( messageId )
    MailManager.setDeleted( messageId )

    player.sendOwnedData( ExChangePostStateSingle( false, messageId, ChangePostState.Deleted ) )
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MAIL_SUCCESSFULLY_CANCELLED ) )
}

async function processAttachments( message: MailMessage, player : L2PcInstance ) : Promise<boolean> {
    let attachments: ItemContainer = message.getLoadedAttachments()
    if ( !attachments || attachments.getSize() === 0 ) {
        return true
    }

    let weight = 0
    let slots = 0

    /*
        Please note that direct iteration of inventory contents while removing items
        can change order of available items.
     */
    let items = attachments.getItems().slice( 0 )
    let shouldExit = items.some( ( item: L2ItemInstance ) => {
        if ( !item ) {
            return false
        }

        if ( item.getOwnerId() !== player.getObjectId() ) {
            void recordMailViolation( player.getObjectId(), '8fcaec73-6f5b-45a9-826a-21ecda215064', message.getId(), message.getReceiverId(), message.getSenderId(), 'Player tried to get not owned item from cancelled attachment' )
            return true
        }

        if ( item.getItemLocation() !== ItemLocation.MAIL ) {
            // should this be classified as data integrity issue?
            void recordMailViolation( player.getObjectId(), '97805659-9cdb-46d8-ab17-42914c5449d4', message.getId(), message.getReceiverId(), message.getSenderId(), 'Player tried to get items not from mail' )
            return true
        }

        if ( item.getLocationSlot() !== message.getId() ) {
            // should this be classified as data integrity issue?
            void recordMailViolation( player.getObjectId(), '255381b4-a023-41d3-8359-869300f51fd3', message.getId(), message.getReceiverId(), message.getSenderId(), 'Player tried to get items from different attachment' )
            return true
        }

        weight += item.getCount() * item.getItem().getWeight()
        if ( !item.isStackable() ) {
            slots += item.getCount()
        } else if ( !player.getInventory().getItemByItemId( item.getId() ) ) {
            slots++
        }

        return false
    } )

    if ( shouldExit ) {
        return false
    }

    if ( !player.getInventory().validateCapacity( slots ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_CANCEL_INVENTORY_FULL ) )
        return false
    }

    if ( !player.getInventory().validateWeight( weight ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_CANCEL_INVENTORY_FULL ) )
        return false
    }

    for ( const item of items ) {
        if ( !item ) {
            continue
        }

        let count = item.getCount()
        let newItem: L2ItemInstance = await attachments.transferItem( item.getObjectId(), count, player.getInventory(), 0, attachments.getName() )
        if ( !newItem ) {
            return false
        }

        let messagePacket = new SystemMessageBuilder( SystemMessageIds.YOU_ACQUIRED_S2_S1 )
            .addItemNameWithId( item.getId() )
            .addNumber( count )
            .getBuffer()

        player.sendOwnedData( messagePacket )
    }

    return true
}