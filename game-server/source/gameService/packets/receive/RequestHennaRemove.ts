import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Henna } from '../../models/items/L2Henna'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerHennaCache } from '../../cache/PlayerHennaCache'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestHennaRemove( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let symbolId: number = packet.readD()
    ReadableClientPacketPool.recycleValue( packet )

    let playerHenna = PlayerHennaCache.getHennas( player.getObjectId() )

    let slot = playerHenna.findIndex( item => {
        return item && item.getDyeId() === symbolId
    } )

    if ( slot < 0 ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let henna: L2Henna = playerHenna[ slot ]
    if ( player.getAdena() >= henna.getCancelFee() ) {
        return player.removeHenna( slot )
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
    player.sendOwnedData( ActionFailed() )
}