import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { MailMessage } from '../../models/mail/MailMessage'
import { MailManager } from '../../instancemanager/MailManager'
import { ItemContainer } from '../../models/itemcontainer/ItemContainer'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemLocation } from '../../enums/ItemLocation'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2World } from '../../L2World'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { ExChangePostStateSingle } from '../send/ExChangePostState'
import { ItemTypes } from '../../values/InventoryValues'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, PlayerReceivesTradeItemEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import aigle from 'aigle'
import { FastRateLimit } from 'fast-ratelimit'
import { AreaType } from '../../models/areas/AreaType'
import { PlayerPermission } from '../../enums/PlayerPermission'
import { ChangePostState } from '../../enums/ChangePostState'
import { recordMailViolation } from '../../helpers/PlayerViolations'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 5, // time-to-live value of token bucket (in seconds)
} )

export async function RequestPostAttachment( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player || !ConfigManager.general.allowMail() || !ConfigManager.general.allowAttachments() ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    if ( !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.sendMessage( 'Transactions are disabled for your Access Level' )
        return
    }

    if ( !player.isInArea( AreaType.Peace ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_RECEIVE_NOT_IN_PEACE_ZONE ) )
        return
    }

    if ( player.getActiveTradeList() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_RECEIVE_DURING_EXCHANGE ) )
        return
    }

    if ( player.isEnchanting() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_RECEIVE_DURING_ENCHANT ) )
        return
    }

    if ( player.getPrivateStoreType() !== PrivateStoreType.None ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_RECEIVE_PRIVATE_STORE ) )
        return
    }

    let messageId: number = new ReadableClientPacket( packetData ).readD()

    let message: MailMessage = MailManager.getMessage( messageId )
    if ( !message ) {
        return
    }

    if ( message.getReceiverId() !== player.getObjectId() ) {
        return recordMailViolation( player.getObjectId(), '85551317-844f-4121-bf76-d19121770eb9', message.getId(), message.getReceiverId(), message.getSenderId(), 'Player tried to receive not owned mail message', )
    }

    if ( !message.hasAttachments() ) {
        return
    }

    let attachments: ItemContainer = message.getLoadedAttachments()
    if ( !attachments ) {
        return
    }

    let weight = 0
    let slots = 0

    /*
        Please note that direct iteration of inventory contents while removing items
        can change order of available items.
     */
    let items = attachments.getItems().slice( 0 )
    let shouldExit = items.some( ( item: L2ItemInstance ): boolean => {
        if ( item ) {
            return false
        }

        if ( item.getOwnerId() !== player.getObjectId() ) {
            void recordMailViolation( player.getObjectId(), '1a8a12cf-eaef-4cb1-891e-6859690b8a3e', message.getId(), message.getReceiverId(), message.getSenderId(), 'Player tried to get not owned item from cancelled attachment' )
            return true
        }

        if ( item.getItemLocation() !== ItemLocation.MAIL ) {
            // should this be classified as data integrity issue?
            void recordMailViolation( player.getObjectId(), '84059dac-7577-4487-bc58-190c5f68d792', message.getId(), message.getReceiverId(), message.getSenderId(), 'Player tried to get items not from mail' )
            return true
        }

        if ( item.getLocationSlot() !== message.getId() ) {
            // should this be classified as data integrity issue?
            void recordMailViolation( player.getObjectId(), '88eb96bb-f552-4e8f-8ff9-ace55b763e91', message.getId(), message.getReceiverId(), message.getSenderId(), 'Player tried to get items from different attachment' )
            return true
        }

        weight += item.getCount() * item.getItem().getWeight()
        if ( !item.isStackable() ) {
            slots += item.getCount()
        } else if ( !player.getInventory().getItemByItemId( item.getId() ) ) {
            slots++
        }

        return false
    } )

    if ( shouldExit ) {
        return
    }

    if ( !player.getInventory().validateCapacity( slots ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_RECEIVE_INVENTORY_FULL ) )
        return
    }

    if ( !player.getInventory().validateWeight( weight ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_RECEIVE_INVENTORY_FULL ) )
        return
    }

    let adena = message.getCODAdena()
    if ( adena > 0 && !( await player.reduceAdena( adena, true, 'RequestPostAttachment' ) ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_RECEIVE_NO_ADENA ) )
        return
    }

    MailManager.removeCOD( message )

    let shouldLogItems: boolean = ListenerCache.hasGeneralListener( EventType.PlayerReceivesTradeItem )
    let eventPromises: Array<Promise<void>> = shouldLogItems ? [] : null

    shouldExit = await aigle.resolve( items ).someSeries( async ( item: L2ItemInstance ) => {
        if ( !item ) {
            return false
        }

        let count = item.getCount()
        let newItem: L2ItemInstance = await attachments.transferItem( item.getObjectId(), item.getCount(), player.getInventory(), 0, attachments.getName() )

        if ( shouldLogItems ) {
            let data = EventPoolCache.getData( EventType.PlayerReceivesTradeItem ) as PlayerReceivesTradeItemEvent

            data.type = 'RequestPostAttachment'
            data.itemId = item.getId()
            data.itemAmount = item.getCount()
            data.receiverId = player.getObjectId()
            data.senderId = message.getSenderId()

            eventPromises.push( ListenerCache.sendGeneralEvent( EventType.PlayerReceivesTradeItem, data ) )
        }

        if ( !newItem ) {
            return true
        }

        let messagePacket = new SystemMessageBuilder( SystemMessageIds.YOU_ACQUIRED_S2_S1 )
                .addItemNameWithId( item.getId() )
                .addNumber( count )
                .getBuffer()
        player.sendOwnedData( messagePacket )

        return false
    } )

    if ( eventPromises && eventPromises.length > 0 ) {
        await Promise.all( eventPromises )
    }

    if ( shouldExit ) {
        return
    }

    MailManager.removeAttachments( messageId )

    let sender: L2PcInstance = L2World.getPlayer( message.getSenderId() )
    if ( adena > 0 ) {
        if ( sender ) {
            await sender.addAdena( adena, 'PayMail', player.getObjectId(), false )

            let messagePacket = new SystemMessageBuilder( SystemMessageIds.PAYMENT_OF_S1_ADENA_COMPLETED_BY_S2 )
                    .addNumber( adena )
                    .addCharacterName( player )
                    .getBuffer()
            sender.sendOwnedData( messagePacket )
        } else {
            let paidAdena: L2ItemInstance = ItemManagerCache.createItem( ItemTypes.Adena, 'PayMail', adena, player.getObjectId() )
            await paidAdena.setOwnership( message.getSenderId(), ItemLocation.INVENTORY )
            await paidAdena.updateDatabase()

            L2World.removeObject( paidAdena )
        }
    } else if ( sender ) {
        let messagePacket = new SystemMessageBuilder( SystemMessageIds.S1_ACQUIRED_ATTACHED_ITEM )
                .addPlayerCharacterName( player )
                .getBuffer()

        sender.sendOwnedData( messagePacket )
    }

    player.sendOwnedData( ExChangePostStateSingle( true, messageId, ChangePostState.Read ) )
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MAIL_SUCCESSFULLY_RECEIVED ) )
}