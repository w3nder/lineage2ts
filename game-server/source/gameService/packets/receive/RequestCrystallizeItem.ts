import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ActionFailed } from '../send/ActionFailed'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { CrystalType } from '../../models/items/type/CrystalType'
import { ConfigManager } from '../../../config/ConfigManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import _ from 'lodash'
import { CommonSkillIds } from '../../enums/skills/CommonSkillIds'
import { recordReceivedPacketViolation } from '../../helpers/PlayerViolations'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 3, // time-to-live value of token bucket (in seconds)
} )

const skillAbilityMap = {
    [ CrystalType.C ]: 1,
    [ CrystalType.B ]: 2,
    [ CrystalType.A ]: 3,
    [ CrystalType.S ]: 4,
}

export async function RequestCrystallizeItem( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    let packet = new ReadableClientPacket( packetData )
    let objectId = packet.readD(), count = packet.readQ()

    if ( count <= 0 ) {
        return recordReceivedPacketViolation( player.getObjectId(), '2afd4910-aed2-4830-b7fa-f2f670b70471', 'Count cannot be or lower than zero', RequestCrystallizeItem.name, objectId, count )
    }

    if ( ( player.getPrivateStoreType() !== PrivateStoreType.None ) || player.isInCrystallize() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_TRADE_DISCARD_DROP_ITEM_WHILE_IN_SHOPMODE ) )
        return
    }

    let skillLevel = player.getSkillLevel( CommonSkillIds.CrystallizeItems )
    if ( skillLevel <= 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CRYSTALLIZE_LEVEL_TOO_LOW ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    let item : L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    if ( !item
            || item.isShadowItem()
            || item.isTimeLimitedItem()
            || item.isHeroItem()
            || !item.getItem().isCrystallizable()
            || ( item.getItem().getCrystalCount() <= 0 )
            || ( item.getItem().getCrystalType() === CrystalType.NONE ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    if ( count > item.getCount() ) {
        count = item.getCount()
    }

    if ( !player.getInventory().canManipulateWithItemId( item.getId() ) ) {
        return player.sendMessage( 'You cannot use this item.' )
    }

    let crystalizeLevel = skillAbilityMap[ item.getItem().getItemGradeSPlus() ]
    let canCrystallize = skillLevel > _.defaultTo( crystalizeLevel, Number.MAX_SAFE_INTEGER )

    if ( !canCrystallize ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CRYSTALLIZE_LEVEL_TOO_LOW ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    player.setInCrystallize( true )

    if ( item.isEquipped() ) {
        await player.getInventory().unEquipItemInSlot( item.getLocationSlot() )
        if ( item.getEnchantLevel() > 0 ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.EQUIPMENT_S1_S2_REMOVED )
                    .addNumber( item.getEnchantLevel() )
                    .addItemInstanceName( item )
                    .getBuffer()
            player.sendOwnedData( packet )
        } else {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_DISARMED )
                    .addItemInstanceName( item )
                    .getBuffer()
            player.sendOwnedData( packet )
        }
    }

    let removedItem : L2ItemInstance = await player.getInventory().destroyItem( item, count, 0, 'RequestCrystallizeItem' )
    if ( !removedItem ) {
        // TODO : handle violation
        return
    }

    // TODO : add feature that increases/decreases crystal count base on multiplier, possibly randomization option
    let crystalId = removedItem.getItem().getCrystalItemId()
    let crystalAmount = Math.floor( removedItem.getCrystalCount() * ConfigManager.tuning.getCrystallizeCountMultiplier() )

    if ( crystalAmount > 0 ) {
        await player.getInventory().addItem( crystalId, crystalAmount, player.objectId, 'RequestCrystallizeItem' )
    }

    let crystalPacket = new SystemMessageBuilder( SystemMessageIds.S1_CRYSTALLIZED )
            .addItemNameWithId( crystalId )
            .getBuffer()
    player.sendOwnedData( crystalPacket )

    let earnedPacket = new SystemMessageBuilder( SystemMessageIds.EARNED_S2_S1_S )
            .addItemNameWithId( crystalId )
            .addNumber( crystalAmount )
            .getBuffer()
    player.sendOwnedData( earnedPacket )

    player.broadcastUserInfo()

    player.setInCrystallize( false )
}