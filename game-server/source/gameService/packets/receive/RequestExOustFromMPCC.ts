import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestExOustFromMPCC( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player || !player.isInParty() || player.getParty().isInCommandChannel() ) {
        return
    }

    let name : string = new ReadableClientPacket( packetData ).readS()

    let target : L2PcInstance = L2World.getPlayerByName( name )
    if ( !target ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_CANT_FOUND ) )
        return
    }

    if ( target.getObjectId() === player.getObjectId()
            || !target.isInParty()
            || player.getParty().getCommandChannel().getLeaderObjectId() !== player.getObjectId()
            || player.getParty().getCommandChannel() !== target.getParty().getCommandChannel() ) {
        return
    }

    target.getParty().getCommandChannel().removeParty( target.getParty() )
    target.getParty().broadcastPacket( SystemMessageBuilder.fromMessageId( SystemMessageIds.DISMISSED_FROM_COMMAND_CHANNEL ) )

    if ( player.getParty().isInCommandChannel() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_PARTY_DISMISSED_FROM_COMMAND_CHANNEL )
                .addString( target.getParty().getLeader().getName() )
                .getBuffer()
        player.getParty().getCommandChannel().broadcastPacket( packet )
    }
}