import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2AirShipInstance } from '../../models/actor/instance/L2AirShipInstance'
import { AirShipManager } from '../../instancemanager/AirShipManager'
import { VehiclePathPoint } from '../../models/VehiclePathPoint'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { FastRateLimit } from 'fast-ratelimit'
import { WorldFlyingLimits } from '../../enums/WorldFlyingLimits'

const STEP = 300

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function MoveToLocationAirShip( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !player.isInAirShip() ) {
        return
    }

    let ship : L2AirShipInstance = player.getAirShip()
    if ( !ship.isCaptain( player ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let command = packet.readD(), parameterOne = packet.readD()
    let parameterTwo = 0

    if ( packet.hasMoreData() ) {
        parameterTwo = packet.readD()
    }

    let zValue = ship.getZ()
    switch ( command ) {
        case 0:
            if ( !ship.canBeControlled() ) {
                return
            }

            if ( parameterOne < WorldFlyingLimits.GraciaMaxX ) {
                return AIEffectHelper.notifyMoveWithCoordinates( ship, parameterOne, parameterTwo, zValue )
            }

            break

        case 1:
            if ( !ship.canBeControlled() ) {
                return
            }

            return

        case 2:
            if ( !ship.canBeControlled() ) {
                return
            }

            if ( zValue < WorldFlyingLimits.GraciaMaxZ ) {
                zValue = Math.min( zValue + STEP, WorldFlyingLimits.GraciaMaxZ )
                return AIEffectHelper.notifyMoveWithCoordinates( ship, ship.getX(), ship.getY(), zValue )
            }
            break

        case 3:
            if ( !ship.canBeControlled() ) {
                return
            }

            if ( zValue > WorldFlyingLimits.GraciaMinZ ) {
                zValue = Math.max( zValue - STEP, WorldFlyingLimits.GraciaMinZ )
                return AIEffectHelper.notifyMoveWithCoordinates( ship, ship.getX(), ship.getY(), zValue )
            }

            break

        case 4:
            if ( !ship.isInDock() || ship.isMoving() ) {
                return
            }

            let destinations : Array<VehiclePathPoint> = AirShipManager.getTeleportDestination( ship.getDockId(), parameterOne )
            if ( !destinations ) {
                return
            }

            let fuelConsumption = AirShipManager.getFuelConsumption( ship.getDockId(), parameterOne )
            if ( fuelConsumption > 0 ) {
                if ( fuelConsumption > ship.getFuel() ) {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_AIRSHIP_CANNOT_TELEPORT ) )
                    return
                }

                ship.setFuel( ship.getFuel() - fuelConsumption )
            }

            return ship.executePath( destinations )
    }
}