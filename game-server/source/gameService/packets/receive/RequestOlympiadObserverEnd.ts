import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export function RequestOlympiadObserverEnd( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.inObserverMode() ) {
        player.leaveOlympiadObserverMode()
    }
}