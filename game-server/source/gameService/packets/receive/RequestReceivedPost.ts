import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { MailMessage } from '../../models/mail/MailMessage'
import { MailManager } from '../../instancemanager/MailManager'
import { ExChangePostStateSingle } from '../send/ExChangePostState'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ExReplyReceivedPost } from '../send/ExReplyReceivedPost'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { AreaType } from '../../models/areas/AreaType'
import { ChangePostState } from '../../enums/ChangePostState'
import { L2MessagesTableItemStatus } from '../../../database/interface/MessagesTableApi'
import { recordBuySellViolation, recordMailViolation } from '../../helpers/PlayerViolations'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestReceivedPost( client: GameClient, packetData: Buffer ) : void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player || !ConfigManager.general.allowMail() ) {
        return
    }

    let messageId : number = new ReadableClientPacket( packetData ).readD()

    let message: MailMessage = MailManager.getMessage( messageId )
    if ( !message ) {
        return
    }

    if ( !player.isInArea( AreaType.Peace ) && message.hasAttachments() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_USE_MAIL_OUTSIDE_PEACE_ZONE ) )
        return
    }

    if ( message.getReceiverId() !== player.getObjectId() ) {
        void recordMailViolation( player.getObjectId(), '503d0b0a-643f-437b-ab42-812eaa006d0a', message.getId(), message.getReceiverId(), message.getSenderId(), 'Player tried to receive not owned mail message', )
        return
    }

    player.sendOwnedData( ExReplyReceivedPost( message ) )
    player.sendOwnedData( ExChangePostStateSingle( true, messageId, ChangePostState.Read ) )

    return MailManager.setMessageStatus( messageId, L2MessagesTableItemStatus.Read )
}