import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2Clan } from '../../models/L2Clan'
import { CrestType, L2Crest } from '../../models/L2Crest'
import { CrestCache } from '../../cache/CrestCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 10, // time-to-live value of token bucket (in seconds)
} )

export async function RequestSetPledgeCrest( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()
    if ( !clan ) {
        return
    }

    if ( clan.getDissolvingExpiryTime() > Date.now() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_SET_CREST_WHILE_DISSOLUTION_IN_PROGRESS ) )
        return
    }

    if ( !player.hasClanPrivilege( ClanPrivilege.RegisterCrest ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let length : number = packet.readD()

    if ( length < 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WRONG_SIZE_UPLOADED_CREST ) )
        return
    }

    if ( length > 256 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_SIZE_OF_THE_IMAGE_FILE_IS_INAPPROPRIATE ) )
        return
    }

    if ( length === 0 && clan.getCrestId() !== 0 ) {
        await clan.changeClanCrest( 0 )
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_CREST_HAS_BEEN_DELETED ) )
    }

    if ( clan.getLevel() < 3 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_LVL_3_NEEDED_TO_SET_CREST ) )
        return
    }

    let imageData : Buffer = packet.readB( length )
    let crest : L2Crest = await CrestCache.createCrest( imageData, CrestType.PLEDGE )

    if ( crest ) {
        await clan.changeClanCrest( crest.getId() )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_CREST_WAS_SUCCESSFULLY_REGISTRED ) )
    }
}