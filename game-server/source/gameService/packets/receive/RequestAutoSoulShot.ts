import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ExAutoSoulShot, ExAutoSoulShotStatus } from '../send/ExAutoSoulShot'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { BeastSoulShot } from '../../handler/itemhandlers/BeastSoulShot'
import { recordReceivedPacketViolation } from '../../helpers/PlayerViolations'

export async function RequestAutoSoulShot( client: GameClient, packetData: Buffer ) : Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( ( player.getPrivateStoreType() !== PrivateStoreType.None ) || player.getActiveRequester() || player.isDead() ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let itemId = packet.readD(), type = packet.readD()

    let item = player.getInventory().getItemByItemId( itemId )
    if ( !item ) {
        return
    }

    if ( type === ExAutoSoulShotStatus.Enabled ) {
        if ( !player.getInventory().canManipulateWithItemId( item.getId() ) ) {
            player.sendMessage( 'Cannot use this item.' )
            return
        }

        /*
            Handle fishing shots
         */
        if ( ( itemId < 6535 ) || ( itemId > 6540 ) ) {
            if ( ( itemId === 6645 ) || ( itemId === 6646 ) || ( itemId === 6647 ) || ( itemId === 20332 ) || ( itemId === 20333 ) || ( itemId === 20334 ) ) {
                if ( player.hasSummon() ) {
                    if ( item.getEtcItem().getItemHandler() === BeastSoulShot ) {
                        if ( player.getSummon().getSoulShotsPerHit() > item.getCount() ) {
                            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_SOULSHOTS_FOR_PET ) )
                            return
                        }
                    } else {
                        if ( player.getSummon().getSpiritShotsPerHit() > item.getCount() ) {
                            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_SOULSHOTS_FOR_PET ) )
                            return
                        }
                    }

                    player.addAutoSoulShot( itemId )
                    player.sendOwnedData( ExAutoSoulShot( itemId, type ) )

                    let packet = new SystemMessageBuilder( SystemMessageIds.USE_OF_S1_WILL_BE_AUTO )
                            .addItemInstanceName( item )
                            .getBuffer()
                    player.sendOwnedData( packet )

                    await player.rechargeShots( true, true )
                    await player.getSummon().rechargeShots( true, true )

                } else {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_SERVITOR_CANNOT_AUTOMATE_USE ) )
                }
            } else {
                if ( ( player.getActiveWeaponItem() !== player.getFistsWeaponItem() ) && ( item.getItem().getCrystalType() === player.getActiveWeaponItem().getItemGradeSPlus() ) ) {
                    player.addAutoSoulShot( itemId )
                    player.sendOwnedData( ExAutoSoulShot( itemId, type ) )
                } else {
                    if ( ( ( itemId >= 2509 ) && ( itemId <= 2514 ) )
                            || ( ( itemId >= 3947 ) && ( itemId <= 3952 ) )
                            || ( itemId === 5790 ) || ( ( itemId >= 22072 ) && ( itemId <= 22081 ) ) ) {
                        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SPIRITSHOTS_GRADE_MISMATCH ) )
                    } else {
                        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SOULSHOTS_GRADE_MISMATCH ) )
                    }

                    player.addAutoSoulShot( itemId )
                    player.sendOwnedData( ExAutoSoulShot( itemId, type ) )
                }

                let packet = new SystemMessageBuilder( SystemMessageIds.USE_OF_S1_WILL_BE_AUTO )
                        .addItemInstanceName( item )
                        .getBuffer()
                player.sendOwnedData( packet )
                await player.rechargeShots( true, true )
            }
        }
        return
    }

    if ( type === ExAutoSoulShotStatus.Disabled ) {
        player.removeAutoSoulShot( itemId )
        player.sendOwnedData( ExAutoSoulShot( itemId, type ) )

        let packet = new SystemMessageBuilder( SystemMessageIds.AUTO_USE_OF_S1_CANCELLED )
                .addItemInstanceName( item )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    return recordReceivedPacketViolation( player.getObjectId(), '2c7936d7-428c-458d-8502-6526ebfb1133', 'Operation type value cannot be processed', 'RequestAutoSoulShot', type )
}