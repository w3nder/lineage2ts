import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { StartRotation } from '../send/StartRotation'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function StartRotating( client: GameClient, packetData: Buffer ): void {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let degree = packet.readD(), side = packet.readD()

    let airShip = player.getAirShip()
    if ( airShip && airShip.isCaptain( player ) ) {
        return BroadcastHelper.dataBasedOnVisibility( airShip, StartRotation( airShip.getObjectId(), degree, side, 0 ) )
    }

    return BroadcastHelper.dataBasedOnVisibility( player, StartRotation( player.getObjectId(), degree, side, 0 ) )
}