import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { NpcHtmlMessagePath } from '../send/NpcHtmlMessage'
import { L2NpcValues } from '../../values/L2NpcValues'
import { DataManager } from '../../../data/manager'
import { HtmlActionCache } from '../../cache/HtmlActionCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestLinkHtml( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let link : string = new ReadableClientPacket( packetData ).readS()
    if ( !link ) {
        return
    }

    let [ objectId, correctedLink ] = HtmlActionCache.validateHtmlAction( player.getObjectId(), `link ${link}` )
    if ( objectId === -1 ) {
        return
    }

    if ( objectId > 0 && !GeneralHelper.isInsideRangeOfObjectId( player, objectId, L2NpcValues.interactionDistance ) ) {
        return
    }

    let path = `data/html/${correctedLink}`
    if ( !DataManager.getHtmlData().hasItem( path ) ) {
        return
    }

    player.sendOwnedData( NpcHtmlMessagePath( DataManager.getHtmlData().getItem( path ), path, player.getObjectId(), objectId ) )
}