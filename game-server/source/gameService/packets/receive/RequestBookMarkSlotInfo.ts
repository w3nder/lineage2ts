import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExGetBookMarkInfoPacketWithPlayer } from '../send/ExGetBookMarkInfoPacket'

export function RequestBookMarkSlotInfo( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    player.sendOwnedData( ExGetBookMarkInfoPacketWithPlayer( player ) )
}