import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2World } from '../../L2World'
import { Skill } from '../../models/Skill'
import { SkillCache } from '../../cache/SkillCache'
import { AcquireSkillType } from '../../enums/AcquireSkillType'
import { L2SkillLearn } from '../../models/L2SkillLearn'
import { ClassId } from '../../models/base/ClassId'
import { AcquireSkillInfo, AcquireSkillInfoWithSp } from '../send/AcquireSkillInfo'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestAcquireSkillInfo( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let id = packet.readD(), level = packet.readD(), skillType = packet.readD()

    if ( id <= 0 || level <= 0 ) {
        return
    }

    let trainer: L2Npc = L2World.getObjectById( player.getLastFolkNPC() ) as L2Npc
    if ( !trainer || !trainer.isNpc() ) {
        return
    }

    if ( !player.canInteract( trainer ) && !player.isGM() ) {
        return
    }

    let skill: Skill = SkillCache.getSkill( id, level )
    if ( !skill ) {
        return
    }

    // Hack check. Doesn't apply to all Skill Types
    let previousSkillLevel = player.getSkillLevel( id )
    if ( ( previousSkillLevel > 0 ) && !( ( skillType === AcquireSkillType.Transfer ) || ( skillType === AcquireSkillType.SubPledge ) ) ) {
        if ( previousSkillLevel === level ) {
            return
        }

        if ( previousSkillLevel !== ( level - 1 ) ) {
            return
        }
    }

    let skillLearn: L2SkillLearn = SkillCache.getSkillLearn( skillType, id, level, player )
    if ( !skillLearn ) {
        return
    }

    switch ( skillType ) {
        case AcquireSkillType.Transform:
        case AcquireSkillType.Fishing:
        case AcquireSkillType.Subclass:
        case AcquireSkillType.Collect:
        case AcquireSkillType.Transfer:
            player.sendOwnedData( AcquireSkillInfo( skillType, skillLearn ) )
            return
        case AcquireSkillType.Class:
            let learningClassId = ClassId.getClassIdByIdentifier( player.getLearningClass() )
            if ( trainer.getTemplate().canTeach( learningClassId ) ) {
                let customSp = skillLearn.getCalculatedLevelUpSp( ClassId.getClassIdByIdentifier( player.getClassId() ), learningClassId )
                player.sendOwnedData( AcquireSkillInfoWithSp( skillType, skillLearn, customSp ) )
            }
            break
        case AcquireSkillType.Pledge:
            if ( !player.isClanLeader() ) {
                return
            }
            player.sendOwnedData( AcquireSkillInfo( skillType, skillLearn ) )
            break
        case AcquireSkillType.SubPledge:
            if ( !player.isClanLeader() || !player.hasClanPrivilege( ClanPrivilege.NameTroops ) ) {
                return
            }
            player.sendOwnedData( AcquireSkillInfo( skillType, skillLearn ) )
            return
    }
}