import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ClassId } from '../../models/base/ClassId'
import { ExShowDominionRegistry } from '../send/ExShowDominionRegistry'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

export async function RequestJoinDominionWar( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let clan : L2Clan = player.getClan()

    if ( TerritoryWarManager.getIsRegistrationOver() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_TERRITORY_REGISTRATION_PERIOD ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let territoryId = packet.readD(), isClanValue = packet.readD(), isJoiningValue = packet.readD()

    let castleId = territoryId - 80
    let ownerClan = TerritoryWarManager.getTerritory( castleId ).getOwnerClan()
    if ( clan && ownerClan && ownerClan.getId() === clan.getId() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_TERRITORY_OWNER_CLAN_CANNOT_PARTICIPATE_AS_MERCENARIES ) )
        return
    }

    if ( isClanValue === 1 ) {
        if ( !player.hasClanPrivilege( ClanPrivilege.CastleSiege ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
            return
        }

        if ( !clan ) {
            return
        }

        if ( isJoiningValue === 1 ) {
            if ( Date.now() < clan.getDissolvingExpiryTime() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_PARTICIPATE_IN_SIEGE_WHILE_DISSOLUTION_IN_PROGRESS ) )
                return
            }

            if ( TerritoryWarManager.checkIsRegisteredClan( -1, clan ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ALREADY_REQUESTED_TW_REGISTRATION ) )
                return
            }

            await TerritoryWarManager.registerClan( castleId, clan )
        } else {
            await TerritoryWarManager.removeClan( castleId, clan )
        }

    } else {
        if ( player.getLevel() < 40 || ClassId.getClassIdByIdentifier( player.getClassId() ).level < 2 ) {
            return
        }

        if ( isJoiningValue === 1 ) {
            if ( TerritoryWarManager.checkIsRegistered( -1, player.getObjectId() ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ALREADY_REQUESTED_TW_REGISTRATION ) )
                return
            }

            if ( TerritoryWarManager.checkIsRegisteredClan( -1, clan ) ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ALREADY_REQUESTED_TW_REGISTRATION ) )
                return
            }

            await TerritoryWarManager.registerMercenary( castleId, player )
        } else {
            await TerritoryWarManager.removeMercenary( castleId, player )
        }
    }

    player.sendOwnedData( ExShowDominionRegistry( castleId, player ) )
}