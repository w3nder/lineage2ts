import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PartyMatchManager } from '../../cache/partyMatchManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestDismissPartyRoom( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let roomId : number = new ReadableClientPacket( packetData ).readD()
    PartyMatchManager.deleteRoomById( roomId )
}