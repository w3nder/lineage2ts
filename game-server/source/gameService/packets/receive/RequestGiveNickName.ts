import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2ClanMember } from '../../models/L2ClanMember'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestGiveNickName( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let targetName = packet.readS(), title = packet.readS()

    if ( player.isNoble() && player.getName().toLowerCase() === targetName.toLowerCase() ) {
        player.setTitle( title )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TITLE_CHANGED ) )
        player.broadcastTitleInfo()
        return
    }

    if ( !player.hasClanPrivilege( ClanPrivilege.GrantTitle ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
        return
    }

    if ( player.getClan().getLevel() < 3 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_LVL_3_NEEDED_TO_ENDOWE_TITLE ) )
        return
    }

    let matchingMember : L2ClanMember = player.getClan().getClanMemberByName( targetName as string )
    if ( matchingMember ) {
        let otherPlayer : L2PcInstance = matchingMember.getPlayerInstance()
        if ( otherPlayer ) {
            otherPlayer.setTitle( title )
            otherPlayer.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TITLE_CHANGED ) )
            otherPlayer.broadcastTitleInfo()
            return
        }

        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_NOT_FOUND_IN_THE_GAME ) )
    }

    return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_MUST_BE_IN_CLAN ) )
}