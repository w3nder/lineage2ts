import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CommunityBoardManager } from '../../cache/CommunityBoardManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import _ from 'lodash'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestBBSWrite( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let values : Array<string> = _.times( 6, () : string => packet.readS() )

    return CommunityBoardManager.onWriteCommand( player, values )
}