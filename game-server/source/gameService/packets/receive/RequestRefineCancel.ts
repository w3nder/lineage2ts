import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { CrystalType } from '../../models/items/type/CrystalType'
import { ExVariationCancelResult, ExVariationCancelResultType } from '../send/ExVariationCancelResult'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { InventoryUpdateStatus } from '../../enums/InventoryUpdateStatus'
import { L2World } from '../../L2World'
import { FastRateLimit } from 'fast-ratelimit'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, PlayerAugmentItemEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestRefineCancel( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let objectId: number = new ReadableClientPacket( packetData ).readD()

    let worldItem = L2World.getObjectById( objectId ) as L2ItemInstance
    if ( !worldItem || !worldItem.isItem() || worldItem.getOwnerId() !== player.getObjectId() ) {
        // TODO : record violation, item does not belong to player
        player.sendOwnedData( ExVariationCancelResult( ExVariationCancelResultType.Failure ) )
        return
    }

    let targetItem: L2ItemInstance = player.getInventory().getItemByObjectId( objectId )
    if ( !targetItem ) {
        player.sendOwnedData( ExVariationCancelResult( ExVariationCancelResultType.Failure ) )
        return
    }

    if ( !targetItem.isAugmented() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.AUGMENTATION_REMOVAL_CAN_ONLY_BE_DONE_ON_AN_AUGMENTED_ITEM ) )
        player.sendOwnedData( ExVariationCancelResult( ExVariationCancelResultType.Failure ) )
        return
    }

    let price
    switch ( targetItem.getItem().getCrystalType() ) {
        case CrystalType.C:
            if ( targetItem.getCrystalCount() < 1720 ) {
                price = 95000
            } else if ( targetItem.getCrystalCount() < 2452 ) {
                price = 150000
            } else {
                price = 210000
            }
            break

        case CrystalType.B:
            if ( targetItem.getCrystalCount() < 1746 ) {
                price = 240000
            } else {
                price = 270000
            }
            break

        case CrystalType.A:
            if ( targetItem.getCrystalCount() < 2160 ) {
                price = 330000
            } else if ( targetItem.getCrystalCount() < 2824 ) {
                price = 390000
            } else {
                price = 420000
            }
            break

        case CrystalType.S:
            price = 480000
            break

        case CrystalType.S80:
        case CrystalType.S84:
            price = 920000
            break

        default:
            player.sendOwnedData( ExVariationCancelResult( ExVariationCancelResultType.Failure ) )
            return
    }

    if ( !( await player.reduceAdena( price, true, 'RequestRefineCancel' ) ) ) {
        player.sendOwnedData( ExVariationCancelResult( ExVariationCancelResultType.Failure ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
        return
    }

    if ( targetItem.isEquipped() ) {
        await player.disarmWeapons()
    }

    targetItem.removeAugmentation()
    player.getInventory().markItem( targetItem, InventoryUpdateStatus.Modified )
    player.sendOwnedData( ExVariationCancelResult( ExVariationCancelResultType.Success ) )

    if ( ListenerCache.hasItemTemplateEvent( targetItem.getId(), EventType.PlayerAugmentItem ) ) {
        let eventData = EventPoolCache.getData( EventType.PlayerAugmentItem ) as PlayerAugmentItemEvent

        eventData.objectId = targetItem.getObjectId()
        eventData.playerId = player.getObjectId()
        eventData.isAugmented = false
        eventData.itemId = targetItem.getId()

        return ListenerCache.sendItemTemplateEvent( targetItem.getId(), EventType.PlayerAugmentItem, eventData )
    }
}