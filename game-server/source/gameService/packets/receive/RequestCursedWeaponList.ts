import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExCursedWeaponList } from '../send/ExCursedWeaponList'

export function RequestCursedWeaponList( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    player.sendOwnedData( ExCursedWeaponList() )
}