import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ExAskJoinPartyRoom } from '../send/ExAskJoinPartyRoom'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestAskJoinPartyRoom( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let name : string = new ReadableClientPacket( packetData ).readS()
    let target : L2PcInstance = L2World.getPlayerByName( name )
    if ( !target ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_NOT_FOUND_IN_THE_GAME ) )
        return
    }

    if ( target.isProcessingRequest() ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_BUSY_TRY_LATER )
                .addPlayerCharacterName( target )
                .getBuffer()
        player.sendOwnedData( packet )
        return
    }

    player.onTransactionRequest( target )
    target.sendOwnedData( ExAskJoinPartyRoom( player.getName() ) )
}