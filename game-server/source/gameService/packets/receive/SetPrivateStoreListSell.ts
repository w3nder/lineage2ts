import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PacketVariables } from '../PacketVariables'
import { TradeList } from '../../models/TradeList'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ActionFailed } from '../send/ActionFailed'
import { PrivateStoreMessageSell } from '../send/PrivateStoreMessageSell'
import { ExPrivateStoreSetWholesaleMessage } from '../send/ExPrivateStoreSetWholesaleMessage'
import { PrivateStoreManageListSell } from '../send/PrivateStoreManageListSell'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'
import { AreaFlags } from '../../models/areas/AreaFlags'
import { PlayerPermission } from '../../enums/PlayerPermission'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

interface SellItem {
    itemId: number
    count: number
    price: number
}

export async function SetPrivateStoreListSell( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let sale = packet.readD(), totalItems = packet.readD()
    let isPackageSale = sale === 1

    if ( ( totalItems < 1 ) || ( totalItems > PacketVariables.maximumItemsLimit ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_ITEM_COUNT ) )
        player.setPrivateStoreType( PrivateStoreType.None )
        player.broadcastUserInfo()
        return
    }

    let items : Array<SellItem> = []

    while ( totalItems > 0 ) {
        let itemId = packet.readD(), count = packet.readQ(), price = packet.readQ()

        if ( itemId < 1 || count < 1 || price < 0 ) {
            return
        }

        items.push( {
            itemId,
            count,
            price
        } )

        totalItems--
    }

    if ( !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
    }

    if ( player.isUnderAttack() || player.isInDuel() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_OPERATE_PRIVATE_STORE_DURING_COMBAT ) )
        player.sendOwnedData( PrivateStoreManageListSell( player, isPackageSale ) )
        return player.sendOwnedData( ActionFailed() )
    }

    if ( player.hasAreaFlags( AreaFlags.NoOpenStore ) ) {
        player.sendOwnedData( PrivateStoreManageListSell( player, isPackageSale ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_PRIVATE_STORE_HERE ) )
        return player.sendOwnedData( ActionFailed() )
    }

    if ( items.length > player.getPrivateSellStoreLimit() ) {
        player.sendOwnedData( PrivateStoreManageListSell( player, isPackageSale ) )
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_EXCEEDED_QUANTITY_THAT_CAN_BE_INPUTTED ) )
    }

    let tradeList : TradeList = player.getSellList()
    tradeList.clear()
    tradeList.setPackaged( isPackageSale )

    let totalCost = player.getAdena()
    let maxAdena = getMaxAdena()

    let shouldExit = items.some( ( item: SellItem ) : boolean => {
        let price = getPrice( item )
        if ( ( maxAdena - totalCost ) < price ) {
            return false
        }

        totalCost += price
        return !addToTradeList( tradeList, item, maxAdena )
    } )

    if ( shouldExit ) {
        return player.sendOwnedData( ActionFailed() )
    }

    await player.sitDown()
    if ( isPackageSale ) {
        player.setPrivateStoreType( PrivateStoreType.PackageSell )
    } else {
        player.setPrivateStoreType( PrivateStoreType.Sell )
    }

    player.broadcastUserInfo()

    if ( isPackageSale ) {
        return BroadcastHelper.dataToSelfInRange( player, ExPrivateStoreSetWholesaleMessage( player ) )
    }

    return BroadcastHelper.dataToSelfInRange( player, PrivateStoreMessageSell( player ) )
}

function getPrice( item: SellItem ) {
    return item.count * item.price
}

function addToTradeList( list: TradeList, item: SellItem, maxAdena: number ) : boolean {
    if ( ( maxAdena / item.count ) < item.price ) {
        return false
    }

    list.addItem( item.itemId, item.count, item.price )
    return true
}