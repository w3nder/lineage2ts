import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { NpcItemAuction } from '../../models/auction/NpcItemAuction'
import { ItemAuction } from '../../models/auction/ItemAuction'
import { ItemAuctionManager } from '../../cache/ItemAuctionManager'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestBidItemAuction( client: GameClient, packetData: Buffer ) : Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let npcId = packet.readD(), bidAmount = packet.readQ()

    if ( bidAmount < 0 || bidAmount > getMaxAdena() ) {
        return
    }

    let npcAuction : NpcItemAuction = ItemAuctionManager.getAuction( npcId )
    if ( npcAuction ) {
        let auction : ItemAuction = npcAuction.getCurrentAuction()
        if ( auction && await auction.registerBid( player, bidAmount ) ) {
            npcAuction.adjustNextAuctionTime()
        }
    }
}