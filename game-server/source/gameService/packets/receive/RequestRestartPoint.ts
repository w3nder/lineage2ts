import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ReadableClientPacketPool } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestRestartPoint( client: GameClient, packetData: Buffer ): Promise<void> {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !player.canRevive() ) {
        return
    }

    if ( player.isFakeDeath() ) {
        await player.stopFakeDeath( true )
        return
    }

    if ( !player.isDead() ) {
        return
    }

    let packet = ReadableClientPacketPool.getValue().setBuffer( packetData )
    let pointType: number = packet.readD()
    ReadableClientPacketPool.recycleValue( packet )

    return player.onRestartPoint( pointType )
}