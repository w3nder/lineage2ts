import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { PartyMatchManager } from '../../cache/partyMatchManager'
import { ExManagePartyRoomMember } from '../send/ExManagePartyRoomMember'
import { PacketDispatcher } from '../../PacketDispatcher'
import { JoinParty } from '../send/JoinParty'
import { PlayerGroupCache } from '../../cache/PlayerGroupCache'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

const enum JoinReplyType {
    DisabledByClient = -1,
    Rejected = 0,
    Accepted = 1
}

export function RequestAnswerJoinParty( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let requester: L2PcInstance = player.getActiveRequester()
    if ( !requester ) {
        return
    }

    let response: JoinReplyType = new ReadableClientPacket( packetData ).readD()

    requester.sendOwnedData( JoinParty( response ) )

    switch ( response ) {
        case JoinReplyType.DisabledByClient:
            let packet = new SystemMessageBuilder( SystemMessageIds.C1_IS_SET_TO_REFUSE_PARTY_REQUEST )
                    .addPlayerCharacterName( player )
                    .getBuffer()
            requester.sendOwnedData( packet )
            break

        case JoinReplyType.Accepted:
            if ( !requester.isInParty() ) {
                PlayerGroupCache.createNewParty( requester, requester.getPartyDistributionType() )
            }

            if ( requester.getParty().getMemberCount() >= 9 ) {
                let packet: Buffer = SystemMessageBuilder.fromMessageId( SystemMessageIds.PARTY_FULL )

                player.sendCopyData( packet )
                requester.sendCopyData( packet )
                return
            }

            PlayerGroupCache.joinParty( requester, player )

            let requesterRoom = PartyMatchManager.getPlayerRoom( requester.getObjectId() )
            if ( !requesterRoom ) {
                break
            }

            let playerRoom = PartyMatchManager.getPlayerRoom( player.getObjectId() )
            if ( requesterRoom === playerRoom ) {
                let packet: Buffer = ExManagePartyRoomMember( player.getObjectId(), requesterRoom, 1 )
                requesterRoom.members.forEach( ( memberId: number ) => {
                    PacketDispatcher.sendCopyData( memberId, packet )
                } )

                break
            }

            if ( playerRoom ) {
                break
            }

            PartyMatchManager.addToExistingRoom( requester.getObjectId(), player )
            break
    }

    if ( requester.isInParty() ) {
        requester.getParty().setPendingInvitation( false )
    }

    player.setActiveRequester( null )
    requester.onTransactionResponse()
}