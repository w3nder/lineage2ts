import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { RecipeItemMakeInfoWithPlayer } from '../send/RecipeItemMakeInfo'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { L2RecipeDefinition } from '../../models/l2RecipeDefinition'
import { DataManager } from '../../../data/manager'
import { ActionFailed } from '../send/ActionFailed'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestRecipeItemMakeInfo( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let recipeId: number = new ReadableClientPacket( packetData ).readD()
    let recipe: L2RecipeDefinition = DataManager.getRecipeData().getRecipeList( recipeId )
    if ( !recipe ) {
        return player.sendOwnedData( ActionFailed() )
    }

    player.sendOwnedData( RecipeItemMakeInfoWithPlayer( recipeId, player, true ) )
}