import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PartyMatchManager, PartyMatchRoom } from '../../cache/partyMatchManager'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ExClosePartyRoom } from '../send/ExClosePartyRoom'
import { PartyMatchWaitingList } from '../../cache/PartyMatchWaitingList'
import { ListPartyWaiting } from '../send/ListPartyWaiting'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { L2World } from '../../L2World'

export function RequestRemoveFromPartyRoom( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let playerId : number = new ReadableClientPacket( packetData ).readD()
    let room : PartyMatchRoom = PartyMatchManager.getPlayerRoom( playerId )
    if ( !room || room.leaderId !== player.getObjectId() ) {
        return
    }

    let member = L2World.getPlayer( playerId )
    if ( player.isInParty()
            && member.isInParty()
            && ( player.getParty().getLeaderObjectId() === member.getParty().getLeaderObjectId() ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_DISMISS_PARTY_MEMBER ) )
        return
    }

    PartyMatchManager.removePlayer( playerId )
    member.sendOwnedData( ExClosePartyRoom() )

    PartyMatchWaitingList.addPlayer( member )

    member.sendOwnedData( ListPartyWaiting( 0, true, member.getLevel() ) )
    member.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.OUSTED_FROM_PARTY_ROOM ) )
    member.broadcastUserInfo()
}