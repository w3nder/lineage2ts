import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PrivateStoreMessageSell } from '../send/PrivateStoreMessageSell'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const maximumLength = 30
const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function SetPrivateStoreMessageSell( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player : L2PcInstance = client.player
    if ( !player || !player.getSellList() ) {
        return
    }

    let message : string = new ReadableClientPacket( packetData ).readS()
    if ( message.length === 0 || message.length > maximumLength ) {
        return
    }

    player.getSellList().setTitle( message )
    player.sendOwnedData( PrivateStoreMessageSell( player ) )
}