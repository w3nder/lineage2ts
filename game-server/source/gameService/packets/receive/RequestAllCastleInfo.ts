import { GameClient } from '../../GameClient'
import { ExShowCastleInfo } from '../send/ExShowCastleInfo'

import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export function RequestAllCastleInfo( client: GameClient ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    client.sendPacket( ExShowCastleInfo() )
}