import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ClassId } from '../../models/base/ClassId'
import { Skill } from '../../models/Skill'
import { SkillCache } from '../../cache/SkillCache'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { SkillItemValues } from '../../values/SkillItemValues'
import { L2EnchantSkillLearn } from '../../models/L2EnchantSkillLearn'
import { DataManager } from '../../../data/manager'
import { EnchantSkillHolder } from '../../models/L2EnchantSkillGroup'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { ExEnchantSkillResult } from '../send/ExEnchantSkillResult'
import { UserInfo } from '../send/UserInfo'
import { ExBrExtraUserInfo } from '../send/ExBrExtraUserInfo'
import { ExEnchantSkillInfo } from '../send/ExEnchantSkillInfo'
import { EnchantType, ExEnchantSkillInfoDetail } from '../send/ExEnchantSkillInfoDetail'
import { ItemTypes } from '../../values/InventoryValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { PlayerShortcutCache } from '../../cache/PlayerShortcutCache'
import _ from 'lodash'

export async function RequestExEnchantSkillRouteChange( client: GameClient, packetData: Buffer ) : Promise<void> {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( player.getLevel() < 76 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_ON_THIS_LEVEL ) )
        return
    }

    if ( ClassId.getClassIdByIdentifier( player.getClassId() ).level < 3 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_IN_THIS_CLASS ) )
        return
    }

    if ( !player.isAllowedToEnchantSkills() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CANNOT_USE_SKILL_ENCHANT_ATTACKING_TRANSFORMED_BOAT ) )
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let skillId = packet.readD(), skillLevel = packet.readD()

    if ( skillId < 0 || skillLevel <= 0 ) {
        return
    }

    let skill : Skill = SkillCache.getSkill( skillId, skillLevel )
    if ( !skill ) {
        return
    }

    let reqItemId = SkillItemValues.ChangeEnchantBook

    let skillLearn : L2EnchantSkillLearn = DataManager.getSkillData().getSkillEnchantmentBySkillId( skillId )
    if ( !skillLearn ) {
        return
    }

    let beforeEnchantSkillLevel = player.getSkillLevel( skillId )
    if ( beforeEnchantSkillLevel <= 100 ) {
        return
    }

    let currentEnchantLevel = beforeEnchantSkillLevel % 100
    if ( currentEnchantLevel !== ( skillLevel % 100 ) ) {
        return
    }

    let esHolder : EnchantSkillHolder = skillLearn.getEnchantSkillHolder( skillLevel )
    let requiredSp = esHolder.getSpCost()
    let totalAmount = esHolder.getAdenaCost()

    if ( player.getSp() >= requiredSp ) {

        let spb : L2ItemInstance = player.getInventory().getItemByItemId( reqItemId )
        if ( !spb && ConfigManager.character.enchantSkillSpBookNeeded() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_ITENS_NEEDED_TO_CHANGE_SKILL_ENCHANT_ROUTE ) )
            return
        }

        if ( player.getInventory().getAdenaAmount() < totalAmount ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_OF_THE_ITEMS_NEEDED_TO_ENCHANT_THAT_SKILL ) )
            return
        }

        let check = player.removeSp( requiredSp )
        if ( ConfigManager.character.enchantSkillSpBookNeeded() ) {
            check = check && await player.destroyItemByObjectId( spb.getObjectId(), 1, true, 'RequestExEnchantSkillRouteChange' )
        }

        check = check && await player.destroyItemByItemId( ItemTypes.Adena, totalAmount, true, 'RequestExEnchantSkillRouteChange' )
        if ( !check ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ALL_OF_THE_ITEMS_NEEDED_TO_ENCHANT_THAT_SKILL ) )
            return
        }

        let levelPenalty = _.random( Math.min( 4, currentEnchantLevel ) )
        skillLevel -= levelPenalty
        if ( ( skillLevel % 100 ) === 0 ) {
            skillLevel = skillLearn.getBaseLevel()
        }

        skill = SkillCache.getSkill( skillId, skillLevel )

        if ( skill ) {
            await player.addSkill( skill, true )
            player.sendOwnedData( ExEnchantSkillResult( true ) )
        }

        player.sendDebouncedPacket( UserInfo )
        player.sendDebouncedPacket( ExBrExtraUserInfo )

        if ( levelPenalty === 0 ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.SKILL_ENCHANT_CHANGE_SUCCESSFUL_S1_LEVEL_WILL_REMAIN )
                    .addSkillNameById( skillId )
                    .getBuffer()
            player.sendOwnedData( packet )
        } else {
            let packet = new SystemMessageBuilder( SystemMessageIds.SKILL_ENCHANT_CHANGE_SUCCESSFUL_S1_LEVEL_WAS_DECREASED_BY_S2 )
                    .addSkillNameById( skillId )
                    .addNumber( levelPenalty )
                    .getBuffer()
            player.sendOwnedData( packet )
        }

        player.sendSkillList()

        let afterEnchantSkillLevel = player.getSkillLevel( skillId )
        player.sendOwnedData( ExEnchantSkillInfo( skillId, afterEnchantSkillLevel ) )
        player.sendOwnedData( ExEnchantSkillInfoDetail( EnchantType.ChangeRoute, skillId, afterEnchantSkillLevel, player ) )
        return PlayerShortcutCache.updateSkillShortcut( player, skillId, afterEnchantSkillLevel )
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DONT_HAVE_ENOUGH_SP_TO_ENCHANT_THAT_SKILL ) )
}