import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { ExShowContactList } from '../send/ExShowContactList'

export function RequestExShowContactList( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player || !ConfigManager.general.allowMail() ) {
        return
    }

    player.sendOwnedData( ExShowContactList( player.getObjectId() ) )
}