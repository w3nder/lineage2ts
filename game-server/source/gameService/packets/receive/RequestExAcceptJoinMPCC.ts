import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2CommandChannel } from '../../models/L2CommandChannel'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'

export function RequestExAcceptJoinMPCC( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player || !player.isInParty() ) {
        return
    }

    let requester : L2PcInstance = player.getActiveRequester()
    if ( !requester ) {
        return
    }

    let response : number = new ReadableClientPacket( packetData ).readD()

    if ( response !== 1 ) {
        requester.sendMessage( 'The player declined to join your Command Channel.' )
        return
    }

    let isNewCommandChannel = false
    if ( !requester.getParty().isInCommandChannel() ) {
        new L2CommandChannel( requester )

        requester.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.COMMAND_CHANNEL_FORMED ) )
        isNewCommandChannel = true
    }

    requester.getParty().getCommandChannel().addParty( player.getParty() )

    if ( !isNewCommandChannel ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.JOINED_COMMAND_CHANNEL ) )
    }

    player.setActiveRequester( null )
    requester.onTransactionResponse()
}