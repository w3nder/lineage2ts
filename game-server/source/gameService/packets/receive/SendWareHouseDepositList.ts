import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PacketVariables } from '../PacketVariables'
import { ActionFailed } from '../send/ActionFailed'
import { ItemContainer } from '../../models/itemcontainer/ItemContainer'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2World } from '../../L2World'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { ItemTypes } from '../../values/InventoryValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import aigle from 'aigle'
import { FastRateLimit } from 'fast-ratelimit'
import { PlayerPermission } from '../../enums/PlayerPermission'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 2, // time-to-live value of token bucket (in seconds)
} )

export async function SendWareHouseDepositList( client: GameClient, packetData: Buffer ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendMessage( 'You are depositing items too fast.' )
    }

    if ( player.getActiveEnchantItemId() !== L2PcInstanceValues.EmptyId ) {
        // TODO : Util.handleIllegalPlayerAction(player, "Player " + player.getName() + " tried to use enchant Exploit!");
        return
    }

    if ( !ConfigManager.character.karmaPlayerCanUseWareHouse() && ( player.getKarma() > 0 ) ) {
        return
    }

    let packet = new ReadableClientPacket( packetData )
    let totalItems: number = packet.readD()

    if ( totalItems < 1 || totalItems > PacketVariables.maximumItemsLimit ) {
        player.sendOwnedData( ActionFailed() )
        return
    }

    let items: Array<DepositItem> = []

    while ( totalItems > 0 ) {
        let objectId = packet.readD(), count = packet.readQ()

        if ( count < 1 || objectId < 1 ) {
            return player.sendOwnedData( ActionFailed() )
        }

        items.push( {
            count,
            objectId,
        } )

        totalItems--
    }

    let warehouse: ItemContainer = player.getActiveWarehouse()
    if ( !warehouse ) {
        return
    }

    let manager: L2Npc = L2World.getObjectById( player.getLastFolkNPC() ) as L2Npc
    if ( ( !manager || !manager.isWarehouse() || !player.canInteract( manager ) ) && !player.isGM() ) {
        return
    }

    if ( !warehouse.isPrivate() && !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.sendMessage( 'Transactions are disabled for your Access Level.' )
        return
    }

    let fee = items.length * Math.abs( ConfigManager.tuning.getWarehouseItemDepositFee() )
    let currentAdena = player.getAdena()
    let slots = 0
    let shouldExit = items.some( ( depositItem: DepositItem ) => {
        let item: L2ItemInstance = player.checkItemManipulation( depositItem.objectId, depositItem.count )

        if ( !item ) {
            return true
        }

        if ( item.getId() === ItemTypes.Adena ) {
            currentAdena -= depositItem.count
        }

        if ( !item.isStackable() ) {
            slots += depositItem.count
            return false
        }

        if ( !warehouse.getItemByItemId( item.getId() ) ) {
            slots++
        }

        return false
    } )

    if ( shouldExit ) {
        return
    }

    if ( !warehouse.validateCapacity( slots ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_EXCEEDED_QUANTITY_THAT_CAN_BE_INPUTTED ) )
        return
    }

    if ( ( currentAdena < fee ) || !( await player.reduceAdena( fee, false, 'SendWareHouseDepositList' ) ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ADENA ) )
        return
    }

    if ( player.getActiveTradeList() ) {
        return
    }

    await aigle.resolve( items ).each( async ( depositItem: DepositItem ) => {
        let oldItem: L2ItemInstance = player.checkItemManipulation( depositItem.objectId, depositItem.count )

        if ( !oldItem.isDepositable( warehouse.isPrivate() ) || !oldItem.isAvailable( player, true, warehouse.isPrivate() ) ) {
            return
        }

        let newItem: L2ItemInstance = await player.getInventory().transferItem( depositItem.objectId, depositItem.count, warehouse )
        if ( !newItem ) {
            return
        }
    } )
}

interface DepositItem {
    objectId: number
    count: number
}