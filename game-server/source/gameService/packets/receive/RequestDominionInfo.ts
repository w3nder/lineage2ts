import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ExReplyDominionInfo } from '../send/ExReplyDominionInfo'
import { ExShowOwnedThingsPosition } from '../send/ExShowOwnedThingsPosition'

export function RequestDominionInfo( client: GameClient ) {
    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    player.sendOwnedData( ExReplyDominionInfo() )
    player.sendOwnedData( ExShowOwnedThingsPosition() )
}