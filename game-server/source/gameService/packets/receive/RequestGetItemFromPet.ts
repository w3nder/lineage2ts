import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2PcInstanceValues } from '../../values/L2PcInstanceValues'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'

const packetLimiter = new FastRateLimit( {
    threshold: 2, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function RequestGetItemFromPet( client: GameClient, packetData: Buffer ) : Promise<any> {
    let player: L2PcInstance = client.player
    if ( !player || !player.hasPet() ) {
        return
    }

    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return player.sendMessage( 'You get items from pet too fast.' )
    }

    let packet = new ReadableClientPacket( packetData )
    let objectId = packet.readD(), amount = packet.readQ()

    if ( amount <= 0 ) {
        return
    }

    let pet : L2PetInstance = player.getSummon() as L2PetInstance
    if ( player.getActiveEnchantItemId() !== L2PcInstanceValues.EmptyId ) {
        return
    }

    let item : L2ItemInstance = pet.getInventory().getItemByObjectId( objectId )
    if ( !item ) {
        return
    }

    if ( amount > item.getCount() ) {
        return
    }

    return pet.getInventory().transferItem( objectId, amount, player.getInventory(),player.getObjectId(), 'RequestGetItemFromPet' )
}