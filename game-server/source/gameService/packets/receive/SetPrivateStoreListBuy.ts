import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { ActionFailed } from '../send/ActionFailed'
import { PrivateStoreManageListBuy } from '../send/PrivateStoreManageListBuy'
import { TradeList } from '../../models/TradeList'
import { PrivateStoreMessageBuy } from '../send/PrivateStoreMessageBuy'
import { PacketVariables } from '../PacketVariables'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { recordBuySellViolation } from '../../helpers/PlayerViolations'
import { getMaxAdena } from '../../helpers/ConfigurationHelper'
import { AreaFlags } from '../../models/areas/AreaFlags'
import { PlayerPermission } from '../../enums/PlayerPermission'

const packetLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: 3, // time-to-live value of token bucket (in seconds)
} )

export async function SetPrivateStoreListBuy( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    // TODO : consider additional checks: isMoving, isTeleporting, isDead, isStunned (or other immobile effects)

    if ( !player.getAccessLevel().hasPermission( PlayerPermission.AllowTransaction ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_ARE_NOT_AUTHORIZED_TO_DO_THAT ) )
        return
    }

    if ( player.isUnderAttack() || player.isInDuel() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANT_OPERATE_PRIVATE_STORE_DURING_COMBAT ) )
        player.sendOwnedData( PrivateStoreManageListBuy( player ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    if ( player.hasAreaFlags( AreaFlags.NoOpenStore ) ) {
        player.sendOwnedData( PrivateStoreManageListBuy( player ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_PRIVATE_STORE_HERE ) )
        player.sendOwnedData( ActionFailed() )
        return
    }

    let items : Array<BuyItem> = []
    let packet = new ReadableClientPacket( packetData )
    let amount : number = packet.readD()

    if ( amount < 1 || amount > PacketVariables.maximumItemsLimit ) {
        return
    }

    while ( amount > 0 ) {
        let itemId = packet.readD()
        packet.skipD( 1 )

        let count = packet.readQ(), price = packet.readQ()
        packet.skipB( 16 )

        if ( itemId < 1 || count < 1 || price < 1 ) {
            return
        }

        let data : BuyItem = {
            itemId,
            count,
            price
        }

        items.push( data )
        amount--
    }

    if ( items.length === 0 ) {
        player.setPrivateStoreType( PrivateStoreType.None )
        player.broadcastUserInfo()
        return
    }

    let tradeList : TradeList = player.getBuyList()
    tradeList.clear()

    if ( items.length > player.getPrivateBuyStoreLimit() ) {
        player.sendOwnedData( PrivateStoreManageListBuy( player ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_EXCEEDED_QUANTITY_THAT_CAN_BE_INPUTTED ) )
        return
    }

    let totalCost = 0
    let maxAdena = getMaxAdena()

    let shouldExit = items.some( ( item : BuyItem ) : boolean => {
        if ( !tradeList.addItemByItemId( item.itemId, item.count, item.price ) ) {
            recordBuySellViolation( player.getObjectId(), 'f581132e-e30b-4a33-8b8b-11ae1a8f9090', 'Player buy store provides invalid item data', item.itemId, item.count, item.price )
            return true
        }

        let itemCost = item.price * item.count
        if ( ( maxAdena - totalCost ) < itemCost ) {
            return false
        }

        totalCost += itemCost

        if ( totalCost > player.getAdena() ) {
            player.sendOwnedData( PrivateStoreManageListBuy( player ) )
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_PURCHASE_PRICE_IS_HIGHER_THAN_MONEY ) )
            return true
        }

        return true
    } )

    if ( shouldExit ) {
        return
    }

    await player.sitDown()
    player.setPrivateStoreType( PrivateStoreType.Buy )
    player.broadcastUserInfo()

    return BroadcastHelper.dataToSelfBasedOnVisibility( player, PrivateStoreMessageBuy( player ) )
}

interface BuyItem {
    itemId: number
    count: number
    price: number
}