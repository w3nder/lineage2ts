import { GameClient } from '../../GameClient'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { BuffInfo } from '../../models/skills/BuffInfo'
import { AbnormalType } from '../../models/skills/AbnormalType'
import { AbstractEffect } from '../../models/effects/AbstractEffect'
import { ActionFailed } from '../send/ActionFailed'
import { SystemMessageBuilder } from '../send/SystemMessage'
import { SystemMessageIds } from '../SystemMessageIdValues'
import { L2Object } from '../../models/L2Object'
import { L2World } from '../../L2World'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { PrivateStoreType } from '../../enums/PrivateStoreType'
import { ReadableClientPacket } from '../../../packets/ReadableClientPacket'
import { FastRateLimit } from 'fast-ratelimit'
import { BlockedAction } from '../../enums/BlockedAction'

const packetLimiter = new FastRateLimit( {
    threshold: 3, // available tokens over timespan
    ttl: 1, // time-to-live value of token bucket (in seconds)
} )

export async function AttackRequest( client: GameClient, packetData: Buffer ) {
    if ( !packetLimiter.consumeSync( client.accountName ) ) {
        return
    }

    let player: L2PcInstance = client.player
    if ( !player ) {
        return
    }

    let info : BuffInfo = player.getEffectList().getBuffInfoByAbnormalType( AbnormalType.BOT_PENALTY )
    if ( info ) {
        let shouldExit = info.getEffects().some( ( effect: AbstractEffect ) => {
            return effect.blocksAction( BlockedAction.Attack )
        } )

        if ( shouldExit ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_HAVE_BEEN_REPORTED_SO_ACTIONS_NOT_ALLOWED ) )
            return player.sendOwnedData( ActionFailed() )
        }
    }

    let objectId : number = new ReadableClientPacket( packetData ).readD()

    // avoid using expensive operations if not needed
    let target : L2Object
    if ( player.getTargetId() === objectId ) {
        target = player.getTarget()
    } else {
        target = L2World.getObjectById( objectId )
    }

    if ( !target ) {
        return player.sendOwnedData( ActionFailed() )
    }

    if ( !target.isTargetable() && !player.hasActionOverride( PlayerActionOverride.TargetBlock ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    // Players can't attack objects in the other instances
    // except from multiverse
    if ( ( target.getInstanceId() !== player.getInstanceId() ) && ( player.getInstanceId() !== -1 ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    // Only GMs can directly attack invisible characters
    if ( !target.isVisibleFor( player ) ) {
        return player.sendOwnedData( ActionFailed() )
    }

    if ( player.getTarget().getObjectId() !== target.getObjectId() ) {
        return target.onInteraction( player, true )
    }

    if ( ( target.getObjectId() !== player.getObjectId() )
            && ( player.getPrivateStoreType() === PrivateStoreType.None )
            && !player.getActiveRequester() ) {
        return target.onForcedAttack( player )
    }

    player.sendOwnedData( ActionFailed() )
}