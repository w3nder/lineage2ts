export const enum CastleUpdateType {
    Treasury = 1,
    CrestStatus,
    Tax = 4,
    TicketCount = 8
}