export const enum ResidenceType {
    Unknown,
    Fort,
    Castle
}