import { NpcTemplateType } from './NpcTemplateType'

export const enum GeometryId {
    None = 'none',
    MobSmall = 'mob_small',
    MobMedium = 'mob_medium',
    MobLarge = 'mob_large',
    RaidbossTiny = 'raidboss_tiny',
    RaidbossSmall = 'raidboss_small',
    RaidbossMedium = 'raidboss_medium',
    RaidbossLarge = 'raidboss_large',
    RaidbossExtraLarge = 'raidboss_xlarge',
    GrandbossSmall = 'grandboss_small',
    GrandbossMedium = 'grandboss_medium',
    GrandbossLarge = 'grandboss_large',
    PlayerDrop = 'playerDrop',
    RaidbossSpawnSmall = 'rbSpawnSmall',
    RaidbossSpawnMedium = 'rbSpawnMedium',

    NpcSpawnSmall = 'npcSpawnSmall',
    NpcSpawnMedium = 'npcSpawnMedium',
    NpcSpawnLarge = 'npcSpawnLarge',
    NpcSpawnXLarge = 'npcSpawnXLarge',

    NpcMoveTiny = 'npcMoveTiny',
    NpcMoveSmall = 'npcMoveSmall',
    NpcMoveMedium = 'npcMoveMedium',
    NpcMoveLarge = 'npcMoveLarge',
    NpcMoveXLarge = 'npcMoveXLarge',
    NpcMoveXXLarge = 'npcMoveXXLarge',
    NpcMoveHuge = 'npcMoveHuge',
    NpcMoveXHuge = 'npcMoveXHuge',

    PartyTeleport = 'partyTeleport',
    ZonePartyTeleport = 'zonePartyTeleport',
    SpellTeleport = 'spellTeleport',
    ValakasTeleport = 'valakasTeleport',

    Geo256 = 'geodata256',
    GeoGrid256 = 'geoGrid256',

    PetMoveSmall = 'petMoveSmall'
}

export function getGeometryByType( type: NpcTemplateType, radius: number ) : GeometryId {
    switch ( type ) {
        case NpcTemplateType.L2RaidBoss:
            if ( radius < 20 ) {
                return GeometryId.RaidbossTiny
            }

            if ( radius < 30 ) {
                return GeometryId.RaidbossSmall
            }

            if ( radius < 40 ) {
                return GeometryId.RaidbossMedium
            }

            if ( radius < 50 ) {
                return GeometryId.RaidbossLarge
            }

            return GeometryId.RaidbossExtraLarge

        case NpcTemplateType.L2GrandBoss:
            if ( radius < 50 ) {
                return GeometryId.GrandbossSmall
            }

            if ( radius < 60 ) {
                return GeometryId.GrandbossMedium
            }

            return GeometryId.GrandbossLarge

        default:
            if ( radius < 20 ) {
                return GeometryId.MobSmall
            }

            if ( radius < 30 ) {
                return GeometryId.MobMedium
            }

            return GeometryId.MobLarge
    }
}

export function getMoveGeometry( radius: number ) : GeometryId {
    if ( radius === 0 ) {
        return GeometryId.None
    }

    if ( radius <= 60 ) {
        return GeometryId.NpcMoveTiny
    }

    if ( radius <= 80 ) {
        return GeometryId.NpcMoveSmall
    }

    if ( radius <= 100 ) {
        return GeometryId.NpcMoveMedium
    }

    if ( radius <= 120 ) {
        return GeometryId.NpcMoveLarge
    }

    if ( radius <= 140 ) {
        return GeometryId.NpcMoveXLarge
    }

    if ( radius <= 160 ) {
        return GeometryId.NpcMoveXXLarge
    }

    if ( radius <= 180 ) {
        return GeometryId.NpcMoveHuge
    }

    return GeometryId.NpcMoveXHuge
}

export function getSpawnGeometry( radius: number ) : GeometryId {
    if ( radius === 0 ) {
        return GeometryId.None
    }

    if ( radius <= 40 ) {
        return GeometryId.NpcSpawnSmall
    }

    if ( radius <= 60 ) {
        return GeometryId.NpcSpawnMedium
    }

    if ( radius <= 80 ) {
        return GeometryId.NpcSpawnLarge
    }

    return GeometryId.NpcSpawnXLarge
}