export const enum ResidenceTeleportType {
    Normal = 'normal',
    Banishment = 'banish',
    PVP = 'chaotic',
    Other = 'other'
}