export const enum CastleFunctions {
    None = 0,
    Teleport = 1,
    RegenerateHp = 2,
    RegenerateMp = 3,
    RestoreExpOnDeath = 4,
    SupportBuffs = 5,
}