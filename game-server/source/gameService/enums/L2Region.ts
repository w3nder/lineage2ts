import { L2MapTile } from './L2MapTile'

export const enum L2Region {
    Length = 2048,
    RegionsPerTile = L2MapTile.Size / 2048,
    SearchOffset = 100,
    LargeObjectUpdateRadius = 2048 * 2
}