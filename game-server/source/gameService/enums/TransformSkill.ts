import { Skill } from '../models/Skill'

export interface TransformSkill {
    skill: Skill
    minimumLevel: number
}