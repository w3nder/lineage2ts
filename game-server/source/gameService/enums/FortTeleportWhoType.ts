export enum FortTeleportWhoType {
    All,
    Attacker,
    Owner,
}