import { Skill } from '../models/Skill'

export enum AISkillUsageType {
    Buff,
    Attack,
    Special
}

export const enum AISkillUsageBehavior {
    None,
    Physical,
    Magical,
    Both
}

/*
    Skill focus designates what kind of AI traits npc has to use
    particular set of skills. Such focus helps out certain behavior
    when npc must only use buffs or attack skills, or even special skills,
    in terms of performance and code complexity (easier to reason).
 */
export const enum AISkillFocus {
    None,
    AllSkills,
    Buff,
    Attack,
    Special
}

export type AISkillMap = Record<AISkillUsageType, Array<Skill>>
export type AIUsageProbabilities = Record<AISkillUsageType, number>

/*
    Skill usage represents available skills that npc can choose to execute.
    Skill map represents collection of skills per usage type.
    Index map represents rolling indexes of choosing next skill.

    Keep in mind that skill map must have randomized skills on spawn, since easy
    approach to skill selection can be simply iteration and seeing if skill can be applicable via distance.

    AI behavior is used to select particular AIController trait that would favor distanced mage behavior vs up close attack.
    When behavior is both, iterative approach should be used.
 */
export interface AISkillUsage {
    available: AISkillMap
    probability: AIUsageProbabilities
    ai: AISkillUsageBehavior
    focus: AISkillFocus
}