export const enum MsInDays {
    One = 86400000,
    Two = 86400000 * 2,
    Three = 86400000 * 3,
    Four = 86400000 * 4,
    Five = 86400000 * 5,
    Six = 86400000 * 6,
    Seven = 86400000 * 7,
    Fifteen = 15 * 86400000
}

export const enum MsInMinutes {
    Fifteen = 15 * 60 * 1000,
    Thirty = 30 * 60 * 1000
}

export const enum MsInHours {
    One = 3600000,
    Twelve = 12 * 3600000
}