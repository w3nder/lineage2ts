export const enum BlockedAction {
    Attack = -1,
    Trade = -2,
    Party = -3,
    Interaction = -4,
    Chat = -5,
}