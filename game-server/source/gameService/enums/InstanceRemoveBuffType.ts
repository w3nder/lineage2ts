export enum InstanceRemoveBuffType {
    NONE,
    ALL,
    WHITELIST,
    BLACKLIST,
}