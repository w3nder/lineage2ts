export const enum PlayerLoadState {
    None,
    Loaded,
    EnteredWorld,
    Unloaded,
    Unloading
}