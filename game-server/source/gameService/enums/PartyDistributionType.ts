export const enum PartyDistributionType {
    FindersKeepers,
    Random,
    RandomIncludingSpoil,
    ByTurn,
    ByTurnIncludingSpoil
}

export const PartyDistributionTypeValues = {
    [ PartyDistributionType.FindersKeepers ]: 487,
    [ PartyDistributionType.Random ]: 488,
    [ PartyDistributionType.RandomIncludingSpoil ]: 798,
    [ PartyDistributionType.ByTurn ]: 799,
    [ PartyDistributionType.ByTurnIncludingSpoil ]: 800
}

export interface PartyDistributionTypeValue {
    id: number,
    systemStringId: number
}

export function getPartyDistributionTypeById( type: PartyDistributionType ) : PartyDistributionTypeValue {
    let systemStringId : number = PartyDistributionTypeValues[ type ]
    if ( !systemStringId ) {
        return null
    }

    return {
        id: type,
        systemStringId
    }
}