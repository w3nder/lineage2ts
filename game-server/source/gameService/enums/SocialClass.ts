export enum SocialClass {
    VAGABOND,
    VASSAL,
    APPRENTICE,
    HEIR,
    KNIGHT,
    ELDER,
    BARON,
    VISCOUNT,
    COUNT,
    MARQUIS,
    DUKE,
    GRAND_DUKE,
    DISTINGUISHED_KING,
    EMPEROR
}