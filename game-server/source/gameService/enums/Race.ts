import _ from 'lodash'

export enum Race {
    HUMAN,
    ELF,
    DARK_ELF,
    ORC,
    DWARF,
    KAMAEL,
    ANIMAL,
    BEAST,
    BUG,
    CASTLE_GUARD,
    CONSTRUCT,
    DEMONIC,
    DIVINE,
    DRAGON,
    ELEMENTAL,
    ETC,
    FAIRY,
    GIANT,
    HUMANOID,
    MERCENARY,
    NONE,
    PLANT,
    SIEGE_WEAPON,
    UNDEAD
}

export const RaceMapping : Record<string, Partial<Race>> = {
    human: Race.HUMAN,
    darkelf: Race.DARK_ELF,
    dwarf: Race.DWARF,
    elf: Race.ELF,
    orc: Race.ORC,
    kamael: Race.KAMAEL
}

export const ReverseRaceMapping = _.invert( RaceMapping )