import _ from 'lodash'

export enum InstanceType {
    L2Object,
    L2ItemInstance,
    L2Character,
    L2Npc,
    L2Playable,
    L2Summon,
    L2Decoy,
    L2PcInstance,
    L2NpcInstance,
    L2MerchantInstance,
    L2WarehouseInstance,
    L2StaticObjectInstance,
    L2DoorInstance,
    L2TerrainObjectInstance,
    L2EffectPointInstance,
    // Summons, Pets, Decoys and Traps
    L2ServitorInstance,
    L2PetInstance,
    L2BabyPetInstance,
    L2DecoyInstance,
    L2TrapInstance,
    // Attackable
    L2Attackable,
    L2GuardInstance,
    L2QuestGuardInstance,
    L2MonsterInstance,
    L2ChestInstance,
    L2ControllableMobInstance,
    L2FeedableBeastInstance,
    L2TamedBeastInstance,
    L2FriendlyMobInstance,
    L2RiftInvaderInstance,
    L2RaidBossInstance,
    L2GrandBossInstance,
    // FlyMobs
    L2FlyNpcInstance,
    L2FlyMonsterInstance,
    L2FlyRaidBossInstance,
    L2FlyTerrainObjectInstance,
    // Sepulchers
    L2SepulcherNpcInstance,
    L2SepulcherMonsterInstance,
    L2FestivalGuideInstance,
    L2FestivalMonsterInstance,
    // Vehicles
    L2Vehicle,
    L2BoatInstance,
    L2AirShipInstance,
    L2ControllableAirShipInstance,
    // Siege
    L2DefenderInstance,
    L2ArtefactInstance,
    L2ControlTowerInstance,
    L2FlameTowerInstance,
    L2SiegeFlagInstance,
    // Fort Siege
    L2FortCommanderInstance,
    // Fort NPCs
    L2FortLogisticsInstance,
    L2FortManagerInstance,
    // Seven Signs
    L2SignsPriestInstance,
    L2DawnPriestInstance,
    L2DuskPriestInstance,
    L2DungeonGatekeeperInstance,
    // City NPCs
    L2AdventurerInstance,
    L2AuctioneerInstance,
    L2ClanHallManagerInstance,
    L2FishermanInstance,
    L2ObservationInstance,
    L2OlympiadManagerInstance,
    L2PetManagerInstance,
    L2RaceManagerInstance,
    L2TeleporterInstance,
    L2TrainerInstance,
    L2VillageMasterInstance,
    // Doormens
    L2DoormenInstance,
    L2CastleDoormenInstance,
    L2FortDoormenInstance,
    L2ClanHallDoormenInstance,
    // Custom
    L2NpcBufferInstance,
    L2EventMobInstance,
    L2BlockInstance,
    L2EventMonsterInstance,
    L2VillageMasterKamaelInstance,
    L2VillageMasterOrcInstance,
    L2VillageMasterPriestInstance,
    L2VillageMasterFighterInstance,
    L2VillageMasterDwarfInstance,
    L2VillageMasterDElfInstance,
    L2VillageMasterMysticInstance,
    L2Tower,
    L2TerritoryWardInstance
}

export const InstanceTypeMap = {
    [ InstanceType.L2Object ]: null,
    [ InstanceType.L2ItemInstance ]: InstanceType.L2Object,
    [ InstanceType.L2Character ]: InstanceType.L2Object,
    [ InstanceType.L2Npc ]: InstanceType.L2Character,
    [ InstanceType.L2Playable ]: InstanceType.L2Character,
    [ InstanceType.L2Summon ]: InstanceType.L2Playable,
    [ InstanceType.L2Decoy ]: InstanceType.L2Character,
    [ InstanceType.L2PcInstance ]: InstanceType.L2Playable,
    [ InstanceType.L2NpcInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2MerchantInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2WarehouseInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2StaticObjectInstance ]: InstanceType.L2Character,
    [ InstanceType.L2DoorInstance ]: InstanceType.L2Character,
    [ InstanceType.L2TerrainObjectInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2EffectPointInstance ]: InstanceType.L2Npc,
// Summons, Pets, Decoys and Traps
    [ InstanceType.L2ServitorInstance ]: InstanceType.L2Summon,
    [ InstanceType.L2PetInstance ]: InstanceType.L2Summon,
    [ InstanceType.L2BabyPetInstance ]: InstanceType.L2PetInstance,
    [ InstanceType.L2DecoyInstance ]: InstanceType.L2Decoy,
    [ InstanceType.L2TrapInstance ]: InstanceType.L2Npc,
// Attackable
    [ InstanceType.L2Attackable ]: InstanceType.L2Npc,
    [ InstanceType.L2GuardInstance ]: InstanceType.L2Attackable,
    [ InstanceType.L2QuestGuardInstance ]: InstanceType.L2GuardInstance,
    [ InstanceType.L2MonsterInstance ]: InstanceType.L2Attackable,
    [ InstanceType.L2ChestInstance ]: InstanceType.L2MonsterInstance,
    [ InstanceType.L2ControllableMobInstance ]: InstanceType.L2MonsterInstance,
    [ InstanceType.L2FeedableBeastInstance ]: InstanceType.L2MonsterInstance,
    [ InstanceType.L2TamedBeastInstance ]: InstanceType.L2FeedableBeastInstance,
    [ InstanceType.L2FriendlyMobInstance ]: InstanceType.L2Attackable,
    [ InstanceType.L2RiftInvaderInstance ]: InstanceType.L2MonsterInstance,
    [ InstanceType.L2RaidBossInstance ]: InstanceType.L2MonsterInstance,
    [ InstanceType.L2GrandBossInstance ]: InstanceType.L2RaidBossInstance,
// FlyMobs
    [ InstanceType.L2FlyNpcInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2FlyMonsterInstance ]: InstanceType.L2MonsterInstance,
    [ InstanceType.L2FlyRaidBossInstance ]: InstanceType.L2RaidBossInstance,
    [ InstanceType.L2FlyTerrainObjectInstance ]: InstanceType.L2Npc,
// Sepulchers
    [ InstanceType.L2SepulcherNpcInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2SepulcherMonsterInstance ]: InstanceType.L2MonsterInstance,
// Festival
    [ InstanceType.L2FestivalGuideInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2FestivalMonsterInstance ]: InstanceType.L2MonsterInstance,
// Vehicles
    [ InstanceType.L2Vehicle ]: InstanceType.L2Character,
    [ InstanceType.L2BoatInstance ]: InstanceType.L2Vehicle,
    [ InstanceType.L2AirShipInstance ]: InstanceType.L2Vehicle,
    [ InstanceType.L2ControllableAirShipInstance ]: InstanceType.L2AirShipInstance,
// Siege
    [ InstanceType.L2DefenderInstance ]: InstanceType.L2Attackable,
    [ InstanceType.L2ArtefactInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2ControlTowerInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2FlameTowerInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2SiegeFlagInstance ]: InstanceType.L2Npc,
// Fort Siege
    [ InstanceType.L2FortCommanderInstance ]: InstanceType.L2DefenderInstance,
// Fort NPCs
    [ InstanceType.L2FortLogisticsInstance ]: InstanceType.L2MerchantInstance,
    [ InstanceType.L2FortManagerInstance ]: InstanceType.L2MerchantInstance,
// Seven Signs
    [ InstanceType.L2SignsPriestInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2DawnPriestInstance ]: InstanceType.L2SignsPriestInstance,
    [ InstanceType.L2DuskPriestInstance ]: InstanceType.L2SignsPriestInstance,
    [ InstanceType.L2DungeonGatekeeperInstance ]: InstanceType.L2Npc,
// City NPCs
    [ InstanceType.L2AdventurerInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2AuctioneerInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2ClanHallManagerInstance ]: InstanceType.L2MerchantInstance,
    [ InstanceType.L2FishermanInstance ]: InstanceType.L2MerchantInstance,
    [ InstanceType.L2ObservationInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2OlympiadManagerInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2PetManagerInstance ]: InstanceType.L2MerchantInstance,
    [ InstanceType.L2RaceManagerInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2TeleporterInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2TrainerInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2VillageMasterInstance ]: InstanceType.L2NpcInstance,
// Doormens
    [ InstanceType.L2DoormenInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2CastleDoormenInstance ]: InstanceType.L2DoormenInstance,
    [ InstanceType.L2FortDoormenInstance ]: InstanceType.L2DoormenInstance,
    [ InstanceType.L2ClanHallDoormenInstance ]: InstanceType.L2DoormenInstance,
// Custom
    [ InstanceType.L2NpcBufferInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2EventMobInstance ]: InstanceType.L2Npc,
    [ InstanceType.L2VillageMasterDwarfInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2VillageMasterDElfInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2VillageMasterFighterInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2VillageMasterKamaelInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2VillageMasterMysticInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2VillageMasterOrcInstance ]: InstanceType.L2NpcInstance,
    [ InstanceType.L2VillageMasterPriestInstance ]: InstanceType.L2NpcInstance,
}

/*
    As of January 2021, object property look-ups are fastest compared to Set or even Map objects, or arrays
 */
function getTreeTypes( type : InstanceType ) : { [ key: number ] : boolean } {
    let currentType = type
    let types : { [ key: number ] : boolean } = {}

    while ( _.isNumber( currentType ) ) {
        types[ currentType ] = true
        currentType = InstanceTypeMap[ currentType ]
    }

    return types
}

const dependencyTreeMap : { [ key: number] : { [ key: number ] : boolean } } = _.reduce( InstanceTypeMap, ( finalMap: { [ key: number] : { [ key: number ] : boolean } }, value: InstanceType, key: string ) => {
    let parentType = _.parseInt( key )
    finalMap[ parentType ] = getTreeTypes( parentType )

    return finalMap
}, {} )

export const InstanceTypeHelper = {
    includesTypes( originalType: InstanceType, types: Array<InstanceType> ) : boolean {
        return types.some( ( currentType: InstanceType ) => {
            return dependencyTreeMap[ originalType ][ currentType ]
        } )
    },

    includesType( originalType: InstanceType, type: InstanceType ) : boolean {
        return dependencyTreeMap[ originalType ] && dependencyTreeMap[ originalType ][ type ]
    }
}

