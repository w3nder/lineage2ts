export const enum TeleportRestriction {
    None,
    Busy,
    OnlyOwner,
    AccessDisabled
}