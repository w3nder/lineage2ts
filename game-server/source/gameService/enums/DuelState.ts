export const enum DuelState {
    None,
    Action,
    Dead,
    Winner,
    Interrupted
}