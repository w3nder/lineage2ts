export enum ListenerRegisterType {
    NpcId,
    AreaId,
    ItemId,
    Olympiad,
    General,
    WorldObject
}