export const enum ClanHallFunctionType {
    None,
    Teleport,
    CreateItem,
    RestoreHp,
    RestoreMp,
    RestoreXp,
    SupportBuffs,
    DecorationFront,
    DecorationCurtains,
    RestoreCP,
    CraftingMpDecrease,
    QuestXPBonus,
    QuestSPBonus
}