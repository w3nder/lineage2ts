import _ from 'lodash'

export const enum ShotType {
    Soulshot,
    Spiritshot,
    BlessedSpiritshot,
    FishingSoulshot
}

const ShotTypeMask : Array<number> = _.times( 10, ( value: number ) => {
    return 1 << value
} )


export const ShotTypeHelper = {
    getMask( type: ShotType ) : number {
        return ShotTypeMask[ type ]
    }
}