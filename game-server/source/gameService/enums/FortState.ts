export const enum FortState {
    NpcOwned,
    Abandoned,
    ClanClaimed
}