export const enum WorldFlyingLimits {
    /** Gracia border Flying objects not allowed to the east of it. */
    GraciaMaxX = -166168,
    GraciaMaxZ = 6105,
    GraciaMinZ = -895,
}