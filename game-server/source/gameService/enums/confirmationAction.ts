import _ from 'lodash'

export const enum ConfirmationAction {
    None = 0,
    AdminCommand = 1
}

const PlayerActionMask: Array<number> = _.times( 5, ( value: number ) => {
    return 1 << value
} )

export const PlayerActionHelper = {
    getMask( action: ConfirmationAction ): number {
        return PlayerActionMask[ action ]
    },
}