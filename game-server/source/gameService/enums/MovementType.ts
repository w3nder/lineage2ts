export const enum MovementType {
    Walking = 0,
    Swimming = 1,
    Flying = 2
}