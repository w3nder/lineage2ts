/*
    Plase note not to change order of enums due to
    numeric values used in database.
 */
export const enum ItemLocation {
    VOID,
    INVENTORY,
    PAPERDOLL,
    WAREHOUSE,
    CLANWH,
    PET,
    PET_EQUIP,
    LEASE,
    REFUND,
    MAIL,
    FREIGHT
}