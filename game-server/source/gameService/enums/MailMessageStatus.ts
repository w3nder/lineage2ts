export const enum MailMessageStatus {
    Read = 0,
    Unread = 1,
}

export const enum MailMessageFlags {
    HasAttachments,
    IsReturned
}