export const enum VitalityPointsPerLevel {
    None = 1,
    One = 240,
    Two = 2000,
    Three = 13000,
    Four = 17000,
    Top = 20000
}

export const enum VitalityLevel {
    None,
    One,
    Two,
    Three,
    Four,
    Top
}

export const VitalityLevelPoints : Record<VitalityLevel, VitalityPointsPerLevel> = {
    [ VitalityLevel.None ]: VitalityPointsPerLevel.None,
    [ VitalityLevel.One ]: VitalityPointsPerLevel.One,
    [ VitalityLevel.Two ]: VitalityPointsPerLevel.Two,
    [ VitalityLevel.Three ]: VitalityPointsPerLevel.Three,
    [ VitalityLevel.Four ]: VitalityPointsPerLevel.Four,
    [ VitalityLevel.Top ]: VitalityPointsPerLevel.Top,
}