export const enum OlympiadSide {
    None = -1,
    First = 1,
    Second = 2
}