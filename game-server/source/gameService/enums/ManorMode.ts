export enum ManorMode {
    DISABLED,
    MODIFIABLE,
    MAINTENANCE,
    APPROVED
}