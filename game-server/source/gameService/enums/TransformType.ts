export enum TransformType {
    MODE_CHANGE,
    NON_COMBAT,
    CURSED,
    RIDING_MODE,
    PURE_STAT,
    FLYING,
    COMBAT
}