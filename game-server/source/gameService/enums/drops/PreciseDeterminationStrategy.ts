export const enum PreciseDeterminationStrategy {
    ALWAYS,
    DEFAULT,
    NEVER
}