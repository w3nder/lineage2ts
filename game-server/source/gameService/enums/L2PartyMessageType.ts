export enum L2PartyMessageType {
    Expelled,
    Left,
    None,
    Disconnected
}