export const enum InventoryUpdateStatus {
    Unchanged,
    Added,
    Modified,
    Removed
}