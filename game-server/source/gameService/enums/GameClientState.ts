export const enum GameClientState {
    None,
    Initial,
    Verified,
    SelectingCharacter,
    Playing
}