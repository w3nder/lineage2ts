export enum ForumType {
    ROOT,
    NORMAL,
    CLAN,
    MEMO,
    MAIL
}