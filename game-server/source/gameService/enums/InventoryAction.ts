export const enum InventoryAction {
    None,
    SkillProduced,
    Sweeper,
    QuestReward,
    ExtractableItem,
    PartyLoot,
    PlayerAutoloot,
    Fishing,
    Merchandise,
    PremiumItem,
    Other
}