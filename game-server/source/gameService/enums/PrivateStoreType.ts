export const enum PrivateStoreType {
    None = 0,
    Sell = 1,
    PrepareSell = 2,
    Buy = 3,
    PrepareBuy = 4,
    Manufacture = 5,
    PackageSell = 8
}

export const BuyStoreTypes = new Set<PrivateStoreType>( [
    PrivateStoreType.Buy,
    PrivateStoreType.PrepareBuy
] )

export const SellStoreTypes = new Set<PrivateStoreType>( [
    PrivateStoreType.Sell,
    PrivateStoreType.PrepareSell,
    PrivateStoreType.PackageSell
] )