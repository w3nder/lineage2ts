export const enum PlayerState {
    RESTING,
    MOVING,
    RUNNING,
    STANDING,
    FLYING,
    BEHIND,
    FRONT,
    CHAOTIC,
    OLYMPIAD,
    TRANSFORMED
}