export enum ForumVisibility {
    INVISIBLE,
    ALL,
    CLAN_MEMBER_ONLY,
    OWNER_ONLY
}