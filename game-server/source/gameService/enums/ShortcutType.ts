export enum ShortcutType {
    NONE,
    ITEM,
    SKILL,
    ACTION,
    MACRO,
    RECIPE,
    BOOKMARK,
}

export enum ShortcutOwnerType {
    Initial,
    Player,
    Pet
}