export const enum RegenerationFlags {
    None = 0,
    Hp = 1,
    Mp = 2,
    Cp = 4,
}