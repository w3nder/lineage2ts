export const enum RestartPointType {
    Default = 0,
    ClanHall = 1,
    Castle = 2,
    Fortress = 3,
    SiegeHQ = 4,
    Festival = 5,
    Agathion = 6,
}