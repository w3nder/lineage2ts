import { IMovingData, L2MovingWorker, MovingThreadVariables } from './threads/models/IMovingManager'
import { BroadcastChannel } from 'worker_threads'
import { L2Object } from './models/L2Object'
import { L2Character } from './models/actor/L2Character'
import { WorkerMessageEvent } from 'threads/dist/types/master'
import { L2DataApi } from '../data/interface/l2DataApi'
import { spawn, Worker } from 'threads'

const movingWorkerChannel = new BroadcastChannel( MovingThreadVariables.StopChannel )
export type MovementHook = ( character: L2Character ) => void

class Manager implements L2DataApi {
    movingWorker: L2MovingWorker = null
    movingObjects: { [ key: number ]: L2Object } = {}
    hooks: Record<number, Set<MovementHook>> = {}

    startMovement( character: L2Character ): void {
        if ( this.hooks[ character.getMovementId() ] ) {
            this.hooks[ character.getMovementId() ].forEach( ( hook: MovementHook ) => hook( character ) )
        }

        this.movingWorker.startMovement( character.getMovementId() )
    }

    registerMovingCharacter( object: L2Character ): void {
        this.movingObjects[ object.getMovementId() ] = object
        this.movingWorker.registerObject( object.sharedData.buffer )
    }

    unRegisterMovingCharacter( object: L2Character ): void {
        this.movingWorker.unRegisterObject( object.getMovementId() )
        delete this.movingObjects[ object.getMovementId() ]
    }

    stopMoving( character: L2Character ): void {
        return this.movingWorker.cancelMovement( character.getMovementId() )
    }

    processMovingWorkerNotification( { data } : WorkerMessageEvent<IMovingData> ): void {
        let object: L2Object = this.movingObjects[ data ]
        if ( object ) {
            ( object as L2Character ).onStopMoving( true )
        }
    }

    async load(): Promise<Array<string>> {
        movingWorkerChannel.onmessage = this.processMovingWorkerNotification.bind( this )
        this.movingWorker = await spawn( new Worker( './threads/MovingManager' ) ) as unknown as L2MovingWorker

        return [
            'L2MoveManager : loaded MoveManager worker',
        ]
    }

    registerGeoData( regionCode: number, data: SharedArrayBuffer ) : void {
        this.movingWorker.registerGeoData( regionCode, data )
    }

    unRegisterGeoData( regionCode: number ) : void {
        this.movingWorker.unRegisterGeoData( regionCode )
    }

    /*
        Using movement ids which are permanently assigned to world objects versus object ids that can change.
     */
    revalidateZValues( movementIds: Array<number> ) : void {
        this.movingWorker.revalidateZValues( movementIds )
    }

    addMovementHook( movementId: number, hook: MovementHook ) : void {
        let allHooks = this.hooks[ movementId ]
        if ( !allHooks ) {
            allHooks = new Set<MovementHook>( [ hook ] )
            this.hooks[ movementId ] = allHooks

            return
        }

        allHooks.add( hook )
    }

    deleteMovementHooks( movementId: number ) : void {
        delete this.hooks[ movementId ]
    }

    hasMovementHook( movementId: number, hook: MovementHook ) : boolean {
        let allHooks = this.hooks[ movementId ]
        if ( !allHooks ) {
            return false
        }

        return allHooks.has( hook )
    }

    removeMovementHook( movementId: number, hook: MovementHook ) : void {
        let allHooks = this.hooks[ movementId ]
        if ( !allHooks ) {
            return
        }

        allHooks.delete( hook )
    }
}

export const L2MoveManager = new Manager()