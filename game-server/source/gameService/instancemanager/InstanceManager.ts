import { Instance } from '../models/entity/Instance'
import { InstanceWorld } from '../models/instancezone/InstanceWorld'
import { DatabaseManager } from '../../database/manager'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { DataManager } from '../../data/manager'
import {
    L2InstanceDataDoorProperties,
    L2InstanceDataItem,
    L2InstanceDataLocationProperties,
    L2InstanceDataSpawnProperties,
} from '../../data/interface/InstanceDataApi'
import { Location } from '../models/Location'
import { InstanceReenterType } from '../enums/InstanceReenterType'
import { InstanceReenterDayOfWeek, InstanceReenterHelper } from '../helpers/InstanceReenterHelper'
import { InstanceRemoveBuffType } from '../enums/InstanceRemoveBuffType'
import { L2World } from '../L2World'
import { L2CharacterInstanceTimeTableItem } from '../../database/interface/CharacterInstanceTimeTableApi'
import _ from 'lodash'

export type PlayerInstanceTime = Map<number, number>

class Manager implements L2DataApi {
    instances: { [ key: number ]: Instance } = {}
    instanceWorlds: { [ key: number ]: InstanceWorld } = {}
    playerInstanceTimes: { [ key: number ]: PlayerInstanceTime } = {}
    dynamicInstanceId: number = 300000

    getInstance( id: number ): Instance {
        return this.instances[ id ]
    }

    getInstanceIdForPlayer( playerId: number ): number {
        let outcome = 0
        _.each( this.instances, ( currentInstance: Instance ) => {
            if ( currentInstance && currentInstance.containsPlayer( playerId ) ) {
                outcome = currentInstance.getId()
                return false
            }
        } )

        return outcome
    }

    async getAllInstanceTimes( playerId: number ): Promise<PlayerInstanceTime> {
        if ( !this.playerInstanceTimes[ playerId ] ) {
            await this.restoreInstanceTimes( playerId )
        }

        return this.playerInstanceTimes[ playerId ]
    }

    async restoreInstanceTimes( playerId: number ): Promise<void> {
        let playerInstanceTimes = new Map<number, number>()
        let results: Array<L2CharacterInstanceTimeTableItem> = await DatabaseManager.getCharacterInstanceTime().getTimes( playerId )
        let instancesToDelete: Array<number> = []
        let currentTime = Date.now()

        results.forEach( ( databaseItem: L2CharacterInstanceTimeTableItem ) => {
            let { instanceId, time } = databaseItem

            if ( time < currentTime ) {
                instancesToDelete.push( instanceId )
                return
            }

            playerInstanceTimes.set( instanceId, time )
        } )

        this.playerInstanceTimes[ playerId ] = playerInstanceTimes

        // TODO : what if we want to ensure instance times do not get erased ? if server crashes server times are gone
        return DatabaseManager.getCharacterInstanceTime().deleteInstanceTime( playerId, ...instancesToDelete )
    }

    getPlayerWorld( objectId: number ): InstanceWorld {
        return _.find( this.instanceWorlds, ( world: InstanceWorld ) => {
            return world && world.isAllowed( objectId )
        } )
    }

    destroyInstance( instanceId: number ): void {
        if ( instanceId <= 0 ) {
            return
        }

        let currentInstance: Instance = this.instances[ instanceId ]
        if ( currentInstance ) {
            currentInstance.removeNpcs()
            currentInstance.removePlayers()
            currentInstance.removeDoors()
            currentInstance.cancelTimer()

            _.unset( this.instances, instanceId )
            _.unset( this.instanceWorlds, instanceId )

            L2World.destroyInstanceGrid( instanceId )
        }
    }

    createDynamicInstance( nameId: string ): number {
        this.dynamicInstanceId++
        let instance = new Instance( this.dynamicInstanceId, nameId )

        this.instances[ this.dynamicInstanceId ] = instance
        if ( nameId ) {
            this.loadInstanceByName( nameId, instance )
        }

        return this.dynamicInstanceId
    }

    deleteInstanceTime( objectId: number, instanceId: number ): Promise<void> {
        _.unset( this.playerInstanceTimes[ objectId ], instanceId )
        return DatabaseManager.getCharacterInstanceTime().deleteInstanceTime( objectId, instanceId )
    }

    async load(): Promise<Array<string>> {
        this.instances[ 0 ] = new Instance( 0, 'universe' )

        return [
            `InstanceManager loaded ${ _.size( this.instances ) } instances.`,
            `InstanceManager loaded ${ _.size( this.instanceWorlds ) } worlds.`,
        ]
    }

    getInstanceIdName( id: number ): string {
        return _.defaultTo( DataManager.getInstanceData().getInstanceNames()[ id ], 'UnknownInstance' )
    }

    getWorld( instanceId: number ): InstanceWorld {
        return this.instanceWorlds[ instanceId ]
    }

    loadInstanceByName( nameId: string, instance: Instance ): void {
        let data: L2InstanceDataItem = DataManager.getInstanceData().getInstanceData( nameId )

        if ( !data ) {
            return
        }

        L2World.createInstanceGrid( instance.getId() )

        instance.name = data.name
        instance.ejectTime = _.defaultTo( data.ejectTime, instance.ejectTime )
        instance.allowRandomWalk = _.defaultTo( data.allowRandomWalk, instance.allowRandomWalk )

        if ( data.activityTime > 0 ) {
            let nextValue = data.activityTime * 60000

            instance.checkTimeUpTask = setTimeout( instance.runCheckTimeUpTask.bind( instance ), 15000, nextValue )
            instance.instanceEndTime = Date.now() + nextValue + 15000
        }

        instance.allowSummon = _.defaultTo( data.allowSummon, instance.allowSummon )
        instance.emptyDestroyTime = _.defaultTo( data.emptyDestroyTime, instance.emptyDestroyTime )
        instance.showTimer = _.defaultTo( data.showTimer.showTimer, instance.showTimer )
        instance.isTimerIncreaseValue = _.defaultTo( data.showTimer.isTimerIncrease, instance.isTimerIncreaseValue )

        instance.timerText = _.defaultTo( data.showTimer.timerText, instance.timerText )

        _.each( data.doors, ( doorData: L2InstanceDataDoorProperties ) => {

            if ( !instance.doors[ doorData.doorId ] ) {

                /*
                    Due to circular dependency resolution doors need to be created in one single place, DoorData.
                 */
                // TODO : create door with coordinates directly, since setting instance id adds grid removal/insertion overhead
                // let template = DataManager.getDoorData().getDoorTemplate( doorData.doorId )
                // if ( !template ) {
                //     return
                // }
                //
                // let door : L2DoorInstance = new L2DoorInstance( template )
                //
                // door.setIsVisible( false )
                // door.setInstanceId( instance.getId() )
                // door.setCurrentHp( door.getMaxHp() )
                //
                // door.setIsVisible( true )
                // door.setXYZ( door.getTemplate().getX(), door.getTemplate().getY(), door.getTemplate().getZ() )
                // door.spawnMeNow()
                //
                // instance.doors[ doorData.doorId ] = door.getObjectId()
            }
        } )

        _.each( data.spawns, this.loadSpawnGroup.bind( this, instance ) )

        if ( data.exitPoint ) {
            instance.exitLocation = new Location( data.exitPoint.x, data.exitPoint.y, data.exitPoint.y )
        }

        _.each( data.spawnPoints, ( point: L2InstanceDataLocationProperties ) => {
            instance.enterLocations.push( new Location( point.x, point.y, point.z ) )
        } )

        instance.type = _.defaultTo( InstanceReenterType[ data.reEnter.type ], instance.type )

        _.each( data.reEnter.times, ( timeData: any ) => {
            let { time } = timeData
            if ( time > 0 ) {
                instance.resetData.push( InstanceReenterHelper.fromTime( time ) )
            }
        } )

        _.each( data.reEnter.conditions, ( condition: any ) => {
            let { day, hour, minute } = condition

            let dayValue: InstanceReenterDayOfWeek = InstanceReenterDayOfWeek[ _.toUpper( day ) as string ]
            if ( !_.isUndefined( dayValue ) ) {
                instance.resetData.push( InstanceReenterHelper.fromTimeOptions( dayValue, hour, minute ) )
            }
        } )

        instance.removeBuffType = _.defaultTo( InstanceRemoveBuffType[ data.removeBuffs.removeBuffType ], instance.removeBuffType )
    }

    loadSpawnGroup( instance: Instance, items: Array<L2InstanceDataSpawnProperties>, groupName: string ): void {
        // _.each( items, ( data: L2InstanceDataSpawnProperties ) => {
        //     let spawn : L2Spawn = L2Spawn.fromNpcId( data.npcId )
        //
        //     spawn.setX( data.x )
        //     spawn.setY( data.y )
        //     spawn.setZ( data.z )
        //     spawn.setAmount( 1 )
        //
        //     spawn.setHeading( data.heading )
        //     spawn.setRespawnDelay( data.respawn, _.get( data, 'respawnRandom' ) )
        //
        //     data.respawn === 0 ? spawn.stopRespawn() : spawn.startRespawn()
        //     spawn.setInstanceId( instance.getId() )
        //
        //     let allowRandomWalk : string = _.get( data, 'allowRandomWalk' )
        //     spawn.setHasNoRandomWalk( allowRandomWalk ? allowRandomWalk !== 'true' : !instance.allowRandomWalk )
        //     spawn.setAreaName( _.get( data, 'areaName' ) )
        //     spawn.setGlobalMapId( _.get( data, 'globalMapId' ) )
        //
        //     if ( groupName === 'general' ) {
        //         let npc : L2Npc = spawn.doSpawn()
        //         let delay = _.get( data, 'onKillDelay', -1 )
        //
        //         if ( delay > 0 && npc.isAttackable() ) {
        //             ( npc as L2Attackable ).setOnKillDelay( delay )
        //         }
        //
        //         instance.generalSpawns.push( spawn )
        //         return
        //     }
        //
        //     if ( !instance.manualSpawn[ groupName ] ) {
        //         instance.manualSpawn[ groupName ] = []
        //     }
        //
        //     instance.manualSpawn[ groupName ].push( spawn )
        // } )
    }

    addWorld( world: InstanceWorld ): void {
        this.instanceWorlds[ world.getInstanceId() ] = world
    }

    async setInstanceTime( playerId: number, instanceId: number, time: number ): Promise<void> {
        if ( !this.playerInstanceTimes[ playerId ] ) {
            await this.restoreInstanceTimes( playerId )
        }

        await DatabaseManager.getCharacterInstanceTime().addInstanceTime( playerId, instanceId, time )
        this.playerInstanceTimes[ playerId ][ instanceId ] = time
    }

    async getInstanceTime( playerId: number, instanceId: number ): Promise<number> {
        if ( !this.playerInstanceTimes[ playerId ] ) {
            await this.restoreInstanceTimes( playerId )
        }

        return this.playerInstanceTimes[ playerId ][ instanceId ]
    }
}

export const InstanceManager = new Manager()