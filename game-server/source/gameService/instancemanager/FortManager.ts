import { L2Clan } from '../models/L2Clan'
import { Fort } from '../models/entity/Fort'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { DatabaseManager } from '../../database/manager'
import { L2FortTableData } from '../../database/interface/FortTableApi'
import _, { DebouncedFunc } from 'lodash'
import { ListenerCache } from '../cache/ListenerCache'
import { ListenerRegisterType } from '../enums/ListenerRegisterType'
import { EventType, GeoRegionActivatedEvent } from '../models/events/EventType'
import chalk from 'chalk'
import { RegionActivation } from '../models/RegionActivation'
import RBush from 'rbush'
import { ISpacialIndexResidence, SpatialIndexHelper } from '../models/SpatialIndexData'
import knn from 'rbush-knn'
import { ILocational } from '../models/Location'
import { ServerLog } from '../../logger/Logger'

class Manager implements L2DataApi {
    activatedRegions: RegionActivation = new RegionActivation()
    regions: Record<number, Fort>
    fortsById: Record<number, Fort>
    allFortIds: Array<number>
    allForts: Array<Fort>
    fortZonesGrid: RBush<ISpacialIndexResidence> = new RBush<ISpacialIndexResidence>()

    fortUpdates: Set<number> = new Set<number>()
    debounceFortUpdate: DebouncedFunc<any> = _.debounce( this.runFortUpdate.bind( this ), 15000, {
        maxWait: 30000
    } )

    findNearestFort( object: ILocational ): Fort {
        let foundItem = knn( this.fortZonesGrid, object.getX(), object.getY(), 1 )[ 0 ] as ISpacialIndexResidence
        return foundItem.residence as Fort
    }

    getFort( object: ILocational ): Fort {
        let box = SpatialIndexHelper.getBox( object.getX(), object.getY(), 1 )
        let foundItem = this.fortZonesGrid.search( box ).find( ( item: ISpacialIndexResidence ) : boolean => item.area.isObjectInside( object ) )

        SpatialIndexHelper.returnBox( box )

        return foundItem?.residence as Fort
    }

    getFortById( id: number ): Fort {
        return this.fortsById[ id ]
    }

    getFortByOwner( clan: L2Clan ): Fort {
        return this.allForts.find( ( currentFort: Fort ) : boolean => {
            return currentFort.getOwnerClan() === clan
        } )
    }

    getForts() : ReadonlyArray<Fort> {
        return this.allForts
    }

    getResidenceIds(): ReadonlyArray<number> {
        return this.allFortIds
    }

    async load(): Promise<Array<string>> {
        let fortItems = await DatabaseManager.getFortTable().getAll()
        this.fortsById = {}
        this.allForts = []
        this.allFortIds = []
        this.regions = {}

        let gridItems : Array<ISpacialIndexResidence> = []

        fortItems.forEach( ( data: L2FortTableData ): void => {
            let fort = new Fort( data )

            this.fortsById[ data.id ] = fort
            this.allForts.push( fort )
            this.allFortIds.push( data.id )

            gridItems.push( {
                ...fort.residenceArea.getSpatialIndex(),
                residence: fort
            } )
        } )

        this.fortZonesGrid.load( gridItems )

        ListenerCache.registerListener( ListenerRegisterType.General, EventType.GeoRegionActivated, this, this.onRegionActivation.bind( this ) )

        return [
            `FortManager : loaded ${ this.allFortIds.length } forts.`,
        ]
    }

    async onRegionActivation( data: GeoRegionActivatedEvent ): Promise<void> {
        if ( this.activatedRegions.isActivated( data.code ) ) {
            return
        }

        let fort = this.regions[ data.code ]
        if ( !fort || fort.isActivated() ) {
            return
        }

        this.activatedRegions.markActivated( data.code )
        let startTime = Date.now()

        /*
            Initialization is delayed due to npc spawns attached to fort residence.
            Fort needs to figure out what kind of events need to be sent to spawns
            to indicate which spawns are activated during which part of siege.
         */
        await fort.initialize()

        ServerLog.info( `FortManager: activated ${ chalk.greenBright( fort.getName() ) } fort in ${ Date.now() - startTime } ms` )
    }

    runFortUpdate() : Promise<void> {
        let items : Array<L2FortTableData> = []

        for ( const fortId of this.fortUpdates ) {
            let fort = this.fortsById[ fortId ]
            if ( fort ) {
                items.push( fort.data )
            }
        }

        this.fortUpdates.clear()

        return DatabaseManager.getFortTable().updateAll( items )
    }

    scheduleUpdate( fortId: number ) : void {
        this.fortUpdates.add( fortId )
        this.debounceFortUpdate()
    }
}

export const FortManager = new Manager()