import { Siegable, SiegableType } from '../models/entity/Siegable'
import { L2Clan } from '../models/L2Clan'
import { L2SiegeClan } from '../models/L2SiegeClan'
import { L2SiegeFlagInstance } from '../models/actor/instance/L2SiegeFlagInstance'
import { TerritoryWard } from './territory/TerritoryWard'
import { Territory } from './territory/Territory'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { TerritoryNPCSpawn } from './territory/TerritoryNPCSpawn'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { L2World } from '../L2World'
import { GeneralHelper } from '../helpers/GeneralHelper'
import { PacketDispatcher } from '../PacketDispatcher'
import { DatabaseManager } from '../../database/manager'
import { CastleManager } from './CastleManager'
import { L2DoorInstance } from '../models/actor/instance/L2DoorInstance'
import _ from 'lodash'
import { SiegeRole } from '../enums/SiegeRole'
import { EventType, TWPlayerDeathEvent, TWPlayerLoginEvent } from '../models/events/EventType'
import { EventPoolCache } from '../cache/EventPoolCache'
import { L2Npc } from '../models/actor/L2Npc'
import { ListenerCache } from '../cache/ListenerCache'

export const TerritoryWarItems = {
    81: 13757,
    82: 13758,
    83: 13759,
    84: 13760,
    85: 13761,
    86: 13762,
    87: 13763,
    88: 13764,
    89: 13765,
}

// TODO : better data structures and types, especially for points
class Manager implements Siegable {
    // Territory War settings
    registeredClans: { [ key: number ]: Array<L2Clan> } = {}
    registeredMercenaries: { [ key: number ]: Array<number> } = {}
    territoryList: { [ key: number ]: Territory } = {}
    disguisedPlayers: Array<number> = []
    territoryWards: Array<TerritoryWard> = []
    clanFlags: { [ key: number ]: L2SiegeFlagInstance } = {} // clanId to flag mapping
    participantPoints: { [ key: number ]: Array<number> } = {}
    startTWDate: number = Date.now()
    isRegistrationOver: boolean = true
    isTWChannelOpen: boolean = false
    isTWInProgress: boolean = false
    scheduledStartTWTask: any = null
    scheduledEndTWTask: any = null
    scheduledRewardOnlineTask: any = null

    announceToParticipants( packet: Buffer, exp: number, sp: number ) {
        _.each( this.territoryList, ( territory: Territory ) => {
            let clan = territory.getOwnerClan()
            if ( clan ) {
                _.each( clan.getOnlineMembers( 0 ), ( memberId: number ) => {
                    PacketDispatcher.sendCopyData( memberId, packet )

                    let player = L2World.getPlayer( memberId )
                    if ( player && ( exp > 0 || sp > 0 ) ) {
                        player.addExpAndSp( exp, sp )
                    }
                } )
            }
        } )

        _.each( this.registeredClans, ( clanList: Array<L2Clan> ) => {
            _.each( clanList, ( clan: L2Clan ) => {
                _.each( clan.getOnlineMembers( 0 ), ( memberId: number ) => {
                    PacketDispatcher.sendCopyData( memberId, packet )

                    let player = L2World.getPlayer( memberId )
                    if ( player && ( exp > 0 || sp > 0 ) ) {
                        player.addExpAndSp( exp, sp )
                    }
                } )
            } )
        } )

        let manager = this
        _.each( this.registeredMercenaries, ( playerIds: Array<number> ) => {
            _.each( playerIds, ( playerId: number ) => {
                let player = L2World.getPlayer( playerId )

                if ( player && ( player.getClan() || !manager.checkIsRegisteredClan( -1, player.getClan() ) ) ) {
                    PacketDispatcher.sendCopyData( playerId, packet )

                    if ( player && ( exp > 0 || sp > 0 ) ) {
                        player.addExpAndSp( exp, sp )
                    }
                }
            } )
        } )
    }

    checkIsAttacker( clan: L2Clan ): boolean {
        return false
    }

    checkIsDefender( clan: L2Clan ): boolean {
        return false
    }

    async endSiege(): Promise<void> {
    }

    getAttackerClan( clanId: number ): L2SiegeClan

    getAttackerClan( clan: L2Clan ): L2SiegeClan

    getAttackerClan( clanId: number | L2Clan ): L2SiegeClan {
        return undefined
    }

    getAttackerClans(): Array<L2SiegeClan> {
        return undefined
    }

    getAttackersInZone(): Array<number> {
        return undefined
    }

    getDefenderClan( clanId: number ): L2SiegeClan

    getDefenderClan( clan: L2Clan ): L2SiegeClan

    getDefenderClan( clanId: number | L2Clan ): L2SiegeClan {
        return undefined
    }

    getDefenderClans(): Array<L2SiegeClan> {
        return undefined
    }

    getFameAmount(): number {
        return 0
    }

    getFameFrequency(): number {
        return 0
    }

    getFlag( clan: L2Clan ): Array<number> {
        return undefined
    }

    getSiegeDate(): number {
        return 0
    }

    giveFame(): boolean {
        return false
    }

    async startSiege(): Promise<void> {
    }

    updateSiege(): void {
    }

    checkIsRegistered( castleId: number, objectId: number ): boolean {
        if ( castleId === -1 ) {
            let outcome = false
            _.each( this.registeredMercenaries, ( mercenaries: Array<number> ) => {
                if ( mercenaries.includes( objectId ) ) {
                    outcome = true
                    return false
                }
            } )

            return outcome
        }

        return this.registeredMercenaries[ castleId ].includes( objectId )
    }

    checkIsRegisteredClan( castleId: number, clan: L2Clan ): boolean {
        if ( !clan ) {
            return
        }

        if ( clan.getCastleId() > 0 ) {
            return castleId === -1 || clan.getCastleId() === castleId
        }

        if ( castleId ) {
            let outcome = false
            _.each( this.registeredClans, ( clanGroup: Array<L2Clan> ) => {
                if ( clanGroup.includes( clan ) ) {
                    outcome = true
                    return false
                }
            } )

            return outcome
        }

        return this.registeredClans[ castleId ].includes( clan )
    }

    async dropCombatFlag( player: L2PcInstance, isKilled: boolean, isSpawnBack: boolean ): Promise<void> {
        let playerId = player.getObjectId()
        let ward = this.territoryWards.find( ward => ward.playerId === playerId )
        if ( !ward ) {
            return
        }

        await ward.dropIt()

        if ( isKilled ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.THE_CHAR_THAT_ACQUIRED_S1_WARD_HAS_BEEN_KILLED )
                .addString( ward.getNpc().getName().replace( /\sWard/g, '' ) )
                .getBuffer()
            this.announceToParticipants( packet, 0, 0 )
        }

        if ( !this.isTWInProgress ) {
            return
        }

        if ( isKilled ) {
            await ward.spawnMe()
        } else if ( isSpawnBack ) {
            await ward.spawnMe()
        } else {
            _.each( this.territoryList[ ward.getOwnerCastleId() ].getOwnedWard(), ( spawn: TerritoryNPCSpawn ) => {
                if ( spawn.getId() === ward.getTerritoryId() ) {
                    //spawn.setNPC( spawn.getNpc().getSpawn().doSpawn() )
                    ward.unSpawnMe()
                    ward.setNpc( spawn.getNpc() )
                }
            } )
        }
    }

    // TODO : convert to use clanId
    getFlagForClan( clan: L2Clan ): L2SiegeFlagInstance {
        return this.clanFlags[ clan.getId() ]
    }

    // TODO : convert to use castleId
    getSiegeFlagForClan( clan: L2Clan ): L2SiegeFlagInstance {
        if ( clan.getCastleId() > 0 ) {
            let territory = this.territoryList[ clan.getCastleId() ]
            if ( !territory ) {
                return
            }

            return territory.getHQ()
        }
    }

    // TODO : better look ups by id
    getRegisteredTerritoryId( player: L2PcInstance ): number {
        if ( !player || !this.isTWChannelOpen || player.getLevel() < ConfigManager.territoryWar.getPlayerMinLevel() ) {
            return 0
        }

        let playerClan = player.getClan()
        if ( playerClan ) {
            if ( playerClan.getCastleId() > 0 ) {
                return playerClan.getCastleId() + 80
            }

            let outcome = 0
            // @ts-ignore
            _.each( this.registeredClans, ( clans: Array<L2Clan>, castleId: number ) => {
                if ( clans.includes( playerClan ) ) {
                    outcome = castleId + 80
                    return false
                }
            } )

            if ( outcome !== 0 ) {
                return outcome
            }
        }

        let outcome = 0

        // @ts-ignore
        _.each( this.registeredMercenaries, ( mercenaries: Array<number>, castleId: number ) => {
            if ( mercenaries.includes( player.getObjectId() ) ) {
                outcome = castleId + 80
                return false
            }
        } )

        return outcome
    }

    getTerritory( castleId: number ): Territory {
        return this.territoryList[ castleId ]
    }

    getFlagForTerritory( territoryId: number ): L2SiegeFlagInstance {
        let territory = this.territoryList[ territoryId - 80 ]
        if ( !territory ) {
            return
        }

        return territory.getHQ()
    }

    giveTWPoint( player: L2PcInstance, side: number, type: number ): void {
        if ( side === 0 ) {
            return
        }

        let manager = this
        if ( player.getParty() && ( type < 5 ) ) {
            _.each( player.getParty().getMembers(), ( memberId: number ) => {
                let memberPlayer = L2World.getPlayer( memberId )
                if ( !player
                    || ( memberPlayer.getSiegeSide() === side )
                    || ( memberPlayer.getSiegeSide() === 0 )
                    || !GeneralHelper.checkIfInRange( 2000, player, memberPlayer, false ) ) {
                    return
                }

                if ( !manager.participantPoints[ memberId ] ) {
                    manager.participantPoints[ memberId ] = [
                        memberPlayer.getSiegeSide(),
                        0,
                        0,
                        0,
                        0,
                        0,
                        0,
                    ]
                }

                manager.participantPoints[ memberId ][ type ] += 1
            } )

        } else {
            if ( ( player.getSiegeSide() === side ) || ( player.getSiegeSide() === 0 ) ) {
                return
            }

            let objectId = player.getObjectId()
            if ( !this.participantPoints[ objectId ] ) {
                this.participantPoints[ objectId ] = [
                    player.getSiegeSide(),
                    0,
                    0,
                    0,
                    0,
                    0,
                    0,
                ]
            }

            this.participantPoints[ objectId ][ type ] += 1
        }
    }

    isDisguised( playerId: number ): boolean {
        return this.disguisedPlayers.includes( playerId )
    }

    getTerritoryWardById( id: number ): TerritoryWard {
        return _.find( this.territoryWards, ( ward: TerritoryWard ) => {
            return ward.getTerritoryId() === id
        } )
    }

    setSiegeFlagForClan( clan: L2Clan, flag: L2SiegeFlagInstance ): void {
        let castleId = clan.getCastleId()
        if ( castleId > 0 ) {
            this.territoryList[ castleId ].setHQ( flag )
        }
    }

    addClanFlag( clan: L2Clan, flag: L2SiegeFlagInstance ) {
        this.clanFlags[ clan.getId() ] = flag
    }

    getAllTerritories(): Array<Territory> {
        return _.filter( this.territoryList, ( territory: Territory ) => {
            return !!territory.getOwnerClan()
        } )
    }

    getTWStartTime(): number {
        return this.startTWDate
    }

    getRegisteredClans( castleId: number ): Array<L2Clan> {
        return this.registeredClans[ castleId ]
    }

    getRegisteredMercenaries( castleId: number ): Array<number> {
        return this.registeredMercenaries[ castleId ]
    }

    getIsRegistrationOver(): boolean {
        return this.isRegistrationOver
    }

    async registerClan( castleId: number, clan: L2Clan ): Promise<void> {
        if ( !clan || _.includes( this.registeredClans[ castleId ], clan ) ) {
            return
        }

        if ( !this.registeredClans[ castleId ] ) {
            this.registeredClans[ castleId ] = []
        }

        this.registeredClans[ castleId ].push( clan )
        await this.changeRegistration( castleId, clan.getId(), false )
    }

    async registerMercenary( castleId: number, player: L2PcInstance ): Promise<void> {
        if ( !player || ( player.getLevel() < ConfigManager.territoryWar.getPlayerMinLevel() ) || _.includes( this.registeredMercenaries[ castleId ], player.getObjectId() ) ) {
            return
        }

        if ( !this.registeredMercenaries[ castleId ] ) {
            this.registeredMercenaries[ castleId ] = []
        }

        this.registeredMercenaries[ castleId ].push( player.getObjectId() )
        await this.changeRegistration( castleId, player.getObjectId(), false )
    }

    async removeClan( castleId: number, clan: L2Clan ): Promise<void> {
        if ( !clan ) {
            return
        }

        if ( _.includes( this.registeredClans[ castleId ], clan ) ) {
            _.pull( this.registeredClans[ castleId ], clan )
            await this.changeRegistration( castleId, clan.getId(), true )
        }
    }

    async removeMercenary( castleId: number, player: L2PcInstance ): Promise<void> {
        if ( !player ) {
            return
        }

        if ( _.includes( this.registeredMercenaries[ castleId ], player.getObjectId() ) ) {
            _.pull( this.registeredMercenaries[ castleId ], player.getObjectId() )
            await this.changeRegistration( castleId, player.getObjectId(), true )
        }
    }


    async changeRegistration( castleId: number, objectId: number, shouldDelete: boolean ) {
        if ( shouldDelete ) {
            return DatabaseManager.getTerritoryRegistrations().remove( castleId, objectId )
        }

        return DatabaseManager.getTerritoryRegistrations().add( castleId, objectId )
    }

    getAllTerritoryWards(): Array<TerritoryWard> {
        return this.territoryWards
    }

    addDisguisedPlayer( objectId: number ): void {
        this.disguisedPlayers.push( objectId )
    }

    isInstance( type: SiegableType ): boolean {
        return type === SiegableType.TerritoryWarManager
    }

    isAllyField( player: L2PcInstance, fieldId: number ): boolean {
        if ( !player || player.getSiegeSide() === 0 ) {
            return false
        }

        if ( ( player.getSiegeSide() - 80 ) === fieldId ) {
            return true
        }

        if ( ( fieldId > 100 )
            && this.territoryList[ player.getSiegeSide() - 80 ]
            && this.territoryList[ player.getSiegeSide() - 80 ].getFortId() === fieldId ) {
            return true
        }

        return false
    }

    getTerritoryWard( territoryId: number ): TerritoryWard {
        return _.find( this.territoryWards, ( ward: TerritoryWard ) => {
            return ward.getTerritoryId() === territoryId
        } )
    }

    calculateReward( player: L2PcInstance ): [ number, number ] {
        let pointData = this.participantPoints[ player.getObjectId() ]
        if ( !pointData ) {
            return [ 0, 0 ]
        }

        let reward: [ number, number ] = [ 0, 0 ]
        reward[ 0 ] = pointData[ 0 ]
        // badges for being online. if char was not online at least 10 mins
        // than he cant get rewards(also this will handle that player already get his/her rewards)
        if ( _.nth( pointData, 6 ) < 10 ) {
            return reward
        }

        reward[ 1 ] += ( pointData[ 6 ] > 70 ? 7 : Math.floor( pointData[ 6 ] * 0.1 ) )
        // badges for player Quests
        reward[ 1 ] += pointData[ 2 ] * 7
        // badges for player Kills
        if ( pointData[ 1 ] < 50 ) {
            reward[ 1 ] += Math.floor( pointData[ 1 ] * 0.1 )
        } else if ( pointData[ 1 ] < 120 ) {
            reward[ 1 ] += Math.floor( 5 + ( ( pointData[ 1 ] - 50 ) / 14 ) )
        } else {
            reward[ 1 ] += 10
        }
        // badges for territory npcs
        reward[ 1 ] += pointData[ 3 ]
        // badges for territory catapults
        reward[ 1 ] += pointData[ 4 ] * 2
        // badges for territory Wards
        reward[ 1 ] += ( pointData[ 5 ] > 0 ? 5 : 0 )

        // badges for territory quest done
        let territory: Territory = this.territoryList[ pointData[ 0 ] - 80 ]
        if ( !territory ) {
            return reward
        }

        let [ first, second ] = territory.getQuestDone()
        reward[ 1 ] += Math.min( first, 10 )
        reward[ 1 ] += Math.min( second, 0 )
        reward[ 1 ] += territory.getOwnedWardIds().length

        return reward
    }

    resetReward( player: L2PcInstance ): void {
        let pointData = this.participantPoints[ player.getObjectId() ]
        if ( pointData ) {
            pointData[ 6 ] = 0
        }
    }

    getDominionWarState(): number {
        return this.isTWInProgress ? 5 : 0
    }

    onTerritoryCatapultDestroyed( castleId: number ): void {
        if ( this.territoryList[ castleId ] ) {
            this.territoryList[ castleId ].changeNpcSpawn( 2, false )
        }

        // TODO : move logic to CastleManager
        CastleManager.getCastleById( castleId ).getDoors().forEach( ( doorId: number ) => {
            let door = L2World.getObjectById( doorId ) as L2DoorInstance
            if ( door ) {
                door.openMe()
            }
        } )
    }

    isInProgress(): boolean {
        return this.isTWInProgress
    }

    giveQuestPoints( player: L2PcInstance ): void {
        let data = this.participantPoints[ player.getObjectId() ]
        if ( !data ) {
            data = [
                player.getSiegeSide(),
                0,
                0,
                0,
                0,
                0,
                0
            ]

            this.participantPoints[ player.getObjectId() ] = data
        }

        data[ 2 ]++
    }

    onLogin( player: L2PcInstance ): void {
        let territoryId = this.getRegisteredTerritoryId( player )
        if ( territoryId === 0 ) {
            return
        }

        if ( this.isTWInProgress ) {
            player.setSiegeRole( SiegeRole.Attacker )
        }

        player.setSiegeSide( territoryId )

        if ( ListenerCache.hasGeneralListener( EventType.TWPlayerLogin ) ) {
            let data = EventPoolCache.getData( EventType.TWPlayerLogin ) as TWPlayerLoginEvent

            data.territoryId = territoryId
            data.playerId = player.getObjectId()
            data.classId = player.getClassId()

            ListenerCache.sendGeneralEvent( EventType.TWPlayerLogin, data )
        }
    }

    onDeath( attacker: L2PcInstance, target: L2PcInstance ): Promise<void> {
        if ( target.getLevel() < 61 ) {
            return
        }

        if ( !ListenerCache.hasGeneralListener( EventType.TWPlayerDeath ) ) {
            return
        }

        let data = EventPoolCache.getData( EventType.TWPlayerDeath ) as TWPlayerDeathEvent

        data.attackerId = attacker.getObjectId()
        data.territoryId = attacker.getSiegeSide()
        data.targetId = target.getObjectId()
        data.targetClassId = target.getClassId()
        data.partyPlayerIds = this.getOnDeathPartyIds( attacker, target )

        return ListenerCache.sendGeneralEvent( EventType.TWPlayerDeath, data, target.getClassId() )
    }

    private getOnDeathPartyIds( attacker: L2PcInstance, target: L2PcInstance ): Array<number> {
        let party = attacker.getParty()

        if ( !party ) {
            if ( attacker.getSiegeSide() !== 0 && attacker.getSiegeSide() !== target.getSiegeSide() ) {
                return [ attacker.getObjectId() ]
            }

            return []
        }

        return party.getMembers().reduce( ( allIds: Array<number>, objectId: number ): Array<number> => {
            let player = L2World.getPlayer( objectId )
            if ( player
                && player.getSiegeSide() !== 0
                && player.getSiegeSide() !== target.getSiegeSide()
                && ( objectId === attacker.getObjectId() || GeneralHelper.checkIfInRange( 2000, target, player, false ) ) ) {
                allIds.push( objectId )
            }

            return allIds
        }, [] )
    }

    addTerritoryWard( territoryId: number, newCastleId: number, oldCastleId: number, notify: boolean ): L2Npc {
        let territory = this.territoryList[ newCastleId ]
        if ( !territory ) {
            return null
        }

        // TODO: implement me
    }

    territoryCatapultDestroyed( castleId: number ): void {
        let territory = this.territoryList[ castleId ]
        if ( territory ) {
            territory.changeNPCsSpawn( 2, false )
        }

        CastleManager.getCastleById( castleId ).getDoors().forEach( ( objectId: number ) => {
            let door = L2World.getObjectById( objectId ) as L2DoorInstance
            if ( !door ) {
                return
            }

            door.openMe()
        } )
    }

    getTerritoryPlayerIds( territoryId: number ): Array<number> {
        // TODO : inspect usages of L2PcInstance.setSiegeSide and return players registered for particular territory id
        return []
    }
}

export const TerritoryWarManager = new Manager()