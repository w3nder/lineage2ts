import { Location } from '../../models/Location'
import { L2World } from '../../L2World'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { TerritoryWarManager } from '../TerritoryWarManager'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ItemManagerCache } from '../../cache/ItemManagerCache'
import { getSlotFromItem } from '../../models/itemcontainer/ItemSlotHelper'
import { ISpawnLogic } from '../../models/spawns/ISpawnLogic'
import { L2ManualSpawn } from '../../models/spawns/type/L2ManualSpawn'
import { DataManager } from '../../../data/manager'
import { L2ItemSlots } from '../../enums/L2ItemSlots'

export class TerritoryWard {
    playerId: number = 0 // TODO : why use two player identifiers?
    item: L2ItemInstance
    npcObjectId: number = 0
    spawn: ISpawnLogic

    location: Location
    oldLocation: Location

    itemId: number
    ownerCastleId: number

    territoryId: number
    npcSpawns: Array<ISpawnLogic> = []

    async dropIt() : Promise<void> {
        let player = L2World.getPlayer( this.playerId )
        let item: L2ItemInstance = this.item

        player.setCombatFlagEquipped( false )
        let slot : L2ItemSlots = getSlotFromItem( item )
        await player.getInventory().unEquipItemInBodySlot( slot )
        await player.destroyItem( item, true, 'TerritoryWard.dropIt' )
        player.broadcastUserInfo()

        this.item = null
        this.location = new Location( player.getX(), player.getY(), player.getZ(), player.getHeading() )
        this.playerId = 0
    }

    async spawnMe() : Promise<void> {
        if ( this.playerId !== 0 ) {
            await this.dropIt()
        }

        if ( !this.spawn ) {
            let template = DataManager.getNpcData().getTemplate( 36491 + this.territoryId )
            this.spawn = L2ManualSpawn.fromParameters( template, 1, this.location.getX(), this.location.getY(), this.location.getZ(), 0, 0 )
        }

        this.spawn.startSpawn()
        this.npcObjectId = this.spawn.getNpcObjectIds()[ 0 ]
    }

    /*
        TODO : integrate with spawns
     */
    async unSpawnMe() : Promise<void> {
        if ( this.playerId !== 0 ) {
            await this.dropIt()
        }

        let npc = L2World.getObjectById( this.npcObjectId ) as L2Npc
        if ( npc && !npc.isDecayed() ) {
            await npc.deleteMe()
        }

        this.npcObjectId = 0
    }

    spawnBack() {
        this.spawnMe()
    }

    async activate( player: L2PcInstance, item: L2ItemInstance ): Promise<void> {
        if ( player.isMounted() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_EQUIP_ITEM_DUE_TO_BAD_CONDITION ) )
            await player.destroyItem( item, true, 'TerritoryWard.activate' )
            return this.spawnMe()
        }

        if ( TerritoryWarManager.getRegisteredTerritoryId( player ) === 0 ) {
            player.sendMessage( 'Non participants can\'t pickup Territory Wards!' )
            await player.destroyItem( item, true, 'TerritoryWard.activate' )
            return this.spawnMe()
        }

        this.playerId = player.getObjectId()

        let npc = L2World.getObjectById( this.npcObjectId ) as L2Npc

        if ( !npc ) {
            return
        }

        this.oldLocation = new Location( npc.getX(), npc.getY(), npc.getZ(), npc.getHeading() )
        this.npcObjectId = 0

        if ( !item ) {
            this.item = ItemManagerCache.createItem( this.itemId, 'TerritoryWard.activate', 1, player.getObjectId() )
        } else {
            this.item = item
        }

        await player.getInventory().equipItem( this.item )

        let equippedPacket = new SystemMessageBuilder( SystemMessageIds.S1_EQUIPPED )
                .addItemInstanceName( this.item )
                .getBuffer()
        player.sendOwnedData( equippedPacket )

        player.broadcastUserInfo()
        player.setCombatFlagEquipped( true )

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_VE_ACQUIRED_THE_WARD ) )
        TerritoryWarManager.giveTWPoint( player, this.territoryId, 5 )
    }

    getOwnerCastleId(): number {
        return this.ownerCastleId
    }

    setOwnerCastleId( value: number ) : void {
        this.ownerCastleId = value
    }

    getTerritoryId() {
        return this.territoryId
    }

    setNpc( npc: L2Npc ) {
        this.npcObjectId = npc.getObjectId()
    }

    getNpc(): L2Npc {
        return L2World.getObjectById( this.npcObjectId ) as L2Npc
    }

    getPlayer() {
        return L2World.getPlayer( this.playerId )
    }
}