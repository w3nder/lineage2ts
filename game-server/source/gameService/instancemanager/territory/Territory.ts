import { L2Clan } from '../../models/L2Clan'
import { L2SiegeFlagInstance } from '../../models/actor/instance/L2SiegeFlagInstance'
import { TerritoryNPCSpawn } from './TerritoryNPCSpawn'
import { L2Npc } from '../../models/actor/L2Npc'
import aigle from 'aigle'
import { ILocational } from '../../models/Location'

export class Territory {
    territoryId: number
    castleId: number // territory Castle
    fortId: number // territory Fortress
    ownerClan: L2Clan
    spawns: Array<TerritoryNPCSpawn> = []
    territoryWardSpawnPlaces: Array<TerritoryNPCSpawn> = []
    isInProgress: boolean = false
    territoryHQ: L2SiegeFlagInstance = null
    questDone: Array<number> = []

    getHQ(): L2SiegeFlagInstance {
        return this.territoryHQ
    }

    getOwnedWardIds(): Array<number> {
        return this.territoryWardSpawnPlaces.reduce( ( allIds: Array<number>, spawn: TerritoryNPCSpawn ) => {
            if ( spawn.getId() > 0 ) {
                allIds.push( spawn.getId() )
            }

            return allIds
        }, [] )
    }

    getOwnedWard(): Array<TerritoryNPCSpawn> {
        return this.territoryWardSpawnPlaces
    }

    getOwnerClan() {
        return this.ownerClan
    }

    setHQ( flag: L2SiegeFlagInstance ) {
        this.territoryHQ = flag
    }

    setOwnerClan( clan: L2Clan ) {
        this.ownerClan = clan
    }

    getTerritoryId() {
        return this.territoryId
    }

    getCastleId() {
        return this.castleId
    }

    getFortId() {
        return this.fortId
    }

    getQuestDone() {
        return this.questDone
    }

    getSpawns(): Array<TerritoryNPCSpawn> {
        return this.spawns
    }

    async changeNpcSpawn( type: number, isNewSpawn: boolean ) : Promise<void> {
        if ( type < 0 || type > 3 ) {
            return
        }

        await aigle.resolve( this.spawns ).each( async ( spawn: TerritoryNPCSpawn ) : Promise<void> => {
            if ( spawn.type !== type ) {
                return
            }

            if ( isNewSpawn ) {
                spawn.setNPC( this.spawnNpc( spawn.getId(), spawn.location ) )
                return
            }

            let npc: L2Npc = spawn.getNpc()
            if ( npc && !npc.isDead() ) {
                await npc.deleteMe()
            }

            spawn.setNPC( null )
        } )
    }

    changeNPCsSpawn( type: number, isSpawn: boolean ) {
        // TODO : implement me
    }

    // TODO : convert to use geo regional spawn
    private spawnNpc( npcId: number, location: ILocational ): L2Npc {
        // let currentSpawn: L2Spawn = L2Spawn.fromNpcId( npcId )
        //
        // currentSpawn.setAmount( 1 )
        // currentSpawn.setX( location.getX() )
        // currentSpawn.setY( location.getY() )
        // currentSpawn.setZ( location.getZ() )
        // currentSpawn.setHeading( location.getHeading() )
        // currentSpawn.stopRespawn()
        //
        // return currentSpawn.doSpawn( false )

        return null
    }
}