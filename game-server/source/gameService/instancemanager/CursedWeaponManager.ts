import { CursedWeapon } from '../models/CursedWeapon'
import { DataManager } from '../../data/manager'
import { L2World } from '../L2World'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { L2Attackable } from '../models/actor/L2Attackable'
import { InstanceType } from '../enums/InstanceType'
import { L2Character } from '../models/actor/L2Character'
import _ from 'lodash'

const noDropInstanceTypes = new Set<InstanceType>( [
    InstanceType.L2DefenderInstance,
    InstanceType.L2RiftInvaderInstance,
    InstanceType.L2FestivalMonsterInstance,
    InstanceType.L2GuardInstance,
    InstanceType.L2GrandBossInstance,
    InstanceType.L2FeedableBeastInstance,
    InstanceType.L2FortCommanderInstance,
] )

class Manager {

    nextDropWeapon: CursedWeapon

    async activate( player: L2PcInstance, item: L2ItemInstance ): Promise<void> {
        let weapon: CursedWeapon = DataManager.getCursedWeaponData().getCursedWeapons()[ item.getId() ]

        if ( player.isCursedWeaponEquipped() ) { // cannot own 2 cursed swords
            let equippedWeapon: CursedWeapon = DataManager.getCursedWeaponData().getCursedWeapons()[ player.getCursedWeaponEquippedId() ]
            // TODO: give the bonus level in a more appropriate manner.
            // The following code adds "_stageKills" levels. This will also show in the char status.
            // I do not have enough info to know if the bonus should be shown in the pk count, or if it
            // should be a full "_stageKills" bonus or just the remaining from the current count till the of the current stage...
            // This code is a TEMP fix, so that the cursed weapon's bonus level can be observed with as little change in the code as possible, until proper info arises.
            equippedWeapon.setNbKills( equippedWeapon.getStageKills() - 1 )
            await equippedWeapon.increaseKills()

            // erase the newly obtained cursed weapon
            weapon.setPlayer( player ) // NECESSARY in order to find which inventory the weapon is in!
            return weapon.endOfLife() // expire the weapon and clean up.
        }

        return weapon.activate( player, item )
    }

    announce( packet: Buffer ): void {
        L2World.broadcastDataToOnlinePlayers( packet )
    }

    getCursedWeapon( itemId: number ): CursedWeapon {
        return DataManager.getCursedWeaponData().getCursedWeapons()[ itemId ]
    }

    getCursedWeaponIds(): Array<number> {
        return DataManager.getCursedWeaponData().getCursedWeaponIds()
    }

    getCursedWeapons() {
        return DataManager.getCursedWeaponData().getCursedWeapons()
    }

    getLevel( itemId: number ): number {
        let weapon: CursedWeapon = DataManager.getCursedWeaponData().getCursedWeapons()[ itemId ]
        return weapon.getLevel()
    }

    isCursedItem( itemId: number ): boolean {
        return !!DataManager.getCursedWeaponData().getCursedWeapons()[ itemId ]
    }

    async performDrop( attackable: L2Attackable, player: L2PcInstance ) : Promise<void> {
        if ( this.nextDropWeapon ) {
            await this.nextDropWeapon.performDrop( attackable, player )
        }

        this.nextDropWeapon = null
    }

    increaseKills( itemId: number ) : Promise<void> {
        return this.getCursedWeapon( itemId ).increaseKills()
    }

    dropWeapon( itemId: number, attacker: L2Character ) : Promise<void> {
        let weapon : CursedWeapon = this.getCursedWeapon( itemId )

        if ( weapon ) {
            return weapon.dropBy( attacker )
        }
    }

    isDropReady( character: L2Attackable ) : boolean {
        if ( !character || character.getInstanceId() !== 0 ) {
            return false
        }

        if ( noDropInstanceTypes.has( character.getInstanceType() ) ) {
            return false
        }

        this.nextDropWeapon = Object.values( this.getCursedWeapons() ).find( ( weapon: CursedWeapon ) : boolean => {
            return !weapon.isActive() && _.random( 100000 ) < weapon.dropRate
        } )

        return !!this.nextDropWeapon
    }
}

export const CursedWeaponManager = new Manager()