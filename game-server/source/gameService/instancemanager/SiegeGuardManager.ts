import { Castle } from '../models/entity/Castle'
import { DatabaseManager } from '../../database/manager'

export class SiegeGuardManager {
    castle: Castle

    constructor( castle: Castle ) {
        this.castle = castle
    }

    async removeMercs() {
        return DatabaseManager.getCastleSiegeGuards().removeHiredGuards( this.getCastle().getResidenceId() )
    }

    getCastle() {
        return this.castle
    }
}