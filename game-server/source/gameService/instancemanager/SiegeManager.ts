import { ConfigManager } from '../../config/ConfigManager'
import { TowerSpawn } from '../models/TowerSpawn'
import { Siege } from '../models/entity/Siege'
import { CastleManager } from './CastleManager'
import { Castle } from '../models/entity/Castle'
import { L2Object } from '../models/L2Object'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { Skill } from '../models/Skill'
import { SkillCache } from '../cache/SkillCache'
import { L2Clan } from '../models/L2Clan'
import { DatabaseManager } from '../../database/manager'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2CastleTrapUpgradeTableData } from '../../database/interface/CastleTrapUpgradeTableApi'
import { L2SiegeConfigurationTowerSpawn } from '../../config/interface/SiegeConfigurationApi'
import { Location } from '../models/Location'
import aigle from 'aigle'
import _ from 'lodash'

class Manager implements L2DataApi {
    controlTowers: { [ key: number ]: Array<TowerSpawn> } = {}
    flameTowers: { [ key: number ]: Array<TowerSpawn> } = {}

    async addSiegeSkills( player: L2PcInstance ): Promise<void> {
        let siegeSkills = SkillCache.getSiegeSkills( player.isNoble(), player.getClan().getCastleId() > 0 )
        await aigle.resolve( siegeSkills ).each( ( skill: Skill ) => {
            return player.addSkill( skill, false )
        } )
    }

    async checkIsRegistered( clan: L2Clan, residenceId: number ): Promise<boolean> {
        if ( !clan ) {
            return false
        }

        if ( clan.getCastleId() > 0 ) {
            return true
        }

        return DatabaseManager.getSiegeClans().isRegistered( clan.getId(), residenceId )
    }

    async checkIsRegisteredForAll( clan: L2Clan, residenceIds: Array<number> ) : Promise<boolean> {
        return DatabaseManager.getSiegeClans().hasRegistrationForAny( clan.getId(), residenceIds )
    }

    createTowerSpawn( ...towerData: Array<L2SiegeConfigurationTowerSpawn> ): Array<TowerSpawn> {
        return towerData.map( ( data: L2SiegeConfigurationTowerSpawn ) => {
            return new TowerSpawn( data.npcId, new Location( data.x, data.y, data.z ) )
        } )
    }

    getAttackerMaxClans(): number {
        return ConfigManager.siege.getAttackerMaxClans()
    }

    getAttackerRespawnDelay(): number {
        return ConfigManager.siege.getAttackerRespawn()
    }

    getBloodAllianceReward(): number {
        return ConfigManager.siege.getBloodAllianceReward()
    }

    getDefenderMaxClans(): number {
        return ConfigManager.siege.getDefenderMaxClans()
    }

    getFlagMaxCount(): number {
        return ConfigManager.siege.getMaxFlags()
    }

    getFlameTowers( castleId: number ): Array<TowerSpawn> {
        return this.flameTowers[ castleId ]
    }

    getSiege( x: number, y: number, z: number ): Siege {
        let outcome: Siege = null
        _.each( CastleManager.getCastles(), ( castle: Castle ) => {
            if ( castle.getSiege().checkIfInZone( x, y, z ) ) {
                outcome = castle.getSiege()
                return false
            }
        } )

        return outcome
    }

    getSiegeClanMinLevel(): number {
        return ConfigManager.siege.getClanMinLevel()
    }

    getSiegeFromObject( object: L2Object ): Siege {
        return this.getSiege( object.getX(), object.getY(), object.getZ() )
    }

    getSiegeLength(): number {
        return ConfigManager.siege.getSiegeLength()
    }

    getSieges(): Array<Siege> {
        return _.map( CastleManager.getCastles(), ( castle: Castle ) => {
            return castle.getSiege()
        } )
    }

    async load(): Promise<Array<string>> {

        this.controlTowers[ 1 ] = this.createTowerSpawn(
                ConfigManager.siege.getGludioControlTower1(),
                ConfigManager.siege.getGludioControlTower2(),
                ConfigManager.siege.getGludioControlTower3(),
        )

        this.flameTowers[ 1 ] = this.createTowerSpawn(
                ConfigManager.siege.getGludioFlameTower1(),
                ConfigManager.siege.getGludioFlameTower1(),
        )

        this.controlTowers[ 2 ] = this.createTowerSpawn(
                ConfigManager.siege.getDionControlTower1(),
                ConfigManager.siege.getDionControlTower2(),
                ConfigManager.siege.getDionControlTower3(),
        )

        this.flameTowers[ 2 ] = this.createTowerSpawn(
                ConfigManager.siege.getDionFlameTower1(),
                ConfigManager.siege.getDionFlameTower1(),
        )


        this.controlTowers[ 3 ] = this.createTowerSpawn(
                ConfigManager.siege.getGiranControlTower1(),
                ConfigManager.siege.getGiranControlTower2(),
                ConfigManager.siege.getGiranControlTower3(),
        )

        this.flameTowers[ 3 ] = this.createTowerSpawn(
                ConfigManager.siege.getGiranFlameTower1(),
                ConfigManager.siege.getGiranFlameTower1(),
        )

        this.controlTowers[ 4 ] = this.createTowerSpawn(
                ConfigManager.siege.getOrenControlTower1(),
                ConfigManager.siege.getOrenControlTower2(),
                ConfigManager.siege.getOrenControlTower3(),
        )

        this.flameTowers[ 4 ] = this.createTowerSpawn(
                ConfigManager.siege.getOrenFlameTower1(),
                ConfigManager.siege.getOrenFlameTower1(),
        )

        this.controlTowers[ 5 ] = this.createTowerSpawn(
                ConfigManager.siege.getAdenControlTower1(),
                ConfigManager.siege.getAdenControlTower2(),
                ConfigManager.siege.getAdenControlTower3(),
        )

        this.flameTowers[ 5 ] = this.createTowerSpawn(
                ConfigManager.siege.getAdenFlameTower1(),
                ConfigManager.siege.getAdenFlameTower1(),
        )

        this.controlTowers[ 6 ] = this.createTowerSpawn(
                ConfigManager.siege.getInnadrilControlTower1(),
                ConfigManager.siege.getInnadrilControlTower2(),
                ConfigManager.siege.getInnadrilControlTower3(),
        )

        this.flameTowers[ 6 ] = this.createTowerSpawn(
                ConfigManager.siege.getInnadrilFlameTower1(),
                ConfigManager.siege.getInnadrilFlameTower1(),
        )

        this.controlTowers[ 7 ] = this.createTowerSpawn(
                ConfigManager.siege.getGoddardControlTower1(),
                ConfigManager.siege.getGoddardControlTower2(),
                ConfigManager.siege.getGoddardControlTower3(),
        )

        this.flameTowers[ 7 ] = this.createTowerSpawn(
                ConfigManager.siege.getGoddardFlameTower1(),
                ConfigManager.siege.getGoddardFlameTower1(),
        )

        this.controlTowers[ 8 ] = this.createTowerSpawn(
                ConfigManager.siege.getRuneControlTower1(),
                ConfigManager.siege.getRuneControlTower2(),
                ConfigManager.siege.getRuneControlTower3(),
        )

        this.flameTowers[ 8 ] = this.createTowerSpawn(
                ConfigManager.siege.getRuneFlameTower1(),
                ConfigManager.siege.getRuneFlameTower1(),
        )

        this.controlTowers[ 9 ] = this.createTowerSpawn(
                ConfigManager.siege.getSchuttgartControlTower1(),
                ConfigManager.siege.getSchuttgartControlTower2(),
                ConfigManager.siege.getSchuttgartControlTower3(),
        )

        this.flameTowers[ 9 ] = this.createTowerSpawn(
                ConfigManager.siege.getSchuttgartFlameTower1(),
                ConfigManager.siege.getSchuttgartFlameTower1(),
        )

        let manager = this
        await aigle.resolve( CastleManager.getCastles() ).each( async ( castle: Castle ) => {
            if ( castle.getOwnerId() !== 0 ) {
                await manager.loadTrapUpgrade( castle.getResidenceId() )
            }
        } )

        return [
            `SiegeManager: loaded ${ _.size( this.flameTowers ) } flame towers.`,
            `SiegeManager: loaded ${ _.size( this.controlTowers ) } control towers.`,
        ]
    }

    async loadTrapUpgrade( residenceId: number ): Promise<void> {
        let data: Array<L2CastleTrapUpgradeTableData> = await DatabaseManager.getCastleTrapUpgrade().getAll( residenceId )
        let manager = this

        data.forEach( ( item: L2CastleTrapUpgradeTableData ) => {
            let tower: TowerSpawn = _.get( manager.flameTowers, [ residenceId, item.towerIndex ] )
            if ( tower ) {
                tower.setUpgradeLevel( item.level )
            }
        } )
    }

    async removeSiegeSkills( player: L2PcInstance ) : Promise<void> {
        let skills = SkillCache.getSiegeSkills( player.isNoble(), player.getClan().getCastleId() > 0 )
        await aigle.resolve( skills ).each( ( skill: Skill ) => {
            return player.removeSkill( skill )
        } )
    }
}

export const SiegeManager = new Manager()