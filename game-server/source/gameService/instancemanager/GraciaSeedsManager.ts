
const ENERGY_SEEDS = 'EnergySeeds'
const SOITYPE = 2
const SOATYPE = 3
const SODTYPE = 1

class Manager {
    state: number = 1
    tiatKilled: number = 0
    stateChangeDate: number = Date.now()

    getState() : number {
        return this.state
    }
}

export const GraciaSeedsManager = new Manager()