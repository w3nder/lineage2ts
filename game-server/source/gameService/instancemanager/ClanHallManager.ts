import { L2Clan } from '../models/L2Clan'
import { ClanHall } from '../models/entity/ClanHall'
import { AuctionableHall } from '../models/entity/clanhall/AuctionableHall'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { DatabaseManager } from '../../database/manager'
import { AuctionManager } from './AuctionManager'
import { ClanHallAuction } from '../models/auction/ClanHallAuction'
import { ClanHallTableData } from '../../database/interface/ClanhallTableApi'
import _, { DebouncedFunc } from 'lodash'
import aigle from 'aigle'
import { L2World } from '../L2World'
import { AreaType } from '../models/areas/AreaType'
import { ClanHallArea } from '../models/areas/type/ClanHall'
import { AreaCache } from '../cache/AreaCache'
import { ClanHallFunction } from '../models/clan/ClanHallFunction'

const enum ClanHallParameters {
    UpdateInterval = 30000
}

class Manager implements L2DataApi {
    claimedClanHalls: { [ key: number ]: AuctionableHall } = {}
    freeClanHalls: { [ key: number ]: AuctionableHall } = {}
    allClanHalls: { [ key: number ]: AuctionableHall } = {}

    updateTask: DebouncedFunc<any>
    updateIds : Set<number> = new Set<number>()
    lastOwnerUpdateTime: number = 0

    constructor() {
        this.updateTask = _.debounce( this.runUpdateTask.bind( this ), ClanHallParameters.UpdateInterval, {
            trailing: true,
            maxWait: ClanHallParameters.UpdateInterval * 2,
        } )
    }

    scheduleHallUpdate( id: number, forceUpdate: boolean = false ) : void {
        this.updateIds.add( id )
        this.updateTask()

        if ( forceUpdate ) {
            this.updateTask.flush()
        }
    }

    getAllClanHalls() : Array<AuctionableHall> {
        return Object.values( this.allClanHalls )
    }

    getClanHallById( id: number ): AuctionableHall {
        return this.allClanHalls[ id ]
    }

    getClanHallByOwner( clan: L2Clan ): AuctionableHall {
        return _.find( this.claimedClanHalls, ( hall: AuctionableHall ) : boolean => {
            return hall.getOwnerId() === clan.getId()
        } )
    }

    getNearbyClanHall( x: number, y: number ): ClanHall {
        let area = L2World.getNearestAreaType( x, y, AreaType.ClanHall ) as ClanHallArea
        if ( !area ) {
            return
        }

        return this.getClanHallById( area.getResidenceId() )
    }

    async load(): Promise<Array<string>> {
        let data: Array<ClanHallTableData> = await DatabaseManager.getClanhallTable().getAll()
        await aigle.resolve( data ).eachLimit( 10, async ( item ) : Promise<void> => {
            let hall = new AuctionableHall( AreaCache.getAreaByResidenceId( item.hallId, AreaType.ClanHall ) as ClanHallArea )

            hall.clanHallId = item.hallId
            hall.name = item.name
            hall.ownerId = item.ownerId
            hall.description = item.description

            hall.location = item.location
            hall.paidUntil = item.paidUntil
            hall.grade = item.grade
            hall.isPaidOff = item.isPaidOff

            hall.lease = item.leaseAmount

            await hall.initialize()

            this.allClanHalls[ hall.clanHallId ] = hall

            if ( hall.getOwnerId() > 0 ) {
                this.claimedClanHalls[ hall.clanHallId ] = hall
                return
            }

            this.freeClanHalls[ hall.clanHallId ] = hall
            let hallAuction: ClanHallAuction = AuctionManager.getAuction( hall.clanHallId )
            if ( hallAuction && hall.getLeaseAmount() > 0 ) {
                return AuctionManager.initNPC( hall.clanHallId )
            }
        } )

        return [
            `ClanHallManager : loaded total ${ _.size( this.allClanHalls ) } clan halls.`,
            `ClanHallManager : has ${ _.size( this.freeClanHalls ) } free clan halls.`,
            `ClanHallManager : has ${ _.size( this.claimedClanHalls ) } claimed clan halls.`,
        ]
    }

    setFree( clanHallId: number ) {
        let clanHall: AuctionableHall = this.claimedClanHalls[ clanHallId ]
        if ( !clanHall ) {
            return
        }

        this.freeClanHalls[ clanHallId ] = clanHall

        clanHall.free()
        delete this.claimedClanHalls[ clanHallId ]
        this.lastOwnerUpdateTime = Date.now()
    }

    setOwner( hallId: number, clan: L2Clan ): void {
        let hall: AuctionableHall = this.claimedClanHalls[ hallId ]
        if ( !hall ) {
            this.claimedClanHalls[ hallId ] = this.freeClanHalls[ hallId ]
            delete this.freeClanHalls[ hallId ]
        } else {
            hall.free()
        }

        this.claimedClanHalls[ hallId ].setOwner( clan )
        this.scheduleHallUpdate( hall.getId() )
        this.lastOwnerUpdateTime = Date.now()
    }

    async runUpdateTask() : Promise<void> {
        let clanHalls : Array<AuctionableHall> = _.compact( Array.from( this.updateIds ).map( ( id : number ) : AuctionableHall => {
            return this.allClanHalls[ id ]
        } ) )

        this.updateIds.clear()

        if ( clanHalls.length === 0 ) {
            return
        }

        await DatabaseManager.getClanhallTable().updateHalls( clanHalls )

        let hallFunctions : Array<ClanHallFunction> = _.flatMap( clanHalls, ( clanHall : ClanHall ) : Array<ClanHallFunction> => Object.values( clanHall.functions ) )
        return DatabaseManager.getClanhallFunctions().updateFunctions( hallFunctions )
    }

    getLastOwnerUpdateTime() : number {
        return this.lastOwnerUpdateTime
    }
}

export const ClanHallManager = new Manager()