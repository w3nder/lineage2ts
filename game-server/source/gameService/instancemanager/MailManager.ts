import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { DatabaseManager } from '../../database/manager'
import { MailMessage } from '../models/mail/MailMessage'
import { L2World } from '../L2World'
import { PacketDispatcher } from '../PacketDispatcher'
import { ExNoticePostArrived } from '../packets/send/ExNoticePostArrived'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { IDFactoryCache } from '../cache/IDFactoryCache'
import { ConfigManager } from '../../config/ConfigManager'
import { L2MessagesTableItem, L2MessagesTableItemStatus } from '../../database/interface/MessagesTableApi'
import _, { DebouncedFunc } from 'lodash'
import { ItemManagerCache } from '../cache/ItemManagerCache'
import { ServerLog } from '../../logger/Logger'
import { RoaringBitmap32 } from 'roaring'
import { CharacterNamesCache } from '../cache/CharacterNamesCache'
import { MsInHours, MsInMinutes } from '../enums/MsFromTime'

type AllMessages = { [ messageId: number ]: MailMessage }

interface Outbox {
    messages: Array<MailMessage>
    allIds: Set<number>
}

interface Inbox extends Outbox {
    unreadIds: Set<number>
}

class Manager {
    messages: AllMessages = {}
    inboxes : Record<number, Inbox> = {}
    outboxes: Record<number, Outbox> = {}
    messagesToUpdate: RoaringBitmap32 = new RoaringBitmap32()
    deletedMessages: Set<MailMessage> = new Set<MailMessage>()

    deleteExpiredMessages : NodeJS.Timeout

    debounceMessageSave: DebouncedFunc<() => void>
    debounceMessageDeletion: DebouncedFunc<() => void>

    constructor() {
        this.debounceMessageSave = _.debounce( this.runMessageSave.bind( this ), 15000, {
            maxWait: 30000
        } )

        this.debounceMessageDeletion = _.debounce( this.runMessageDeletion.bind( this ), 10000, {
            maxWait: 20000
        } )

        this.deleteExpiredMessages = setInterval( this.runDeleteExpiredMessages.bind( this ), MsInMinutes.Thirty )
    }

    async loadPlayer( objectId: number ) : Promise<void> {
        let items = ConfigManager.general.allowMail() ? await DatabaseManager.getMessages().getMessages( objectId ) : []

        let inbox : Inbox = {
            messages: [],
            unreadIds: new Set<number>(),
            allIds: new Set<number>()
        }

        let outbox : Outbox = {
            messages: [],
            allIds: new Set<number>()
        }

        let playerIds : Set<number> = new Set<number>()

        for ( const item of items ) {
            const message = this.convertToMailMessage( item )

            if ( message.getReceiverId() === objectId ) {
                inbox.messages.push( message )

                if ( message.getStatus() === L2MessagesTableItemStatus.Unread ) {
                    inbox.unreadIds.add( message.getId() )
                }

                inbox.allIds.add( message.getId() )
            }

            if ( message.getSenderId() === objectId ) {
                outbox.messages.push( message )
                outbox.allIds.add( message.getId() )
            }

            this.messages[ message.getId() ] = message

            playerIds.add( message.getReceiverId() )
            playerIds.add( message.getSenderId() )

            if ( message.hasAttachments() ) {
                await message.loadAttachments()
            }
        }

        this.inboxes[ objectId ] = inbox
        this.outboxes[ objectId ] = outbox

        return CharacterNamesCache.prefetchNames( Array.from( playerIds ) )
    }

    private convertToMailMessage( item: L2MessagesTableItem ) : MailMessage {
        return this.getMessage( item.messageId ) ?? new MailMessage( item )
    }

    unloadPlayer( objectId: number ) : Promise<unknown> {
        let promises : Array<Promise<unknown>> = []

        this.unloadMailboxMessages( this.inboxes[ objectId ], promises )
        this.unloadMailboxMessages( this.outboxes[ objectId ], promises )

        delete this.inboxes[ objectId ]
        delete this.outboxes[ objectId ]

        return Promise.all( promises )
    }

    async runMessageSave() : Promise<void> {
        let messages : Array<L2MessagesTableItem> = []

        for ( const messageId of this.messagesToUpdate ) {
            let message = this.messages[ messageId ]
            if ( message ) {
                messages.push( message.data )

                /*
                    We need to check if message is left to be updated and is still being used
                    by any mailboxes. In case where no mailboxes exist, we can safely save and
                    then remove such message from id registry.
                 */
                if ( this.isUsedByMailboxes( message ) ) {
                    this.routeMessageToInbox( message )
                } else {
                    delete this.messages[ messageId ]
                }
            }
        }

        this.messagesToUpdate.clear()

        if ( messages.length === 0 ) {
            return
        }

        await DatabaseManager.getMessages().updateMessages( messages )
        ServerLog.info( `MailManager: updated ${messages.length} messages` )
    }


    hasUnreadMessages( objectId: number ): boolean {
        return this.inboxes[ objectId ].unreadIds.size > 0
    }

    getOutboxSize( objectId: number ): number {
        return this.outboxes[ objectId ].messages.length
    }

    getInboxSize( objectId: number ): number {
        return this.inboxes[ objectId ].messages.length
    }

    /*
        TODO : check if player is able to send message since there exists SystemMessageIds.NO_MORE_MESSAGES_TODAY
        which limits player to only ten messages in last 24 hours.
        However, the limit is artificial and is questionable since there are other ways to prevent message spam.
     */
    sendMessage( message: MailMessage ): Promise<void> {
        this.addMessage( message )
        this.routeMessageToOutbox( message )

        let attachments = message.getLoadedAttachments()
        if ( attachments && attachments.getItems().length > 0 ) {
            for ( const item of attachments.getItems() ) {
                // TODO : ensure way to check if items have been saved to database
                ItemManagerCache.scheduleItemUpdate( item )
            }
        }

        return CharacterNamesCache.prefetchNames( [ message.getSenderId(), message.getReceiverId() ] )
    }

    private addMessage( message: MailMessage ) : void {
        this.messages[ message.getId() ] = message
        this.scheduleMessageUpdate( message.getId() )
    }

    private routeMessageToOutbox( message: MailMessage ) : void {
        let outbox = this.outboxes[ message.getSenderId() ]
        if ( !outbox || outbox.allIds.has( message.getId() ) || message.isReturned() ) {
            return
        }

        outbox.messages.push( message )
        outbox.allIds.add( message.getId() )
    }

    private routeMessageToInbox( message: MailMessage ) : void {
        let inbox = this.inboxes[ message.getReceiverId() ]
        if ( !inbox || inbox.allIds.has( message.getId() ) ) {
            return
        }

        inbox.messages.push( message )
        inbox.allIds.add( message.getId() )
        inbox.unreadIds.add( message.getId() )

        let player = L2World.getObjectById( message.getReceiverId() )
        if ( player ) {
            PacketDispatcher.sendOwnedData( message.getReceiverId(), ExNoticePostArrived( true ) )
        }
    }

    async runMessageDeletion() : Promise<void> {
        let messageIds : Array<number> = []

        this.deletedMessages.forEach( message => {
            messageIds.push( message.getId() )
            IDFactoryCache.releaseId( message.getId() )
        } )

        this.deletedMessages.clear()

        await DatabaseManager.getMessages().deleteMessages( messageIds )
        ServerLog.info( `MailManager: deleted ${ messageIds.length} messages` )
    }

    getInbox( objectId: number ): Array<MailMessage> {
        return this.inboxes[ objectId ]?.messages ?? []
    }

    getMessage( messageId: number ): MailMessage {
        return this.messages[ messageId ]
    }

    getOutbox( objectId: number ): Array<MailMessage> {
        return this.outboxes[ objectId ]?.messages ?? []
    }

    /*
        Each message id can be used for attachments, which are stored with id of owner (message id),
        hence replacing such id with simpler variant can conflict with existing player ids.
     */
    getNextId(): number {
        return IDFactoryCache.getNextId()
    }

    setMessageStatus( messageId : number, status: L2MessagesTableItemStatus ) : void {
        let message = this.messages[ messageId ]
        if ( !message ) {
            return
        }

        message.data.status = status

        this.scheduleMessageUpdate( messageId )
    }

    /*
        When message is deleted it must not be made accessed again, even if
        actual message deletion operations can be delayed. Hence, all messages
        are accumulated and eventually erased in database.

        Note that message with attachments must not be deleted. Either message must
        be returned to sender, otherwise attachments must claimed by player or returned to warehouse.
     */
    setDeleted( messageId: number ) : void {
        let message = this.getMessage( messageId )
        if ( !message || message.hasAttachments() ) {
            return
        }

        return this.scheduleDeletion( message )
    }

    private scheduleDeletion( message: MailMessage ) : void {
        this.messagesToUpdate.remove( message.getId() )
        this.deletedMessages.add( message )
        delete this.messages[ message.getId() ]

        this.removeMessageLinks( message )
        this.debounceMessageDeletion()
    }

    /*
        While messages can have attachments, all items must be transferred to another location
        before message can be declared attachments free. In such case, care must be taken not to
        erase any items stored as attachments.
     */
    removeAttachments( messageId: number ) : void {
        let message = this.getMessage( messageId )

        if ( message.getLoadedAttachments() && message.getLoadedAttachments().getSize() !== 0 ) {
            return
        }

        message.attachments = null
        message.data.hasAttachments = false

        this.scheduleMessageUpdate( messageId )
    }

    private scheduleMessageUpdate( messageId: number ) : void {
        this.messagesToUpdate.add( messageId )
        this.debounceMessageSave()
    }

    private removeMessageLinks( message: MailMessage ) : void {
        let inbox = this.inboxes[ message.getReceiverId() ]
        if ( inbox && inbox.allIds.has( message.getId() ) ) {
            inbox.allIds.delete( message.getId() )
            inbox.messages = inbox.messages.filter( existingMessage => existingMessage.getId() !== message.getId() )
        }

        let outbox = this.outboxes[ message.getSenderId() ]
        if ( outbox && outbox.allIds.has( message.getId() ) ) {
            outbox.allIds.delete( message.getId() )
            outbox.messages = inbox.messages.filter( existingMessage => existingMessage.getId() !== message.getId() )
        }
    }

    private async runDeleteExpiredMessages() : Promise<void> {
        let currentTime = Date.now()
        await DatabaseManager.getMessages().deleteReadMessagesOlderThan( currentTime )

        let codItems = await DatabaseManager.getMessages().getCodMessagesOlderThan( currentTime )
        for ( const codItem of codItems ) {
            if ( !codItem.hasAttachments ) {
                continue
            }

            let message = this.convertToMailMessage( codItem )
            await this.returnAttachmentsToWarehouse( message )
            this.scheduleDeletion( message )
        }

        let unreadItems = await DatabaseManager.getMessages().getUnreadMessagesOlderThan( currentTime )
        for ( const unreadItem of unreadItems ) {
            let message = this.convertToMailMessage( unreadItem )
            let returnedMessage = MailMessage.asReturned( message )

            await this.sendMessage( returnedMessage )
            this.scheduleDeletion( message )
        }
    }

    private async returnAttachmentsToWarehouse( message: MailMessage ) : Promise<void> {
        if ( !message.hasAttachments() ) {
            return
        }

        await message.loadAttachments()
        let attachments = message.getLoadedAttachments()

        if ( attachments.getSize() > 0 ) {
            let sender: L2PcInstance = L2World.getPlayer( message.getSenderId() )
            if ( sender ) {
                await sender.initializeWarehouse()
                await attachments.returnToWarehouse( sender.getWarehouse() )
                sender.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MAIL_RETURNED ) )
            } else {
                await attachments.returnToWarehouse( null )
            }

            await attachments.deleteMe()

            let receiver: L2PcInstance = L2World.getPlayer( message.getReceiverId() )
            if ( receiver ) {
                receiver.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.MAIL_RETURNED ) )
            }
        }

        message.attachments = null
        message.data.hasAttachments = false
    }

    private isUsedByMailboxes( message : MailMessage ) : boolean {
        return !!this.outboxes[ message.getSenderId() ] || !!this.inboxes[ message.getReceiverId() ]
    }

    private unloadMailboxMessages( mailbox: Outbox, promises : Array<Promise<unknown>> ) : void {
        if ( !mailbox ) {
            return
        }

        for ( const message of mailbox.messages ) {
            if ( this.isUsedByMailboxes( message ) ) {
                continue
            }

            if ( !this.messagesToUpdate.has( message.getId() ) ) {
                delete this.messages[ message.getId() ]
            }

            if ( message.hasAttachments() && message.getLoadedAttachments() ) {
                promises.push( message.getLoadedAttachments().deleteMe() )
            }
        }

        mailbox.messages.length = 0
        mailbox.allIds.clear()
    }

    getDefaultExpirationTime() : number {
        return Math.max( ConfigManager.general.getMailDefaultExpirationTime(), MsInHours.One )
    }

    getCodExpirationTime() : number {
        return Math.max( ConfigManager.general.getMailCodExpirationTime(), MsInHours.One )
    }

    removeCOD( message: MailMessage ) : void {
        message.data.codAdena = 0
        this.scheduleMessageUpdate( message.getId() )
    }
}

export const MailManager = new Manager()