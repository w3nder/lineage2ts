import { L2PcInstance } from './models/actor/instance/L2PcInstance'
import { L2Object } from './models/L2Object'
import { L2PetInstance } from './models/actor/instance/L2PetInstance'
import { L2WorldRegion } from './models/L2WorldRegion'
import { ILocational } from './models/Location'
import { PacketDispatcher } from './PacketDispatcher'
import {
    ISpatialBoundingBox,
    ISpatialIndexArea,
    ISpatialIndexData,
    ISpatialIndexRegion,
    SpatialIndexHelper
} from './models/SpatialIndexData'
import { L2DataApi } from '../data/interface/l2DataApi'
import { L2Character } from './models/actor/L2Character'
import _ from 'lodash'
import { L2Npc } from './models/actor/L2Npc'
import logSymbols from 'log-symbols'
import RBush from 'rbush'
import knn from 'rbush-knn'
import { L2Region } from './enums/L2Region'
import { L2MapTile, L2WorldLimits } from './enums/L2MapTile'
import { L2WorldArea } from './models/areas/WorldArea'
import { AreaType } from './models/areas/AreaType'
import { TownArea } from './models/areas/type/Town'
import { ServerLog } from '../logger/Logger'

const RegionsPerX = Math.floor( ( L2WorldLimits.MaxX - L2WorldLimits.MinX ) / L2Region.Length )
const RegionsPerY = Math.floor( ( L2WorldLimits.MaxY - L2WorldLimits.MinY ) / L2Region.Length )
const RegionXIndex = Math.abs( Math.floor( L2WorldLimits.MinX / L2Region.Length ) )
const RegionYIndex = Math.abs( Math.floor( L2WorldLimits.MinY / L2Region.Length ) )

const enum L2WorldSearchValues {
    maxZDifference = 1000,
}

const defaultEmptyResults = Object.freeze( [] )

class World implements L2DataApi {
    allPlayers: { [ key: number ]: L2PcInstance } = {}
    allObjects: { [ key: number ]: L2Object } = {}
    petsInstances: { [ key: number ]: L2PetInstance } = {}
    allRegions: Array<L2WorldRegion> = []
    allAreasGrid: RBush<ISpatialIndexArea> = new RBush<ISpatialIndexArea>()
    allRegionsGrid: RBush<ISpatialIndexRegion> = new RBush<ISpatialIndexRegion>()
    objectGrids: { [ key: number ]: RBush<ISpatialIndexData> } = {}
    playerGrids: { [ key: number ]: RBush<ISpatialIndexData> } = {}

    constructor() {
        this.createRegions()
        this.createInstanceGrid( 0 )
    }

    addPet( ownerId: number, pet: L2PetInstance ): L2PetInstance {
        this.petsInstances[ ownerId ] = pet
        return pet
    }

    addPlayer( player: L2PcInstance ) {
        this.allPlayers[ player.objectId ] = player
    }

    createInstanceGrid( instanceId: number ): void {
        if ( this.objectGrids[ instanceId ] || this.playerGrids[ instanceId ] ) {
            ServerLog.error( 'Cannot re-create instance grids for id=%d', instanceId )
            return
        }

        this.objectGrids[ instanceId ] = new RBush()
        this.playerGrids[ instanceId ] = new RBush()
    }

    destroyInstanceGrid( instanceId: number ): void {
        if ( !this.objectGrids[ instanceId ] || !this.playerGrids[ instanceId ] ) {
            ServerLog.error( 'Cannot destroy instance grids for id=%d', instanceId )
            return
        }

        this.objectGrids[ instanceId ].clear()
        this.playerGrids[ instanceId ].clear()

        delete this.objectGrids[ instanceId ]
        delete this.playerGrids[ instanceId ]
    }

    broadcastDataToOnlinePlayers( packet: Buffer ) {
        _.each( this.allPlayers, ( player: L2PcInstance ) => {
            if ( player && player.isOnline() ) {
                PacketDispatcher.sendCopyData( player.getObjectId(), packet )
            }
        } )
    }

    /*
        Order of X vs Y matters as we fill up array of all regions. Inner loop would matter as to how we would compute an index of region
        inside continuous array.
     */
    createRegions() {
        _.times( RegionsPerY, ( widthIndex: number ) => {
            _.times( RegionsPerX, ( heightIndex: number ) => {
                let minX = ( widthIndex - RegionXIndex ) * L2Region.Length
                let maxX = ( ( widthIndex + 1 ) - RegionXIndex ) * L2Region.Length
                let minY = ( heightIndex - RegionYIndex ) * L2Region.Length
                let maxY = ( ( heightIndex + 1 ) - RegionYIndex ) * L2Region.Length
                let tileX = Math.floor( widthIndex / L2Region.RegionsPerTile ) + L2MapTile.XMin
                let tileY = Math.floor( heightIndex / L2Region.RegionsPerTile ) + L2MapTile.YMin

                this.allRegions.push( new L2WorldRegion( tileX, tileY, minX, maxX, minY, maxY ) )
            } )
        } )

        this.allRegionsGrid.load( this.allRegions.map( region => region.geometry ) )

        this.allRegions.forEach( ( currentRegion : L2WorldRegion ) => {
            let box = SpatialIndexHelper.getPlainBox( currentRegion.geometry.minX - L2Region.SearchOffset,
                    currentRegion.geometry.minY - L2Region.SearchOffset,
                    currentRegion.geometry.maxX + L2Region.SearchOffset,
                    currentRegion.geometry.maxY + L2Region.SearchOffset )
            currentRegion.surroundingRegions = this.allRegionsGrid.search( box ).reduce( ( regions: Array<L2WorldRegion>, index: ISpatialIndexRegion ) => {
                if ( index.region !== currentRegion ) {
                    regions.push( index.region )
                }

                return regions
            }, [] )

            SpatialIndexHelper.returnBox( box )

            if ( currentRegion.surroundingRegions.length > 8 || currentRegion.surroundingRegions.length < 3 ) {
                throw new Error( 'Incorrect amount of surrounding regions' )
            }
        } )
    }

    getRegionsWithRadius( x: number, y: number, radius: number = L2Region.SearchOffset ) : Array<L2WorldRegion> {
        let box = SpatialIndexHelper.getBox( x, y, radius )
        let regions : Array<L2WorldRegion> = this.allRegionsGrid.search( box ).map( index => index.region )
        SpatialIndexHelper.returnBox( box )

        return regions
    }

    getAllPlayers() {
        return this.allPlayers
    }

    getObjectById( objectId: number ): L2Object {
        return this.allObjects[ objectId ]
    }

    getPet( ownerId: number ): L2PetInstance {
        return this.petsInstances[ ownerId ]
    }

    getPlayer( objectId: number ): L2PcInstance {
        return this.allPlayers[ objectId ]
    }

    // TODO : replace by CharacterNameCache since those names will be persisted through login/logout flow
    getPlayerByName( name: string ): L2PcInstance {
        return _.find( this.allPlayers, ( player: L2PcInstance ) => {
            return player.getName() === name
        } )
    }

    /*
        Working with continuous array of regions:
        - X value is page of items
        - Y value is index inside page
        - hence addition of computed indexes
     */
    getRegion( x: number, y: number ): L2WorldRegion {
        let xValue = Math.floor( ( x - L2WorldLimits.MinX ) / L2Region.Length ) * RegionsPerX
        let yValue = Math.floor( ( y - L2WorldLimits.MinY ) / L2Region.Length )
        return this.allRegions[ xValue + yValue ]
    }

    getVisibleObjects( object: L2Object, radius: number ): Array<L2Object> {
        if ( !object || !object.isVisible() ) {
            return defaultEmptyResults as Array<L2Object>
        }

        return this.getObjectGridItems( object, radius ).map( ( data: ISpatialIndexData ) => {
            return L2World.getObjectById( data.objectId )
        } )
    }

    getVisibleObjectIds( object: L2Object, radius: number ): Array<number> {
        if ( !object || !object.isVisible() ) {
            return defaultEmptyResults as Array<number>
        }

        return this.getObjectGridItems( object, radius ).map( data => data.objectId )
    }

    getVisiblePlayers( object: L2Object, radius: number, includeOriginator: boolean = false ): Array<L2PcInstance> {
        let items: Array<ISpatialIndexData> = this.getPlayerGridItems( object, radius )

        return _.reduce( items, ( finalItems: Array<L2PcInstance>, data: ISpatialIndexData ) => {
            let player = L2World.getPlayer( data.objectId )
            if ( ( includeOriginator || ( !includeOriginator && object.getObjectId() !== data.objectId ) )
                    && player
                    && Math.abs( player.getZ() - object.getZ() ) <= L2WorldSearchValues.maxZDifference ) {
                finalItems.push( player )
            }

            return finalItems
        }, [] )
    }

    getAllVisiblePlayers( object: ILocational, radius: number ): Array<L2PcInstance> {
        return _.map( this.getPlayerGridItems( object, radius ), ( data: ISpatialIndexData ): L2PcInstance => {
            return L2World.getPlayer( data.objectId )
        } )
    }

    getAllVisiblePlayerIds( object: ILocational, radius: number ): Array<number> {
        return this.getPlayerGridItems( object, radius ).map( data => data.objectId )
    }

    getPlayerGridItems( object: ILocational, radius: number ): Array<ISpatialIndexData> {
        let boundingBox = SpatialIndexHelper.getBoundingBox( object, radius )
        let items: Array<ISpatialIndexData> = this.playerGrids[ object.getInstanceId() ].search( boundingBox )

        SpatialIndexHelper.returnBox( boundingBox )
        boundingBox = null

        return items
    }

    getObjectGridItems( object: ILocational, radius: number ): Array<ISpatialIndexData> {
        let boundingBox = SpatialIndexHelper.getBoundingBox( object, radius )
        let items: Array<ISpatialIndexData> = this.objectGrids[ object.getInstanceId() ].search( boundingBox )

        SpatialIndexHelper.returnBox( boundingBox )
        boundingBox = null

        return items
    }

    getVisibleCharacters( object: L2Object, radius: number, includeOriginator: boolean = false ): Array<L2Character> {
        const predicate = ( character: L2Object ) => {
            return character.isCharacter()
        }

        return L2World.getVisibleObjectsByPredicate( object, radius, predicate, includeOriginator ) as Array<L2Character>
    }

    getVisibleNpcs( object: L2Object, radius: number, includeOriginator: boolean = false ): Array<L2Object> {
        const predicate = ( character: L2Object ) => {
            return character.isNpc()
        }

        return L2World.getVisibleObjectsByPredicate( object, radius, predicate, includeOriginator ) as Array<L2Npc>
    }

    getVisibleObjectsByPredicate( object: L2Object, radius: number, predicateMethod: ( object: L2Object ) => boolean, includeOriginator: boolean = false ): Array<L2Object> {
        if ( !object || !object.isVisible() ) {
            return defaultEmptyResults as Array<L2Object>
        }

        return this.getObjectGridItems( object, radius ).reduce( ( characters: Array<L2Object>, data: ISpatialIndexData ) => {
            if ( !includeOriginator && data.objectId === object.getObjectId() ) {
                return characters
            }

            let character = L2World.getObjectById( data.objectId )

            if ( character && predicateMethod( character ) ) {
                characters.push( character )
            }

            return characters
        }, [] )
    }

    getVisiblePlayersByPredicate( object: L2Object, radius: number, predicateMethod: ( player: L2PcInstance ) => boolean, includeOriginator: boolean = false ): Array<L2PcInstance> {
        if ( !object || !object.isVisible() ) {
            return defaultEmptyResults as Array<L2PcInstance>
        }

        return this.getPlayerGridItems( object, radius ).reduce( ( characters: Array<L2PcInstance>, data: ISpatialIndexData ) => {
            if ( !includeOriginator && data.objectId === object.getObjectId() ) {
                return characters
            }

            let character = L2World.getPlayer( data.objectId )

            if ( character && predicateMethod( character ) ) {
                characters.push( character )
            }

            return characters
        }, [] )
    }

    getVisiblePlayerIdsByPredicate( object: L2Object, radius: number, predicateMethod: ( playerId: number ) => boolean, includeOriginator: boolean = false ): Array<number> {
        if ( !object || !object.isVisible() ) {
            return defaultEmptyResults as Array<number>
        }

        return this.getPlayerGridItems( object, radius ).reduce( ( characters: Array<number>, data: ISpatialIndexData ) => {
            if ( !includeOriginator && data.objectId === object.getObjectId() ) {
                return characters
            }

            if ( predicateMethod( data.objectId ) ) {
                characters.push( data.objectId )
            }

            return characters
        }, [] )
    }

    getVisibleObjectIdsByPredicate( object: L2Object, radius: number, predicateMethod: ( objectId: number ) => boolean, includeOriginator: boolean = false ): Array<number> {
        if ( !object || !object.isVisible() ) {
            return defaultEmptyResults as Array<number>
        }

        return this.getObjectGridItems( object, radius ).reduce( ( objectIds: Array<number>, data: ISpatialIndexData ) => {
            if ( !includeOriginator && data.objectId === object.getObjectId() ) {
                return objectIds
            }

            if ( predicateMethod( data.objectId ) ) {
                objectIds.push( data.objectId )
            }

            return objectIds
        }, [] )
    }

    getFirstObjectByPredicate( object: L2Object, radius: number, predicateMethod: ( object: L2Object ) => boolean, includeOriginator: boolean = false ): L2Object {
        if ( !object || !object.isVisible() ) {
            return null
        }

        let outcome: ISpatialIndexData = this.getObjectGridItems( object, radius ).find( ( data: ISpatialIndexData ) => {
            let object = L2World.getObjectById( data.objectId )

            if ( !object || ( !includeOriginator && data.objectId === object.getObjectId() ) ) {
                return false
            }

            return predicateMethod( object )
        } )

        return outcome ? L2World.getObjectById( outcome.objectId ) : null
    }

    getEnteredAreas( object: L2Object ): Array<L2WorldArea> {
        return this.getAreasWithCoordinates( object.getX(), object.getY(), 5 ).filter( area => area.isObjectInside( object ) )
    }

    getAreasWithCoordinates( x: number, y: number, radius: number = 5 ): Array<L2WorldArea> {
        let intersectionBox = SpatialIndexHelper.getBox( x, y, radius )
        let items: Array<ISpatialIndexArea> = this.allAreasGrid.search( intersectionBox )

        SpatialIndexHelper.returnBox( intersectionBox )
        intersectionBox = null

        return items.map( currentItem => currentItem.area )
    }

    getAreasWithBoundingBox( minX: number, minY: number, maxX: number, maxY: number ): Array<L2WorldArea> {
        let intersectionBox = SpatialIndexHelper.getPlainBoundingBox( minX, minY, maxX, maxY )
        let items: Array<ISpatialIndexArea> = L2World.allAreasGrid.search( intersectionBox )

        SpatialIndexHelper.returnBox( intersectionBox )
        intersectionBox = null

        return items.map( currentItem => currentItem.area )
    }

    getObjectIdsByRegion( region: L2WorldRegion, instanceId: number = 0 ) : Array<number> {
        return this.objectGrids[ instanceId ].search( region.geometry ).map( data => data.objectId )
    }

    forObjectsInRegionWithPredicate( region: L2WorldRegion, instanceId: number = 0, predicate: ( data: L2Object ) => void ) : void {
        return this.objectGrids[ instanceId ].search( region.geometry ).forEach( ( data ) : void => {
            return predicate( this.getObjectById( data.objectId ) )
        } )
    }

    removeObject( object: L2Object ) {
        let region = object.getWorldRegion()
        if ( region ) {
            region.removeVisibleObject( object )
        }

        delete this.allObjects[ object.getObjectId() ]
        this.objectGrids[ object.getInstanceId() ].remove( object.getSpatialIndex() )
    }

    removePet( ownerId: number ): void {
        delete this.petsInstances[ ownerId ]
    }

    removePlayer( player: L2PcInstance ) {
        this.playerGrids[ player.getInstanceId() ].remove( player.getSpatialIndex() )
        delete this.allPlayers[ player.getObjectId() ]
    }

    storeObject( object: L2Object ): void {
        if ( !this.allObjects[ object.objectId ] ) {
            this.allObjects[ object.objectId ] = object
            return
        }

        ServerLog.fatal( 'Cannot store object for existing objectId = %d', object.objectId )
    }

    addAreas( areas: Array<ISpatialIndexArea> ): void {
        this.allAreasGrid.load( areas )
    }

    addArea( area: L2WorldArea ) : void {
        this.allAreasGrid.insert( area.getSpatialIndex() )
    }

    removeArea( area: L2WorldArea ) : void {
        this.allAreasGrid.remove( area.getSpatialIndex() )
    }

    removeSpatialIndex( object: L2Object ): void {
        if ( object.isPlayer() ) {
            this.playerGrids[ object.getInstanceId() ].remove( object.getSpatialIndex() )
        }

        this.objectGrids[ object.getInstanceId() ].remove( object.getSpatialIndex() )
    }

    addSpatialIndex( object: L2Object ): void {
        if ( object.isPlayer() ) {
            this.playerGrids[ object.getInstanceId() ].insert( object.getSpatialIndex() )
        }

        this.objectGrids[ object.getInstanceId() ].insert( object.getSpatialIndex() )
    }

    async load(): Promise<Array<string>> {
        return [
            `${ logSymbols.info} L2World : mapped ${ this.allRegions.length } regions`,
        ]
    }

    getNearestTowns( location: ILocational, amount: number ): Array<TownArea> {
        return this.getNearestAreasByPredicate( location, amount, ( data: ISpatialIndexArea ): boolean => {
            return data.area.type === AreaType.Town && ( data.area as TownArea ).properties.taxById > 0
        } ) as Array<TownArea>
    }

    getNearestAreasByPredicate( location: ILocational, amount: number, predicate: ( data: ISpatialIndexArea ) => boolean ): Array<L2WorldArea> {
        let items: Array<ISpatialIndexArea> = knn( this.allAreasGrid, location.getX(), location.getY(), amount, predicate )
        return items.map( currentItem => currentItem.area )
    }

    getNearestAreaType( x: number, y: number, type: AreaType ): L2WorldArea {
        let items: Array<ISpatialIndexArea> = knn( this.allAreasGrid, x, y, 1, ( data: ISpatialIndexArea ) : boolean => {
            return data.area.type === type
        } )

        if ( items.length > 0 ) {
            return items[ 0 ].area
        }

        return null
    }

    getNearestObjects( location: ILocational, amount: number, predicate: ( object: L2Object ) => boolean ): Array<L2Object> {
        let items: Array<ISpatialIndexData> = knn( this.objectGrids[ location.getInstanceId() ], location.getX(), location.getY(), amount, ( data: ISpatialIndexData ): boolean => {
            let object = L2World.getObjectById( data.objectId )
            return predicate( object )
        } )

        return items.map( ( data: ISpatialIndexData ) => {
            return L2World.getObjectById( data.objectId )
        } )
    }

    getNearestObject( location: ILocational, distance: number, predicate: ( object: L2Object ) => boolean ): L2Object {
        let items: Array<ISpatialIndexData> = knn( this.objectGrids[ location.getInstanceId() ], location.getX(), location.getY(), 1, ( data: ISpatialIndexData ): boolean => {
            let object = L2World.getObjectById( data.objectId )
            return predicate( object )
        }, distance )

        if ( items.length ) {
            return L2World.getObjectById( items[ 0 ].objectId )
        }

        return
    }

    getNearestPlayer( location: ILocational, distance: number, predicate: ( object: L2Object ) => boolean ): L2PcInstance {
        let items: Array<ISpatialIndexData> = knn( this.playerGrids[ location.getInstanceId() ], location.getX(), location.getY(), 1, ( data: ISpatialIndexData ): boolean => {
            let player = L2World.getPlayer( data.objectId )
            if ( !player ) {
                return false
            }

            return predicate( player )
        }, distance )

        if ( items.length ) {
            return L2World.getPlayer( items[ 0 ].objectId )
        }

        return
    }

    /*
        Slightly different version of normal getVisibleObjectsByPredicate since it allows termination of iteration by return value of predicate
     */
    forEachObjectByPredicate( object: L2Object, radius: number, predicateMethod: ( object: L2Object ) => void | boolean, includeOriginator: boolean = false ): void {
        if ( !object ) {
            return
        }

        _.each( this.getObjectGridItems( object, radius ), ( data: ISpatialIndexData ) => {
            if ( !includeOriginator && data.objectId === object.getObjectId() ) {
                return
            }

            let character = L2World.getObjectById( data.objectId )

            if ( !character ) {
                return
            }

            return predicateMethod( character )
        } )
    }

    forEachPlayerByPredicate( object: L2Object, radius: number, predicateMethod: ( player: L2PcInstance ) => void | boolean, includeOriginator: boolean = false ): void {
        if ( !object ) {
            return
        }

        _.each( this.getPlayerGridItems( object, radius ), ( data: ISpatialIndexData ) => {
            if ( !includeOriginator && data.objectId === object.getObjectId() ) {
                return
            }

            let player = L2World.getPlayer( data.objectId )

            if ( !player ) {
                return
            }

            return predicateMethod( player )
        } )
    }

    getSize(): number {
        return _.size( this.allObjects )
    }

    isInAreaRange( object: ILocational, range: number, type: AreaType ) : boolean {
        return this.getAreasWithCoordinates( object.getX(), object.getY(), range ).some( ( area: L2WorldArea ) => {
            return area.type === type && area.isObjectInside( object )
        } )
    }

    forPlayersByBox( box: ISpatialBoundingBox, predicateMethod: ( player : L2PcInstance ) => void, instanceId : number = 0 ) : void {
        return this.playerGrids[ instanceId ].search( box ).forEach( ( data : ISpatialIndexData ) => {
            let player = this.getPlayer( data.objectId )
            if ( player ) {
                predicateMethod( player )
            }
        } )
    }

    forObjectsByBox( box: ISpatialBoundingBox, predicateMethod: ( object : L2Object ) => void, instanceId: number = 0 ) : void {
        return this.objectGrids[ instanceId ].search( box ).forEach( ( data: ISpatialIndexData ) => {
            let object = this.getObjectById( data.objectId )
            if ( object ) {
                predicateMethod( object )
            }
        } )
    }

    getPlayerIdsByBox( box: ISpatialBoundingBox, instanceId: number = 0 ) : Array<number> {
        return this.playerGrids[ instanceId ].search( box ).map( ( data: ISpatialIndexData ) : number => data.objectId )
    }

    getPlayersByBox( box: ISpatialBoundingBox, instanceId: number = 0 ) : Array<L2PcInstance> {
        return this.playerGrids[ instanceId ].search( box ).reduce( ( allPlayers: Array<L2PcInstance>, data: ISpatialIndexData ) : Array<L2PcInstance> => {
            let player = this.getPlayer( data.objectId )
            if ( player ) {
                allPlayers.push( player )
            }

            return allPlayers
        }, [] )
    }

    forAreasByBox( box: ISpatialBoundingBox, predicateMethod: ( data: ISpatialIndexArea ) => void ) : void {
        return this.allAreasGrid.search( box ).forEach( predicateMethod )
    }
}

export const L2World = new World()