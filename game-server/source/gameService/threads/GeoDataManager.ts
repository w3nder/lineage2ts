import { IGeoDataManager } from './models/IMovingManager'
import { GeoRegion } from '../../geodata/GeoRegion'
import { GeoDataOperations } from '../../geodata/GeoDataOperations'
import _ from 'lodash'

class Manager extends GeoDataOperations implements IGeoDataManager {
    registerGeoData( regionCode: number, data: SharedArrayBuffer ): void {
        this.regions[ regionCode ] = new GeoRegion( Buffer.from( data ) )
        this.hasLoadedRegions = true
    }

    unRegisterGeoData( regionCode: number ): void {
        delete this.regions[ regionCode ]
        this.hasLoadedRegions = _.isEmpty( this.regions )
    }
}

export const GeoDataManager = new Manager()