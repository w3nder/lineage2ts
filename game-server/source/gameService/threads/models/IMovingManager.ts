
export type L2MovingWorker = IMovingManager & IGeoDataManager

export interface IMovingManager {
    cancelMovement( objectId: number ): void
    registerObject( buffer: SharedArrayBuffer ): void
    startMovement( objectId: number ): void
    unRegisterObject( objectId: number ): void
    updatePosition( objectId: number ): void
    revalidateZValues( objectIds: Array<number> ) : void
}

export interface IGeoDataManager {
    registerGeoData( regionCode: number, data: SharedArrayBuffer ) : void
    unRegisterGeoData( regionCode: number ) : void
}

export const enum MovingThreadVariables {
    StopChannel = 'L2MovementStopped',
}

export type IMovingData = number