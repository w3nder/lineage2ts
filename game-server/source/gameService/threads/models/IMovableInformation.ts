import { SharedData } from './SharedData'
import Timeout = NodeJS.Timeout

export interface IMovableInformation {
    sharedData: SharedData
    move: MoveData
    task: Timeout
}

export const enum MoveState {
    Default,
    ApplyFinishDelay,
    FinishingMove
}

export interface MoveData {
    lastTime: number
    lastX: number
    lastY: number
    counter: number
    state: MoveState
    waiting: number
}