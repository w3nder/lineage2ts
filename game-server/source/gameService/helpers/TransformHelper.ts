import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { Transform } from '../models/actor/Transform'
import { DataManager } from '../../data/manager'

export const TransformHelper = {
    transformPlayer( id: number, player: L2PcInstance ): Promise<void> {
        let transform: Transform = DataManager.getTransformData().getTransform( id )
        if ( transform ) {
            return player.transform( transform )
        }
    },
}