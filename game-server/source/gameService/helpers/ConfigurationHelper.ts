import { ConfigManager } from '../../config/ConfigManager'
import { ItemTypes } from '../values/InventoryValues'

export function getItemLimit( itemId: number ) : number {
    return ConfigManager.tuning.getItemInventoryLimits()[ itemId ] ?? Number.MAX_SAFE_INTEGER
}

export function getMaxAdena() : number {
    return getItemLimit( ItemTypes.Adena )
}