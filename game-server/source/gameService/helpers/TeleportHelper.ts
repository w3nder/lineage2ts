import { PointGeometry } from '../models/drops/PointGeometry'
import { GeometryId } from '../enums/GeometryId'
import aigle from 'aigle'
import { AIEffectHelper } from '../aicontroller/helpers/AIEffectHelper'
import { AIIntent } from '../aicontroller/enums/AIIntent'
import { L2World } from '../L2World'
import { L2Character } from '../models/actor/L2Character'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { DataManager } from '../../data/manager'
import _ from 'lodash'
import { TeleportWhereType } from '../enums/TeleportWhereType'
import { ILocational, Location } from '../models/Location'
import { CastleManager } from '../instancemanager/CastleManager'
import { FortManager } from '../instancemanager/FortManager'
import { L2SiegeFlagInstance } from '../models/actor/instance/L2SiegeFlagInstance'
import { TerritoryWarManager } from '../instancemanager/TerritoryWarManager'
import { L2Npc } from '../models/actor/L2Npc'
import { ClanHallManager } from '../instancemanager/ClanHallManager'
import { SiegableHall } from '../models/entity/clanhall/SiegableHall'
import { SevenSigns } from '../directives/SevenSigns'
import { SevenSignsSeal, SevenSignsSide } from '../values/SevenSignsValues'
import { Instance } from '../models/entity/Instance'
import { InstanceManager } from '../instancemanager/InstanceManager'
import { L2Clan } from '../models/L2Clan'
import { ClanHall } from '../models/entity/ClanHall'
import { ClanHallSiegeManager } from '../instancemanager/ClanHallSiegeManager'
import { RespawnRegionCache } from '../cache/RespawnRegionCache'
import { AreaCache } from '../cache/AreaCache'
import { AreaType } from '../models/areas/AreaType'
import { RespawnArea } from '../models/areas/type/Respawn'
import { L2WorldArea } from '../models/areas/WorldArea'
import { ResidenceTeleportType } from '../enums/ResidenceTeleportType'

export async function teleportPlayersToGeometryCoordinates( geometryId: GeometryId, playerIds: ReadonlyArray<number>, x: number, y: number, z: number, heading: number = undefined ) : Promise<void> {
    let geometry = PointGeometry.acquire( geometryId )
    let adjustedZ = z + 40

    await aigle.resolve( playerIds ).eachSeries( async ( playerId: number ) : Promise<void> => {
        let player = L2World.getPlayer( playerId )
        if ( !player ) {
            return
        }

        geometry.prepareNextPoint()

        let adjustedX = x + geometry.getX()
        let adjustedY = y + geometry.getY()

        if ( !player.getAIController().isIntent( AIIntent.WAITING ) ) {
            await AIEffectHelper.setNextIntent( player, AIIntent.WAITING )
        }

        await player.teleportToLocationCoordinates( adjustedX, adjustedY, adjustedZ, heading ? heading : player.getHeading() )

        let summon = player.getSummon()
        if ( !summon ) {
            return
        }

        geometry.prepareNextPoint()

        if ( !summon.getAIController().isIntent( AIIntent.WAITING ) ) {
            await AIEffectHelper.setNextIntent( summon, AIIntent.WAITING )
        }

        await summon.teleportToLocationCoordinates( geometry.getX(), geometry.getY(), adjustedZ, heading ? heading : summon.getHeading() )
    } )

    geometry.release()
}

export async function teleportCharacterToGeometryCoordinates( geometryId: GeometryId, character: L2Character, x: number, y: number, z: number, heading: number = character.getHeading(), instanceId: number = character.getInstanceId() ) : Promise<void> {
    if ( !character ) {
        return
    }

    let geometry = PointGeometry.acquire( geometryId )
    if ( !geometry ) {
        return
    }

    let adjustedZ = z + 40

    geometry.prepareNextPoint()

    let adjustedX = x + geometry.getX()
    let adjustedY = y + geometry.getY()

    if ( character.hasAIController() && !character.getAIController().isIntent( AIIntent.WAITING ) ) {
        await AIEffectHelper.setNextIntent( character, AIIntent.WAITING )
    }

    await character.teleportToLocationCoordinates( adjustedX, adjustedY, adjustedZ, heading, instanceId )

    geometry.release()
}

function getPoint( name: string, isPvp: boolean ) : Location {
    let defaultData = DataManager.getRespawnPoints().getByName( name )
    return _.sample( isPvp ? defaultData.pvpPoints : defaultData.normalPoints )
}

function getRestartLocation( character: L2Character, respawnPoint: string, isPvp: boolean ): Location {
    let data = DataManager.getRespawnPoints().getByName( respawnPoint )
    if ( !data ) {
        return getPoint( 'aden_town', isPvp )
    }

    if ( data.bannedRace && data.bannedRace.race === character.getRace() ) {
        return getPoint( data.bannedRace.location, isPvp )
    }

    return _.sample( isPvp ? data.pvpPoints : data.normalPoints )
}

export function getTeleportLocation( character: L2Character, whereTo: TeleportWhereType ): ILocational {
    if ( character.isPlayer() ) {
        let player: L2PcInstance = character.getActingPlayer()

        let clan = player.getClan()
        if ( clan && !player.isFlyingMounted() && !player.isFlying() ) {

            if ( whereTo === TeleportWhereType.CLANHALL ) {
                let clanHall = getClanHallByOwner( clan )

                if ( clanHall && clanHall.hasArea() ) {
                    if ( player.getKarma() > 0 ) {
                        return clanHall.getResidenceTeleport( ResidenceTeleportType.PVP )
                    }

                    return clanHall.getResidenceTeleport( ResidenceTeleportType.Normal )
                }
            }

            if ( whereTo === TeleportWhereType.CASTLE ) {
                let castle = CastleManager.getCastleByOwner( clan )

                if ( !castle ) {
                    castle = CastleManager.getCastle( player )

                    if ( !( castle && castle.getSiege().isInProgress() && castle.getSiege().getDefenderClan( clan ) ) ) {
                        castle = null
                    }
                }

                if ( castle && castle.getResidenceId() > 0 ) {
                    if ( player.getKarma() > 0 ) {
                        return castle.getResidenceTeleport( ResidenceTeleportType.PVP )
                    }

                    return castle.getResidenceTeleport( ResidenceTeleportType.Normal )
                }
            }

            if ( whereTo === TeleportWhereType.FORTRESS ) {
                let fort = FortManager.getFortByOwner( clan )

                if ( !fort ) {
                    fort = FortManager.getFort( player )

                    if ( !fort
                        || !fort.getSiege().isInProgress()
                        || fort.getOwnerClan() !== clan ) {
                        fort = null
                    }
                }

                if ( fort && fort.getResidenceId() > 0 ) {
                    if ( player.getKarma() > 0 ) {
                        return fort.getTeleportLocation( ResidenceTeleportType.PVP )
                    }

                    return fort.getTeleportLocation( ResidenceTeleportType.Normal )
                }
            }

            if ( whereTo === TeleportWhereType.SIEGEFLAG ) {
                let territoryWarFlag: L2SiegeFlagInstance = TerritoryWarManager.getSiegeFlagForClan( clan )
                if ( territoryWarFlag ) {
                    return territoryWarFlag
                }

                let castle = CastleManager.getCastle( player )
                if ( castle && castle.getSiege().isInProgress() ) {
                    let flags = castle.getSiege().getFlag( clan )
                    if ( flags && flags.length > 0 ) {
                        let npc = L2World.getObjectById( flags[ 0 ] ) as L2Npc
                        return npc
                    }
                }

                let fort = FortManager.getFort( player )
                if ( fort && fort.getSiege().isInProgress() ) {
                    let flags = fort.siege.getFlag( clan )
                    if ( flags && flags.length > 0 ) {
                        let npc = L2World.getObjectById( flags[ 0 ] ) as L2Npc
                        return npc
                    }
                }

                let clanHall = ClanHallManager.getNearbyClanHall( character.getX(), character.getY() )
                if ( clanHall && clanHall.isSiegableHall() ) {
                    let siegableHall = clanHall as SiegableHall
                    let flags = siegableHall.getSiege().getFlag( clan )
                    if ( flags && flags.length > 0 ) {
                        let npc = L2World.getObjectById( flags[ 0 ] ) as L2Npc
                        return npc
                    }
                }
            }
        }

        if ( player.getKarma() > 0 ) {
            let area: RespawnArea = AreaCache.getFirstAreaForType( player, AreaType.Respawn ) as RespawnArea
            if ( area ) {
                return getRestartLocation( character, area.getRestartName( player ), true )
            }

            return _.sample( RespawnRegionCache.getRespawnPoint( player ).pvpPoints )
        }

        let castle = CastleManager.getCastle( player )
        if ( castle && castle.getSiege().isInProgress() ) {
            let isDefender = castle.getSiege().checkIsDefender( clan )
            let isAttacker = castle.getSiege().checkIsAttacker( clan )
            let isPermitted = SevenSigns.getWinningSealSide( SevenSignsSeal.Strife ) === SevenSignsSide.Dawn
            if ( isDefender || isAttacker && isPermitted ) {
                return castle.getResidenceTeleport( ResidenceTeleportType.Other )
            }
        }

        if ( player.getInstanceId() > 0 ) {
            let currentInstance: Instance = InstanceManager.getInstance( player.getInstanceId() )
            if ( currentInstance ) {
                let location = currentInstance.getExitLocation()
                if ( location ) {
                    return location
                }
            }
        }
    }

    let area: RespawnArea = AreaCache.getFirstAreaForType( character, AreaType.Respawn ) as RespawnArea
    if ( area ) {
        return getRestartLocation( character, area.getRestartName( character as L2PcInstance ), false )
    }

    return _.sample( RespawnRegionCache.getRespawnPoint( character ).normalPoints )
}

function getClanHallByOwner( clan: L2Clan ): ClanHall {
    let value: ClanHall = ClanHallManager.getClanHallByOwner( clan )

    if ( value ) {
        return value
    }

    return ClanHallSiegeManager.getClanHallByOwner( clan )
}

export function teleportAreaPlayersToCoordinates( area: L2WorldArea, x: number, y: number, z: number, heading: number = undefined ): Promise<void> {
    let playerIds = L2World.getPlayerIdsByBox( area.getSpatialIndex() )
    return teleportPlayersToGeometryCoordinates( GeometryId.ZonePartyTeleport, playerIds, x, y, z, heading )
}