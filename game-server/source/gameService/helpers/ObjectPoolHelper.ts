import { create } from 'deepool'

interface DeePoolOperations<Type> {
    use() : Type
    recycle( data : Type ) : void
    grow( size: number ) : void
    size() : number
}

export type ObjectPoolProducer<Type> = () => Type

export const AvailableObjectPools : Record<string, ObjectPool<unknown>> = {}

export class ObjectPool<Type> {
    name: string
    objectPool: DeePoolOperations<Type>

    constructor( name: string, producerMethod: ObjectPoolProducer<Type>, growSize: number = 1 ) {
        this.name = name
        this.objectPool = create( producerMethod )
        this.objectPool.grow( growSize )

        AvailableObjectPools[ name ] = this
    }

    getValue() : Type {
        return this.objectPool.use()
    }

    recycleValue( data: Type ) : void {
        this.objectPool.recycle( data )
    }

    recycleValues( items: Array<Type> ) {
        for ( const item of items ) {
            this.objectPool.recycle( item )
        }
    }
}