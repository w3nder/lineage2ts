export const HtmlComposer = {
    createHpGauge( width: number,
                   currentValue: number,
                   maximumValue: number,
                   displayPercentage: boolean ): string {
        return this.createGauge( width,
                                 Math.floor( currentValue ),
                                 Math.floor( maximumValue ),
                                 displayPercentage,
                                 'L2UI_CT1.Gauges.Gauge_DF_Large_HP_bg_Center',
                                 'L2UI_CT1.Gauges.Gauge_DF_Large_HP_Center' )
    },

    createMpGauge( width: number,
                   currentValue: number,
                   maximumValue: number,
                   displayPercentage: boolean ): string {
        return this.createGauge( width,
                                 Math.floor( currentValue ),
                                 Math.floor( maximumValue ),
                                 displayPercentage,
                                 'L2UI_CT1.Gauges.Gauge_DF_Large_MP_bg_Center',
                                 'L2UI_CT1.Gauges.Gauge_DF_Large_MP_Center' )
    },

    createGauge( width: number,
                 currentValue: number,
                 maximumValue: number,
                 displayPercentage: boolean,
                 backgroundImage: string,
                 mainImage: string,
                 height: number = 17,
                 spacing: number = -13 ): string {
        let adjustedValue = Math.min( currentValue,
                                      maximumValue )
        let displayValue = displayPercentage ?
                `<table cellpadding=0 cellspacing=2><tr><td>${ ( ( adjustedValue / maximumValue ) * 100 ).toFixed( 2 ) }%</td></tr></table>` :
                `<table cellpadding=0 cellspacing=0><tr><td width=${ width } align=right>${ adjustedValue }</td>
                 <td width=10 align=center>/</td><td width=${ Math.floor( ( width - 10 ) / 2 ) }>${ maximumValue }</td></tr></table>`

        return `<table width=${ width } cellpadding=0 cellspacing=0><tr>
                <td background="${ backgroundImage }"><img src="${ mainImage }" width=${ Math.floor( ( adjustedValue / maximumValue ) * width ) } height=${ height }></td>
                </tr><tr>
                <td align=center><table cellpadding=0 cellspacing=${ spacing }><tr><td>${ displayValue }</td>
                </tr></table></td></tr></table>`
    },
}