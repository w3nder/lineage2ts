import { ListenerCache } from '../cache/ListenerCache'
import {
    BuySellViolationEvent, DataIntegrityViolationEvent, DataIntegrityViolationType,
    EventType,
    GeneralViolationEvent, ItemViolationEvent, MailViolationEvent,
    PacketDataViolationEvent,
    SkillViolationEvent,
    SummonViolationEvent, TradeViolationEvent,
} from '../models/events/EventType'
import { EventPoolCache } from '../cache/EventPoolCache'
import { PrivateStoreType } from '../enums/PrivateStoreType'

// TODO : convert to use object pool for events

export function recordGeneralViolation( playerId: number, errorId: string, message: string ) : Promise<void> {
    if ( ListenerCache.hasGeneralListener( EventType.GeneralViolation ) ) {
        let data: GeneralViolationEvent = {
            errorId,
            message,
            playerId,
        }

        return ListenerCache.sendGeneralEvent( EventType.GeneralViolation, data )
    }
}

export function recordBuySellViolation( playerId: number, errorId: string, message: string, ...ids : Array<number> ) : Promise<void> {
    if ( ListenerCache.hasGeneralListener( EventType.BuySellViolation ) ) {
        let data: BuySellViolationEvent = {
            errorId,
            message,
            ids,
            playerId,
        }

        return ListenerCache.sendGeneralEvent( EventType.BuySellViolation, data )
    }
}

export function recordSkillViolation( playerId: number, errorId: string, message: string, skillId: number, skillLevel: number ) : Promise<void> {
    if ( ListenerCache.hasGeneralListener( EventType.SkillViolation ) ) {
        let data: SkillViolationEvent = {
            errorId,
            message,
            playerId,
            skillId,
            skillLevel
        }

        return ListenerCache.sendGeneralEvent( EventType.SkillViolation, data )
    }
}

export function recordSummonViolation( playerId: number, errorId: string, message: string, summonNpcId: number = 0, summonObjectId: number = 0 ) : Promise<void> {
    if ( ListenerCache.hasGeneralListener( EventType.SummonViolation ) ) {
        let data: SummonViolationEvent = {
            errorId,
            message,
            playerId,
            summonNpcId,
            summonObjectId
        }

        return ListenerCache.sendGeneralEvent( EventType.SummonViolation, data )
    }
}

export function recordReceivedPacketViolation( playerId: number, errorId: string, message: string, packetName : string, ...values: Array<number> ) : Promise<void> {
    if ( ListenerCache.hasGeneralListener( EventType.PacketDataViolation ) ) {
        let data = EventPoolCache.getData( EventType.PacketDataViolation ) as PacketDataViolationEvent

        data.errorId = errorId
        data.packetName = packetName
        data.message = message
        data.playerId = playerId
        data.values = values

        return ListenerCache.sendGeneralEvent( EventType.PacketDataViolation, data )
    }
}

export function recordTradeViolation( errorId: string, message: string, sellerPlayerId: number, buyerPlayerId: number, storeType: PrivateStoreType, objectIds: Array<number> ) : Promise<void> {
    if ( ListenerCache.hasGeneralListener( EventType.TradeViolation ) ) {
        let data: TradeViolationEvent = {
            errorId,
            message,
            sellerPlayerId,
            buyerPlayerId,
            storeType,
            objectIds
        }

        return ListenerCache.sendGeneralEvent( EventType.TradeViolation, data )
    }
}

export function recordItemViolation( playerId: number, errorId: string, message: string, itemId: number, itemAmount: number, itemObjectId: number = 0 ) : Promise<void> {
    if ( ListenerCache.hasGeneralListener( EventType.ItemViolation ) ) {
        let data = EventPoolCache.getData( EventType.ItemViolation ) as ItemViolationEvent

        data.errorId = errorId
        data.itemId = itemId
        data.itemAmount = itemAmount
        data.playerId = playerId
        data.message = message
        data.itemObjectId = itemObjectId

        return ListenerCache.sendGeneralEvent( EventType.ItemViolation, data )
    }
}

export function recordDataViolation( playerId: number, errorId: string, type: DataIntegrityViolationType, message: string, ids : Array<number> = [] ) : Promise<void> {
    if ( ListenerCache.hasGeneralListener( EventType.DataIntegrityViolation ) ) {
        let data = EventPoolCache.getData( EventType.DataIntegrityViolation ) as DataIntegrityViolationEvent

        data.errorId = errorId
        data.message = message
        data.type = type
        data.playerId = playerId
        data.ids = ids

        return ListenerCache.sendGeneralEvent( EventType.DataIntegrityViolation, data )
    }
}

export function recordMailViolation( playerId: number, errorId: string, messageId: number, receiverId: number, senderId: number, message: string ) : Promise<void> {
    if ( ListenerCache.hasGeneralListener( EventType.MailViolation ) ) {
        let data = EventPoolCache.getData( EventType.MailViolation ) as MailViolationEvent

        data.errorId = errorId
        data.message = message
        data.playerId = playerId
        data.messageId = messageId
        data.receiverId = receiverId
        data.senderId = senderId

        return ListenerCache.sendGeneralEvent( EventType.MailViolation, data )
    }
}