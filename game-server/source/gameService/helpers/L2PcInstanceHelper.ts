import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { L2World } from '../L2World'
import { DatabaseManager } from '../../database/manager'
import { L2Weapon } from '../models/items/L2Weapon'
import { ItemManagerCache } from '../cache/ItemManagerCache'
import { SubClass } from '../models/base/SubClass'
import { HeroCache } from '../cache/HeroCache'
import { L2Clan } from '../models/L2Clan'
import { ClanCache } from '../cache/ClanCache'
import { L2ClanMember } from '../models/L2ClanMember'
import { ClanPriviledgeHelper } from '../models/ClanPrivilege'
import { Skill } from '../models/Skill'
import { allExceptions, PlayerActionOverride } from '../values/PlayerConditions'
import { SkillCache } from '../cache/SkillCache'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { ExUseSharedGroupItem } from '../packets/send/ExUseSharedGroupItem'
import { L2CharacterItemReuse } from '../../database/interface/CharacterItemReuseSaveTableApi'
import { PlayerVariablesManager } from '../variables/PlayerVariablesManager'
import { AccountVariablesManager } from '../variables/AccountVariablesManager'
import { DimensionalTransferManager } from '../cache/DimensionalTransferManager'
import { PlayerUICache } from '../cache/PlayerUICache'
import aigle from 'aigle'
import { BlocklistCache } from '../cache/BlocklistCache'
import { PlayerShortcutCache } from '../cache/PlayerShortcutCache'
import { PlayerContactCache } from '../cache/PlayerContactCache'
import { TeleportBookmarkCache } from '../cache/TeleportBookmarkCache'
import _ from 'lodash'
import { InstanceType } from '../enums/InstanceType'
import { PlayerLoadState } from '../enums/PlayerLoadState'
import { FastRateLimit } from 'fast-ratelimit'
import { ListenerCache } from '../cache/ListenerCache'
import { EventType, GeneralViolationEvent } from '../models/events/EventType'
import { EventPoolCache } from '../cache/EventPoolCache'
import { L2PlayerRecipeShopItem } from '../../database/interface/CharacterRecipeShoplistTableApi'
import { DataManager } from '../../data/manager'
import { L2CharacterSubclassItem } from '../../database/interface/CharacterSubclassesApi'
import { PlayerRecipeCache } from '../cache/PlayerRecipeCache'
import { PlayerHennaCache } from '../cache/PlayerHennaCache'
import { ServerLog } from '../../logger/Logger'

const playerKillLimiter = new FastRateLimit( {
    threshold: 1, // available tokens over timespan
    ttl: Math.max( 1, ConfigManager.customs.getPlayerKillInterval() ), // time-to-live value of token bucket (in seconds)
} )

const CONDITION_OVERRIDE_KEY = 'cond_override'

// TODO : create violation events
export const L2PcInstanceHelper = {
    async load( playerId: number ): Promise<L2PcInstance> {
        let player: L2PcInstance = await DatabaseManager.getCharacterTable().loadPlayer( playerId )
        if ( !player ) {
            return null
        }

        let currentTime = Date.now()

        if ( player.getClanJoinExpiryTime() < currentTime ) {
            player.setClanJoinExpiryTime( 0 )
        }

        if ( player.getClanCreateExpiryTime() < currentTime ) {
            player.setClanCreateExpiryTime( 0 )
        }

        let classId = player.getClassId()
        player.setFistsWeaponItem( L2PcInstanceHelper.findFistsWeaponItem( classId ) )

        let subClassItems = await DatabaseManager.getCharacterSubclasses().getSubclasses( playerId )
        let amount = DataManager.getExperienceData().getExpForLevel( ConfigManager.character.getBaseSubclassLevel() )

        player.setSubClasses( subClassItems.reduce( ( finalMap: { [ key: number ]: SubClass }, item: L2CharacterSubclassItem ) => {
            let subclass = new SubClass( player, amount )

            subclass.setClassId( item.classId )
            subclass.setExp( item.exp )
            subclass.setLevel( item.level )
            subclass.setSp( item.sp )
            subclass.setClassIndex( item.classIndex )

            finalMap[ item.classIndex ] = subclass

            if ( classId !== player.getBaseClass() && subclass.getClassId() === classId ) {
                player.setClassIndex( subclass.getClassIndex() )
            }

            return finalMap
        }, {} ) )

        if ( ( player.getClassIndex() === 0 ) && ( classId !== player.getBaseClass() ) ) {
            await player.setClassId( player.getBaseClass() )

            if ( ListenerCache.hasGeneralListener( EventType.GeneralViolation ) ) {
                let data = EventPoolCache.getData( EventType.GeneralViolation ) as GeneralViolationEvent

                data.errorId = '4b8f6057-8368-4339-b312-deb6ea5c9d52'
                data.message = 'Player reverted to base class. Possibly has tried a re-login exploit while subclassing.'
                data.playerId = playerId

                await ListenerCache.sendGeneralEvent( EventType.GeneralViolation, data )
            }
        } else {
            player.setActiveClass( classId )
        }

        await player.setHero( HeroCache.isHero( playerId ), false )

        let clan: L2Clan = ClanCache.getClan( player.getClanId() )
        player.setClan( clan )

        if ( player.getClan() != null ) {
            if ( player.getClan().getLeaderId() !== player.getObjectId() ) {
                if ( player.getPowerGrade() === 0 ) {
                    player.setPowerGrade( 5 )
                }
                player.setClanPrivileges( player.getClan().getRankPrivileges( player.getPowerGrade() ) )
            } else {
                player.setClanPrivileges( ClanPriviledgeHelper.getFullPrivileges() )
                player.setPowerGrade( 1 )
            }
            player.setPledgeClass( L2ClanMember.calculatePledgeClass( player ) )
        } else {
            if ( player.isNoble() ) {
                player.setPledgeClass( 5 )
            }

            if ( player.isHero() ) {
                player.setPledgeClass( 8 )
            }

            player.setClanPrivileges( ClanPriviledgeHelper.getDefaultPrivileges() )
        }

        player.characters = await DatabaseManager.getCharacterTable().getOtherCharacterNames( playerId, player.getAccountName() )

        await player.getInventory().restore()
        await player.getFreight().restore()
        await player.initializeWarehouse()

        await L2PcInstanceHelper.loadPlayerSkills( player )
        await PlayerShortcutCache.loadPlayer( player )
        await PlayerContactCache.loadPlayer( playerId )
        await PlayerHennaCache.loadPlayer( player.getObjectId(), player.getClassIndex() )

        await TeleportBookmarkCache.loadPlayer( playerId )
        await PlayerRecipeCache.loadPlayer( player.getObjectId(), player.getClassIndex() )

        // TODO : move to separate data cache
        if ( ConfigManager.character.storeRecipeShopList() ) {
            let items = await DatabaseManager.getCharacterRecipeShoplist().getItem( player.getObjectId() )

            items.forEach( ( item: L2PlayerRecipeShopItem ) => {
                player.addManufactureItem( item.recipeId, item.price )
            } )
        }

        await DimensionalTransferManager.loadPlayer( playerId )
        player.hasPetItems = await DatabaseManager.getItems().hasPetItems( playerId )

        await player.rewardSkills()

        await L2PcInstanceHelper.addItemReuseSaveResults( player )

        if ( ConfigManager.character.storeSkillCooltime() ) {
            await player.restoreEffects()
        }

        if ( player.getCurrentHp() < 0.5 ) {
            player.setIsDead( true )
            player.stopHpMpRegeneration()
        }

        player.setPet( L2World.getPet( player.getObjectId() ) )
        if ( player.hasSummon() ) {
            player.getSummon().setOwner( player )
        }

        player.refreshOverloadPenalty()
        player.refreshExpertisePenalty()

        await BlocklistCache.loadPlayer( playerId )

        if ( ConfigManager.character.storeUISettings() ) {
            await PlayerUICache.loadData( player.getObjectId() )
        }

        if ( ConfigManager.customs.getTreasureChestVisibility() > 0 ) {
            player.hiddenInstanceTypes.add( InstanceType.L2ChestInstance )
            player.clearHiddenInstanceTypes()
        }

        await L2PcInstanceHelper.createVariables( player )

        player.loadState = PlayerLoadState.Loaded

        ServerLog.info( `Loaded player "${player.getName()}" data in ${Date.now() - currentTime} ms` )

        return player
    },

    findFistsWeaponItem( classId: number ): L2Weapon {
        if ( classId >= 0x00 && classId <= 0x09 ) {
            // human fighter fists
            return ItemManagerCache.getTemplate( 246 ) as L2Weapon
        }

        if ( classId >= 0x0a && classId <= 0x11 ) {
            // human mage fists
            return ItemManagerCache.getTemplate( 251 ) as L2Weapon
        }

        if ( classId >= 0x12 && classId <= 0x18 ) {
            // elven fighter fists
            return ItemManagerCache.getTemplate( 244 ) as L2Weapon
        }

        if ( classId >= 0x19 && classId <= 0x1e ) {
            // elven mage fists
            return ItemManagerCache.getTemplate( 249 ) as L2Weapon
        }

        if ( classId >= 0x1f && classId <= 0x25 ) {
            // dark elven fighter fists
            return ItemManagerCache.getTemplate( 245 ) as L2Weapon
        }

        if ( classId >= 0x26 && classId <= 0x2b ) {
            // dark elven mage fists
            return ItemManagerCache.getTemplate( 250 ) as L2Weapon
        }

        if ( classId >= 0x2c && classId <= 0x30 ) {
            // orc fighter fists
            return ItemManagerCache.getTemplate( 248 ) as L2Weapon
        }

        if ( classId >= 0x31 && classId <= 0x34 ) {
            // orc mage fists
            return ItemManagerCache.getTemplate( 252 ) as L2Weapon
        }

        if ( classId >= 0x35 && classId <= 0x39 ) {
            // dwarven fists
            return ItemManagerCache.getTemplate( 247 ) as L2Weapon
        }

        return null
    },

    async loadPlayerSkills( player: L2PcInstance ) {
        const skills: Array<Skill> = await DatabaseManager.getCharacterSkills().getSkills( player )

        const isRemovePossible: boolean = ConfigManager.general.skillCheckEnable()
                && ( !player.hasActionOverride( PlayerActionOverride.SkillAction ) || ConfigManager.general.skillCheckGM() )
                && ConfigManager.general.skillCheckRemove()

        let skillsToRemove: Array<Skill> = []
        await aigle.resolve( skills ).each( ( skill: Skill ) => {
            if ( !skill ) {
                return
            }

            if ( isRemovePossible && !SkillCache.isSkillAllowed( player, skill ) ) {
                skillsToRemove.push( skill )
                return
            }

            return player.addSkill( skill )
        } )

        if ( skillsToRemove.length > 0 ) {
            console.log( 'L2PcInstanceHelper.loadPlayerSkills',
                    `Removing ${ skillsToRemove.length } invalid skills for player id = ${ player.getObjectId() }, name = ${ player.getName() }` )
            await DatabaseManager.getCharacterSkills().deleteSkills( player, skillsToRemove )
        }
    },

    async addItemReuseSaveResults( player: L2PcInstance ) {
        let databaseItems: Array<L2CharacterItemReuse> = await DatabaseManager.getCharacterItemReuseSave().getTimes( player.getObjectId() )

        databaseItems.forEach( ( databaseItem: L2CharacterItemReuse ) => {
            let { itemId, reuseDelay, reuseExpiration } = databaseItem
            let isInInventory = true

            let item: L2ItemInstance = player.getInventory().getItemByItemId( itemId )
            if ( !item ) {
                item = player.getWarehouse().getItemByItemId( itemId )
                isInInventory = false
            }

            if ( item
                    && item.getId() === itemId
                    && item.getReuseDelay() > 0 ) {

                let remainingTime = reuseExpiration - Date.now()
                if ( remainingTime > 0 ) {
                    player.getItemReuse().add( item, reuseDelay, reuseExpiration )

                    if ( isInInventory && item.isEtcItem() ) {
                        let group: number = item.getSharedReuseGroup()
                        if ( group > 0 ) {
                            player.sendOwnedData( ExUseSharedGroupItem( itemId, group, remainingTime, reuseDelay ) )
                        }
                    }
                }
            }
        } )

        return
    },

    async createVariables( player: L2PcInstance ) {
        await PlayerVariablesManager.restoreMe( player.getObjectId() )
        await AccountVariablesManager.restoreMe( player.getAccountName() )

        if ( player.isGM() ) {
            let mask = _.defaultTo( PlayerVariablesManager.get( player.getObjectId(), CONDITION_OVERRIDE_KEY ) as number, allExceptions )
            player.setOverrideCondition( mask )
        }
    },
}

export function isValidPlayerKill( target: L2PcInstance ) : boolean {
    return playerKillLimiter.consumeSync( target.getAccountName() )
}