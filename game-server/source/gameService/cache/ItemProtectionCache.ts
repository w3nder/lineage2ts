import { ConfigManager } from '../../config/ConfigManager'

const itemExpiration : { [ objectId: number ] : number } = {}

export const ItemProtectionCache = {
    addProtection( objectId: number, isRaid: boolean ) : void {
        const duration = isRaid ? ConfigManager.character.getRaidLootRightsInterval() : ConfigManager.tuning.getDropProtectionDuration()
        itemExpiration[ objectId ] = Date.now() + duration
    },

    /*
        Used for specific item circumstances, where only GM or owner should be able to pick such item up.
     */
    addPermanentProtection( objectId: number ) : void {
        itemExpiration[ objectId ] = Number.MAX_SAFE_INTEGER
    },

    removeProtection( objectId: number ) : void {
        delete itemExpiration[ objectId ]
    },

    isProtected( objectId: number ) : boolean {
        return itemExpiration[ objectId ] > Date.now()
    }
}