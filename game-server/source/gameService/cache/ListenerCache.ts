import {
    EventListenerData,
    EventListenerMethod,
    EventListenerTerminatedMethod
} from '../models/events/listeners/EventListenerData'
import { EventTerminationResult, EventType, EventTypeData } from '../models/events/EventType'
import { ListenerRegisterType } from '../enums/ListenerRegisterType'
import { ListenerDescriptionTriggers, ListenerLogic } from '../models/ListenerLogic'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ListenerManager } from '../instancemanager/ListenerManager'
import { EventPoolCache } from './EventPoolCache'
import to from 'await-to-js'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import _ from 'lodash'
import aigle from 'aigle'
import { L2Object } from '../models/L2Object'
import { ServerLog } from '../../logger/Logger'

type EventListeners = Record<EventType, Array<EventListenerData>>
type IdListeners = Record<number, EventListeners>
export type ListenerPredicate = ( data: EventListenerData ) => boolean

class Manager {
    objectInstanceListeners: IdListeners = {}
    npcTemplateListeners: IdListeners = {}
    areaListeners: IdListeners = {}
    itemTemplateListeners: IdListeners = {}
    generalListeners: EventListeners = {} as EventListeners

    addGeneralListener( type: EventType, owner: Object, method: EventListenerMethod, triggers?: ListenerDescriptionTriggers ): void {
        return this.processAddGeneralListener( this.generalListeners, type, owner, method, triggers )
    }

    addItemTemplateListener( id: number, type: EventType, owner: Object, method: EventListenerMethod, triggers?: ListenerDescriptionTriggers ): void {
        return this.processAddListener( this.itemTemplateListeners, id, type, owner, method, triggers )
    }

    addNpcTemplateListener( id: number, type: EventType, owner: Object, method: EventListenerMethod, triggers?: ListenerDescriptionTriggers ): void {
        return this.processAddListener( this.npcTemplateListeners, id, type, owner, method, triggers )
    }

    addObjectListener( id: number, type: EventType, owner: Object, method: EventListenerMethod, triggers?: ListenerDescriptionTriggers ): void {
        this.processAddListener( this.objectInstanceListeners, id, type, owner, method, triggers )
    }

    addAreaListener( id: number, type: EventType, owner: Object, method: EventListenerMethod, triggers?: ListenerDescriptionTriggers ): void {
        return this.processAddListener( this.areaListeners, id, type, owner, method, triggers )
    }

    getGeneralListeners( type: EventType ): Array<EventListenerData> {
        return this.generalListeners[ type ]
    }

    getNpcTemplateListeners( id: number, type: EventType ): Array<EventListenerData> {
        return _.get( this.npcTemplateListeners, [ id, type ] )
    }

    getObjectListeners( id: number, type: EventType ): Array<EventListenerData> {
        return _.get( this.objectInstanceListeners, [ id, type ] )
    }

    getPossibleCharacterListeners( player: L2Object, type: EventType ): Array<Array<EventListenerData>> {
        return [
            this.getObjectListeners( player.getObjectId(), type ),
            this.generalListeners[ type ],
        ]
    }

    async getTerminatedResult( player: L2PcInstance, type: EventType, parameters: EventTypeData ): Promise<EventTerminationResult> {
        return this.processTerminatedResult( player.getObjectId(), player.getObjectId(), type, parameters, _.flatten( this.getPossibleCharacterListeners( player, type ) ) )
    }

    private async processTerminatedResult( originatorId: number,
                                           playerId: number,
                                           type: EventType,
                                           parameters: EventTypeData,
                                           listeners: Array<EventListenerData>,
                                           targetId?: number ): Promise<EventTerminationResult> {
        let outcome: EventTerminationResult

        await aigle.resolve( listeners ).each( async ( data: EventListenerData ) => {
            if ( Number.isInteger( targetId ) && data.targetIds && !data.targetIds.has( targetId ) ) {
                return
            }

            if ( data && ( data.owner as ListenerLogic )?.notifyEventWithTermination ) {
                let result = await ( data.owner as ListenerLogic ).notifyEventWithTermination( playerId, originatorId, data.method as EventListenerTerminatedMethod, parameters )

                if ( !outcome || _.get( result, 'priority', 0 ) > _.get( outcome, 'priority', 0 ) ) {
                    outcome = result
                    return
                }
            }
        } )

        EventPoolCache.recycleData( type, parameters )

        return outcome
    }

    async getGeneralTerminatedResult( type: EventType, parameters: EventTypeData, targetId?: number ): Promise<EventTerminationResult> {
        return this.processTerminatedResult( 0, 0, type, parameters, this.getGeneralListeners( type ), targetId )
    }

    async getTerminatedItemEventResult( item: L2ItemInstance, type: EventType, player: L2PcInstance, parameters: EventTypeData, targetId?: number ): Promise<EventTerminationResult> {
        let playerId: number = player ? player.getObjectId() : 0
        let originatorId: number = item.getObjectId()

        return this.processTerminatedResult( originatorId, playerId, type, parameters, this.getItemTemplateListeners( item.getId(), type ), targetId )
    }

    async getNpcTerminatedResult( npc: L2Object, player: L2PcInstance, type: EventType, parameters: EventTypeData, targetId?: number ): Promise<EventTerminationResult> {
        return this.processTerminatedResult( npc.getObjectId(), player.getObjectId(), type, parameters, this.getNpcTemplateListeners( npc.getId(), type ), targetId )
    }

    hasNpcTemplateListeners( id: number, type: EventType ): boolean {
        return !!this.getNpcTemplateListeners( id, type ) || !!this.getGeneralListeners( type )
    }

    hasObjectListener( id: number, type: EventType, owner: Object = null ): boolean {
        let listeners: Array<EventListenerData> = this.getObjectListeners( id, type )
        if ( listeners && owner ) {
            return listeners.some( ( data: EventListenerData ) => {
                return data.owner === owner
            } )
        }

        return listeners && listeners.length > 0
    }

    hasAreaListeners( id: number, type: EventType ): boolean {
        return !!this.getAreaListeners( id, type ) || !!this.getGeneralListeners( type )
    }

    hasGeneralListener( type: EventType ): boolean {
        return !!this.getGeneralListeners( type )
    }

    hasItemTemplateEvent( id: number, type: EventType ): boolean {
        return !!this.getItemTemplateListeners( id, type ) || !!this.getGeneralListeners( type )
    }

    processAddGeneralListener( listeners: { [ type: string ]: Array<EventListenerData> },
            type: EventType,
            owner: Object,
            method: EventListenerMethod,
            triggers?: ListenerDescriptionTriggers ): void {
        if ( !listeners[ type ] ) {
            listeners[ type ] = []
        }

        let event: EventListenerData = {
            owner,
            method,
            targetIds: triggers?.targetIds
        }

        listeners[ type ].push( event )
    }

    private processAddListener( listeners: { [ id: number ]: { [ type: string ]: Array<EventListenerData> } },
            id: number,
            type: EventType,
            owner: Object,
            method: EventListenerMethod,
            triggers: ListenerDescriptionTriggers | null ): void {

        if ( !listeners[ id ] ) {
            listeners[ id ] = {}
        }

        if ( !listeners[ id ][ type ] ) {
            listeners[ id ][ type ] = []
        }

        let event: EventListenerData = {
            owner,
            method,
            targetIds: triggers?.targetIds
        }

        listeners[ id ][ type ].push( event )
    }

    async processListeners( allListeners: Array<EventListenerData>, parameters: EventTypeData, targetId: number ): Promise<void> {
        await aigle.resolve( allListeners ).each( async ( data: EventListenerData ) => {
            if ( !data.method ) {
                return
            }

            if ( Number.isInteger( targetId ) && data.targetIds && !data.targetIds.has( targetId ) ) {
                return
            }

            return data.method( parameters )
        } )
    }

    registerListener( registration: ListenerRegisterType,
            type: EventType,
            owner: Object,
            method: EventListenerMethod,
            ids: Array<number> = null,
            triggers?: ListenerDescriptionTriggers ): void {

        switch ( registration ) {
            case ListenerRegisterType.WorldObject:
                _.each( ids, ( currentId: number ) => {
                    ListenerCache.addObjectListener( currentId, type, owner, method, triggers )
                } )

                return

            case ListenerRegisterType.NpcId:
                _.each( ids, ( currentId: number ) => {
                    ListenerCache.addNpcTemplateListener( currentId, type, owner, method, triggers )
                } )

                return

            case ListenerRegisterType.AreaId:
                _.each( ids, ( currentId: number ) => {
                    ListenerCache.addAreaListener( currentId, type, owner, method, triggers )
                } )

                return

            case ListenerRegisterType.ItemId:
                _.each( ids, ( currentId: number ) => {
                    ListenerCache.addItemTemplateListener( currentId, type, owner, method, triggers )
                } )

                return

            case ListenerRegisterType.General:
                return ListenerCache.addGeneralListener( type, owner, method, triggers )
        }

        ServerLog.error( `ListenerCache.registerListener: unknown registration type = ${registration}` )
    }

    unRegisterGeneralListener( type: EventType, owner: Object ) : void {
        let listeners = this.generalListeners[ type ]
        if ( listeners ) {
            _.remove( listeners, ( data: EventListenerData ) : boolean => data.owner === owner )
        }
    }

    removeObjectListener( id: number, type: EventType, predicate: ListenerPredicate ) {
        let listeners = _.get( this.objectInstanceListeners, [ id, type ] )
        if ( listeners ) {
            _.remove( listeners, predicate )
        }
    }

    async sendGeneralEvent( type: EventType, parameters: EventTypeData, targetId?: number ): Promise<void> {
        let [ error ] = await to( this.processListeners( this.generalListeners[ type ], parameters, targetId ) )

        EventPoolCache.recycleData( type, parameters )
        this.logError( error )
    }

    async sendItemTemplateEvent( templateId: number, type: EventType, parameters: EventTypeData, targetId?: number ): Promise<void> {
        let [ error ] = await to( this.processListeners( _.get( this.itemTemplateListeners, [ templateId, type ] ), parameters, targetId ) )

        EventPoolCache.recycleData( type, parameters )
        this.logError( error )
    }

    async sendNpcTemplateEvent( templateId: number, type: EventType, parameters: EventTypeData, targetId?: number ): Promise<void> {
        let [ error ] = await to( this.processListeners( _.get( this.npcTemplateListeners, [ templateId, type ] ), parameters, targetId ) )

        EventPoolCache.recycleData( type, parameters )
        this.logError( error )
    }

    async sendObjectEvent( objectId: number, type: EventType, parameters: EventTypeData, targetId?: number ): Promise<void> {
        let [ error ] = await to( this.processListeners( this.getObjectListeners( objectId, type ), parameters, targetId ) )

        EventPoolCache.recycleData( type, parameters )
        this.logError( error )
    }

    async sendQuestEvent( questName: string, playerId: number, originatorId: number, type: EventType, parameters: EventTypeData ): Promise<void> {
        let quest = ListenerManager.getListenerByName( questName )
        if ( quest ) {
            let [ error ] = await to( quest.invokeEvent( playerId, originatorId, type, parameters ) )
            this.logError( error )
        }

        EventPoolCache.recycleData( type, parameters )
    }

    async sendZoneEvent( id: number, type: EventType, parameters: EventTypeData, targetId?: number ): Promise<void> {
        let [ error ] = await to( this.processListeners( this.getAreaListeners( id, type ), parameters, targetId ) )

        EventPoolCache.recycleData( type, parameters )
        this.logError( error )
    }

    getAreaListeners( id: number, type: EventType ): Array<EventListenerData> {
        return _.get( this.areaListeners, [ id, type ] )
    }

    getItemTemplateListeners( id: number, type: EventType ): Array<EventListenerData> {
        return _.get( this.itemTemplateListeners, [ id, type ] )
    }

    // TODO : use proper logger
    logError( error: Error ): void {
        if ( !error ) {
            return
        }

        ServerLog.error( error, 'ListenerCache error:' )
    }
}

export const ListenerCache = new Manager()