import { L2DataApi } from '../../data/interface/l2DataApi'
import { DatabaseManager } from '../../database/manager'
import { L2World } from '../L2World'
import { L2Clan } from '../models/L2Clan'
import { ConfigManager } from '../../config/ConfigManager'
import aigle from 'aigle'
import _ from 'lodash'
import * as schedule from 'node-schedule'

const cronExpression: string = '30 6 * * *'
function sortMethod ( pair: [ string, number ] ) : number {
    return pair[ 1 ]
}

class Manager implements L2DataApi {
    playerPointsMap: { [ key: number ]: Map<number, number> }
    playerTotalsMap: { [ key: number ]: number }

    task: any // cron task to reset player points

    async load(): Promise<Array<string>> {
        this.playerPointsMap = await DatabaseManager.getCharacterRaidPoints().getAll()
        this.playerTotalsMap = _.reduce( this.playerPointsMap, ( finalMap: Record<number, number>, playerMap: Map<number, number>, key: string ) => {

            // TODO : Fix totals
            let total = 0
            playerMap.forEach( ( total: number, currentValue: number ) => {
                total = currentValue + total
            } )

            finalMap[ _.parseInt( key ) ] = total

            return finalMap
        }, {} )

        if ( this.task ) {
            this.task.cancel()
        }

        this.task = schedule.scheduleJob( cronExpression, this.runResetPointsTask.bind( this ) )

        return [
            `RaidBossPointsManager loaded ${ _.size( this.playerPointsMap ) } player scores.`,
        ]
    }

    async runResetPointsTask(): Promise<void> {
        let clans : Set<L2Clan> = new Set<L2Clan>()
        let promises = []

        this.getRankMap().forEach( ( score: number, playerId: number ) => {
            let player = L2World.getPlayer( playerId )
            let clan: L2Clan = player.getClan()

            if ( !clan ) {
                return
            }

            clans.add( clan )
            promises.push( clan.addReputationScore( getReputationScore( score ), false ) )
        } )

        await Promise.all( promises )
        await aigle.resolve( Array.from( clans ) ).each( ( clan : L2Clan ) => {
            return clan.saveReputationScore()
        } )

        this.playerPointsMap = {}
        this.playerTotalsMap = {}

        return DatabaseManager.getCharacterRaidPoints().removeAll()
    }

    getPointsByOwnerId( objectId: number ): number {
        return this.playerTotalsMap[ objectId ] || 0
    }

    getPointsMap( objectId: number ) : Map<number, number> {
        return this.playerPointsMap[ objectId ]
    }

    calculateRank( objectId: number ) {
        return _.defaultTo( this.getRankMap().get( objectId ), 0 )
    }

    /*
        TODO : store rank map and recompute it only on certain delay
     */
    getRankMap(): Map<number, number> {
        let pairs: Array<[ string, number ]> = _.toPairs( this.playerTotalsMap )

        return _.reduce( _.sortBy( pairs, [ sortMethod ] ), ( finalMap: Map<number, number>, currentPair, index: number ) => {

            finalMap.set( _.parseInt( currentPair[ 0 ] ), index + 1 )

            return finalMap
        }, new Map<number, number>() )
    }

    async addPoints( objectId: number, bossId: number, points: number ): Promise<void> {
        if ( !this.playerPointsMap[ objectId ] ) {
            this.playerPointsMap[ objectId ] = new Map<number, number>()
            this.playerTotalsMap[ objectId ] = 0
        }

        let oldPoints = _.defaultTo( this.playerPointsMap[ objectId ].get( bossId ), 0 )

        this.playerTotalsMap[ objectId ] = this.playerTotalsMap[ objectId ] - oldPoints + points
        this.playerPointsMap[ objectId ].set( bossId, points )

        return DatabaseManager.getCharacterRaidPoints().updatePoints( objectId, bossId, points )
    }
}

function getReputationScore( score: number ): number {
    switch ( score ) {
        case 1:
            return ConfigManager.clan.get1stRaidRankingPoints()
        case 2:
            return ConfigManager.clan.get2ndRaidRankingPoints()
        case 3:
            return ConfigManager.clan.get3rdRaidRankingPoints()
        case 4:
            return ConfigManager.clan.get4thRaidRankingPoints()
        case 5:
            return ConfigManager.clan.get5thRaidRankingPoints()
        case 6:
            return ConfigManager.clan.get6thRaidRankingPoints()
        case 7:
            return ConfigManager.clan.get7thRaidRankingPoints()
        case 8:
            return ConfigManager.clan.get8thRaidRankingPoints()
        case 9:
            return ConfigManager.clan.get9thRaidRankingPoints()
        case 10:
            return ConfigManager.clan.get10thRaidRankingPoints()
    }

    return score <= 50 ? ConfigManager.clan.getUpTo50thRaidRankingPoints() : ConfigManager.clan.getUpTo100thRaidRankingPoints()
}

export const RaidBossPointsManager = new Manager()