import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { RelationChangedType } from '../packets/send/RelationChanged'
import { TerritoryWarManager } from '../instancemanager/TerritoryWarManager'
import { ArenaParticipantsHolder } from '../models/ArenaParticipantsHolder'
import { BlockCheckerManager } from '../instancemanager/BlockCheckerManager'

class Manager {
    players: Record<number, number> = {}

    computeRelationStatus( player: L2PcInstance ) : void {
        let result = RelationChangedType.None

        let clan = player.getClan()
        if ( clan ) {
            result |= RelationChangedType.InClan

            if ( player.getAllyId() !== 0 ) {
                result |= RelationChangedType.InClanAlliance
            }

            if ( clan.isLeader( player.getObjectId() ) ) {
                result |= RelationChangedType.ClanLeader
            }
        }

        let siegeState = player.getSiegeRole()
        if ( siegeState !== 0 ) {
            if ( TerritoryWarManager.getRegisteredTerritoryId( player ) !== 0 ) {
                result |= RelationChangedType.TerritoryWar
            } else {
                result |= RelationChangedType.InSiege

                if ( siegeState === 1 ) {
                    result |= RelationChangedType.SiegeAttacker
                }
            }
        }

        let blockChecker = player.getBlockCheckerArena()

        if ( blockChecker !== -1 ) {
            result |= RelationChangedType.InSiege
            let holder: ArenaParticipantsHolder = BlockCheckerManager.getHolder( blockChecker )
            if ( holder.getPlayerTeam( player ) === 0 ) {
                result |= RelationChangedType.Enemy
            } else {
                result |= RelationChangedType.Ally
            }
            result |= RelationChangedType.SiegeAttacker
        }

        this.players[ player.getObjectId() ] = result
    }

    getStatus( objectId: number ) : RelationChangedType {
        return this.players[ objectId ] || RelationChangedType.None
    }

    removeStatus( objectId: number ) : void {
        delete this.players[ objectId ]
    }
}

export const PlayerRelationStatus = new Manager()