import { CrystalType } from '../models/items/type/CrystalType'
import { LifeStoneGrade } from '../enums/LifeStoneGrade'

const allStones = {
    8723: createLifeStone( LifeStoneGrade.None, 0 ),
    8724: createLifeStone( LifeStoneGrade.None, 1 ),
    8725: createLifeStone( LifeStoneGrade.None, 2 ),
    8726: createLifeStone( LifeStoneGrade.None, 3 ),
    8727: createLifeStone( LifeStoneGrade.None, 4 ),
    8728: createLifeStone( LifeStoneGrade.None, 5 ),
    8729: createLifeStone( LifeStoneGrade.None, 6 ),
    8730: createLifeStone( LifeStoneGrade.None, 7 ),
    8731: createLifeStone( LifeStoneGrade.None, 8 ),
    8732: createLifeStone( LifeStoneGrade.None, 9 ),

    8733: createLifeStone( LifeStoneGrade.Mid, 0 ),
    8734: createLifeStone( LifeStoneGrade.Mid, 1 ),
    8735: createLifeStone( LifeStoneGrade.Mid, 2 ),
    8736: createLifeStone( LifeStoneGrade.Mid, 3 ),
    8737: createLifeStone( LifeStoneGrade.Mid, 4 ),
    8738: createLifeStone( LifeStoneGrade.Mid, 5 ),
    8739: createLifeStone( LifeStoneGrade.Mid, 6 ),
    8740: createLifeStone( LifeStoneGrade.Mid, 7 ),
    8741: createLifeStone( LifeStoneGrade.Mid, 8 ),
    8742: createLifeStone( LifeStoneGrade.Mid, 9 ),

    8743: createLifeStone( LifeStoneGrade.High, 0 ),
    8744: createLifeStone( LifeStoneGrade.High, 1 ),
    8745: createLifeStone( LifeStoneGrade.High, 2 ),
    8746: createLifeStone( LifeStoneGrade.High, 3 ),
    8747: createLifeStone( LifeStoneGrade.High, 4 ),
    8748: createLifeStone( LifeStoneGrade.High, 5 ),
    8749: createLifeStone( LifeStoneGrade.High, 6 ),
    8750: createLifeStone( LifeStoneGrade.High, 7 ),
    8751: createLifeStone( LifeStoneGrade.High, 8 ),
    8752: createLifeStone( LifeStoneGrade.High, 9 ),

    8753: createLifeStone( LifeStoneGrade.Top, 0 ),
    8754: createLifeStone( LifeStoneGrade.Top, 1 ),
    8755: createLifeStone( LifeStoneGrade.Top, 2 ),
    8756: createLifeStone( LifeStoneGrade.Top, 3 ),
    8757: createLifeStone( LifeStoneGrade.Top, 4 ),
    8758: createLifeStone( LifeStoneGrade.Top, 5 ),
    8759: createLifeStone( LifeStoneGrade.Top, 6 ),
    8760: createLifeStone( LifeStoneGrade.Top, 7 ),
    8761: createLifeStone( LifeStoneGrade.Top, 8 ),
    8762: createLifeStone( LifeStoneGrade.Top, 9 ),

    9573: createLifeStone( LifeStoneGrade.None, 10 ),
    9574: createLifeStone( LifeStoneGrade.Mid, 10 ),
    9575: createLifeStone( LifeStoneGrade.High, 10 ),
    9576: createLifeStone( LifeStoneGrade.Top, 10 ),

    10483: createLifeStone( LifeStoneGrade.None, 11 ),
    10484: createLifeStone( LifeStoneGrade.Mid, 11 ),
    10485: createLifeStone( LifeStoneGrade.High, 11 ),
    10486: createLifeStone( LifeStoneGrade.Top, 11 ),

    12754: createLifeStone( LifeStoneGrade.Accessory, 0 ),
    12755: createLifeStone( LifeStoneGrade.Accessory, 1 ),
    12756: createLifeStone( LifeStoneGrade.Accessory, 2 ),
    12757: createLifeStone( LifeStoneGrade.Accessory, 3 ),
    12758: createLifeStone( LifeStoneGrade.Accessory, 4 ),
    12759: createLifeStone( LifeStoneGrade.Accessory, 5 ),
    12760: createLifeStone( LifeStoneGrade.Accessory, 6 ),
    12761: createLifeStone( LifeStoneGrade.Accessory, 7 ),
    12762: createLifeStone( LifeStoneGrade.Accessory, 8 ),
    12763: createLifeStone( LifeStoneGrade.Accessory, 9 ),

    12821: createLifeStone( LifeStoneGrade.Accessory, 10 ),
    12822: createLifeStone( LifeStoneGrade.Accessory, 11 ),

    12840: createLifeStone( LifeStoneGrade.Accessory, 0 ),
    12841: createLifeStone( LifeStoneGrade.Accessory, 1 ),
    12842: createLifeStone( LifeStoneGrade.Accessory, 2 ),
    12843: createLifeStone( LifeStoneGrade.Accessory, 3 ),
    12844: createLifeStone( LifeStoneGrade.Accessory, 4 ),
    12845: createLifeStone( LifeStoneGrade.Accessory, 5 ),
    12846: createLifeStone( LifeStoneGrade.Accessory, 6 ),
    12847: createLifeStone( LifeStoneGrade.Accessory, 7 ),
    12848: createLifeStone( LifeStoneGrade.Accessory, 8 ),
    12849: createLifeStone( LifeStoneGrade.Accessory, 9 ),
    12850: createLifeStone( LifeStoneGrade.Accessory, 10 ),
    12851: createLifeStone( LifeStoneGrade.Accessory, 11 ),

    14008: createLifeStone( LifeStoneGrade.Accessory, 12 ),

    14166: createLifeStone( LifeStoneGrade.None, 12 ),
    14167: createLifeStone( LifeStoneGrade.Mid, 12 ),
    14168: createLifeStone( LifeStoneGrade.High, 12 ),
    14169: createLifeStone( LifeStoneGrade.Top, 12 ),

    16160: createLifeStone( LifeStoneGrade.None, 13 ),
    16161: createLifeStone( LifeStoneGrade.Mid, 13 ),
    16162: createLifeStone( LifeStoneGrade.High, 13 ),
    16163: createLifeStone( LifeStoneGrade.Top, 13 ),
    16177: createLifeStone( LifeStoneGrade.Accessory, 13 ),

    16164: createLifeStone( LifeStoneGrade.None, 13 ),
    16165: createLifeStone( LifeStoneGrade.Mid, 13 ),
    16166: createLifeStone( LifeStoneGrade.High, 13 ),
    16167: createLifeStone( LifeStoneGrade.Top, 13 ),
    16178: createLifeStone( LifeStoneGrade.Accessory, 13 ),
}

const lifeStoneLevels : Array<number> = [
    46,
    49,
    52,
    55,
    58,
    61,
    64,
    67,
    70,
    76,
    80,
    82,
    84,
    85
]

export interface LifeStone {
    grade: number
    level: number
}

function createLifeStone( grade: number, level: number ) : LifeStone {
    return {
        grade,
        level
    }
}

class Manager {
    getLifeStone( id: number ) : LifeStone {
        return allStones[ id ]
    }

    getPlayerLevel( lifeStone: LifeStone ) : number {
        return lifeStoneLevels[ lifeStone.level ]
    }

    getGemStoneId( grade: CrystalType ) {
        switch ( grade ) {
            case CrystalType.C:
            case CrystalType.B:
                return LifeStoneGrade.GemstoneD
            case CrystalType.A:
            case CrystalType.S:
                return LifeStoneGrade.GemstoneC
            case CrystalType.S80:
            case CrystalType.S84:
                return LifeStoneGrade.GemstoneB
        }

        return 0
    }

    getGemStoneCount( grade: CrystalType, lifeStone: LifeStone ) : number {
        if ( lifeStone.grade === LifeStoneGrade.Accessory ) {
            switch ( grade ) {
                case CrystalType.C:
                    return 200
                case CrystalType.B:
                    return 300
                case CrystalType.A:
                    return 200
                case CrystalType.S:
                    return 250
                case CrystalType.S80:
                    return 360
                case CrystalType.S84:
                    return 480
            }

            return 0
        }

        switch ( grade ) {
            case CrystalType.C:
                return 20
            case CrystalType.B:
                return 30
            case CrystalType.A:
                return 20
            case CrystalType.S:
                return 25
            case CrystalType.S80:
            case CrystalType.S84:
                return 36
        }

        return 0
    }
}

export const LifeStoneCache = new Manager()