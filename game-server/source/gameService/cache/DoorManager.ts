import { L2DoorInstance } from '../models/actor/instance/L2DoorInstance'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2DoorTemplate } from '../models/actor/templates/L2DoorTemplate'
import { ListenerCache } from './ListenerCache'
import { ListenerRegisterType } from '../enums/ListenerRegisterType'
import { EventType, GeoRegionActivatedEvent } from '../models/events/EventType'
import { GeoPolygonCache } from './GeoPolygonCache'
import _ from 'lodash'
import { DataManager } from '../../data/manager'
import { getGeoRegionCode } from '../enums/L2MapTile'
import { ServerLog } from '../../logger/Logger'

/*
    TODO: use spacial grid for door lookup
 */
class Manager implements L2DataApi {
    doorsByTemplateId: Record<number, L2DoorInstance>
    doorsByName: Record<string, L2DoorInstance>
    regions: Record<number, Array<L2DoorTemplate>>
    activatedRegions: Set<number> = new Set<number>()

    getDoor( templateId: number ): L2DoorInstance {
        return this.doorsByTemplateId[ templateId ]
    }

    getDoorByName( name: string ): L2DoorInstance {
        return this.doorsByName[ name ]
    }

    async load(): Promise<Array<string>> {
        this.doorsByTemplateId = {}
        this.doorsByName = {}
        this.regions = {}
        this.activatedRegions.clear()

        let templates = DataManager.getDoorData().getAllTemplates()

        for ( const template of templates ) {
            let code = getGeoRegionCode( template.getX(), template.getY() )

            let regionTemplates = this.regions[ code ]
            if ( !regionTemplates ) {
                regionTemplates = []
                this.regions[ code ] = regionTemplates
            }

            regionTemplates.push( template )
        }

        ListenerCache.registerListener( ListenerRegisterType.General, EventType.GeoRegionActivated, this, this.onRegionActivation.bind( this ) )

        return [
            `DoorManager: prepared ${ _.size( this.regions ) } geo-regions for on-demand load of ${ templates.length } door templates`,
        ]
    }

    hasSpawnedRegion( code: number ): boolean {
        return this.activatedRegions.has( code )
    }

    onRegionActivation( data: GeoRegionActivatedEvent ): void {
        if ( this.hasSpawnedRegion( data.code ) ) {
            return
        }

        let templates = this.regions[ data.code ]
        if ( !templates ) {
            return
        }

        let startTime = Date.now()
        let amount = 0
        for ( const template of templates ) {
            this.createDoor( template )
            amount++
        }

        this.activatedRegions.add( data.code )

        ServerLog.info( `DoorManager: activated region X=${ data.x } Y=${ data.y }, created ${ amount } doors in ${ Date.now() - startTime } ms` )
    }

    private createDoor( template: L2DoorTemplate ): void {
        let door = new L2DoorInstance( template )

        door.setCurrentHp( door.getMaxHp() )
        door.setIsVisible( true )
        door.setXYZ( template.getX(), template.getY(), GeoPolygonCache.getZ( template.getX(), template.getY(), door.getTemplate().getZ() ) )
        door.spawnMeNow()

        if ( this.doorsByTemplateId[ template.getId() ] ) {
            throw new Error( `Door with ${ template.getId() } has already been created.` )
        }

        this.doorsByTemplateId[ template.getId() ] = door
        this.doorsByName[ template.name ] = door
    }
}

export const DoorManager = new Manager()