import { Skill } from '../models/Skill'
import { DataManager } from '../../data/manager'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ISkillCapable } from '../interface/ISkillCapable'
import { L2SkillLearn } from '../models/L2SkillLearn'
import { ConfigManager } from '../../config/ConfigManager'
import { PlayerSkillHolder } from '../models/holders/PlayerSkillHolder'
import { ClassId } from '../models/base/ClassId'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { Race } from '../enums/Race'
import { AcquireSkillType } from '../enums/AcquireSkillType'
import { L2Clan } from '../models/L2Clan'
import { SubClass } from '../models/base/SubClass'
import aigle from 'aigle'
import _ from 'lodash'
import { CommonSkillIds } from '../enums/skills/CommonSkillIds'

function populateHashId( hashIds: Array<number>, skill: L2SkillLearn ) : Array<number> {
    if ( skill.getRaces().length === 0 ) {
        hashIds.push( DataManager.getSkillData().getHashCode( skill.getSkillId(), skill.getSkillLevel() ) )
    }

    return hashIds
}

type CompleteClassMapItem = { [ skillId: number ]: Array<L2SkillLearn> }
type CompleteClassTreeItem = { [ hashCode: number ]: L2SkillLearn }

class CacheManager implements L2DataApi {
    skillsByClassIdHashCodes: { [ key: number ]: Array<number> } // Occupation skills
    skillsByRaceHashCodes: { [ key: number ]: Array<number> } // Race-specific Transformations
    allSkillsHashCodes: Array<number>
    nobleSkills: Array<Skill>
    heroSkills: Array<Skill>
    completeClassTree: Record<number, CompleteClassTreeItem> = {}
    completeClassMap: Record<number, CompleteClassMapItem> = {}
    residenceSkillTree: Record<number, Array<Skill>> = {}

    async addGMSkills( player: L2PcInstance, auraSkills: boolean ): Promise<any> {
        let skills = auraSkills ? DataManager.getSkillTreesData().getGameMasterAuraSkillTree() : DataManager.getSkillTreesData().getGameMasterSkillTree()

        return aigle.resolve( skills ).each( ( skill: L2SkillLearn ): Promise<any> => {
            return player.addSkill( SkillCache.getSkill( skill.getSkillId(), skill.getSkillLevel() ), false )
        } )
    }

    async switchGMSkills( player: L2PcInstance, auraSkills: boolean ): Promise<void> {
        let possibleSkills = auraSkills ? DataManager.getSkillTreesData().getGameMasterSkillTree() : DataManager.getSkillTreesData().getGameMasterAuraSkillTree()
        let manager = this
        await aigle.resolve( possibleSkills ).eachSeries( ( skill: L2SkillLearn ) => {
            return player.removeSkill( manager.getSkill( skill.getSkillId(), skill.getSkillLevel() ), false )
        } )

        return this.addGMSkills( player, auraSkills )
    }

    generateMappings() {
        let manager = this

        _.each( _.keys( DataManager.getSkillTreesData().getClassSkillTrees() ), ( classId: string ) => {
            let allSkills = manager.getCompleteClassSkillTree( parseInt( classId ) )

            let allKeys = _.keys( allSkills ).map( value => _.parseInt( value ) )

            manager.skillsByClassIdHashCodes[ classId ] = allKeys.sort( ( one: number, two: number ) => {
                return one - two
            } )
        } )

        let raceIds = _.filter( _.values( Race ), _.isNumber )
        _.each( raceIds, ( raceId: number ) => {
            let hashIds: Array<number> = []

            _.each( DataManager.getSkillTreesData().getFishingSkillTree(), ( skill: L2SkillLearn ) => {
                if ( skill.getRaces().includes( raceId ) ) {
                    hashIds.push( DataManager.getSkillData().getHashCode( skill.getSkillId(), skill.getSkillLevel() ) )
                }
            } )

            _.each( DataManager.getSkillTreesData().getTransformSkillTree(), ( skill: L2SkillLearn ) => {
                if ( skill.getRaces().includes( raceId ) ) {
                    hashIds.push( DataManager.getSkillData().getHashCode( skill.getSkillId(), skill.getSkillLevel() ) )
                }
            } )

            manager.skillsByRaceHashCodes[ raceId ] = hashIds.sort( ( one: number, two: number ) => {
                return one - two
            } )
        } )

        let hashIds: Array<number> = []

        _.reduce( DataManager.getSkillTreesData().getCommonSkillTree(), populateHashId, hashIds )
        _.reduce( DataManager.getSkillTreesData().getFishingSkillTree(), populateHashId, hashIds )
        _.reduce( DataManager.getSkillTreesData().getTransformSkillTree(), populateHashId, hashIds )
        _.reduce( DataManager.getSkillTreesData().getCollectSkillTree(), populateHashId, hashIds )

        this.allSkillsHashCodes = hashIds.sort( ( one: number, two: number ) => {
            return one - two
        } )
    }

    getAllAvailableSkills( player: L2PcInstance, classId: number, includeFSSkills: boolean, includeAutoGet: boolean ): Array<Skill> {
        let holder: PlayerSkillHolder = new PlayerSkillHolder( player )
        let learnableSkills: Array<L2SkillLearn> = this.getAvailableSkills( player, classId, includeFSSkills, includeAutoGet, holder )
        while ( !_.isEmpty( learnableSkills ) ) {
            _.each( learnableSkills, ( learnable: L2SkillLearn ) => {
                let skill: Skill = SkillCache.getSkill( learnable.getSkillId(), learnable.getSkillLevel() )
                holder.addSkill( skill )
            } )

            learnableSkills = this.getAvailableSkills( player, classId, includeFSSkills, includeAutoGet, holder )
        }

        return _.values( holder.getSkills() )
    }

    getAvailableAutoGetSkills( player: L2PcInstance ): Array<L2SkillLearn> {
        let outcome = []
        let skills = this.getCompleteClassSkillTree( player.getClassId() )
        if ( _.isEmpty( skills ) ) {
            return outcome
        }

        let race = player.getRace()
        _.each( skills, ( skill: L2SkillLearn ) => {
            if ( !_.isEmpty( skill.getRaces() ) && !skill.getRaces().includes( race ) ) {
                return
            }

            if ( skill.isAutoGet() && ( player.getLevel() >= skill.getGetLevel() ) ) {
                let oldSkill: Skill = player.getSkills()[ skill.getSkillId() ]
                if ( oldSkill ) {
                    if ( ( oldSkill.getLevel() < skill.getSkillLevel() ) ) {
                        outcome.push( skill )
                    }
                } else {
                    outcome.push( skill )
                }
            }
        } )

        return outcome
    }

    getAvailableCollectSkills( player: L2PcInstance ): Array<L2SkillLearn> {
        return _.reduce( this.getCollectSkillTree(), ( allSkills: Array<L2SkillLearn>, skill: L2SkillLearn ) => {
            let oldSkill: Skill = player.getSkills()[ skill.getSkillId() ]
            if ( oldSkill ) {
                if ( oldSkill.getLevel() === ( skill.getSkillLevel() - 1 ) ) {
                    allSkills.push( skill )
                }
            } else if ( skill.getSkillLevel() === 1 ) {
                allSkills.push( skill )
            }

            return allSkills
        }, [] )
    }

    getAvailableResidentialSkills( residenceId: number ): Array<Skill> {
        if ( !this.residenceSkillTree[ residenceId ] ) {
            this.residenceSkillTree[ residenceId ] = _.reduce( DataManager.getSkillTreesData().getPledgeSkillTree(), ( allSkills: Array<Skill>, skill: L2SkillLearn ) => {
                if ( skill.isResidentialSkill() && skill.getResidenceIds().includes( residenceId ) ) {
                    let currentSkill = this.getSkill( skill.getSkillId(), skill.getSkillLevel() )
                    if ( currentSkill ) {
                        allSkills.push( currentSkill )
                    }
                }

                return allSkills
            }, [] )
        }

        return this.residenceSkillTree[ residenceId ]
    }

    getAvailableSkills( player: L2PcInstance, classId: number, includeFSSkills: boolean, includeAutoGet: boolean, holder: ISkillCapable ): Array<L2SkillLearn> {
        let result = []
        let skillMap = this.getCompleteClassSkillTree( classId )

        _.each( skillMap, ( skill: L2SkillLearn ) => {
            if ( skill.getSkillId() === CommonSkillIds.DivineInspiration
                    && !ConfigManager.character.autoLearnDivineInspiration()
                    && includeAutoGet
                    && !player.isGM() ) {
                return
            }

            if ( ( ( includeAutoGet && skill.isAutoGet() )
                    || skill.isLearnedByNpc()
                    || ( includeFSSkills && skill.isLearnedByFS() ) ) && ( player.getLevel() >= skill.getGetLevel() ) ) {
                let oldSkill: Skill = holder.getKnownSkill( skill.getSkillId() )
                if ( oldSkill ) {

                    if ( ( oldSkill.getLevel() === ( skill.getSkillLevel() - 1 ) ) ) {
                        result.push( skill )
                    }
                    return
                }

                if ( skill.getSkillLevel() === 1 ) {
                    result.push( skill )
                }
            }
        } )


        return result
    }

    getClassSkill( skillId: number, level: number, classId: number ): L2SkillLearn {
        let hashCode = DataManager.getSkillData().getHashCode( skillId, level )

        let skillMap = this.getCompleteClassSkillTree( classId )
        return skillMap[ hashCode ]
    }

    getCollectSkill( id: number, level: number ): L2SkillLearn {
        let skillHashCode = this.getSkillHashCode( id, level )
        return DataManager.getSkillTreesData().getCollectSkillTree()[ skillHashCode ]
    }

    getCollectSkillTree(): { [ key: number ]: L2SkillLearn } {
        return DataManager.getSkillTreesData().getCollectSkillTree()
    }

    getCompleteClassSkillTree( playerClassId: number ): CompleteClassTreeItem {
        if ( !this.completeClassTree[ playerClassId ] ) {
            this.completeClassTree[ playerClassId ] = this.generateCompleteClassSkillTree( playerClassId )
        }

        return this.completeClassTree[ playerClassId ]
    }

    private generateCompleteClassSkillTree( playerClassId: number ): CompleteClassTreeItem {
        let classHierarchy: Array<number> = []
        let classId = playerClassId
        while ( _.isNumber( classId ) ) {
            classHierarchy.push( classId )
            classId = DataManager.getSkillTreesData().getParentClassMap()[ classId ]
        }

        let results: Array<{ [ key: number ]: L2SkillLearn }> = _.map( classHierarchy, ( treeId: number ) => {
            return DataManager.getSkillTreesData().getClassSkillTrees()[ treeId ]
        } )

        results.push( DataManager.getSkillTreesData().getCommonSkillTree() )

        return _.merge( {}, ...results )
    }

    getCompleteClassSkillIdMap( playerClassId: number ): CompleteClassMapItem {
        if ( !this.completeClassMap[ playerClassId ] ) {
            this.completeClassMap[ playerClassId ] = this.generateCompleteClassSkillIdMap( playerClassId )
        }

        return this.completeClassMap[ playerClassId ]
    }

    private generateCompleteClassSkillIdMap( playerClassId: number ): CompleteClassMapItem {
        let classHierarchy: Array<number> = []
        let classId = playerClassId
        while ( _.isNumber( classId ) ) {
            classHierarchy.push( classId )
            classId = DataManager.getSkillTreesData().getParentClassMap()[ classId ]
        }

        let results: Array<{ [ key: number ]: L2SkillLearn }> = _.map( classHierarchy, ( treeId: number ) => {
            return DataManager.getSkillTreesData().getClassSkillTrees()[ treeId ]
        } )

        results.push( DataManager.getSkillTreesData().getCommonSkillTree() )

        return results.reduce( ( total: CompleteClassMapItem, currentSkills: { [ hashCode: number ]: L2SkillLearn } ) => {
            _.each( currentSkills, ( skill: L2SkillLearn ) => {
                let possibleSkills = total[ skill.getSkillId() ]
                if ( !possibleSkills ) {
                    total[ skill.getSkillId() ] = [ skill ]
                    return total
                }

                possibleSkills.push( skill )
            } )

            return total
        }, {} )
    }

    getFishingSkill( id: number, level: number ): L2SkillLearn {
        let skillHashCode = this.getSkillHashCode( id, level )
        return DataManager.getSkillTreesData().getFishingSkillTree()[ skillHashCode ]
    }

    getHeroSkills(): Array<Skill> {
        return this.heroSkills
    }

    getMaxLevel( skillId: number ): number {
        let level = DataManager.getSkillData().getSkillsAtMaxLevel()[ skillId ]
        if ( !level ) {
            return 0
        }

        return level
    }

    getNobleSkills(): Array<Skill> {
        return this.nobleSkills
    }

    getPledgeSkill( id: number, level: number ): L2SkillLearn {
        let skillHashCode = this.getSkillHashCode( id, level )
        return DataManager.getSkillTreesData().getPledgeSkillTree()[ skillHashCode ]
    }

    getSiegeSkills( noble: boolean, hasCastle: boolean ): Array<Skill> {
        let siegeSkills = [ 246, 247 ]

        if ( noble ) {
            siegeSkills.push( 326 )
        }

        if ( hasCastle ) {
            siegeSkills.push( 844 )
            siegeSkills.push( 845 )
        }

        return _.map( siegeSkills, ( skillId ) => {
            let hashId = DataManager.getSkillData().getHashCode( skillId, 1 )
            return DataManager.getSkillData().getAllHashedSkills()[ hashId ]
        } )
    }

    getSkill( skillId: number, skillLevel: number ): Skill {
        let id = DataManager.getSkillData().getHashCode( skillId, skillLevel )
        let skill = DataManager.getSkillData().getAllHashedSkills()[ id ]

        if ( !skill ) {
            let maxLevel = this.getMaxLevel( skillId )
            if ( maxLevel > 0 && skillLevel > maxLevel ) {
                let maxSkillId = DataManager.getSkillData().getHashCode( skillId, maxLevel )
                return DataManager.getSkillData().getAllHashedSkills()[ maxSkillId ]
            }

            return null
        }

        return skill
    }

    getLearnedSkill( value: L2SkillLearn ): Skill {
        return this.getSkill( value.getSkillId(), value.getSkillLevel() )
    }

    getSkillHashCode( id: number, level: number ): number {
        return DataManager.getSkillData().getHashCode( id, level )
    }

    getSkillLearn( type: AcquireSkillType, id: number, level: number, player: L2PcInstance ): L2SkillLearn {
        switch ( type ) {
            case AcquireSkillType.Class:
                return this.getClassSkill( id, level, player.getLearningClass() )
            case AcquireSkillType.Transform:
                return this.getTransformSkill( id, level )
            case AcquireSkillType.Fishing:
                return this.getFishingSkill( id, level )
            case AcquireSkillType.Pledge:
                return this.getPledgeSkill( id, level )
            case AcquireSkillType.SubPledge:
                return this.getSubPledgeSkill( id, level )
            case AcquireSkillType.Transfer:
                return this.getTransferSkill( id, level, player.getClassId() )
            case AcquireSkillType.Subclass:
                return this.getSubClassSkill( id, level )
            case AcquireSkillType.Collect:
                return this.getCollectSkill( id, level )
        }

        return null
    }

    getSubClassSkill( id: number, level: number ): L2SkillLearn {
        let skillHashCode = this.getSkillHashCode( id, level )
        return DataManager.getSkillTreesData().getSubClassSkillTree()[ skillHashCode ]
    }

    getSubPledgeSkill( id: number, level: number ): L2SkillLearn {
        let skillHashCode = this.getSkillHashCode( id, level )
        return DataManager.getSkillTreesData().getSubPledgeSkillTree()[ skillHashCode ]
    }

    getTransferSkill( id: number, level: number, classId: number ): L2SkillLearn {
        let parentClassName = ClassId.getClassIdByIdentifier( classId ).parent
        if ( parentClassName ) {
            let parentClass = ClassId.getClassId( parentClassName )
            let skillMap = DataManager.getSkillTreesData().getTransferSkillTrees()[ parentClass.id ]
            if ( skillMap ) {
                let hashCode = DataManager.getSkillData().getHashCode( id, level )
                return skillMap[ hashCode ]
            }
        }

        return null
    }

    getTransformSkill( id: number, level: number ): L2SkillLearn {
        let skillHashCode = this.getSkillHashCode( id, level )
        return DataManager.getSkillTreesData().getTransformSkillTree()[ skillHashCode ]
    }

    isClanSkill( id: number, level: number ): boolean {
        let hashCode = DataManager.getSkillData().getHashCode( id, level )
        return !!DataManager.getSkillTreesData().getPledgeSkillTree()[ hashCode ] || !!DataManager.getSkillTreesData().getSubPledgeSkillTree()[ hashCode ]
    }

    isEnchantable( skillId: number ): boolean {
        return DataManager.getSkillData().getEnchantableSkillIds().has( skillId )
    }

    isGMSkill( id: number, level: number = 0 ): boolean {
        if ( level <= 0 ) {
            return !!_.find( DataManager.getSkillTreesData().getGameMasterSkillTree(), { skillId: id } )
                    || !!_.find( DataManager.getSkillTreesData().getGameMasterAuraSkillTree(), { skillId: id } )
        }

        let hashCode = DataManager.getSkillData().getHashCode( id, level )
        return !!DataManager.getSkillTreesData().getGameMasterSkillTree()[ hashCode ] || !!DataManager.getSkillTreesData().getGameMasterAuraSkillTree()[ hashCode ]
    }

    isHeroSkill( id: number, level: number ): boolean {
        let hashCode = DataManager.getSkillData().getHashCode( id, level )
        if ( DataManager.getSkillTreesData().getNobleSkillTree()[ hashCode ] ) {
            return true
        }

        return _.some( DataManager.getSkillTreesData().getHeroSkillTree(), ( skill: L2SkillLearn ) => {
            return skill.getSkillId() === id && level === -1
        } )
    }

    isSkillAllowed( player: L2PcInstance, skill: Skill ): boolean {
        if ( skill.isExcludedFromCheck() || ( player.isGM() && skill.isGMSkill() ) ) {
            return true
        }

        let maxLevel = SkillCache.getMaxLevel( skill.getId() )
        let currentLevel = Math.min( skill.getLevel(), maxLevel )
        let hashCode = DataManager.getSkillData().getHashCode( skill.getId(), currentLevel )

        if ( this.skillsByClassIdHashCodes[ player.getClassId() ].includes( hashCode )
                || this.skillsByRaceHashCodes[ player.getRace() ].includes( hashCode )
                || this.allSkillsHashCodes.includes( hashCode ) ) {
            return true
        }

        return !!this.getTransferSkill( skill.getId(), currentLevel, player.getClassId() )
    }

    async load(): Promise<Array<string>> {
        this.skillsByClassIdHashCodes = {}
        this.skillsByRaceHashCodes = {}
        this.allSkillsHashCodes = []

        this.generateMappings()

        this.heroSkills = _.compact( _.map( DataManager.getSkillTreesData().getHeroSkillTree(), this.getLearnedSkill.bind( this ) ) )
        this.nobleSkills = _.compact( _.map( DataManager.getSkillTreesData().getNobleSkillTree(), this.getLearnedSkill.bind( this ) ) )

        return [
            `SkillCache generated ${ _.size( this.skillsByClassIdHashCodes ) } skill to class mappings`,
            `SkillCache generated ${ _.size( this.skillsByRaceHashCodes ) } skill to race mappings`,
            `SkillCache generated ${ _.size( this.allSkillsHashCodes ) } hash codes`,
        ]
    }

    getMinimumLevelForNewSkill( player: L2PcInstance, skillMap: { [ key: number ]: L2SkillLearn } ): number {
        let level = Number.MAX_SAFE_INTEGER
        _.each( skillMap, ( skill: L2SkillLearn ) => {
            if ( skill.isLearnedByNpc() && player.getLevel() < skill.getGetLevel() ) {
                level = Math.min( level, skill.getGetLevel() )
                return
            }
        } )

        if ( level === Number.MAX_SAFE_INTEGER ) {
            return 0
        }

        return level
    }

    getAvailableSubPledgeSkills( clan: L2Clan ): Array<L2SkillLearn> {
        return _.filter( DataManager.getSkillTreesData().getSubPledgeSkillTree(), ( skill: L2SkillLearn ) => {
            return clan.getLevel() >= skill.getGetLevel() && clan.isLearnableSubSkill( skill.getSkillId(), skill.getSkillLevel() )
        } )
    }

    getAvailablePledgeSkills( clan: L2Clan ): Array<L2SkillLearn> {
        return _.filter( DataManager.getSkillTreesData().getPledgeSkillTree(), ( skill: L2SkillLearn ) => {
            let oldSkill: Skill = clan.getSkills()[ skill.getSkillId() ]
            if ( oldSkill && ( oldSkill.getLevel() + 1 ) === skill.getSkillLevel() ) {
                return true
            }

            return skill.getSkillLevel() === 1
        } )
    }

    getAvailableTransferSkills( player: L2PcInstance ): Array<L2SkillLearn> {
        let classId: number = player.getClassId()

        if ( ClassId.getLevelByClassId( player.getClassId() ) === 3 ) {
            classId = ClassId.getClassId( ClassId.getClassIdByIdentifier( classId ).parent ).id
        }

        let transferSkillTree: { [ key: number ]: L2SkillLearn } = DataManager.getSkillTreesData().getTransferSkillTrees()[ classId ]
        if ( !transferSkillTree ) {
            return null
        }

        return _.reduce( transferSkillTree, ( allSkills: Array<L2SkillLearn>, skill: L2SkillLearn ) => {
            if ( !player.getKnownSkill( skill.getSkillId() ) ) {
                allSkills.push( skill )
            }

            return allSkills
        }, [] )
    }

    getAvailableFishingSkills( player: L2PcInstance ): Array<L2SkillLearn> {
        let playerRace: Race = player.getRace()

        return _.reduce( DataManager.getSkillTreesData().getFishingSkillTree(), ( allSkills: Array<L2SkillLearn>, skill: L2SkillLearn ) => {
            if ( skill.getRaces().length > 0 && !skill.getRaces().includes( playerRace ) ) {
                return allSkills
            }

            if ( skill.isLearnedByNpc() && player.getLevel() >= skill.getGetLevel() ) {
                let oldSkill: Skill = player.getSkills()[ skill.getSkillId() ]
                if ( oldSkill ) {
                    if ( oldSkill.getLevel() === ( skill.getSkillLevel() - 1 ) ) {
                        allSkills.push( skill )
                    }
                } else if ( skill.getSkillLevel() === 1 ) {
                    allSkills.push( skill )
                }
            }

            return allSkills
        }, [] )
    }

    getAvailableSubClassSkills( player: L2PcInstance ): Array<L2SkillLearn> {
        return _.reduce( DataManager.getSkillTreesData().getSubClassSkillTree(), ( allSkills: Array<L2SkillLearn>, skill: L2SkillLearn ) => {

            if ( player.getLevel() > skill.getGetLevel() ) {
                _.each( player.getSubClasses(), ( subClass: SubClass ) => {
                    let subClassConds = skill.getSubClassConditions()
                    if ( !_.isEmpty( subClassConds )
                            && subClass.getClassIndex() <= subClassConds.length
                            && subClass.getClassIndex() === subClassConds[ subClass.getClassIndex() - 1 ].slot
                            && subClassConds[ subClass.getClassIndex() - 1 ].level <= subClass.getLevel() ) {

                        let oldSkill = player.getSkills()[ skill.getSkillId() ]
                        if ( oldSkill ) {
                            if ( oldSkill.getLevel() === ( skill.getSkillLevel() - 1 ) ) {
                                allSkills.push( skill )
                                return false
                            }
                        } else if ( skill.getSkillLevel() === 1 ) {
                            allSkills.push( skill )
                            return false
                        }
                    }
                } )
            }

            return allSkills
        }, [] )
    }

    getAvailableTransformSkills( player: L2PcInstance ): Array<L2SkillLearn> {
        return _.reduce( DataManager.getSkillTreesData().getTransformSkillTree(), ( allSkills: Array<L2SkillLearn>, skill: L2SkillLearn ) => {
            if ( player.getLevel() >= skill.getGetLevel()
                    && skill.getRaces().includes( player.getRace() ) ) {

                let oldSkill: Skill = player.getSkills()[ skill.getSkillId() ]
                if ( oldSkill ) {
                    if ( oldSkill.getLevel() === ( skill.getSkillLevel() - 1 ) ) {
                        allSkills.push( skill )
                    }
                } else if ( skill.getSkillLevel() === 1 ) {
                    allSkills.push( skill )
                }
            }

            return allSkills
        }, [] )
    }
}

export const SkillCache = new CacheManager()