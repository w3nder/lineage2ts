import { ItemLocation } from '../enums/ItemLocation'
import { L2World } from '../L2World'
import { IDFactoryCache } from './IDFactoryCache'
import { DatabaseManager } from '../../database/manager'
import { L2Object } from '../models/L2Object'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { L2Item } from '../models/items/L2Item'
import { DataManager } from '../../data/manager'
import { InstanceType } from '../enums/InstanceType'
import { L2Attackable } from '../models/actor/L2Attackable'
import { ConfigManager } from '../../config/ConfigManager'
import { L2EventMonsterInstance } from '../models/actor/instance/L2EventMonsterInstance'
import { ListenerCache } from './ListenerCache'
import { EventType, ItemCreateEvent } from '../models/events/EventType'
import { EventPoolCache } from './EventPoolCache'
import { ItemProtectionCache } from './ItemProtectionCache'
import { ItemTypes } from '../values/InventoryValues'
import { InventoryUpdateStatus } from '../enums/InventoryUpdateStatus'
import { RoaringBitmap32 } from 'roaring'
import _, { DebouncedFunc } from 'lodash'
import Timeout = NodeJS.Timeout

function getItemExpirationDuration( item: L2ItemInstance ): number {
    if ( item.getId() === ItemTypes.Adena ) {
        return ConfigManager.tuning.getAutoDestroyAdenaMillis()
    }

    if ( item.isArmor() ) {
        return ConfigManager.tuning.getAutoDestroyArmorMillis()
    }

    if ( item.isWeapon() ) {
        return ConfigManager.tuning.getAutoDestroyWeaponMillis()
    }

    if ( item.getItem().isRecipe() ) {
        return ConfigManager.tuning.getAutoDestroyRecipeMillis()
    }

    if ( item.getItem().hasExImmediateEffect() ) {
        return ConfigManager.tuning.getAutoDestroyHerbMillis()
    }

    return ConfigManager.tuning.getAutoDestroyOtherMillis()
}

class Manager {
    idsToUpdate: RoaringBitmap32 = new RoaringBitmap32()
    idsToAutoDestroy: RoaringBitmap32 = new RoaringBitmap32()
    debounceItemUpdate: DebouncedFunc<() => void>
    autoDestroyTask: Timeout

    constructor() {
        /*
            TODO: Further optimize times for item update.
            Particular consideration must be taken for deleted items, since these must be removed as soon
            as possible.
         */
        this.debounceItemUpdate = _.debounce( this.runItemUpdatesTask.bind( this ), 3000, {
            maxWait: 10000
        } )
    }

    scheduleItemUpdate( item: L2ItemInstance ): void {
        this.idsToUpdate.add( item.getObjectId() )
        this.debounceItemUpdate()
    }

    unScheduleItemUpdate( item: L2ItemInstance ): void {
        this.idsToUpdate.remove( item.getObjectId() )
    }

    autoDestroyItem( item: L2ItemInstance ): void {
        if ( ConfigManager.general.getProtectedItems().has( item.getId() ) ) {
            return
        }

        if ( getItemExpirationDuration( item ) > 0 ) {
            item.setDropTime( Date.now() )
            this.idsToAutoDestroy.add( item.getObjectId() )
            this.startAutoDestroyTask()
        }
    }

    startAutoDestroyTask(): void {
        if ( !this.autoDestroyTask ) {
            this.autoDestroyTask = setInterval( this.runAutoDestroyTask.bind( this ), Math.max( 10, ConfigManager.tuning.getAutoDestroyInterval() ) * 1000 )
        }
    }

    stopAutoDestroyTask(): void {
        if ( this.autoDestroyTask ) {
            clearInterval( this.autoDestroyTask )
            this.autoDestroyTask = null
        }
    }

    runAutoDestroyTask(): void {
        if ( this.idsToAutoDestroy.size === 0 ) {
            return this.stopAutoDestroyTask()
        }

        let currentTime: number = Date.now()
        let idsToRemove: RoaringBitmap32 = new RoaringBitmap32()

        this.idsToAutoDestroy.forEach( ( objectId: number ) => {
            if ( ItemProtectionCache.isProtected( objectId ) ) {
                return
            }

            let item = L2World.getObjectById( objectId ) as L2ItemInstance

            if ( !item
                    || item.getDropTime() === 0
                    || item.getItemLocation() !== ItemLocation.VOID ) {
                idsToRemove.add( objectId )
                return
            }

            let timePeriod: number = currentTime - item.getDropTime()

            if ( timePeriod > getItemExpirationDuration( item ) ) {
                item.resetProperties()

                L2World.removeObject( item )
                IDFactoryCache.releaseId( objectId )
                idsToRemove.add( objectId )

                return
            }
        } )

        this.idsToAutoDestroy.removeMany( idsToRemove )
    }

    runItemUpdatesTask(): Promise<Array<void>> {
        if ( this.idsToUpdate.size === 0 ) {
            return
        }

        let items: Array<L2ItemInstance> = []

        for ( const currentItemId of this.idsToUpdate ) {
            let currentItem = L2World.getObjectById( currentItemId ) as L2ItemInstance
            if ( currentItem ) {
                items.push( currentItem )
            }
        }

        this.idsToUpdate.clear()
        return this.processDatabaseOperations( items )
    }

    destroyItem( item: L2ItemInstance ): Promise<void> {
        this.unScheduleItemUpdate( item )

        item.setInventoryStatus( InventoryUpdateStatus.Removed )
        item.resetProperties()

        L2World.removeObject( item )
        IDFactoryCache.releaseId( item.getObjectId() )

        return this.deleteItems( [ item ] )
    }

    createItem( itemId: number, reason: string, amount: number, playerId: number ): L2ItemInstance {
        let item: L2ItemInstance = new L2ItemInstance( IDFactoryCache.getNextId(), itemId )

        if ( item.isStackable() && amount > 1 ) {
            item.setCount( amount )
        }

        if ( ListenerCache.hasItemTemplateEvent( itemId, EventType.ItemCreate ) ) {
            let eventData = EventPoolCache.getData( EventType.ItemCreate ) as ItemCreateEvent

            eventData.reason = reason
            eventData.itemObjectId = item.getObjectId()
            eventData.playerId = playerId
            eventData.amount = item.isStackable() ? amount : 1

            ListenerCache.sendItemTemplateEvent( item.getId(), EventType.ItemCreate, eventData )
        }

        return item
    }

    createLootItem( itemId: number, reason: string, count: number, ownerObjectId: number, droppedBy: L2Object ): L2ItemInstance {
        let item: L2ItemInstance = this.createItem( itemId, reason, count, ownerObjectId )

        if ( droppedBy && droppedBy.isAttackable() && ( droppedBy as L2Attackable ).isRaid() ) {
            let raidCharacter: L2Attackable = droppedBy as L2Attackable
            if ( raidCharacter.getFirstCommandChannelAttacked() && !ConfigManager.character.autoLootRaids() ) {
                item.setOwnerId( raidCharacter.getFirstCommandChannelAttacked().getLeaderObjectId() )
                ItemProtectionCache.addProtection( item.getObjectId(), true )
            }

            return item
        }

        if ( !ConfigManager.character.autoLoot() || ( droppedBy && droppedBy.isInstanceType( InstanceType.L2EventMonsterInstance ) && ( droppedBy as L2EventMonsterInstance ).eventDropOnGround() ) ) {
            item.setOwnerId( ownerObjectId )
            ItemProtectionCache.addProtection( item.getObjectId(), false )
        }

        return item
    }

    getTemplate( id: number ): L2Item {
        return DataManager.getItems().getTemplate( id )
    }

    async deleteItems( items: Array<L2ItemInstance> ) {
        let objectIds: Array<number> = items.map( item => item.getObjectId() )

        await this.deleteItemIds( objectIds )
        let petControlItems = items.reduce( ( ids: Array<number>, item: L2ItemInstance ): Array<number> => {
            if ( item.getItem().isPetItem() ) {
                ids.push( item.getObjectId() )
            }

            return ids
        }, [] )

        if ( petControlItems.length > 0 ) {
            await DatabaseManager.getPets().deleteManyByControlId( petControlItems )
        }

        items.forEach( ( item: L2ItemInstance ) => {
            item.existsInDb = false

            item.resetProperties()
        } )
    }

    private deleteItemIds( objectIds: Array<number> ): Promise<unknown> {
        return Promise.all( [
            DatabaseManager.getItems().deleteByObjectIds( objectIds ),
            DatabaseManager.getItemElementalsTable().deleteByObjectIds( objectIds ),
        ] )
    }

    async insertItems( items: Array<L2ItemInstance> ) {
        await Promise.all( [
            DatabaseManager.getItems().insertItems( items ),
            DatabaseManager.getItemElementalsTable().updateItems( items )
        ] )

        items.forEach( ( currentItem: L2ItemInstance ) => {
            currentItem.existsInDb = true
        } )
    }

    async updateItems( items: Array<L2ItemInstance> ) {
        await DatabaseManager.getItems().updateItems( items )

        items.forEach( ( currentItem: L2ItemInstance ) => {
            currentItem.existsInDb = true
        } )
    }

    processDatabaseOperations( items: ReadonlyArray<L2ItemInstance> ): Promise<Array<void>> {
        let toCreate: Array<L2ItemInstance> = []
        let toUpdate: Array<L2ItemInstance> = []
        let toDelete: Array<L2ItemInstance> = []

        for ( const currentItem of items ) {
            if ( !currentItem ) {
                continue
            }

            if ( currentItem.shouldDelete() ) {
                toDelete.push( currentItem )
                continue
            }

            if ( currentItem.shouldCreate() ) {
                toCreate.push( currentItem )
                continue
            }

            if ( currentItem.shouldUpdate() ) {
                toUpdate.push( currentItem )
                currentItem.resetInventoryStatus()
                continue
            }
        }

        let actions: Array<Promise<void>> = []
        if ( toDelete.length > 0 ) {
            actions.push( this.deleteItems( toDelete ) )
        }

        if ( toCreate.length > 0 ) {
            actions.push( this.insertItems( toCreate ) )
        }

        if ( toUpdate.length > 0 ) {
            actions.push( this.updateItems( toUpdate ) )
        }

        return Promise.all( actions )
    }

    performAllItemsUpdate() : Promise<Array<void>> {
        this.debounceItemUpdate.cancel()

        return this.runItemUpdatesTask()
    }
}

export const ItemManagerCache = new Manager()