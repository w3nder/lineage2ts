import { NpcItemAuction } from '../models/auction/NpcItemAuction'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { DatabaseManager } from '../../database/manager'
import { L2ItemAuctionDataItem } from '../../data/interface/ItemAuctionDataApi'
import { DataManager } from '../../data/manager'
import { L2ItemAuctionTableData } from '../../database/interface/ItemAuctionTableApi'
import { ItemAuction } from '../models/auction/ItemAuction'
import { ItemAuctionState } from '../models/auction/ItemAuctionState'
import { L2ItemAuctionBidTableData } from '../../database/interface/ItemAuctionBidTableApi'
import { ItemAuctionBid } from '../models/auction/ItemAuctionBid'
import aigle from 'aigle'
import _ from 'lodash'

class Manager implements L2DataApi {
    auctions: { [ key: number ] : NpcItemAuction }
    currentAuctionId: number

    getAuction( id: number ) : NpcItemAuction {
        return this.auctions[ id ]
    }

    async load(): Promise<Array<string>> {
        let lastId = await DatabaseManager.getItemAuctions().getLastId()

        this.currentAuctionId = _.defaultTo( lastId, 0 ) + 1
        this.auctions = {}

        let itemData : Array<L2ItemAuctionDataItem> = await DataManager.getItemAuctions().getAll()
        let npcData : { [ npcId: string ] : Array<L2ItemAuctionDataItem> } = _.groupBy( itemData, 'npcId' )

        let manager = this
        let auctionIdsToRemove : Array<number> = []
        let auctionAutoExpirationTime : number = Date.now()
        let errorMessages : Array<string> = []

        let existingAuctionItems : Array<L2ItemAuctionTableData> = await DatabaseManager.getItemAuctions().getAuctions( _.keys( npcData ).map( value => _.parseInt( value ) ) )
        let existingAuctionData : { [ npcId: number ] : Array<L2ItemAuctionTableData> } = _.groupBy( existingAuctionItems, 'npcId' )

        let auctionIds : Array<number> = _.uniq( _.map( existingAuctionItems, 'id' ) )
        let bidAuctionItems : Array<L2ItemAuctionBidTableData> = await DatabaseManager.getItemAuctionBids().getAuctionBids( auctionIds )
        let bidAuctionData : { [ auctionId: number ] : Array<L2ItemAuctionBidTableData> } = _.groupBy( bidAuctionItems, 'id' )

        await aigle.resolve( npcData ).each( async ( dataItems: Array<L2ItemAuctionDataItem>, npcId: string ) : Promise<void> => {
            let npcTemplateId : number = _.parseInt( npcId )

            if ( !DataManager.getNpcData().getTemplate( npcTemplateId ) ) {
                errorMessages.push( `ItemAuctionManager : bad static auction data for npcId = ${npcId}` )
                return
            }

            let npcAuction = new NpcItemAuction( npcTemplateId )

            npcAuction.auctionDataItems = dataItems

            let npcExistingAuctions : Array<ItemAuction> = await aigle.resolve( existingAuctionData[ npcId ] ).reduce( async ( allAuctions : Array<ItemAuction>, item: L2ItemAuctionTableData ) : Promise<Array<ItemAuction>> => {
                if ( item.startTime >= item.endTime
                        || _.isUndefined( ItemAuctionState[ item.state ] )
                        || item.state === ItemAuctionState.FINISHED ) {
                    auctionIdsToRemove.push( item.id )
                    return allAuctions
                }

                let itemData : L2ItemAuctionDataItem = _.find( dataItems, { auctionId: item.auctionDataId } )
                if ( !itemData ) {
                    auctionIdsToRemove.push( item.id )
                    return allAuctions
                }

                let bids : Array<ItemAuctionBid> = _.map( bidAuctionData[ item.id ], ( bidData: L2ItemAuctionBidTableData ) : ItemAuctionBid => {
                    return {
                        lastBid: bidData.amount,
                        playerId: bidData.playerId
                    }
                } )

                let isExpiredAuction : boolean = item.state === ItemAuctionState.STARTED && item.endTime < auctionAutoExpirationTime
                if ( isExpiredAuction && bids.length === 0 ) {
                    auctionIdsToRemove.push( item.id )
                    return allAuctions
                }

                let itemAuction = new ItemAuction( item.id,
                        npcAuction.npcId,
                        item.startTime,
                        item.endTime,
                        itemData,
                        bids,
                        item.state )

                if ( isExpiredAuction ) {
                    /*
                        Auction has bids but is expired, hence highest bidder now will receive reward.
                        Finishing auction should remove auction data from database.
                     */
                    await npcAuction.onAuctionFinished( itemAuction )
                    return allAuctions
                }

                allAuctions.push( itemAuction )

                return allAuctions
            }, [] )

            manager.auctions[ npcId ] = npcAuction

            return npcAuction.prepareAuction( npcExistingAuctions )
        } )

        if ( auctionIdsToRemove.length > 0 ) {
            await DatabaseManager.getItemAuctions().deleteAuctions( auctionIdsToRemove )
            await DatabaseManager.getItemAuctionBids().deleteAuctions( auctionIdsToRemove )
        }

        let totalItems : Array<number> = _.map( this.auctions, ( auction : NpcItemAuction ) : number => {
            return _.size( auction.auctionDataItems )
        } )

        let ongoingAuctions : Array<number> = _.map( this.auctions, ( auction : NpcItemAuction ) : number => {
            return auction.currentAuction ? 1 : 0
        } )

        let otherMessages : Array<string> = [
            `ItemAuctionManager : loaded ${_.size( this.auctions )} static NPC auction sets.`,
            `ItemAuctionManager : all auctions have total ${_.sum( totalItems )} items.`,
            `ItemAuctionManager : total ${_.sum( ongoingAuctions )} ongoing auctions.`,
            `ItemAuctionManager : removed ${ auctionIdsToRemove.length } expired auctions.`,
        ]

        return _.concat( errorMessages, otherMessages )
    }

    getNextAuctionId() : number {
        return this.currentAuctionId++
    }
}

export const ItemAuctionManager = new Manager()