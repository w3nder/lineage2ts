import _ from 'lodash'

export interface PlayerEventStatus {
    name: string
    isBlockingExit : boolean
    isBlockingDeathPenalty : boolean
    canRevive : boolean
}

class Manager {
    playerStatus : { [ key: number ] : Array<PlayerEventStatus> } = {}

    getPlayerStatus( objectId: number ) : Array<PlayerEventStatus> {
        return this.playerStatus[ objectId ]
    }

    // TODO : only used in TvT for now
    addPlayerStatus( objectId: number, status : PlayerEventStatus ) : void {
        if ( !this.playerStatus[ objectId ] ) {
            this.playerStatus[ objectId ] = []
        }

        this.playerStatus[ objectId ].push( status )
    }

    removePlayerStatus( objectId: number, name: string ) : void {
        if ( !this.playerStatus[ objectId ] ) {
            return
        }

        _.remove( this.playerStatus[ objectId ], ( status : PlayerEventStatus ) => status.name !== name )
    }

    removeAll( objectId: number ) : void {
        _.unset( this.playerStatus, objectId )
    }
}

export const PlayerEventStatusCache = new Manager()