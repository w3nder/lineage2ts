import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2FishingChampionshipTableItem } from '../../database/interface/FishingChampionshipTableApi'
import { DatabaseManager } from '../../database/manager'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { DataManager } from '../../data/manager'
import { ItemManagerCache } from './ItemManagerCache'
import { L2Item } from '../models/items/L2Item'
import { NpcHtmlMessagePath } from '../packets/send/NpcHtmlMessage'
import { L2Npc } from '../models/actor/L2Npc'
import { CharacterNamesCache } from './CharacterNamesCache'
import parser from 'cron-parser'
import { MinHeap } from '@datastructures-js/heap'
import aigle from 'aigle'
import _ from 'lodash'
import moment from 'moment'
import { MsInDays } from '../enums/MsFromTime'
import { ServerLog } from '../../logger/Logger'
import Timeout = NodeJS.Timeout

const midResultTemplate: string = `
<tr><td width=70 align=center>%position%</td>
<td width=110 align=center>%name%</td>
<td width=80 align=center>%length%</td></tr>
`

const otherRewardsTemplate: string = `
<tr>
<td width=70 align=center>Other places</td>
<td width=110 align=center>%prizeItem%</td>
<td width=80 align=center>%prizeAmount%</td>
</tr>
`

const htmlLastResultsPath = 'overrides/html/fisherman/championship/last-results.htm'
const htmlMidResultsPath = 'overrides/html/fisherman/championship/current-results.htm'

class Manager implements L2DataApi {
    consideredPlayers: { [ playerId: number ]: L2FishingChampionshipTableItem } = {}
    winingPlayers: Array<L2FishingChampionshipTableItem>

    endDate: number = 0
    endChamionshipTask: Timeout

    winningLadder: MinHeap<L2FishingChampionshipTableItem> = new MinHeap<L2FishingChampionshipTableItem>( ( item: L2FishingChampionshipTableItem ) => item.fishLength )

    midResultHtml: string
    midResultsTask: Timeout

    championResultHtml: string

    async addNewFish( player: L2PcInstance, lureId: number ): Promise<void> {
        if ( !ConfigManager.general.allowFishingChampionship() ) {
            return
        }

        let length = _.random( 60, 89 ) + _.random( 1, true )
        if ( lureId >= 8484 && lureId <= 8486 ) {
            length += _.random( 3, true )
        }

        let message = new SystemMessageBuilder( SystemMessageIds.CAUGHT_FISH_S1_LENGTH )
                .addString( length.toFixed( 3 ) )
                .getBuffer()
        player.sendOwnedData( message )

        let playerData: L2FishingChampionshipTableItem = this.consideredPlayers[ player.getObjectId() ]
        if ( playerData ) {

            if ( length > playerData.fishLength ) {
                playerData.fishLength = length
                this.rebuildWinningLadder()

                await DatabaseManager.getFishingChampionship().update( playerData )
                return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.REGISTERED_IN_FISH_SIZE_RANKING ) )
            }

            return
        }

        let worstPerformer: L2FishingChampionshipTableItem = this.winningLadder.root()
        if ( worstPerformer && length <= worstPerformer.fishLength ) {
            return
        }

        let data: L2FishingChampionshipTableItem = {
            hasClaimedReward: false,
            fishLength: length,
            playerId: player.getObjectId(),
            rewardType: 0,
        }

        this.consideredPlayers[ player.getObjectId() ] = data
        this.winningLadder.insert( data )

        if ( this.winningLadder.size() > ConfigManager.general.getFishingChampionshipParticipantAmount() ) {
            this.winningLadder.extractRoot()
            _.unset( this.consideredPlayers, worstPerformer.playerId )
        }

        await DatabaseManager.getFishingChampionship().add( data )
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.REGISTERED_IN_FISH_SIZE_RANKING ) )
    }

    async generateChampionshipResultsHtml(): Promise<string> {
        let template: L2Item = ItemManagerCache.getTemplate( ConfigManager.general.getFishingChampionshipRewardItemId() )
        let values: Array<L2FishingChampionshipTableItem> = this.winingPlayers
        let characterIds = this.winingPlayers.map( item => item.playerId )
        let sortedResults: Array<string>

        if ( characterIds.length > 0 ) {
            await CharacterNamesCache.prefetchNames( characterIds )
            sortedResults = await aigle.resolve( ConfigManager.general.getFishingChampionshipParticipantAmount() ).timesLimit( 10, async ( index: number ): Promise<string> => {
                let data: L2FishingChampionshipTableItem = _.nth( values, index )
                if ( !data ) {
                    return midResultTemplate
                            .replace( '%position%', ( index + 1 ).toString() )
                            .replace( '%name%', 'None' )
                            .replace( '%length%', '0.000' )
                }

                return midResultTemplate
                        .replace( '%position%', ( index + 1 ).toString() )
                        .replace( '%name%', CharacterNamesCache.getCachedNameById( data.playerId ) )
                        .replace( '%length%', data.fishLength.toFixed( 3 ) )
            } )
        } else {
            sortedResults = _.times( ConfigManager.general.getFishingChampionshipParticipantAmount(), ( index: number ) => {
                return midResultTemplate
                        .replace( '%position%', ( index + 1 ).toString() )
                        .replace( '%name%', 'None' )
                        .replace( '%length%', '0.000' )
            } )
        }

        let otherRewards: string = ''

        if ( ConfigManager.general.getFishingChampionshipParticipantAmount() > 5 ) {
            otherRewards = otherRewardsTemplate
                    .replace( '%prizeAmount%', ConfigManager.general.getFishingChampionshipRewardOthers().toString() )
                    .replace( /%prizeItem%/g, template.getName() )
        }

        return DataManager.getHtmlData().getItem( htmlLastResultsPath )
                .replace( '%results%', sortedResults.join( '' ) )
                .replace( /%prizeItem%/g, template.getName() )
                .replace( '%prizeFirst%', _.toString( ConfigManager.general.getFishingChampionshipReward1() ) )
                .replace( '%prizeTwo%', _.toString( ConfigManager.general.getFishingChampionshipReward2() ) )

                .replace( '%prizeThree%', _.toString( ConfigManager.general.getFishingChampionshipReward3() ) )
                .replace( '%prizeFour%', _.toString( ConfigManager.general.getFishingChampionshipReward4() ) )
                .replace( '%prizeFive%', _.toString( ConfigManager.general.getFishingChampionshipReward5() ) )
                .replace( '%interval%', this.getIntervalWording() )

                .replace( '%otherReward%', otherRewards )
    }

    getIntervalWording(): string {
        let nextAuctionTime = parser.parseExpression( ConfigManager.general.getFishingChampionshipIntervalCron() )
        let timeDifference = nextAuctionTime.next().toDate().valueOf() - nextAuctionTime.prev().toDate().valueOf()
        let days: number = Math.floor( timeDifference / MsInDays.One )

        return `${ days } days`
    }

    getNextEndDate(): number {
        let nextAuctionTime = parser.parseExpression( ConfigManager.general.getFishingChampionshipIntervalCron() )
        return nextAuctionTime.next().toDate().valueOf()
    }

    getWinningPlace( playerId: number ): number {
        return 1 + _.findIndex( this.winingPlayers, ( item: L2FishingChampionshipTableItem ): boolean => {
            return item.playerId === playerId && !item.hasClaimedReward
        } )
    }

    markClaimedReward( playerId: number ): void {
        _.each( this.winingPlayers, ( item: L2FishingChampionshipTableItem ): boolean => {
            if ( item.playerId === playerId ) {
                item.hasClaimedReward = true
                DatabaseManager.getFishingChampionship().update( item )
                return false
            }
        } )
    }

    async load(): Promise<Array<string>> {
        if ( !ConfigManager.general.allowFishingChampionship() ) {
            return [
                'FishingChampionshipManager : not loaded, disabled by configuration',
            ]
        }

        this.winningLadder = new MinHeap()
        this.endDate = this.getNextEndDate()

        let playerData: Array<L2FishingChampionshipTableItem> = await DatabaseManager.getFishingChampionship().getAll()
        let partitions: Array<Array<L2FishingChampionshipTableItem>> = _.partition( playerData, { rewardType: 0 } )

        this.consideredPlayers = _.keyBy( partitions[ 0 ], 'playerId' )
        this.winingPlayers = _.sortBy( partitions[ 1 ], 'fishLength' ).reverse()

        this.rebuildWinningLadder()

        while ( this.winningLadder.size() > ConfigManager.general.getFishingChampionshipParticipantAmount() ) {
            let data: L2FishingChampionshipTableItem = this.winningLadder.extractRoot()
            _.unset( this.consideredPlayers, data.playerId )
        }

        let interval = this.endDate - Date.now()
        this.endChamionshipTask = setTimeout( this.runEndChampionshipTask.bind( this ), Math.max( 1000, interval ) )

        let template: L2Item = ItemManagerCache.getTemplate( ConfigManager.general.getFishingChampionshipRewardItemId() )

        let output: Array<string> = [
            `FishingChampionshipManager : loaded ${ _.size( this.consideredPlayers ) } competing players.`,
            `FishingChampionshipManager : loaded ${ _.size( this.winingPlayers ) } previously winning players.`,
        ]

        if ( template ) {
            output.push( `FishingChampionshipManager : reward item is '${ template.getName() }'.` )
        } else {
            output.push( 'FishingChampionshipManager : reward item cannot be determined.' )
        }

        return output
    }

    rebuildWinningLadder() {
        let manager = this
        this.winningLadder.clear()

        _.each( this.consideredPlayers, ( item: L2FishingChampionshipTableItem ) => {
            manager.winningLadder.insert( item )
        } )
    }

    resetMidResultHtml() {
        this.midResultHtml = null
        this.midResultsTask = null
    }

    runEndChampionshipTask() {
        this.winingPlayers = _.take( _.compact( this.winningLadder.sort() ), ConfigManager.general.getFishingChampionshipParticipantAmount() )

        _.each( this.winingPlayers, ( item: L2FishingChampionshipTableItem ) => {
            item.rewardType = 1
        } )

        this.winningLadder.clear()

        this.consideredPlayers = {}
        this.championResultHtml = null

        this.endDate = this.getNextEndDate()
        this.endChamionshipTask = setTimeout( this.runEndChampionshipTask.bind( this ), this.endDate - Date.now() )

        ServerLog.info( `FishingChampionshipManager : ended championship with ${ _.size( this.winingPlayers ) } winners` )
        ServerLog.info( `FishingChampionshipManager : next championship end date is ${ moment( this.endDate ).format( 'dddd, MMMM Do YYYY, h:mm a' ) }` )

        return this.saveResults()
    }

    async runMidResultGenerateHtml(): Promise<void> {
        let template: L2Item = ItemManagerCache.getTemplate( ConfigManager.general.getFishingChampionshipRewardItemId() )
        if ( !template ) {
            return
        }

        let values: Array<L2FishingChampionshipTableItem> = this.winningLadder.clone().sort()
        let characterIds = this.winingPlayers.map( item => item.playerId )
        let sortedResults: Array<string>

        if ( characterIds.length > 0 ) {
            await CharacterNamesCache.prefetchNames( characterIds )
            sortedResults = await aigle.resolve( ConfigManager.general.getFishingChampionshipParticipantAmount() ).timesLimit( 10, async ( index: number ) => {
                let item: L2FishingChampionshipTableItem = _.nth( values, index )
                if ( !item ) {
                    return midResultTemplate
                            .replace( '%position%', ( index + 1 ).toString() )
                            .replace( '%name%', 'None' )
                            .replace( '%length%', '0.000' )
                }

                return midResultTemplate
                        .replace( '%position%', ( index + 1 ).toString() )
                        .replace( '%name%', await CharacterNamesCache.getNameById( item.playerId ) )
                        .replace( '%length%', item.fishLength.toFixed( 3 ) )
            } )
        } else {
            sortedResults = _.times( ConfigManager.general.getFishingChampionshipParticipantAmount(), ( index: number ) => {
                return midResultTemplate
                        .replace( '%position%', ( index + 1 ).toString() )
                        .replace( '%name%', 'None' )
                        .replace( '%length%', '0.000' )
            } )
        }

        let otherRewards: string = ''

        if ( ConfigManager.general.getFishingChampionshipParticipantAmount() > 5 ) {
            otherRewards = otherRewardsTemplate
                    .replace( '%prizeAmount%', ConfigManager.general.getFishingChampionshipRewardOthers().toString() )
                    .replace( /%prizeItem%/g, template.getName() )
        }

        this.midResultHtml = DataManager.getHtmlData().getItem( htmlMidResultsPath )
                .replace( '%results%', sortedResults.join( '' ) )
                .replace( /%prizeItem%/g, template.getName() )
                .replace( '%prizeFirst%', _.toString( ConfigManager.general.getFishingChampionshipReward1() ) )
                .replace( '%prizeTwo%', _.toString( ConfigManager.general.getFishingChampionshipReward2() ) )

                .replace( '%prizeThree%', _.toString( ConfigManager.general.getFishingChampionshipReward3() ) )
                .replace( '%prizeFour%', _.toString( ConfigManager.general.getFishingChampionshipReward4() ) )
                .replace( '%prizeFive%', _.toString( ConfigManager.general.getFishingChampionshipReward5() ) )
                .replace( '%interval%', this.getIntervalWording() )

                .replace( '%otherReward%', otherRewards.trim() )

        this.midResultsTask = setTimeout( this.resetMidResultHtml.bind( this ), 60000 )
        await this.saveResults()
    }

    async saveResults(): Promise<void> {
        await DatabaseManager.getFishingChampionship().removeAll()

        if ( !_.isEmpty( this.winingPlayers ) ) {
            await DatabaseManager.getFishingChampionship().addAll( this.winingPlayers )
        }

        if ( !_.isEmpty( this.consideredPlayers ) ) {
            await DatabaseManager.getFishingChampionship().addAll( _.values( this.consideredPlayers ) )
        }
    }

    scheduleMidResultsTask() {
        if ( !this.midResultsTask ) {
            this.midResultsTask = setTimeout( this.runMidResultGenerateHtml.bind( this ), 10000 )
        }
    }

    async showChampionshipScreen( player: L2PcInstance, npc: L2Npc ): Promise<void> {
        let template: L2Item = ItemManagerCache.getTemplate( ConfigManager.general.getFishingChampionshipRewardItemId() )
        if ( !template ) {
            return
        }

        if ( !this.championResultHtml ) {
            this.championResultHtml = await this.generateChampionshipResultsHtml()
        }

        player.sendOwnedData( NpcHtmlMessagePath( this.championResultHtml, htmlLastResultsPath, player.getObjectId(), npc.getObjectId() ) )
    }

    showMidResult( player: L2PcInstance, npcObjectId: number = 0 ): void {
        if ( !this.midResultHtml ) {
            const calculatingPath = 'overrides/html/fisherman/championship/results-calculating.htm'
            let html: string = DataManager.getHtmlData().getItem( calculatingPath )
            player.sendOwnedData( NpcHtmlMessagePath( html, calculatingPath, player.getObjectId() ) )

            this.scheduleMidResultsTask()
            return
        }

        player.sendOwnedData( NpcHtmlMessagePath( this.midResultHtml, htmlMidResultsPath, player.getObjectId(), npcObjectId ) )
    }
}

export const FishingChampionshipManager = new Manager()