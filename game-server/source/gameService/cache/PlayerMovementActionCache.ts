import { L2PcInstance } from '../models/actor/instance/L2PcInstance'

export type PlayerMovementAction = ( player: L2PcInstance, targetX : number, targetY : number, targetZ : number ) => Promise<void> | void

class Manager {
    actions: Record<number, PlayerMovementAction> = {}

    getAction( objectId: number ) : PlayerMovementAction {
        return this.actions[ objectId ]
    }

    setAction( objectId: number, action: PlayerMovementAction ) : void {
        this.actions[ objectId ] = action
    }

    clearAction( objectId: number ) : void {
        delete this.actions[ objectId ]
    }
}

export const PlayerMovementActionCache = new Manager()