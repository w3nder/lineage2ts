import { L2DataApi } from '../../data/interface/l2DataApi'
import _ from 'lodash'
import { DataManager } from '../../data/manager'
import { L2TeleportData } from '../../data/interface/TeleportsDataApi'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'

class Manager implements L2DataApi {
    locations : { [ teleportId: number ] : L2TeleportData }

    async load(): Promise<Array<string>> {
        let records : Array<L2TeleportData> = await DataManager.getTeleports().getAll()
        let messages : Array<string> = []

        this.locations = {}

        records.forEach( ( teleport: L2TeleportData ) => {
            if ( this.locations[ teleport.id ] ) {
                messages.push( `TeleportData: redefining teleport for id = ${teleport.id}` )
            }

            this.locations[ teleport.id ] = teleport
        } )

        messages.push( `TeleportLocations : mapped ${_.size( this.locations )} locations from possible ${records.length} records.` )

        return messages
    }

    getLocation( teleportId: number ) : L2TeleportData {
        return this.locations[ teleportId ]
    }

    async teleportPlayer( player: L2PcInstance, id: number ) : Promise<void> {
        let teleport = this.getLocation( id )
        if ( !teleport ) {
            return
        }

        if ( teleport.amount > 0 && !await player.destroyItemByItemId( teleport.itemId, teleport.amount, true ) ) {
            return
        }

        return player.teleportToLocationCoordinates( teleport.x, teleport.y, teleport.z, player.getHeading() )
    }
}

export const TeleportLocationManager = new Manager()