import moment from 'moment'
import { ListenerCache } from './ListenerCache'
import { EventType, GameTimeHourEvent } from '../models/events/EventType'
import { EventPoolCache } from './EventPoolCache'
import chalk from 'chalk'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { GeneralHelper } from '../helpers/GeneralHelper'
import { ServerLog } from '../../logger/Logger'
import Timeout = NodeJS.Timeout

const enum GameTimeDuration {
    Day = 3600000 * 4,
    MorningHour = 6,
    DayEndHour = 18,
    MorningHourMinutes = 6 * 60,
    HourDurationMs = ( 3600000 * 4 ) / 24,
    MinuteDurationMs = ( 3600000 * 4 ) / ( 24 * 60 )
}

class Manager implements L2DataApi {
    referenceTime: number
    timeChangeTimer: Timeout
    isNightTime: boolean = false
    currentHour: number = 0

    constructor() {
        this.referenceTime = moment().startOf( 'day' ).valueOf()

        this.recomputeTime()
    }

    private setupHourTimer(): void {
        clearTimeout( this.timeChangeTimer )

        this.timeChangeTimer = setInterval( this.runTimeChange.bind( this ), GameTimeDuration.HourDurationMs )
        this.runTimeChange()
    }

    isNight(): boolean {
        return this.isNightTime
    }

    getHour(): number {
        return this.currentHour
    }

    private getRawMinutes(): number {
        return ( this.getGameTime() % GameTimeDuration.HourDurationMs ) / GameTimeDuration.MinuteDurationMs
    }

    getMinutes(): number {
        return Math.floor( this.getRawMinutes() )
    }

    getGameTime(): number {
        return ( Date.now() - this.referenceTime ) % GameTimeDuration.Day
    }

    getTotalMinutes(): number {
        return Math.floor( this.getGameTime() / GameTimeDuration.MinuteDurationMs )
    }

    getNextPeriodChange(): number {
        if ( this.isNight() ) {
            return Math.floor( GameTimeDuration.MorningHourMinutes - this.getTotalMinutes() ) * GameTimeDuration.MinuteDurationMs
        }

        return this.getGameTime()
    }

    private computeHour(): number {
        return Math.floor( this.getGameTime() / GameTimeDuration.HourDurationMs )
    }

    private computeNightTime(): boolean {
        return this.currentHour < GameTimeDuration.MorningHour || this.currentHour > GameTimeDuration.DayEndHour
    }

    private recomputeTime(): void {
        this.currentHour = this.computeHour()
        this.isNightTime = this.computeNightTime()
    }

    private getTimeMessage(): string {
        return `GameTime: ${ chalk.cyan( this.currentHour ) } hour, ${ this.isNightTime ? chalk.red( 'night' ) : chalk.blueBright( 'day' ) } time.`
    }

    private runTimeChange(): Promise<void> {
        this.recomputeTime()

        ServerLog.info( this.getTimeMessage() )

        if ( ListenerCache.hasGeneralListener( EventType.GameTimeHour ) ) {
            let eventData = EventPoolCache.getData( EventType.GameTimeHour ) as GameTimeHourEvent

            eventData.isNight = this.isNightTime
            eventData.gameHour = this.currentHour

            return ListenerCache.sendGeneralEvent( EventType.GameTimeHour, eventData, this.currentHour )
        }
    }

    async load(): Promise<Array<string>> {
        clearTimeout( this.timeChangeTimer )

        let duration = ( 60 - this.getRawMinutes() ) * GameTimeDuration.MinuteDurationMs
        this.timeChangeTimer = setTimeout( this.setupHourTimer.bind( this ), duration )

        return [
            this.getTimeMessage(),
            `GameTime: starting hour tracking in ${ GeneralHelper.formatMillis( duration ).join( ', ' ) }`
        ]
    }
}

export const GameTime = new Manager()