import { L2RecipeDefinition } from '../models/l2RecipeDefinition'
import { DatabaseManager } from '../../database/manager'
import {
    L2CharacterRecipeUpdate,
    L2CharacterRecipeUpdateType
} from '../../database/interface/CharacterRecipebookTableApi'
import _, { DebouncedFunc } from 'lodash'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { ServerLog } from '../../logger/Logger'


type BookModifications = Map<number, L2CharacterRecipeUpdate> // recipeId to saved data
const objectPool = new ObjectPool<L2CharacterRecipeUpdate>( 'PlayerRecipeCache', () : L2CharacterRecipeUpdate => {
    return {
        classIndex: 0,
        isDwarven: false,
        objectId: 0,
        recipeId: 0,
        type: undefined
    }
} )

export interface RecipeBooks {
    dwarven: Set<number>
    common: Set<number>
    classIndex: number
}

class Manager {

    books : Record<number, RecipeBooks> = {}
    modifications: Map<number, BookModifications> = new Map<number, BookModifications>()
    debouncedDatabaseWrite: DebouncedFunc<() => void>

    constructor() {
        this.debouncedDatabaseWrite = _.debounce( this.runDatabaseWrite.bind( this ), 10000, {
            maxWait: 20000
        } )
    }

    /*
        Loading of new player can happen when either:
        - player logs into account with specific character, a new world player load
        - player switches class, which causes new recipes to be replaced on top of old data
     */
    async loadPlayer( objectId: number, classIndex: number ) : Promise<void> {
        if ( this.modifications.has( objectId ) ) {
            await this.storePlayerModifications( objectId )
        }

        const [ commonRecipeIds, dwarvenRecipeIds ]: [ Array<number>, Array<number> ] = await Promise.all( [
            DatabaseManager.getCharacterRecipebook().getCommonRecipeIds( objectId ),
            DatabaseManager.getCharacterRecipebook().getDwarvenRecipeIds( objectId, classIndex ),
        ] )

        this.books[ objectId ] = {
            dwarven: new Set<number>( dwarvenRecipeIds ),
            common: new Set<number>( commonRecipeIds ),
            classIndex
        }
    }

    async unLoadPlayer( objectId: number ) : Promise<void> {
        delete this.books[ objectId ]

        if ( this.modifications.has( objectId ) ) {
            await this.storePlayerModifications( objectId )
            this.modifications.delete( objectId )
        }
    }

    getBooks( objectId: number ) : RecipeBooks {
        return this.books[ objectId ]
    }

    removeRecipe( objectId: number, recipeId: number, classIndex: number ) : void {
        let books = this.getBooks( objectId )
        if ( !books ) {
            return
        }

        if ( books.common.delete( recipeId ) ) {
            this.scheduleDelete( objectId, recipeId, classIndex, false )
            return
        }

        if ( books.dwarven.delete( recipeId ) ) {
            this.scheduleDelete( objectId, recipeId, classIndex, true )
            return
        }
    }

    addRecipe( objectId: number, recipe : L2RecipeDefinition, classIndex: number ) : void {
        let books = this.getBooks( objectId )
        if ( !books ) {
            return
        }

        if ( recipe.isDwarvenRecipe ) {
            if ( books.dwarven.has( recipe.id ) ) {
                return
            }

            books.dwarven.add( recipe.id )
            return this.scheduleUpsert( objectId, recipe.id, classIndex, true )
        }

        if ( books.common.has( recipe.id ) ) {
            return
        }

        books.common.add( recipe.id )
        this.scheduleUpsert( objectId, recipe.id, classIndex, false )
    }

    private async storePlayerModifications( objectId: number ) : Promise<void> {
        let toUpdate = []
        let toDelete = []

        this.extractPlayerModifications( objectId, toUpdate, toDelete )
        return this.writeData( toUpdate, toDelete )
    }

    private async runDatabaseWrite() : Promise<void> {
        let toUpdate = []
        let toDelete = []

        for ( const objectId of this.modifications.keys() ) {
            this.extractPlayerModifications( objectId, toUpdate, toDelete )
        }

        return this.writeData( toUpdate, toDelete )
    }

    private async writeData( toUpdate: Array<L2CharacterRecipeUpdate>, toDelete: Array<L2CharacterRecipeUpdate> ) : Promise<void> {
        if ( toDelete.length > 0 ) {
            await DatabaseManager.getCharacterRecipebook().removeRecipes( toDelete )
            objectPool.recycleValues( toDelete )
        }

        if ( toUpdate.length > 0 ) {
            await DatabaseManager.getCharacterRecipebook().addRecipes( toUpdate )
            objectPool.recycleValues( toUpdate )
        }

        ServerLog.info( `PlayerRecipeCache: removed ${toDelete.length} and updated ${toUpdate.length} recipes` )
    }

    private extractPlayerModifications( objectId: number, toUpdate: Array<L2CharacterRecipeUpdate>, toDelete: Array<L2CharacterRecipeUpdate> ) : void {
        let updates = this.modifications.get( objectId )

        for ( const recipeUpdate of updates.values() ) {
            switch ( recipeUpdate.type ) {
                case L2CharacterRecipeUpdateType.Delete:
                    toDelete.push( recipeUpdate )
                    break

                case L2CharacterRecipeUpdateType.Upsert:
                    toUpdate.push( recipeUpdate )
                    break
            }
        }

        updates.clear()
    }

    private getUpdateForRecipe( objectId: number, recipeId : number ) : L2CharacterRecipeUpdate {
        let updates = this.modifications.get( objectId )
        if ( !updates ) {
            updates = new Map<number, L2CharacterRecipeUpdate>()
            this.modifications.set( objectId, updates )
        }

        let recipeUpdate = updates.get( recipeId )
        if ( !recipeUpdate ) {
            recipeUpdate = objectPool.getValue()
            updates.set( recipeId, recipeUpdate )
        }

        return recipeUpdate
    }

    private scheduleUpsert( objectId: number, recipeId: number, classIndex: number, isDwarven : boolean ) : void {
        let update = this.getUpdateForRecipe( objectId, recipeId )

        update.objectId = objectId
        update.recipeId = recipeId
        update.classIndex = classIndex
        update.isDwarven = isDwarven
        update.type = L2CharacterRecipeUpdateType.Upsert

        this.debouncedDatabaseWrite()
    }

    private scheduleDelete( objectId: number, recipeId: number, classIndex: number, isDwarven : boolean ) : void {
        let update = this.getUpdateForRecipe( objectId, recipeId )

        update.objectId = objectId
        update.recipeId = recipeId
        update.classIndex = classIndex
        update.isDwarven = isDwarven
        update.type = L2CharacterRecipeUpdateType.Delete

        this.debouncedDatabaseWrite()
    }
}

export const PlayerRecipeCache = new Manager()