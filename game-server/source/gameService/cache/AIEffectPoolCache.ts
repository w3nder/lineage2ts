import { AIEffect, AIEventData } from '../aicontroller/enums/AIEffect'
import { AIEffectObjectFactory, AIEffectTypes } from '../aicontroller/AIEventObjectProducers'
import _ from 'lodash'
import { ObjectPool } from '../helpers/ObjectPoolHelper'

class Manager {
    effects: { [ type: number ]: ObjectPool<AIEventData> }

    constructor() {
        this.effects = _.mapValues( AIEffectTypes, ( method: AIEffectObjectFactory, effectId: string ) => {
            return new ObjectPool( `AIEffectTypes - ${ effectId }`, method )
        } )
    }

    getData( type: AIEffect ): AIEventData {
        if ( !this.effects[ type ] ) {
            return {}
        }

        return this.effects[ type ].getValue() as AIEventData
    }

    recyleData( type: AIEffect, data: AIEventData ): void {
        if ( !this.effects[ type ] ) {
            return
        }

        this.effects[ type ].recycleValue( data )
    }
}

export const AIEffectPoolCache = new Manager()