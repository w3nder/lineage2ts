import { L2DataApi } from '../../data/interface/l2DataApi'
import { getExistingSkill } from '../models/holders/SkillHolder'
import { AugmentationChance } from '../models/augmentation/AugmentationChance'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { L2Augmentation } from '../models/L2Augmentation'
import { ConfigManager } from '../../config/ConfigManager'
import { Options } from '../models/options/Options'
import { DataManager } from '../../data/manager'
import { L2AugmentationSkillsDataItem } from '../../data/interface/AugmentationSkillsDataApi'
import _ from 'lodash'
import { Skill } from '../models/Skill'
import { LifeStoneGrade } from '../enums/LifeStoneGrade'
import { L2ItemSlots } from '../enums/L2ItemSlots'

const STAT_BLOCKSIZE = 3640
const STAT_SUBBLOCKSIZE = 91
const MIN_SKILL_ID = STAT_BLOCKSIZE * 4

// skills
const BLUE_START = 14561
const SKILLS_BLOCKSIZE = 178

// basestats
const BASESTAT_STR = 16341
const BASESTAT_MEN = 16344

// accessory
const ACC_START = 16669
const ACC_BLOCKS_NUM = 10
const ACC_STAT_SUBBLOCKSIZE = 21

const ACC_RING_START = ACC_START
const ACC_RING_SKILLS = 18
const ACC_RING_BLOCKSIZE = ACC_RING_SKILLS + ( 4 * ACC_STAT_SUBBLOCKSIZE )
const ACC_RING_END = ( ACC_RING_START + ( ACC_BLOCKS_NUM * ACC_RING_BLOCKSIZE ) ) - 1

const ACC_EAR_START = ACC_RING_END + 1
const ACC_EAR_SKILLS = 18
const ACC_EAR_BLOCKSIZE = ACC_EAR_SKILLS + ( 4 * ACC_STAT_SUBBLOCKSIZE )
const ACC_EAR_END = ( ACC_EAR_START + ( ACC_BLOCKS_NUM * ACC_EAR_BLOCKSIZE ) ) - 1

const ACC_NECK_START = ACC_EAR_END + 1
const ACC_NECK_SKILLS = 24
const ACC_NECK_BLOCKSIZE = ACC_NECK_SKILLS + ( 4 * ACC_STAT_SUBBLOCKSIZE )

type NormalAugmentation = {
    [ stoneId: number ]: Array<AugmentationChance>
}

type WeaponAugmentation = {
    [ classType: string ]: NormalAugmentation
}

class Manager implements L2DataApi {
    augmentations: WeaponAugmentation
    accessoryAugmentations: NormalAugmentation
    skills: { [ key: number ]: Skill }
    blueSkills: Array<Array<number>>
    redSkills: Array<Array<number>>
    purpleSkills: Array<Array<number>>

    generateRandomAccessoryAugmentation( level: number, bodyPart: number, stoneId: number ): L2Augmentation {
        let stat12 = 0
        let stat34 = 0

        if ( ConfigManager.character.retailLikeAugmentationAccessory() ) {
            let chances12 = []
            let Chances34 = []

            _.each( this.accessoryAugmentations[ stoneId ], ( chance: AugmentationChance ) => {
                if ( chance.variationId === 1 ) {
                    chances12.push( chance )
                } else {
                    Chances34.push( chance )
                }
            } )

            let rate = _.random( 10000 )
            let seed = 10000

            _.each( chances12, ( chance: AugmentationChance ) => {
                if ( seed > rate ) {
                    seed -= ( chance.augmentChance * 100 )
                    stat12 = chance.augmentId
                }
            } )

            let category = _.random( 100 )
            if ( category < 55 ) {
                category = 55
            } else if ( category < 90 ) {
                category = 35
            } else if ( category < 99 ) {
                category = 9
            } else {
                category = 1
            }

            let _selectedChances34final: Array<AugmentationChance> = _.filter( Chances34, ( chance: AugmentationChance ) => {
                return chance.categoryChance === category
            } )

            rate = _.random( 10000 )
            seed = 10000

            _.each( _selectedChances34final, ( chance: AugmentationChance ) => {
                if ( seed > rate ) {
                    seed -= ( chance.augmentChance * 100 )
                    stat34 = chance.augmentId
                }
            } )

            return new L2Augmentation( ( ( stat34 << 16 ) + stat12 ) )
        }

        level = Math.min( level, 9 )
        let base = 0
        let skillsLength = 0

        switch ( bodyPart ) {
            case L2ItemSlots.AllFingerSlots:
                base = ACC_RING_START + ( ACC_RING_BLOCKSIZE * level )
                skillsLength = ACC_RING_SKILLS
                break
            case L2ItemSlots.AllEarSlots:
                base = ACC_EAR_START + ( ACC_EAR_BLOCKSIZE * level )
                skillsLength = ACC_EAR_SKILLS
                break
            case L2ItemSlots.Neck:
                base = ACC_NECK_START + ( ACC_NECK_BLOCKSIZE * level )
                skillsLength = ACC_NECK_SKILLS
                break
            default:
                return null
        }

        let resultColor = _.random( 0, 3 )

        // first augmentation (stats only)
        stat12 = _.random( ACC_STAT_SUBBLOCKSIZE )
        let options: Options
        if ( _.random( 1, 100 ) <= ConfigManager.character.getAugmentationAccSkillChance() ) {
            // second augmentation (skill)
            stat34 = base + _.random( skillsLength )
            options = DataManager.getOptionsData().getOptions( stat34 )
        }

        if ( !options || ( !options.hasActiveSkill() && !options.hasPassiveSkill() && !options.hasActivationSkill() ) ) {
            // second augmentation (stats)
            // calculating any different from stat12 value inside sub-block
            // starting from next and wrapping over using remainder
            stat34 = ( stat12 + 1 + _.random( ACC_STAT_SUBBLOCKSIZE - 1 ) ) % ACC_STAT_SUBBLOCKSIZE
            // this is a stats - skipping skills
            stat34 = base + skillsLength + ( ACC_STAT_SUBBLOCKSIZE * resultColor ) + stat34
        }

        // stat12 has stats only
        stat12 = base + skillsLength + ( ACC_STAT_SUBBLOCKSIZE * resultColor ) + stat12

        return new L2Augmentation( ( ( stat34 << 16 ) + stat12 ) )
    }

    generateRandomAugmentation( level: number, grade: number, bodyPart: number, stoneId: number, targetItem: L2ItemInstance ): L2Augmentation {
        switch ( bodyPart ) {
            case L2ItemSlots.AllFingerSlots:
            case L2ItemSlots.AllEarSlots:
            case L2ItemSlots.Neck:
                return this.generateRandomAccessoryAugmentation( level, bodyPart, stoneId )
        }

        return this.generateRandomWeaponAugmentation( level, grade, stoneId, targetItem )
    }

    generateRandomWeaponAugmentation( level: number, grade: number, stoneId: number, item: L2ItemInstance ): L2Augmentation {
        let stat12 = 0
        let stat34 = 0

        if ( ConfigManager.character.retailLikeAugmentation() ) {
            if ( item.getItem().isMagicWeapon() ) {
                let chances12: Array<AugmentationChance> = []
                let Chances34: Array<AugmentationChance> = []

                _.each( _.get( this.augmentations, [ 'mage', stoneId ] ), ( chance: AugmentationChance ) => {
                    if ( chance.variationId === 1 ) {
                        chances12.push( chance )
                    } else {
                        Chances34.push( chance )
                    }
                } )

                let rate = _.random( 10000 )
                let seed = 10000

                _.each( chances12, ( chance: AugmentationChance ) => {
                    if ( seed > rate ) {
                        seed -= ( chance.augmentChance * 100 )
                        stat12 = chance.augmentId
                    }
                } )

                let gradeChance: Array<number>
                switch ( grade ) {
                    case LifeStoneGrade.None:
                        gradeChance = ConfigManager.character.getRetailLikeAugmentationNoGradeChance()
                        return
                    case LifeStoneGrade.Mid:
                        gradeChance = ConfigManager.character.getRetailLikeAugmentationMidGradeChance()
                        break
                    case LifeStoneGrade.High:
                        gradeChance = ConfigManager.character.getRetailLikeAugmentationHighGradeChance()
                        break
                    case LifeStoneGrade.Top:
                        gradeChance = ConfigManager.character.getRetailLikeAugmentationTopGradeChance()
                        break
                    default:
                        gradeChance = ConfigManager.character.getRetailLikeAugmentationNoGradeChance()
                        break
                }

                let category = _.random( 100 )
                if ( category < gradeChance[ 0 ] ) {
                    category = 55
                } else if ( category < ( gradeChance[ 0 ] + gradeChance[ 1 ] ) ) {
                    category = 35
                } else if ( category < ( gradeChance[ 0 ] + gradeChance[ 1 ] + gradeChance[ 2 ] ) ) {
                    category = 7
                } else {
                    category = 3
                }

                let _selectedChances34final: Array<AugmentationChance> = _.filter( Chances34, ( chance: AugmentationChance ) => {
                    return chance.categoryChance === category
                } )

                rate = _.random( 10000 )
                seed = 10000

                _.each( _selectedChances34final, ( chance: AugmentationChance ) => {
                    if ( seed > rate ) {
                        seed -= ( chance.augmentChance * 100 )
                        stat34 = chance.augmentId
                    }
                } )
            } else {

                let chances12 = []
                let chances34 = []

                _.each( _.get( this.augmentations, [ 'warrior', stoneId ] ), ( chance: AugmentationChance ) => {
                    if ( chance.variationId === 1 ) {
                        chances12.push( chance )
                    } else {
                        chances34.push( chance )
                    }
                } )

                let rate = _.random( 10000 )
                let seed = 10000

                _.each( chances12, ( chance: AugmentationChance ) => {
                    if ( seed > rate ) {
                        seed -= ( chance.augmentChance * 100 )
                        stat12 = chance.augmentId
                    }
                } )

                let gradeChance: Array<number>
                switch ( grade ) {
                    case LifeStoneGrade.None:
                        gradeChance = ConfigManager.character.getRetailLikeAugmentationNoGradeChance()
                        return
                    case LifeStoneGrade.Mid:
                        gradeChance = ConfigManager.character.getRetailLikeAugmentationMidGradeChance()
                        break
                    case LifeStoneGrade.High:
                        gradeChance = ConfigManager.character.getRetailLikeAugmentationHighGradeChance()
                        break
                    case LifeStoneGrade.Top:
                        gradeChance = ConfigManager.character.getRetailLikeAugmentationTopGradeChance()
                        break
                    default:
                        gradeChance = ConfigManager.character.getRetailLikeAugmentationNoGradeChance()
                        break
                }

                let category = _.random( 100 )
                if ( category < gradeChance[ 0 ] ) {
                    category = 55
                } else if ( category < ( gradeChance[ 0 ] + gradeChance[ 1 ] ) ) {
                    category = 35
                } else if ( category < ( gradeChance[ 0 ] + gradeChance[ 1 ] + gradeChance[ 2 ] ) ) {
                    category = 7
                } else {
                    category = 3
                }

                let _selectedChances34final: Array<AugmentationChance> = _.filter( chances34, ( chance: AugmentationChance ) => {
                    return chance.categoryChance === category
                } )

                rate = _.random( 10000 )
                seed = 10000

                _.each( _selectedChances34final, ( chance: AugmentationChance ) => {
                    if ( seed > rate ) {
                        seed -= ( chance.augmentChance * 100 )
                        stat34 = chance.augmentId
                    }
                } )
            }
            return new L2Augmentation( ( ( stat34 << 16 ) + stat12 ) )
        }

        let generateSkill = false
        let generateGlow = false

        // life stone level is used for stat Id and skill level, but here the max level is 9
        level = Math.min( level, 9 )

        switch ( grade ) {
            case LifeStoneGrade.None:
                if ( _.random( 1, 100 ) <= ConfigManager.character.getAugmentationNGSkillChance() ) {
                    generateSkill = true
                }
                if ( _.random( 1, 100 ) <= ConfigManager.character.getAugmentationNGGlowChance() ) {
                    generateGlow = true
                }
                break
            case LifeStoneGrade.Mid:
                if ( _.random( 1, 100 ) <= ConfigManager.character.getAugmentationMidSkillChance() ) {
                    generateSkill = true
                }
                if ( _.random( 1, 100 ) <= ConfigManager.character.getAugmentationMidGlowChance() ) {
                    generateGlow = true
                }
                break
            case LifeStoneGrade.High:
                if ( _.random( 1, 100 ) <= ConfigManager.character.getAugmentationHighSkillChance() ) {
                    generateSkill = true
                }
                if ( _.random( 1, 100 ) <= ConfigManager.character.getAugmentationHighGlowChance() ) {
                    generateGlow = true
                }
                break
            case LifeStoneGrade.Top:
                if ( _.random( 1, 100 ) <= ConfigManager.character.getAugmentationTopSkillChance() ) {
                    generateSkill = true
                }
                if ( _.random( 1, 100 ) <= ConfigManager.character.getAugmentationTopGlowChance() ) {
                    generateGlow = true
                }
                break
            case LifeStoneGrade.Accessory:
                if ( _.random( 1, 100 ) <= ConfigManager.character.getAugmentationAccSkillChance() ) {
                    generateSkill = true
                }
        }

        if ( !generateSkill && ( _.random( 1, 100 ) <= ConfigManager.character.getAugmentationBaseStatChance() ) ) {
            stat34 = _.random( BASESTAT_STR, BASESTAT_MEN )
        }

        // Second: decide which grade the augmentation result is going to have:
        // 0:yellow, 1:blue, 2:purple, 3:red
        // The chances used here are most likely custom,
        // what's known is: you can't have yellow with skill(or baseStatModifier)
        // noGrade stone can not have glow, mid only with skill, high has a chance(custom), top allways glow
        let resultColor = _.random( 0, 100 )
        if ( ( stat34 === 0 ) && !generateSkill ) {
            if ( resultColor <= ( ( 15 * grade ) + 40 ) ) {
                resultColor = 1
            } else {
                resultColor = 0
            }
        } else {
            if ( ( resultColor <= ( ( 10 * grade ) + 5 ) ) || ( stat34 !== 0 ) ) {
                resultColor = 3
            } else if ( resultColor <= ( ( 10 * grade ) + 10 ) ) {
                resultColor = 1
            } else {
                resultColor = 2
            }
        }

        // generate a skill if necessary
        if ( generateSkill ) {
            switch ( resultColor ) {
                case 1:
                    stat34 = _.sample( this.blueSkills[ level ] )
                    break
                case 2:
                    stat34 = _.sample( this.purpleSkills[ level ] )
                    break
                case 3:
                    stat34 = _.sample( this.redSkills[ level ] )
                    break
            }
        }

        // Third: Calculate the subblock offset for the chosen color,
        // and the level of the lifeStone
        // from large number of retail augmentations:
        // no skill part
        // Id for stat12:
        // A:1-910 B:911-1820 C:1821-2730 D:2731-3640 E:3641-4550 F:4551-5460 G:5461-6370 H:6371-7280
        // Id for stat34(this defines the color):
        // I:7281-8190(yellow) K:8191-9100(blue) L:10921-11830(yellow) M:11831-12740(blue)
        // you can combine I-K with A-D and L-M with E-H
        // using C-D or G-H Id you will get a glow effect
        // there seems no correlation in which grade use which Id except for the glowing restriction
        // skill part
        // Id for stat12:
        // same for no skill part
        // A same as E, B same as F, C same as G, D same as H
        // A - no glow, no grade LS
        // B - weak glow, mid grade LS?
        // C - glow, high grade LS?
        // D - strong glow, top grade LS?

        // is neither a skill nor basestat used for stat34? then generate a normal stat
        let offset: number
        if ( stat34 === 0 ) {
            let value = _.random( 2, 3 )
            let colorOffset = ( resultColor * ( 10 * STAT_SUBBLOCKSIZE ) ) + ( value * STAT_BLOCKSIZE ) + 1
            offset = ( level * STAT_SUBBLOCKSIZE ) + colorOffset

            stat34 = _.random( offset, ( offset + STAT_SUBBLOCKSIZE ) - 1 )
            if ( generateGlow && ( grade >= 2 ) ) {
                offset = ( level * STAT_SUBBLOCKSIZE ) + ( ( value - 2 ) * STAT_BLOCKSIZE ) + ( grade * ( 10 * STAT_SUBBLOCKSIZE ) ) + 1
            } else {
                offset = ( level * STAT_SUBBLOCKSIZE ) + ( ( value - 2 ) * STAT_BLOCKSIZE ) + ( _.random( 0, 1 ) * ( 10 * STAT_SUBBLOCKSIZE ) ) + 1
            }
        } else {
            if ( !generateGlow ) {
                offset = ( level * STAT_SUBBLOCKSIZE ) + ( _.random( 0, 1 ) * STAT_BLOCKSIZE ) + 1
            } else {
                offset = ( level * STAT_SUBBLOCKSIZE ) + ( _.random( 0, 1 ) * STAT_BLOCKSIZE ) + ( ( ( grade + resultColor ) / 2 ) * ( 10 * STAT_SUBBLOCKSIZE ) ) + 1
            }
        }
        stat12 = _.random( offset, ( offset + STAT_SUBBLOCKSIZE ) - 1 )

        return new L2Augmentation( ( ( stat34 << 16 ) + stat12 ) )
    }

    async load(): Promise<Array<string>> {

        this.augmentations = {}
        this.accessoryAugmentations = {}
        this.skills = {}

        this.blueSkills = _.times( 10, () => [] )
        this.redSkills = _.times( 10, () => [] )
        this.purpleSkills = _.times( 10, () => [] )

        let messages: Array<string> = []

        if ( ConfigManager.character.retailLikeAugmentationAccessory() ) {
            let processedCount: number = this.loadAccessoryAugmentation()
            messages.push( `AugmentationCache : loaded ${ processedCount } accessory augmentations.` )
        }

        if ( ConfigManager.character.retailLikeAugmentation() ) {
            let processedCount: number = this.loadWeaponAugmentation()
            messages.push( `AugmentationCache : loaded ${ processedCount } weapon augmentations.` )
        } else {
            let processedCount: number = this.loadSkillAugmentation()
            messages.push( `AugmentationCache : loaded ${ processedCount } possible augmentation skills.` )
        }

        return messages
    }

    loadAccessoryAugmentation() : number {
        let data : Array<AugmentationChance> = DataManager.getAugmentationAccessory().getAll()

        data.forEach( ( item: AugmentationChance ) => {
            if ( !this.accessoryAugmentations[ item.stoneId ] ) {
                this.accessoryAugmentations[ item.stoneId ] = []
            }

            this.accessoryAugmentations[ item.stoneId ].push( item )
        } )

        return data.length
    }

    loadWeaponAugmentation() : number {
        let data : Array<AugmentationChance> = DataManager.getAugmentationWeapons().getAll()

        data.forEach( ( item: AugmentationChance ) => {
            let { type, stoneId } = item
            if ( !this.augmentations[ type ] ) {
                this.augmentations[ type ] = {}
            }

            if ( !this.augmentations[ type ][ stoneId ] ) {
                this.augmentations[ type ][ stoneId ] = []
            }

            this.augmentations[ type ][ item.stoneId ].push( item )
        } )

        return data.length
    }

    /*
        TODO : use skill augmentation
     */
    loadSkillAugmentation() : number {
        let data = DataManager.getAugmentationSkills().getAll()

        data.forEach( ( item: L2AugmentationSkillsDataItem ) => {
            if ( item.skillId === 0 || item.skillLevel === 0 ) {
                return
            }

            let colorIndex = Math.floor( ( item.augmentationId - BLUE_START ) / SKILLS_BLOCKSIZE )

            switch ( item.type ) {
                case 'blue':
                    this.blueSkills[ colorIndex ].push( item.augmentationId )
                    break

                case 'purple':
                    this.purpleSkills[ colorIndex ].push( item.augmentationId )
                    break

                case 'red':
                    this.redSkills[ colorIndex ].push( item.augmentationId )
                    break
            }

            this.skills[ item.augmentationId ] = getExistingSkill( item.skillId, item.skillLevel )
        } )

        return data.length
    }
}

export const AugmentationCache = new Manager()