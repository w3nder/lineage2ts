import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { DatabaseManager } from '../../database/manager'
import { L2ItemsOnGroundTableData } from '../../database/interface/ItemsOnGroundTableApi'
import { L2World } from '../L2World'
import { CursedWeaponManager } from '../instancemanager/CursedWeaponManager'
import { ItemManagerCache } from './ItemManagerCache'
import aigle from 'aigle'
import { RoaringBitmap32 } from 'roaring'
import { ItemLocation } from '../enums/ItemLocation'
import Timeout = NodeJS.Timeout

class Manager implements L2DataApi {
    items: RoaringBitmap32 = new RoaringBitmap32()
    saveItemsTask: Timeout

    async load(): Promise<Array<string>> {
        if ( !ConfigManager.general.saveDroppedItem() ) {

            return [
                'ItemsOnGroundManager : no items restored due to configuration.',
            ]
        }

        let itemData: Array<L2ItemsOnGroundTableData> = await DatabaseManager.getItemsOnGround().getAll()

        await aigle.resolve( itemData ).each( async ( data: L2ItemsOnGroundTableData ) => {
            if ( !data.itemId || !data.objectId || L2World.getObjectById( data.objectId ) ) {
                return
            }

            let item = new L2ItemInstance( data.objectId, data.itemId )

            if ( item.isStackable() && data.count > 1 ) {
                item.setCount( data.count )
            }

            item.setEnchantLevelDirect( data.enchantLevel )
            item.setXYZ( data.x, data.y, data.z )

            item.setDropTime( data.dropTime )
            item.setDeletionProtected( data.dropTime === -1 )
            item.setIsVisible( true )

            this.items.add( item.objectId )

            if ( data.dropTime > -1 ) {
                ItemManagerCache.autoDestroyItem( item )
            }
        } )

        if ( ConfigManager.general.isSaveDroppedResetAfterLoad() ) {
            await DatabaseManager.getItemsOnGround().removeAll()
        }

        return [
            `ItemsOnGroundManager : restored ${ itemData.length } items`,
        ]
    }

    removeObject( item: L2ItemInstance ) {
        this.items.delete( item.getObjectId() )
    }

    async runSaveItemsTask(): Promise<void> {
        if ( !ConfigManager.general.saveDroppedItem() ) {
            return this.stopTask()
        }

        let expiration = ConfigManager.general.getSaveDroppedExpiration()
        if ( expiration > 0 ) {
            await DatabaseManager.getItemsOnGround().removeByDropTime( Date.now() - expiration )
        } else {
            await DatabaseManager.getItemsOnGround().removeAll()
        }

        if ( this.items.size === 0 ) {
            return this.stopTask()
        }

        let data : Array<L2ItemsOnGroundTableData> = []

        for ( const objectId of this.items ) {
            let item: L2ItemInstance = L2World.getObjectById( objectId ) as L2ItemInstance

            if ( !item || item.getItemLocation() !== ItemLocation.VOID ) {
                continue
            }

            data.push( {
                count: item.getCount(),
                dropTime: item.isDeletionProtected() ? -1 : item.getDropTime(),
                enchantLevel: item.getEnchantLevel(),
                itemId: item.getId(),
                objectId,
                type: item.getItem().instanceType,
                x: item.getX(),
                y: item.getY(),
                z: item.getZ(),
            } )
        }

        return DatabaseManager.getItemsOnGround().addAll( data )
    }

    save( item: L2ItemInstance ): void {
        if ( !ConfigManager.general.saveDroppedItem() || CursedWeaponManager.isCursedItem( item.getId() ) ) {
            return
        }

        item.setDeletionProtected( ConfigManager.general.getSaveDroppedProtectedTypes().has( item.getItem().instanceType ) )

        this.items.add( item.getObjectId() )

        if ( !this.saveItemsTask && ConfigManager.general.getSaveDroppedItemInterval() > 0 ) {
            this.saveItemsTask = setInterval( this.runSaveItemsTask.bind( this ), ConfigManager.general.getSaveDroppedItemInterval() )
        }
    }

    stopTask(): void {
        if ( this.saveItemsTask ) {
            clearInterval( this.saveItemsTask )
            this.saveItemsTask = null
        }
    }
}

export const ItemsOnGroundManager = new Manager()