import _, { DebouncedFunc } from 'lodash'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { DatabaseManager } from '../../database/manager'
import { ServerLog } from '../../logger/Logger'
import { L2CharacterFriendUpdate } from '../../database/interface/CharacterFriendsTableApi'
import { FriendState, FriendStatus } from '../packets/send/FriendStatus'
import { PacketDispatcher } from '../PacketDispatcher'

const enum FriendUpdateType {
    Add,
    Remove
}

interface FriendUpdate extends L2CharacterFriendUpdate {
    type: FriendUpdateType
}

const objectPool = new ObjectPool<FriendUpdate>( 'PlayerFriendsCache', () : FriendUpdate => {
    return {
        fromId: 0,
        toId: 0,
        type: undefined
    }
} )

class Manager {
    friends: Record<number, Set<number>> = {}
    modifications: Map<number, Map<number, FriendUpdate>> = new Map<number, Map<number, FriendUpdate>>()

    debouncedDatabaseWrite: DebouncedFunc<() => void>

    constructor() {
        this.debouncedDatabaseWrite = _.debounce( this.runDatabaseWrite.bind( this ), 10000, {
            maxWait: 20000
        } )
    }

    async loadPlayer( objectId: number, name: string ) : Promise<void> {
        let ids = await DatabaseManager.getCharacterFriends().getFriendIds( objectId )
        this.friends[ objectId ] = new Set( ids )

        return this.notifyFriends( objectId, name, FriendState.Online )
    }

    async unLoadPlayer( objectId: number, name: string ) : Promise<void> {
        this.notifyFriends( objectId, name, FriendState.Offline )

        if ( this.modifications.has( objectId ) ) {
            await this.storePlayerModifications( objectId )
            this.modifications.delete( objectId )
        }

        delete this.friends[ objectId ]
    }

    async runDatabaseWrite() : Promise<void> {
        let toUpdate = []
        let toDelete = []

        for ( const objectId of this.modifications.keys() ) {
            this.extractPlayerModifications( objectId, toUpdate, toDelete )
        }

        return this.writeData( toUpdate, toDelete )
    }

    private async writeData( toUpdate: Array<FriendUpdate>, toDelete: Array<FriendUpdate> ) : Promise<void> {
        if ( toDelete.length > 0 ) {
            await DatabaseManager.getCharacterFriends().removeFriends( toDelete )
            objectPool.recycleValues( toDelete )
        }

        if ( toUpdate.length > 0 ) {
            await DatabaseManager.getCharacterFriends().addFriends( toUpdate )
            objectPool.recycleValues( toUpdate )
        }

        ServerLog.info( `PlayerFriendsCache: removed ${toDelete.length} and updated ${toUpdate.length} items` )
    }

    private extractPlayerModifications( objectId: number, toUpdate: Array<FriendUpdate>, toDelete: Array<FriendUpdate> ) : void {
        let updates = this.modifications.get( objectId )

        for ( const updateItem of updates.values() ) {
            switch ( updateItem.type ) {
                case FriendUpdateType.Remove:
                    toDelete.push( updateItem )
                    break

                case FriendUpdateType.Add:
                    toUpdate.push( updateItem )
                    break
            }
        }

        updates.clear()
    }

    private async storePlayerModifications( objectId: number ) : Promise<void> {
        let toUpdate = []
        let toDelete = []

        this.extractPlayerModifications( objectId, toUpdate, toDelete )
        return this.writeData( toUpdate, toDelete )
    }

    getFriends( objectId: number ) : ReadonlySet<number> {
        return this.friends[ objectId ]
    }

    addFriend( fromId: number, toId: number ) : void {
        let fromFriends = this.friends[ fromId ]
        let shouldWrite : boolean = false

        if ( fromFriends && !fromFriends.has( toId ) ) {
            fromFriends.add( toId )
            this.addFriendModification( fromId, toId, FriendUpdateType.Add )
            shouldWrite = true
        }

        let toFriends = this.friends[ toId ]
        if ( toFriends && !toFriends.has( fromId ) ) {
            toFriends.add( fromId )
            this.addFriendModification( toId, fromId, FriendUpdateType.Add )
            shouldWrite = true
        }

        if ( shouldWrite ) {
            this.debouncedDatabaseWrite()
        }
    }

    removeFriend( fromId: number, toId: number ) : void {
        let fromFriends = this.friends[ fromId ]
        let shouldWrite : boolean = false

        if ( fromFriends && fromFriends.delete( toId ) ) {
            this.addFriendModification( fromId, toId, FriendUpdateType.Remove )
            shouldWrite = true
        }

        let toFriends = this.friends[ toId ]
        if ( toFriends && toFriends.delete( fromId ) ) {
            this.addFriendModification( toId, fromId, FriendUpdateType.Remove )
            shouldWrite = true
        }

        if ( shouldWrite ) {
            this.debouncedDatabaseWrite()
        }
    }

    private getFriendForUpdate( fromId: number, toId: number ) : FriendUpdate {
        let updates = this.modifications.get( fromId )
        if ( !updates ) {
            updates = new Map<number, FriendUpdate>()
            this.modifications.set( fromId, updates )
        }

        let item = updates.get( toId )
        if ( !item ) {
            item = objectPool.getValue()
            updates.set( toId, item )
        }

        return item
    }

    private addFriendModification( fromId: number, toId: number, type: FriendUpdateType ) : void {
        let update = this.getFriendForUpdate( fromId, toId )

        update.fromId = fromId
        update.toId = toId
        update.type = type
    }

    private notifyFriends( objectId: number, name: string, state: FriendState ) : void {
        let friendIds = this.getFriends( objectId )
        if ( !friendIds || friendIds.size === 0 ) {
            return
        }

        let packet = FriendStatus( objectId, name, state )
        friendIds.forEach( ( playerId: number ) => {
            PacketDispatcher.sendCopyData( playerId, packet )
        } )
    }
}

export const PlayerFriendsCache = new Manager()