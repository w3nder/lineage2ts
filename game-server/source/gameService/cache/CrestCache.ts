import { CrestType, L2Crest } from '../models/L2Crest'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { DatabaseManager } from '../../database/manager'
import { IDFactoryCache } from './IDFactoryCache'
import _ from 'lodash'

class Manager implements L2DataApi {
    crests: { [ key: number ]: L2Crest }

    /*
        TODO: initially load only crests for castle owner clans
        - when individual clan information is loaded, load its crest into cache
     */
    async load(): Promise<Array<string>> {
        this.crests = {}
        let crests : Array<L2Crest> = await DatabaseManager.getCrestsTable().getAll()

        let cache = this
        _.each( crests, ( crest: L2Crest ) => {
            cache.crests[ crest.getId() ] = crest
        } )

        return [
           `CrestCache loaded ${_.size( this.crests )} existing crests.`
        ]
    }

    /*
        TODO : add debounced way of saving/removing data to avoid situations where
        packet inbound traffic can affect performance of server
     */
    async createCrest( imageData: Buffer, type: CrestType ) : Promise<L2Crest> {
        let crest = new L2Crest()

        crest.id = IDFactoryCache.getNextId()
        crest.imageData = imageData
        crest.type = type

        this.crests[ crest.id ] = crest

        await DatabaseManager.getCrestsTable().addCrest( crest.id, imageData, type )

        return crest
    }

    async removeCrest( crestId: number ) {
        _.unset( this.crests, crestId )

        IDFactoryCache.releaseId( crestId )

        return DatabaseManager.getCrestsTable().removeCrest( crestId )
    }

    getCrest( id: number ) : L2Crest {
        return this.crests[ id ]
    }
}

export const CrestCache = new Manager()