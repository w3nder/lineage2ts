import { RoaringBitmap32 } from 'roaring'
import { DatabaseManager } from '../../database/manager'
import _, { DebouncedFunc } from 'lodash'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../config/ConfigManager'
import { SystemMessageBuilder, SystemMessageHelper } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { CharacterNamesCache } from './CharacterNamesCache'
import { PacketDispatcher } from '../PacketDispatcher'
import { EtcStatusUpdate } from '../packets/send/EtcStatusUpdate'
import { PlayerFriendsCache } from './PlayerFriendsCache'
import { L2CharacterFriendUpdate } from '../../database/interface/CharacterFriendsTableApi'
import { ObjectPool } from '../helpers/ObjectPoolHelper'
import { ServerLog } from '../../logger/Logger'

interface PlayerBlocklist {
    playerIds: RoaringBitmap32
    isMessageRefusal: boolean
}

const enum BlocklistUpdateType {
    Add,
    Remove
}

interface BlocklistUpdate extends L2CharacterFriendUpdate {
    type: BlocklistUpdateType
}

const objectPool = new ObjectPool( 'BlocklistCache', () : BlocklistUpdate => {
    return {
        fromId: 0,
        toId: 0,
        type: undefined
    }
} )

class Manager {
    data: { [ playerId: number ]: PlayerBlocklist } = {}
    modifications: Map<number, Map<number, BlocklistUpdate>> = new Map<number, Map<number, BlocklistUpdate>>()

    debouncedDatabaseWrite: DebouncedFunc<() => void>

    constructor() {
        this.debouncedDatabaseWrite = _.debounce( this.runDatabaseWrite.bind( this ), 10000, {
            maxWait: 20000
        } )
    }

    async runDatabaseWrite() : Promise<void> {
        let toUpdate = []
        let toDelete = []

        for ( const objectId of this.modifications.keys() ) {
            this.extractPlayerModifications( objectId, toUpdate, toDelete )
        }

        return this.writeData( toUpdate, toDelete )
    }

    private async writeData( toUpdate: Array<BlocklistUpdate>, toDelete: Array<BlocklistUpdate> ) : Promise<void> {
        if ( toDelete.length > 0 ) {
            await DatabaseManager.getCharacterFriends().removeBlocklists( toDelete )
            objectPool.recycleValues( toDelete )
        }

        if ( toUpdate.length > 0 ) {
            await DatabaseManager.getCharacterFriends().addBlocklists( toUpdate )
            objectPool.recycleValues( toUpdate )
        }

        ServerLog.info( `BlocklistCache: removed ${toDelete.length} and updated ${toUpdate.length} items` )
    }

    private extractPlayerModifications( objectId: number, toUpdate: Array<BlocklistUpdate>, toDelete: Array<BlocklistUpdate> ) : void {
        let updates = this.modifications.get( objectId )

        for ( const updateItem of updates.values() ) {
            switch ( updateItem.type ) {
                case BlocklistUpdateType.Remove:
                    toDelete.push( updateItem )
                    break

                case BlocklistUpdateType.Add:
                    toUpdate.push( updateItem )
                    break
            }
        }

        updates.clear()
    }

    /*
        Current implementation assumes to never unload blocklist data due to integration
        with sending mail.
     */
    async loadPlayer( playerId: number ): Promise<void> {
        let otherPlayerIds: Array<number> = await DatabaseManager.getCharacterFriends().getBlocklistIds( playerId )
        let existingData: PlayerBlocklist = this.data[ playerId ]
        if ( existingData ) {
            existingData.playerIds.addMany( otherPlayerIds )
            return
        }

        this.data[ playerId ] = {
            playerIds: new RoaringBitmap32( otherPlayerIds ),
            isMessageRefusal: false
        }
    }

    isInBlockList( playerId: number, targetId: number ): boolean {
        let existingData: PlayerBlocklist = this.data[ playerId ]
        if ( !existingData ) {
            return false
        }

        return existingData.playerIds.has( targetId )
    }

    async addToPlayerBlockList( player: L2PcInstance, targetId: number ): Promise<void> {
        let existingData: PlayerBlocklist = this.data[ player.getObjectId() ]
        if ( !existingData ) {
            return
        }

        if ( existingData.playerIds.has( targetId ) ) {
            player.sendMessage( 'Already in ignore list.' )
            return
        }

        if ( existingData.playerIds.size >= ConfigManager.character.getBlockListLimit() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_CAN_ONLY_ENTER_UP_TO_128_NAMES_IN_YOUR_BLOCK_LIST ) )
            return
        }

        let targetName = await CharacterNamesCache.getNameById( targetId )
        let playerFriendsIds = PlayerFriendsCache.getFriends( player.getObjectId() )

        if ( playerFriendsIds.has( targetId ) ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_ALREADY_IN_FRIENDS_LIST )
                    .addString( targetName )
                    .getBuffer()
            player.sendOwnedData( packet )
            return
        }

        existingData.playerIds.add( targetId )
        this.addBlocklistModification( player.getObjectId(), targetId, BlocklistUpdateType.Remove )

        let messagePacket = new SystemMessageBuilder( SystemMessageIds.S1_WAS_ADDED_TO_YOUR_IGNORE_LIST )
                .addString( targetName )
                .getBuffer()
        player.sendOwnedData( messagePacket )

        let packet = new SystemMessageBuilder( SystemMessageIds.S1_HAS_ADDED_YOU_TO_IGNORE_LIST )
                .addString( player.getName() )
                .getBuffer()
        PacketDispatcher.sendOwnedData( targetId, packet )
    }

    async removeFromPlayerBlockList( playerId: number, targetId: number ): Promise<void> {
        let existingData: PlayerBlocklist = this.data[ playerId ]
        if ( !existingData ) {
            return
        }

        let targetName = await CharacterNamesCache.getNameById( targetId )

        if ( !existingData.playerIds.has( targetId ) ) {
            PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
            return
        }

        existingData.playerIds.remove( targetId )
        this.addBlocklistModification( playerId, targetId, BlocklistUpdateType.Remove )

        let packet: Buffer = new SystemMessageBuilder( SystemMessageIds.S1_WAS_REMOVED_FROM_YOUR_IGNORE_LIST )
                .addString( targetName )
                .getBuffer()
        PacketDispatcher.sendOwnedData( playerId, packet )
    }

    async describeBlocklist( playerId: number ): Promise<void> {
        let existingData: PlayerBlocklist = this.data[ playerId ]
        if ( !existingData ) {
            return
        }

        PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.BLOCK_LIST_HEADER ) )
        let otherPlayerIds: Array<number> = existingData.playerIds.toArray()
        await CharacterNamesCache.prefetchNames( otherPlayerIds )

        otherPlayerIds.forEach( ( otherPlayerId: number, index: number ) => {
            PacketDispatcher.sendOwnedData( playerId, SystemMessageHelper.sendString( `${ index + 1 }. ${ CharacterNamesCache.getCachedNameById( otherPlayerId ) }` ) )
        } )

        PacketDispatcher.sendOwnedData( playerId, SystemMessageBuilder.fromMessageId( SystemMessageIds.FRIEND_LIST_FOOTER ) )
    }

    async isInBlockListByOwner( playerId: number, targetId: number ): Promise<boolean> {
        let existingData: PlayerBlocklist = this.data[ playerId ]
        if ( existingData ) {
            return existingData.isMessageRefusal || this.isInBlockList( playerId, targetId )
        }

        await this.loadPlayer( playerId )
        return this.isInBlockList( playerId, targetId )
    }

    isBlocked( receiverId: number, producerId: number ) : boolean {
        let existingData: PlayerBlocklist = this.data[ receiverId ]
        if ( !existingData ) {
            return false
        }

        return existingData.isMessageRefusal || this.isInBlockList( receiverId, producerId )
    }

    getMessageRefusal( playerId: number ) : boolean {
        let existingData: PlayerBlocklist = this.data[ playerId ]
        if ( !existingData ) {
            return false
        }

        return existingData.isMessageRefusal
    }

    setMessageRefusal( playerId: number, value: boolean ) : void {
        let existingData: PlayerBlocklist = this.data[ playerId ]
        if ( !existingData ) {
            return
        }

        existingData.isMessageRefusal = value
        PacketDispatcher.sendDebouncedPacket( playerId, EtcStatusUpdate )
    }

    private getBlocklistForUpdate( fromId: number, toId: number ) : BlocklistUpdate {
        let updates = this.modifications.get( fromId )
        if ( !updates ) {
            updates = new Map<number, BlocklistUpdate>()
            this.modifications.set( fromId, updates )
        }

        let item = updates.get( toId )
        if ( !item ) {
            item = objectPool.getValue()
            updates.set( toId, item )
        }

        return item
    }

    private addBlocklistModification( fromId: number, toId: number, type: BlocklistUpdateType ) : void {
        let update = this.getBlocklistForUpdate( fromId, toId )

        update.fromId = fromId
        update.toId = toId
        update.type = type

        this.debouncedDatabaseWrite()
    }
}

export const BlocklistCache = new Manager()