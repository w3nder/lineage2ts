import { DatabaseManager } from '../../database/manager'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2CharactersTableNamePair } from '../../database/interface/CharactersTableApi'
import _ from 'lodash'

class Manager {
    idToName: { [ key: number ]: string } = {}
    nameToId: { [ key: string ]: number } = {}
    idToAccessLevel: { [ key: number ]: number } = {}

    async getNameById( id: number ): Promise<string> {
        let name = this.idToName[ id ]

        if ( name ) {
            return name
        }

        let foundName: string = await DatabaseManager.getCharacterTable().getNameById( id )
        if ( foundName ) {
            this.idToName[ id ] = foundName
            this.nameToId[ foundName ] = id
        }

        return foundName
    }

    async getIdByName( name: string ): Promise<number> {
        let id = this.getCachedIdByName( name )

        if ( id ) {
            return id
        }

        let foundId: number = await DatabaseManager.getCharacterTable().getIdByName( name )
        if ( foundId ) {
            this.nameToId[ name ] = id
            this.idToName[ foundId ] = name
        }

        return foundId
    }

    getCachedIdByName( name: string ) : number {
        return this.nameToId[ name ]
    }

    async getAccessLevelById( objectId: number ) {
        let level = this.idToAccessLevel[ objectId ]

        if ( _.isNumber( level ) ) {
            return level
        }

        let accessLevel = await DatabaseManager.getCharacterTable().getAccessLevelById( objectId )

        if ( _.isNumber( accessLevel ) ) {
            this.idToAccessLevel[ objectId ] = accessLevel
        }

        return accessLevel
    }

    addPlayer( player: L2PcInstance ): void {
        this.idToName[ player.getObjectId() ] = player.getName()
        this.nameToId[ player.getName() ] = player.getObjectId()
        this.idToAccessLevel[ player.objectId ] = player.getAccessLevel().level
    }

    removePlayer( objectId: number ): void {
        let name = this.idToName[ objectId ]
        if ( name ) {
            _.unset( this.nameToId, name )
        }

        _.unset( this.idToName, objectId )
        _.unset( this.idToAccessLevel, objectId )
    }

    async prefetchNames( objectIds: Array<number> ): Promise<void> {
        let unknownObjectIds: Array<number> = objectIds.filter( ( objectId: number ): boolean => {
            return !this.idToName[ objectId ]
        } )

        if ( unknownObjectIds.length === 0 ) {
            return
        }

        let data: Array<L2CharactersTableNamePair> = await DatabaseManager.getCharacterTable().getNameByIds( unknownObjectIds )
        data.forEach( ( item: L2CharactersTableNamePair ) => {
            this.idToName[ item.objectId ] = item.name
            this.nameToId[ item.name ] = item.objectId
        } )
    }

    getCachedNameById( objectId: number ): string {
        return this.idToName[ objectId ]
    }

    getCachedNames( objectIds: Array<number> ): Array<string> {
        return objectIds.reduce( ( allNames: Array<string>, currentId: number ) => {
            let name = CharacterNamesCache.getCachedNameById( currentId )
            if ( name ) {
                allNames.push( name )
            }

            return allNames
        }, [] )
    }
}

export const CharacterNamesCache = new Manager()