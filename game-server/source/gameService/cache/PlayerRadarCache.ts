import { L2Radar } from '../models/L2Radar'

const playerRadars : { [ objectId: number ] : L2Radar } = {}

export const PlayerRadarCache = {
    getRadar( objectId: number ) : L2Radar {
        let radar : L2Radar = playerRadars[ objectId ]

        if ( !radar ) {
            radar = new L2Radar( objectId )
            playerRadars[ objectId ] = radar
        }

        return radar
    },

    removeRadar( objectId: number ) : void {
        delete playerRadars[ objectId ]
    }
}