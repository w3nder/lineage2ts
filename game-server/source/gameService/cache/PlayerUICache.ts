import { DatabaseManager } from '../../database/manager'
import _, { DebouncedFunc } from 'lodash'
import { DataManager } from '../../data/manager'
import { UICategories, UIKeys } from '../../data/interface/UIDataApi'

const categories: { [ playeId: number ]: UICategories } = {}
const keys: { [ playeId: number ]: UIKeys } = {}
const timers: { [ playerId: number ]: DebouncedFunc<() => void> } = {}
const defaultWaitTime: number = 10000
const extendedWaitTime: number = defaultWaitTime * 3

export const PlayerUICache = {
    async loadData( playerId: number ): Promise<void> {
        categories[ playerId ] = await DatabaseManager.getCharacterUICategories().getCategories( playerId )
        keys[ playerId ] = await DatabaseManager.getCharacterUIActions().getKeys( playerId )

        if ( _.isEmpty( categories[ playerId ] ) ) {
            categories[ playerId ] = { ...DataManager.getUIData().getCategories() }
        }

        if ( _.isEmpty( categories[ playerId ] ) ) {
            keys[ playerId ] = { ...DataManager.getUIData().getKeys() }
        }
    },

    async saveToDatabase( playerId: number ): Promise<void> {
        await DatabaseManager.getCharacterUICategories().setCategories( playerId, categories[ playerId ] )
        return DatabaseManager.getCharacterUIActions().setKeys( playerId, keys[ playerId ] )
    },

    hasUIData( playerId: number ): boolean {
        return !!( categories[ playerId ] && keys[ playerId ] )
    },

    setSettings( playerId: number, categoryData: UICategories, keyData: UIKeys ): void {
        categories[ playerId ] = categoryData
        keys[ playerId ] = keyData

        PlayerUICache.scheduleUpdate( playerId )
    },

    scheduleUpdate( playerId: number ): void {
        if ( !timers[ playerId ] ) {
            timers[ playerId ] = _.debounce( PlayerUICache.saveToDatabase.bind( null, playerId ), defaultWaitTime, { maxWait: extendedWaitTime } )
        }

        timers[ playerId ]()
    },

    async removeData( playerId: number ): Promise<void> {
        if ( timers[ playerId ] ) {
            timers[ playerId ].cancel()

            delete timers[ playerId ]
            await PlayerUICache.saveToDatabase( playerId )
        }

        delete categories[ playerId ]
        delete keys[ playerId ]
    },

    getKeys( playerId: number ): UIKeys {
        return keys[ playerId ]
    },

    getCategories( playerId: number ): UICategories {
        return categories[ playerId ]
    },
}