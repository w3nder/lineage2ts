import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { OlympiadGameValues } from '../models/olympiad/AbstractOlympiadGame'
import { DatabaseManager } from '../../database/manager'
import { L2NpcTemplate } from '../models/actor/templates/L2NpcTemplate'
import { DataManager } from '../../data/manager'
import { HeroDiaryEntry, HeroPlayerData } from '../values/HeroValues'
import { L2Clan } from '../models/L2Clan'
import { ConfigManager } from '../../config/ConfigManager'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { BroadcastHelper } from '../helpers/BroadcastHelper'
import { SocialAction, SocialActionType } from '../packets/send/SocialAction'
import { L2HeroDiaryTableItem } from '../../database/interface/HeroDiaryTableApi'
import { CastleManager } from '../instancemanager/CastleManager'
import { Castle } from '../models/entity/Castle'
import { L2HeroesTableItem } from '../../database/interface/HeroesTableApi'
import { L2CharactersTableClanInfo } from '../../database/interface/CharactersTableApi'
import { ClanCache } from './ClanCache'
import { regenerateHeroPacket } from '../packets/send/ExHeroList'
import _ from 'lodash'
import aigle from 'aigle'
import moment from 'moment'

const ACTION_RAID_KILLED = 1
const ACTION_HERO_GAINED = 2
const ACTION_CASTLE_TAKEN = 3

class CacheManager implements L2DataApi {
    heroes: { [ key: number ]: HeroPlayerData }
    allTimeHeroes: { [ key: number ]: HeroPlayerData }
    counts: { [ key: number ]: any }
    fights: { [ key: number ]: Array<any> } = {}
    diaries: { [ key: number ]: Array<HeroDiaryEntry> }
    messages: { [ key: number ]: string }

    getHeroByClass( classId: number ): number {
        let outcome = 0

        _.each( this.heroes, ( data: any, key: string ) => {
            if ( data[ OlympiadGameValues.CLASS_ID ] === classId ) {
                outcome = _.parseInt( key )
                return false
            }
        } )

        return outcome
    }

    isHero( objectId: number ): boolean {
        return _.get( this.heroes, [ objectId, 'isClaimed' ], false )
    }

    async load(): Promise<Array<string>> {
        this.heroes = {}
        this.allTimeHeroes = {}
        this.counts = {}
        this.fights = {}

        this.diaries = {}
        this.messages = {}

        let heroData: Array<L2HeroesTableItem> = await DatabaseManager.getHeroes().getAll()
        let objectIds: Array<number> = _.map( heroData, 'objectId' )
        let heroObjectIds: Array<number> = []

        let cache = this
        _.each( heroData, ( item: L2HeroesTableItem ) => {
            let data: HeroPlayerData = {
                played: item.played,
                allyCrestId: 0,
                allyName: '',
                count: item.count,
                clanCrestId: 0,
                clanName: '',
                classId: item.classId,
                isClaimed: item.isClaimed,
                name: '',
                objectId: item.objectId,
            }

            if ( item.played === 1 ) {
                cache.heroes[ item.objectId ] = data

                if ( !_.isEmpty( item.message ) ) {
                    cache.messages[ item.objectId ] = item.message
                }

                heroObjectIds.push( item.objectId )

                return
            }

            cache.allTimeHeroes[ item.objectId ] = data
        } )

        let objectIdChunks: Array<Array<number>> = _.chunk( objectIds, 512 )
        let clanData: Array<L2CharactersTableClanInfo> = _.flatten( await aigle.resolve( objectIdChunks ).map( async ( currentIds: Array<number> ) => {
            return DatabaseManager.getCharacterTable().getClanInfoByIds( currentIds )
        } ) )

        _.each( clanData, ( item: L2CharactersTableClanInfo ) => {
            let playerData: HeroPlayerData = cache.heroes[ item.objectId ] ? cache.heroes[ item.objectId ] : cache.allTimeHeroes[ item.objectId ]

            playerData.name = item.name

            if ( item.clanId > 0 ) {
                let currentClan: L2Clan = ClanCache.getClan( item.clanId )
                playerData.clanName = currentClan.getName()
                playerData.clanCrestId = currentClan.getCrestId()

                if ( currentClan.getAllyId() > 0 ) {
                    playerData.allyName = currentClan.getAllyName()
                    playerData.allyCrestId = currentClan.getAllyCrestId()
                }
            }
        } )

        let diaryData: Array<L2HeroDiaryTableItem> = await DatabaseManager.getHeroDiary().getItemsByIds( heroObjectIds )
        let diaryDataMap: { [ objectId: number ]: Array<L2HeroDiaryTableItem> } = _.groupBy( diaryData, 'objectId' )

        _.each( diaryDataMap, ( items: Array<L2HeroDiaryTableItem>, objectId: string ) => {
            cache.diaries[ objectId ] = _.map( _.orderBy( items, [ 'time' ], [ 'desc' ] ), cache.extractDiary )
        } )

        // TODO : load fights data that is required to show on whole server
        // TODO : load individual data only on player login, remove such data on player logout

        regenerateHeroPacket()

        return [
            `HeroCache: loaded ${ _.size( this.heroes ) } player heroes`,
            `HeroCache: loaded ${ _.size( this.allTimeHeroes ) } all time heroes`,
        ]
    }

    setHeroMessage( objectId: number, message: string ): void {
        this.messages[ objectId ] = message
    }

    async setRaidBossKilled( objectId: number, npcId: number ): Promise<void> {
        let template: L2NpcTemplate = DataManager.getNpcData().getTemplate( npcId )
        if ( !template ) {
            return
        }

        if ( this.diaries[ objectId ] ) {

            this.diaries[ objectId ].push( {
               date: moment().format( 'YYYY-MM-DD HH' ),
               action: `${ template.getName() } was slain`,
           } )

            return DatabaseManager.getHeroDiary().addEntry( objectId, Date.now(), ACTION_RAID_KILLED, npcId )
        }
    }

    getHeroes(): { [ objectId: number ]: HeroPlayerData } {
        return this.heroes
    }

    async claimHero( player: L2PcInstance ): Promise<void> {
        let data: HeroPlayerData = this.heroes[ player.getObjectId() ]
        if ( !data ) {
            data = {
                played: 0,
                allyCrestId: 0,
                allyName: '',
                count: 0,
                clanCrestId: 0,
                clanName: '',
                classId: 0,
                name: '',
                objectId: player.getObjectId(),
                isClaimed: false,
            }

            this.heroes[ player.getObjectId() ] = data
        }

        data.isClaimed = true

        let clan: L2Clan = player.getClan()
        if ( clan && clan.getLevel() >= 5 ) {
            await clan.addReputationScore( ConfigManager.clan.getHeroPoints(), true )

            let message: Buffer = new SystemMessageBuilder( SystemMessageIds.CLAN_MEMBER_C1_BECAME_HERO_AND_GAINED_S2_REPUTATION_POINTS )
                    .addString( player.getName() )
                    .addNumber( ConfigManager.clan.getHeroPoints() )
                    .getBuffer()
            clan.broadcastDataToOnlineMembers( message )
        }

        await player.setHero( true )
        BroadcastHelper.dataToSelfInRange( player, SocialAction( player.getObjectId(), SocialActionType.HeroClaimed ) )

        player.broadcastUserInfo()

        await this.setHeroGained( player.getObjectId() )
        await this.loadFights( player.getObjectId() )
        await this.loadDiary( player.getObjectId() )

        this.setHeroMessage( player.getObjectId(), '' )

        await this.updateHeroes()

        regenerateHeroPacket()
    }

    async updateHeroes(): Promise<void> {
        // TODO : implement logic, make sure that queries are only touching one table at a time
    }

    updateHeroesWithDefaults(): Promise<void> {
        return DatabaseManager.getHeroes().resetAllPlayed()
    }

    async loadDiary( objectId: number ): Promise<void> {
        let data: Array<L2HeroDiaryTableItem> = await DatabaseManager.getHeroDiary().getItems( objectId )
        this.diaries[ objectId ] = _.map( data, this.extractDiary )
    }

    extractDiary( item: L2HeroDiaryTableItem ): HeroDiaryEntry {
        let action: string = ''

        switch ( item.action ) {
            case ACTION_RAID_KILLED:
                let template: L2NpcTemplate = DataManager.getNpcData().getTemplate( item.value )
                if ( template ) {
                    action = `${ template.getName() } was defeated`
                }

                break

            case ACTION_HERO_GAINED:
                action = 'Gained Hero status'
                break

            case ACTION_CASTLE_TAKEN:
                let castle: Castle = CastleManager.getCastleById( item.value )
                if ( castle ) {
                    action = `${ castle.getName() } Castle was successfully taken`
                }

                break
        }

        return {
            action,
            date: moment( item.time ).format( 'yyyy-MM-dd HH' ),
        }
    }

    async loadFights( objectId: number ): Promise<void> {
        // TODO : implement logic
    }

    setHeroGained( objectId: number ): Promise<void> {
        return this.setDiaryData( objectId, ACTION_HERO_GAINED, 0 )
    }

    setDiaryData( objectId: number, action: number, value: number ): Promise<void> {
        return DatabaseManager.getHeroDiary().addEntry( objectId, Date.now(), action, value )
    }

    isUnclaimedHero( objectId: number ): boolean {
        let data: HeroPlayerData = this.heroes[ objectId ]
        if ( data ) {
            return !data.isClaimed
        }

        return false
    }
}

export const HeroCache = new CacheManager()