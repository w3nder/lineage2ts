import { L2Clan } from '../models/L2Clan'
import { DatabaseManager } from '../../database/manager'
import { SystemMessageBuilder } from '../packets/send/SystemMessage'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { IDFactoryCache } from './IDFactoryCache'
import { L2ClanMember } from '../models/L2ClanMember'
import { ClanPriviledgeHelper } from '../models/ClanPrivilege'
import { PledgeShowInfoUpdate } from '../packets/send/PledgeShowInfoUpdate'
import { PledgeShowMemberListUpdate } from '../packets/send/PledgeShowMemberListUpdate'
import { PledgeShowMemberListAll } from '../packets/send/PledgeShowMemberListAll'
import { ListenerCache } from './ListenerCache'
import {
    ClanWarFinishEvent,
    ClanWarStartEvent,
    EventTerminationResult,
    EventType,
    PlayerCreatedClanEvent,
    PlayerDestroysClanEvent,
    PlayerStartCreateClanEvent,
    PlayerStartDestroyClanEvent,
} from '../models/events/EventType'
import { SiegeManager } from '../instancemanager/SiegeManager'
import { Siege } from '../models/entity/Siege'
import { FortSiege } from '../models/entity/FortSiege'
import { FortSiegeManager } from '../instancemanager/FortSiegeManager'
import { ClanHallSiegeManager } from '../instancemanager/ClanHallSiegeManager'
import { SiegableHall } from '../models/entity/clanhall/SiegableHall'
import { ClanHallAuction } from '../models/auction/ClanHallAuction'
import { AuctionManager } from '../instancemanager/AuctionManager'
import { FortManager } from '../instancemanager/FortManager'
import { Fort } from '../models/entity/Fort'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { L2ClanValues } from '../values/L2ClanValues'
import { L2ClanWarsTableData } from '../../database/interface/ClanWarsTableApi'
import { ConfigManager } from '../../config/ConfigManager'
import { EventPoolCache } from './EventPoolCache'
import _ from 'lodash'
import aigle from 'aigle'
import { L2ClanTableItem } from '../../database/interface/ClanDataTableApi'
import { RoaringBitmap32 } from 'roaring'
import { L2DatabaseOperations } from '../../database/operations/DatabaseOperations'
import Buffer from 'buffer'
import Timeout = NodeJS.Timeout

class Cache implements L2DataApi, L2DatabaseOperations {
    clans: { [ key: number ]: L2Clan } = {}
    scheduledRemovals: { [ clanId: number ]: Timeout } = {}
    clansToUpdate: RoaringBitmap32 = new RoaringBitmap32()
    clanUpdateTimeout: Timeout = null
    clanNameMap : Record<string, number> = {}

    getClan( id: number | string ): L2Clan {
        return this.clans[ id ]
    }

    getClanAllies( allianceId: number ): Array<L2Clan> {
        return _.filter( this.clans, ( clan: L2Clan ) => {
            return clan.getAllyId() === allianceId
        } )
    }

    // TODO : find better way than iterate over all available clans, use hasharray ?
    getClanByName( name: string ): L2Clan {
        let id : number = this.clanNameMap[ name ]
        if ( !id ) {
            return null
        }

        return this.getClan( id )
    }

    async createClan( player: L2PcInstance, name: string ): Promise<L2Clan> {
        if ( !player || !name ) {
            return null
        }

        if ( player.getLevel() < ConfigManager.clan.getMinimumPlayerLevel() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DO_NOT_MEET_CRITERIA_IN_ORDER_TO_CREATE_A_CLAN ) )
            return null
        }

        if ( player.getClanId() !== 0 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_TO_CREATE_CLAN ) )
            return null
        }

        if ( Date.now() < player.getClanCreateExpiryTime() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_MUST_WAIT_XX_DAYS_BEFORE_CREATING_A_NEW_CLAN ) )
            return null
        }

        if ( name.length < 2 || !this.isValidClanName( name ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_NAME_INCORRECT ) )
            return null
        }

        if ( name.length > L2ClanValues.CLAN_NAME_MAX_LENGTH ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_NAME_TOO_LONG ) )
            return null
        }

        let existingClan: L2Clan = this.getClanByName( name )
        if ( existingClan ) {
            let message = new SystemMessageBuilder( SystemMessageIds.S1_ALREADY_EXISTS )
                    .addString( name )
                    .getBuffer()
            player.sendOwnedData( message )
            return null
        }

        if ( ListenerCache.hasGeneralListener( EventType.PlayerStartCreateClan ) ) {
            let data = EventPoolCache.getData( EventType.PlayerStartCreateClan ) as PlayerStartCreateClanEvent

            data.playerId = player.getObjectId()
            data.clanName = name

            let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayerStartCreateClan, data )
            if ( result && result.terminate ) {
                return null
            }
        }

        let clan: L2Clan = new L2Clan( IDFactoryCache.getNextId() )

        clan.setName( name )
        this.recordClan( clan )

        let leader: L2ClanMember = L2ClanMember.fromPlayer( clan.getId(), player )

        clan.setLeader( leader )
        leader.setPlayerInstance( player )
        await DatabaseManager.getClanDataTable().createClan( clan )

        player.setClan( clan )
        player.setPledgeClass( L2ClanMember.calculatePledgeClass( player ) )
        player.setClanPrivileges( ClanPriviledgeHelper.getFullPrivileges() )

        player.sendOwnedData( PledgeShowInfoUpdate( clan ) )
        player.sendOwnedData( PledgeShowMemberListAll( clan ) )
        player.sendOwnedData( PledgeShowMemberListUpdate( player ) )
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_CREATED ) )

        clan.onPledgeShowMemberListAll( [ player.getObjectId() ] )

        if ( ListenerCache.hasGeneralListener( EventType.PlayerCreatedClan ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerCreatedClan ) as PlayerCreatedClanEvent

            eventData.leaderId = player.getObjectId()
            eventData.clanId = clan.getId()
            eventData.clanName = name

            await ListenerCache.sendGeneralEvent( EventType.PlayerCreatedClan, eventData )
        }

        return clan
    }

    async destroyClan( clanId: number ): Promise<void> {
        let clan: L2Clan = this.getClan( clanId )
        if ( !clan ) {
            return
        }

        if ( ListenerCache.hasGeneralListener( EventType.PlayerStartDestroyClan ) ) {
            let terminatedData = EventPoolCache.getData( EventType.PlayerStartDestroyClan ) as PlayerStartDestroyClanEvent

            terminatedData.playerId = clan.getLeaderId()
            terminatedData.clanName = clan.getName()
            terminatedData.clanSize = clan.getMembersCount()
            terminatedData.clanLevel = clan.getLevel()
            terminatedData.clanId = clanId

            let result: EventTerminationResult = await ListenerCache.getGeneralTerminatedResult( EventType.PlayerStartDestroyClan, terminatedData )
            if ( result && result.terminate ) {
                return
            }
        }

        clan.broadcastDataToOnlineMembers( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLAN_HAS_DISPERSED ) )
        let castleId = clan.getCastleId()
        if ( castleId === 0 ) {
            await aigle.resolve( SiegeManager.getSieges() ).each( async ( siege: Siege ) => {
                return siege.removeSiegeClan( clan )
            } )
        }

        let fortId = clan.getFortId()
        if ( fortId === 0 ) {
            await aigle.resolve( FortSiegeManager.getSieges() ).each( async ( siege: FortSiege ) => {
                return siege.removeAttacker( clan )
            } )
        }

        let hallId = clan.getHideoutId()
        if ( hallId === 0 ) {
            await aigle.resolve( ClanHallSiegeManager.getConquerableHalls() ).each( async ( hall: SiegableHall ) => {
                return hall.removeAttacker( clan )
            } )
        }

        let auction: ClanHallAuction = AuctionManager.getAuction( clan.getAuctionIdWithBid() )
        if ( auction ) {
            await auction.cancelBid( clan.getId() )
        }

        /*
            TODO : explore alternatives to item deletion
            - dimensional merchant
            - mail in bulk
            - player warehouse
            - player immediate inventory (may overload, however consider that items are saved)

            Main goal is to preserve items, and allow only player to delete these out of their inventory by dropping or otherwise.
         */
        await clan.getWarehouse().destroyAllItems( clan.getLeaderId(), 'ClanCache:destroyClan' )

        let clanSize = clan.getMembersCount()
        await aigle.resolve( clan.getMembers() ).each( ( member: L2ClanMember ) => {
            return clan.removeClanMember( member.getObjectId(), 0 )
        } )

        this.eraseClan( clan )

        IDFactoryCache.releaseId( clanId )

        await Promise.all( [
            DatabaseManager.getClanDataTable().deleteClan( clanId ),
            DatabaseManager.getClanPrivileges().deleteClan( clanId ),
            DatabaseManager.getClanSkills().deleteClan( clanId ),
            DatabaseManager.getClanSubpledges().deleteClan( clanId ),
            DatabaseManager.getClanWarsTable().deleteClan( clanId ),
            DatabaseManager.getClanNotices().deleteClan( clanId ),
            DatabaseManager.getCastleTable().resetClanTax( clanId ),
        ] )

        if ( fortId > 0 ) {
            let fort: Fort = FortManager.getFortById( fortId )
            if ( fort ) {
                let owner: L2Clan = fort.getOwnerClan()
                if ( clanId === owner.getId() ) {
                    await fort.removeOwner()
                }
            }
        }

        if ( hallId > 0 ) {
            let hall: SiegableHall = ClanHallSiegeManager.getSiegableHall( hallId )
            if ( hall && hall.getOwnerId() === clanId ) {
                hall.free()
            }
        }

        if ( ListenerCache.hasGeneralListener( EventType.PlayerDestroyedClan ) ) {
            let eventData = EventPoolCache.getData( EventType.PlayerDestroyedClan ) as PlayerDestroysClanEvent

            eventData.playerId = clan.getLeaderId()
            eventData.clanId = clanId
            eventData.clanName = clan.getName()
            eventData.clanSize = clanSize
            eventData.clanLevel = clan.getLevel()

            return ListenerCache.sendGeneralEvent( EventType.PlayerDestroyedClan, eventData )
        }
    }

    async storeClanWars( firstClanId: number, secondClanId: number ): Promise<void> {
        let firstClan: L2Clan = this.getClan( firstClanId )
        let secondClan: L2Clan = this.getClan( secondClanId )

        if ( ListenerCache.hasGeneralListener( EventType.ClanWarStart ) ) {
            let eventData: ClanWarStartEvent = {
                clanOneId: firstClanId,
                clanTwoId: secondClanId,
            }

            await ListenerCache.sendGeneralEvent( EventType.ClanWarStart, eventData )
        }

        firstClan.setEnemyClan( secondClan )
        secondClan.setAttackerClan( firstClan )

        firstClan.broadcastClanStatus()
        secondClan.broadcastClanStatus()

        await DatabaseManager.getClanWarsTable().setWar( firstClanId, secondClanId )

        let firstMessage = new SystemMessageBuilder( SystemMessageIds.CLAN_WAR_DECLARED_AGAINST_S1_IF_KILLED_LOSE_LOW_EXP )
                .addString( secondClan.getName() )
                .getBuffer()
        firstClan.broadcastDataToOnlineMembers( firstMessage )

        let secondMessage = new SystemMessageBuilder( SystemMessageIds.CLAN_S1_DECLARED_WAR )
                .addString( firstClan.getName() )
                .getBuffer()
        secondClan.broadcastDataToOnlineMembers( secondMessage )
    }

    async deleteClanWars( firstClanId: number, secondClanId: number ): Promise<void> {
        let firstClan: L2Clan = this.getClan( firstClanId )
        let secondClan: L2Clan = this.getClan( secondClanId )

        if ( ListenerCache.hasGeneralListener( EventType.ClanWarFinish ) ) {
            let eventData: ClanWarFinishEvent = {
                clanOneId: firstClanId,
                clanTwoId: secondClanId,
            }

            await ListenerCache.sendGeneralEvent( EventType.ClanWarFinish, eventData )
        }

        firstClan.deleteEnemyClan( secondClan )
        secondClan.deleteAttackerClan( firstClan )
        firstClan.broadcastClanStatus()
        secondClan.broadcastClanStatus()

        await DatabaseManager.getClanWarsTable().removeWar( firstClanId, secondClanId )

        let firstMessage: Buffer = new SystemMessageBuilder( SystemMessageIds.WAR_AGAINST_S1_HAS_STOPPED )
                .addString( secondClan.getName() )
                .getBuffer()
        firstClan.broadcastDataToOnlineMembers( firstMessage )

        let secondMessage: Buffer = new SystemMessageBuilder( SystemMessageIds.CLAN_S1_HAS_DECIDED_TO_STOP )
                .addString( firstClan.getName() )
                .getBuffer()
        secondClan.broadcastDataToOnlineMembers( secondMessage )
    }

    scheduleRemoveClan( clanId: number, removeImmediately: boolean = false ): void {
        let interval = removeImmediately ? 1000 : Math.max( this.getClan( clanId ).getDissolvingExpiryTime() - Date.now(), 300000 )
        this.scheduledRemovals[ clanId ] = setTimeout( this.runScheduledRemoval.bind( this ), interval, clanId )
    }

    async runScheduledRemoval( clanId: number ): Promise<void> {
        if ( !this.getClan( clanId ) ) {
            return
        }

        if ( this.getClan( clanId ).getDissolvingExpiryTime() !== 0 ) {
            await this.destroyClan( clanId )
        }

        _.unset( this.scheduledRemovals, clanId )
    }

    isAllyExists( name: string ): boolean {
        let matchedName = _.toLower( name )
        return _.some( this.clans, ( clan: L2Clan ) => {
            return _.toLower( clan.getAllyName() ) === matchedName
        } )
    }

    getClans(): { [ key: number ]: L2Clan } {
        return this.clans
    }

    async load(): Promise<Array<string>> {
        let dataItems = await DatabaseManager.getClanDataTable().getAll()
        let clans: Array<L2Clan> = dataItems.map( this.restoreClan )

        this.clans = {}

        clans.forEach( ( clan: L2Clan ) => {
            this.recordClan( clan )

            if ( clan.getDissolvingExpiryTime() !== 0 ) {
                this.scheduleRemoveClan( clan.getId() )
            }
        } )

        // TODO : consider to restore only clans that own residences and load additional clans dynamically on player login
        await aigle.resolve( clans ).eachLimit( 10, async ( clan: L2Clan ) => {
            await clan.restoreRest()

            // TODO : restore clan warehouse only when at least one clan members has logged in
            await clan.getWarehouse().restore()

            let allyId = clan.getAllyId()
            if ( allyId !== 0
                    && clan.getId() !== allyId
                    && !this.clans[ allyId ] ) {
                clan.setAllyId( 0 )
                clan.setAllyName( null )
                clan.setAllyCrestId( 0 )
                clan.scheduleUpdate()
            }
        } )

        let warData: Array<L2ClanWarsTableData> = await DatabaseManager.getClanWarsTable().getAll()

        warData.forEach( ( item: L2ClanWarsTableData ) => {
            let clanOne = this.getClan( item.clanOneId )
            let clanTwo = this.getClan( item.clanTwoId )

            if ( clanOne && clanTwo ) {
                clanOne.setEnemyClan( clanTwo )
                clanTwo.setAttackerClan( clanTwo )
            }
        } )

        return [
            `ClanCache: loaded ${ _.size( this.clans ) } clans.`,
            `ClanCache: scheduled ${ _.size( this.scheduledRemovals ) } clans to remove.`,
            `ClanCache: reset ${ this.clansToUpdate.size } clans to default values.`,
            `ClanCache: processed ${ _.size( warData ) } war declarations.`,
        ]
    }

    isValidClanName( name: string ): boolean {
        return ConfigManager.clan.getClanNameTemplate().test( name )
    }

    scheduleUpdate( id: number ) : void {
        this.clansToUpdate.add( id )

        if ( !this.clanUpdateTimeout ) {
            this.clanUpdateTimeout = setTimeout( this.performClansUpdate.bind( this ), 15000 )
        }
    }

    private performClansUpdate() : Promise<void> {
        this.clanUpdateTimeout = null

        let clans : Array<L2Clan> = []
        this.clansToUpdate.forEach( ( id: number ) => {
            let clan = this.getClan( id )
            if ( clan ) {
                clans.push( clan )
            }
        } )

        this.clansToUpdate.clear()

        if ( clans.length === 0 ) {
            return
        }

        return DatabaseManager.getClanDataTable().updateMultipleClanStatuses( clans )
    }

    private restoreClan( data: L2ClanTableItem ) : L2Clan {
        let clan = new L2Clan( data.id )

        clan.setName( data.name )
        clan.setLevel( data.level )
        clan.setCastleId( data.ownedCastleId )
        clan.bloodAllianceCount = data.bloodAllianceCount

        clan.bloodOathCount = data.bloodOathCount
        clan.setAllyId( data.allyId )
        clan.setAllyName( data.allyName )
        clan.setAllyPenaltyExpiryTime( data.allyPenaltyExpiryTime, data.allyPenaltyType )

        clan.setCharacterPenaltyExpiryTime( data.joinPenaltyExpiryTime )
        clan.setDissolvingExpiryTime( data.dissolveExpiryTime )
        clan.setCrestId( data.crestId )
        clan.setCrestLargeId( data.crestLargeId )

        clan.setAllyCrestId( data.allyCrestId )
        clan.setReputationScoreDirect( data.reputation )
        clan.auctionIdWithBid = data.lastAuctionBidId
        clan.newLeaderId = data.newLeaderId

        clan.setLeaderId( data.currentLeaderId )

        return clan
    }

    private recordClan( clan: L2Clan ) : void {
        this.clans[ clan.getId() ] = clan
        this.clanNameMap[ clan.getName().toLowerCase() ] = clan.getId()
    }

    private eraseClan( clan: L2Clan ) : void {
        delete this.clans[ clan.getId() ]
        delete this.clanNameMap[ clan.getName().toLowerCase() ]
    }

    shutdown(): Promise<void> {
        clearTimeout( this.clanUpdateTimeout )

        if ( this.clansToUpdate.size > 0 ) {
            return this.performClansUpdate()
        }
    }

    async getClanForMember( objectId: number ) : Promise<L2Clan> {
        let clanId = await DatabaseManager.getCharacterTable().getClanIdForCharacter( objectId )

        return this.getClan( clanId )
    }
}

export const ClanCache = new Cache()