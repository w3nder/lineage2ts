import { L2Attackable } from '../models/actor/L2Attackable'
import { DataManager } from '../../data/manager'
import { AIIntent } from '../aicontroller/enums/AIIntent'
import { EmptyTrait } from '../aicontroller/traits/EmptyTrait'
import { PathWalkingTrait } from '../aicontroller/traits/npc/PathWalkingTrait'
import { SocialAnimationTrait } from '../aicontroller/traits/npc/SocialAnimationTrait'
import { DefaultFleeingTrait } from '../aicontroller/traits/npc/defaults/DefaultFleeingTrait'
import { InteractWaitingTrait } from '../aicontroller/traits/npc/InteractWaitingTrait'
import { DefaultCastTrait } from '../aicontroller/traits/npc/defaults/DefaultCastTrait'
import { NonMovingCastAnyRange } from '../aicontroller/traits/npc/NonMovingCastTrait'
import { DefaultReturnTrait } from '../aicontroller/traits/npc/defaults/DefaultReturnTrait'
import { RandomWalkTrait } from '../aicontroller/traits/npc/RandomWalkTrait'
import { InstanceType } from '../enums/InstanceType'
import { IAITrait, TraitSettings } from '../aicontroller/interface/IAITrait'
import { DefaultAttackTrait } from '../aicontroller/traits/npc/defaults/DefaultAttackTrait'
import { AISkillFocus, AISkillUsageBehavior } from '../enums/AISkillUsage'
import { AllSkillsWarriorTrait } from '../aicontroller/traits/npc/AllSkillsWarriorTrait'
import {
    AttackSkillWarriorTrait,
    BuffSkillWarriorTrait,
    SpecialSkillWarriorTrait
} from '../aicontroller/traits/npc/SkillFocusWarriorTrait'
import { AllSkillsMageTrait } from '../aicontroller/traits/npc/AllSkillsMageTrait'
import {
    AttackSkillMageTrait,
    BuffSkillMageTrait,
    SpecialSkillMageTrait
} from '../aicontroller/traits/npc/SkillFocusMageTrait'
import { L2Object } from '../models/L2Object'
import { L2Npc } from '../models/actor/L2Npc'

class Manager {
    npcIdTraits: Record<number, TraitSettings> = {}

    getNpcTraits( npc: L2Object ) : Readonly<TraitSettings> {
        let settings = this.npcIdTraits[ npc.getId() ]
        if ( settings ) {
            return settings
        }

        return this.getDefaultNpcTraits( npc as L2Npc )
    }

    setNpcTraits( npcId: number, settings: TraitSettings ) : void {
        this.npcIdTraits[ npcId ] = settings
    }

    setManyNpcTraits( npcIds: ReadonlyArray<number>, settings: TraitSettings ) : void {
        for ( const npcId of npcIds ) {
            this.setNpcTraits( npcId, settings )
        }
    }

    private getDefaultNpcTraits( npc: L2Npc ): TraitSettings {
        let template = DataManager.getNpcData().getTemplate( npc.getId() )
        if ( !template ) {
            return this.createEmptyTraits()
        }

        if ( npc.isAttackable() ) {
            return this.createAttackableTraits( npc as L2Attackable )
        }

        return this.createDefaultTraits( npc as L2Npc )
    }

    createEmptyTraits() : TraitSettings {
        return {
            [ AIIntent.WAITING ]: EmptyTrait,
            [ AIIntent.ATTACK ]: EmptyTrait,
            [ AIIntent.FLEE ]: EmptyTrait,
            [ AIIntent.INTERACT ]: EmptyTrait,
            [ AIIntent.CAST ]: EmptyTrait,
            [ AIIntent.RETURN ]: EmptyTrait,
            [ AIIntent.FOLLOW ]: EmptyTrait,
            [ AIIntent.REST ]: EmptyTrait,
        }
    }

    private createDefaultTraits( npc: L2Npc ): TraitSettings {
        return {
            [ AIIntent.WAITING ]: npc.hasWalkingRoute() ? PathWalkingTrait : SocialAnimationTrait,
            [ AIIntent.ATTACK ]: EmptyTrait,
            [ AIIntent.FLEE ]: DefaultFleeingTrait,
            [ AIIntent.INTERACT ]: InteractWaitingTrait,
            [ AIIntent.CAST ]: npc.isMoveCapable() ? DefaultCastTrait : NonMovingCastAnyRange,
            [ AIIntent.RETURN ]: npc.isMoveCapable() ? DefaultReturnTrait : EmptyTrait,
            [ AIIntent.FOLLOW ]: EmptyTrait,
            [ AIIntent.REST ]: EmptyTrait,
        }
    }

    private createAttackableTraits( npc: L2Attackable ): TraitSettings {
        return {
            [ AIIntent.WAITING ]: npc.hasWalkingRoute() ? PathWalkingTrait : RandomWalkTrait,
            [ AIIntent.ATTACK ]: this.getAttackTrait( npc ),
            [ AIIntent.RETURN ]: this.canReturnToSpawnPoint( npc ) ? DefaultReturnTrait : EmptyTrait,
            [ AIIntent.INTERACT ]: InteractWaitingTrait,
            [ AIIntent.CAST ]: DefaultCastTrait,
            [ AIIntent.FLEE ]: DefaultFleeingTrait,
            [ AIIntent.FOLLOW ]: EmptyTrait,
            [ AIIntent.REST ]: EmptyTrait,
        }
    }

    private canReturnToSpawnPoint( npc: L2Attackable ): boolean {
        return npc.canReturnToSpawnPoint()
            || ( npc.isGuard() && !npc.hasWalkingRoute() )
            || !npc.isInstanceType( InstanceType.L2FestivalMonsterInstance )
    }

    private getAttackTrait( npc: L2Attackable ) : IAITrait {
        let skillUsage = npc.getTemplate().aiSkillUsage
        if ( !skillUsage ) {
            return DefaultAttackTrait
        }

        if ( skillUsage.ai === AISkillUsageBehavior.Physical ) {
            switch ( skillUsage.focus ) {
                case AISkillFocus.AllSkills:
                    return AllSkillsWarriorTrait

                case AISkillFocus.Attack:
                    return AttackSkillWarriorTrait

                case AISkillFocus.Buff:
                    return BuffSkillWarriorTrait

                case AISkillFocus.Special:
                    return SpecialSkillWarriorTrait
            }
        }

        if ( skillUsage.ai === AISkillUsageBehavior.Magical ) {
            switch ( skillUsage.focus ) {
                case AISkillFocus.AllSkills:
                    return AllSkillsMageTrait

                case AISkillFocus.Attack:
                    return AttackSkillMageTrait

                case AISkillFocus.Buff:
                    return BuffSkillMageTrait

                case AISkillFocus.Special:
                    return SpecialSkillMageTrait
            }
        }

        return DefaultAttackTrait
    }
}

export const AITraitCache = new Manager()