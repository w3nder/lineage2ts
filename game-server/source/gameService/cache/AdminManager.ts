import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder, SystemMessageHelper } from '../packets/send/SystemMessage'
import { PacketDispatcher } from '../PacketDispatcher'
import { SystemMessageIds } from '../packets/SystemMessageIdValues'
import { L2World } from '../L2World'
import _ from 'lodash'

class Manager {
    gameMasterMap: Map<number, boolean> = new Map<number, boolean>()

    addGm( objectId : number, isHidden: boolean ): void {
        this.gameMasterMap[ objectId ] = isHidden
    }

    broadcastMessageToGMs( message: string ) {
        let packet: Buffer = SystemMessageHelper.sendString( message )

        this.gameMasterMap.forEach( ( unused: boolean, playerId: number ) => {
            PacketDispatcher.sendCopyData( playerId, packet )
        } )
    }

    deleteGm( objectId : number ): void {
        delete this.gameMasterMap[ objectId ]
    }

    getAllGMs() : Array<number> {
        return Array.from( this.gameMasterMap.keys() )
    }

    isGMOnline( includeHidden: boolean = true ) {
        return _.some( this.gameMasterMap, ( isHidden: boolean ) => {
            return isHidden || !includeHidden
        } )
    }

    sendGMList( player: L2PcInstance ) {
        if ( this.isGMOnline( player.isGM() ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.GM_LIST ) )

            this.getNames( player.isGM() ).forEach( ( name: string ) => {
                let packet = new SystemMessageBuilder( SystemMessageIds.GM_C1 )
                        .addString( name )
                        .getBuffer()
                player.sendOwnedData( packet )
            } )
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_GM_PROVIDING_SERVICE_NOW ) )
    }

    getNames( includeHidden: boolean ) {
        let names : Array<string> = []

        this.gameMasterMap.forEach( ( isHidden: boolean, playerId: number ) => {
            let adminPlayer = L2World.getPlayer( playerId )

            if ( !isHidden && adminPlayer ) {
                names.push( adminPlayer.getName() )
                return
            }

            if ( includeHidden && adminPlayer ) {
                names.push( `${adminPlayer.getName()} [ invisible ]` )
            }
        } )

        return names
    }
}

export const AdminManager = new Manager()