import { L2DataApi } from '../../data/interface/l2DataApi'
import { AllCommands, CommandPermissions, PlayerAccess } from '../models/PlayerAccess'
import { DataManager } from '../../data/manager'
import { L2PlayerAccessDataItem } from '../../data/interface/PlayerAccessDataApi'
import { AllPermissions, PlayerPermission } from '../enums/PlayerPermission'
import _ from 'lodash'

/*
    TODO : consider adding ability to persist access modifications
    - loading data from database, not data layer
    - allow admin commands to add/modify access to non-gm access levels
 */

const DefaultAdminAccess = new PlayerAccess( 'Static Master Access', 100, 0x00CCFF, 0x00CCFF, AllPermissions, AllCommands )

class Manager implements L2DataApi {
    private levels : Record<number, PlayerAccess>

    async load(): Promise<Array<string>> {
        this.levels = {}

        let items = await DataManager.getPlayerAccess().getAll()

        items.forEach( ( item: L2PlayerAccessDataItem ) => {
            this.levels[ item.level ] = this.createAccess( item )
        } )

        return [
            `PlayerAccess: loaded ${_.size( this.levels )} levels`
        ]
    }

    private createAccess( item: L2PlayerAccessDataItem ) : PlayerAccess {
        let nameColor = parseInt( `0x${item.nameColor}` )
        let titleColor = parseInt( `0x${item.titleColor}` )

        if ( !Number.isInteger( nameColor ) ) {
            nameColor = 0xFFFFFF
        }

        if ( !Number.isInteger( titleColor ) ) {
            titleColor = 0xFFFFFF
        }

        let permissions = _.reduce( item.permissions, ( allPermissions: Set<PlayerPermission>, item: string ) : Set<PlayerPermission> => {
            let value = PlayerPermission[ item ]
            if ( Number.isInteger( value ) ) {
                allPermissions.add( value )
            }

            return allPermissions
        }, new Set<PlayerPermission> )

        let commands : CommandPermissions = {
            allowed: item.commands.allowedRegex ? new RegExp( item.commands.allowedRegex ) : null,
            confirmation: item.commands.confirmationRegex ? new RegExp( item.commands.confirmationRegex ) : null,
            restricted: item.commands.restrictedRegex ? new RegExp( item.commands.restrictedRegex ) : null
        }

        return new PlayerAccess( item.name, item.level, nameColor, titleColor, permissions, commands )
    }

    getLevel( level: number ) : PlayerAccess {
        return this.levels[ level ]
    }

    getAdminLevel() : PlayerAccess {
        return DefaultAdminAccess
    }

    getAllLevels() : Readonly<Array<PlayerAccess>> {
        return Object.values( this.levels )
    }
}

export const PlayerAccessCache = new Manager()