import { L2DimensionalTransferItem } from '../models/items/L2DimensionalTransferItem'
import { DatabaseManager } from '../../database/manager'
import _, { DebouncedFunc } from 'lodash'
import { RoaringBitmap32 } from 'roaring'
import { ListenerCache } from './ListenerCache'
import {
    DimensionalItemAddEvent,
    DimensionalItemRemoveEvent,
    DimensionalItemUpdateEvent,
    EventType
} from '../models/events/EventType'

// TODO : add 90 day expiration time
class Manager {
    items : { [ playerId : number ] : Array<L2DimensionalTransferItem> } = {}
    debounceItemUpdate: DebouncedFunc<() => void>
    debounceItemDelete: DebouncedFunc<() => void>
    idsToUpdate: RoaringBitmap32 = new RoaringBitmap32()
    idsToDelete: RoaringBitmap32 = new RoaringBitmap32()

    constructor() {
        this.debounceItemUpdate = _.debounce( this.runUpdate.bind( this ), 5000, {
            maxWait: 10000
        } )

        this.debounceItemDelete = _.debounce( this.runDelete.bind( this ), 5000, {
            maxWait: 10000
        } )
    }

    async loadPlayer( playerId : number ) : Promise<void> {
        this.items[ playerId ] = await DatabaseManager.getDimensionalTransferItems().getItems( playerId )
    }

    getItems( playerId : number ) : Array<L2DimensionalTransferItem> {
        return this.items[ playerId ]
    }

    hasItems( playerId: number ) : boolean {
        return this.items[ playerId ] && this.items[ playerId ].length > 0
    }

    removeItem( playerId : number, itemId: number ) : void {
        _.remove( this.items[ playerId ], { itemId } )

        this.onRemoveItem( playerId, itemId )

        if ( !this.hasItems( playerId ) ) {
            this.unLoadPlayer( playerId )

            this.idsToDelete.add( playerId )
            this.debounceItemDelete()

            return
        }

        this.idsToUpdate.add( playerId )
        this.debounceItemUpdate()
    }

    private getExistingItem( playerId: number, itemId: number ) : L2DimensionalTransferItem {
        return _.find( this.items[ playerId ], { itemId } )
    }

    reduceItemAmount( playerId : number, itemId: number, amount: number ) : void {
        let existingItem : L2DimensionalTransferItem = this.getExistingItem( playerId, itemId )
        if ( !existingItem ) {
            return
        }

        if ( existingItem.amount <= amount ) {
            return this.removeItem( playerId, itemId )
        }

        return this.updateItem( playerId, itemId, existingItem.amount - amount, existingItem )
    }

    updateItem( playerId: number, itemId : number, amount : number, existingItem: L2DimensionalTransferItem = this.getExistingItem( playerId, itemId ) ) : void {
        if ( !existingItem ) {
            return
        }

        let oldAmount = existingItem.amount

        existingItem.amount = amount
        existingItem.updateDate = Date.now()

        this.idsToUpdate.add( playerId )
        this.debounceItemUpdate()

        this.onUpdateItem( playerId, existingItem, oldAmount )
    }

    /*
        We don't want to deal with item properties such as enchant value or others since it complicates
        item description shown in client. Best way is to provide basic item information.
        Player must be notified that he has item via separate packet.
     */
    addItem( playerId: number, itemId : number, amount : number, senderName : string = null ) : void {
        let existingItem : L2DimensionalTransferItem = this.getExistingItem( playerId, itemId )
        if ( existingItem ) {
            let oldAmount = existingItem.amount
            existingItem.amount = existingItem.amount + amount

            if ( senderName ) {
                existingItem.sender = senderName
            }

            existingItem.updateDate = Date.now()
            this.onUpdateItem( playerId, existingItem, oldAmount )

            return
        }

        let addedItem : L2DimensionalTransferItem = {
            amount,
            itemId,
            sender: _.defaultTo( senderName, '' ),
            createDate: Date.now(),
            updateDate: Date.now()
        }

        this.initializePlayer( playerId )
        this.items[ playerId ].push( addedItem )
        this.idsToUpdate.add( playerId )
        this.onAddItem( playerId, addedItem )

        this.debounceItemUpdate()
    }

    unLoadPlayer( playerId : number ) : void {
        delete this.items[ playerId ]
    }

    private initializePlayer( playerId : number ) : void {
        if ( this.items[ playerId ] ) {
            return
        }

        this.items[ playerId ] = []
    }

    removeAll( playerId: number ) : void {
        this.unLoadPlayer( playerId )
        this.idsToDelete.add( playerId )
        this.debounceItemDelete()
    }

    private async runUpdate() : Promise<void> {
        let playerIds = this.idsToUpdate.toArray()
        this.idsToUpdate.clear()

        for ( const playerId of playerIds ) {
            let items = this.getItems( playerId )
            if ( items ) {
                await DatabaseManager.getDimensionalTransferItems().updateItems( playerId, items )
            }
        }
    }

    private async runDelete() : Promise<void> {
        let playerIds = this.idsToDelete.toArray()
        this.idsToDelete.clear()

        for ( const playerId of playerIds ) {
            let items = this.getItems( playerId )
            if ( !items ) {
                await DatabaseManager.getDimensionalTransferItems().removeCharacter( playerId )
            }
        }
    }

    private onUpdateItem( playerId: number, item : L2DimensionalTransferItem, oldAmount: number ) : void {
        if ( ListenerCache.hasGeneralListener( EventType.DimensionalItemUpdate ) ) {
            let parameters : DimensionalItemUpdateEvent = {
                itemId: item.itemId,
                newAmount: item.amount,
                oldAmount,
                playerId
            }

            ListenerCache.sendGeneralEvent( EventType.DimensionalItemUpdate, parameters, item.itemId )
        }
    }

    private onAddItem( playerId: number, item : L2DimensionalTransferItem ) : void {
        if ( ListenerCache.hasGeneralListener( EventType.DimensionalItemAdd ) ) {
            let parameters : DimensionalItemAddEvent = {
                itemId: item.itemId,
                amount: item.amount,
                playerId
            }

            ListenerCache.sendGeneralEvent( EventType.DimensionalItemAdd, parameters, item.itemId )
        }
    }

    private onRemoveItem( playerId: number, itemId: number ) : void {
        if ( ListenerCache.hasGeneralListener( EventType.DimensionalItemRemove ) ) {
            let parameters : DimensionalItemRemoveEvent = {
                itemId,
                playerId
            }

            ListenerCache.sendGeneralEvent( EventType.DimensionalItemRemove, parameters, itemId )
        }
    }
}

export const DimensionalTransferManager = new Manager()