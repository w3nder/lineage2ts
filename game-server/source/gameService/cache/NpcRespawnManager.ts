import { L2DataApi } from '../../data/interface/l2DataApi'
import _, { DebouncedFunc } from 'lodash'
import { L2NpcRespawnTableItem } from '../../database/interface/NpcRespawnTableApi'
import { DatabaseManager } from '../../database/manager'

class Manager implements L2DataApi {
    records: Record<string, L2NpcRespawnTableItem>
    idsToRemove: Set<string> = new Set<string>()
    idsToUpdate: Set<string> = new Set<string>()

    debounceDeletion: DebouncedFunc<() => void>
    debounceUpdate: DebouncedFunc<() => void>

    constructor() {
        this.debounceDeletion = _.debounce( this.runDeleteRecords.bind( this ), 30000, {
            maxWait: 60000
        } )

        this.debounceUpdate = _.debounce( this.runUpdateRecords.bind( this ), 15000, {
            maxWait: 30000
        } )
    }

    async load(): Promise<Array<string>> {
        this.records = _.keyBy( await DatabaseManager.getNpcRespawns().getAll(), item => item.id )
        return [
            `NpcRespawnManager: loaded ${_.size( this.records )} records`
        ]
    }

    getRecord( id: string ) : L2NpcRespawnTableItem {
        return this.records[ id ]
    }

    removeRecord( id: string ) : void {
        this.idsToRemove.add( id )
        this.idsToUpdate.delete( id )

        delete this.records[ id ]

        this.debounceDeletion()
    }

    updateRecord( data: L2NpcRespawnTableItem ) : void {
        this.records[ data.id ] = data

        this.idsToUpdate.add( data.id )
        this.idsToRemove.delete( data.id )

        this.debounceUpdate()
    }

    private runDeleteRecords() : Promise<void> {
        if ( this.idsToRemove.size === 0 ) {
            return
        }

        let ids = Array.from( this.idsToRemove )

        this.idsToRemove.clear()

        return DatabaseManager.getNpcRespawns().remove( ids )
    }

    private runUpdateRecords() : Promise<void> {
        if ( this.idsToUpdate.size === 0 ) {
            return
        }

        let ids = Array.from( this.idsToUpdate )

        this.idsToUpdate.clear()

        let records = ids.reduce( ( items: Array<L2NpcRespawnTableItem>, currentId : string ) : Array<L2NpcRespawnTableItem> => {
            let item = this.records[ currentId ]

            if ( item ) {
                items.push( item )
            }

            return items
        }, [] )

        return DatabaseManager.getNpcRespawns().updateAll( records )
    }
}

export const NpcRespawnManager = new Manager()