export enum WarehouseListType {
    WEAPON,
    ARMOR,
    ETCITEM,
    MATERIAL,
    RECIPE,
    AMULETT,
    SPELLBOOK,
    SHOT,
    SCROLL,
    CONSUMABLE,
    SEED,
    POTION,
    QUEST,
    PET,
    OTHER,
    ALL
}