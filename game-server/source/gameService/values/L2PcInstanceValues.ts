export const enum L2PcInstanceValues {
    EmptyId = -1,
    RequestDurationMillis = 15000,
    FALLING_VALIDATION_DELAY = 10000
}