export const enum L2NpcValues {
    interactionDistance = 150,
    itemDropRange = 75,
    staticAnimationInterval = 15,
    sinEaterNpcId = 12564
}