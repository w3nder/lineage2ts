export const enum MultisellValues {
    PC_BANG_POINTS = -100,
    CLAN_REPUTATION = -200,
    FAME = -300,
    PAGE_SIZE = 40
}