export const StatsMultipliersPropertyName = 'characterMultipliers'
export const PremiumMultipliersPropertyName = 'premiumMultipliers'

export const enum MultipliersVariableNames {
    exp = 'expBonus',
    sp = 'spBonus',
    vitality = 'vitalityBonus',
    vitalityPoints = 'vitalityPoints',
    extractableItems = 'extractableItemBonus',
    fishingExperienceBonus = 'fishingExperienceBonus',
    enchantScrollBonus = 'enchantScrollBonus'
}

export const enum CommonVariables {
    AutoLootEnabled = 'autoloot.enabled',
    AutoLootItemsEnabled = 'autoloot.items',
    AutoLootHerbsEnabled = 'autoloot.herbs'
}