export const enum SkillItemValues {
    NormalEnchantBook = 6622,
    SafeEnchantBook = 9627,
    ChangeEnchantBook = 9626,
    UntrainEnchantBook = 9625,
}