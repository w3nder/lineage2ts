import { GameClient } from './GameClient'
import { L2GameClientRegistry } from './L2GameClientRegistry'
import { ConfigManager } from '../config/ConfigManager'
import _, { DebouncedFunc } from 'lodash'
import { PacketHelper } from './packets/PacketVariables'
export type DebouncedPacketMethod = ( id : number ) => Buffer
type DebouncedPacketMap = Map<DebouncedPacketMethod, DebouncedFunc<DebouncedPacketMethod>>

const throttledPacketMap = new Map<number, DebouncedPacketMap>()

export const PacketDispatcher = {
    /*
        Some packets such as sound, are pre-generated for use, ofter used liberally in quests.
        Other uses would include packets destined to a group of players. In such case,
        overhead producing same packet over and over can be avoided by copying data to each client.
        Since memory is easy to come by, there should be minimal impact compared to re-generation
        of packet.
     */
    sendCopyData( whoIsSending: number, data: Buffer ) : void {
        let gameClient: GameClient = L2GameClientRegistry.getClientByPlayerId( whoIsSending )

        if ( gameClient ) {
            gameClient.sendPacket( PacketHelper.copyPacket( data ) )
        }
    },

    /*
        Main difference is usage, when packet is constructed we want to avoid copying buffer contents
        due to packet size that is currently added in GameClient.sendPacket. Packet data have
        packet size already specified/set when Buffer is produced, then packet can be encrypted in-place
        without any intermediary memory for un-encrypted data.
     */
    sendOwnedData( whoIsSending: number, data: Buffer ) : void {
        let gameClient: GameClient = L2GameClientRegistry.getClientByPlayerId( whoIsSending )

        if ( gameClient ) {
            gameClient.sendPacket( data )
        }
    },

    /*
        Packets that often are called to be updated/re-generated would need to be debounced.
        Such strategy allows virtually unlimited calls to occur in short period of time,
        while only generating one call to send data to client.
        Artificial limitations are put into place to limit such calls to packets that only accept
        objectId of player as input. Such limitations allow less memory for caching function parameters
        and to make code readable, while avoiding mistakes of positional argument types.
     */
    sendDebouncedPacket( whoIsSending: number, packetProducingMethod: DebouncedPacketMethod ) : void {
        let gameClient: GameClient = L2GameClientRegistry.getClientByPlayerId( whoIsSending )

        if ( gameClient ) {
            let packetMap: DebouncedPacketMap = throttledPacketMap.get( whoIsSending )
            let throttledMethod: DebouncedFunc<DebouncedPacketMethod> = packetMap.get( packetProducingMethod )
            if ( throttledMethod ) {
                throttledMethod( whoIsSending )
                return
            }

            throttledMethod = _.debounce( ( id: number ) => {
                let client = L2GameClientRegistry.getClientByPlayerId( id )
                if ( !client || client.isDetached() ) {
                    return
                }

                client.sendPacket( packetProducingMethod( id ) )
            }, ConfigManager.server.getPacketDebounceInterval(), { trailing: true, maxWait: ConfigManager.server.getPacketDebounceInterval() + 200 } )

            packetMap.set( packetProducingMethod, throttledMethod )

            throttledMethod( whoIsSending )
        }
    },

    cancelDebouncedPacket( whoIsSending: number, packetProducingMethod: DebouncedPacketMethod ) : void {
        let packetMap: DebouncedPacketMap = throttledPacketMap.get( whoIsSending )
        if ( !packetMap ) {
            return
        }

        let throttledMethod: DebouncedFunc<DebouncedPacketMethod> = packetMap.get( packetProducingMethod )
        if ( !throttledMethod ) {
            return
        }

        throttledMethod.cancel()
    },

    createDebouncedPackets( objectId: number ): void {
        if ( throttledPacketMap.has( objectId ) ) {
            return
        }

        throttledPacketMap.set( objectId, new Map<DebouncedPacketMethod, DebouncedFunc<DebouncedPacketMethod>>() )
    },

    removeDebouncedPackets( objectId: number ): void {
        let packets = throttledPacketMap.get( objectId )
        if ( !packets ) {
            return
        }

        packets.forEach( ( method : DebouncedFunc<DebouncedPacketMethod> ) => method.cancel() )
        throttledPacketMap.delete( objectId )
    },
}