import { L2PcInstance } from '../models/actor/instance/L2PcInstance'

export interface IAdminCommand {
    onCommand( command: string, player: L2PcInstance ) : Promise<void>
    getCommandNames() : Array<string>
    getDescription( command: string ) : Array<string>
}