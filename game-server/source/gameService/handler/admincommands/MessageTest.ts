import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import _ from 'lodash'
import { formatOptionsDescription } from './helpers/FormatHelpers'
import parse from 'yargs-parser'

const enum MessageCommands {
    SendMessageId = 'message',
    SendMessageParameters = 'message-parameters'
}

export const MessageTest: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            MessageCommands.SendMessageId,
            MessageCommands.SendMessageParameters,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case MessageCommands.SendMessageId:
                return [
                    formatOptionsDescription( MessageCommands.SendMessageId, '< systemMessageId >', 'Execute message to display its wording' ),
                ]

            case MessageCommands.SendMessageParameters:
                return [
                    formatOptionsDescription( MessageCommands.SendMessageParameters, '< systemMessageId > [item:Id] [skill:Id] [npc:Id] [zone:x,y,x] [castle:Id] [text:\'text\']', 'Allows to specify parameters inline to display system message' ),
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case MessageCommands.SendMessageId:
                return onMessage( commandChunks, player )

            case MessageCommands.SendMessageParameters:
                return onMessageParameters( command, commandChunks, player )
        }
    },
}

function onMessage( commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ currentCommand, messageIdValue ] = commandChunks

    if ( !messageIdValue ) {
        return
    }

    let messageId: number = _.parseInt( messageIdValue )
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( messageId ) )
}

function onMessageParameters( command: string, commandChunks: Array<string>, player: L2PcInstance ): void {
    let [ currentCommand, messageIdValue ] = commandChunks

    if ( !messageIdValue ) {
        return
    }

    let messageId: number = _.parseInt( messageIdValue )
    let packet = new SystemMessageBuilder( messageId )

    let parameters = parse( command )

    parameters._.slice( 2 ).forEach( ( parameter: string ) => {
        let [ type, data ] = _.split( parameter, ':' )

        if ( !data ) {
            return
        }

        switch ( type ) {
            case 'item':
                let itemId = _.defaultTo( _.parseInt( data ), 1 )
                packet.addItemNameWithId( itemId )
                return

            case 'skill':
                let skillId = _.defaultTo( _.parseInt( data ), 1 )
                packet.addSkillNameById( skillId )
                return

            case 'zone':
                let [ x, y, z ] = _.split( data, ',' ).map( value => _.parseInt( value ) )
                if ( x && y && z ) {
                    packet.regionFromCoordinates( x, y, z )
                }
                return

            case 'castle':
                let castleId = _.defaultTo( _.parseInt( data ), 1 )
                packet.addCastleId( castleId )
                return

            case 'text':
                packet.addString( data.replaceAll( '\'', '' ) )
                return

            case 'npc':
                let npcId = _.defaultTo( _.parseInt( data ), 1 )
                packet.addNpcNameWithTemplateId( npcId )
                return
        }
    } )

    player.sendOwnedData( packet.getBuffer() )
}