import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { GeoPolygonCache } from '../../cache/GeoPolygonCache'
import { L2Object } from '../../models/L2Object'
import { L2World } from '../../L2World'
import { L2WorldRegion } from '../../models/L2WorldRegion'
import { L2Character } from '../../models/actor/L2Character'
import { ValidateLocation } from '../../packets/send/ValidateLocation'
import _ from 'lodash'
import { PointGeometry } from '../../models/drops/PointGeometry'
import { GeometryId } from '../../enums/GeometryId'
import { DrawHelper } from './helpers/DrawHelper'
import { AdminHtml } from './helpers/AdminHtml'
import { PlayerMovementActionCache } from '../../cache/PlayerMovementActionCache'

const enum GeoCommands {
    PositionCheck = 'geo-check',
    TargetCheck = 'geo-target',
    Sync = 'geo-sync',
    Region = 'geo-region',
    HeightMap = 'geo-height-map',
    Menu = 'geo-menu',
    MoveInfo = 'geo-move-info'
}

const enum GeoHeightColor {
    Normal = 0x26F196
}

export const Geo: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            GeoCommands.PositionCheck,
            GeoCommands.TargetCheck,
            GeoCommands.Sync,
            GeoCommands.Region,
            GeoCommands.HeightMap,
            GeoCommands.Menu,
            GeoCommands.MoveInfo,
        ]
    },

    getDescription( command: GeoCommands ): Array<string> {
        switch ( command ) {
            case GeoCommands.PositionCheck:
                return [
                    formatDescription( GeoCommands.PositionCheck, 'Show current player position height' ),
                ]

            case GeoCommands.TargetCheck:
                return [
                    formatDescription( GeoCommands.TargetCheck, 'Show target position height' ),
                ]

            case GeoCommands.Sync:
                return [
                        formatOptionsDescription( GeoCommands.Sync, '[ height = 0 ]', 'Assign current player (or optionally target) current geo position with additional Z height added.' ),
                        'If player has target selected, target is used for such operation. Otherwise, current player will have (x,y,z) position synchronized to geodata.'
                ]

            case GeoCommands.Region:
                return [
                        formatDescription( GeoCommands.Region, 'Shows region and geo tile information' )
                ]

            case GeoCommands.HeightMap:
                return [
                    formatDescription( GeoCommands.HeightMap, 'Shows surrounding geodata height grid' )
                ]

            case GeoCommands.Menu:
                return [
                    formatDescription( GeoCommands.Menu, 'Shows available geo commands' )
                ]

            case GeoCommands.MoveInfo:
                return [
                    formatDescription( GeoCommands.MoveInfo, 'Shows information about geodata for clicked position' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case GeoCommands.PositionCheck:
                return performPositionCheck( player, player )

            case GeoCommands.TargetCheck:
                return performPositionCheck( player.getTarget(), player )

            case GeoCommands.Sync:
                return syncPosition( player.getTarget(), player, commandChunks )

            case GeoCommands.Region:
                return showRegion( player )

            case GeoCommands.HeightMap:
                return onHeightMap( player )

            case GeoCommands.Menu:
                return AdminHtml.showHtml( player, 'overrides/html/command/geo/menu.htm' )

            case GeoCommands.MoveInfo:
                player.sendMessage( 'Geo Move Info ready. Click where you want to see geo-data information' )
                return PlayerMovementActionCache.setAction( player.getObjectId(), onMoveInfoClicked )
        }
    },
}

function performPositionCheck( object: L2Object, player: L2PcInstance ): void {
    if ( !object ) {
        return player.sendMessage( 'Target unavailable, cannot display coordinates.' )
    }

    let geoZ = GeoPolygonCache.getObjectZ( object )
    player.sendMessage( `"${ object.getName() }" geo position (x = ${object.getX()}, y = ${object.getY()}, z = ${geoZ}): current Z = ${object.getZ()}` )
}

function syncPosition( object: L2Object, player: L2PcInstance, commandChunks: Array<string> ) : void {
    if ( object && !object.isCharacter() ) {
        return player.sendMessage( 'Non-character target. Only characters can synchronize geo coordinates' )
    }

    let [ , heightValue ] = commandChunks

    let target = object ? object as L2Character : player
    let height = GeoPolygonCache.getObjectZ( target ) + _.defaultTo( parseInt( heightValue, 10 ), 0 )

    target.setZ( height )

    player.sendMessage( `"${target.getName()}" synchronized geo position (x = ${target.getX()}, y = ${target.getY()}, z = ${target.getZ()})` )
    player.sendOwnedData( ValidateLocation( target ) )
}

function showRegion( player: L2PcInstance ) : void {
    let regions = L2World.getRegionsWithRadius( player.getX(), player.getY(), 10 )
    if ( regions.length === 0 ) {
        player.sendMessage( `Unable to determine world region for (x = ${player.getX()}, y = ${player.getY()})` )
    }

    regions.forEach( ( region: L2WorldRegion ) => {
        player.sendMessage( `Region for tile (x,y) : ${region.tileX} ${region.tileY}` )
        player.sendMessage( `Region boundaries (minX, maxX, minY, maxY): ${ region.geometry.minX } ${ region.geometry.maxX } ${ region.geometry.minY } ${ region.geometry.maxY }` )
    } )
}

function onHeightMap( player: L2PcInstance ) : void {
    let startTime = Date.now()
    let geometry = PointGeometry.acquire( GeometryId.Geo256 )
    let gridGeometry = PointGeometry.acquire( GeometryId.GeoGrid256 )

    gridGeometry.refresh()

    let baseX = player.getX() + geometry.xOffset
    let baseY = player.getY() + geometry.yOffset

    while ( gridGeometry.size() > 0 ) {
        gridGeometry.prepareNextPoint()

        let x = baseX + gridGeometry.getX()
        let y = baseY + gridGeometry.getY()

        sendGeoHeightGridItem( player, gridGeometry.size().toString(), geometry, x, y, player.getZ() )
    }

    player.sendMessage( `Computed height map in ${Date.now() - startTime} ms` )
    geometry.release()
    gridGeometry.release()
}

function sendGeoHeightGridItem( player : L2PcInstance, name: string, geometry: PointGeometry, positionX: number, positionY: number, positionZ: number ) : void {
    let packet = DrawHelper.getDrawPacket( player, name )

    geometry.refresh()

    while ( geometry.size() > 0 ) {
        geometry.prepareNextPoint()

        let x = positionX + geometry.getX()
        let y = positionY + geometry.getY()
        let z = GeoPolygonCache.getZ( x, y, positionZ )

        packet.addPoint( '', GeoHeightColor.Normal, true, x, y, z )
    }

    player.sendOwnedData( packet.getBuffer() )
}

function onMoveInfoClicked( player: L2PcInstance, targetX : number, targetY : number, targetZ : number ) : void {
    PlayerMovementActionCache.clearAction( player.getObjectId() )
    let z = GeoPolygonCache.getZ( targetX, targetY, targetZ )
    player.sendMessage( `Position(x,y,z) : ${targetX}, ${targetY}, ${targetZ} has geo Z=${z}` )
}