import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { EventType, GMChangeAccessLevelEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { DatabaseManager } from '../../../database/manager'
import { L2GameClientRegistry } from '../../L2GameClientRegistry'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import _ from 'lodash'
import { PlayerAccess } from '../../models/PlayerAccess'
import { PlayerAccessCache } from '../../cache/PlayerAccessCache'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'

const enum Commands {
    ChangeAccess = 'player-access-change',
    Menu = 'player-access-menu',
}

export const PlayerAccessLevels: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            Commands.ChangeAccess,
            Commands.Menu,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case Commands.ChangeAccess:
                return [
                    formatOptionsDescription( Commands.ChangeAccess, '< level >', 'for selected target' ),
                    formatOptionsDescription( Commands.ChangeAccess, '< level > < player name >', 'for specified player name' ),
                    'Changes access level of targeted player',
                ]

            case Commands.Menu:
                return [
                    formatDescription( Commands.Menu, 'View available player access commands and data' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case Commands.ChangeAccess:
                return onChangeAccess( commandChunks, player )

            case Commands.Menu:
                return onMenu( player )
        }
    },
}

async function onChangeAccess( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let [ unused, parameterOne, parameterTwo ] = commandChunks

    if ( !parameterTwo ) {
        if ( !parameterOne ) {
            return
        }

        let level = parseInt( parameterOne )
        let target = player.getTarget()
        if ( !target || !target.isPlayer() ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        }

        return changeAccess( player, target as L2PcInstance, level )
    }

    if ( !parameterOne ) {
        return
    }

    let level = parseInt( parameterTwo )
    let otherPlayer = L2World.getPlayerByName( parameterOne )
    if ( otherPlayer ) {
        return changeAccess( player, otherPlayer, level )
    }

    let objectId = await CharacterNamesCache.getIdByName( parameterOne )
    if ( !objectId ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
    }

    return changeAccessOffline( player, objectId, level )
}

async function changeAccess( player: L2PcInstance, target: L2PcInstance, level: number ): Promise<void> {
    if ( _.isUndefined( level ) ) {
        player.sendMessage( 'Incorrect access level. Specify numeric level.' )
        return
    }

    if ( level < 0 ) {
        target.setAccessLevel( level, 'Admin command' )
        target.sendMessage( 'Your character has been banned.' )
        await sendChangeAccessEvent( player.getObjectId(), target.getObjectId(), level )

        let client = L2GameClientRegistry.getClientByPlayerId( target.getObjectId() )
        if ( client ) {
            return client.logout()
        }

        return target.unload()
    }

    let access: PlayerAccess = PlayerAccessCache.getLevel( level )
    if ( !access ) {
        return player.sendMessage( `You are trying to set incorrect access level '${ level }' , please try again with a valid one!` )
    }

    target.setAccessLevel( level , 'Admin command' )

    await DatabaseManager.getCharacterTable().setAccessLevel( target.getObjectId(), level )
    target.sendMessage( `Current character access level has been changed to '${ access.name }' (${ access.level })` )

    player.sendMessage( `'${ target.getName() }' access level has been changed to '${ access.name }' (${ access.level })` )
    return sendChangeAccessEvent( player.getObjectId(), target.getObjectId(), level )
}

async function changeAccessOffline( player: L2PcInstance, objectId: number, level: number ): Promise<void> {
    if ( level >= 0 && !PlayerAccessCache.getLevel( level ) ) {
        return player.sendMessage( `You are trying to set incorrect access level '${ level }' , please try again with a valid one!` )
    }

    await DatabaseManager.getCharacterTable().setAccessLevel( objectId, level )
    return sendChangeAccessEvent( player.getObjectId(), objectId, level )
}

function sendChangeAccessEvent( originatorId: number, targetId: number, level: number ) : Promise<void> {
    if ( ListenerCache.hasGeneralListener( EventType.GMChangeAccessLevel ) ) {
        let data: GMChangeAccessLevelEvent = {
            level,
            originatorId,
            targetId,
        }

        return ListenerCache.sendGeneralEvent( EventType.GMChangeAccessLevel, data )
    }
}

function onMenu( player: L2PcInstance ) : void {
    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/playerAccess/menu-item.htm' )

    let items = PlayerAccessCache.getAllLevels().map( ( access: PlayerAccess ) : string => {
        return itemHtml
            .replace( '%name%', access.name )
            .replace( '%level%', access.level.toString() )
    } )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/playerAccess/menu.htm' )
        .replace( '%items%', items.join( '' ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}