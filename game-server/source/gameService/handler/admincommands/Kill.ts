import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2World } from '../../L2World'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2Character } from '../../models/actor/L2Character'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2Object } from '../../models/L2Object'
import { EventType, GMKillTargetsEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { InstanceType } from '../../enums/InstanceType'
import _ from 'lodash'
import aigle from 'aigle'
import { formatOptionsDescription } from './helpers/FormatHelpers'

const enum KillCommands {
    KillOne = 'kill',
    KillInRadius = 'kill-radius',
    KillByObjectId = 'kill-objectId'
}

export const Kill: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            KillCommands.KillOne,
            KillCommands.KillInRadius,
            KillCommands.KillByObjectId,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case KillCommands.KillOne:
                return [
                    formatOptionsDescription( KillCommands.KillOne, '[player name]', 'kills targeted character or player with specified name' ),
                ]

            case KillCommands.KillByObjectId:
                return [
                    formatOptionsDescription( KillCommands.KillByObjectId, '< objectId >', 'Kills character specified by objectId (can be npc or player)' ),
                ]

            case KillCommands.KillInRadius:
                return [
                    formatOptionsDescription( KillCommands.KillInRadius, '[radius = 500]', 'Kills all mobs/players within specified radius' ),
                    'If radius is not specified, default of 500 is used.',
                ]
        }
    },

    onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case KillCommands.KillOne:
                return onAdminKill( commandChunks, player )

            case KillCommands.KillInRadius:
                return onAdminRadiusKill( commandChunks, player )

            case KillCommands.KillByObjectId:
                return onAdminKillByObjectId( commandChunks, player )
        }
    },
}

/*
    Two possible outcomes of killing a mob:
    - mob simply dies
    - mob dies and drops loot

    In certain instances loot may be un-desirable due to other players may benefit from GM actions, hence by default
    attacker should not be specified (which causes drop loot calculations).
 */
async function killTarget( target: L2Character, attackerPlayer: L2PcInstance = null ): Promise<void> {
    if ( target.isPlayer() ) {
        if ( !target.isGM() ) {
            await target.stopAllEffects()
        }

        return target.reduceCurrentHp( target.getMaxHp() + target.getMaxCp() + 1, null, null )
    }

    if ( ConfigManager.customs.championEnable() && target.isChampion() ) {
        return target.reduceCurrentHp( ( target.getMaxHp() * ConfigManager.customs.getChampionHp() ) + 1, null, null )
    }

    let hasInvulnerability = target.isInvulnerable()
    if ( hasInvulnerability ) {
        target.setIsInvulnerable( false )
    }

    await target.reduceCurrentHp( target.getMaxHp() + 1, attackerPlayer, null )

    if ( hasInvulnerability ) {
        target.setIsInvulnerable( true )
    }
}

function canKillTarget( target: L2Object ): boolean {
    return target && !target.isInstanceType( InstanceType.L2ControllableMobInstance ) && ( target.isAttackable() || target.isPlayable() )
}

function sendKilledEvent( targetIds: Array<number>, player: L2PcInstance ): void {
    if ( ListenerCache.hasGeneralListener( EventType.GMKillTargets ) ) {
        let data: GMKillTargetsEvent = {
            originatorId: player.getObjectId(),
            targetIds,
        }

        ListenerCache.sendGeneralEvent( EventType.GMKillTargets, data )
    }
}

async function onAdminKill( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ currentCommand, playerName ] = commandChunks

    return attemptToKillTarget( playerName ? L2World.getPlayerByName( playerName ) : player.getTarget(), player, player )
}

async function onAdminRadiusKill( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ currentCommand, radiusValue ] = commandChunks
    let radius = radiusValue ? _.parseInt( radiusValue ) : 500

    if ( radius > 20000 ) {
        return
    }

    let targetIds: Array<number> = []
    await aigle.resolve( L2World.getVisibleObjectsByPredicate( player, radius, ( target: L2Object ): boolean => {
        return Math.abs( player.getZ() - target.getZ() ) < 250 && canKillTarget( target )
    } ) ).eachLimit( 10, ( object: L2Object ): Promise<void> => {
        targetIds.push( object.getObjectId() )
        return killTarget( object as L2Character )
    } )

    if ( targetIds.length > 0 ) {
        sendKilledEvent( targetIds, player )
    }

    player.sendMessage( `Killed ${ targetIds.length } characters in ${ radius } radius` )
}

async function onAdminKillByObjectId( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ currentCommand, objectIdValue ] = commandChunks

    return attemptToKillTarget( L2World.getObjectById( _.parseInt( objectIdValue ) ), player )
}

async function attemptToKillTarget( target: L2Object, player: L2PcInstance, attacker: L2PcInstance = null ): Promise<void> {
    if ( !target ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
    }

    if ( !canKillTarget( target ) ) {
        return player.sendMessage( `Supplied target of type '${ InstanceType[ target.getInstanceType() ] }' cannot be killed.` )
    }

    await killTarget( target as L2Character, attacker )
    return sendKilledEvent( [ target.getObjectId() ], player )
}