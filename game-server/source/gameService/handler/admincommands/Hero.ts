import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { HeroCache } from '../../cache/HeroCache'
import { formatDescription } from './helpers/FormatHelpers'

const enum HeroCommands {
    ToggleHeroStatus = 'hero',
    SetAsHero = 'sethero'
}

export const HeroManagement: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            HeroCommands.SetAsHero,
            HeroCommands.ToggleHeroStatus,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case HeroCommands.SetAsHero:
                return [
                    formatDescription( HeroCommands.SetAsHero, 'sets claimed status of hero for targeted player' ),
                ]

            case HeroCommands.ToggleHeroStatus:
                return [
                    formatDescription( HeroCommands.ToggleHeroStatus, 'toggles hero status for targeted player' ),
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let currentCommand: string = command.split( ' ' )[ 0 ]

        switch ( currentCommand ) {
            case HeroCommands.SetAsHero:
                return onSetHeroBy( player )

            case HeroCommands.ToggleHeroStatus:
                return onToggleHeroBy( player )
        }
    },
}

async function onToggleHeroBy( player: L2PcInstance ): Promise<void> {
    let target = player.getTarget()
    if ( !target || !target.getActingPlayer() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    let targetPlayer: L2PcInstance = target.getActingPlayer()
    await targetPlayer.setHero( !targetPlayer.isHero() )
    targetPlayer.broadcastUserInfo()

    let action: string = targetPlayer.isHero() ? 'added' : 'removed'
    player.sendMessage( `Player '${ targetPlayer.getName() }' has hero status ${ action }` )
    return
}

async function onSetHeroBy( player: L2PcInstance ): Promise<void> {
    let target = player.getTarget()
    if ( !target || !target.getActingPlayer() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return
    }

    let targetPlayer: L2PcInstance = target.getActingPlayer()
    if ( HeroCache.isHero( targetPlayer.getObjectId() ) ) {
        player.sendMessage( `Player '${ targetPlayer.getName() }' has already claimed the hero status.` )
        return
    }

    if ( !HeroCache.isUnclaimedHero( targetPlayer.getObjectId() ) ) {
        player.sendMessage( `Player '${ targetPlayer.getName() }' cannot claim the hero status.` )
        return
    }

    await HeroCache.claimHero( targetPlayer )
    player.sendMessage( `Player '${ targetPlayer.getName() }' has claimed hero status.` )
    return
}