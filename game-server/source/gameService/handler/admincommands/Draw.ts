import { IAdminCommand } from '../IAdminCommand'
import { formatDescription } from './helpers/FormatHelpers'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ProximalDiscoveryManager } from '../../cache/ProximalDiscoveryManager'
import { DrawHelper } from './helpers/DrawHelper'

const enum DrawCommands {
    Clear = 'draw-clear',
    VisibleBoundary = 'draw-boundary'
}

const colorNew = 0x3DFF1C
const colorStale = 0x41FFD0

export const Draw : IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
                DrawCommands.VisibleBoundary,
                DrawCommands.Clear
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case DrawCommands.VisibleBoundary:
                return [
                    formatDescription( DrawCommands.VisibleBoundary, 'shows boundaries of player visible objects' ),
                    'Find New - boundary signifies region where visible objects can be viewed by player, beyond such region no new objects can be discovered',
                    'Clear Stale - boundary to clear any objects that should be deleted from view'
                ]

            case DrawCommands.Clear:
                return [
                    formatDescription( DrawCommands.Clear, 'remove drawn boundaries.' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        switch ( command ) {
            case DrawCommands.VisibleBoundary:
                return onShowVisibleBoundary( player )

            case DrawCommands.Clear:
                return DrawHelper.clearPackets( player.getObjectId() )
        }
    }
}

function onShowVisibleBoundary( player: L2PcInstance ) : void {
    let packet = DrawHelper.getDrawPacket( player )
    let zValue = player.getZ() + player.getCollisionHeight()

    packet.addSquare( 'Find New', colorNew, true, player.getX(), player.getY(), zValue, ProximalDiscoveryManager.getDistanceToWatchObject() )
    packet.addSquare( 'Clear Stale', colorStale, true, player.getX(), player.getY(), zValue, ProximalDiscoveryManager.getDistanceToForgetObject() )

    player.sendOwnedData( packet.getBuffer() )
}