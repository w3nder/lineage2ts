import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { AreaCache } from '../../cache/AreaCache'
import { AreaDiscoveryManager } from '../../cache/AreaDiscoveryManager'
import { DataManager } from '../../../data/manager'
import { L2World } from '../../L2World'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { L2WorldArea } from '../../models/areas/WorldArea'
import { L2AreaItemPropertyValue } from '../../../data/interface/AreaDataApi'
import _ from 'lodash'
import { DrawHelper } from './helpers/DrawHelper'
import { WorldAreaActions } from '../../models/areas/WorldAreaActions'
import { GeoPolygonCache } from '../../cache/GeoPolygonCache'

const enum Commands {
    ShowEnteredAreas = 'areas',
    AreasInRadius = 'areas-radius',
    ViewArea = 'area-view',
    ViewAreaShape = 'area-view-shape',
    Teleport = 'area-teleport',
    SimulateEnter = 'area-enter',
    SimulateExit = 'area-exit',
    SimulateActivation = 'area-activation',
    SimulateDeactivation = 'area-deactivation',
}

function createViewAreaBypass( areaId: number ) : string {
    return `bypass -h admin_${Commands.ViewArea} ${ areaId }`
}

function createAreaBypass( command: Commands, areaId: number ) : string {
    return `bypass admin_${command} ${ areaId }`
}

export const AreaCommands : IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            Commands.ShowEnteredAreas,
            Commands.AreasInRadius,
            Commands.ViewArea,
            Commands.ViewAreaShape,
            Commands.Teleport,
            Commands.SimulateEnter,
            Commands.SimulateExit,
            Commands.SimulateActivation,
            Commands.SimulateDeactivation,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case Commands.ShowEnteredAreas:
                return [
                    formatDescription( Commands.ShowEnteredAreas, 'Show entered areas for player position' )
                ]

            case Commands.AreasInRadius:
                return [
                    formatOptionsDescription( Commands.AreasInRadius, '[ radius = 500 ]', 'Shows available areas in player radius' ),
                    'Radius defaults to 500 if not specified.',
                    'Maximum radius value is 10000'
                ]

            case Commands.ViewArea:
                return [
                    formatOptionsDescription( Commands.ViewArea, '<areaId>', 'View area information using numeric id' )
                ]

            case Commands.ViewAreaShape:
                return [
                    formatOptionsDescription( Commands.ViewAreaShape, '<areaId>', 'Draw area shape specified by numeric id' )
                ]

            case Commands.Teleport:
                return [
                    formatOptionsDescription( Commands.Teleport, '<areaId>', 'Teleport to one of the points defined for area shape, area defined by numeric id' )
                ]

            case Commands.SimulateEnter:
                return [
                    formatOptionsDescription( Commands.SimulateEnter, '<areaId>', 'Simulate area enter, area specified by numeric id' ),
                    'Normally used for area testing'
                ]

            case Commands.SimulateExit:
                return [
                    formatOptionsDescription( Commands.SimulateExit, '<areaId>', 'Simulate area exit, area specified by numeric id' ),
                    'Normally used for area testing'
                ]

            case Commands.SimulateActivation:
                return [
                    formatOptionsDescription( Commands.SimulateActivation, '<areaId>', 'Simulate activation of area, area specified by numeric id' ),
                    'Normally used for area testing'
                ]

            case Commands.SimulateDeactivation:
                return [
                    formatOptionsDescription( Commands.SimulateDeactivation, '<areaId>', 'Simulate de-activation of area, area specified by numeric id' ),
                    'Normally used for area testing'
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        const commandChunks = command.split( ' ' )
        switch ( commandChunks[ 0 ] ) {
            case Commands.ShowEnteredAreas:
                return onShowAreas( player )

            case Commands.AreasInRadius:
                return onAreasInRadius( commandChunks, player )

            case Commands.ViewArea:
                return onViewArea( commandChunks, player )

            case Commands.ViewAreaShape:
                return onViewAreaShape( commandChunks, player )

            case Commands.Teleport:
                return onTeleportToArea( commandChunks, player )

            case Commands.SimulateEnter:
                return onSimulateAreaAction( commandChunks, player, WorldAreaActions.Enter )

            case Commands.SimulateExit:
                return onSimulateAreaAction( commandChunks, player, WorldAreaActions.Exit )

            case Commands.SimulateActivation:
                return onSimulateAreaAction( commandChunks, player, WorldAreaActions.Activation )

            case Commands.SimulateDeactivation:
                return onSimulateAreaAction( commandChunks, player, WorldAreaActions.Deactivation )
        }
    }
}

function onShowAreas( player: L2PcInstance ) : void {
    let areaItem = DataManager.getHtmlData().getItem( 'overrides/html/command/areas/showAreas-item.htm' )
    let items : Array<string> = []

    for ( const areaId of AreaDiscoveryManager.getEnteredAreaIds( player.getObjectId() ) ) {
        let area = AreaCache.getAreaById( areaId )
        items.push(
            areaItem
                .replace( '%id%', area.data.id )
                .replace( '%type%', area.data.type )
                .replace( '%dynamicId%', area.id.toString() )
                .replace( '%viewAction%', createViewAreaBypass( areaId ) )
                .replace( '%shapeAction%', createAreaBypass( Commands.ViewAreaShape, areaId ) )
                .replace( '%teleportAction%', createAreaBypass( Commands.Teleport, areaId ) )
        )
    }

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/areas/showAreas.htm' )
        .replace( '%title%', `Entered areas - ${player.getX()}, ${player.getY()}, ${player.getZ()}` )
        .replace( '%items%', items.join( '' ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onAreasInRadius( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let radius = commandChunks[ 1 ] ? parseInt( commandChunks[ 1 ], 10 ) : 500
    if ( !Number.isInteger( radius ) || radius < 1 ) {
        return player.sendMessage( 'Please specify valid and positive numeric value for radius' )
    }

    let areaItem = DataManager.getHtmlData().getItem( 'overrides/html/command/areas/showAreas-item.htm' )
    let items = L2World.getAreasWithCoordinates( player.getX(), player.getY(), Math.min( radius, 10000 ) ).map( ( area: L2WorldArea ) : string => {
        return areaItem
            .replace( '%id%', area.data.id )
            .replace( '%type%', area.data.type )
            .replace( '%dynamicId%', area.id.toString() )
            .replace( '%viewAction%', createViewAreaBypass( area.id ) )
            .replace( '%shapeAction%', createAreaBypass( Commands.ViewAreaShape, area.id ) )
            .replace( '%teleportAction%', createAreaBypass( Commands.Teleport, area.id ) )
    } )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/areas/showAreas.htm' )
        .replace( '%title%', `Areas in radius=${radius} - ${player.getX()}, ${player.getY()}, ${player.getZ()}` )
        .replace( '%items%', items.join( '' ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onViewArea( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let areaId = parseInt( commandChunks[ 1 ], 10 )
    if ( !Number.isInteger( areaId ) || areaId < 1 ) {
        return player.sendMessage( 'Please specify valid and positive numeric value for area id' )
    }

    let area = AreaCache.getAreaById( areaId )
    if ( !area ) {
        return player.sendMessage( `No area exists with id=${areaId}` )
    }

    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/areas/viewArea-item.htm' )
    let items : Array<string> = _.reduce( area.data.properties, ( allItems: Array<string>, value : L2AreaItemPropertyValue, key: string ) : Array<string> => {

        /*
            Area properties such as forts and other residences can have teleport points, hence any object type of value
            would require either too many entries or particular way to represent path and value, complicating handing.
         */
        if ( !Array.isArray( value ) && !_.isObject( value ) ) {
            allItems.push(
                itemHtml
                    .replace( '%name%', key )
                    .replace( '%value%', `${value}` )
            )
        }

        return allItems
    }, [] )

    let actions: Array<string> = []

    area.actions.forEach( ( type: WorldAreaActions ) => {
        actions.push( WorldAreaActions[ type ] )
    } )

    let buttonsHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/areas/viewArea-buttons.htm' )
        .replace( '%enter%', createAreaBypass( Commands.SimulateEnter, area.id ) )
        .replace( '%exit%', createAreaBypass( Commands.SimulateExit, area.id ) )
        .replace( '%activate%', createAreaBypass( Commands.SimulateActivation, area.id ) )
        .replace( '%deactivate%', createAreaBypass( Commands.SimulateDeactivation, area.id ) )
        .replace( '%shapeAction%', createAreaBypass( Commands.ViewAreaShape, area.id ) )
        .replace( '%teleportAction%', createAreaBypass( Commands.Teleport, area.id ) )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/areas/viewArea.htm' )
        .replaceAll( '%id%', area.data.id )
        .replace( '%type%', area.data.type )
        .replace( '%points%', ( area.form.polygon.length / 2 ).toString() )
        .replace( '%minZ%', area.form.minimumZ.toString() )
        .replace( '%maxZ%', area.form.maximumZ.toString() )
        .replace( '%items%', items.join( '' ) )
        .replace( '%actions%', actions.join( ',' ) )
        .replace( '%buttons%', buttonsHtml )
        .replace( '%affectYou%', AreaDiscoveryManager.getEnteredAreaIds( player.getObjectId() ).has( area.id ) ? 'Yes' : 'No' )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onViewAreaShape( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let areaId = parseInt( commandChunks[ 1 ], 10 )
    if ( !Number.isInteger( areaId ) || areaId < 1 ) {
        return player.sendMessage( 'Please specify valid and positive numeric value for area id' )
    }

    let area = AreaCache.getAreaById( areaId )
    if ( !area ) {
        return player.sendMessage( `No area exists with id=${areaId}` )
    }

    let packet = DrawHelper.getDrawPacket( player )
    let zValue = player.getZ() + player.getCollisionHeight()

    /*
        There are always more than two points available, meaning minimum polygon value size is 6.
     */
    let limit = area.form.polygon.length - 4
    for ( let index = 0; index < limit; index = index + 2 ) {
        packet.addLine( area.data.id, 0x3DFF1C, true, area.form.polygon[ index ], area.form.polygon[ index + 1 ], zValue, area.form.polygon[ index + 2 ], area.form.polygon[ index + 3 ], zValue )
    }

    packet.addLine( area.data.id, 0x3DFF1C, true, area.form.polygon.at( -2 ), area.form.polygon.at( -1 ), zValue, area.form.polygon[ 0 ], area.form.polygon[ 1 ], zValue )

    player.sendOwnedData( packet.getBuffer() )
}

async function onTeleportToArea( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let areaId = parseInt( commandChunks[ 1 ], 10 )
    if ( !Number.isInteger( areaId ) || areaId < 1 ) {
        return player.sendMessage( 'Please specify valid and positive numeric value for area id' )
    }

    let area = AreaCache.getAreaById( areaId )
    if ( !area ) {
        return player.sendMessage( `No area exists with id=${areaId}` )
    }

    let index = Math.floor( Math.random() * ( area.form.polygon.length / 2 ) ) * 2
    let areaZ = GeoPolygonCache.getZ( area.form.polygon[ index ], area.form.polygon[ index + 1 ], area.form.maximumZ )

    return player.teleportToLocationCoordinates( area.form.polygon[ index ], area.form.polygon[ index + 1 ], areaZ )
}

function onSimulateAreaAction( commandChunks: Array<string>, player: L2PcInstance, action: WorldAreaActions ) : void {
    let areaId = parseInt( commandChunks[ 1 ], 10 )
    if ( !Number.isInteger( areaId ) || areaId < 1 ) {
        return player.sendMessage( 'Please specify valid and positive numeric value for area id' )
    }

    let area = AreaCache.getAreaById( areaId )
    if ( !area ) {
        return player.sendMessage( `No area exists with id=${areaId}` )
    }

    if ( !area.supportsAction( action ) ) {
        return player.sendMessage( `Area does not support action type - ${WorldAreaActions[ action ]}` )
    }

    return area.processAction( player, action )
}