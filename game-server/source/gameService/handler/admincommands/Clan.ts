import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { DataManager } from '../../../data/manager'
import { ClanHallManager } from '../../instancemanager/ClanHallManager'
import { CastleManager } from '../../instancemanager/CastleManager'
import { FortManager } from '../../instancemanager/FortManager'
import { L2ClanMember } from '../../models/L2ClanMember'
import { EventType, GMClanChangeLeaderEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ClanCache } from '../../cache/ClanCache'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { L2World } from '../../L2World'
import _ from 'lodash'

const enum ClanCommands {
    Info = 'clan-info',
    ChangeLeader = 'clan-changeleader',
    ShowPending = 'clan-showpending',
    ConvertPending = 'clan-convertpending'
}

export const Clan: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            ClanCommands.Info,
            ClanCommands.ChangeLeader,
            ClanCommands.ShowPending,
            ClanCommands.ConvertPending,
        ]
    },

    getDescription( command: string ): Array<string> {
        return
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks: Array<string> = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case ClanCommands.Info:
                return onClanInfo( commandChunks, player )

            case ClanCommands.ChangeLeader:
                return onClanChangeLeader( commandChunks, player )

            case ClanCommands.ShowPending:
                return onShowPending( commandChunks, player )

            case ClanCommands.ConvertPending:
                return onConvertPending( commandChunks, player )
        }
    },
}

function onClanInfo( commandChunks: Array<string>, player: L2PcInstance ): void {
    let otherPlayer: L2PcInstance = getPlayer( commandChunks, player )
    if ( !otherPlayer ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
        return
    }

    let clan: L2Clan = otherPlayer.getClan()
    if ( !clan ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_MUST_BE_IN_CLAN ) )
        return
    }

    let htmlPath = 'overrides/html/command/clan/info.htm'
    let html: string = DataManager.getHtmlData().getItem( htmlPath )
                                  .replace( /%clanName%/g, clan.getName() )
                                  .replace( /%clanLeader%/g, clan.getLeaderName() )
                                  .replace( /%clanLevel%/g, clan.getLevel().toString() )
                                  .replace( /%hasCastle%/g, clan.getCastleId() > 0 ? CastleManager.getCastleById( clan.getCastleId() ).getName() : 'No' )

                                  .replace( /%hasClanHall%/g, clan.getHideoutId() > 0 ? ClanHallManager.getClanHallById( clan.getHideoutId() ).getName() : 'No' )
                                  .replace( /%hasFortress%/g, clan.getFortId() > 0 ? FortManager.getFortById( clan.getFortId() ).getName() : 'No' )
                                  .replace( /%reputationPoints%/g, clan.getReputationScore().toString() )
                                  .replace( /%playerAmount%/g, clan.getMembersCount().toString() )

                                  .replace( /%allyName%/g, clan.getAllyId() > 0 ? clan.getAllyName() : 'No ally' )
                                  .replace( /%playerId%/g, player.getObjectId().toString() )
                                  .replace( /%playerName%/g, player.getName() )

    player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), 0, 0 ) )
}

async function onClanChangeLeader( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let otherPlayer: L2PcInstance = getPlayer( commandChunks, player )
    if ( !otherPlayer ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
        return
    }

    let clan: L2Clan = otherPlayer.getClan()
    if ( !clan ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_MUST_BE_IN_CLAN ) )
        return
    }

    let member: L2ClanMember = clan.getClanMember( otherPlayer.getObjectId() )
    if ( member ) {
        if ( player.isAcademyMember() ) {
            return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.RIGHT_CANT_TRANSFERRED_TO_ACADEMY_MEMBER ) )
        }

        await clan.setNewLeader( member )
    }

    if ( ListenerCache.hasGeneralListener( EventType.GMClanChangeLeader ) ) {
        let data: GMClanChangeLeaderEvent = {
            clanId: clan.getId(),
            leaderId: otherPlayer.getObjectId(),
            originatorId: player.getObjectId(),
        }

        return ListenerCache.sendGeneralEvent( EventType.GMClanChangeLeader, data )
    }
}

async function onShowPending( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let leaderIds: Array<number> = _.reduce( ClanCache.getClans(), ( ids: Array<number>, clan: L2Clan ): Array<number> => {
        if ( clan.getNewLeaderId() > 0 ) {
            ids.push( clan.getNewLeaderId() )
        }

        return ids
    }, [] )

    await CharacterNamesCache.prefetchNames( leaderIds )
    let lines: Array<string> = _.map( ClanCache.getClans(), ( clan: L2Clan ): string => {
        let newLeaderName: string = CharacterNamesCache.getCachedNameById( clan.getNewLeaderId() )
        return `<tr><td>${ clan.getName() }</td><td>${ newLeaderName }</td><td><a action="bypass -h admin_${ ClanCommands.ConvertPending } ${ clan.getId() }">Force</a></td></tr>`
    } )

    let path = 'overrides/html/command/clan/changes.htm'
    let html: string = DataManager.getHtmlData().getItem( path )
                                  .replace( '%data%', lines.join( '' ) )
    player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), 0, 1 ) )
}

async function onConvertPending( commandChunks: Array<string>, player: L2PcInstance ): Promise<void> {
    let [ currentCommand, clanIdValue ] = commandChunks

    if ( !clanIdValue ) {
        return
    }

    let clan: L2Clan = ClanCache.getClan( clanIdValue )
    if ( !clan ) {
        return
    }

    let member: L2ClanMember = clan.getClanMember( clan.getNewLeaderId() )
    if ( !member ) {
        return
    }

    await clan.setNewLeader( member )

    if ( ListenerCache.hasGeneralListener( EventType.GMClanChangeLeader ) ) {
        let data: GMClanChangeLeaderEvent = {
            clanId: clan.getId(),
            leaderId: clan.getNewLeaderId(),
            originatorId: player.getObjectId(),
        }

        return ListenerCache.sendGeneralEvent( EventType.GMClanChangeLeader, data )
    }
}

function getPlayer( commandChunks: Array<string>, player: L2PcInstance ): L2PcInstance {
    let [ currentCommand, playerNameValue ] = commandChunks

    if ( playerNameValue ) {
        return L2World.getPlayerByName( playerNameValue )
    }

    let target = player.getTarget()
    if ( !target || !target.isPlayer() ) {
        return null
    }

    return target as L2PcInstance
}