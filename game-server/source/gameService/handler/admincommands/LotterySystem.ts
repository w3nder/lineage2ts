import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import _ from 'lodash'
import { LotteryManager } from '../../cache/LotteryManager'
import { ConfigManager } from '../../../config/ConfigManager'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'

const enum LotteryCommands {
    Status = 'lottery-status',
    Reset = 'lottery-reset',
    Stop = 'lottery-stop',
    Start = 'lottery-start'
}

export const LotterySystem: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            LotteryCommands.Status,
            LotteryCommands.Reset,
            LotteryCommands.Stop,
            LotteryCommands.Start,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case LotteryCommands.Status:
                return [
                    formatDescription( LotteryCommands.Status, 'Display current parameters of lottery system.' ),
                ]

            case LotteryCommands.Reset:
                return [
                    formatOptionsDescription( LotteryCommands.Reset, '[ jackpotAmount = 0 ] [ minutesBeforeStart = 0 ]', 'Restart lottery system' ),
                    'Specify jackpot amount, amount defaults to zero if omitted',
                    'Supply amount of minutes you wish to wait before lottery starts, minutes default to zero if omitted',
                ]

            case LotteryCommands.Stop:
                return [
                    formatOptionsDescription( LotteryCommands.Stop, '[ minutesBeforeStop = 0 ]', 'Stop lottery system' ),
                    'Aborts current lottery submissions and puts lottery into waiting state.',
                    'Supply amount of minutes you wish to wait before lottery stops, minutes default to zero if omitted',
                ]

            case LotteryCommands.Start:
                return [
                    formatOptionsDescription( LotteryCommands.Start, '[ jackpotAmount = 0 ] [ minutesBeforeStart = 0 ]', 'Start lottery system' ),
                    'Starts lottery system if it has been stopped, amount defaults to zero if omitted',
                    'Supply amount of minutes you wish to wait before lottery starts, minutes default to zero if omitted',
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        if ( !ConfigManager.general.allowLottery() ) {
            return player.sendMessage( 'Lottery system is disabled by configuration.' )
        }

        let commandChunks: Array<string> = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case LotteryCommands.Status:
                return onLotteryStatus( player )

            case LotteryCommands.Reset:
                return onLotteryReset( commandChunks, player )

            case LotteryCommands.Stop:
                return onLotteryStop( commandChunks, player )

            case LotteryCommands.Start:
                return onLotteryStart( commandChunks, player )
        }
    },

}

async function onLotteryStart( parameters: Array<string>, player: L2PcInstance ): Promise<void> {
    if ( LotteryManager.isStarted ) {
        player.sendMessage( 'Lottery system is already started.' )
        return
    }

    let [ unused, jackpotValue, minutesValue ] = parameters

    let jackpotAmount = _.defaultTo( _.parseInt( jackpotValue ), 0 )
    let minutesAmount = _.defaultTo( _.parseInt( minutesValue ), 0 )

    await LotteryManager.scheduleLotteryStart( minutesAmount, jackpotAmount )
    player.sendMessage( `Lottery system is starting in ${ minutesAmount } minutes.` )
}

async function onLotteryStop( parameters: Array<string>, player: L2PcInstance ): Promise<void> {
    if ( !LotteryManager.isStarted ) {
        player.sendMessage( 'Lottery system is already stopped.' )
        return
    }

    let [ unused, minutesValue ] = parameters

    let minutesAmount = _.defaultTo( _.parseInt( minutesValue ), 0 )
    LotteryManager.scheduleLotteryStop( minutesAmount )

    player.sendMessage( `Lottery system is stopping in ${ minutesAmount } minutes.` )
}

async function onLotteryReset( parameters: Array<string>, player: L2PcInstance ): Promise<void> {
    if ( LotteryManager.isStarted ) {
        await LotteryManager.finishLottery()
    }

    return onLotteryStart( parameters, player )
}

// TODO : starting/finishing date needs to be adjusted for admin command manipulations
function onLotteryStatus( player: L2PcInstance ): void {
    if ( LotteryManager.isStarted ) {
        return player.sendMessage( `Lottery system is started with jackpot at ${ LotteryManager.adenaPrize } and finishing in ${ GeneralHelper.formatSeconds( LotteryManager.getNextDate() / 1000 ).join( ',' ) }` )
    }

    return player.sendMessage( `Lottery system is stopped with last jackpot at ${ LotteryManager.adenaPrize } and starting in ${ GeneralHelper.formatSeconds( LotteryManager.getNextDate() / 1000 ).join( ',' ) }` )
}