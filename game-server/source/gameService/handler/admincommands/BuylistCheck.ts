import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2BuyList } from '../../models/buylist/L2BuyList'
import { BuyListManager } from '../../cache/BuyListManager'
import { BuyList } from '../../packets/send/BuyList'
import { ExBuySellList } from '../../packets/send/ExBuySellList'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { AdminHtml } from './helpers/AdminHtml'

const enum BuyCommands {
    InvokeBylist = 'buylist',
    GmShop = 'gmshop'
}

export const BuylistCheck: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            BuyCommands.GmShop,
            BuyCommands.InvokeBylist,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case BuyCommands.InvokeBylist:
                return [
                    formatOptionsDescription( BuyCommands.InvokeBylist, '< buylistId >', 'opens shop with specified buylist id' ),
                ]

            case BuyCommands.GmShop:
                return [
                    formatDescription( BuyCommands.GmShop, 'shows GM shop menu' ),
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let [ currentCommand, idValue ] = command.split( ' ' )

        switch ( currentCommand ) {
            case BuyCommands.InvokeBylist:
                if ( !idValue ) {
                    return player.sendMessage( 'Specify buylist id' )
                }

                let id = parseInt( idValue )
                let buyList: L2BuyList = BuyListManager.getBuyList( id )

                if ( buyList ) {
                    player.sendOwnedData( BuyList( buyList, player.getAdena(), 0 ) )
                    player.sendOwnedData( ExBuySellList( player, false ) )
                    player.sendOwnedData( ActionFailed() )
                    return
                }

                return player.sendMessage( `No buylist found with id = ${ id }` )

            case BuyCommands.GmShop:
                return AdminHtml.showAdminHtml( player, 'gmshops.htm' )
        }
    },
}