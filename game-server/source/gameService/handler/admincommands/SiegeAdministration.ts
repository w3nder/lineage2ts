import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { SiegableHall } from '../../models/entity/clanhall/SiegableHall'
import { ClanHallSiegeManager } from '../../instancemanager/ClanHallSiegeManager'
import { ClanCache } from '../../cache/ClanCache'
import { L2Clan } from '../../models/L2Clan'
import moment, { Moment } from 'moment'
import {
    EventType,
    GMSiegeHallAddAttackerEvent,
    GMSiegeHallClearAttackersEvent,
    GMSiegeHallEndSiegeEvent,
    GMSiegeHallForwardSiegeEvent,
    GMSiegeHallRemoveAttackerEvent,
    GMSiegeHallSetDateEvent,
    GMSiegeHallStartSiegeEvent,
} from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { ClanHallSiegeEngine } from '../../models/entity/clanhall/ClanHallSiegeEngine'
import { SiegeStatus } from '../../enums/SiegeStatus'
import { SiegeInfoForHall } from '../../packets/send/SiegeInfo'
import _ from 'lodash'

const incomingDateFormat = 'DD-MM-YYYY HH:mm'
const showDateFormat = 'dddd, MMMM Do YYYY, h:mm a'

const enum SiegeCommands {
    StartSiege = 'siege-start',
    EndSiege = 'siege-stop',
    SetSiegeDate = 'siege-setdate',
    AddAttacker = 'siege-attacker-add',
    RemoveAttacker = 'siege-attacker-remove',
    RemoveAllAttackers = 'siege-remove-attackers',
    ShowAttackers = 'siege-show',
    ProgressSiege = 'siege-speedup'
}

export const SiegeAdministration: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            SiegeCommands.StartSiege,
            SiegeCommands.EndSiege,
            SiegeCommands.SetSiegeDate,
            SiegeCommands.AddAttacker,
            SiegeCommands.RemoveAttacker,
            SiegeCommands.RemoveAllAttackers,
            SiegeCommands.ShowAttackers,
            SiegeCommands.ProgressSiege,
        ]
    },

    getDescription( command: string ): Array<string> {
        return [
            'implement me',
        ]
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        if ( !ConfigManager.diagnostic.isLoadListeners() ) {
            player.sendMessage( 'Clan Hall Sieges are disabled' )
            return
        }

        let [ currentCommand, hallIdValue, parameterOne ] = _.split( command, ' ' )
        if ( !hallIdValue ) {
            player.sendMessage( 'Please specify hall id as part of command.' )
            return
        }

        let hall: SiegableHall = getSiegableHall( hallIdValue )
        if ( !hall ) {
            player.sendMessage( `Siegable hall for id '${ hallIdValue }' is not found` )
            return
        }

        if ( !hall.getSiege() ) {
            player.sendMessage( `Siegable hall for id '${ hallIdValue }' has no siege activity` )
            return
        }

        switch ( currentCommand ) {
            case SiegeCommands.StartSiege:
                await onStartSiege( hall, player )
                return

            case SiegeCommands.EndSiege:
                await onEndSiege( hall, player )
                return

            case SiegeCommands.SetSiegeDate:
                await onSetSiegeDate( hall, player, parameterOne )
                return

            case SiegeCommands.AddAttacker:
                onAddAttacker( hall, player, parameterOne )
                return

            case SiegeCommands.RemoveAttacker:
                onRemoveSingleAttacker( hall, player, parameterOne )
                return

            case SiegeCommands.RemoveAllAttackers:
                onRemoveAllAttackers( hall, player )
                return

            case SiegeCommands.ShowAttackers:
                return player.sendOwnedData( SiegeInfoForHall( hall, player ) )

            case SiegeCommands.ProgressSiege:
                await onForwardSiege( hall, player )
                return
        }
    },
}

function getSiegableHall( hallId: string ): SiegableHall {
    let id = _.parseInt( hallId )

    if ( id === 0 ) {
        return null
    }

    return ClanHallSiegeManager.getSiegableHall( id )
}

async function onStartSiege( hall: SiegableHall, player: L2PcInstance ): Promise<void> {
    if ( hall.isInSiege() ) {
        return player.sendMessage( `Siegable hall for id '${ hall.getId() }' is already in siege` )
    }

    let owner: L2Clan = ClanCache.getClan( hall.getOwnerId() )
    if ( owner ) {
        hall.free()
        owner.setHideoutId( 0 )
        hall.addAttacker( owner )
    }

    await hall.getSiege().startSiege()

    if ( ListenerCache.hasGeneralListener( EventType.GMSiegeHallStartSiege ) ) {
        let data: GMSiegeHallStartSiegeEvent = {
            hallId: hall.getId(),
            originatorId: player.getObjectId(),
        }

        return ListenerCache.sendGeneralEvent( EventType.GMSiegeHallStartSiege, data )
    }
}

async function onEndSiege( hall: SiegableHall, player: L2PcInstance ): Promise<void> {
    if ( !hall.isInSiege() ) {
        return player.sendMessage( `Siegable hall for id '${ hall.getId() }' is not in siege` )
    }

    await hall.getSiege().endSiege()

    if ( ListenerCache.hasGeneralListener( EventType.GMSiegeHallEndSiege ) ) {
        let data: GMSiegeHallEndSiegeEvent = {
            hallId: hall.getId(),
            originatorId: player.getObjectId(),
        }

        return ListenerCache.sendGeneralEvent( EventType.GMSiegeHallEndSiege, data )
    }
}

async function onSetSiegeDate( hall: SiegableHall, player: L2PcInstance, parameterOne: string ): Promise<void> {
    if ( !hall.isRegistering() ) {
        return player.sendMessage( 'Cannot change siege date while hall is in siege' )
    }

    if ( !parameterOne ) {
        return player.sendMessage( `Please specify siege date as ${ incomingDateFormat }` )
    }

    let futureDate: Moment = moment( parameterOne, incomingDateFormat )
    if ( !futureDate.isValid() ) {
        return player.sendMessage( `Please specify siege date as ${ incomingDateFormat }` )
    }

    if ( futureDate.isBefore( moment() ) ) {
        return player.sendMessage( 'Please specify siege date in future' )
    }

    player.sendMessage( `Siegable hall for id '${ hall.getId() }' has siege date on '${ futureDate.format( showDateFormat ) }'` )

    hall.setNextSiegeDate( futureDate.valueOf() )
    hall.getSiege().updateSiege()
    await ClanHallSiegeManager.scheduleHallUpdate( hall.getId(), true )

    if ( ListenerCache.hasGeneralListener( EventType.GMSiegeHallSetDate ) ) {
        let data: GMSiegeHallSetDateEvent = {
            hallId: hall.getId(),
            nextDate: futureDate.valueOf(),
            originatorId: player.getObjectId(),
        }

        return ListenerCache.sendGeneralEvent( EventType.GMSiegeHallSetDate, data )
    }
}

function onAddAttacker( hall: SiegableHall, player: L2PcInstance, parameterOne: string ): void {
    if ( hall.isInSiege() ) {
        player.sendMessage( 'Clan hall is in siege, cannot add attackers now' )
        return
    }

    let attackerClan: L2Clan
    if ( parameterOne ) {
        attackerClan = ClanCache.getClanByName( parameterOne )
        if ( !attackerClan ) {
            return player.sendMessage( 'Specified clan does not exist' )
        }

        if ( hall.getSiege().checkIsAttacker( attackerClan ) ) {
            return player.sendMessage( 'Clan is already participating as an attacker' )
        }
    } else {
        let target = player.getTarget()
        if ( !target ) {
            return player.sendMessage( 'You must target a clan member of the attacker' )
        }

        if ( !target.isPlayer() ) {
            return player.sendMessage( 'You must target a player' )
        }

        attackerClan = ( target as L2PcInstance ).getClan()
        if ( !attackerClan ) {
            return player.sendMessage( 'You must target a player with a clan' )
        }

        if ( hall.getSiege().checkIsAttacker( attackerClan ) ) {
            return player.sendMessage( 'Your target\'s clan is already participating as an attacker' )
        }
    }

    hall.addAttacker( attackerClan )

    if ( ListenerCache.hasGeneralListener( EventType.GMSiegeHallAddAttacker ) ) {
        let data: GMSiegeHallAddAttackerEvent = {
            attackerClanId: attackerClan.getId(),
            hallId: hall.getId(),
            originatorId: player.getObjectId(),
        }

        ListenerCache.sendGeneralEvent( EventType.GMSiegeHallAddAttacker, data )
    }
}

function onRemoveSingleAttacker( hall: SiegableHall, player: L2PcInstance, parameterOne: string ): void {
    if ( hall.isInSiege() ) {
        player.sendMessage( 'Clan hall is in siege, cannot remove attackers' )
        return
    }

    let clan: L2Clan
    if ( parameterOne ) {
        clan = ClanCache.getClanByName( parameterOne )
        if ( !clan ) {
            return player.sendMessage( 'Specified clan does not exist' )
        }

        if ( !hall.getSiege().checkIsAttacker( clan ) ) {
            return player.sendMessage( 'Clan is not participating as an attacker' )
        }
    } else {
        let target = player.getTarget()
        if ( !target ) {
            return player.sendMessage( 'You must target a clan member of the attacker' )
        }

        if ( !target.isPlayer() ) {
            return player.sendMessage( 'You must target a player' )
        }

        clan = ( target as L2PcInstance ).getClan()
        if ( !clan ) {
            return player.sendMessage( 'You must target a player with a clan' )
        }

        if ( !hall.getSiege().checkIsAttacker( clan ) ) {
            return player.sendMessage( 'Your target\'s clan is not participating as an attacker' )
        }
    }

    hall.removeAttacker( clan )

    if ( ListenerCache.hasGeneralListener( EventType.GMSiegeHallRemoveAttacker ) ) {
        let data: GMSiegeHallRemoveAttackerEvent = {
            attackerClanId: clan.getId(),
            hallId: hall.getId(),
            originatorId: player.getObjectId(),
        }

        ListenerCache.sendGeneralEvent( EventType.GMSiegeHallRemoveAttacker, data )
    }
}

function onRemoveAllAttackers( hall: SiegableHall, player: L2PcInstance ): void {
    if ( hall.isInSiege() ) {
        player.sendMessage( 'The clan hall is in siege, cannot remove attackers' )
        return
    }

    hall.getSiege().attackers = {}

    if ( ListenerCache.hasGeneralListener( EventType.GMSiegeHallClearAttackers ) ) {
        let data: GMSiegeHallClearAttackersEvent = {
            hallId: hall.getId(),
            originatorId: player.getObjectId(),
        }

        ListenerCache.sendGeneralEvent( EventType.GMSiegeHallClearAttackers, data )
    }
}

async function onForwardSiege( hall: SiegableHall, player: L2PcInstance ): Promise<void> {
    let engine: ClanHallSiegeEngine = hall.getSiege()

    if ( !engine ) {
        return
    }

    engine.cancelSiegeTask()

    switch ( hall.getSiegeStatus() ) {
        case SiegeStatus.REGISTERING:
            await engine.runPrepareOwnerTask()

        case SiegeStatus.WAITING_BATTLE:
            await engine.startSiege()

        case SiegeStatus.RUNNING:
            await engine.endSiege()
    }

    if ( ListenerCache.hasGeneralListener( EventType.GMSiegeHallForwardSiege ) ) {
        let data: GMSiegeHallForwardSiegeEvent = {
            hallId: hall.getId(),
            originatorId: player.getObjectId(),
        }

        return ListenerCache.sendGeneralEvent( EventType.GMSiegeHallForwardSiege, data )
    }
}