import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { DataManager } from '../../../data/manager'
import { ConfigManager } from '../../../config/ConfigManager'
import { EventType, GMAddLevelEvent, GMSetLevelEvent } from '../../models/events/EventType'
import { ListenerCache } from '../../cache/ListenerCache'
import { L2World } from '../../L2World'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import _ from 'lodash'
import aigle from 'aigle'

const enum PlayerLevelCommands {
    AddLevel = 'level-add',
    SetLevel = 'level-set',
    RemoveLevelInRadius = 'level-remove-radius',
    LevelUp = 'level-up',
}

export const PlayerLevel: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            PlayerLevelCommands.AddLevel,
            PlayerLevelCommands.SetLevel,
            PlayerLevelCommands.RemoveLevelInRadius,
            PlayerLevelCommands.LevelUp,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case PlayerLevelCommands.AddLevel:
                return [
                    formatOptionsDescription( PlayerLevelCommands.AddLevel, '[amount = 1]', 'Adds specified amount of levels to targeted player.' ),
                ]

            case PlayerLevelCommands.SetLevel:
                return [
                    formatOptionsDescription( PlayerLevelCommands.SetLevel, '[amount = 1]', 'Sets level of targeted player to specified amount.' ),
                ]

            case PlayerLevelCommands.RemoveLevelInRadius:
                return [
                    formatOptionsDescription( PlayerLevelCommands.RemoveLevelInRadius, '[amount = 1] [radius = 500]', 'Add/remove levels of players centered on targeted player, in specified radius.' ),
                ]

            case PlayerLevelCommands.LevelUp:
                return [
                    formatDescription( PlayerLevelCommands.LevelUp, 'Increases targeted player level. A shortened form of command //level-add 1' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case PlayerLevelCommands.AddLevel:
                return onAddLevel( commandChunks, player )

            case PlayerLevelCommands.SetLevel:
                return onSetLevel( commandChunks, player )

            case PlayerLevelCommands.RemoveLevelInRadius:
                return onRemoveLevelInRadius( commandChunks, player )

            case PlayerLevelCommands.LevelUp:
                return onLevelUp( player )
        }
    },
}

async function onAddLevel( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let amount: number = _.defaultTo( _.parseInt( commandChunks[ 1 ] ), 1 )

    let target = player.getTarget()
    if ( !target || !target.isPlayer() ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
    }

    let targetPlayer = ( target as L2PcInstance )

    await targetPlayer.addLevel( amount )

    if ( ListenerCache.hasGeneralListener( EventType.GMAddLevel ) ) {
        return ListenerCache.sendGeneralEvent( EventType.GMAddLevel, createEventData( player, targetPlayer, amount ) )
    }
}

async function onSetLevel( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let amount: number = _.defaultTo( _.parseInt( commandChunks[ 1 ] ), 1 )

    let target = player.getTarget()
    if ( !target || !target.isPlayer() ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
    }

    let targetPlayer = ( target as L2PcInstance )
    let previousLevel: number = targetPlayer.getLevel()
    let nextLevel: number = _.clamp( amount, 1, ConfigManager.character.getMaxPlayerLevel() )

    return setPlayerLevel( targetPlayer, player, nextLevel - previousLevel )
}

function createEventData( player: L2PcInstance, target: L2PcInstance, amount: number ): GMAddLevelEvent | GMSetLevelEvent {
    return {
        originatorId: player.getObjectId(),
        targetId: target.getObjectId(),
        amount,
        previousLevel: target.getLevel(),
    }
}

async function setPlayerLevel( targetPlayer: L2PcInstance, player: L2PcInstance, levelDifference: number ): Promise<void> {
    let previousLevel: number = targetPlayer.getLevel()
    let nextLevel = previousLevel + levelDifference

    targetPlayer.setLevel( nextLevel )
    targetPlayer.setExp( DataManager.getExperienceData().getExpForLevel( Math.min( nextLevel, targetPlayer.getMaxExpLevel() ) ) )
    await targetPlayer.onLevelChange( nextLevel > previousLevel )
    targetPlayer.broadcastInfo()

    if ( ListenerCache.hasGeneralListener( EventType.GMSetLevel ) ) {
        return ListenerCache.sendGeneralEvent( EventType.GMSetLevel, createEventData( player, targetPlayer, levelDifference ) )
    }
}

async function onRemoveLevelInRadius( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let [ , amountValue, radiusValue ] = commandChunks

    let amount: number = _.defaultTo( _.parseInt( amountValue ), 1 )
    let radius = _.defaultTo( _.parseInt( radiusValue ), 500 )

    let target = player.getTarget()
    if ( !target || !target.isPlayer() ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
    }

    let levelDifference: number = _.clamp( amount, -ConfigManager.character.getMaxPlayerLevel(), ConfigManager.character.getMaxPlayerLevel() )
    let players = L2World.getVisiblePlayers( target, radius, false )

    await aigle.resolve( players ).each( ( currentPlayer: L2PcInstance ): Promise<void> => {
        if ( currentPlayer.isGM() ) {
            return
        }

        return setPlayerLevel( currentPlayer, player, levelDifference )
    } )
}

async function onLevelUp( player: L2PcInstance ) : Promise<void> {
    let target = player.getTarget()
    if ( !target || !target.isPlayer() ) {
        return player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_INCORRECT ) )
    }

    let targetPlayer = ( target as L2PcInstance )
    return setPlayerLevel( targetPlayer, player, 1 )
}