import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatOptionsDescription } from './helpers/FormatHelpers'
import { DataManager } from '../../../data/manager'
import { createPagination } from './helpers/SearchHelper'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { L2ArmorSet } from '../../models/L2ArmorSet'
import { createButton, getArmorTypeColor, getCrystalColor } from './helpers/AdminHtml'
import { parseInt } from 'lodash'
import { createSearchItemByIdBypass } from './SearchItem'
import { CrystalType } from '../../models/items/type/CrystalType'
import { L2Armor } from '../../models/items/L2Armor'
import { ArmorType } from '../../enums/items/ArmorType'
import { createSearchSkillByIdBypass } from './SearchSkill'

const enum Commands {
    Menu = 'armor-sets',
    View = 'armor-sets-view',
    Equip = 'armor-sets-equip'
}

const itemsPerPage = 60

export const ArmorSets : IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            Commands.Menu,
            Commands.View,
            Commands.Equip
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case Commands.Menu:
                return [
                    formatOptionsDescription( Commands.Menu, '[page=1]', 'Show menu options for Armor Sets for current page' )
                ]

            case Commands.View:
                return [
                    formatOptionsDescription( Commands.View, '<setId>', 'View armor set information' )
                ]

            case Commands.Equip:
                return [
                    formatOptionsDescription( Commands.Equip, '<setId> <modifier=light|heavy>', 'Receive and equip armor set pieces' )
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        const chunks = command.split( ' ' )

        switch ( chunks[ 0 ] ) {
            case Commands.Menu:
                return onMenu( chunks, player )

            case Commands.View:
                return onView( chunks, player )

            case Commands.Equip:
                return onEquip( chunks, player )
        }
    }
}

export function createArmorSetsMenuBypass( page: number = 1 ) : string {
    return `bypass -h admin_${ Commands.Menu } ${ page }`
}

export function createViewArmorSetBypass( setId: number ) : string {
    return `bypass -h admin_${ Commands.View } ${ setId }`
}

export function createArmorSetEquipBypass( id: number, index: number ) : string {
    return `bypass admin_${ Commands.Equip } ${ id } ${ index }`
}

function getArmorTypes( armor: L2ArmorSet ) : Array<string> {
    let armorTypes : Set<string> = new Set<string>()

    armor.itemIds.forEach( itemId => {
        let template = DataManager.getItems().getTemplate( itemId )
        armorTypes.add( ArmorType[ ( template as L2Armor ).getItemType() ] )
    } )

    if ( armorTypes.size > 1 ) {
        armorTypes.delete( 'NONE' )
    }

    return Array.from( armorTypes )
}

function getArmorSetSkillButton( armor: L2ArmorSet ) : string {
    if ( !armor.setSkill ) {
        return 'None'
    }

    return createButton( 'View', createSearchSkillByIdBypass( armor.setSkill.getId() ) )
}

function getArmorEnchantSkillButton( armor: L2ArmorSet ) : string {
    if ( !armor.enchant6Skill ) {
        return 'None'
    }

    return createButton( 'View', createSearchSkillByIdBypass( armor.enchant6Skill.getId() ) )
}

function createButtons( armor: L2ArmorSet ) : string {
    let maxVariations = Math.max(
        armor.feet?.size ?? 0,
        armor.legs?.size ?? 0,
        armor.gloves?.size ?? 0,
        armor.head?.size ?? 0,
        armor.shield?.size ?? 0 )

    const buttons : Array<string> = []

    for ( let index = 1; index <= maxVariations; index++ ) {
        let button = createButton( `Equip ${ index } set`, createArmorSetEquipBypass( armor.id, index ), 80 )
        buttons.push( `<td width=80>${button}</td>` )
    }

    buttons.unshift( `<td width=80>${ createButton( 'Armor Sets', createArmorSetsMenuBypass(), 80 ) } </td>` )

    return buttons.join( '' )
}

function onMenu( chunks: Array<string>, player: L2PcInstance ) : void {
    let page = 1
    if ( chunks.length > 1 ) {
        page = parseInt( chunks[ 1 ], 10 )
    }

    if ( !Number.isInteger( page ) ) {
        return player.sendMessage( `${Commands.Menu} : please specify positive integer for page` )
    }

    let allPages = Math.ceil( DataManager.getArmorSetsData().getAll().length / 60 )

    if ( page > allPages ) {
        return player.sendMessage( `${ Commands.Menu} : page value cannot be more than ${ allPages }` )
    }

    let currentPageSets = DataManager.getArmorSetsData().getAll().slice( ( page - 1 ) * itemsPerPage, page * itemsPerPage )
    if ( currentPageSets.length === 0 ) {
        return player.sendMessage( `${ Commands.Menu} : no items for page ${page}` )
    }

    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/armor-sets/menu-item.htm' )
    let renderedItems = currentPageSets.map( ( armorSet: L2ArmorSet ) : string => {
        let template = DataManager.getItems().getTemplate( armorSet.chestId ) as L2Armor

        return itemHtml
            .replace( '%name%', armorSet.name )
            .replace( '%crystalColor%', getCrystalColor( template.crystalType ) )
            .replace( '%grade%', CrystalType[ template.crystalType ] )
            .replace( '%type%', ArmorType[ template.type ].slice( 0,1 ) )
            .replace( '%typeColor%', getArmorTypeColor( template.type ) )
            .replace( '%action%', `admin_${ Commands.View } ${armorSet.id}` )
    } ).join( '' )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/armor-sets/menu.htm' )
        .replace( '%pagination%', createPagination( allPages, `bypass -h admin_${ Commands.Menu }` ) )
        .replace( '%items%', renderedItems )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onView( chunks: Array<string>, player: L2PcInstance ) : void {
    if ( chunks.length === 1 ) {
        return player.sendMessage( `${ Commands.View } : please specify armor set id` )
    }

    let id = parseInt( chunks[ 1 ], 10 )
    if ( !Number.isInteger( id ) || id < 1 ) {
        return player.sendMessage( `${ Commands.View } : please specify positive integer for id` )
    }

    let armorSet = DataManager.getArmorSetsData().getById( id )
    if ( !armorSet ) {
        return player.sendMessage( `${ Commands.View} : no armor set found for id=${id}` )
    }

    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/armor-sets/view-item.htm' )
    let items = Array.from( armorSet.itemIds ).map( ( itemId: number ) : string => {
        let template = DataManager.getItems().getTemplate( itemId )
        return itemHtml
            .replace( '%icon%', template.icon )
            .replace( '%name%', template.name )
            .replace( '%action%', createSearchItemByIdBypass( itemId ) )
    } ).join( '' )

    let chestTemplate = DataManager.getItems().getTemplate( armorSet.chestId )
    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/armor-sets/view.htm' )
        .replace( '%id%', armorSet.id.toString() )
        .replace( '%buttons%', createButtons( armorSet ) )
        .replace( '%items%', items )
        .replaceAll( '%name%', armorSet.name )

        .replace( '%grade%', CrystalType[ chestTemplate.crystalType ] )
        .replace( '%types%', getArmorTypes( armorSet ).join( ', ' ) )
        .replace( '%itemCount%', armorSet.itemIds.size.toString() )
        .replace( '%setSkill%', getArmorSetSkillButton( armorSet ) )

        .replace( '%enchant6Skill%', getArmorEnchantSkillButton( armorSet ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function getSetItem( itemIds: Set<number>, setNumber: number ) : number {
    if ( itemIds.size === 1 || itemIds.size < setNumber ) {
        return itemIds.keys().next().value
    }

    return Array.from( itemIds )[ setNumber - 1 ]
}

function getItemForType( armorSet: L2ArmorSet, setNumber : number ) : Array<number> {
    let itemIds : Array<number> = [ armorSet.chestId ]

    if ( armorSet.feet?.size > 0 ) {
        itemIds.push( getSetItem( armorSet.feet, setNumber ) )
    }

    if ( armorSet.gloves?.size > 0 ) {
        itemIds.push( getSetItem( armorSet.gloves, setNumber ) )
    }

    if ( armorSet.legs?.size > 0 ) {
        itemIds.push( getSetItem( armorSet.legs, setNumber ) )
    }

    if ( armorSet.head?.size > 0 ) {
        itemIds.push( getSetItem( armorSet.head, setNumber ) )
    }

    if ( armorSet.shield?.size > 0 ) {
        itemIds.push( getSetItem( armorSet.shield, setNumber ) )
    }

    return itemIds
}

async function onEquip( chunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    if ( chunks.length < 3 ) {
        return player.sendMessage( `${ Commands.Equip } : please specify setId and modifier` )
    }

    let setId : number = parseInt( chunks[ 1 ], 10 )
    if ( !Number.isInteger( setId ) || setId < 1 ) {
        return player.sendMessage( `${ Commands.Equip } : please specify positive numeric value for set id` )
    }

    let armorSet = DataManager.getArmorSetsData().getById( setId )
    if ( !armorSet ) {
        return player.sendMessage( `${ Commands.Equip } : armor set does not exist` )
    }

    let setNumber = parseInt( chunks[ 2 ], 10 )
    if ( !setNumber || !Number.isInteger( setNumber ) || setNumber < 1 ) {
        return player.sendMessage( `${ Commands.Equip } : please specify valid set index` )
    }

    let itemIds: Array<number> = getItemForType( armorSet, setNumber )

    for ( const itemId of itemIds ) {
        if ( !player.getInventory().hasItemWithId( itemId ) ) {
            await player.getInventory().addItem( itemId,1, player.getObjectId(), Commands.Equip )
        }

        let item = player.getInventory().getItemByItemId( itemId )
        await player.getInventory().equipItem( item )
    }

    player.sendMessage( `${ Commands.Equip } : set with name=${ armorSet.name } and id=${ armorSet.id } equipped item ids: ${ itemIds.join( ', ' )}` )
}