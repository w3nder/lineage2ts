import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatDescription, formatOptionsDescription } from './helpers/FormatHelpers'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { SevenSigns } from '../../directives/SevenSigns'
import moment from 'moment'
import { AllSeals, AllSides, SevenSignsPeriod, SevenSignsSeal, SevenSignsSide } from '../../values/SevenSignsValues'
import {
    EnumToSeal,
    EnumToSide,
    L2SevenSignsCharacterSeal,
    L2SevenSignsCharacterSide, SealToEnum,
    ShortSealName, SideToEnum,
    SideToShortName
} from '../../enums/SevenSigns'
import { SSQStatus, SSQStatusPage } from '../../packets/send/SSQStatus'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2World } from '../../L2World'

const enum Commands {
    Main = 'ss-main',
    Target = 'ss-target',
    Status = 'ss-status',
    SetPeriod = 'ss-period-set',
    NextPeriod = 'ss-period-next',
    AddContribution = 'ss-contribution-add',
    RemoveContribution = 'ss-contribution-remove',
    ResetContribution = 'ss-contribution-reset',
    RegisterPlayer = 'ss-player-register'
}

const dateFormat = 'dddd, MMMM Do YYYY, h:mm:ss a Z'

export const SevenSignsAdministration : IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            Commands.Main,
            Commands.Target,
            Commands.SetPeriod,
            Commands.NextPeriod,
            Commands.Status,
            Commands.AddContribution,
            Commands.RemoveContribution,
            Commands.RegisterPlayer,
            Commands.ResetContribution,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case Commands.Main:
                return [
                    formatDescription( Commands.Main, 'Show Seven Signs information' )
                ]

            case Commands.Target:
                return [
                    formatDescription( Commands.Target, 'Show target player information for Seven Signs' )
                ]

            case Commands.Status:
                return [
                    formatOptionsDescription( Commands.Status, '<page>=[ 1 | 2 | 3 | 4 ]','Receive SSQStatus packet about Seven Signs information' )
                ]

            case Commands.SetPeriod:
                return [
                    formatOptionsDescription( Commands.SetPeriod, '<period>=[ 1 | 2 | 3 | 4 ]', 'Initialize SevenSigns to particular period specified by number' ),
                    'Number options: initialization=1, competition=2, results=3, validation=4'
                ]

            case Commands.NextPeriod:
                return [
                    formatDescription( Commands.NextPeriod, 'Runs SevenSigns period change logic.' )
                ]

            case Commands.AddContribution:
                return [
                    formatOptionsDescription( Commands.AddContribution, '<bluStones> <greenStones> <redStones>', 'Add stone amounts to current player SevenSigns record' )
                ]

            case Commands.RemoveContribution:
                return [
                    formatOptionsDescription( Commands.RemoveContribution, '<bluStones> <greenStones> <redStones>', 'Remove stone amounts from current player SevenSigns record' )
                ]

            case Commands.RegisterPlayer:
                return [
                    formatOptionsDescription( Commands.RegisterPlayer, '<side=[dawn | dusk]> <seal=[avarice | gnosis | strife]> [objectId]', 'Register GM player or player specified by objectId' )
                ]

            case Commands.ResetContribution:
                return [
                    formatOptionsDescription( Commands.ResetContribution, '[ objectId ]', 'Reset player stone contribution scores and remove them from Seven Signs totals.' ),
                    'If objectId is not specified, current player target is being used.'
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case Commands.Main:
                return onShowMain( player )

            case Commands.Target:
                return onTargetInfo( player )

            case Commands.Status:
                return onStatus( commandChunks, player )

            case Commands.SetPeriod:
                return onSetPeriod( commandChunks, player )

            case Commands.AddContribution:
                return onAddContribution( commandChunks, player )

            case Commands.RemoveContribution:
                return onRemoveContribution( commandChunks, player )

            case Commands.RegisterPlayer:
                return onRegisterPlayer( commandChunks, player )

            case Commands.ResetContribution:
                return onResetContribution( commandChunks, player )

            case Commands.NextPeriod:
                return SevenSigns.runSevenSignsPeriodChange()
        }
    }
}

async function onShowMain( player: L2PcInstance ) : Promise<void> {
    let sealSideHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/sevenSigns/main-sealItem.htm' )
    let scoreHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/sevenSigns/main-stoneItem.htm' )
    let sealScores : Array<string> = []

    SevenSigns.getTotals( SevenSignsSide.Dawn ).forEach( ( score: number, seal: SevenSignsSeal ) : void => {
        sealScores.push(
            scoreHtml
                .replace( '%side%', SideToShortName[ SevenSignsSide.Dawn ] )
                .replace( '%seal%', SevenSigns.getSealName( seal, true ) )
                .replace( '%amount%', score.toString() )
        )
    } )

    SevenSigns.getTotals( SevenSignsSide.Dusk ).forEach( ( score: number, seal: SevenSignsSeal ) : void => {
        sealScores.push(
            scoreHtml
                .replace( '%side%', SideToShortName[ SevenSignsSide.Dusk ] )
                .replace( '%seal%', SevenSigns.getSealName( seal, true ) )
                .replace( '%amount%', score.toString() )
        )
    } )

    let sealSides : Array<string> = AllSeals.map( ( seal: SevenSignsSeal ) : string => {
        let side = SevenSigns.getWinningSealSide( seal ) ?? SevenSignsSide.None

        return sealSideHtml
            .replace( '%seal%', ShortSealName[ seal ] )
            .replace( '%side%', SideToShortName[ side ] )
            .replace( '%total%', SevenSigns.getSealTotal( seal,side ).toString() )
    } )

    let winningSide = SevenSigns.getSideName( SevenSigns.isSealValidationPeriod() ? SevenSigns.getWinnerSide() : SevenSigns.getProjectedWinningSide() )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/sevenSigns/main.htm' )
        .replace( '%period%', SevenSigns.getCurrentPeriodName() )
        .replace( '%nextDate%', SevenSigns.getNextPeriodDate() )
        .replace( '%playerAmount%', SevenSigns.getParticipationAmount().toString() )
        .replace( '%winningSide%', winningSide )

        .replace( '%lastUpdateDate%', moment( SevenSigns.getLastUpdateDate() ).format( dateFormat ) )
        .replace( '%sealSides%', sealSides.join( '' ) )
        .replace( '%sealScores%', sealScores.join( '' ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function onTargetInfo( player: L2PcInstance ) : void {
    let target = player.getTarget()
    if ( !target || !target.isPlayer() ) {
        return player.sendMessage( 'No target player selected' )
    }

    let data = SevenSigns.getPlayerData( target.getObjectId() )
    let contributionTime = data ? data.lastContributionTime : 0

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/sevenSigns/playerInfo.htm' )
        .replace( '%name%', target.getName() )
        .replace( '%side%', data ? EnumToSide[ data.side ] : L2SevenSignsCharacterSide.None )
        .replace( '%seal%', data ? EnumToSeal[ data.seal ] : L2SevenSignsCharacterSeal.None )
        .replace( '%score%', data ? data.contributionScore.toString() : '0' )

        .replace( '%adena%', data ? data.ancientAdenaCount.toString() : '0' )
        .replace( '%redStones%', data ? data.redStoneCount.toString() : '0' )
        .replace( '%greenStones%', data ? data.greenStoneCount.toString() : '0' )
        .replace( '%blueStones%', data ? data.blueStoneCount.toString() : '0' )

        .replace( '%contributionDate%', contributionTime > 0 ? moment( contributionTime ).format( dateFormat ) : 'Never Contributed' )
        .replace( '%joinDate%', data ? moment( data.joinTime ).format( dateFormat ) : 'Never Joined' )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

async function onStatus( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let page = parseInt( commandChunks[ 1 ] )
    if ( page < 0 || page > 4 || !Number.isInteger( page ) ) {
        return player.sendMessage( 'Please supply valid page value' )
    }

    player.sendOwnedData( await SSQStatus( player.getObjectId(), page as SSQStatusPage ) )
}

async function onSetPeriod( commandChunks: Array<string>, player: L2PcInstance ) : Promise<void> {
    let period = parseInt( commandChunks[ 1 ] ) ?? 1
    if ( period < 0 || period > 4 ) {
        return player.sendMessage( 'Please supply valid period value' )
    }

    return SevenSigns.setPeriod( period as SevenSignsPeriod )
}

function onAddContribution( commandChunks: Array<string>, player: L2PcInstance ) : void {
    if ( commandChunks.length !== 4 ) {
        return player.sendMessage( 'Please specify all three stone amounts, even if they are zero.' )
    }

    let data = SevenSigns.getPlayerData( player.getObjectId() )
    if ( !data ) {
        return player.sendMessage( 'Character is not participant of Seven Signs' )
    }

    let stoneValues = commandChunks.slice( 1 ).map( value => parseInt( value, 10 ) )
    let hasInvalidValue = stoneValues.some( value => !Number.isInteger( value ) || value < 0 )
    if ( hasInvalidValue ) {
        return player.sendMessage( 'Please specify positive or zero numeric stone amounts.' )
    }

    let score = SevenSigns.addPlayerStoneContribution( player.getObjectId(), stoneValues[ 0 ], stoneValues[ 1 ], stoneValues[ 2 ] )
    if ( score < 0 ) {
        return player.sendMessage( `SevenSigns contribution was not accepted due to exceeding maximum player contribution score of ${ ConfigManager.sevenSigns.getMaxPlayerContribution() }` )
    }

    player.sendMessage( `Your SevenSigns contribution score of ${score} has been applied.` )
}

function onRemoveContribution( commandChunks: Array<string>, player: L2PcInstance ) : void {
    if ( commandChunks.length !== 4 ) {
        return player.sendMessage( 'Please specify all three stone amounts, even if they are zero.' )
    }

    let data = SevenSigns.getPlayerData( player.getObjectId() )
    if ( !data ) {
        return player.sendMessage( 'Character is not participant of Seven Signs' )
    }

    let stoneValues = commandChunks.slice( 1 ).map( value => parseInt( value, 10 ) )
    let hasInvalidValue = stoneValues.some( value => !Number.isInteger( value ) || value < 0 )
    if ( hasInvalidValue ) {
        return player.sendMessage( 'Please specify positive or zero numeric stone amounts.' )
    }

    let score = SevenSigns.addPlayerStoneContribution( player.getObjectId(), -stoneValues[ 0 ], -stoneValues[ 1 ], -stoneValues[ 2 ] )
    player.sendMessage( `Your SevenSigns contribution score of ${score} has been applied.` )
}

function onRegisterPlayer( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let [ , sideValue, sealValue, objectIdValue ] = commandChunks
    if ( !sideValue || !sealValue ) {
        return player.sendMessage( 'Please specify side and seal for player registration' )
    }

    let side = SideToEnum[ sideValue.toLowerCase() ]
    let seal = SealToEnum[ sealValue.toLowerCase() ]

    if ( !AllSides.includes( side ) ) {
        return player.sendMessage( 'Please specify correct side value' )
    }

    if ( !AllSeals.includes( seal ) ) {
        return player.sendMessage( 'Please specify correct seal value' )
    }

    let objectId = objectIdValue ? parseInt( objectIdValue ) : player.getObjectId()
    if ( !Number.isInteger( objectId ) ) {
        return player.sendMessage( 'Please specify numeric objectId' )
    }

    SevenSigns.updatePlayerInformation( objectId, side, seal )

    if ( objectId === player.getObjectId() ) {
        return player.sendMessage( `Your registration for ${sideValue} side of ${sealValue} seal is successful!` )
    }

    player.sendMessage( 'Player registration has been successful!' )
}

function onResetContribution( commandChunks: Array<string>, player: L2PcInstance ) : void {
    let [ , objectIdValue ] = commandChunks
    let objectId = objectIdValue ? parseInt( objectIdValue, 10 ) : player.getTargetId()
    if ( !Number.isInteger( objectId ) ) {
        return player.sendMessage( 'Please specify valid objectId or target' )
    }

    let object = L2World.getObjectById( objectId )
    if ( !object || !object.isPlayer() ) {
        return player.sendMessage( 'Please specify logged in player by objectId or target' )
    }

    SevenSigns.resetPlayerContribution( objectId )
    player.sendMessage( 'Player contribution scores have been reset.' )
}