import { ExServerPrimitive } from '../../../packets/send/builder/ExServerPrimitive'
import { GeneralHelper } from '../../../helpers/GeneralHelper'
import { L2World } from '../../../L2World'
import { L2Character } from '../../../models/actor/L2Character'
import Timeout = NodeJS.Timeout
import { L2PcInstance } from '../../../models/actor/instance/L2PcInstance'

interface DrawParameters {
    packets: Array<ExServerPrimitive>
    clearTimeout: Timeout
}

const playerPackets : Record<number, DrawParameters> = {}
const duration = GeneralHelper.minutesToMillis( 5 )

export const DrawHelper = {
    getDrawPacket( character: L2Character, name: string = character.getName(), x : number = character.getX(), y : number = character.getY(), z: number = -65535 ) : ExServerPrimitive {
        let packet = new ExServerPrimitive( name, x, y, z )

        let data = playerPackets[ character.getObjectId() ]
        if ( !data ) {
            data = {
                packets: [ packet ],
                clearTimeout: setTimeout( DrawHelper.clearPackets, duration, character.getObjectId() )
            }

            playerPackets[ character.getObjectId() ] = data

            return packet
        }

        data.packets.push( packet )
        data.clearTimeout.refresh()

        return packet
    },

    clearPackets( playerId: number ) : void {
        let data = playerPackets[ playerId ]
        if ( !data ) {
            return
        }

        delete playerPackets[ playerId ]
        clearTimeout( data.clearTimeout )

        let player = L2World.getPlayer( playerId )
        if ( !player ) {
            return
        }

        data.packets.forEach( packet => player.sendOwnedData( packet.getBuffer() ) )
    },

    clearExistingPacket( player: L2PcInstance, name: string ) : ExServerPrimitive {
        let data = playerPackets[ player.getObjectId() ]
        if ( !data ) {
            return
        }

        let packet = data.packets.find( ( packet: ExServerPrimitive ) : boolean => {
            return packet.name === name
        } )

        if ( packet ) {
            player.sendOwnedData( packet.getBuffer() )
        }
    }
}