export function formatDescription( command: string, description: string ): string {
    return `//${ command } - ${ description }`
}

export function formatOptionsDescription( command: string, options: string, description: string ): string {
    return `//${ command } ${options} - ${ description }`
}