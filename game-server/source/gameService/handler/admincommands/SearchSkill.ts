import { IAdminCommand } from '../IAdminCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { formatOptionsDescription } from './helpers/FormatHelpers'
import _ from 'lodash'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessage } from '../../packets/send/NpcHtmlMessage'
import { createPagination } from './helpers/SearchHelper'
import { SkillCache } from '../../cache/SkillCache'
import { SkillOperateType } from '../../models/skills/SkillOperateType'
import { Skill } from '../../models/Skill'
import { AbnormalType } from '../../models/skills/AbnormalType'
import { L2ExtractableProductItem } from '../../models/L2ExtractableSkill'

const pageLimit: number = 60

const enum SearchSkillCommands {
    Name = 'search-skill-name',
    Id = 'search-skill-id'
}

export const SearchSkill: IAdminCommand = {
    getCommandNames(): Array<string> {
        return [
            SearchSkillCommands.Name,
            SearchSkillCommands.Id,
        ]
    },

    getDescription( command: string ): Array<string> {
        switch ( command ) {
            case SearchSkillCommands.Name:
                return [
                    formatOptionsDescription( SearchSkillCommands.Name, '<partial name>', 'Shows matching skills by name' ),
                ]

            case SearchSkillCommands.Id:
                return [
                    formatOptionsDescription( SearchSkillCommands.Id, '<id>', 'Shows detailed description of skill specified by exact numeric id' ),
                ]
        }
    },

    async onCommand( command: string, player: L2PcInstance ): Promise<void> {
        let commandChunks = command.split( ' ' )

        switch ( commandChunks[ 0 ] ) {
            case SearchSkillCommands.Name:
                return onSearchByName( commandChunks, player )

            case SearchSkillCommands.Id:
                return onSearchById( commandChunks, player )
        }
    },
}

export function createSearchSkillByIdBypass( id: number ) : string {
    return `bypass -h admin_${ SearchSkillCommands.Id } ${ id }`
}

/*
    While command description describes skill name as input,
    additional parameter can be supplied for pagination value.
 */
function onSearchByName( commandChunks: Array<string>, player: L2PcInstance ): void {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please specify skill name for search.' )
    }

    let pageValue = _.last( commandChunks )
    let currentPage = parseInt( pageValue, 10 )
    let skillName = currentPage ? commandChunks.slice( 1, commandChunks.length - 1 ).join( ' ' ) : commandChunks.slice( 1 ).join( ' ' )
    if ( !skillName ) {
        return player.sendMessage( 'Please specify non-empty skill name for search.' )
    }

    let ids: Array<number> = DataManager.getSkillData().getIdsByPartialName( skillName )
    if ( ids.length === 0 ) {
        return player.sendMessage( `No item templates found for name '${ skillName }'` )
    }

    let paginationIndex = currentPage ? currentPage - 1 : 0
    let paginatedIds = _.chunk( ids, pageLimit )
    let currentPageIds = paginatedIds[ paginationIndex ]
    if ( !currentPageIds ) {
        return player.sendMessage( `Cannot display page ${ paginationIndex } for skill search name '${ skillName }'.` )
    }

    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/searchSkill/skillName-item.htm' )
    let items: string = currentPageIds.map( ( id: number ): string => {
        let skill = SkillCache.getSkill( id, 1 )

        return itemHtml
                .replaceAll( '%id%', id.toString() )
                .replace( '%name%', skill ? skill.getName() : `Not found skill for id = ${ id }` )
                .replace( '%action%', `admin_${ SearchSkillCommands.Id } ${ id } ${ skillName }` )
    } ).join( '' )

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/searchSkill/skillName.htm' )
                          .replace( '%items%', items )
                          .replace( '%name%', skillName )
                          .replace( '%pagination%', createPagination( paginatedIds.length, `bypass -h admin_${ SearchSkillCommands.Name } ${ skillName }` ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}

function getSearchButton( text: string ): string {
    if ( !text ) {
        return ''
    }

    return `<td><button value="Back To Search" action="bypass -h admin_${ SearchSkillCommands.Name } ${ text }" width=100 height=21 back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF"></td>`
}

function getEffectType( skill: Skill ): string {
    switch ( skill.operateType ) {
        case SkillOperateType.A1:
            return 'Instant'

        case SkillOperateType.A4:
        case SkillOperateType.A2:
            return 'Continuous -> Instant'

        case SkillOperateType.A3:
            return 'Instant -> Continuous'

        case SkillOperateType.CA1:
            return 'Instant Effect (Channeling)'

        case SkillOperateType.CA5:
            return 'Continuous effect (Channeling)'

        case SkillOperateType.DA1:
            return 'Charge/Rush Instant'

        case SkillOperateType.DA2:
            return 'Charge/Rush Continuous'

        case SkillOperateType.P:
            return 'Passive'

        case SkillOperateType.T:
            return 'Toggle'
    }
}

function getExtractableItems( skill: Skill ): string {
    let data = skill.getExtractableSkill()

    if ( !data || data.products.length === 0 ) {
        return ''
    }

    let itemHtml = DataManager.getHtmlData().getItem( 'overrides/html/command/searchSkill/skillId-info-item.htm' )
    let infoItems: Array<string> = []

    data.products.forEach( ( product: L2ExtractableProductItem ) => {
        product.items.forEach( ( item ) => {
            let template = DataManager.getItems().getTemplate( item.itemId )
            infoItems.push(
                    itemHtml
                            .replace( '%icon%', template.icon )
                            .replace( '%name%', `${ template.getName() } - ${ product.chance }%` )
                            .replace( '%amount%', item.amount.toString() ),
            )
        } )
    } )

    if ( infoItems.length === 0 ) {
        return ''
    }

    return DataManager.getHtmlData().getItem( 'overrides/html/command/searchSkill/skillId-info.htm' )
            .replace( '%name%', 'Extractable Items' )
            .replace( '%items%', infoItems.join( '' ) )
}

function getApplyInfo( skill: Skill, player: L2PcInstance ): string {
    if ( skill.isPassive() || skill.isToggle() ) {
        return ''
    }

    let skillLevels = _.times( SkillCache.getMaxLevel( skill.getId() ), index => index + 1 ).join( ';' )
    return DataManager.getHtmlData().getItem( 'overrides/html/command/searchSkill/skillId-apply.htm' )
                      .replaceAll( '%skillId%', skill.getId().toString() )
                      .replaceAll( '%playerId%', player.getObjectId().toString() )
                      .replace( '%skillLevels%', skillLevels )
}

function onSearchById( commandChunks: Array<string>, player: L2PcInstance ): void {
    if ( commandChunks.length === 1 ) {
        return player.sendMessage( 'Please specify skill id for search.' )
    }

    let [ , idValue, ...searchTextValues ] = commandChunks
    let id = parseInt( idValue, 10 )
    if ( !id ) {
        return player.sendMessage( 'Please specify valid number skill id for search.' )
    }

    let searchText = searchTextValues.join( ' ' )

    let skill = SkillCache.getSkill( id, 1 )
    if ( !skill ) {
        return player.sendMessage( `No skill found for id = ${ id }` )
    }

    let item = skill.getItemConsumeId() > 0 ? DataManager.getItems().getTemplate( skill.getItemConsumeId() ) : null

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/searchSkill/skillId.htm' )
                          .replaceAll( '%skillId%', id.toString() )
                          .replaceAll( '%icon%', skill.icon ? skill.icon : 'L2UI.SquareBlank' )
                          .replace( '%name%', skill.getName() )
                          .replace( '%effectType%', getEffectType( skill ) )

                          .replace( '%type%', SkillOperateType[ skill.operateType ] )
                          .replace( '%target%', skill.targetVerifyMethod.name )
                          .replace( '%selectTarget%', skill.targetSelectionMethod.name )
                          .replaceAll( '%playerId%', player.getObjectId().toString() )

                          .replace( '%buttons%', getSearchButton( searchText ) )
                          .replace( '%castRange%', skill.getCastRange().toString() )
                          .replace( '%abnormalType%', _.startCase( _.replace( AbnormalType[ skill.getAbnormalType() ], '_', ' ' ).toLowerCase() ) )
                          .replace( '%consumeItemName%', item ? item.getName() : 'None' )

                          .replace( '%consumeItemAmount%', item ? skill.getItemConsumeCount().toString() : 'None' )
                          .replace( '%maxLevel%', SkillCache.getMaxLevel( id ).toString() )
                          .replace( '%extractableInfo%', getExtractableItems( skill ) )
                          .replace( '%applyInfo%', getApplyInfo( skill, player ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId() ) )
}