import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ItemSkillsTemplate } from './ItemSkillsTemplate'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2EtcItem } from '../../models/items/L2EtcItem'
import { EtcItemType } from '../../enums/items/EtcItemType'

export async function ItemSkills( character: L2Playable, item: L2ItemInstance, forceUse: boolean ) : Promise<boolean> {
    let player : L2PcInstance = character.getActingPlayer()
    if ( player.isInOlympiadMode() && item.getItem().isOlympiadRestricted ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_ITEM_IS_NOT_AVAILABLE_FOR_THE_OLYMPIAD_EVENT ) )
        return false
    }

    if ( !character.isPlayer() && item.getItem().isEtcItem() && ( item.getItem() as L2EtcItem ).type === EtcItemType.ELIXIR ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    return ItemSkillsTemplate( character, item, forceUse )
}