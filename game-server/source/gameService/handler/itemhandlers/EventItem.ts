import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { Skill } from '../../models/Skill'
import { L2BlockInstance } from '../../models/actor/instance/L2BlockInstance'
import { ArenaParticipantsHolder } from '../../models/ArenaParticipantsHolder'
import { BlockCheckerManager } from '../../instancemanager/BlockCheckerManager'
import { L2World } from '../../L2World'
import aigle from 'aigle'

export async function EventItem( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    let player : L2PcInstance = character as L2PcInstance
    let itemId = item.getId()

    switch ( itemId ) {
        case 13787: // Handy's Block Checker Bond
            return useBlockCheckerItem( player, item )

        case 13788: // Handy's Block Checker Land Mine
            return useBlockCheckerItem( player, item )

        default:
            break
    }

    return false
}

async function useBlockCheckerItem( player: L2PcInstance, item: L2ItemInstance ) : Promise<boolean> {
    let blockCheckerArena = player.getBlockCheckerArena()
    if ( blockCheckerArena === -1 ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                .addItemInstanceName( item )
                .getBuffer()
        player.sendOwnedData( packet )
        return false
    }

    if ( !item.getItem().hasSkills() ) {
        return false
    }

    let currentSkill : Skill = item.getItem().getSkills()[ 0 ]
    let isConsumed = player.destroyItemWithCount( item, 1, true )
    if ( !isConsumed ) {
        return false
    }

    let block : L2BlockInstance = player.getTarget() as L2BlockInstance

    let holder : ArenaParticipantsHolder = BlockCheckerManager.getHolder( blockCheckerArena )
    if ( holder ) {
        let team = holder.getPlayerTeam( player )
        await aigle.resolve( L2World.getVisiblePlayers( block, currentSkill.getEffectRange() ) ).each( ( enemyPlayer: L2PcInstance ) : Promise<void> => {
            if ( !enemyPlayer ) {
                return
            }

            let enemyTeam = holder.getPlayerTeam( enemyPlayer )
            if ( ( enemyTeam !== -1 ) && ( enemyTeam !== team ) ) {
                return currentSkill.applyEffects( player, enemyPlayer, false, false, true, 0 )
            }
        } )

        return true
    }

    return false
}