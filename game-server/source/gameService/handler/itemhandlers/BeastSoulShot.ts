import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ShotType } from '../../enums/ShotType'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { MagicSkillUseWithCharacters } from '../../packets/send/MagicSkillUse'
import { Skill } from '../../models/Skill'

export async function BeastSoulShot( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    let player: L2PcInstance = character.getActingPlayer()
    let summon = player.getSummon()

    if ( !summon ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PETS_ARE_NOT_AVAILABLE_AT_THIS_TIME ) )
        return false
    }

    if ( summon.isDead() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SOULSHOTS_AND_SPIRITSHOTS_ARE_NOT_AVAILABLE_FOR_A_DEAD_PET ) )
        return false
    }

    if ( summon.isChargedShot( ShotType.Soulshot ) ) {
        return false
    }

    if ( !item.getItem().hasSkills() ) {
        return false
    }

    let itemId = item.getId()
    let shotConsumption = summon.getSoulShotsPerHit()
    let skill: Skill = item.getItem().getSkills()[ 0 ]

    if ( item.getCount() < shotConsumption ) {
        if ( !player.disableAutoShot( itemId ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_SOULSHOTS_FOR_PET ) )
        }

        return false
    }

    let isItemConsumed = await player.destroyItemByObjectId( item.getObjectId(), shotConsumption, false, 'Consume' )
    if ( !isItemConsumed ) {
        if ( !player.disableAutoShot( itemId ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_SOULSHOTS_FOR_PET ) )
        }

        return false
    }

    summon.setChargedShot( ShotType.Soulshot, true )

    let packet = new SystemMessageBuilder( SystemMessageIds.USE_S1_ )
            .addItemNameWithId( itemId )
            .getBuffer()
    player.sendOwnedData( packet )
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PET_USE_SPIRITSHOT ) )

    BroadcastHelper.dataToSelfInRange( player, MagicSkillUseWithCharacters( summon, summon, skill.getId(), skill.getLevel(), 0, 0 ), 600 )
    return true
}