import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ShowXMasSeal } from '../../packets/send/ShowXMasSeal'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'

export async function SpecialXMas( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    BroadcastHelper.dataToSelfBasedOnVisibility( character, ShowXMasSeal( item.getId() ) )
    return true
}