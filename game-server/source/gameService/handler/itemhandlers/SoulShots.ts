import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Weapon } from '../../models/items/L2Weapon'
import { ActionType } from '../../models/items/type/ActionType'
import { ShotType } from '../../enums/ShotType'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { MagicSkillUseWithCharacters } from '../../packets/send/MagicSkillUse'
import _ from 'lodash'
import { Skill } from '../../models/Skill'

export async function SoulShots( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    if ( !item.getItem().hasSkills() ) {
        return false
    }

    let player : L2PcInstance = character as L2PcInstance
    if ( player.isChargedShot( ShotType.Soulshot ) ) {
        return false
    }

    let weapon : L2ItemInstance = player.getActiveWeaponInstance()
    let weaponItem : L2Weapon = player.getActiveWeaponItem()
    let skill : Skill = item.getItem().getSkills()[ 0 ]
    let itemId = item.getId()

    if ( !weapon || ( weaponItem.getSoulShotCount() === 0 ) ) {
        if ( !player.getAutoSoulShot().includes( itemId ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_USE_SOULSHOTS ) )
        }

        return false
    }

    let gradeCheck = item.isEtcItem()
            && ( item.getEtcItem().getDefaultAction() === ActionType.SOULSHOT )
            && ( weapon.getItem().getItemGradeSPlus() === item.getItem().getItemGradeSPlus() )

    if ( !gradeCheck ) {
        if ( !player.getAutoSoulShot().includes( itemId ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SOULSHOTS_GRADE_MISMATCH ) )
        }
        return false
    }

    let soulShotCount = weaponItem.getSoulShotCount()
    if ( ( weaponItem.getReducedSoulShot() > 0 ) && Math.random() < weaponItem.getReducedSoulShotChance() ) {
        soulShotCount = weaponItem.getReducedSoulShot()
    }

    let isConsumed = await player.destroyItemByObjectId( item.getObjectId(), soulShotCount, false )
    if ( !isConsumed ) {
        if ( !player.disableAutoShot( itemId ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_SOULSHOTS ) )
        }

        return false
    }

    weapon.setChargedShot( ShotType.Soulshot, true )

    let packet = new SystemMessageBuilder( SystemMessageIds.USE_S1_ )
            .addItemNameWithId( itemId )
            .getBuffer()
    player.sendOwnedData( packet )

    BroadcastHelper.dataToSelfInRange( player, MagicSkillUseWithCharacters( player, player, skill.getId(), skill.getLevel(), 0, 0 ), 600 )
    return true
}