import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'

export async function Disguise( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    let player: L2PcInstance = character as L2PcInstance

    let id = TerritoryWarManager.getRegisteredTerritoryId( player )
    if ( id === ( item.getId() - 13596 ) ) {
        if ( player.getClan() && ( player.getClan().getCastleId() > 0 ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TERRITORY_OWNING_CLAN_CANNOT_USE_DISGUISE_SCROLL ) )
            return false
        }

        TerritoryWarManager.addDisguisedPlayer( player.getObjectId() )
        player.broadcastUserInfo()

        await player.destroyItemByObjectId( item.getObjectId(), 1, false )
        return true
    }

    if ( id > 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_DISGUISE_SCROLL_MEANT_FOR_DIFFERENT_TERRITORY ) )
        return false
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TERRITORY_WAR_SCROLL_CAN_NOT_USED_NOW ) )
    return false
}