import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { TvTEvent } from '../../models/entity/TvTEvent'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { Skill } from '../../models/Skill'
import { ActionType } from '../../models/items/type/ActionType'
import aigle from 'aigle'
import { recordDataViolation } from '../../helpers/PlayerViolations'
import { DataIntegrityViolationType } from '../../models/events/EventType'

export async function ItemSkillsTemplate( player: L2Playable, item: L2ItemInstance, forceUse: boolean ): Promise<boolean> {
    if ( !player.isPlayer() && !player.isPet() ) {
        return false
    }

    if ( player.isPet() && !item.isTradeable() ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    if ( !item.isPotion()
            && !item.isElixir()
            && !item.isScroll()
            && !item.getItem().hasExImmediateEffect()
            && player.isCastingNow() ) {
        return false
    }

    if ( !TvTEvent.isScrollUseAllowed( player.getObjectId() ) ) {
        player.sendOwnedData( ActionFailed() )
        return false
    }

    if ( !checkReuse( player, player.getItemRemainingReuseTime( item ), item, null ) ) {
        return false
    }

    if ( player.isCastingNow() ) {
        return false
    }

    if ( item.getEtcItem().getSkills().length === 0 ) {
        await recordDataViolation( player.getActingPlayerId(), '06b19031-38e2-489d-98a2-63ef99931c46', DataIntegrityViolationType.Item, 'Item has no skills', [ item.getId() ] )
        return false
    }

    let hasConsumeSkill = false
    let target = player.getTarget()

    let applicableSkills : Array<Skill> = item.getEtcItem().getSkills().filter( ( skill : Skill ) : boolean => {
        if ( skill.checkCondition( player, target ) ) {
            return true
        }

        if ( player.isSkillDisabled( skill ) ) {
            return false
        }

        return checkReuse( player, player.getSkillRemainingReuseTime( skill ), item, skill )
    } )

    if ( applicableSkills.length === 0 ) {
        return false
    }

    let shouldExit = await aigle.resolve( applicableSkills ).someSeries( async ( skill: Skill ) => {
        if ( skill.getItemConsumeId() > 0 ) {
            hasConsumeSkill = true
        }

        if ( player.isPet() ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.PET_USES_S1 )
                    .addSkillName( skill )
                    .getBuffer()
            player.sendOwnedData( packet )
        }

        /*
            Handling of items that have 'Herb' effects.
            While it is possible to use simultaneous cast, due to dynamic skill use
            multiple cast processes can be launched and terminated without finishing all of them.
            Thus, it represents optimization in case of herbs due to limited effect they produce on player.
         */
        if ( ( item.getItem().hasImmediateEffect() || item.getItem().hasExImmediateEffect() ) && skill.isStatic() ) {
            return player.doImmediateCast( skill )
        }

        if ( skill.isSimultaneousCast() ) {
            await player.doSimultaneousCast( skill )
        } else {
            let isMagicSuccess = await player.useMagic( skill, forceUse, false )
            if ( !isMagicSuccess ) {
                return true
            }
        }

        if ( skill.getReuseDelay() > 0 ) {
            player.addSkillReuse( skill, skill.getReuseDelay() )
        }

        return false
    } )

    if ( shouldExit ) {
        return false
    }

    if ( checkConsume( item, hasConsumeSkill ) ) {
        let isConsumed = await player.destroyItemByObjectId( item.getObjectId(), 1, false )
        if ( !isConsumed ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_ITEMS ) )
            return false
        }
    }

    return true
}

function checkReuse( playable: L2Playable, remainingTime: number, item: L2ItemInstance, skill: Skill ): boolean {
    if ( remainingTime > 0 ) {
        let hours = Math.floor( remainingTime / 3600000 )
        let minutes = Math.floor( ( remainingTime % 3600000 ) / 60000 )
        let seconds = Math.floor( ( remainingTime / 1000 ) % 60 )

        let packet: SystemMessageBuilder
        if ( hours > 0 ) {
            packet = new SystemMessageBuilder( SystemMessageIds.S2_HOURS_S3_MINUTES_S4_SECONDS_REMAINING_FOR_REUSE_S1 )
            if ( !skill || skill.isStatic() ) {
                packet.addItemInstanceName( item )
            } else {
                packet.addSkillName( skill )
            }

            packet.addNumber( hours )
            packet.addNumber( minutes )
        } else if ( minutes > 0 ) {
            packet = new SystemMessageBuilder( SystemMessageIds.S2_MINUTES_S3_SECONDS_REMAINING_FOR_REUSE_S1 )
            if ( !skill || skill.isStatic() ) {
                packet.addItemInstanceName( item )
            } else {
                packet.addSkillName( skill )
            }

            packet.addNumber( minutes )
        } else {
            packet = new SystemMessageBuilder( SystemMessageIds.S2_SECONDS_REMAINING_FOR_REUSE_S1 )
            if ( !skill || skill.isStatic() ) {
                packet.addItemInstanceName( item )
            } else {
                packet.addSkillName( skill )
            }
        }
        packet.addNumber( seconds )
        playable.sendOwnedData( packet.getBuffer() )

        return false
    }

    return true
}

function checkConsume( item: L2ItemInstance, hasConsumeSkill: boolean ): boolean {
    return [ ActionType.CAPSULE, ActionType.SKILL_REDUCE ].includes( item.getItem().getDefaultAction() )
            && !hasConsumeSkill
            && item.getItem().hasImmediateEffect()
}