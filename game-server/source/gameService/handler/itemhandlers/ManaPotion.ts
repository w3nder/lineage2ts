import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ItemSkills } from './ItemSkills'

export async function ManaPotion( character: L2Playable, item: L2ItemInstance, forceUse: boolean ): Promise<boolean> {
    if ( !ConfigManager.customs.enableManaPotionSupport() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOTHING_HAPPENED ) )
        return false
    }

    return ItemSkills( character, item, forceUse )
}