import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2Object } from '../../models/L2Object'
import { L2MonsterInstance } from '../../models/actor/instance/L2MonsterInstance'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { CastleManorManager } from '../../instancemanager/CastleManorManager'
import { L2Seed } from '../../models/L2Seed'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CastleManager } from '../../instancemanager/CastleManager'

export async function Seed( character: L2Playable, item: L2ItemInstance ): Promise<boolean> {
    if ( !ConfigManager.general.allowManor() ) {
        return false
    }

    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    let playerTarget: L2Object = character.getTarget()
    if ( !playerTarget ) {
        return false
    }

    if ( !playerTarget.isNpc() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return false
    }

    if ( !playerTarget.isMonster()
            || ( playerTarget as L2MonsterInstance ).isRaid()
            || playerTarget.isChest() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_TARGET_IS_UNAVAILABLE_FOR_SEEDING ) )
        return false
    }

    let monster = playerTarget as L2MonsterInstance
    if ( monster.isDead() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INCORRECT_TARGET ) )
        return false
    }

    if ( monster.isSeeded() ) {
        character.sendOwnedData( ActionFailed() )
        return false
    }

    let seed: L2Seed = CastleManorManager.getSeed( item.getId() )
    if ( !seed ) {
        return false
    }

    if ( seed.getCastleId() !== CastleManager.getCastleIdByRespawn( character ) ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THIS_SEED_MAY_NOT_BE_SOWN_HERE ) )
        return false
    }

    let player: L2PcInstance = character as L2PcInstance
    monster.setSeeded( seed, player )

    // TODO : add ability to queue up skills for AIController
    if ( item.getItem().hasSkills() ) {
        await Promise.all( item.getItem().getSkills().map( skill => player.useMagic( skill, false, false ) ) )
    }

    return true
}