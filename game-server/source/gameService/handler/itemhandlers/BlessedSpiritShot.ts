import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2Playable } from '../../models/actor/L2Playable'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Weapon } from '../../models/items/L2Weapon'
import { ActionType } from '../../models/items/type/ActionType'
import { ShotType } from '../../enums/ShotType'
import { BroadcastHelper } from '../../helpers/BroadcastHelper'
import { MagicSkillUseWithCharacters } from '../../packets/send/MagicSkillUse'
import { Skill } from '../../models/Skill'

export async function BlessedSpiritShot( character: L2Playable, item: L2ItemInstance ) : Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    if ( !item.getItem().hasSkills() ) {
        return false
    }

    let player: L2PcInstance = character as L2PcInstance
    if ( player.isChargedShot( ShotType.BlessedSpiritshot ) ) {
        return false
    }

    let weapon: L2ItemInstance = player.getActiveWeaponInstance()
    let weaponItem: L2Weapon = player.getActiveWeaponItem()
    let skill: Skill = item.getItem().getSkills()[ 0 ]
    let itemId = item.getId()

    if ( !weapon || !weaponItem || ( weaponItem.getSpiritShotCount() === 0 ) ) {
        if ( !player.getAutoSoulShot().includes( itemId ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CANNOT_USE_SPIRITSHOTS ) )
        }
        return false
    }

    let gradeCheck = item.isEtcItem()
            && ( item.getEtcItem().getDefaultAction() === ActionType.SPIRITSHOT )
            && ( weapon.getItem().getItemGradeSPlus() === item.getItem().getItemGradeSPlus() )

    if ( !gradeCheck ) {
        if ( !player.getAutoSoulShot().includes( itemId ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.SPIRITSHOTS_GRADE_MISMATCH ) )
        }

        return false
    }

    let isItemConsumed = await player.destroyItemByObjectId( item.getObjectId(), weaponItem.getSpiritShotCount(), false )
    if ( !isItemConsumed ) {
        if ( !player.disableAutoShot( itemId ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_ENOUGH_SPIRITSHOTS ) )
        }

        return false
    }

    player.setChargedShot( ShotType.BlessedSpiritshot, true )

    let packet = new SystemMessageBuilder( SystemMessageIds.USE_S1_ )
            .addItemNameWithId( itemId )
            .getBuffer()
    player.sendOwnedData( packet )
    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ENABLED_SPIRITSHOT ) )

    BroadcastHelper.dataToSelfInRange( player, MagicSkillUseWithCharacters( player, player, skill.getId(), skill.getLevel(), 0, 0 ), 600 )
    return true
}