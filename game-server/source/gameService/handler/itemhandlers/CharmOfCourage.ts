import { L2Playable } from '../../models/actor/L2Playable'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { EtcStatusUpdate } from '../../packets/send/EtcStatusUpdate'
import { CrystalType } from '../../models/items/type/CrystalType'

export async function CharmOfCourage( character: L2Playable, item: L2ItemInstance ) : Promise<boolean> {
    if ( !character.isPlayer() ) {
        return false
    }

    let player: L2PcInstance = character as L2PcInstance

    let type : CrystalType
    let itemCrystalType = item.getItem().getItemGrade()

    if ( type < 20 ) {
        type = CrystalType.NONE
    } else if ( type < 40 ) {
        type = CrystalType.D
    } else if ( type < 52 ) {
        type = CrystalType.C
    } else if ( type < 61 ) {
        type = CrystalType.B
    } else if ( type < 76 ) {
        type = CrystalType.A
    } else {
        type = CrystalType.S
    }

    if ( itemCrystalType < type ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.S1_CANNOT_BE_USED )
                .addItemNameWithId( item.getId() )
                .getBuffer()
        player.sendOwnedData( packet )
        return false
    }

    let isItemConsumed = await player.destroyItemByObjectId( item.getObjectId(), 1, false )
    if ( isItemConsumed ) {
        player.setCharmOfCourage( true )
        player.sendDebouncedPacket( EtcStatusUpdate )
        return true
    }

    return false
}