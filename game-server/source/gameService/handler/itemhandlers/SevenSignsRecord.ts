import { L2Playable } from '../../models/actor/L2Playable'
import { SSQStatus } from '../../packets/send/SSQStatus'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

export async function SevenSignsRecord( character: L2Playable ): Promise<boolean> {
    if ( !character.isPlayer() ) {
        character.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ITEM_NOT_FOR_PETS ) )
        return false
    }

    character.sendOwnedData( await SSQStatus( character.getObjectId(), 1 ) )
    return true
}