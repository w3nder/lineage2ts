import { IActionShiftHandler } from '../IActionShiftHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { ILocational } from '../../models/Location'
import { DataManager } from '../../../data/manager'

function locationToString( location: ILocational ): string {
    return `X: ${ location.getX() } Y:${ location.getY() } Z: ${ location.getZ() } Heading: ${ location.getHeading() } InstanceId: ${ location.getInstanceId() }`
}

const path = 'overrides/html/actions/itemactionshift.htm'
export const L2ItemInstanceActionShift: IActionShiftHandler = {
    getInstanceType(): InstanceType {
        return InstanceType.L2ItemInstance
    },

    async onAction( player: L2PcInstance, target: L2Object ): Promise<boolean> {
        if ( player.isGM() ) {
            let html = DataManager.getHtmlData().getItem( path )
                                  .replace( /%objectId%/g, target.getObjectId().toString() )
                                  .replace( /%targetId%/g, target.getId().toString() )
                                  .replace( /%ownerId%/g, ( target as L2ItemInstance ).getOwnerId().toString() )
                                  .replace( /%location%/g, locationToString( target ) )
                                  .replace( /%class%/g, InstanceType[ target.getInstanceType() ] )
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), 0 ) )
        }

        return true
    },

}