import { IActionShiftHandler } from '../IActionShiftHandler'
import { InstanceType } from '../../enums/InstanceType'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { L2Npc } from '../../models/actor/L2Npc'
import { showGMStats, showStats } from '../bypasshandlers/viewNpc'
import { ConfigManager } from '../../../config/ConfigManager'

export const L2NpcActionShift: IActionShiftHandler = {
    getInstanceType(): InstanceType {
        return InstanceType.L2Npc
    },

    async onAction( player: L2PcInstance, target: L2Object ): Promise<boolean> {
        if ( player.isGM() ) {
            showGMStats( player, target )
            return true
        }

        if ( ConfigManager.customs.viewNpcEnabled() ) {
            if ( !target.isNpc() ) {
                return false
            }

            player.setTarget( target )
            showStats( player, target as L2Npc )
        }

        return true
    },
}