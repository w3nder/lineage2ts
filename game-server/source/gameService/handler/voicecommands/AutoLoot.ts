import { IVoicedCommand } from '../IVoicedCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { PlayerVariablesManager } from '../../variables/PlayerVariablesManager'
import { CommonVariables } from '../../values/L2PcValues'

export const AutoLoot: IVoicedCommand = {
    isEnabled(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'loot',
            'autoloot',
            'itemloot',
            'herbloot',
        ]
    },

    async onStart( command: string, player: L2PcInstance ): Promise<void> {
        if ( !ConfigManager.customs.autoLootVoiceCommand() ) {
            return
        }

        switch ( command ) {
            case 'loot':
                player.sendMessage( 'Using Voices Methods:\n.autoloot: Loot all item(s).\n.itemloot: Loot all better item(s).\n.herbloot: Loot recovery(s) herb(s).' )

                if ( player.isAutoLootEnabled() ) {
                    player.sendMessage( 'Auto Loot: enabled.' )
                }

                if ( player.shouldLootItem() ) {
                    player.sendMessage( 'Auto Loot Item: enabled.' )
                }

                if ( player.shouldLootHerb() ) {
                    player.sendMessage( 'Auto Loot Herbs: enabled.' )
                }

                return

            case 'autoloot':
                if ( !ConfigManager.character.autoLoot() ) {
                    if ( player.isAutoLootEnabled() ) {
                        PlayerVariablesManager.set( player.getObjectId(), CommonVariables.AutoLootEnabled, false )
                        player.sendMessage( 'Auto Loot: disabled' )
                        return
                    }

                    PlayerVariablesManager.set( player.getObjectId(), CommonVariables.AutoLootEnabled, true )
                    player.sendMessage( 'Auto Loot: enabled' )
                    return
                }

                player.sendMessage( 'Auto Loot : not enabled on server' )
                return

            case 'itemloot':
                if ( player.shouldLootItem() ) {
                    PlayerVariablesManager.set( player.getObjectId(), CommonVariables.AutoLootItemsEnabled, false )
                    player.sendMessage( 'Auto Loot Item: disabled' )
                    return
                }

                PlayerVariablesManager.set( player.getObjectId(), CommonVariables.AutoLootItemsEnabled, true )
                player.sendMessage( 'Auto Loot Item: enabled' )

                if ( player.isAutoLootEnabled() ) {
                    PlayerVariablesManager.set( player.getObjectId(), CommonVariables.AutoLootItemsEnabled, false )
                    player.sendMessage( 'Auto Loot Item has priority' )
                    player.sendMessage( 'Auto Loot: disabled' )
                }

                return

            case 'herbloot':
                if ( player.shouldLootHerb() ) {
                    PlayerVariablesManager.set( player.getObjectId(), CommonVariables.AutoLootHerbsEnabled, false )
                    player.sendMessage( 'Auto Loot Herbs: disabled' )

                    return
                }

                PlayerVariablesManager.set( player.getObjectId(), CommonVariables.AutoLootHerbsEnabled, true )
                player.sendMessage( 'Auto Loot Herbs: enabled' )

                return
        }
    },
}