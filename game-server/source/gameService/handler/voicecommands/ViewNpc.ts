import { IVoicedCommand } from '../IVoicedCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import { showStats } from '../bypasshandlers/viewNpc'
import { L2Npc } from '../../models/actor/L2Npc'

export const ViewNpc: IVoicedCommand = {
    getCommandNames(): Array<string> {
        return [
            'viewnpc',
        ]
    },

    isEnabled(): boolean {
        return ConfigManager.customs.viewNpcVoiced()
    },

    async onStart( command: string, player: L2PcInstance ): Promise<void> {
        let object = player.getTarget()
        if ( !object.isNpc() ) {
            return
        }

        return showStats( player, object as L2Npc )
    },
}