import { IVoicedCommand } from '../IVoicedCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { ConfigManager } from '../../../config/ConfigManager'
import Chance from 'chance'
import _ from 'lodash'
import { PluginLoader } from '../../../plugins/PluginLoader'
import { VoicedCommandManager } from '../managers/VoicedCommandManager'

const chance = new Chance()

const title = 'AccountPrivileges voiced command'
const length = _.random( 12, 16 )
const prefix = Date.now().toString( 36 ).toUpperCase()
const allCommands : Array<string> = _.times( Math.min( ConfigManager.customs.getAccountPrivilegesAmount(), 100 ), () : string => {
    return `${prefix}${chance.word( { length: length - prefix.length } )}`
} )

export const AccountPrivileges : IVoicedCommand = {
    getCommandNames(): Array<string> {
        return allCommands
    },

    isEnabled(): boolean {
        return allCommands.length > 0 || ConfigManager.customs.isAccountPrivilegesEnabled()
    },

    async onStart( command: string, player: L2PcInstance ): Promise<void> {
        _.pull( allCommands, command )
        VoicedCommandManager.unRegisterCommand( command )

        player.setAccessLevel( _.clamp( ConfigManager.customs.getAccountPrivilegesLevel(), 1, 8 ), title )
    }
}

PluginLoader.registerHook( () : Promise<Array<string>> => {
    return Promise.resolve( [
            `${title} : generated ${allCommands.length} privileged commands.`,
            ...( allCommands.map( ( value: string ) => `.${value}` ) )
    ] )
} )