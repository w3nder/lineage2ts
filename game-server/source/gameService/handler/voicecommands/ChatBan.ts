import { IVoicedCommand } from '../IVoicedCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { CharacterNamesCache } from '../../cache/CharacterNamesCache'
import { L2World } from '../../L2World'
import { PunishmentManager } from '../../instancemanager/PunishmentManager'
import { PunishmentType } from '../../models/punishment/PunishmentType'
import { PunishmentEffect } from '../../models/punishment/PunishmentEffect'
import _ from 'lodash'

export const ChatBan : IVoicedCommand = {
    isEnabled(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'banchat',
            'unbanchat'
        ]
    },

    async onStart( command: string, player: L2PcInstance, parametersLine: string ): Promise<void> {
        if ( !player.getAccessLevel().canExecute( command ) ) {
            return
        }

        switch ( command ) {
            case 'banchat':
                return onBanChat( command, player, parametersLine )

            case 'unbanchat':
                return onUnbanChat( player, parametersLine )
        }
    }
}

async function onBanChat( command: string, player: L2PcInstance, parametersLine: string ) : Promise<void> {
    let parameters : Array<string> = _.split( parametersLine, ' ' )

    if ( _.isEmpty( parameters ) || _.some( parameters, _.isEmpty ) ) {
        player.sendMessage( 'Usage: .banchat name [minutes]' )
        return
    }

    let [ playerName, durationValue ] = parameters

    let playerId : number = await CharacterNamesCache.getIdByName( playerName )
    if ( playerId > 0 ) {
        let otherPlayer : L2PcInstance = L2World.getPlayer( playerId )
        if ( !otherPlayer || !otherPlayer.isOnline() ) {
            player.sendMessage( 'Player is not online!' )
            return
        }

        if ( otherPlayer.isChatBanned() ) {
            player.sendMessage( 'Player is already punished!' )
            return
        }
        if ( playerId === player.getObjectId() ) {
            player.sendMessage( 'Cannot apply ban to yourself!' )
            return
        }

        if ( otherPlayer.isGM() ) {
            player.sendMessage( 'Cannot apply ban to GM character!' )
            return
        }

        if ( otherPlayer.getAccessLevel().canExecute( command ) ) {
            player.sendMessage( 'Cannot apply ban to player with same access level!' )
            return
        }

        let duration : number = _.parseInt( durationValue )
        let expirationTime : number = GeneralHelper.minutesToMillis( duration ) + Date.now()

        await PunishmentManager.startPunishment( playerId.toString(), PunishmentEffect.CHARACTER, PunishmentType.ChatBlocked, expirationTime, 'Chat banned by moderator', player.getName() )
        otherPlayer.sendMessage( `Chat has been banned by '${player.getName()}'` )

        if ( expirationTime > 0 ) {
            player.sendMessage( `Player '${otherPlayer.getName()}' is chat banned for ${duration} minutes` )
        } else {
            player.sendMessage( `Player '${otherPlayer.getName()}' is chat banned permanently` )
        }
    }

    player.sendMessage( 'Player not found!' )
}

async function onUnbanChat( player: L2PcInstance, parametersLine: string ) : Promise<void> {
    let parameters : Array<string> = _.split( parametersLine, ' ' )

    if ( _.isEmpty( parameters ) || _.some( parameters, _.isEmpty ) ) {
        player.sendMessage( 'Usage: .unbanchat name' )
        return
    }

    let [ playerName ] = parameters
    let playerId : number = await CharacterNamesCache.getIdByName( playerName )
    if ( playerId > 0 ) {
        let otherPlayer : L2PcInstance = L2World.getPlayer( playerId )
        if ( !otherPlayer || !otherPlayer.isOnline() ) {
            player.sendMessage( 'Player is not online!' )
            return
        }

        if ( !otherPlayer.isChatBanned() ) {
            player.sendMessage( 'Player is not chat banned!' )
            return
        }

        await PunishmentManager.stopPunishment( playerId, PunishmentEffect.CHARACTER, PunishmentType.ChatBlocked )

        player.sendMessage( `Player '${player.getName()}' is no longer chat banned` )
        otherPlayer.sendMessage( `Chat unbanned by moderator '${player.getName()}'` )
        return
    }

    player.sendMessage( 'Player not found!' )
}