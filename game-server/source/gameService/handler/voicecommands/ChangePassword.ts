import { IVoicedCommand } from '../IVoicedCommand'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { LoginServerService } from '../../../loginService/LoginServerService'
import _ from 'lodash'
import { ConfigManager } from '../../../config/ConfigManager'

export const ChangePassword: IVoicedCommand = {
    isEnabled(): boolean {
        return ConfigManager.customs.changePasswordEnabled()
    },

    getCommandNames(): Array<string> {
        return [
            'changepassword',
        ]
    },

    async onStart( command: string, player: L2PcInstance, parametersLine: string ): Promise<void> {
        if ( _.isEmpty( parametersLine ) ) {
            let path = 'overrides/html/command/changePassword/menu.htm'
            let html : string = DataManager.getHtmlData().getItem( path )
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId() ) )
            return
        }

        if ( !LoginServerService.isConnected() ) {
            player.sendMessage( 'Password change is not possible at this time. Try again later.' )
            return
        }

        let parameters : Array<string> = _.split( parametersLine, ' ' )

        if ( _.some( parameters, _.isEmpty ) ) {
            player.sendMessage( 'Invalid password data! You need to supply old and new passwords.' )
            return
        }

        let [ currentPassword, newPassword, newPasswordRepeated ] = parameters
        if ( newPassword !== newPasswordRepeated ) {
            player.sendMessage( 'New password fields must match' )
            return
        }

        if ( currentPassword === newPassword ) {
            player.sendMessage( 'New password must be different than your current password!' )
            return
        }

        if ( newPassword.length < 6 ) {
            player.sendMessage( 'New password is shorter than six characters! Please try with a longer one.' )
            return
        }

        if ( newPassword.length > 16 ) {
            player.sendMessage( 'The new password is longer than 16 characters! Please try with a shorter one.' )
            return
        }

        return LoginServerService.sendChangePassword( player.getAccountName(), currentPassword, newPassword )
    },

}