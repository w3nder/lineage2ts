import { IParseBoardHandler } from './IParseBoardHandler'
import { L2PcInstance } from '../models/actor/instance/L2PcInstance'

export interface IWriteBoardHandler extends IParseBoardHandler{
    writeCommand( player: L2PcInstance, values: Array<string> )
}