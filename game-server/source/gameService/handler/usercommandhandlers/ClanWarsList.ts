import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Clan } from '../../models/L2Clan'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { DatabaseManager } from '../../../database/manager'
import { L2ClanWarsMixedData } from '../../../database/interface/ClanWarsTableApi'

type MixedDataMethod = ( id: number ) => Promise<L2ClanWarsMixedData>

export const ClanWarsList: IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [
            88,
            89,
            90,
        ]
    },

    async onStart( player: L2PcInstance, commandId: number ): Promise<boolean> {
        let clan: L2Clan = player.getClan()
        if ( !clan ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOT_JOINED_IN_ANY_CLAN ) )
            return false
        }

        let dataMethod: MixedDataMethod = getDataMethod( player, commandId )

        if ( !dataMethod ) {
            return false
        }

        let item: L2ClanWarsMixedData = await dataMethod( clan.getId() )
        if ( item.allyName ) {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_S2_ALLIANCE )
                    .addString( item.clanName )
                    .addString( item.allyName )
                    .getBuffer()
            player.sendOwnedData( packet )
        } else {
            let packet = new SystemMessageBuilder( SystemMessageIds.S1_NO_ALLI_EXISTS )
                    .addString( item.clanName )
                    .getBuffer()
            player.sendOwnedData( packet )
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FRIEND_LIST_FOOTER ) )

        return true
    },
}

function getDataMethod( player: L2PcInstance, commandId: number ): MixedDataMethod {
    switch ( commandId ) {
        case 88:
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLANS_YOU_DECLARED_WAR_ON ) )
            return DatabaseManager.getClanWarsTable().getAttackList
        case 89:
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.CLANS_THAT_HAVE_DECLARED_WAR_ON_YOU ) )
            return DatabaseManager.getClanWarsTable().getUnderAttackList
        case 90:
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WAR_LIST ) )
            return DatabaseManager.getClanWarsTable().getWarList
    }

    return null
}