import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { Olympiad } from '../../cache/Olympiad'

export const OlympiadStat : IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 109 ]
    },

    async onStart( player: L2PcInstance ): Promise<boolean> {
        let objectId : number = player.getObjectId()
        let target : L2Object = player.getTarget()
        if ( target ) {
            if ( target.isPlayer() && target.getActingPlayer().isNoble() ) {
                objectId = target.getObjectId()
            } else {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOBLESSE_ONLY ) )
                return false
            }
        } else if ( !player.isNoble() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NOBLESSE_ONLY ) )
            return false
        }

        let recordsMessage = new SystemMessageBuilder( SystemMessageIds.THE_CURRENT_RECORD_FOR_THIS_OLYMPIAD_SESSION_IS_S1_MATCHES_S2_WINS_S3_DEFEATS_YOU_HAVE_EARNED_S4_OLYMPIAD_POINTS )
                .addNumber( Olympiad.getCompetitionDone( objectId ) )
                .addNumber( Olympiad.getCompetitionWon( objectId ) )
                .addNumber( Olympiad.getCompetitionLost( objectId ) )
                .addNumber( Olympiad.getNoblePoints( objectId ) )
                .getBuffer()
        player.sendOwnedData( recordsMessage )

        let matchesMessage = new SystemMessageBuilder( SystemMessageIds.YOU_HAVE_S1_MATCHES_REMAINING_THAT_YOU_CAN_PARTECIPATE_IN_THIS_WEEK_S2_CLASSED_S3_NON_CLASSED_S4_TEAM )
                .addNumber( Olympiad.getRemainingWeeklyMatches( objectId ) )
                .addNumber( Olympiad.getRemainingWeeklyMatchesClassed( objectId ) )
                .addNumber( Olympiad.getRemainingWeeklyMatchesNonClassed( objectId ) )
                .addNumber( Olympiad.getRemainingWeeklyMatchesTeam( objectId ) )
                .getBuffer()
        player.sendOwnedData( matchesMessage )

        return true
    }
}