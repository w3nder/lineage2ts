import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'

export const Mount: IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 61 ]
    },

    async onStart( player: L2PcInstance ): Promise<boolean> {

        if ( player.hasSummon() && await player.mountSummon( player.getSummon() ) ) {
            return true
        }

        return false
    },
}