import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { SiegeManager } from '../../instancemanager/SiegeManager'
import { Siege } from '../../models/entity/Siege'
import { L2Clan } from '../../models/L2Clan'
import { L2World } from '../../L2World'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../data/manager'

const memberTemplate = '<tr><td width=170>#name#</td><td width=100>#status#</td></tr>'
const insideZoneMessage = 'Castle Siege in Progress'
const outsideZoneMessage = 'No Castle Siege Area'

export const SiegeStatus: IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 99 ]
    },

    async onStart( player: L2PcInstance ): Promise<boolean> {
        if ( !player.isNoble() || !player.isClanLeader() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_NOBLESSE_LEADER_CAN_VIEW_SIEGE_STATUS_WINDOW ) )
            return false
        }

        let clan: L2Clan = player.getClan()
        let validSiege: Siege = SiegeManager.getSieges().find( ( siege: Siege ) => {
            return siege.isInProgress() && ( siege.checkIsAttacker( clan ) || siege.checkIsDefender( clan ) )
        } )

        if ( !validSiege ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_NOBLESSE_LEADER_CAN_VIEW_SIEGE_STATUS_WINDOW ) )
            return false
        }

        let siegeArea = validSiege.getCastle().siegeArea
        let memberList: Array<string> = []
        clan.getOnlineMembers().forEach( ( objectId: number ) => {
            let member: L2PcInstance = L2World.getPlayer( objectId )
            if ( member ) {
                memberList.push( memberTemplate
                        .replace( '#name#', member.getName() )
                        .replace( '#status#', siegeArea.isObjectInside( member ) ? insideZoneMessage : outsideZoneMessage ),
                )
            }
        } )

        let path = 'data/html/siege/siege_status.htm'
        let html: string = DataManager.getHtmlData().getItem( path )
                .replace( /%kill_count%/, clan.getSiegeKills().toString() )
                .replace( /%death_count%/, clan.getSiegeDeaths().toString() )
                .replace( /%member_list%/, memberList.join( '' ) )
        player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId() ) )

        return true
    },

}