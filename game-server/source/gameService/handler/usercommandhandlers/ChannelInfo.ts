import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2CommandChannel } from '../../models/L2CommandChannel'
import { ExMultiPartyCommandChannelInfo } from '../../packets/send/ExMultiPartyCommandChannelInfo'

export const ChannelInfo : IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 97 ]
    },

    async onStart( player: L2PcInstance, commandId: number ): Promise<boolean> {
        if ( !player.getParty() || !player.getParty().getCommandChannel() ) {
            return false
        }

        let channel : L2CommandChannel = player.getParty().getCommandChannel()
        player.sendOwnedData( ExMultiPartyCommandChannelInfo( channel ) )
        return true
    }

}