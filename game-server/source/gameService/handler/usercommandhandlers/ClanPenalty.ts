import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { NpcHtmlMessage, NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import moment from 'moment'

const template: string = `
<html><body><center>
<table width=270 border=0 bgcolor=111111>
<tr><td width=170>Penalty</td><td width=100 align=center>Expiration Date</td></tr>
</table>
<table width=270 border=0><tr>#content#</tr></table>
<img src="L2UI.SquareWhite" width=270 height=1>
</center></body></html>
`

export const ClanPenalty: IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 100 ]
    },

    async onStart( player: L2PcInstance ): Promise<boolean> {
        let penalty = false
        let currentTime = Date.now()
        let chunks: Array<string> = []

        let currentDate = moment().format( 'YYYY-MM-DD' )

        if ( player.getClanJoinExpiryTime() > currentTime ) {
            chunks.push( `<td width=170>Unable to join a clan.</td><td width=100 align=center>${ currentDate }</td>` )
            penalty = true
        }

        if ( player.getClanCreateExpiryTime() > currentTime ) {
            chunks.push( `<td width=170>Unable to create a clan.</td><td width=100 align=center>${ currentDate }</td>` )
            penalty = true
        }

        if ( player.getClan() && player.getClan().getCharacterPenaltyExpiryTime() > currentTime ) {
            chunks.push( `<td width=170>Unable to invite a clan member.</td><td width=100 align=center>${ currentDate }</td>` )
            penalty = true
        }

        if ( !penalty ) {
            chunks.push( '<td width=170>No penalty is imposed.</td><td width=100 align=center></td>' )
        }

        let html: string = template.replace( '#content#', chunks.join( '' ) )
        player.sendOwnedData( NpcHtmlMessagePath( html, null, player.getObjectId() ) )

        return true
    },
}