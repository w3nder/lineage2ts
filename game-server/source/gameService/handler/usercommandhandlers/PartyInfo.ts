import { IUserCommandHandler } from '../IUserCommandHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2Party } from '../../models/L2Party'
import { PartyDistributionType } from '../../enums/PartyDistributionType'

export const PartyInfo: IUserCommandHandler = {
    getCommandIds(): Array<number> {
        return [ 81 ]
    },

    async onStart( player: L2PcInstance ): Promise<boolean> {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.PARTY_INFORMATION ) )

        if ( player.isInParty() ) {
            let party: L2Party = player.getParty()
            switch ( party.getDistributionType() ) {
                case PartyDistributionType.FindersKeepers:
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LOOTING_FINDERS_KEEPERS ) )
                    break
                case PartyDistributionType.Random:
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LOOTING_RANDOM ) )
                    break
                case PartyDistributionType.RandomIncludingSpoil:
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LOOTING_RANDOM_INCLUDE_SPOIL ) )
                    break
                case PartyDistributionType.ByTurn:
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LOOTING_BY_TURN ) )
                    break
                case PartyDistributionType.ByTurnIncludingSpoil:
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.LOOTING_BY_TURN_INCLUDE_SPOIL ) )
                    break
            }

            if ( !party.isLeader( player ) ) {
                let packet = new SystemMessageBuilder( SystemMessageIds.PARTY_LEADER_C1 )
                        .addPlayerCharacterName( party.getLeader() )
                        .getBuffer()
                player.sendOwnedData( packet )
            }

            player.sendMessage( `Member count: ${ party.getMemberCount() } of 9` )
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FRIEND_LIST_FOOTER ) )
        return true
    },

}