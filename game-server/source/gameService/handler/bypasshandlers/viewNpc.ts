import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { L2World } from '../../L2World'
import { L2Npc } from '../../models/actor/L2Npc'
import _ from 'lodash'
import { DataManager } from '../../../data/manager'
import { HtmlComposer } from '../../helpers/HtmlComposer'
import { WeaponType } from '../../models/items/type/WeaponType'
import { ElementalsHelper } from '../../models/Elementals'
import { NpcHtmlMessage, NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { GeneralHelper } from '../../helpers/GeneralHelper'
import { IDropItem } from '../../models/drops/IDropItem'
import { GeneralDropItem } from '../../models/drops/GeneralDropItem'
import { GroupedGeneralDropItem } from '../../models/drops/GroupedGeneralDropItem'
import { Formulas } from '../../models/stats/Formulas'
import { InstanceType } from '../../enums/InstanceType'
import { L2Attackable } from '../../models/actor/L2Attackable'
import { Race } from '../../enums/Race'
import { AIIntent } from '../../aicontroller/enums/AIIntent'
import { TraitRunnerAIController } from '../../aicontroller/types/TraitRunnerAIController'
import { AIType } from '../../enums/AIType'
import { ConfigManager } from '../../../config/ConfigManager'
import { AISkillUsageBehavior, AISkillUsageType } from '../../enums/AISkillUsage'
import { Skill } from '../../models/Skill'
import { ISpawnLogic } from '../../models/spawns/ISpawnLogic'
import { DefenceElementTypes, ElementalType } from '../../enums/ElementalType'
import { DropListScope } from '../../enums/drops/DropListScope'
import { createButton } from '../admincommands/helpers/AdminHtml'
import { createRouteViewBypass } from '../admincommands/Routes'
import { createGeometryInfoBypass } from '../admincommands/Geometry'
import { GeometryId } from '../../enums/GeometryId'

const enum ViewNpcCommands {
    View = 'viewNpc'
}

const dropsPerPage: number = 10

const enum ViewOperations {
    stats = 'stats',
    drops = 'drops',
    gmStats = 'gmStats',
    gmSkills = 'gmSkills'
}

const aiIntentTemplate = `
<tr><td><table width=270 border=0 bgcolor=131210><tr><td width=100><font color=FFAA00>AI Intent:</font></td>
<td align=right width=170>%intention%</td></tr></table></td></tr>
`

const aiTraitTemplate = `
<tr><td><table width=270 border=0><tr><td width=100><font color=FFAA00>AI Trait:</font></td>
<td align=right width=170>%name%</td></tr></table></td></tr>`

const aiTypeTemplate = `
<tr><td><table width=270 border=0 bgcolor=131210><tr><td width=100><font color=FFAA00>Npc AI Type</font></td>
<td align=right width=170>%type%</td></tr></table></td></tr>
`

const aiClanTemlate = `
<tr><td><table width=270 border=0><tr><td width=100><font color=FFAA00>Clan & Range:</font></td>
<td align=right width=170>%clans% & %range%</td></tr></table></td></tr>
`

const aiEnemyClanTemplate = `
<tr><td><table width=270 border=0 bgcolor=131210><tr><td width=100><font color=FFAA00>Ignore & Range:</font></td>
<td align=right width=170>%clans% & %range%</td></tr></table></td></tr>
`

const routeNameTemplate = `
<tr><td><table width=270 border=0><tr><td width=100><font color=LEVEL>Route:</font></td>
<td align=right width=170>%name%</td></tr></table></td></tr>
`

export const ViewNpc: IBypassHandler = {
    hasRangeValidation(): boolean {
        return false
    },

    getCommandNames(): Array<string> {
        return [
            ViewNpcCommands.View,
        ]
    },

    async onStart( command: string, player: L2PcInstance ): Promise<boolean> {
        let commandChunks: Array<string> = _.split( command, ' ' )

        if ( commandChunks.length < 2 ) {
            return false
        }

        let subCommand = commandChunks[ 1 ]
        let target: L2Object
        let npc: L2Npc

        switch ( subCommand ) {
            case ViewOperations.stats:

                if ( commandChunks.length > 2 ) {
                    target = L2World.getObjectById( _.parseInt( commandChunks[ 2 ] ) )
                } else {
                    target = player.getTarget()
                }

                npc = target && target.isNpc() ? target as L2Npc : null
                if ( !npc ) {
                    return false
                }

                showStats( player, npc )
                break

            case ViewOperations.drops:
                let dropListValue = commandChunks[ 2 ]
                let dropListScope: DropListScope = DropListScope[ dropListValue ]

                target = L2World.getObjectById( _.parseInt( _.nth( commandChunks, 3 ) ) )
                npc = target && target.isNpc() ? target as L2Npc : null

                if ( !npc ) {
                    return false
                }

                let page = _.defaultTo( _.parseInt( _.nth( commandChunks, 4 ) ), 0 )
                showDrops( player, npc, dropListScope, page )
                break

            case ViewOperations.gmStats:
                if ( !player.isGM() ) {
                    return false
                }

                if ( commandChunks.length > 2 ) {
                    target = L2World.getObjectById( _.parseInt( commandChunks[ 2 ] ) )
                } else {
                    target = player.getTarget()
                }

                npc = target && target.isNpc() ? target as L2Npc : null
                if ( !npc ) {
                    return false
                }

                showGMStats( player, npc )
                break

            case ViewOperations.gmSkills:
                if ( !player.isGM() ) {
                    return false
                }

                if ( commandChunks.length > 2 ) {
                    target = L2World.getObjectById( _.parseInt( commandChunks[ 2 ] ) )
                } else {
                    target = player.getTarget()
                }

                npc = target && target.isNpc() ? target as L2Npc : null
                if ( !npc ) {
                    return false
                }

                showGMSkills( player, npc )
                break
        }

        return true
    },
}

export function createGmStatsBypass( objectId: number ): string {
    return `bypass -h ${ ViewNpcCommands.View } ${ ViewOperations.gmStats } ${objectId}`
}

function generateSkillUsageButton( objectId: number ) : string {
    return `<button value="View Usage" action="bypass viewnpc ${ ViewOperations.gmSkills } ${objectId}" width=80 height=21 back="L2UI_CT1.Button_DF_Down" fore="L2UI_CT1.Button_DF">`
}

function generateDroplistMenuHtml( npc: L2Npc ): string {
    const drops: Array<string> = _.reduce( [ DropListScope.DEATH, DropListScope.CORPSE ], ( lines: Array<string>, scope: DropListScope ): Array<string> => {
        if ( npc.getTemplate().dropLists[ scope ] ) {
            let name: string = scope === DropListScope.CORPSE ? 'Spoil' : 'Drop'
            lines.push( `<td align=center><button value="Show ${ name }" width=100 height=25 action="bypass ${ ViewNpcCommands.View } ${ ViewOperations.drops } ${ DropListScope[ scope ] } ${ npc.getObjectId() }" back="L2UI_CT1.Button_DF_Calculator_Down" fore="L2UI_CT1.Button_DF_Calculator"/></td>` )
        }
        return lines
    }, [] )

    return drops.length > 0 ? `<table width=275 cellpadding=0 cellspacing=0><tr>${ drops.join( '' ) }</tr></table>` : '<center><font name="ScreenMessageSmall" color="CD9000">No Spoil/Drop items possible</font></center>'
}

export function showStats( player: L2PcInstance, npc: L2Npc ): void {
    let path = 'overrides/html/command/viewNpc/stats.htm'
    let html = DataManager.getHtmlData().getItem( path )
                          .replace( '%name%', npc.getName() )
                          .replace( '%hpGauge%', HtmlComposer.createHpGauge( 250, npc.getCurrentHp(), npc.getMaxHp(), false ) )
                          .replace( '%mpGauge%', HtmlComposer.createMpGauge( 250, npc.getCurrentMp(), npc.getMaxMp(), false ) )
                          .replace( '%atktype%', _.capitalize( WeaponType[ npc.getAttackType() ] ) )

                          .replace( '%atkrange%', npc.getStat().getPhysicalAttackRange().toString() )
                          .replace( '%patk%', formatNumber( npc.getPowerAttack( player ) ) )
                          .replace( '%pdef%', formatNumber( npc.getPowerDefence( player ) ) )
                          .replace( '%matk%', formatNumber( npc.getMagicAttack( player, null ) ) )

                          .replace( '%mdef%', formatNumber( npc.getMagicDefence( player, null ) ) )
                          .replace( '%atkspd%', npc.getPowerAttackSpeed().toString() )
                          .replace( '%castspd%', npc.getMagicAttackSpeed().toString() )
                          .replace( '%critrate%', npc.getStat().getCriticalHit( player, null ).toString() )

                          .replace( '%evasion%', formatNumber( npc.getEvasionRate( player ) ) )
                          .replace( '%accuracy%', formatNumber( npc.getStat().getAccuracy() ) )
                          .replace( '%speed%', formatNumber( npc.getStat().getMoveSpeed() ) )
                          .replace( '%attributeatktype%', ElementalsHelper.getElementName( npc.getStat().getAttackElement() ) )

                          .replace( '%attributeatkvalue%', npc.getStat().getAttackElementValue( npc.getStat().getAttackElement() ).toString() )
                          .replace( '%attributefire%', npc.getStat().getDefenseElementValue( ElementalType.FIRE ).toString() )
                          .replace( '%attributewater%', npc.getStat().getDefenseElementValue( ElementalType.WATER ).toString() )
                          .replace( '%attributewind%', npc.getStat().getDefenseElementValue( ElementalType.WIND ).toString() )

                          .replace( '%attributeearth%', npc.getStat().getDefenseElementValue( ElementalType.EARTH ).toString() )
                          .replace( '%attributedark%', npc.getStat().getDefenseElementValue( ElementalType.DARK ).toString() )
                          .replace( '%attributeholy%', npc.getStat().getDefenseElementValue( ElementalType.HOLY ).toString() )
                          .replace( '%dropsMenu%', player.isGM() || ConfigManager.customs.viewNpcItems() ? generateDroplistMenuHtml( npc ) : '' )

                          .replace( '%hpRegeneration%', formatNumber( Formulas.calculateHpRegeneration( npc ) ) )
                          .replace( '%mpRegeneration%', formatNumber( Formulas.calculateMpRegeneration( npc ) ) )

    let npcSpawn: ISpawnLogic = npc.getNpcSpawn()
    let spawnData = npcSpawn?.getSpawnData()
    if ( !npcSpawn || !spawnData || spawnData.respawnMs === 0 ) {
        html = html.replace( '%respawn%', 'None' )
    } else {
        if ( spawnData.respawnExtraMs ) {
            html = html.replace( '%respawn%', `between ${ GeneralHelper.formatSeconds( spawnData.respawnMs / 1000 ).join( ', ' ) } - ${ GeneralHelper.formatSeconds( ( spawnData.respawnMs + spawnData.respawnExtraMs ) / 1000 ).join( ', ' ) }` )
        } else {
            html = html.replace( '%respawn%', GeneralHelper.formatSeconds( spawnData.respawnMs / 1000 ).join( ', ' ) )
        }
    }

    return player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
}

function createHeader( page: number, scope: DropListScope, objectId: number ): string {
    if ( page === 0 ) {
        return ''
    }

    let buttons: string = _.times( page, ( index: number ) => {
        return `<td align=center><button value="${ index + 1 }" width=20 height=20 action="bypass ${ ViewNpcCommands.View } ${ ViewOperations.drops } ${ DropListScope[ scope ] } ${ objectId } ${ index }" back="L2UI_CT1.Button_DF_Calculator_Down" fore="L2UI_CT1.Button_DF_Calculator"></td>`
    } ).join( '' )

    return `<table><tr>${ buttons }</tr></table>`
}

function getAmountValue( dropChance: number, minAmount: number, maxAmount: number, isePrecise: boolean ): string {
    let min: number, max: number

    if ( !isePrecise || ( dropChance <= 100 ) ) {
        min = minAmount
        max = maxAmount
    } else {
        let multiplier = Math.floor( dropChance / 100 )
        min = multiplier * minAmount
        max = ( dropChance % 100 ) > 0 ? ( multiplier + 1 ) * maxAmount : multiplier * maxAmount
    }

    min = Math.floor( min )
    max = Math.floor( max )

    if ( min === max ) {
        return GeneralHelper.getAdenaFormat( min )
    }

    return `${ GeneralHelper.getAdenaFormat( min ) } - ${ GeneralHelper.getAdenaFormat( max ) }`
}

function createNormalHtml( drop: GeneralDropItem, player: L2PcInstance, npc: L2Npc ): string {
    let template = DataManager.getItems().getTemplate( drop.getItemId() )

    return DataManager.getHtmlData().getItem( 'overrides/html/command/viewNpc/normalDropTemplate.html' )
                      .replace( '%icon%', template.icon )
                      .replace( '%name%', template.name )
                      .replace( '%amount%', getAmountValue( drop.getAttackerChanceModifier( npc, player ), drop.min, drop.max, drop.isPreciseCalculated() ) )
                      .replace( '%chance%', formatNumber( Math.min( 100, drop.getTargetAttackerChance( npc, player ) ) ) )
}

function createNormalizedItemsHtml( drop: GroupedGeneralDropItem ): string {
    return drop.getItems().map( ( currentDrop: GeneralDropItem ): string => {
        let template = DataManager.getItems().getTemplate( currentDrop.itemId )
        return DataManager.getHtmlData().getItem( 'overrides/html/command/viewNpc/normalDropTemplate.html' )
                          .replace( '%icon%', template.icon ? template.icon : 'icon.etc_question_mark_i00' )
                          .replace( '%name%', template.getName() )
                          .replace( '%amount%', getAmountValue( currentDrop.getChance(), currentDrop.min, currentDrop.max, currentDrop.isPreciseCalculated() ) )
                          .replace( '%chance%', formatNumber( Math.min( 100, currentDrop.getChance() ) ) )
    } ).join( '' )
}

function createGroupedHtml( drop: GroupedGeneralDropItem, player: L2PcInstance, npc: L2Npc ): string {
    if ( drop.getItems().length === 1 ) {
        let item: GeneralDropItem = drop.getItems()[ 0 ]
        let clonedItem = new GeneralDropItem(
                item.itemId,
                item.min,
                item.max,
                item.getChance() * drop.getChance() / 100,
                item.amountMultiplierStrategy,
                item.chanceMultiplierStrategy,
                drop.preciseStrategy,
                drop.attackerStrategy )

        return createNormalHtml( clonedItem, player, npc )
    }

    return DataManager.getHtmlData().getItem( 'overrides/html/command/viewNpc/groupedDropTemplate.html' )
                      .replace( '%chance%', formatNumber( Math.min( 100, drop.getChance() ) ) )
                      .replace( '%items%', createNormalizedItemsHtml( drop ) )
                      .replace( '%groupName%', `One from ${ drop.getItems().length } items` )
}

function createDropHtml( player: L2PcInstance, npc: L2Npc, dropPages: Array<IDropItem> ): string {
    let leftSide: Array<string> = []
    let rightSide: Array<string> = []

    _.each( dropPages, ( drop: IDropItem, index: number ): void => {
        let html: string = drop.isGroupItem() ? createGroupedHtml( drop as GroupedGeneralDropItem, player, npc ) : createNormalHtml( drop as GeneralDropItem, player, npc )
        if ( index % 2 === 0 ) {
            leftSide.push( html )
        } else {
            rightSide.push( html )
        }
    } )

    return `<table><tr><td>${ leftSide.join( '' ) }</td><td>${ rightSide.join( '' ) }</td></tr></table>`
}

export function showDrops( player: L2PcInstance, npc: L2Npc, scope: DropListScope, page: number ) {
    let drops: Array<IDropItem> = npc.getTemplate().dropLists[ scope ]
    if ( _.isEmpty( drops ) ) {
        return player.sendMessage( `No possible drops for ${DropListScope[ scope ]}` )
    }

    let dropPages: Array<Array<IDropItem>> = _.chunk( drops, dropsPerPage )
    let currentPageDrops = dropPages[ page ]
    if ( !currentPageDrops ) {
        currentPageDrops = _.last( dropPages )
    }

    let html = DataManager.getHtmlData().getItem( 'overrides/html/command/viewNpc/items.htm' )
                          .replace( '%name%', npc.getName() )
                          .replace( '%dropsMenu%', player.isGM() || ConfigManager.customs.viewNpcItems() ? generateDroplistMenuHtml( npc ) : '' )
                          .replace( '%pages%', createHeader( page, scope, npc.getObjectId() ) )
                          .replace( '%items%', createDropHtml( player, npc, currentPageDrops ) )
                          .replace( '%type%', scope === DropListScope.DEATH ? 'Drops' : 'Spoils' )

    GeneralHelper.sendCommunityBoardHtml( player, html, null, npc.getObjectId() )
}

export function formatColoredStat( value: number ): string {
    if ( value >= 40 ) {
        return `<font color=FF5251>${ value }</font>`
    }

    if ( value >= 30 ) {
        return `<font color=FFEB12>${ value }</font>`
    }

    if ( value >= 20 ) {
        return `<font color=25FBFF>${ value }</font>`
    }

    return `<font color=4AFF56>${ value }</font>`
}

export function formatColoredElement( value: number, minimumValue: number ): string {
    let computedValue = value - minimumValue

    if ( computedValue >= 50 ) {
        return `<font color=FF5251>${ value }</font>`
    }

    if ( computedValue >= 40 ) {
        return `<font color=FF9E68>${ value }</font>`
    }

    if ( computedValue >= 30 ) {
        return `<font color=FFEB12>${ value }</font>`
    }

    if ( computedValue >= 20 ) {
        return `<font color=FF9DE1>${ value }</font>`
    }

    if ( computedValue >= 10 ) {
        return `<font color=25FBFF>${ value }</font>`
    }

    return `<font color=4AFF56>${ value }</font>`
}

export function formatNumber( value: number ): string {
    if ( Number.isInteger( value ) ) {
        return GeneralHelper.getAdenaFormat( value )
    }

    return value.toFixed( 3 )
}

export function getPrettyObjectId( value: number ) : string {
    return _.map( _.chunk( value.toString( 16 ).toUpperCase(), 4 ), ( letters: Array<string> ) => letters.join( '' ) ).join( ' ' )
}

function getCoordinates( x: number, y: number, z: number ) : string {
    return `${ x } ${ y } ${ z }`
}

function hasAISkills( npc: L2Npc ) : boolean {
    let usage = npc.getTemplate().aiSkillUsage
    return usage && usage.ai !== AISkillUsageBehavior.None
}

export function showGMStats( player: L2PcInstance, target: L2Object ): void {
    player.setTarget( target )

    let npc = target as L2Npc
    let aggroRange: string = npc.isAttackable() && npc.isAggressive() ? ( target as L2Attackable ).getAggroRange().toString() : 'None'
    let attackAttribute = npc.getAttackElement()
    let prettyObjectId: string = _.map( _.chunk( npc.getObjectId().toString( 16 ).toUpperCase(), 4 ), ( letters: Array<string> ) => letters.join( '' ) ).join( ' ' )

    let defenceElements: Array<number> = DefenceElementTypes.map( ( type: ElementalType ): number => npc.getDefenceElementValue( type ) )
    let minDefenceElementValue = _.min( defenceElements )

    let htmlPath = 'overrides/html/command/viewNpc/gmStats.htm'
    let html = DataManager.getHtmlData().getItem( htmlPath )
        .replace( /%objectId%/g, target.getObjectId().toString() )
        .replace( /%class%/g, InstanceType[ target.getInstanceType() ] )
        .replace( /%race%/g, Race[ npc.getTemplate().getRace() ] )
        .replace( /%templateId%/g, npc.getTemplate().getId().toString() )

        .replace( /%level%/g, npc.getTemplate().getLevel().toString() )
        .replace( /%name%/g, npc.getTemplate().getName() )
        .replace( /%npcId%/g, npc.getTemplate().getId().toString() )
        .replace( /%aggro%/g, aggroRange.toString() )

        // TODO : add percentage for hp/mp values
        .replace( /%hp%/g, Math.floor( npc.getCurrentHp() ).toString() )
        .replace( /%hpmax%/g, npc.getMaxHp().toString() )
        .replace( /%mp%/g, Math.floor( npc.getCurrentMp() ).toString() )
        .replace( /%mpmax%/g, npc.getMaxMp().toString() )

        .replace( /%patk%/g, formatNumber( npc.getPowerAttack( null ) ) )
        .replace( /%matk%/g, formatNumber( npc.getMagicAttack( null, null ) ) )
        .replace( /%pdef%/g, formatNumber( Math.floor( npc.getPowerDefence( null ) ) ) )
        .replace( /%mdef%/g, formatNumber( Math.floor( npc.getMagicDefence( null, null ) ) ) )

        .replace( /%accu%/g, formatNumber( npc.getAccuracy() ) )
        .replace( /%evas%/g, formatNumber( npc.getEvasionRate( null ) ) )
        .replace( /%crit%/g, formatNumber( npc.getCriticalHit( null, null ) ) )
        .replace( /%rspd%/g, formatNumber( Math.floor( npc.getRunSpeed() ) ) )

        .replace( /%aspd%/g, formatNumber( npc.getPowerAttackSpeed() ) )
        .replace( /%cspd%/g, formatNumber( npc.getMagicAttackSpeed() ) )
        .replace( /%atkType%/g, WeaponType[ npc.getTemplate().getBaseAttackType().toString() ] )
        .replace( /%atkRng%/g, formatNumber( npc.getTemplate().getBaseAttackRange() ) )

        .replace( /%str%/g, formatColoredStat( npc.getSTR() ) )
        .replace( /%dex%/g, formatColoredStat( npc.getDEX() ) )
        .replace( /%con%/g, formatColoredStat( npc.getCON() ) )
        .replace( /%int%/g, formatColoredStat( npc.getINT() ) )

        .replace( /%wit%/g, formatColoredStat( npc.getWIT() ) )
        .replace( /%men%/g, formatColoredStat( npc.getMEN() ) )
        .replace( /%loc%/g, getCoordinates( target.getX(), target.getY(), target.getZ() ) )
        .replace( /%heading%/g, npc.getHeading().toString() )

        .replace( /%collision_radius%/g, formatNumber( npc.getTemplate().getFCollisionRadius() ) )
        .replace( /%collision_height%/g, formatNumber( npc.getTemplate().getFCollisionHeight() ) )
        .replace( /%distance%/g, formatNumber( Math.round( player.calculateDistance( target, true ) ) ) )
        .replace( /%ele_atk%/g, ElementalsHelper.getElementName( attackAttribute ) )

        .replace( /%ele_atk_value%/g, formatNumber( npc.getAttackElementValue( attackAttribute ) ) )
        .replace( /%ele_dfire%/g, formatColoredElement( defenceElements[ ElementalType.FIRE ], minDefenceElementValue ) )
        .replace( /%ele_dwater%/g, formatColoredElement( defenceElements[ ElementalType.WATER ], minDefenceElementValue ) )
        .replace( /%ele_dwind%/g, formatColoredElement( defenceElements[ ElementalType.WIND ], minDefenceElementValue ) )

        .replace( /%ele_dearth%/g, formatColoredElement( defenceElements[ ElementalType.EARTH ], minDefenceElementValue ) )
        .replace( /%ele_dholy%/g, formatColoredElement( defenceElements[ ElementalType.HOLY ], minDefenceElementValue ) )
        .replace( /%ele_ddark%/g, formatColoredElement( defenceElements[ ElementalType.DARK ], minDefenceElementValue ) )
        .replace( /%objectIdFormatted%/g, prettyObjectId )

        .replace( '%hpRegeneration%', formatNumber( Formulas.calculateHpRegeneration( npc ) ) )
        .replace( '%mpRegeneration%', formatNumber( Formulas.calculateMpRegeneration( npc ) ) )
        .replace( '%walkRoute%', npc.getWalkRoute() ? createButton( 'View', createRouteViewBypass( npc.getWalkRoute().route.name ) ) : 'None' )
        .replace( '%moveGeometry%', npc.getTemplate().moveGeometryId !== GeometryId.None ? createButton( 'View', createGeometryInfoBypass( npc.getTemplate().moveGeometryId ) ) : 'None' )

        .replace( '%dropGeometry%', npc.getTemplate().dropGeometryId !== GeometryId.None ? createButton( 'View', createGeometryInfoBypass( npc.getTemplate().dropGeometryId ) ) : 'None' )

    let spawn = npc.getNpcSpawn()
    if ( spawn ) {

        let spawnData = spawn.getSpawnData()
        let spawnLocation = npc.getSpawnLocation()

        if ( spawnData.position ) {
            let position = spawnData.position
            html = html.replace( /%spawntype%/g, 'Absolute Position' )
                       .replace( /%spawn%/g, getCoordinates( position.x, position.y, npc.getZ() ) )
        } else {
            html = html.replace( /%spawntype%/g, 'Random Territory Point' )
                       .replace( /%spawn%/g, getCoordinates( spawnLocation.getX(), spawnLocation.getY(), spawnLocation.getZ() ) )
        }

        // TODO : add button to view territory data: ids, min/max bounding rectangle and point generation properties
        html = html.replace( /%spawnDistance%/g, Math.round( player.calculateDistance( spawnLocation ) ).toString() )
                   .replace( /%territory%/g, spawnData.position ? 'None' : spawn.getMakerData().spawnTerritoryIds.length.toString() )

        if ( spawnData.respawnMs === 0 ) {
            html = html.replace( /%resp%/g, 'None' )
        } else if ( spawnData.respawnExtraMs ) {
            html = html.replace( /%resp%/g, `${ Math.floor( spawnData.respawnMs / 1000 ) }-${ Math.floor( spawnData.respawnExtraMs / 1000 ) } sec` )
        } else {
            html = html.replace( /%resp%/g, `${ Math.floor( spawnData.respawnMs / 1000 ) } sec` )
        }
    } else {
        html = html.replace( /%territory%/g, '<font color=FF0000>None</font>' )
                   .replace( /%spawntype%/g, '<font color=FF0000>None</font>' )
                   .replace( /%spawn%/g, '<font color=FF0000>None</font>' )
                   .replace( /%spawnDistance%/g, '<font color=FF0000>None</font>' )
                   .replace( /%resp%/g, '<font color=FF0000>None</font>' )
    }

    let clans = _.join( npc.getTemplate().getClans().map( DataManager.getNpcData().getClanName ), ', ' )
    let enemyClans = _.join( npc.getTemplate().getIgnoreClanNpcIds().map( DataManager.getNpcData().getClanName ), ', ' )

    html = html.replace( /%ai_intent%/g, aiIntentTemplate.replace( /%intention%/, npc.hasAIController() ? AIIntent[ npc.getAIController().currentIntent ] : 'Not Activated' ) )
               .replace( /%ai_trait%/g, aiTraitTemplate.replace( /%name%/, npc.hasAIController() ? ( npc.getAIController() as TraitRunnerAIController ).currentTrait.getName() : 'Not Available' ) )
               .replace( /%ai_type%/g, aiTypeTemplate.replace( /%type%/, AIType[ npc.getAIType() ] ) )
               .replace( /%ai_clan%/g, aiClanTemlate.replace( /%clans%/, clans ? clans : 'No clans' ).replace( /%range%/, npc.getTemplate().getClanHelpRange().toString() ) )
               .replace( /%ai_enemy_clan%/g, aiEnemyClanTemplate.replace( /%clans%/, enemyClans ? enemyClans : 'No ignore clans' ).replace( /%range%/, npc.getTemplate().getAggroRange().toString() ) )

    let route = npc.getWalkRoute()
    if ( route ) {
        html = html.replace( /%route%/g, routeNameTemplate.replace( /%name%/g, route.route.name ) )
    } else {
        html = html.replace( /%route%/g, '' )
    }

    html = html.replace( '%skillUsage%', hasAISkills( npc ) ? generateSkillUsageButton( npc.getObjectId() ) : 'No Active Skills' )

    player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId() ) )
}

function getSkillUsageBehavior( usage: AISkillUsageBehavior ) : string {
    switch ( usage ) {
        case AISkillUsageBehavior.Both:
            return 'Fighter and Mage'

        case AISkillUsageBehavior.Magical:
            return 'Mage'

        case AISkillUsageBehavior.Physical:
            return 'Fighter'
    }

    return 'Default'
}

function showGMSkills( player: L2PcInstance, npc: L2Npc ) : void {
    if ( !hasAISkills( npc ) ) {
        return player.sendMessage( `Npc "${ npc.getName()}" has no AI skills` )
    }

    let skillUsage = npc.getTemplate().aiSkillUsage
    let htmlPath = 'overrides/html/command/viewNpc/gmSkills.htm'
    let html = DataManager.getHtmlData().getItem( htmlPath )
                          .replace( '%usage%', getSkillUsageBehavior( skillUsage.ai ) )
                          .replace( /%level%/g, npc.getTemplate().getLevel().toString() )
                          .replace( /%name%/g, npc.getTemplate().getName() )
                          .replace( /%objectId%/g, npc.getObjectId().toString() )
                          .replace( /%templateId%/g, npc.getTemplate().getId().toString() )

    let skills : Array<string> = _.flatMap( skillUsage.available, ( currentSkills: Array<Skill>, key: string ) : Array<string> => {
        let type = AISkillUsageType[ parseInt( key, 10 ) ]
        return currentSkills.map( ( skill : Skill ) : string => {
            return `<tr><td>${skill.getName()} ( <font color=FFAA00>${skill.getId()}</font> )</td><td>Level ${skill.getLevel()}</td><td>${type}</td></tr>`
        } )
    } )

    html = html.replace( '%skills%', skills.join( '' ) )

    player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId() ) )
}