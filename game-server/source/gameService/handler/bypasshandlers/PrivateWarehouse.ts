import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { WareHouseDepositList } from '../../packets/send/WareHouseDepositList'
import { WarehouseDepositType } from '../../values/warehouseDepositType'
import { WarehouseListType } from '../../values/WarehouseListType'
import { SortedWareHouseWithdrawalList } from '../../packets/send/SortedWareHouseWithdrawalList'
import { getWarehouseWithdrawSortOrder, WarehouseWithdrawSort } from '../../values/SortedWareHouseWithdrawalListValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { WareHouseWithdrawalList } from '../../packets/send/WareHouseWithdrawalList'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../data/manager'
import _ from 'lodash'

export const PrivateWarehouse: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'withdrawp',
            'withdrawsortedp',
            'depositp',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator.isNpc() ) {
            return false
        }

        if ( player.isEnchanting() ) {
            return false
        }

        if ( command.toLowerCase().startsWith( 'withdrawp' ) ) {
            await player.initializeWarehouse()

            if ( ConfigManager.customs.enableWarehouseSortingPrivate() ) {
                let path = 'data/html/mods/WhSortedP.htm'
                let html: string = DataManager.getHtmlData().getItem( path )
                player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), originator.getObjectId() ) )
            } else {
                showWithdrawWindow( player, null, 0 )
            }

            return true
        }

        if ( command.toLowerCase().startsWith( 'withdrawsortedp' ) ) {
            let parameters: Array<string> = command.split( ' ' )
            await player.initializeWarehouse()

            if ( parameters.length > 2 ) {
                showWithdrawWindow( player, WarehouseListType[ parameters[ 1 ] ], getWarehouseWithdrawSortOrder( parameters[ 2 ] ) )
            } else if ( parameters.length > 1 ) {
                showWithdrawWindow( player, WarehouseListType[ parameters[ 1 ] ], WarehouseWithdrawSort.A2Z )
            } else {
                showWithdrawWindow( player, WarehouseListType.ALL, WarehouseWithdrawSort.A2Z )
            }

            return true
        }

        if ( command.toLowerCase().startsWith( 'depositp' ) ) {
            player.sendOwnedData( ActionFailed() )

            await player.initializeWarehouse()
            player.setActiveWarehouse( player.getWarehouse() )

            player.setInventoryBlockingStatus( true )
            player.sendOwnedData( WareHouseDepositList( player, WarehouseDepositType.Private ) )
            return true
        }

        return false
    },
}

function showWithdrawWindow( player: L2PcInstance, itemType: WarehouseListType, sortOrder: number ): void {
    player.sendOwnedData( ActionFailed() )
    player.setActiveWarehouse( player.getWarehouse() )

    if ( player.getActiveWarehouse().getSize() === 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_ITEM_DEPOSITED_IN_WH ) )
        return
    }

    if ( _.isNumber( itemType ) ) {
        player.sendOwnedData( SortedWareHouseWithdrawalList( player.getActiveWarehouse(), player.getAdena(), WarehouseDepositType.Private, itemType, sortOrder ) )
        return
    }

    player.sendOwnedData( WareHouseWithdrawalList( player.getActiveWarehouse(), WarehouseDepositType.Private, player.getAdena() ) )
}