import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { MultisellCache } from '../../cache/MultisellCache'
import { L2Npc } from '../../models/actor/L2Npc'
import _ from 'lodash'

export const Multisell: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'multisell',
            'exc_multisell',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator.isNpc() ) {
            return false
        }

        let normalizedCommand: string = command.toLowerCase()
        if ( normalizedCommand.startsWith( 'multisell' ) ) {
            let listId = _.parseInt( command.substring( 9 ).trim() )
            MultisellCache.separateAndSend( listId, player, originator as L2Npc, false )
            return true
        }

        if ( normalizedCommand.startsWith( 'exc_multisell' ) ) {
            let listId = _.parseInt( command.substring( 13 ).trim() )
            MultisellCache.separateAndSend( listId, player, originator as L2Npc, true )
            return true
        }

        return false
    },
}