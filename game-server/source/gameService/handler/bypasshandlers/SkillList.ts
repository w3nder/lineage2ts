import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { ConfigManager } from '../../../config/ConfigManager'
import { L2Npc } from '../../models/actor/L2Npc'
import { ClassId, IClassId } from '../../models/base/ClassId'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { NpcHtmlMessage, NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { SkillCache } from '../../cache/SkillCache'
import _ from 'lodash'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, NpcTeachesSkillsEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { L2SkillLearn } from '../../models/L2SkillLearn'
import { AcquireSkillList } from '../../packets/send/builder/AcquireSkillList'
import { AcquireSkillType } from '../../enums/AcquireSkillType'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { DataManager } from '../../../data/manager'

export const SkillList: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'SkillList',
            'learn_skill'
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator || !originator.isNpc() ) {
            return false
        }

        if ( ConfigManager.character.skillLearn() ) {
            showSkillList( player, originator as L2Npc, player.getClassId() )
            return true
        }

        showMessage( command, player, originator as L2Npc )

        return true
    },
}

function showMessage( command: string, player: L2PcInstance, npc: L2Npc ): void {
    let id = command.substring( 9 ).trim()
    if ( id.length !== 0 ) {
        return showSkillList( player, npc, _.parseInt( id ) )
    }

    let classesToTeach: Array<IClassId> = _.map( npc.getClassesToTeach(), ClassId.getClassIdByIdentifier )
    let parentClass = ClassId.getClassIdByIdentifier( player.getClassId() ).parent
    let ownClass = _.some( classesToTeach, ( classId: IClassId ) => {
        return ClassId.isChildOf( classId, parentClass )
    } )

    let items : Array<string> = []

    if ( !ownClass ) {
        let characterType = ClassId.getClassIdByIdentifier( player.getClassId() ).isMage ? 'fighter' : 'mage'
        items.push( `Skills of your class are the easiest to learn.<br>Skills of another class of your race are a little harder.<br>Skills for classes of another race are extremely difficult.<br>But the hardest of all to learn are the ${ characterType } skills!<br>` )
    }

    if ( classesToTeach.length > 0 ) {
        let foundClass: boolean = false
        let classCheck = ClassId.getClassIdByIdentifier( player.getClassId() )

        while ( !foundClass && _.isNumber( classCheck ) ) {
            classesToTeach.forEach( ( classId: IClassId ) => {
                if ( classId.level > classCheck.level ) {
                    return
                }

                if ( SkillCache.getAvailableSkills( player, classId.id, false, false, player ).length === 0 ) {
                    return
                }

                items.push( `<a action="bypass -h SkillList ${ classId.id }">Learn ${ ClassId.getName( classId ) }'s class Skills</a><br>\n` )
                foundClass = true
            } )

            classCheck = ClassId.getClassId( classCheck.parent )
        }
    } else {
        items.push( 'No Skills.<br>' )
    }

    let html = DataManager.getHtmlData().getItem( 'overrides/html/bypass/skillList/learn.html' )
        .replace( '%message%', items.join( '' ) )

    player.sendOwnedData( NpcHtmlMessage( html, player.getObjectId(), npc.getObjectId() ) )
    player.sendOwnedData( ActionFailed() )
}

export function showSkillList( player: L2PcInstance, npc: L2Npc, classId: number ): void {
    let npcId: number = npc.getTemplate().getId()

    if ( ListenerCache.hasNpcTemplateListeners( npcId, EventType.NpcTeachesSkills ) ) {
        let eventData = EventPoolCache.getData( EventType.NpcTeachesSkills ) as NpcTeachesSkillsEvent

        eventData.playerId = player.getObjectId()
        eventData.playerLevel = player.getLevel()
        eventData.npcId = npcId
        eventData.npcObjectId = npc.getObjectId()

        ListenerCache.sendNpcTemplateEvent( npcId, EventType.NpcTeachesSkills, eventData )
        return
    }

    if ( !npc.getTemplate().canTeach( ClassId.getClassIdByIdentifier( classId ) ) ) {
        npc.showNoTeachHtml( player )
        return
    }

    if ( npc.getClassesToTeach().length === 0 ) {
        let html = DataManager.getHtmlData().getItem( 'overrides/html/template/noSkillsToLearn.html' )
            .replace( '%npcId%', npcId.toString() )
            .replace( '%classId%', player.getClassId().toString() )
        player.sendOwnedData( NpcHtmlMessagePath( html, null, player.getObjectId(), npcId ) )
        return
    }

    let skills: Array<L2SkillLearn> = SkillCache.getAvailableSkills( player, classId, false, false, player )
    let skillBuilder: AcquireSkillList = new AcquireSkillList( AcquireSkillType.Class )
    player.setLearningClass( classId )

    skills.forEach( ( skill: L2SkillLearn ) => {
        if ( SkillCache.getSkill( skill.getSkillId(), skill.getSkillLevel() ) ) {
            skillBuilder.addSkill( skill.getSkillId(),
                skill.getSkillLevel(),
                skill.getCalculatedLevelUpSp( ClassId.getClassIdByIdentifier( player.getClassId() ), ClassId.getClassIdByIdentifier( classId ) ),
                0 )
        }
    } )

    if ( skillBuilder.getCount() > 0 ) {
        player.sendOwnedData( skillBuilder.getBuffer() )
        return
    }

    let minLevel = SkillCache.getMinimumLevelForNewSkill( player, SkillCache.getCompleteClassSkillTree( classId ) )
    if ( minLevel > 0 ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.DO_NOT_HAVE_FURTHER_SKILLS_TO_LEARN_S1 )
            .addNumber( minLevel )
            .getBuffer()
        player.sendOwnedData( packet )

        return
    }

    if ( ClassId.getClassIdByIdentifier( player.getClassId() ).level === 1 ) {
        let packet = new SystemMessageBuilder( SystemMessageIds.NO_SKILLS_TO_LEARN_RETURN_AFTER_S1_CLASS_CHANGE )
            .addNumber( 2 )
            .getBuffer()
        player.sendOwnedData( packet )

        return
    }

    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_MORE_SKILLS_TO_LEARN ) )
}