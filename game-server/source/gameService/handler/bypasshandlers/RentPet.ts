import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { InstanceType } from '../../enums/InstanceType'
import { ConfigManager } from '../../../config/ConfigManager'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { SetupGauge, SetupGaugeColors } from '../../packets/send/SetupGauge'
import _ from 'lodash'
import { DataManager } from '../../../data/manager'

const petCost: Array<number> = [
    1800,
    7200,
    720000,
    6480000,
]

const petRideTime: Array<number> = [
    30,
    60,
    600,
    1800,
]

// TODO : convert to bypass listener so that pet cost/ride time can be configurable
export const RentPet: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'RentPet',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator || !originator.isMerchant() ) {
            return false
        }

        if ( !ConfigManager.general.allowRentPet() ) {
            return false
        }

        if ( !ConfigManager.npc.getPetRentNPCs().includes( originator.getId() ) ) {
            return false
        }

        let commandChunks = _.split( command, ' ' )
        if ( commandChunks.length < 1 ) {
            let path = 'overrides/html/bypass/rentpet/main.htm'
            let html = DataManager.getHtmlData().getItem( path )
            player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), originator.getObjectId() ) )
            return true
        }

        await tryRentPet( player, _.parseInt( commandChunks[ 1 ] ) )

        return true
    },
}

async function tryRentPet( player: L2PcInstance, value: number ): Promise<any> {
    if ( !player
            || player.hasSummon()
            || player.isMounted()
            || player.isRentedPet()
            || player.isTransformed()
            || player.isCursedWeaponEquipped()
            || !( await player.disarmWeapons() ) ) {
        return
    }

    if ( ( value < 1 ) || ( value > 4 ) ) {
        return
    }

    let petId: number
    let price: number = 1

    if ( value > 10 ) {
        petId = 12526
        value -= 10
        price /= 2
    } else {
        petId = 12621
    }

    let index = value - 1

    if ( !await player.reduceAdena( Math.floor( petCost[ index ] * price ), true, 'PetRent.tryRentPet' ) ) {
        return
    }

    let time = petRideTime[ index ]
    let duration = time * 1000
    player.sendOwnedData( SetupGauge( player.getObjectId(), SetupGaugeColors.Other, duration, duration ) )
    player.startRentPet( time )

    return player.mountWithId( petId, 0, false )
}