import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { L2Npc } from '../../models/actor/L2Npc'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../data/manager'
import { ClanCache } from '../../cache/ClanCache'
import { L2Clan } from '../../models/L2Clan'

/*
    TODO: move to listener
    - cache generated html per castle id
    - add events for castle owner change and re-generate html
 */
export const TerritoryStatus: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'TerritoryStatus',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator || !originator.isNpc() ) {
            return false
        }

        let npc: L2Npc = originator as L2Npc
        let html: string
        let htmlPath: string
        if ( npc.getCastle().getOwnerId() > 0 ) {
            let clan: L2Clan = ClanCache.getClan( npc.getCastle().getOwnerId() )
            htmlPath = 'data/html/territorystatus.htm'
            html = DataManager.getHtmlData().getItem( htmlPath )
                    .replace( /%clanname%/, clan.getName() )
                    .replace( /%clanleadername%/, clan.getLeaderName() )
        } else {
            htmlPath = 'data/html/territorynoclan.htm'
            html = DataManager.getHtmlData().getItem( htmlPath )
        }

        html = html.replace( /%castlename%/, npc.getCastle().getName() )
                .replace( /%taxpercent%/, npc.getCastle().getTaxPercent().toString() )

        if ( npc.getCastle().getResidenceId() > 6 ) {
            html = html.replace( /%territory%/, 'The Kingdom of Elmore' )
        } else {
            html = html.replace( /%territory%/, 'The Kingdom of Aden' )
        }

        player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), npc.getObjectId() ) )
        return true
    },
}