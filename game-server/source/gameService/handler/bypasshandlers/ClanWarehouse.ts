import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { InstanceType } from '../../enums/InstanceType'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { ConfigManager } from '../../../config/ConfigManager'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { getWarehouseWithdrawSortOrder, WarehouseWithdrawSort } from '../../values/SortedWareHouseWithdrawalListValues'
import { WarehouseListType } from '../../values/WarehouseListType'
import { WarehouseDepositType } from '../../values/warehouseDepositType'
import { WareHouseDepositList } from '../../packets/send/WareHouseDepositList'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { WareHouseWithdrawalList } from '../../packets/send/WareHouseWithdrawalList'
import { SortedWareHouseWithdrawalList } from '../../packets/send/SortedWareHouseWithdrawalList'
import _ from 'lodash'
import aigle from 'aigle'
import { ClanPrivilege } from '../../enums/ClanPriviledge'

export const ClanWarehouse: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'withdrawc',
            'withdrawsortedc',
            'depositc',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator.isInstanceType( InstanceType.L2WarehouseInstance ) && !originator.isInstanceType( InstanceType.L2ClanHallManagerInstance ) ) {
            return false
        }

        if ( player.isEnchanting() ) {
            return false
        }

        if ( !player.getClan() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DO_NOT_HAVE_THE_RIGHT_TO_USE_CLAN_WAREHOUSE ) )
            return false
        }

        if ( player.getClan().getLevel() === 0 ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_LEVEL_1_CLAN_OR_HIGHER_CAN_USE_WAREHOUSE ) )
            return false
        }

        let currentCommand = _.toLower( command )
        if ( currentCommand.startsWith( 'withdrawc' ) ) {
            if ( ConfigManager.customs.enableWarehouseSortingClan() ) {
                let path = 'data/html/mods/WhSortedC.htm'
                let message = DataManager.getHtmlData().getItem( path )
                player.sendOwnedData( NpcHtmlMessagePath( message, path, player.getObjectId(), originator.getObjectId() ) )
            } else {
                await showWithdrawWindow( player, null, 0 )
            }

            return true
        }

        if ( currentCommand.startsWith( 'withdrawsortedc' ) ) {
            let parameters: Array<string> = command.split( ' ' )

            if ( parameters.length > 2 ) {
                await showWithdrawWindow( player, WarehouseListType[ parameters[ 1 ] ], getWarehouseWithdrawSortOrder( parameters[ 2 ] ) )
            } else if ( parameters.length > 1 ) {
                await showWithdrawWindow( player, WarehouseListType[ parameters[ 1 ] ], WarehouseWithdrawSort.A2Z )
            } else {
                await showWithdrawWindow( player, WarehouseListType.ALL, WarehouseWithdrawSort.A2Z )
            }

            return true
        }

        if ( currentCommand.startsWith( 'depositc' ) ) {
            player.sendOwnedData( ActionFailed() )
            player.setActiveWarehouse( player.getClan().getWarehouse() )
            player.setInventoryBlockingStatus( true )

            player.sendOwnedData( WareHouseDepositList( player, WarehouseDepositType.Clan ) )
            return true
        }

        return false
    },
}

async function showWithdrawWindow( player: L2PcInstance, type: WarehouseListType, order: number ): Promise<void> {
    player.sendOwnedData( ActionFailed() )

    if ( !player.hasClanPrivilege( ClanPrivilege.WarehouseAccess ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.YOU_DO_NOT_HAVE_THE_RIGHT_TO_USE_CLAN_WAREHOUSE ) )
        return
    }

    let warehouse = player.getClan().getWarehouse()
    player.setActiveWarehouse( player.getClan().getWarehouse() )

    if ( warehouse.getSize() === 0 ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_ITEM_DEPOSITED_IN_WH ) )
        return
    }

    let items : Array<L2ItemInstance> = warehouse.getItems().filter( ( item: L2ItemInstance ) => {
        return item && item.isTimeLimitedItem() && item.getRemainingTime() <= 0
    } )

    await aigle.resolve( items ).each( ( item: L2ItemInstance ) => warehouse.destroyItem( item, item.getCount() ) )

    if ( _.isNumber( type ) ) {
        return player.sendOwnedData( SortedWareHouseWithdrawalList( warehouse, player.getAdena(), WarehouseDepositType.Clan, type, order ) )
    }

    player.sendOwnedData( WareHouseWithdrawalList( warehouse, WarehouseDepositType.Clan, player.getAdena() ) )
}