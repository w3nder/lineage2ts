import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { ExShowBaseAttributeCancelWindow } from '../../packets/send/ExShowBaseAttributeCancelWindow'

export const ReleaseAttribute: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'ReleaseAttribute',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator || !originator.isNpc() ) {
            return false
        }

        player.sendOwnedData( ExShowBaseAttributeCancelWindow( player ) )
        return true
    },
}