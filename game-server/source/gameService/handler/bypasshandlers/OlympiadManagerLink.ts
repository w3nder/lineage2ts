import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { ConfigManager } from '../../../config/ConfigManager'
import { InstanceType } from '../../enums/InstanceType'
import { L2OlympiadManagerInstance } from '../../models/actor/instance/L2OlympiadManagerInstance'
import { OlympiadValues } from '../../values/OlympiadValues'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { ClassId } from '../../models/base/ClassId'
import { OlympiadManager } from '../../models/olympiad/OlympiadManager'
import { Olympiad } from '../../cache/Olympiad'
import { CompetitionType } from '../../models/olympiad/CompetitionType'
import { MultisellCache } from '../../cache/MultisellCache'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2ItemInstance } from '../../models/items/instance/L2ItemInstance'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { HeroCache } from '../../cache/HeroCache'
import { ExHeroList } from '../../packets/send/ExHeroList'
import _ from 'lodash'

const buffs: Array<number> = [
    4357, // Haste Lv2
    4342, // Wind Walk Lv2
    4356, // Empower Lv3
    4355, // Acumen Lv3
    4351, // Concentration Lv6
    4345, // Might Lv3
    4358, // Guidance Lv3
    4359, // Focus Lv3
    4360, // Death Whisper Lv3
    4352, // Berserker Spirit Lv2
]

function fewerThanMessage() {
    return `Fewer than ${ ConfigManager.olympiad.getRegistrationDisplayNumber() }`
}

function moreThanMessage() {
    return `More than ${ ConfigManager.olympiad.getRegistrationDisplayNumber() }`
}

function getPath( name: string ): string {
    return `${ OlympiadValues.OLYMPIAD_HTML_PATH }${ name }`
}

export const OlympiadManagerLink: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'olympiaddesc',
            'olympiadnoble',
            'olybuff',
            'olympiad',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator || originator.isInstanceType( InstanceType.L2OlympiadManagerInstance ) ) {
            return false
        }

        let currentCommand = _.toLower( command )

        if ( currentCommand.startsWith( 'olympiaddesc' ) ) {
            ( originator as L2OlympiadManagerInstance ).showChatWindowExtended( player,
                                                                              _.parseInt( currentCommand.substring( 13, 14 ) ),
                                                                              currentCommand.substring( 14 ) )
            return true
        }

        if ( currentCommand.startsWith( 'olympiadnoble' ) ) {

            if ( player.isCursedWeaponEquipped() ) {
                let htmlPath = getPath( 'noble_cursed_weapon.htm' )
                let html: string = DataManager.getHtmlData().getItem( htmlPath )
                player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), originator.getObjectId() ) )
                return false
            }


            if ( player.getClassIndex() !== 0 ) {
                let htmlPath = getPath( 'noble_sub.htm' )
                let html: string = DataManager.getHtmlData().getItem( htmlPath )

                player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), originator.getObjectId() ) )
                return false
            }

            if ( !player.isNoble() || ClassId.getLevelByClassId( player.getClassId() ) < 3 ) {
                let htmlPath = getPath( 'noble_thirdclass.htm' )
                let html: string = DataManager.getHtmlData().getItem( htmlPath )

                player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), originator.getObjectId() ) )
                return false
            }

            let value = _.parseInt( command.substring( 14 ) )
            switch ( value ) {
                case 0: // H5 match selection
                    if ( !OlympiadManager.isRegisteredInCompetition( player ) ) {
                        let htmlPath = getPath( 'noble_desc2a.htm' )
                        let html: string = DataManager.getHtmlData().getItem( htmlPath )
                                .replace( /%olympiad_period%/g, Olympiad.getPeriod().toString() )
                                .replace( /%olympiad_cycle%/g, Olympiad.getCurrentCycle().toString() )
                                .replace( /%olympiad_opponent%/g, OlympiadManager.getCountOpponents().toString() )
                        player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), originator.getObjectId() ) )
                        return true
                    }

                    let unregisterPath = getPath( 'noble_unregister.htm' )
                    let selectionHtml: string = DataManager.getHtmlData().getItem( unregisterPath )

                    player.sendOwnedData( NpcHtmlMessagePath( selectionHtml, unregisterPath, player.getObjectId(), originator.getObjectId() ) )
                    return true

                case 1: // unregister
                    OlympiadManager.unRegisterNoble( player )
                    return true

                case 2: // show waiting list | not used anymore ?
                    return true

                case 3: // There are %points% Grand Olympiad points granted for this event. | not used anymore ?
                    return true

                case 4: // register non classed
                    OlympiadManager.registerNoble( player, CompetitionType.NON_CLASSED )
                    return true

                case 5: // register classed
                    OlympiadManager.registerNoble( player, CompetitionType.CLASSED )
                    return true

                case 6: // request tokens reward
                    let requestPasses = Olympiad.getNoblessePasses( player )
                    let currentPath = requestPasses > 0 ? 'noble_settle.htm' : 'noble_nopoints2.htm'

                    let requestTokenPath = getPath( currentPath )
                    let requestHtml: string = DataManager.getHtmlData().getItem( requestTokenPath )
                    player.sendOwnedData( NpcHtmlMessagePath( requestHtml, requestTokenPath, player.getObjectId(), originator.getObjectId() ) )
                    return true

                case 7: // Equipment Rewards
                    MultisellCache.separateAndSend( 102, player, originator as L2Npc, false )
                    return true

                case 8: // Misc. Rewards
                    MultisellCache.separateAndSend( 103, player, originator as L2Npc, false )
                    return true

                case 9: // Your Grand Olympiad Score from the previous period is %points% point(s) | not used anymore ?
                    return true

                case 10: // give tokens to player
                    let tokenPasses: number = Olympiad.getNoblessePasses( player, true )
                    if ( tokenPasses > 0 ) {
                        let item: L2ItemInstance = await player.getInventory().addItem( ConfigManager.olympiad.getCompetitionRewardItem(), tokenPasses, originator.getObjectId(), 'OlympiadManagerLink.onStart' )

                        let message = new SystemMessageBuilder( SystemMessageIds.EARNED_S2_S1_S )
                                .addNumber( tokenPasses )
                                .addItemInstanceName( item )
                                .getBuffer()
                        player.sendOwnedData( message )
                    }

                    return true

                case 11: // register team
                    OlympiadManager.registerNoble( player, CompetitionType.TEAMS )
                    break
                default:
                    return true
            }
        }

        if ( currentCommand.startsWith( 'olybuff' ) ) {
            let buffCount = player.getOlympiadBuffCount()
            if ( buffCount <= 0 ) {
                return false
            }

            let parameters: Array<string> = currentCommand.split( ' ' )

            let index = _.parseInt( parameters[ 1 ] )
            if ( ( index < 0 ) || ( index > buffs.length ) ) {
                return false
            }

            // TODO : additional implementation
            return true
        }

        if ( currentCommand.startsWith( 'olympiad' ) ) {
            let value = _.parseInt( command.substring( 9, 10 ) )

            switch ( value ) {
                case 2: // show rank for a specific class
                    let classId = _.parseInt( command.substring( 11 ) )

                    if ( ( classId >= 88 && classId <= 118 )
                            || ( classId >= 131 && classId <= 134 )
                            || classId === 136 ) {
                        let names: Array<string> = await Olympiad.getClassLeaderBoard( classId )
                        let htmlPath = getPath( 'olympiad_ranking.htm' )
                        let html: string = DataManager.getHtmlData().getItem( htmlPath )

                        _.times( 10, ( index: number ) => {
                            let currentName: string = _.nth( names, index )
                            let place = index + 1

                            if ( currentName ) {
                                html = html.replace( `%place${ place }%`, place.toString() )
                                        .replace( `%rank${ place }%`, currentName )
                                return
                            }

                            html = html.replace( `%place${ place }%`, '' )
                                    .replace( `%rank${ place }%`, '' )
                        } )

                        player.sendOwnedData( NpcHtmlMessagePath( html, htmlPath, player.getObjectId(), originator.getObjectId() ) )
                    }

                    return true
                case 4: // hero list
                    player.sendOwnedData( ExHeroList() )
                    return true

                case 5: // Hero Certification
                    let certificationPath: string
                    if ( HeroCache.isUnclaimedHero( player.getObjectId() ) ) {
                        await HeroCache.claimHero( player )
                        certificationPath = 'hero_receive.htm'
                    } else {
                        certificationPath = 'hero_notreceive.htm'
                    }

                    let finalCertificationPath = getPath( certificationPath )
                    let certificationHtml: string = DataManager.getHtmlData().getItem( finalCertificationPath )
                    player.sendOwnedData( NpcHtmlMessagePath( certificationHtml, finalCertificationPath, player.getObjectId(), originator.getObjectId() ) )
                    return true
            }

            return true
        }

        return true
    },
}