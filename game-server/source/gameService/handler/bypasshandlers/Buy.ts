import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { L2MerchantInstance } from '../../models/actor/instance/L2MerchantInstance'

export const Buy: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'Buy',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator || !originator.isMerchant() ) {
            return false
        }

        let chunks: Array<string> = command.split( ' ' )

        if ( chunks.length < 2 ) {
            return false
        }

        ( originator as L2MerchantInstance ).showBuyWindow( player, parseInt( chunks[ 1 ] ) )

        return true
    },
}