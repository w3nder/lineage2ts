import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { DataManager } from '../../../data/manager'
import _ from 'lodash'

export const Link: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'Link',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        let htmlPath = command.substring( 4 ).trim()
        if ( _.isEmpty( htmlPath ) ) {
            return false
        }

        if ( htmlPath.includes( '..' ) ) {
            return false
        }

        let originatorObjectId = originator ? originator.getObjectId() : 0
        let finalPath = `data/html/${ htmlPath }`
        let html = DataManager.getHtmlData().getItem( finalPath )

        player.sendOwnedData( NpcHtmlMessagePath( html, finalPath, player.getObjectId(), originatorObjectId ) )
        return true
    },
}