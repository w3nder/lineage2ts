import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Npc } from '../../models/actor/L2Npc'
import { L2World } from '../../L2World'
import { ExOlympiadMatchList } from '../../packets/send/ExOlympiadMatchList'
import { Olympiad } from '../../cache/Olympiad'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { InstanceType } from '../../enums/InstanceType'
import { OlympiadManager } from '../../models/olympiad/OlympiadManager'
import { OlympiadGameTask } from '../../models/olympiad/OlympiadGameTask'
import { OlympiadGameManager } from '../../models/olympiad/OlympiadGameManager'
import _ from 'lodash'

export const OlympiadArenaChange = 'arenachange'
export const OlympiadObservation: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'watchmatch',
            OlympiadArenaChange,
        ]
    },

    async onStart( command: string, player: L2PcInstance ): Promise<boolean> {
        if ( command.startsWith( 'watchmatch' ) ) {
            if ( !Olympiad.isInCompetitionPeriod() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_OLYMPIAD_GAME_IS_NOT_CURRENTLY_IN_PROGRESS ) )
                return false
            }

            player.sendOwnedData( ExOlympiadMatchList() )
            return true
        }

        let manager: L2Npc = L2World.getObjectById( player.getLastFolkNPC() ) as L2Npc
        if ( !manager || !manager.isInstanceType( InstanceType.L2OlympiadManagerInstance ) ) {
            return false
        }

        if ( !player.inObserverMode() && !player.isInsideRadius( manager, 300 ) ) {
            return false
        }

        if ( OlympiadManager.isRegisteredInCompetition( player ) ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.WHILE_YOU_ARE_ON_THE_WAITING_LIST_YOU_ARE_NOT_ALLOWED_TO_WATCH_THE_GAME ) )
            return false
        }

        if ( !Olympiad.isInCompetitionPeriod() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_OLYMPIAD_GAME_IS_NOT_CURRENTLY_IN_PROGRESS ) )
            return false
        }

        if ( player.isOnEvent() ) {
            player.sendMessage( 'You can not observe games while registered on an event' )
            return false
        }

        let arenaId = _.parseInt( command.substring( 12 ).trim() )
        let nextArena: OlympiadGameTask = OlympiadGameManager.getOlympiadTask( arenaId )
        if ( nextArena ) {
            await player.enterOlympiadObserverMode( nextArena.getSpectatorLocation(), arenaId )
            player.setInstanceId( OlympiadGameManager.getOlympiadTask( arenaId ).getInstanceId() )
        }

        return true
    },
}