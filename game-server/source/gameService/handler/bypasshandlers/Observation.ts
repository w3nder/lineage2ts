import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { InstanceType } from '../../enums/InstanceType'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { Location } from '../../models/Location'
import { SiegeManager } from '../../instancemanager/SiegeManager'
import { L2Npc } from '../../models/actor/L2Npc'
import { ActionFailed } from '../../packets/send/ActionFailed'
import _ from 'lodash'

const locations: Array<Array<number>> = [
    // Gludio
    [ -18347, 114000, -2360, 500 ],
    [ -18347, 113255, -2447, 500 ],

    // Dion
    [ 22321, 155785, -2604, 500 ],
    [ 22321, 156492, -2627, 500 ],

    // Giran
    [ 112000, 144864, -2445, 500 ],
    [ 112657, 144864, -2525, 500 ],

    // Innadril
    [ 116260, 244600, -775, 500 ],
    [ 116260, 245264, -721, 500 ],

    // Oren
    [ 78100, 36950, -2242, 500 ],
    [ 78744, 36950, -2244, 500 ],

    // Aden
    [ 147457, 9601, -233, 500 ],
    [ 147457, 8720, -252, 500 ],

    // Goddard
    [ 147542, -43543, -1328, 500 ],
    [ 147465, -45259, -1328, 500 ],

    // Rune
    [ 20598, -49113, -300, 500 ],
    [ 18702, -49150, -600, 500 ],

    // Schuttgart
    [ 77541, -147447, 353, 500 ],
    [ 77541, -149245, 353, 500 ],

    // Coliseum
    [ 148416, 46724, -3000, 80 ],
    [ 149500, 46724, -3000, 80 ],
    [ 150511, 46724, -3000, 80 ],

    // Dusk
    [ -77200, 88500, -4800, 500 ],
    [ -75320, 87135, -4800, 500 ],
    [ -76840, 85770, -4800, 500 ],
    [ -76840, 85770, -4800, 500 ],
    [ -79950, 85165, -4800, 500 ],

    // Dawn
    [ -79185, 112725, -4300, 500 ],
    [ -76175, 113330, -4300, 500 ],
    [ -74305, 111965, -4300, 500 ],
    [ -75915, 110600, -4300, 500 ],
    [ -78930, 110005, -4300, 500 ],
]

export const Observation: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'observesiege',
            'observeoracle',
            'observe',
        ]
    },

    async onStart( commandLine: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator.isInstanceType( InstanceType.L2ObservationInstance ) ) {
            return false
        }

        if ( player.hasSummon() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.NO_OBSERVE_WITH_PET ) )
            return false
        }

        if ( player.isOnEvent() ) {
            player.sendMessage( 'Cannot use during ongoing Event' )
            return false
        }

        let currentCommand = commandLine.split( ' ' )[ 0 ].toLowerCase()
        let index = _.parseInt( _.nth( currentCommand.split( ' ' ), 1 ) )

        if ( ( index < 0 ) || ( index > ( locations.length - 1 ) ) ) {
            return false
        }

        let [ x, y, z, cost ] = locations[ index ]
        let location = new Location( x, y, z )

        switch ( currentCommand ) {
            case 'observesiege':
                if ( SiegeManager.getSiege( x, y, z ) ) {
                    await processObservation( player, originator as L2Npc, location, cost )
                } else {
                    player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.ONLY_VIEW_SIEGE ) )
                }

                return true
            case 'observeoracle': // Oracle Dusk/Dawn
                await processObservation( player, originator as L2Npc, location, cost )
                return true
            case 'observe': // Observe
                await processObservation( player, originator as L2Npc, location, cost )
                return true
        }

        return false
    },
}

async function processObservation( player: L2PcInstance, npc: L2Npc, location: Location, cost: number ): Promise<void> {
    if ( await player.reduceAdena( cost, true, 'Observation BypassHandler' ) ) {
        await player.enterObserverMode( location )
    }

    player.sendOwnedData( ActionFailed() )
}