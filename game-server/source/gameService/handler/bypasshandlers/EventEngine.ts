import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { L2Event } from '../../models/entity/L2Event'

export const EventEngine: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'event_participate',
            'event_unregister'
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator.isNpc() ) {
            return false
        }

        let name = command.toLowerCase()

        switch ( name ) {
            case 'event_participate':
                L2Event.registerPlayer( player )
                return true

            case 'event_unregister':
                L2Event.removeAndResetPlayer( player )
                return true
        }

        return false
    }
}