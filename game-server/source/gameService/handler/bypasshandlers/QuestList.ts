import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { InstanceType } from '../../enums/InstanceType'
import { ExShowQuestInfo } from '../../packets/send/ExShowQuestInfo'

export const QuestList : IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [ 'questlist' ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator.isInstanceType( InstanceType.L2AdventurerInstance ) ) {
            return false
        }

        player.sendOwnedData( ExShowQuestInfo() )
        return true
    }
}