import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { InstanceType } from '../../enums/InstanceType'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'

export const BuyShadowItem: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'BuyShadowItem',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !originator || !originator.isMerchant() ) {
            return false
        }

        let htmlPath = getHtmlPath( player.getLevel() )
        let message = DataManager.getHtmlData().getItem( htmlPath )
        player.sendOwnedData( NpcHtmlMessagePath( message, htmlPath, player.getObjectId(), originator.getObjectId() ) )

        return true
    },
}

function getHtmlPath( level: number ): string {
    if ( ( level >= 40 ) && ( level < 46 ) ) {
        return 'data/html/common/shadow_item_d.htm'
    }

    if ( ( level >= 46 ) && ( level < 52 ) ) {
        return 'data/html/common/shadow_item_c.htm'
    }

    if ( level >= 52 ) {
        return 'data/html/common/shadow_item_b.htm'
    }

    return 'data/html/common/shadow_item-lowlevel.htm'
}