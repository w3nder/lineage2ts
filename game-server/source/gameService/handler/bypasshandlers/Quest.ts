import { IBypassHandler } from '../IBypassHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Character } from '../../models/actor/L2Character'
import { L2Npc } from '../../models/actor/L2Npc'
import { QuestState } from '../../models/quest/QuestState'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, NpcTalkEvent } from '../../models/events/EventType'
import { EventListenerData } from '../../models/events/listeners/EventListenerData'
import { ListenerLogic } from '../../models/ListenerLogic'
import { L2World } from '../../L2World'
import { ListenerManager } from '../../instancemanager/ListenerManager'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessage, NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { ActionFailed } from '../../packets/send/ActionFailed'
import { QuestHelper } from '../../../listeners/helpers/QuestHelper'
import { QuestStateCache } from '../../cache/QuestStateCache'
import { QuestStateLimits } from '../../models/quest/State'
import { EventPoolCache } from '../../cache/EventPoolCache'

const toLeadAndBeLeadQuestId = 118
const theLeaderAndTheFollowerQuestId = 123

export const Quest: IBypassHandler = {
    hasRangeValidation(): boolean {
        return true
    },

    getCommandNames(): Array<string> {
        return [
            'Quest',
        ]
    },

    async onStart( command: string, player: L2PcInstance, originator: L2Character ): Promise<boolean> {
        if ( !command ) {
            return false
        }

        if ( !originator.isNpc() ) {
            return false
        }

        let quest = command.substring( 5 ).trim()

        if ( quest.length === 0 ) {
            await showQuestWindowNoName( player, originator as L2Npc )

            return true
        }

        let questNameEnd: number = quest.indexOf( ' ' )
        if ( questNameEnd === -1 ) {
            await showQuestWindowByName( player, originator as L2Npc, quest )

            return true
        }

        await QuestHelper.processQuestEvent( player, originator as L2Npc, quest.substring( 0, questNameEnd ), quest.substring( questNameEnd ).trim() )

        return true
    },
}

async function showQuestWindowNoName( player: L2PcInstance, npc: L2Npc ): Promise<void> {
    let canStartQuest = false
    let applicableQuests = new Set<ListenerLogic>()
    let isToLeadAndBeLeadQuest: boolean = false

    ListenerCache.getNpcTemplateListeners( npc.getId(), EventType.NpcTalk )?.forEach( ( data: EventListenerData ) => {
        let quest: ListenerLogic = data.owner as ListenerLogic

        if ( quest && quest.isVisibleInQuestWindow() ) {
            let state: QuestState = QuestStateCache.getQuestState( player.getObjectId(), quest.getName() )
            if ( state ) {
                applicableQuests.add( quest )

                isToLeadAndBeLeadQuest = isToLeadAndBeLeadQuest || quest.getId() === toLeadAndBeLeadQuestId

                if ( quest.canStartQuest( player ) ) {
                    canStartQuest = true
                }
            }
        }
    } )

    ListenerCache.getNpcTemplateListeners( npc.getId(), EventType.NpcStartQuest )?.forEach( ( data: EventListenerData ) => {
        let quest: ListenerLogic = data.owner as ListenerLogic

        if ( quest.isVisibleInQuestWindow() ) {
            applicableQuests.add( quest )

            if ( !canStartQuest && quest.canStartQuest( player ) ) {
                canStartQuest = true
            }
        }
    } )

    if ( canStartQuest ) {
        if ( applicableQuests.size > 1 || ( isToLeadAndBeLeadQuest && L2World.getPlayer( player.getApprentice() ) ) ) {
            return showQuestChooseWindow( Array.from( applicableQuests ), player, npc )
        }

        if ( applicableQuests.size === 1 ) {
            return showQuestWindowByQuest( player, npc, applicableQuests.values().next().value )
        }
    }

    player.sendOwnedData( NpcHtmlMessage( QuestHelper.getNoQuestMessage(), player.getObjectId(), npc.getObjectId() ) )
    player.sendOwnedData( ActionFailed() )
}

async function showQuestWindowByName( player: L2PcInstance, npc: L2Npc, questName: string ): Promise<void> {
    let currentQuest: ListenerLogic = ListenerManager.getListenerByName( questName )

    if ( currentQuest ) {
        return showQuestWindowByQuest( player, npc, currentQuest )
    }

    player.sendOwnedData( NpcHtmlMessage( QuestHelper.getNoQuestMessage(), player.getObjectId(), npc.getObjectId() ) )
    player.sendOwnedData( ActionFailed() )
}

async function showQuestWindowByQuest( player: L2PcInstance, npc: L2Npc, currentQuest: ListenerLogic ): Promise<void> {

    let isValidQuest: boolean = currentQuest.getId() > 0 && currentQuest.getId() < 20000
    if ( isValidQuest
            && ( player.getWeightPenalty() >= 3 || !player.isInventoryUnder90( true ) ) ) {
        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.INVENTORY_LESS_THAN_80_PERCENT ) )
        return
    }

    if ( isValidQuest
            && !QuestStateCache.getQuestState( player.getObjectId(), currentQuest.getName() )
            && QuestStateCache.getAllActiveStatesSize( player.getObjectId() ) >= QuestStateLimits.MaximumQuestsAllowed ) {

        let path = 'data/html/fullquest.html'
        let html = DataManager.getHtmlData().getItem( path )

        return player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId() ) )
    }

    let eventData = EventPoolCache.getData( EventType.NpcTalk ) as NpcTalkEvent

    eventData.playerId = player.getObjectId()
    eventData.characterId = npc.getObjectId()
    eventData.characterNpcId = npc.getId()

    return currentQuest.startOnTalkEvent( eventData ).finally( () => {
        EventPoolCache.recycleData( EventType.NpcTalk, eventData )
    } )
}

function showQuestChooseWindow( quests: Array<ListenerLogic>, player: L2PcInstance, npc: L2Npc ): void {
    let messageParts: Array<string> = quests.reduce( ( allChunks: Array<string>, quest: ListenerLogic ) => {

        let questState: QuestState = QuestStateCache.getQuestState( player.getObjectId(), quest.getName() )
        let state: string
        let color: string

        if ( !questState || questState.isCreated() ) {
            state = ''
            if ( quest.canStartQuest( player ) ) {
                color = 'bbaa88'
            } else {
                color = 'a62f31'
            }

        } else if ( questState.isStarted() ) {
            state = ' (In Progress)'
            color = 'ffdd66'
        } else if ( questState.isCompleted() ) {
            state = ' (Done)'
            color = '787878'
        }

        allChunks.push( `<a action="bypass -h Quest ${ quest.getName() }"><font color="${ color }">[ ${ quest.getDescription() }${ state } ]</font></a><br>` )

        if ( [ toLeadAndBeLeadQuestId, theLeaderAndTheFollowerQuestId ].includes( quest.getId() )
                && L2World.getPlayer( player.getApprentice() ) ) {
            allChunks.push( `<a action="bypass -h Quest ${ quest.getName() } sponsor"><font color="${ color }">[ ${ quest.getDescription() }${ state } (Sponsor)]</font></a><br>` )
        }

        return allChunks
    }, [] )

    player.sendOwnedData( NpcHtmlMessage( messageParts.join( '' ), player.getObjectId(), npc.getObjectId() ) )
}