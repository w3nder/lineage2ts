import { PunishmentHandlerBase } from '../IPunishmentHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PunishmentRecord } from '../../models/punishment/PunishmentRecord'
import { EtcStatusUpdate } from '../../packets/send/EtcStatusUpdate'
import { PunishmentType } from '../../models/punishment/PunishmentType'

export class ChatBanHandler extends PunishmentHandlerBase {
    applyPunishment( player: L2PcInstance, record: PunishmentRecord ): void {
        let delay = Math.floor( ( record.expirationTime - Date.now() ) / 1000 )

        if ( delay > 0 ) {
            let amount : string = delay > 60 ? `${Math.floor( delay / 60 )} minutes` : `${delay} seconds`
            player.sendMessage( `You've been chat banned for ${amount}.` )
        } else {
            player.sendMessage( 'You\'ve been chat banned forever.' )
        }

        player.sendDebouncedPacket( EtcStatusUpdate )
    }

    removePunishment( player: L2PcInstance, record: PunishmentRecord ): void {
        player.sendMessage( 'Your Chat ban has been lifted' )
        player.sendDebouncedPacket( EtcStatusUpdate )
    }

    getType(): PunishmentType {
        return PunishmentType.ChatBlocked
    }
}