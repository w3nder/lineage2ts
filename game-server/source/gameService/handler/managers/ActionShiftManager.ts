import { InstanceType, InstanceTypeMap } from '../../enums/InstanceType'
import { IActionShiftHandler } from '../IActionShiftHandler'

class Handler {
    actions: { [ key: number ]: IActionShiftHandler } = {}

    getHandler( type: InstanceType ): IActionShiftHandler {
        let currentType = type
        while ( true ) {
            if ( !currentType ) {
                return null
            }

            if ( this.actions[ currentType ] ) {
                return this.actions[ currentType ]
            }

            currentType = InstanceTypeMap[ currentType ]
        }
    }

    load( allHandlers: Array<IActionShiftHandler> ): void {
        allHandlers.forEach( this.registerHandler.bind( this ) )
    }

    registerHandler( handler: IActionShiftHandler ) {
        this.actions[ handler.getInstanceType() ] = handler
    }
}

export const ActionShiftManager = new Handler()