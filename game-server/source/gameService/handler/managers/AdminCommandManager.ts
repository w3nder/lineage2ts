import { IAdminCommand } from '../IAdminCommand'
import { L2DataApi } from '../../../data/interface/l2DataApi'
import { ServerAdministration } from '../admincommands/ServerAdministration'
import { Announcements } from '../admincommands/Announcements'
import { Buffs } from '../admincommands/Buffs'
import { PlayerAccessLevels } from '../admincommands/PlayerAccessLevels'
import { SiegeAdministration } from '../admincommands/SiegeAdministration'
import { Clan } from '../admincommands/Clan'
import { CreateItem } from '../admincommands/CreateItem'
import { PlayerLevel } from '../admincommands/PlayerLevel'
import { BuylistCheck } from '../admincommands/BuylistCheck'
import { TargetSay } from '../admincommands/TargetSay'
import { Heal } from '../admincommands/Heal'
import { Html } from '../admincommands/Html'
import { Kill } from '../admincommands/Kill'
import { Spawn } from '../admincommands/Spawn'
import { DoorAdministration } from '../admincommands/DoorAdministration'
import { MessageTest } from '../admincommands/MessageTest'
import { Scan } from '../admincommands/Scan'
import { Teleport } from '../admincommands/Teleport'
import { PlayerClassId } from '../admincommands/PlayerClassId'
import { LotterySystem } from '../admincommands/LotterySystem'
import { HeroManagement } from '../admincommands/Hero'
import _ from 'lodash'
import { OlympiadManagement } from '../admincommands/Olympiad'
import { PetManagement } from '../admincommands/Pet'
import { Help } from '../admincommands/Help'
import { Draw } from '../admincommands/Draw'
import { Geo } from '../admincommands/Geo'
import logSymbols from 'log-symbols'
import { VitalityCommands } from '../admincommands/Vitality'
import { CubicAdministration } from '../admincommands/Cubics'
import { SearchItem } from '../admincommands/SearchItem'
import { SearchSkill } from '../admincommands/SearchSkill'
import { ApplySkill } from '../admincommands/ApplySkill'
import { NpcRoutes } from '../admincommands/Routes'
import { SearchNpc } from '../admincommands/SearchNpc'
import { DimensionalItems } from '../admincommands/DimensionalItems'
import { Movement } from '../admincommands/Movement'
import { SevenSignsAdministration } from '../admincommands/SevenSigns'
import { AreaCommands } from '../admincommands/Area'
import { GeometryCommands } from '../admincommands/Geometry'
import { ArmorSets } from '../admincommands/ArmorSets'

export const AllAdminHandlers: Array<IAdminCommand> = [
    ServerAdministration,
    Announcements,
    Buffs,
    PlayerAccessLevels,
    SiegeAdministration,
    Clan,
    CreateItem,
    PlayerLevel,
    BuylistCheck,
    TargetSay,
    Heal,
    Html,
    Kill,
    Spawn,
    DoorAdministration,
    MessageTest,
    Scan,
    Teleport,
    PlayerClassId,
    LotterySystem,
    HeroManagement,
    OlympiadManagement,
    PetManagement,
    Help,
    Draw,
    Geo,
    VitalityCommands,
    CubicAdministration,
    SearchItem,
    SearchSkill,
    ApplySkill,
    NpcRoutes,
    SearchNpc,
    DimensionalItems,
    Movement,
    SevenSignsAdministration,
    AreaCommands,
    GeometryCommands,
    ArmorSets
]

type AdminCommandRegistry = Record<string, IAdminCommand>
type AdminCommandByLetter = Record<string, Array<string>>

class Manager implements L2DataApi {
    registry: AdminCommandRegistry
    allCommands: Array<string>
    commandsByLetter: AdminCommandByLetter

    getHandler( name: string ): IAdminCommand {
        return this.registry[ name ]
    }

    async load(): Promise<Array<string>> {
        this.registry = {}
        this.allCommands = []

        AllAdminHandlers.forEach( this.registerCommand.bind( this ) )

        this.allCommands = this.allCommands.sort()
        this.commandsByLetter = this.allCommands.reduce( ( total: AdminCommandByLetter, command: string ): AdminCommandByLetter => {
            let letter = command[ 0 ]
            if ( !total[ letter ] ) {
                total[ letter ] = []
            }

            total[ letter ].push( command )
            return total
        }, {} )

        _.each( this.commandsByLetter, ( value: Array<string>, key: string, collection: AdminCommandByLetter ) => {
            collection[ key ] = value.sort()
        } )

        return [
            `${ logSymbols.success } AdminCommandManager: loaded ${ _.size( this.registry ) } handlers.`,
        ]
    }

    registerCommand( handler: IAdminCommand ): void {
        this.registry = handler.getCommandNames().reduce( ( registry: AdminCommandRegistry, name: string ): AdminCommandRegistry => {
            let fullCommandName = `admin_${ name }`
            if ( registry[ fullCommandName ] ) {
                console.log( logSymbols.error, 'admin command', fullCommandName, 'is being redefined!' )
            }

            registry[ fullCommandName ] = handler

            return registry
        }, this.registry )

        this.allCommands.push( ...handler.getCommandNames() )
    }

    getAllCommands(): Array<string> {
        return this.allCommands
    }

    getCommandsByStartLetter( character: string ): Array<string> {
        return this.commandsByLetter[ character ]
    }
}

export const AdminCommandManager = new Manager()