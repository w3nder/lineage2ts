import { IUserCommandHandler } from '../IUserCommandHandler'

class Manager {
    registry: { [ key: number ] : IUserCommandHandler } = {}

    getHandler( commandId: number ) : IUserCommandHandler {
        return this.registry[ commandId ]
    }

    registerHandler( handler: IUserCommandHandler ) : void {
        let manager = this
        handler.getCommandIds().forEach( ( id: number ) => {
            manager.registry[ id ] = handler
        } )
    }

    load( allHandlers: Array<IUserCommandHandler> ): void {
        allHandlers.forEach( this.registerHandler.bind( this ) )
    }
}

export const UserCommandManager = new Manager()