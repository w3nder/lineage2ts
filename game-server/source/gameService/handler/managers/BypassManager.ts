import { IBypassHandler } from '../IBypassHandler'
import _ from 'lodash'

class Manager {
    registry: { [ key: string ]: IBypassHandler } = {}
    commandsWithoutRangeValidation: Array<string> = [
        '_bbs',
        'bbs',
        '_mail',
        '_friend',
        '_match',
        '_diary',
        '_olympiad?command',
        'manor_menu_select',
    ]

    load( allHandlers: Array<IBypassHandler> ) {
        _.each( allHandlers, this.registerHandler.bind( this ) )
    }

    getHandler( sequence: string ): IBypassHandler {
        let chunks: Array<string> = sequence.split( ' ' )
        return this.registry[ chunks[ 0 ].toLowerCase() ]
    }

    registerHandler( handler: IBypassHandler ): void {
        let manager = this
        _.each( handler.getCommandNames(), ( name: string ) => {
            manager.registry[ name.toLowerCase() ] = handler

            if ( !handler.hasRangeValidation() ) {
                manager.commandsWithoutRangeValidation.push( name )
            }
        } )
    }

    getCommandsWithoutRangeValidation(): Array<string> {
        return this.commandsWithoutRangeValidation
    }
}

export const BypassManager = new Manager()