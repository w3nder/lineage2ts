import { IPunishmentHandler } from '../IPunishmentHandler'
import { PunishmentType } from '../../models/punishment/PunishmentType'

class Manager {
    registry: { [ key: number ]: IPunishmentHandler } = {}

    getHandler( type: PunishmentType ): IPunishmentHandler {
        return this.registry[ type ]
    }

    load( allHandlers: Array<IPunishmentHandler> ): void {
        allHandlers.forEach( this.registerHandler.bind( this ) )
    }

    registerHandler( handler: IPunishmentHandler ): void {
        this.registry[ handler.getType() ] = handler
    }
}

export const UserPunishmentManager = new Manager()