import { IActionHandler } from '../IActionHandler'
import { InstanceType, InstanceTypeMap } from '../../enums/InstanceType'

class Handler {
    actions: { [key: number]: IActionHandler } = {}

    getHandler( type: InstanceType ): IActionHandler {
        let currentType = type

        while ( true ) {
            if ( !currentType ) {
                return null
            }

            if ( this.actions[ currentType ] ) {
                return this.actions[ currentType ]
            }

            currentType = InstanceTypeMap[ currentType ]
        }
    }

    registerHandler( handler: IActionHandler ) {
        this.actions[ handler.getInstanceType() ] = handler
    }

    load( allHandlers: Array<IActionHandler> ) : void {
        allHandlers.forEach( this.registerHandler.bind( this ) )
    }
}

export const ActionManager = new Handler()