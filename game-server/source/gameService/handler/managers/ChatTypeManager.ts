import { IChatHandler } from '../IChatHandler'

class Manager {
    registry: { [key: string]: IChatHandler } = {}

    load( allHandlers: Array<IChatHandler> ) {
        allHandlers.forEach( this.registerHandler.bind( this ) )
    }

    getHandler( type: number ): IChatHandler {
        return this.registry[ type ]
    }

    registerHandler( handler: IChatHandler ): void {
        this.registry[ handler.getType() ] = handler
    }
}

export const ChatTypeManager = new Manager()