import { IChatHandler } from '../IChatHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { ConfigManager } from '../../../config/ConfigManager'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { L2GameClientRegistry } from '../../L2GameClientRegistry'
import { GameClient } from '../../GameClient'
import { BlocklistCache } from '../../cache/BlocklistCache'
import { ChatType } from './ChatTypes'

export const TypeTell: IChatHandler = {
    async applyMessage( message: string, type: number, player: L2PcInstance, target: L2PcInstance ): Promise<void> {
        if ( target && !target.isSilenceModeForPlayer( player.getObjectId() ) ) {
            if ( ConfigManager.general.jailDisableChat() && target.isJailed() && !player.hasActionOverride( PlayerActionOverride.ChatAccess ) ) {
                player.sendMessage( 'Player is in jail.' )
                return
            }

            if ( target.isChatBanned() ) {
                player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_PERSON_IS_IN_MESSAGE_REFUSAL_MODE ) )
                return
            }

            let client: GameClient = L2GameClientRegistry.getClientByPlayerId( target.getObjectId() )
            if ( !client || client.isDetached() ) {
                player.sendMessage( 'Player is in offline mode.' )
                return
            }

            if ( !BlocklistCache.isBlocked( target.getObjectId(), player.getObjectId() ) ) {
                if ( ConfigManager.character.silenceModeExclude() && player.isSilenceMode() ) {
                    player.addSilenceModeExcluded( target.getObjectId() )
                }

                target.sendOwnedData( CreatureSay.fromText( target.getObjectId(), player.getObjectId(), type, player.getName(), message ) )
                player.sendOwnedData( CreatureSay.fromText( 0, player.getObjectId(), type, `->${ target.getName() }`, message ) )
                return
            }

            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.THE_PERSON_IS_IN_MESSAGE_REFUSAL_MODE ) )
            return
        }

        player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.TARGET_IS_NOT_FOUND_IN_THE_GAME ) )
    },

    getType(): ChatType {
        return ChatType.Tell
    },
}