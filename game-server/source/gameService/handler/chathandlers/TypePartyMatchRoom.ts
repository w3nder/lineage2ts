import { IChatHandler } from '../IChatHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PartyMatchManager, PartyMatchRoom } from '../../cache/partyMatchManager'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { BlocklistCache } from '../../cache/BlocklistCache'
import { PacketDispatcher } from '../../PacketDispatcher'
import { ChatType } from './ChatTypes'

export const TypePartyMatchRoom: IChatHandler = {
    async applyMessage( message: string, type: number, player: L2PcInstance ): Promise<void> {
        let room: PartyMatchRoom = PartyMatchManager.getPlayerRoom( player.getObjectId() )
        if ( !room ) {
            return
        }

        let packet: Buffer = CreatureSay.fromText( 0, player.getObjectId(), type, player.getName(), message )

        room.members.forEach( ( receiverId: number ) => {
            if ( !BlocklistCache.isBlocked( receiverId, player.getObjectId() ) ) {
                PacketDispatcher.sendCopyData( receiverId, packet )
            }
        } )
    },

    getType(): ChatType {
        return ChatType.PartymatchRoom
    },
}