import { IChatHandler } from '../IChatHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { ChatType } from './ChatTypes'

export const TypeClan : IChatHandler = {
    async applyMessage( message: string, type: number, player: L2PcInstance ): Promise<void> {
        if ( !player.getClan() ) {
            return
        }

        let packet : Buffer = CreatureSay.fromText( 0, player.getObjectId(), type, player.getName(), message )
        player.getClan().broadcastBlockedDataToOtherOnlineMembers( packet, player )

    },

    getType(): ChatType {
        return ChatType.Clan
    }
}