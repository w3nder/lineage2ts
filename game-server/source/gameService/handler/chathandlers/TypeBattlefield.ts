import { IChatHandler } from '../IChatHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { TerritoryWarManager } from '../../instancemanager/TerritoryWarManager'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { L2World } from '../../L2World'
import _ from 'lodash'
import { ChatType } from './ChatTypes'

export const TypeBattlefield : IChatHandler = {
    async applyMessage( message: string, type: number, player: L2PcInstance ): Promise<void> {
        if ( TerritoryWarManager.isTWChannelOpen && player.getSiegeSide() > 0 ) {
            let packet : Buffer = CreatureSay.fromText( 0, player.getObjectId(), type, player.getName(), message )

            _.each( L2World.getAllPlayers(), ( otherPlayer: L2PcInstance ) => {
                if ( otherPlayer.getSiegeSide() === player.getSiegeSide() ) {
                    otherPlayer.sendCopyData( packet )
                }
            } )
        }
    },

    getType(): ChatType {
        return ChatType.Battlefield
    }
}