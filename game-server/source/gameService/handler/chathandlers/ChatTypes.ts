/*
    Please see Say2 packet definition for types of messages which can be broadcast.
    The same types are used when sending text messages to client.
 */
export const enum ChatType {
    All = 0,
    Shout = 1,
    Tell = 2,
    Party = 3, // #
    Clan = 4, // @
    PetitionPlayer = 6,
    PetitionGM = 7,
    Trade = 8,
    Alliance = 9,
    PartymatchRoom = 14,
    PartyroomCommander = 15,
    PartyroomAll = 16,
    HeroVoice = 17,
    Battlefield = 20,
}