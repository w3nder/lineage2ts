import { IChatHandler } from '../IChatHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { PlayerActionOverride } from '../../values/PlayerConditions'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { L2World } from '../../L2World'
import { BlocklistCache } from '../../cache/BlocklistCache'
import _ from 'lodash'
import { ChatType } from './ChatTypes'

export const TypeHeroVoice: IChatHandler = {
    applyMessage( message: string, type: number, player: L2PcInstance ): Promise<void> {
        if ( !player.isHero() && !player.hasActionOverride( PlayerActionOverride.ChatAccess ) ) {
            return
        }

        // TODO : need rate limiting here ?
        let packet: Buffer = CreatureSay.fromText( 0, player.getObjectId(), type, player.getName(), message )

        _.each( L2World.getAllPlayers(), ( otherPlayer: L2PcInstance ) => {
            if ( otherPlayer && !BlocklistCache.isBlocked( otherPlayer.getObjectId(), player.getObjectId() ) ) {
                otherPlayer.sendCopyData( packet )
            }
        } )
    },

    getType(): ChatType {
        return ChatType.HeroVoice
    },
}