import { IChatHandler } from '../IChatHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { CreatureSay } from '../../packets/send/builder/CreatureSay'
import { ChatType } from './ChatTypes'

export const TypePartyRoomCommander : IChatHandler = {
    async applyMessage( message: string, type: number, player: L2PcInstance ): Promise<void> {
        let party = player.getParty()
        if ( !party || !party.isInCommandChannel() || party.getCommandChannel().getLeaderObjectId() !== player.getObjectId() ) {
            return
        }

        let packet : Buffer = CreatureSay.fromText( 0, player.getObjectId(), type, player.getName(), message )
        party.getCommandChannel().broadcastBlockedDataToPartyMembers( packet, player )
    },

    getType(): ChatType {
        return ChatType.PartyroomCommander
    }
}