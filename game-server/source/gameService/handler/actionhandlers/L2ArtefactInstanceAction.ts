import { IActionHandler } from '../IActionHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { L2Npc } from '../../models/actor/L2Npc'

export const L2ArtefactInstanceAction: IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        return ( target as L2Npc ).canTarget( player )
    },

    isReadyToInteract( player: L2PcInstance, target: L2Object ): boolean {
        if ( player.getTargetId() !== target.getObjectId() ) {
            player.setTarget( target )
            return false
        }

        return true
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2ArtefactInstance
    },

    async performActions(): Promise<void> {
        /*
            No actions defined.
         */
    },
}