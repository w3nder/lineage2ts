import { IActionHandler } from '../IActionHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { L2BlockInstance } from '../../models/actor/instance/L2BlockInstance'
import { InstanceType } from '../../enums/InstanceType'

export const L2BlockAction : IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        if ( !( target as L2BlockInstance ).canTarget( player ) ) {
            return false
        }

        player.setLastFolkNPC( target as L2BlockInstance )

        if ( player.getTargetId() !== target.getObjectId() ) {
            player.setTarget( target )
        }

        return false
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2BlockInstance
    },

    isReadyToInteract(): boolean {
        return false
    },

    performActions(): Promise<void> {
        return Promise.resolve( undefined )
    }
}