import { IActionHandler } from '../IActionHandler'
import { InstanceType } from '../../enums/InstanceType'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { L2StaticObjectInstance } from '../../models/actor/instance/L2StaticObjectInstance'
import { L2NpcValues } from '../../values/L2NpcValues'
import { DataManager } from '../../../data/manager'
import { NpcHtmlMessagePath } from '../../packets/send/NpcHtmlMessage'
import { ShowTownMap } from '../../packets/send/ShowTownMap'
import { L2StaticObjectType } from '../../enums/L2StaticObjectType'

export const L2StaticObjectInstanceAction: IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        return target.isStaticObject()
    },

    isReadyToInteract( player: L2PcInstance, target: L2Object ): boolean {
        if ( player.getTargetId() !== target.getObjectId() ) {
            player.setTarget( target )
            return false
        }

        return !player.isInsideRadius( target, L2NpcValues.interactionDistance )
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2StaticObjectInstance
    },

    async performActions( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<void> {
        if ( !shouldInteract ) {
            return
        }

        let staticObject: L2StaticObjectInstance = target as L2StaticObjectInstance
        switch ( staticObject.getType() ) {
            case L2StaticObjectType.ArenaSign:
                let path = staticObject.getId() === 24230101 ? 'data/html/signboards/tomb_of_crystalgolem.htm' : 'data/html/signboards/pvp_signboard.htm'
                let html = DataManager.getHtmlData().getItem( path )

                return player.sendOwnedData( NpcHtmlMessagePath( html, path, player.getObjectId(), staticObject.getObjectId() ) )

            case L2StaticObjectType.MapSign:
                return player.sendOwnedData( ShowTownMap( staticObject.mapLocation ) )
        }
    },

}
