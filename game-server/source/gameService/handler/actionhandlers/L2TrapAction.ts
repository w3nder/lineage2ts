import { IActionHandler } from '../IActionHandler'
import { InstanceType } from '../../enums/InstanceType'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'

export const L2TrapAction: IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        let lockedTarget = player.getLockedTarget()
        if ( lockedTarget && lockedTarget.getObjectId() !== target.getObjectId() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_CHANGE_TARGET ) )
            return false
        }

        return true
    },

    isReadyToInteract( player: L2PcInstance, target: L2Object ): boolean {
        player.setTarget( target )
        return true
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2TrapInstance
    },

    async performActions(): Promise<void> {
        /*
            Empty actions here.
         */
    },

}