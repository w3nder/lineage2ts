import { IActionHandler } from '../IActionHandler'
import { L2PcInstance } from '../../models/actor/instance/L2PcInstance'
import { L2Object } from '../../models/L2Object'
import { InstanceType } from '../../enums/InstanceType'
import { SystemMessageIds } from '../../packets/SystemMessageIdValues'
import { SystemMessageBuilder } from '../../packets/send/SystemMessage'
import { L2PetInstance } from '../../models/actor/instance/L2PetInstance'
import { L2Character } from '../../models/actor/L2Character'
import { PetStatusShow } from '../../packets/send/PetStatusShow'
import { AIEffectHelper } from '../../aicontroller/helpers/AIEffectHelper'
import { ListenerCache } from '../../cache/ListenerCache'
import { EventType, PlayerSummonTalkEvent } from '../../models/events/EventType'
import { EventPoolCache } from '../../cache/EventPoolCache'
import { PathFinding } from '../../../geodata/PathFinding'

export const L2PetInstanceAction: IActionHandler = {
    canInteract( player: L2PcInstance, target: L2Object ): boolean {
        let lockedTarget = player.getLockedTarget()
        if ( lockedTarget && lockedTarget.getObjectId() !== target.getObjectId() ) {
            player.sendOwnedData( SystemMessageBuilder.fromMessageId( SystemMessageIds.FAILED_CHANGE_TARGET ) )
            return false
        }

        return true
    },

    isReadyToInteract( player: L2PcInstance, target: L2Object ): boolean {
        if ( player.getTargetId() !== target.getObjectId() ) {
            player.setTarget( target )
            return false
        }

        return true
    },

    getInstanceType(): InstanceType {
        return InstanceType.L2PetInstance
    },

    async performActions( player: L2PcInstance, target: L2Object, shouldInteract: boolean ): Promise<void> {
        if ( !shouldInteract ) {
            return
        }

        let isOwner = player.getObjectId() === ( target as L2PetInstance ).getOwnerId()
        if ( target.isAutoAttackable( player ) && !isOwner ) {
            if ( PathFinding.canSeeTargetWithPosition( player, target ) ) {
                AIEffectHelper.notifyForceAttack( player, target )
                return player.onActionRequest()
            }

            return
        }

        if ( !( target as L2Character ).isInsideRadius( player, 150 ) ) {
            if ( PathFinding.canSeeTargetWithPosition( player, target ) ) {
                AIEffectHelper.notifyInteractWithTarget( player, target.getObjectId() )
                return player.onActionRequest()
            }

            return
        }

        if ( isOwner ) {
            player.sendOwnedData( PetStatusShow( target as L2PetInstance ) )

            if ( ListenerCache.hasGeneralListener( EventType.PlayerSummonTalk ) ) {
                let data = EventPoolCache.getData( EventType.PlayerSummonTalk ) as PlayerSummonTalkEvent

                data.playerId = player.getObjectId()
                data.targetId = target.getObjectId()
                data.shouldInteract = shouldInteract

                await ListenerCache.sendGeneralEvent( EventType.PlayerSummonTalk, data )
            }
        }

        player.updateNotMoveUntil()
    },
}