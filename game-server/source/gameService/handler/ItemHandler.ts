import { BeastSoulShot } from './itemhandlers/BeastSoulShot'
import { BeastSpiritShot } from './itemhandlers/BeastSpiritShot'
import { BlessedSpiritShot } from './itemhandlers/BlessedSpiritShot'
import { Book } from './itemhandlers/Book'
import { Bypass } from './itemhandlers/Bypass'
import { Calculator } from './itemhandlers/Calculator'
import { CharmOfCourage } from './itemhandlers/CharmOfCourage'
import { Disguise } from './itemhandlers/Disguise'
import { ItemSkillsTemplate } from './itemhandlers/ItemSkillsTemplate'
import { ItemSkills } from './itemhandlers/ItemSkills'
import { Elixir } from './itemhandlers/Elixir'
import { EnchantAttribute } from './itemhandlers/EnchantAttribute'
import { EnchantScrolls } from './itemhandlers/EnchantScrolls'
import { EventItem } from './itemhandlers/EventItem'
import { ExtractableItems } from './itemhandlers/ExtractableItems'
import { FishShots } from './itemhandlers/FishShots'
import { Harvester } from './itemhandlers/Harvester'
import { ManaPotion } from './itemhandlers/ManaPotion'
import { Maps } from './itemhandlers/Maps'
import { MercTicket } from './itemhandlers/MercTicket'
import { NicknameColor } from './itemhandlers/NicknameColor'
import { PetFood } from './itemhandlers/PetFood'
import { Recipes } from './itemhandlers/Recipes'
import { RollingDice } from './itemhandlers/RollingDice'
import { Seed } from './itemhandlers/Seed'
import { SevenSignsRecord } from './itemhandlers/SevenSignsRecord'
import { SoulShots } from './itemhandlers/SoulShots'
import { SpecialXMas } from './itemhandlers/SpecialXMas'
import { SpiritShot } from './itemhandlers/SpiritShot'
import { SummonItems } from './itemhandlers/SummonItems'
import { TeleportBookmark } from './itemhandlers/TeleportBookmark'
import { L2Playable } from '../models/actor/L2Playable'
import { L2ItemInstance } from '../models/items/instance/L2ItemInstance'

export const AvailableItemHandlers: { [name: string]: ItemHandlerMethod } = {
    'beastSoulShot': BeastSoulShot,
    'beastSpiritShot': BeastSpiritShot,
    'blessedSpiritShot': BlessedSpiritShot,
    'showHtml': Book,
    'showHtmlBypass': Bypass,
    'calculator': Calculator,
    'charmOfCourage': CharmOfCourage,
    'disguise': Disguise,
    'applySkillsTemplate': ItemSkillsTemplate,
    'applySkills': ItemSkills,
    'elixir': Elixir,
    'enchantAttribute': EnchantAttribute,
    'enchantScroll': EnchantScrolls,
    'eventItem': EventItem, // current item data has no item with such action
    'extractItems': ExtractableItems,
    'fishShot': FishShots,
    'harvester': Harvester,
    'manaPotion': ManaPotion,
    'map': Maps,
    'mercenary': MercTicket,
    'nicknameColor': NicknameColor,
    'petFood': PetFood,
    'recipe': Recipes,
    'dice': RollingDice,
    'seed': Seed,
    'sevenSignsRecord': SevenSignsRecord,
    'soulShot': SoulShots,
    'specialXMas': SpecialXMas,
    'spiritShot': SpiritShot,
    'summonItemSkills': SummonItems,
    'teleportBookmark': TeleportBookmark
}

export type ItemHandlerMethod = ( character: L2Playable, item: L2ItemInstance, forceUse: boolean ) => Promise<boolean>