import { L2PcInstance } from '../models/actor/instance/L2PcInstance'

export interface IVoicedCommand {
    getCommandNames(): Array<string>
    onStart( command: string, player: L2PcInstance, parametersLine: string ): Promise<void>
    isEnabled() : boolean
}