import { L2PcInstance } from '../models/actor/instance/L2PcInstance'
import { L2Character } from '../models/actor/L2Character'

export interface IBypassHandler {
    onStart( command: string, player: L2PcInstance, originator: L2Character ) : Promise<boolean>
    getCommandNames() : Array<string>
    hasRangeValidation(): boolean
}