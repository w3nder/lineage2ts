import { Augment } from '../handler/bypasshandlers/Augment'
import { Buy } from '../handler/bypasshandlers/Buy'
import { BuyShadowItem } from '../handler/bypasshandlers/BuyShadowItem'
import { ChatLink } from '../handler/bypasshandlers/ChatLink'
import { ClanWarehouse } from '../handler/bypasshandlers/ClanWarehouse'
import { EventEngine } from '../handler/bypasshandlers/EventEngine'
import { DimensionalTransferFreight } from '../handler/bypasshandlers/DimensionalTransferFreight'
import { ItemAuctionLink } from '../handler/bypasshandlers/ItemAuctionLink'
import { Link } from '../handler/bypasshandlers/Link'
import { Lottery } from '../handler/bypasshandlers/Lottery'
import { Multisell } from '../handler/bypasshandlers/Multisell'
import { ViewNpc } from '../handler/bypasshandlers/viewNpc'
import { Observation } from '../handler/bypasshandlers/Observation'
import { OlympiadManagerLink } from '../handler/bypasshandlers/OlympiadManagerLink'
import { OlympiadObservation } from '../handler/bypasshandlers/OlympiadObservation'
import { PlayerHelp } from '../handler/bypasshandlers/PlayerHelp'
import { PrivateWarehouse } from '../handler/bypasshandlers/PrivateWarehouse'
import { Quest } from '../handler/bypasshandlers/Quest'
import { QuestList } from '../handler/bypasshandlers/QuestList'
import { RentPet } from '../handler/bypasshandlers/RentPet'
import { Rift } from '../handler/bypasshandlers/Rift'
import { SkillList } from '../handler/bypasshandlers/SkillList'
import { SupportBlessing } from '../handler/bypasshandlers/SupportBlessing'
import { SupportMagic } from '../handler/bypasshandlers/SupportMagic'
import { TerritoryStatus } from '../handler/bypasshandlers/TerritoryStatus'
import { TutorialClose } from '../handler/bypasshandlers/TutorialClose'
import { VoiceCommand } from '../handler/bypasshandlers/VoiceCommand'
import { Wear } from '../handler/bypasshandlers/Wear'
import { L2DataApi } from '../../data/interface/l2DataApi'
import { BypassManager } from '../handler/managers/BypassManager'
import _ from 'lodash'

const allHandlers = [
    Augment,
    Buy,
    BuyShadowItem,
    ChatLink,
    ClanWarehouse,
    EventEngine,
    DimensionalTransferFreight,
    ItemAuctionLink,
    Link,
    Lottery,
    Multisell,
    ViewNpc,
    Observation,
    OlympiadManagerLink,
    OlympiadObservation,
    PlayerHelp,
    PrivateWarehouse,
    Quest,
    QuestList,
    RentPet,
    Rift,
    SkillList,
    SupportBlessing,
    SupportMagic,
    TerritoryStatus,
    TutorialClose,
    VoiceCommand,
    Wear
]

class Manager implements L2DataApi {
    async load(): Promise<Array<string>> {
        BypassManager.load( allHandlers )
        return [
            `BypassManager: loaded ${_.size( BypassManager.registry )} handlers.`
        ]
    }

}

export const BypassHandleLoader = new Manager()