import { L2DataApi } from '../../data/interface/l2DataApi'
import { UserPunishmentManager } from '../handler/managers/UserPunishmentMananger'
import { IPunishmentHandler } from '../handler/IPunishmentHandler'
import { BanHandler } from '../handler/punishmenthandlers/BanHandler'
import { ChatBanHandler } from '../handler/punishmenthandlers/ChatBanHandler'
import { JailHandler } from '../handler/punishmenthandlers/JailHandler'
import _ from 'lodash'

const allHandlers: Array<IPunishmentHandler> = [
    new BanHandler(),
    new ChatBanHandler(),
    new JailHandler()
]


class Manager implements L2DataApi {
    async load(): Promise<Array<string>> {
        UserPunishmentManager.load( allHandlers )
        return [
            `PunishmentHandleLoader: loaded ${ _.size( UserPunishmentManager.registry ) } handlers.`,
        ]
    }
}

export const PunishmentHandleLoader = new Manager()