import { L2DataApi } from '../../data/interface/l2DataApi'
import { ActionManager } from '../handler/managers/ActionManager'
import { IActionHandler } from '../handler/IActionHandler'
import { L2ArtefactInstanceAction } from '../handler/actionhandlers/L2ArtefactInstanceAction'
import { L2DecoyAction } from '../handler/actionhandlers/L2DecoyAction'
import { L2DoorInstanceAction } from '../handler/actionhandlers/L2DoorInstanceAction'
import { L2ItemInstanceAction } from '../handler/actionhandlers/L2ItemInstanceAction'
import { L2NpcAction } from '../handler/actionhandlers/L2NpcAction'
import { L2PcInstanceAction } from '../handler/actionhandlers/L2PcInstanceAction'
import { L2PetInstanceAction } from '../handler/actionhandlers/L2PetInstanceAction'
import { L2StaticObjectInstanceAction } from '../handler/actionhandlers/L2StaticObjectInstanceAction'
import { L2SummonAction } from '../handler/actionhandlers/L2SummonAction'
import { L2TrapAction } from '../handler/actionhandlers/L2TrapAction'
import { L2GuardAction } from '../handler/actionhandlers/L2GuardAction'
import { L2SepulcherNpcAction } from '../handler/actionhandlers/L2SepulcherNpcAction'
import _ from 'lodash'
import { L2DefenderAction } from '../handler/actionhandlers/L2DefenderAction'
import { L2BlockAction } from '../handler/actionhandlers/L2BlockAction'

const allHandlers: Array<IActionHandler> = [
    L2ArtefactInstanceAction,
    L2DecoyAction,
    L2DoorInstanceAction,
    L2ItemInstanceAction,
    L2NpcAction,
    L2PcInstanceAction,
    L2PetInstanceAction,
    L2StaticObjectInstanceAction,
    L2SummonAction,
    L2TrapAction,
    L2GuardAction,
    L2SepulcherNpcAction,
    L2DefenderAction,
    L2BlockAction,
]


export const ActionHandleLoader: L2DataApi = {
    async load(): Promise<Array<string>> {
        ActionManager.load( allHandlers )

        return [
            `ActionManager: loaded ${ _.size( ActionManager.actions ) } handlers.`,
        ]
    },
}