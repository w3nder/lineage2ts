import { DatabaseManager } from '../../database/manager'
import { BasicVariablesManager } from './BasicVariablesManager'
import _ from 'lodash'

class Manager extends BasicVariablesManager {
    async restoreMe( name: string ): Promise<void> {
        let data = await DatabaseManager.getAccountVariables().getVariables( name )
        if ( !_.isUndefined( data ) ) {
            this.registry[ name ] = data
        }
    }

    protected performStoreUpdate() : Promise<void> {
        return DatabaseManager.getAccountVariables().setManyVariables( this.registry, this.updateIds )
    }
}

export const AccountVariablesManager = new Manager()