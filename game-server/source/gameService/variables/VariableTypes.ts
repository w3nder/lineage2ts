import { AbstractVariablesMap } from './AbstractVariablesManager'

// TODO : convert to use non-persistent variable cache
export const enum VariableNames {
    pcRequest = 'pcRequest',
    doorRequest = 'doorRequest',
    petRequest = 'petRequest',
    polymorphData = 'polymorphData'
}

export interface PcRequestVariable extends AbstractVariablesMap {
    playerId: number
    itemId: number
    amount: number
    expireTime: number
}

export interface DoorRequestVariable extends AbstractVariablesMap {
    objectId: number
}

export interface PetRequestVariable extends AbstractVariablesMap {
    objectId: number
    itemId: number
}

export interface PolymorphData extends AbstractVariablesMap {
    polyId: number
    polyType: string
}