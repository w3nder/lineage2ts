import { ObjectPool } from '../helpers/ObjectPoolHelper'

// TODO : use instead of raw ItemHolder
export interface ItemData {
    itemId : number
    amount : number
}

// TODO : consider usage of object pool where needed, such as L2PcInstance.addAdditionalSweepItems or performItemDrop
const ItemDataCache = new ObjectPool( 'ItemData', () : ItemData => {
    return {
        itemId: 0,
        amount: 0
    }
} )

export const ItemDataHelper = {
    recycle( item : ItemData ) : void {
        ItemDataCache.recycleValue( item )
    },

    recycleMany( items : Array<ItemData> ) : void {
        ItemDataCache.recycleValues( items )
    },

    createItem( itemId: number, amount: number ) : ItemData {
        let item = ItemDataCache.getValue()

        item.itemId = itemId
        item.amount = amount

        return item
    }
}