export interface ItemDefinition {
    id: number
    count: number
}

export interface PlayerEquipmentItem extends ItemDefinition {
    isEquipped: boolean
}

export function createItemDefinition( id: number, count: number ) : ItemDefinition {
    return {
        id,
        count
    }
}