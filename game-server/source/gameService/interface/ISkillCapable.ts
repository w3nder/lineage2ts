import { Skill } from '../models/Skill'

export interface ISkillCapable {
    getSkills() : { [ key: number ]: Skill }
    addSkill( skill: Skill ) : void
    getKnownSkill( id: number ) : Skill
    getSkillLevel( id: number ) : number
}