export interface RangeChanceDefinition {
    min: number
    max: number
    chance: number
}