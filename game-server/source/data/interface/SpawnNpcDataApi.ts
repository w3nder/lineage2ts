import { L2NpcMinion } from '../../gameService/models/actor/templates/L2NpcMinion'

export interface L2SpawnNpcDataApi {
    getAll() : Array<L2SpawnNpcDataItem>
    getNpcIdsByRoute( routeId: string ) : Array<number>
}

export interface L2SpawnNpcPosition {
    x: number
    y: number
    z: number
    heading: number
}

export interface L2SpawnNpcDataItem {
    npcId: number
    makerId: string
    position: L2SpawnNpcPosition
    amount: number
    respawnMs: number
    respawnExtraMs: number
    aiName: string
    aiParameters: Record<string, number | string>
    returnDistance: number
    recordId: string
    minions: Array<L2NpcMinion>
}