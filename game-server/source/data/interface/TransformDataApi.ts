import { L2DataApi } from './l2DataApi'
import { Transform } from '../../gameService/models/actor/Transform'

export interface TransformDataApi extends L2DataApi {
    getTransform( id: number ) : Transform
}