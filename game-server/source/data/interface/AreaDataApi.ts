export interface L2AreaDataApi {
    getAllAreas() : Promise<Array<L2AreaItem>>
}

export type L2AreaItemPropertyValue = string | number | boolean | Record<string, string | number>

export interface L2AreaItem {
    id: string
    type: string
    points: Array<L2AreaPoint>
    minZ: number
    maxZ: number
    properties: Record<string, L2AreaItemPropertyValue>
}

export interface L2AreaPoint {
    x: number
    y: number
    z?: number
}