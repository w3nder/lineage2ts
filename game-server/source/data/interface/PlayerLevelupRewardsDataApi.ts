import { L2DataApi } from './l2DataApi'

export interface L2PlayerLevelupRewardsDataApi extends L2DataApi {
    getRewards( level : number ) : Array<L2PlayerLevelupRewardData>
}

export interface L2PlayerLevelupRewardData {
    race: string
    level : number
    items : Array<L2PlayerLevelupRewardItem>
}

export interface L2PlayerLevelupRewardItem {
    itemId : number
    amount: number
}