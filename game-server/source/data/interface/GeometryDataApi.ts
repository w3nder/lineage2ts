import { L2DataApi } from './l2DataApi'
import { RoaringBitmap32 } from 'roaring'

export interface L2GeometryDataApi extends L2DataApi {
    getItem( id: string ) : L2GeometryDataItem
}

export const enum L2GeometryDataType {
    PointCircle = 'pointCircle',
    PointSquare = 'pointSquare'
}

export interface L2GeometryDataItem {
    id: string
    type: L2GeometryDataType
    size: number
    data: RoaringBitmap32
    parameters: L2CircleGeometryParameters | L2SquareGeometryParameters
}

export interface L2CircleGeometryParameters {
    maxRadius: number
    minRadius: number
    stepIncrement: number
}

export interface L2SquareGeometryParameters {
    xLength: number
    yLength: number
    xIncrement: number
    yIncrement: number
}