import { L2DataApi } from './l2DataApi'
import { L2Item } from '../../gameService/models/items/L2Item'

export interface ItemDataApi extends L2DataApi{
    getTemplate( itemId: number ) : L2Item
    getIdsByPartialName( name: string ) : Array<number>
}