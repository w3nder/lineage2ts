import { L2DataApi } from './l2DataApi'
import { L2FishingRod } from '../../gameService/models/fishing/L2FishingRod'

export interface FishingRodsDataApi extends L2DataApi {
    getFishingRod( itemId: number ) : L2FishingRod
}