import { L2DataApi } from './l2DataApi'

export interface ExperienceDataApi extends L2DataApi{
    getExpForLevel( level: number ) : number
    getPercentFromCurrentLevel( currentExp: number, currentLevel: number ) : number
}