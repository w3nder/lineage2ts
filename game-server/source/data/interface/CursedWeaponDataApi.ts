import { L2DataApi } from './l2DataApi'
import { CursedWeapon } from '../../gameService/models/CursedWeapon'

export interface CursedWeaponDataApi extends L2DataApi {
    getCursedWeapons() : { [ key: number ] : CursedWeapon }
    getCursedWeaponIds(): Array<number>
}