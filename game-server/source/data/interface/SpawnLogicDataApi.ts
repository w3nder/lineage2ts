export interface L2SpawnLogicDataApi {
    getAll() : Record<string, L2SpawnLogicItem>
}

export interface L2SpawnLogicItem {
    makerId: string
    spawnTerritoryIds: Array<string>
    logicName: string
    maxAmount: number
    aiParameters: Record<string, number | string>
    isFlying: boolean
    eventType: string | null
    avoidTerritoryIds: Array<string> | null
}