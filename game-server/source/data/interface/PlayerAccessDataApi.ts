export interface L2PlayerAccessDataApi {
    getAll() : Promise<Array<L2PlayerAccessDataItem>>
}

export interface L2PlayerAccessCommandConfiguration {
    allowedRegex: string
    restrictedRegex: string
    confirmationRegex: string
}

export interface L2PlayerAccessDataItem {
    level: number
    name: string
    nameColor: string
    titleColor: string
    permissions: Array<string>
    commands: L2PlayerAccessCommandConfiguration
}