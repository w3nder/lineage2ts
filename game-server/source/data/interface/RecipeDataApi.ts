import { L2DataApi } from './l2DataApi'
import { L2RecipeDefinition } from '../../gameService/models/l2RecipeDefinition'

export interface RecipeDataApi extends L2DataApi {
    getRecipeList( recipeId: number ) : L2RecipeDefinition
    getRecipeByItemId( itemId: number ) : L2RecipeDefinition
}