import { L2DataApi } from './l2DataApi'
import { ActionKey } from '../../gameService/models/ActionKey'

export interface UIDataApi extends L2DataApi {
    getCategories() : UICategories
    getKeys() : UIKeys
}

export type UICategories = { [ key: number ]: Array<number> }
export type UIKeys = { [ key: number ]: Array<ActionKey> }