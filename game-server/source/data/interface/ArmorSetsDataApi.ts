import { L2DataApi } from './l2DataApi'
import { L2ArmorSet } from '../../gameService/models/L2ArmorSet'

export interface ArmorSetsDataApi extends L2DataApi{
    getArmorSet( itemId: number ) : L2ArmorSet
    getAll(): ReadonlyArray<L2ArmorSet>
    getById( id: number ): L2ArmorSet
}