import { IClassId } from '../../gameService/models/base/ClassId'
import { L2DataApi } from './l2DataApi'
import { L2PcTemplate } from '../../gameService/models/actor/templates/L2PcTemplate'

export interface PlayerTemplateData extends L2DataApi {
    getTemplateByClassId( classId: IClassId ) : L2PcTemplate
    getTemplateById( classId: number ) : L2PcTemplate
}