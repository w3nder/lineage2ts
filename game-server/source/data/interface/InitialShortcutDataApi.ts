import { L2DataApi } from './l2DataApi'
import { ShortcutType } from '../../gameService/enums/ShortcutType'

export interface InitialShortcutDataApi extends L2DataApi {
    getInitialShortcuts() : Array<L2InitialShortcutItem>
    getClassShortcuts() : { [ key: number ]: Array<L2InitialShortcutItem> }
}

export interface L2InitialShortcutItem {
    classId: number
    page: number
    slot: number
    type: ShortcutType
    id: number
    level: number
}