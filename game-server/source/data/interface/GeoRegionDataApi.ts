import { L2DataOperationsApi } from './OperationsApi'

export interface L2GeoRegionDataApi extends L2DataOperationsApi {
    getRegionData( regionX : number, regionY: number ) : Promise<Buffer>
    initialize() : Promise<void>
    deInitialize() : Promise<void>
    isInitialized() : boolean
}