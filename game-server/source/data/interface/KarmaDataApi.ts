import { L2DataApi } from './l2DataApi'

export interface KarmaDataApi extends L2DataApi {
    getMultiplier( level: number ) : number
}