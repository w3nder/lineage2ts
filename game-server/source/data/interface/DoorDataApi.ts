import { L2DataApi } from './l2DataApi'
import { L2DoorTemplate } from '../../gameService/models/actor/templates/L2DoorTemplate'

export interface DoorDataApi extends L2DataApi {
    getDoorsByGroup( name: string ) : Array<number>
    getDoorTemplate( id: number ) : L2DoorTemplate
    getAllTemplates() : Array<L2DoorTemplate>
    getIdByName( name: string ) : number
}