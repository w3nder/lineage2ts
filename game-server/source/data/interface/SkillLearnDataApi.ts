import { L2DataApi } from './l2DataApi'

export interface SkillLearnDataApi extends L2DataApi {
    getClassIds( npcId: number ) : Array<number>
}