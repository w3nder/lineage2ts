export interface L2TeleportsDataApi {
    getAll() : Promise<Array<L2TeleportData>>
}

export interface L2TeleportData {
    id: number
    name: string
    x: number
    y: number
    z: number
    itemId: number
    amount: number
    nobleOnly: boolean
    showItemPrice: boolean
}