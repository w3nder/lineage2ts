import { L2DataApi } from './l2DataApi'

export interface InstanceDataApi extends L2DataApi {
    getInstanceNames(): { [ key: number ]: string }
    getInstanceData( nameId: string ) : L2InstanceDataItem
}

export interface L2InstanceDataSpawnProperties extends L2InstanceDataLocationProperties {
    heading: number
    npcId: number
    respawn: number
}

export interface L2InstanceDataLocationProperties {
    x: number
    y: number
    z: number
}

export interface L2InstanceDataDoorProperties {
    doorId: number
}

export interface L2InstanceDataItem {
    nameId: string
    name: string
    ejectTime: number
    allowRandomWalk: boolean
    activityTime: number
    allowSummon: boolean
    emptyDestroyTime: number

    exitPoint : L2InstanceDataLocationProperties

    showTimer : {
        isTimerIncrease : boolean
        showTimer: boolean
        timerText: string
    }

    doors: Array<L2InstanceDataDoorProperties>

    spawns: {
        [ groupName : string ] : Array<L2InstanceDataSpawnProperties>
    }

    spawnPoints: Array<L2InstanceDataLocationProperties>

    reEnter: {
        type: string
        times: Array<{
            time: number
        }>
        conditions: Array<{
            day: string
            hour: number
            minute: number
        }>
    }

    removeBuffs: {
        removeBuffType : string
    }
}