import { L2DataApi } from './l2DataApi'

export interface L2HitConditionBonusDataApi extends L2DataApi {
    getHighBonus() : number
    getLowBonus() : number
    getNightBonus() : number
    getBackBonus() : number
    getFrontBonus() : number
    getSideBonus() : number
}