import { L2DataApi } from './l2DataApi'

export interface L2FishingMonsterDataApi extends L2DataApi{
    getFishingMonster( level: number ) : L2FishingMonsterDataItem
}

export interface L2FishingMonsterDataItem {
    id: number
    chance: number
    minimumLevel: number
    maximumLevel: number
}