import { L2DataApi } from './l2DataApi'
import { EnchantScroll } from '../../gameService/models/items/enchant/EnchantScroll'
import { EnchantSupportItem } from '../../gameService/models/items/enchant/EnchantSupportItem'

export interface L2EnchantItemDataApi extends L2DataApi {
    getScrollById( id: number ) : EnchantScroll
    getSupportItemById( id: number ) : EnchantSupportItem
}