import { L2DataApi } from './l2DataApi'
import { L2StaticObjectInstance } from '../../gameService/models/actor/instance/L2StaticObjectInstance'
import { L2StaticObjectType } from '../../gameService/enums/L2StaticObjectType'

export interface L2StaticObjectsDataApi extends L2DataApi {
    getType( type : L2StaticObjectType ) : Array<L2StaticObjectInstance>
}