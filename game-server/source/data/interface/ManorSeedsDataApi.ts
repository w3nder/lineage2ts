import { L2Seed } from '../../gameService/models/L2Seed'

export interface L2ManorSeedsDataApi {
    getAll() : Promise<Array<L2Seed>>
}