export interface L2BuylistDataApi {
    getAll() : Promise<Array<L2BuylistDataItem>>
}

export interface L2BuylistDataItem {
    id: number
    products: Array<L2BuylistDataProduct>
    npcId: number
}

export interface L2BuylistDataProduct {
    itemId: number
    price: number
    restockDelay: number
    amount: number
}