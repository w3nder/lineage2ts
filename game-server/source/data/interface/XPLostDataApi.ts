import { L2DataApi } from './l2DataApi'

export interface L2XPLostDataApi extends L2DataApi{
    getXpPercent( level: number ) : number
}