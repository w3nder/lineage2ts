import { L2ReloadableDataApi } from './l2DataApi'

export interface HtmlDataApi extends L2ReloadableDataApi {
    getItem( path: string ) : string
    hasItem( path: string ) : boolean
    getActions( path: string ) : L2HtmlActions
    getSize() : L2HtmlDataSize
}

export type L2HtmlActions = Record<string, Array<string>>

export interface L2HtmlDataSize {
    entries: number
    actions: number
}