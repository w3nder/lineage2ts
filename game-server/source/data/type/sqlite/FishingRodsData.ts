import { FishingRodsDataApi } from '../../interface/FishingRodsDataApi'
import { L2FishingRod } from '../../../gameService/models/fishing/L2FishingRod'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'
import logSymbols from 'log-symbols'

const query = 'select * from fishing_rods'
let allRods: { [ key: number ]: L2FishingRod }

export const SQLiteFishingRodsData: FishingRodsDataApi = {
    getFishingRod( itemId: number ): L2FishingRod {
        return allRods[ itemId ]
    },

    async load(): Promise<Array<string>> {
        allRods = {}
        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, Helper.createFishingRod )
        return [
            `${ logSymbols.success } FishingRods loaded ${ _.size( allRods ) } rod items`,
        ]
    },
}

const Helper = {
    createFishingRod( databaseItem: any ) {
        let rod: L2FishingRod = {
            damage: databaseItem.damage,
            id: databaseItem.id,
            itemId: databaseItem.itemId,
            level: databaseItem.level,
            name: databaseItem.name,
        }

        allRods[ rod.itemId ] = rod
    },
}