import { AgathionDataApi, L2AgathionDataItem } from '../../interface/AgathionDataApi'
import logSymbols from 'log-symbols'
import _ from 'lodash'
import { SQLiteDataEngine } from '../../engines/sqlite'

const query : string = 'select npcId, itemId, energy from agathions'
let agathionsByItemId : Record<number, L2AgathionDataItem>
let agathionsByNpcId : Record<number, L2AgathionDataItem>

export const SQLiteAgathionData : AgathionDataApi = {
    getByItemId( itemId: number ): L2AgathionDataItem {
        return agathionsByItemId[ itemId ]
    },

    getByNpcId( npcId: number ): L2AgathionDataItem {
        return agathionsByNpcId[ npcId ]
    },

    async load() {
        let databaseItems = SQLiteDataEngine.getMany( query )
        agathionsByItemId = {}
        agathionsByNpcId = {}

        databaseItems.forEach( ( databaseItem: any ) => {
            let item : L2AgathionDataItem = {
                energy: databaseItem.energy,
                itemId: databaseItem.itemId,
                npcId: databaseItem.npcId
            }

            agathionsByNpcId[ item.npcId ] = item

            if ( item.itemId ) {
                agathionsByItemId[ item.itemId ] = item
            }
        } )

        return [
            `${logSymbols.success} AgathionData : loaded ${ _.size( agathionsByItemId )} item associations`,
            `${logSymbols.success} AgathionData : loaded ${ _.size( agathionsByNpcId )} npc associations`,
        ]
    }
}