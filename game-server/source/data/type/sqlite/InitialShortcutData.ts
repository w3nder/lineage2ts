import { InitialShortcutDataApi, L2InitialShortcutItem } from '../../interface/InitialShortcutDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { ShortcutType } from '../../../gameService/enums/ShortcutType'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const query = 'select * from initial_shortcuts'

let classData : { [ key: number ]: Array<L2InitialShortcutItem> } = {}
let allShortcuts : Array<L2InitialShortcutItem> = []

export const SQLiteInitialShortcutData : InitialShortcutDataApi = {
    getClassShortcuts(): { [ key: number ]: Array<L2InitialShortcutItem> } {
        return classData
    },

    getInitialShortcuts(): Array<L2InitialShortcutItem> {
        return allShortcuts
    },

    async load() {
        classData = {}
        allShortcuts = []
        let databaseItems: Array<unknown> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) => {
            let data : L2InitialShortcutItem = {
                classId: databaseItem.classId,
                id: databaseItem.shortcutId,
                level: databaseItem.shortcutLevel,
                page: databaseItem.pageId,
                slot: databaseItem.slot,
                type: ShortcutType[ databaseItem.shortcutType as string ]
            }

            if ( _.isNumber( data.classId ) ) {
                if ( !classData[ data.classId ] ) {
                    classData[ data.classId ] = []
                }

                classData[ data.classId ].push( data )
                return
            }

            allShortcuts.push( data )
        } )

        return [
            `${logSymbols.success} InitialShortcutData : loaded ${_.size( classData )} class groups.`,
            `${logSymbols.success} InitialShortcutData : loaded ${_.size( allShortcuts )} total shortcuts from ${_.size( databaseItems )} records.`
        ]
    }
}