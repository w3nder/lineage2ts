import { L2AreaDataApi, L2AreaItem } from '../../interface/AreaDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'

const query = 'select * from areas'

export const SQLiteAreaData : L2AreaDataApi = {
    async getAllAreas(): Promise<Array<L2AreaItem>> {
        let databaseItems: Array<Object> = SQLiteDataEngine.getMany( query )

        return databaseItems.map( ( databaseItem: Object ) : L2AreaItem => {
            return {
                id: databaseItem[ 'id' ],
                maxZ: databaseItem[ 'maxZ' ],
                minZ: databaseItem[ 'minZ' ],
                points: JSON.parse( databaseItem[ 'points_json' ] ),
                properties: databaseItem[ 'properties_json' ] ? JSON.parse( databaseItem[ 'properties_json' ] ) : undefined,
                type: databaseItem[ 'type' ]
            }
        } )
    }
}