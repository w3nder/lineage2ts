import { L2MultisellDataApi } from '../../interface/MultisellDataApi'
import { ListContainer } from '../../../gameService/models/multisell/ListContainer'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { Entry } from '../../../gameService/models/multisell/Entry'
import { Ingredient } from '../../../gameService/models/multisell/Ingredient'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const query = 'select * from multisell_data'

let entries : { [ key: number ] : ListContainer }
let nextEntryId : number

export const SQLiteMultisellData : L2MultisellDataApi = {
    getEntry( listId: number ): ListContainer {
        return entries[ listId ]
    },

    getEntries(): { [ key: number]: ListContainer } {
        return entries
    },

    async load(): Promise<Array<string>> {
        entries = {}
        nextEntryId = 1

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, Helper.createListEntry )

        return [
            `${logSymbols.success} MultisellData loaded ${_.size( entries )} lists.`
        ]
    }
}

const Helper = {
    createListEntry( databaseItem: any ) {
        let list = new ListContainer( databaseItem[ 'id' ] )

        list.applyTaxes = databaseItem[ 'applyTaxes' ] === 'true'
        list.maintainEnchantment = databaseItem[ 'maintainEnchantment' ] === 'true'

        if ( databaseItem[ 'npcIds_json' ] ) {
            list.npcsAllowed = JSON.parse( databaseItem[ 'npcIds_json' ] )
        }

        if ( databaseItem[ 'items_json' ] ) {
            let itemData = JSON.parse( databaseItem[ 'items_json' ] )
            list.entries = _.map( itemData, Helper.createEntry )
        }

        entries[ list.listId ] = list
    },

    createEntry( item: any ) : Entry {
        let entry = new Entry( nextEntryId++ )

        entry.ingredients = _.map( item.ingredients, Helper.createIngredient )
        entry.products = _.map( item.production, Helper.createIngredient )

        return entry
    },

    createIngredient( ingredientData: any ) : Ingredient {
        let ingredient = new Ingredient()

        ingredient.itemId = ingredientData.id
        ingredient.itemCount = ingredientData.count
        ingredient.isTaxIngredient = _.get( ingredientData, 'isTaxIngredient', false )
        ingredient.maintainIngredient = _.get( ingredientData, 'maintainIngredient', false )

        return ingredient
    }
}