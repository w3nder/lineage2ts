import { L2RespawnBannedRace, L2RespawnPointData, L2RespawnPointsDataApi } from '../../interface/RespawnPointsDataApi'
import logSymbols from 'log-symbols'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { LocationXY } from '../../../gameService/models/LocationProperties'
import { RaceMapping } from '../../../gameService/enums/Race'
import { Location } from '../../../gameService/models/Location'

const query = 'select * from respawn_points'
let points : Record<string, L2RespawnPointData>

export const SQLiteRespawnPointsData : L2RespawnPointsDataApi = {
    getAll(): Array<L2RespawnPointData> {
        return Object.values( points )
    },

    getByName( name: string ) : L2RespawnPointData {
        return points[ name ]
    },

    async load(): Promise<Array<string>> {
        points = {}
        let databaseItems = SQLiteDataEngine.getMany( query )

        databaseItems.forEach( ( item: any ) => {
            let point : L2RespawnPointData = {
                bannedRace: getBannedRace( item.bannedRaceJson ),
                name: item.name,
                normalPoints: getPoints( JSON.parse( item.pointsJson ) ),
                pvpPoints: getPoints( JSON.parse( item.pvpJson ) ),
                bbs: item.bbs,
                messageId: item.messageId,
                tiles: getMaps( item.mapsJson )
            }

            points[ point.name ] = point
        } )

        return [
            `${logSymbols.success} RespawnPoints : loaded ${databaseItems.length} locations`
        ]
    }
}

type LocationPoint = [ number, number, number ]

function getPoints( data : Array<LocationPoint> ) : Array<Location> {
    return data.map( ( item: LocationPoint ) : Location => {
        return new Location( item[ 0 ], item[ 1 ], item[ 2 ] )
    } )
}

function getBannedRace( data: string ) : L2RespawnBannedRace {
    if ( !data ) {
        return
    }

    let properties = JSON.parse( data )

    return {
        location: properties.location,
        race: RaceMapping[ properties.race ]
    }
}

function getMaps( data: string ) : Array<LocationXY> {
    if ( !data ) {
        return
    }

    return JSON.parse( data )
}