import { BaseStatsDataApi } from '../../interface/BaseStatsDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'
import logSymbols from 'log-symbols'

const bonusToAttributes : { [ key: string ]: Array<number> } = {}

const query = 'select * from stat_bonus'

export const SQLiteBaseStatsData : BaseStatsDataApi = {
    getCON: (): Array<number> => {
        return bonusToAttributes[ 'CON' ]
    },

    getDEX: (): Array<number> => {
        return bonusToAttributes[ 'DEX' ]
    },

    getINT: (): Array<number> => {
        return bonusToAttributes[ 'INT' ]
    },

    getMEN: (): Array<number> => {
        return bonusToAttributes[ 'MEN' ]
    },

    getSTR: (): Array<number> => {
        return bonusToAttributes[ 'STR' ]
    },

    getWIT(): Array<number> {
        return bonusToAttributes[ 'WIT' ]
    },

    async load() {
        let databaseItems : Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) => {
            let { attribute } = databaseItem
            let values : Array<number> = JSON.parse( databaseItem[ 'values_json' ] )
            bonusToAttributes[ attribute ] = values
        } )

        return [
            `${logSymbols.success} BaseStats loaded ${_.size( bonusToAttributes )} attribute sets.`
        ]
    }
}
