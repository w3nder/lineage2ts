import { EnchantItemHPBonusDataApi } from '../../interface/EnchantItemHPBonusDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { CrystalType } from '../../../gameService/models/items/type/CrystalType'
import logSymbols from 'log-symbols'
import _ from 'lodash'

let armorHPBonuses : { [ key: number ]: Array<number> } = {}

const query = 'select * from enchant_item_hpbonus'

export const SQLiteEnchantItemHPBonusData : EnchantItemHPBonusDataApi = {
    getHPBonus( grade: CrystalType ): Array<number> {
        return armorHPBonuses[ grade ]
    },

    async load() {
        armorHPBonuses = {}
        let output = []
        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) => {
            let { grade } = databaseItem
            let key = CrystalType[ grade ]

            if ( _.isUndefined( key ) ) {
                output.push( `${logSymbols.error} Missing grade '${grade}' encountered!` )
                return
            }

            let values = JSON.parse( databaseItem[ 'values_json' ] )
            armorHPBonuses[ key ] = values
        } )

        output.push( `${logSymbols.success} EnchantItemHPBonus loaded ${_.size( armorHPBonuses )} bonus grades.` )
        return output
    }
}