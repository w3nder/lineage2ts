import _ from 'lodash'

export const JsonHelper = {
    parse<T>( jsonValue: string ) : T {
        if ( !jsonValue ) {
            return null
        }

        let parsedValue = _.attempt( JSON.parse, jsonValue )
        if ( _.isError( parsedValue ) ) {
            return null
        }

        return parsedValue
    }
}