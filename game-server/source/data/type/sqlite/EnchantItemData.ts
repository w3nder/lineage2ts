import { L2EnchantItemDataApi } from '../../interface/EnchantItemDataApi'
import { EnchantScroll } from '../../../gameService/models/items/enchant/EnchantScroll'
import { EnchantSupportItem } from '../../../gameService/models/items/enchant/EnchantSupportItem'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { AbstractEnchantItem } from '../../../gameService/models/items/enchant/AbstractEnchantItem'
import { CrystalType } from '../../../gameService/models/items/type/CrystalType'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const scrollQuery = 'select * from enchant_item_scrolls'
const supportQuery = 'select * from enchant_item_support'

let scrollMap : { [ key: number ]: EnchantScroll }
let supportMap : { [ key: number] : EnchantSupportItem }

export const SQLiteEnchantItemData: L2EnchantItemDataApi = {
    getScrollById( id: number ): EnchantScroll {
        return scrollMap[ id ]
    },

    getSupportItemById( id: number ): EnchantSupportItem {
        return supportMap[ id ]
    },

    async load(): Promise<Array<string>> {
        scrollMap = {}
        supportMap = {}

        let scrollItems: Array<any> = SQLiteDataEngine.getMany( scrollQuery )
        _.each( scrollItems, Helper.createScrollItem )

        let supportItems: Array<any> = SQLiteDataEngine.getMany( supportQuery )
        _.each( supportItems, Helper.createSupportItem )

        return [
           `${logSymbols.success} EnchantItemData loaded ${_.size( scrollMap )} scrolls.`,
           `${logSymbols.success} EnchantItemData loaded ${_.size( supportMap )} support items.`,
        ]
    }
}

const Helper = {
    createScrollItem( databaseItem: any ) {
        let item = new EnchantScroll()

        Helper.applyDefaults( item, databaseItem )

        item.groupId = _.defaultTo( databaseItem[ 'scrollGroupId' ], 0 )

        if ( databaseItem[ 'items_json' ] ) {
            item.items = JSON.parse( databaseItem[ 'items_json' ] )
        }

        item.setFlags()

        scrollMap[ item.id ] = item
    },

    createSupportItem( databaseItem: any ) {
        let item = new EnchantSupportItem()

        Helper.applyDefaults( item, databaseItem )
        item.setFlags()

        supportMap[ item.id ] = item
    },

    applyDefaults( item: AbstractEnchantItem, databaseItem: any ) {
        item.id = databaseItem[ 'id' ]
        item.grade = CrystalType[ _.defaultTo( databaseItem[ 'targetGrade' ], 'NONE' ) as string ]
        item.maxEnchantLevel = _.defaultTo( databaseItem[ 'maxEnchant' ], 65535 )
        item.bonusRate = _.defaultTo( databaseItem[ 'bonusRate' ], 0 )
    }
}