import { SkillTreesDataApi } from '../../interface/SkillTreesDataApi'
import { L2SkillLearn } from '../../../gameService/models/L2SkillLearn'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { SkillHolder } from '../../../gameService/models/holders/SkillHolder'
import { Race } from '../../../gameService/enums/Race'
import { SocialClass } from '../../../gameService/enums/SocialClass'
import { DataManager } from '../../manager'
import logSymbols from 'log-symbols'
import _ from 'lodash'

// ClassId, Map of Skill Hash Code, L2SkillLearn
let classSkillTrees: { [key: number]: { [key: number]: L2SkillLearn } }
let transferSkillTrees: { [key: number]: { [key: number]: L2SkillLearn } }

// Skill Hash Code, L2SkillLearn
let collectSkillTree: { [key: number]: L2SkillLearn }
let fishingSkillTree: { [key: number]: L2SkillLearn }
let pledgeSkillTree: { [key: number]: L2SkillLearn }
let subClassSkillTree: { [key: number]: L2SkillLearn }
let subPledgeSkillTree: { [key: number]: L2SkillLearn }
let transformSkillTree: { [key: number]: L2SkillLearn }
let commonSkillTree: { [key: number]: L2SkillLearn }

// Other skill trees
let nobleSkillTree: { [key: number]: L2SkillLearn }
let heroSkillTree: { [key: number]: L2SkillLearn }
let gameMasterSkillTree: { [key: number]: L2SkillLearn }
let gameMasterAuraSkillTree: { [key: number]: L2SkillLearn }

/** Parent class IDs are read and stored in this map, to allow easy customization. */
let parentClassMap: { [key: number]: number }

const query = 'select * from skill_trees'

export const SQLiteSkillTreesData: SkillTreesDataApi = {

    getCollectSkillTree(): { [key: number]: L2SkillLearn } {
        return collectSkillTree
    },

    getCommonSkillTree(): { [key: number]: L2SkillLearn } {
        return commonSkillTree
    },

    getFishingSkillTree(): { [key: number]: L2SkillLearn } {
        return fishingSkillTree
    },

    getGameMasterAuraSkillTree(): { [key: number]: L2SkillLearn } {
        return gameMasterAuraSkillTree
    },

    getGameMasterSkillTree(): { [key: number]: L2SkillLearn } {
        return gameMasterSkillTree
    },

    getHeroSkillTree(): { [key: number]: L2SkillLearn } {
        return heroSkillTree
    },

    getNobleSkillTree(): { [key: number]: L2SkillLearn } {
        return nobleSkillTree
    },

    getParentClassMap(): { [key: number]: number } {
        return parentClassMap
    },

    getPledgeSkillTree(): { [key: number]: L2SkillLearn } {
        return pledgeSkillTree
    },

    getSubClassSkillTree(): { [key: number]: L2SkillLearn } {
        return subClassSkillTree
    },

    getSubPledgeSkillTree(): { [key: number]: L2SkillLearn } {
        return subPledgeSkillTree
    },

    getTransferSkillTrees(): { [key: number]: { [key: number]: L2SkillLearn } } {
        return transferSkillTrees
    },

    getTransformSkillTree(): { [key: number]: L2SkillLearn } {
        return transformSkillTree
    },

    getClassSkillTrees() {
        return classSkillTrees
    },


    async load() {
        classSkillTrees = {}
        transferSkillTrees = {}
        collectSkillTree = {}
        fishingSkillTree = {}
        pledgeSkillTree = {}
        subClassSkillTree = {}
        subPledgeSkillTree = {}
        transformSkillTree = {}
        commonSkillTree = {}
        nobleSkillTree = {}
        heroSkillTree = {}
        gameMasterSkillTree = {}
        gameMasterAuraSkillTree = {}
        parentClassMap = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, Helper.processSkillTree )

        return [
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( classSkillTrees ) } class trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( transferSkillTrees ) } transfer trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( collectSkillTree ) } collect trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( fishingSkillTree ) } fishing trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( pledgeSkillTree ) } pledge trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( subClassSkillTree ) } subclass trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( subPledgeSkillTree ) } sub-pledge trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( transformSkillTree ) } transform trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( commonSkillTree ) } common trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( nobleSkillTree ) } noble trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( gameMasterSkillTree ) } game master trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( gameMasterAuraSkillTree ) } game master aura trees`,
            `${ logSymbols.success } SkillTreesData loaded ${ _.size( parentClassMap ) } class mappings`,
        ]
    }
}

const Helper = {
    processSkillTree( databaseItem: any ) {
        let { type, classId, parentClassId } = databaseItem
        let skillItems = JSON.parse( databaseItem[ 'skills_json' ] )

        _.each( skillItems, ( item: any ) => {
            Helper.createSkill( item, type, classId, parentClassId )
        } )
    },

    createSkill( data: any, type: string, classId: number, parentClassId: number ) {
        if ( classId > -1 && parentClassId > -1 && classId !== parentClassId && !parentClassMap[ classId ] ) {
            parentClassMap[ classId ] = parentClassId
        }

        let skill = new L2SkillLearn()

        _.assign( skill, _.pick( data, [ 'skillName', 'skillId', 'getLevel' ] ) )

        skill.skillLevel = _.get( data, 'skillLvl', 1 )
        skill.autoGet = _.get( data, 'autoGet', false )
        skill.levelUpSp = _.get( data, 'levelUpSp', 0 )
        skill.residenceSkill = _.get( data, 'residenceSkill', false )
        skill.learnedByNpc = _.get( data, 'learnedByNpc', false )
        skill.learnedByFS = _.get( data, 'learnedByFS', false )

        if ( data.item ) {
            _.each( _.castArray( data.item ), ( item: any ) => {
                let { count, id } = item
                skill.addRequiredItem( {
                    id,
                    count
                } )
            } )
        }

        if ( data.preRequisiteSkill ) {
            _.each( _.castArray( data.preRequisiteSkill ), ( item: any ) => {
                let { id, lvl } = item
                skill.addPrerequisiteSkill( new SkillHolder( id, lvl ) )
            } )
        }

        if ( data.race ) {
            _.each( _.castArray( data.race ), ( name: string ) => {
                skill.addRace( Race[ name ] )
            } )
        }

        if ( data.residenceId ) {
            skill.residenceIds = _.castArray( data.residenceId )
        }

        if ( data.socialClass ) {
            skill.socialClass = SocialClass[ data.socialClass as string ]
        }

        if ( data.subClassConditions ) {
            _.each( _.castArray( data.subClassConditions ), ( item: any ) => {
                let { lvl, slot } = item
                skill.addSubclassConditions( slot, lvl )
            } )
        }

        let hashCode = DataManager.getSkillData().getHashCode( skill.getSkillId(), skill.getSkillLevel() )
        switch ( type ) {
            case 'classSkillTree':
                if ( classId !== -1 ) {
                    if ( !classSkillTrees[ classId ] ) {
                        classSkillTrees[ classId ] = {}
                    }

                    classSkillTrees[ classId ][ hashCode ] = skill
                } else {
                    commonSkillTree[ hashCode ] = skill
                }
                break
            case 'transferSkillTree':
                if ( !transferSkillTrees[ classId ] ) {
                    transferSkillTrees[ classId ] = {}
                }

                transferSkillTrees[ classId ][ hashCode ] = skill
                break
            case 'collectSkillTree':
                collectSkillTree[ hashCode ] = skill
                break
            case 'fishingSkillTree':
                fishingSkillTree[ hashCode ] = skill
                break
            case 'pledgeSkillTree':
                pledgeSkillTree[ hashCode ] = skill
                break
            case 'subClassSkillTree':
                subClassSkillTree[ hashCode ] = skill
                break
            case 'subPledgeSkillTree':
                subPledgeSkillTree[ hashCode ] = skill
                break
            case 'transformSkillTree':
                transformSkillTree[ hashCode ] = skill
                break
            case 'nobleSkillTree':
                nobleSkillTree[ hashCode ] = skill
                break
            case 'heroSkillTree':
                heroSkillTree[ hashCode ] = skill
                break
            case 'gameMasterSkillTree':
                gameMasterSkillTree[ hashCode ] = skill
                break
            case 'gameMasterAuraSkillTree':
                gameMasterAuraSkillTree[ hashCode ] = skill
                break
            default:
                console.log( `${ logSymbols.error } SkillTreesData uknown skill type ${ type }` )
                break
        }
    }
}