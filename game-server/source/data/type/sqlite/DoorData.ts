import { DoorDataApi } from '../../interface/DoorDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { L2DoorTemplate } from '../../../gameService/models/actor/templates/L2DoorTemplate'
import _ from 'lodash'
import logSymbols from 'log-symbols'
import { DoorOpenFlags } from '../../../gameService/enums/DoorOpenFlags'

const query = 'select * from door_data'

let templates: Record<number, L2DoorTemplate>
let groups: Record<number, Array<number>>
let idByName : Record<string, number>

export const SQLiteDoorData: DoorDataApi = {
    getIdByName( name: string ): number {
        return idByName[ name ]
    },

    getAllTemplates(): Array<L2DoorTemplate> {
        return Object.values( templates )
    },

    getDoorTemplate( id: number ): L2DoorTemplate {
        return templates[ id ]
    },

    getDoorsByGroup( name: string ): Array<number> {
        return groups[ name ]
    },

    async load(): Promise<Array<string>> {
        templates = {}
        groups = {}
        idByName = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, DoorHelper.processItem )
        return [
            `${ logSymbols.success } DoorData : loaded ${ _.size( templates ) } templates and ${ _.size( groups ) } groups.`,
        ]
    }
}

const DoorHelper = {
    processItem( databaseItem: any ) {
        let parsedData = JSON.parse( databaseItem[ 'properties_json' ] ) as Record<string, unknown>

        if ( !_.has( parsedData, 'baseHpMax' ) ) {
            parsedData.baseHpMax = 1
        }

        let template = DoorHelper.createTemplate( databaseItem, parsedData )

        if ( template.getGroupName() ) {
            if ( !groups[ template.getGroupName() ] ) {
                groups[ template.getGroupName() ] = []
            }

            groups[ template.getGroupName() ].push( template.getId() )
        }

        templates[ template.getId() ] = template
        idByName[ databaseItem.name ] = databaseItem.id
    },

    createTemplate( databaseItem: any, data: any ): L2DoorTemplate {
        let template = new L2DoorTemplate()

        template.doorId = databaseItem.id
        template.name = databaseItem.name
        template.position = data.pos

        template.height = _.get( data, 'height' )
        template.nodeZ = _.get( data, 'nodeZ' )
        template.nodeX = _.get( data, 'nodeX' )
        template.nodeY = _.get( data, 'nodeY' )

        template.emitter = _.get( data, 'emitter_id', 0 )
        template.showHp = _.get( data, 'hp_showable', true )
        template.isWall = _.get( data, 'is_wall', false )
        template.groupName = _.get( data, 'group', null )

        template.childDoorId = _.get( data, 'child_id_event', -1 )

        let masterDoorMap = {
            'act_open': 1,
            'act_close': -1,
            'act_nothing': 0
        }

        template.masterDoorClose = masterDoorMap[ _.get( data, 'master_close_event', 'act_nothing' ) as string ]
        template.masterDoorOpen = masterDoorMap[ _.get( data, 'master_open_event', 'act_nothing' ) as string ]
        template.isTargetable = _.get( data, 'targetable', true )
        template.defaultStatus = _.get( data, 'default_status', 'close' ) === 'open'

        template.closeTime = _.get( data, 'close_time', -1 )
        template.level = _.get( data, 'level', 0 )
        template.openType = _.get( data, 'open_method', 0 )
        template.checkCollision = _.get( data, 'check_collision', true )

        if ( ( template.openType & DoorOpenFlags.Time ) === DoorOpenFlags.Time ) {
            template.openTime = _.get( data, 'open_time', 1 )
            template.randomTime = _.get( data, 'random_time', -1 )
        }

        template.isAttackableDoor = _.get( data, 'is_attackable', false )
        template.clanhallId = _.get( data, 'clanhall_id', 0 )
        template.stealth = _.get( data, 'stealth', false )

        template.collisionHeight = _.get( data, 'height', 1 )

        let [ nodeX, posX ] = template.nodeX
        let [ nodeY, posY ] = template.nodeY

        let collisionRadius = Math.min( Math.abs( nodeX - posX ), Math.abs( nodeY - posY ) )
        if ( collisionRadius < 20 ) {
            collisionRadius = 20
        }

        template.collisionRadius = collisionRadius

        return template
    }
}