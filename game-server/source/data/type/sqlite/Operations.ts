import { L2DataOperationsApi } from '../../interface/OperationsApi'
import { SQLiteDataEngine } from '../../engines/sqlite'

export const SQLiteDataOperations: L2DataOperationsApi = {
    shutdown() {
        return SQLiteDataEngine.shutdown()
    }
}