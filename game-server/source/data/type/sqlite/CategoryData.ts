import { CategoryDataApi } from '../../interface/CategoryDataApi'
import { CategoryType } from '../../../gameService/enums/CategoryType'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'
import logSymbols from 'log-symbols'

let CATEGORIES : { [ key: number ]: Array<number> } = {}

const query = 'select * from category_data'

export const SQLiteCategoryData : CategoryDataApi = {
    getCategoryByType( type: CategoryType ): Array<number> {
        return CATEGORIES[ type ]
    },

    isInCategory( type: CategoryType, id: number ): boolean {
        let categoryData : Array<number> = SQLiteCategoryData.getCategoryByType( type )
        if ( categoryData ) {
            return categoryData.includes( id )
        }

        return false
    },

    async load() {
        CATEGORIES = {}
        let output = []

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) => {
            let { name } = databaseItem
            let key = CategoryType[ name ]

            if ( _.isUndefined( key ) ) {
                output.push( `${logSymbols.error} Missing category '${name}'` )
                return
            }

            let values = JSON.parse( databaseItem[ 'ids_json' ] )
            CATEGORIES[ key ] = values
        } )

        output.push( `${logSymbols.success} CategoryData loaded ${_.size( CATEGORIES )} types.` )

        return output
    }
}