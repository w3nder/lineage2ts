import { L2CycleStepDataApi, L2CycleStepDataItem } from '../../interface/CycleStepDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { JsonHelper } from './helpers/JsonHelper'

export const SQLiteCycleStepData : L2CycleStepDataApi = {
    getAll(): Array<L2CycleStepDataItem> {
        const query = 'select * from cycle_step_data'
        let databaseItems = SQLiteDataEngine.getMany( query )

        return databaseItems.map( ( databaseItem: any ) : L2CycleStepDataItem => {
            return {
                areaIds: JsonHelper.parse<Array<string>>( databaseItem[ 'areaIdsJson' ] ),
                changeTimeCron: databaseItem[ 'changeTimeCron' ],
                cycleId: databaseItem[ 'id' ],
                doorIds: JsonHelper.parse( databaseItem[ 'openDoorIdsJson' ] ),
                dropDuration: databaseItem[ 'dropTime' ],
                inverval: {
                    points: databaseItem[ 'intervalPoint' ],
                    time: databaseItem[ 'intervalTime' ]
                },
                lockDuration: databaseItem[ 'lockTime' ],
                mapId: databaseItem[ 'mapId' ],
                mapPoint: JsonHelper.parse( databaseItem[ 'mapPoint' ] ),
                points: databaseItem[ 'pointLimit' ],
                respawnPoint: JsonHelper.parse( databaseItem[ 'respawnPoint' ] ),
                respawnPvpPoint: JsonHelper.parse( databaseItem[ 'respawnPvpPoint' ] ),
                respawnRange: JsonHelper.parse( databaseItem[ 'respawnRangeJson' ] ),
                stepId: databaseItem[ 'stepId' ]
            }
        } )
    }
}