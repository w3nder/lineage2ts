import { L2ItemAuctionDataApi, L2ItemAuctionDataItem } from '../../interface/ItemAuctionDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteItemAuctionData : L2ItemAuctionDataApi = {
    async getAll(): Promise<Array<L2ItemAuctionDataItem>> {
        const query = 'select * from item_auctions'
        const databaseItems = SQLiteDataEngine.getMany( query )

        return _.map( databaseItems, ( databaseItem: any ) : L2ItemAuctionDataItem => {
            return {
                amount: databaseItem[ 'amount' ],
                auctionId: databaseItem[ 'auctionId' ],
                durationMinutes: databaseItem[ 'durationMinutes' ],
                initialBid: databaseItem[ 'initialBid' ],
                itemId: databaseItem[ 'itemId' ],
                npcId: databaseItem[ 'npcId' ]
            }
        } )
    }
}