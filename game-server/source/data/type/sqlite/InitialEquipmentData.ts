import { InitialEquipmentDataApi } from '../../interface/InitialEquipmentDataApi'
import { ConfigManager } from '../../../config/ConfigManager'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'
import { PlayerEquipmentItem } from '../../../gameService/interface/ItemDefinition'

let equipmentList : { [ key: number ]: Array<PlayerEquipmentItem> } = {}

const getQuery = () : string => {
    let tableName = ConfigManager.character.initialEquipmentEvent() ? 'initial_equipment_event' : 'initial_equipment'
    return `select * from ${tableName}`
}

export const SQLiteInitialEquipmentData : InitialEquipmentDataApi = {
    getEquipmentList( classId: number ): Array<PlayerEquipmentItem> {
        return equipmentList[ classId ]
    },

    async load() {
        equipmentList = {}
        let databaseItems: Array<any> = SQLiteDataEngine.getMany( getQuery() )

        _.each( databaseItems, ( databaseItem: any ) => {
            let { classId, itemId, count, equipped } = databaseItem

            if ( !equipmentList[ classId ] ) {
                equipmentList[ classId ] = []
            }

            equipmentList[ classId ].push( {
                id: itemId,
                count,
                isEquipped: equipped === 'true'
            } )
        } )

        return [
            `${logSymbols.success} InitialEquipmentData loaded ${_.size( equipmentList )} classes from ${_.size( databaseItems )} records.`
        ]
    }
}