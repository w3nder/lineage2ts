import { L2ArmorSet } from '../../../gameService/models/L2ArmorSet'
import { ArmorSetsDataApi } from '../../interface/ArmorSetsDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { getExistingSkill } from '../../../gameService/models/holders/SkillHolder'
import _ from 'lodash'
import logSymbols from 'log-symbols'
import { Skill } from '../../../gameService/models/Skill'

let itemIdToSet : Record<number, L2ArmorSet>
let allSets: Array<L2ArmorSet>
let idToSet : Record<number, L2ArmorSet>

const query = 'select * from armor_sets order by name'
const supportedItemIdProperties : Array<string> = [ 'feet', 'gloves','head','legs','shield' ]
const supportedAttributes : Array<string> = [ 'str', 'con', 'dex', 'int', 'men', 'wit' ]

function applyProperties( json: string, item: L2ArmorSet ) : void {
    if ( json ) {
        let properties = JSON.parse( json ) as Record<string, number>

        for ( const name of supportedAttributes ) {
            let value = properties[ name ]
            if ( value && item[ name ] === 0 ) {
                item[ name ] = value
            }
        }
    }
}

function assignItemIds( dataItem: any, propertyName: string, item: L2ArmorSet, setIds: Set<number> ) : void {
    let itemIds = dataItem[ propertyName ]
    if ( Array.isArray( itemIds ) ) {
        item[ propertyName ] = new Set<number>( itemIds )
        itemIds.forEach( ( id : number ) => setIds.add( id ) )
    }
}

function addSetItems( json: string, item: L2ArmorSet ) : Set<number> {
    if ( !json ) {
        return
    }

    let dataItem = JSON.parse( json )
    let itemIds = new Set<number>()

    item.chestId = dataItem[ 'chest' ]
    supportedItemIdProperties.forEach( ( propertyName : string ) => assignItemIds( dataItem, propertyName, item, itemIds ) )

    return itemIds
}

function getSkill( value: string ) : Skill {
    let skillChunks : Array<string> = value.split( '-' )
    return getExistingSkill( parseInt( skillChunks[ 0 ], 10 ), parseInt( skillChunks[ 1 ], 10 ) )
}

export const SQLiteArmorSetsData : ArmorSetsDataApi = {
    getById( id: number ): L2ArmorSet {
        return idToSet[ id ]
    },

    getAll(): ReadonlyArray<L2ArmorSet> {
        return allSets
    },

    getArmorSet( itemId: number ): L2ArmorSet {
        return itemIdToSet[ itemId ]
    },

    async load() {
        itemIdToSet = {}
        allSets = []
        idToSet = {}

        let databaseItems : Array<any> = SQLiteDataEngine.getMany( query )

        databaseItems.forEach( ( databaseItem: unknown ) => {
            let item = new L2ArmorSet( databaseItem[ 'id' ], databaseItem[ 'name' ] )

            item.itemIds = addSetItems( databaseItem[ 'items_json' ], item )
            applyProperties( databaseItem[ 'attributes_json' ], item )

            if ( databaseItem[ 'skills_json' ] ) {
                let skillProperties = JSON.parse( databaseItem[ 'skills_json' ] ) as Record<string, string>

                if ( skillProperties.enchant6 ) {
                    item.enchant6Skill = getSkill( skillProperties.enchant6 )
                }

                if ( skillProperties.shield ) {
                    item.shieldSkill = getSkill( skillProperties.shield )
                }

                if ( skillProperties.set ) {
                    item.setSkill = getSkill( skillProperties.set )
                }
            }

            itemIdToSet[ item.chestId ] = item
            item.itemIds.forEach( ( id: number ) => itemIdToSet[ id ] = item )
            allSets.push( item )
            idToSet[ item.id ] = item

            item.itemIds.add( item.chestId )
        } )

        return [
            `${logSymbols.success} ArmorSetsData : loaded ${databaseItems.length} armor sets used by ${_.size( itemIdToSet )} items.`,
        ]
    }
}