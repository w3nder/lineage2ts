import { L2FishingMonsterDataApi, L2FishingMonsterDataItem } from '../../interface/FishingMonsterDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const query = 'select * from fishing_monsters'

type FishingMonsterCollection = { [ level: number ]: L2FishingMonsterDataItem }
let allMonsters: FishingMonsterCollection

export const SQLiteFishingMonsterData: L2FishingMonsterDataApi = {
    getFishingMonster( level: number ): L2FishingMonsterDataItem {
        return allMonsters[ level ]
    },

    async load(): Promise<Array<string>> {
        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )
        allMonsters = _.reduce( databaseItems, Helper.processItem, {} )

        return [
            `${ logSymbols.success } FishingMonsters loaded ${ _.size( databaseItems ) } monsters for ${ _.size( allMonsters ) } player levels.`,
        ]
    },
}

const Helper = {
    processItem( allItems: FishingMonsterCollection, databaseItem: any ): FishingMonsterCollection {
        let item: L2FishingMonsterDataItem = {
            chance: databaseItem[ 'probability' ],
            id: databaseItem[ 'id' ],
            maximumLevel: databaseItem[ 'userMaxLevel' ],
            minimumLevel: databaseItem[ 'userMinLevel' ],
        }

        _.range( item.minimumLevel, item.maximumLevel + 1 ).forEach( ( level: number ) => {
            allItems[ level ] = item
        } )

        return allItems
    },
}