import { L2SpawnTerritoriesDataApi, L2SpawnTerritoryItem } from '../../interface/SpawnTerritoriesDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'
import { DeserializationFormat, RoaringBitmap32 } from 'roaring'

const query : string = 'select * from spawn_territory_ex'

export const SQLiteSpawnTerritories : L2SpawnTerritoriesDataApi = {
    getAll(): Record<string, L2SpawnTerritoryItem> {
        let databaseItems = SQLiteDataEngine.getMany( query )

        return _.reduce( databaseItems, ( allItems : Record<string, L2SpawnTerritoryItem>, databaseItem: Object ) : Record<string, L2SpawnTerritoryItem> => {
            let territory = createSpawnTerritory( databaseItem )

            allItems[ territory.id ] = territory

            return allItems
        }, {} )
    }
}

function createSpawnTerritory( databaseItem: Object ) : L2SpawnTerritoryItem {
    let points = JSON.parse( databaseItem[ 'points_json' ] )
    let parameters = databaseItem[ 'parameters_json' ] ? JSON.parse( databaseItem[ 'parameters_json' ] ) : null
    let geometryPoints = null

    if ( databaseItem[ 'geometryPoints' ] ) {
        geometryPoints = new RoaringBitmap32().deserialize( databaseItem[ 'geometryPoints' ], DeserializationFormat.croaring )
        geometryPoints.shrinkToFit()
    }

    return {
        geometryPoints,
        name: databaseItem[ 'name' ],
        id: databaseItem[ 'id' ],
        maxZ: databaseItem[ 'maxZ' ],
        minZ: databaseItem[ 'minZ' ],
        parameters,
        points,
        regionX: databaseItem[ 'regionX' ],
        regionY: databaseItem[ 'regionY' ]
    }
}