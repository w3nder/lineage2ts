import { L2GeoRegionDataApi } from '../../interface/GeoRegionDataApi'
import { SQLiteGeoDataEngine } from '../../engines/sqlite-geo'

export const SQLiteGeoRegionData : L2GeoRegionDataApi = {
    isInitialized(): boolean {
        return SQLiteGeoDataEngine.isStarted()
    },

    async deInitialize(): Promise<void> {
        if ( !SQLiteGeoDataEngine.isStarted() ) {
            return
        }

        return SQLiteGeoDataEngine.shutdown()
    },

    async initialize(): Promise<void> {
        if ( SQLiteGeoDataEngine.isStarted() ) {
            return
        }

        return SQLiteGeoDataEngine.start()
    },

    async getRegionData( regionX: number, regionY: number ): Promise<Buffer> {
        const query = 'select data from geoindex where regionX = ? and regionY = ?'
        let databaseItem = SQLiteGeoDataEngine.getOne( query, regionX, regionY )

        if ( !databaseItem ) {
            return
        }

        return databaseItem.data
    },

    shutdown() {
        SQLiteGeoDataEngine.shutdown()
    }
}