import { L2PlayerAccessDataApi, L2PlayerAccessDataItem } from '../../interface/PlayerAccessDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'

export const SQLitePlayerAccessData : L2PlayerAccessDataApi = {
    async getAll(): Promise<Array<L2PlayerAccessDataItem>> {
        const query = 'select * from player_access_levels'
        let dataItems: Array<unknown> = SQLiteDataEngine.getMany( query )

        return dataItems.map( ( item: unknown ) : L2PlayerAccessDataItem => {
            let commandsData = _.attempt( JSON.parse, item[ 'commands_json' ] )
            let permissionsData = _.attempt( JSON.parse, item[ 'permissions_json' ] )

            return {
                commands: _.isError( commandsData ) ? {} : commandsData,
                level: item[ 'level' ] as number,
                name: item[ 'name' ] as string,
                nameColor: item[ 'nameColor' ] as string,
                permissions: _.isError( permissionsData ) ? [] : permissionsData,
                titleColor: item[ 'titleColor' ] as string
            }
        } )
    }
}