import { L2StaticObjectsDataApi } from '../../interface/StaticObjectsDataApi'
import { L2StaticObjectInstance } from '../../../gameService/models/actor/instance/L2StaticObjectInstance'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'
import { L2StaticObjectType } from '../../../gameService/enums/L2StaticObjectType'
import logSymbols from 'log-symbols'

const query = 'select * from static_objects'

let allObjects: Record<number, Array<L2StaticObjectInstance>>

export const SQLiteStaticObjectsData: L2StaticObjectsDataApi = {
    getType( type: L2StaticObjectType ): Array<L2StaticObjectInstance> {
        return allObjects[ type ]
    },

    async load(): Promise<Array<string>> {
        allObjects = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, Helper.createObject )
        return [
            `${ logSymbols.success } StaticObjects : loaded ${ _.size( databaseItems ) } entries.`
        ]
    }
}

const Helper = {
    createObject( databaseItem: any ) {
        let object = new L2StaticObjectInstance( databaseItem.id )

        object.type = _.get( databaseItem, 'type', L2StaticObjectType.MapSign )
        object.name = _.get( databaseItem, 'name', 'unknown' )

        object.mapLocation = {
            name: _.get( databaseItem, 'texture', 'none' ),
            x: _.get( databaseItem, 'mapX', 0 ),
            y: _.get( databaseItem, 'mapY', 0 )
        }

        object.spawnMe(
                _.get( databaseItem, 'x', 0 ),
                _.get( databaseItem, 'y', 0 ),
                _.get( databaseItem, 'z', 0 )
        )

        if ( !allObjects[ object.type ] ) {
            allObjects[ object.type ] = []
        }

        allObjects[ object.type ].push( object )
    }
}