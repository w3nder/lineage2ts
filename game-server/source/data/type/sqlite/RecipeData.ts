import { RecipeDataApi } from '../../interface/RecipeDataApi'
import { L2RecipeDefinition } from '../../../gameService/models/l2RecipeDefinition'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { StatType } from '../../../gameService/enums/StatType'
import { L2RecipeItem } from '../../../gameService/models/l2RecipeItem'
import _ from 'lodash'
import logSymbols from 'log-symbols'

let allRecipes: { [key: number]: L2RecipeDefinition }
let recipesByItemId : { [key: number]: L2RecipeDefinition }

const query = 'select * from recipe_data'

export const SQLiteRecipeData: RecipeDataApi = {
    getRecipeByItemId( itemId: number ): L2RecipeDefinition {
        return recipesByItemId[ itemId ]
    },

    getRecipeList( recipeId: number ): L2RecipeDefinition {
        return allRecipes[ recipeId ]
    },

    async load() {
        allRecipes = {}
        recipesByItemId = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, RecipeHelper.createRecipe )
        return [
            `${ logSymbols.success } RecipeData loaded ${ _.size( allRecipes ) } recipes from ${ _.size( databaseItems ) } records.`
        ]
    }
}

const RecipeHelper = {
    createRecipe( databaseItem: any ): void {
        let recipe = {} as L2RecipeDefinition

        recipe.id = databaseItem[ 'id' ]
        recipe.recipeItemId = databaseItem[ 'recipeId' ]
        recipe.recipeName = databaseItem[ 'name' ]
        recipe.successRate = databaseItem[ 'successRate' ]
        recipe.productionItemId = databaseItem[ 'productionId' ]

        recipe.productionAmount = databaseItem[ 'productionCount' ]
        recipe.rareItemId = databaseItem[ 'productionRareId' ]
        recipe.rareCount = databaseItem[ 'productionRareCount' ]
        recipe.rarity = databaseItem[ 'productionRareRarity' ]

        recipe.isDwarvenRecipe = databaseItem[ 'type' ] === 'dwarven'
        recipe.level = databaseItem[ 'craftLevel' ]

        let statName = databaseItem[ 'statUseName' ]
        let statValue = databaseItem[ 'statUseValue' ]
        recipe.statConsume = {
            type: StatType[ statName as string ],
            value: statValue
        }

        if ( databaseItem[ 'ingredients_json' ] ) {
            let ingredients = JSON.parse( databaseItem[ 'ingredients_json' ] )
            recipe.items = _.map( ingredients, ( value: number, key: string ) : L2RecipeItem => {
                return {
                    itemId: _.parseInt( key ),
                    quantity: value
                }
            } )
        }

        allRecipes[ recipe.id ] = recipe
        recipesByItemId[ recipe.recipeItemId ] = recipe
    },
}