import { L2AugmentationSkillsDataApi, L2AugmentationSkillsDataItem } from '../../interface/AugmentationSkillsDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteAugmentationSkillData : L2AugmentationSkillsDataApi = {
    getAll(): Array<L2AugmentationSkillsDataItem> {
        const query = 'select * from augmentation_skills'
        let databaseItems : Array<any> = SQLiteDataEngine.getMany( query )
        return _.map( databaseItems, ( databaseItem: any ) : L2AugmentationSkillsDataItem => {
            return {
                augmentationId: databaseItem[ 'id' ],
                skillId: databaseItem[ 'skillId' ],
                skillLevel: databaseItem[ 'skillLevel' ],
                type: databaseItem[ 'type' ]
            }
        } )
    }
}