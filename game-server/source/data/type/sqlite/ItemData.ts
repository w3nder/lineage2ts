import { ItemDataApi } from '../../interface/ItemDataApi'
import { EmptyElementals, EmptyFunctionTemplates, L2Item } from '../../../gameService/models/items/L2Item'
import { L2Armor } from '../../../gameService/models/items/L2Armor'
import { L2Weapon } from '../../../gameService/models/items/L2Weapon'
import { L2EtcItem } from '../../../gameService/models/items/L2EtcItem'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { CrystalType } from '../../../gameService/models/items/type/CrystalType'
import { ActionType } from '../../../gameService/models/items/type/ActionType'
import { WeaponType } from '../../../gameService/models/items/type/WeaponType'
import { ItemType2 } from '../../../gameService/enums/items/ItemType2'
import { EtcItemType } from '../../../gameService/enums/items/EtcItemType'
import { L2ExtractableProduct } from '../../../gameService/models/L2ExtractableProduct'
import { ArmorType } from '../../../gameService/enums/items/ArmorType'
import { Condition } from '../../../gameService/models/conditions/Condition'
import { FunctionTemplate } from '../../../gameService/models/stats/functions/FunctionTemplate'
import { ExtractableItems } from '../../../gameService/handler/itemhandlers/ExtractableItems'
import { AvailableItemHandlers } from '../../../gameService/handler/ItemHandler'
import { ItemTypes } from '../../../gameService/values/InventoryValues'
import _ from 'lodash'
import logSymbols from 'log-symbols'
import { getExistingSkill } from '../../../gameService/models/holders/SkillHolder'
import { ItemTableSlots } from '../../../gameService/enums/L2ItemSlots'
import {
    DefaultDefenceElementAttributes,
    DefenceElementTypes,
    ElementalType
} from '../../../gameService/enums/ElementalType'
import { L2ItemDefaults } from '../../../gameService/enums/L2ItemDefaults'
import { createCondition } from './helpers/ItemConditionHelper'
import { Elementals } from '../../../gameService/models/Elementals'
import { Stats } from '../../../gameService/models/stats/Stats'
import { StatFunctionName } from '../../../gameService/enums/StatFunction'
import { ItemInstanceType } from '../../../gameService/models/items/ItemInstanceType'
import { createOrGetFunctionTemplate, getFunctionTemplateAmount } from './helpers/FunctionHelper'

let allTemplates: { [ key: number ]: L2Item } = {}
let allItemConditions : Record<number, Array<Condition>> = {}

const itemQuery = 'select * from items order by id'
const conditionQuery = 'select * from item_conditions'

let currentMessageLog: Array<string> = []

export const SQLiteItemData: ItemDataApi = {
    getIdsByPartialName( name: string ): Array<number> {
        const nameQuery = 'select id from items where name like ? order by id'
        const results = SQLiteDataEngine.getManyWithParameters( nameQuery, `%${name}%` )

        if ( !results ) {
            return []
        }

        return results.map( ( databaseItem: any ) : number => databaseItem.id )
    },

    getTemplate( id: number ): L2Item {
        return allTemplates[ id ]
    },

    async load() {
        allTemplates = {}
        allItemConditions = {}
        currentMessageLog = []

        SQLiteDataEngine.getMany( conditionQuery ).forEach( ItemHelper.processCondition )
        SQLiteDataEngine.getMany( itemQuery ).forEach( ItemHelper.processItem )

        return [
            `${ logSymbols.success } Items data : loaded ${ _.size( allTemplates ) } items using ${ _.size( allItemConditions ) } conditions and ${ getFunctionTemplateAmount() } existing function templates`,
            ...currentMessageLog
        ]
    }
}

const shotActionTypes = new Set<ActionType>( [
    ActionType.SOULSHOT,
    ActionType.SUMMON_SOULSHOT,
    ActionType.SUMMON_SPIRITSHOT,
    ActionType.SPIRITSHOT
] )

enum DepositType {
    None = 0,
    Warehouse = 1,
    ClanWarehouse = 2,
    CastleWarehouse = 4,
    Other = 8
}

const ItemHelper = {
    processCondition( databaseItem: Record<string, string | number> ) : void {
        let definitions : Record<string, string | number> = _.attempt( JSON.parse, databaseItem.definition_json )
        if ( _.isError( definitions ) ) {
            currentMessageLog.push( `${ logSymbols.error } Items : unable to parse condition definition for id=${databaseItem.id} and json=${databaseItem.definition_json}` )
            return
        }

        allItemConditions[ databaseItem.id as number ] = _.reduce( definitions, ( allConditions: Array<Condition>, value: string | number | boolean, key: string ) : Array<Condition> => {
            let condition = createCondition( key, value )
            if ( condition ) {
                allConditions.push( condition )
            }

            return allConditions
        }, [] )
    },

    processItem( databaseItem: Record<string, unknown> ): void {
        let item: L2Item = ItemHelper.createItem( databaseItem )

        if ( !item ) {
            return
        }

        allTemplates[ item.getId() ] = item

        item.functionTemplates = ItemHelper.createFunctionTemplates( databaseItem, item.instanceType )
        item.elementals = ItemHelper.createElementals( databaseItem )

        if ( databaseItem.equipConditionId ) {
            item.equipConditions = allItemConditions[ databaseItem.equipConditionId as number ]

            if ( !item.equipConditions ) {
                currentMessageLog.push( `${logSymbols.error} Items : unable to get equip conditions with id=${databaseItem.equipConditionId} for itemId=${item.itemId}` )
            }
        }

        if ( databaseItem.useConditionId ) {
            item.useConditions = allItemConditions[ databaseItem.useConditionId as number ]

            if ( !item.useConditions ) {
                currentMessageLog.push( `${logSymbols.error} Items : unable to get use conditions with id=${databaseItem.useConditionId} for itemId=${item.itemId}` )
            }
        }

        item.elementalDefenceAttributes = ItemHelper.createDefenceAttributes( item )
    },

    createItem( databaseItem: Record<string, unknown> ): L2Item {
        switch ( databaseItem.type ) {
            case 'weapon':
                return ItemHelper.createL2Weapon( databaseItem )

            case 'accessary':
            case 'armor':
                return ItemHelper.createL2Armor( databaseItem )

            default:
                return ItemHelper.createL2EtcItem( databaseItem )
        }
    },

    createL2Weapon( databaseItem: any ): L2Item {
        let item = new L2Weapon()

        ItemHelper.assignL2ItemProperties( item, databaseItem )

        item.type = databaseItem.weaponType ? WeaponType[ databaseItem.weaponType.toUpperCase() as string ] : WeaponType.NONE
        item.isMagicWeaponValue = databaseItem.isMagicWeapon === 1
        item.soulShotCount = databaseItem.ssCount ?? 0
        item.spiritShotCount = databaseItem.spsCount ?? 0

        item.randomDamage = databaseItem.randomDamage ?? 0
        item.mpConsume = databaseItem.mpConsume ?? 0
        item.baseAttackRange = databaseItem.attackRange ?? 0
        item.baseAttackAngle = databaseItem.attackAngle ?? 120

        item.changeWeaponId = databaseItem.changeWeaponId ?? 0

        if ( databaseItem.equipOption ) {
            let options = new Set<string>( databaseItem.equipOption.split( ';' ) )

            item.isForceEquip = options.has( 'force_equip' )
            item.isAttackWeapon = !options.has( 'no_attack' )
            item.useWeaponSkillsOnly = options.has( 'only_use_weapon_skill' )
        }

        if ( databaseItem.reducedSS ) {
            let chunks = databaseItem.reducedSS.split( ';' )
            item.reducedSoulshotChance = _.defaultTo( parseFloat( chunks[ 0 ] ), 0 )
            item.reducedSoulshot = _.defaultTo( _.parseInt( chunks[ 1 ] ), 0 )
        }

        if ( databaseItem.reducedMpConsume ) {
            let chunks = databaseItem.reducedMpConsume.split( ';' )
            item.reducedMpConsumeChance = _.defaultTo( parseFloat( chunks[ 0 ] ), 0 )
            item.reducedMpConsume = _.defaultTo( _.parseInt( chunks[ 1 ] ), 0 )
        }

        if ( databaseItem.itemSkillsEnchant4 ) {
            let skillChunks = databaseItem.itemSkillsEnchant4.split( ';' )
            let currentSkill = skillChunks[ 0 ].split( '-' )
            item.enchant4Skill = getExistingSkill( _.parseInt( currentSkill[ 0 ] ), _.parseInt( currentSkill[ 1 ] ) )
        }

        if ( databaseItem.magicSkill_json ) {
            let skillData = _.attempt( JSON.parse, databaseItem.magicSkill_json )
            if ( !_.isError( skillData ) ) {
                item.skillOnMagic = getExistingSkill( skillData.id, skillData.level )
                item.skillsOnMagicChance = skillData.chance
            }
        }

        if ( databaseItem.criticalAttackSkill ) {
            let skillChunks = databaseItem.criticalAttackSkill.split( ';' )
            let currentSkill = skillChunks[ 0 ].split( '-' )
            item.skillOnCrit = getExistingSkill( _.parseInt( currentSkill[ 0 ] ), _.parseInt( currentSkill[ 1 ] ) )
        }

        return item
    },

    createL2EtcItem( databaseItem: any ): L2Item {
        let item = new L2EtcItem()

        ItemHelper.assignL2ItemProperties( item, databaseItem )

        if ( shotActionTypes.has( item.getDefaultAction() ) ) {
            item.type = EtcItemType.SHOT
        } else {
            item.type = ItemHelper.getEtcItemType( item, databaseItem )
        }

        if ( item.isQuestItem() ) {
            item.type2 = ItemType2.QUEST
        } else if ( item.getId() === ItemTypes.Adena || item.getId() === ItemTypes.AncientAdena ) {
            item.type2 = ItemType2.MONEY
        }

        if ( databaseItem.actionType ) {
            item.itemHandler = AvailableItemHandlers[ databaseItem.actionType ]

            if ( !item.itemHandler ) {
                currentMessageLog.push( `${logSymbols.error} Item id=${item.itemId} has unknown actionType=${databaseItem.actionType}` )
            }
        }
        if ( databaseItem.extractableItems_json ) {
            let extractableItems = _.attempt( JSON.parse, databaseItem.extractableItems_json )
            if ( !_.isError( extractableItems ) ) {
                item.extractableItems = extractableItems.map( ( item : any ) : L2ExtractableProduct => {
                    return {
                        chance: item.chance,
                        itemId: item.itemId,
                        max: item.maxAmount,
                        min: item.minAmount
                    }
                } )
            }
        }

        if ( !item.getItemHandler() && item.extractableItems.length > 0 ) {
            item.itemHandler = ExtractableItems
        }

        return item
    },

    createL2Armor( databaseItem: any ): L2Item {
        let item = new L2Armor()
        ItemHelper.assignL2ItemProperties( item, databaseItem )

        item.type = ArmorType[ databaseItem.armorType as string ?? ArmorType.NONE ]
        item.setDefaults()

        if ( databaseItem.itemSkillsEnchant4 ) {
            let [ skillId, skillLevel ] = databaseItem.itemSkillsEnchant4.split( '-' )
            if ( skillId && skillLevel ) {
                item.enchant4Skill = getExistingSkill( parseInt( skillId, 10 ), parseInt( skillLevel, 10 ) )
            }
        }

        return item
    },

    // TODO : rewview for correctness various flags that deal with item storage
    assignL2ItemProperties( item: L2Item, databaseItem: any ): void {

        item.itemId = databaseItem.id
        item.name = databaseItem.name
        item.icon = databaseItem.icon
        item.weight = databaseItem.weight ?? 0

        item.sortOrder = databaseItem.materialSortOrder as number
        item.equipReuseDelay = ( databaseItem.equipReuseDelay ?? 0 ) * 1000
        item.duration = databaseItem.duration ?? -1
        item.time = databaseItem.lifeTimeMinutes ?? -1

        item.bodyPart = databaseItem.slot ? ItemTableSlots[ databaseItem.slot as string ] : ItemTableSlots.none
        item.referencePrice = databaseItem.defaultPrice ?? 0
        item.crystalType = databaseItem.crystalType ? CrystalType[ databaseItem.crystalType.toUpperCase() as string ] : CrystalType.NONE
        item.crystalCount = databaseItem.crystalCount ?? 0

        item.stackable = databaseItem.isStackable === 1
        item.sellable = databaseItem.isTradable === 1
        item.dropable = databaseItem.isDroppable === 1
        item.destroyable = databaseItem.isDestructable === 1

        item.tradeable = databaseItem.forPrivateStore === 1
        item.depositable = !!databaseItem.keepType
        item.elementable = databaseItem.hasElementals === 1
        item.enchantable = databaseItem.isEnchantable === 1

        item.questItem = databaseItem.type === 'questitem'
        item.freightable = ( databaseItem.keepType & DepositType.Other ) === DepositType.Other
        item.isOlympiadRestricted = databaseItem.forOlympiad === 1
        item.forNpc = databaseItem.forNpc === 1

        item.immediateEffect = databaseItem.immediateEffect === 1
        item.exImmediateEffect = databaseItem.exImmediateEffect === 1
        item.defaultAction = databaseItem.defaultAction ? ActionType[ databaseItem.defaultAction as string ] : ActionType.NONE
        item.manaConsumeOnSkill = databaseItem.manaConsumeOnSkill ?? 0

        item.defaultEnchantLevel = databaseItem.defaultEnchantLevel ?? 0
        item.reuseDelay = databaseItem.reuseDelay ?? ( databaseItem.equipReuseDelay ?? 0 )
        item.sharedReuseGroup = databaseItem.delayShareGroup ?? 0
        item.referencePriceX2 = Math.floor( item.referencePrice * 2 )

        item.referenceId = databaseItem.referenceId

        let allSkills: string = databaseItem.itemSkills

        if ( allSkills ) {
            item.skillIds = new Set<number>()

            allSkills.split( ';' ).forEach( ( skillFragment: string ) => {
                let [ skillId, skillLevel ] = skillFragment.split( '-' ).map( value => parseInt( value, 10 ) )

                if ( !skillId || !skillLevel ) {
                    currentMessageLog.push( `${ logSymbols.warning } ItemId = ${ item.itemId } failed to load skillId = ${ skillId } and skillLevel = ${ skillLevel }` )
                    return
                }

                let skill = getExistingSkill( skillId, skillLevel )
                if ( !skill ) {
                    return
                }

                if ( skill.isPassive() ) {
                    item.passiveSkills = true
                }

                item.skills.push( skill )
                item.skillIds.add( skillId )
            } )
        }

        let unEquipSkill = databaseItem.unEquipSkill
        if ( unEquipSkill ) {
            let [ skillId, skillLevel ] = unEquipSkill.split( '-' )
            if ( skillId && skillLevel ) {
                item.unequipSkill = getExistingSkill( _.parseInt( skillId ), _.parseInt( skillLevel ) )
            }
        }

        item.common = item.itemId >= 11605 && item.itemId <= 12361
        item.heroItem = ( item.itemId >= 6611 && item.itemId <= 6621 )
            || ( item.itemId >= 9388 && item.itemId <= 9390 )
            || item.itemId === 6842
        item.pvpItem = ( ( item.itemId >= 10667 ) && ( item.itemId <= 10835 ) )
            || ( item.itemId >= 12852 && item.itemId <= 12977 )
            || ( item.itemId >= 14363 && item.itemId <= 14525 )
            || item.itemId === 14528
            || item.itemId === 14529
            || item.itemId === 14558
            || ( item.itemId >= 15913 && item.itemId <= 16024 )
            || ( item.itemId >= 16134 && item.itemId <= 16147 )
            || item.itemId === 16149
            || item.itemId === 16151
            || item.itemId === 16153
            || item.itemId === 16155
            || item.itemId === 16157
            || item.itemId === 16159
            || ( item.itemId >= 16168 && item.itemId <= 16176 )
            || ( item.itemId >= 16179 && item.itemId <= 16220 )
    },

    getEtcItemType( item: L2Item, databaseItem: any ) : EtcItemType {
        let type = databaseItem.etcItemType ? EtcItemType[ databaseItem.etcItemType as string ] : EtcItemType.NONE

        if ( type !== EtcItemType.NONE ) {
            return type
        }

        let name = item.name.toLowerCase()
        if ( name.startsWith( 'amulet:' ) ) {
            return EtcItemType.Amulet
        }

        if ( name.startsWith( 'spellbook:' ) ) {
            return EtcItemType.Spellbook
        }

        return type
    },

    createDefenceAttributes( item: L2Item ) : Array<number> {
        if ( item.isArmorItem() && item.getElementals().length > 0 ) {
            return DefenceElementTypes.map( ( element: ElementalType ) => item.getElementalValue( element ) ?? L2ItemDefaults.DefenceElementAttribute )
        }

        return DefaultDefenceElementAttributes
    },

    createFunctionTemplates( databaseItem: Record<string, unknown>, type: ItemInstanceType ) : ReadonlyArray<FunctionTemplate> {
        let allFunctions: Array<FunctionTemplate> = []

        let statOperation = type === ItemInstanceType.L2Weapon ? StatFunctionName.SetProperty : StatFunctionName.Add
        if ( databaseItem.magicDefense ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.MAGIC_DEFENCE, databaseItem.magicDefense as number ) )
        }

        if ( databaseItem.accuracyModify ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.ACCURACY_COMBAT, databaseItem.accuracyModify as number ) )
        }

        if ( databaseItem.physicalDefense ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.POWER_DEFENCE, databaseItem.physicalDefense as number ) )
        }

        if ( databaseItem.mpBonus ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.MAX_MP, databaseItem.mpBonus as number ) )
        }

        if ( databaseItem.magicDamage ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.MAGIC_ATTACK, databaseItem.magicDamage as number ) )
        }

        if ( databaseItem.physicalDamage ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.POWER_ATTACK, databaseItem.physicalDamage as number ) )
        }

        if ( databaseItem.criticalRate ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.CRITICAL_RATE, databaseItem.physicalDamage as number ) )
        }

        if ( databaseItem.attackSpeed ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.POWER_ATTACK_SPEED, databaseItem.physicalDamage as number ) )
        }

        if ( databaseItem.shieldDefenseRate ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.SHIELD_RATE, databaseItem.shieldDefenseRate as number ) )
        }

        if ( databaseItem.shieldDefense ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.SHIELD_DEFENCE, databaseItem.shieldDefense as number ) )
        }

        if ( databaseItem.attackRange ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.POWER_ATTACK_RANGE, databaseItem.attackRange as number ) )
        }


        if ( databaseItem.evasionRate ) {
            allFunctions.push( createOrGetFunctionTemplate( statOperation, Stats.EVASION_RATE, databaseItem.evasionRate as number ) )
        }

        if ( allFunctions.length === 0 ) {
            return EmptyFunctionTemplates
        }

        return allFunctions
    },

    createElementals( databaseItem: Record<string, unknown> ) : ReadonlyArray<Elementals> {
        if ( databaseItem.defenseAttributes ) {
            let attributeValues = ( databaseItem.defenseAttributes as string ).split( ';' ).map( value => Math.floor( parseInt( value, 10 ) ) )
            if ( attributeValues.length !== DefenceElementTypes.length ) {
                currentMessageLog.push( `${ logSymbols.error } Items : unable to process defence attributes for itemId=${databaseItem.id} and value=${databaseItem.defenseAttributes}` )
                return EmptyElementals
            }

            return DefenceElementTypes.reduce( ( allAttributes: Array<Elementals>, type: ElementalType, index: number ) : Array<Elementals> => {
                let value = attributeValues[ index ]
                if ( Number.isInteger( value ) && value !== 0 ) {
                    allAttributes.push( new Elementals( type, value, true ) )
                }

                return allAttributes
            }, [] )
        }

        if ( databaseItem.attackAttributes ) {
            let [ type, rawValue ] = ( databaseItem.attackAttributes as string ).split( ';' )
            if ( !type || !rawValue ) {
                currentMessageLog.push( `${ logSymbols.error } Items: unable to process attack attribute for itemId=${databaseItem.id} and value=${databaseItem.attackAttributes}` )
                return EmptyElementals
            }

            let value = parseInt( rawValue, 10 )
            if ( !Number.isInteger( value ) ) {
                currentMessageLog.push( `${ logSymbols.error } Items: unable to parse attack attribute integer value for itemId=${databaseItem.id} and raw value=${rawValue}` )
                return EmptyElementals
            }

            switch ( type ) {
                case 'fire':
                    return [ new Elementals( ElementalType.FIRE, value, false ) ]

                case 'earth':
                    return [ new Elementals( ElementalType.EARTH, value, false ) ]

                default:
                    currentMessageLog.push( `${logSymbols.error} Items: unknown attack attribute type for itemId=${databaseItem.id} and type=${type}` )
                    return EmptyElementals
            }
        }

        return EmptyElementals
    }
}

