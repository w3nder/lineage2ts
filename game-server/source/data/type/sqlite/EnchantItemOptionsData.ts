import { EnchantItemOptionsDataApi } from '../../interface/EnchantItemOptionsDataApi'
import { EnchantOptions, EnchantOptionsParameters } from '../../../gameService/models/options/EnchantOptions'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const query : string = 'select * from enchant_item_options'

let itemOptions: { [ key: number ]: { [ key: number ]: EnchantOptions } } = {}

export const SQLiteEnchantItemOptionsData: EnchantItemOptionsDataApi = {
    getOptions: ( itemId: number, enchantLevel: number ): EnchantOptions => {
        return _.get( itemOptions, [ itemId, enchantLevel ], null )
    },

    async load() : Promise<Array<string>> {
        itemOptions = {}

        let counter = 0
        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        let lines: Array<string> = []

        databaseItems.forEach( ( databaseItem: any ) => {
            let { itemId, level } = databaseItem
            let options = JSON.parse( databaseItem[ 'options_json' ] )

            if ( !Array.isArray( options ) || options.length !== 3 ) {
                lines.push( `${ logSymbols.error } EnchantItemOptions: bad options data for itemId = ${ itemId }` )
            }

            let data: EnchantOptions = {
                level,
                options: options as unknown as EnchantOptionsParameters,
            }

            if ( !itemOptions[ itemId ] ) {
                itemOptions[ itemId ] = {}
            }

            itemOptions[ itemId ][ level ] = data
            counter++
        } )

        lines.push( `${ logSymbols.success } EnchantItemOptions : loaded ${ counter } levels for ${ _.size( itemOptions ) } item ids using ${ _.size( databaseItems ) } records.`, )

        return lines
    },
}