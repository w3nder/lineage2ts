import { InstanceDataApi, L2InstanceDataItem } from '../../interface/InstanceDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

let instanceNames: { [ key: number ]: string } = {}
let instanceData : { [ nameId: string ] : L2InstanceDataItem }

const queryNames = 'select * from instance_names'
const queryData = 'select * from instance_data'

export const SQLiteInstanceData: InstanceDataApi = {
    getInstanceData( nameId: string ): L2InstanceDataItem {
        return instanceData[ nameId ]
    },

    getInstanceNames() {
        return instanceNames
    },

    async load() {
        instanceNames = {}
        instanceData = {}

        let nameItems: Array<any> = SQLiteDataEngine.getMany( queryNames )

        _.each( nameItems, ( databaseItem: any ) => {
            instanceNames[ databaseItem.id ] = databaseItem.name
        } )

        let dataItems: Array<any> = SQLiteDataEngine.getMany( queryData )

        _.each( dataItems, Helper.createInstanceData )

        return [
            `${logSymbols.success} InstanceData : loaded ${_.size( instanceNames )} names.`,
            `${logSymbols.success} InstanceData : loaded ${_.size( instanceData )} instance property sets.`,
        ]
    }
}

const Helper = {
    createInstanceData( databaseItem: any ) : void {

        let data : L2InstanceDataItem = {
            activityTime: databaseItem[ 'activityTime' ],
            allowRandomWalk: databaseItem[ 'allowRandomWalk' ] !== null ? databaseItem[ 'allowRandomWalk' ] === 'true' : null,
            allowSummon: databaseItem[ 'allowSummon' ] !== null ? databaseItem[ 'allowSummon' ] === 'true' : null,
            doors: Helper.getJsonValue( databaseItem[ 'doors_json' ], null ),
            ejectTime: databaseItem[ 'ejectTime' ],
            emptyDestroyTime: databaseItem[ 'emptyDestroyTime' ],
            exitPoint: Helper.getJsonValue( databaseItem[ 'exitPoint_json' ], null ),
            name: databaseItem[ 'name' ],
            nameId: databaseItem[ 'nameId' ],
            reEnter: Helper.getJsonValue( databaseItem[ 'reEnter_json' ], {} ),
            removeBuffs: Helper.getJsonValue( databaseItem[ 'removeBuffs_json' ], {} ),
            showTimer: Helper.getJsonValue( databaseItem[ 'showTimer_json' ], {} ),
            spawnPoints: Helper.getJsonValue( databaseItem[ 'spawnPoints_json' ], null ),
            spawns: Helper.getJsonValue( databaseItem[ 'spawns_json' ], null ),
        }

        instanceData[ data.nameId ] = data
    },

    getJsonValue( value: any, defaultValue: any ) : any {
        if ( value ) {
            return JSON.parse( value )
        }

        return defaultValue
    }
}