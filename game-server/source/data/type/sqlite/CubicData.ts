import {
    L2CubicDataApi,
    L2CubicDataCondition,
    L2CubicDataHealTarget,
    L2CubicDataItem,
    L2CubicDataSkill,
    L2CubicDataTarget,
    L2CubicDataTargetType
} from '../../interface/CubicDataApi'
import logSymbols from 'log-symbols'
import _ from 'lodash'
import { SQLiteDataEngine } from '../../engines/sqlite'

let data: Record<number, Record<number, L2CubicDataItem>>
let allItems : Array<L2CubicDataItem>

const query: string = 'select * from cubic_data'

export const SQLiteCubicData: L2CubicDataApi = {
    getAll(): Array<L2CubicDataItem> {
        return allItems
    },

    getAllById( id: number ): Array<L2CubicDataItem> {
        let levelMap = data[ id ]
        if ( !levelMap ) {
            return null
        }

        return Object.values( levelMap )
    },

    getItem( id: number, level: number ): L2CubicDataItem {
        let levelMap = data[ id ]
        if ( !levelMap ) {
            return null
        }

        return levelMap[ level ]
    },

    async load(): Promise<Array<string>> {
        data = {}
        allItems = []

        let databaseItems = SQLiteDataEngine.getMany( query )

        databaseItems.forEach( createCubicItem )

        Object.values( data ).forEach( ( items:Array<L2CubicDataItem> ) => _.sortBy( items, 'level' ) )

        return [
            `${ logSymbols.success } CubicData: loaded ${ _.size( data ) } cubics from ${databaseItems.length} data items`,
        ]
    }
}

function getCondition( databaseItem: any ) : L2CubicDataCondition {
    let jsonValue = databaseItem[ 'condition_json' ]
    if ( !jsonValue ) {
        return null
    }

    let value = _.attempt( JSON.parse, jsonValue )

    if ( _.isError( value ) ) {
        return null
    }

    return {
        applyBelow: value.applyBelow,
        applyHp: value.applyHp,
        chance: value.chance
    }
}

function getSkills( databaseItem: any ) : Array<L2CubicDataSkill> {
    let values : Array<any> = _.attempt( JSON.parse, databaseItem[ 'skills_json' ] )
    if ( _.isError( values ) ) {
        throw new Error( `Unable to parse JSON skills for cubic data for id=${databaseItem.id}` )
    }

    if ( !Array.isArray( values ) ) {
        throw new Error( `Incorrect skills JSON type, expected array for cubic data for id=${databaseItem.id}` )
    }

    return values.map( ( value: any ) : L2CubicDataSkill => {
        return {
            activationChance: value.activationChance,
            id: value.id,
            level: value.level,
            staticTarget: value.staticTarget,
            useChance: value.useChance
        }
    } )
}

function getTargetType( value: string ) : L2CubicDataTargetType {
    if ( !value ) {
        return L2CubicDataTargetType.PlayerTarget
    }

    if ( value.startsWith( 'heal' ) ) {
        return L2CubicDataTargetType.HealSelection
    }

    if ( value === 'by_skill' ) {
        return L2CubicDataTargetType.SkillTarget
    }

    return L2CubicDataTargetType.PlayerTarget
}

function getTarget( databaseItem: any ) : L2CubicDataTarget {
    let jsonValue = databaseItem[ 'target_json' ]
    if ( !jsonValue ) {
        return null
    }

    let value = _.attempt( JSON.parse, jsonValue )
    if ( _.isError( value ) ) {
        return null
    }

    let type = getTargetType( value.type )

    if ( type !== L2CubicDataTargetType.HealSelection ) {
        return {
            type
        }
    }

    return {
        type,
        hpThreshold: value.hpThreshold,
        skillThresholds: value.skillThresholds
    } as L2CubicDataHealTarget
}

function createCubicItem( databaseItem: any ) : void {
    let levelMap = data[ databaseItem.id ]
    if ( !levelMap ) {
        levelMap = {}
        data[ databaseItem.id ] = levelMap
    }

    let cubicItem = {
        condition: getCondition( databaseItem ),
        delay: databaseItem.delay,
        id: databaseItem.id,
        level: databaseItem.level,
        maxActions: databaseItem.maxActions,
        power: databaseItem.power,
        target: getTarget( databaseItem ),
        skills: getSkills( databaseItem )
    }

    allItems.push( cubicItem )
    levelMap[ cubicItem.level ] = cubicItem
}