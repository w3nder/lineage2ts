import { L2AugmentationAccessoryDataApi } from '../../interface/AugmentationAccessoryDataApi'
import { AugmentationChance } from '../../../gameService/models/augmentation/AugmentationChance'
import { SQLiteDataEngine } from '../../engines/sqlite'
import _ from 'lodash'

export const SQLiteAugmentationAccessoryData : L2AugmentationAccessoryDataApi = {
    getAll(): Array<AugmentationChance> {
        const query = 'select * from augmentation_accessory'
        let databaseItems = SQLiteDataEngine.getMany( query )
        return _.flatMap( databaseItems, ( databaseItem: any ) : Array<AugmentationChance> => {

            let augmentationData : Array<any> = JSON.parse( databaseItem[ 'chance_json' ] )

            return _.map( augmentationData, ( augmentationItem: any ) : AugmentationChance => {
                return {
                    augmentChance: augmentationItem[ 'chance' ],
                    augmentId: augmentationItem[ 'id' ],
                    categoryChance: databaseItem[ 'categoryProbability' ],
                    stoneId: databaseItem[ 'stoneId' ],
                    type: null,
                    variationId: databaseItem[ 'variationId' ]
                }
            } )
        } )
    }
}