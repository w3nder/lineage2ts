import { L2SpawnNpcDataApi, L2SpawnNpcDataItem, L2SpawnNpcPosition } from '../../interface/SpawnNpcDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { L2NpcMinion } from '../../../gameService/models/actor/templates/L2NpcMinion'
import _ from 'lodash'

const query: string = 'select * from spawn_npc_data_ex'

export const SQLiteSpawnNpcData : L2SpawnNpcDataApi = {
    getNpcIdsByRoute( routeId: string ): Array<number> {
        const routeQuery = 'select distinct npcId from spawn_npc_data_ex where json_extract(ai_json, \'$.superPointName\') = ?'
        let databaseItems = SQLiteDataEngine.getManyWithParameters( routeQuery, routeId )

        return databaseItems.map( ( databaseItem: any ) => databaseItem.npcId )
    },

    getAll(): Array<L2SpawnNpcDataItem> {
        let databaseItems = SQLiteDataEngine.getMany( query )

        return databaseItems.map( createSpawnData )
    }
}

function getPosition( data: any ) : L2SpawnNpcPosition {
    if ( !data ) {
        return null
    }

    let coordinates : Array<number> = JSON.parse( data )
    return {
        x: coordinates[ 0 ],
        y: coordinates[ 1 ],
        z: coordinates[ 2 ],
        heading: coordinates[ 3 ]
    }
}

function createSpawnData( databaseItem: any ) : L2SpawnNpcDataItem {
    let aiParameters = databaseItem[ 'ai_json' ] ? JSON.parse( databaseItem[ 'ai_json' ] ) : null
    let minions = databaseItem[ 'minions_json' ] ? JSON.parse( databaseItem[ 'minions_json' ] ) : null

    return {
        aiName: databaseItem[ 'aiName' ],
        aiParameters,
        amount: databaseItem[ 'amount' ],
        returnDistance: databaseItem[ 'chaseDistance' ],
        makerId: databaseItem[ 'makerId' ],
        minions: _.map( minions, ( item: unknown ) : L2NpcMinion => {
            return {
                amount: item[ 'amount' ],
                npcId: item[ 'npcId' ],
                respawnMs: item[ 'durationMs' ]
            }
        } ),
        npcId: databaseItem[ 'npcId' ],
        position: getPosition( databaseItem[ 'position_json' ] ),
        recordId: databaseItem[ 'recordId' ],
        respawnExtraMs: databaseItem[ 'respawnExtraMs' ] || 0,
        respawnMs: databaseItem[ 'respawnMs' ]
    }
}