import { SkillLearnDataApi } from '../../interface/SkillLearnDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

let npcClassIds : { [ key: number ] : Array<number> }
const query = 'select * from skill_learn'

export const SQLiteSkillLearnData : SkillLearnDataApi = {
    getClassIds( npcId: number ): Array<number> {
        return npcClassIds[ npcId ]
    },

    async load(): Promise<Array<string>> {
        npcClassIds = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) => {
            let classIds = JSON.parse( databaseItem[ 'classIds_json' ] )

            npcClassIds[ databaseItem[ 'npcId' ] ] = classIds
        } )

        return [
            `${logSymbols.success} SkillLearnData loaded ${_.size( npcClassIds )} npcs`
        ]
    }
}