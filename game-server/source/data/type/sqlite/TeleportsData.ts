import { L2TeleportData, L2TeleportsDataApi } from '../../interface/TeleportsDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'

const query = 'SELECT id, name, x, y, z, itemId, amount, isNoble, showPrice FROM teleports'

export const SQLiteTeleportData: L2TeleportsDataApi = {
    async getAll(): Promise<Array<L2TeleportData>> {
        return SQLiteDataEngine.getMany( query ).map( ( databaseItem: unknown ) : L2TeleportData => {
            return {
                id: databaseItem[ 'id' ],
                name: databaseItem[ 'name' ],
                amount: databaseItem[ 'amount' ],
                itemId: databaseItem[ 'itemId' ],
                nobleOnly: databaseItem[ 'isNoble' ] === 1,
                x: databaseItem[ 'x' ],
                y: databaseItem[ 'y' ],
                z: databaseItem[ 'z' ],
                showItemPrice: databaseItem[ 'showPrice' ] === 1
            }
        } )
    },
}