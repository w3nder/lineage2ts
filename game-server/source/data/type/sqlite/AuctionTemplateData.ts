import { L2AuctionTemplateDataApi, L2AuctionTemplateTypes } from '../../interface/AuctionTemplateDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import { L2ClanHallAuctionItem } from '../../../database/interface/ClanHallAuctionsTableApi'
import logSymbols from 'log-symbols'
import _ from 'lodash'

const query = 'select * from auction_templates'
let clanHallTemplates: Record<number, L2ClanHallAuctionItem>

export const SQLiteAuctionTemplateData: L2AuctionTemplateDataApi = {
    getClanHall( id: number ): L2ClanHallAuctionItem {
        return clanHallTemplates[ id ]
    },

    async load(): Promise<Array<string>> {
        clanHallTemplates = {}

        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( item: any ) : void => {
            switch ( item.type ) {
                case L2AuctionTemplateTypes.ClanHall:
                    return Helper.createAuction( item )
            }
        } )

        return [
            `${ logSymbols.success } AuctionTemplates : loaded ${ _.size( clanHallTemplates ) } clan hall templates.`,
        ]
    },
}

const Helper = {
    createAuction( databaseItem: any ): void {
        clanHallTemplates[ databaseItem.id ] = {
            currentBid: 0,
            endDate: 0,
            id: databaseItem.id,
            itemId: 0,
            itemName: databaseItem.name,
            itemObjectId: 0,
            itemQuantity: 1,
            itemType: databaseItem.type,
            sellerClanName: 'Npc Clan',
            sellerId: 0,
            sellerName: 'Npc',
            startingBid: databaseItem.startingBid
        }
    },
}