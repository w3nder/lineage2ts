import { KarmaDataApi } from '../../interface/KarmaDataApi'
import { SQLiteDataEngine } from '../../engines/sqlite'
import logSymbols from 'log-symbols'
import _ from 'lodash'

let karmaTable : { [ key: number] : number } = {}

const query = 'select * from pc_karma_increase'

export const SQLiteKarmaData : KarmaDataApi = {
    getMultiplier( level: number ): number {
        return _.get( karmaTable, level, 0 )
    },

    async load() {
        karmaTable = {}
        let databaseItems: Array<any> = SQLiteDataEngine.getMany( query )

        _.each( databaseItems, ( databaseItem: any ) => {
            let { level, value } = databaseItem
            karmaTable[ level ] = value
        } )

        return [
            `${logSymbols.success} KarmaData loaded ${_.size( karmaTable )} levels from ${_.size( databaseItems )} records.`
        ]
    }
}