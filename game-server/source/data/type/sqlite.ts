import { IDataType } from './IDataType'
import { SQLiteAgathionData } from './sqlite/AgathionData'
import { SQLiteArmorSetsData } from './sqlite/ArmorSetsData'
import { SQLiteBaseStatsData } from './sqlite/BaseStatsData'
import { SQLiteCategoryData } from './sqlite/CategoryData'
import { SQLiteCursedWeaponData } from './sqlite/CursedWeaponData'
import { SQLiteEnchantItemHPBonusData } from './sqlite/EnchantItemHPBonusData'
import { SQLiteEnchantItemOptionsData } from './sqlite/EnchantItemOptionsData'
import { SQLiteExperienceData } from './sqlite/ExperienceData'
import { SQLiteHtmlData } from './sqlite/HtmlData'
import { SQLiteItemData } from './sqlite/ItemData'
import { SQLiteKarmaData } from './sqlite/KarmaData'
import { SQLiteNpcData } from './sqlite/NpcData'
import { SQLiteOptionsData } from './sqlite/OptionsData'
import { SQLitePetData } from './sqlite/PetData'
import { SQLitePlayerTemplateData } from './sqlite/PlayerTemplateData'
import { SQLiteSkillData } from './sqlite/SkillData'
import { SQLiteSkillTreesData } from './sqlite/SkillTreesData'
import { SQLiteTransformData } from './sqlite/TransformData'
import { SQLiteUIData } from './sqlite/UIData'
import { SQLiteRecipeData } from './sqlite/RecipeData'
import { SQLiteInitialShortcutData } from './sqlite/InitialShortcutData'
import { SQLiteInitialEquipmentData } from './sqlite/InitialEquipmentData'
import { SQLitePlayerCreationPointData } from './sqlite/PlayerCreationPointData'
import { SQLiteInstanceData } from './sqlite/InstanceData'
import { SQLiteDoorData } from './sqlite/DoorData'
import { SQLiteFishData } from './sqlite/FishData'
import { SQLiteFishingMonsterData } from './sqlite/FishingMonsterData'
import { SQLiteFishingRodsData } from './sqlite/FishingRodsData'
import { SQLiteSkillLearnData } from './sqlite/SkillLearnData'
import { SQLiteStaticObjectsData } from './sqlite/StaticObjectsData'
import { SQLiteXPLostData } from './sqlite/XPLostData'
import { SQLiteHennaData } from './sqlite/HennaData'
import { SQLiteBuylistData } from './sqlite/BuylistData'
import { SQLiteMerchantPriceData } from './sqlite/MerchantPriceData'
import { SQLiteEnchantItemGroupsData } from './sqlite/EnchantItemGroupsData'
import { SQLiteEnchantItemData } from './sqlite/EnchantItemData'
import { SQLiteMultisellData } from './sqlite/MultisellData'
import { SQLiteHitConditionBonusData } from './sqlite/HitConditionBonus'
import { SQLiteGeoRegionData } from './sqlite/GeoRegionData'
import { SQLiteClassListData } from './sqlite/ClassListData'
import { SQLiteItemAuctionData } from './sqlite/ItemAuctionData'
import { SQLiteAugmentationAccessoryData } from './sqlite/AugmentationAccessoryData'
import { SQLiteAugmentationWeaponData } from './sqlite/AugmentationWeaponData'
import { SQLiteAugmentationSkillData } from './sqlite/AugmentationSkillData'
import { SQLiteManorSeedsData } from './sqlite/ManorSeedsData'
import { SQLiteAuctionTemplateData } from './sqlite/AuctionTemplateData'
import { SQLiteSoulCrystalData } from './sqlite/SoulCrystalData'
import { SQLiteQuestData } from './sqlite/QuestData'
import { SQLitePlayerLevelupRewardsData } from './sqlite/PlayerLevelupRewardsData'
import { IGeoDataType } from './IGeoDataType'
import { SQLiteTeleportData } from './sqlite/TeleportsData'
import { SQLiteGeometryData } from './sqlite/GeometryData'
import { SQLiteSpawnTerritories } from './sqlite/SpawnTerritoriesData'
import { SQLiteSpawnLogicData } from './sqlite/SpawnLogicData'
import { SQLiteSpawnNpcData } from './sqlite/SpawnNpcData'
import { SQLiteNpcRoutesData } from './sqlite/NpcRoutesData'
import { SQLiteCycleStepData } from './sqlite/CycleStepData'
import { SQLiteDataOperations } from './sqlite/Operations'
import { SQLiteServitorSkills } from './sqlite/ServitorSkillsData'
import { SQLiteCubicData } from './sqlite/CubicData'
import { SQLiteAreaData } from './sqlite/AreaData'
import { SQLiteRespawnPointsData } from './sqlite/RespawnPointsData'
import { SQLitePlayerAccessData } from './sqlite/PlayerAccessData'

const SQLiteDataLoadOrder: Array<Function> = [
    SQLiteAgathionData.load,
    SQLiteBaseStatsData.load,
    SQLiteCategoryData.load,
    SQLiteEnchantItemOptionsData.load,
    SQLiteExperienceData.load,
    SQLiteHtmlData.load,
    SQLiteInitialEquipmentData.load,
    SQLiteInitialShortcutData.load,
    SQLiteInstanceData.load,
    SQLiteKarmaData.load,
    SQLiteSkillData.load,
    SQLiteSkillLearnData.load,
    SQLiteSkillTreesData.load,
    SQLiteArmorSetsData.load,
    SQLiteItemData.load,
    SQLiteNpcData.load, // needs skills to be loaded earlier
    SQLiteCursedWeaponData.load,
    SQLiteEnchantItemHPBonusData.load,
    SQLiteOptionsData.load,
    SQLitePetData.load,
    SQLitePlayerCreationPointData.load,
    SQLitePlayerTemplateData.load,
    SQLiteRecipeData.load,
    SQLiteDoorData.load,
    SQLiteFishData.load,
    SQLiteFishingMonsterData.load,
    SQLiteFishingRodsData.load,
    SQLiteTransformData.load,
    SQLiteStaticObjectsData.load,
    SQLiteUIData.load,
    SQLiteXPLostData.load,
    SQLiteHennaData.load,
    SQLiteMerchantPriceData.load,
    SQLiteEnchantItemGroupsData.load,
    SQLiteEnchantItemData.load,
    SQLiteMultisellData.load,
    SQLiteHitConditionBonusData.load,
    SQLiteNpcRoutesData.load,
    SQLiteClassListData.load,
    SQLiteAuctionTemplateData.load,
    SQLiteSoulCrystalData.load, // quest id 350 feature
    SQLiteQuestData.load,
    SQLitePlayerLevelupRewardsData.load,
    SQLiteGeometryData.load,
    SQLiteCubicData.load,
    SQLiteRespawnPointsData.load
]

export const SQLiteData: IDataType = {
    loadOrder: SQLiteDataLoadOrder,
    playerAccess: SQLitePlayerAccessData,
    agathionData: SQLiteAgathionData,
    armorSetsData: SQLiteArmorSetsData,
    baseStatsData: SQLiteBaseStatsData,
    categoryData: SQLiteCategoryData,
    cursedWeaponData: SQLiteCursedWeaponData,
    enchantItemHPBonusData: SQLiteEnchantItemHPBonusData,
    enchantItemOptionsData: SQLiteEnchantItemOptionsData,
    experienceData: SQLiteExperienceData,
    htmlData: SQLiteHtmlData,
    instanceData: SQLiteInstanceData,
    itemData: SQLiteItemData,
    karmaData: SQLiteKarmaData,
    npcData: SQLiteNpcData,
    optionData: SQLiteOptionsData,
    petData: SQLitePetData,
    playerTemplateData: SQLitePlayerTemplateData,
    skillData: SQLiteSkillData,
    skillTreesData: SQLiteSkillTreesData,
    transformData: SQLiteTransformData,
    uiData: SQLiteUIData,
    recipeData: SQLiteRecipeData,
    initialShortcutData: SQLiteInitialShortcutData,
    initialEquipmentData: SQLiteInitialEquipmentData,
    playerCreationPointData: SQLitePlayerCreationPointData,
    doorData: SQLiteDoorData,
    fishData: SQLiteFishData,
    fishingMonsterData: SQLiteFishingMonsterData,
    fishingRodsData: SQLiteFishingRodsData,
    skillLearnData: SQLiteSkillLearnData,
    staticObjects: SQLiteStaticObjectsData,
    xpLostData: SQLiteXPLostData,
    hennaData: SQLiteHennaData,
    buylistData: SQLiteBuylistData,
    merchantPriceData: SQLiteMerchantPriceData,
    enchantItemGroups: SQLiteEnchantItemGroupsData,
    enchantItemData: SQLiteEnchantItemData,
    multisellData: SQLiteMultisellData,
    hitConditionBonus: SQLiteHitConditionBonusData,
    npcRoutes: SQLiteNpcRoutesData,
    classListData: SQLiteClassListData,
    itemAuctionData: SQLiteItemAuctionData,
    augmentationAccessory: SQLiteAugmentationAccessoryData,
    augmentationWeapons: SQLiteAugmentationWeaponData,
    augmentationSkills: SQLiteAugmentationSkillData,
    manorSeeds: SQLiteManorSeedsData,
    auctionTemplates: SQLiteAuctionTemplateData,
    soulCrystalData: SQLiteSoulCrystalData,
    questData: SQLiteQuestData,
    playerLevelupRewards: SQLitePlayerLevelupRewardsData,
    teleports: SQLiteTeleportData,
    geometry: SQLiteGeometryData,
    spawnTerritories: SQLiteSpawnTerritories,
    spawnLogic: SQLiteSpawnLogicData,
    spawnNpcData: SQLiteSpawnNpcData,
    cycleStepData: SQLiteCycleStepData,
    operations: SQLiteDataOperations,
    servitorSkills: SQLiteServitorSkills,
    cubics: SQLiteCubicData,
    areas: SQLiteAreaData,
    respawnPoints: SQLiteRespawnPointsData
}

export const SQLiteGeoData: IGeoDataType = {
    region: SQLiteGeoRegionData
}