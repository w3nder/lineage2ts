import { SQLiteData, SQLiteGeoData } from './type/sqlite'
import { ExperienceDataApi } from './interface/ExperienceDataApi'
import { BaseStatsDataApi } from './interface/BaseStatsDataApi'
import { PlayerTemplateData } from './interface/PlayerTemplateData'
import { ArmorSetsDataApi } from './interface/ArmorSetsDataApi'
import { ItemDataApi } from './interface/ItemDataApi'
import { SkillDataApi } from './interface/SkillDataApi'
import { OptionDataApi } from './interface/OptionDataApi'
import { EnchantItemOptionsDataApi } from './interface/EnchantItemOptionsDataApi'
import { EnchantItemHPBonusDataApi } from './interface/EnchantItemHPBonusDataApi'
import { CategoryDataApi } from './interface/CategoryDataApi'
import { PetDataApi } from './interface/PetDataApi'
import { SkillTreesDataApi } from './interface/SkillTreesDataApi'
import { AgathionDataApi } from './interface/AgathionDataApi'
import { NpcDataApi } from './interface/NpcDataApi'
import { CursedWeaponDataApi } from './interface/CursedWeaponDataApi'
import { KarmaDataApi } from './interface/KarmaDataApi'
import { TransformDataApi } from './interface/TransformDataApi'
import { HtmlDataApi } from './interface/HtmlDataApi'
import { UIDataApi } from './interface/UIDataApi'
import { RecipeDataApi } from './interface/RecipeDataApi'
import { InitialShortcutDataApi } from './interface/InitialShortcutDataApi'
import { InitialEquipmentDataApi } from './interface/InitialEquipmentDataApi'
import { PlayerCreationPointDataApi } from './interface/PlayerCreationPointDataApi'
import { InstanceDataApi } from './interface/InstanceDataApi'
import { DoorDataApi } from './interface/DoorDataApi'
import { FishDataApi } from './interface/FishDataApi'
import { L2FishingMonsterDataApi } from './interface/FishingMonsterDataApi'
import { FishingRodsDataApi } from './interface/FishingRodsDataApi'
import { SkillLearnDataApi } from './interface/SkillLearnDataApi'
import { L2StaticObjectsDataApi } from './interface/StaticObjectsDataApi'
import { L2XPLostDataApi } from './interface/XPLostDataApi'
import { L2HennaDataApi } from './interface/HennaDataApi'
import { L2BuylistDataApi } from './interface/BuylistDataApi'
import { L2MerchantPriceDataApi } from './interface/MerchantPriceDataApi'
import { L2EnchantItemGroupsDataApi } from './interface/EnchantItemGroupsDataApi'
import { L2EnchantItemDataApi } from './interface/EnchantItemDataApi'
import { L2MultisellDataApi } from './interface/MultisellDataApi'
import { ConfigManager } from '../config/ConfigManager'
import { IDataType } from './type/IDataType'
import { L2HitConditionBonusDataApi } from './interface/HitConditionBonusDataApi'
import { L2GeoRegionDataApi } from './interface/GeoRegionDataApi'
import { L2ClassListDataApi } from './interface/ClassListDataApi'
import { L2ItemAuctionDataApi } from './interface/ItemAuctionDataApi'
import { L2AugmentationAccessoryDataApi } from './interface/AugmentationAccessoryDataApi'
import { L2AugmentationWeaponsDataApi } from './interface/AugmentationWeaponsDataApi'
import { L2AugmentationSkillsDataApi } from './interface/AugmentationSkillsDataApi'
import { L2ManorSeedsDataApi } from './interface/ManorSeedsDataApi'
import { L2AuctionTemplateDataApi } from './interface/AuctionTemplateDataApi'
import { L2SoulCrystalDataApi } from './interface/SouldCrystalDataApi'
import { L2PlayerLevelupRewardsDataApi } from './interface/PlayerLevelupRewardsDataApi'
import { L2QuestDataApi } from './interface/QuestDataApi'
import { IGeoDataType } from './type/IGeoDataType'
import { L2TeleportsDataApi } from './interface/TeleportsDataApi'
import { L2GeometryDataApi } from './interface/GeometryDataApi'
import { L2SpawnTerritoriesDataApi } from './interface/SpawnTerritoriesDataApi'
import { L2SpawnLogicDataApi } from './interface/SpawnLogicDataApi'
import { L2SpawnNpcDataApi } from './interface/SpawnNpcDataApi'
import { L2NpcRoutesDataApi } from './interface/NpcRoutesDataApi'
import { L2CycleStepDataApi } from './interface/CycleStepDataApi'
import { L2DataOperationsApi } from './interface/OperationsApi'
import { L2ServitorSkillsDataApi } from './interface/ServitorSkillsDataApi'
import { L2CubicDataApi } from './interface/CubicDataApi'
import { L2AreaDataApi } from './interface/AreaDataApi'
import { L2RespawnPointsDataApi } from './interface/RespawnPointsDataApi'
import { L2PlayerAccessDataApi } from './interface/PlayerAccessDataApi'

const allEngines: { [ name: string ]: IDataType } = {
    'sqlite': SQLiteData,
}

const geoEngines: Record<string, IGeoDataType> = {
    'sqlite': SQLiteGeoData
}

let dataEngine : IDataType = allEngines[ ConfigManager.data.getDataEngine() ]
let geoEngine : IGeoDataType = geoEngines[ ConfigManager.data.getGeoEngine() ]

ConfigManager.data.subscribe( () => {
    dataEngine = allEngines[ ConfigManager.data.getDataEngine() ]
    geoEngine = geoEngines[ ConfigManager.data.getGeoEngine() ]
} )

export const DataManager = {
    getLoadOrder(): Array<Function> {
        return dataEngine.loadOrder
    },

    getExperienceData(): ExperienceDataApi {
        return dataEngine.experienceData
    },

    getPlayerAccess() : L2PlayerAccessDataApi {
        return dataEngine.playerAccess
    },

    getBaseStatsData(): BaseStatsDataApi {
        return dataEngine.baseStatsData
    },

    getPlayerTemplateData(): PlayerTemplateData {
        return dataEngine.playerTemplateData
    },

    getArmorSetsData(): ArmorSetsDataApi {
        return dataEngine.armorSetsData
    },

    getItems(): ItemDataApi {
        return dataEngine.itemData
    },

    getSkillData(): SkillDataApi {
        return dataEngine.skillData
    },

    getOptionsData(): OptionDataApi {
        return dataEngine.optionData
    },

    getEnchantItemOptionsData(): EnchantItemOptionsDataApi {
        return dataEngine.enchantItemOptionsData
    },

    getEnchantItemHPBonusData(): EnchantItemHPBonusDataApi {
        return dataEngine.enchantItemHPBonusData
    },

    getCategoryData(): CategoryDataApi {
        return dataEngine.categoryData
    },

    getPetData(): PetDataApi {
        return dataEngine.petData
    },

    getSkillTreesData(): SkillTreesDataApi {
        return dataEngine.skillTreesData
    },

    getAgathionData(): AgathionDataApi {
        return dataEngine.agathionData
    },

    getNpcData(): NpcDataApi {
        return dataEngine.npcData
    },

    getCursedWeaponData(): CursedWeaponDataApi {
        return dataEngine.cursedWeaponData
    },

    getKarmaData(): KarmaDataApi {
        return dataEngine.karmaData
    },

    getTransformData(): TransformDataApi {
        return dataEngine.transformData
    },

    getHtmlData(): HtmlDataApi {
        return dataEngine.htmlData
    },

    getUIData(): UIDataApi {
        return dataEngine.uiData
    },

    getRecipeData(): RecipeDataApi {
        return dataEngine.recipeData
    },

    getInitialShortcutData(): InitialShortcutDataApi {
        return dataEngine.initialShortcutData
    },

    getInitialEquipmentData(): InitialEquipmentDataApi {
        return dataEngine.initialEquipmentData
    },

    getPlayerCreationPointData(): PlayerCreationPointDataApi {
        return dataEngine.playerCreationPointData
    },

    getInstanceData(): InstanceDataApi {
        return dataEngine.instanceData
    },

    getDoorData(): DoorDataApi {
        return dataEngine.doorData
    },

    getFishData(): FishDataApi {
        return dataEngine.fishData
    },

    getFishingMonsterData(): L2FishingMonsterDataApi {
        return dataEngine.fishingMonsterData
    },

    getFishingRodsData(): FishingRodsDataApi {
        return dataEngine.fishingRodsData
    },

    getSkillLearnData(): SkillLearnDataApi {
        return dataEngine.skillLearnData
    },

    getStaticObjects(): L2StaticObjectsDataApi {
        return dataEngine.staticObjects
    },

    getXPLostData(): L2XPLostDataApi {
        return dataEngine.xpLostData
    },

    getHennas(): L2HennaDataApi {
        return dataEngine.hennaData
    },

    getBuylistData(): L2BuylistDataApi {
        return dataEngine.buylistData
    },

    getMerchantPriceData(): L2MerchantPriceDataApi {
        return dataEngine.merchantPriceData
    },

    getEnchantItemGroups(): L2EnchantItemGroupsDataApi {
        return dataEngine.enchantItemGroups
    },

    getEnchantItemData(): L2EnchantItemDataApi {
        return dataEngine.enchantItemData
    },

    getMultisellData(): L2MultisellDataApi {
        return dataEngine.multisellData
    },

    getHitConditionBonus(): L2HitConditionBonusDataApi {
        return dataEngine.hitConditionBonus
    },

    getGeoRegionData(): L2GeoRegionDataApi {
        return geoEngine.region
    },

    getClassListData(): L2ClassListDataApi {
        return dataEngine.classListData
    },

    getItemAuctions(): L2ItemAuctionDataApi {
        return dataEngine.itemAuctionData
    },

    getAugmentationAccessory() : L2AugmentationAccessoryDataApi {
        return dataEngine.augmentationAccessory
    },

    getAugmentationWeapons() : L2AugmentationWeaponsDataApi {
        return dataEngine.augmentationWeapons
    },

    getAugmentationSkills() : L2AugmentationSkillsDataApi {
        return dataEngine.augmentationSkills
    },

    getManorSeeds() : L2ManorSeedsDataApi {
        return dataEngine.manorSeeds
    },

    getAuctionTemplates() : L2AuctionTemplateDataApi {
        return dataEngine.auctionTemplates
    },

    getSoulCrystalData() : L2SoulCrystalDataApi {
        return dataEngine.soulCrystalData
    },

    getPlayerLevelupRewards() : L2PlayerLevelupRewardsDataApi {
        return dataEngine.playerLevelupRewards
    },

    getQuestData() : L2QuestDataApi {
        return dataEngine.questData
    },

    getTeleports() : L2TeleportsDataApi {
        return dataEngine.teleports
    },

    getGeometry() : L2GeometryDataApi {
        return dataEngine.geometry
    },

    getSpawnTerritories() : L2SpawnTerritoriesDataApi {
        return dataEngine.spawnTerritories
    },

    getSpawnLogic() : L2SpawnLogicDataApi {
        return dataEngine.spawnLogic
    },

    getSpawnNpcData() : L2SpawnNpcDataApi {
        return dataEngine.spawnNpcData
    },

    getNpcRoutes() : L2NpcRoutesDataApi {
        return dataEngine.npcRoutes
    },

    getStepCycleData() : L2CycleStepDataApi {
        return dataEngine.cycleStepData
    },

    operations() : L2DataOperationsApi {
        return dataEngine.operations
    },

    getServitorSkills() : L2ServitorSkillsDataApi {
        return dataEngine.servitorSkills
    },

    getCubics() : L2CubicDataApi {
        return dataEngine.cubics
    },

    getAreas() : L2AreaDataApi {
        return dataEngine.areas
    },

    getRespawnPoints() : L2RespawnPointsDataApi {
        return dataEngine.respawnPoints
    }
}
