# List of todos

## Player character snapshot

We need to create a version of character data that can be restored at later time. The data should contain inventory and
warehouse snapshots, along with character specific stats and skills, also any pets and their progress. Special care
should be given restoration process due to object ids of items that would previously be given away to other players.
One way to do is assign all new object ids for all data items.
Use cases:

- detected botting, moved into bot only instance, recorded date and create character snapshot
- GM ability to create diff of items/stats from player inspection
- ability to create backup data for server migrations (migration can be separate ETL job, but backup ensures data
  inspection)

## Bot only instance

- without npcs, let them go around and search things

## Typed array improvements

- consider usages of Array<number> and sorting
- using native Int32Array or similar will be much faster sorting

## Scroll of Escape improvements

- use `ConditionNotUnderSiege` for escape scrolls
- modify condition to use config to dynamically enable/disable blocking of scroll usage

## Implement barter system

- first, consider who gets access (part of clan, a merchant in each city, each merchant in game)
- cycle of rotation, every 30 minutes or 3 hours
- barter, exchange of X amount of items for Y amount of different goods
- no adena buy/sell option
- use crystals, soul/spirit shots, red/blue/green stone from seven signs, raid boss artifacts
- trade for pieces of armor, crafting material, recipes, enchant scrolls armor/weapon, all grades
- trade quantity changes with time: at start lower quantity is exchanged, at end highest quantity (force player to wait)

## Nagle's algorithm

- consider turning it off since client is most likely using delayed ACK system
- measure GC and CPU metrics to find out which approach is beneficial
- node has flags that can turn on/off certain other networking features, explore such options

## Opentelemetry integration

- add non-layered telemetry integration for CPU/memory usage when configs are reloaded
- ensure telemetry works against local Signoz installation
- console.log statements can coexist with telemetry, however consider separate logger
- add traces to login <-> game servers interactions
- add traces to bulk item operation updates
- add traces to player save operations (as one trace per type of data)
- add traces to time npc spawn and death (item drop generation)

## Logger integration
- replace console.log statements with integrated logger
- ensure Opentelemetry integration possible with new logger, make such integration default option
- add configuration for log level
- not all console.log statements should use log level, thus always logged

## Dependencies replacement

- remove `moment` package and replace it with `dayjs`

## Add hot module reloading

- review available options if they would be able to be used, due to how certain game server data caches are working
- possibly implement re-loading for listeners, ala L2J

## ValidatePosition and bots

- L2 client requests to validate position each second when moving, at 1000 ms interval
- clients must be able to simulate such call or be automatically marked as bots
- keep last epoch of ValidatePosition request and check difference if more than 1500 ms (assume lag)
- marking a bot should trigger observation session, freezing character in place and recording certain packet timings
- upon observation session completion (choose 5 to 30 second window), bot is teleported into jail or bot only instance of the world (no npcs there)

## Termination signal handling

- add termination signal use case for closing database connections/files
- send login server a message about server de-registration

## Teleport calculator
- a voiced command that tells you which GK to go to, from current position
- computes GK path, with possible radar navigation for first GK
- show GK routes you have to take in html page

## Reduce Promise usage
- certain functions call to database to update/save/delete value, extract into cache layer so that such operations can be done in bulk
- certain chain of events use terminated listener, review if that is needed since avoiding such use removes promise chain
- review aigle.resolve() calls to see which promises can be done in parallel
- inspect skill usage of promises 

## Docker image improvements
- introduce bundler to produce single js file for login and game servers
- compiled dependencies can be installed separately (these cannot be bundled as JS anyway)
- explore using alpine image (problematic since you need a build chain to compile certain dependencies)
- compress layers where it would help size
- add support for arm64 platform ( arm7 can technically be useful, however not many boards feature 2GB+ RAM)

## RaidBoss varieties
- add variety to each raid boss that would signify overall strength of combat (such as "Champion", "Epic", "Undefeated", etc.)
- compute on server load npc templates that such varieties would use (multiply stats by 10,20 or 30%)
- add checks for additional loot (consider only consumable items drops increased to reduce farming, add additional exp and adena?)
- add admin panel integration to switch raid bosses in region or whole world to particular variety

## Player trade anomaly detection
- add admin command that would show player information regarding player shops that sell goods grossly under price value
- such way is used by players to preserve trading spots, which should be released when normal trade is finished
- example would be buying resources for one or two adena, or selling grossly over normal price such as 1KK adena for animal bone
- information should include player coordinates, name and ability to cancel player shop/manufacture action (via either connection reset, or login ban for X amount of minutes)

## Nevit and Spoilers/Crafting alternative mechanic
- add bonuses for spoiling, where there is X % chance of harvesting mob two times
- add bonuses for crafting, for recipes that produce multiple item X chance to produce 1/5 additional items

## Inspect data loading layer to establish types
- add types for various data loading items (currently alot of these are not established types, however properties are named)

## Refactor bypass keywords
- remove `admin_` requirement for admin html bypasses and use `admin` keyword (lowercase and without underscore), this is inline with `Quest` keyword already used
- add html bypass to target listeners with npc id, something like `listener <npcId>` or `listener <name>`, useful to trigger multiple bypasses instead of invocation by quest name

## Simplify rate limiter
- current usage of `fast-ratelimit` package is using timers via setTimeout
- consider using `Date.now()` value with comparison of minimum allowed time duration between rate limiter checks
- main usage is with game server packets, other usages are sprinkled in various handlers (such as global chat)
- such package can be internal to project since virtually all rate limiters use nodejs timers
- benefit is to reduce timer usage and its associated CPU/memory footprint
- consider if currently usage package would still be useful in other parts of code

# Permanent buff addition system
- add system to allow players to exchange X item for Y amount of attributes
- driven by listener system, recorded per player as to how much times can buff apply
- separate system to track and apply such buffs to characters, database based storage
- example: exchange item X for Y mDefence once
- restrict to use of defencive attributes since attack and any cast mechanic would provide dis-balance

# Exchange system for rare items
- trade X and Y items with enchant values of Z (example, +3 or +4, highest safest enchant, etc.) for item A
- limit trade per player period or amount of trades possible per player/account
- can be based on daily random quest or player status (example, clan leader or clan owning a clan hall, or specifically PK or non-clan joined players)
- consider exchange for a time limited item or even a buff (a cubic would be a good start, since it is not common buff)
- track popularity of items traded (both sides), and if possible lock quantity of exchanges per day

# Dropped items vs L2ItemInstance
- optimization to use lighter objects when drops/spoils are performed (perhaps with object pool)
- currently L2ItemInstance is created and placed on object grid, however creation is both CPU and memory heavy
- assuming at least 30% of items will be adena, a simpler solution would be to create shell object with itemId and amount only
- when such item is picked up it can have fast path to add to existing items (amount change) or to create L2ItemInstance

# Lodash replacement
- consider using npm module es-toolkit
- performance seems to be much better https://es-toolkit.slash.page/performance.html

# All timers must have references
- setTimeout or setInterval can be created without return value being stored anywhere
- in such case there is no way to cancel/clear timer, which may present a problem
- solution is to store timer values and find out in which places they need to be cancelled

# Auto-use items
- usage of received packet `RequestAutoSoulShot` from client has itemId, replied to client with packet `ExAutoSoulShot`
- normal usage is to have soulshot/spiritshot auto action after attack
- expand usage to potions, so that potion is used on re-use time (check if same skills effects are in effect)
- consider using effect end as trigger, possibly using a listener as integration

# Add datapack sqlite table hash
- when datapack is created populate metadata about current node version and architecture
- for each table create a hash that can be used to know if datapack has been modified after being constructed

# Discovery of new players based on limit per interval
- it is possible to have discovery show data for all visible players in range
- however that can represent a big CPU spike due to sheer amount of players nearby
- a slower approach can add X amount of new players per discovery interval

# Add voice commands to enable/disable xp gain
- command names can be: .xpon .xpoff
- utilize permissions on access level, that can be toggled
- integrate permissions into XP gain logic
- ensure that modifying permissions of access level only affects player

# Consider moving boolean flags on player into separate cache
- utilize RoaringBitmap32
- goal is simplification of class fields and reduction in memory footprint

# Spawn territory build tool
- admin tool to build a territory for npc spawn
- such territory does not need to have npc assigned when created
- territory can be made from existing geometry forms (triangle, square, oval, rhombus or a five speared star)
- territory under edit can be repositioned (center point) or scaled in increments (+/- 20 points as example), or rotated
- name should include account name and time of creation
- must utilize command link to generate points: create form first, send data to geometry watcher which will populate points
- add flag if territory must be saved to cli project files, otherwise it can live in server datapack only

# Initial point data
- convert to use geometry points
- normally has few points to choose for player spawn, however geometry will introduce uniqueness
- use normal points as geometry shape, and then generate points same way as for spawn territories
- include normal points into final geometry point set

# Scroll of Enchant Weapon of Destruction modifications
- add configuration where such scrolls are enabled/disabled for drop or spoil
- add configuration to enable them act as normal blessed enchant scrolls 
or specify different enchant preservation limit (current is +15 per HF lore)
- optionally specify chance multiplier to lower success of enchant

# Vitality alternative use
- spoil drops can have higher chances based on vitality level
- crafting % recipes can have higher chance based on vitality level
- crafting amount can be influenced, for etc items only

# Regional mob drops/spoils
- shows top drops/spoils by % in currently activated world/map region (or region admin player is in)
- search drop/spoil by name/itemId in current region, to show available mobs
- ability to show mob coordinates and teleport to mob

# IP restricted area type/functionality
- restrict players to have only unique IP connections per area (no dualbox)
- optionally restrict per IP mask or range
- useful for various PVP areas such as sieges where dualbox restriction must be employed
- optionally add functionality to castle/fort/clan related PVP areas via config
- teleport non-confirming players, upon entry, to nearest town (or perhaps apply debuff, paralysis and then teleport)

# Potions duration modifiers
- configuration options to add X amount of seconds to health potions, as example
- reduce duration by X seconds for specific potion item ids
- restricted to specific map region, or permitted in all (specific application can be to assist in newbie starting locations)

# Alternative spoil/pick drop mechanic for stacked items
- when item is picked up in party, whole item can be assigned to random player in party
- alternatively quantity of item can be distributed among all party members, based on minimum count
- such distribution allows multiple players to benefit from party loot distribution, including spoiled stacked items

# Check client issue when enchanting equipped earrings or rings
- normal inventory update does not make client recognize new enchantment as visible
- to solve such issue, ItemInfo packet must be sent after enchantment is successful
- issue exists on Interlude client, however may not exist on HF client

# Additional clan functions with per-use cost
- restore HP/MP (no CP) in 1k increments, adena/specific item charged to either clan warehouse or player claiming restoration
- vitality points recharge in X increments, up to 3rd level (4th level is possible however consider impact of too much xp/sp gain) and Y amount of times per day
- spoil/manor harvest quantity improvement, +1, +2 or +3 to quantity, as counter, bought in packs of 100, charged as adena/specific item

# Customize clan hall sell time
- in case of small server, waiting full period of bidding has no purpose since there are no other clans to bid
- add logic to detect if no multiple bids are placed in 72 hours, end auction and assign clan hall
- if bids are placed normal period can be assumed
- consider to also use criteria of how many clans are bidding

# Raid monster captures castle
- for castles that are not captured by a player clan
- each castle stage can represent raidboss monster that can still count toward raid progress
- can be a dynamic event for PvE, scripted for raidboss to appear at certain time to capture castle
- there can be a backstory and possibility of paying X amount of items to prevent such invasion

# Limited period/time items
- consider alternative mechanic to only count time wearing an item

# Various area types
- custom areas to allow/disallow players using global chat
- allow/disallow craft or buy/sell store
- allow/disallow scroll of escape or usage of particular items (healing potions or CP potions)
- allow/disallow spawn of pets or agathions
- allow/disallow specific private craft from recipe (common/dwarf)
- allow/disallow pets, meaning pets will be un-summoned when player enters such an area
- trade area that uses grid to assign closest spot (to allow walkable space between characters), otherwise rejects trading