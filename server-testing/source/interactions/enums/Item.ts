export interface BasicItem {
    id: number
    amount: number
}

export enum L2ItemIds {
    Adena = 57,
    AncientAdena = 5575,

    /*
        Soulshot & Spiritshot
     */
    SoulshotNG = 1835,
    SoulshotBeast = 6645,
    SoulshotD = 1463,
    SoulshotC = 1464,
    SoulshotB = 1465,
    SoulshotA = 1466,
    SoulshotS = 1467,

    BlessedSpiritshotNG = 3947,
    BlessedSpiritshotD = 3948,
    BlessedSpiritshotC = 3949,
    BlessedSpiritshotB = 3950,
    BlessedSpiritshotA = 3951,
    BlessedSpiritshotS = 3952,

    /*
        Crystals
     */
    CrystalD = 1458,
    CrystalC = 1459,
    CrystalB = 1460,
    CrystalA = 1461,
    CrystalS = 1462,

    /*
        Potions
     */
    HastePotion = 734,
    ManaPotion = 728,
    Antidote = 1831,
    GreaterAntidote = 1832,

    /*
        Scrolls
     */
    ScrollOfEscape = 736,
    ScrollOfResurrection = 737,

    SOETalkingIslandVillage = 7554,
    SOEElvenVillage = 7555,
    SOEDarkElvenVillage = 7556,
    SOETOrcVillage = 7557,
    SOETDwarvenVillage = 7558,
    SOETTownOfGiran = 7559,

    ScrollEnchantWeaponS = 959,
    ScrollEnchantArmorS = 960,
    ScrollEnchantWeaponA = 729,
    ScrollEnchantArmorA = 730,
    ScrollEnchantWeaponB = 947,
    ScrollEnchantArmorB = 948,
    ScrollEnchantWeaponC = 951,
    ScrollEnchantArmorC = 952,
    ScrollEnchantWeaponD = 955,
    ScrollEnchantArmorD = 956,

    SPScroll10k = 15439,
    SPScroll500k = 17184,
    SPScroll1kk = 17185,
    SPScroll5kk = 17276,
    SPScroll100kk = 17277,

    /*
        Recipes
     */
    RecipeSoulshotD = 1804,
    RecipeSoulshotC = 1805,
    RecipeSoulshotB = 1806,
    RecipeSoulshotA = 1807,
    RecipeSoulshotS = 1808,

    RecipeBSpiritshotD = 3953,
    RecipeBSpiritshotC = 3954,
    RecipeBSpiritshotB = 3955,
    RecipeBSpiritshotA = 3956,
    RecipeBSpiritshotS = 3957,

    /*
        Herbs
     */
    HerbOfLife = 8600,
    HerbOfMana = 8603,
    HerbOfPower = 8606,
    HerbOfMagic = 8607,
    HerbOfGreaterLife = 8601,
    HerbOfGreaterMana = 8604,
    HerbOfAlacrity = 8608,
    HerbOfCastingSpeed = 8609,
    HerbOfSpeed = 8611,
    HerbOfVampiricRage = 10655,
    HerbOfCriticalAttackPower = 10656,
    HerbOfCriticalAttackProbability = 8610,
    HerbOfWarrior = 8612,
}