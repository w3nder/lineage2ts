import { L2ClientLogic } from '../../client/L2ClientLogic'
import { L2LoginParameters } from '../../client/L2LoginClient'
import { L2ServerDefaults } from '../../client/enums/L2ServerDefaults'
import { L2ClientError } from '../../client/L2BaseClient'
import { PacketEvent } from '../../client/packets/PacketMethodTypes'

export class LoginFlow extends L2ClientLogic {
    constructor() {
        super( LoginFlow.name )
    }
    getLoginProperties(): L2LoginParameters {
        return {
            host: L2ServerDefaults.LocalhostIpAddress,
            password: 'admin',
            port: L2ServerDefaults.LoginServerPort,
            serverId: L2ServerDefaults.ServerId,
            userName: 'admin'
        }
    }
    onLoginServerFlowFinished( error : L2ClientError, data: PacketEvent ) : void {
        this.abortAllConnections()

        if ( error ) {
            return super.onLoginServerFlowFinished( error, data )
        }

        this.notifySuccess( 'finished full login flow' )
        this.onClientExit( true )
    }
}

new LoginFlow().start()