import { L2FullClientLogic } from '../../../client/L2FullClientLogic'
import { L2LoginParameters } from '../../../client/L2LoginClient'
import { DefaultLoginParameters } from '../../../client/enums/L2ServerDefaults'
import { BasicItem, L2ItemIds } from '../../enums/Item'
import _ from 'lodash'
import { BypassCommand } from '../../../client/packets/game/send/BypassCommand'
import { TargetAction, TargetActionType } from '../../../client/packets/game/send/TargetAction'
import { L2Item } from '../../../client/models/L2Item'
import { L2World } from '../../../client/L2World'
import { ItemState } from '../../../client/enums/ItemState'
import { L2ObjectType } from '../../../client/enums/L2ObjectType'

const enum StepSequence {
    TargetSelf,
    Create,
    VerifyCreate,
    Drop,
    VerifyDrop,
    PickItem,
    VerifyPickItem,
    Delete,
    VerifyDelete
}

const itemsToCreate: Array<BasicItem> = [
    {
        id: L2ItemIds.Adena,
        amount: _.random( 1000 )
    }
]

class CreateAndDropItem extends L2FullClientLogic {
    timer: NodeJS.Timeout
    step: StepSequence = StepSequence.TargetSelf
    dropedObjectids: Array<number> = []

    constructor() {
        super( CreateAndDropItem.name )
    }

    getLoginProperties(): L2LoginParameters {
        return {
            ...DefaultLoginParameters,
            password: 'tester',
            userName: 'tester'
        }
    }

    onWorldEntered() {
        let isGM = this.getPlayer().isGM

        if ( !isGM ) {
            this.notifyFailure( 'Player is not GM' )
            this.onClientExit( false )

            return
        }

        /*
            We must wait for inventory packets to fully arrive before
            we are able to start verification.
         */
        this.timer = setInterval( this.runSequence.bind( this ), 1000 )
    }

    runSequence(): void {
        switch ( this.step ) {
            case StepSequence.TargetSelf:
                this.gameClient.sendPacket( TargetAction( this.getPlayerId(), TargetActionType.TargetThenInteract ) )

                this.step = StepSequence.Create
                return

            case StepSequence.Create:
                if ( this.verifyTargetNotSelf() ) {
                    this.notifyFailure( 'Failed to target player' )
                    return this.onClientExit( false )
                }

                if ( this.shouldRetry() ) {
                    return
                }

                this.notifySuccess( 'Verified no existing items in inventory' )
                this.createItems()

                this.step = StepSequence.VerifyCreate
                return

            case StepSequence.VerifyCreate:
                if ( this.verifyItemAmountDifferent() ) {
                    return this.onClientExit( false )
                }

                this.notifySuccess( `Verified creation for ${ itemsToCreate.length } items` )

                this.step = StepSequence.Drop
                return

            case StepSequence.Drop:
                this.dropItems()

                this.notifySuccess( `Requested inventory drop to ground for ${this.dropedObjectids.length} items` )
                this.step = StepSequence.VerifyDrop
                return

            case StepSequence.VerifyDrop:
                if ( this.verifyItemsExistInInventory() ) {
                    this.notifyFailure( 'Items were not dropped' )
                    return this.onClientExit( false )
                }

                if ( this.verifyItemsNotOnGround() ) {
                    this.notifyFailure( 'Unable to see items on ground for pick up' )
                    return this.onClientExit( false )
                }

                this.notifySuccess( 'Verified items were dropped from inventory' )
                this.step = StepSequence.PickItem
                return

            case StepSequence.PickItem:
                /*
                    If we have more than one item, we must pick each item separately.
                    Server will move player toward each item on ground, issuing
                    inventory update. Hence, we must wait until all items are picked
                    up before moving into verification step.
                 */
                if ( this.dropedObjectids.length > 0 ) {
                    this.notifySuccess( 'Requesting to pick next item from ground' )
                    this.requestItemPickUp()
                    return
                }

                this.notifySuccess( 'Waiting for last item to be picked up' )
                this.step = StepSequence.VerifyPickItem
                return

            case StepSequence.VerifyPickItem:
                if ( this.verifyItemAmountDifferent() ) {
                    this.notifyFailure( 'Items were not picked up' )
                    return this.onClientExit( false )
                }

                this.notifySuccess( 'Verified picked up items are the same as were dropped' )
                this.step = StepSequence.Delete
                return

            case StepSequence.Delete:
                itemsToCreate.forEach( ( item: BasicItem ) => this.inventory.destroyAllItemsById( item.id ) )

                this.notifySuccess( `Requested to delete ${itemsToCreate.length} picked up items` )
                this.step = StepSequence.VerifyDelete
                return

            case StepSequence.VerifyDelete:
                clearInterval( this.timer )

                if ( this.verifyItemsExistInInventory() ) {
                    return this.onClientExit( false )
                }

                this.notifySuccess( `Verified deletion for ${ itemsToCreate.length } items` )
                this.performLogOut()
                return
        }
    }

    getCharacterName(): string {
        return 'tester1'
    }

    shouldRetry(): boolean {
        let shouldRetry = false
        itemsToCreate.forEach( ( item: BasicItem ): void => {
            let amount = this.inventory.getItemCount( item.id )
            if ( amount > 0 ) {
                this.notifyWarning( `Item check: removing existing itemId=${item.id} and amount=${amount}` )
                this.inventory.destroyAllItemsById( item.id )
                shouldRetry = true
            }
        } )

        return shouldRetry
    }

    verifyItemAmountDifferent(): boolean {
        return itemsToCreate.some( ( item: BasicItem ): boolean => {
            let amount = this.inventory.getItemCount( item.id )
            let hasError = amount !== item.amount

            if ( hasError ) {
                this.notifyFailure( 'Item creation failure:', `expected=${ item.amount }, received=${ amount } amount for itemId=${ item.id }` )
            }

            return hasError
        } )
    }

    verifyItemsExistInInventory(): boolean {
        return itemsToCreate.some( ( item: BasicItem ): boolean => {
            let amount = this.inventory.getItemCount( item.id )
            let hasError = amount !== 0

            if ( hasError ) {
                this.notifyFailure( 'Item verification failure:', `received=${ amount }, expected=0 amount for itemId=${ item.id }` )
            }

            return hasError
        } )
    }

    createItems() : void {
        itemsToCreate.forEach( ( item: BasicItem ) => {
            let text = `create-givetarget ${item.id} ${item.amount}`
            this.gameClient.sendPacket( BypassCommand( text ) )
        } )
    }

    verifyTargetNotSelf() : boolean {
        let player = this.getPlayer()
        return player.targetId !== player.objectId
    }

    dropItems() : void {
        let player = this.getPlayer()
        itemsToCreate.forEach( ( item: BasicItem ) => {
            let x = player.x + _.random( -30, 30 )
            let y = player.y + _.random( -30, 30 )

            this.dropedObjectids.push( ...this.inventory.dropItem( item.id, x, y, player.z ) )
        } )
    }

    verifyItemsNotOnGround() : boolean {
        return this.dropedObjectids.some( ( objectId: number ) : boolean => {
            let item = L2World.getObject( objectId ) as L2Item
            return !item || item.type !== L2ObjectType.Item || item.state !== ItemState.Dropped
        } )
    }

    requestItemPickUp() : void {
        this.gameClient.sendPacket( TargetAction( this.dropedObjectids.pop(), TargetActionType.TargetThenInteract ) )
    }
}

new CreateAndDropItem().start()