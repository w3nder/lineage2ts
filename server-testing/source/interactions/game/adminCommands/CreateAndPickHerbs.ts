import { L2FullClientLogic } from '../../../client/L2FullClientLogic'
import { L2LoginParameters } from '../../../client/L2LoginClient'
import { DefaultLoginParameters } from '../../../client/enums/L2ServerDefaults'
import { L2ItemIds } from '../../enums/Item'
import _ from 'lodash'
import { BypassCommand } from '../../../client/packets/game/send/BypassCommand'
import { L2World } from '../../../client/L2World'
import { L2ObjectType } from '../../../client/enums/L2ObjectType'
import { L2Item } from '../../../client/models/L2Item'
import { RoaringBitmap32 } from 'roaring'
import { TargetAction, TargetActionType } from '../../../client/packets/game/send/TargetAction'
import { RequestRemoveBuff } from '../../../client/packets/game/send/RequestRemoveBuff'

const enum Sequence {
    CreateHerbs,
    LocateHerbs,
    PrepareForPickUp,
    VerifyHpMp,
    PerformPickUp,
    VerifyEffects
}

const herbIds: Array<number> = [
    L2ItemIds.HerbOfSpeed,
    L2ItemIds.HerbOfGreaterLife,
    L2ItemIds.HerbOfGreaterMana,
    L2ItemIds.HerbOfGreaterLife,
    L2ItemIds.HerbOfGreaterMana,
    L2ItemIds.HerbOfGreaterLife,
    L2ItemIds.HerbOfGreaterMana
]

const enum Distance {
    DropDistance = 5
}

class CreateAndPickHerbs extends L2FullClientLogic {
    timer: NodeJS.Timeout
    step: Sequence = Sequence.CreateHerbs
    index: number = 0
    herbsOnGround: Array<L2Item> = []
    bypassCommands: Array<string> = [
        'set-hp-percent 10',
        'set-mp-percent 10'
    ]
    lastEffectUpdate: number = 0

    constructor() {
        super( CreateAndPickHerbs.name )
    }

    getLoginProperties(): L2LoginParameters {
        return {
            ...DefaultLoginParameters,
            password: 'tester',
            userName: 'tester'
        }
    }

    onWorldEntered() {
        let isGM = this.getPlayer().isGM

        if ( !isGM ) {
            this.notifyFailure( 'Player is not GM' )
            this.onClientExit( false )

            return
        }

        this.resetTimer( 1000 )
    }

    resetTimer( timeMs: number ): void {
        if ( this.timer ) {
            clearInterval( this.timer )
        }

        this.timer = setInterval( this.runSequence.bind( this ), timeMs )
    }

    runSequence(): void {
        switch ( this.step ) {
            case Sequence.CreateHerbs:
                if ( !this.info.statusEffects ) {
                    this.notifySuccess( 'Waiting for character effects to be sent by server' )
                    return
                }

                if ( this.info.statusEffects.effects.length > 0 ) {
                    this.removeSingleStatusEffect()
                    return
                }

                if ( !this.createNextItem() ) {
                    return
                }

                this.lastEffectUpdate = this.info.statusEffects.lastUpdate
                this.notifySuccess( 'Created all items on ground' )

                this.step = Sequence.LocateHerbs
                return

            case Sequence.LocateHerbs:
                if ( !this.verifyAllItemsOnGround() ) {
                    this.notifyFailure( 'Unable to locate all required items on ground' )
                    return this.onClientExit( false )
                }

                this.notifySuccess( `Located ${ this.herbsOnGround.length } herbs for pick-up` )
                this.gameClient.sendPacket( TargetAction( this.getPlayerId(), TargetActionType.TargetThenInteract ) )

                this.step = Sequence.PrepareForPickUp
                return

            case Sequence.PrepareForPickUp:
                this.sendAdminCommand( this.bypassCommands.pop() )

                if ( this.bypassCommands.length === 0 ) {
                    this.step = Sequence.VerifyHpMp
                }

                return

            case Sequence.VerifyHpMp:
                if ( !this.verifyHealthAndMana() ) {
                    return this.onClientExit( false )
                }

                this.notifySuccess( 'Verified player Hp and Mp values' )
                this.step = Sequence.PerformPickUp
                return

            case Sequence.PerformPickUp:
                this.notifySuccess( 'Requesting to pick next item from ground' )
                this.requestItemPickUp()

                if ( this.herbsOnGround.length === 0 ) {
                    this.step = Sequence.VerifyEffects
                }

                return

            case Sequence.VerifyEffects:
                if ( !this.verifyNoItemsOnGround() ) {
                    this.notifyFailure( 'Not all items were picked up' )
                    return this.onClientExit( false )
                }

                if ( !this.verifyHerbEffects() ) {
                    this.displayStats()
                    return
                }

                this.notifySuccess( 'Verified herb effects' )
                this.performLogOut()
                return
        }
    }

    getCharacterName(): string {
        return 'tester1'
    }

    verifyAllItemsOnGround(): boolean {
        let itemIds = new RoaringBitmap32( herbIds )
        let objects = L2World.getObjectsByType( this.getPlayer(), Distance.DropDistance + Distance.DropDistance, L2ObjectType.Item )

        objects.forEach( ( item: L2Item ) => {
            if ( item.amount === 1 && herbIds.includes( item.itemId ) ) {
                this.herbsOnGround.push( item )
                itemIds.remove( item.itemId )
            }
        } )

        return itemIds.isEmpty
    }

    verifyNoItemsOnGround(): boolean {
        return L2World.getObjectsByType( this.getPlayer(), Distance.DropDistance + 10, L2ObjectType.Item ).length === 0
    }

    displayStats(): void {
        let player = this.getPlayer()
        this.notifySuccess( `Player ${ player.name } stats:`, `Hp=${ player.hp } (max ${ player.maxHp }) and Mp=${ player.mp } (max ${ player.maxMp }), Effects=${ JSON.stringify( this.info.statusEffects.effects ) }` )
    }

    createNextItem(): boolean {
        let player = this.getPlayer()
        let x = player.x + _.random( -Distance.DropDistance, Distance.DropDistance )
        let y = player.y + _.random( -Distance.DropDistance, Distance.DropDistance )
        let itemId = herbIds[ this.index ]

        let command = `create-item-location ${ itemId } ${ x } ${ y } ${ player.z } 1`
        this.sendAdminCommand( command )

        this.index++

        return herbIds.length === this.index
    }

    requestItemPickUp(): void {
        this.gameClient.sendPacket( TargetAction( this.herbsOnGround.pop().objectId, TargetActionType.TargetThenInteract ) )
    }

    verifyHealthAndMana(): boolean {
        let player = this.getPlayer()
        if ( ( player.mp / player.maxMp ) >= 0.7 ) {
            this.notifyFailure( 'Unable to reduce player mana' )
            return false
        }

        if ( ( player.hp / player.maxHp ) >= 0.7 ) {
            this.notifyFailure( 'Unable to reduce player health' )
            return false
        }

        if ( this.info.statusEffects.effects.length > 0 ) {
            this.notifyFailure( 'Unable to clear all effects' )
            return false
        }

        return true
    }

    verifyHerbEffects(): boolean {
        let player = this.getPlayer()
        if ( ( player.mp / player.maxMp ) < 0.5 ) {
            this.notifyFailure( 'Player Mp did not change' )
            return false
        }

        if ( ( player.hp / player.maxHp ) < 0.5 ) {
            this.notifyFailure( 'Player Hp did not change' )
            return false
        }

        if ( this.lastEffectUpdate === this.info.statusEffects.lastUpdate ) {
            this.notifyFailure( 'Status effects were not updated' )
            return false
        }

        if ( this.info.statusEffects.effects.length !== 1 ) {
            this.notifyFailure( 'Player has different than expected status effects' )
            return false
        }

        let [ effect ] = this.info.statusEffects.effects
        if ( effect.skillId !== 2285 || effect.skillLevel !== 1 ) {
            this.notifyFailure( `Wrong status effect with skillId=${effect.skillId} and skillLevel=${effect.skillLevel}` )
            return false
        }

        return true
    }

    removeSingleStatusEffect(): void {
        let buff = this.info.statusEffects.effects.pop()
        if ( !buff ) {
            return
        }

        this.gameClient.sendPacket( RequestRemoveBuff( this.getPlayerId(), buff.skillId, buff.skillLevel ) )
        this.notifySuccess( `Removing effect skillId=${ buff.skillId } and skillLevel=${ buff.skillLevel }` )
    }
}

new CreateAndPickHerbs().start()