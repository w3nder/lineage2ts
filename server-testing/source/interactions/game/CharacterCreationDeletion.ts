import { L2ClientLogic } from '../../client/L2ClientLogic'
import { L2LoginParameters } from '../../client/L2LoginClient'
import { L2ServerDefaults } from '../../client/enums/L2ServerDefaults'
import { PacketListenerMethod } from '../../client/packets/PacketMethodTypes'
import { CharacterSelectionInfo, CharacterSelectionInfoEvent } from '../../client/packets/game/receive/CharacterSelectionInfo'
import { CreateCharacter, CreateCharacterData } from '../../client/packets/game/send/CreateCharacter'
import { AvailableFaces } from '../../client/enums/PlayerFace'
import { AvailableHairColors, AvailableHairStyles } from '../../client/enums/PlayerHair'
import { Race } from '../../client/enums/Race'
import { AvailablePlayerSexes } from '../../client/enums/PlayerSex'
import { ClassId } from '../../client/enums/ClassId'
import {
    CharacterCreationFailure,
    CharacterCreationFailureEvent,
    CharacterCreationFailureMessage,
} from '../../client/packets/game/receive/CharacterCreationFailure'
import { CharacterCreationSuccess, CharacterCreationSuccessEvent } from '../../client/packets/game/receive/CharacterCreationSuccess'
import { ShowAvailableCharacters } from '../../client/packets/game/send/ShowAvailableCharacters'
import { DeleteCharacter } from '../../client/packets/game/send/DeleteCharacter'
import { CharacterDeleteFail, CharacterDeleteFailEvent, CharacterDeleteReason } from '../../client/packets/game/receive/CharacterDeleteFail'
import { CharacterDeleteSuccess } from '../../client/packets/game/receive/CharacterDeleteSuccess'
import Chance from 'chance'

const chance = new Chance()

const enum CreationDeletionState {
    None,
    Created,
    Deleted
}

export class CharacterCreationDeletion extends L2ClientLogic {
    interactionState: CreationDeletionState = CreationDeletionState.None
    playerSlot: number = -1
    playerName: string
    constructor() {
        super( CharacterCreationDeletion.name )
    }

    getLoginProperties(): L2LoginParameters {
        return {
            host: L2ServerDefaults.LocalhostIpAddress,
            password: 'admin',
            port: L2ServerDefaults.LoginServerPort,
            serverId: L2ServerDefaults.ServerId,
            userName: 'admin',
        }
    }

    getGamePacketListeners(): Record<string, PacketListenerMethod> {
        return {
            [ CharacterSelectionInfo.name ]: this.onCharacterSelectionInfo.bind( this ),
            [ CharacterCreationFailure.name ]: this.onCharacterCreationFailure.bind( this ),
            [ CharacterCreationSuccess.name ]: this.onCharacterCreationSuccess.bind( this ),
            [ CharacterDeleteFail.name ]: this.onCharacterDeleteFail.bind( this ),
            [ CharacterDeleteSuccess.name ]: this.onCharacterDeleteSuccess.bind( this )
        }
    }

    /*
        1. Receive available characters
        - check if there are available slots
        - proceed with character creation

        3. If we have created our character.
        - determine slot to delete since characters can be randomized
        - verify slot for newly created character
        - send deletion request with slot

        5. Confirmation of deletion success.
        - as before, extract character slot
        - verify that slot does not exist
     */
    onCharacterSelectionInfo( data: CharacterSelectionInfoEvent ): void {
        switch ( this.interactionState ) {
            case CreationDeletionState.Created:
                this.playerSlot = data.characters.findIndex( value => value.name === this.playerName )
                if ( this.playerSlot === -1 ) {
                    return this.terminateClient( false, `Unable to find created character using name: ${ this.playerName }` )
                }

                this.notifySuccess( 'Attempting to delete new character in slot', this.playerSlot.toString( 10 ) )
                return this.gameClient.sendPacket( DeleteCharacter( this.playerSlot ) )

            case CreationDeletionState.Deleted:
                this.playerSlot = data.characters.findIndex( value => value.name === this.playerName )
                if ( this.playerSlot !== -1 ) {
                    return this.terminateClient( false, `Character failed to be deleted by server: ${ this.playerName }` )
                }

                return this.terminateClient( true, 'Character creation/deletion flow has finished' )

            default:
                if ( data.characters.length === data.characterLimit ) {
                    return this.terminateClient( false, `No available characters to create. Limit is ${ data.characterLimit }` )
                }

                return this.gameClient.sendPacket( CreateCharacter( this.getCharacterData() ) )
        }
    }

    /*
        2. Server responds with character creation success
        - normally such response automatically means character is created, however we can do our own check on status
        - character created had higher index value than previous characters, however due to randomization on server side
          ( server configuration ), we cannot be certain that index is indeed correct.
          Thus, request available characters from server to verify new index. Keep in mind that server will have rate protection
          on both create character and character selection packets, which must be remedied by wait time.
     */
    onCharacterCreationSuccess( data: CharacterCreationSuccessEvent ) : void {
        if ( !data.isCharacterCreated ) {
            return this.terminateClient( false, 'Server refused character creation' )
        }

        this.notifySuccess( 'Created character with name', this.playerName )
        this.interactionState = CreationDeletionState.Created
        this.gameClient.sendPacket( ShowAvailableCharacters() )
    }

    /*
        4. Deletion is completed
        - server should send us available characters now
     */
    async onCharacterDeleteSuccess() : Promise<void> {
        this.interactionState = CreationDeletionState.Deleted
    }

    /*
        Server can reply to CreateCharacter packet to signify no character was created due to:
        - server found invalid character parameters such as name/face/hair and classId
        - server settings no longer allow character creation
        - account cannot create more characters
     */
    onCharacterCreationFailure( data: CharacterCreationFailureEvent ): void {
        this.terminateClient( false, CharacterCreationFailureMessage[ data.reason ] )
    }

    /*
        Deletion of character can fail for various reasons, mostly related to deleting clan member.
        A general error can mean:
        - server refused to delete a character
        - server cannot delete already non-existent character
        - deletion operation was performed too fast, in relation to previous deletion request (rate limit)
     */
    onCharacterDeleteFail( data: CharacterDeleteFailEvent ) : void {
        this.terminateClient( false, `Character deletion failed with reason : ${ CharacterDeleteReason[ data.reason ] }` )
    }

    /*
        Character data.
        - race option is used for verification of classId by server
        - invalid values for enums, classId (picking non-starting values) or long name will fail creation
     */
    getCharacterData(): CreateCharacterData {
        this.playerName = `Test${ chance.word( { capitalize: true } )}`

        return {
            classId: ClassId.DarkFighter,
            face: chance.pickone( AvailableFaces ),
            hairColor: chance.pickone( AvailableHairColors ),
            hairStyle: chance.pickone( AvailableHairStyles ),
            name: this.playerName,
            race: Race.DarkElf,
            sex: chance.pickone( AvailablePlayerSexes ),
        }
    }
}

new CharacterCreationDeletion().start()