import { DeclaredServerPacket } from '../../DeclaredServerPacket'

const binaryData : Array<number> = [
        0x04,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
]

export function RequestServerList( one: number, two: number ) : Buffer {
    return new DeclaredServerPacket( 32 )
            .writeC( 0x05 )
            .writeD( one )
            .writeD( two )
            .writeB( Buffer.from( binaryData ) )
            .getBuffer()
}