import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export enum ServerType {
    Normal = 1,
    Relax = 2,
    PublicTest = 4,
    Unclassified = 8,
    CharacterCreationRestricted = 16,
    Event = 32,
    FreeForAll = 64
}

export enum ServerStatus {
    Auto = 0x00,
    Good = 0x01,
    Normal = 0x02,
    Full = 0x03,
    Down = 0x04,
    GmOnly = 0x05,
}

export interface GameServerDetails extends PacketEvent {
    id: number
    ip: string
    port: number
    ageLimit: number
    isPVP: boolean
    onlinePlayers: number
    maximumPlayers: number
    status: ServerStatus
    serverType: ServerType // composite bitmask value
    usingBrackets: boolean
}

export interface ServerCharacterDetails extends PacketEvent {
    serverId: number
    ownedCharacterAmount: number
    accountExpirations: Array<number>
}

export interface LoginServerListEvent extends PacketEvent {
    lastServerId: number
    availableServers: Array<GameServerDetails>
    charactersOnServers: Array<ServerCharacterDetails>
}

export function LoginServerList( packet: ReadableClientPacket ) : LoginServerListEvent {
    let amount = packet.readC(), lastServerId = packet.readC()
    let availableServers = createAvailableServers( packet , amount )
    let charactersOnServers = createServerDetails( packet )

    return {
        availableServers,
        lastServerId,
        charactersOnServers
    }
}

function createAvailableServers( packet: ReadableClientPacket, amount: number ) : Array<GameServerDetails> {
    let servers : Array<GameServerDetails> = []

    while ( amount > 0 ) {
        servers.push( readAvailableServer( packet ) )
        amount--
    }

    return servers
}

function readAvailableServer( packet: ReadableClientPacket ) : GameServerDetails {
    let id = packet.readC()
    let ip = _.times( 4, () : number => packet.readC() ).join( '.' )

    let port = packet.readD(),
            ageLimit = packet.readC(),
            isPVP = packet.readC() === 0x01,
            onlinePlayers = packet.readH(),
            maximumPlayers = packet.readH(),
            status = packet.readC(),
            serverType = packet.readD(),
            usingBrackets = packet.readC() === 0x01

    return {
        ageLimit,
        id,
        ip,
        isPVP,
        maximumPlayers,
        onlinePlayers,
        port,
        serverType,
        status,
        usingBrackets
    }
}

function createServerDetails( packet: ReadableClientPacket ) : Array<ServerCharacterDetails> {
    let servers : Array<ServerCharacterDetails> = []
    let amount = packet.readC()

    while ( amount > 0 ) {
        servers.push( readServerCharacterDetail( packet ) )
        amount--
    }

    return servers
}

function readServerCharacterDetail( packet: ReadableClientPacket ) : ServerCharacterDetails {
    let serverId = packet.readC(), playerAmount = packet.readC(), amount = packet.readC()

    let accountExpirations : Array<number> = []
    while ( amount > 0 ) {
        accountExpirations.push( packet.readD() )
        amount--
    }

    return {
        accountExpirations,
        ownedCharacterAmount: playerAmount,
        serverId
    }
}