import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface LoginServerApprovedEvent extends PacketEvent {
    one: number
    two: number
}

export function ServerLoginApproved( packet: ReadableClientPacket ) : LoginServerApprovedEvent {
    let one = packet.readD(), two = packet.readD()

    return {
        one,
        two
    }
}