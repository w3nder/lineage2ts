import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export enum LoginFailReason {
    NoMessage = 0X00,
    SystemErrorLoginLater = 0X01,
    WrongCredentials = 0X02,
    AccessFailedTryAgainLater = 0X04,
    AccountInfoIncorrectContactSupport = 0X05,
    AccountInUse = 0X07,
    Under18Years = 0X0C,
    ServerOverloaded = 0X0F,
    ServerMaintenance = 0X10,
    TempPassExpired = 0X11,
    GameTimeExpired = 0X12,
    NoTimeLeft = 0X13,
    SystemError = 0X14,
    AccessFailed = 0X15,
    RestrictedIp = 0X16,
    WeekUsageFinished = 0X1E,
    SecurityCardNumberInvalid = 0X1F,
    AgeNotVerifiedCantLogBetween10Pm6Am = 0X20,
    ServerCannotBeAccessedByYourCoupon = 0X21,
    DualBox = 0X23,
    Inactive = 0X24,
    UserAgreementRejectedOnWebsite = 0X25,
    GuardianConsentRequired = 0X26,
    UserAgreementDeclinedOrWithdrawalRequest = 0X27,
    AccountSuspendedCall = 0X28,
    ChangePasswordAndQuizOnWebsite = 0X29,
    AlreadyLoggedInto10Accounts = 0X2A,
    MasterAccountRestricted = 0X2B,
    CertificationFailed = 0X2E,
    TelephoneCertificationUnavailable = 0X2F,
    TelephoneSignalsDelayed = 0X30,
    CertificationFailedLineBusy = 0X31,
    CertificationServiceNumberExpiredOrIncorrect = 0X32,
    CertificationServiceCurrentlyBeingChecked = 0X33,
    CertificationServiceCantBeUsedHeavyVolume = 0X34,
    CertificationServiceExpiredGameplayBlocked = 0X35,
    CertificationFailed3TimesGameplayBlocked30Min = 0X36,
    CertificationDailyUseExceeded = 0X37,
    CertificationUnderwayTryAgainLater = 0X38,
}

export interface LoginFailedEvent extends PacketEvent {
    reason: LoginFailReason
}

export function LoginFail( packet: ReadableClientPacket ) : LoginFailedEvent {
    return {
        reason: packet.readC()
    }
}