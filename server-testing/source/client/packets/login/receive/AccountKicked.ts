import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export enum AccountKickedReason {
    DataStealer = 0X01,
    GenericViolation = 0X08,
    SevenDaysSuspended = 0X10,
    PermanentlyBanned = 0X20,
}

export interface LoginAccountKickedEvent extends PacketEvent {
    reason: AccountKickedReason
}

export function LoginAccountKicked( packet: ReadableClientPacket ): LoginAccountKickedEvent {
    return {
        reason: packet.readC(),
    }
}