import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function ShowAvailableCharacters() : Buffer {
    return new DeclaredServerPacket( 3 )
            .writeC( 0xd0 )
            .writeH( 0x36 )
            .getBuffer()
}