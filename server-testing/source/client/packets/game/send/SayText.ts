import { DeclaredServerPacket, getStringSize } from '../../DeclaredServerPacket'

export const enum SayTextType {
    All = 0,
    Shout = 1,
    Tell = 2,
    Party = 3, // #
    Clan = 4, // @
    GM = 5,
    PetitionPlayer = 6,
    PetitionGM = 7,
    Trade = 8,
    Alliance = 9,
    Announce = 10,
    Boat = 11,
    Friend = 12,
    MSNChat = 13,
    PartymatchRoom = 14,
    PartyroomCommander = 15,
    PartyroomAll = 16,
    HeroVoice = 17,
    CriticalAnnouncement = 18,
    ScreenAnnouncement = 19,
    Battlefield = 20,
    MpccRoom = 21,
    NpcAll = 22,
    NpcShout = 23,
}

/*
    Please note that not all text types would be supported by game server.
    Certain types are only applicable to GM or Npc interactions. While others
    would be restricted conditionally, such as hero voice.
 */
export function SayText( text: string, type: SayTextType, playerName: string = null ) : Buffer {
    let playerNameSize = playerName ? getStringSize( playerName ) : 0
    let size = 5 + getStringSize( text ) + playerNameSize
    let packet = new DeclaredServerPacket( size )
            .writeC( 0x49 )
            .writeS( text )
            .writeD( type )

    if ( playerName ) {
        packet.writeS( playerName )
    }

    return packet.getBuffer()
}