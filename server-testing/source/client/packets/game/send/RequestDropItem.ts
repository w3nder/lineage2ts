import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function RequestDropItem( objectId: number, amount: number, x: number, y: number, z: number ) : Buffer {
    return new DeclaredServerPacket( 25 )
        .writeC( 0x17 )
        .writeD( objectId )
        .writeQ( amount )
        .writeD( x )

        .writeD( y )
        .writeD( z )
        .getBuffer()
}