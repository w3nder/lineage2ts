import { DeclaredServerPacket } from '../../DeclaredServerPacket'

const enum AnswerJoinPartyType {
    Decline = 0,
    Accept = 1
}

export function AnswerJoinParty( value: AnswerJoinPartyType = AnswerJoinPartyType.Accept ) : Buffer {
    return new DeclaredServerPacket( 5 )
        .writeC( 0x43 )
        .writeD( value )
        .getBuffer()
}