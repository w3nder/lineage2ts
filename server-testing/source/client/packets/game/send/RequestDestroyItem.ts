import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function RequestDestroyItem( objectId: number, amount: number ) : Buffer {
    return new DeclaredServerPacket( 13 )
        .writeC( 0x60 )
        .writeD( objectId )
        .writeQ( amount )
        .getBuffer()
}