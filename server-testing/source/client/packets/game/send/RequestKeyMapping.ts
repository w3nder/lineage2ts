import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function RequestKeyMapping() : Buffer {
    return new DeclaredServerPacket( 3 )
            .writeC( 0xd0 )
            .writeH( 0x21 )
            .getBuffer()
}