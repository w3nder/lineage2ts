import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function RequestManorList() : Buffer {
    return new DeclaredServerPacket( 3 )
            .writeC( 0xd0 )
            .writeH( 0x01 )
            .getBuffer()
}