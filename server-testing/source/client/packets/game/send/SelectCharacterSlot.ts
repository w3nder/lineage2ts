import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function SelectCharacterSlot( slotIndex: number ) : Buffer {
    return new DeclaredServerPacket( 19 )
            .writeC( 0x12 )
            .writeD( slotIndex )
            .writeH( 0 )
            .writeD( 0 )
            .writeD( 0 )
            .writeD( 0 )
            .getBuffer()
}