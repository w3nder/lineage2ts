import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function DeleteCharacter( slot: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x0d )
            .writeD( slot )
            .getBuffer()
}