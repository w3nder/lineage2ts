import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function ProtocolVersion( version: number ) : Buffer {
    return new DeclaredServerPacket( 5 )
            .writeC( 0x0e )
            .writeD( version )
            .getBuffer()
}