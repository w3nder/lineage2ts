import { DeclaredServerPacket } from '../../DeclaredServerPacket'
import { L2Character } from '../../../models/L2Character'

export function ValidatePosition( character: L2Character, vehicleId: number = 0 ) : Buffer {
    return new DeclaredServerPacket( 21 )
            .writeC( 0x59 )
            .writeD( character.x )
            .writeD( character.y )
            .writeD( character.z )

            .writeD( character.heading )
            .writeD( vehicleId )
            .getBuffer()
}