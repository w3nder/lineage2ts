import { DeclaredServerPacket } from '../../DeclaredServerPacket'

export function RequestAutoSoulShot( itemId: number, isEnabled: boolean ): Buffer {
    return new DeclaredServerPacket( 11 )
        .writeC( 0xd0 )
        .writeH( 0x0d )
        .writeD( itemId )
        .writeD( isEnabled ? 1 : 0 )
        .getBuffer()
}