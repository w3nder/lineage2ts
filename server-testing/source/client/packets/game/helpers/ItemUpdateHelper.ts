import { ReadableClientPacket } from '../../ReadableClientPacket'
import { L2Item } from '../../../models/L2Item'
import { L2World } from '../../../L2World'
import { ItemType } from '../../../enums/ItemType'
import { L2ObjectType } from '../../../enums/L2ObjectType'
import { ItemState } from '../../../enums/ItemState'

export function getItemForUpdate( objectId: number, defaultState: ItemState = ItemState.Unknown ) : L2Item {
    let existingItem : L2Item = L2World.getObject( objectId ) as L2Item
    if ( !existingItem ) {
        existingItem = {
            expirationTime: 0,
            mana: 0,
            equipMask: 0,
            spatialIndex: {
                objectId,
                minX: 0,
                minY: 0,
                maxX: 0,
                maxY: 0,
            },
            price: 0,
            isQuestItem: false,
            ownerObjectId: 0,
            augmentationId: 0,
            state: defaultState,
            isStackable: false,
            amount: 0,
            enchantLevel: 0,
            isEquipped: false,
            itemId: 0,
            itemType: ItemType.Other,
            objectId,
            type: L2ObjectType.Item,
            x: 0,
            y: 0,
            z: 0
        }

        L2World.setObject( existingItem )
    }

    return existingItem
}

export function readNormalItem( packet : ReadableClientPacket , defaultState: ItemState = ItemState.Unknown, ownerObjectId : number = 0 ) : L2Item {
    let objectId = packet.readD()
    let existingItem : L2Item = getItemForUpdate( objectId, defaultState )

    if ( ownerObjectId > 0 ) {
        existingItem.ownerObjectId = ownerObjectId
    }

    existingItem.itemId = packet.readD()

    packet.skipD( 1 )

    existingItem.amount = packet.readQ()
    existingItem.itemType = packet.readH()

    packet.skipH( 1 )

    existingItem.isEquipped = packet.readH() === 0x01
    existingItem.equipMask = packet.readD()
    existingItem.enchantLevel = packet.readH()

    packet.skipH( 1 )

    existingItem.augmentationId = packet.readD()
    existingItem.mana = packet.readD()

    let remainingSeconds = packet.readD()

    if ( remainingSeconds > 0 ) {
        existingItem.expirationTime = Date.now() + remainingSeconds * 1000
    }

    packet.skipB( 22 )

    return existingItem
}