import { L2Player } from '../../../models/L2Player'
import { L2World } from '../../../L2World'
import { L2ObjectType } from '../../../enums/L2ObjectType'
import { WaitType } from '../../../enums/WaitType'
import { MovementType } from '../../../enums/MovementType'
import { L2Character } from '../../../models/L2Character'
import { ClanPrivilege } from '../../../enums/ClanPrivileges'
import { MountType } from '../../../enums/MountType'
import { PlayerPvpFlag } from '../../../enums/PlayerPvpFlag'
import { PlayerStoreType } from '../../../enums/PlayerStoreType'
import { L2Summon } from '../../../models/L2Summon'
import { L2SummonType } from '../../../enums/SummonType'
import { L2Object } from '../../../models/L2Object'
import { L2Airship } from '../../../models/L2Vehicle'

export function getPlayerForUpdate( objectId: number ): L2Player {
    let player = L2World.getObject( objectId ) as L2Player
    if ( !player ) {
        player = {
            abnormalEffects: 0,
            displayEffects: 0,
            enchantEffects: 0,
            specialEffects: 0,
            accuracy: 0,
            canCrystallizeItems: false,
            clanPrivileges: ClanPrivilege.None,
            cp: 0,
            crit: 0,
            evasionRate: 0,
            isGM: false,
            magicAttack: 0,
            magicDefence: 0,
            maxCp: 0,
            pkKills: 0,
            powerAttack: 0,
            powerDefence: 0,
            pvpKills: 0,
            CON: 0,
            DEX: 0,
            INT: 0,
            MEN: 0,
            STR: 0,
            WIT: 0,
            baseClassId: 0,
            classId: 0,
            exp: 0,
            hp: 0,
            isVisible: false,
            karma: 0,
            level: 0,
            magicAttackSpeed: 0,
            maxHp: 0,
            maxMp: 0,
            mountType: MountType.None,
            mountNpcId: 0,
            mp: 0,
            paperdollObjectId: {},
            powerAttackSpeed: 0,
            pvpFlag: PlayerPvpFlag.None,
            race: undefined,
            runSpeed: 0,
            sex: undefined,
            sp: 0,
            storeType: PlayerStoreType.None,
            walkSpeed: 0,
            weightInventory: 0,
            weightLimit: 0,
            allyId: 0,
            clanId: 0,
            destinationX: 0,
            destinationY: 0,
            destinationZ: 0,
            heading: 0,
            isAttackable: false,
            isDead: false,
            isFlying: false,
            isInCombat: false,
            isRunning: false,
            isTargetable: false,
            movementType: MovementType.Walking,
            name: '',
            objectId,
            paperdollItemId: {},
            stats: {},
            targetId: 0,
            templateId: 0,
            title: '',
            type: L2ObjectType.Player,
            waitType: WaitType.Standing,
            x: 0,
            y: 0,
            z: 0,
            spatialIndex: {
                objectId,
                minX: 0,
                minY: 0,
                maxX: 0,
                maxY: 0,
            }
        }

        L2World.setObject( player )
    }

    return player
}

export function getCharacterForUpdate( objectId: number ): L2Character {
    let npc = L2World.getObject( objectId ) as L2Character
    if ( !npc ) {
        npc = {
            abnormalEffects: 0,
            displayEffects: 0,
            enchantEffects: 0,
            specialEffects: 0,
            spatialIndex: {
                objectId,
                minX: 0,
                minY: 0,
                maxX: 0,
                maxY: 0,
            },
            hp: 0,
            level: 0,
            maxHp: 0,
            maxMp: 0,
            mp: 0,
            allyId: 0,
            clanId: 0,
            destinationX: 0,
            destinationY: 0,
            destinationZ: 0,
            heading: 0,
            isAttackable: false,
            isDead: false,
            isFlying: false,
            isInCombat: false,
            isRunning: false,
            isTargetable: false,
            movementType: MovementType.Walking,
            name: '',
            objectId,
            stats: {},
            templateId: 0,
            title: '',
            type: L2ObjectType.Npc,
            waitType: WaitType.Standing,
            x: 0,
            y: 0,
            z: 0
        }

        L2World.setObject( npc )
    }

    return npc
}

export function getSummonForUpdate( objectId: number ): L2Summon {
    let summon = L2World.getObject( objectId ) as L2Summon
    if ( !summon ) {
        summon = {
            abnormalEffects: 0,
            displayEffects: 0,
            enchantEffects: 0,
            specialEffects: 0,
            exp: 0,
            feed: 0,
            karma: 0,
            maxFeed: 0,
            pvpFlag: PlayerPvpFlag.None,
            sp: 0,
            ownerObjectId: 0,
            level: 0,
            hp: 0,
            maxHp: 0,
            maxMp: 0,
            mp: 0,
            summonType: L2SummonType.None,
            allyId: 0,
            clanId: 0,
            destinationX: 0,
            destinationY: 0,
            destinationZ: 0,
            heading: 0,
            isAttackable: false,
            isDead: false,
            isFlying: false,
            isInCombat: false,
            isRunning: false,
            isTargetable: false,
            movementType: MovementType.Walking,
            name: '',
            objectId,
            stats: {},
            templateId: 0,
            title: '',
            type: L2ObjectType.Summon,
            waitType: WaitType.Standing,
            x: 0,
            y: 0,
            z: 0,
            spatialIndex: {
                objectId,
                minX: 0,
                minY: 0,
                maxX: 0,
                maxY: 0,
            }
        }

        L2World.setObject( summon )
    }

    return summon
}

export function getObjectForUpdate( objectId: number, type: L2ObjectType = L2ObjectType.None ): L2Object {
    let npc = L2World.getObject( objectId )
    if ( !npc ) {
        npc = {
            objectId,
            type,
            x: 0,
            y: 0,
            z: 0,
            spatialIndex: {
                objectId,
                minX: 0,
                minY: 0,
                maxX: 0,
                maxY: 0,
            },
        }

        L2World.setObject( npc )
    }

    return npc
}

export function getAirshipForUpdate( objectId: number ): L2Airship {
    let airship = L2World.getObject( objectId ) as L2Airship
    if ( !airship ) {
        airship = {
            fuel: 0,
            maxFuel: 0,
            moveSpeed: 0,
            rotationSpeed: 0,
            captainId: 0,
            destinationX: 0,
            destinationY: 0,
            destinationZ: 0,
            heading: 0,
            helmObjectId: 0,
            objectId,
            type: L2ObjectType.Airship,
            x: 0,
            y: 0,
            z: 0,
            spatialIndex: {
                objectId,
                minX: 0,
                minY: 0,
                maxX: 0,
                maxY: 0,
            },
        }

        L2World.setObject( airship )
    }

    return airship
}