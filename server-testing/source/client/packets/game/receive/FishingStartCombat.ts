import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { FishGrade } from '../../../enums/FishGrade'

export const enum FishingMode {
    Standard,
    Alternate
}

export const enum FishDeceptiveMode {
    None,
    Enabled
}

export interface FishingStartCombatEvent extends PacketEvent {
    playerObjectId: number
    time: number
    fishHp: number
    mode: FishingMode
    fishGrade: FishGrade
    deceptiveMode: FishDeceptiveMode
}
export function FishingStartCombat( packet: ReadableClientPacket ) : FishingStartCombatEvent {
    let playerObjectId = packet.readD(),
            time = packet.readD(),
            fishHp = packet.readD(),
            mode = packet.readC(),
            fishGrade = packet.readC(),
            deceptiveMode = packet.readC()

    return {
        playerObjectId,
        time,
        fishHp,
        fishGrade,
        mode,
        deceptiveMode
    }
}