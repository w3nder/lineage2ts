import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum SayTextType {
    All = 0,
    Shout = 1,
    Tell = 2,
    Party = 3, // #
    Clan = 4, // @
    GM = 5,
    PetitionPlayer = 6,
    PetitionGM = 7,
    Trade = 8,
    Alliance = 9,
    Announce = 10,
    Boat = 11,
    Friend = 12,
    MSNChat = 13,
    PartymatchRoom = 14,
    PartyroomCommander = 15,
    PartyroomAll = 16,
    HeroVoice = 17,
    CriticalAnnouncement = 18,
    ScreenAnnouncement = 19,
    Battlefield = 20,
    MpccRoom = 21,
    NpcAll = 22,
    NpcShout = 23,
}

export interface SayTextEvent extends PacketEvent {
    objectId: number
    type: SayTextType
    stringId: number
    messages: Array<string>
    characterName: string
    characterTemplateId: number
}

export function SayText( packet: ReadableClientPacket ) : SayTextEvent {
    let objectId = packet.readD(),
            type = packet.readD()

    let characterTemplateId = 0
    let characterName = ''
    if ( type !== SayTextType.Boat ) {
        characterName = packet.readS()
    } else {
        characterTemplateId = packet.readD()
    }

    let stringId = packet.readD()
    let messages : Array<string> = []

    while ( packet.hasMoreData() ) {
        messages.push( packet.readS() )
    }

    return {
        objectId,
        type,
        characterTemplateId,
        characterName,
        stringId,
        messages
    }
}