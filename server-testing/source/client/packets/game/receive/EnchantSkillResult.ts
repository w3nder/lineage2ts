import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface EnchantSkillResultEvent extends PacketEvent {
    isSuccess: boolean
}
export function EnchantSkillResult( packet: ReadableClientPacket ) : EnchantSkillResultEvent {
    return {
        isSuccess: packet.readD() === 1
    }
}