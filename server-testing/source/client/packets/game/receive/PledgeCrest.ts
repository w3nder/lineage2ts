import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PledgeCrestEvent extends PacketEvent {
    crestId: number
    crestData: Buffer
}

export function PledgeCrest( packet: ReadableClientPacket ) : PledgeCrestEvent {
    let crestId = packet.readD(),
            size = packet.readD()

    return {
        crestId,
        crestData: packet.readB( size )
    }
}