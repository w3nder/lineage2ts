import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowSeedSettingData {
    id: number
    level: number
    rewardOneId: number
    rewardTwoId: number
    saleLimit: number
    referencePrice: number
    minPrice: number
    maxPrice: number
    currentProductionStartAmount: number
    currentProductionPrice: number
    nextProductionStartAmount: number
    nextProductionPrice: number
}

export interface ShowSeedSettingEvent extends PacketEvent {
    manorId: number
    seeds: Array<ShowSeedSettingData>
}

export function ShowSeedSetting( packet: ReadableClientPacket ) : ShowSeedSettingEvent {
    let manorId = packet.readD(),
            size = packet.readD()

    return {
        manorId,
        seeds: _.times( size, () : ShowSeedSettingData => {
            let id = packet.readD(),
                    level = packet.readD()

            packet.skipB( 1 )

            let rewardOneId = packet.readD()

            packet.skipB( 1 )

            let rewardTwoId = packet.readD(),
                    saleLimit = packet.readD(),
                    referencePrice = packet.readD(),
                    minPrice = packet.readD(),
                    maxPrice = packet.readD(),
                    currentProductionStartAmount = packet.readQ(),
                    currentProductionPrice = packet.readQ(),
                    nextProductionStartAmount = packet.readQ(),
                    nextProductionPrice = packet.readQ()

            return {
                id,
                level,
                rewardOneId,
                rewardTwoId,
                saleLimit,
                referencePrice,
                minPrice,
                maxPrice,
                currentProductionStartAmount,
                currentProductionPrice,
                nextProductionStartAmount,
                nextProductionPrice
            }
        } )
    }
}