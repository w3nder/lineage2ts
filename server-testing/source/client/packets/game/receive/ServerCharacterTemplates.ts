import { PacketEvent } from '../../PacketMethodTypes'
import { ReadableClientPacket } from '../../ReadableClientPacket'

/*
    Character creation templates sent by server.
    No reason to read any values here since we can assume defaults.
*/
export interface ServerCharacterTemplatesEvent extends PacketEvent {

}

export function ServerCharacterTemplates( packet : ReadableClientPacket ) : ServerCharacterTemplatesEvent {
    return {}
}