import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowCropSettingCrop {
    startAmount: number
    price: number
    rewardId: number
}
export interface ShowCropSettingSeed {
    cropId: number
    level: number
    rewardOne: number
    rewardTwo: number
    saleLimit: number
    minimumPrice: number
    maximumPrice: number
    currentCrop: ShowCropSettingCrop
    nextCrop: ShowCropSettingCrop
}

export interface ShowCropSettingEvent extends PacketEvent {
    manorId: number
    availableSeeds: Array<ShowCropSettingSeed>
}

function readCropData( packet : ReadableClientPacket ) : ShowCropSettingCrop {
    let startAmount = packet.readQ(),
            price = packet.readQ(),
            rewardId = packet.readC()

    return {
        startAmount,
        price,
        rewardId
    }
}
export function ShowCropSetting( packet: ReadableClientPacket ) : ShowCropSettingEvent {
    let manorId = packet.readD(),
            size = packet.readD()

    return {
        manorId,
        availableSeeds: _.times( size, () : ShowCropSettingSeed => {
            let cropId = packet.readD(),
                    level = packet.readD()

            packet.skipB( 1 )

            let rewardOne = packet.readD()

            packet.skipB( 1 )

            let rewardTwo = packet.readD(),
                    saleLimit = packet.readD()

            packet.skipD( 1 )

            let minimumPrice = packet.readD(),
                    maximumPrice = packet.readD(),
                    currentCrop = readCropData( packet ),
                    nextCrop = readCropData( packet )

            return {
                cropId,
                level,
                rewardOne,
                rewardTwo,
                saleLimit,
                minimumPrice,
                maximumPrice,
                currentCrop,
                nextCrop
            }
        } )
    }
}