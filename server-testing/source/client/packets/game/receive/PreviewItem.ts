import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { InventorySlot } from '../../../enums/Paperdoll'
import _ from 'lodash'

export const PreviewItemSlots: Array<InventorySlot> = [
    InventorySlot.Under,
    InventorySlot.RightEar,
    InventorySlot.LeftEar,
    InventorySlot.Neck,
    InventorySlot.RightFinger,
    InventorySlot.LeftFinger,
    InventorySlot.Head,
    InventorySlot.RightHand,
    InventorySlot.LeftHand,
    InventorySlot.Gloves,
    InventorySlot.Chest,
    InventorySlot.Legs,
    InventorySlot.Feet,
    InventorySlot.Cloak,
    InventorySlot.RightHand,
    InventorySlot.Hair,
    InventorySlot.HairSecondary,
    InventorySlot.RightBracelet,
    InventorySlot.LeftBracelet,
]

export interface PreviewItemEvent extends PacketEvent {
    itemIds: Array<number>
}

export function PreviewItem( packet: ReadableClientPacket ) : PreviewItemEvent {
    let size = packet.readD()

    if ( size !== PreviewItemSlots.length ) {
        return {
            itemIds: []
        }
    }

    return {
        itemIds: _.times( size, () : number => packet.readD() )
    }
}