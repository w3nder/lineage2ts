import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getCharacterForUpdate } from '../helpers/CharacterUpdateHelper'
import { L2ObjectType } from '../../../enums/L2ObjectType'
import { L2Door } from '../../../models/L2Door'

export interface StaticObjectEvent extends PacketEvent {
    objectId: number
    isDoor: boolean
}

export function StaticObject( packet : ReadableClientPacket ) : StaticObjectEvent {
    let templateId = packet.readD(),
            objectId = packet.readD(),
            isDoor = packet.readD() === 1

    /*
        Have to cast character to L2Door due to typescript not liking
        using 'as' expression later in code.
     */
    let staticObject = getCharacterForUpdate( objectId ) as L2Door

    staticObject.templateId = templateId
    staticObject.isTargetable = packet.readD() === 1

    if ( isDoor ) {
        staticObject.name = 'Door'
        staticObject.type = L2ObjectType.Door

        packet.skipD( 1 )
        staticObject.isClosed = packet.readD() === 1
    } else {
        packet.skipD( 2 )
    }

    staticObject.isAttackable = packet.readD() === 1
    staticObject.hp = packet.readD()
    staticObject.maxHp = packet.readD()

    return {
        objectId,
        isDoor
    }
}