import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export interface PartySmallWindowDeleteEvent extends PacketEvent {
    memberObjectId: number
}
export function PartySmallWindowDelete( packet : ReadableClientPacket ) : PartySmallWindowDeleteEvent {
    let memberObjectId = packet.readD()

    let player = getPlayerForUpdate( memberObjectId )
    player.name = packet.readS()

    return {
        memberObjectId
    }
}