import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { PartyDistributionType } from '../../../enums/PartyDistributionType'

export interface PartyMatchDetailEvent extends PacketEvent {
    id: number
    memberLimit: number
    minimumLevel: number
    maximumLevel: number
    type: PartyDistributionType
    bbsId: number
    title: string
}

export function PartyMatchDetail( packet: ReadableClientPacket ) : PartyMatchDetailEvent {
    let id = packet.readD(),
            memberLimit = packet.readD(),
            minimumLevel = packet.readD(),
            maximumLevel = packet.readD(),
            type = packet.readD(),
            bbsId = packet.readD(),
            title = packet.readS()

    return {
        id,
        memberLimit,
        minimumLevel,
        maximumLevel,
        title,
        type,
        bbsId
    }
}