import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface FishingEndEvent extends PacketEvent {
    playerObjectId: number
    isSuccess: boolean
}
export function FishingEnd( packet : ReadableClientPacket ) : FishingEndEvent {
    let playerObjectId = packet.readD(),
            isSuccess = packet.readC() === 1

    return {
        playerObjectId,
        isSuccess
    }
}