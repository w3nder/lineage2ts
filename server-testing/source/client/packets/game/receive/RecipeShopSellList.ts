import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { RecipeShopManageItem } from '../../../models/PrivateStore'
import _ from 'lodash'

export interface RecipeShopSellListEvent extends PacketEvent {
    makerPlayerObjectId: number
    makerMp: number
    makerMaxMp: number
    buyerAdena: number
    items: Array<RecipeShopManageItem>
}

export function RecipeShopSellList( packet: ReadableClientPacket ) : RecipeShopSellListEvent {
    let makerPlayerObjectId = packet.readD(),
            makerMp = packet.readD(),
            makerMaxMp = packet.readD(),
            buyerAdena = packet.readQ(),
            size = packet.readD()

    return {
        makerPlayerObjectId,
        makerMp,
        makerMaxMp,
        buyerAdena,
        items: _.times( size, () : RecipeShopManageItem => {
            let recipeId = packet.readD()

            packet.skipD( 1 )

            let adenaCost = packet.readQ()

            return {
                recipeId,
                adenaCost
            }
        } )
    }
}