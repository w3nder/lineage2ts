import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import _ from 'lodash'
import { ItemState } from '../../../enums/ItemState'

export interface ItemListEvent extends PacketEvent {
    showInventoryWindow: boolean
    itemObjectIds : Array<number>
}
export function ItemList( packet : ReadableClientPacket ) : ItemListEvent {
    let showInventoryWindow = packet.readH() === 0x01
    let amount = packet.readH()

    return {
        showInventoryWindow,
        itemObjectIds: _.times( amount, () : number => {
            return readNormalItem( packet, ItemState.PickedUp ).objectId
        } )
    }
}