import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'
import _ from 'lodash'

export interface MultiPartyCommandChannelInfoEvent extends PacketEvent {
    playerCount: number
    leaderPlayerObjectIds : Array<number>
    channelLeaderName: string
}
export function MultiPartyCommandChannelInfo( packet : ReadableClientPacket ) : MultiPartyCommandChannelInfoEvent {
    let channelLeaderName = packet.readS()

    packet.skipD( 1 )

    let playerCount = packet.readD(),
            size = packet.readD()

    return {
        channelLeaderName,
        playerCount,
        leaderPlayerObjectIds: _.times( size, () : number => {
            let name = packet.readS(),
                    objectId = packet.readD()

            packet.skipD( 1 )

            let player = getPlayerForUpdate( objectId )
            player.name = name

            return objectId
        } )
    }
}