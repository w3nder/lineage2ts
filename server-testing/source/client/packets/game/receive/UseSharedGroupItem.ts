import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface UseSharedGroupItemEvent extends PacketEvent {
    itemId: number
    groupId: number
    remainingTime: number
    expirationTime: number
}

export function UseSharedGroupItem( packet: ReadableClientPacket ) : UseSharedGroupItemEvent {
    let itemId = packet.readD(),
            groupId = packet.readD(),
            remainingTime = packet.readD(),
            expirationTime = packet.readD()

    return {
        itemId,
        groupId,
        remainingTime,
        expirationTime
    }
}