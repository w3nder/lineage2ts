import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getObjectForUpdate } from '../helpers/CharacterUpdateHelper'
import { L2ObjectType } from '../../../enums/L2ObjectType'
import { L2Vehicle } from '../../../models/L2Vehicle'

export interface VehicleDepartureEvent extends PacketEvent {
    objectId: number
    speed: number
    rotation: number
}
export function VehicleDeparture( packet : ReadableClientPacket ) : VehicleDepartureEvent {
    let objectId = packet.readD(),
            speed = packet.readD(),
            rotation = packet.readD()

    let vehicle = getObjectForUpdate( objectId, L2ObjectType.Vehicle ) as L2Vehicle
    vehicle.destinationX = packet.readD()
    vehicle.destinationY = packet.readD()
    vehicle.destinationZ = packet.readD()

    return {
        objectId,
        speed,
        rotation
    }
}