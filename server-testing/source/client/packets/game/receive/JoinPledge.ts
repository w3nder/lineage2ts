import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface JoinPledgeEvent extends PacketEvent {
    pledgeId: number
}

export function JoinPledge( packet: ReadableClientPacket ) : JoinPledgeEvent {
    return {
        pledgeId: packet.readD()
    }
}