import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface SiegeInfoEvent extends PacketEvent {
    residenceId: number
    isPlayerClanOwner: boolean
    clanId: number
    clanName: string
    clanLeaderName: string
    allyId: number
    allyName: string
    currentTime: number
    siegeTime: number
}

export function SiegeInfo( packet: ReadableClientPacket ) : SiegeInfoEvent {
    let residenceId = packet.readD(),
            isPlayerClanOwner = packet.readD() === 1,
            clanId = packet.readD(),
            clanName = packet.readS(),
            clanLeaderName = packet.readS(),
            allyId = packet.readD(),
            allyName = packet.readS(),
            currentTime = packet.readD(),
            siegeTime = packet.readD()

    return {
        residenceId,
        isPlayerClanOwner,
        clanId,
        clanName,
        clanLeaderName,
        allyId,
        allyName,
        currentTime,
        siegeTime
    }
}