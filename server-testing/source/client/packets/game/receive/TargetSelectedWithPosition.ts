import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2World } from '../../../L2World'
import { L2ObjectType } from '../../../enums/L2ObjectType'
import { L2Player } from '../../../models/L2Player'

export interface TargetSelectedWithPositionEvent extends PacketEvent {
    playerObjectId: number
    targetObjectId: number
}

export function TargetSelectedWithPosition( packet: ReadableClientPacket ): TargetSelectedWithPositionEvent {
    let playerObjectId = packet.readD(),
            targetObjectId = packet.readD()

    let existingCharacter = L2World.getObject( playerObjectId ) as L2Player
    if ( existingCharacter && existingCharacter.type === L2ObjectType.Player ) {
        existingCharacter.targetId = targetObjectId
        existingCharacter.x = packet.readD()
        existingCharacter.y = packet.readD()
        existingCharacter.z = packet.readD()
    }

    return {
        playerObjectId,
        targetObjectId,
    }
}