import { ReadableClientPacket } from '../../ReadableClientPacket'
import { readSystemMessageParameters, SystemMessageEvent } from './SystemMessage'

export interface ConfirmDialogEvent extends SystemMessageEvent {
    time: number
    requesterId: number
}

export function ConfirmDialog( packet: ReadableClientPacket ): ConfirmDialogEvent {
    let messageId = packet.readD(),
            size = packet.readD()
    let parameters = readSystemMessageParameters( packet, size )

    let time = packet.readD(),
            requesterId = packet.readD()
    return {
        messageId,
        parameters,
        time,
        requesterId,
        receivedTime: Date.now()
    }
}