import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface TutorialShowQuestionMarkEvent extends PacketEvent {
    id: number
}
export function TutorialShowQuestionMark( packet : ReadableClientPacket ) : TutorialShowQuestionMarkEvent {
    return {
        id: packet.readD()
    }
}