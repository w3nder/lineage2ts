import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface StartScenePlayerEvent extends PacketEvent {
    movieId: number
}

export function StartScenePlayer( packet: ReadableClientPacket ) : StartScenePlayerEvent {
    return {
        movieId: packet.readD()
    }
}