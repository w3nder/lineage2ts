import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface AutoAttackStartEvent extends PacketEvent {
    targetId: number
}
export function AutoAttackStart( packet : ReadableClientPacket ) : AutoAttackStartEvent {
    return {
        targetId: packet.readD()
    }
}