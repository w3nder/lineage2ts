import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import _ from 'lodash'

/*
    Packet can be sent for both player or clan warehouse, so
    particular caution must be observed to differentiate.
 */
export interface GMViewWarehouseWithdrawListEvent extends PacketEvent {
    name: string
    adena: number
    itemObjectIds: Array<number>
}

export function GMViewWarehouseWithdrawList( packet: ReadableClientPacket ) : GMViewWarehouseWithdrawListEvent {
    let name = packet.readS(),
            adena = packet.readQ(),
            size = packet.readH()

    return {
        name,
        adena,
        itemObjectIds: _.times( size, () : number => {
            let item = readNormalItem( packet )

            packet.skipD( 1 )

            return item.objectId
        } )
    }
}