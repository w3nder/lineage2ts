import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { PartyDistributionType } from '../../../enums/PartyDistributionType'
import _ from 'lodash'
import { getPlayerForUpdate, getSummonForUpdate } from '../helpers/CharacterUpdateHelper'
import { L2Summon } from '../../../models/L2Summon'

export interface PartySmallWindowAllEvent extends PacketEvent {
    leaderObjectId: number
    memberObjectIds: Array<number>
    type: PartyDistributionType

}

export function PartySmallWindowAll( packet: ReadableClientPacket ): PartySmallWindowAllEvent {
    let leaderObjectId = packet.readD(),
            type = packet.readD(),
            size = packet.readD()

    let memberObjectIds: Array<number> = []

    _.times( size, () => {
        let memberObjectId = packet.readD()
        memberObjectIds.push( memberObjectId )

        let player = getPlayerForUpdate( memberObjectId )

        player.name = packet.readS()
        player.cp = packet.readD()
        player.maxCp = packet.readD()
        player.hp = packet.readD()
        player.maxHp = packet.readD()
        player.mp = packet.readD()
        player.maxMp = packet.readD()
        player.level = packet.readD()
        player.classId = packet.readD()

        packet.skipD( 1 )

        player.race = packet.readD()

        packet.skipD( 2 )

        let summonObjectId = packet.readD()
        if ( summonObjectId === 0 ) {
            return
        }

        memberObjectIds.push( summonObjectId )

        let summon = getSummonForUpdate( summonObjectId ) as L2Summon

        summon.templateId = packet.readD() - 1000000
        summon.summonType = packet.readD()
        summon.name = packet.readS()
        summon.hp = packet.readD()
        summon.maxHp = packet.readD()
        summon.mp = packet.readD()
        summon.maxMp = packet.readD()
        summon.level = packet.readD()
    } )

    return {
        leaderObjectId,
        memberObjectIds,
        type
    }
}