import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowSentPostData {
    id: number
    title: string
    senderName: string
    isLocked: boolean
    expirationInSeconds: number
    isRead: boolean
    hasAttachments: boolean
}

export interface ShowSentPostListEvent extends PacketEvent {
    messages: Array<ShowSentPostData>
}

export function ShowSentPostList( packet: ReadableClientPacket ) : ShowSentPostListEvent {
    packet.skipD( 1 )

    let size = packet.readD()
    return {
        messages: _.times( size, () : ShowSentPostData => {
            let id = packet.readD(),
                    title = packet.readS(),
                    senderName = packet.readS(),
                    isLocked = packet.readD() === 1,
                    expirationInSeconds = packet.readD(),
                    isRead = packet.readD() === 0

            packet.skipD( 1 )

            let hasAttachments = packet.readD() === 1

            return {
                id,
                title,
                senderName,
                isLocked,
                expirationInSeconds,
                isRead,
                hasAttachments
            }
        } )
    }
}