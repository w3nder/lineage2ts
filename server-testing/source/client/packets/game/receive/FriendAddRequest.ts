import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface FriendAddRequestEvent extends PacketEvent {
    name: string
}

export function FriendAddRequest( packet: ReadableClientPacket ) : FriendAddRequestEvent {
    return {
        name: packet.readS()
    }
}