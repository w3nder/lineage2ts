import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getAirshipForUpdate } from '../helpers/CharacterUpdateHelper'
import { L2ObjectType } from '../../../enums/L2ObjectType'

export interface AirShipInfoEvent extends PacketEvent {
    objectId: number
}

export function AirShipInfo( packet: ReadableClientPacket ): AirShipInfoEvent {
    let objectId = packet.readD()
    let airship = getAirshipForUpdate( objectId )

    airship.x = packet.readD()
    airship.y = packet.readD()
    airship.z = packet.readD()
    airship.heading = packet.readD()

    airship.captainId = packet.readD()
    airship.moveSpeed = packet.readD()
    airship.rotationSpeed = packet.readD()
    airship.helmObjectId = packet.readD()

    packet.skipD( 6 )

    airship.fuel = packet.readD()
    airship.maxFuel = packet.readD()

    return {
        objectId
    }
}