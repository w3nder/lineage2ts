import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface StorageMaxCountEvent extends PacketEvent {
    inventory: number
    warehouse: number
    clan: number
    sellStore: number
    buyStore: number
    dwarfRecipes: number
    commonRecipes: number
    inventoryExtra: number
    inventoryQuest: number
}
export function StorageMaxCount( packet : ReadableClientPacket ) : StorageMaxCountEvent {
    let inventory = packet.readD(),
            warehouse = packet.readD(),
            clan = packet.readD(),
            sellStore = packet.readD(),
            buyStore = packet.readD(),
            dwarfRecipes = packet.readD(),
            commonRecipes = packet.readD(),
            inventoryExtra = packet.readD(),
            inventoryQuest = packet.readD()

    return {
        inventory,
        warehouse,
        clan,
        sellStore,
        buyStore,
        dwarfRecipes,
        commonRecipes,
        inventoryExtra,
        inventoryQuest
    }
}