import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'
import { readLocation } from '../helpers/PropertyHelper'

export interface EnterObservationModeEvent extends PacketEvent, LocationProperties {}

export function EnterObservationMode( packet: ReadableClientPacket ) : EnterObservationModeEvent {
    return readLocation( packet ) as EnterObservationModeEvent
}