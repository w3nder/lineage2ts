import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface SendTradeRequestEvent extends PacketEvent {
    requesterObjectId: number
}
export function SendTradeRequest( packet: ReadableClientPacket ) : SendTradeRequestEvent {
    return {
        requesterObjectId: packet.readD()
    }
}