import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum OlympiadModeType {
    Exit = 0,
    Arena = 2,
    Observer = 3
}

export interface OlympiadModeEvent extends PacketEvent {
    type: OlympiadModeType
}

export function OlympiadMode( packet: ReadableClientPacket ) : OlympiadModeEvent {
    return {
        type: packet.readC()
    }
}