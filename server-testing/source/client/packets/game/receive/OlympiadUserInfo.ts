import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export const enum OlympiadUserInfoSide {
    None = -1,
    First = 1,
    Second = 2
}

export interface OlympiadUserInfoEvent extends PacketEvent {
    playerObjectId: number
    olympiadSide: OlympiadUserInfoSide
}
export function OlympiadUserInfo( packet: ReadableClientPacket ) : OlympiadUserInfoEvent {
    let olympiadSide = packet.readC(),
            playerObjectId = packet.readD()

    let player = getPlayerForUpdate( playerObjectId )

    player.name = packet.readS()
    player.classId = packet.readD()
    player.hp = packet.readD()
    player.maxHp = packet.readD()
    player.cp = packet.readD()
    player.maxCp = packet.readD()

    return {
        playerObjectId,
        olympiadSide
    }
}