import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowFortressInfoData {
    id: number
    ownerClanName: string
    isSiegeInProgress: boolean
    ownedTime: number
}
export interface ShowFortressInfoEvent extends PacketEvent {
    forts: Array<ShowFortressInfoData>
}

export function ShowFortressInfo( packet: ReadableClientPacket ) : ShowFortressInfoEvent {
    let size = packet.readD()

    return {
        forts: _.times( size, () : ShowFortressInfoData => {
            let id = packet.readD(),
                    ownerClanName = packet.readS(),
                    isSiegeInProgress = packet.readD() === 1,
                    ownedTime = packet.readD()

            return {
                id,
                ownerClanName,
                isSiegeInProgress,
                ownedTime
            }
        } )
    }
}