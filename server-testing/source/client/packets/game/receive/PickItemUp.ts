import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getItemForUpdate } from '../helpers/ItemUpdateHelper'
import { ItemState } from '../../../enums/ItemState'

export interface PickItemUpEvent extends PacketEvent {
    pickerObjectId: number
    itemObjectId: number
    x: number
    y: number
    z: number
}

/*
    Used only to signify animation on client side. While some coordinate information is provided,
    game server inventory management will send out either ItemList or InventoryUpdate packet if current
    player has gained item. Otherwise, one can assume other players caused generation of such packet.
 */
export function PickItemUp( packet: ReadableClientPacket ): PickItemUpEvent {
    let pickerObjectId = packet.readD(),
            itemObjectId = packet.readD(),
            x = packet.readD(),
            y = packet.readD(),
            z = packet.readD()

    let existingItem = getItemForUpdate( itemObjectId )

    existingItem.x = x
    existingItem.y = y
    existingItem.z = z
    existingItem.state = ItemState.PickedUp
    existingItem.ownerObjectId = pickerObjectId

    return {
        pickerObjectId,
        itemObjectId,
        x,
        y,
        z,
    }
}