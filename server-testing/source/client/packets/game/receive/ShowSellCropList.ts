import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowSellCropData {
    itemObjectId: number
    itemId: number
    level: number
    rewardOneId: number
    rewardTwoId: number
    manorId: number
    cropAmount: number
    cropPrice: number
    cropRewardType: number // TODO : add enum for type value
}

export interface ShowSellCropListEvent extends PacketEvent {
    manorId: number
    crops: Array<ShowSellCropData>
}

export function ShowSellCropList( packet: ReadableClientPacket ) : ShowSellCropListEvent {
    let manorId = packet.readD(),
            size = packet.readD()

    return {
        manorId,
        crops: _.times( size, () : ShowSellCropData => {
            let itemObjectId = packet.readD(),
                    itemId = packet.readD(),
                    level = packet.readD()

            packet.skipB( 1 )

            let rewardOneId = packet.readD()

            packet.skipB( 1 )

            let rewardTwoId = packet.readD(),
                    manorId = packet.readD(),
                    cropAmount = packet.readQ(),
                    cropPrice = packet.readQ(),
                    cropRewardType = packet.readC()

            return {
                itemObjectId,
                itemId,
                level,
                rewardOneId,
                rewardTwoId,
                manorId,
                cropAmount,
                cropPrice,
                cropRewardType
            }
        } )
    }
}