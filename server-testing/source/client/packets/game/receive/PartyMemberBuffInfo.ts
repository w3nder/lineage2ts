import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export const enum PartyMemberType {
    Player,
    Pet,
    Servitor
}

export interface PartyMemberBuffData {
    id: number
    level: number
    time: number
}

export interface PartyMemberBuffInfoEvent extends PacketEvent {
    memberObjectId: number
    memberType: PartyMemberType
    skillBuffs: Array<PartyMemberBuffData>
}
export function PartyMemberBuffInfo( packet: ReadableClientPacket ) : PartyMemberBuffInfoEvent {
    let memberType = packet.readD(),
            memberObjectId = packet.readD(),
            size = packet.readD()

    return {
        memberObjectId,
        memberType,
        skillBuffs: _.times( size, () : PartyMemberBuffData => {
            let id = packet.readD(),
                    level = packet.readH(),
                    time = packet.readD()

            return {
                id,
                level,
                time
            }
        } )
    }
}