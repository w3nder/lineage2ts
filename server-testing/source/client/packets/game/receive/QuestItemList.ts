import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import _ from 'lodash'

export interface QuestItemListEvent extends PacketEvent {
    itemObjectIds: Array<number>
}
export function QuestItemList( packet : ReadableClientPacket ) : QuestItemListEvent {
    let size = packet.readH()

    return {
        itemObjectIds: _.times( size, () : number => {
            let item = readNormalItem( packet )
            item.isQuestItem = true

            return item.objectId
        } )
    }
}