import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PledgeStatusEvent extends PacketEvent {
    leaderObjectId: number
    clanId: number
    crestId: number
    allyId: number
    allyCrestId: number
}

export function PledgeStatus( packet: ReadableClientPacket ) : PledgeStatusEvent {
    let leaderObjectId = packet.readD(),
            clanId = packet.readD(),
            crestId = packet.readD(),
            allyId = packet.readD(),
            allyCrestId = packet.readD()

    return {
        leaderObjectId,
        clanId,
        crestId,
        allyId,
        allyCrestId
    }
}