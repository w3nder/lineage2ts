import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PledgeSkillAddEvent extends PacketEvent {
    id: number
    level: number
}

export function PledgeSkillAdd( packet: ReadableClientPacket ) : PledgeSkillAddEvent {
    let id = packet.readD(),
            level = packet.readD()

    return {
        id,
        level
    }
}