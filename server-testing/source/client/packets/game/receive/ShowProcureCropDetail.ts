import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowProcureCropDetailItem {
    castleId: number
    amount: number
    price: number
    rewardType: number // TODO : looks like data is coming from client, research and create enum
}

export interface ShowProcureCropDetailEvent extends PacketEvent {
    cropId: number
    cropData: Array<ShowProcureCropDetailItem>
}

export function ShowProcureCropDetail( packet: ReadableClientPacket ) : ShowProcureCropDetailEvent {
    let cropId = packet.readD(),
            size = packet.readD()

    return {
        cropId,
        cropData: _.times( size, () : ShowProcureCropDetailItem => {
            let castleId = packet.readD(),
                    amount = packet.readQ(),
                    price = packet.readQ(),
                    rewardType = packet.readC()

            return {
                castleId,
                amount,
                price,
                rewardType
            }
        } )
    }
}