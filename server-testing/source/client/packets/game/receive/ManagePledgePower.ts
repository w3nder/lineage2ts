import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { ClanPrivilege } from '../../../enums/ClanPrivileges'

export interface ManagePledgePowerEvent extends PacketEvent {
    privilegeBitmask: ClanPrivilege
}

export function ManagePledgePower( packet: ReadableClientPacket ) : ManagePledgePowerEvent {
    packet.skipD( 2 )

    return {
        privilegeBitmask: packet.readD()
    }
}