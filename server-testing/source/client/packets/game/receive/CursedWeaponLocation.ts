import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'
import _ from 'lodash'

export interface CursedWeaponLocationData extends LocationProperties {
    itemId: number
    isActivated: boolean
}
export interface CursedWeaponLocationEvent extends PacketEvent {
    items: Array<CursedWeaponLocationData>
}
export function CursedWeaponLocation( packet: ReadableClientPacket ) : CursedWeaponLocationEvent {
    let size = packet.readH()

    return {
        items: _.times( size, () : CursedWeaponLocationData => {
            let itemId = packet.readD(),
                    isActivated = packet.readD() === 1,
                    x = packet.readD(),
                    y = packet.readD(),
                    z = packet.readD()

            return {
                itemId,
                isActivated,
                x,
                y,
                z
            }
        } )
    }
}