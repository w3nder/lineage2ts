import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface TutorialShowHtmlEvent extends PacketEvent {
    html: string
}
export function TutorialShowHtml( packet : ReadableClientPacket ) : TutorialShowHtmlEvent {
    return {
        html: packet.readS()
    }
}