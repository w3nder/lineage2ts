import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PutEnchantSupportItemResultEvent extends PacketEvent {
    itemObjectId: number
}
export function PutEnchantSupportItemResult( packet: ReadableClientPacket ) : PutEnchantSupportItemResultEvent {
    return {
        itemObjectId: packet.readD()
    }
}