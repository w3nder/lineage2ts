import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface SkillCoolTimeData extends PacketEvent {
    id: number
    level: number
    reuse: number
    remaining: number
}

export interface SkillCoolTimeEvent extends PacketEvent {
    skills: Array<SkillCoolTimeData>
}
export function SkillCoolTime( packet : ReadableClientPacket ) : SkillCoolTimeEvent {
    let size = packet.readD()

    return {
        skills: _.times( size, () : SkillCoolTimeData => {
            let id = packet.readD(),
                    level = packet.readD(),
                    reuse = packet.readD(),
                    remaining = packet.readD()

            return {
                id,
                level,
                reuse,
                remaining
            }
        } )
    }
}