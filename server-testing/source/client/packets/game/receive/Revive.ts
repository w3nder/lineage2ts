import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface GameReviveEvent extends PacketEvent {
    objectId: number
}
export function Revive( packet: ReadableClientPacket ) : GameReviveEvent {
    return {
        objectId: packet.readD()
    }
}