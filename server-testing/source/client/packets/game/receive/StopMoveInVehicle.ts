import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export interface StopMoveInVehicleEvent extends PacketEvent {
    playerId: number
    vehicleId: number
}
export function StopMoveInVehicle( packet : ReadableClientPacket ) : StopMoveInVehicleEvent {
    let playerId = packet.readD(),
            vehicleId = packet.readD()

    let player = getPlayerForUpdate( playerId )

    player.x = packet.readD()
    player.y = packet.readD()
    player.z = packet.readD()
    player.heading = packet.readD()

    return {
        playerId,
        vehicleId
    }
}