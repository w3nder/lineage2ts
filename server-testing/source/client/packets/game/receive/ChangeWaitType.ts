import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2World } from '../../../L2World'
import { L2Character } from '../../../models/L2Character'
import { WaitType } from '../../../enums/WaitType'

export interface ChangeWaitTypeEvent extends PacketEvent {
    characterObjectId: number
    type: WaitType
}
export function ChangeWaitType( packet : ReadableClientPacket ) : ChangeWaitTypeEvent {
    let characterObjectId = packet.readD(),
            type = packet.readD()

    let existingCharacter = L2World.getObject( characterObjectId ) as L2Character
    if ( existingCharacter ) {
        existingCharacter.waitType = type
        existingCharacter.x = packet.readD()
        existingCharacter.y = packet.readD()
        existingCharacter.z = packet.readD()
    }

    return {
        characterObjectId,
        type
    }
}