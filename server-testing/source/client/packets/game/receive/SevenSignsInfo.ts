import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum SevenSignsWinner {
    None,
    Dusk,
    Dawn
}

export interface SevenSignsInfoEvent extends PacketEvent {
    winner: SevenSignsWinner
}
export function SevenSignsInfo( packet: ReadableClientPacket ) : SevenSignsInfoEvent {
    return {
        winner: packet.readH() - 256
    }
}