import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface RestartResponseEvent extends PacketEvent {
    isSuccess: boolean
}
export function RestartResponse( packet : ReadableClientPacket ) : RestartResponseEvent {
    return {
        isSuccess: packet.readD() === 1
    }
}