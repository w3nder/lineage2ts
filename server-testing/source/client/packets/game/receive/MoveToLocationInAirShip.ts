import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export interface MoveToLocationInAirShipEvent extends PacketEvent, LocationProperties {
    playerObjectId: number
    airshipObjectId: number
}
export function MoveToLocationInAirShip( packet: ReadableClientPacket ) : MoveToLocationInAirShipEvent {
    let playerObjectId = packet.readD(),
            airshipObjectId = packet.readD(),
            x = packet.readD(),
            y = packet.readD(),
            z = packet.readD()

    let player = getPlayerForUpdate( playerObjectId )
    player.heading = packet.readD()
    player.destinationX = x
    player.destinationY = y
    player.destinationZ = z

    return {
        playerObjectId,
        airshipObjectId,
        x,
        y,
        z
    }
}