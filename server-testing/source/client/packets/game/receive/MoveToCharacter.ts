import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getCharacterForUpdate, getObjectForUpdate } from '../helpers/CharacterUpdateHelper'

export interface MoveToCharacterEvent extends PacketEvent {
    originatorId: number
    targetId: number
    distance: number
}
export function MoveToCharacter( packet: ReadableClientPacket ) : MoveToCharacterEvent {
    let originatorId = packet.readD(),
            targetId = packet.readD(),
            distance = packet.readD()

    let originator = getCharacterForUpdate( originatorId )
    originator.x = packet.readD()
    originator.y = packet.readD()
    originator.z = packet.readD()

    let target = getObjectForUpdate( targetId )
    target.x = packet.readD()
    target.y = packet.readD()
    target.z = packet.readD()

    originator.destinationX = target.x
    originator.destinationY = target.y
    originator.destinationZ = target.z

    return {
        originatorId,
        targetId,
        distance
    }
}