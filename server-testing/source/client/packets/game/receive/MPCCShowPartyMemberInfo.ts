import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'
import _ from 'lodash'

export interface MPCCShowPartyMemberInfoEvent extends PacketEvent {
    memberIds: Array<number>
}
export function MPCCShowPartyMemberInfo( packet: ReadableClientPacket ) : MPCCShowPartyMemberInfoEvent {
    let size = packet.readD()

    return {
        memberIds: _.times( size, () : number => {
            let name = packet.readS(),
                    objectId = packet.readD(),
                    classId = packet.readD()

            let player = getPlayerForUpdate( objectId )
            player.name = name
            player.classId = classId

            return objectId
        } )
    }
}