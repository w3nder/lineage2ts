import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { CharacterStatOrder, CharacterStats } from '../../../models/CharacterStats'
import _ from 'lodash'

export interface HennaInfoEvent extends PacketEvent {
    stats: CharacterStats
    dyeIds: Array<number>
}
export function HennaInfo( packet : ReadableClientPacket ) : HennaInfoEvent {
    let stats = CharacterStatOrder.reduce( ( stats: CharacterStats, property: string ) : CharacterStats => {
        stats[ property ] = packet.readC()
        return stats
    }, {} as CharacterStats )

    packet.skipD( 1 ) // available henna slots are always 3

    let size = packet.readD()

    return {
        stats,
        dyeIds: _.times( size, () : number => {
            let dyeId = packet.readD()
            packet.skipD( 1 )

            return dyeId
        } )
    }
}