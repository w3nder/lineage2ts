import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface FriendSayEvent extends PacketEvent {
    senderName: string
    receiverName: string
    message: string
}

export function FriendSay( packet: ReadableClientPacket ) : FriendSayEvent {
    packet.skipD( 1 )

    let receiverName = packet.readS(),
            senderName = packet.readS(),
            message = packet.readS()

    return {
        receiverName,
        senderName,
        message
    }
}