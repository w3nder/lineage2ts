import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum NpcStateEffect {
    Event = 1,
    Hungry = 0x64,
    EnjoyingFood = 0x65,
}
export interface ChangeNpcStateEvent extends PacketEvent {
    objectId: number
    effect: NpcStateEffect
}
export function ChangeNpcState( packet : ReadableClientPacket ) : ChangeNpcStateEvent {
    let objectId = packet.readD(),
            effect = packet.readD()

    return {
        objectId,
        effect
    }
}