import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum ChooseInventoryAttributeType {
    NONE = 0,
    FIRE = 1,
    WATER = 1 << 1,
    WIND = 1 << 2,
    EARTH = 1 << 3,
    HOLY = 1 << 5,
    DARK = 1 << 6,
}

export interface ChooseInventoryAttributeItemEvent extends PacketEvent {
    itemId: number
    maxLevel: number
    attributes: Set<ChooseInventoryAttributeType>
}

const maskOrder : Array<ChooseInventoryAttributeType> = [
    ChooseInventoryAttributeType.FIRE,
    ChooseInventoryAttributeType.WATER,
    ChooseInventoryAttributeType.WIND,
    ChooseInventoryAttributeType.EARTH,
    ChooseInventoryAttributeType.HOLY,
    ChooseInventoryAttributeType.DARK
]

export function ChooseInventoryAttributeItem( packet: ReadableClientPacket ) : ChooseInventoryAttributeItemEvent {
    let itemId = packet.readD()
    let attributes = maskOrder.reduce( ( values: Set<number>, value: ChooseInventoryAttributeType ) : Set<number> => {
        if ( packet.readD() === 1 ) {
            values.add( value )
        }

        return values
    }, new Set<number>() )

    let maxLevel = packet.readD()

    return {
        itemId,
        maxLevel,
        attributes
    }
}