import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface DuelEndEvent extends PacketEvent {
    isPartyDuel: boolean
}
export function DuelEnd( packet: ReadableClientPacket ) : DuelEndEvent {
    return {
        isPartyDuel: packet.readD() === 1
    }
}