import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface OlympiadPlayerBuff {
    skillId: number
    skillLevel: number
    time: number
}

export interface OlympiadPlayerBuffsEvent extends PacketEvent {
    playerId: number
    buffs: Array<OlympiadPlayerBuff>
}

export function OlympiadPlayerBuffs( packet: ReadableClientPacket ) : OlympiadPlayerBuffsEvent {
    let playerId = packet.readD(),
            size = packet.readH()

    return {
        playerId,
        buffs: _.times( size, () : OlympiadPlayerBuff => {
            let skillId = packet.readD(),
                    skillLevel = packet.readD(),
                    time = packet.readD()

            return {
                skillId,
                skillLevel,
                time
            }
        } )
    }
}