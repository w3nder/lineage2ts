import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getCharacterForUpdate } from '../helpers/CharacterUpdateHelper'

export interface NpcInfoEvent extends PacketEvent {
    objectId: number
}

export function NpcInfo( packet: ReadableClientPacket ): NpcInfoEvent {
    let objectId = packet.readD()
    let character = getCharacterForUpdate( objectId )

    character.templateId = packet.readD() - 1000000
    character.isAttackable = packet.readD() === 0x01
    character.x = packet.readD()
    character.y = packet.readD()
    character.z = packet.readD()
    character.heading = packet.readD()

    packet.skipB( 89 )

    character.isRunning = packet.readC() === 0x01
    character.isInCombat = packet.readC() === 0x01
    character.isDead = packet.readC() === 0x01

    packet.skipB( 5 )

    character.name = packet.readS()

    packet.skipD( 1 )

    character.title = packet.readS()

    packet.skipD( 3 )

    character.abnormalEffects = packet.readD()
    character.clanId = packet.readD()

    packet.skipD( 1 )

    character.allyId = packet.readD()

    packet.skipD( 1 )

    character.movementType = packet.readC()

    packet.skipB( 17 )

    character.enchantEffects = packet.readD()
    character.isFlying = packet.readD() === 0x01

    packet.skipD( 2 )

    character.isTargetable = packet.readC() === 0x01

    packet.skipB( 1 )

    character.specialEffects = packet.readD()
    character.displayEffects = packet.readD()

    return {
        objectId,
    }
}