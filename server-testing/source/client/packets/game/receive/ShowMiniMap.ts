import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { SevenSignsPeriod } from '../../../enums/SevenSignsPeriod'

export interface ShowMiniMapEvent extends PacketEvent {
    id: number
    period: SevenSignsPeriod
}

export function ShowMiniMap( packet: ReadableClientPacket ) : ShowMiniMapEvent {
    let id = packet.readD(),
            period = packet.readC()

    return {
        id,
        period
    }
}