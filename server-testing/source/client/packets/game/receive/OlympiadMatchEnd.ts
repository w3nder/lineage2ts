import { PacketEvent } from '../../PacketMethodTypes'

export interface OlympiadMatchEndEvent extends PacketEvent {}
export function OlympiadMatchEnd() : OlympiadMatchEndEvent {
    return {}
}