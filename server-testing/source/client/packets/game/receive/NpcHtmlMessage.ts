import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface NpcHtmlMessageEvent extends PacketEvent {
    npcObjectId: number
    html: string
    itemId: number
}
export function NpcHtmlMessage( packet : ReadableClientPacket ) : NpcHtmlMessageEvent {
    let npcObjectId = packet.readD(),
            html = packet.readS(),
            itemId = packet.readD()

    return {
        npcObjectId,
        html,
        itemId
    }
}