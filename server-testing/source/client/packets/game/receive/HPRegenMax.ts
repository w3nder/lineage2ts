import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface HPRegenMaxEvent extends PacketEvent {
    time: number
    interval: number
    amountPerInterval: number
}
export function HPRegenMax( packet: ReadableClientPacket ) : HPRegenMaxEvent {
    packet.skipD( 1 )

    let time = packet.readD(),
            interval = packet.readD(),
            amountPerInterval = packet.readD()

    return {
        time,
        interval,
        amountPerInterval
    }
}