import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface LoadEventTopRankersEvent extends PacketEvent {
    eventId: number
    day: number
    amount: number
    bestScore: number
    ownScore: number
}
export function LoadEventTopRankers( packet: ReadableClientPacket ) : LoadEventTopRankersEvent {
    let eventId = packet.readD(),
            day = packet.readD(),
            amount = packet.readD(),
            bestScore = packet.readD(),
            ownScore = packet.readD()

    return {
        eventId,
        day,
        amount,
        bestScore,
        ownScore
    }
}