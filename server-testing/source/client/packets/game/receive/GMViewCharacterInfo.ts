import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2Player, PaperdollToItemId, PaperdollToObjectId } from '../../../models/L2Player'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'
import { InventorySlot, PaperdollOrder } from '../../../enums/Paperdoll'

export interface GMViewCharacterInfoEvent extends PacketEvent {
    playerObjectId: number
}

/*
    Slight differences in format from UserInfo packet.
 */
export function GMViewCharacterInfo( packet: ReadableClientPacket ) : GMViewCharacterInfoEvent {
    let x = packet.readD(),
            y = packet.readD(),
            z = packet.readD()

    packet.skipD( 1 )

    let playerObjectId = packet.readD()
    let player : L2Player = getPlayerForUpdate( playerObjectId )

    player.x = x
    player.y = y
    player.z = z
    player.name = packet.readS()
    player.race = packet.readD()
    player.sex = packet.readD()
    player.baseClassId = packet.readD()
    player.level = packet.readD()
    player.exp = packet.readQ()

    packet.skipF( 1 )

    player.STR = packet.readD()
    player.DEX = packet.readD()
    player.CON = packet.readD()
    player.INT = packet.readD()
    player.WIT = packet.readD()
    player.MEN = packet.readD()

    player.maxHp = packet.readD()
    player.hp = packet.readD()
    player.maxMp = packet.readD()
    player.mp = packet.readD()
    player.sp = packet.readD()

    player.weightInventory = packet.readD()
    player.weightLimit = packet.readD()

    packet.skipD( 1 )

    player.paperdollObjectId = PaperdollOrder.reduce( ( paperdoll: PaperdollToObjectId, slot: InventorySlot ) : PaperdollToObjectId => {
        paperdoll[ slot ] = packet.readD()
        return paperdoll
    }, player.paperdollObjectId )

    player.paperdollItemId = PaperdollOrder.reduce( ( paperdoll: PaperdollToItemId, slot: InventorySlot ) : PaperdollToItemId => {
        paperdoll[ slot ] = packet.readD()
        return paperdoll
    }, player.paperdollItemId )

    packet.skipD( PaperdollOrder.length + 2 )

    player.powerAttack = packet.readD()
    player.powerAttackSpeed = packet.readD()
    player.powerDefence = packet.readD()
    player.evasionRate = packet.readD()
    player.accuracy = packet.readD()
    player.crit = packet.readD()
    player.magicAttack = packet.readD()
    player.magicAttackSpeed = packet.readD()

    packet.skipD( 1 )

    player.magicDefence = packet.readD()
    player.pvpFlag = packet.readD()
    player.karma = packet.readD()
    player.runSpeed = packet.readD()
    player.walkSpeed = packet.readD()

    packet.skipD( 6 )
    packet.skipF( 4 )
    packet.skipD( 3 )

    player.isGM = packet.readD() === 1
    player.title = packet.readS()
    player.clanId = packet.readD()

    packet.skipD( 4 )

    player.mountType = packet.readC()
    player.storeType = packet.readC()
    player.canCrystallizeItems = packet.readC() === 1
    player.pkKills = packet.readD()
    player.pvpKills = packet.readD()

    packet.skipH( 2 )

    player.classId = packet.readD()

    packet.skipD( 1 )

    player.maxCp = packet.readD()
    player.cp = packet.readD()
    player.movementType = packet.readC()
    player.clanPrivileges = packet.readD()

    return {
        playerObjectId
    }
}