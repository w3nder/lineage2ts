import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface PledgePowerGradePrivilege {
    rank: number
    partyId: number
}

export interface PledgePowerGradeListEvent extends PacketEvent {
    privileges: Array<PledgePowerGradePrivilege>
}

export function PledgePowerGradeList( packet: ReadableClientPacket ) : PledgePowerGradeListEvent {
    let size = packet.readD()

    return {
        privileges: _.times( size, () : PledgePowerGradePrivilege => {
            let rank = packet.readD(),
                    partyId = packet.readD()

            return {
                rank,
                partyId
            }
        } )
    }
}