import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export const enum PledgeWarStatus {
    Declared,
    UnderAttack
}

export interface PledgeWarClan {
    name: string
    status: PledgeWarStatus
}

export interface PledgeWarListEvent extends PacketEvent {
    clans: Array<PledgeWarClan>
}

export function PledgeWarList( packet: ReadableClientPacket ) : PledgeWarListEvent {
    packet.skipD( 2 )

    let size = packet.readD()

    return {
        clans: _.times( size, () : PledgeWarClan => {
            let name = packet.readS(),
                    status = packet.readD()

            packet.skipD( 1 )

            return {
                name,
                status
            }
        } )
    }
}