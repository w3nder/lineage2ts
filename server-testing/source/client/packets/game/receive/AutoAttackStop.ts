import { PacketEvent } from '../../PacketMethodTypes'
import { ReadableClientPacket } from '../../ReadableClientPacket'

export interface AutoAttackStopEvent extends PacketEvent {
    targetId: number
}
export function AutoAttackStop( packet : ReadableClientPacket ) : AutoAttackStopEvent {
    return {
        targetId: packet.readD()
    }
}