import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2World } from '../../../L2World'
import { L2Character } from '../../../models/L2Character'

export interface ChangeMoveTypeEvent extends PacketEvent {
    characterObjectId: number
    isRunning: boolean
}
export function ChangeMoveType( packet: ReadableClientPacket ) : ChangeMoveTypeEvent {
    let characterObjectId = packet.readD(),
            isRunning = packet.readD() === 0x01

    let existingCharacter = L2World.getObject( characterObjectId ) as L2Character
    if ( existingCharacter ) {
        existingCharacter.isRunning = isRunning
    }

    return {
        characterObjectId,
        isRunning
    }
}