import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface BaseAttributeCancelResultEvent extends PacketEvent {
    itemObjectId: number
    elementId: number
}

export function BaseAttributeCancelResult( packet : ReadableClientPacket ) : BaseAttributeCancelResultEvent {
    packet.skipD( 1 )

    let itemObjectId = packet.readD(),
            elementId = packet.readD()

    return {
        itemObjectId,
        elementId
    }
}