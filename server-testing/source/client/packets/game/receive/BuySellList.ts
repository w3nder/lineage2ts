import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import _ from 'lodash'
import { ItemState } from '../../../enums/ItemState'

export interface BuySellListEvent extends PacketEvent {
    sellableObjectIds: Array<number>
    refundObjectIds: Array<number>
    isInventoryVisible: boolean
}
export function BuySellList( packet : ReadableClientPacket ) : BuySellListEvent {
    packet.skipD( 1 )

    let sellableSize = packet.readH()
    let sellableObjectIds = _.times( sellableSize, () : number => {
        let item = readNormalItem( packet )
        item.price = packet.readQ()

        return item.objectId
    } )

    let refundSize = packet.readH()
    let refundObjectIds = _.times( refundSize, () : number => {
        let item = readNormalItem( packet )

        packet.skipD( 1 )
        item.price = packet.readQ()

        return item.objectId
    } )

    let isInventoryVisible = packet.readC() === 1

    return {
        sellableObjectIds,
        refundObjectIds,
        isInventoryVisible
    }
}