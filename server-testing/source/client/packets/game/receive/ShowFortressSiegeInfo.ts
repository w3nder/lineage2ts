import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowFortressSiegeInfoEvent extends PacketEvent {
    residenceId: number
    barrackCommanderAmounts: Array<number>
}

export function ShowFortressSiegeInfo( packet: ReadableClientPacket ) : ShowFortressSiegeInfoEvent {
    let residenceId = packet.readD(),
            size = packet.readD()

    return {
        residenceId,
        barrackCommanderAmounts: _.times( size, () : number => packet.readD() )
    }
}