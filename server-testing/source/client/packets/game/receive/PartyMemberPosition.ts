import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'
import _ from 'lodash'
export interface PartyMemberPositionEvent extends PacketEvent {
    objectIds: Array<number>
}
export function PartyMemberPosition( packet : ReadableClientPacket ) : PartyMemberPositionEvent {
    let size = packet.readD()

    return {
        objectIds: _.times( size, () : number => {
            let objectId = packet.readD()

            let player = getPlayerForUpdate( objectId )
            player.x = packet.readD()
            player.y = packet.readD()
            player.z = packet.readD()

            return objectId
        } )
    }
}