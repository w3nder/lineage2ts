import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { PartyDistributionType } from '../../../enums/PartyDistributionType'

export interface AskModifyPartyLootingEvent extends PacketEvent {
    leaderName: string
    type: PartyDistributionType
}

export function AskModifyPartyLooting( packet: ReadableClientPacket ) : AskModifyPartyLootingEvent {
    let leaderName = packet.readS(),
            type = packet.readD()

    return {
        leaderName,
        type
    }
}