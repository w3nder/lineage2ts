import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum SetupGaugeColors {
    Blue = 0,
    Red = 1,
    Cyan = 2,
    Other = 3
}

export interface SetupGaugeEvent extends PacketEvent {
    characterObjectId: number
    color: SetupGaugeColors
    currentTime: number
    maximumTime: number
}
export function SetupGauge( packet : ReadableClientPacket ) : SetupGaugeEvent {
    let characterObjectId = packet.readD(),
            color = packet.readD(),
            currentTime = packet.readD(),
            maximumTime = packet.readD()

    return {
        characterObjectId,
        currentTime,
        color,
        maximumTime
    }
}