import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface HeroListData {
    name: string
    classId: number
    clanName: string
    clanCrestId: number
    allyName: string
    allyCrestId: number
    amount: number
}
export interface HeroListEvent extends PacketEvent {
    heroes: Array<HeroListData>
}
export function HeroList( packet : ReadableClientPacket ) : HeroListEvent {
    let size = packet.readD()

    return {
        heroes: _.times( size, () : HeroListData => {
            let name = packet.readS(),
                    classId = packet.readD(),
                    clanName = packet.readS(),
                    clanCrestId = packet.readD(),
                    allyName = packet.readS(),
                    allyCrestId = packet.readD(),
                    amount = packet.readD()

            return {
                name,
                classId,
                clanName,
                clanCrestId,
                allyName,
                allyCrestId,
                amount
            }
        } )
    }
}