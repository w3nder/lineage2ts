import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { CharacterStatOrder, CharacterStats } from '../../../models/CharacterStats'
import _ from 'lodash'

export interface GMHennaInfoEvent extends PacketEvent {
    totalSlots: number
    stats: CharacterStats
    dyeIds: Array<number>
}

export function GMHennaInfo( packet: ReadableClientPacket ) : GMHennaInfoEvent {
    let stats = CharacterStatOrder.reduce( ( stats: CharacterStats, property: string ): CharacterStats => {
        stats[ property ] = packet.readC()
        return stats
    }, {} as CharacterStats )

    let totalSlots = packet.readD()

    let size = packet.readD()

    return {
        stats,
        totalSlots,
        dyeIds: _.times( size, (): number => {
            let dyeId = packet.readD()
            packet.skipD( 1 )

            return dyeId
        } )
    }
}