import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface SpawnEmitterEvent extends PacketEvent {
    npcObjectId: number
    playerObjectId: number
}

export function SpawnEmitter( packet: ReadableClientPacket ) : SpawnEmitterEvent {
    let npcObjectId = packet.readD(),
            playerObjectId = packet.readD()

    return {
        npcObjectId,
        playerObjectId
    }
}