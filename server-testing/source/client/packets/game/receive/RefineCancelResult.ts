import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { RefineResultType } from '../../../enums/RefineResultType'

export interface RefineResultEvent extends PacketEvent {
    result: RefineResultType
}

export function RefineCancelResult( packet: ReadableClientPacket ) : RefineResultEvent {
    return {
        result: packet.readD()
    }
}