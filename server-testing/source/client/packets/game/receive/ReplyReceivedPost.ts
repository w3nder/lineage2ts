import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import _ from 'lodash'
import { PostMessageType } from '../../../enums/PostMessageType'

export interface ReplyReceivedPostEvent extends PacketEvent {
    messageId: number
    isLocked: boolean
    senderName: string
    messageTitle: string
    messageContent: string
    itemObjectIds: Array<number>
    requestedAdena: number
    messageType: PostMessageType
}
export function ReplyReceivedPost( packet: ReadableClientPacket ) : ReplyReceivedPostEvent {
    let messageId = packet.readD(),
            isLocked = packet.readD() === 1

    packet.skipD( 1 )

    let senderName = packet.readS(),
            messageTitle = packet.readS(),
            messageContent = packet.readS(),
            size = packet.readD()

    let itemObjectIds = _.times( size, () : number => {
        let item = readNormalItem( packet )
        packet.skipD( 1 )

        return item.objectId
    } )

    let requestedAdena = packet.readQ()

    packet.skipD( 1 )

    let messageType = packet.readD()
    return {
        messageId,
        isLocked,
        senderName,
        messageTitle,
        messageContent,
        requestedAdena,
        messageType,
        itemObjectIds
    }
}