import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'

export interface GetOnAirShipEvent extends PacketEvent, LocationProperties {
    playerObjectId: number
    shipObjectId: number
}
export function GetOnAirShip( packet : ReadableClientPacket ) : GetOnAirShipEvent {
    let playerObjectId = packet.readD(),
            shipObjectId = packet.readD(),
            x = packet.readD(),
            y = packet.readD(),
            z = packet.readD()

    return {
        playerObjectId,
        shipObjectId,
        x,
        y,
        z
    }
}