import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum SetCompassZoneType {
    Altered = 0x08,
    WarZoneStart = 0x0A,
    WarZoneEnd = 0x0B,
    Peace = 0x0C,
    SevenSigns = 0x0D,
    Pvp = 0x0E,
    General = 0x0F,
}

export interface SetCompassZoneCodeEvent extends PacketEvent {
    type: SetCompassZoneType
}
export function SetCompassZoneCode( packet : ReadableClientPacket ) : SetCompassZoneCodeEvent {
    return {
        type: packet.readD()
    }
}