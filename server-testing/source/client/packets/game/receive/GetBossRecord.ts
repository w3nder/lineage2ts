import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface GetBossRecordEvent extends PacketEvent {
    rank: number
    total: number
    points: Map<number, number> // raidId to score
}
export function GetBossRecord( packet: ReadableClientPacket ) : GetBossRecordEvent {
    let rank = packet.readD(),
            total = packet.readD(),
            size = packet.readD(),
            points = new Map<number, number>()

    _.times( size, () => {
        points.set( packet.readD(), packet.readD() )
        packet.skipD( 1 )
    } )

    return {
        rank,
        total,
        points
    }
}