import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'
import { readLocation } from '../helpers/PropertyHelper'
import _ from 'lodash'

export const enum ExServerPrimitiveType {
    Point = 1,
    Line = 2
}

interface BasePrimitive {
    type: ExServerPrimitiveType
    name: string
    color: number
    isNameColored: boolean
}

interface ExServerPrimitivePoint extends BasePrimitive {
    point: LocationProperties
}

interface ExServerPrimitiveLine extends BasePrimitive {
    start: LocationProperties
    end: LocationProperties
}

export type ExServerPrimitiveData = ExServerPrimitivePoint | ExServerPrimitiveLine

export interface ServerPrimitiveEvent extends PacketEvent {
    name: string
    origin: LocationProperties
    items: Array<ExServerPrimitiveData>
}

function readPrimitiveData( packet: ReadableClientPacket ) : ExServerPrimitiveData {
    let type = packet.readC(),
            name = packet.readS(),
            rColor = packet.readD() << 16,
            gColor = packet.readD() << 8,
            bColor = packet.readD(),
            isNameColored = packet.readD() === 1

    if ( type === ExServerPrimitiveType.Point ) {
        return {
            type,
            name,
            color: rColor & gColor & bColor,
            isNameColored,
            point: readLocation( packet )
        }
    }

    let start = readLocation( packet ),
            end = readLocation( packet )

    return {
        type,
        name,
        color: rColor & gColor & bColor,
        isNameColored,
        start,
        end
    }
}

export function ServerPrimitive( packet: ReadableClientPacket ) : ServerPrimitiveEvent {
    let name = packet.readS(),
            origin = readLocation( packet )

    packet.skipD( 2 )
    let size = packet.readD()

    return {
        name,
        origin,
        items: _.times( size, () : ExServerPrimitiveData => {
            return readPrimitiveData( packet )
        } )
    }
}