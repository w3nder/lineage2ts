import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import _ from 'lodash'
import { PrivateStoreManageAvailable, PrivateStoreManageItem } from '../../../models/PrivateStore'

export interface PrivateStoreManageBuyable extends PrivateStoreManageItem {
    availableAmount: number
}

export interface PrivateStoreManageListBuyEvent extends PacketEvent {
    playerObjectId: number
    playerAdena: number
    availableItems: Array<PrivateStoreManageAvailable>
    buyItems: Array<PrivateStoreManageBuyable>
}

export function PrivateStoreManageListBuy( packet: ReadableClientPacket ) : PrivateStoreManageListBuyEvent {
    let playerObjectId = packet.readD(),
            playerAdena = packet.readQ(),
            size = packet.readD()

    let availableItems : Array<PrivateStoreManageAvailable> = _.times( size, () : PrivateStoreManageAvailable => {
        let item = readNormalItem( packet ),
                referencePrice = packet.readQ()

        return {
            itemObjectId: item.objectId,
            referencePrice
        }
    } )

    size = packet.readD()

    let buyItems : Array<PrivateStoreManageBuyable> = _.times( size, () : PrivateStoreManageBuyable => {
        let item = readNormalItem( packet ),
                price = packet.readQ(),
                referencePrice = packet.readQ(),
                availableAmount = packet.readQ()

        return {
            itemObjectId: item.objectId,
            price,
            referencePrice,
            availableAmount
        }
    } )

    return {
        playerAdena,
        playerObjectId,
        availableItems,
        buyItems
    }
}