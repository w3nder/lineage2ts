import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum FriendOperationType {
    Add = 1,
    Remove = 3
}
export interface FriendOperationEvent extends PacketEvent {
    type:FriendOperationType,
    objectId : number
    name: string
}

export function FriendOperation( packet: ReadableClientPacket ) : FriendOperationEvent {
    let type = packet.readD(),
            objectId = packet.readD(),
            name = packet.readS()

    return {
        type,
        objectId,
        name
    }
}