import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface NoticePostSentEvent extends PacketEvent {
    showAnimation: boolean
}
export function NoticePostSent( packet: ReadableClientPacket ) : NoticePostSentEvent {
    return {
        showAnimation: packet.readD() === 1
    }
}