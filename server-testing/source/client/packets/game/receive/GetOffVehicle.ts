import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getCharacterForUpdate } from '../helpers/CharacterUpdateHelper'

export interface GetOffVehicleEvent extends PacketEvent {
    characterObjectId: number
    vehicleObjectId: number
}

export function GetOffVehicle( packet: ReadableClientPacket ) : GetOffVehicleEvent {
    let characterObjectId = packet.readD(),
            vehicleObjectId = packet.readD()

    let character = getCharacterForUpdate( characterObjectId )

    character.x = packet.readD()
    character.y = packet.readD()
    character.z = packet.readD()

    return {
        characterObjectId,
        vehicleObjectId
    }
}