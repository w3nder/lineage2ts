import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getObjectForUpdate } from '../helpers/CharacterUpdateHelper'
import { L2ObjectType } from '../../../enums/L2ObjectType'
import { L2Vehicle } from '../../../models/L2Vehicle'

export interface VehicleInfoEvent extends PacketEvent {
    objectId: number
}

export function VehicleInfo( packet : ReadableClientPacket ) : VehicleInfoEvent {
    let objectId = packet.readD()
    let character = getObjectForUpdate( objectId, L2ObjectType.Vehicle ) as L2Vehicle

    character.x = packet.readD()
    character.y = packet.readD()
    character.z = packet.readD()
    character.heading = packet.readD()

    return {
        objectId
    }
}