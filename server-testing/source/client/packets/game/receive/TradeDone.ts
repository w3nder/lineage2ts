import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface TradeDoneEvent extends PacketEvent {
    isSuccess: boolean
}
export function TradeDone( packet : ReadableClientPacket ) : TradeDoneEvent {
    return {
        isSuccess: packet.readD() === 0x01
    }
}