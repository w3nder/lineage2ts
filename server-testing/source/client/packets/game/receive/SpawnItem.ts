import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getItemForUpdate } from '../helpers/ItemUpdateHelper'
import { ItemState } from '../../../enums/ItemState'

export interface SpawnItemEvent extends PacketEvent {
    objectId: number
}
export function SpawnItem( packet: ReadableClientPacket ) : SpawnItemEvent {
    let objectId = packet.readD()
    let item = getItemForUpdate( objectId, ItemState.Dropped )

    item.itemId = packet.readD()
    item.x = packet.readD()
    item.y = packet.readD()
    item.z = packet.readD()
    item.isStackable = packet.readD() === 0x01
    item.amount = packet.readQ()

    return {
        objectId,
    }
}