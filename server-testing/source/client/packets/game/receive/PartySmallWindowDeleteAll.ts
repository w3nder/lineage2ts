import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PartySmallWindowDeleteAllEvent extends PacketEvent {

}
export function PartySmallWindowDeleteAll( packet: ReadableClientPacket ) : PartySmallWindowDeleteAllEvent {
    return {}
}