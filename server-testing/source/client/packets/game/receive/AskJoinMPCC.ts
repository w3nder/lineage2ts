import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface AskJoinMPCCEvent extends PacketEvent {
    name: string
}

export function AskJoinMPCC( packet: ReadableClientPacket ) : AskJoinMPCCEvent {
    return {
        name: packet.readS()
    }
}