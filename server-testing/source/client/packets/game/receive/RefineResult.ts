import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { RefineResultType } from '../../../enums/RefineResultType'

export interface RefineResultEvent extends PacketEvent {
    stats: number
    type: RefineResultType
}

export function RefineResult( packet: ReadableClientPacket ) : RefineResultEvent {
    let stats = packet.readQ(),
            type = packet.readD()

    return {
        stats,
        type
    }
}