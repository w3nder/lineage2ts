import { PacketEvent } from '../../PacketMethodTypes'

export interface OpenMPCCEvent extends PacketEvent {}
export function OpenMPCC() : OpenMPCCEvent {
    return {}
}