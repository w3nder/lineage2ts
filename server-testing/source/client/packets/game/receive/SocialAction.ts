import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum SocialActionType {
    Congratulate = 3,
    Bow = 7,
    CursedWeaponAcquired = 17,
    HeroClaimed = 20016,
    LevelUp = 2122
}

export interface SocialActionEvent extends PacketEvent {
    characterObjectId: number
    action : SocialActionType
}
export function SocialAction( packet : ReadableClientPacket ) : SocialActionEvent {
    let characterObjectId = packet.readD(),
            action = packet.readD()

    return {
        characterObjectId,
        action
    }
}