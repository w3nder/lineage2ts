import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2World } from '../../../L2World'
import { L2Player } from '../../../models/L2Player'

export interface CurrentPlayerTargetEvent extends PacketEvent {
    objectId: number
    levelDifference: number
}
export function CurrentPlayerTarget( packet : ReadableClientPacket ) : CurrentPlayerTargetEvent {
    let objectId = packet.readD(),
            levelDifference = packet.readH()

    return {
        objectId,
        levelDifference
    }
}