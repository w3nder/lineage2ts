import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum ConfirmAddingContactStatus {
    Removed,
    Added
}

export interface ConfirmAddingContactEvent extends PacketEvent {
    name: string
    status: ConfirmAddingContactStatus
}
export function ConfirmAddingContact( packet: ReadableClientPacket ) : ConfirmAddingContactEvent {
    let name = packet.readS(),
            status = packet.readD()

    return {
        name,
        status
    }
}