import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ListPartyWaitingMember {
    name: string
    classId: number
}

export interface ListPartyWaitingRoom {
    id: number
    title: string
    bbsId: number
    minimumLevel: number
    maximumLevel: number
    maximumSize: number
    leaderName: string
    members: Array<ListPartyWaitingMember>
}

export interface ListPartyWaitingEvent extends PacketEvent {
    rooms: Array<ListPartyWaitingRoom>
}

export function ListPartyWaiting( packet: ReadableClientPacket ) : ListPartyWaitingEvent {
    packet.skipD( 1 )

    let size = packet.readD()

    return {
        rooms: _.times( size, () : ListPartyWaitingRoom => {
            let id = packet.readD(),
                    title = packet.readS(),
                    bbsId = packet.readD(),
                    minimumLevel = packet.readD(),
                    maximumLevel = packet.readD(),
                    maximumSize = packet.readD(),
                    leaderName = packet.readS(),
                    memberSize = packet.readD()

            return {
                id,
                title,
                bbsId,
                minimumLevel,
                maximumLevel,
                maximumSize,
                leaderName,
                members: _.times( memberSize, () : ListPartyWaitingMember => {
                    let classId = packet.readD(),
                            name = packet.readS()

                    return {
                        classId,
                        name
                    }
                } )
            }
        } )
    }
}