import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getCharacterForUpdate } from '../helpers/CharacterUpdateHelper'

export interface RotationEvent extends PacketEvent {
    objectId: number
}
export function Rotation( packet : ReadableClientPacket ) : RotationEvent {
    let objectId = packet.readD()
    let character = getCharacterForUpdate( objectId )
    character.heading = packet.readD()

    return {
        objectId
    }
}