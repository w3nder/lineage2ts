import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface CloseMPCCEvent extends PacketEvent {}
export function CloseMPCC( packet: ReadableClientPacket ) : CloseMPCCEvent {
    return {}
}