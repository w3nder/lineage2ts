import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface NoticePostArrivedEvent extends PacketEvent {
    showAnimation: boolean
}
export function NoticePostArrived( packet: ReadableClientPacket ) : NoticePostArrivedEvent {
    return {
        showAnimation: packet.readD() === 1
    }
}