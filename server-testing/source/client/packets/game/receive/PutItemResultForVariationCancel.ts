import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getItemForUpdate } from '../helpers/ItemUpdateHelper'

export interface PutItemResultForVariationCancelEvent extends PacketEvent {
    itemObjectId: number
    price: number
}
export function PutItemResultForVariationCancel( packet: ReadableClientPacket ) : PutItemResultForVariationCancelEvent {
    let itemObjectId = packet.readD()

    let item = getItemForUpdate( itemObjectId )

    item.itemId = packet.readD()
    item.augmentationId = packet.readD()

    packet.skipD( 1 )

    let price = packet.readQ()

    return {
        itemObjectId,
        price
    }
}