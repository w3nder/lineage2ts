import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface SetSummonRemainTimeEvent extends PacketEvent {
    maxTime: number
    remainingTime: number
}

export function SetSummonRemainTime( packet: ReadableClientPacket ) : SetSummonRemainTimeEvent {
    let maxTime = packet.readD(),
            remainingTime = packet.readD()

    return {
        maxTime,
        remainingTime
    }
}