import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface AttributeEnchantResultEvent extends PacketEvent {
    addedResult: number
}
export function AttributeEnchantResult( packet: ReadableClientPacket ) : AttributeEnchantResultEvent {
    return {
        addedResult: packet.readD()
    }
}