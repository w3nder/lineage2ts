import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface ClosePartyRoomEvent extends PacketEvent {}
export function ClosePartyRoom( packet : ReadableClientPacket ) : ClosePartyRoomEvent {
    return {}
}