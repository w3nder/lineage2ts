import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface RecipeBookItemListEvent extends PacketEvent {
    isDwarven: boolean
    recipeIds: Array<number>
    maxMp: number
}
export function RecipeBookItemList( packet : ReadableClientPacket ) : RecipeBookItemListEvent {
    let isDwarven = packet.readD() === 0,
            maxMp = packet.readD(),
            size = packet.readD()

    return {
        isDwarven,
        maxMp,
        recipeIds: _.times( size, () : number => {
            let recipeId = packet.readD()
            packet.skipD( 1 )

            return recipeId
        } )
    }
}