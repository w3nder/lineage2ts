import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface DominionWarStartEvent extends PacketEvent {
    playerObjectId: number
    territoryId: number
    isDisguised: boolean
}
export function DominionWarStart( packet: ReadableClientPacket ) : DominionWarStartEvent {
    let playerObjectId = packet.readD()

    packet.skipD( 1 )

    let territoryId = packet.readD(),
            isDisguised = packet.readD() === 1

    return {
        playerObjectId,
        territoryId,
        isDisguised
    }
}