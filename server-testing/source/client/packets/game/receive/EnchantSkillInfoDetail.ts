import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { EnchantType } from '../../../enums/EnchantType'

export interface EnchantSkillInfoDetailEvent extends PacketEvent {
    type: EnchantType
    skillId: number
    skillLevel: number
    exp: number
    sp: number
    paymentItemId: number
    paymentItemAmount: number
    enchantBookId: number
    enchantBookAmount: number
}
export function EnchantSkillInfoDetail( packet : ReadableClientPacket ) : EnchantSkillInfoDetailEvent {
    let type = packet.readD(),
            skillId = packet.readD(),
            skillLevel = packet.readD(),
            sp = packet.readD(),
            exp = packet.readD()

    packet.skipD( 1 )

    let paymentItemId = packet.readD(),
            paymentItemAmount = packet.readD(),
            enchantBookId = packet.readD(),
            enchantBookAmount = packet.readD()

    return {
        type,
        skillId,
        skillLevel,
        sp,
        exp,
        paymentItemId,
        paymentItemAmount,
        enchantBookId,
        enchantBookAmount
    }
}