import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface GMViewSkillData {
    isPassive: boolean
    id: number
    level: number
    isDisabledClanSkill: boolean
    isEnchantable: boolean
}

export interface GMViewSkillInfoEvent extends PacketEvent {
    playerName: string
    skills: Array<GMViewSkillData>
}

export function GMViewSkillInfo( packet: ReadableClientPacket ) : GMViewSkillInfoEvent {
    let playerName = packet.readS(),
            size = packet.readD()

    return {
        playerName,
        skills: _.times( size, () : GMViewSkillData => {
            let isPassive = packet.readD() === 1,
                    level = packet.readD(),
                    id = packet.readD(),
                    isDisabledClanSkill = packet.readC() === 1,
                    isEnchantable = packet.readC() === 1

            return {
                isPassive,
                id,
                level,
                isDisabledClanSkill,
                isEnchantable
            }
        } )
    }
}