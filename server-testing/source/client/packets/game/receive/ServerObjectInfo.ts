import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getCharacterForUpdate } from '../helpers/CharacterUpdateHelper'

export interface ServerObjectInfoEvent extends PacketEvent {
    objectId: number
}

export function ServerObjectInfo( packet: ReadableClientPacket ) : ServerObjectInfoEvent {
    let objectId = packet.readD()

    let character = getCharacterForUpdate( objectId )

    character.templateId = packet.readD() - 1000000
    character.name = packet.readS()
    character.isAttackable = packet.readD() === 1
    character.x = packet.readD()
    character.y = packet.readD()
    character.z = packet.readD()
    character.heading = packet.readD()

    packet.skipF( 4 )

    character.hp = packet.readD()
    character.maxHp = packet.readD()

    return {
        objectId
    }
}