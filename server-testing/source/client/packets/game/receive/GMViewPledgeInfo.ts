import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { Race } from '../../../enums/Race'
import _ from 'lodash'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export interface GMViewPledgeClan {
    id: number
    name: string
    leaderName: string
    crestId: number
    level: number
    castleId: number
    hideoutId: number
    fortId: number
    rank: number
    reputation: number
    allyId: number
    allyName: string
    allyCrestId: number
    isAtWar: boolean
}

export interface GMViewPledgeMember {
    name: string
    level: number
    classId: number
    race: Race
    isOnline: boolean
    hasSponsor: boolean
    objectId: number
}

export interface GMViewPledgeInfoEvent extends PacketEvent {
    playerName: string
    clan: GMViewPledgeClan
    members: Array<GMViewPledgeMember>
}

export function GMViewPledgeInfo( packet: ReadableClientPacket ) : GMViewPledgeInfoEvent {
    let playerName = packet.readS(),
            id = packet.readD()

    packet.skipD( 1 )

    let name = packet.readS(),
            leaderName = packet.readS(),
            crestId = packet.readD(),
            level = packet.readD(),
            castleId = packet.readD(),
            hideoutId = packet.readD(),
            fortId = packet.readD(),
            rank = packet.readD(),
            reputation = packet.readD()

    packet.skipD( 2 )

    let allyId = packet.readD(),
            allyName = packet.readS(),
            allyCrestId = packet.readD(),
            isAtWar = packet.readD() === 1

    packet.skipD( 1 )

    let size = packet.readD()

    return {
        playerName,
        clan: {
            id,
            name,
            leaderName,
            crestId,
            level,
            castleId,
            hideoutId,
            fortId,
            rank,
            reputation,
            allyId,
            allyName,
            allyCrestId,
            isAtWar
        },
        members: _.times( size, () : GMViewPledgeMember => {
            let name = packet.readS(),
                    level = packet.readD(),
                    classId = packet.readD()

            packet.skipD( 1 )

            let race = packet.readD(),
                    objectId = packet.readD(),
                    hasSponsor = packet.readD() === 1

            let isOnline = objectId !== 0
            if ( isOnline ) {
                let player = getPlayerForUpdate( objectId )

                player.name = name
                player.level = level
                player.classId = classId
                player.race = race
            }

            return {
                name,
                level,
                classId,
                objectId,
                hasSponsor,
                race,
                isOnline
            }
        } )
    }
}