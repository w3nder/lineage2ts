import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface ShowVariationMakeWindowEvent extends PacketEvent {}

export function ShowVariationMakeWindow( packet: ReadableClientPacket ) : ShowVariationMakeWindowEvent {
    return {}
}