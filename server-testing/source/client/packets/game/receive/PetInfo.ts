import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getSummonForUpdate } from '../helpers/CharacterUpdateHelper'

export interface PetInfoEvent extends PacketEvent {
    objectId: number
}

export function PetInfo( packet: ReadableClientPacket ) : PetInfoEvent {
    let type = packet.readD(),
            objectId = packet.readD()
    let summon = getSummonForUpdate( objectId )

    summon.summonType = type
    summon.templateId = packet.readD() - 1000000
    summon.isAttackable = packet.readD() === 0x01
    summon.x = packet.readD()
    summon.y = packet.readD()
    summon.z = packet.readD()
    summon.heading = packet.readD()

    packet.skipB( 89 )

    summon.isRunning = packet.readC() === 0x01
    summon.isInCombat = packet.readC() === 0x01
    summon.isDead = packet.readC() === 0x01

    packet.skipB( 5 )

    summon.name = packet.readS()

    packet.skipD( 1 )

    summon.title = packet.readS()

    packet.skipD( 1 )

    summon.pvpFlag = packet.readD()
    summon.karma = packet.readD()
    summon.feed = packet.readD()
    summon.maxFeed = packet.readD()
    summon.hp = packet.readD()
    summon.maxHp = packet.readD()
    summon.mp = packet.readD()
    summon.maxMp = packet.readD()
    summon.sp = packet.readD()
    summon.level = packet.readD()
    summon.exp = packet.readQ()

    return {
        objectId,
    }
}