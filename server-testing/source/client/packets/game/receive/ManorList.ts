import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

/*
    Packet results do not change between game sessions.
    Contain castle names associated to residence id.
 */
export interface ManorListEvent extends PacketEvent {
    [ residenceId: number ] : string
}
export function ManorList( packet: ReadableClientPacket ) : ManorListEvent {
    let event = {}
    let size = packet.readD()

    _.times( size, () => {
        let residenceId = packet.readD()
        event[ residenceId ] = packet.readS()
    } )

    return event
}