import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface PledgeSkillData {
    id: number
    level: number
}

export interface PledgeSkillSubData extends PledgeSkillData {
    type: number // TODO : add enum
}

export interface PledgeSkillListEvent extends PacketEvent {
    skills: Array<PledgeSkillData>
    subSkills: Array<PledgeSkillSubData>
}

export function PledgeSkillList( packet: ReadableClientPacket ) : PledgeSkillListEvent {
    let skillSize = packet.readD(),
            subSize = packet.readD()

    let skills : Array<PledgeSkillData> = _.times( skillSize, () : PledgeSkillData => {
        let id = packet.readD(),
                level = packet.readD()

        return {
            id,
            level
        }
    } )

    let subSkills : Array<PledgeSkillSubData> = _.times( subSize, () : PledgeSkillSubData => {
        let type = packet.readD(),
                id = packet.readD(),
                level = packet.readD()

        return {
            type,
            id,
            level
        }
    } )

    return {
        skills,
        subSkills
    }
}