import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getCharacterForUpdate } from '../helpers/CharacterUpdateHelper'

export interface ValidateLocationEvent extends PacketEvent {
    objectId: number
}

export function ValidateLocation( packet: ReadableClientPacket ): ValidateLocationEvent {
    let objectId = packet.readD()

    let object = getCharacterForUpdate( objectId )
    object.x = packet.readD()
    object.y = packet.readD()
    object.z = packet.readD()
    object.heading = packet.readD()

    return {
        objectId,
    }
}