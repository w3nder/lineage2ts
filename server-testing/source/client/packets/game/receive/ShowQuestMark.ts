import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface ShowQuestMarkEvent extends PacketEvent {
    questId: number
}

export function ShowQuestMark( packet: ReadableClientPacket ) : ShowQuestMarkEvent {
    return {
        questId: packet.readD()
    }
}