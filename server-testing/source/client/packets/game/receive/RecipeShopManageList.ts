import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'
import { RecipeShopManageItem } from '../../../models/PrivateStore'

export interface RecipeShopManageListEvent extends PacketEvent {
    playerObjectId: number
    availableAdena: number
    hasDwarfRecipes: boolean
    recipeIds: Array<number>
    manufactureItems: Array<RecipeShopManageItem>
}

export function RecipeShopManageList( packet: ReadableClientPacket ) : RecipeShopManageListEvent {
    let playerObjectId = packet.readD(),
            availableAdena = packet.readD(),
            hasDwarfRecipes = packet.readD() === 1,
            recipeSize = packet.readD()

    let recipeIds = _.times( recipeSize, () : number => {
        let id = packet.readD()

        packet.skipD( 1 )

        return id
    } )

    let manufactureSize = packet.readD()

    return {
        playerObjectId,
        availableAdena,
        hasDwarfRecipes,
        recipeIds,
        manufactureItems: _.times( manufactureSize, () : RecipeShopManageItem => {
            let recipeId = packet.readD()

            packet.skipD( 1 )

            let adenaCost = packet.readQ()

            return {
                recipeId,
                adenaCost
            }
        } )
    }
}