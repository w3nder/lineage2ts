import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ReplyDominionTerritory {
    id: number
    name: string
    clanName: string
    emblemIds: Array<number>
    warStartTime: number
}
export interface ReplyDominionInfoEvent extends PacketEvent {
    territories: Array<ReplyDominionTerritory>
}
export function ReplyDominionInfo( packet: ReadableClientPacket ) : ReplyDominionInfoEvent {
    let size = packet.readD()

    return {
        territories: _.times( size, () : ReplyDominionTerritory => {
            let id = packet.readD(),
                    name = packet.readS(),
                    clanName = packet.readS(),
                    emblemSize = packet.readD()

            let emblemIds = _.times( emblemSize, () : number => packet.readD() ),
                    warStartTime = packet.readD()

            return {
                id,
                name,
                clanName,
                emblemIds,
                warStartTime
            }
        } )
    }
}