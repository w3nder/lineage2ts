import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import { ItemState } from '../../../enums/ItemState'
import _ from 'lodash'

export interface PrivateStoreSellItem extends PacketEvent {
    itemObjectId: number
    price: number
    referencePrice: number
}

export interface PrivateStoreListSellEvent extends PacketEvent {
    sellerObjectId: number
    isPackage: boolean
    buyerAdena: number
    items: Array<PrivateStoreSellItem>
}
export function PrivateStoreListSell( packet : ReadableClientPacket ) : PrivateStoreListSellEvent {
    let sellerObjectId = packet.readD(),
            isPackage = packet.readD() === 1,
            buyerAdena = packet.readQ(),
            size = packet.readD()

    return {
        sellerObjectId,
        isPackage,
        buyerAdena,
        items: _.times( size, () : PrivateStoreSellItem => {
            let itemObjectId = packet.readD()

            readNormalItem( packet, ItemState.Selling, sellerObjectId )

            let price = packet.readQ(),
                    referencePrice = packet.readQ()

            return {
                itemObjectId,
                price,
                referencePrice
            }
        } )
    }
}