import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PledgeInfoUpdateEvent extends PacketEvent {
    clanId: number
    crestId: number
    level: number
    castleId: number
    hideoutId: number
    fortId: number
    rank: number
    reputation: number
    allyId: number
    allyName: string
    allyCrestId: number
    isAtWar: boolean
}

export function PledgeInfoUpdate( packet: ReadableClientPacket ) : PledgeInfoUpdateEvent {
    let clanId = packet.readD(),
            crestId = packet.readD(),
            level = packet.readD(),
            castleId = packet.readD(),
            hideoutId = packet.readD(),
            fortId = packet.readD(),
            rank = packet.readD(),
            reputation = packet.readD()

    packet.skipD( 2 )

    let allyId = packet.readD(),
            allyName = packet.readS(),
            allyCrestId = packet.readD(),
            isAtWar = packet.readD() === 1

    return {
        clanId,
        crestId,
        level,
        castleId,
        hideoutId,
        fortId,
        rank,
        reputation,
        allyId,
        allyName,
        allyCrestId,
        isAtWar
    }
}