import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'
import { SiegeClanType } from '../../../enums/SiegeClanType'

export interface ResidenceDefenderClanData {
    id: number
    name: string
    leaderName: string
    crestId: number
    type: SiegeClanType
    allyId: number
    allyName: string
    allyCrestId: number
}
export interface ResidenceDefenderClansEvent extends PacketEvent {
    residenceId: number
    clans: Array<ResidenceDefenderClanData>
}

export function ResidenceDefenderClans( packet: ReadableClientPacket ) : ResidenceDefenderClansEvent {
    let residenceId = packet.readD()

    packet.skipD( 4 )

    let size = packet.readD()

    return {
        residenceId,
        clans: _.times( size, () : ResidenceDefenderClanData => {
            let id = packet.readD(),
                    name = packet.readS(),
                    leaderName = packet.readS(),
                    crestId = packet.readD()

            packet.skipD( 1 )

            let type = packet.readD(),
                    allyId = packet.readD(),
                    allyName = packet.readS()

            packet.skipS()

            let allyCrestId = packet.readD()

            return {
                id,
                name,
                leaderName,
                crestId,
                type,
                allyId,
                allyName,
                allyCrestId
            }
        } )
    }
}