import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PledgeDismissalEvent extends PacketEvent {}

export function PledgeDismissal( packet: ReadableClientPacket ) : PledgeDismissalEvent {
    return {}
}