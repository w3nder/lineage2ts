import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface SubPledgeSkillAddEvent extends PacketEvent {
    type: number
    id: number
    level: number
}

export function SubPledgeSkillAdd( packet: ReadableClientPacket ) : SubPledgeSkillAddEvent {
    let type = packet.readD(),
            id = packet.readD(),
            level = packet.readD()

    return {
        type,
        id,
        level
    }
}