import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { InventorySlot } from '../../../enums/Paperdoll'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'
import { L2Player, PaperdollToItemId } from '../../../models/L2Player'

/*
    Paperdoll order is specific to current packet since it has
    different order of slots than other packets.
 */
const PaperdollOrder : Array<InventorySlot> = [
    InventorySlot.Under,
    InventorySlot.Head,
    InventorySlot.RightHand,
    InventorySlot.LeftHand,
    InventorySlot.Gloves,
    InventorySlot.Chest,
    InventorySlot.Legs,
    InventorySlot.Feet,
    InventorySlot.Cloak,
    InventorySlot.RightHand,
    InventorySlot.Hair,
    InventorySlot.HairSecondary,
    InventorySlot.RightBracelet,
    InventorySlot.LeftBracelet,
    InventorySlot.Decoration1,
    InventorySlot.Decoration2,
    InventorySlot.Decoration3,
    InventorySlot.Decoration4,
    InventorySlot.Decoration5,
    InventorySlot.Decoration6,
    InventorySlot.Belt
]

/*
    Packet data is personalized per game server connection. Meaning certain fields
    like visibility and title would be different.
 */
export interface PlayerInfoEvent extends PacketEvent {
    objectId: number
}
export function PlayerInfo( packet : ReadableClientPacket ) : PlayerInfoEvent {
    let x = packet.readD(),
            y = packet.readD(),
            z = packet.readD()

    packet.skipD( 1 )

    let objectId = packet.readD(),
            name = packet.readS(),
            race = packet.readD(),
            baseClassId = packet.readD()

    let player : L2Player = getPlayerForUpdate( objectId )

    player.x = x
    player.y = y
    player.z = z
    player.name = name

    player.race = race
    player.baseClassId = baseClassId
    player.paperdollItemId = PaperdollOrder.reduce( ( paperdoll : PaperdollToItemId, slot: InventorySlot ) : PaperdollToItemId => {
        paperdoll[ slot ] = packet.readD()
        return paperdoll
    }, player.paperdollItemId )

    /*
        Paperdoll slots with augmentationId specified.
        Skipping whole section for now.
     */
    packet.skipD( PaperdollOrder.length + 2 )

    player.pvpFlag = packet.readD()
    player.karma = packet.readD()
    player.magicAttackSpeed = packet.readD()
    player.powerAttackSpeed = packet.readD()

    packet.skipD( 1 )

    player.runSpeed = packet.readD()
    player.walkSpeed = packet.readD()

    /*
        Skipping alot of values we don't use, including title,
        since title changes depending on GM access level.
     */
    packet.skipD( 33 )
    packet.skipS()

    player.clanId = packet.readD()

    packet.skipB( 13 )

    player.isRunning = packet.readC() === 0x01
    player.isInCombat = packet.readC() === 0x01

    packet.skipB( 1 )

    player.isVisible = packet.readC() === 0
    player.mountType = packet.readC()
    player.storeType = packet.readC()

    let cubicSize = packet.readH()
    packet.skipB( cubicSize * 2 + 5 )

    player.movementType = packet.readC()

    packet.skipB( 6 )

    player.classId = packet.readD()

    packet.skipB( 29 )

    player.heading = packet.readD()

    return {
        objectId,
    }
}