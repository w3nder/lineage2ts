import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum JoinPartyReply {
    DisabledByClient = -1,
    Rejected = 0,
    Accepted = 1
}
export interface JoinPartyEvent extends PacketEvent {
    reply: JoinPartyReply
}
export function JoinParty( packet : ReadableClientPacket ) : JoinPartyEvent {
    return {
        reply: packet.readD()
    }
}