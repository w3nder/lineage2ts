import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface ShowVariationCancelWindowEvent extends PacketEvent {}

export function ShowVariationCancelWindow( packet: ReadableClientPacket ) : ShowVariationCancelWindowEvent {
    return {}
}