import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'
import { readNormalItem } from '../helpers/ItemUpdateHelper'

export interface PrivateStoreBuyData {
    price: number
    referencePrice: number
    availableAmount: number
    itemObjectId: number
}

export interface PrivateStoreListBuyEvent extends PacketEvent {
    sellerObjectId: number
    buyerAdena: number
    items: Array<PrivateStoreBuyData>
}

export function PrivateStoreListBuy( packet: ReadableClientPacket ) : PrivateStoreListBuyEvent {
    let sellerObjectId = packet.readD(),
            buyerAdena = packet.readQ(),
            size = packet.readD()

    return {
        sellerObjectId,
        buyerAdena,
        items: _.times( size, () : PrivateStoreBuyData => {
            let item = readNormalItem( packet )

            packet.skipD( 1 )

            let price = packet.readQ(),
                    referencePrice = packet.readQ(),
                    availableAmount = packet.readQ()

            return {
                itemObjectId: item.objectId,
                price,
                referencePrice,
                availableAmount
            }
        } )
    }
}