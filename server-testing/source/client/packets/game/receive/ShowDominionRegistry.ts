import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface ShowDominionRegistryTerritory {
    id: number
    ownedWardIds : Array<number>
}

export interface ShowDominionRegistryEvent extends PacketEvent {
    castleId: number
    clanName: string
    clanLeaderName: string
    clanAllyName: string
    registeredClans: number
    registeredMercenaries: number
    warTime: number
    currentTime: number
    canCancelClanRegistration: boolean
    canCancelMercenaryRegistration: boolean
    territories: Array<ShowDominionRegistryTerritory>
}

export function ShowDominionRegistry( packet: ReadableClientPacket ) : ShowDominionRegistryEvent {
    let castleId = packet.readD() - 80,
            clanName = packet.readS(),
            clanLeaderName = packet.readS(),
            clanAllyName = packet.readS(),
            registeredClans = packet.readD(),
            registeredMercenaries = packet.readD(),
            warTime = packet.readD(),
            currentTime = packet.readD(),
            canCancelClanRegistration = packet.readD() === 1,
            canCancelMercenaryRegistration = packet.readD() === 1

    packet.skipD( 1 )

    return {
        castleId,
        clanName,
        clanLeaderName,
        clanAllyName,
        registeredClans,
        registeredMercenaries,
        warTime,
        currentTime,
        canCancelClanRegistration,
        canCancelMercenaryRegistration,
        territories: _.times( packet.readD(), () : ShowDominionRegistryTerritory => {
            let id = packet.readD(),
                    size = packet.readD()

            return {
                id,
                ownedWardIds: _.times( size, () : number => packet.readD() )
            }
        } )
    }
}