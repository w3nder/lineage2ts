import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface AirShipTeleportData {
    fuel: number
    x: number
    y: number
    z: number
}
export interface AirShipTeleportListEvent extends PacketEvent {
    dockId: number
    teleports: Array<AirShipTeleportData>
}
export function AirShipTeleportList( packet : ReadableClientPacket ) : AirShipTeleportListEvent {
    let dockId = packet.readD(),
            size = packet.readD()

    return {
        dockId,
        teleports: _.times( size, () : AirShipTeleportData => {
            packet.skipD( 1 )
            let fuel = packet.readD(),
                    x = packet.readD(),
                    y = packet.readD(),
                    z = packet.readD()

            return {
                fuel,
                x,
                y,
                z
            }
        } )
    }
}