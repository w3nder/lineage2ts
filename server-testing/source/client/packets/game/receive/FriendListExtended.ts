import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'
import _ from 'lodash'
import { FriendData } from './FriendList'

export interface FriendListExtendedData extends FriendData{
    classId: number
    level: number
}
export interface FriendListExtendedEvent extends PacketEvent {
    friends: Array<FriendListExtendedData>
}

export function FriendListExtended( packet: ReadableClientPacket ) : FriendListExtendedEvent {
    let size = packet.readD()

    return {
        friends: _.times( size, () : FriendListExtendedData => {
            let playerId = packet.readD(),
                    name = packet.readS(),
                    isOnline = packet.readD() === 1

            packet.skipD( 1 )

            let classId = packet.readD(),
                    level = packet.readD()

            if ( isOnline ) {
                let player = getPlayerForUpdate( playerId )

                player.name = name
                player.classId = classId
                player.level = level
            }

            return {
                playerId,
                name,
                isOnline,
                classId,
                level
            }
        } )
    }
}