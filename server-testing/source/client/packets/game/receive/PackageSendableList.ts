import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { readNormalItem } from '../helpers/ItemUpdateHelper'
import _ from 'lodash'

export interface PackageSendableListEvent extends PacketEvent {
    targetObjectId: number
    availableAdena: number
    itemObjectIds: Array<number>
}

export function PackageSendableList( packet: ReadableClientPacket ) : PackageSendableListEvent {
    let targetObjectId = packet.readD(),
            availableAdena = packet.readQ(),
            size = packet.readD()

    return {
        targetObjectId,
        availableAdena,
        itemObjectIds: _.times( size, () : number => {
            let item = readNormalItem( packet )

            packet.skipD( 1 )

            return item.objectId
        } )
    }
}