import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export const enum MailMessageStatus {
    Deleted,
    Read,
    Rejected,
}

export interface ChangeMailMessageData {
    messageId: number
    state: MailMessageStatus
}

export interface ChangeMailMessageStateEvent extends PacketEvent {
    messagesReceived : boolean
    items: Array<ChangeMailMessageData>
}
export function ChangeMailMessageState( packet: ReadableClientPacket ) : ChangeMailMessageStateEvent {
    let messagesReceived = packet.readD() === 1,
            size = packet.readD()

    return {
        messagesReceived,
        items: _.times( size, () : ChangeMailMessageData => {
            let messageId = packet.readD(),
                    state = packet.readD()

            return {
                messageId,
                state
            }
        } )
    }
}