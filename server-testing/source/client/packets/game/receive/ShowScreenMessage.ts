import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum ShowScreenMessagePosition {
    TopLeft = 0X01,
    TopCenter = 0x02,
    TopRight = 0X03,
    MiddleLeft = 0X04,
    MiddleCenter = 0X05,
    MiddleRight = 0X06,
    BottomCenter = 0X07,
    BottomRight = 0X08,
}

export const enum ShowScreenMessageSize {
    Normal = 0x00,
    Small = 0x01,
}

export interface ShowScreenMessageEvent extends PacketEvent {
    messageId: number
    position: ShowScreenMessagePosition
    size: ShowScreenMessageSize
    time: number
    npcStringId: number
    lines: Array<string>
}
export function ShowScreenMessage( packet: ReadableClientPacket ) : ShowScreenMessageEvent {
    packet.skipD( 1 )

    let messageId = packet.readD(),
            position = packet.readD()

    packet.skipD( 1 )

    let size = packet.readD()

    packet.skipD( 3 )

    let time = packet.readD()

    packet.skipD( 1 )

    let npcStringId = packet.readD()
    let lines : Array<string> = []

    if ( npcStringId === -1 ) {
        lines.push( packet.readS() )
    } else {
        while ( packet.hasMoreData() ) {
            lines.push( packet.readS() )
        }
    }

    return {
        messageId,
        position,
        size,
        time,
        npcStringId,
        lines
    }
}