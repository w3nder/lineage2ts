import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { ClanPrivilege } from '../../../enums/ClanPrivileges'

export interface PledgePowerInfoEvent extends PacketEvent {
    name: string
    grade: number // TODO : add enum
    privilegesMask: ClanPrivilege
}

export function PledgePowerInfo( packet: ReadableClientPacket ) : PledgePowerInfoEvent {
    let grade = packet.readD(),
            name = packet.readS(),
            privilegesMask = packet.readD()

    return {
        grade,
        name,
        privilegesMask
    }
}