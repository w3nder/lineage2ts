import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { L2World } from '../../../L2World'
import { L2Character } from '../../../models/L2Character'
import _ from 'lodash'
import { StatusUpdateProperty } from '../../../enums/StatusUpdateProperty'
import { L2ObjectType } from '../../../enums/L2ObjectType'
import { L2Player } from '../../../models/L2Player'

export interface StatusUpdateEvent extends PacketEvent {
    characterObjectId: number
    parameters: Record<string, number>
}
export function StatusUpdate( packet : ReadableClientPacket ) : StatusUpdateEvent {
    let characterObjectId = packet.readD(),
            amount = packet.readD()

    let existingCharacter = L2World.getObject( characterObjectId ) as L2Character
    let parameters = {}

    if ( existingCharacter ) {
        _.times( amount, () => {
            let property = packet.readD()
            let value = packet.readD()

            parameters[ StatusUpdateProperty[ property ] ] = value

            switch ( property ) {
                case StatusUpdateProperty.CurrentHP:
                    existingCharacter.hp = value
                    break

                case StatusUpdateProperty.MaxHP:
                    existingCharacter.maxHp = value
                    break

                case StatusUpdateProperty.CurrentMP:
                    existingCharacter.mp = value
                    break

                case StatusUpdateProperty.MaxMP:
                    existingCharacter.maxMp = value
                    break

                case StatusUpdateProperty.CurrentCP:
                    if ( existingCharacter.type !== L2ObjectType.Player ) {
                        break
                    }

                    ( existingCharacter as L2Player ).cp = value
                    break

                case StatusUpdateProperty.MaxCP:
                    if ( existingCharacter.type !== L2ObjectType.Player ) {
                        break
                    }

                    ( existingCharacter as L2Player ).maxCp = value
                    break

                default:
                    existingCharacter.stats[ property ] = value
                    break
            }
        } )
    }

    return {
        characterObjectId,
        parameters
    }
}