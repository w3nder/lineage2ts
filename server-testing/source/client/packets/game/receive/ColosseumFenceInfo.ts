import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getObjectForUpdate } from '../helpers/CharacterUpdateHelper'
import { L2ObjectType } from '../../../enums/L2ObjectType'
import { L2Fence } from '../../../models/L2Fence'

export interface ColosseumFenceInfoEvent extends PacketEvent {
    objectId: number
}

export function ColosseumFenceInfo( packet: ReadableClientPacket ): ColosseumFenceInfoEvent {
    let objectId = packet.readD()

    let object = getObjectForUpdate( objectId, L2ObjectType.Fence ) as L2Fence

    object.state = packet.readD()
    object.x = packet.readD()
    object.y = packet.readD()
    object.z = packet.readD()
    object.width = packet.readD()
    object.height = packet.readD()

    return {
        objectId
    }
}