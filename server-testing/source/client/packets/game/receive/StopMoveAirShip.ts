import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getAirshipForUpdate } from '../helpers/CharacterUpdateHelper'

export interface StopMoveAirShipEvent extends PacketEvent {
    shipObjectId: number
}

export function StopMoveAirShip( packet: ReadableClientPacket ) : StopMoveAirShipEvent {
    let shipObjectId = packet.readD()

    let airship = getAirshipForUpdate( shipObjectId )

    airship.x = packet.readD()
    airship.y = packet.readD()
    airship.z = packet.readD()
    airship.heading = packet.readD()

    return {
        shipObjectId
    }
}