import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { LocationProperties } from '../../../models/LocationProperties'
import _ from 'lodash'

export interface GetBookMarkInfoData extends LocationProperties {
    id: number
    name: string
    icon: number
    tag: string
}
export interface GetBookMarkInfoEvent extends PacketEvent {
    freeSlots: number
    bookmarks: Array<GetBookMarkInfoData>
}
export function GetBookMarkInfo( packet: ReadableClientPacket ) : GetBookMarkInfoEvent {
    packet.skipD( 1 )

    let freeSlots = packet.readD(),
            size = packet.readD()

    return {
        freeSlots,
        bookmarks: _.times( size, () : GetBookMarkInfoData => {
            let id = packet.readD(),
                    x = packet.readD(),
                    y = packet.readD(),
                    z = packet.readD(),
                    name = packet.readS(),
                    icon = packet.readD(),
                    tag = packet.readS()

            return {
                id,
                x,
                y,
                z,
                name,
                icon,
                tag
            }
        } )
    }
}