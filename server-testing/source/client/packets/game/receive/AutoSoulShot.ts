import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export const enum AutoSoulShotStatus {
    Disabled,
    Enabled
}

export interface AutoSoulShotEvent extends PacketEvent {
    itemId: number
    status: AutoSoulShotStatus
}
export function AutoSoulShot( packet: ReadableClientPacket ) : AutoSoulShotEvent {
    let itemId = packet.readD(),
            status = packet.readD()

    return {
        itemId,
        status
    }
}