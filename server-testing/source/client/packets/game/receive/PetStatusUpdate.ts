import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getSummonForUpdate } from '../helpers/CharacterUpdateHelper'

export interface PetStatusUpdateEvent extends PacketEvent {
    petObjectId: number
}

export function PetStatusUpdate( packet: ReadableClientPacket ) : PetStatusUpdateEvent {
    let summonType = packet.readD(),
            petObjectId = packet.readD()

    let pet = getSummonForUpdate( petObjectId )

    pet.summonType = summonType
    pet.x = packet.readD()
    pet.y = packet.readD()
    pet.z = packet.readD()
    pet.title = packet.readS()
    pet.feed = packet.readD()
    pet.maxFeed = packet.readD()
    pet.hp = packet.readD()
    pet.maxHp = packet.readD()
    pet.mp = packet.readD()
    pet.maxMp = packet.readD()
    pet.level = packet.readD()
    pet.exp = packet.readQ()

    return {
        petObjectId
    }
}