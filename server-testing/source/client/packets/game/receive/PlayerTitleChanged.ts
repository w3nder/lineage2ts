import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getPlayerForUpdate } from '../helpers/CharacterUpdateHelper'

export interface PlayerTitleChangedEvent extends PacketEvent {
    playerObjectId: number
}

export function PlayerTitleChanged( packet : ReadableClientPacket ) : PlayerTitleChangedEvent {
    let playerObjectId = packet.readD()
    let player = getPlayerForUpdate( playerObjectId )

    player.title = packet.readS()

    return {
        playerObjectId
    }
}