import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface TradeOtherDoneEvent extends PacketEvent {}
export function TradeOtherDone( packet : ReadableClientPacket ) : TradeOtherDoneEvent {
    return {}
}