import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface GMViewQuestData {
    id: number
    flags: number
}

export interface GMViewQuestInfoEvent extends PacketEvent {
    playerName: string
    quests: Array<GMViewQuestData>
}

export function GMViewQuestInfo( packet: ReadableClientPacket ) : GMViewQuestInfoEvent {
    let playerName = packet.readS(),
            size = packet.readH()

    return {
        playerName,
        quests: _.times( size, () : GMViewQuestData => {
            let id = packet.readD(),
                    flags = packet.readD()

            return {
                id,
                flags
            }
        } )
    }
}