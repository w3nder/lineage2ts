import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export interface BasicActionListEvent extends PacketEvent {
    actionIds: Array<number>
}

export function BasicActionList( packet : ReadableClientPacket ) : BasicActionListEvent {
    let size = packet.readD()

    return {
        actionIds: _.times( size, () : number => packet.readD() )
    }
}