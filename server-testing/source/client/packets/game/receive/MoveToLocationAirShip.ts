import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { getAirshipForUpdate } from '../helpers/CharacterUpdateHelper'

export interface MoveToLocationAirShipEvent extends PacketEvent {
    objectId: number
}
export function MoveToLocationAirShip( packet: ReadableClientPacket ) : MoveToLocationAirShipEvent {
    let objectId = packet.readD()

    let airship = getAirshipForUpdate( objectId )

    airship.destinationX = packet.readD()
    airship.destinationY = packet.readD()
    airship.destinationZ = packet.readD()

    airship.x = packet.readD()
    airship.y = packet.readD()
    airship.z = packet.readD()

    return {
        objectId
    }
}