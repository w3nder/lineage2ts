import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import { AttackEffect } from '../../../enums/AttackEffects'
import { L2World } from '../../../L2World'
import { L2Character } from '../../../models/L2Character'
import _ from 'lodash'

export interface TargetAttackedEvent extends PacketEvent {
    attackerObjectId: number
    targetObjectIds: Array<number>
    damage: number
    effects: AttackEffect
}
export function TargetAttacked( packet : ReadableClientPacket ) : TargetAttackedEvent {
    let attackerObjectId = packet.readD()
    let targetObjectIds = [ packet.readD() ]
    let damage = packet.readD()
    let effects = packet.readC()

    let existingCharacter = L2World.getObject( attackerObjectId ) as L2Character
    if ( existingCharacter ) {
        existingCharacter.x = packet.readD()
        existingCharacter.y = packet.readD()
        existingCharacter.z = packet.readD()
    }

    let remainingHits = packet.readH()
    _.times( remainingHits, () => {
        targetObjectIds.push( packet.readD() )
        damage += packet.readD()

        packet.skipD( 1 )
    } )

    return {
        attackerObjectId,
        targetObjectIds,
        damage,
        effects
    }
}