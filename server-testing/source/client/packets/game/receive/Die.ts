import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface GameDieEvent extends PacketEvent {
    objectId: number
    canTeleport: boolean
    escapeToHideout: boolean
    escapeToCastle: boolean
    escapeToHQ: boolean
    isSweepable: boolean
    canSelfResurrect: boolean
    escapeToFort: boolean
}

export function Die( packet: ReadableClientPacket ): GameDieEvent {
    let objectId = packet.readD(),
            canTeleport = packet.readD() === 0x01,
            escapeToHideout = packet.readD() === 0x01,
            escapeToCastle = packet.readD() === 0x01,
            escapeToHQ = packet.readD() === 0x01,
            isSweepable = packet.readD() === 0x01,
            canSelfResurrect = packet.readD() === 0x01,
            escapeToFort = packet.readD() === 0x01

    return {
        canSelfResurrect,
        canTeleport,
        escapeToCastle,
        escapeToFort,
        escapeToHQ,
        escapeToHideout,
        isSweepable,
        objectId
    }
}