import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface NpcQuestHtmlMessageEvent extends PacketEvent {
    originatorObjectId: number
    html: string
    questId: number
}
export function NpcQuestHtmlMessage( packet : ReadableClientPacket ) : NpcQuestHtmlMessageEvent {
    let originatorObjectId = packet.readD(),
            html = packet.readS(),
            questId = packet.readD()

    return {
        originatorObjectId,
        html,
        questId
    }
}