import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface PledgeMemberInfoEvent extends PacketEvent {
    type: number // TODO : add enums
    playerName: string
    playerTitle: string
    grade: number // TODO : add enums
    clanName: string
    playerSponsorName: string
}

export function PledgeMemberInfo( packet: ReadableClientPacket ) : PledgeMemberInfoEvent {
    let type = packet.readD(),
            playerName = packet.readS(),
            playerTitle = packet.readS(),
            grade = packet.readD(),
            clanName = packet.readS(),
            playerSponsorName = packet.readS()

    return {
        type,
        playerName,
        playerTitle,
        grade,
        clanName,
        playerSponsorName
    }
}