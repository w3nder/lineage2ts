import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'
import _ from 'lodash'

export const enum SkillListEffect {
    None = 0,
    Passive = 1,
    Disabled = 1 << 2,
    Enchanted = 1 << 3
}

export interface SkillListItem extends PacketEvent{
    id: number
    level: number
    effect: SkillListEffect
}
export interface SkillListEvent extends PacketEvent {
    skills: Array<SkillListItem>
}
export function SkillList( packet : ReadableClientPacket ) : SkillListEvent {
    let size = packet.readD()

    return {
        skills: _.times( size, () : SkillListItem => {
            let effect = packet.readD() === 1 ? SkillListEffect.Passive : SkillListEffect.None
            let id = packet.readD()
            let level = packet.readD()

            if ( packet.readC() === 1 ) {
                effect |= SkillListEffect.Disabled
            }

            if ( packet.readC() === 1 ) {
                effect |= SkillListEffect.Enchanted
            }

            return {
                effect,
                id,
                level
            }
        } )
    }
}