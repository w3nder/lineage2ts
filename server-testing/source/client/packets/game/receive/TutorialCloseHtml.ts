import { ReadableClientPacket } from '../../ReadableClientPacket'
import { PacketEvent } from '../../PacketMethodTypes'

export interface TutorialCloseHtmlEvent extends PacketEvent {

}
export function TutorialCloseHtml( packet : ReadableClientPacket ) : TutorialCloseHtmlEvent {
    return {}
}