import { EmptyPacketHandler, PacketMethodMap } from './PacketMethodTypes'
import { Die } from './game/receive/Die'
import { Revive } from './game/receive/Revive'
import { SpawnItem } from './game/receive/SpawnItem'
import { DeleteObject } from './game/receive/DeleteObject'
import { CharacterSelectionInfo } from './game/receive/CharacterSelectionInfo'
import { AccessFailed } from './game/receive/AccessFailed'
import { CharacterSelected } from './game/receive/CharacterSelected'
import { NpcInfo } from './game/receive/NpcInfo'
import { ServerCharacterTemplates } from './game/receive/ServerCharacterTemplates'
import { CharacterCreationSuccess } from './game/receive/CharacterCreationSuccess'
import { CharacterCreationFailure } from './game/receive/CharacterCreationFailure'
import { ItemList } from './game/receive/ItemList'
import { TradeStart } from './game/receive/TradeStart'
import { DropItem } from './game/receive/DropItem'
import { PickItemUp } from './game/receive/PickItemUp'
import { StatusUpdate } from './game/receive/StatusUpdate'
import { NpcHtmlMessage } from './game/receive/NpcHtmlMessage'
import { ServerClose } from './game/receive/ServerClose'
import { TradeOwnAdd } from './game/receive/TradeOwnAdd'
import { TradeOtherAdd } from './game/receive/TradeOtherAdd'
import { TradeDone } from './game/receive/TradeDone'
import { ActionFailed } from './game/receive/ActionFailed'
import { InventoryUpdate } from './game/receive/InventoryUpdate'
import { TeleportToLocation } from './game/receive/TeleportToLocation'
import { TargetSelectedWithPosition } from './game/receive/TargetSelectedWithPosition'
import { TargetUnselectedWithPosition } from './game/receive/TargetUnselectedWithPosition'
import { AutoAttackStart } from './game/receive/AutoAttackStart'
import { AutoAttackStop } from './game/receive/AutoAttackStop'
import { SocialAction } from './game/receive/SocialAction'
import { ChangeMoveType } from './game/receive/ChangeMoveType'
import { ChangeWaitType } from './game/receive/ChangeWaitType'
import { GameServerKey } from './game/receive/GameServerKey'
import { MoveToLocation } from './game/receive/MoveToLocation'
import { NpcSay } from './game/receive/NpcSay'
import { PlayerInfo } from './game/receive/PlayerInfo'
import { UserInfo } from './game/receive/UserInfo'
import { TargetAttacked } from './game/receive/TargetAttacked'
import { AskJoinParty } from './game/receive/AskJoinParty'
import { JoinParty } from './game/receive/JoinParty'
import { WareHouseDeposit } from './game/receive/WareHouseDeposit'
import { WareHouseWithdrawal } from './game/receive/WareHouseWithdrawal'
import { StopMove } from './game/receive/StopMove'
import { MagicSkillUse } from './game/receive/MagicSkillUse'
import { SayText } from './game/receive/SayText'
import { PartySmallWindowAll } from './game/receive/PartySmallWindowAll'
import { PartySmallWindowAdd } from './game/receive/PartySmallWindowAdd'
import { PartySmallWindowDeleteAll } from './game/receive/PartySmallWindowDeleteAll'
import { PartySmallWindowDelete } from './game/receive/PartySmallWindowDelete'
import { PartySmallWindowUpdate } from './game/receive/PartySmallWindowUpdate'
import { MagicSkillLaunched } from './game/receive/MagicSkillLaunched'
import { SkillList } from './game/receive/SkillList'
import { VehicleInfo } from './game/receive/VehicleInfo'
import { StopRotation } from './game/receive/StopRotation'
import { SystemMessage } from './game/receive/SystemMessage'
import { SetupGauge } from './game/receive/SetupGauge'
import { VehicleDeparture } from './game/receive/VehicleDeparture'
import { SendTradeRequest } from './game/receive/SendTradeRequest'
import { RestartResponse } from './game/receive/RestartResponse'
import { MoveToCharacter } from './game/receive/MoveToCharacter'
import { SevenSignsInfo } from './game/receive/SevenSignsInfo'
import { FriendList } from './game/receive/FriendList'
import { ValidateLocation } from './game/receive/ValidateLocation'
import { StartRotation } from './game/receive/StartRotation'
import { ShowBoard } from './game/receive/ShowBoard'
import { StopMoveInVehicle } from './game/receive/StopMoveInVehicle'
import { TradeOtherDone } from './game/receive/TradeOtherDone'
import { LeaveWorld } from './game/receive/LeaveWorld'
import { AbnormalStatusUpdate } from './game/receive/AbnormalStatusUpdate'
import { PledgeInfo } from './game/receive/PledgeInfo'
import { StaticObject } from './game/receive/StaticObject'
import { PrivateStoreListSell } from './game/receive/PrivateStoreListSell'
import { TutorialShowHtml } from './game/receive/TutorialShowHtml'
import { TutorialShowQuestionMark } from './game/receive/TutorialShowQuestionMark'
import { TutorialClientAction } from './game/receive/TutorialClientAction'
import { TutorialCloseHtml } from './game/receive/TutorialCloseHtml'
import { DeletePet } from './game/receive/DeletePet'
import { CurrentPlayerTarget } from './game/receive/CurrentPlayerTarget'
import { PartyMemberPosition } from './game/receive/PartyMemberPosition'
import { VehicleStarted } from './game/receive/VehicleStarted'
import { SkillCoolTime } from './game/receive/SkillCoolTime'
import { PlayerTitleChanged } from './game/receive/PlayerTitleChanged'
import { PlayerRelationChanged } from './game/receive/PlayerRelationChanged'
import { SnoopMessage } from './game/receive/SnoopMessage'
import { RecipeBookItemList } from './game/receive/RecipeBookItemList'
import { RecipeItemMakeInfo } from './game/receive/RecipeItemMakeInfo'
import { HennaDrawInfo } from './game/receive/HennaDrawInfo'
import { HennaInfo } from './game/receive/HennaInfo'
import { HennaRemoveList } from './game/receive/HennaRemoveList'
import { HennaRemoveInfo } from './game/receive/HennaRemoveInfo'
import { HennaEquipList } from './game/receive/HennaEquipList'
import { ConfirmDialog } from './game/receive/ConfirmDialog'
import { PartyMemberBuffInfo } from './game/receive/PartyMemberBuffInfo'
import { EtcStatusUpdate } from './game/receive/EtcStatusUpdate'
import { FishingEnd } from './game/receive/FishingEnd'
import { ManorList } from './game/receive/ManorList'
import { FishingHpRegen } from './game/receive/FishingHpRegen'
import { StorageMaxCount } from './game/receive/StorageMaxCount'
import { SetCompassZoneCode } from './game/receive/SetCompassZoneCode'
import { ShowScreenMessage } from './game/receive/ShowScreenMessage'
import { RedSky } from './game/receive/RedSky'
import { DuelAskStart } from './game/receive/DuelAskStart'
import { UISettings } from './game/receive/UISettings'
import { NpcQuestHtmlMessage } from './game/receive/NpcQuestHtmlMessage'
import { Rotation } from './game/receive/Rotation'
import { QuestItemList } from './game/receive/QuestItemList'
import { VoteSystemInfo } from './game/receive/VoteSystemInfo'
import { ShowContactList } from './game/receive/ShowContactList'
import { NevitAdventPointInfo } from './game/receive/NevitAdventPointInfo'
import { NevitAdventTimeChange } from './game/receive/NevitAdventTimeChange'
import { AirShipInfo } from './game/receive/AirShipInfo'
import { AirShipTeleportList } from './game/receive/AirShipTeleportList'
import { AskCoupleAction } from './game/receive/AskCoupleAction'
import { AskJoinMPCC } from './game/receive/AskJoinMPCC'
import { AskJoinPartyRoom } from './game/receive/AskJoinPartyRoom'
import { AskModifyPartyLooting } from './game/receive/AskModifyPartyLooting'
import { AttributeEnchantResult } from './game/receive/AttributeEnchantResult'
import { AutoSoulShot } from './game/receive/AutoSoulShot'
import { BaseAttributeCancelResult } from './game/receive/BaseAttributeCancelResult'
import { BasicActionList } from './game/receive/BasicActionList'
import { AgathionEnergyInfo } from './game/receive/AgathionEnergyInfo'
import { ExtraUserInfo } from './game/receive/ExtraUserInfo'
import { LoadEventTopRankers } from './game/receive/LoadEventTopRankers'
import { BuySellList } from './game/receive/BuySellList'
import { ChangeNpcState } from './game/receive/ChangeNpcState'
import { ChangeMailMessageState } from './game/receive/ChangeMailMessageState'
import { ChooseInventoryAttributeItem } from './game/receive/ChooseInventoryAttributeItem'
import { CloseMPCC } from './game/receive/CloseMPCC'
import { ClosePartyRoom } from './game/receive/ClosePartyRoom'
import { ColosseumFenceInfo } from './game/receive/ColosseumFenceInfo'
import { ConfirmAddingContact } from './game/receive/ConfirmAddingContact'
import { CubeGameChangePoints } from './game/receive/CubeGameChangePoints'
import { CubeGameChangeTeam } from './game/receive/CubeGameChangeTeam'
import { CursedWeaponList } from './game/receive/CursedWeaponList'
import { CursedWeaponLocation } from './game/receive/CursedWeaponLocation'
import { DominionWarStart } from './game/receive/DominionWarStart'
import { DuelEnd } from './game/receive/DuelEnd'
import { DuelReady } from './game/receive/DuelReady'
import { DuelStart } from '../models/DuelStart'
import { DuelUpdateUserInfo } from './game/receive/DuelUpdateUserInfo'
import { EnchantSkillInfo } from './game/receive/EnchantSkillInfo'
import { EnchantSkillInfoDetail } from './game/receive/EnchantSkillInfoDetail'
import { EnchantSkillResult } from './game/receive/EnchantSkillResult'
import { FishingStart } from './game/receive/FishingStart'
import { FishingStartCombat } from './game/receive/FishingStartCombat'
import { GetBookMarkInfo } from './game/receive/GetBookMarkInfo'
import { GetBossRecord } from './game/receive/GetBossRecord'
import { GetDimensionalItemList } from './game/receive/GetDimensionalItemList'
import { GetOnAirShip } from './game/receive/GetOnAirShip'
import { HeroList } from './game/receive/HeroList'
import { ItemAuctionInfo } from './game/receive/ItemAuctionInfo'
import { PartyMatchingWaitingRoomList } from './game/receive/PartyMatchingWaitingRoomList'
import { ManagePartyRoomMember } from './game/receive/ManagePartyRoomMember'
import { MoveToLocationAirShip } from './game/receive/MoveToLocationAirShip'
import { MoveToLocationInAirShip } from './game/receive/MoveToLocationInAirShip'
import { MPCCPartyInfoUpdate } from './game/receive/MPCCPartyInfoUpdate'
import { MPCCShowPartyMemberInfo } from './game/receive/MPCCShowPartyMemberInfo'
import { MultiPartyCommandChannelInfo } from './game/receive/MultiPartyCommandChannelInfo'
import { NevitAdventEffect } from './game/receive/NevitAdventEffect'
import { NoticePostArrived } from './game/receive/NoticePostArrived'
import { NoticePostSent } from './game/receive/NoticePostSent'
import { NotifyDimensionalItem } from './game/receive/NotifyDimensionalItem'
import { OlympiadMatchEnd } from './game/receive/OlympiadMatchEnd'
import { OlympiadMatchList } from './game/receive/OlympiadMatchList'
import { OlympiadMode } from './game/receive/OlympiadMode'
import { OlympiadUserInfo } from './game/receive/OlympiadUserInfo'
import { OpenMPCC } from './game/receive/OpenMPCC'
import { PartyPetWindowAdd } from './game/receive/PartyPetWindowAdd'
import { PartyPetWindowDelete } from './game/receive/PartyPetWindowDelete'
import { PartyPetWindowUpdate } from './game/receive/PartyPetWindowUpdate'
import { PartyRoomMember } from './game/receive/PartyRoomMember'
import { PledgeCrestLarge } from './game/receive/PledgeCrestLarge'
import { PrivateStoreSetWholesaleMessage } from './game/receive/PrivateStoreSetWholesaleMessage'
import { PutCommissionResultForVariationMake } from './game/receive/PutCommissionResultForVariationMake'
import { PutEnchantSupportItemResult } from './game/receive/PutEnchantSupportItemResult'
import { PutEnchantTargetItemResult } from './game/receive/PutEnchantTargetItemResult'
import { PutIntensiveResultForVariationMake } from './game/receive/PutIntensiveResultForVariationMake'
import { PutItemResultForVariationCancel } from './game/receive/PutItemResultForVariationCancel'
import { PutItemResultForVariationMake } from './game/receive/PutItemResultForVariationMake'
import { HPRegenMax } from './game/receive/HPRegenMax'
import { ReplyDominionInfo } from './game/receive/ReplyDominionInfo'
import { ReplyPostItemList } from './game/receive/ReplyPostItemList'
import { ReplyReceivedPost } from './game/receive/ReplyReceivedPost'
import { ReplySentPost } from './game/receive/ReplySentPost'
import { RequestChangeNicknameColor } from './game/receive/RequestChangeNicknameColor'
import { ChatItemLink } from './game/receive/ChatItemLink'
import { SendUIInfo } from './game/receive/SendUIInfo'
import { SetPartyLooting } from './game/receive/SetPartyLooting'
import { ShowAuctionHallInfo } from './game/receive/ShowAuctionHallInfo'
import { ShowBaseAttributeCancelWindow } from './game/receive/ShowBaseAttributeCancelWindow'
import { ShowCastleInfo } from './game/receive/ShowCastleInfo'
import { ShowCropInfo } from './game/receive/ShowCropInfo'
import { ShowCropSetting } from './game/receive/ShowCropSetting'
import { ShowDominionRegistry } from './game/receive/ShowDominionRegistry'
import { ShowFortressInfo } from './game/receive/ShowFortressInfo'
import { ShowFortressMapInfo } from './game/receive/ShowFortressMapInfo'
import { ShowFortressSiegeInfo } from './game/receive/ShowFortressSiegeInfo'
import { ShowManorDefaultInfo } from './game/receive/ShowManorDefaultInfo'
import { ShowOwnedThingsPosition } from './game/receive/ShowOwnedThingsPosition'
import { ShowProcureCropDetail } from './game/receive/ShowProcureCropDetail'
import { ShowQuestMark } from './game/receive/ShowQuestMark'
import { ShowReceivedPostList } from './game/receive/ShowReceivedPostList'
import { ShowSeedInfo } from './game/receive/ShowSeedInfo'
import { ShowSeedMapInfo } from './game/receive/ShowSeedMapInfo'
import { ShowSeedSetting } from './game/receive/ShowSeedSetting'
import { ShowSellCropList } from './game/receive/ShowSellCropList'
import { ShowSentPostList } from './game/receive/ShowSentPostList'
import { ShowVariationCancelWindow } from './game/receive/ShowVariationCancelWindow'
import { ShowVariationMakeWindow } from './game/receive/ShowVariationMakeWindow'
import { SpawnEmitter } from './game/receive/SpawnEmitter'
import { StartScenePlayer } from './game/receive/StartScenePlayer'
import { StopMoveAirShip } from './game/receive/StopMoveAirShip'
import { SubPledgeSkillAdd } from './game/receive/SubPledgeSkillAdd'
import { UseSharedGroupItem } from './game/receive/UseSharedGroupItem'
import { RefineCancelResult } from './game/receive/RefineCancelResult'
import { RefineResult } from './game/receive/RefineResult'
import { VitalityPointInfo } from './game/receive/VitalityPointInfo'
import { FlyToLocationCoordinates } from './game/receive/FlyToLocationCoordinates'
import { FriendAddRequest } from './game/receive/FriendAddRequest'
import { FriendListExtended } from './game/receive/FriendListExtended'
import { FriendOperation } from './game/receive/FriendOperation'
import { FriendStatus } from './game/receive/FriendStatus'
import { GetOffVehicle } from './game/receive/GetOffVehicle'
import { GetOnVehicle } from './game/receive/GetOnVehicle'
import { GMHennaInfo } from './game/receive/GMHennaInfo'
import { GMViewCharacterInfo } from './game/receive/GMViewCharacterInfo'
import { GMViewInventory } from './game/receive/GMViewInventory'
import { GMViewPledgeInfo } from './game/receive/GMViewPledgeInfo'
import { GMViewQuestInfo } from './game/receive/GMViewQuestInfo'
import { GMViewSkillInfo } from './game/receive/GMViewSkillInfo'
import { GMViewWarehouseWithdrawList } from './game/receive/GMViewWarehouseWithdrawList'
import { JoinPledge } from './game/receive/JoinPledge'
import { FriendSay } from './game/receive/FriendSay'
import { ListPartyWaiting } from './game/receive/ListPartyWaiting'
import { MagicSkillCanceled } from './game/receive/MagicSkillCanceled'
import { ManagePledgePower } from './game/receive/ManagePledgePower'
import { MoveToLocationInVehicle } from './game/receive/MoveToLocationInVehicle'
import { MultiSellList } from './game/receive/MultiSellList'
import { Music } from './game/receive/Music'
import { EnterObservationMode } from './game/receive/EnterObservationMode'
import { ExitObservationMode } from './game/receive/ExitObservationMode'
import { DoorEventTrigger } from './game/receive/DoorEventTrigger'
import { PackageSendableList } from './game/receive/PackageSendableList'
import { PackageDestinationList } from './game/receive/PackageDestinationList'
import { PartyMatchDetail } from './game/receive/PartyMatchDetail'
import { PetInfo } from './game/receive/PetInfo'
import { PetItemList } from './game/receive/PetItemList'
import { PetStatusInfo } from './game/receive/PetStatusInfo'
import { PetStatusUpdate } from './game/receive/PetStatusUpdate'
import { PledgeCrest } from './game/receive/PledgeCrest'
import { PledgePowerGradeList } from './game/receive/PledgePowerGradeList'
import { PledgeMemberInfo } from './game/receive/PledgeMemberInfo'
import { PledgePowerInfo } from './game/receive/PledgePowerInfo'
import { SubPledgeInfo } from './game/receive/SubPledgeInfo'
import { PledgeWarList } from './game/receive/PledgeWarList'
import { PledgeInfoUpdate } from './game/receive/PledgeInfoUpdate'
import { PledgeMemberAdd } from './game/receive/PledgeMemberAdd'
import { PledgeMemberList } from './game/receive/PledgeMemberList'
import { PledgeMemberDelete } from './game/receive/PledgeMemberDelete'
import { PledgeDismissal } from './game/receive/PledgeDismissal'
import { PledgeMemberUpdate } from './game/receive/PledgeMemberUpdate'
import { PledgeSkillList } from './game/receive/PledgeSkillList'
import { PledgeSkillAdd } from './game/receive/PledgeSkillAdd'
import { PledgeStatus } from './game/receive/PledgeStatus'
import { PrivateStoreListBuy } from './game/receive/PrivateStoreListBuy'
import { PrivateStoreManageListBuy } from './game/receive/PrivateStoreManageListBuy'
import { PrivateStoreManageListSell } from './game/receive/PrivateStoreManageListSell'
import { PrivateStoreMessageBuy } from './game/receive/PrivateStoreMessageBuy'
import { PrivateStoreMessageSell } from './game/receive/PrivateStoreMessageSell'
import { QuestList } from './game/receive/QuestList'
import { RadarControl } from './game/receive/RadarControl'
import { RecipeShopItemInfo } from './game/receive/RecipeShopItemInfo'
import { RecipeShopManageList } from './game/receive/RecipeShopManageList'
import { RecipeShopMessage } from './game/receive/RecipeShopMessage'
import { RecipeShopSellList } from './game/receive/RecipeShopSellList'
import { AcquireSkillList } from './game/receive/AcquireSkillList'
import { PlayerRidesMount } from './game/receive/PlayerRidesMount'
import { MacroList } from './game/receive/MacroList'
import { ServerObjectInfo } from './game/receive/ServerObjectInfo'
import { SetSummonRemainTime } from './game/receive/SetSummonRemainTime'
import { PreviewItem } from './game/receive/PreviewItem'
import { PreviewItemList } from './game/receive/PreviewItemList'
import { ShortBuffStatus } from './game/receive/ShortBuffStatus'
import { ShowCalculator } from './game/receive/ShowCalculator'
import { ShowMiniMap } from './game/receive/ShowMiniMap'
import { ShowTownMap } from './game/receive/ShowTownMap'
import { ShowChristmasSeal } from './game/receive/ShowChristmasSeal'
import { ResidenceSiegeClans } from './game/receive/ResidenceSiegeClans'
import { ResidenceDefenderClans } from './game/receive/ResidenceDefenderClans'
import { SiegeInfo } from './game/receive/SiegeInfo'
import { SevenSignsStatus } from './game/receive/SevenSignsStatus'
import { EnchantResult } from './game/receive/EnchantResult'
import { CharacterDeleteFail } from './game/receive/CharacterDeleteFail'
import { CharacterDeleteSuccess } from './game/receive/CharacterDeleteSuccess'
import { ServerPrimitive } from './game/receive/ServerPrimitive'
import { OlympiadPlayerBuffs } from './game/receive/OlympiadPlayerBuffs'
import { QuestNpcLog } from './game/receive/QuestNpcLog'
import { PetInventoryUpdate } from './game/receive/PetInventoryUpdate'
import { GameGuardQuery } from './game/receive/GameGuardQuery'

export const GamePacketMethods: PacketMethodMap = {
    0x00: Die,
    0x01: Revive,
    0x05: SpawnItem,
    0x08: DeleteObject,
    0x09: CharacterSelectionInfo,
    0x0a: AccessFailed,
    0x0b: CharacterSelected,
    0x0c: NpcInfo,
    0x0d: ServerCharacterTemplates,
    0x0f: CharacterCreationSuccess,
    0x10: CharacterCreationFailure,
    0x11: ItemList,
    0x12: EmptyPacketHandler, // SunRise
    0x13: EmptyPacketHandler, // SunSet
    0x14: TradeStart,
    0x16: DropItem,
    0x17: PickItemUp,
    0x18: StatusUpdate,
    0x19: NpcHtmlMessage,
    0x20: ServerClose,
    0x1a: TradeOwnAdd,
    0x1b: TradeOtherAdd,
    0x1c: TradeDone,
    0x1f: ActionFailed,
    0x21: InventoryUpdate,
    0x22: TeleportToLocation,
    0x23: TargetSelectedWithPosition,
    0x24: TargetUnselectedWithPosition,
    0x25: AutoAttackStart,
    0x26: AutoAttackStop,
    0x27: SocialAction,
    0x28: ChangeMoveType,
    0x29: ChangeWaitType,
    0x2e: GameServerKey,
    0x2f: MoveToLocation,
    0x30: NpcSay,
    0x31: PlayerInfo,
    0x32: UserInfo,
    0x33: TargetAttacked,
    0x39: AskJoinParty,
    0x3a: JoinParty,
    0x41: WareHouseDeposit,
    0x42: WareHouseWithdrawal,
    0x44: EmptyPacketHandler, // ShortcutRegistered
    0x45: EmptyPacketHandler, // InitialShortcuts
    0x47: StopMove,
    0x48: MagicSkillUse,
    0x4a: SayText,
    0x4b: EmptyPacketHandler, // EquipUpdate
    0x4e: PartySmallWindowAll,
    0x4f: PartySmallWindowAdd,
    0x50: PartySmallWindowDeleteAll,
    0x51: PartySmallWindowDelete,
    0x52: PartySmallWindowUpdate,
    0x54: MagicSkillLaunched,
    0x5f: SkillList,
    0x60: VehicleInfo,
    0x61: StopRotation,
    0x62: SystemMessage,
    0x63: EmptyPacketHandler, // StartPledgeWar
    0x65: EmptyPacketHandler, // StopPledgeWar
    0x67: EmptyPacketHandler, // SurrenderPledgeWar
    0x6b: SetupGauge,
    0x6c: VehicleDeparture,
    0x6d: EmptyPacketHandler, // VehicleCheckLocation
    0x70: SendTradeRequest,
    0x71: RestartResponse,
    0x72: MoveToCharacter,
    0x73: SevenSignsInfo,
    0x74: GameGuardQuery,
    0x75: FriendList,
    0x79: ValidateLocation,
    0x7a: StartRotation,
    0x7b: ShowBoard,
    0x7f: StopMoveInVehicle,
    0x80: EmptyPacketHandler, // ValidateLocationInVehicle
    0x82: TradeOtherDone,
    0x84: LeaveWorld,
    0x85: AbnormalStatusUpdate,
    0x89: PledgeInfo,
    0x9f: StaticObject,
    0xa1: PrivateStoreListSell,
    0xa6: TutorialShowHtml,
    0xa7: TutorialShowQuestionMark,
    0xa8: TutorialClientAction,
    0xa9: TutorialCloseHtml,
    0xb4: PetInventoryUpdate,
    0xb7: DeletePet,
    0xb9: CurrentPlayerTarget,
    0xba: PartyMemberPosition,
    0xc0: VehicleStarted,
    0xc7: SkillCoolTime,
    0xcc: PlayerTitleChanged,
    0xce: PlayerRelationChanged,
    0xd6: EmptyPacketHandler, // SpecialCamera
    0xd7: EmptyPacketHandler, // NormalCamera
    0xdb: SnoopMessage,
    0xdc: RecipeBookItemList,
    0xdd: RecipeItemMakeInfo,
    0xe4: HennaDrawInfo,
    0xe5: HennaInfo,
    0xe6: HennaRemoveList,
    0xe7: HennaRemoveInfo,
    0xee: HennaEquipList,
    0xf3: ConfirmDialog,
    0xf4: PartyMemberBuffInfo,
    0xf9: EtcStatusUpdate,
    0xd4: FlyToLocationCoordinates,
    0x83: FriendAddRequest,
    0x58: FriendListExtended,
    0x76: FriendOperation,
    0x77: FriendStatus,
    0x6f: GetOffVehicle,
    0x6e: GetOnVehicle,
    0xf0: GMHennaInfo,
    0x95: GMViewCharacterInfo,
    0x9a: GMViewInventory,
    0x96: GMViewPledgeInfo,
    0x99: GMViewQuestInfo,
    0x97: GMViewSkillInfo,
    0x9b: GMViewWarehouseWithdrawList,
    0x2d: JoinPledge,
    0x78: FriendSay,
    0x9c: ListPartyWaiting,
    0x49: MagicSkillCanceled,
    0x2a: ManagePledgePower,
    0x7e: MoveToLocationInVehicle,
    0xd0: MultiSellList,
    0x9e: Music,
    0xeb: EnterObservationMode,
    0xec: ExitObservationMode,
    0xcf: DoorEventTrigger,
    0xd2: PackageSendableList,
    0xc8: PackageDestinationList,
    0x9d: PartyMatchDetail,
    0xb2: PetInfo,
    0xb3: PetItemList,
    0xfc: EmptyPacketHandler, // enable petition evaluation UI button, PetitionVotePacket
    0xb1: PetStatusInfo,
    0xb6: PetStatusUpdate,
    0x6a: PledgeCrest,
    0x8e: PledgeInfoUpdate,
    0x5c: PledgeMemberAdd,
    0x5a: PledgeMemberList,
    0x5d: PledgeMemberDelete,
    0x88: PledgeDismissal,
    0x5b: PledgeMemberUpdate,
    0xcd: PledgeStatus,
    0xbe: PrivateStoreListBuy,
    0xbd: PrivateStoreManageListBuy,
    0xa0: PrivateStoreManageListSell,
    0xbf: PrivateStoreMessageBuy,
    0xa2: PrivateStoreMessageSell,
    0x86: QuestList,
    0xf1: RadarControl,
    0xe0: RecipeShopItemInfo,
    0xde: RecipeShopManageList,
    0xe1: RecipeShopMessage,
    0xdf: RecipeShopSellList,
    0x90: AcquireSkillList,
    0x8c: PlayerRidesMount,
    0xe8: MacroList,
    0x92: ServerObjectInfo,
    0xd1: SetSummonRemainTime,
    0xf6: PreviewItem,
    0xf5: PreviewItemList,
    0xfa: ShortBuffStatus,
    0xe2: ShowCalculator,
    0xa3: ShowMiniMap,
    0xea: ShowTownMap,
    0xf8: ShowChristmasSeal,
    0xca: ResidenceSiegeClans,
    0xcb: ResidenceDefenderClans,
    0xc9: SiegeInfo,
    0xfb: SevenSignsStatus,
    0x87: EnchantResult,
    0x1e: CharacterDeleteFail,
    0x1d: CharacterDeleteSuccess,
}

export const GameExtendedPacketMethods: PacketMethodMap = {
    0x1f: FishingEnd,
    0x22: ManorList,
    0x28: FishingHpRegen,
    0x2f: StorageMaxCount,
    0x33: SetCompassZoneCode,
    0x39: ShowScreenMessage,
    0x41: RedSky,
    0x4c: DuelAskStart,
    0x70: UISettings,
    0x8d: NpcQuestHtmlMessage,
    0xc1: Rotation,
    0xc6: QuestItemList,
    0xc9: VoteSystemInfo,
    0xd3: ShowContactList,
    0xdf: NevitAdventPointInfo,
    0xe1: NevitAdventTimeChange,
    0x60: AirShipInfo,
    0x9a: AirShipTeleportList,
    0xbb: AskCoupleAction,
    0x1a: AskJoinMPCC,
    0x35: AskJoinPartyRoom,
    0xbf: AskModifyPartyLooting,
    0x61: AttributeEnchantResult,
    0x0c: AutoSoulShot,
    0x75: BaseAttributeCancelResult,
    0x5f: BasicActionList,
    0xde: AgathionEnergyInfo,
    0xda: ExtraUserInfo,
    0xbd: LoadEventTopRankers,
    0xb7: BuySellList,
    0xbe: ChangeNpcState,
    0xb3: ChangeMailMessageState,
    0x62: ChooseInventoryAttributeItem,
    0x13: CloseMPCC,
    0x09: ClosePartyRoom,
    0x03: ColosseumFenceInfo,
    0xd2: ConfirmAddingContact,
    0x98: CubeGameChangePoints,
    0x97: CubeGameChangeTeam,
    0x46: CursedWeaponList,
    0x47: CursedWeaponLocation,
    0xa3: DominionWarStart,
    0x4f: DuelEnd,
    0x4d: DuelReady,
    0x4e: DuelStart,
    0x50: DuelUpdateUserInfo,
    0x2a: EnchantSkillInfo,
    0x5e: EnchantSkillInfoDetail,
    0xa7: EnchantSkillResult,
    0x1e: FishingStart,
    0x27: FishingStartCombat,
    0x84: GetBookMarkInfo,
    0x34: GetBossRecord,
    0x86: GetDimensionalItemList,
    0x63: GetOnAirShip,
    0x79: HeroList,
    0x68: ItemAuctionInfo,
    0x36: PartyMatchingWaitingRoomList,
    0x0a: ManagePartyRoomMember,
    0x65: MoveToLocationAirShip,
    0x6d: MoveToLocationInAirShip,
    0x5b: MPCCPartyInfoUpdate,
    0x4b: MPCCShowPartyMemberInfo,
    0x31: MultiPartyCommandChannelInfo,
    0xe0: NevitAdventEffect,
    0xa9: NoticePostArrived,
    0xb4: NoticePostSent,
    0x85: NotifyDimensionalItem,
    0x2d: OlympiadMatchEnd,
    0xd4: OlympiadMatchList,
    0x7c: OlympiadMode,
    0x7a: OlympiadUserInfo,
    0x12: OpenMPCC,
    0x18: PartyPetWindowAdd,
    0x6a: PartyPetWindowDelete,
    0x19: PartyPetWindowUpdate,
    0x08: PartyRoomMember,
    0x1b: PledgeCrestLarge,
    0x80: PrivateStoreSetWholesaleMessage,
    0x55: PutCommissionResultForVariationMake,
    0x82: PutEnchantSupportItemResult,
    0x81: PutEnchantTargetItemResult,
    0x54: PutIntensiveResultForVariationMake,
    0x57: PutItemResultForVariationCancel,
    0x53: PutItemResultForVariationMake,
    0x01: HPRegenMax,
    0x92: ReplyDominionInfo,
    0xb2: ReplyPostItemList,
    0xab: ReplyReceivedPost,
    0xad: ReplySentPost,
    0x83: RequestChangeNicknameColor,
    0x6c: ChatItemLink,
    0x8e: SendUIInfo,
    0xc0: SetPartyLooting,
    0x16: ShowAuctionHallInfo,
    0x74: ShowBaseAttributeCancelWindow,
    0x14: ShowCastleInfo,
    0x24: ShowCropInfo,
    0x2b: ShowCropSetting,
    0x90: ShowDominionRegistry,
    0x15: ShowFortressInfo,
    0x7d: ShowFortressMapInfo,
    0x17: ShowFortressSiegeInfo,
    0x25: ShowManorDefaultInfo,
    0x93: ShowOwnedThingsPosition,
    0x78: ShowProcureCropDetail,
    0x20: EmptyPacketHandler, // ExShowQuestInfo
    0x21: ShowQuestMark,
    0xaa: ShowReceivedPostList,
    0x23: ShowSeedInfo,
    0xa1: ShowSeedMapInfo,
    0x26: ShowSeedSetting,
    0x2c: ShowSellCropList,
    0xac: ShowSentPostList,
    0x52: ShowVariationCancelWindow,
    0x51: ShowVariationMakeWindow,
    0x5d: SpawnEmitter,
    0x99: StartScenePlayer,
    0x66: StopMoveAirShip,
    0x76: SubPledgeSkillAdd,
    0x4a: UseSharedGroupItem,
    0x58: RefineCancelResult,
    0x56: RefineResult,
    0xa0: VitalityPointInfo,
    0x3c: PledgePowerGradeList,
    0x3d: PledgePowerInfo,
    0x3e: PledgeMemberInfo,
    0x3f: PledgeWarList,
    0x40: SubPledgeInfo,
    0x3a: PledgeSkillList,
    0x3b: PledgeSkillAdd,
    0x11: ServerPrimitive,
    0x7B: OlympiadPlayerBuffs,
    0xC5: QuestNpcLog
}