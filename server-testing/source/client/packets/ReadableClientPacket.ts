const enum PacketWritableChunks {
    C = 1,
    D = 4,
    F = 8,
    H = 2,
    Q = 8
}

export class ReadableClientPacket {
    currentBuffer: Buffer
    offset: number

    constructor( buffer, startOffset = 1 ) {
        this.currentBuffer = buffer
        this.offset = startOffset
    }

    readC() : number {
        let value = this.currentBuffer.readUInt8( this.offset )
        this.offset += PacketWritableChunks.C

        return value
    }

    readH() : number {
        let value = this.currentBuffer.readUInt16LE( this.offset )
        this.offset += PacketWritableChunks.H

        return value
    }

    readD() : number {
        let value = this.currentBuffer.readInt32LE( this.offset )
        this.offset += PacketWritableChunks.D

        return value
    }

    readF() : number {
        let value = this.currentBuffer.readDoubleLE( this.offset )
        this.offset += PacketWritableChunks.F

        return value
    }

    readB( length : number ) : Buffer {
        let value = this.currentBuffer.subarray( this.offset, this.offset + length )
        this.offset += length

        return preserveData( value )
    }

    readS() : string {
        let currentIndex

        for ( currentIndex = this.offset; currentIndex < this.currentBuffer.length; currentIndex += 2 ) {
            if ( this.currentBuffer.readUInt16LE( currentIndex ) === 0x00 ) {
                break
            }
        }

        let value = this.currentBuffer.toString( 'ucs2', this.offset, currentIndex )
        this.offset = currentIndex + 2

        return value
    }

    readSLimit( size: number ) : string {
        let currentIndex

        for ( currentIndex = this.offset; currentIndex < this.currentBuffer.length; currentIndex += 2 ) {
            if ( this.currentBuffer.readUInt16LE( currentIndex ) === 0x00 ) {
                break
            }
        }

        let readLimit : number = Math.min( this.offset + ( size * 2 ), currentIndex )
        let value = this.currentBuffer.toString( 'ucs2', this.offset, readLimit )

        this.offset = currentIndex + 2

        return value
    }

    readQ() : number {
        let value = this.currentBuffer.readBigInt64LE( this.offset )
        this.offset += PacketWritableChunks.Q

        return Number( value )
    }

    skipD( amount : number = 1 ) : void {
        this.offset += amount * PacketWritableChunks.D
    }

    skipS() : void {
        let currentIndex

        for ( currentIndex = this.offset; currentIndex < this.currentBuffer.length; currentIndex += 2 ) {
            if ( this.currentBuffer.readUInt16LE( currentIndex ) === 0x00 ) {
                break
            }
        }

        this.offset = currentIndex + 2
    }

    skipB( amount : number ) : void {
        this.offset += amount
    }

    skipF( amount : number = 1 ) : void {
        this.offset += amount * PacketWritableChunks.F
    }

    skipH( amount : number = 1 ) : void {
        this.offset += amount * PacketWritableChunks.H
    }

    hasMoreData() {
        return this.offset < ( this.currentBuffer.length - 1 )
    }
}

export function preserveData( dynamicPacket : Buffer ) : Buffer {
    let data : Buffer = Buffer.allocUnsafeSlow( dynamicPacket.length )
    dynamicPacket.copy( data )

    return data
}