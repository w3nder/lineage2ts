import { ReadableClientPacket } from './ReadableClientPacket'

type PacketPossibleValue = number | boolean | string | Buffer
type PacketObjectValue = { [ name: string ] : any } // any, because TS is dumb to trace object values vs interfaces
export type PacketEventValue = PacketPossibleValue | Array<PacketPossibleValue> | PacketObjectValue
export type PacketEvent = { [ name: string ] : PacketEventValue }
export type PacketHandlingMethod = ( packet: ReadableClientPacket ) => PacketEvent
export type PacketMethodMap = Record<number, PacketHandlingMethod>
export type PacketListenerMethod = ( event: PacketEvent ) => void
export type PacketListenerMap = Record<string, Array<PacketListenerMethod>>

/*
    Utility method for packets that have no need of implementation, even if server might send them.
 */
export const EmptyPacketHandler = ( () => {} ) as unknown as PacketHandlingMethod
