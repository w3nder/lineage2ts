import { L2Object } from './L2Object'
import { FenceState } from '../enums/FenceState'

export interface L2Fence extends L2Object {
    width: number
    height: number
    state: FenceState
}