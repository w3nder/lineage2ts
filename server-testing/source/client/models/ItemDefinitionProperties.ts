export interface ItemDefinitionProperties {
    itemId: number
    amount: number
}