export interface MovingProperties {
    destinationX: number
    destinationY: number
    destinationZ: number
    heading: number
}