export interface PrivateStoreManageAvailable {
    referencePrice: number
    itemObjectId: number
}

export interface PrivateStoreManageItem extends PrivateStoreManageAvailable {
    price: number
}

export interface RecipeShopManageItem {
    recipeId: number
    adenaCost: number
}