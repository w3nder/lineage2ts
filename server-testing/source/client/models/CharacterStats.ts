export interface CharacterStats {
    STR: number
    DEX: number
    CON: number
    INT: number
    WIT: number
    MEN: number
}

export const CharacterStatOrder : Array<keyof CharacterStats> = [
    'INT',
    'STR',
    'CON',
    'MEN',
    'DEX',
    'WIT'
]