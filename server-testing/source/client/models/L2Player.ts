import { Race } from '../enums/Race'
import { ClassId } from '../enums/ClassId'
import { PlayerPvpFlag } from '../enums/PlayerPvpFlag'
import { MountType } from '../enums/MountType'
import { PlayerStoreType } from '../enums/PlayerStoreType'
import { PlayerSex } from '../enums/PlayerSex'
import { ClanPrivilege } from '../enums/ClanPrivileges'
import { L2Character } from './L2Character'
import { CharacterStats } from './CharacterStats'
import { PlayerPointProperties } from './PointProperties'

export type PaperdollToItemId = { [ slot: number ]: number } // slot to itemId mapping
export type PaperdollToObjectId = { [ slot: number ]: number } // slot to objectId mapping
export interface L2Player extends L2Character, CharacterStats, PlayerPointProperties {
    targetId: number
    paperdollItemId: PaperdollToItemId
    paperdollObjectId: PaperdollToObjectId
    race: Race
    baseClassId: ClassId
    karma: number
    pvpFlag: PlayerPvpFlag
    powerAttack: number
    magicAttack: number
    magicAttackSpeed: number
    powerAttackSpeed: number
    runSpeed: number
    walkSpeed: number
    mountType: MountType
    mountNpcId: number
    storeType: PlayerStoreType
    isVisible: boolean
    sex: PlayerSex
    exp: number
    sp: number
    weightInventory: number
    weightLimit: number
    powerDefence: number
    magicDefence: number
    crit: number
    accuracy: number
    evasionRate: number
    isGM: boolean
    canCrystallizeItems: boolean
    pkKills: number
    pvpKills: number
    clanPrivileges: ClanPrivilege
}