import { L2SummonType } from '../enums/SummonType'
import { L2Character } from './L2Character'
import { PointProperties } from './PointProperties'
import { PlayerPvpFlag } from '../enums/PlayerPvpFlag'

export interface L2Summon extends L2Character, PointProperties {
    summonType: L2SummonType
    ownerObjectId: number
    karma: number
    feed: number
    maxFeed: number
    pvpFlag: PlayerPvpFlag
    sp: number
    exp: number
}