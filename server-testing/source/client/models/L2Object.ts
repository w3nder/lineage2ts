import { L2ObjectType } from '../enums/L2ObjectType'
import { LocationProperties } from './LocationProperties'
import { ISpatialIndexData } from './SpatialIndex'

export interface L2Object extends LocationProperties {
    objectId: number
    type: L2ObjectType
    spatialIndex: ISpatialIndexData
}