import { L2Object } from './L2Object'
import { ItemType } from '../enums/ItemType'
import { ItemState } from '../enums/ItemState'
import { ItemDefinitionProperties } from './ItemDefinitionProperties'

export interface L2Item extends L2Object, ItemDefinitionProperties {
    itemId: number
    amount: number
    enchantLevel: number
    itemType: ItemType
    isEquipped: boolean
    isStackable: boolean
    state: ItemState
    augmentationId: number
    ownerObjectId: number
    isQuestItem: boolean
    price: number
    equipMask: number
    mana: number
    expirationTime: number
}