export const enum L2ObjectType {
    None,
    Item,
    Npc,
    Player,
    Summon,
    Vehicle,
    Door,
    Airship,
    Fence
}