export const enum L2ClientState {
    None,
    Connecting,
    SelectingServer,
    SelectingCharacter,
    Playing,
    Disconnected
}