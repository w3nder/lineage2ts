export const enum PlayerFace {
    A,
    B,
    C,
}

export const AvailableFaces : Array<PlayerFace> = [
    PlayerFace.A,
    PlayerFace.B,
    PlayerFace.C,
]