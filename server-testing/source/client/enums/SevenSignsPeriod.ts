export const enum SevenSignsPeriod {
    Recruiting = 0,
    Competing = 1,
    CompetitionResults = 2,
    SealValidation = 3,
}