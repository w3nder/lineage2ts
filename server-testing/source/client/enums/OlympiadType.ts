export const enum OlympiadType {
    Teams = -1,
    Normal = 0,
    NonClassed = 1,
    Classed = 2
}