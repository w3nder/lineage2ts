export const enum AttackEffect {
    Soulshot = 0x10,
    Critical = 0x20,
    ShieldBlock = 0x40,
    Miss = 0x80,
}