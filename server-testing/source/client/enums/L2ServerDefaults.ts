import { L2LoginParameters } from '../L2LoginClient'

export const L2ServerDefaults = {
    LoginServerPort: 2106,
    ServerId: 1,
    LocalhostIpAddress: '127.0.0.1'
}

export const DefaultLoginParameters : L2LoginParameters = {
    host: L2ServerDefaults.LocalhostIpAddress,
    password: 'admin',
    port: L2ServerDefaults.LoginServerPort,
    serverId: L2ServerDefaults.ServerId,
    userName: 'admin'
}