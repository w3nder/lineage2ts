export enum StatusUpdateProperty {
    Level = 0x01,
    Exp = 0x02,
    STR = 0x03,
    DEX = 0x04,
    CON = 0x05,
    INT = 0x06,
    WIT = 0x07,
    MEN = 0x08,

    CurrentHP = 0x09,
    MaxHP = 0x0a,
    CurrentMP = 0x0b,
    MaxMP = 0x0c,

    SP = 0x0d,
    InventoryWeight = 0x0e,
    MaxInventoryWeight = 0x0f,

    PowerAttack = 0x11,
    AttackSpeed = 0x12,
    PowerDefence = 0x13,
    Evasion = 0x14,
    Accuracy = 0X15,
    Critical = 0X16,
    MagicAttack = 0x17,
    CastSpeed = 0x18,
    MagicDefence = 0x19,
    PvpFlagged = 0x1a,
    Karma = 0x1b,

    CurrentCP = 0x21,
    MaxCP = 0x22,
}