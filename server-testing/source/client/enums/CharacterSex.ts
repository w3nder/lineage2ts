export enum CharacterSex {
    Male,
    Female,
    Other,
}