export const enum FenceState {
    Hidden,
    OpenBorders,
    ClosedBorders
}