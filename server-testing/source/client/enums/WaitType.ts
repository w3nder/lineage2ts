export const enum WaitType {
    Sitting,
    Standing,
    StartingFakeDeath,
    StoppingFakeDeath,
}