export const enum PlayerHairStyle {
    A,
    B,
    C,
    D,
    E,
}

export const enum PlayerHairColor {
    A,
    B,
    C,
    D,
}

export const AvailableHairStyles: Array<PlayerHairStyle> = [
    PlayerHairStyle.A,
    PlayerHairStyle.B,
    PlayerHairStyle.C,
    PlayerHairStyle.D,
    PlayerHairStyle.E,
]

export const AvailableHairColors: Array<PlayerHairColor> = [
    PlayerHairColor.A,
    PlayerHairColor.B,
    PlayerHairColor.C,
    PlayerHairColor.D,
]