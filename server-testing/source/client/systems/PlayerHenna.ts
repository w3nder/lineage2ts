import { IGameSystem } from './IGameSystem'
import { PacketListenerMethod } from '../packets/PacketMethodTypes'
import { HennaInfo, HennaInfoEvent } from '../packets/game/receive/HennaInfo'
import { CharacterStats } from '../models/CharacterStats'

export class PlayerHenna implements IGameSystem {
    henna: CharacterStats
    dyeIds: Array<number>

    listeners: Record<string, PacketListenerMethod> = {
        [ HennaInfo.name ]: this.onHennaInfo.bind( this ),
    }

    onHennaInfo( data: HennaInfoEvent ) : void {
        this.henna = data.stats
        this.dyeIds = data.dyeIds
    }

    getHennaStat( stat: keyof CharacterStats ) : number {
        if ( !this.henna ) {
            return 0
        }

        return this.henna[ stat ]
    }

    hasDye( value: number ) : boolean {
        return this.dyeIds?.includes( value )
    }
}