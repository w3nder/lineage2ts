import { IGameSystem } from './IGameSystem'
import { PacketListenerMethod } from '../packets/PacketMethodTypes'
import { L2SummonType } from '../enums/SummonType'
import { PetStatusInfo, PetStatusInfoEvent } from '../packets/game/receive/PetStatusInfo'
import { PetStatusUpdate } from '../packets/game/receive/PetStatusUpdate'

export class PetInfo implements IGameSystem {
    summonType: L2SummonType
    listeners: Record<string, PacketListenerMethod> = {
        [ PetStatusInfo.name ]: this.onPetStatusInfo.bind( this ),
        [ PetStatusUpdate.name ]: null
    }

    onPetStatusInfo( data: PetStatusInfoEvent ) : void {
        this.summonType = data.summonType
    }
}