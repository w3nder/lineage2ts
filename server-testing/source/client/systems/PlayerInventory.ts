import { IGameSystem } from './IGameSystem'
import { PacketListenerMethod } from '../packets/PacketMethodTypes'
import { ItemList, ItemListEvent } from '../packets/game/receive/ItemList'
import { L2World } from '../L2World'
import { L2Item } from '../models/L2Item'
import { InventoryUpdate, InventoryUpdateEvent } from '../packets/game/receive/InventoryUpdate'
import { ItemState } from '../enums/ItemState'
import { BaseInventory } from './base/BaseInventory'
import { QuestItemList, QuestItemListEvent } from '../packets/game/receive/QuestItemList'
import { L2GameClient } from '../L2GameClient'
import { RequestDestroyItem } from '../packets/game/send/RequestDestroyItem'
import { RequestDropItem } from '../packets/game/send/RequestDropItem'
import { L2ItemIds } from '../../interactions/enums/Item'
import { RequestAutoSoulShot } from '../packets/game/send/RequestAutoSoulShot'

const shotItemIds : Array<number> = [
    L2ItemIds.SoulshotNG,
    L2ItemIds.SoulshotBeast,
    L2ItemIds.SoulshotD,
    L2ItemIds.SoulshotC,
    L2ItemIds.SoulshotB,
    L2ItemIds.SoulshotA,
    L2ItemIds.SoulshotS,

    L2ItemIds.BlessedSpiritshotNG,
    L2ItemIds.BlessedSpiritshotD,
    L2ItemIds.BlessedSpiritshotC,
    L2ItemIds.BlessedSpiritshotB,
    L2ItemIds.BlessedSpiritshotA,
    L2ItemIds.BlessedSpiritshotS,
]

export class PlayerInventory extends BaseInventory implements IGameSystem {
    listeners: Record<string, PacketListenerMethod> = {
        [ ItemList.name ]: this.onItemList.bind( this ),
        [ InventoryUpdate.name ]: this.onInventoryUpdate.bind( this ),
        [ QuestItemList.name ]: this.onQuestItems.bind( this )
    }

    onInventoryWindowCallback: () => void

    constructor( playerObjectId: number, client: L2GameClient, onInventoryWindow : () => void | null = null ) {
        super( playerObjectId, client )
        this.onInventoryWindowCallback = onInventoryWindow
    }

    private onItemList( data: ItemListEvent ) : void {
        this.allItems.removeAll()

        data.itemObjectIds.forEach( this.addItemByObjectId.bind( this ) )

        if ( data.showInventoryWindow && this.onInventoryWindowCallback ) {
            this.onInventoryWindowCallback()
        }
    }

    private onInventoryUpdate( data: InventoryUpdateEvent ) : void {
        /*
            While added or removed item objectIds need to be handled for
            inventory operations, modified items get their properties
            changed when reading packet. Hence, no additional inventory
            operation is needed to proceed further.
         */
        data.addedObjectIds.forEach( this.addItemByObjectId.bind( this ) )
        data.removedObjectIds.forEach( ( objectId: number ) => {
            this.allItems.removeByKey( objectId )

            let item = L2World.getObject( objectId ) as L2Item
            if ( !item ) {
                return
            }

            if ( item.state !== ItemState.Dropped ) {
                L2World.deleteObject( objectId )
            }
        } )
    }

    private onQuestItems( data: QuestItemListEvent ) : void {
        data.itemObjectIds.forEach( this.addItemByObjectId.bind( this ) )
    }

    destroyAllItemsById( itemOrObjectId: number ) : void {
        let items = this.getItems( itemOrObjectId )

        items.forEach( ( item: L2Item ) => {
            this.client.sendPacket( RequestDestroyItem( item.objectId, item.amount ) )
        } )
    }

    /*
        While we can request to drop item from server, server will send us update
        for inventory if item has indeed been dropped. Hence, we must not modify inventory
        at this step.
     */
    dropItem( itemOrObjectId: number, x: number, y: number, z: number ) : Array<number> {
        return this.getItems( itemOrObjectId ).map( ( item: L2Item ) : number => {
            this.client.sendPacket( RequestDropItem( item.objectId, item.amount, x, y, z ) )

            return item.objectId
        } )
    }

    getAvaiableShotsItemId() : number {
        return shotItemIds.find( ( itemId: number ) : boolean => this.hasItem( itemId ) )
    }

    setAutoShots( itemId: number, isEnabled : boolean = true ) : void {
        return this.client.sendPacket( RequestAutoSoulShot( itemId, isEnabled ) )
    }
}