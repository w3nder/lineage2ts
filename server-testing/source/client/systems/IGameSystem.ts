import { PacketListenerMethod } from '../packets/PacketMethodTypes'

export interface IGameSystem {
    listeners: Record<string, PacketListenerMethod>
}