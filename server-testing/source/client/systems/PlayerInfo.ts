import { IGameSystem } from './IGameSystem'
import { PacketListenerMethod } from '../packets/PacketMethodTypes'
import { VitalityPointInfo, VitalityPointInfoEvent } from '../packets/game/receive/VitalityPointInfo'
import { FriendData, FriendList, FriendListEvent } from '../packets/game/receive/FriendList'
import { EtcStatusUpdate, EtcStatusUpdateEvent } from '../packets/game/receive/EtcStatusUpdate'
import { UISettings, UISettingsEvent } from '../packets/game/receive/UISettings'
import { NevitAdventPointInfo, NevitAdventPointInfoEvent } from '../packets/game/receive/NevitAdventPointInfo'
import { UserInfo } from '../packets/game/receive/UserInfo'
import { ExtraUserInfo, ExtraUserInfoEvent } from '../packets/game/receive/ExtraUserInfo'
import { StatusUpdate } from '../packets/game/receive/StatusUpdate'
import { VoteSystemInfo, VoteSystemInfoEvent } from '../packets/game/receive/VoteSystemInfo'
import { QuestList, QuestListEvent } from '../packets/game/receive/QuestList'
import { NotifyDimensionalItem } from '../packets/game/receive/NotifyDimensionalItem'
import { AbnormalStatusUpdate, AbnormalStatusUpdateEvent } from '../packets/game/receive/AbnormalStatusUpdate'
import _ from 'lodash'

export class PlayerInfo implements IGameSystem {
    vitalityPoints: number = 0
    friends: Array<FriendData>
    status: EtcStatusUpdateEvent
    ui: UISettingsEvent
    nevitPoints: number = 0
    recommendation: VoteSystemInfoEvent
    quests: QuestListEvent
    hasDimentionalItem: boolean = false
    effect: ExtraUserInfoEvent
    statusEffects: AbnormalStatusUpdateEvent
    statusUpdateTime: number = 0

    /*
        Not all packets can have listener methods, since
        some packets like UserInfo store player data elsewhere.
     */
    listeners: Record<string, PacketListenerMethod> = {
        [ UserInfo.name ]: null,
        [ ExtraUserInfo.name ]: this.onExtraUserInfo.bind( this ),
        [ StatusUpdate.name ]: this.onStatusUpdate.bind( this ),
        [ VitalityPointInfo.name ]: this.onVitalityPoints.bind( this ),
        [ FriendList.name ]: this.onFriendList.bind( this ),
        [ EtcStatusUpdate.name ]: this.onEtcStatusUpdate.bind( this ),
        [ UISettings.name ]: this.onUISettings.bind( this ),
        [ NevitAdventPointInfo.name ]: this.onNevitPoints.bind( this ),
        [ VoteSystemInfo.name ]: this.onVoteInfo.bind( this ),
        [ QuestList.name ]: this.onQuestList.bind( this ),
        [ NotifyDimensionalItem.name ]: this.onNotifyDimentionalItem.bind( this ),
        [ AbnormalStatusUpdate.name ]: this.onAbnormalStatusUpdate.bind( this )
    }

    onVitalityPoints( data: VitalityPointInfoEvent ): void {
        this.vitalityPoints = data.points
    }

    onFriendList( data: FriendListEvent ): void {
        this.friends = data.friends
    }

    onEtcStatusUpdate( data: EtcStatusUpdateEvent ): void {
        this.status = data
    }

    onUISettings( data: UISettingsEvent ): void {
        this.ui = data
    }

    onNevitPoints( data: NevitAdventPointInfoEvent ): void {
        this.nevitPoints = data.points
    }

    onVoteInfo( data: VoteSystemInfoEvent ): void {
        this.recommendation = data
    }

    onQuestList( data: QuestListEvent ): void {
        this.quests = data
    }

    onNotifyDimentionalItem(): void {
        this.hasDimentionalItem = true
    }

    onExtraUserInfo( data: ExtraUserInfoEvent ): void {
        this.effect = data
    }

    onAbnormalStatusUpdate( data: AbnormalStatusUpdateEvent ) : void {
        this.statusEffects = data
    }

    onStatusUpdate() : void {
        this.statusUpdateTime = Date.now()
    }
}