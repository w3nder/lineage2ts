import * as net from 'net'
import { Socket } from 'net'
import { EncryptionOperations } from './security/EncryptionOperations'
import { PacketEvent, PacketHandlingMethod, PacketListenerMap, PacketListenerMethod, PacketMethodMap } from './packets/PacketMethodTypes'
import { ReadableClientPacket } from './packets/ReadableClientPacket'
import _ from 'lodash'
import logSymbols from 'log-symbols'
import pkgDir from 'pkg-dir'
import dotenv from 'dotenv'
import { L2ClientPacketProcessor } from './types/PacketProcessor'

const rootDirectory = pkgDir.sync( __dirname )
dotenv.config( { path: `${ rootDirectory }/.env` } )

export interface L2ConnectionParameters {
    port: number
    host: string
}

export interface L2ClientError {
    message: string
    id: string
}

export const enum L2ClientState {
    Initialized,
    Connected,
    Disconnected
}

export type L2BaseClientCallback = ( error: L2ClientError, data?: PacketEvent ) => void

export abstract class L2BaseClient<ClientProperties extends L2ConnectionParameters> {
    connection: Socket
    parameters: ClientProperties
    callback: L2BaseClientCallback
    abstract encryption: EncryptionOperations
    abstract packetMap: PacketMethodMap
    listenerMap: PacketListenerMap = {}
    terminateConnection: boolean = false
    state: L2ClientState = L2ClientState.Initialized

    packetTimes: Record<string, number> = {}
    packetEvents: Record<string, PacketEvent> = {}
    packetProcessors: Set<L2ClientPacketProcessor> = new Set<L2ClientPacketProcessor>()

    constructor( connectionParameters : ClientProperties, callback: L2BaseClientCallback ) {
        this.parameters = connectionParameters
        this.callback = callback

        this.createConnection()

        this.connection.on( 'data', this.onConnectionData.bind( this ) )
        this.connection.on( 'end', this.onConnectionEnd.bind( this ) )
        this.connection.on( 'error', this.onConnectionError.bind( this ) )
    }

    createConnection() {
        this.connection = net.createConnection( {
            timeout: 8000,
            port: this.parameters.port,
            host: this.parameters.host
        }, this.onConnectionStart.bind( this ) )
    }

    onConnectionStart() : void {
        this.state = L2ClientState.Connected
    }

    onConnectionData( data: Buffer ): void {
        let packetSizeIndex = 0

        while ( packetSizeIndex < data.length ) {
            let packetSize = data.readInt16LE( packetSizeIndex )
            let packetData = data.subarray( packetSizeIndex + 2, packetSizeIndex + packetSize )
            packetSizeIndex += packetSize

            let decryptedData : Buffer

            try {
                decryptedData = this.encryption.decrypt( packetData )

                let method : PacketHandlingMethod = this.getPacketMethod( decryptedData )

                if ( !method ) {
                    console.log( `Unknown packet for header = 0x${ this.getPacketSignature( decryptedData ).toString( 16 ) }` )
                    continue
                }

                let offset = this.getPacketOffset( decryptedData.readUInt8( 0 ) )
                let event : PacketEvent = method( new ReadableClientPacket( decryptedData, offset ) )

                if ( this.listenerMap[ method.name ] ) {
                    this.listenerMap[ method.name ].map( ( listener: PacketListenerMethod ) => {
                        if ( listener ) {
                            listener( event )
                        }
                    } )
                }

                this.updatePacketTracking( method.name, event, decryptedData.subarray( offset ) )
            } catch ( error ) {
                // TODO : unify logging
                console.log( logSymbols.error, 'Error processing packet signature', this.getPacketSignature( decryptedData ).toString( 16 ) )
                console.log( logSymbols.error, 'Message:', error.message )
                console.trace( error )

                this.terminateConnection = true
            }

            /*
                Check for connection termination is placed separately to allow certain packet handlers to
                terminate connection based on input data. In other words, placing connection abort on just try/catch error scenario
                is insufficient.
             */
            if ( this.terminateConnection ) {
                return this.abortConnection()
            }
        }
    }

    /**
     * See more about Nodejs connection errors at
     * https://betterstack.com/community/guides/scaling-nodejs/nodejs-errors/
     */
    onConnectionError( error : { code: string } & Error ) : void {
        switch ( error.code ) {
            case 'ECONNRESET':
                return this.callback( {
                    message: `Server connection closed for ${this.parameters.host}:${this.parameters.port}`,
                    id: '5c174d7e-59c8-462f-8539-ecf3d15aa87e'
                } )

            case 'ENOTFOUND':
                return this.callback( {
                    message: `Unable to resolve server DNS/IP address for ${this.parameters.host}`,
                    id: 'de36ad38-d399-4d32-a96f-134a913c2df5'
                } )

            case 'ECONNREFUSED':
                return this.callback( {
                    message: `Server is refusing connection for ${this.parameters.host}:${this.parameters.port}`,
                    id: 'afe0e02c-998e-4e6d-a518-7a94bddf2d80'
                } )

            case 'ECONNABORTED':
                return this.callback( {
                    message: `Server forcibly terminated connection for ${this.parameters.host}:${this.parameters.port}`,
                    id: 'b1bc6607-41fe-44a0-a6ab-14d217839846'
                } )

            case 'EHOSTUNREACH':
                return this.callback( {
                    message: `Connection failed due to firewall or gateway at ${this.parameters.host}:${this.parameters.port}`,
                    id: '3310b95b-89d6-4421-bdde-b56b18a90f9e'
                } )
        }

        this.callback( {
            message: error.message,
            id: '7c5315ca-95c0-4d07-b830-0775b7f7471b'
        } )
    }

    getPacketMethod( packetData: Buffer ) : PacketHandlingMethod {
        return this.packetMap[ this.getPacketSignature( packetData ) ]
    }

    getPacketSignature( packetData: Buffer ) : number {
        return packetData.readUInt8( 0 )
    }

    abstract onConnectionEnd() : void


    abortConnection() {
        if ( this.connection.destroyed ) {
            return
        }

        this.connection.end()
        this.connection.destroy()
    }

    sendPacket( packet: Buffer ) {
        this.connection.write( this.encryption.encrypt( packet ) )
    }

    addPacketListeners( listeners: Record<string, PacketListenerMethod> ) : void {
        this.listenerMap = _.reduce( listeners, ( map: PacketListenerMap, packetListener: PacketListenerMethod, name: string ) : PacketListenerMap => {
            if ( !map[ name ] ) {
                map[ name ] = []
            }

            map[ name ].push( packetListener )

            return map
        }, this.listenerMap )
    }

    removePacketListeners( listeners: Record<string, PacketListenerMethod> ) : void {
        this.listenerMap = _.reduce( listeners, ( map: PacketListenerMap, packetListener: PacketListenerMethod, name: string ) : PacketListenerMap => {
            if ( map[ name ] ) {
                _.pull( map[ name ], packetListener )
            }

            return map
        }, this.listenerMap )
    }

    getPacketOffset( value: number ) : number {
        return 1
    }

    /*
        Used to record last update time when packet has been sent
        or determine if packet has indeed been processed.
     */
    updatePacketTracking( name: string, data: PacketEvent, rawPacket: Buffer ) : void {
        this.packetTimes[ name ] = Date.now()
        this.packetEvents[ name ] = data

        if ( this.packetProcessors.size > 0 ) {
            this.packetProcessors.forEach( ( processor: L2ClientPacketProcessor ) => processor( name, data, rawPacket ) )
        }
    }

    addPacketProcessor( processor: L2ClientPacketProcessor ) : void {
        this.packetProcessors.add( processor )
    }

    removePacketProcessor( processor: L2ClientPacketProcessor ) : void {
        this.packetProcessors.delete( processor )
    }
}