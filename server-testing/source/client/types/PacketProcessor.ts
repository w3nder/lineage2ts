import { PacketEvent } from '../packets/PacketMethodTypes'

export type L2ClientPacketProcessor = ( packetName: string, data: PacketEvent, rawPacket: Readonly<Buffer> ) => void
