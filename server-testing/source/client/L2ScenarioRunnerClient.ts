import { L2ClientLogic } from './L2ClientLogic'
import { L2LoginParameters } from './L2LoginClient'
import { PacketEvent, PacketListenerMethod } from './packets/PacketMethodTypes'
import { LeaveWorld } from './packets/game/receive/LeaveWorld'
import { ServerClose } from './packets/game/receive/ServerClose'
import { TeleportToLocation } from './packets/game/receive/TeleportToLocation'
import { CurrentPlayerTarget, CurrentPlayerTargetEvent } from './packets/game/receive/CurrentPlayerTarget'
import { TargetSelectedWithPosition } from './packets/game/receive/TargetSelectedWithPosition'
import { TargetUnselectedWithPosition } from './packets/game/receive/TargetUnselectedWithPosition'
import { PickItemUp } from './packets/game/receive/PickItemUp'
import { DropItem, DropItemEvent } from './packets/game/receive/DropItem'
import { NpcInfo, NpcInfoEvent } from './packets/game/receive/NpcInfo'
import { ServerObjectInfo, ServerObjectInfoEvent } from './packets/game/receive/ServerObjectInfo'
import { PetInfo } from './systems/PetInfo'
import { PlayerInfo } from './systems/PlayerInfo'
import { SpawnItem, SpawnItemEvent } from './packets/game/receive/SpawnItem'
import { DeleteObject, DeleteObjectEvent } from './packets/game/receive/DeleteObject'
import { StaticObject, StaticObjectEvent } from './packets/game/receive/StaticObject'
import { ValidateLocation, ValidateLocationEvent } from './packets/game/receive/ValidateLocation'
import { CharacterSelectionInfo } from './packets/game/receive/CharacterSelectionInfo'
import { CharacterCreationFailure } from './packets/game/receive/CharacterCreationFailure'
import { CharacterCreationSuccess } from './packets/game/receive/CharacterCreationSuccess'
import { CharacterDeleteFail } from './packets/game/receive/CharacterDeleteFail'
import { CharacterDeleteSuccess } from './packets/game/receive/CharacterDeleteSuccess'
import { GameCharacterSelectedEvent } from './packets/game/receive/CharacterSelected'
import { PlayerInventory } from './systems/PlayerInventory'
import { PetInventory } from './systems/PetInventory'
import { RequestKeyMapping } from './packets/game/send/RequestKeyMapping'
import { RequestManorList } from './packets/game/send/RequestManorList'
import { RequestEnterWorld } from './packets/game/send/RequestEnterWorld'
import { UserInfo } from './packets/game/receive/UserInfo'
import { PlayerHenna } from './systems/PlayerHenna'
import { ChatMessages } from './systems/ChatMessages'
import { L2Player } from './models/L2Player'
import { L2World } from './L2World'
import { PetInfoEvent } from './packets/game/receive/PetInfo'
import { PlayerInfoEvent } from './packets/game/receive/PlayerInfo'
import { L2ObjectType } from './enums/L2ObjectType'
import { L2GameAccount } from './L2GameClient'
import { PacketRecorder } from './models/PacketRecorder'

// TODO : reconcile functionality with L2FullClient both have duplicated code
export class L2ScenarioRunnerClient extends L2ClientLogic {
    loginProperties: L2LoginParameters
    hasClientExited: boolean = false
    isEnteredWorld: boolean = false
    characterName: string

    inventory: PlayerInventory = null
    petInventory: PetInventory = null
    henna: PlayerHenna = new PlayerHenna()
    info: PlayerInfo = new PlayerInfo()
    petInfo: PetInfo = new PetInfo()
    chat: ChatMessages = new ChatMessages()
    packetRecorder : PacketRecorder = new PacketRecorder()

    allPackets : Record<string, PacketListenerMethod> = {
        [ LeaveWorld.name ]: this.onLeaveWorld.bind( this ),
        [ ServerClose.name ]: this.onServerClose.bind( this ),
        [ TeleportToLocation.name ]: null,
        [ CurrentPlayerTarget.name ]: this.onPlayerSetTarget.bind( this ),
        [ TargetSelectedWithPosition.name ]: null,
        [ TargetUnselectedWithPosition.name ]: null,
        [ PickItemUp.name ]: null,
        [ DropItem.name ]: this.addSpatialIndex.bind( this ),
        [ NpcInfo.name ]: this.addSpatialIndex.bind( this ),
        [ ServerObjectInfo.name ]: this.addSpatialIndex.bind( this ),
        [ PetInfo.name ]: this.addSpatialIndex.bind( this ),
        [ PlayerInfo.name ]: this.addSpatialIndex.bind( this ),
        [ SpawnItem.name ]: this.onSpawnItem.bind( this ),
        [ DeleteObject.name ]: this.onDeleteObject.bind( this ),
        [ StaticObject.name ]: this.addSpatialIndex.bind( this ),
        [ ValidateLocation.name ]: this.addSpatialIndex.bind( this ),
        [ CharacterSelectionInfo.name ]: null,
        [ CharacterCreationFailure.name ]: null,
        [ CharacterCreationSuccess.name ]: null,
        [ CharacterDeleteFail.name ]: null,
        [ CharacterDeleteSuccess.name ]: null,

        ...this.info.listeners,
        ...this.petInfo.listeners,
        ...this.chat.listeners,
        ...this.henna.listeners,
    }

    worldEnteredListeners : Record<string, PacketListenerMethod> = {
        [ UserInfo.name ]: this.prepareWorldEntered.bind( this )
    }


    onReadyToEnterWorld( data: GameCharacterSelectedEvent ) {
        this.inventory = new PlayerInventory( data.objectId, this.gameClient )
        this.petInventory = new PetInventory( data.objectId, this.gameClient )

        this.gameClient.addPacketListeners( this.inventory.listeners )
        this.gameClient.addPacketListeners( this.petInventory.listeners )
        this.gameClient.addPacketListeners( this.getWorldEnteredListeners() )

        /*
            Current packet order is taken from observation of how L2 client
            sends packets to load game character world.
         */
        this.gameClient.sendPacket( RequestKeyMapping() )
        this.gameClient.sendPacket( RequestManorList() )
        this.gameClient.sendPacket( RequestEnterWorld() )
    }

    private getWorldEnteredListeners() : Record<string, PacketListenerMethod> {
        return this.worldEnteredListeners
    }

    private prepareWorldEntered() : void {
        this.gameClient.removePacketListeners( this.worldEnteredListeners )
        this.isEnteredWorld = true
    }

    constructor() {
        super( L2ScenarioRunnerClient.name )
    }

    getLoginProperties(): L2LoginParameters {
        return this.loginProperties
    }

    setLoginProperties( data: L2LoginParameters ) : void {
        this.loginProperties = data
    }

    getGamePacketListeners(): Record<string, PacketListenerMethod> {
        return this.allPackets
    }

    sendGamePacket( data: Buffer ) : void {
        this.gameClient.sendPacket( data )
    }

    getPacketData( name: string ) : PacketEvent {
        return this.gameClient.packetEvents[ name ]
    }

    onClientExit( isSuccess: boolean = true ) {
        this.hasClientExited = true
    }

    getPlayer() : L2Player {
        return L2World.getObject( this.gameClient.playerObjectId ) as L2Player
    }

    getPlayerId() : number {
        return this.gameClient.playerObjectId
    }

    onPlayerSetTarget( data: CurrentPlayerTargetEvent ) : void {
        let player = this.getPlayer()
        if ( !player ) {
            throw new Error( `Unable to set target of client's player=${this.getPlayerId()}` )
        }

        player.targetId = data.objectId
    }

    addSpatialIndex( data: NpcInfoEvent | ServerObjectInfoEvent | PetInfoEvent | PlayerInfoEvent | StaticObjectEvent | ValidateLocationEvent | DropItemEvent ) : void {
        let object = L2World.getObject( data.objectId )
        if ( !object ) {
            return
        }

        L2World.removeSpatialIndex( object )
        L2World.addSpatialIndex( object )
    }

    onLeaveWorld() : void {
        this.terminateClient( true, 'Client session ended' )
    }

    onServerClose() : void {
        this.terminateClient( false, 'Server aborted client session' )
    }

    /*
        Packet will provision entry to create data in L2World with object id.
        Sanity check is only needed to ensure that data has not been wiped before entering
        spatial grid.
     */
    onSpawnItem( data: SpawnItemEvent ) : void {
        let item = L2World.getObject( data.objectId )
        if ( !item ) {
            return
        }

        L2World.addSpatialIndex( item )
    }

    /*
        Deleting object does not mean that client should remove all the data associated with it.
        Primary purpose of such packet is to remove an object from view. In server, it is used
        to indicate that object will no longer be tracked or receive updates in future. However,
        since such packet is used in sequence when item is being picked up, care must be taken
        to preserve existing item data.
     */
    onDeleteObject( data : DeleteObjectEvent ) : void {
        let object = L2World.getObject( data.objectId )

        if ( !object ) {
            return
        }

        L2World.removeSpatialIndex( object )

        if ( object.type === L2ObjectType.Item ) {
            return
        }

        L2World.deleteObject( data.objectId )
    }

    getGameProperties() : L2GameAccount {
        return {
            characterName: this.characterName
        }
    }

    startGameClientPacketRecording() : void {
        this.gameClient.addPacketProcessor( this.packetRecorder.packetProcessor )
    }

    stopGameClientPacketRecording() : void {
        this.gameClient.removePacketProcessor( this.packetRecorder.packetProcessor )
    }
}

export const DefaultScenarioClient = new L2ScenarioRunnerClient()