import { L2BaseClient, L2BaseClientCallback, L2ClientError, L2ClientState, L2ConnectionParameters } from './L2BaseClient'
import { LoginEncryption } from './security/LoginEncryption'
import { LoginPacketMethods } from './packets/LoginPacketMethods'
import { PacketListenerMethod, PacketMethodMap } from './packets/PacketMethodTypes'
import { AccountKickedReason, LoginAccountKicked, LoginAccountKickedEvent } from './packets/login/receive/AccountKicked'
import { LoginGameGuardSuccess, LoginGameGuardSuccessEvent } from './packets/login/receive/GameGuardSuccess'
import { LoginInitialConfiguration, LoginInitialConfigurationEvent } from './packets/login/receive/InitialConfiguration'
import { LoginFail, LoginFailedEvent, LoginFailReason } from './packets/login/receive/LoginFail'
import { generateLoginSuccess, LoginSuccess, LoginSuccessEvent } from './packets/login/receive/LoginSuccess'
import { LoginServerDeniedEvent, ServerLoginDenied, ServerLoginDeniedReason } from './packets/login/receive/ServerLoginDenied'
import { LoginServerApprovedEvent, ServerLoginApproved } from './packets/login/receive/ServerLoginApproved'
import { GameServerDetails, LoginServerList, LoginServerListEvent } from './packets/login/receive/ServerList'
import { AuthenticateGameGuard } from './packets/login/send/AuthenticateGameGuard'
import { RequestAuthenticatedLogin } from './packets/login/send/RequestAuthenticatedLogin'
import { RequestServerList } from './packets/login/send/RequestServerList'
import { RequestServerLogin } from './packets/login/send/RequestServerLogin'
import _ from 'lodash'

export interface L2LoginParameters extends L2ConnectionParameters {
    serverId: number
    userName: string
    password: string
}

export class L2LoginClient extends L2BaseClient<L2LoginParameters> {
    encryption: LoginEncryption = new LoginEncryption()
    packetMap: PacketMethodMap = LoginPacketMethods

    initialConfig: LoginInitialConfigurationEvent
    loginConfig: LoginSuccessEvent
    gameServer: GameServerDetails

    constructor( properties : L2LoginParameters, callback: L2BaseClientCallback, clientListeners : Record<string, PacketListenerMethod> ) {
        super( properties, callback )

        if ( properties.userName.length > 16 ) {
            throw new Error( 'Username is too long, must be less than 16 characters.' )
        }

        if ( properties.password.length > 16 ) {
            throw new Error( 'Password is too long, must be less than 16 characters.' )
        }

        this.loginConfig = generateLoginSuccess()

        let listeners = _.assign( this.getDefaultFlowListeners(), clientListeners )
        this.addPacketListeners( listeners )
    }

    getDefaultFlowListeners() : Record<string, PacketListenerMethod> {
        return {
            [ LoginAccountKicked.name ]: this.onAccountKicked.bind( this ),
            [ LoginGameGuardSuccess.name ]: this.onGameGuardSuccess.bind( this ),
            [ LoginInitialConfiguration.name ]: this.onInitialConfiguration.bind( this ),
            [ LoginFail.name ]: this.onLoginFail.bind( this ),
            [ LoginSuccess.name ]: this.onLoginSuccess.bind( this ),
            [ ServerLoginDenied.name ]: this.onServerLoginDenied.bind( this ),
            [ ServerLoginApproved.name ]: this.onServerLoginApproved.bind( this ),
            [ LoginServerList.name ]: this.onServerList.bind( this )
        }
    }

    onConnectionEnd(): void {
        this.encryption = null

        let error = this.getConnectionEndError()
        if ( error ) {
            this.callback( error )
        }

        this.initialConfig = null
        this.loginConfig = null
    }

    getConnectionEndError() : L2ClientError {
        if ( this.state === L2ClientState.Disconnected ) {
            return
        }

        if ( this.terminateConnection ) {
            return
        }

        if ( !this.initialConfig || !this.loginConfig ) {
            return {
                message: 'Login server abruptly terminated connection',
                id: '9608d178-8071-45d2-aa5f-01a00fc8f854'
            }
        }

        return {
            message: 'Closing connection to login server',
            id: '13a44747-afce-4ed6-9bdc-6d1352cbbf6c'
        }
    }

    /*
        Step 1. When client opens connection, server sends welcome packet.
        - Receive encryption parameters and sessionId
        - Send Game Guard confirmation with sessionId
     */
    onInitialConfiguration( data : LoginInitialConfigurationEvent ) : void {
        this.initialConfig = data
        this.encryption.setKey( data.blowfishKey )

        this.sendPacket( AuthenticateGameGuard( data.sessionId ) )
    }

    /*
        Step 2. Once Game Guard authentication is sent, we can receive success packet
        - Receive and confirm sessionId
        - Send encrypted authentication credentials, verified by login server
     */
    onGameGuardSuccess( data : LoginGameGuardSuccessEvent ) : void {
        if ( this.initialConfig.sessionId !== data.sessionId ) {
            return this.callback( {
                message: 'Invalid sessionId from server!',
                id: '1d742af0-9bc8-4e08-b044-6c80b6b1a163'
            } )
        }

        this.sendPacket( RequestAuthenticatedLogin( this.parameters.userName, this.parameters.password, this.initialConfig.sessionId, this.initialConfig.publicKey ) )
    }

    /*
        Step 3-A. Client would show license agreement.
        - receive two session values
        - request server list using sent values
     */
    onLoginSuccess( data : LoginSuccessEvent ) : void {
        this.loginConfig = data
        this.sendPacket( RequestServerList( data.one, data.two ) )
    }

    /*
        Step 3-B or 4. Available servers are shown ( server can send server list instead of login success )
        - receive data about available servers
        - request to login into particular server by id
     */
    onServerList( data : LoginServerListEvent ) : void {
        if ( data.availableServers.length === 0 ) {
            this.terminateConnection = true
            return this.callback( {
                message: 'No available game servers',
                id: 'e403ec46-49c4-41b4-9771-7909225109fa'
            } )
        }

        let serverId = this.parameters.serverId ?? data.lastServerId
        let server = this.getValidGameServer( data.availableServers, serverId )
        if ( server ) {
            this.gameServer = server
            return this.sendPacket( RequestServerLogin( this.loginConfig.one, this.loginConfig.two, serverId ) )
        }

        this.terminateConnection = true
        this.callback( {
            message: 'No valid game server available',
            id: '9912614b-314a-492c-a1a5-016fac0c4f52'
        } )
    }

    /*
        Step 5. Server has approved client to join (it can reject due to various reasons such as server is full)
        - receive yet another pair of session values
        - close connection to login server
        - ready to open connection to game server
     */
    onServerLoginApproved( data : LoginServerApprovedEvent ) : void {
        this.state = L2ClientState.Disconnected
        this.callback( null, data )
    }

    onServerLoginDenied( data : LoginServerDeniedEvent ) : void {
        this.terminateConnection = true
        this.callback( {
            message: `Server denied login: ${ ServerLoginDeniedReason[ data.reason ] }`,
            id: 'df0d7b56-01fd-4911-b807-e13722c516f2'
        } )
    }

    onLoginFail( data : LoginFailedEvent ) : void {
        this.terminateConnection = true
        this.callback( {
            message: `Login failed : ${ LoginFailReason[ data.reason ]}`,
            id: '593c2a6f-4469-484d-a75f-e85d6a8ef8e7'
        } )
    }

    onAccountKicked( data : LoginAccountKickedEvent ) : void {
        this.terminateConnection = true
        this.callback( {
            message: `Account is not available due to : ${ AccountKickedReason[ data.reason ] }`,
            id: 'be98f47f-e9ad-48b9-a4db-3693b0bcd2f1'
        } )
    }

    getValidGameServer( servers: Array<GameServerDetails>, serverId: number ) : GameServerDetails {
        return servers.find( ( server: GameServerDetails ) : boolean => server.id === serverId )
    }
}