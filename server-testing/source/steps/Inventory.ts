import { Given, Then, When } from '@cucumber/cucumber'
import { DefaultScenarioClient } from '../client/L2ScenarioRunnerClient'
import { getItemIdFromName } from './helpers/inventoryOperations'
import _ from 'lodash'

Then( 'Inventory.Has item {string} with quantity {int}', ( itemName: string, quantity: number ) => {
    let itemId = getItemIdFromName( itemName )

    let amount = DefaultScenarioClient.inventory.getItemCount( itemId )
    if ( amount !== quantity ) {
        throw new Error( `Incorrect item quantity of ${amount} detected` )
    }
} )

Given( 'Inventory.Drop item {string} in radius {int} from player', async ( itemName: string, radius: number ) => {
    let itemId = getItemIdFromName( itemName )
    let player = DefaultScenarioClient.getPlayer()

    let x = player.x + _.random( -radius, radius )
    let y = player.y + _.random( -radius, radius )

    DefaultScenarioClient.inventory.dropItem( itemId, x, y, player.z )
} )

When( 'Inventory.Delete all {string}', ( itemName: string ) => {
    let itemId = getItemIdFromName( itemName )
    DefaultScenarioClient.inventory.destroyAllItemsById( itemId )
} )

Then( 'Inventory.Has no item {string}', ( itemName: string ) => {
    let itemId = getItemIdFromName( itemName )
    if ( DefaultScenarioClient.inventory.hasItem( itemId ) ) {
        throw new Error( `Inventory contains item ${itemName}` )
    }
} )