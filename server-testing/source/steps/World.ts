import { Then } from '@cucumber/cucumber'
import { L2World } from '../client/L2World'
import { DefaultScenarioClient } from '../client/L2ScenarioRunnerClient'
import { L2ObjectType } from '../client/enums/L2ObjectType'
import { L2Item } from '../client/models/L2Item'
import { getItemIdFromName } from './helpers/inventoryOperations'
import { ItemState } from '../client/enums/ItemState'
import { TargetAction, TargetActionType } from '../client/packets/game/send/TargetAction'

Then( 'World.Pick dropped item {string} with quantity {int} in radius {int} of player', ( itemName: string, quantity: number, radius: number ) => {
    let itemId = getItemIdFromName( itemName )
    let item = L2World.getObjectsByType( DefaultScenarioClient.getPlayer(), radius, L2ObjectType.Item ).find( ( item: L2Item ) => item.itemId === itemId ) as L2Item
    if ( !item ) {
        throw new Error( `No item found in radius ${radius} of player` )
    }

    if ( item.amount !== quantity ) {
        throw new Error( `Invalid item quantity of ${quantity}` )
    }

    if ( item.state !== ItemState.Dropped ) {
        throw new Error( 'Item is not dropped' )
    }

    DefaultScenarioClient.sendGamePacket( TargetAction( item.objectId, TargetActionType.TargetThenInteract ) )
} )