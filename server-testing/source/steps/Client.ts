import { AfterAll, BeforeAll, Given, Then } from '@cucumber/cucumber'
import { DefaultScenarioClient } from '../client/L2ScenarioRunnerClient'
import { scheduler } from 'node:timers/promises'
import { ShowAvailableCharacters } from '../client/packets/game/send/ShowAvailableCharacters'
import { L2ServerDefaults } from '../client/enums/L2ServerDefaults'
import { TargetAction, TargetActionType } from '../client/packets/game/send/TargetAction'
import { SelectCharacterSlot } from '../client/packets/game/send/SelectCharacterSlot'
import { getCharacterSlot } from './helpers/characterSelection'
import { BypassCommand } from '../client/packets/game/send/BypassCommand'
import { getItemIdFromName } from './helpers/inventoryOperations'
import logSymbols from 'log-symbols'
import chalk from 'chalk'

BeforeAll( async function () {
    // eslint-disable-next-line no-invalid-this
    let configuration = this.parameters

    let userName = configuration.credentials[ 'userName' ] as string
    if ( !userName ) {
        throw new Error( `No userName specified for login, using parameters ${JSON.stringify( configuration )}` )
    }

    let password = configuration.credentials[ 'password' ] as string
    if ( !password ) {
        throw new Error( `No password specified for login, using parameters ${JSON.stringify( configuration )}` )
    }

    DefaultScenarioClient.setLoginProperties( {
        host: L2ServerDefaults.LocalhostIpAddress,
        password,
        port: L2ServerDefaults.LoginServerPort,
        serverId: L2ServerDefaults.ServerId,
        userName,
    } )

    DefaultScenarioClient.start()

    /*
        We need to ensure that sufficient time passes before we are able to interact
        with client again. Login server flow must be performed and only then connection
        to game server can start.
     */
    await scheduler.wait( 2000 )

    if ( DefaultScenarioClient.hasClientExited ) {
        if ( !DefaultScenarioClient.gameClient ) {
            throw new Error( 'Unable to connect to login server' )
        }

        throw new Error( 'Unable to connect to game server' )
    }

    if ( !DefaultScenarioClient.gameClient ) {
        throw new Error( 'Unable to connect to login server' )
    }
} )

AfterAll( () => {
    DefaultScenarioClient.abortAllConnections()
    DefaultScenarioClient.hasClientExited = true
} )

Then( 'Client.Request for available characters', async () => {
    DefaultScenarioClient.sendGamePacket( ShowAvailableCharacters() )
    await scheduler.wait( 500 )
} )

Given( 'Client.Request to target self', async () => {
    DefaultScenarioClient.sendGamePacket( TargetAction( DefaultScenarioClient.getPlayerId(), TargetActionType.TargetThenInteract ) )
    await scheduler.wait( 200 )
} )

Given( 'Client.Enters game using player {string}', async ( name: string ) => {
    let slot = getCharacterSlot( name )
    if ( slot === -1 ) {
        throw new Error( `Unable to find existing player character with name "${name}"` )
    }

    DefaultScenarioClient.sendGamePacket( SelectCharacterSlot( slot ) )
    await scheduler.wait( 2000 )
} )

Given( 'Client.Request bypass {string}', async ( command: string ) => {
    DefaultScenarioClient.sendGamePacket( BypassCommand( command ) )
    await scheduler.wait( 1000 )
} )

Given( 'Client.Request bypass create-givetarget of item {string} with quantity {int}', async ( itemName: string, amount: number ) => {
    let itemId = getItemIdFromName( itemName )

    DefaultScenarioClient.sendGamePacket( BypassCommand( `create-givetarget ${itemId} ${amount}` ) )
    await scheduler.wait( 1000 )
} )

Given( 'Client.Packets are recorded', () => {
    DefaultScenarioClient.startGameClientPacketRecording()
} )

Given( 'Client.Packets are no longer recorded', () => {
    DefaultScenarioClient.stopGameClientPacketRecording()
} )

Then( 'Client.Packets are written in file {string}', async ( fileName: string ) => {
    await DefaultScenarioClient.packetRecorder.writeJsonFile( fileName )

    console.log( logSymbols.info, 'Recorded', chalk.yellow( DefaultScenarioClient.packetRecorder.size().toString() ), 'packets' )
    DefaultScenarioClient.packetRecorder.clear()
} )