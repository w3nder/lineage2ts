@cleanup
Feature: Removes all characters that are created with particular prefix
  Scenario: Remove characters
    Given Character.Exists one with prefix "Test"
    Then Character.Delete characters with prefix "Test"