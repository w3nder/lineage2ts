Feature: Creating a character and then deleting it

  # Please note that waiting is required to avoid being rate limited by server
  Scenario Outline: Creating and deleting using multiple race and class values
    When Character.Create using race <race> , class <class> and random parameters
    Then Client.Request for available characters
    Given Character.Verify exists
    Then Character.Delete current character
    Then Wait for 4 seconds
    Examples:
      | race      | class            |
      | "Kamael"  | "MaleSoldier"    |
      | "Kamael"  | "FemaleSoldier"  |
      | "DarkElf" | "DarkFighter"    |
      | "DarkElf" | "DarkMage"       |
      | "Elf"     | "ElvenFighter"   |
      | "Elf"     | "ElvenMage"      |
      | "Human"   | "Fighter"        |
      | "Orc"     | "OrcFighter"     |
      | "Orc"     | "OrcMage"        |
      | "Dwarf"   | "DwarvenFighter" |