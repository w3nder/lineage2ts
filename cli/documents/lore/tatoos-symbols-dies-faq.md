# Lineage 2 Tattos, Symbols and Dyes Frequently Asked Questions

There are a lot of questions concerning Tattoos, so I thought I’d put together this little guide.

As always I’d like to thank the ‘donkey in a sombrero’ SirEl for his comments and suggestions, his input on this FAQ was invaluable.

### I keep hearing about tattoos and symbols and dyes. What are all those things?
- In Lineage II all characters of a certain race/class start out with the same combination of stats. Symbols (commonly referred to as Tattoos by players) are a way to customize your character by changing your stats. These Symbols/Tattoos are created by taking 10 dyes of the same type to a Symbol maker and paying a fairly hefty fee.

### Can anyone and any class do this customization?
- No. In order to use any Symbols/Tattoos you must have completed at least your first class change. However most players do not bother with getting Tattoos until after the second class change. And although all classes can do certain changes, you are limited to what kind of change you can make by your class.

### Well, if I can make myself better, why do people wait until after the second class change?
- For several reasons. First, any Tattoo you got before your second class change becomes inactive after you finally do it. So if you had a Tattoo as an Elf Wizard and you change to a Spellsinger, you lose your Tattoo. This means you are out the money for them and they can be expensive.

The kinds of dyes you can use to make Tattoos are different for the below 40 crowd. The ordinary Dyes, which are used before the second class transfer, are much weaker. They only add at most +1 to a stat and can decrease it up to -3, while the Greater Dyes will increase one up to +4, with a -6 as the maximum. Also, before your second class change, you can only have two Tattoos, while after the change you can have up to three.

Because the amount of time you spend going from level 20 to 40 is short, relative to the time you spend past level 40, it’s usually better to wait and save your funds for later on. Also, if you have a pre-second class change Tattoo, it will become unusable but you must pay a Symbol maker to get it removed. If you don’t need the slots, you can just leave it and it will have no effect, but if you want to use the slots it occupies you must remove it.

### Okay, so where do I get these dyes?
- They can come from several places. The ordinary ones can be bought at many of the town merchants, but we already discussed how they are generally not considered worth the Adena. These ordinary ones are also sometimes gotten as drops or spoils and there are new Common Craft recipes for making dyes. (Common Craft is a whole other topic, since it requires access to the recipe and items from fishing.)

Some of Greater Dyes can be bought with Ancient Adena from the Trader (Merchant) of Mammon who is only available once every two weeks IF you were on the winning side AND the Seal of Avarice was broken by the winning side. Some of the Greater Dyes can also be found as drops or spoils (some only from Raid bosses) and there are Common Craft recipes for some of the dyes.

### How much are those dyes from that Mammon guy?
- The Greater Dye cost for +4/-4 are 174,000 each, +3/-3 are 108,000, +2/-2 are 72,000 each and the +1/-1 is 60,000. Costs are all in Ancient Adena remember, not regular Adena. And remember you need 10 of each dye!

### Alright, so I want to modify a stat. How do I know which stat does what?
- There is a complete listing of what stats do what here: Lineage 2 Stats Effects List.

### Great! So I can give myself +10 on some stat?
- Not through Tattoos, the most you can modify a single stat is +5 with Tattoos, though you can decrease a single stat down to an insane amount. Note however that bonuses from things other than Tattoos are not counted in this. You can have +5 Str from a Tattoo and still get the bonus from a Plated Leather set, for a total of +10, however your bonuses from gear will not show on your character status screen.

### Okay, sounds good. What else do I need to know about these Tattoos?
- All dyes have positive and negative effects. That means if you want to raise a stat, you also must lower another one. You can’t raise or lower any stat you want, you are limited by what combinations are available and what class you are.

### What do you mean ‘combinations available’?
- Well, the dyes all come in pairs of raising one stat and lowering another but there are only six of these stat pairs in existence, Con/Dex, Con/Str, Dex/Str, Int/Men, Int/Wit, and Men/Wit. Since the positive stat is always written first you will also see dyes listed as Dex/Con, Str/Con, Str/Dex, Men/Int, Wit/Int, and Wit/Men, but the combinations are still the same.

### Okay, so what’s this about class?
- Not every class can use every dye. If you look at the table to see what the different stats do, you will note that Int ‘Increases damage of magic attacks and success rate of curse spells.’ Now if you are playing a pure Healer/Buffer who doesn’t solo chances are you don’t care at all about this and so lowering Int would seem like the perfect solution. Unfortunately for you, you can’t.

### Alright, I think I understand this now. Which Tattoo should I get for my class?
- Well, it really depends. Although it might seem easy at first to say, it will depend a lot on a couple of factors. First, what is your play style? There are usually several different ways to play most characters, for instance some healers like to be ‘battle healers’ and wear light armor and melee, while others simply want the fastest heals they can cast or the highest percentage return on XP per resurrection. Some of the direct damage dealers want to be a ‘Glass Cannon’ with the highest damage output they can possibly get even sacrificing survivability, while others play more conservatively and want to last longer while doing their damage.


A second huge factor in all this is your armor/weapon setup. You may not mind losing some casting speed by decreasing your Wit, if you are using an armor set that includes a casting bonus, plus a weapon that has an Acumen SA (Special Ability) added to it. If your armor adds extra HP, you may want to sacrifice Con for more damage in the form of Str.

Lastly, do you want this for PvE or PvP? As one friend said ‘the PvE gladiator setup gives a -9 CON; that is a kiss of death in PvP.’

So basically it comes down to you deciding what it is that’s important to you. Look carefully at what each stat modifies and what stat combination is available to you. Talk to some of the people playing your same class to find out if they went the direction you are considering and whether or not they found it worked for them.

Most of the time you are going to look for things that are at least an equal swap, although there are dyes that give you +2 to one stat while taking -4 or other combinations like that, those dyes are not preferred.

Just remember, there are some common things all classes seem to gravitate towards, so you will probably find that most use the same kinds of tattoos, but the system does allow you to be a rebel if you like.

Also, you can’t use two +4 Tattoos on the same stat, since the most you are allowed to increase is +5. If you want +5, you will need to do a +3 and a +2 or a +4 and a +1, that sort of combination, but the +3 and +2 work out cheaper than +4 and +1, just FYI.

### Where do I find this ‘Symbol Maker’?
- You can find one in every major town in the Magic shops.

### What if I want to change Tattoos or just get one removed?
- You can’t just swap one Tattoo for another; you have to have the slot empty and that means removing it. To remove it, you pay a fee to the symbol maker and in exchange the tattoo is gone and you are given half the dyes it took to make it (5 dyes) in return for the removal.

Source: https://guidescroll.com/2011/12/lineage-2-tattos-symbols-and-dyes-faq/