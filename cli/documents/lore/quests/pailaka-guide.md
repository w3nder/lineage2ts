# Lineage II Palaika Quest Guide 61-67

## About quest
To start the quest, go to the side of The Einhasad Temple in Giran where you will find an NPC named “Devils Isle Survivor” next to a tree.

Make sure you’re ready to start the quest before speaking to him because as soon as you click ‘Accept the challenge of Palaika’, you will be automatically teleported to the next instance zone.

Think about taking some potions with you: greater antidotes, QHP, mana, HP and any other essentials: soe, pet shots (for the summon that you will be given during the quest), l2day buff scrolls. If possible, get yourself buffed before heading out.
Inside the instance zone, the mobs can use poison and bleed on you, so be ready for and expect the unexpected!

The instance takes place on Devil’s Isle. The setup is identical to that of the normal layout. Once in place, you won’t be able to go back, unless you use a SOE. If you need to rebuff when you’re there, it’s possible to return to town with a SOE. The mobs that you kill during the quest won’t reappear, provided you don’t exceed the quest’s given time limit for completing the quest.

Upon arriving at the island, you will be able to speak to the NPC “Devil’s Isle Supporter”.

He will give you your quest objectives and a special weapon. It’s a two-handed sword. Even though it’s classed as an ‘Ancient Sword’, it has nothing to do with a real ancient sword. Therefore, the kamael skills don’t work with it: it won’t be able to be turned into a kamael weapon, but it does have a very strong m.atk and p.atk. Moreover, it lets you summon a tigress!

It’s an excellent summon, especially with pet soulshots – she uses 4 every hit. She hits very hard and has a powerful skill which lets her inflict nice damage.
There is just one problem: double-clicking on her won’t bring up the ‘attack’ button for the servitor. To solve this, there are 2 options:
1) If you’re a summoner: summon one of your servitors and drag that servitors attack button to your hotbar and use that one – it will then act as an attack button for the tigress. All your skills will also work with her.
2) If you’re not a summoner: make a macro “/summonattack” and place it on your hotbat. Use this one when you want the tigresse to attack an enemy. Remember the tigress acts exactly like an ordinary servitor.


## Setup of rooms, mobs & bosses

With a strong sword and fierce tigresse at your hands, you’ll be well equipped. However, the task that awaits you won’t be that easy. Devil’s Isle is like a maze – so you’ll probably get lost from time to time.

Each room is filled with mobs. They all look the same as normal, but their levels go up as you go along. These mobs are social and agro. They are undead, so it would be wise to use holy attacks if possible. Having some unholy resistance buffs could be really useful.

Before reaching the bosses, you’ll have to clear out some rooms. Make sure not to agro too many mobs at the same time, otherwise it’s certain death and nobody will be able to rez you. The only way of getting a rez would be by waiting 90 minutes or by relogging.

The mobs throw a powerful poison at you, but you can protect yourself by using greater antidotes ot palaika antidotes.

Here are the mobs that you are going to meet:

All the mobs in the instance are identical, but their levels go up as you advance. As for the bosses: there are 4 of them to kill before reaching the final one.

In a lot of the rooms, you’ll come across ‘treasure boxes’. It’s highly recommended that you open them (by ‘killing’ them) because they contain potions which could could in handy later on in the quest: QHP, restore HP, antidotes, unholy resistance potions, archer resistance potions, 3 second invincibility potions. These potions are VERY useful. If your stock is going down, try to find some more chests!

## First Boss: KAMS
To find him: from your starting point, go straight on, don’t take the first path that leads to the right. Clean up the first room, then continue to the second. The boss is there!

Kams is agro, like all the other bosses. So if you play a relatively feeble class (like a mage, for instance), summon the tigresse and get her to tank the boss. He hits hard and can cast poison. Therefore, have some antidotes and HP potions at the ready. There is no need for a strategy to kill this boss, since he has no minions. Just be careful. When you’ve killed him, you’ll receive a scroll to improve the ancient sword you were given. Check that you have it in your inventory/quest window.

Return to the first room and take the path that leads towards the right (the one that you haven’t been in). Kill the mobs until you catch sight of a room with two archers inside a room on the right. You will meet up with an NPC named ‘Dwarf Adventurer’. Clean out the room then speak to him and he’ll let you use the scroll on the sword, ultimately improving your weapon. To do that, the tigress must be unsummoned.

With the improved sword comes a better special skill for the tigresse – ‘Ice Chill’. It’s like a water a nuke. You can use it to inflict nice damage but it isn’t mandatory. Before continuing on to the second boss, resummon the tigresse!

Go down the ramp which is located to the left of the dwarf and continue straight on until you find a room filled with ‘Powder kegs’.

You can use these powder kegs to your advantage because when they explode, they kill all the mobs around it – it’s like an AOE nuke, almost. CAREFUL! Make sure not to be close to the kegs when you blow them up or you and your tigress will suffer the consequences.
A single barrel won’t be enough to kill all the mobs, and you risk starting a train of mobs on your butt. Do it tactically. It makes sense to create an explosion on a barrel near several mobs.
For your information: mobs killed by the barel explosion won’t give you any exp. There are few rooms fill with barrels. Make the most of them for clearing out as many mobs as possible before finding the next boss.

## Second Boss: Hikoro
A tip so you don’t get lost: don’t take any path underneath the water. If you find yourself faces with two doors, never take the one under the water – take the other one – the one which is above the weater.

Hikoro is surrounded by some mobs. It’s recommended you kill them first (perhaps by sending in the tigresse) while trying hard not to agro the boss, obviously.

Once the room is cleared, kill the boss. Watch out – he casts poison and bleed.

This boss won’t give you a scroll – you’ll get that with the third boss.

## Third Boss: Alkaso

To find him: Return to your path and enter the room neighboured to the one where Hikoro was. You’re going to be faced with two passages: take the one on the right. Then continue straight on until you find ‘Alkaso’.

This boss is stronger than the first two – and he casts bleed. He is at the back of the room which leads you into the upper part of devil’s isle. When you’ve killed the boss, talk to the dwarf who is located beside him to further improve your sword and tigresse – a new skill will be given called ‘Ice Ball’. Take the door downstairs and then go up.

In the upper part of th eisland, you’ll find several rooms on your left. Each one contains treasure boxes. Clear out the rooms and open all the chests afterwards so stock up on the potions I mentioned before. Make sure you get them before attacking next boss.


## Fourth Boss: Gerbera

So that you don’t get lost: all the rooms are cul-de-sacs and there is only one path that takes you to the fourth boss. Gerbera is in a room on your left at the highest part of the isle. To get there, go to the end of the long corridor, up the path on your right at the very end.
This is the last boss before Lematan (the final boss). He is found not fat from Zaken’s spot.

After having killed Gerbera, you’ll arrive at this rocky ledge which you must jump off of to reach Zaken’s lair.

You’re near the end!
If your tigresse is hurt or her re-use timer is about to expire, it’s advised that you resummon before jumping into the water because you’re going to be swimming for a bit before arriving at the entrance to the passage which takes you to the boss spot. You’re also going to be welcomed by 2 skeleton archers. If you have one of them, use a resist archer potion because more of the same mob types await you.

## Final Boss: Lematan
The more archers you see, the closer you’re getting to the final boss. Keep going until you end up on a set of stairs – Lematan will be on your left. Word of advice: move to the right to kill the archers then get on the platform to prepare yourself before attacking Lematan.

Lematan is agro, so make sure you’re ready before getting close to him!Summon the tigresse first and use her skill. It’s very effective against Lematan.

Something important to know: When Lematan reaches half HP, he runs towards the end of the quay and teleport himself onto Zaken’s boat. Run up the stairs and along the ramp to reach him. When you reach the bridge, some minions called ‘Lematan’s Followers’ will appear to heal their master. While the minions are healing him, Lematan can’t move…

There are different strategies to take to complete the task and kill Lematan.

1) Send in the tigresse on the minions while you concentrate your attack on the boss. This is an effective technique but quite complex because you have to juggle between you and the tigress’ skills. Moreover, the minions respawn. So this strategy could become long and troublesome.

2) Try to move Lematan onto the bridge, far away from his minions who won’t be able to act as a healer because of the distance. However, make sure you don’t lead him too far across the bridge, otherwise he will teleport back to the middle of the boat.

3) Lead Lematan behind the pillar on the boat where he will be out of sigh of his minions. This is the method I used and it worked for me. I had Lematan agro me as I runned behind it, then got my tigresse to assist me. I did this a spellhowler and was spamming normal HP pots – it was a pretty close call, but you should be OK.


As soon as you have killed Lematan, le ‘Dwarf Adventurer’ will appear. Speak to him to complete the quest and receive your well-earned reward!

Tip: If you have an Anchor, Root or Shackle type spell, you can cast that on Lematan to stop him from teleporting (because in order to teleport, he needs to be able to reach the other end of the quay) so you could kill him easier.
The only problem is that if you kill Lematan outside the boat, the Dwarf Adventurer won’t appear on the boat. You’ll have to swim back and meet back up with him and find the path that takes you to him. A tip: swim to the right and then go right again towards the dwarf and you’ll find him.

Good luck!

Source: https://guidescroll.com/2011/09/lineage-ii-palaika-quest-guide-61-67/