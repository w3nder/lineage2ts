# Lineage II Kamaloka Bosses Guide

In this guide, I will cover only the X3 and X6 (ie. 23,26,33,etc.) bosses and not the dungeons (X9). I have not done the kamaloka instances past 56 so I will not include information pertaining to them.

`Note`: All bosses (with the exception of few, ie. Seer Flouros, the eye) will counter magic with skills. This means that they will be most likely to target your caster (Healer, Buffer, Mage) with their skills and NOT the tank using hate or pure DPS.

## 23 Boss
- `Appearance`: Bugbear
- `Special Skills`: AOE Stun
- `Minions`: None
- `Strategy`: Just attack, very easy to 2-man this one.

## 26 Boss
- `Appearance`: Ol Mahum
- `Special Skills`: AOE hit (don’t keep your baby pet nearby)
- `Minions`: One
- `Strategy`: Very easy to 2-man provided both players can wear heavy–otherwise use three players. Have lowest DPS tank hit the minion to take aggro, then just ignore it until the fight is over.

## 33 Boss
- `Appearance`: Undead Bear
- `Special Skills`: Stationary AOE Smash (x5), Rsk. Haste
- `Minions`: None
- `Strategy`: Another easy boss, he will periodically run to one player and sit his ass down to smash the ground dealing AOE damage around him. Simply run away until he finishes doing this (5 times).

`HINT`: When he gets to 10% health, he’ll say something and buff himself with haste2 and obtain nasty attack speed (which also speeds up his AOE smashing). When his HP is this low, try to tank his smashes only if you can to ensure that you can continue hitting him while he tries to cast haste. What you’re trying to do is interrupt his haste cast (Which can be done quite easily)

## 36 Boss
- `Appearance`: Floating Eye
- `Special Skills`: Aura Flare, Drain Health
- `Minions`: Unlimited
- `Strategy`: The boss and his minion will use both magic and physical attacks often so ensure you have a good set of jewelery. He will periodically summon an eye to assist him but unlike other kamaloka bosses, he will summon more and more despite already having an active one.

`HINT`: Whenever he spawns a minion, kill it–Otherwise you may end up with 5 pelting at you.. and you will die. Even with Top D jewelery, his life drain will crit often for 400-900 damage so make sure you keep your HP above 50%.

`NOTE`: I recommend that the main tank uses a polearm for this boss to hit both the boss and the minion at the same time.

## 43 Boss
- `Appearance`: Mutant Stakato
- `Special Skills`: Nothing notable
- `Minions`: One, healer
- `Strategy`: When his HP dips below 50%, he will summon a healer which heals him for quite a bit. This boss is extremely difficult and you will probably not be able to 2-man this one. After you kill his healer, he will spawn another one right away. I recommend sleeping the minion constantly so that it is not healing him and you are not wasting DPS (because he will spawn another after it dies anyways).

`HINT`: Most people do the 36 boss until they are level 41, allowing them to do the 46 boss–Effectively skipping this one. This is not a bad idea.

## 46 Boss
- `Appearance`: Dragon
- `Special Skills`: Soul Confine
- `Minions`: None
- `Strategy`: This boss is easy to 2-man provided that both players provide adequate DPS. This boss will periodically use Soul Confine on one player (Lasts roughly 10 seconds) which is similar to paralysis–except you cannot receive damage while confined. He also uses an AOE water-element skill, but it does not do a lot of damage unless you are not wearing jewelery.

## 53 Boss
- `Appearance`: Demon
- `Special Skills`: Disarm, Sonic Blaster
- `Minions`: Suicide Bomb
- `Strategy`: This boss can also be 2-man’ed quite easily provided both players provide adequate DPS. He will periodically disarm your highest DPS’er and use Sonic Blaster on the caster. Don’t forget to re-equip your weapon!

`HINT`: He will also periodically summon a minion which flies to a player (usually the caster, but I’ve seen the melee DD take it) and explodes. A critical can do 2600 damage, even using a +3 BO set, otherwise it will normally do 200-500. Keep your HP near full because of this–you don’t want to risk getting crit and killed.

`NOTE`: If you’d rather not tank the explosion, you can simply run away JUST before it explodes. If you run away too early, it will simply follow you until you stop

## 56 Boss
- `Appearance`: Undead Knight
- `Special Skills`: None
- `Minions`: Dark Panther (Unlimited)
- `Strategy`: You’ll need at least three players for this one. This boss does rather high DPS on his own and also periodically summons panthers (do not bother killing these). He will call out a players name and all panthers will attack that player. Have the target simply run around while others kill the boss and take turns running around as you are targetted by the panthers.

Source: https://guidescroll.com/2011/09/lineage-ii-kamaloka-bosses-guide/