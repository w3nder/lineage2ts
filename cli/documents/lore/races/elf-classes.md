# Fighter First Professions

## Elven Knight
- Elven Knights are Fighters who have received swordsmanship training on a professional level. 
- In order to acquire more effective attack and defense skills, they continue to train on their swordsmanship and have mithril armor and weapons that best utilize Elven fighting skills.
- Elven Knights differ from other Knights in their high agility and ability to cast recovery magic. 
- Their quick movement speed and haste spells make them highly regarded in groups.

## Elven Scout
- Some Elves prefer bows to swords, and these Fighters become Elven Scouts. 
- They are comfortable living outdoors in the forest and their basic equipment is a bow, a dagger, and a suit of light armor.
- Elven Scout is similar to that of a Rogue whose skills are best suited to the forests. 
- With their healing and support magic skills, they are an asset to any group. 
- Their high dexterity and quick movement make them excellent scouts and masters of ranged attacks.

# Mystic First Professions

## Elven Wizard
- Elven Wizards divide the magical forces in the world into four elements, as well as light and darkness. 
- They then amplify the power of these elements and combine them to use in their magic. 
- Elven Wizards can use mid-level elemental magic and summon magic.
- Elven Wizard is similar to the playstyle of other Wizards, however, the characteristics of the magic they use and the types of creatures they summon are different from Wizards of other races. 
- Though fast casters, the magical attack power of Elven Wizards is somewhat weak such that they cannot use black magic.

## Elven Oracle
- Elven Oracles are Mystics who borrow the power of the gods to perform miracles. 
- Though they have similar abilities as Human Clerics, they worship different gods and are as such granted different spells and skills.
- Elven Oracle is similar to that of a Cleric. However, Elven Oracles are possessed of faster movement and casting speeds and focus on a different array of skills.

# Fighter Second Professions

## Temple Knight
- The Temple Knight, who has risen to the height of swordsmanship, borrows the power of the gods of light and water for both his magic and his sword. 
- He engages in battle by summoning cubics to assist in attacks through Summon Storm Cubic, or by summoning cubics that can recover his own physical strength through Summon Life Cubic.

## Swordsinger
- The Swordsinger has a beautiful voice that he uses to raise the morale of his party members and to assist them in battle. 
- He helps to recover the physical strength of party members through Song of Life and raises the defensive power of party members using Song of Earth. 
- The sound of the Swordsinger's voice is welcome in any party.

## Plainswalker
- The Plainswalker, who is more accustomed to outdoor life than indoor living, is an adventurer skilled in all areas of battle and exploration. 
- He attacks opponents from behind using Backstab and can entice one unique monster to benefit his party by using Lure.

## Silver Ranger
- First and foremost, the Silver Ranger is the ideal Elven fighter in terms of skill with the bow. 
- He can perform Double Shot, which shoots two arrows consecutively and Burst Shot, which hits multiple enemies within the range of his target.

# Mystic Second Professions

## Spellsinger
- The Spellsinger has become intimately familiar with elemental magic and deals in magic that primarily uses water. 
- He uses a shield called Freezing Skin that reflects damage directed towards him and uses the Ice Dagger to throw ice blades at his opponents.

## Elemental Summoner
- The Elemental Summoner engages in battle using servitors and is skilled in a variety of summon magic. 
- Like other summon mystics, he engages in battle vicariously. 
- He raises the defensive power and magic defensive power of servitors through Servitor Physical Shield and Servitor Magic Shield, respectively. 
- The Elemental Summoner also raises attack speed through Servitor Haste.

## Elven Elder
- The Elven Elder stands at the forefront in the religion of water and aids his allies using various methods of support magic. 
- He raises the defensive capability of shields through Bless Shield. Occasionally, the Elven Elder will even bring down the Might of Heaven upon the undead.

# Fighter Third Professions

## Eva's Templar
- Eva's Templar is a prestigious title conferred on the strongest Temple Knight protecting Eva and the forest of the Elves. 
- Knights who presented their swords to the goddess Eva, who became the guardian of water and the elves following the fall of Shilen, their top priority is protecting colleagues rather than destroying the enemy.

## Sword Muse
- Sword Muses are Sword Singers who have reached the apex of the singing of spirit songs unique to Elves. 
- Spirit songs are beautiful songs infused with the power of the spirit. 
- Magic exclusive to Elves, these songs enhance the combat capability and morale of their own forces and to seek the divine protection of the spirits and the goddess before heading out to the front. 
- Following the Elmoreden imperial era, the number of Sword Singers who reached the level of Muse continuously dwindled and came to be deemed as legends, but recently began to resurface with the emergence of the woman of the lake.

## Wind Rider
- Elves, a race belonging to the forest and water, are intimate with the wind. 
- Among them, the most accomplished Plains Walkers, travelers who are able to appear and disappear like the wind, are called Wind Riders. 
- Equipped with extraordinary agility and grace, they are explorers adept at combat and exploration and acclimated to outdoor life.

## Moonlight Sentinel
- The bow is a very important weapon in war for Elves, who traditionally prefer long distance hit-and-run combat over short distance melee combat. 
- A title conferred on the most accomplished archers among the Elf warriors, the Moonlight Sentinel is named after an ancient hunting ritual performed under the light of the full moon.

# Mystic Third Professions

## Mystic Muse
- Mystic Muse is the title given to Spellsingers who have reached the peak of the four element wizardry. 
- Referred to as muses, they are rumored to possess the beautiful and mystical voice of the gods. 
- Although they possess power equal to the Archmages of the Ivory Tower, the highest institution of element wizardry, they rarely venture out of the forest and remain largely unknown.

## Elemental Master
- Elemental Master is the title bestowed on the most accomplished Elemental Summoners. 
- Elemental Masters are knowledgeable summoners who practice summoning techniques exclusive to Elves, the foundation of which lies in the belief that magic is the natural phenomenon of power.

## Eva's Saint
- Of Eva's priests who spread her teachings, Elders who have reached the pinnacle of their faith and power are called Eva's Saints. 
- They are devoted to peace, treat the sick, and spread love, but they are also resolute priests who are undeterred from participating in battle in order to protect their allies and uphold peace.

Reference link: https://legacy-lineage2.com/Knowledge/race_elf_changejob.html