# Human Fighter first professions

## Warrior
- The Warrior class is an advanced class of the Human Fighter who pursues physical power. 
- Since Warriors do not belong to regular military, most pursue work independently. 
- Warriors focus on having strong swordsmanship skills. 
- Focused on gaining strength and power, they tend to distance themselves from other people to better focus on their training. 
- Warriors tend to live on the outskirts of villages and adopt an uncultivated lifestyle.
- Class is specialized for close physical combat with an emphasis on attack skills. 
- Warriors tend to concentrate their effort on raising their attack skills and killing monsters. 
- During party play, warriors can assume the role of a tank to some extent. 
- However, their primary skill and benefit is in dealing damage, and lots of it.

## Human Knight
- Human Knights receive training on the military arts from a government organization (or an organization of equal capacity). 
- Knights consider honor their top priority and are prepared to sacrifice their lives for their country and king. 
- A one-handed sword and shield are their basic equipment.
- Class is specialized for close physical combat with an emphasis on defense. 
- Their defense skills are excellent and their self-heal skill allows them to hunt for a longer period of time without the assistance of a companion. 
- In party play, Knights focus on attracting monsters’ attacks and attention to themselves - allowing other party members to focus on dealing damage to make the killing blow.

## Rogue
- Rogues are Fighters who consider agility more important than power — and profit more important than honor. 
- Unlike the previous two classes, these Fighters rely heavily on ranged attack weapons. 
- They prefer a small sword, bow and light armor to a large sword or thick armor that may slow down their agile attacks. 
- The main tasks of a Rogue include reconnaissance, scouting, spying, and assassination. 
- On a rare occasion, Rogues may be hired as a group to be utilized as an army of archers.
- Hunting as a Rogue can be incredibly risky. 
- Relying on evasion for defense and armed with daggers and bows for attacks, Rogues may find it difficult to battle high level monsters on their own. 
- During party play, Rogues are wise to avoid acting as a tank. 
- Instead, in many combat situations, they can be useful by utilizing their high rate of critical attack or following their party and inflicting significant ranged damage using their bow skills.

# Human Mystic first professions

## Human Wizard
- Human Wizards divide the magical forces in the world into four elements, as well as light and darkness. 
- They then amplify the power of these elements and combine them to use in their magic. 
- Most Human Wizards came from the Ivory Tower of Oren or are disciples of Human Wizards who came from the tower long ago. 
- They can use the middle level elemental magic, black magic and summon magic.
- Human Wizards are focused on elemental magic, summon magic, and black magic, and as such can summon a creature and use it as a tank, or absorb HP from a corpse and convert the HP to MP to reduce their downtime. 
- During party play, Human Wizards deal great damage with their powerful spells.

## Cleric
- Clerics are Human Mystics who borrow the power of the gods to perform miracles. 
- Although they often belong to religious organizations, some of them refuse to align to any one religion and act on their own according to their own beliefs. 
- Their magic is mostly used to help other people. Due to the nature of their profession, they often travel to make pilgrimages for their religion.
- Clerics have chosen white magic and support magic as the focus of their profession rather than direct magical attacks. 
- During solo play, Clerics can increase their abilities by using magic and by wearing light armor. 
- However, since their physical prowess falls quite short of the Fighter class, they are slow hunters. 
- Since it is possible for them to have continuous hunting by healing their own wounds, they are not entirely inferior when it comes to solo play.
- Due to class usage of both support magic and white magic, they are considered critical in parties, and are welcome in almost any party to cast Might, Shield, and Heal spells. 
- In addition, during a crisis situation, a Cleric can perform crowd control by using spells such as Sleep.

# Human Fighter second professions

## Warlord
- The Warlord is more capable in one-on-many battles than in one-on-one combat and prefers long-range weapons, such as poles. 
- He casts gusts of thunderstorms that cause enemies to faint in their tracks. 
- The Warlord demonstrates his power most effectively in groups, such as during siege battles.

## Gladiator
- None is better at one-on-one close combat than the Gladiator. 
- Enemies attempt to avoid his powerful attacks in vain. 
- The Gladiator displays his superior swordsmanship using the Sonic Storm or with two-bladed swords he implements the Triple Slash.

## Paladin
- The Paladin prays for the power of the holy gods to protect all precious things in battle and to defeat the evil spirits that appear before him. 
- Healing himself through Holy Blessing, the Paladin uses Hate Aura to protect his fellow party members.

## Dark Avenger
- Unlike the Paladin who uses mostly white magic, the Dark Avenger uses black magic to vex his opponents. 
- He absorbs the physical strength from corpses using Life Scavenge and emanates attack points with Reflect Damage.

## Treasure Hunter
- The Treasure Hunter, known as the "danger that lurks in the darkness", acts according to his own private goals. 
- The Treasure Hunter moves without fear by using Silent Move and is skilled at using Backstab against his enemies to eliminate them from behind.

## Hawkeye
- Rather than using daggers, the Hawkeye threatens his opponents from afar using longbows. 
- He becomes an even more powerful force when joined by a group. 
- The Hawkeye's aim is accurate against single opponents, while he attacks groups of enemies effectively with Burst Shot.

# Human Mystic second professions

## Sorcerer (or Sorceress)
- The Sorcerer is a very perplexing being who borrows the power of the elements to cast extreme elemental magic. 
- He supports his allies by pouring fire down upon enemies from a distance using Blazing Circle. 
- Enemies within the Sorcerer's range are knocked unconscious with Sleeping Cloud.

# Necromancer
- The Necromancer makes use of the power of darkness by reviving the black magic of the titans. 
- He summons zombies from corpses with Summon Zombie and causes great harm to his enemies through Corpse Burst.

# Warlock
- The Warlock considers the power of magic to be a living entity. 
- He can create something from nothing by using the highest level of summon magic. 
- Like other summon mystics, he engages in battle vicariously by raising the defensive power and magic defensive power of servitors through Servitor Physical Shield and Servitor Magic Shield, respectively. 
- The Warlock can also raise attack speed through Servitor Haste.

## Bishop
- The Bishop is a holy person of this era who brings about miracles of healing and recovery using the highest level of recovery magic. 
- If necessary, he will engage in hand-to-hand combat to protect himself. 
- The Bishop recovers physical power through Vitalize, can heal addiction and bleeding, and uses specialized magic on the undead.

## Prophet
- The Prophet makes his allies strong and his enemies weak using the highest level of protection magic. 
- With the blessing of the gods, he increases the maximum value of physical strength for another party through Bless the Body. 
- The Prophet also increases his own MP through Bless the Soul, and if necessary can perform Return to go back to a village.

# Human Fighter third professions

## Duelist
- Regarded as the most powerful swordsmen in one-on-one combat, Duelists prefer dual swords as their weapon of choice, and they overcome the enemy with storm-like swiftness. 
- They do not wear heavy armor, as they have inherited the Gladiator mentality of regarding their attack as their best defense.

## Dreadnought
- Called upon by ancient powers, the most powerful Warlords are conferred the designation of Dreadnought. 
- Adept at one-on-many combat, they are endowed with singular combat prowess, enabling them to annihilate enemies regardless of their number. 
- Dreadnoughts prefer large weapons such as the polearm.

## Phoenix Knight
- These hallowed Paladins worship the god of light to protect their lord and uphold justice. 
- Phoenix Knights use healing spells invoked by the blessing of the god of light. 
- They are guardian knights who fight to protect fellow knights rather than to kill the enemy.

## Hell Knight
- Rejecting hypocrisy and pretense, these knights of hell direct their wrath at the enemy. 
- Devoted to vengeance and the god of darkness and unwavering in their beliefs, these formidable Dark Avengers are willing to destroy anything that gets in the way.

## Adventurer
- Free-spirited explorers unfettered by class or rank, Adventurers are nomads perpetually in search of the big jackpot that will bring them money and status. 
- Agile warriors adept at hit-and-run combat, they are not to be taken lightly, as they can identify and take advantage of the target's weakness with precision.

## Sagittarius
- Sagittarius is a prestigious title conferred on the most accomplished Human archers. 
- Under the Elmoreden Empire, this title was bestowed on numerous master Hawkeyes. 
- Many of the skills they devised were handed down through the generations, keeping the tradition of the Sagittarius intact. 
- Following the decline of their nation and the ensuing period of disarray, the skills of the Sagittarius became obsolete and not a single archer was awarded the title. 
- Many archers, compelled by whispers of destiny, now embark on their own journeys, but only a handful return. 
- Those brave souls inherit the legend of the Sagittarius.

# Human Mystic third professions

## Archmage
- A title bestowed on the most accomplished sorcerer of the Ivory Tower during the Elmoreden era, the Archmage handles the four basic elements flawlessly. 
- Transcending the limitations unsurpassed by all other sorcerers, they achieved a daunting level of wizardry and were seen as a symbol of the power of strong magic. 
- Following the fall of the empire, a true Archmage did not emerge for some time. 
- Recently, Archmages have begun to re-emerge, accompanied by rumors of the woman of the lake.

## Soultaker
- Masters of the dead and black magic, Necromancers who are able to truly conquer the power of the spirit are given the title of Soultaker. 
- Unlike Sorcerers or Archmages who handle element magic, Soultakers handle black magic. 
- Persecuted since the imperial period, many of their skills were once rendered obsolete. 
- It was believed that those skills could not be recovered; however, instigated by the resurrection of the Archmage, a number of black magicians embarked on a journey in search of a lost vision. 
- The handful that returned were resurrected as Soultakers.

## Arcana Lord
- Arcana Lord is a title given to Warlocks that are the most skillful in rendering manifestations of elemental magic. 
- Calling upon creatures from other realms, they must possess unqualified control over the arcana, the requisite power path which leads to the summoning.

## Cardinal
- Bishops who have transcended the teachings of the order of the god of light and reached a new standing are granted the title of Cardinal. 
- Priests researching the studies of countless ancient clerics embarked on a search of the cause of the discrepancy between their miracles and current white magic. 
- Years later, only a handful returned and formed the Order of the Forgotten Miracles, compelling them to proclaim the resurrection of Cardinals.

## Hierophant
- Priests who have chosen to diverge from the Order's orthodox doctrine - "unless for reasons of self-defense, you must not harm others with magic" - believe in resorting to aggressive tactics to achieve their end. 
- A great number of Prophets are working to make the world a better place today, but those among them who have reached a new pinnacle after hearing fate's whisper are referred to as Hierophants.

Reference link: https://legacy-lineage2.com/Knowledge/race_human_changejob.html