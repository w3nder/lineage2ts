# Lineage II Mini Pet Guide

## Buffalo good for fighter class
- Baby has Small Heals
- Improved does Greater heal 
- lv 55 : Bless the Body lvl 6. (20mins), Might lvl 3. (2 mins)
- lv 60 : Shield lvl 3. (2mins), Guidance lvl 3.(2mins)
- lv 65 : Haste lvl 2. (2mins), Vampiric Rage lvl 4. (2mins)
- lv 70 : Focus lvl 3. (2mins), Death whisper (2mins)

## Kookabura good for mage class
- Baby has Small Heals 
- Improved: Recharges mp (battle heals as well when you are at 30% hp)
- lv 55 : Bless the Soul lvl 6.(20mins), Empower lvl 3.(2mins)
- lv 60 : Bless the Body lvl 6. (20mins), Shield lvl 3. (2mins)
- lv 65 : Concentration lvl 6. (2mins), Acumen lvl 3.(2mins)
- lv 70 : (consensus seems to be no new skill at 70) 
- If the owner is +15 lvls of the pet, there is -10mp penalty per each level.

## Cougar Casts both fighter & mage buffs
- Baby has Small Heals 
- lvl 55 : Might lvl 3.(2mins), Empower lvl 3. (2mins)
- lvl 60 : Bless the Body lvl 6.(20mins), Shield lvl 3. (2mins)
- lvl 65 : Acumen lvl 3.(2mins), Haste lvl 2. (2mins)
- lvl 70 : Vampiric Rage lvl 4. (2mins), Focus lvl 3. (2mins)

Please note, even though these baby pets can equip a weapon, don’t bother buying one, they are lovers not fighters.

A Hatchling is cute baby dragon, again not much of a fighter but at 55 when evolved into a Strider they can be ridden. If you see someone flying in the air with a dragon, that is a Wyvern. Castle owning clan leaders can use a normal Strider to summon a Wyvern, B-grade crystals are required and be sure to have Wyvern food on the pet. Certain clan halls (Aden, Forrest of the Dead and Beast Farm I believe, there may be others) also have a Wyvern Manager where the clan leader can summon a Wyvern. More here http://www.lineage2.com/Knowledge/pet_6.html

You can exchange a Strider to a Guardian Strider via Territory War badges. I do not know what advantages a Guardian Strider has over a normal Strider so I anyone has one and can post their stats and skills and maybe a screenshot that would be cool =D

Dino pets are obtained by hunting on Primeval Isle. They do not drop on the ground but instead go directly into your inventory. They eat plain wolf food, do not have any buffs and cannot wear pet weapons or armor but they can wear the Jewels. Thus far they cannot be evolved but they come in level 55 and 70. The level 70’s come from the Sailren raid.

You can obtain a dino egg for around anywhere from 500k to 900k in private shops or one could be found on primeval isle. Im not an expert on this as i got mine for free from random finds on Primeval island i am the proud owner of three. I got mine at lvl forty and it was 15 lvls higher than me.This means that it is my own personal super DD.

A dienonychus has terrifyingly low defense so you have to be extra careful but it can take out most mobs 15 to 1 lvls lower than it in 2-3 hits depending on how much hp it has.The most time i have seen in taking down an enemy is 7 hits on a Nos on alligator island with x2 hp and hightened p/m def.Obviously im not a very high level and so most of you probably won’t take this guide seriously but i hope you will think about getting one of these epic awesome litle DD’s. I was too late in getting a wolf (too high lvl to train one) and was not happy with the list of items on the deluxe fairy stone list :/ so when I found the eggs on primeval isle I was very impressed. The dienonychus can not possibly compare in def to great wolves or striders but has amazing potential as a damage deals, it comes at lvl 55 with a skill called tail swipe. This skill attacks a range of mobs in front of the character and deals insane damage to my level of mobs. The dino consumes food for wolves but cannot equip armor. It can however equip jewellery to boost its m-def. It is very similar to a wolf on attack steroids so pair well with tanks, I use a hate skill and then unleash the dino to eat the mob. Good luck to you who decide to raise this little scaly glass-jawed whirlwind

## Pet Quests

| Pet        | Min Level | Quest Name                  | Location         | Npc            | Quest Type    |
|------------|-----------|-----------------------------|------------------|----------------|---------------|
| Kookaburra | 24        | Help the Son                | Gludio           | Lundy          | 1 Time / Solo |
| Buffalo    | 25        | Help the Uncle!             | Dion             | Waters         | 1 Time / Solo |
| Cougar     | 26        | Help the Sister!            | Heine            | Cooper         | 1 Time / Solo |
| Wolf       | 15        | Get a Pet                   | Gludin           | Martin         | Repeatable    |
| Hatchling  | none      | Little Wing                 | Giran            | Cooper         | Repeatable    |
| Strider    | 45        | Little Wing’s Big Adventure | Hunter’s Village | Wiseman Cronos | Repeatable    |


## Tips:

- Always have plenty of the correct food on your pet. They will feed themselves if the food is in their inventory but if it dies from starvation it is gone forever  (rumor has it in Freya they won’t disappear from starving but can be rezzed in the 24 hour time period. Yay!) There is a thread that someone’s pet died from starvation even though it had the right food on it because there was another type of food in it’s inventory (I carry multiple pets with different food and haven’t had this problem yet) http://boards.lineage2.com/showthrea…light=pet+food
- If pet dies to mobs, you have only 24 hours to rez it or it will be gone forever.
- To target your pet to rez him or heal/buff or whatever, you click on his name bar, or if for some reason the bar is gone, you can press the shift key and hold it while you click the pet’s dead body. You can also type /target petname if he has a name.
- If your toon doesn’t have the rez skill, carry the plain resurrection scrolls from the grocery in case they die from mobs when they are low level. There are blessed pet rez scrolls available from the Dawn Priest if you are signed but they cost a bit of AA and no point until they are higher level.
- Get gear that is in your price range, you don’t need top gear, anything helps imo  Some gear can be upgraded but it seems more expensive than buying the new gear. The only pet that needs a weapon is the wolf, other pets aren’t really good fighters. Armor and a Jewel helps for all the pets.

## Keep them Safe ~ Pets no longer have to fight to gain xp. 
I find it best to hunt in an area where none of the mobs are aggro and let the pet follow or park them in a safe spot and pull to them but you have to learn the tricks to keeping the baby pets from following you to heal and buff.
Example, don’t run into a pack of aggro mobs if your health is low and the pet wants to chase to heal you. Use some heal pots if you need to. You can turn off his buffs for 5 minutes from one of his actions if necessary. If I am aoe’ing with a pet parked at a safe spot I will sometimes Alt-Click off his buffs and wait for him to rebuff before I pull if the time on them is short. Decent armor and jewel help, get the best you can afford. For mobs that AOE, if I am single mob hunting in a spot where I have to run around to get the mobs, I click the pets “Movement Mode” button to stop him just out of the AOE range.

If a wolf is near your level or above and has decent armor, weapon and jewelry you should be able to have him fight alongside you, just be sure to get the aggro first and put heal pots on the wolf if you don’t have a heal skill. Wolves will stay put if you click Movement Mode in their action window. Click again if you want them to follow you.

Wolves can be evolved to Great Wolves at level 55 via the Pet Manager. If you are in a clan that holds a Rune or Aden clan hall, you can make the 55 Great Wolf ridable. Otherwise it needs to be level 70 to become a ridable fenrir.

Wolf info and leveling guide: Lineage II Wolf Pet Guide Thanks Melkhyn! (some of the wolf tips in Mel’s guide are good for other pets too

The best armor and weapons can be found in Giran and Rune I believe. Correct me if wrong.

Source: https://guidescroll.com/2011/09/lineage-ii-mini-pet-guide/