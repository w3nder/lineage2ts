# Who are Maguens?
A new type of pets added in Freya update. There are two types: Maguen and Elite Maguen. Maguens (not pets) may appear in Seed of Annihilation zone while you’re hunting. They have their own energy - Plasma – and appear in any of three locations: Bistakon, Reptilikon, Cokrakon). Players collecting Maguen Plasma can get unique buffs or a new pet.

Go to Seed of Annihilation zone, at the entrance find NPC Nemo. He’ll tell you about Maguens, about how their magic works and how to obtain them. TakeItem 15487.jpg Maguen Plasma Collector from Nemo to collect Maguen Plasma, and get instructions about how to use it.

While hunting in Seed of Annihilation zone, you’ll be meeting Maguens. They are not aggressive, and there’s a whirl of energy above their heads. This whirl of energy constantly changes its colour. The type of plasma you gather depends on the colour of the whirl. When players use Item 15487.jpg Maguen Plasma Collectors , Maguens throw balls of plasma at them.

If you got plasma from a red whirl, your character may receive the following buffs:
Skill 6367 Maguen Plasma - Bistakon
Skill 6343 Maguen Plasma - Power - You are once again blessed by the Wild Maguen, who further increases your M. Atk. and P. Atk.

If you got plasma from a green whirl, your character may receive the following buffs:
Skill 6368 Maguen Plasma - Cokrakon
Skill 6365 Maguen Plasma - Speed - You are once again blessed by the Wild Maguen, who further increases your Speed, Atk. Spd. and Casting Speed.

If you got plasma from a blue whirl, your character may receive the following buffs:
Skill 6369 Maguen Plasma - Reptilikon
Skill 6366 Maguen Plasma - Critical - You are once again blessed by the Wild Maguen, who further increases your Critical Rate and Critical Damage.

Maguen buffs can be cancelled. Attention! If your character receives a buff which doesn’t match the hunting zone (like Bistakon buff in Reptilikon zone), they receive increased damage from mobs. If a buff matches the zone, the damage is decreased.

While gathering plasma, you can get Item 15490 Maguen Collection Box or Item 15491 Elite Maguen Collection Box,
where you can find Item 15488 Maguen Pet Collar and Item 15489 Elite Maguen Pet Collar , accordingly.

### Elite Maguen Pet 
Collar Elite Maguen Pet Collar	Necklace that summons an Elite Maguen. The summoned Maguen can eat wolf-type feed, and Takes a part of the XP gained by the Pet master.
### Maguen Pet Collar 
Maguen Pet Collar	Necklace that summons a Maguen. The summoned Maguen can eat wolf-type feed, and Takes a part of the XP gained by the Pet master.

# Bringing up Maguens
The process of bringing up Maguens is the same as for any other type of pets. Maguens receive XP when their master hunts and kills mobs, consumes around 27% of their master’s XP, can use Spiritshots and Beast Soulshots. Death penalty is around 4% of XP. If you don’t resurrect the pet, it disappear forever after 24 hours. Maguens eat Item 2515.jpg Wolf Food .

# Maguen skills
Maguens have similar skills, but Elite Maguen's skills are more powerful.

Maguen Strike - Speed is decreased by 50%. Duration: 10 sec.
Maguen Power Strike - Speed is decreased by 90%. Duration: 10 sec. (only Elite Maguens have such a skill)
Maguen Speed Walk - Increases Speed by 40. Duration: 20 min.
Elite Maguen Speed Walk - Increases Speed by 45. Duration: 20 min.
Maguen Recall – Teleports the master to Seed of Annihilation
Maguen Party Return – Teleports all party members to Seed of Annihilation. (only Elite Maguens have such a skill)

Source: https://l2wiki.lineage2splendor.com/Maguens/