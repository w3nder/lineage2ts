# Auction System

With the Kamael update, Item Broker NPCs appeared in the cities of Rune, Aden, and Giran conducting auctions. Mondays: Town of Aden Wednesdays: Rune Township Fridays: Town of Giran Auction Time (European servers): 21:00–00:00 GMT+3

## Behavior Rules
- Players place bids through the interface, and the winner is the one whose bid is the highest at the end of the auction.
- Players can place the following bids: x2, +50%, +10%, +5%, exact amount.
- When a player places a bid, they will see an envelope icon on their screen and a message in the system chat.
- If a bid is outbid, the envelope icon disappears, and the player needs to collect their bid from the NPC to place a new bid (if the player does not collect the bid, the money will disappear after a week).
- If the maximum bid is placed in the last 10 minutes of the auction, the auction is extended by 5 minutes.
- If more than 2 bids are placed within these 5 minutes, the auction is extended for an additional 3 minutes.
- The auction winner retrieves their item from the storage.

## Auction Items (Gracia Final update)

- All types of Improved NG weapons (NG-grade weapons sharpened to +15)
- High/Top LS 80–82
- SA 15–16 of all colors

### Spellbooks:

- Archers Will
- Fighters Will
- Magician Will
- Protection of Rune
- Protection of Elemental
- Protection of Alignment

### Head Accessories without skills:

- Refined Chicken Hat
- Refined Wizard Hat
- Refined Black Feather Mask
- Refined Romantic Chapeau
- Refined Carnival Circlet
- Refined Clown Hat

Source: https://medium.com/@L2Top/auction-in-lineage-2-6b872ba5bb63