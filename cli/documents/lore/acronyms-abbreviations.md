# Lineage 2 Acronyms and Abbreviations

## Races
- DE – Dark Elf
- LE – Light Elf
- Dorf – Dwarf

## Classes
- DA – Dark Avenger
- TH – Treasure Hunter
- TK – Temple Knight
- DD – Damage Dealer (fighter class basically)
- HE – Hawkeye
- SE – Shilien Elder

## Items
- SOP – Stone of Purity
- SOE – Scroll of Escape
- SoE – Staff of Evil
- SLS – Samurai Longsword
- Ori – Orihakrun Ore
- Ore – Usually Orihakrun Ore
- Skin – Animal Skins
- FP – Full Plate Armor
- Brig – Brigadine Armor
- SoR – Sword of Revolution
- sor – Sword of Reflection
- SoS – Sword of Solidarity
- SoD – Sword of Damascus
- sod – Sword of Delusion
- SA – Special Ability / Weapon Bestowment

## Spells
- res – Resurrection Type Spell
- xp res – Resurrection Type Spell that gives back lost XP
- rez – same as res
- xp rez – same as xp res
- ww – Wind Walker

## Places
- GC – Giants Cave
- ToI – Tower of Insolence
- FoM – Forrest of Mirrors
- DP – Death Pass
- DI – Devil’s Isle
- AC – Abandoned Camp
- FT – Forgotten Temple
- Temple – Forgotten Temple
- OB – Orc Barracks
- Cruma – Cruma Tower
- CT – Cruma Tower
- FG – Forbidden Gateway
- DV – Dragon Valley
- RoD – Ruins of Despair
- RoA – Ruins of Agony
- IT – Ivory Tower

## Chat / Forum Terms
- AFK – away from keyboard
- WTS – want to sell
- WTB – want to buy
- WTT – want to trade
- lfg – looking for group
- rec – recommend this person (rate them)
- PK – player killer
- noob – newbie or someone new to the game or lacking skills
- KOS – kill on sight
- +++ – please give me
- ^^ – happy
- `>.<`– angry or frustrated
- T.T – crying
- IMO – in my opinion
- IMHO – in my honest opinion
- ROFL – roll on floor laughing
- LOL – laugh out loud (anyone ever accidentally use this in real life)
- woot – yay!
- kthxby – okay thank you bye, sarcastic avoid using unless wanting PK (see PK)
- PTS – Public Test Server

Source: https://guidescroll.com/2011/12/lineage-2-acronyms-and-abbreviations-list/