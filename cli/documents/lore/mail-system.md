# The mail system in Lineage 2

## There are 2 types of mail: Normal and Cash-on-Delivery (COD):

- Normal mail allows players to send messages to other players with/without attached items. When the letter is sent, the recipient will see an envelope icon. The sender pays the standard shipping cost.
- Cash-on-Delivery (COD) mail contains an attached item, and to receive it, the recipient must pay the sender the specified amount of adena.

## Sending Mail Procedure for sending mail:
- You can open the mail interface by pressing Alt+X and selecting the “mail” option, or by typing /mailbox in the chat.
- The maximum number of attachable items is 8, and the shipping cost depends on the total weight of the package.
- Sending and receiving mail is only possible in peaceful zones.
- Mail delivery time is 30 seconds.
- You can only cancel sending a letter before it is opened by the recipient or if the recipient has not claimed the attached item. If the mail is undelivered or unclaimed.
- You can select the type of mail in the mailbox interface.
- The sender of COD receives adena from the recipient directly into the inventory after the recipient clicks the “pay” button.

## Receiving Mail Procedure for receiving mail:
- Upon receiving mail, an envelope icon appears on the screen, and to open the letter, simply click on it. The mail icon appears slightly above the chat.
- The received item cannot be taken from the mail if there are no free slots in the inventory or if receiving the item would exceed the weight limit by more than 100%. For COD mail, the recipient receives the item only after clicking the “Pay” button, at which point the required amount of adena disappears from the inventory.

## Returning and Deleting Mail
- Normal mail without attachments cannot be returned. It can only be received or deleted. Before deleting it, you must open it.
- Mail with attachments can be returned or received, but it cannot be deleted.
- Unread mail is automatically returned to the sender after 15 days. Mail without attachments is deleted after 15 days and will not be returned to the sender.
- Unread mail with attachments is returned to the sender after 15 days, the item is returned to the personal warehouse, and the mail is deleted.
- Mail requiring payment is automatically canceled 12 hours after being sent.

Source: https://medium.com/@L2Top/the-mail-system-in-lineage-2-ec5cd3f7cedb