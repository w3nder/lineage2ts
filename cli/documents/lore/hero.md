# Heroes

- A player who has recorded the highest score in each class in the Grand Olympiad, a Player vs. Player tournament played among the Noblesse, is conferred the title of Hero. Heroes are awarded exclusive weapons, skills, abilities and a distinct aura.

## Hero Abilities

- Heroes are able to use weapons provided for their exclusive use. Once selected, weapons cannot be replaced. However, after one month, all weapons are automatically returned and Hero players will be able to select another weapon. The weapons are available from the Monument of Heroes and cannot be dropped, traded, enchanted, and/or destroyed. S Grade spiritshots, soulshots, and arrows are consumed.
- Heroes are able to chat in a special channel (%). This chat font color is sky blue and is displayed to all players on the server. Heroes are only able to speak in the global channel once every 10 seconds. It is possible to turn Hero Voice on and off in the chat message options.
- The names of Heroes are listed in the commemorative Monument of Heroes in each village.

# Hero Skills

- Hero skills may be acquired through the Grand Olympiad Manager upon becoming a Hero; however, you cannot use a Hero skill when in subclass status.

| Skill Name       | Description                                                                                                                                                                                                                                                                                                 |
|------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Heroic Miracle   | Instantly and significantly increases P.Def., M.Def., and resistance to buff canceling attacks as well as increases Speed. Consumes 40 Soul Ores.                                                                                                                                                           |
| Heroic Berserker | Temporarily yet significantly increases Accuracy, P. Atk., M. Atk., Atk. Spd., Casting Spd., Speed, resistance to buff canceling attacks and debuff attacks, and the effects of HP regeneration magic. Significantly decreases P. Def., M. Def., and Evasion while the effect lasts. Consumes 40 Soul Ores. |
| Heroic Valor     | Temporarily yet significantly increases P. Atk., P.Def., and resistance to buff canceling attacks. Consumes 80 Spirit Ores.                                                                                                                                                                                 |
| Heroic Grandeur  | Instantly and significantly decreases nearby enemies' P. Def., M. Def., Evasion, and Shield defense probability, and increases the chance of negating the effects of magical damage, and blocks all physical and magic skill effects. Consumes 80 Spirit Ores.                                              |
| Heroic Dread     | Instantly instills fear into enemies and causes them to flee with increased Speed (momentum). Consumes 80 Spirit Ores.                                                                                                                                                                                      |

## Gaining Hero Abilities

- The Noblesse symbol will appear in Status window when the character is a Noblesse; the Hero symbol displays when the player is a Hero or on stand-by Hero status. The Hero gains Hero abilities when by visiting the Monument of Heroes, not just when the Noblesse has been ranked 1st in the Olympiad.
- If an individual who was conferred the title of Hero in a previous phase is awarded the title again, he must first be on stand-by and activate the Hero abilities by visiting the Monument of Heroes again.

## Determining the Hero

- Following a month-long PvP competition, the Hero is selected according to Olympiad points, and the Hero retains the title throughout the duration of the next month’s competition. The Hero is determined on the first day of every month.
- The Hero will be the individual who has accumulated the most Olympiad points according to their main class, has competed more than 9 times in that period, and claimed at least 1 victory. The Hero will be placed in stand-by mode and exclusive abilities and weaponry are removed after the one month period.
- For example, even if a player has accumulated the most Olympiad points in the current period, if he has competed less than 9 times or has never claimed a victory, he is ineligible to become a Hero; it is possible that periods may pass without a Hero being designated.
- When two players with the same class have accumulated identical Olympiad points, the one who has competed most rounds will be designated Hero.
- When there are multiple 1st place winners from each class, the character that has won the most games becomes a Hero. When the number of games won is the same, then the character who has the highest winning percentage becomes a Hero. When Olympiad points, number of wins, and winning percentage are all the same, then there will be no Hero in each class.

## Rank Confirmation

- All Noblesse have access to the top 10 ranking of the last period’s Olympiad results. Those who have competed on less than four occasions will be excluded from the ranking. In the list, the Olympiad point value, which determines the final standing, will not be revealed. Only the character names are listed on the ranking. In cases where players have accumulated identical points, up to 15 individuals with equal points can be displayed.