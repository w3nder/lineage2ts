#The 4 Sepulchers - how to survive guide

##Strategy:
Always have the dagger with silent walk to taunt mobs and lead them back to the knight, so the knight tanks, SPS sleeps.
Have the whole party move in silent move when you are proceeding into the next room.
To stay away from the magic mobs, read the following map

______door to the next room__________
[1 2]
| |
| |
| |
| |
| |
| |
[___________door from the last room____ ]

The mages, EE/bishop and prophet should always stay in the areas 1 or 2

The first 4 rooms are pretty straight forward,

For the first 2 rooms: mages stay in the [ ] area right in front of the doors so you won't magic mobs can't atk you, all the other fighters stand on the other side opposite of the mages. EE recharge yourself and others, bishop heals HP, SPS group sleeps all the mobs that are NEAR you. As soon as you get the key, proceed to the next room ASAP (there is a time limit).

For the 3rd room: Stay in the middle near the treasure chest, wait til that guy that keeps calling all of the names of the players in your party, wait for a bit for it to get all the aggro mobs, and you go back to one of the corners on the side of the door that you came in from, FINISH these mobs, do not leave until you get the key, once you have the key, have the whole party on silent move and get to the next door.

For the 4th room: I can't remember which mob you have to kill to get the key, I'll have to check once again to update this part, but once again key and you can move into the 5th room.

5th room is the hardest one, once all your players are standing in the middle, everyone's having a de-buff which healing from a party member has no effect. This part, still, bishop and EE stand at that [ ] area in front of the door while the others kill, they might kill in the middle of the room, while they are doing that, these players need to use HP potions to heal. That is before the Stone Statues come out. As soon as Stone Statues come out, everyone can be healed by party members. Get the key and proceed to the boss room.

Boss is pretty straight forward, have the SPS/EE/Prophet sleep+root the boss minions while everyone else kills the boss.

If EE's MP runs out, you are all screwed... so never NEVER let it run out.

I am playing on the Taiwan server so I really don't know the name of the mobs, I'll have to check the sites to fill those in, if you know them, please let me know and I'll update this post.

Quest rewards:

Each room you pass through (or be in) you'll get treasure chests, once you are out of these rooms, (either you warpped yourself or warpped by NPC for finishing the quest), you'll be teleported in Pilgrims Temple, the NPC in the middle of the room will help you open these chests, you can get lots of different materials from opening these chests including S grade weapon enchant, and S grade weapon parts (so far that's the best ones I know)

How to get the S grade weapon recipe you want: If you are also doing the "Relic from the Old Empire" Quest, and have 1000 quest items, run the 4 Sepulchers (just need 1) again, and you can pick the S grade weapon Rec you want

Source Link: https://forum.pmfun.com/viewtopic.php?p=1158&sid=d7116316eef7be059c2215dfa849dbb5#p1158