# Vitality Mechanics

## Vitality Consumption

- Vitality Points Consumed = ( ( (Exp) / (Level^2) )*100 ) / 9

Exp = Base exp gained from mob
Level = Mob level

### Example 1 1xHP Mob
Bone Grinder 12,903xp Level 70
( ( (12,903) / (70^2) )*100 ) / 9
Vit Consumed = 29 points.

### Example 2 5xHP Mob
Karik 32,922xp Level 70
( ( (32,922) / (70^2) )*100 ) / 9
Vit Consumed = 75 points.

This means even if you only gain 1 exp point your vitality ‘points’ will still change. 
If you kill a gremlin at level 80 and gain 1 exp point you will lose the same number of vitality points as you would if you were only level 1.
In the example, the Karik has approx. 5x the HP as the Bone Grinder and removes 2.5x the vitality and only gives 2.5x the experience. 
With this said, unless you’re in a full group for the XP bonus, don’t hunt anything with any multiplier – go for the 1x mobs if you’re specifically looking to make good use of your vitality.

## Vitality Points

- Level 0 – 1 contains 240 points.
Those 240 points take 1 hour of being logged out to regain.
- Level 1 – 2 contains 1,560 points.
Those 1,560 points take 6hr30m of being logged out to regain.
- Level 2 – 3 contains 12,800 points.
Those 12,800 points take 53hr20m of being logged out to regain.
- Level 3 – 4 contains 3,600 points.
Those 1,560 points take 15 hours of being logged out to regain.
- Level 4 – Max contains 1,800 points.
Those 1,800 points take 7hr30m of being logged out to regain

Max vitality points = 20,000 taking a total 83hr 20m to regain (approx. 3.5 days from 0 – 20,000).

- You get 1 point regained for every minute in a peace zone.
- You get 4 points regained every minute when logged out.
- From lvl 76+ the vitality consumption rate is 1.5x normal. From 79 its 2x normal.
- Vitality herbs reverse the process of killing mobs. No additional bonus.
- Parties increase your vitality drop too but this hasn’t been worked out.
- Raids generally return 20-30 points, not calculated exactly yet.
- Additional exceptions which aren’t listed apply.

Source: https://guidescroll.com/2011/09/lineage-ii-vitality-guide/

## Vitality System:
Vitality system is a way for characters to earn additional XP through hunting. 
There is a grey bar beneath your MP bar that shows the level and the amount of vitality your character has
There are 4 stages of vitality, each stage corresponding to a different XP bonus (note: after reaching 75 the vitality bonus decreases a bit)

H5 Vitality and EXP
- Level 1 - 150% increase in experience gain.
- Level 2 - 200% increase in experience gain.
- Level 3 - 250% increase in experience gain.
- Level 4 - 300% increase in experience gain.

- vitality is consumed when hunting normal monsters or kamaloka (x3,x6)
- vitality is replenished when hunting raids,making Pailaka quests,using vitality replenishing herbs (drops from mobs), being logged off, or sitting in town (but very slow)

Source: https://l2vaevictis.forumotion.net/t689-h5-vitality-and-exp