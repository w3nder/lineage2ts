# Mentoring system

The mentoring system in Lineage 2 is a feature designed to help new players (mentees) and provide benefits to experienced players (mentors). Here’s a detailed overview:

## Becoming a Mentor or Mentee

To participate, characters must meet specific criteria:
- Mentors: Must be level 105 or higher with the Awakening (3rd Liberation for Ertheia) completed. A mentor can have up to three mentees at once.
- Mentees: Must be level 104 or lower without completing the Awakening. Each mentee can have only one mentor.

## Benefits of the Mentoring System
- Mentor Benefits: While the mentee is online, the mentor receives a 5% boost in acquired XP and SP. Mentors also gain Mentee’s Marks as their mentees level up, which can be exchanged for items at the Mentor Guide shop.
- Mentee Benefits: When the mentor is online, mentees receive buffs equivalent to Iss Enchanter’s skills and a special summoning skill to call their mentor for assistance.

## Forming a Mentorship Contract
- Open the contacts window (Alt + Y) and navigate to the Mentoring tab.
- Add a mentee by typing their name and clicking “Add to list.”
- If the mentee accepts the invitation, the contract is activated.

## Contract Rules and Buffs
- Contracts can be broken at any time without needing the other party’s consent. If a mentor breaks the contract, they face a two-day penalty before forming a new one. Mentees can find a new mentor immediately.
- Mentees get a special headset item upon contract formation and Graduation Box upon reaching level 105, at which point they graduate and the contract ends.
- Level-Based Rewards for Mentors
- Level 85: 5,000 Mentee’s Marks
- Level 99: 10,000 Mentee’s Marks
- Level 103: 25,000 Mentee’s Marks
- Level 105: 60,000 Mentee’s Marks

This mentoring system enriches the gameplay experience by fostering collaboration between new and veteran players, encouraging mentorship, and providing mutual benefits.

Source : https://medium.com/@L2Top/comprehensive-guide-to-the-mentoring-system-in-lineage-2-395048ebe7c0