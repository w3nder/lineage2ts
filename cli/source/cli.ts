import { Lineage2CLI } from './ui/Lineage2CLI'
import { Command } from 'commander'

const program = new Command()

program
    .option( '--provision-database', 'Create service databases' )
    .option( '--provision-datapack', 'Create datapack assets' )
    .option( '--provision-geopack', 'Create geopack assets' )
    .option( '--database-type', 'Specify database type: sqlite' )
    .option( '--database-destination', 'Database tables for: login, game, both' )
    .option( '--datapack-name', 'Specify file name for datapack' )
    .option( '--geopack-name', 'Specify file name for geopack' )

program.parse( process.argv )

new Lineage2CLI( program ).start().catch( console.error )