import _ from 'lodash'
import { RoaringBitmap32, SerializationFormat } from 'roaring'
import { CircleGeometryParameters, GeometryType, IGeometryGenerator } from './Interfaces'

/*
    Computing polygon points:
    - compute circumference of circle
    - divide by 3 to get amount of points you get along circle line

    Example:
    - radius of 20
    - circumference is 125.6637
    - division by 3 gets you Math.floor(result) of 41
 */

/*
    Implementation left out for future testing/reference.
 */
function unwrap( value: number ) : any {
    let x = 0x0000FFFF & value
    let y = value >> 16

    return { x, y }
}

export class DropCircle implements IGeometryGenerator {
    name: string
    minRadius: number
    maxRadius: number
    radiusIncrement: number
    data: RoaringBitmap32 = new RoaringBitmap32()
    distanceBetweenPoints: number

    constructor( name: string, minRadius: number, maxRadius: number, radiusIncrement: number, pointDistance : number = 5 ) {
        this.name = name
        this.minRadius = minRadius
        this.maxRadius = maxRadius
        this.radiusIncrement = radiusIncrement
        this.distanceBetweenPoints = pointDistance

        if ( this.minRadius === this.maxRadius ) {
            this.data.addMany( this.createCirclePoints( this.minRadius, this.maxRadius, this.maxRadius ) )
            return
        }

        for ( let radius = this.minRadius; radius <= this.maxRadius; radius = radius + this.radiusIncrement ) {
            this.data.addMany( this.createCirclePoints( radius, this.maxRadius, this.maxRadius ) )
        }
    }

    private createCirclePoints( radius: number, centerX: number, centerY: number ): Array<number> {
        let circumference = Math.PI * 2 * radius
        let amount = Math.floor( circumference / this.distanceBetweenPoints )

        return _.times( amount, ( index: number ): number => {
            let ratio = ( index * Math.PI * 2 ) / amount
            let x = centerX + Math.round( radius * Math.cos( ratio ) )
            let y = centerY + Math.round( radius * Math.sin( ratio ) )

            return x + ( y << 16 )
        } )
    }

    generatePoints() : Buffer {
        // @ts-ignore
        return this.data.serialize( SerializationFormat.croaring )
    }

    getParameters() : CircleGeometryParameters {
        return {
            maxRadius: this.maxRadius,
            minRadius: this.minRadius,
            stepIncrement: this.radiusIncrement,
        }
    }

    getName(): string {
        return this.name
    }

    getSize(): number {
        return this.data.size
    }

    getType(): GeometryType {
        return GeometryType.PointCircle
    }
}