export const enum GeometryType {
    PointCircle = 'pointCircle',
    PointSquare = 'pointSquare',
    SpawnTerritory = 'spawnTerritory'
}

export type GeometryParameters = Record<string, number>

export interface CircleGeometryParameters extends GeometryParameters {
    minRadius: number
    maxRadius: number
    stepIncrement: number
}

export interface SquareGeometryParameters extends GeometryParameters {
    xLength: number
    yLength: number
    xIncrement: number
    yIncrement: number
}

/*
    Special use for spawn territories which are not part of normal
    geometries (drops/moves), however are used as geometry points for spawn position.
 */
export interface TerritoryPointsParameters extends GeometryParameters {
    minX: number
    minY: number
    maxX: number
    maxY: number
    distance: number
    centerX: number
    centerY: number
    size: number
}

export interface IGeometryGenerator {
    getName() : string
    generatePoints() : Buffer
    getParameters() : GeometryParameters
    getType() : GeometryType
    getSize() : number
}