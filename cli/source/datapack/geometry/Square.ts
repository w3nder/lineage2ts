import { GeometryType, IGeometryGenerator, SquareGeometryParameters } from './Interfaces'
import { RoaringBitmap32, SerializationFormat } from 'roaring'

export class Square implements IGeometryGenerator {
    private data: RoaringBitmap32 = new RoaringBitmap32()
    private readonly name : string

    private readonly xLength: number
    private readonly yLength: number
    private readonly xIncrement: number
    private readonly yIncrement: number

    constructor( name: string, xLength: number, yLength: number, xIncrement: number, yIncrement: number ) {
        this.name = name
        this.xLength = xLength
        this.yLength = yLength
        this.xIncrement = xIncrement
        this.yIncrement = yIncrement

        for ( let xPosition = 0; xPosition < xLength; xPosition = xPosition + xIncrement ) {
            for ( let yPosition = 0; yPosition < yLength; yPosition = yPosition + yIncrement ) {
                this.data.add( xPosition + ( yPosition << 16 ) )
            }
        }
    }

    generatePoints(): Buffer {
        // @ts-ignore
        return this.data.serialize( SerializationFormat.croaring )
    }

    getName(): string {
        return this.name
    }

    getParameters(): SquareGeometryParameters {
        return {
            xLength: this.xLength,
            yLength: this.yLength,
            xIncrement: this.xIncrement,
            yIncrement: this.yIncrement
        }
    }

    getSize(): number {
        return this.data.size
    }

    getType(): GeometryType {
        return GeometryType.PointSquare
    }
}