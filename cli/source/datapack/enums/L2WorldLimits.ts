export const enum L2MapTile {
    XMin = 11,
    YMin = 10,
    XMax = 26,
    YMax = 26,

    /*
        Same value for size, however used differently depending on situation to avoid usage of Math.floor for division.
     */
    Size = 32768,
    SizeShift = 15
}

export const enum L2WorldLimits {
    MinX = ( L2MapTile.XMin - 20 ) * L2MapTile.Size,
    MaxX = ( L2MapTile.XMax - 19 ) * L2MapTile.Size,

    MinY = ( L2MapTile.YMin - 18 ) * L2MapTile.Size,
    MaxY = ( L2MapTile.YMax - 17 ) * L2MapTile.Size,
}