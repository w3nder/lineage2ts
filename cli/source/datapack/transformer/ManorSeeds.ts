import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'manor_seeds': 'data/seeds.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'cropId',
    'seedId',
    'castleId',
    'matureId',
    'rewardOne',
    'rewardTwo',
    'alternative',
    'level',
    'seedLimit',
    'cropLimit'
]

const query = `INSERT INTO manor_seeds (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`


class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let objects = _.get( parsedData, 'list.castle' )
        _.each( _.castArray( objects ), ( item: any ) => {
            let castleId = item.id

            _.each( item.crop, ( crop: any ) => {
                itemsToInsert.push( [
                    crop.id,
                    crop.seedId,
                    castleId,
                    crop[ 'mature_Id' ],
                    crop[ 'reward1' ],
                    crop[ 'reward2' ],
                    _.toString( crop[ 'alternative' ] ),
                    crop.level,
                    crop[ 'limit_seed' ],
                    crop[ 'limit_crops' ]
                ] )
            } )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Manor Seeds'
    }
}

export const ManorSeedsTransformer = new Transformer( files, parserOptions )