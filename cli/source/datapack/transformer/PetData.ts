import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import aigle from 'aigle'
import { TransformerOperations } from '../helpers/TransformerOperations'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'
import _ from 'lodash'

const rootDirectory = pkgDir.sync( __dirname )
const fileDirectory = 'data/stats/pets'

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true,
}

const columns = [
    'id',
    'itemId',
    'indexId',
    'set_json',
    'skills_json',
    'levels_json',
    'minimumLevel',
]

const query = `INSERT INTO pet_data (${ columns.join( ',' ) })
               values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        const files = searchFiles( 'xml', `${ rootDirectory }/${ fileDirectory }` )

        return aigle.resolve( files ).eachSeries( async ( fileName: string ) => {
            transformer.lastFileProcessed = fileName
            let xmlData = await fs.readFile( fileName )
            let parsedData = await this.getParser().parse( xmlData.toString(), transformer.parserOptions )

            return engine.insertMany( transformer.getQuery(), transformer.transformData( parsedData ) )
        } )
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let petData = _.get( parsedData, 'pets.pet' )

        if ( !_.isArray( petData ) ) {
            petData = [ petData ]
        }

        _.each( petData, ( item: any ) => {
            let { id, itemId, index } = item

            if ( item.set ) {
                item.set = _.reduce( _.castArray( item.set ), ( properties: any, parameter: any ) => {

                    if ( parameter.name === 'food' ) {
                        properties[ parameter.name ] = _.toString( parameter.val ).split( ';' ).map( ( value ) => parseInt( value, 10 ) )
                    } else {
                        properties[ parameter.name ] = parameter.val
                    }

                    return properties
                }, {} )
            }

            if ( _.has( item, 'skills.skill' ) ) {
                item.skills = item.skills.skill
            }

            if ( _.has( item, 'stats.stat' ) ) {
                if ( !_.isArray( item.stats.stat ) ) {
                    item.stats.stat = [ item.stats.stat ]
                }

                item.stats = _.map( item.stats.stat, ( statItem: any ) => {
                    let { level } = statItem
                    let properties = _.reduce( statItem.set, ( properties: any, parameter: any ) => {
                        properties[ parameter.name ] = parameter.name === 'speed_on_ride' ? parameter : parameter.val
                        return properties
                    }, {} )

                    properties.level = level

                    return properties
                } )

            }

            itemsToInsert.push( [
                id,
                itemId,
                index,
                TransformerOperations.getJson( item.set ),
                TransformerOperations.getJson( item.skills ),
                TransformerOperations.getJson( item.stats ),
                _.min( _.map( item.stats, 'level' ) ),
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Pet Data'
    }
}

export const PetDataTransformer = new Transformer( null, parserOptions )