import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'enchant_skill_groups': 'data/enchantSkillGroups.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return 'INSERT into enchant_skill_groups (groupId, level, adena, exp, sp, rates_json) values (?,?,?,?,?,?)'
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.group' ), ( group: any ) => {
            let { id } = group
            group.enchant = _.castArray( group.enchant )

            _.each( group.enchant, ( item: any ) => {
                let rates = _.times( 24, ( index: number ) => {
                    return _.get( item, `chance${index + 76}`, 0 )
                } )

                itemsToInsert.push( [
                    id,
                    item.level,
                    item.adena,
                    item.exp,
                    item.sp,
                    TransformerOperations.getJson( rates )
                ] )
            } )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Enchant Skill Groups'
    }
}

export const EnchantSkillGroupsTransformer = new Transformer( files, parserOptions )