import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'
import { TransformerOperations } from '../helpers/TransformerOperations'

const files = {
    'fishing_rods': 'data/stats/fishing/fishingRods.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'id',
    'itemId',
    'level',
    'name',
    'damage'
]

const query = TransformerOperations.createQuery( 'fishing_rods', columns )


class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let fishItems = _.get( parsedData, 'list.fishingRod' )
        _.each( _.castArray( fishItems ), ( item: any ) => {

            itemsToInsert.push( [
                item.fishingRodId,
                item.fishingRodItemId,
                item.fishingRodLevel,
                item.fishingRodName,
                item.fishingRodDamage,
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Fishing Rods'
    }
}

export const FishingRodsTransformer = new Transformer( files, parserOptions )