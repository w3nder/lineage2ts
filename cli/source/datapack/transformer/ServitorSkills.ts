import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'

const parseOptions = {
    delimiter: ',',
}

const query = TransformerOperations.createQuery( 'servitor_skills', [
    'npcId',
    'skillId',
    'skillLevel'
] )

class ServitorSkills extends BaseCsvTransformer {
    getName(): string {
        return 'Servitor Skills'
    }

    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            return [
                item.npcId,
                item.skillId,
                item.skillLevel
            ]
        } )
    }
}

export const ServitorSkillsTransformer = new ServitorSkills( 'overrides/data/csv/servitorSkills.csv', parseOptions )