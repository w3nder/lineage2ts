import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'enchant_item_scrolls': 'data/enchantItemData.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'id',
    'targetGrade',
    'bonusRate',
    'maxEnchant',
    'scrollGroupId',
    'items_json'
]

const query = TransformerOperations.createQuery( 'enchant_item_scrolls', columns )


class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let objects = _.get( parsedData, 'list.enchant' )
        _.each( _.castArray( objects ), ( item: any ) => {
            let ids = item.item ? _.map( _.castArray( item.item ), 'id' ) : null

            itemsToInsert.push( [
                item.id,
                item.targetGrade,
                item.bonusRate,
                item.maxEnchant,
                item.scrollGroupId,
                TransformerOperations.getJson( ids )
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Enchant Item Scrolls'
    }
}

export const EnchantItemScrollsTransformer = new Transformer( files, parserOptions )