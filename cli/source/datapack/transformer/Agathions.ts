import { TransformerOperations } from '../helpers/TransformerOperations'
import fs from 'fs/promises'
import parser from 'csvtojson'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerProcess } from '../helpers/TransformerProcess'
import pkgDir from 'pkg-dir'

const parseOptions = {
    delimiter: ',',
}

const rootDirectory = pkgDir.sync( __dirname )

const query = TransformerOperations.createQuery( 'agathions', [
    'npcId',
    'itemId',
    'energy',
    'targetType',
    'properties_json'
] )

class AgathionData implements TransformerProcess {
    operationName: string
    dataFileName: string = 'overrides/data/csv/agathions.csv'

    getName(): string {
        return 'Agathions'
    }

    getQuery(): string {
        return query
    }

    async getCsvFile( fileName: string ) : Promise<Array<unknown>> {
        let fileContents = await fs.readFile( `${rootDirectory}/${fileName}` )
        return parser( parseOptions ).fromString( fileContents.toString() )
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        this.operationName = 'Reading csv data'
        let agathionData = await this.getCsvFile( this.dataFileName )
        let npcReferences = await this.getCsvFile( 'overrides/data/csv/npcReferenceIds.csv' )

        this.operationName = this.dataFileName
        return engine.insertMany( this.getQuery(), this.transformData( agathionData, npcReferences ) )
    }

    transformData( agathionItems: Array<unknown>, npcReferences: Array<unknown> ): Array<Array<any>> {
        let npcReferenceMap = npcReferences.reduce( ( finalMap: Record<string, number>, item: any ) : Record<string, number> => {

            finalMap[ item.referenceId ] = parseInt( item.npcId )

            return finalMap
        }, {} ) as Record<string, number>

        return agathionItems.map( ( item: any ) => {
            return [
                npcReferenceMap[ item.npcName ],
                item.itemId === '' ? null : parseInt( item.itemId ),
                item.energy === '' ? null : parseInt( item.energy ),
                item.targetType === 'by_skill' ? null : item.targetType,
                item.propertiesJson
            ]
        } )
    }

    getLastProcessedOperation(): string {
        return this.operationName
    }
}

export const AgathionsTransformer = new AgathionData()