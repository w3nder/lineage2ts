import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'category_data': 'data/categoryData.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    getQuery( tableName: string ): string {
        return `INSERT into ${ tableName } (name, ids_json) values (?,?)`
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.category' ), ( item: any ) => {
            let { name } = item
            let ids = _.castArray( item.id )
            itemsToInsert.push( [
                name,
                TransformerOperations.getJson( ids )
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Category Data'
    }
}

export const CategoryDataTransformer = new Transformer( files, parserOptions )