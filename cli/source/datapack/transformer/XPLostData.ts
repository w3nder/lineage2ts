import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'xp_lost': 'data/stats/chars/playerXpPercentLost.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'level',
    'value'
]

const query = `INSERT INTO xp_lost (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let objects = _.get( parsedData, 'list.xpLost' )
        _.each( _.castArray( objects ), ( item: any ) => {
            itemsToInsert.push( [
                item.level,
                item.val
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'XP Lost Data'
    }
}

export const XPLostDataTransformer = new Transformer( files, parserOptions )