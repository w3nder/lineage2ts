import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'
import { TransformerOperations } from '../helpers/TransformerOperations'

const files = {
    'augmentation_skillmap': 'data/stats/augmentation/augmentation_skillmap.xml',
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true,
}

const columns = [
    'id',
    'skillId',
    'skillLevel',
    'type',
]

const query = TransformerOperations.createQuery( 'augmentation_skills', columns )

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let augmentations = _.get( parsedData, 'list.augmentation' )
        _.each( _.castArray( augmentations ), ( item: any ) => {
            itemsToInsert.push( [
                item.id,
                item.skillId.val,
                item.skillLevel.val,
                item.type.val,
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Augmentation Skills'
    }
}

export const AugmentationSkillsTransformer = new Transformer( files, parserOptions )