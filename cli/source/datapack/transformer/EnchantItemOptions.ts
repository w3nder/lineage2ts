import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'enchant_item_options': 'data/enchantItemOptions.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return 'INSERT into enchant_item_options (itemId, level, options_json) values (?,?,?)'
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.item' ), ( item: any ) => {
            let { id } = item

            _.each( item.options, ( optionData: any ) => {
                let { level, option1, option2, option3 } = optionData

                itemsToInsert.push( [
                    id,
                    level,
                    TransformerOperations.getJson( [ option1 ?? 0, option2 ?? 0, option3 ?? 0 ] )
                ] )
            } )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Enchant Item Options'
    }
}

export const EnchantItemOptionsTransformer = new Transformer( files, parserOptions )