import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'door_data': 'data/doors.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const query = TransformerOperations.createQuery( 'door_data', [
    'id',
    'name',
    'properties_json'
] )

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.door' ), ( item: any ) => {
            let { id, name } = item

            let count = 1
            let allValues = []
            while ( count < 5 ) {
                let value = _.split( _.get( item, `node${count}` ), ',' ).map( value => _.parseInt( value ) )
                allValues.push( value )
                _.unset( item, `node${count}` )

                count++
            }

            let [ nodeX, nodeY ] = _.zip( ...allValues )
            _.assign( item, { nodeX, nodeY } )

            delete item.id
            delete item.name

            if ( item.pos ) {
                item.pos = item.pos.split( ';' ).map( value => parseInt( value ) )
            }

            itemsToInsert.push( [
                id,
                name,
                TransformerOperations.getJson( item )
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Door Data'
    }
}

export const DoorDataTransformer = new Transformer( files, parserOptions )