import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'

const parseOptions = {
    delimiter: ',',
}

const query = TransformerOperations.createQuery( 'respawn_points', [
    'name',
    'pointsJson',
    'pvpJson',
    'bannedRaceJson',
    'bbs',
    'messageId',
    'mapsJson'
] )

class RespawnPoints extends BaseCsvTransformer {
    getName(): string {
        return 'Respawn Points'
    }

    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return parsedData.map( ( item: any ) => {
            let bannedRace
            if ( item.bannedRace ) {
                let [ race, location ] = item.bannedRace.split( ';' )
                bannedRace = {
                    race,
                    location
                }
            }

            let maps
            if ( item.maps ) {
                maps = JSON.parse( item.maps ).map( ( map : string ) => {
                    let [ x, y ] = map.split( ';' ).map( value => parseInt( value, 10 ) )

                    return {
                        x,
                        y
                    }
                } )
            }

            return [
                item.name,
                item.pointsJson,
                item.pvpJson,
                TransformerOperations.getJson( bannedRace ),
                parseInt( item.bbs ) ?? 0,
                parseInt( item.location ) ?? 0,
                TransformerOperations.getJson( maps )
            ]
        } )
    }
}

export const RespawnPointsTransformer = new RespawnPoints( 'overrides/data/csv/respawnPoints.csv', parseOptions )