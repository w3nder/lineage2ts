import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'levelup_crystal_npcs': 'data/levelUpCrystalData.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const absorbActionTypeTranslation = {
    LAST_HIT: 'lastHitPlayer',
    FULL_PARTY: 'eachPlayerInParty',
    PARTY_ONE_RANDOM: 'randomPlayerInParty',
    PARTY_RANDOM: 'randomPlayerPartyChance'
}

function extractChanceLevels( chanceItem: any ) : Array<number> {
    if ( chanceItem.levelList ) {
        return _.map( _.split( chanceItem.levelList, ',' ), ( value: string ) => {
            return _.parseInt( value.trim() )
        } )
    }

    if ( !chanceItem.maxLevel || !_.isNumber( chanceItem.maxLevel ) ) {
        throw new Error( `max level needs to be a number : ${chanceItem.maxLevel}` )
    }

    return _.range( chanceItem.maxLevel + 1 )
}

function prepareChanceDetails( chanceItem: any ) : any {
    let outcome = {
        chance: chanceItem.chance,
        requiresSkill: _.get( chanceItem, 'skill', false ),
        type: absorbActionTypeTranslation[ _.get( chanceItem, 'absorbType', 'LAST_HIT' ) ],
        levels: extractChanceLevels( chanceItem )
    }

    return outcome
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return 'INSERT OR REPLACE into levelup_crystal_npcs (npcId, chances_json) values (?, ?)'
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let items = _.get( parsedData, 'list.npc.item' )

        return _.map( _.castArray( items ), ( item: any ) => {
            let { detail } = item
            return [
                item.npcId,
                TransformerOperations.getJson( _.map( _.castArray( detail ), prepareChanceDetails ) )
            ]
        } )
    }

    getName(): string {
        return 'Levelup Crystal Npcs'
    }
}

export const LevelupCrystalNpcsTransformer = new Transformer( files, parserOptions )