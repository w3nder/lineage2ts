import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'skill_learn': 'data/skillLearn.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return `INSERT into skill_learn (npcId, classIds_json)
                values (?, ?)`
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.npc' ), ( item: any ) => {
            let { id, classId } = item

            itemsToInsert.push( [
                id,
                TransformerOperations.getJson( classId )
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Skill Learn Data'
    }
}

export const SkillLearnTransformer = new Transformer( files, parserOptions )