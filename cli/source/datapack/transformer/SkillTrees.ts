import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerOperations } from '../helpers/TransformerOperations'
import aigle from 'aigle'
import _ from 'lodash'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'

const rootDirectory = pkgDir.sync( __dirname )
const fileDirectory = 'data/skillTrees'

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'type',
    'classId',
    'parentClassId',
    'skills_json'
]

const query = `INSERT INTO skill_trees (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        const files = searchFiles( 'xml', `${ rootDirectory }/${ fileDirectory }` )

        return aigle.resolve( files ).eachSeries( async ( fileName: string ) => {
            transformer.lastFileProcessed = fileName
            let xmlData = await fs.readFile( fileName )
            let parsedData = await this.getParser().parse( xmlData.toString(), transformer.parserOptions )

            return engine.insertMany( transformer.getQuery(), transformer.transformData( parsedData ) )
        } )
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let skillTrees = _.get( parsedData, 'list.skillTree' )

        if ( !_.isArray( skillTrees ) ) {
            skillTrees = [ skillTrees ]
        }

        _.each( skillTrees, ( item: any ) => {
            let { type, classId, parentClassId } = item

            if ( item.skill ) {
                if ( !_.isArray( item.skill ) ) {
                    item.skill = [ item.skill ]
                }
            }

            itemsToInsert.push( [
                type,
                classId,
                parentClassId,
                TransformerOperations.getJson( item.skill )
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Skill Trees Data'
    }
}

export const SkillTreesTransformer = new Transformer( null, parserOptions )
