import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'
import * as fs from 'fs/promises'
import pkgDir from 'pkg-dir'
import parser from 'csvtojson'
import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'

const rootDirectory = pkgDir.sync( __dirname )

const query = 'INSERT INTO armor_sets (id, name, items_json, skills_json, attributes_json) values (?, ?, ?, ?, ?)'

function extractIds( value: string ) : Array<number> {
    if ( !value ) {
        return null
    }

    return value.split( ';' ).map( itemId => parseInt( itemId, 10 ) )
}

type PaperdollItems = Record<string, number | Array<number>>
function getPaperdollItems( item: Record<string, string> ) : PaperdollItems {
    return _.pickBy( {
        chest: parseInt( item.slotChest, 10 ),
        feet: extractIds( item.slotFeet ),
        gloves: extractIds( item.slotGloves ),
        head: extractIds( item.slotHead ),
        legs: extractIds( item.slotLegs ),
        shield: extractIds( item.slotLhand )
    }, _.identity )
}

class Transformer extends BaseCsvTransformer {
    itemNameMap: Record<string, string>
    skillReferenceMap: Record<string, string>

    getQuery(): string {
        return query
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let itemNames = await this.getCsvFile( 'overrides/data/csv/items/itemNames.csv' )
        let skillReferences = await this.getCsvFile( 'overrides/data/csv/skillReferenceIds.csv' )

        this.itemNameMap = itemNames.reduce( ( finalMap: Record<string, string>, item: any ) : Record<string, string> => {

            finalMap[ item.id ] = item.name

            return finalMap
        }, {} ) as Record<string, string>

        this.skillReferenceMap = skillReferences.reduce( ( finalMap: Record<string, string>, item: any ): Record<string, string> => {
            finalMap[ item.name ] = `${item.skillId}-${item.skillLevel}`

            return finalMap
        }, {} ) as Record<string, string>

        let fileContents = await fs.readFile( `${rootDirectory}/${this.fileName}` )
        let parsedData = await parser( this.parserOptions ).fromString( fileContents.toString() )

        return engine.insertMany( this.getQuery(), this.transformData( parsedData ) )
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return parsedData.map( ( item : Record<string, string> ) => {
            let items : PaperdollItems = getPaperdollItems( item )
            let skills = this.getSetSkills( item )
            let attributes : Record<string, number> = [ 'str', 'con', 'dex', 'int', 'men', 'wit' ].reduce( ( allAttributes: Record<string, number>, name : string ) : Record<string, number> => {
                let value = item[ name ] as string

                if ( value ) {
                    let amount = parseInt( value.split( ';' )[ 0 ], 10 )
                    if ( Number.isInteger( amount ) ) {
                        allAttributes[ name ] = amount
                    }
                }

                return allAttributes
            }, {} )

            return [
                parseInt( item.id, 10 ),
                this.createSimilarityName( _.flatten( Object.values( items ) ) ),
                TransformerOperations.getJson( items ),
                TransformerOperations.getJson( skills ),
                TransformerOperations.getJson( attributes ),
            ]
        } )
    }

    getName(): string {
        return 'Armor Sets'
    }

    getSetSkills( item: Record<string, string> ) : Record<string, string> {
        return _.pickBy( {
            set: this.extractSkill( item.setEffectSkill ),
            enchant6: this.extractSkill( item.setAdditional2EffectSkill ),
            shield: this.extractSkill( item.setAdditionalEffectSkill )
        }, _.identity )
    }

    extractSkill( value: string ) : string {
        if ( !value ) {
            return null
        }

        let skillData = this.skillReferenceMap[ value ]
        if ( !skillData ) {
            throw new Error( `Unable to find referenced skill with name=${value}` )
        }

        return skillData
    }

    createSimilarityName( itemIds: Array<number> ) : string {
        let nameChunks : Array<Array<string>> = itemIds.map( itemId => {
            let name = this.itemNameMap[ itemId ]
            if ( !name ) {
                throw new Error( `Unable to find existing name for itemId=${itemId}` )
            }

            return name.split( ' ' )
        } )
        let firstName = nameChunks[ 0 ]
        let otherNames = nameChunks.slice( 1 )

        let similarNames : Array<string> = []

        for ( const word of firstName ) {
            if ( similarNames.length === 0 && word.length < 4 ) {
                continue
            }

            if ( otherNames.every( nameChunks => nameChunks.includes( word ) ) ) {
                similarNames.push( word )
            }
        }

        let outcome = similarNames.join( ' ' )
        if ( outcome ) {
            return outcome
        }

        return 'Unknown'
    }
}

export const ArmorSetsTransformer = new Transformer( 'overrides/data/csv/items/itemSets.csv' )