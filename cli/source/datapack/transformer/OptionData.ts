import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import aigle from 'aigle'
import _ from 'lodash'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'

const rootDirectory = pkgDir.sync( __dirname )
const fileDirectory = 'data/stats/options'

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'id',
    'for_json',
    'active_skill_json',
    'passive_skill_json',
    'attack_skill_json',
    'magic_skill_json',
    'critical_skill_json',
]

const query = `INSERT INTO option_data (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`

const getJson = ( value: any ): string => {
    if ( _.isEmpty( value ) ) {
        return null
    }

    return JSON.stringify( value )
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        const files = searchFiles( 'xml', `${ rootDirectory }/${ fileDirectory }` )

        await aigle.resolve( files ).eachSeries( async ( fileName: string ) => {
            transformer.lastFileProcessed = fileName
            let xmlData = await fs.readFile( fileName )
            let parsedData = await this.getParser().parse( xmlData.toString(), transformer.parserOptions )

            return engine.insertMany( transformer.getQuery(), transformer.transformData( parsedData ) )
        } )
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        let options = _.get( parsedData, 'list.option' )

        if ( !_.isArray( options ) ) {
            options = [ options ]
        }

        _.each( options, ( item: any ) => {
            let { id } = item

            itemsToInsert.push( [
                id,
                getJson( item.for ),
                getJson( item[ 'active_skill' ] ),
                getJson( item[ 'passive_skill' ] ),
                getJson( item[ 'attack_skill' ] ),
                getJson( item[ 'magic_skill' ] ),
                getJson( item[ 'critical_skill' ] ),
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Item Option Data'
    }
}

export const OptionDataTransformer = new Transformer( null, parserOptions )
