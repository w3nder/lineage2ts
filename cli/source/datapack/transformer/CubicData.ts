import { TransformerOperations } from '../helpers/TransformerOperations'
import fs from 'fs/promises'
import parser from 'csvtojson'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerProcess } from '../helpers/TransformerProcess'
import pkgDir from 'pkg-dir'

const parseOptions = {
    delimiter: ',',
}

const rootDirectory = pkgDir.sync( __dirname )

const query = TransformerOperations.createQuery( 'cubic_data', [
    'id',
    'level',
    'delay',
    'maxActions',
    'power',
    'target_json',
    'condition_json',
    'skills_json'
] )

interface SkillItem {
    id: number
    level: number
}

interface Condition {
    applyBelow: boolean
    chance: number
    applyHp: number
}

interface CubicSkill extends SkillItem {
    activationChance: number
    useChance: number
    staticTarget: boolean
}

interface CubicTarget {
    type: string
    hpThreshold?: number
    skillThresholds?: Array<number>
}

function getChance( value: string ) : number {
    return parseInt( value.replace( '%', '' ) ) * 0.01
}

function createCondition( value: string ) : Condition {
    if ( !value ) {
        return null
    }

    let [ applyFlag, chanceValue, hpValue ] = value.trim().split( ';' )
    return {
        applyBelow: applyFlag === '0',
        applyHp: parseInt( hpValue ),
        chance: getChance( chanceValue )
    }
}

function createSkills( skillReference: Record<string, SkillItem>, ...values: Array<string> ) : Array<CubicSkill> {
    return values.reduce( ( allSkills: Array<CubicSkill>, value: string ): Array<CubicSkill> => {
        if ( value ) {
            let skillValues : Array<string> = value.trim().split( ';' )

            if ( skillValues.length === 3 ) {
                skillValues.unshift( '100%' )
            }

            let [ activationValue, name, useValue, staticValue ] = skillValues
            let skill = skillReference[ name ]

            allSkills.push( {
                activationChance: getChance( activationValue ),
                id: skill.id,
                level: skill.level,
                staticTarget: staticValue === '1',
                useChance: getChance( useValue )
            } )
        }
        return allSkills
    }, [] )
}

function createTarget( value: string ) : CubicTarget {
    let parsedValues = value.trim().split( ';' )
    if ( parsedValues.length === 1 ) {
        return {
            type: parsedValues[ 0 ]
        }
    }

    let [ hpThreshold, ...skillThresholds ] = parsedValues.slice( 1 ).map( value => {
        return parseInt( value.replace( '%', '' ) ) * 0.01
    } )

    return {
        type: parsedValues[ 0 ],
        hpThreshold,
        skillThresholds
    }
}

class CubicsData implements TransformerProcess {
    operationName: string

    getName(): string {
        return 'Cubic Data'
    }

    getQuery(): string {
        return query
    }

    async getCsvFile( fileName: string ) : Promise<Array<unknown>> {
        let fileContents = await fs.readFile( `${rootDirectory}/${fileName}` )
        return parser( parseOptions ).fromString( fileContents.toString() )
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let dataFileName: string = 'overrides/data/csv/cubics.csv'
        this.operationName = dataFileName
        let agathionData = await this.getCsvFile( dataFileName )

        let referencePath = 'overrides/data/csv/skillReferenceIds.csv'
        this.operationName = referencePath
        let skillReferences = await this.getCsvFile( referencePath )

        this.operationName = 'Processing csv data'
        return engine.insertMany( this.getQuery(), this.transformData( agathionData, skillReferences ) )
    }

    transformData( cubicItems: Array<unknown>, skillReferences: Array<unknown> ): Array<Array<any>> {
        let skillReferenceMap = skillReferences.reduce( ( finalMap: Record<string, SkillItem>, item: any ) : Record<string, SkillItem> => {

            finalMap[ item.name ] = {
                id: parseInt( item.skillId ),
                level: parseInt( item.skillLevel ),
            }

            return finalMap
        }, {} ) as Record<string, SkillItem>

        return cubicItems.map( ( item: any ) => {
            return [
                parseInt( item.id ),
                parseInt( item.level ),
                parseInt( item.delay ),
                parseInt( item.maxActions ),
                item.power ? Math.trunc( parseFloat( item.power ) ) : null,
                TransformerOperations.getJson( createTarget( item.targetType ) ),
                TransformerOperations.getJson( createCondition( item.hpCondition ) ),
                TransformerOperations.getJson( createSkills( skillReferenceMap, item.skillOne, item.skillTwo, item.skillThree ) )
            ]
        } )
    }

    getLastProcessedOperation(): string {
        return this.operationName
    }
}

export const CubicDataTransformer = new CubicsData()