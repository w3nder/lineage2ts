import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'hitcondition_bonus': 'data/stats/hitConditionBonus.xml',
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true,
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return 'INSERT into hitcondition_bonus (type, value) values (?, ?)'
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []
        let properties = _.get( parsedData, 'hitConditionBonus' )
        _.each( properties, ( item: any, key: string ) => {
            if ( !_.isObject( item ) ) {
                return
            }

            itemsToInsert.push( [
                key,
                item[ 'val' ],
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Hit Condition Bonus Data'
    }
}

export const HitConditionBonusTransformer = new Transformer( files, parserOptions )