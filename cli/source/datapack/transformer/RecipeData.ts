import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'recipe_data': 'data/recipes.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'id',
    'recipeId',
    'name',
    'craftLevel',
    'type',
    'successRate',
    'productionId',
    'productionCount',
    'productionRareId',
    'productionRareCount',
    'productionRareRarity',
    'statUseName',
    'statUseValue',

    'ingredients_json',
]

const query = `INSERT INTO recipe_data (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.item' ), ( item: any ) => {
            let { id, recipeId, name, craftLevel, type, successRate } = item

            if ( !_.isArray( item.ingredient ) ) {
                item.ingredient = [ item.ingredient ]
            }

            item.ingredient = _.reduce( item.ingredient, ( properties: any, ingredient: any ) => {
                properties[ ingredient.id ] = ingredient.count
                return properties
            }, {} )

            if ( _.isArray( item.production ) || _.isArray( item.productionRare ) || _.isArray( item.statUse ) ) {
                throw Error( `Improper recipe data detected for recipeId=${ recipeId } and name=${ name }` )
            }

            itemsToInsert.push( [
                id,
                recipeId,
                name,
                craftLevel,
                type,
                successRate,
                _.get( item, 'production.id', null ),
                _.get( item, 'production.count', null ),
                _.get( item, 'productionRare.id', null ),
                _.get( item, 'productionRare.count', null ),
                _.get( item, 'productionRare.rarity', null ),
                _.get( item, 'statUse.name', null ),
                _.get( item, 'statUse.value', null ),
                JSON.stringify( item.ingredient )
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Recipe Data'
    }
}

export const RecipeDataTransformer = new Transformer( files, parserOptions )