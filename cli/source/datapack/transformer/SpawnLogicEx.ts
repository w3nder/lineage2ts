import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const parseOptions = {
    delimiter: ',',
}

const query = TransformerOperations.createQuery( 'spawn_logic_ex', [
        'makerId',
        'territories_json',
        'logic',
        'maxAmount',
        'ai_json',
        'flying',
        'eventType',
        'avoidTerritories_json'
] )

export function cleanValue( value: string ) : string {
    return value.replaceAll( '{','' ).replaceAll( '}','' )
}

export function getAiParameters( value: string ) {
    if ( !value ) {
        return {}
    }

    let pairs = cleanValue( value ).split( ';' )

    return pairs.reduce( ( parameters: Object, currentPair: string ) => {
        let index = currentPair.indexOf( '=' )
        let key = _.camelCase( currentPair.slice( 0, index ) )
        let value = currentPair.slice( index + 1 ).trim()
        let numericValue = parseFloat( value )

        parameters[ key ] = Number.isNaN( numericValue ) || numericValue.toString().length !== value.length ? value : numericValue

        return parameters
    }, {} )
}

function getAvoidedTerritories( values: Array<string>, territories: Object ) : Array<string> {
    return values.map( ( id: string ) => {
        let territoryValue = territories[ id ]

        if ( !territoryValue ) {
            throw new Error( `Unable to find avoided territory with id=${id}` )
        }

        return territoryValue
    } )
}

class SpawnLogicEx extends BaseCsvTransformer {
    getName(): string {
        return 'Spawn Logic Extra'
    }

    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return _.map( parsedData, ( item: any ) => {
            let parameters = JSON.parse( item.parametersJson )
            let territories = JSON.parse( item.territoryJson )

            let aiParameters = getAiParameters( parameters.aiParameters )
            let eventType = aiParameters[ 'eventName' ] ? aiParameters[ 'eventName' ] : null

            let avoidedTerritoryIds : Array<string> = parameters.bannedTerritory ? cleanValue( parameters.bannedTerritory ).split( ';' ) : []
            let avoidedTerritories = getAvoidedTerritories( avoidedTerritoryIds, territories )

            avoidedTerritoryIds.forEach( ( id: string ) => delete territories[ id ] )

            return [
                    item.name,
                    TransformerOperations.getJson( Object.values( territories ) ),
                    parameters.ai,
                    parameters.maximumNpc,
                    TransformerOperations.getJson( _.omit( aiParameters, [ 'eventName' ] ) ),
                    parameters.flying === '1' ? 1 : null,
                    eventType,
                    avoidedTerritories.length === 0 ? null : TransformerOperations.getJson( avoidedTerritories )
            ]
        } )
    }

}

export const SpawnLogicExTransfomer = new SpawnLogicEx( 'overrides/data/csv/npcMakerEx.csv', parseOptions )