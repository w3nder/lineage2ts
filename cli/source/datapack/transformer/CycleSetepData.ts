import { BaseCsvTransformer } from '../helpers/BaseCsvTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'

const parseOptions = {
    delimiter: ',',
}

const query = TransformerOperations.createQuery( 'cycle_step_data', [
    'id',
    'stepId',
    'pointLimit',
    'dropTime',
    'intervalPoint',
    'intervalTime',
    'lockTime',
    'mapId',
    'changeTimeCron',
    'areaIdsJson',
    'openDoorIdsJson',
    'respawnPoint',
    'respawnPvpPoint',
    'respawnRangeJson',
    'mapPointJson'
] )

function checkValue( value: string ) : string | null {
    return value.length === 0 ? null : value
}

const dayIndex : Record<string, number> = {
    'mon': 1,
    'fri': 5
}
function getCron( value: string | null ) : string | null {
    if ( !value ) {
        return null
    }

    let dateValues : Array<string> = JSON.parse( value )
    let timeValues : Array<string> = dateValues[ 1 ].split( ':' )

    return `${timeValues[ 1 ]} ${timeValues[ 0 ]} * * ${dayIndex[ dateValues[ 0 ] ]}`
}

class StepData extends BaseCsvTransformer {
    getName(): string {
        return 'Cycle Step Data'
    }

    getQuery(): string {
        return query
    }

    transformData( parsedData: Array<unknown> ): Array<Array<any>> {
        return parsedData.map( ( item :any ) => {
            return [
                parseInt( item.cycleId, 10 ),
                parseInt( item.stepId, 10 ),
                parseInt( item.pointLimit, 10 ),
                parseInt( item.dropTime, 10 ),
                parseInt( item.intervalPoint, 10 ),
                parseInt( item.intervalTime, 10 ),
                parseInt( item.lockTime, 10 ),
                item.mapId ? parseInt( item.mapId, 10 ) : null,
                getCron( checkValue( item.changeTime ) ),
                checkValue( item.areaIds ),
                checkValue( item.openDoorIds ),
                checkValue( item.respawnPoint ),
                checkValue( item.respawnPvpPoint ),
                checkValue( item.respawnRange ),
                checkValue( item.mapPoint ),
            ]
        } )
    }
}

export const CycleStepTransformer = new StepData( 'overrides/data/csv/cycleStepData.csv', parseOptions )