import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'class_list': 'data/stats/chars/classList.xml',
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true,
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return 'INSERT into class_list (classId, name, parentClassId) values (?,?,?)'
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []
        let categories = _.get( parsedData, 'list.class' )

        _.each( _.castArray( categories ), ( item: any ) => {
            let { classId, name, parentClassId } = item

            itemsToInsert.push( [
                classId,
                name,
                _.defaultTo( parentClassId, null )
            ] )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'ClassList Data'
    }
}

export const ClassListDataTransformer = new Transformer( files, parserOptions )
