import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerProcess } from '../helpers/TransformerProcess'
import _ from 'lodash'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'

const rootDirectory = pkgDir.sync( __dirname )
const fileName = 'data/stats/expData.json'

const query = 'INSERT INTO experience_data (level, exp) values (?, ?)'

class Transformer implements TransformerProcess {

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let jsonData = await fs.readFile( `${ rootDirectory }/${ fileName }` )
        let experienceData = JSON.parse( jsonData.toString() )

        let itemsToInsert = _.map( experienceData, ( value: number, key: string ) => {
            return [ _.parseInt( key ), value ]
        } )

        return engine.insertMany( query, itemsToInsert )
    }

    getName(): string {
        return 'Experience Data'
    }

    getLastProcessedOperation(): string {
        return fileName
    }
}

export const ExperienceDataTransformer = new Transformer()