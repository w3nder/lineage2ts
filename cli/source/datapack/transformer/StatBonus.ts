import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'

const files = {
    'stat_bonus': 'data/stats/statBonus.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert : Array<Array<any>> = []

        _.each( _.get( parsedData, 'list' ), ( item: any, key: string ) => {
            if ( _.has( item, 'stat' ) ) {
                let values = _.map( _.castArray( item.stat ), 'bonus' )

                itemsToInsert.push( [
                    key,
                    TransformerOperations.getJson( values )
                ] )
            }
        } )

        return itemsToInsert
    }

    getQuery(): string {
        return 'INSERT into stat_bonus (attribute, values_json) values (?,?)'
    }

    getName(): string {
        return 'Bonus Stat Data'
    }
}

export const StatBonusTransformer = new Transformer( files, parserOptions )