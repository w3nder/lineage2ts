import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'player_creation_points': 'data/stats/chars/pcCreationPoints.xml',
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true,
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return 'INSERT OR REPLACE into player_creation_points (classId, x, y, z) values (?,?,?,?)'
    }

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []

        _.each( _.get( parsedData, 'list.startPoints' ), ( item: any ) => {
            let { classId } = item

            if ( !_.isArray( classId ) ) {
                classId = [ classId ]
            }

            _.each( classId, ( classIdValue: number ) => {
                _.each( item.spawn, ( coordinates: any ) => {
                    let { x, y, z } = coordinates

                    itemsToInsert.push( [ classIdValue, x, y, z ] )
                } )
            } )
        } )

        return itemsToInsert
    }

    getName(): string {
        return 'Player Creation Points'
    }
}

export const PlayerCreationPointsTransformer = new Transformer( files, parserOptions )