import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import _ from 'lodash'

const files = {
    'pc_karma_increase': 'data/stats/chars/pcKarmaIncrease.xml'
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

class Transformer extends BaseXmlTransformer {

    transformData( parsedData: any ): Array<Array<any>> {
        let itemsToInsert : Array<Array<any>> = []

        _.each( _.get( parsedData, 'pcKarmaIncrease.increase' ), ( item: any, ) => {
            let { lvl, val } = item
            itemsToInsert.push( [ lvl, val ] )
        } )

        return itemsToInsert
    }

    getQuery( tableName: string ): string {
        return `INSERT into ${tableName} (level, value) values (?,?)`
    }

    getName(): string {
        return 'Player Karma Increase Data'
    }
}

export const PcKarmaIncreaseTransformer = new Transformer( files, parserOptions )