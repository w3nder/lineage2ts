import { BaseXmlTransformer } from '../helpers/BaseXmlTransformer'
import { IDatapackEngine } from '../engine/IDatapackEngine'
import { TransformerOperations } from '../helpers/TransformerOperations'
import _ from 'lodash'
import aigle from 'aigle'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'
import path from 'path'

const rootDirectory = pkgDir.sync( __dirname )
const fileDirectory = 'data/instances'

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: '',
    parseAttributeValue: true
}

const columns = [
    'nameId',
    'name',
    'ejectTime',
    'allowRandomWalk',
    'activityTime',
    'allowSummon',
    'emptyDestroyTime',
    'exitPoint_json',
    'showTimer_json',
    'doors_json',
    'spawns_json',
    'spawnPoints_json',
    'reEnter_json',
    'removeBuffs_json'
]

const query = TransformerOperations.createQuery( 'instance_data', columns )

function createShowTimerData( data: any ) {
    return {
        showTimer: _.get( data, 'val', false ),
        isTimerIncrease: _.get( data, 'increase', false ),
        timerText: _.get( data, 'text', '' )
    }
}

function createDoorData( data: any ) {
    if ( !data ) {
        return null
    }

    return _.castArray( data.door )
}

function createSpawnsData( data: any ) {
    if ( !data ) {
        return null
    }

    return _.reduce( _.castArray( data.group ), ( finalMap: { [ groupName : string ] : Array<any> }, currentGroup : any ) => {
        let { name } = currentGroup

        finalMap[ name ] = _.castArray( currentGroup.spawn )

        return finalMap
    }, {} )
}

function createSpawnPointsData( data: any ) {
    if ( !_.has( data, 'Location' ) ) {
        return null
    }

    return _.castArray( data[ 'Location' ] )
}

function createReEnterData( data: any ) {
    if ( !data ) {
        return null
    }

    let [ times, conditions ] = _.partition( _.castArray( _.get( data, 'reset', [] ) ), ( item: any ) : boolean => {
        return _.has( item, 'time' )
    } )

    return {
        type: _.get( data, 'additionStyle', null ),
        conditions,
        times
    }
}

function createRemoveBuffsData( data: any ) {
    if ( !data ) {
        return null
    }

    return {
        removeBuffType: data.type
    }
}

class Transformer extends BaseXmlTransformer {

    getQuery(): string {
        return query
    }

    async startProcess( engine: IDatapackEngine ): Promise<void> {
        let transformer = this
        const files = searchFiles( 'xml', `${ rootDirectory }/${ fileDirectory }` )

        return aigle.resolve( files ).eachLimit( 10, async ( fileName: string ) => {
            let xmlData = await fs.readFile( fileName )
            let parsedData = await this.getParser().parse( xmlData.toString(), transformer.parserOptions )

            return engine.insertMany( transformer.getQuery(), transformer.transformData( parsedData, fileName ) )
        } )
    }

    transformData( parsedData: any, fileName: string ): Array<Array<any>> {
        let itemsToInsert: Array<Array<any>> = []
        let instance = _.get( parsedData, 'instance' )

        if ( instance ) {
            let {
                name,
                ejectTime,
                allowRandomWalk,
                activityTime,
                allowSummon,
                emptyDestroyTime,
                exitPoint,
                showTimer,
                doorlist,
                spawnlist,
                spawnPoints,
                reenter,
                removeBuffs
            } = instance

            itemsToInsert.push( [
                _.replace( path.basename( fileName ), '.xml', '' ),
                name,
                _.isNumber( ejectTime ) ? ejectTime * 1000 : null,
                _.isBoolean( allowRandomWalk ) ? _.toString( allowRandomWalk ) : null,
                _.get( activityTime, 'val', null ),
                _.toString( _.get( allowSummon, 'val', null ) ),
                _.get( emptyDestroyTime, 'val', null ),
                TransformerOperations.getJson( exitPoint ),
                TransformerOperations.getJson( createShowTimerData( showTimer ) ),
                TransformerOperations.getJson( createDoorData( doorlist ) ),
                TransformerOperations.getJson( createSpawnsData( spawnlist ) ),
                TransformerOperations.getJson( createSpawnPointsData( spawnPoints ) ),
                TransformerOperations.getJson( createReEnterData( reenter ) ),
                TransformerOperations.getJson( createRemoveBuffsData( removeBuffs ) ),
            ] )
        }

        return itemsToInsert
    }

    getName(): string {
        return 'Instance Data'
    }
}

export const InstanceDataTransformer = new Transformer( null, parserOptions )
