/**
 * Copied from https://github.com/claytongulick/quickhull and enhanced with Typescript types
 * MIT License
 * QuickHull.js
 *
 * Implementation of the QuickHull algorithm for finding convex hull of a set of points
 *
 * Original author: Clay Gulick (https://github.com/claytongulick)
 */

type MinMaxPoints = [ PolygonPoint, PolygonPoint ]
export interface PolygonPoint {
    x: number
    y: number
}


export function QuickHull( points : Array<PolygonPoint> ) : Array<PolygonPoint> {
    if ( points.length === 3 ) {
        return points
    }

    const output : Array<PolygonPoint> = []
    const minMaxPoints = getMinMaxPoints( points )

    addSegments( minMaxPoints, points, output )

    //reverse line direction to get points on other side
    addSegments( minMaxPoints.reverse() as MinMaxPoints, points, output )

    return output
}

/**
 * Return the min and max points in the set along the X axis
 * Returns [ {x,y}, {x,y} ]
 * @param {Array} points - An array of {x,y} objects
 */
function getMinMaxPoints( points: Array<PolygonPoint> ) : MinMaxPoints {
    let minPoint = points[ 0 ]
    let maxPoint = points[ 0 ]

    for ( let index = 1; index < points.length; index++ ) {
        if ( points[ index ].x < minPoint.x )
            minPoint = points[ index ]
        if ( points[ index ].x > maxPoint.x )
            maxPoint = points[ index ]
    }

    return [ minPoint, maxPoint ]
}

/**
 * Calculates the distance of a point from a line
 * @param {Array} point - Array [x,y]
 * @param {Array} line - Array of two points [ [x1,y1], [x2,y2] ]
 */
function distanceFromLine( point: PolygonPoint , line: MinMaxPoints ) : number {
    let vY = line[ 1 ].y - line[ 0 ].y
    let vX = line[ 0 ].x - line[ 1 ].x
    return ( vX * ( point.y - line[ 0 ].y ) + vY * ( point.x - line[ 0 ].x ) )
}

/**
 * Determines the set of points that lay outside the line (positive), and the most distal point
 * Returns: {points: [ [x1, y1], ... ], max: [x,y] ]
 * @param inputPoints
 * @param line
 */
interface DistalPoints {
    points: Array<PolygonPoint>
    max: PolygonPoint
}
function distalPoints( line : MinMaxPoints, points: Array<PolygonPoint> ) : DistalPoints {
    const outerPoints : Array<PolygonPoint> = []
    let distalPoint : PolygonPoint = null

    for ( let index = 0; index < points.length; index++ ) {
        let point = points[ index ]
        let distance = distanceFromLine( point,line )

        if ( distance <= 0 ) {
            continue
        }

        outerPoints.push( point )

        let maxDistance = 0
        if ( distance > maxDistance ) {
            distalPoint = point
            maxDistance = distance
        }

    }

    return {
        points: outerPoints,
        max: distalPoint
    }
}

function addSegments( line : MinMaxPoints, points: Array<PolygonPoint>, output : Array<PolygonPoint> ) : void {
    const distal = distalPoints( line, points )
    if ( !distal.max ) {
        output.push( line[ 0 ] )
        return
    }

    addSegments( [ line[ 0 ], distal.max ], distal.points, output )
    addSegments( [ distal.max, line[ 1 ] ], distal.points, output )
}