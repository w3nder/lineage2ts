import _, { parseInt } from 'lodash'

export const TransformerOperations = {
    normalizeValues ( item: any, path: string, finalPath: string ) {
        if ( _.has( item, path ) ) {
            let value = _.get( item, path )
            _.set( item, finalPath, _.castArray( value ) )
        }
    },

    getJson( value: any ): string {
        if ( _.isEmpty( value ) ) {
            return null
        }

        return JSON.stringify( value )
    },

    normalizeAllValues( properties: any ) {
        _.each( properties, ( value: any, key: string ) => {
            value = _.castArray( value )
            properties[ key ] = value
        } )

        return properties
    },

    createQuery( tableName: string, columns: Array<string>, useReplace: boolean = false ) : string {
        return `INSERT ${useReplace ? 'OR REPLACE ' : ''}INTO ${tableName} (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`
    },

    parseInt( value: string ) : number | null {
        if ( !value ) {
            return null
        }

        let outcome = parseInt( value, 10 )
        if ( !Number.isInteger( outcome ) ) {
            return null
        }

        return outcome
    },

    parseFloat( value: string ) : number | null {
        if ( !value ) {
            return null
        }

        let outcome = parseFloat( value )
        if ( Number.isNaN( outcome ) ) {
            return null
        }

        return outcome
    },

    /*
        Convert chance value (%) to value between 0 and 1
     */
    parseChance( value: string ) : number | null {
        if ( !value ) {
            return null
        }

        if ( value.length > 2 ) {
            return 1
        }

        let adjustedValue = value.length === 1 ? `0.0${value}` : `0.${value}`
        let outcome = parseFloat( adjustedValue )
        if ( Number.isNaN( outcome ) ) {
            return null
        }

        return outcome
    }
}