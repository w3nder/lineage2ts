import { SQLiteEngine } from './engine/sqlite'
import { DatapackEngineName, IDatapackEngine } from './engine/IDatapackEngine'

const availableEngines = {
    [ DatapackEngineName.sqlite ]: SQLiteEngine
}

export function getDatapackEngine( name ) : IDatapackEngine {
    return availableEngines[ name ]
}