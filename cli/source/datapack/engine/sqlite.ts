import { DatapackEngineName, IDatapackEngine } from './IDatapackEngine'
import _ from 'lodash'
import pkgDir from 'pkg-dir'
import Database from 'better-sqlite3'

const rootDirectory = pkgDir.sync( __dirname )

export class SQLiteDataEngine implements IDatapackEngine {
    database: any

    async start( filePath : string ): Promise<void> {
        this.database = new Database( this.getAdjustedPath( filePath ), {
            fileMustExist: false
        } )

        this.database.pragma( 'journal_mode = WAL' )
    }

    getName(): string {
        return DatapackEngineName.sqlite
    }

    async executeQuery ( query ) : Promise<void> {
        this.database.exec( query )
    }

    async finish () {
        this.database.close()
    }

    async insertMany ( query, items: Array<Array<any>> ) {
        let statement = this.database.prepare( query )

        const insertBulk = this.database.transaction( ( allItems ) => _.each( allItems, ( itemParameters ) => {
            statement.run( ...itemParameters )
        } ) )

        insertBulk( items )
    }

    protected getAdjustedPath( filePath: string ) : string {
        return `${rootDirectory}/${filePath}`
    }

    executeWithParameters( query: string, parameters: Array<string | number> ): Promise<void> {
        return this.database.prepare( query ).run( ...parameters )
    }

    insertOne( query: string, parameters: Array<string | number | boolean> ): Promise<void> {
        return this.database.prepare( query ).run( ...parameters )
    }
}

export const SQLiteEngine : IDatapackEngine = new SQLiteDataEngine()

export const enum SQLiteDefaultParameters {
    datapackName = 'datapack.database',
    geopackName = 'geopack.database'
}