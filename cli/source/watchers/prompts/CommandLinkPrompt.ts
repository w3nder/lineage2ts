// @ts-ignore
import { Form } from 'enquirer'

export interface CommandLinkPromptFields {
    address: string
    playerName: string
}

export const DefaultCommandLinkFields : CommandLinkPromptFields = {
    address: 'tcp://127.0.0.1:9999',
    playerName: 'admin'
}

export function getCommandLinkPrompt() {
    return new Form( {
        name: 'connection',
        message: 'Link connection settings:',
        choices: [
            { name: 'playerName', message: 'Player Name', initial: DefaultCommandLinkFields.playerName },
            { name: 'address', message: 'Address', initial: DefaultCommandLinkFields.address }
        ]
    } )
}