import { BaseFileWatcher } from './helpers/BaseFileWatcher'
import { cleanHtmlFileName, createHtmlColumnsData, HtmlTransformerQuery } from '../datapack/transformer/HtmlData'
import logSymbols from 'log-symbols'
import { CommandLink } from './helpers/CommandLink'
import { CommandLinkPromptFields, DefaultCommandLinkFields, getCommandLinkPrompt } from './prompts/CommandLinkPrompt'
import { scheduler } from 'timers/promises'
import { CommandLinkInput, CommandLinkType } from './enums/CommandLinkParameters'

const command : CommandLinkInput = {
    type: CommandLinkType.admin,
    command: 'admin_html-reload',
    replyId: 'admin'
}

class HtmlWatcher extends BaseFileWatcher {
    link: CommandLink

    async onAdd( filePath: string ): Promise<void> {
        console.log( logSymbols.info, 'Adding file:', filePath )
        await this.updateFileContents( filePath )
        this.notifyLink()
    }

    async onChange( filePath: string ): Promise<void> {
        console.log( logSymbols.info, 'Updating file:', filePath )
        await this.updateFileContents( filePath )
        this.notifyLink()
    }

    async onRemove( filePath: string ): Promise<void> {
        console.log( logSymbols.info, 'Removing file record:', filePath )
        await this.engine.executeWithParameters( 'DELETE from html_cache where name=?', [ cleanHtmlFileName( filePath ) ] )
        this.notifyLink()
    }

    async updateFileContents( filePath: string ) : Promise<void> {
        let columns = await createHtmlColumnsData( filePath )
        return this.engine.insertOne( HtmlTransformerQuery, columns )
    }

    getName(): string {
        return 'HTML File Updater'
    }

    notifyLink() : void {
        this.link.sendData( command )
    }

    async shutdownSequence(): Promise<void> {
        this.link.shutdown()
        return super.shutdownSequence()
    }

    async initialize( path : string ) : Promise<void> {
        let parameters = await this.getPromptParameters()
        this.link = new CommandLink( parameters.address )

        command.replyId = parameters.playerName

        return super.initialize( path )
    }

    private async getPromptParameters() : Promise<CommandLinkPromptFields> {
        let prompt = getCommandLinkPrompt()
        let shouldFinishPrompt : boolean = false

        prompt.on( 'keypress', () => {
            shouldFinishPrompt = true
        } )

        let runningPrompt = prompt.run() as Promise<CommandLinkPromptFields>

        let parameters = await Promise.race(
            [
                runningPrompt,
                scheduler.wait( 10000 )
            ]
        ) as CommandLinkPromptFields

        if ( !parameters ) {
            if ( shouldFinishPrompt ) {
                return runningPrompt
            }

            await prompt.cancel()

            console.log( logSymbols.info, 'Using default command link settings' )
            return DefaultCommandLinkFields
        }

        return parameters
    }
}

/*
    Purpose of html watcher is to:
    - react to any html changes
    - update datapack with changed html file contents
    - allow developers to shortcut datapack flow and reload html contents on game-server (see //html-reload )
 */
new HtmlWatcher( 'overrides/html' )