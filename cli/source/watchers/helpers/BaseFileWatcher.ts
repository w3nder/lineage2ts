import * as chokidar from 'chokidar'
import pkgDir from 'pkg-dir'
import { SQLiteDataEngine } from '../../datapack/engine/sqlite'
import logSymbols from 'log-symbols'
import path from 'path'

const rootDirectory = pkgDir.sync( __dirname )

class SQLiteHtmlEngine extends SQLiteDataEngine {
    protected getAdjustedPath( filePath: string ): string {
        let gameServerPath = rootDirectory.replace( 'cli', 'game-server' )
        return `${gameServerPath}/${filePath}`
    }
}

export abstract class BaseFileWatcher {
    engine: SQLiteHtmlEngine

    constructor( path: string ) {
        console.log( logSymbols.info, 'File Watcher:', this.getName() )
        this.initialize( path )
    }

    async initialize( filePath: string ) : Promise<void> {
        this.engine = new SQLiteHtmlEngine()

        let directory = path.normalize( `${rootDirectory}/${filePath}` )
        chokidar.watch( directory, {
            ignoreInitial: true,
            persistent: true
        } )
            .on( 'add', this.onAdd.bind( this ) )
            .on( 'change', this.onChange.bind( this ) )
            .on( 'unlink', this.onRemove.bind( this ) )
            .on( 'ready', this.onReady.bind( this ) )

        /*
            Used by various restart utilities that manage application process.
         */
        process.once( 'SIGUSR1', this.shutdownSequence.bind( this ) )
        process.once( 'SIGUSR2', this.shutdownSequence.bind( this ) )

        /*
            Used by terminal applications to interrupt process, aka Ctrl + C.
         */
        process.on( 'SIGINT', this.shutdownSequence.bind( this ) )

        /*
            Termination signal used by OS to signify process must be killed (similar to SIGKILL).
         */
        process.on( 'SIGTERM', this.shutdownSequence.bind( this ) )
        console.log( logSymbols.success, 'Monitoring directory:', directory )
    }

    abstract onAdd( filePath: string ) : Promise<void>
    abstract onChange( filePath: string ) : Promise<void>
    abstract onRemove( filePath: string ) : Promise<void>
    protected onReady() : Promise<void> {
        console.log( logSymbols.success, 'Listening for events...' )
        return this.engine.start( this.getDatabasePath() )
    }

    async shutdownSequence() : Promise<void> {
        await this.engine.finish()
        console.log( logSymbols.info, 'Shutdown complete' )

        process.exit()
    }

    protected getDatabasePath() : string {
        return 'datapack.database'
    }

    abstract getName() : string
}