import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'

const rootDirectory = pkgDir.sync( __dirname )
const emptyTabs = new RegExp( /^\t+$/ )
const emptySpaces = new RegExp( /^\s+$/ )
const tabs = new RegExp( /\t+/g )
const spaces = new RegExp( /\s+/g )

const enum SettingMode {
    None,
    RespawnArea,
    RestartPoint
}

function asArray( value: string ) : string {
    return value.replaceAll( '{', '[' ).replaceAll( '}', ']' )
}

function cleanValue( value: string, first: string = '[', second: string = ']' ): string {
    return value.replaceAll( first, '' ).replaceAll( second, '' )
}

function cleanComments( value: string ) : string {
    let index = value.indexOf( '//' )
    if ( index > 0 ) {
       return value.substring( 0, index )
    }

    return value
}

function createArea( data: Set<string>, type: string ) : Object {
    let properties = {
        type
    }

    let respawnNames = new Set<string>()
    let raceProperties = {}

    data.forEach( ( propertyLine: string ) : void => {
        let [ name, value ] = propertyLine.split( '=' ).map( value => value.trim() ) as [ string, string ]

        if ( name && value ) {

            switch ( name ) {
                case 'range':
                    name = 'points'
                    value = asArray( value )
                    break

                case 'race':
                    let [ race, location ] = cleanValue( cleanValue( value ), '{', '}' ).split( ';' )
                    raceProperties[ race ] = location
                    respawnNames.add( location )
                    return
            }

            properties[ name ] = value
        }
    } )

    properties[ 'propertiesJson' ] = respawnNames.size === 1 ? JSON.stringify( {
        anyRace: respawnNames.values().next().value
    } ) : JSON.stringify( raceProperties )

    return properties
}

function createPoint( data: Set<string> ) : Object {
    let points: Array<Array<number>> = []
    let pvpPoints: Array<Array<number>> = []
    let maps: Array<string> = []
    let properties = {}

    data.forEach( ( propertyLine: string ) : void => {
        let [ name, value ] = propertyLine.split( '=' ).map( value => value.trim() ) as [ string, string ]

        if ( name && value ) {

            value = cleanComments( value ).trim()

            switch ( name ) {
                case 'point_name':
                    name = 'name'
                    value = cleanValue( value )
                    break

                case 'point':
                    points.push( cleanValue( value, '{', '}' ).split( ';' ).map( value => parseInt( value ) ) )
                    return

                case 'chao_point':
                    pvpPoints.push( cleanValue( value, '{', '}' ).split( ';' ).map( value => parseInt( value ) ) )
                    return

                case 'map':
                    let mapValue = cleanValue( value, '{', '}' )
                    if ( mapValue ) {
                        maps.push( mapValue )
                    }

                    return

                case 'loc_name':
                    name = 'location'
                    break

                case 'banned_race':
                    name = 'bannedRace'
                    value = cleanValue( cleanValue( value ), '{', '}' )
                    if ( !value ) {
                        return
                    }

                    break

                case 'bbs':
                    value
            }

            properties[ name ] = value
        }
    } )

    if ( points.length > 0 ) {
        properties[ 'pointsJson' ] = JSON.stringify( points )
    }

    if ( pvpPoints.length > 0 ) {
        properties[ 'pvpJson' ] = JSON.stringify( pvpPoints )
    }

    if ( maps.length > 0 ) {
        properties[ 'maps' ] = JSON.stringify( maps )
    }

    return properties
}

function prepareLine( line: string ) : string {
    return line
        .replaceAll( tabs, ' ' )
        .replaceAll( spaces, ' ' )
        .replaceAll( '= ', '=' )
        .replaceAll( ' =', '=' )
}

async function processFile(): Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${ rootDirectory }/setting.txt` ),
        crlfDelay: Infinity
    } )

    const allAreas: Array<Object> = []
    const allPoints: Array<Object> = []
    const areaHeaders: Set<string> = new Set<string>( [ 'type', 'points', 'propertiesJson' ] )
    const pointHeaders: Set<string> = new Set<string>( [ 'name', 'pointsJson', 'pvpJson', 'bannedRace', 'bbs', 'location', 'maps' ] )
    let mode : SettingMode = SettingMode.None
    const data : Set<string> = new Set<string>()

    let shouldProcess : boolean = false

    for await ( const rawLine of lines ) {
        let line = prepareLine( rawLine ).trim()
        if ( line.startsWith( '//' )
            || !line
            || emptyTabs.test( line )
            || emptySpaces.test( line ) ) {
            continue
        }

        if ( line === 'restart_point_begin' ) {
            shouldProcess = true
            continue
        }

        if ( line === 'restart_point_end' ) {
            shouldProcess = false
            continue
        }

        if ( !shouldProcess ) {
            continue
        }

        if ( line === 'area_begin' ) {
            data.clear()
            mode = SettingMode.RespawnArea
            continue
        }

        if ( line === 'area_end' ) {
            allAreas.push( createArea( data, 'respawn' ) )
            mode = SettingMode.None
            continue
        }

        if ( line === 'point_begin' ) {
            data.clear()
            mode = SettingMode.RestartPoint
            continue
        }

        if ( line === 'point_end' ) {
            allPoints.push( createPoint( data ) )
            mode = SettingMode.None
            continue
        }

        switch ( mode ) {
            case SettingMode.RespawnArea:
                if ( line.startsWith( 'race' ) || line.startsWith( 'range' ) ) {
                    data.add( line )
                }

                break

            case SettingMode.RestartPoint:
                data.add( line )
                break
        }
    }


    const areaStream = fs.createWriteStream( `${ rootDirectory }/respawnAreas.csv` )

    const areaCsv = format( {
        delimiter: ',',
        headers: Array.from( areaHeaders )
    } )

    areaCsv.pipe( areaStream )

    allAreas.forEach( ( item: Object ) => {
        areaCsv.write( item )
    } )

    areaCsv.end()

    const pointStream = fs.createWriteStream( `${ rootDirectory }/respawnPoints.csv` )

    const pointCsv = format( {
        delimiter: ',',
        headers: Array.from( pointHeaders )
    } )

    pointCsv.pipe( pointStream )

    allPoints.forEach( ( item: Object ) => {
        pointCsv.write( item )
    } )

    pointCsv.end()
}

processFile()