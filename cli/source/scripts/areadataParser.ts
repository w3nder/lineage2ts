import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'
import _ from 'lodash'

const rootDirectory = pkgDir.sync( __dirname )

function cleanValue( value: string, first: string = '[', second: string = ']' ): string {
    return value.replaceAll( first, '' ).replaceAll( second, '' )
}

function asArray( value: string ) : string {
    return value.replaceAll( '{', '[' ).replaceAll( '}', ']' )
}

function getProperty( value: string ): [ string, string ] {
    return cleanValue( cleanValue( value, '{', '}' ), '[', ']' )
        .split( '=' )
        .map( value => value.trim() ) as [ string, string ]
}

function createZone( chunks: Array<string>, headers: Set<string> ): Object {
    return chunks.reduce( ( definition: Object, propertyLine: string ): Object => {
        let [ name, value ] = propertyLine.split( '=' ).map( value => value.trim() ) as [ string, string ]

        if ( name && value ) {
            switch ( name ) {
                case 'water_range':
                    name = 'points'
                    value = asArray( value )
                    break

                case 'map_no':
                    name = 'region'
                    value = cleanValue( value, '{', '}' ).replace( ';','-' )
                    break

                case 'range':
                    name = 'points'
                    value = asArray( value )
                    break

                case 'name':
                case 'skill_name':
                    value = cleanValue( value )
                    break

                case 'skill_list':
                    value = cleanValue( value, '{', '}' ).replaceAll( '@','' )
                    break
            }

            definition[ name ] = value
            headers.add( name )
        }

        return definition
    }, {} )
}

async function processFile(): Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${ rootDirectory }/areas.txt` ),
        crlfDelay: Infinity
    } )

    const allZones: Array<Object> = []
    const headers: Set<string> = new Set<string>()

    for await ( const line of lines ) {
        if ( line.startsWith( '//' ) || !line ) {
            continue
        }

        const chunks = line.replaceAll( ' = ', '=' )
            .replaceAll( /(?<=\d)(\s)(?=\d)/g, ';' )
            .replaceAll( /(?<=\d)(\s)(?=-)/g, ';' )
            .replaceAll( 'Min:(', '{' )
            .replaceAll( ') Max:(', '};{' )
            .replaceAll( ')}', '}}' )
            .replaceAll( '.000000','' )
            .split( /\t|\s/ )

        if ( _.head( chunks ) === 'area_begin' && _.last( chunks ) === 'area_end' ) {
            allZones.push( createZone( chunks.slice( 1, chunks.length - 1 ), headers ) )
        }
    }


    const zoneStream = fs.createWriteStream( `${ rootDirectory }/areas.csv` )

    const zoneCsv = format( {
        delimiter: ',',
        headers: Array.from( headers )
    } )

    zoneCsv.pipe( zoneStream )

    allZones.forEach( ( item: Object ) => {
        zoneCsv.write( item )
    } )

    zoneCsv.end()
}

processFile()