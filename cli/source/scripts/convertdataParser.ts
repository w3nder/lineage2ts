import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'
import _ from 'lodash'

const rootDirectory = pkgDir.sync( __dirname )
const emptyTabs = new RegExp( /^\t+$/ )
const emptySpaces = new RegExp( /^\s+$/ )
const tabs = new RegExp( /\t+/g )
const spaces = new RegExp( /\s+/g )

function prepareLine( line: string ) : string {
    return line
        .replaceAll( tabs, ' ' )
        .replaceAll( spaces, ' ' )
        .replaceAll( '= ', '=' )
        .replaceAll( ' =', '=' )
}

function cleanValue( value: string, first: string = '[', second: string = ']' ) : string {
    return value.replaceAll( first,'' ).replaceAll( second,'' )
}

interface ConvertNames {
    fromId: string
    toId: string
}

async function processFile() : Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${rootDirectory}/convertdata.txt` ),
        crlfDelay: Infinity
    } )

    const convertWeaponNamesStream = fs.createWriteStream( `${rootDirectory}/convertWeaponIds.csv` )

    const convertWeaponNamesCsv = format( { delimiter: ',', headers: true } )
    convertWeaponNamesCsv.pipe( convertWeaponNamesStream )

    const allNames : Array<ConvertNames> = []

    for await ( const rawLine of lines ) {
        let line = prepareLine( rawLine ).trim()
        if ( !line
            || emptyTabs.test( line )
            || emptySpaces.test( line ) ) {
            continue
        }

        const chunks = line.split( ' ' )
        if ( chunks[ 0 ] !== 'convert_begin' ) {
            continue
        }

        let currentItem : ConvertNames = {
            fromId: '',
            toId: ''
        }

        for ( const chunk of chunks ) {
            const [ type, referenceId ] = chunk.split( '=' )

            switch ( type ) {
                case 'input_item':
                    currentItem.fromId = cleanValue( referenceId )
                    break

                case 'output_item':
                    currentItem.toId = cleanValue( referenceId )
                    break
            }
        }

        if ( currentItem.fromId && currentItem.toId ) {
            allNames.push( currentItem )
        } else {
            throw new Error( `Unable to parse current line=${line}` )
        }
    }

    for ( const item of allNames ) {
        convertWeaponNamesCsv.write( item )
    }

    convertWeaponNamesCsv.end()
}

processFile()