import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'

const rootDirectory = pkgDir.sync( __dirname )
const emptyTabs = new RegExp( /^\t+$/ )
const emptySpaces = new RegExp( /^\s+$/ )
const tabs = new RegExp( /\t+/g )
const spaces = new RegExp( /\s+/g )

function asArray( value: string ) : string {
    return value.replaceAll( '{', '[' ).replaceAll( '}', ']' )
}

function cleanValue( value: string, first: string = '[', second: string = ']' ): string {
    return value.replaceAll( first, '' ).replaceAll( second, '' )
}

function createZone( chunks: Array<string>, headers: Set<string>, type: string ) : Object {
    let startingProperties = {
        type
    }

    return chunks.reduce( ( zone: Object, propertyLine: string ) : Object => {
        let [ name, value ] = propertyLine.split( '=' ).map( value => value.trim() ) as [ string, string ]

        if ( name && value ) {

            switch ( name ) {
                case 'range':
                    name = 'points'
                    value = asArray( value )
                    break

                case 'system_msg_id':
                    name = 'messageId'
                    break

                case 'name':
                    value = cleanValue( value )
                    break
            }

            zone[ name ] = value
            headers.add( name )
        }

        return zone
    }, startingProperties )
}

async function processFile(): Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${ rootDirectory }/RestrictAreaData.txt` ),
        crlfDelay: Infinity
    } )

    const allZones: Array<Object> = []
    const headers: Set<string> = new Set<string>( [ 'type' ] )

    for await ( const line of lines ) {
        if ( line.startsWith( '//' )
            || !line
            || emptyTabs.test( line )
            || emptySpaces.test( line ) ) {
            continue
        }

        const chunks = line
            .replaceAll( tabs, ' ' )
            .replaceAll( spaces, ' ' )
            .replaceAll( '= ', '=' )
            .replaceAll( ' =', '=' )
            .split( /\s/ ).map( value => value.trim() )

        switch ( chunks[ 0 ] ) {
            case 'nofly_begin':
                allZones.push( createZone( chunks.slice( 1, chunks.length - 1 ), headers, 'noFly' ) )
                break

            case 'transformable_begin':
                allZones.push( createZone( chunks.slice( 1, chunks.length - 1 ), headers, 'landing' ) )
                break

            case 'no_call_pc_begin':
                allZones.push( createZone( chunks.slice( 1, chunks.length - 1 ), headers, 'noSummonPlayer' ) )
                break

            case 'no_drop_item_begin':
                allZones.push( createZone( chunks.slice( 1, chunks.length - 1 ), headers, 'noItemDrop' ) )
                break

            case 'no_save_bookmark_begin':
                allZones.push( createZone( chunks.slice( 1, chunks.length - 1 ), headers, 'noBookmarkSave' ) )
                break

            case 'no_use_bookmark_begin':
                allZones.push( createZone( chunks.slice( 1, chunks.length - 1 ), headers, 'noBookmarkUse' ) )
                break

            default:
                throw new Error( `Unknown marker: ${chunks[ 0 ]}` )
        }
    }


    const zoneStream = fs.createWriteStream( `${ rootDirectory }/restrictedAreas.csv` )

    const zoneCsv = format( {
        delimiter: ',',
        headers: Array.from( headers )
    } )

    zoneCsv.pipe( zoneStream )

    allZones.forEach( ( item: Object ) => {
        zoneCsv.write( item )
    } )

    zoneCsv.end()
}

processFile()