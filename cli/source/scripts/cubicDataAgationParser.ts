import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'
import _ from 'lodash'

const rootDirectory = pkgDir.sync( __dirname )

interface AgathionDefinition {
    npcName: string
    level: string
    slot: string
    targetType: string
    itemId: string
    energy: string
    maxEnergy: string
    duration: string
    delay: string
    maxCount: string
    use: string
    power: string
    properties: Record<string, string>
}

const agathionFieldMapping: Record<string, string> = {
    'npc_name': 'npcName',
    'level': 'level',
    'slot': 'slot',
    'target_type': 'targetType',
    'op_cond': 'condition',
    'item_ids': 'itemId',
    'energy': 'energy',
    'max_energy': 'maxEnergy',
    'duration': 'duration',
    'delay': 'delay',
    'max_count': 'maxCount',
    'use_up': 'use',
    'power': 'power'
}

function cleanValue( value: string, first: string = '[', second: string = ']' ): string {
    return value.replaceAll( first, '' ).replaceAll( second, '' )
}

function getProperty( value: string ): [ string, string ] {
    return cleanValue( cleanValue( value, '{', '}' ), '[', ']' )
        .split( '=' )
        .map( value => value.trim() ) as [ string, string ]
}

function createAgathion( chunks: Array<string> ): AgathionDefinition {
    return chunks.reduce( ( definition: AgathionDefinition, propertyLine: string ): AgathionDefinition => {
        let [ name, value ] = getProperty( propertyLine )

        if ( name && value ) {
            let mappedKey = agathionFieldMapping[ name ]
            if ( mappedKey ) {
                definition[ mappedKey ] = value
            } else {
                definition.properties[ name ] = value
            }
        }

        return definition
    }, {
        properties: {}
    } as AgathionDefinition )
}

async function processFile(): Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${ rootDirectory }/CubicData.txt` ),
        crlfDelay: Infinity
    } )

    const agathionStream = fs.createWriteStream( `${ rootDirectory }/agathions.csv` )

    const agathionCsv = format( {
        delimiter: ',', headers: [
            'npcName',
            'level',
            'slot',
            'targetType',
            'itemId',
            'energy',
            'maxEnergy',
            'duration',
            'delay',
            'maxCount',
            'use',
            'power',
            'propertiesJson'
        ]
    } )

    agathionCsv.pipe( agathionStream )

    const allAgathions: Array<AgathionDefinition> = []
    let currentAgathion: AgathionDefinition

    for await ( const line of lines ) {
        if ( line.startsWith( '//' ) ) {
            continue
        }

        const chunks = line.split( '\t' )

        switch ( chunks[ 0 ] ) {
            case 'agathion_begin':
                currentAgathion = createAgathion( chunks )

                if ( _.last( chunks ) === 'agathion_end' ) {
                    allAgathions.push( currentAgathion )
                    currentAgathion = null
                }

                break

            case 'agathion_end':
                if ( currentAgathion ) {
                    allAgathions.push( currentAgathion )
                    currentAgathion = null
                }

                break

            default:
                if ( !currentAgathion || chunks.length > 1 ) {
                    break
                }

                let [ name, value ] = getProperty( chunks[ 0 ] )
                currentAgathion.properties[ name ] = value

                break
        }
    }


    allAgathions.forEach( ( item: AgathionDefinition ) => {
        let properties = item.properties

        delete item[ 'properties' ]

        agathionCsv.write( {
            ...item,
            propertiesJson: JSON.stringify( properties )
        } )
    } )

    agathionCsv.end()
}

processFile()