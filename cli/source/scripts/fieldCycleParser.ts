import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'
import _, { parseInt } from 'lodash'

const rootDirectory = pkgDir.sync( __dirname )

interface StepDefinition {
    cycleId: string
    stepId: string
    pointLimit: string
    lockTime: string
    dropTime: string
    intervalTime: string
    intervalPoint: string
    openDoorIds: string
    areaIds: string
    respawnRange: string
    respawnPoint: string
    respawnPvpPoint: string
    mapPoint: string
    mapId: string
    changeTime: string
}

function cleanValue( value: string, first: string = '[', second: string = ']' ) : string {
    return value.replaceAll( first,'' ).replaceAll( second,'' )
}

function getIds( value: string ) : string {
    let doorIds = cleanValue( value, '{', '}' ).split( ';' ).map( ( doorId: string ) : string => {
        return cleanValue( doorId )
    } )

    if ( doorIds.length === 0 || ( doorIds.length === 1 && doorIds[ 0 ] === '' ) ) {
        return ''
    }

    return JSON.stringify( doorIds )
}

function extractPointCoordinates( value: string ) : Array<number> {
    return cleanValue( value, '{', '}' )
        .split( ';' )
        .map( coordinate => parseInt( coordinate, 10 ) )
}

function getRespawnRange( value: string ) : string {
    let rangePoints = value.split( '};{' ).map( ( point: string ) : Array<number> => {
        return extractPointCoordinates( point )
    } )

    if ( rangePoints.length === 0 ) {
        return ''
    }

    return JSON.stringify( rangePoints )
}

function getPoint( value: string ) : string {
    let points = extractPointCoordinates( value )
    if ( points.length === 0 ) {
        return ''
    }

    return JSON.stringify( points )
}

function getCleanValue( value: string ) : string {
    /*
        Some values include // style comments, which should be stripped
     */
    return value.split( ' ' )[ 0 ]
}

async function processFile() : Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${rootDirectory}/FieldCycle.txt` ),
        crlfDelay: Infinity
    } )

    const dataStream = fs.createWriteStream( `${rootDirectory}/cycleStepData.csv` )

    const stepDataCsv = format( { delimiter: ',', headers: true } )
    stepDataCsv.pipe( dataStream )

    let currentStep : StepDefinition
    let cycleId : string
    const allSteps : Array<StepDefinition> = []

    for await ( const line of lines ) {
        if ( line.startsWith( '//' ) ) {
            continue
        }

        const chunks = line.replaceAll( '\t', '' ).split( '=' ).map( value => value.trim() )

        switch ( chunks[ 0 ] ) {
            case 'id':
                cycleId = chunks[ 1 ]
                break

            case 'step':
                currentStep = {
                    cycleId,
                    stepId: chunks[ 1 ],
                    pointLimit: '',
                    dropTime: '',
                    intervalPoint: '',
                    intervalTime: '',
                    lockTime: '',
                    mapId: '',
                    changeTime: '',
                    areaIds: '',
                    openDoorIds: '',
                    respawnPoint: '',
                    respawnPvpPoint: '',
                    respawnRange: '',
                    mapPoint: '',
                }

                allSteps.push( currentStep )
                break

            case 'step_point':
                currentStep.pointLimit = getCleanValue( chunks[ 1 ] )
                break

            case 'lock_time':
                currentStep.lockTime = getCleanValue( chunks[ 1 ] )
                break

            case 'drop_time':
                currentStep.dropTime = getCleanValue( chunks[ 1 ] )
                break

            case 'interval_time':
                currentStep.intervalTime = getCleanValue( chunks[ 1 ] )
                break

            case 'interval_point':
                currentStep.intervalPoint = getCleanValue( chunks[ 1 ] )
                break

            case 'open_door':
                currentStep.openDoorIds = getIds( chunks[ 1 ] )
                break

            case 'area_on':
                currentStep.areaIds = getIds( chunks[ 1 ] )
                break

            case 'range':
                currentStep.respawnRange = getRespawnRange( chunks[ 1 ] )
                break

            case 'normal_point':
                currentStep.respawnPoint = getPoint( chunks[ 1 ] )
                break

            case 'chao_point':
                currentStep.respawnPvpPoint = getPoint( chunks[ 1 ] )
                break

            case 'map_point':
                currentStep.mapPoint = getPoint( chunks[ 1 ] )
                break

            case 'map_string':
                currentStep.mapId = getCleanValue( chunks[ 1 ] )
                break

            case 'step_change_time':
                currentStep.changeTime = getIds( chunks[ 1 ] )
                break

            case 'step_end':
            case 'step_begin':
                currentStep = null
                break
        }
    }


    allSteps.forEach( ( step: StepDefinition ) => {
        stepDataCsv.write( step )
    } )

    stepDataCsv.end()
}

processFile()