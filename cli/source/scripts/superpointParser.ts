import readline from 'readline'
import fs from 'fs'
import { format } from '@fast-csv/format'
import pkgDir from 'pkg-dir'
import _ from 'lodash'

const rootDirectory = pkgDir.sync( __dirname )

interface RouteDefinition {
    name: string
    npcReferenceId: string
    type: string
    nodes: Array<Array<number>>
}

function cleanValue( value: string, first: string = '[', second: string = ']' ) : string {
    return value.replaceAll( first,'' ).replaceAll( second,'' )
}

async function processFile() : Promise<void> {
    const lines = readline.createInterface( {
        input: fs.createReadStream( `${rootDirectory}/superpointinfo.txt` ),
        crlfDelay: Infinity
    } )

    const walkingStream = fs.createWriteStream( `${rootDirectory}/npcWalkingRoutes.csv` )

    const walkingRoutesCsv = format( { delimiter: ',', headers: true } )
    walkingRoutesCsv.pipe( walkingStream )

    let currentRoute : RouteDefinition
    const allRoutes : Record<string, RouteDefinition> = {}

    for await ( const line of lines ) {
        const chunks = line.replaceAll( '\t', '' ).split( '=' )

        switch ( chunks[ 0 ] ) {
            /*
                It is possible to have a name being empty. However invalid it can be,
                we must continue processing using valid name.
             */
            case 'superpoint_name':
                let name = cleanValue( chunks[ 1 ] )
                if ( name ) {
                    currentRoute = {
                        name,
                        npcReferenceId: undefined,
                        nodes: [],
                        type: undefined
                    }
                }

                break

            case 'npc_name':
                if ( currentRoute ) {
                    currentRoute.npcReferenceId = cleanValue( chunks[ 1 ] )
                }
                break

            case 'move_type':
                if ( currentRoute ) {
                    currentRoute.type = cleanValue( chunks[ 1 ] )
                }
                break

            case 'fstring_index':
            case 'social_number':
            case 'delay':
                if ( chunks[ 1 ] !== '-1' ) {
                    throw new Error( `${chunks[ 0 ]} property is not supported` )
                }
                break

            case 'superpoint_end':
                if ( currentRoute ) {
                    allRoutes[ currentRoute.name ] = currentRoute
                    currentRoute = null
                }

                break

            default:
                if ( chunks[ 0 ].startsWith( 'node_begin' ) && currentRoute ) {
                    let positionValues = chunks[ 0 ].replace( 'node_begin// ', '' ).split( ',' ).map( value => parseInt( value, 10 ) )

                    if ( positionValues.length !== 3 ) {
                        throw new Error( `Wrong position size: ${ positionValues.length } in route name: ${ currentRoute.name }` )
                    }

                    currentRoute.nodes.push( positionValues )
                    break
                }

                break
        }
    }

    Object.values( allRoutes ).forEach( route => {
        walkingRoutesCsv.write( {
            name: route.name,
            type: route.type,
            npcReferenceId: route.npcReferenceId,
            nodesJson: JSON.stringify( route.nodes )
        } )
    } )
    walkingRoutesCsv.end()
}

processFile()