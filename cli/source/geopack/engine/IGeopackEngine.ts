export interface IGeopackEngine {
    executeQuery( query: string ) : Promise<void>
    finish() : Promise<void>
    insertMany( query: string, items: Array<Array<any>> ) : Promise<void>
    insertOne( query: string, parameters: Array<any> ) : Promise<void>
    start( parameters : any ) : Promise<void>
}