import { IGeopackEngine } from '../engine/IGeopackEngine'

export interface IGeoTransformer {
    startProcess( engine: IGeopackEngine ) : Promise<void>
    getCurrentProcessMessage() : string
}