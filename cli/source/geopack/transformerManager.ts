import { IGeoTransformer } from './interface/IGeoTransformer'
import { GeoindexTransformer } from './transformer/GeoindexTransformer'
import { access } from 'fs/promises'
import to from 'await-to-js'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'

const rootDirectory = pkgDir.sync( __dirname )
const fileDirectory = 'geodata'

export async function createGeopackTransformers() : Promise<Array<IGeoTransformer>> {
    let path = `${ rootDirectory }/${ fileDirectory }`
    let [ error ] = await to( access( path ) )
    if ( error ) {
        return []
    }

    const files : Array<string> = searchFiles( 'l2j', path )

    return files.map( ( fileName : string ) : IGeoTransformer => {
        return new GeoindexTransformer( fileName )
    } )
}