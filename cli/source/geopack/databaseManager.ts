import { SQLiteEngine } from './engine/sqlite'
import { IGeopackEngine } from './engine/IGeopackEngine'
import { SelectDatabaseDriverPromptOption } from '../ui/operations/prompts/serverDatabase'

const availableEngines = {
    [ SelectDatabaseDriverPromptOption.sqlite ]: SQLiteEngine
}

export function getGeopackEngine( name ) : IGeopackEngine {
    return availableEngines[ name ]
}