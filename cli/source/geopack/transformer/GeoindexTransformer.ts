import { IGeoTransformer } from '../interface/IGeoTransformer'
import { IGeopackEngine } from '../engine/IGeopackEngine'
import _ from 'lodash'
import { GeoBlockType, GeoConverter, GeoConverterValues } from '../helper/GeoConverter'
import * as fs from 'fs/promises'
import path from 'path'
import { createHash } from 'node:crypto'

const columns = [
    'regionX',
    'regionY',
    'data',
    'statistics_json'
]

const query = `INSERT INTO geoindex (${ columns.join( ',' ) }) values (${ _.join( _.times( columns.length, _.constant( '?' ) ), ',' ) })`
export class GeoindexTransformer implements IGeoTransformer {
    currentPath: string

    constructor( path: string ) {
        this.currentPath = path
    }

    getCurrentProcessMessage(): string {
        return this.currentPath
    }

    private createHash( data: Buffer ) : string {
        return createHash( 'sha512' ).update( data ).digest( 'hex' )
    }

    async startProcess( engine: IGeopackEngine ): Promise<void> {
        let data : Buffer = await fs.readFile( this.currentPath )
        let [ regionX, regionY ] = path.basename( this.currentPath )
                .replace( '.l2j', '' )
                .split( '_' )
                .map( value => _.parseInt( value ) )

        return engine.insertOne( query, this.convertData( regionX, regionY, data ) )
    }

    convertData( regionX : number, regionY : number, data: Buffer ) : Array<any> {
        const converter = new GeoConverter( data )
        let blocks = _.times( GeoConverterValues.BlocksInRegion, () : Buffer => {
            let type : GeoBlockType = converter.readId()

            switch ( type ) {
                case GeoBlockType.Flat:
                    return converter.getFlatBlock()

                case GeoBlockType.MultiHeight:
                    return converter.getComplexBlock()

                case GeoBlockType.MultiLayer:
                    return converter.getMultilayerBlock()
            }

            throw new Error( `Unknown GeoBlockType value: ${type}` )
        } )

        let convertedData = Buffer.concat( blocks )
        let statistics = {
            ...converter.statistics,
            originalSize: Math.floor( data.length / 1024 ),
            convertedSize: Math.floor( convertedData.length / 1024 ),
            sha512: this.createHash( convertedData )
        }

        return [ regionX, regionY, convertedData, JSON.stringify( statistics ) ]
    }
}