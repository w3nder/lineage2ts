import { DatapackEngineName } from '../../../datapack/engine/IDatapackEngine'
import { SQLiteDefaultParameters } from '../../../datapack/engine/sqlite'
// @ts-ignore
import { Select, Input } from 'enquirer'
export enum DatapackOperations {
    create = 'Create pack',
    exit = 'Exit'
}

export function DatapackOperationsPrompt() {
    return new Select( {
        name: 'command',
        message: 'Choose Datapack operation',
        choices: [ DatapackOperations.create, DatapackOperations.exit ],
    } )
}

export function SelectDatapackNamePrompt() {
    return new Input( {
        name: 'connection',
        message: 'Please provide database file name:',
        initial: SQLiteDefaultParameters.datapackName,
    } )
}

export function SelectDatapackDriverPrompt() {
    return new Select( {
        name: 'driver',
        message: 'Pick database type',
        choices: [ DatapackEngineName.sqlite ],
    } )
}