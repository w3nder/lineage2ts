// @ts-ignore
import { Select, Input, Form, Toggle } from 'enquirer'


export function SelectCommandPrompt() {
    return new Select( {
        name: 'command',
        message: 'Command to perform',
        choices: [ 'Install', 'Create-User', 'Remove-User', 'Exit' ]
    } )
}

export const enum SelectDatabaseDriverPromptOption {
    sqlite = 'SQLite'
}

export function SelectDatabaseDriverPrompt() {
    return new Select( {
        name: 'driver',
        message: 'Pick database type',
        choices: [ SelectDatabaseDriverPromptOption.sqlite ]
    } )
}

export const enum SelectDatabasePromptOption {
    All = 'Both',
    Login = 'Login',
    Game = 'Game'
}

export function SelectDatabasePrompt() {
    return new Select( {
        name: 'database',
        message: 'Create service database',
        choices: [ 'Both','Login', 'Game' ]
    } )
}

export function MariaDBSettingsPrompt() {
    return new Form( {
        name: 'connection',
        message: 'Please provide database connection settings:',
        choices: [
            { name: 'host', message: 'Host or IP', initial: 'localhost' },
            { name: 'port', message: 'Port', initial: '3306' },
            { name: 'user', message: 'Root account Username', initial: 'root' },
            { name: 'password', message: 'Root account Password', initial: 'root' }
        ]
    } )
}

export function CreateUserFieldsPrompt() {
    return [
        new Form( {
            name: 'credentials',
            message: 'Create new user with credentials:',
            choices: [
                { name: 'user', message: 'Login username' },
                { name: 'password', message: 'Password' }
            ]
        } ),
        new Toggle( {
            name: 'isAdmin',
            message: 'Is Admin User?',
            enabled: 'Yes',
            disabled: 'No'
        } )
    ]
}

export function UserNamePrompt() {
    return new Input( {
        message: 'Existing login:'
    } )
}