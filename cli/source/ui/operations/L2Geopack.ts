import { CLIProperties, ICLIOperation } from './ICLIOperation'
import { SelectDatabaseDriverPrompt, SelectDatabaseDriverPromptOption } from './prompts/serverDatabase'
import { GeopackOperationsPrompt, SelectGeopackNamePrompt } from './prompts/geopackPrompts'
import { createGeopackTransformers } from '../../geopack/transformerManager'
import { IGeoTransformer } from '../../geopack/interface/IGeoTransformer'
import { getGeopackEngine } from '../../geopack/databaseManager'
import aigle from 'aigle'
import _ from 'lodash'
import { SQLiteDefaultParameters } from '../../datapack/engine/sqlite'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'
import cliProgress from 'cli-progress'
import logSymbols from 'log-symbols'
import perfy from 'perfy'
import to from 'await-to-js'

export interface L2GeopackProperties extends CLIProperties {
    fileName?: string
}

export class L2Geopack implements ICLIOperation {
    private databaseEngine: string
    private connectionParameters: any

    async getConnectionParameters() {
        switch ( this.databaseEngine ) {
            case SelectDatabaseDriverPromptOption.sqlite:
                return SelectGeopackNamePrompt().run()

            default:
                throw new Error( `${ this.databaseEngine } database type is not defined to process connection parameters.` )
        }
    }

    async getDatabaseSelection() {

        if ( this.databaseEngine && this.connectionParameters ) {
            return this.databaseEngine
        }

        this.databaseEngine = await SelectDatabaseDriverPrompt().run()
        this.connectionParameters = await this.getConnectionParameters()

        await getGeopackEngine( this.databaseEngine ).start( this.connectionParameters )
    }

    async importData() : Promise<void> {
        let databaseEngine = this.databaseEngine
        let errors = []
        let progressBar = new cliProgress.SingleBar( {}, cliProgress.Presets.shades_classic )
        let transformers : Array<IGeoTransformer> = await createGeopackTransformers()

        if ( transformers.length === 0 ) {
            console.log( logSymbols.error, 'Unable to locate geopack files.' )
            return
        }

        progressBar.start( _.size( transformers ), 0 )

        perfy.start( 'geopack-data' )
        await aigle.resolve( transformers ).eachSeries( async ( transformer: IGeoTransformer ) => {
            let [ error ] = await to( transformer.startProcess( getGeopackEngine( databaseEngine ) ) )
            progressBar.increment()

            if ( error ) {
                errors.push( `Failed processing '${ transformer.getCurrentProcessMessage() }' with error: ${error.stack}` )
            }
        } )

        let metrics = perfy.end( 'geopack-data' )

        progressBar.stop()

        if ( errors.length > 0 ) {
            errors.forEach( ( message ) => {
                console.log( logSymbols.error, message )
            } )
        }

        console.log( logSymbols.info, `Processed ${ transformers.length } Geopack operations with ${ errors.length } errors in ${metrics.time} seconds` )
    }

    async installTables() {

        let databaseEngine = this.databaseEngine
        const rootDirectory = await pkgDir( __dirname )
        let files = searchFiles( 'sql', `${ rootDirectory }/sql/${ databaseEngine.toLowerCase() }/geopack` )

        let errors = 0
        await aigle.resolve( files ).eachSeries( async ( currentFile ) => {
            let contents = await fs.readFile( currentFile )
            let [ error ] = await to( getGeopackEngine( databaseEngine ).executeQuery( contents.toString() ) )

            if ( error ) {
                errors++
                return console.log( logSymbols.error, `Failed processing file '${ currentFile }' with error:`, error.message )
            }
        } )

        if ( errors > 0 ) {
            console.log( logSymbols.warning, `Processed ${ files.length } geopack setup files with ${ errors } errors` )
        }

        console.log( logSymbols.success, 'Process finished Geopack initialization.' )
    }

    async interactiveRun(): Promise<void> {
        await this.getDatabaseSelection()

        while ( true ) {
            let command = _.toLower( await GeopackOperationsPrompt().run() )
            switch ( command ) {
                case 'install':
                    await this.installTables()
                    await this.importData()
                    break

                default:
                    return this.finishRun()
            }
        }
    }

    async finishRun() : Promise<void> {
        await getGeopackEngine( this.databaseEngine ).finish()
        this.databaseEngine = null
    }

    async silentRun( properties: L2GeopackProperties ): Promise<void> {
        this.databaseEngine = SelectDatabaseDriverPromptOption.sqlite
        let fileName = _.defaultTo( properties.fileName, SQLiteDefaultParameters.geopackName )

        console.log( logSymbols.info, 'Geopack is using', this.databaseEngine )
        console.log( logSymbols.info, 'Geopack file name:', fileName )

        await getGeopackEngine( this.databaseEngine ).start( fileName )

        await this.installTables()
        await this.importData()

        return this.finishRun()
    }
}