import {
    CreateUserFieldsPrompt, MariaDBSettingsPrompt,
    SelectCommandPrompt,
    SelectDatabaseDriverPrompt, SelectDatabaseDriverPromptOption,
    SelectDatabasePrompt, SelectDatabasePromptOption,
    UserNamePrompt,
} from './prompts/serverDatabase'
import { CLIProperties, ICLIOperation } from './ICLIOperation'
import { getDatabaseEngine } from '../../database/databaseManager'
import aigle from 'aigle'
import to from 'await-to-js'
import _ from 'lodash'
import { searchFiles } from 'find-file-extension'
import pkgDir from 'pkg-dir'
import * as fs from 'fs/promises'
import * as blake3 from 'blake3'
import logSymbols from 'log-symbols'

const hashOptions = {
    length: 64,
}

export interface L2ServerDatabaseProperties extends CLIProperties {
    databaseType?: string
    databaseDestination?: SelectDatabasePromptOption
}

export class L2ServerDatabase implements ICLIOperation {
    private connectionParameters: any
    private databaseEngine: string

    constructor() {
        this.connectionParameters = null
        this.databaseEngine = null
    }

    async interactiveRun() : Promise<void> {
        await this.getDatabaseSelection()

        while ( true ) {
            let command = _.toLower( await SelectCommandPrompt().run() )
            switch ( command ) {
                case 'install':
                    let installationType = await SelectDatabasePrompt().run()
                    await this.installDatabases( installationType )
                    break

                case 'create-user':
                    await this.createLoginUser()
                    break

                case 'remove-user':
                    await this.removeLoginUser()
                    break

                default:
                    return this.finishRun()
            }
        }
    }

    async silentRun( properties: L2ServerDatabaseProperties ) : Promise<void> {
        this.databaseEngine = _.defaultTo( properties.databaseType, SelectDatabaseDriverPromptOption.sqlite )
        let destination : SelectDatabasePromptOption = _.defaultTo( properties.databaseDestination, SelectDatabasePromptOption.All )

        console.log( logSymbols.info, 'Provisioning server database using', this.databaseEngine )
        console.log( logSymbols.info, 'Installation database type is', destination )

        await getDatabaseEngine( this.databaseEngine ).start( {} )
        await this.installDatabases( destination )

        return this.finishRun()
    }

    async installDatabases( installationType: SelectDatabasePromptOption ) : Promise<void> {

        let databaseEngine = this.databaseEngine
        const rootDirectory = await pkgDir( __dirname )
        let loginFiles = searchFiles( 'sql',`${rootDirectory}/sql/${databaseEngine.toLowerCase()}/login` )
        let gameFiles = searchFiles( 'sql',`${rootDirectory}/sql/${databaseEngine.toLowerCase()}/game` )


        if ( [ SelectDatabasePromptOption.All, SelectDatabasePromptOption.Login ].includes( installationType ) ) {
            let errors = 0
            await aigle.resolve( loginFiles ).eachSeries( async ( currentFile ) => {
                let contents = await fs.readFile( currentFile )
                let [ error ] = await to( getDatabaseEngine( databaseEngine ).executeLoginQuery( contents.toString() ) )

                if ( error ) {
                    errors++
                    return console.log( logSymbols.error, `Failed processing file '${currentFile}' with error:`, error.stack )
                }
            } )

            if ( errors > 0 ) {
                console.log( logSymbols.warning, `Processed ${loginFiles.length} login files with ${errors} errors` )
            }
        }

        if ( [ SelectDatabasePromptOption.All, SelectDatabasePromptOption.Game ].includes( installationType ) ) {
            let errors = 0
            await aigle.resolve( gameFiles ).eachSeries( async ( currentFile ) => {
                let contents = await fs.readFile( currentFile )
                let [ error ] = await to( getDatabaseEngine( databaseEngine ).executeGameQuery( contents.toString() ) )

                if ( error ) {
                    errors++
                    return console.log( logSymbols.error, `Failed processing file '${currentFile}' with error:`, error.stack )
                }
            } )

            if ( errors > 0 ) {
                console.log( logSymbols.warning, `Processed ${gameFiles.length} game files with ${errors} errors` )
            }
        }

        console.log( logSymbols.success, 'Process finished database initialization' )
    }

    async createLoginUser() {
        let [ credentials, isAdmin ] = await aigle.resolve( CreateUserFieldsPrompt() ).mapSeries( ( currentPrompt ) => currentPrompt.run() )
        let existingUser = await getDatabaseEngine( this.databaseEngine ).getLoginUser( credentials.user )

        if ( existingUser ) {
            return console.log( logSymbols.error, `Login for user '${credentials.user}' already exists! Choose different user name.` )
        }

        let userInformation = {
            user: credentials.user,
            accessLevel: isAdmin ? 8 : 0,
            password: this.createPasswordHash( credentials.password )
        }

        await getDatabaseEngine( this.databaseEngine ).createLoginUser( userInformation )
        console.log( logSymbols.success, `Created new user '${credentials.user}'` )
    }

    async removeLoginUser() {
        let userName = await UserNamePrompt().run()
        let existingUser = await getDatabaseEngine( this.databaseEngine ).getLoginUser( userName )

        if ( existingUser ) {
            await getDatabaseEngine( this.databaseEngine ).removeLoginUser( userName )
            return console.log( logSymbols.success, `Removed user '${userName}'` )
        }

        console.log( logSymbols.error, `Login for user '${userName}' does not exist! Nothing to remove.` )
    }

    async getDatabaseSelection() {

        if ( this.databaseEngine && this.connectionParameters ) {
            return this.databaseEngine
        }

        this.databaseEngine = await SelectDatabaseDriverPrompt().run()
        this.connectionParameters = await this.getConnectionParameters()

        await getDatabaseEngine( this.databaseEngine ).start( this.connectionParameters )

        return this.databaseEngine
    }

    async getConnectionParameters() {
        switch ( this.databaseEngine ) {
            case 'mariadb':
                return MariaDBSettingsPrompt().run()

            case SelectDatabaseDriverPromptOption.sqlite:
                return

            default:
                throw new Error( `${this.databaseEngine} database type is not defined to process connection parameters.` )
        }
    }

    /*
        Blake3 is fast hash, which means brute-force attacks can unmask password faster than slow algorithms,
        which is not desired, hence double hashing of the hash with sufficient length makes
        brute-force attack much less likely to succeed if at all.
     */

    createPasswordHash( value: string ): string {
        return blake3.hash( blake3.hash( value, hashOptions ).toString( 'base64' ), hashOptions ).toString( 'base64' )
    }

    async finishRun() : Promise<void> {
        await getDatabaseEngine( this.databaseEngine ).finish()
        this.databaseEngine = null
    }
}