export interface CLIProperties {}

export interface ICLIOperation {
    interactiveRun() : Promise<void>
    silentRun( properties: CLIProperties ) : Promise<void>
}