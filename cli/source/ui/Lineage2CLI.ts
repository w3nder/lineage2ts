import { L2ServerDatabaseProperties, L2ServerDatabase } from './operations/L2ServerDatabase'
import { L2Datapack, L2DatapackProperties } from './operations/L2Datapack'
import { L2Geopack, L2GeopackProperties } from './operations/L2Geopack'
import _ from 'lodash'
import { ICLIOperation } from './operations/ICLIOperation'
import aigle from 'aigle'
// @ts-ignore
import { Select } from 'enquirer'

const enum CLIOperations {
    serverDatabase = 'Server Database',
    datapack = 'Datapack',
    geopack = 'Geopack',
    exit = 'Exit',
}

function selectOperationsPrompt() {
    return new Select( {
        name: 'operation',
        message: 'Operation type?',
        choices: [
            CLIOperations.serverDatabase,
            CLIOperations.datapack,
            CLIOperations.geopack,
            CLIOperations.exit,
        ],
    } )
}



const enum OperationPropertyName {
    Datapack = 'provisionDatapack',
    Database = 'provisionDatabase',
    Geopack = 'provisionGeopack'
}

const operationProperties : Array<OperationPropertyName> = [ OperationPropertyName.Database, OperationPropertyName.Datapack, OperationPropertyName.Geopack ]

export class Lineage2CLI {
    operations: Array<OperationPropertyName>
    databaseProperties: L2ServerDatabaseProperties
    datapackProperties: L2DatapackProperties
    geopackProperties: L2GeopackProperties

    constructor( programParameters ) {
        let parameters = programParameters.opts()
        this.operations = operationProperties.filter( property => !!parameters[ property ] )

        this.databaseProperties = {
            databaseType: programParameters.databaseType,
            databaseDestination: programParameters.databaseDestination
        }

        this.datapackProperties = {
            fileName: programParameters.datapackName
        }

        this.geopackProperties = {
            fileName: programParameters.geopackName
        }
    }

    async start(): Promise<void> {
        if ( !_.isEmpty( this.operations ) ) {
            return this.performSilentRun()
        }

        return this.performInteractiveRun()
    }

    async performInteractiveRun() : Promise<void> {
        while ( true ) {
            let operation: CLIOperations = await selectOperationsPrompt().run()

            let cliOperation = this.getOperation( operation )
            if ( !cliOperation ) {
                return
            }

            await cliOperation.interactiveRun()
        }
    }

    async performSilentRun() : Promise<void> {
        await aigle.resolve( this.operations ).eachSeries( ( name: OperationPropertyName ) : Promise<void> => {
            switch ( name ) {
                case OperationPropertyName.Datapack:
                    return this.getOperation( CLIOperations.datapack ).silentRun( this.datapackProperties )

                case OperationPropertyName.Database:
                    return this.getOperation( CLIOperations.serverDatabase ).silentRun( this.databaseProperties )

                case OperationPropertyName.Geopack:
                    return this.getOperation( CLIOperations.geopack ).silentRun( this.geopackProperties )
            }
        } )
    }

    getOperation( operation: CLIOperations ) : ICLIOperation {
        switch ( operation ) {
            case CLIOperations.serverDatabase:
                return new L2ServerDatabase( )

            case CLIOperations.datapack:
                return new L2Datapack()

            case CLIOperations.geopack:
                return new L2Geopack()

            default:
                return null
        }
    }
}