# Lineage2TS Project CLI

## Introduction

- utility to provision various resources login and game servers would need
- such resources include database setup, game server data/geo pack creation, game server operations

## How to run CLI?

- install dependencies via `npm install`
- run cli code via `npm run cli`

## What do I need to create game and login server databases?

Two ways to create database files, either run `npm run databases` or follow steps:

- run cli
- choose `operation type` as `Server Database`
- choose `SQLite` (MariaDB is not currently supported)
- choose `Install`
- choose `Both`
- observe `login.database` and `game.database` files are created
- choose `Exit` and `Exit` to terminate cli application

## What do I need to compile datapack?

Obtain data and make it ready:
- download https://bitbucket.org/l2jserver/l2j-server-datapack/get/develop.zip
- extract files of zip file from `/src/main/resources/data` into `./data` directory relative to `package.json`
- copy contents of zip file from `/src/main/java/com/l2jserver` into `./data` directory relative to `package.json`
- 
Two ways to create datapack file, either run `npm run datapack` or follow steps:

- run `npm run cli`
- choose `operation type` as `Datapack`
- leave default filename as `datapack.database`
- choose `Create pack`
- wait until process finishes
- choose `Exit` and `Exit` again to terminate cli application

## What do I need to compile geopack?

First you need to prepare L2J geodata via following steps:

- download https://bitbucket.org/l2jgeo/l2j_geodata/get/master.zip
- extract files of zip file from `/geodata` into `./geodata` directory relative to `package.json`

Two ways to create geopack file, either run `npm run geopack` or follow steps:

- run `npm run cli`
- choose `operation type` as `Geopack`
- choose `Install`
- wait till process finishes
- choose `Exit` and `Exit` to terminate cli application

## Non-interactive run
- perform initial download steps for datapack and geopack data files
- run cli in non-interactive mode via `npm run cli -- --provision-datapack --provision-database --provision-geopack` (remove provision options as needed)
- useful when provision operations need to run inside CI or a container

## Contributions
Any and all code contributions are welcome. Please see [template](../.gitlab/merge_request_templates/cli.md) for code change acceptance actions.

## License
Any and all Lineage2TS Project files use `AGPL-3.0-or-later` license, see [LICENSE](LICENSE) file for legal description