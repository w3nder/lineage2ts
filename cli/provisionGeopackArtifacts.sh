#!/bin/sh

mkdir working-geo
cd working-geo || exit

curl -OL https://bitbucket.org/l2jgeo/l2j_geodata/get/master.zip
unzip -q master.zip

cp -r l2jgeo-l2j_geodata*/geodata ../geodata
cd ..

npm run geopack

mv geopack.database ../geopack.database