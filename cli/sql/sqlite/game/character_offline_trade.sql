CREATE TABLE IF NOT EXISTS `character_offline_trade` (
  `charId` INTEGER NOT NULL,
  `time` INTEGER NOT NULL DEFAULT 0,
  `type` INTEGER NOT NULL DEFAULT 0,
  `title` TEXT DEFAULT NULL,
  PRIMARY KEY (`charId`)
);