CREATE TABLE IF NOT EXISTS `merchant_lease` (
  `merchant_id` INTEGER NOT NULL DEFAULT 0,
  `player_id` INTEGER NOT NULL DEFAULT 0,
  `bid` INTEGER,
  `type` INTEGER NOT NULL DEFAULT 0,
  `player_name` INTEGER,
  PRIMARY KEY (`merchant_id`,`player_id`,`type`)
);