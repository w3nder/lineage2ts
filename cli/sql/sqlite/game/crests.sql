CREATE TABLE IF NOT EXISTS `crests` (
	`crest_id` INTEGER,
	`data` BLOB NOT NULL,
	`type` INTEGER NOT NULL,
	PRIMARY KEY(`crest_id`)
);