CREATE TABLE IF NOT EXISTS `character_item_reuse` (
  `playerId` INTEGER NOT NULL DEFAULT 0,
  `itemId` INTEGER NOT NULL DEFAULT 0,
  `reuseDelay` INTEGER NOT NULL DEFAULT 0,
  `reuseExpiration` INTEGER NOT NULL DEFAULT 0
);