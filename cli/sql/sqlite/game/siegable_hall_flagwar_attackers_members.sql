CREATE TABLE IF NOT EXISTS `siegable_hall_flagwar_attackers_members` (
  `hall_id` INTEGER NOT NULL DEFAULT 0,
  `clan_id` INTEGER NOT NULL DEFAULT 0,
  `object_id` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`hall_id`, `clan_id`, `object_id`)
);