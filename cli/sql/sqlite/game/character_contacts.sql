CREATE TABLE IF NOT EXISTS `character_contacts` (
  charId INTEGER NOT NULL DEFAULT 0,
  contactId INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY(`charId`,`contactId`)
);