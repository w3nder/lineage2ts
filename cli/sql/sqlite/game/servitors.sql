CREATE TABLE IF NOT EXISTS `servitors` (
  `ownerId` INTEGER NOT NULL,
  `summonSkillId` INTEGER NOT NULL,
  `hp` INTEGER DEFAULT 0,
  `mp` INTEGER DEFAULT 0,
  `remainingTime` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`ownerId`,`summonSkillId`)
);