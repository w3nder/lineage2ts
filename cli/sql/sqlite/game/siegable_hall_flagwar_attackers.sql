CREATE TABLE IF NOT EXISTS `siegable_hall_flagwar_attackers` (
  `hall_id` INTEGER NOT NULL DEFAULT 0,
  `flag` INTEGER NOT NULL DEFAULT 0,
  `npc` INTEGER NOT NULL DEFAULT 0,
  `clan_id` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`flag`, `hall_id`, `clan_id`)
) ;