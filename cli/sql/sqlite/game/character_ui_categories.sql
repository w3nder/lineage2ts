CREATE TABLE IF NOT EXISTS `character_ui_categories` (
  `objectId` INTEGER NOT NULL DEFAULT 0,
  `categoryId` INTEGER NOT NULL,
  `orderIndex` INTEGER NOT NULL,
  `commandId` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`objectId`,`categoryId`,`orderIndex`)
);