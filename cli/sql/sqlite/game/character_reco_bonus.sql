CREATE TABLE IF NOT EXISTS `character_reco_bonus` (
  `charId` INTEGER NOT NULL,
  `rec_have` INTEGER NOT NULL DEFAULT 0,
  `rec_left` INTEGER NOT NULL DEFAULT 0,
  `time_left` INTEGER NOT NULL DEFAULT 0,
  UNIQUE(`charId`)
);