CREATE TABLE IF NOT EXISTS `pets` (
  `controlId` INTEGER NOT NULL,
  `name` TEXT,
  `level` INTEGER NOT NULL,
  `hp` INTEGER DEFAULT 0,
  `mp` INTEGER DEFAULT 0,
  `exp` INTEGER DEFAULT 0,
  `sp` INTEGER DEFAULT 0,
  `feed` INTEGER DEFAULT 0,
  `ownerId` INTEGER NOT NULL DEFAULT 0,
  `isRestored` TEXT CHECK( isRestored IN ('true','false') ) NOT NULL DEFAULT 'false',
  PRIMARY KEY (`controlId`, `ownerId`)
) ;