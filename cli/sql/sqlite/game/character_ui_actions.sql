CREATE TABLE IF NOT EXISTS `character_ui_actions` (
  `objectId` INTEGER NOT NULL DEFAULT 0,
  `categoryId` INTEGER NOT NULL,
  `commandId` INTEGER NOT NULL DEFAULT 0,
  `orderIndex` INTEGER NOT NULL DEFAULT 0,
  `key` INTEGER NOT NULL,
  `toggleOne` INTEGER DEFAULT NULL,
  `toggleTwo` INTEGER DEFAULT NULL,
  `visible` INTEGER NOT NULL,
  PRIMARY KEY (`objectId`,`categoryId`,`commandId`)
);