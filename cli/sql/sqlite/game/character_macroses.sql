CREATE TABLE IF NOT EXISTS `character_macroses` (
  `charId` INTEGER NOT NULL DEFAULT 0,
  `id` INTEGER NOT NULL DEFAULT 0,
  `icon` INTEGER,
  `name` TEXT,
  `descr` TEXT,
  `acronym` TEXT,
  `commands` TEXT,
  PRIMARY KEY (`charId`,`id`)
);