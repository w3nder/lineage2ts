CREATE TABLE IF NOT EXISTS `character_pet_skills_save` (
  `petObjItemId` INTEGER NOT NULL DEFAULT 0,
  `skill_id` INTEGER NOT NULL DEFAULT 0,
  `skill_level` INTEGER NOT NULL DEFAULT 1,
  `remaining_time` INTEGER NOT NULL DEFAULT 0,
  `buff_index` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`petObjItemId`,`skill_id`,`skill_level`)
) ;