CREATE TABLE IF NOT EXISTS `character_offline_trade_items` (
  `charId` INTEGER NOT NULL,
  `item` INTEGER NOT NULL DEFAULT 0, -- itemId(for buy) & ObjectId(for sell)
  `count` INTEGER NOT NULL DEFAULT 0,
  `price` INTEGER NOT NULL DEFAULT 0,
  Primary Key (`charId`,`item`)
);