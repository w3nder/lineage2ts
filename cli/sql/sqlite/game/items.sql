CREATE TABLE IF NOT EXISTS `items` (
  `owner_id` INTEGER, -- object id of the player or clan,owner of this item
  `object_id` INTEGER NOT NULL PRIMARY KEY, -- object id of the item
  `item_id` INTEGER,
  `count` INTEGER NOT NULL DEFAULT 0,
  `enchant_level` INTEGER,
  `locationType` INTEGER, -- inventory,paperdoll,npc,clan warehouse,pet,and so on
  `loc_data` INTEGER,    -- depending on location: equiped slot,npc id,pet id,etc
  `time_of_use` INTEGER, -- time of item use, for calculate of breackages
  `custom_type1` INTEGER DEFAULT 0,
  `custom_type2` INTEGER DEFAULT 0,
  `mana_left` NUMERIC NOT NULL DEFAULT -1,
  `time` NUMERIC NOT NULL DEFAULT 0,
  `agathion_energy` INTEGER NOT NULL DEFAULT 0,
  `attributes` INTEGER DEFAULT NULL
);

CREATE INDEX `items_owner_location` ON `items` (owner_id,locationType);
CREATE INDEX `items_itemId_customType1` ON `items` (item_id,custom_type1);