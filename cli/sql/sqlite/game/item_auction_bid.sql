CREATE TABLE IF NOT EXISTS `item_auction_bid` (
  `id` INTEGER NOT NULL,
  `playerId` INTEGER NOT NULL,
  `amount` INTEGER NOT NULL,
  PRIMARY KEY (`id`,`playerId`)
);