CREATE TABLE IF NOT EXISTS `character_summon_skills_save` (
  `ownerId` INTEGER NOT NULL DEFAULT 0,
  `ownerClassIndex` INTEGER NOT NULL DEFAULT 0,
  `summonSkillId` INTEGER NOT NULL DEFAULT 0,
  `skill_id` INTEGER NOT NULL DEFAULT 0,
  `skill_level` INTEGER NOT NULL DEFAULT 1,
  `remaining_time` INTEGER NOT NULL DEFAULT 0,
  `buff_index` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`ownerId`,`ownerClassIndex`,`summonSkillId`,`skill_id`,`skill_level`)
);