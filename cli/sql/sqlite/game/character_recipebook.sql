CREATE TABLE IF NOT EXISTS `character_recipebook` (
  `charId` INTEGER NOT NULL DEFAULT 0,
  `id` NUMERIC NOT NULL DEFAULT 0,
  `classIndex` INTEGER NOT NULL DEFAULT 0,
  `type` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`,`charId`,`classIndex`)
);