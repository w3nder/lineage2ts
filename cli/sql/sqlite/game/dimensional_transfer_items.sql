CREATE TABLE IF NOT EXISTS `dimensional_transfer_items` (
  `playerId` INTEGER NOT NULL,
  `itemId` INTEGER NOT NULL,
  `amount` INTEGER NOT NULL,
  `sender` TEXT NOT NULL,
  `createDate` datetime default current_timestamp,
  `updateDate` datetime default current_timestamp,
  PRIMARY KEY (`playerId`,`itemId`)
);