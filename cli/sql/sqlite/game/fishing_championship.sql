DROP TABLE IF EXISTS `fishing_championship`;
CREATE TABLE IF NOT EXISTS `fishing_championship` (
  `playerId` INTEGER PRIMARY KEY NOT NULL,
  `fishLength` REAL NOT NULL,
  `rewardType`  INTEGER NOT NULL,
  `hasClaimedReward` TEXT CHECK( hasClaimedReward IN ('true','false') ) NOT NULL DEFAULT 'true'
);