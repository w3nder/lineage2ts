CREATE TABLE IF NOT EXISTS `server_variables` (
  `name`  TEXT PRIMARY KEY NOT NULL,
  `value` TEXT,
  `expirationDate` INTEGER DEFAULT 0
);