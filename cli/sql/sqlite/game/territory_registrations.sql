CREATE TABLE IF NOT EXISTS `territory_registrations` (
  `castleId` INTEGER NOT NULL DEFAULT 0,
  `registeredId` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`castleId`,`registeredId`)
);