CREATE TABLE IF NOT EXISTS `account_variables` (
  `id` TEXT PRIMARY KEY NOT NULL,
  `value_json` TEXT NOT NULL
);