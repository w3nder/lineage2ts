CREATE TABLE IF NOT EXISTS `clan_skills` (
  `clan_id` INTEGER NOT NULL DEFAULT 0,
  `skill_id` INTEGER NOT NULL DEFAULT 0,
  `skill_level` INTEGER NOT NULL DEFAULT 0,
  `skill_name` TEXT DEFAULT NULL,
  `sub_pledge_id` INTEGER NOT NULL DEFAULT '-2',
  PRIMARY KEY (`clan_id`,`skill_id`,`sub_pledge_id`)
) ;