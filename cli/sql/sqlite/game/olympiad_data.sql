CREATE TABLE IF NOT EXISTS `olympiad_data` (
  `id` INTEGER NOT NULL DEFAULT 0,
  `current_cycle` INTEGER NOT NULL DEFAULT 1,
  `period` INTEGER NOT NULL DEFAULT 0,
  `olympiad_end` INTEGER NOT NULL DEFAULT 0,
  `validation_end` INTEGER NOT NULL DEFAULT 0,
  `next_weekly_change` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
);