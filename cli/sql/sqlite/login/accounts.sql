CREATE TABLE IF NOT EXISTS `accounts` (
  `login` TEXT NOT NULL default '' PRIMARY KEY,
  `password` TEXT,
  `createdTime` INTEGER NOT NULL,
  `lastAccess` INTEGER NOT NULL,
  `accessLevel` INTEGER NOT NULL DEFAULT 0,
  `lastServerId` INTEGER DEFAULT 1,
  `variables_json`  TEXT NOT NULL DEFAULT '{}',
  `passwordVersion` INTEGER NOT NULL
);