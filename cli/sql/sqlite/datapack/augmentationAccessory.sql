DROP TABLE IF EXISTS `augmentation_accessory`;

CREATE TABLE IF NOT EXISTS `augmentation_accessory`
(
    `stoneId` INTEGER,
    `variationId` INTEGER,
    `categoryProbability` INTEGER,
    `chance_json` TEXT
);