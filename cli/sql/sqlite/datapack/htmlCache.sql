DROP TABLE IF EXISTS `html_cache`;

CREATE TABLE IF NOT EXISTS `html_cache`
(
    `name` TEXT PRIMARY KEY,
    `contents` TEXT,
    `actions_json` TEXT
);