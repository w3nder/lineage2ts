DROP TABLE IF EXISTS `skill_data`;

CREATE TABLE IF NOT EXISTS `skill_data`
(
    `id` INTEGER PRIMARY KEY ,
    `lastLevel` INTEGER,
    `name` TEXT,
    `enchantGroup1` INTEGER,
    `enchantGroup2` INTEGER,
    `enchantGroup3` INTEGER,
    `enchantGroup4` INTEGER,
    `enchantGroup5` INTEGER,
    `enchantGroup6` INTEGER,
    `enchantGroup7` INTEGER,
    `enchantGroup8` INTEGER,
    `isCustomSkill` TEXT,

    `table_json` TEXT,
    `set_json` TEXT,
    `enchantProperties_json` TEXT,
    `defaults_json` TEXT,
    `enchant1_json` TEXT,
    `enchant2_json` TEXT,
    `enchant3_json` TEXT,
    `enchant4_json` TEXT,
    `enchant5_json` TEXT,
    `enchant6_json` TEXT,
    `enchant7_json` TEXT,
    `enchant8_json` TEXT
);