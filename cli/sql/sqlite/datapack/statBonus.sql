DROP TABLE IF EXISTS `stat_bonus`;

CREATE TABLE IF NOT EXISTS `stat_bonus`
(
    `attribute` TEXT,
    `values_json` TEXT
);