DROP TABLE IF EXISTS `npc_data`;

CREATE TABLE IF NOT EXISTS `npc_data`
(
    `id` INTEGER PRIMARY KEY,
    `referenceId` TEXT,
    `level` INTEGER,
    `displayId` INTEGER,
    `type` TEXT,
    `name` TEXT,
    `usingServerSideName` TEXT,
    `title` TEXT,
    `usingServerSideTitle` TEXT,
    `race` TEXT,
    `corpsetime` INTEGER,
    `excrteffect` TEXT,
    `snpcPropHPRate` REAL,
    `parameters_json` TEXT,
    `equipment_json` TEXT,
    `acquire_json` TEXT,
    `stats_json` TEXT,
    `status_json` TEXT,
    `skilllist_json` TEXT,
    `shots_json` TEXT,
    `ai_json` TEXT,
    `droplists_json` TEXT,
    `collision_json` TEXT
);

CREATE INDEX `name_npc_data` ON `npc_data` (name,id);
CREATE INDEX `level_npc_data` ON `npc_data` (level,id);