DROP TABLE IF EXISTS `instance_names`;

CREATE TABLE IF NOT EXISTS `instance_names`
(
    `id` INTEGER PRIMARY KEY,
    `name` TEXT
);