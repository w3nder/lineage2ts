DROP TABLE IF EXISTS `armor_sets`;

CREATE TABLE IF NOT EXISTS `armor_sets` (
     `id` INTEGER,
     `name` TEXT,
     `items_json` TEXT,
     `skills_json` TEXT,
     `attributes_json` TEXT
);

CREATE INDEX `name_armor_sets` ON `armor_sets` (name,id);