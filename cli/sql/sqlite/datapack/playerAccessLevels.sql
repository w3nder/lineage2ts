DROP TABLE IF EXISTS `player_access_levels`;

CREATE TABLE IF NOT EXISTS `player_access_levels` (
   `level` INTEGER PRIMARY KEY,
   `name` TEXT,
   `nameColor` TEXT,
   `titleColor` TEXT,
   `permissions_json` TEXT,
   `commands_json` TEXT
);