DROP TABLE IF EXISTS `npcbuffs_data`;

CREATE TABLE IF NOT EXISTS `npcbuffs_data`
(
    `npcId` INTEGER,
    `groupId` INTEGER,
    `skillId` INTEGER,
    `skillLevel` INTEGER,
    `consumedItemId` INTEGER,
    `consumedItemAmount` INTEGER,
    PRIMARY KEY (`npcId`,`skillId`,`groupId`)
);