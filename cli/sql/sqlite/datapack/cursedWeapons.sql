DROP TABLE IF EXISTS `cursed_weapons`;

CREATE TABLE IF NOT EXISTS `cursed_weapons`
(
    `itemId` INTEGER PRIMARY KEY,
    `name` TEXT,
    `skillId` INTEGER,
    `disappearChance` INTEGER,
    `dropRate` INTEGER,
    `duration` INTEGER,
    `durationLost` INTEGER,
    `stageKills` INTEGER
);