DROP TABLE IF EXISTS `enchant_item_scrolls`;
DROP TABLE IF EXISTS `enchant_item_support`;

CREATE TABLE IF NOT EXISTS `enchant_item_scrolls`
(
    `id` INTEGER,
    `targetGrade` TEXT,
    `bonusRate` INTEGER,
    `maxEnchant` INTEGER,
    `scrollGroupId` INTEGER,
    `items_json` TEXT
);

CREATE TABLE IF NOT EXISTS `enchant_item_support`
(
    `id` INTEGER,
    `targetGrade` TEXT,
    `bonusRate` INTEGER,
    `maxEnchant` INTEGER
);