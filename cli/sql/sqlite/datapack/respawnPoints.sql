DROP TABLE IF EXISTS `respawn_points`;

CREATE TABLE IF NOT EXISTS `respawn_points`
(
    `name` TEXT PRIMARY KEY,
    `pointsJson` TEXT NOT NULL,
    `pvpJson` TEXT NOT NULL,
    `bannedRaceJson` TEXT,
    `bbs` INTEGER,
    `messageId` INTEGER,
    `mapsJson` TEXT
);