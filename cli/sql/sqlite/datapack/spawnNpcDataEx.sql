DROP TABLE IF EXISTS `spawn_npc_data_ex`;

CREATE TABLE IF NOT EXISTS `spawn_npc_data_ex`
(
    `npcId` INTEGER NOT NULL,
    `makerId` TEXT NOT NULL,
    `position_json` TEXT,
    `amount` INTEGER,
    `respawnMs` INTEGER,
    `respawnExtraMs` INTEGER,
    `aiName` TEXT,
    `ai_json` TEXT,
    `chaseDistance` INTEGER,
    `recordId` TEXT,
    `minions_json` TEXT
);