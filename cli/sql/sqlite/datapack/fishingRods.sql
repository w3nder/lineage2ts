DROP TABLE IF EXISTS `fishing_rods`;

CREATE TABLE IF NOT EXISTS `fishing_rods`
(
    `id` INTEGER PRIMARY KEY,
    `itemId` INTEGER,
    `level` INTEGER,
    `name` TEXT,
    `damage` REAL
);