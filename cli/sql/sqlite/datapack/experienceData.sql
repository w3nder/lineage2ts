DROP TABLE IF EXISTS `experience_data`;

CREATE TABLE IF NOT EXISTS `experience_data`
(
    `level` INTEGER PRIMARY KEY,
    `exp` INTEGER
);