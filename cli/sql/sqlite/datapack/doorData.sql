DROP TABLE IF EXISTS `door_data`;

CREATE TABLE IF NOT EXISTS `door_data`
(
    `id` INTEGER PRIMARY KEY,
    `name` TEXT,
    `properties_json` TEXT
);