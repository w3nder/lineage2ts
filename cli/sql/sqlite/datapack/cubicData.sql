DROP TABLE IF EXISTS `cubic_data`;

CREATE TABLE IF NOT EXISTS `cubic_data`
(
    `id` INTEGER NOT NULL,
    `level` INTEGER NOT NULL,
    `delay` INTEGER NOT NULL,
    `maxActions` INTEGER NOT NULL,
    `power` INTEGER,
    `target_json` TEXT,
    `condition_json` TEXT,
    `skills_json` TEXT NOT NULL,
    PRIMARY KEY (`id`, `level`)
);