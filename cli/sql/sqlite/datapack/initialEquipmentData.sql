DROP TABLE IF EXISTS `initial_equipment`;
DROP TABLE IF EXISTS `initial_equipment_event`;

CREATE TABLE IF NOT EXISTS `initial_equipment` (
	`classId` INTEGER,
	`itemId` INTEGER,
	`count` INTEGER NOT NULL DEFAULT 1,
	`equipped` TEXT CHECK( equipped IN ('true','false') ) NOT NULL DEFAULT 'true'
);
CREATE TABLE IF NOT EXISTS `initial_equipment_event` (
    `classId` INTEGER,
    `itemId` INTEGER,
    `count` INTEGER NOT NULL DEFAULT 1,
    `equipped` TEXT CHECK( equipped IN ('true','false') ) NOT NULL DEFAULT 'true'
);