DROP TABLE IF EXISTS `augmentation_weapons`;

CREATE TABLE IF NOT EXISTS `augmentation_weapons`
(
    `classType` TEXT,
    `stoneId` INTEGER,
    `variationId` INTEGER,
    `categoryProbability` INTEGER,
    `chance_json` TEXT
);