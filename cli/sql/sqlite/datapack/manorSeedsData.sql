DROP TABLE IF EXISTS `manor_seeds`;

CREATE TABLE IF NOT EXISTS `manor_seeds`
(
    `cropId` INTEGER,
    `seedId` INTEGER,
    `castleId` INTEGER,
    `matureId` INTEGER,
    `rewardOne` INTEGER,
    `rewardTwo` INTEGER,
    `alternative` TEXT,
    `level` INTEGER,
    `seedLimit` INTEGER,
    `cropLimit` INTEGER
);