DROP TABLE IF EXISTS `ui_data`;

CREATE TABLE IF NOT EXISTS `ui_data`
(
    `categoryId` INTEGER PRIMARY KEY,
    `commands` TEXT,
    `keys_json` TEXT
);