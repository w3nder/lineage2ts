DROP TABLE IF EXISTS `xp_lost`;

CREATE TABLE IF NOT EXISTS `xp_lost`
(
    `level` INTEGER PRIMARY KEY,
    `value` REAL
);