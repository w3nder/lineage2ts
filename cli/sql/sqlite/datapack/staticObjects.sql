DROP TABLE IF EXISTS `static_objects`;

CREATE TABLE IF NOT EXISTS `static_objects`
(
    `id` INTEGER,
    `name` TEXT,
    `type` INTEGER,
    `x` INTEGER,
    `y` INTEGER,
    `z` INTEGER,
    `texture` TEXT,
    `mapX` INTEGER,
    `mapY` INTEGER
);