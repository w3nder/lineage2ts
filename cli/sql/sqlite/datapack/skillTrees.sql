DROP TABLE IF EXISTS `skill_trees`;

CREATE TABLE IF NOT EXISTS `skill_trees`
(
    `type` TEXT,
    `classId` INTEGER,
    `parentClassId` INTEGER,
    `skills_json` TEXT
);