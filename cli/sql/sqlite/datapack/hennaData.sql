DROP TABLE IF EXISTS `henna_data`;

CREATE TABLE IF NOT EXISTS `henna_data`
(
    `id` INTEGER PRIMARY KEY NOT NULL,
    `name` TEXT,
    `itemId` INTEGER,
    `level` INTEGER,
    `stats_json` TEXT,
    `wear_json` TEXT,
    `cancel_json` TEXT,
    `classId_json` TEXT
);