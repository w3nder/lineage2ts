DROP TABLE IF EXISTS `pc_karma_increase`;

CREATE TABLE IF NOT EXISTS `pc_karma_increase`
(
    `level` INTEGER,
    `value` REAL
);