DROP TABLE IF EXISTS `dimensional_rift_rooms`;

CREATE TABLE IF NOT EXISTS `dimensional_rift_rooms`
(
    `type` INTEGER NOT NULL,
    `roomId` INTEGER NOT NULL,
    `xMin` INTEGER NOT NULL,
    `xMax` INTEGER NOT NULL,
    `yMin` INTEGER NOT NULL,
    `yMax` INTEGER NOT NULL,
    `zMin` INTEGER NOT NULL,
    `zMax` INTEGER NOT NULL,
    `x` INTEGER NOT NULL,
    `y` INTEGER NOT NULL,
    `z` INTEGER NOT NULL,
    `hasBoss` INTEGER NOT NULL DEFAULT 0,
    PRIMARY KEY (`type`,`roomId`)
);