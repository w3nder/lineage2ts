DROP TABLE IF EXISTS `merchant_price_data`;

CREATE TABLE IF NOT EXISTS `merchant_price_data`
(
    `name` TEXT,
    `baseTax` REAL,
    `castleId` INTEGER,
    `townId` INTEGER
);