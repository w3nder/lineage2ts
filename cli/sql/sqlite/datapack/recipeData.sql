DROP TABLE IF EXISTS `recipe_data`;

CREATE TABLE IF NOT EXISTS `recipe_data`
(
    `id` INTEGER PRIMARY KEY,
    `recipeId` INTEGER,
    `name` TEXT,
    `craftLevel` INTEGER,
    `type` TEXT,
    `successRate` INTEGER,
    `productionId` INTEGER,
    `productionCount` INTEGER,
    `productionRareId` INTEGER,
    `productionRareCount` INTEGER,
    `productionRareRarity` INTEGER,
    `statUseName` TEXT,
    `statUseValue` INTEGER,

    `ingredients_json` TEXT
);