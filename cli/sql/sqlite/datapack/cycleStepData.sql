DROP TABLE IF EXISTS `cycle_step_data`;

CREATE TABLE IF NOT EXISTS `cycle_step_data`
(
    `id` INTEGER NOT NULL,
    `stepId` INTEGER NOT NULL,
    `pointLimit` INTEGER NOT NULL,
    `dropTime` INTEGER NOT NULL,
    `intervalPoint` INTEGER NOT NULL,
    `intervalTime` INTEGER NOT NULL,
    `lockTime` INTEGER NOT NULL,
    `mapId` INTEGER,
    `changeTimeCron` TEXT,
    `areaIdsJson` TEXT,
    `openDoorIdsJson` TEXT,
    `respawnPoint` TEXT,
    `respawnPvpPoint` TEXT,
    `respawnRangeJson` TEXT,
    `mapPointJson` TEXT,
    PRIMARY KEY (`id`, `stepId`)
);