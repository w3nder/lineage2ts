DROP TABLE IF EXISTS `levelup_crystal_npcs`;

CREATE TABLE IF NOT EXISTS `levelup_crystal_npcs`
(
    `npcId`        INTEGER PRIMARY KEY,
    `chances_json` TEXT
);