DROP TABLE IF EXISTS `servitor_skills`;

CREATE TABLE IF NOT EXISTS `servitor_skills`
(
    `npcId` INTEGER NOT NULL,
    `skillId` INTEGER NOT NULL,
    `skillLevel` INTEGER NOT NULL,
    PRIMARY KEY (`npcId`,`skillId`,`skillLevel`)
);