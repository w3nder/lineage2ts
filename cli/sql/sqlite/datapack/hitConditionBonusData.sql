DROP TABLE IF EXISTS `hitcondition_bonus`;

CREATE TABLE IF NOT EXISTS `hitcondition_bonus`
(
        `type` TEXT PRIMARY KEY,
        `value` INTEGER
);