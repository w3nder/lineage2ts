DROP TABLE IF EXISTS `agathions`;

CREATE TABLE IF NOT EXISTS `agathions`
(
    `npcId` INTEGER NOT NULL PRIMARY KEY,
    `itemId` INTEGER,
    `energy` INTEGER,
    `targetType` TEXT,
    `properties_json` TEXT
);